# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/vie-pratique/article/transports-111276#sommaire_1">Permis de conduire</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/vie-pratique/article/transports-111276#sommaire_2">Code de la route</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/vie-pratique/article/transports-111276#sommaire_3">Assurances et taxes</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/vie-pratique/article/transports-111276#sommaire_4">Achat et location</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/vie-pratique/article/transports-111276#sommaire_5">Entretien</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/vie-pratique/article/transports-111276#sommaire_6">Réseau routier</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/vie-pratique/article/transports-111276#sommaire_7">Transports en commun</a></li></ul>
<p>Les liaisons aériennes sont bonnes et couvrent l’ensemble du pays.</p>
<p>Il est déconseillé de circuler la nuit ou de se rendre dans des endroits très isolés par la route.</p>
<p>Le touriste qui compte voyager à l’intérieur du Pérou par voie routière doit savoir que les accidents sont nombreux en raison de l’état des routes (en montagne très sinueuses et étroites) et de la manière de conduire. Il est donc vivement recommandé de circuler de jour.</p>
<p>Si vous décidez de voyager en bus, il est préférable d’emprunter des compagnies connues.</p>
<p>En cas d’accident, il est important d’avoir immédiatement recours à un avocat.</p>
<p>Pour des raisons de sécurité, il n’est pas conseillé de prendre un taxi dans la rue (risque d’agression), il est préférable de commander un taxi par téléphone auprès des compagnies reconnues.</p>
<h3 class="spip"><a id="sommaire_1"></a>Permis de conduire</h3>
<p>Le permis de conduire français est valable seulement pendant trois mois à compter de la date d’entrée sur le territoire péruvien.</p>
<p>Le permis de conduire international est valable pour les séjours temporaires (moins de trois mois) et pour les non-résidents.</p>
<p>Pour les français résidents au Pérou, le permis français n’est pas reconnu. Il convient d’obtenir un permis péruvien auprès des autorités locales.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  site du <a href="http://www.mtc.gob.pe/portal/inicio.html" class="spip_out" rel="external">ministère des Transports péruvien</a>. </p>
<h3 class="spip"><a id="sommaire_2"></a>Code de la route</h3>
<p>La conduite s’effectue à droite et la priorité est également, en principe, à droite. Il faut prendre garde au mode de conduite très anarchique et dangereux, le code de la route n’étant pas respecté.</p>
<h3 class="spip"><a id="sommaire_3"></a>Assurances et taxes</h3>
<p>Il n’existe pas de vignette auto, la <i>Tarjeta</i>, équivalent de la carte grise et de la carte d’immatriculation, coûte environ 50 soles.</p>
<p>L’assurance "tiers-collision" est obligatoire, toutefois, nombre de Péruviens roulant sans assurance, il est recommandé de souscrire une assurance "tous risques".</p>
<p>Depuis 2009, un contrôle technique annuel (RTV) est obligatoire tous les trois ans pour les véhicules neufs et tous les ans pour les véhicules de plus de deux ans.</p>
<h3 class="spip"><a id="sommaire_4"></a>Achat et location</h3>
<p>Les marques françaises ou étrangères représentées sont Peugeot, Renault, Citroën, Volvo, Mercedes, BMW, Volkswagen, Fiat, Toyota, Nissan, Honda. Il est plus simple d’acheter un véhicule neuf ou d’occasion sur place. Les droits de douane sont d’environ 25% de la valeur vénale du véhicule.</p>
<p>En ville, toute voiture fait l’affaire. En province, le tout terrain constitue le meilleur choix. Il est conseillé d’utiliser des véhicules aux normes dites "tropicales" qui diffèrent selon les spécificités climatiques du pays ou de son relief. Les fabricants vous renseigneront utilement.</p>
<p>Il est possible d’acheter un véhicule d’occasion mais à un prix plus élevé qu’en France pour des véhicules comparables (de 30 à 50%).</p>
<p>Il est possible de louer des véhicules sur présentation d’une carte de crédit et à des coûts identiques à ceux pratiqués en Europe.</p>
<h3 class="spip"><a id="sommaire_5"></a>Entretien</h3>
<p>En cas de réparation du véhicule, la main d’œuvre est meilleur marché qu’en France. Par contre les pièces détachées sont beaucoup plus onéreuses et les délais de livraison sont assez aléatoires.</p>
<h3 class="spip"><a id="sommaire_6"></a>Réseau routier</h3>
<p>Les villes principales sont reliées entre elles par des routes en général bien asphaltées mais souvent étroites et très sinueuses en montagne.</p>
<h3 class="spip"><a id="sommaire_7"></a>Transports en commun</h3>
<p>A Lima, il n’existe pour l’instant, qu’une ligne de métro aérien (<i>tren electrico</i>) et un système d’autobus par voie rapide (<i>metropolitano</i>).</p>
<p>La circulation d’autobus et de mini-bus (<i>combis</i>) reste chaotique et parfois dangereuse.</p>
<p>Le prix des taxis en ville est abordable (pas de tarifs fixes, il faut négocier le prix avant la course). Pour des raisons de sécurité, il est préférable de commander un taxi par téléphone auprès des compagnies reconnues et de ne pas héler de taxi dans la rue surtout de nuit.</p>
<p>Le réseau ferroviaire pour le transport de passagers est peu développé au Pérou (Arequipa- Puno -Cusco-Quillabamba/ Lima-Huancayo/ Huancayo-Huancavelica). La circulation n’étant pas régulière, il est conseillé de consulter à l’avance les horaires.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/vie-pratique/article/transports-111276). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
