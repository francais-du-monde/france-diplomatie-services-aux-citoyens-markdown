# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/norvege/emploi-stage/article/reglementation-du-travail-111205#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/norvege/emploi-stage/article/reglementation-du-travail-111205#sommaire_2">Contrat de travail – spécificités</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/norvege/emploi-stage/article/reglementation-du-travail-111205#sommaire_3">Fêtes légales</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/norvege/emploi-stage/article/reglementation-du-travail-111205#sommaire_4">Emploi du conjoint</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<p>Les droits et les devoirs des salariés et des employeurs sont définis en Norvège par une <strong>Charte sur l’environnement de travail</strong>. Cette charte, traduite en anglais, est en vente dans toutes les librairies du pays.</p>
<p>L’administration concernée, <a href="http://www.arbeidstilsynet.no/" class="spip_out" rel="external">Inspection du travail</a> a un site comportant de nombreuses informations.</p>
<p>La durée légale du travail est de 37,7 heures maximum par semaine. Les horaires de travail les plus fréquents sont 8h-16h. Dans certains secteurs (industrie), la journée de travail peut débuter à 7 heures. Les heures effectuées au delà de 40 heures sont des heures supplémentaires rémunérées d’au moins 40% de plus lorsqu’elles sont imposées. A noter qu’en matière de rémunération des heures supplémentaires, il n’existe pas de dispositions légales pour les cadres.</p>
<p>D’une manière générale, les droits à congés annuels sont de 25 jours.</p>
<h3 class="spip"><a id="sommaire_2"></a>Contrat de travail – spécificités</h3>
<p>Tout emploi, qu’il soit à durée indéterminée ou temporaire, doit faire l’objet d’un contrat de travail. Une période d’essai ne peut excéder six mois.</p>
<p>Des exemples officiels de contrats sont disponibles sur le site de l’Inspection du travail.</p>
<h3 class="spip"><a id="sommaire_3"></a>Fêtes légales</h3>
<ul class="spip">
<li>Jour de l’an</li>
<li>Dimanche des Rameaux</li>
<li>Jeudi et vendredi saint</li>
<li>Dimanche et lundi de Pâques</li>
<li>1er mai</li>
<li>17 mai : fête nationale (Anniversaire de la constitution)</li>
<li>Ascension</li>
<li>Dimanche et lundi de Pentecôte</li>
<li>Noël et Lendemain de Noël.</li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Emploi du conjoint</h3>
<p>Pas de conditions spécifiques, se reporter aux différentes rubriques de l’emploi.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/norvege/emploi-stage/article/reglementation-du-travail-111205). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
