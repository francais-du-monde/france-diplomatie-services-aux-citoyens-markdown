# Animaux domestiques

<p>Il n’existe pas de texte spécifique au Vietnam pour l’importation d’animaux domestiques (chiens et chats).</p>
<p>Des informations sur l’importation des animaux au Vietnam sont disponibles sur le site du <a href="http://www.consulfrance-hcm.org/article.php3?id_article=802" class="spip_out" rel="external">Consulat de France</a> à Ho Chi Minh Ville.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Notre article thématique <a href="services-aux-citoyens-preparer-son-expatriation-douanes-article-animaux-domestiques.md" class="spip_in">Animaux domestiques</a></p>
<p><i>Mise à jour : février 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/entree-et-sejour/article/animaux-domestiques-111358). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
