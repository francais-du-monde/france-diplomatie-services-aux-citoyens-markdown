# Passeport, visa, permis de travail

<p>Les ressortissants d’un Etat membre de l’Union européenne peuvent se rendre dans un autre pays de l’Union européenne munis d’une carte nationale d’identité ou d’un passeport en cours de validité. Ils ont le droit d’y exercer un emploi dans les mêmes conditions que les nationaux du pays.</p>
<p><strong>Il est important de noter qu’il faut impérativement s’inscrire auprès de l’administration fiscale. Cette démarche consiste à faire une demande de numéro personnel pour les services publics (PPS) en se présentant en personne dans un bureau local de l’aide sociale</strong> (<i>Social Welfare</i>). Ensuite, vous devrez remplir une fiche REG 1 et joindre les documents attestant de votre identité. Vous serez avisé de votre numéro PPS par une lettre envoyée à l’adresse indiquée sur la fiche.</p>
<p>Pour toute information relative aux conditions de séjour en Irlande, il est vivement conseillé de s’adresser directement à la section consulaire de <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-etrangers-en-france/article/annuaire-des-representations-etrangeres-en-france" class="spip_in">l’ambassade d’Irlande en France</a>.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Le <a href="http://www.citizensinformation.ie/categories" class="spip_out" rel="external">portail officiel du service public irlandais</a>. Ce site propose des fiches pratiques en français sur l’expatriation.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/entree-et-sejour/article/passeport-visa-permis-de-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
