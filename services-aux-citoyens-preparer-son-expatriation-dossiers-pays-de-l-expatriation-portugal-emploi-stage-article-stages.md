# Stages

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/emploi-stage/article/stages#sommaire_1">Stage d’étude</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/emploi-stage/article/stages#sommaire_2">Stage professionnel</a></li></ul>
<p>Le Portugal étant membre de l’Union européenne, il est possible d’y effectuer un stage d’étude ou professionnel en bénéficiant des différents programmes communautaires et des accords bilatéraux.</p>
<h3 class="spip"><a id="sommaire_1"></a>Stage d’étude</h3>
<h4 class="spip">Le programme Erasmus</h4>
<p>Le programme <strong>Erasmus</strong> s’adresse aux étudiants désireux de suivre un enseignement à l’étranger, en leur offrant la possibilité d’intégrer une université étrangère pour une durée comprise entre 3 et 12 mois, ou au minimum un trimestre universitaire complet. Ils peuvent aussi dans ce cadre effectuer un stage.</p>
<p><strong>Les candidats doivent remplir les conditions suivantes :</strong></p>
<ul class="spip">
<li>appartenir à un établissement scolaire titulaire de la charte <strong>Erasmus</strong></li>
<li>être un ressortissant de l’un des pays participant au programme d’éducation et de formation tout au long de la vie ou d’un autre pays inscrit à une formation menant à un diplôme reconnu dans un établissement d’enseignement supérieur</li>
<li>être inscrit au moins en deuxième année d’études supérieures</li>
<li>le pays d’envoi ou le pays d’accueil doit être un Etat membre de l’Union européenne.</li></ul>
<p>Les étudiants peuvent obtenir une bourse <strong>Erasmus</strong> pour les aider à couvrir les frais de voyage et de séjour (y compris frais d’assurance) occasionnés au cours de leur période d’études à l’étranger. Ils sont exemptés du paiement des droits d’inscription, de scolarité, d’examens et d’accès à la bibliothèque et aux laboratoires dans l’établissement d’accueil. Une subvention peut être accordée à l’étudiant pour lui permettre de suivre des cours de langues avant son départ.</p>
<p><strong>A savoir :</strong> le paiement de bourses ou de prêts au niveau national aux étudiants qui partent à l’étranger est maintenu pendant la période d’étude <strong>Erasmus</strong> à l’étranger.</p>
<p>Le programme s’adresse non seulement aux étudiants, mais aussi aux professeurs et employés d’entreprise qui souhaitent enseigner à l’étranger, ainsi qu’au personnel universitaire désireux de bénéficier d’une formation à l’étranger.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://ec.europa.eu/education/erasmus/study_fr.htm" class="spip_out" rel="external">Ec.europa.eu</a></li>
<li><a href="http://www.europe-education-formation.fr/page/erasmus" class="spip_out" rel="external">Europe-education-formation.fr</a></li></ul>
<h4 class="spip">Le programme PESSOA</h4>
<p>Ce programme est le Partenariat Hubert Curien (PHC) franco-portugais. Il est mis en œuvre au Portugal par la Fondation pour la science et la technologie (FCT - Département des relations européennes, bilatérales et internationales).</p>
<p>Son objectif est de développer les échanges scientifiques et technologiques d’excellence entre les laboratoires de recherche des deux pays en favorisant les nouvelles coopérations. Il concerne tous les domaines scientifiques, y compris les sciences humaines et sociales.</p>
<h5 class="spip">Critères concernant l’équipe constituée</h5>
<p>L’appel à candidatures est ouvert aux laboratoires de recherche rattachés à des établissements d’enseignement supérieur, à des organismes de recherche ou à des entreprises.</p>
<p>L’implication dans les activités de recherche de doctorants et/ou post-doctorants sous la responsabilité du chef de projet est un critère important de sélection, la formation à et par la recherche étant essentielle.</p>
<h5 class="spip">Comité de sélection des projets</h5>
<p>Les appels à candidatures de ce programme sont lancés sur un rythme annuel. Les instances des deux pays se réunissent, alternativement au Portugal et en France, pour confronter les évaluations et décider conjointement du soutien accordé aux projets sélectionnés</p>
<h5 class="spip">Modalités de fonctionnement</h5>
<ul class="spip">
<li>Le <strong>financement</strong> est accordé sur une base annuelle, pour deux années consécutives ;</li>
<li>Il doit être impérativement consommé entre le 1er janvier et le 31 décembre de l’année concernée et ne peut être reporté sur l’exercice suivant ;</li>
<li>Il porte uniquement sur la prise en charge de la mobilité entre les deux pays des chercheurs engagés dans le programme ;</li>
<li>Tout autre financement nécessaire à la mise en œuvre des projets conjoints devra être assuré par les moyens propres des laboratoires partenaires ou par d’autres sources ;</li>
<li>Le <strong>renouvellement</strong> des crédits pour une seconde année est subordonné à une <strong>consommation optimale des financements</strong> accordés pour la première année et à la <strong>production obligatoire d’un rapport d’étape</strong> avant le 15 novembre de l’année en cours. Ce rapport doit mentionner :
<br>— les résultats scientifiques obtenus (une page en format libre) ;
<br>— un <strong>bilan financier</strong> des actions menées ou programmées avant la fin de l’année civile.</li></ul>
<p>Les moyens accordés par la France couvrent exclusivement le paiement des indemnités de séjour et les voyages des chercheurs français :</p>
<ul class="spip">
<li>indemnités de séjour : 160 euros/jour,</li>
<li>voyages : remboursement sur frais réels dans la limite de 500 euros.</li></ul>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.egide.asso.fr/jahia/Jahia/site/egide/lang/fr/pessoa" class="spip_out" rel="external">Egide.asso.fr</a></li>
<li><a href="http://www.campusfrance.org/fr/pessoa" class="spip_out" rel="external">Campusfrance.org</a></li></ul>
<h4 class="spip">Le programme d’actions universitaires intégrées luso-françaises (PAUILF)</h4>
<p>Le <strong>PAUILF</strong>, lancé par accord entre la Conférence des présidents d’université (CPU) et le Conseil des recteurs des universités portugaises (CRUP) en 2001, est destiné à engager des échanges académiques et scientifiques entre les deux pays. Ses objectifs ont été redéfinis en 2006.</p>
<p>Le <strong>PAUILF</strong> soutient la mise en place de formations en partenariat menant à la délivrance de diplômes conjoints ou de doubles diplômes (notamment au niveau master) et l’accompagnement de thèses en cotutelles. Il prévoit pour cela des financements dont la durée se limite à trois ans, et concerne l’ensemble des champs disciplinaires.</p>
<p>Le <strong>PAUILF</strong> est complémentaire et compatible avec le Partenariat Hubert Curien – <strong>PESSOA</strong> :</p>
<ul class="spip">
<li>les porteurs d’un projet de formation en partenariat ou d’une cotutelle de thèse soutenus par le <strong>PAUILF</strong> seront éligibles au PHC - <strong>PESSOA</strong> pour un projet de recherches conjointes ;</li>
<li>un projet universitaire de recherche retenu dans le cadre d’un PHC - <strong>PESSOA</strong> peut, à l’issue ou au cours de sa mise en œuvre, solliciter un soutien dans le cadre du <strong>PAUILF</strong> pour construire un master en partenariat ou pour conduire une thèse en cotutelle.</li></ul>
<p><strong>Candidatures :</strong></p>
<p>Pour pouvoir être retenus, les projets doivent être présentés simultanément dans les deux pays avec la signature du chef d’établissement (président ou directeur d’école). L’université française doit s’assurer que l’université portugaise avec laquelle elle collabore présente le même projet au CRUP. Les projets doivent être rédigés en français pour la partie française et en portugais pour la partie portugaise. Des établissements habilités dans leurs pays respectifs à délivrer le diplôme proposé dans le cadre du projet pourront se porter candidats.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.ifp-lisboa.com/index.php/detail-universitaire/items/135.html" class="spip_out" rel="external">Ifp-lisboa.com</a></li>
<li><a href="http://www.portugal.campusfrance.org/fr" class="spip_out" rel="external">Portugal.campusfrance.org</a></li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Stage professionnel</h3>
<h4 class="spip">Le programme Leonardo da Vinci</h4>
<p>Le programme <strong>Leonardo da Vinci</strong> est l’un des programmes sectoriels du programme <strong>Education et formation tout au long de la vie</strong>. Il est conçu comme un outil chargé de donner une impulsion aux systèmes de formation et d’enseignement professionnels en Europe dans leur diversité.</p>
<p>Ce programme offre une large palette d’activités et permet la mobilité des personnes désireuses d’acquérir une expérience professionnelle dans un autre pays de l’Union européenne. Il propose notamment aux apprentis ou aux étudiants des stages dans des entreprises européennes.</p>
<p>Il facilite les échanges de bonnes pratiques entre responsables de formation et constitue aussi un levier de choix pour mettre en place de nouveaux outils et méthodes de formation, afin d’étendre leur application au niveau européen.</p>
<p>Le public concerné : lycéens, apprentis, formateurs, salariés en formation, entreprises et tous types d’organismes, publics ou privés, acteurs de la formation professionnelle.</p>
<p>Les actions de « mobilité » permettent aux personnes de se rendre à l’étranger pour vivre une expérience d’apprentissage ou de formation. Il en existe plusieurs types :</p>
<ul class="spip">
<li>les personnes qui suivent une formation professionnelle initiale peuvent effectuer un stage à l’étranger. Les participants peuvent être encore scolarisés ou dépendre d’un dispositif d’enseignement et de formation professionnels (apprentis) ;</li>
<li>les actions « Personnes sur le marché du travail » permettent aux titulaires d’un diplôme de l’enseignement professionnel ou supérieur d’effectuer un stage professionnel à l’étranger afin d’améliorer leurs chances de trouver un emploi ;</li>
<li>les professionnels de l’enseignement et de la formation professionnels (« PRO EFP ») peuvent échanger leurs expériences à l’étranger pour améliorer leurs compétences et approfondir leurs connaissances ;</li>
<li>les visites préparatoires permettent aux personnes travaillant dans le secteur de l’enseignement et de la formation professionnelle de se rendre à l’étranger pour rencontrer des partenaires et planifier un projet dans le cadre du programme.</li></ul>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://ec.europa.eu/education/tools/llp_en.htm" class="spip_out" rel="external">Ec.europa.eu</a></p>
<h4 class="spip">Le programme Comenius</h4>
<p>Relevant de programmes pour l’éducation et la formation tout au long de la vie de l’Union européenne, les actions <strong>Comenius</strong> visent à aider les jeunes et le personnel éducatif à mieux comprendre la diversité des cultures, des langues et des valeurs européennes. Elles aident également les jeunes à acquérir les qualifications et les compétences de base nécessaires à leur développement personnel, à leur activité professionnelle future et à une citoyenneté active.</p>
<p><strong>Comenius vise à :</strong></p>
<ul class="spip">
<li>améliorer et à accroître la mobilité des élèves et du personnel éducatif dans toute l’Union européenne ;</li>
<li>optimiser et développer les partenariats entre les écoles des différents États membres avec la participation d’au moins trois millions d’élèves à des activités éducatives conjointes d’ici 2010 ;</li>
<li>encourager l’apprentissage des langues, l’utilisation de contenus et de services novateurs axés sur les TIC et enfin de meilleures techniques et pratiques d’enseignement ;</li>
<li>améliorer la qualité et la dimension européenne de la formation des enseignants ;</li>
<li>améliorer les approches pédagogiques et la gestion des écoles.</li></ul>
<p>Les actions de "mobilité" <strong>Comenius</strong> permettent à des individus de se rendre à l’étranger pour participer à l’un des projets suivants :</p>
<ul class="spip">
<li><strong>mobilité individuelle des élèves :</strong> offrant aux élèves de l’enseignement secondaire la possibilité d’effectuer un séjour d’études à l’étranger de dix mois maximum ;</li>
<li><strong>bourses pour la formation continue destinée au personnel :</strong> permettant aux enseignants et autres membres du personnel éducatif de suivre une formation à l’étranger, pour une durée maximale de six semaines ;</li>
<li><strong>bourses d’assistanat :</strong> permettant aux futurs enseignants de passer dix mois maximum à travailler dans une école à l’étranger.</li></ul>
<p><strong>Comment participer :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://ec.europa.eu/education/comenius/participate_fr.htm" class="spip_out" rel="external">Ec.europa.eu</a></p>
<h4 class="spip">Volontariat international</h4>
<h5 class="spip">Le volontariat international en entreprise (VIE)</h5>
<p>Le VIE permet aux entreprises françaises de confier à de jeunes ressortissants de l’Espace économique européen (principalement des étudiants, des jeunes diplômés et des chercheurs d’emploi de nationalité française), âgés de 18 à 28 ans, une mission professionnelle rémunérée à l’étranger durant une période modulable de 6 à 24 mois.</p>
<h5 class="spip">Le volontariat international en administration (VIA)</h5>
<p>Le VIA repose sur le même principe que le VIE, mais place le candidat au sein d’une structure française, publique ou parapublique, relevant du ministère des affaires étrangères ou du ministère de l’économie, dans une structure publique locale étrangère (centres de recherche et universités publiques), ou auprès d’organisations internationales ou d’associations agréées.</p>
<p>Les VIE sont placés sous la tutelle de l’ambassade et perçoivent une indemnité mensuelle composée d’une partie fixe (634,80 euros) et d’une partie variable selon le pays d’affectation.</p>
<p><strong>Exemples de secteurs concernés : </strong></p>
<ul class="spip">
<li>en entreprise (VIE) : finances, marketing, commerce international, contrôle de gestion, comptabilité, mécanique, électronique, télécommunications, informatique, BTP, agronomie, tourisme, droit, ressources humaines…</li>
<li>en administration (VIA) : animation culturelle, enseignement, veille économique, commerciale ou scientifique, informatique, sciences politiques, droit, économie, recherche, médecine, hôtellerie-restauration…</li></ul>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li><a href="http://www.civiweb.com/" class="spip_out" rel="external">Civiweb.com</a></li>
<li><a href="http://export.businessfrance.fr/default.html" class="spip_out" rel="external">Business France.fr</a></li></ul>
<h4 class="spip">Le programme Grundtvig</h4>
<p>Le programme <strong>Grundtvig</strong> vise à offrir aux adultes des moyens d’améliorer leurs connaissances et compétences, facilitant leur développement personnel et renforçant leurs perspectives d’emploi.</p>
<p>Il couvre non seulement les enseignants, les formateurs, le personnel éducatif et les organisations proposant ces services, mais aussi les apprenants de l’éducation et de la formation des adultes. Parmi ceux-ci figurent les associations, les services d’orientation et d’information, les organes d’élaboration des politiques ainsi que d’autres organismes, tels que les ONG, les entreprises, les groupes de volontaires et les centres de recherche.</p>
<p>Le programme finance un éventail d’activités, en particulier celles qui soutiennent les études à l’étranger des personnes impliquées dans l’éducation des adultes, par l’intermédiaire d’échanges et de diverses autres expériences professionnelles. Parmi les autres initiatives de grande envergure, citons le réseautage et les partenariats entre les organisations de différent pays.</p>
<p><strong>Quelques exemples d’activités financées :</strong> <a href="http://ec.europa.eu/education/grundtvig/what_fr.htm" class="spip_out" rel="external">Ec.europa.eu</a></p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/emploi-stage/article/stages). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
