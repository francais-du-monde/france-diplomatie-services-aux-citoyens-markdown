# Protection sociale

<h2 class="rub22910">Régime local de sécurité sociale</h2>
<h4 class="spip">Régime local de sécurité sociale</h4>
<p>Vous trouverez une présentation détaillée du système de sécurité sociale tunisienne sur le site de notre partenaire, le <a href="http://www.cleiss.fr/" class="spip_out" rel="external">CLEISS</a> (Centre des liaisons européennes et internationales de sécurité sociale). En voici la table des matières :</p>
<ul class="spip">
<li><a href="http://www.cleiss.fr/docs/regimes/regime_tunisie.html#a" class="spip_out" rel="external">Généralités</a> (Organisation, financement, taux de cotisation sur les salaires…)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_tunisie.html#b" class="spip_out" rel="external">Financement</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_tunisie.html#c" class="spip_out" rel="external">Prestations familiales</a> (Allocations familiales, de naissance, d’adoption…)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_tunisie.html#d" class="spip_out" rel="external">Les assurances sociales</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_tunisie.html#e" class="spip_out" rel="external">Pensions de vieillesse, d’invalidité et de survivants dans le secteur non agricole</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_tunisie.html#f" class="spip_out" rel="external">Accidents du travail - maladies professionnelles</a> (Régime indemnitaire, y compris pour les survivants)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_tunisie.html#g" class="spip_out" rel="external">Chômage</a> (Indemnités et assistance chômage)</li></ul>
<p>Ci-après, tableau récapitulatif des prestations sociales en Tunisie :</p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<th id="id6c28_l0">ASSURANCE</th>
<td headers="id6c28_l0"><strong>NATURE DES PRESTATIONS</strong> (indemnités journalières, pension, capital…)</td>
<td headers="id6c28_l0"><strong>BASE DE CALCUL</strong></td>
<td headers="id6c28_l0"><strong>MONTANT MAXIMUM DE L’AVANTAGE</strong> <strong>(Indiquez la devise : TND)</strong></td></tr>
<tr class="row_even even">
<th id="id6c28_l1">MALADIE</th>
<td headers="id6c28_l1">Indemnités journalières</td>
<td headers="id6c28_l1">2/3 du salaire ou revenu journalier moyen (réglé à partir du 6 ème jour d’arrêt)</td>
<td headers="id6c28_l1">Plafonné à 2 fois le SMIG (*)</td></tr>
<tr class="row_odd odd">
<th id="id6c28_l2">MATERNITÉ</th>
<td headers="id6c28_l2">Indemnités de couche 2 mois maximum à compter de la date de l’accouchement</td>
<td headers="id6c28_l2">2/3 du salaire ou revenu journalier  moyen</td>
<td headers="id6c28_l2">Plafonné à 2 fois le SMIG (*)</td></tr>
<tr class="row_even even">
<th id="id6c28_l3">ACCIDENTS DU TRAVAIL – MALADIES PROFESSIONNELLES</th>
<td headers="id6c28_l3">Indemnités journalières</td>
<td headers="id6c28_l3">2/3 du salaire journalier moyen (référence du salaire le plus élevé des 4 derniers trimestres)</td>
<td headers="id6c28_l3">Non plafonné</td></tr>
<tr class="row_odd odd">
<th id="id6c28_l4">INVALIDITÉ</th>
<td headers="id6c28_l4">Pension</td>
<td headers="id6c28_l4">Minimum : la moitié ou les 2/3 du SMIG Selon les cas</td>
<td headers="id6c28_l4"></td></tr>
<tr class="row_even even">
<th id="id6c28_l5">DÉCÈS – VEUVAGE – SURVIE</th>
<td headers="id6c28_l5">Capital ou pension</td>
<td headers="id6c28_l5">Capital 30 mensualités de rémunération Au maximum, éventuellement majorées Pour enfant à charge. Pension : la moitié ou les 2/3 du SMIC Selon les cas</td>
<td headers="id6c28_l5"></td></tr>
<tr class="row_odd odd">
<th id="id6c28_l6">VIEILLESSE</th>
<td headers="id6c28_l6">Pension</td>
<td headers="id6c28_l6">Rémunération afférente à la fonction la plus élevée effectivement exercée pendant 2 ans</td>
<td headers="id6c28_l6"></td></tr>
<tr class="row_even even">
<th id="id6c28_l7">CHÔMAGE</th>
<td headers="id6c28_l7">Néant</td>
<td headers="id6c28_l7">Néant</td>
<td headers="id6c28_l7">Néant</td></tr>
<tr class="row_odd odd">
<th id="id6c28_l8">PRESTATIONS FAMILIALES</th>
<td headers="id6c28_l8">Allocations</td>
<td headers="id6c28_l8">Forfaitaire en fonction du nombre  d’enfants</td>
<td headers="id6c28_l8">1er enfant : 7,320 dinars/mois 2ème enfant : 6,607 dt/mois 3ème enfant : 5,693 dt/mois</td></tr>
</tbody>
</table>
<p>(*) SMIG = 320 TND</p>
<p><i>Mise à jour : novembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-tunisie-protection-sociale-article-convention-de-securite-sociale-110524.md" title="Convention de sécurité sociale">Convention de sécurité sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-tunisie-protection-sociale-article-regime-local-de-securite-sociale.md" title="Régime local de sécurité sociale">Régime local de sécurité sociale</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/protection-sociale/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
