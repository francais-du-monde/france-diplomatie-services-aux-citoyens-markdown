# Entretien d’embauche

<p>Les principaux points de la préparation et du déroulement d’un entretien d’embauche sont développés sur le site internet <a href="http://www.rekrute.com/editorial_article.php?id=4741" class="spip_out" rel="external">Rekrute.com</a>, spécialisé sur les pays du Maghreb.</p>
<h4 class="spip">Négociation du salaire </h4>
<p>Lors des contacts avec l’employeur, il convient de demander toutes informations permettant de déterminer le salaire net, les avantages en nature, la couverture sociale…</p>
<p>La rémunération se détermine librement entre l’employeur et son futur salarié. Si vous négociez votre salaire dans le cadre d’un contrat local, il convient de prendre garde à la différence entre salaire brut et salaire net. Sachez qu’il est cependant interdit de conclure des contrats en rémunération nette d’impôts. Vous pouvez négocier un certain nombre d’avantages : en nature tels que le véhicule de fonction, prise en charge des voyages en France, prime d’expatriation, etc.</p>
<p>Le salaire net s’obtient par déduction du salaire brut des cotisations sociales (variables selon les entreprises) et de l’impôt sur le revenu retenu à la source.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/emploi-stage/article/entretien-d-embauche). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
