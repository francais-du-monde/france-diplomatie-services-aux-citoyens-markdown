# Entretien d’embauche

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/emploi-stage/article/entretien-d-embauche#sommaire_1">Conduite de l’entretien</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/emploi-stage/article/entretien-d-embauche#sommaire_2">Apparence et attitude</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/emploi-stage/article/entretien-d-embauche#sommaire_3">Négociation du salaire</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/emploi-stage/article/entretien-d-embauche#sommaire_4">Erreurs à éviter</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/emploi-stage/article/entretien-d-embauche#sommaire_5">Après l’entretien</a></li></ul>
<p>Préparez-vous à répondre aux questions concernant votre éducation, votre expérience, les raisons pour lesquelles vous êtes intéressé par l’emploi en question et les renseignements que vous détenez au sujet de l’entreprise. À cet effet, renseignez-vous au préalable sur l’entreprise. Votre interlocuteur peut vous poser des questions pertinentes sur l’emploi concerné. Il peut vous questionner sur votre caractère, vos points forts, vos points faibles, vos activités de loisirs et votre adhésion éventuelle à des clubs voire des organisations. Soyez également prêts à répondre aux questions concernant vos circonstances personnelles, notamment votre état civil. À la fin de l’entrevue, il sera peut-être opportun de poser à votre tour des questions. Préparez certaines questions au préalable.</p>
<p><i>Source : <a href="http://www.ccfs.se/" class="spip_out" rel="external">Chambre de commerce française en Suède</a></i></p>
<h3 class="spip"><a id="sommaire_1"></a>Conduite de l’entretien</h3>
<p>La conduite de votre entretien dépend de la personne qui vous interviewe. Il est commun de commencer avec une petite présentation de l’entreprise et du poste avant que la personne vous demande de vous présenter brièvement.</p>
<p>Votre CV peut servir de fil conducteur et le recruteur peut l’avoir sous les yeux pour passer en revue avec vous les différents points.</p>
<p>Le recruteur vous demandera de quelle manière vous pensez être adapté au poste.</p>
<p>L’entretien peut ensuite se poursuivre par une présentation plus personnelle, comme votre situation de famille ou vos loisirs.</p>
<h3 class="spip"><a id="sommaire_2"></a>Apparence et attitude</h3>
<p>En Suède, la ponctualité est très importante. Arrivez donc quelques minutes en avance à votre entretien.</p>
<p>Soignez votre tenue et votre apparence et habillez-vous selon le poste pour lequel vous postulez et la branche dans laquelle vous postulez. Comme dans toutes les situations, il vaut mieux être trop habillé que pas assez.</p>
<p>On se salue en se serrant la main et en annonçant son nom tout en regardant la personne franchement dans les yeux.</p>
<p>Restez concis, franc et direct.</p>
<p>Présentez-vous et parlez de vous-même de manière positive.</p>
<p>Ne partez pas sur des détails trop intimes ou personnels, restez très professionnel.</p>
<h3 class="spip"><a id="sommaire_3"></a>Négociation du salaire</h3>
<p>Faites vos recherches sur le salaire minimum pour le type de poste que vous recherchez, la branche, ou les personnes de votre qualification et de votre expérience et sachez exactement à quel salaire vous pouvez prétendre</p>
<p>Attendez qu’on vous questionne sur vos prétentions salariales</p>
<p>Donnez alors de façon exacte votre salaire actuel, bénéfices, etc. et annoncez que selon vos recherches, vous pouvez prétendre à tel ou tel salaire annuel.</p>
<h3 class="spip"><a id="sommaire_4"></a>Erreurs à éviter</h3>
<p>Eviter de trop « vous vendre » et de vous concentrer sur ce que vous avez fait par le passé et ce que vous savez faire.</p>
<p>Restez humble et discret.</p>
<p>Attendez que la personne vous interroge.</p>
<p>Ne parlez jamais mal d’un employeur précédent.</p>
<h3 class="spip"><a id="sommaire_5"></a>Après l’entretien</h3>
<p>En Suède, on dit beaucoup par courtoisie que c’était un plaisir de se rencontrer et qu’on a apprécié l’entretien, et on remercie la personne pour le temps qu’elle nous a consacré.</p>
<p>N’hésitez pas à demander quand vous pouvez vous attendre à une réponse de la part du recruteur.</p>
<p>Voici quelques sites en suédois où vous trouverez des conseils :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.intervjuaren.se/" class="spip_out" rel="external">Intervjuaren.se</a></p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/emploi-stage/article/entretien-d-embauche). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
