# Présence française

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/presence-francaise/article/presence-francaise#sommaire_1">Ambassade et consulat de France</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/presence-francaise/article/presence-francaise#sommaire_2">Autorités françaises dans le pays</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/presence-francaise/article/presence-francaise#sommaire_3">Associations dans le pays</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Ambassade et consulat de France</h3>
<p>Consulter notre <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/article/annuaire-des-representations-francaises-a-l-etranger" class="spip_in">Annuaire des représentations françaises à l’étranger</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Autorités françaises dans le pays</h3>
<h4 class="spip">Réseau français de coopération et d’action culturelle</h4>
<p>Les coordonnées des services du réseau de coopération et d’action culturelle français en Belgique ainsi que la description de l’activité de ces services se trouvent dans la rubrique culture du site internet de l’Ambassade ou sur le site internet du réseau culturel : <a href="http://www.latitudefrance.org/AnnuaireCarto.html" class="spip_out" rel="external">Latitude France</a>.</p>
<h4 class="spip">Economie : réseau mondial</h4>
<p>Business France, l’Agence française pour le développement international des entreprises, <a href="http://export.businessfrance.fr/belgique/export-belgique-avec-notre-bureau.html" class="spip_out" rel="external">est présente en Belgique</a>. Sa mission est d’accompagner les entreprises françaises dans leur développement à l’international.</p>
<p>Les services économiques sont également présents. Ils sont une émanation de la Direction générale du Trésor et ont pour missions principales l’analyse macroéconomique et l’animation des relations économiques bilatérales entre la France et les pays où ils sont implantés.</p>
<p><a href="http://www.cfci.be/" class="spip_out" rel="external">Chambre françaises de commerce et d’industrie de Belgique</a>  (CFCI)<br class="manualbr">Artemis Square - Avenue des Arts 8 - 1210 Bruxelles<br class="manualbr">Téléphone : [32] 2 506 88 12 <br class="manualbr">Télécopie : [32] 2 506 88 88<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/presence-francaise/article/presence-francaise#cfcib#mc#cfci.be#" title="cfcib..åt..cfci.be" onclick="location.href=mc_lancerlien('cfcib','cfci.be'); return false;" class="spip_mail">Courriel</a></p>
<p><strong>CFCI pour la région Flamande</strong><br class="manualbr">Résidence Balmoral - Hubert Frère Orbanlaan, 376 - 9000 Gand <br class="manualbr">Téléphone : (32 9) 223 65 03 <br class="manualbr">Télécopie : (32 9) 223 24 23 <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/presence-francaise/article/presence-francaise#info#mc#cfci2f.be#" title="info..åt..cfci2f.be" onclick="location.href=mc_lancerlien('info','cfci2f.be'); return false;" class="spip_mail">Courriel</a></p>
<p><strong>CFCI pour les provinces de Liège et de Luxembourg</strong><br class="manualbr">Rue Haute Sauvenière, 19 – 4000 Liège<br class="manualbr"><strong>Siège administratif</strong> : <br class="manualbr">Rue de Henne, 24 – B-4053 Chaudfontaine <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/presence-francaise/article/presence-francaise#alain.laroche#mc#skynet.be#" title="alain.laroche..åt..skynet.be" onclick="location.href=mc_lancerlien('alain.laroche','skynet.be'); return false;" class="spip_mail">Courriel</a></p>
<h4 class="spip">Vos élus à l’Assemblée des Français de l’étranger</h4>
<p>Pour toute information sur l’Assemblée des Français de l’étranger et pour connaître les conseillers et les Sénateurs qui représentent les Français établis hors de France dans votre circonscription, vous pouvez consulter les sites Internet suivants :</p>
<ul class="spip">
<li><a href="http://www.assemblee-afe.fr/" class="spip_out" rel="external">Assemblée des Français de l’étranger</a> (AFE)</li>
<li>Sénat au service des Français de l’étranger : dossier <a href="http://www.senat.fr/expatries/dossiers_pays/belgique.html" class="spip_out" rel="external">Belgique</a> sur le site du Sénat</li></ul>
<h4 class="spip">Députés des Français de l’étranger</h4>
<p>La révision constitutionnelle du 23 juillet 2008, publiée au <a href="http://legifrance.gouv.fr/affichTexte.do;jsessionid=13C5361DC343DC77C54680D4BB94CF42.tpdjo11v_2?cidTexte=JORFTEXT000023877109&amp;dateTexte=&amp;oldAction=rechJO&amp;categorieLien=id" class="spip_out" rel="external">Journal officiel du 19 avril 2011</a>, introduit la représentation à l’Assemblée nationale des Français établis hors de France. La création de onze circonscriptions législatives à l’étranger permet aux Français expatriés d’élire leur député à l’Assemblée nationale à partir du scrutin de 2012.</p>
<p>Pour plus d’information, consultez le site <a href="http://www.votezaletranger.gouv.fr" class="spip_out" rel="external">Votez à l’étranger</a></p>
<h3 class="spip"><a id="sommaire_3"></a>Associations dans le pays</h3>
<h4 class="spip">Associations françaises</h4>
<p><a href="http://www.adfe.org/belgique" class="spip_out" rel="external">Association démocratique des Français de l’étranger</a> (ADFE)</p>
<p><a href="http://www.ufe.be/" class="spip_out" rel="external">Union des Français à l’étranger</a> (UFE)<br class="manualbr">98/8 Avenue du Bois de la Cambre<br class="manualbr">B-1050 Bruxelles<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/presence-francaise/article/presence-francaise#bruxelles#mc#ufe.be#" title="bruxelles..åt..ufe.be" onclick="location.href=mc_lancerlien('bruxelles','ufe.be'); return false;" class="spip_mail">Courriel</a></p>
<p><strong>Fédération internationale des accueils français à l’étranger (FIAFE)</strong></p>
<p><a href="http://www.accueil-bruxelles.be" class="spip_out" rel="external">Accueil des Françaises à Bruxelles</a><br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/presence-francaise/article/presence-francaise#accueilbruxelles#mc#gmail.com#" title="accueilbruxelles..åt..gmail.com" onclick="location.href=mc_lancerlien('accueilbruxelles','gmail.com'); return false;" class="spip_mail">Courriel</a></p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li>Consulter la thématique <a href="http://www.consulfrance-bruxelles.org/-Associations-francaises-" class="spip_out" rel="external">Associations françaises</a> sur le site du Consulat général de France à Bruxelles.</li>
<li>Consulter la thématique <a href="http://www.ambafrance-be.org/Associations-francaises" class="spip_out" rel="external">Associations françaises</a> sur le site de l’Ambassade de France en Belgique</li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-en-savoir-plus-article-associations-des-francais-de-l-etranger.md" class="spip_in">Associations des Français de l’étranger</a></li></ul>
<p><i>Mise à jour : septembre 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/presence-francaise/article/presence-francaise). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
