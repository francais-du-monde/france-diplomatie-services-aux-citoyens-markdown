# Fiscalité

<h2 class="rub23049">Fiscalité du pays</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/fiscalite-23049/#sommaire_1">Présentation </a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/fiscalite-23049/#sommaire_2">Année fiscale </a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/fiscalite-23049/#sommaire_3">Barème de l’impôt </a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/fiscalite-23049/#sommaire_4">Quitus fiscal </a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/fiscalite-23049/#sommaire_5">Solde du compte en fin de séjour </a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/fiscalite-23049/#sommaire_6">Coordonnées des centres d’information fiscale </a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Présentation </h3>
<h4 class="spip">Notion de résidence</h4>
<p>La fiscalité des personnes physiques est associée à la notion de résidence :</p>
<ul class="spip">
<li>sont considérés comme résidents, les étrangers qui justifient d’un séjour égal ou supérieur à 183 jours au Vietnam dans une période de 12 mois à compter de leur première entrée dans le pays.</li>
<li>sont considérés comme non-résidents, les étrangers séjournant sur le territoire vietnamien moins de 183 jours pendant une période de 12 mois consécutifs à compter de la date de leur première entrée au Vietnam.</li></ul>
<h4 class="spip">Modalités de paiement des impôts</h4>
<p>A titre de principe, l’impôt sur le revenu est <strong>retenu à la source</strong> par l’employeur pour le compte de l’employé (à opposer au régime de la « déclaration spontanée » applicable en France).</p>
<p>C’est à dire que l’impôt est déclaré et payé mensuellement ou trimestriellement de façon provisoire. Au terme de l’année (soit, en l’état, avant le 28 février de l’année suivante), une déclaration de régularisation doit être effectuée (permettant de déterminer le taux effectivement applicable au revenu annuel global).</p>
<p>Les revenus exceptionnels sont imposables à leur date de perception.</p>
<p>S’agissant de l’assiette de l’impôt, le montant mensuel imposable correspond à la totalité du revenu mondial. Les avantages en nature sont généralement imposés à l’exception de certaines catégories spécifiées dans l’ordonnance sur la fiscalité des revenus les plus élevés (l’imposition sur les logements de fonction est calculée en fonction du loyer réellement payé par l’employeur et ne doit pas excéder 15% du revenu imposable global).</p>
<p>Les taux d’imposition applicables aux étrangers résidant au Vietnam sont progressifs selon le barème indiqué dans le tableau ci-dessous (différent de celui applicable aux Vietnamiens).</p>
<h3 class="spip"><a id="sommaire_2"></a>Année fiscale </h3>
<p>L’année fiscale est alignée sur l’année civile.</p>
<h3 class="spip"><a id="sommaire_3"></a>Barème de l’impôt </h3>
<p><strong>Attention</strong> : une réforme de la fiscalité est en cours, une nouvelle loi devrait prochainement entrer en vigueur pour les personnes physiques notamment.</p>
<h4 class="spip">Impôts sur les sociétés</h4>
<p>La loi sur l’<strong>impôt sur les sociétés</strong> est entrée en vigueur au 1er janvier 1999. Elle a été complétée par la circulaire 134-2007-TT-BTC, entrée en vigueur pour l’exercice fiscal 2007. L’impôt sur les sociétés est applicable aux entreprises domestiques ou étrangères ainsi qu’aux parties étrangères des contrats de coopération d’affaires. Les autorités ont annoncé une réduction des taxes sur le revenu des entreprises de 25% à 22%<strong>,</strong> à partir de 2014, et à 20%, à partir de 2016. Dans le cas où le revenu est inférieur à 20 milliards de VND, les taxes seront réduites à 20%, à partir de 2014, et à 17%, à partir de 2016 (décret 218/2013/ND-CP).</p>
<p>Les projets d’investissement de création d’établissement de production dans des secteurs où l’investissement est encouragé bénéficient, sous certaines conditions, de taux réduits de 10%, 15% ou 20%.</p>
<p>La loi du 17 juin 2003, entrée en vigueur la 1er janvier 2004 a supprimé l’imposition sur le rapatriement des dividendes.</p>
<p>Les produits exportés ou importés par les entreprises à capitaux étrangers sont soumis aux droits de douanes prévus par la "Loi sur les droits d’exportation et d’importation" sauf s’ils sont incorporés à leurs actifs immobilisés.</p>
<p>Depuis le 1er janvier 2004, il existe trois taux de TVA au Vietnam : 0 %, 5 %, 10 %. Le taux général de 10 % concerne tous les biens et services, notamment les produits pétroliers et l’électricité. 26 catégories de produits ne sont pas assujetties à la TVA.</p>
<p>Les sols, sous-sols, ressources minières, forestières et maritimes sont la propriété de l’Etat et ne peuvent être exploités qu’en contrepartie d’une redevance (Droit d’usage).</p>
<p>Depuis le 1er janvier 1999 un certain nombre de biens et services sont assujettis au Vietnam à une taxe spéciale à la consommation. Le taux de cette taxe est amené à diminuer sur certaines catégories de produits en fonction des engagements pris par le Vietnam à l’OMC.</p>
<h4 class="spip">Fiscalité des personnes physiques</h4>
<p>La loi vietnamienne prévoit que toute personne de nationalité vietnamienne, tout étranger résidant au Vietnam à titre permanent et tout étranger tirant des revenus résultant de leurs activités au Vietnam sont assujettis à l’IRPP au Vietnam.</p>
<p>Le seuil imposable, applicable aux étrangers comme aux Vietnamiens est de 9 millions de dôngs depuis le 1er juillet 2013. Le taux d’imposition au Vietnam est progressif, allant de 0 à 40 % pour la tranche la plus haute (plus de 80 millions de VND).</p>
<p>Depuis le 1er juillet 2013, la situation fiscale est appréciée individuellement. En cas de personnes à charge - enfants, conjoint, parents… -, le contribuable peut bénéficier d’un abattement allant de 1,6 à 3,6 millions de dôngs.</p>
<h3 class="spip"><a id="sommaire_4"></a>Quitus fiscal </h3>
<p>Un quitus fiscal n’est pas exigé.</p>
<h3 class="spip"><a id="sommaire_5"></a>Solde du compte en fin de séjour </h3>
<p>Un expatrié peut solder son compte en fin de séjour.</p>
<h3 class="spip"><a id="sommaire_6"></a>Coordonnées des centres d’information fiscale </h3>
<p><a href="http://www.mof.gov.vn/" class="spip_out" rel="external">Direction de la Fiscalité</a><br class="manualbr">Ministère des Finances <br class="manualbr">28 Trang Hung Dao<br class="manualbr">Hanoï <br class="manualbr">Tél : (84 4) 220 2828</p>
<p><i>Mise à jour : février 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-vietnam-fiscalite-23049-article-convention-fiscale-111369.md" title="Convention fiscale">Convention fiscale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-vietnam-fiscalite-23049-article-fiscalite-du-pays-111368.md" title="Fiscalité du pays">Fiscalité du pays</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/fiscalite-23049/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
