# Régime local de sécurité sociale

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/protection-sociale/article/regime-local-de-securite-sociale#sommaire_1">Généralités</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/protection-sociale/article/regime-local-de-securite-sociale#sommaire_2">Assurance maladie</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/protection-sociale/article/regime-local-de-securite-sociale#sommaire_3">Assurance - invalidité (A.I.)</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/protection-sociale/article/regime-local-de-securite-sociale#sommaire_4">Assurance vieillesse et survivants (A.V.S.)</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/protection-sociale/article/regime-local-de-securite-sociale#sommaire_5">Prestations complémentaires à l’A.V.S. et à l’A.I. (P.C.)</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/protection-sociale/article/regime-local-de-securite-sociale#sommaire_6">Prestations familiales</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/protection-sociale/article/regime-local-de-securite-sociale#sommaire_7">Chômage (A.C.)</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Généralités</h3>
<h4 class="spip">Structure</h4>
<p>Relèvent obligatoirement du régime suisse de protection sociale les personnes qui résident en Suisse et/ou qui exercent une activité professionnelle en Suisse. En principe chaque personne est assurée individuellement.</p>
<p>Le système suisse de protection sociale comprend cinq domaines :</p>
<ul class="spip">
<li>la prévoyance vieillesse, survivants et invalidité (système dit des 3 piliers)</li>
<li>la couverture en cas de maladie et d’accident</li>
<li>les allocations pour perte de gain en cas de service ou de maternité</li>
<li>l’assurance chômage</li>
<li>les allocations familiales.</li></ul>
<p>L’assurance vieillesse et survivants est la branche principale des assurances sociales, elle sert deux types de rentes : la rente de vieillesse versée au moment de la retraite et la rente de survivants en cas de décès de l’assuré. L’assurance invalidité offre une protection aux personnes qui n’ayant pas encore atteint l’âge de la retraite, ont perdu une partie de leur capacité de travail. Cette assurance est destinée à aider à la réadaptation professionnelle et sociale et ainsi à réduire le nombre de nouvelles rentes en appliquant strictement le principe "la réadaptation prime la rente".</p>
<p>Les branches de maladie et d’accident sont placées sous la tutelle de l’Office fédéral de la santé publique qui dépend du ministère fédéral de l’intérieur.</p>
<p>Les branches vieillesse survivants et invalidité, le régime des allocations pour perte de gain ainsi que les allocations familiales relèvent de l’office fédéral des assurances sociales. L’assurance chômage relève du secrétariat d’Etat à l’économie qui dépend du ministère fédéral de l’économie.</p>
<p>L’assurance vieillesse, survivants invalidité repose sur trois piliers, le premier pilier, celui des pouvoirs publics, comprend l’AVS et l’AI. Les rentes de ces deux assurances sont destinées à couvrir les besoins vitaux des assurés. Dans certains cas particuliers les prestations complémentaires (PC) s’ajoutent à ces rentes.</p>
<p>Le premier pilier est complété par la prévoyance professionnelle vieillesse, survivants et invalidité (2e pilier LPP). Ces deux piliers garantissent à la personne assurée qui prend sa retraite au moins 60 % de son dernier salaire.</p>
<p>Le premier pilier est obligatoire pour tous, y compris les indépendants et les non actifs. Par contre, seuls les salariés sont soumis à titre obligatoire au second pilier.</p>
<p>Le troisième pilier, à savoir la prévoyance individuelle destinée à couvrir des besoins plus étendus est facultative, mais jouit en partie d’avantages fiscaux, à la différence d’autres formes d’épargne.</p>
<p>Ces trois piliers composent ensemble la conception des trois piliers inscrite dans la constitution fédérale depuis 1972.</p>
<p>L’AVS est placée sous la surveillance du conseil fédéral et par délégation sous celle de l’<a href="http://www.bsv.admin.ch/?lang=fr" class="spip_out" rel="external">Office fédéral des assurances sociales</a> (OFAS). Les organes d’exécution sont les caisses de compensation qui encaissent les cotisations et versent les prestations.</p>
<p>L’assurance maladie est gérée par une pluralité d’assureurs. Pour être reconnue pour pratiquer l’assurance maladie sociale, les assureurs doivent appliquer les dispositions légales de manière identique pour tous et distinctes des autres assurances (complémentaires par exemple). En cas d’insolvabilité d’un assureur les coûts des prestations légales sont pris en charge par une institution commune dont le financement est assuré par une contribution des assureurs, prélevée sur les primes d’assurances sociales. Les assureurs, à côté du versement des prestations, encouragent les cantons à la promotion de la santé.</p>
<p>Selon la loi sur l’assurance accidents tous les travailleurs occupés en Suisse sont assurés contre les accidents du travail et les maladies professionnelles et en principe contre les accidents non professionnels.</p>
<p>Les prestations familiales sont traitées par l’OFAS (famille, génération et société - FGS). Le FGS surveille l’application de la loi fédérale sur les allocations familiales (LAFam) applicable aux salariés non agricoles et de la loi fédérale sur les allocations fédérales dans l’agriculture (LFA) applicable aux personnes exerçant une activité agricole, salariée ou non salariée.</p>
<p>Le service de l’emploi est responsable de la mise en œuvre de la loi sur l’assurance chômage. Dans cette mission il a pour partenaire les autorités cantonales, les offices régionaux de placement, les caisses de chômage. L’autorité fédérale du marché du travail garantit aux chômeurs une compensation appropriée de perte de revenu et encourage la réinsertion rapide des chômeurs sur le marché du travail.</p>
<p>L’assurance chômage (AC) verse des prestations en cas de chômage total, partiel, de suspension du travail dû aux intempéries et lorsque que l’employeur est insolvable. Elle finance également les mesures de réinsertion. Les salariés relèvent obligatoirement de l’assurance chômage, les travailleurs indépendants ne sont pas assurés.</p>
<h4 class="spip">Organisation</h4>
<p><strong>Assurances sociales :</strong></p>
<p>Sous la tutelle de l’Office Fédéral des Assurances Sociales (O.F.A.S.), Effingerstrasse, 33 - 3003 Berne, sont compétentes en matière de :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span> <strong>Assurance maladie maternité</strong><br class="manualbr">Les Caisses maladies reconnues (publiques ou privées) et les institutions d’assurance privées autorisées à pratiquer l’assurance maladie. Il existe une institution commune qui assume les coûts des prestations légales en lieu et place des assureurs insolvables. Il existe également la Fondation suisse pour la promotion de la santé.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span> <strong>Assurance vieillesse et survivants et assurance invalidité (A.V.S. - A.I.)</strong><br class="manualbr">Les caisses de compensation publiques ou professionnelles. Les assurés relèvent d’une caisse de compensation tenant leur compte individuel et susceptible de leur délivrer un certificat d’assurance.<br class="manualbr">Pour l’invalidité, il existe les Offices cantonaux de l’assurance invalidité et l’Office Assurance Invalidité pour les assurés résidant à l’étranger (OAI)</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span> <strong>Allocations familiales</strong><br class="manualbr">En matière d’allocations familiales il existe deux règlementations : celle applicable à tous les salariés qui ne travaillent pas dans l’agriculture et celle applicables aux personnes (salariés ou non salariés) qui travaillent dans l’agriculture. Les salariés adressent leur demande d’allocations familiales à leur employeur, ceux qui travaillent pour des employeurs non soumis à l’obligation de cotiser s’adressent à la caisse d’allocations familiales à laquelle ils sont affiliés. Les personnes qui n’exercent pas d’activité s’adressent la caisse cantonale de compensation AVS du canton de leur domicile.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span> <strong>Accidents du travail, maladies professionnelles et accidents non professionnels</strong><br class="manualbr">Les accidents professionnels et non professionnels dépendent de la Caisse nationale suisse d’assurance accidents - Flümattstrasse, 1 - 6002 LUCERNE, des institutions privées d’assurance, des caisses publiques d’assurance accidents et des caisses maladie reconnues.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span> <strong>Chômage</strong><br class="manualbr">Sous la tutelle de l’Office fédéral du développement économique et de l’emploi – Secrétariat d’État à l’économie – Effingerstrasse, 1 – 3003 Berne, les caisses de chômage (cantonales, publiques et d’association) et les caisses de compensation de l’A.V.S. gèrent le risque de privation d’emploi.</p>
<table class="spip">
<thead><tr class="row_first"><th id="ida71a_c0">Office fédéral des assurances sociales</th><th id="ida71a_c1"></th><th id="ida71a_c2"></th><th id="ida71a_c3"></th><th id="ida71a_c4">Secrétariat d’Etat a l’économie </th></tr></thead>
<thead><tr class="row_first"><th id="ida71a_c0">Maladie - Maternité </th><th id="ida71a_c1">Vieillesse - survivants et invalidité </th><th id="ida71a_c2">Allocations familiales </th><th id="ida71a_c3">Accidents et maladies professionnelles </th><th id="ida71a_c4">Chômage </th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="ida71a_c0">- caisses maladie reconnues : caisses maladie publiques et caisses privées - les institutions d’assurances privées soumises à la loi du 17-12-2004 sur la surveillance des assurances et bénéficiant de l’autorisation de pratiquer l’assurance sociale - une institution commune assume les coûts afférents aux prestations légales au lieu et place des assureurs insolvables - Fondation suisse pour la promotion de la santé</td>
<td headers="ida71a_c1">- caisse de compensation (publiques ou professionnelles) - centrale de compensation - pour l’invalidité Offices cantonaux de l’assurance assurance invalidité Office AI pour les assurés résidant à l’étranger (OAI) - pour les prestations complémentaires : caisses cantonales de compensation, sauf à GE, ZH et BS</td>
<td headers="ida71a_c2">- régime fédéral (travailleurs agricoles et petits paysans) : caisses de compensation cantonales - régimes cantonaux (autres travailleurs) : caisses de compensation pour allocations familiales (caisses privées reconnues et caisses cantonales</td>
<td headers="ida71a_c3">- Caisse Nationale suisse d’Assurance en cas d’accidents (CNA) ; elle assure environ 52 % des travailleurs - institutions d’assurance privées soumises à la loi du 17-12-2004 sur la surveillance des assurances - caisses publiques d’assurance- accidents - caisses maladies reconnues au sens de la loi du 18.3.94 sur l’assurance maladie Les assureurs (sauf CNA) gèrent une Caisse supplétive qui alloue les prestations légales aux travailleurs victimes d’un accident que la CNA n’a pas la compétence d’assurer et qui n’ont pas été assurés par leur employeur</td>
<td headers="ida71a_c4">- caisses de chômage (caisses cantonales, caisses publiques et d’association) - autorités désignées par les cantons - offices régionaux de placement - commission tripartite - caisses de compensation de l’AVS - centrale de compensation de l’AVS-employeurs-commission de surveillance</td></tr>
</tbody>
</table>
<h4 class="spip">Financement</h4>
<table class="spip">
<thead><tr class="row_first"><th id="idbfbc_c0">Risques </th><th id="idbfbc_c1"> Salarié </th><th id="idbfbc_c2"> Employeur </th><th id="idbfbc_c3">Indépendant </th><th id="idbfbc_c4"> Plafond annuel</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="idbfbc_c0"><strong>Maladie maternité –PN (AMal)<span class="spip_note_ref"> [<a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/protection-sociale/article/regime-local-de-securite-sociale#nb3-1" class="spip_note" rel="footnote" title="L’assureur fixe le montant des primes qui est identique pour tous (aucune (...)" id="nh3-1">1</a>]</span></strong></td>
<td headers="idbfbc_c1">Primes</td>
<td headers="idbfbc_c2">- (1)</td>
<td headers="idbfbc_c3">Primes</td>
<td headers="idbfbc_c4">-</td></tr>
<tr class="row_even even">
<td headers="idbfbc_c0"><strong>AGP en cas de maternité – PE<span class="spip_note_ref"> [<a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/protection-sociale/article/regime-local-de-securite-sociale#nb3-2" class="spip_note" rel="footnote" title="L’allocation pour perte de gain (APG) consiste en des indemnités journalières (...)" id="nh3-2">2</a>]</span></strong></td>
<td headers="idbfbc_c1">0,25%</td>
<td headers="idbfbc_c2">0,25%</td>
<td headers="idbfbc_c3">0,50%</td>
<td headers="idbfbc_c4">-</td></tr>
<tr class="row_odd odd">
<td headers="idbfbc_c0"><strong>Accidents et maladies</strong>
<p><strong>- non professionnels (AANP) </strong><span class="spip_note_ref"> [<a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/protection-sociale/article/regime-local-de-securite-sociale#nb3-3" class="spip_note" rel="footnote" title="Le taux de la prime (fixé en pour mille du gain assuré) varie en fonction du (...)" id="nh3-3">3</a>]</span></p>
<p><strong>- professionnels (AAP) </strong><span class="spip_note_ref"> [<a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/protection-sociale/article/regime-local-de-securite-sociale#nb3-4" class="spip_note" rel="footnote" title="Les entreprises sont classées en classes et degrés de tarifs selon les (...)" id="nh3-4">4</a>]</span></p>
</td>
<td headers="idbfbc_c1">Primes -</td>
<td headers="idbfbc_c2">Facultatif Primes</td>
<td headers="idbfbc_c3">-</td>
<td headers="idbfbc_c4">du gain assuré 126000 CHF</td></tr>
<tr class="row_even even">
<td headers="idbfbc_c0"><strong>Vieillesse et survivants (AVS)</strong></td>
<td headers="idbfbc_c1">4,20%</td>
<td headers="idbfbc_c2">4,20%</td>
<td headers="idbfbc_c3">7,8%<span class="spip_note_ref"> [<a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/protection-sociale/article/regime-local-de-securite-sociale#nb3-5" class="spip_note" rel="footnote" title="Pour les revenus inférieurs à 55 700 CHF, le taux de cotisations baisse en (...)" id="nh3-5">5</a>]</span></td>
<td headers="idbfbc_c4">-</td></tr>
<tr class="row_odd odd">
<td headers="idbfbc_c0"><strong>Invalidité (AI)</strong></td>
<td headers="idbfbc_c1">0,70%</td>
<td headers="idbfbc_c2">0,70%</td>
<td headers="idbfbc_c3">1,4%<span class="spip_note_ref"> [<a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/protection-sociale/article/regime-local-de-securite-sociale#nb3-6" class="spip_note" rel="footnote" title="Pour les revenus inférieurs à 55 700 CHF, le taux de cotisations baisse en (...)" id="nh3-6">6</a>]</span></td>
<td headers="idbfbc_c4">-</td></tr>
<tr class="row_even even">
<td headers="idbfbc_c0"><strong>Allocations familiales</strong>
<p>- <strong>LFA (agriculture)</strong></p>
<p>- <strong>LAFam</strong><span class="spip_note_ref"> [<a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/protection-sociale/article/regime-local-de-securite-sociale#nb3-7" class="spip_note" rel="footnote" title="La LAFam (loi fédérale sur les allocations familiales) régit les droits des (...)" id="nh3-7">7</a>]</span></p>
</td>
<td headers="idbfbc_c1">-
<p>- (5)</p>
</td>
<td headers="idbfbc_c2">2%
<p>de 0,1 à 4%</p>
</td>
<td headers="idbfbc_c3"><span class="spip_note_ref"> [<a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/protection-sociale/article/regime-local-de-securite-sociale#nb3-8" class="spip_note" rel="footnote" title="Règlementation différente selon les cantons : cotisation obligatoire dans les (...)" id="nh3-8">8</a>]</span></td>
<td headers="idbfbc_c4">-</td></tr>
<tr class="row_odd odd">
<td headers="idbfbc_c0"><strong>Chômage (AC)</strong></td>
<td headers="idbfbc_c1">1,1%</td>
<td headers="idbfbc_c2">1,1%</td>
<td headers="idbfbc_c3">-</td>
<td headers="idbfbc_c4">126000 CHF<span class="spip_note_ref"> [<a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/protection-sociale/article/regime-local-de-securite-sociale#nb3-9" class="spip_note" rel="footnote" title="De plus, l’employeur et le salarié versent chacun une cotisation de (...)" id="nh3-9">9</a>]</span></td></tr>
</tbody>
</table>
<p><i>Sources : centre d’information AVS/AI, <a href="http://www.bsv.admin.ch/?lang=fr" class="spip_out" rel="external">OFAS</a></i></p>
<p>L’adhésion pour les prestations en espèces de l’assurance maladie peut être conclue à titre individuel ou peut être rendue obligatoire du fait d’un contrat de travail ou d’une convention collective ; dans ce cas, l’assurance peut alors être conclue sous forme de contrat collectif auprès de l’assureur. Le montant de l’indemnité journalière est directement négocié avec l’assureur. Il peut moduler les primes en fonction des régions et de l’âge du preneur d’assurance. Si l’adhésion prévoit un délai de carence, l’assureur doit réduire le montant des primes. Les indemnités journalières seront versées pendant 2 ans sur une période de 2,5 ans.</p>
<p>Il convient de rajouter aux risques susmentionnés la Prévoyance professionnelle (2ème pilier) qui se conjugue avec l’assurance vieillesse, invalidité et survivants, et permet au retraité de maintenir son niveau de vie antérieur. L’objectif étant d’atteindre au moins 60% du dernier salaire de l’assuré en cumulant la rente AVS et la prévoyance professionnelle. Les cotisations dépendent des institutions de prévoyance et sont à la charge des employeurs et des salariés. Une cotisation complémentaire facultative est prévue, il s’agit de la Prévoyance individuelle (3ème pilier), le montant des cotisations est déterminé par l’assuré lors de son adhésion et est déductible de l’impôt sur le revenu.</p>
<p>Il existe une exception à l’obligation d’assurance lorsque le revenu perçu au titre d’une activité salariée pour le compte d’un employeur ne dépasse pas 2.300 € par an. Dans ce cas les cotisations AVS ne sont perçues que sur demande expresse de l’assuré.</p>
<h4 class="spip">Recouvrement</h4>
<p>Les cotisations sont versées en principe tous les mois. Toutefois, les employeurs qui ont peu de salariés peuvent verser les cotisations trimestriellement à condition que le total annuel des salaires ne dépasse pas 200 000 francs suisses.</p>
<p>Les cotisations de vieillesse et de survivants (AVS), d’invalidité (AI), de l’allocation pour perte de gain (APG), de chômage (AC) sont recouvrées par la caisse de compensation de l’employeur.</p>
<p>Les cotisations dues au titre des accidents du travail et maladies professionnelles sont recouvrées soit par la caisse nationale suisse d’assurance (SUVA) ; soit par un assureur privé ou public agrée que l’employeur aura choisi.</p>
<p>Les cotisations relatives aux allocations familiales sont versées aux caisses d’allocations familiales auprès desquelles l’employeur se sera préalablement affilié.</p>
<p>Lorsque la masse salariale annuelle totale de l’entreprise est inférieure à 55 6800 CHF et que le salaire annuel par salarié ne dépasse pas 20880 CHF, l’employeur peut avoir recours à la procédure de décompte simplifiée. Si l’employeur opte pour cette solution, il n’aura qu’un seul interlocuteur pour le paiement de toutes les assurances sociales (AVS, AI, APG, AC, AA, AF) y compris pour le paiement de l’impôt sur le revenu qui ont lieu une fois par an.</p>
<p>Lorsque l’employeur ne dispose pas d’établissement en Suisse, il peut en application de l’article 21§2 du règlement (CE) n°987/2009 (anciennement article 109 du règlement (CEE) n° 574/72) demander à son salarié d’exécuter ses obligations en Suisse. Dans le cas d’un tel accord, il doit informer les institutions suisses compétentes au moyen d’un formulaire.</p>
<p>Suite à la révision partielle de l’AVS (assurance vieillesse-survivants), les salariés qui travaillent en Suisse pour le compte d’un employeur établi en dehors du territoire national cotisent à partir du 1er janvier 2012 aux mêmes taux que les autres salariés (suppression de l’ancienne possibilité de profiter d’un barème dégressif). <i>*Au 1er janvier 2012, 1 franc suisse vaut 0.82 euros</i></p>
<h5 class="spip">Assurance maladie</h5>
<p>L’assurance maladie comprend l’assurance obligatoire des soins et une assurance facultative d’indemnités journalières. Les caisses maladie ont le droit de pratiquer en plus de l’assurance maladie sociale, l’assurance complémentaire.</p>
<h5 class="spip">Soins de santé</h5>
<p>Le montant des primes à payer pour les soins de santé est fixé par l’assureur et il est normalement égal pour tous les assurés. Le tarif des primes doit être approuvé par l’Office fédéral de la santé publique. Toutefois, l’assureur peut échelonner ce montant s’il existe des coûts différents selon les cantons. Les primes peuvent également être modifiées pour les assurés qui choisissent des franchises plus élevées ou encore qui s’adressent à des fournisseurs limités par l’assureur. Pour les assurés de moins de 18 ans révolus, l’assureur doit fixer une prime plus basse que celle des assurés plus âgés. Il est autorisé à le faire pour les assurés de moins de 25 ans révolus.</p>
<h5 class="spip">Indemnités journalières</h5>
<p>Le montant des primes pour les indemnités journalières peut être différent en fonction de l’âge d’entrée de l’assuré ou de la région. Si un délai d’attente est applicable au versement de l’indemnité journalière, l’assureur doit réduire les primes de manière correspondante.</p>
<h5 class="spip">Assurance accidents</h5>
<p>Les primes d’assurance obligatoires contre les accidents du travail et maladies professionnelles sont à la charge du seul employeur et sont versées dans la limite d’un plafond (126 000 CHF par an). Les entreprises sont classées en classes et degrés des tarifs selon les risques d’accidents et les conditions propres à l’entreprise. Le taux varie entre 0,41 et 171,93 ‰. S’agissant des accidents non professionnels dont la charge des cotisations incombe au travailleur ou au chômeur, les taux s’échelonnent entre 16,6 ‰ et 27,1 ‰ pour les personnes relevant de la caisse nationale accidents et entre 9,30 ‰ et 19,26 ‰ pour celles relevant d’autres institutions.</p>
<h5 class="spip">Assurance chômage</h5>
<p>En ce qui concerne le chômage, la cotisation partagée par moitié entre l’employeur et le salarié est versée dans la limite d’un plafond : 2,2 % du salaire jusqu’à 126 000 CHF.</p>
<h5 class="spip">Prestations familiales</h5>
<p>Enfin, les prestations familiales sont financées uniquement par l’employeur, sauf dans le Valais où le salarié verse une cotisation.</p>
<h4 class="spip">Prévoyance professionnelle</h4>
<p>La prévoyance professionnelle vieillesse, survivants et invalidité qui représente le 2e pilier de la prévoyance en matière de vieillesse, survivants et invalidité est financée par capitalisation. Elle est obligatoire pour les salariés qui perçoivent d’un même employeur un salaire supérieur à une certaine limite (27840 CHF par an).</p>
<h3 class="spip"><a id="sommaire_2"></a>Assurance maladie</h3>
<p>L’assurance maladie comprend l’assurance soins, qui est obligatoire, et l’assurance indemnités journalières, qui elle, est facultative. Les caisses maladie ont le droit de pratiquer en plus de l’assurance obligatoire, des assurances complémentaires qui ne sont pas régies par la LAMal (loi fédérale sur l’assurance maladie).</p>
<p>Toute personne résidant en Suisse doit s’assurer pour les soins ou être assurée par son représentant légal dans les trois mois qui suivent la naissance ou l’installation en Suisse. L’assuré a le libre choix de la caisse maladie et celle-ci est tenue d’accepter, dans la limite de son rayon d’activité territorial, tout candidat à l’assurance.</p>
<p>L’assurance soins est gérée par les caisses maladie qui sont des personnes de droit privé ou public, sans but lucratif et qui sont reconnues par le département fédéral de l’intérieur et les institutions d’assurance autorisées à pratiquer l’assurance maladie sociale.</p>
<p>En fonction de la situation, l’assurance doit être contractée dans un délai qui peut atteindre trois mois dans certains cas. Si l’assuré demande son affiliation dans le délai imparti, l’assurance prend effet immédiatement. En cas de retard dans l’affiliation, un supplément de prime est exigé. L’assurance fixe le supplément en fonction de la situation financière de l’assuré. Les tarifs pratiqués par les assureurs sont approuvés par l’Office Fédéral des Assurances Sociales. Le montant des primes peut être échelonné par régions ou par cantons. L’assurance est individuelle, il n’existe pas, comme en législation française, de notion d’ayant-droit.</p>
<p>L’assureur doit distinguer nettement pour chaque assuré entre les primes de :</p>
<ul class="spip">
<li>l’assurance obligatoire des soins, la prime pour le risque d’accidents inclus devant être mentionnée séparément</li>
<li>de l’assurance indemnités journalières</li>
<li>des assurances complémentaires</li>
<li>des autres branches d’assurance.</li></ul>
<p>Les assurés de condition économique modeste bénéficieront d’une réduction de leur prime d’assurance grâce aux subsides fédéraux accordés aux cantons.</p>
<h4 class="spip">Assurance soins</h4>
<p>Par maladie, on entend toute atteinte à la santé physique ou mentale qui n’est pas due à un accident et qui exige un examen ou un traitement médical, ou provoque une incapacité de travail. Par accident, on entend toute atteinte dommageable, soudaine et involontaire portée au corps humain par une cause extérieure extraordinaire qui compromet la santé physique ou mentale.</p>
<p>La maternité vise la grossesse et l’accouchement, ainsi que la convalescence pour la mère. Le malade a le libre choix du médecin, du pharmacien, du laboratoire, de l’hôpital…</p>
<p>L’assurance maladie garantit à chacun l’accès aux soins médicaux en cas de maladie ou d’accident non pris en charge par l’assurance accidents. Toute personne domiciliée en Suisse a l’obligation de s’assurer.</p>
<p>Les prestations comprennent les prestations générales en cas de maladie, de maternité, d’accidents (non couvert par une autre assurance accidents), d’infirmité congénitale (non couverte par l’assurance invalidité) et l’interruption non punissable de la grossesse. Sont pris en charge :</p>
<ul class="spip">
<li>les examens, traitements et soins dispensés sous forme ambulatoire, au domicile du patient, en milieu hospitalier ou semi-hospitalier ou dans un établissement médico-social par des médecins, des chiropraticiens ou des personnes fournissant des prestations sur prescription ou sur mandat médicale</li>
<li>les analyses, médicaments, moyens et appareils diagnostiques ou thérapeutiques prescrits par un médecin</li>
<li>le séjour en division commune à l’hôpital</li>
<li>les mesures de prévention (les coûts de certains examens et certaines vaccinations peuvent être pris en charge)</li>
<li>il existe également une participation des caisses maladie aux frais de cures balnéaires, frais de transport et frais de sauvetage</li>
<li>les mesures de réadaptation effectuées ou prescrites par un médecin.</li></ul>
<p>En fonction des choix de l’assuré, le montant de la prime d’assurance peut varier. C’est le cas lorsque l’assuré choisit :</p>
<ul class="spip">
<li>une franchise plus élevée</li>
<li>s’adresse à une organisation HMO (Health maintenance organisation) qui est un réseau de soins composés de médecins généralistes, mais également de spécialistes et de thérapeutes de diverses spécialités. Le patient qui opte pour ce modèle doit toujours consulter en premier lieu son médecin, sauf cas d’urgence. Celui-ci l’adressera au spécialiste du HMO ou s’il n’y en a pas, de l’extérieur ;</li>
<li>possibilité également de renoncer au libre choix du médecin et de choisir un médecin de famille dans un réseau constitué de généralistes indépendants qui se sont regroupés dans une région. Le médecin de famille coordonnera toutes les questions médicales, sauf cas d’urgence ;
 - enfin certains assureurs offrent la possibilité de consulter le médecin par téléphone avant chaque visite.</li></ul>
<h5 class="spip">Prestations spécifiques de maternité</h5>
<p>Il s’agit des examens de contrôle, effectués par un médecin ou une sage-femme, ou prescrits par un médecin (sept examens lors d’une grossesse normale), une contribution aux cours d’accouchement sans douleur, les frais d’accouchement à domicile, dans un hôpital ou dans une institution de soins semi-hospitaliers, ainsi que l’assistance d’un médecin ou d’une sage-femme, les conseils en cas d’allaitement (le remboursement est limité à trois séances).</p>
<h5 class="spip"><strong>Soins dentaires</strong></h5>
<p>En matière de soins dentaires, l’assurance prend uniquement en charge le coût des soins dentaires lorsqu’ils sont occasionnés par une maladie grave non évitable du système de mastication ou lorsqu’ils sont occasionnés par une autre maladie grave ou ses conséquences, ou enfin s’ils sont nécessaires pour traiter une maladie grave ou ses séquelles.</p>
<h5 class="spip"><strong>Participation</strong></h5>
<p>La participation aux frais est composée d’une franchise annuelle appliquée pour les assurés majeurs et d’une quote-part des dépenses en pourcentage. La franchise ordinaire s’élève à 300 CHF par année civile. Il existe également des franchises à option. S’agissant de la quote-part laissée à la charge des assurés majeurs, elle représente 10 % des frais dépassant la franchise dans la limite de 700 CHF pour les adultes et 350 CHF pour les enfants. Plusieurs enfants d’une même famille, assurés par le même assureur, paient ensemble au maximum 1000 CHF. Par ailleurs, les personnes qui ne vivent pas en ménage commun avec les membres de leur famille doivent verser une contribution de 10 CHF par jour d’hospitalisation, sans aucune limitation dans le temps.</p>
<p>Les primes d’assurance peuvent être réduites par les franchises à options pour les majeurs (500 CHF, 1 000 CHF, 1 500 CHF, 2 000 CHF et 2 500 CHF), ou pour les enfants</p>
<p>(100 CHF, 200 CHF, 300 CHF, 400 CHF, 500 CHF et 600 CHF). Les caisses ne sont pas obligées de proposer les paliers, elles peuvent offrir pour les jeunes adultes (de 19 à</p>
<p>25 ans révolus), des franchises différentes de celles des adultes. Les assureurs peuvent également pratiquer en plus de l’assurance soins ordinaires une assurance dans laquelle une réduction de prime est accordée lorsque l’assuré n’a bénéficié d’aucune prestation durant une année. Sont exclues les prestations de maternité et les mesures de prévention.</p>
<p>Aucune participation n’est exigée s’il s’agit de prestations en cas de maternité.</p>
<p>Les assurances impliquant un choix limité des fournisseurs de prestations permettent également une réduction des primes.</p>
<h4 class="spip"><strong>Assurance indemnités journalières</strong></h4>
<p>Cette assurance qui n’est pas obligatoire est ouverte à toute personne âgée de plus de 15 ans et de moins de 65 ans, résidant en Suisse ou y exerçant une activité professionnelle. Les prestations sont servies à partir du troisième jour qui suit le début de la maladie.</p>
<p>Moyennant une réduction de la prime, le délai de carence peut être augmenté. Les assureurs peuvent exclure au moment de l’assurance les maladies préexistantes.</p>
<p>Cette exclusion n’est valable que pendant cinq ans maximum.</p>
<p>Le montant de l’indemnité journalière est fixé entre l’assureur et l’assuré.</p>
<p>En cas d’incapacité de travail, elles sont servies durant au moins 720 jours dans une période de 900 jours.</p>
<p>Depuis le 1er juillet 2005, toutes les mères exerçant une activité lucrative ont droit à un congé maternité payé post-natal de 14 semaines. Des dispositions cantonales (notamment le canton de Genève), règlements du personnel et conventions collectives peuvent prévoir des prestations plus généreuses.</p>
<p>Pour ouvrir droit aux indemnités journalières de maternité, l’assurée doit, au moment de l’accouchement, avoir été assurée minimum 5 mois durant les 270 jours précédents.</p>
<h3 class="spip"><a id="sommaire_3"></a>Assurance - invalidité (A.I.)</h3>
<p>Les assurés doivent présenter une diminution de capacité de gain, permanente ou de longue durée. Cette assurance a, pour premier objectif, la réadaptation ou le reclassement des assurés. La rente d’invalidité n’est servie que si la réadaptation se révèle inutile ou ne permet pas d’atteindre, en totalité ou en partie, le but recherché. Les intéressés ont droit aux prestations de réadaptation sans condition de résidence ou de cotisations, et aux rentes ordinaires d’invalidité, lorsqu’ils justifient d’au moins 1 an de cotisations.</p>
<p>Le droit à pension prend effet au plus tôt 6 mois après le dépôt de la demande d’invalidité dans le but d’inciter les personnes en incapacité de travail pour cause de maladie à déposer une demande d’AI le plus vite possible, afin que celle-ci puisse, par des mesures ciblées d’intervention précoce et de réadaptation, éviter le versement d’une pension.</p>
<p>Par ailleurs pour ne pas décourager les bénéficiaires d’une pension d’invalidité à reprendre une activité professionnelle, leur pension n’est révisée que si l’amélioration de revenu dépasse 1.500 FS par an et seul 1/3 du montant dépassant ce seuil est pris en compte pour la révision de la pension.</p>
<p>Pour obtenir des prestations de l’assurance invalidité, il convient d’être assuré lors de la réalisation du risque.</p>
<p>Le premier pilier vise l’assurance universelle qui couvre tous les besoins vitaux. Pour le deuxième pilier obligatoire également, la loi fixe des exigences de minimum de niveau d’indemnisation. Les institutions de prévoyance peuvent instaurer dans leur règlement interne une prévoyance plus étendue.</p>
<h4 class="spip"><strong>Mesures d’intervention précoce</strong></h4>
<p>La réforme entrée en vigueur en 2008 introduit un système de détection précoce dans le but de prévenir l’invalidité des personnes qui présentent une incapacité de travail ininterrompue de 30 jours au moins ou, qui se sont absentées de manière répétée pour des périodes plus courtes au cours de l’année. L’AI examine la situation personnelle de l’intéressé et détermine si des mesures de réadaptation précoces sont indiquées. Les mesures de réadaptation précoces doivent permettre de maintenir à leur poste les personnes en incapacité de travail ou permettre leur réadaptation à un nouveau poste au sein de la même entreprise ou ailleurs. Les prestations servies dans ce cadre peuvent consister dans une adaptation du poste de travail, des cours de formation, un placement, une orientation professionnelle, une réadaptation socioprofessionnelle, etc.</p>
<p>L’intervention précoce s’achève par :</p>
<ul class="spip">
<li>la décision de mettre en œuvre les mesures de réadaptation ;</li>
<li>la constatation qu’aucune mesure de réadaptation ne peut être mise en œuvre avec succès et le droit à la rente est alors examiné ;</li>
<li>la décision selon laquelle l’intéressé n’a droit ni aux mesures de réadaptation, ni à la rente.</li></ul>
<p>Le montant des mesures d’intervention précoce ne peut dépasser 20.000 CHF par assuré.</p>
<h4 class="spip">Mesures médicales de réadaptation et indemnités journalières</h4>
<p>Pour pouvoir prétendre aux mesures de réadaptation il faut être assujetti à l’assurance obligatoire ou facultative, être invalide ou menacé d’une invalidité, dans la mesure où la capacité de gain ou la capacité d’accomplir ses travaux habituels, qu’il y ait ou non une activité lucrative préalable, peut être rétablie, améliorée par de telles mesures.</p>
<p>L’assuré sans activité lucrative qui suit des mesures de réadaptation et qui s’occupe d’un ou de plusieurs enfants de moins de 16 ans ou d’autres membres de sa famille a droit à une allocation pour frais de garde et d’assistance, sous certaines conditions.</p>
<p>Les prestations servies dans le cadre des mesures de réadaptation sont les suivantes :</p>
<ul class="spip">
<li>mesures médicales qui n’ont pas pour objet le traitement de l’affection, mais qui sont directement nécessaires à la réadaptation professionnelle ;</li>
<li>mesures médicales nécessaires au traitement des infirmités congénitales les mesures socio-professionnelles comme les mesures d’accoutumance au travail, de stimulation, de motivation, de stabilisation de la personnalité et de socialisation de base, mesures d’occupation destinées à maintenir une structuration de la journée ;</li>
<li>les mesures d’orientation professionnelle initiale, reclassement, placement, aide en capital ;</li>
<li>les moyens auxiliaires : appareils orthopédiques, acoustiques, véhicules à moteur, chiens pour aveugle, outils spéciaux pour exercer une activité ou maintenir ou améliorer la capacité de travail ou pour se déplacer, établir des contacts, développer son autonomie personnelle ;</li>
<li>les indemnités journalières sont servies durant l’exécution des mesures de réadaptation si celles-ci empêchent l’intéressé d’exercer une activité professionnelle durant 3 jours consécutifs au moins ou s’il présente une incapacité de travail d’au moins 50 % à l’assuré qui suit une formation professionnelle initiale, l’assuré âgé de moins de 20 ans qui n’a pas encore exercé d’activité lucrative, ont droit à une indemnité journalière s’ils ont entièrement ou partiellement perdu leur capacité de gain.</li></ul>
<p>L’assuré bénéficie d’indemnités journalières pendant l’exécution des mesures de réadaptation si celles-ci l’empêchent d’exercer une activité professionnelle rémunérée durant 3 jours consécutifs au moins ou s’il présente une incapacité de travail d’au moins 50 % pour son activité habituelle.</p>
<p>Les assurés mineurs ont droit à l’ensemble des mesures médicales nécessaires au traitement des infirmités congénitales, même s’ils sont incapables de recevoir une formation professionnelle. En sus des mesures d’ordre professionnel (formation, reclassement), l’A.I. prévoit des mesures de formation scolaire spéciale et une contribution aux frais de soins des mineurs handicapés.</p>
<p>Pendant la période de réadaptation, l’assuré de plus de 20 ans, empêché par les mesures de réadaptation d’exercer une activité lucrative pendant au moins 3 jours consécutifs, ou atteint d’une incapacité de travail de 50 %, a droit à des indemnités journalières.</p>
<p>Les invalides qui reçoivent une formation professionnelle initiale peuvent également prétendre à des indemnités journalières correspondant à un salaire moyen d’un apprenti.</p>
<h4 class="spip">Rentes d’invalidité</h4>
<p>Pour pouvoir prétendre à une rente d’invalidité il faut que :</p>
<ul class="spip">
<li>la capacité de gain ou la capacité à accomplir les travaux habituels ne puisse pas être rétablie, maintenue ou améliorée par des mesures de réadaptation ;</li>
<li>l’assuré présente une incapacité de travail d’au moins 40 % en moyenne durant toute une année sans interruption notable ;</li>
<li>l’assuré justifie d’au moins 3 années de cotisations.</li></ul>
<p>Il existe 4 types de rentes en fonction du taux d’incapacité :</p>
<ul class="spip">
<li>La rente est dite "entière" lorsque le taux d’incapacité est de 70 % au moins ;</li>
<li>Trois quarts de rente sont alloués en cas de diminution de la capacité de gain de 60 % au moins ;</li>
<li>une demi rente en cas d’incapacité de travail d’au moins 50 % et</li>
<li>un quart de rente si l’incapacité de travail est de 40 % au moins.</li></ul>
<p>Pour évaluer le taux d’incapacité, une comparaison est effectuée entre le revenu que l’assuré aurait pu obtenir s’il n’était pas invalide et celui qu’il pourrait obtenir en exerçant l’activité qui peut raisonnablement être exigée de lui après les traitements et les mesures de réadaptation. L’invalidité des assurés n’exerçant pas d’activité professionnelle est évaluée en fonction de l’incapacité d’accomplir ses travaux habituels.</p>
<p>Une rente complète est attribuée à celui qui justifie, à la date de l’incapacité, du même nombre d’années de cotisations que celui de sa classe d’âge. La rente est partielle, lorsque la capacité de gain ou la capacité à accomplir les travaux habituels ne peut pas être rétablie avec les mesures de réadaptation si :</p>
<ul class="spip">
<li>l’assuré a présenté une incapacité de travail d’au moins 40 % en moyenne pendant une année sans interruption notable ;</li>
<li>au terme de cette année il présente un taux d’incapacité d’au moins 40 % ;</li>
<li>il a cotisé durant au moins 3 ans.</li></ul>
<p>Le montant de la rente mensuelle est composé d’une fraction du montant minimal de la rente (montant fixe) et d’une fraction du revenu annuel moyen (montant variable).</p>
<p>La rente d’invalidité complète est au minimum de 1 160 CHF par mois et au maximum 2 320 CHF par mois. La somme de deux rentes individuelles d’un couple ne doit pas dépasser 150 % du montant maximum de la rente d’invalidité, soit 3480 CHF par mois.</p>
<p>Pour les enfants qui pourraient prétendre, lors du décès du rentier à une rente d’orphelin, une rente pour enfant égale à 40 % de la rente d’invalidité est servie (minimum : 464 CHF par mois - maximum : 928 CHF par mois). Si les parents ont droit à une rente pour enfant, le montant des deux rentes pour enfant ne peut dépasser 60 % du montant maximum de la rente d’invalidité soit 1392 CHF par mois.</p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td></td>
<td>1/1</td>
<td>3/4</td>
<td>1/2</td>
<td>1/4</td></tr>
<tr class="row_even even">
<td>Rente d’invalidité minimale (CHF/mois)</td>
<td>1160</td>
<td>870</td>
<td>580</td>
<td>290</td></tr>
<tr class="row_odd odd">
<td>Rente d’invalidité maximale (CHF/mois)</td>
<td>2320</td>
<td>1740</td>
<td>1160</td>
<td>580</td></tr>
<tr class="row_even even">
<td>Rente pour enfant minimale (CHF/mois)</td>
<td>464</td>
<td>348</td>
<td>232</td>
<td>116</td></tr>
<tr class="row_odd odd">
<td>Rente pour enfant maximale (CHF/mois)</td>
<td>928</td>
<td>696</td>
<td>464</td>
<td>232</td></tr>
</tbody>
</table>
<h4 class="spip">Rente extraordinaire</h4>
<p>La rente extraordinaire est accordée aux ressortissants suisses qui résident en Suisse, lorsque les conditions minimales de durée de cotisations exigées ne sont pas remplies et que l’intéressé a néanmoins été assuré pendant le même nombre d’années qu’une personne de la même classe d’âge. Bénéficient de plein droit de la rente extraordinaire les personnes invalides de naissance ou dès leur enfance, domiciliées en Suisse.</p>
<h4 class="spip">Allocation pour impotent</h4>
<p>Cette allocation est octroyée dès lors qu’il existe une impotence d’une durée probable de plus de 12 mois. Est réputée impotente toute personne qui a besoin de façon permanente de l’aide d’autrui ou d’une surveillance personnelle pour accomplir les actes ordinaires de la vie. L’impotence peut être grave, moyenne ou faible.</p>
<p><strong>Montants :</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Montant en CHF</td>
<td>Séjour en home</td>
<td>Séjour à domicile</td></tr>
<tr class="row_even even">
<td>Impotence légère</td>
<td>116.-</td>
<td>464.-</td></tr>
<tr class="row_odd odd">
<td>Impotence moyenne</td>
<td>290.-</td>
<td>1’160.-</td></tr>
<tr class="row_even even">
<td>Impotence grave</td>
<td>464.-</td>
<td>1’856.-</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_4"></a>Assurance vieillesse et survivants (A.V.S.)</h3>
<p>Cette assurance instaurée le 1er janvier 1948, a fait l’objet d’un certain nombre de révisions. Une partie des dispositions de cette assurance est applicable en cas d’invalidité (assujettissement, obligation de cotiser, genre et montant des rentes, gestion). Le législateur a complété ce dispositif en 1966 par un régime de prestations complémentaires à l’A.V.S. et à l’assurance invalidité (A.I) qui tient compte de la situation économique de chaque bénéficiaire.</p>
<p>Une nouvelle conception de la protection a abouti en décembre 1972 à l’adoption d’un nouvel aménagement fixant la place et le rôle de chacun des trois piliers suivants : l’assurance d’Etat, la prévoyance professionnelle et la prévoyance personnelle. Les classes modestes verront leur niveau de vie maintenu par les rentes ; les classes moyennes l’obtiendront grâce à l’appoint de la prévoyance professionnelle et les classes aisées devront avoir recours à la prévoyance individuelle.</p>
<p>Enfin, la dixième réforme de l’AVS introduit l’égalité de traitement entre les hommes et les femmes.</p>
<h4 class="spip">Premier pilier : l’assurance vieillesse et survivants (A.V.S.)</h4>
<h5 class="spip">Assujettissement</h5>
<p>Sont obligatoirement assujettis ceux qui ont leur domicile en Suisse ou qui y exercent une activité lucrative. Ils sont tenus au paiement des cotisations s’ils exercent une activité lucrative ou dès le 1er janvier de l’année qui suit le 20e anniversaire, s’ils n’exercent pas d’activité lucrative. L’obligation cesse avec la fin de l’activité ou, au plus tôt, à 64 ans pour les femmes et à 65 ans pour les hommes.</p>
<p>Est réputé avoir payé lui-même les cotisations, le conjoint inactif d’un assuré qui verse des cotisations au moins égales au double de la cotisation minimale ou la personne qui travaille dans l’entreprise de son conjoint sans percevoir de rémunération.</p>
<h5 class="spip">Rentes "ordinaires"</h5>
<p><strong>Vieillesse</strong></p>
<p><strong>Conditions</strong><br class="manualbr">Pour obtenir une rente ordinaire de vieillesse, il faut avoir payé des cotisations pendant au moins une année et remplir une condition d’âge (64 ans pour les femmes, 65 ans pour les hommes). La rente complète est attribuée à l’assuré (ou à ses survivants) qui compte, au moment de la survenance du risque, une durée complète de cotisations par rapport à sa classe d’âge ; sinon, la rente est partielle.<br class="manualbr">Le montant de la rente est déterminé en fonction du revenu annuel moyen et des années de cotisations. La réforme entrée en vigueur au 1er janvier 1997 a introduit le splitting qui consiste dans la répartition des revenus acquis durant les années de mariage lorsque, les deux conjoints ont droit à la rente, ou une veuve ou un veuf a droit à une rente de vieillesse, ou lorsque le mariage est dissout par le divorce.<br class="manualbr">Des bonifications pour tâches éducatives ou pour tâche d’assistance peuvent être attribuées. Les deux bonifications ne peuvent pas se cumuler.<br class="manualbr">Les personnes qui remplissent les conditions d’octroi d’une rente ordinaire de vieillesse peuvent obtenir son versement anticipé d’un ou deux ans ; le montant de la réduction opérée sur la rente est de 6,8 % pour une année d’anticipation et de 13,6% pour deux années d’anticipation.<br class="manualbr">Les personnes qui ont l’âge de liquidation de la pension de vieillesse peuvent ajourner la liquidation de cet avantage au minimum d’un an et au maximum de cinq ans. La rente est alors augmentée d’un pourcentage compris entre 5,2 % et 31,5%.</p>
<p><strong>Montant</strong><br class="manualbr">La rente ordinaire de vieillesse est composée d’une partie fixe (80 % du montant minimum de la rente) et d’une partie liée aux gains (20 % du revenu annuel moyen dans la limite de six fois le montant minimum de la rente pour une carrière complète). La personne bénéficiant d’une rente de vieillesse a droit à une rente pour chacun des enfants qui, lors de son décès aurait pu bénéficier d’une rente d’orphelin. La rente pour enfant représente 40% de la rente de vieillesse. Pour une rente complète, le montant minimum est de 1 160 CHF par mois et le maximum de 2 320 CHF par mois.<br class="manualbr">Le montant des rentes de vieillesse versé à un couple ne peut pas excéder 150 % du montant maximum, soit 3480 CHF par mois. La rente pour enfant représente 40 % de la rente de vieillesse (minimum : 464 CHF par mois - maximum : 920 CHF par mois). Si les deux parents ont droit à une rente pour enfant, le montant des deux rentes ne peut pas dépasser 60 % du maximum de la rente de vieillesse (1 392 CHF par mois).</p>
<p><strong>Survivants</strong></p>
<p>Peuvent prétendre à une rente de survivants, les veufs et veuves qui au moment du décès de leur conjoint ont un ou plusieurs enfants, les veuves sans enfant âgées de 45 ans et ayant été mariées durant au moins cinq ans ; la personne divorcée peut être assimilée sous certaines conditions à un veuf ou une veuve. En cas de remariage ou de décès, en cas de droit à une rente de vieillesse ou d’invalidité, la rente de veuve ou de veuf continue d’être versée si elle est d’un montant plus élevé. Pour le veuf, la rente est servie jusqu’à ce que le dernier enfant ait atteint l’âge de 18 ans. Depuis le 1er janvier 2007, le partenariat enregistré entre personne du même sexe est pendant toute sa durée assimilé au mariage dans le droit aux assurances sociales : le partenaire survivant, quel que soit le sexe est assimilé à un veuf et la dissolution du partenariat est assimilé à un divorce.</p>
<p>Les enfants dont le père ou la mère est décédé ont également droit à une rente jusqu’à l’âge de 18 ans ou 25 ans en cas de poursuite d’études.</p>
<p><strong>Montant</strong><br class="manualbr">La rente de veuve ou de veuf correspond à 80 % de la rente de vieillesse (minimum : 928 CHF par mois - maximum : 1856 CHF par mois). La rente d’orphelin est égale à 40 % de la rente de vieillesse (minimum : 464 CHF par mois – maximum : 928 CHF par mois).</p>
<h5 class="spip">Rentes extraordinaires</h5>
<p>Les rentes extraordinaires de vieillesse sont accordées, sous condition de ressources, de résidence et de nationalité, à ceux qui n’ont pas droit à une rente ordinaire ou dont la rente ordinaire est inférieure à la rente extraordinaire. La rente extraordinaire est, en général, accordée lorsque le revenu annuel de l’assuré, auquel est ajoutée une part équitable de sa fortune, ne dépasse pas un certain plafond.</p>
<p>Le montant des rentes extraordinaires est le même que celui des minimum des rentes ordinaires complètes correspondantes. Elles sont toutefois réduites dans la mesure où, ajoutées aux deux tiers du revenu et à la part de fortune prise en compte pour l’ouverture du droit, elles dépassent la limite de revenu déterminant en la matière.</p>
<p><strong>Allocation pour impotence</strong></p>
<p>Les bénéficiaires d’une rente de vieillesse qui sont domiciliés en Suisse et souffrent d’une impotence grave ou moyenne ont droit respectivement, en plus de la rente, à une allocation pour impotence égale à 80 % et 50 % du montant minimum de la rente simple de vieillesse, soit 928 CHF ou 580 CHF par mois. Les bénéficiaires de rentes de vieillesse qui percevaient déjà une allocation pour impotence de l’assurance invalidité, immédiatement avant d’atteindre la limite d’âge ouvrant droit à la rente de vieillesse, continuent automatiquement à bénéficier d’une allocation pour impotence du même montant.</p>
<h4 class="spip">Deuxième pilier : la prévoyance professionnelle</h4>
<p>La loi fédérale du 25 juin 1982 sur la prévoyance professionnelle vieillesse, survivants et invalidité (L.P.P.) qui est entrée en vigueur au 1er janvier 1985 est venue compléter le système suisse de prévoyance pour les cas de vieillesse, de décès et d’invalidité.</p>
<h5 class="spip"><strong>Champ d’application</strong></h5>
<p>La L.P.P. prévoit un régime obligatoire pour les salariés, et facultatif pour les indépendants (ainsi que pour les salariés exemptés du régime obligatoire). Pour relever de la L.P.P., il faut être assuré à L’A.V.S./ A.I. fédérale.</p>
<p>Le régime obligatoire englobe tous les salariés qui reçoivent de leur employeur un salaire A.V.S. annuel supérieur à 20 88 CHF, et ce, dès le 1er janvier de l’année qui suit la date où ils ont eu 17 ans (pour les risques de décès et d’invalidité) et 24 ans (pour le risque vieillesse). La partie du salaire qui doit être assurée, ou salaire coordonné est comprise entre 24360 CHF et 83 520 CHF.</p>
<p>Depuis le 1er janvier 2009, les personnes qui effectuent deux ou plusieurs engagements pour le même employeur seront également assurées au 2ème pilier lorsque les périodes pour le même employeur se succèdent à 3 mois au moins d’intervalle et qu’il n’y a pas d’interruption entre chaque engagement. Jusqu’ici il n’y avait pas de totalisation des différentes périodes de travail pour un même employeur : il fallait que chaque période interrompue soit supérieure à 3 mois et une interruption de 99 jours suffisait à faire repartir à zéro le décompte de trois mois.</p>
<p>Les salariés sans activité en Suisse, ou dont l’activité en Suisse n’a probablement pas un caractère durable, peuvent demander à se faire exempter, s’ils prouvent qu’ils bénéficient de mesures de prévoyance suffisantes à l’étranger.</p>
<p>Les travailleurs indépendants, ceux qui sortent du régime obligatoire, ceux exclus du régime obligatoire peuvent cotiser au régime facultatif de la L.P.P.</p>
<h5 class="spip"><strong>Organisation</strong></h5>
<p>Le régime complémentaire de pension L.P.P. est à la fois décentralisé et mis en œuvre par des institutions jouissant d’une large autonomie de gestion. La loi se contente d’indiquer les principes directeurs de la prévoyance vieillesse obligatoire.</p>
<p>Certaines institutions de prévoyance assurent elles-mêmes la couverture de tous les risques, d’autres ont confié la couverture d’un ou plusieurs risques à une compagnie d’assurance, d’autres enfin, ont par un contrat d’assurance collectif, transféré la couverture de tous les risques à une institution d’assurance ou à une fondation bancaire.</p>
<p>Les employeurs sont tenus d’assurer leur personnel en s’affiliant à une institution de prévoyance enregistrée.</p>
<p>La L.P.P. offre aux assurés une protection minimale. Les institutions doivent servir au moins les prestations légales, mais elles peuvent offrir une prévoyance plus étendue.</p>
<h5 class="spip"><strong>Prestations</strong></h5>
<p>La rente de vieillesse est calculée en pourcentage d’un "avoir vieillesse". Cet "avoir vieillesse" est composé des bonifications de vieillesse calculées annuellement en pourcentage du salaire coordonné. Ces pourcentages varient en fonction de l’âge de l’assuré.</p>
<p>L’institution de prévoyance tient pour chaque assuré un compte de vieillesse crédité chaque année civile de l’avoir vieillesse et des intérêts de l’avoir placé.</p>
<p>Les rentes professionnelles sont extrêmement disparates puisqu’elles dépendent non seulement de la carrière de chaque assuré, mais également du mode du calcul retenu par l’institution de prévoyance.</p>
<p>A l’âge de la retraite, l’avoir vieillesse sert de base au calcul de la rente. Cette rente est égale à un pourcentage de l’avoir vieillesse acquis par l’assuré. La prestation peut être versée en capital si le règlement de l’institution de prévoyance le prévoit ou lorsque le montant de la rente est inférieur à 10 % de la rente simple minimale du premier pilier.</p>
<h5 class="spip"><strong>Le libre passage</strong></h5>
<p>Quand les rapports de travail prennent fin, l’avoir vieillesse de l’assuré est transféré sous la forme d’une prestation de libre passage, à l’institution de prévoyance du nouvel employeur.</p>
<h3 class="spip"><a id="sommaire_5"></a>Prestations complémentaires à l’A.V.S. et à l’A.I. (P.C.)</h3>
<p>Versées par les cantons, les prestations complémentaires ont pour objectif de permettre aux personnes âgées, aux survivants et aux invalides de faire face à leurs besoins élémentaires (revenu minimum garanti).</p>
<p>Cette prestation, qui ne doit pas être confondue avec une aide de l’assistance publique ou privée, peut être attribuée aux étrangers habitant la Suisse de manière ininterrompue depuis au moins dix ans ainsi qu’aux réfugiés ou apatrides s’y trouvant, sans discontinuité, depuis au moins cinq ans.</p>
<p>La prestation complémentaire représente la différence entre les revenus disponibles et le montant minimum susvisé. La demande doit être présentée auprès de l’office communal ou de l’agence communale compétent(e).</p>
<h4 class="spip"><strong>Assurance – accidents</strong></h4>
<p>La loi fédérale sur l’assurance accidents est entrée en vigueur le 1er janvier 1984. L’assurance accidents est gérée, selon la catégorie d’assurés, par la Caisse nationale suisse d’assurance accidents (C.N.A.) et par d’autres assureurs accidents agréés (compagnies d’assurances, caisses maladie).</p>
<h4 class="spip"><strong>Personnes assurées</strong></h4>
<p>Sont obligatoirement assurés, les travailleurs occupés en Suisse, y compris les travailleurs à domicile, les apprentis, les stagiaires, les volontaires, les personnes travaillant dans les écoles de métiers ou des ateliers protégés, ainsi que certaines catégories particulières de personnes exerçant une activité lucrative.</p>
<p>Peuvent s’assurer volontairement, les personnes domiciliées en Suisse qui exercent une activité lucrative indépendante et les membres de leur famille qui collaborent à l’entreprise.</p>
<h5 class="spip"><strong>Risques garantis</strong></h5>
<p><strong>Les accidents professionnels</strong><br class="manualbr">Il s’agit des accidents qui surviennent lorsque le travailleur se trouve sous la subordination de son employeur ou lorsqu’il travaille dans l’intérêt de ce dernier sur le lieu de travail ou dans une zone de danger liée au travail.</p>
<p><strong>Les accidents non professionnels</strong><br class="manualbr">Il s’agit de tous les accidents qui ne peuvent pas être considérés comme accidents professionnels, y compris les accidents de trajet (sauf pour les travailleurs occupés à temps partiel dont la durée de travail n’atteint pas le minimum requis de huit heures par semaine et pour lesquels l’accident de trajet est considéré accident professionnel).</p>
<p><strong>Les maladies professionnelles</strong><br class="manualbr">Maladies qui, dans l’exercice de l’activité professionnelle, sont dues exclusivement ou de manière prépondérante à des substances nocives ou à certains travaux ainsi que les autres maladies dont il est prouvé qu’elles ont été causées exclusivement ou de manière nettement prépondérante par l’exercice de l’activité professionnelle</p>
<h5 class="spip"><strong>Prestations de l’assurance</strong></h5>
<p><strong>Soins</strong><br class="manualbr">L’assurance couvre les frais de traitement médical, dentaire, de médicaments, d’hospitalisation, de cures, d’autres moyens et appareils servant à la guérison, de moyens auxiliaires, les frais de transports, les frais funéraires.</p>
<p><strong>Indemnités journalières</strong><br class="manualbr">Elles sont servies à partir du 3e jour qui suit celui de l’accident. En cas d’incapacité totale, l’indemnité s’élève à 80 % des gains assurés, si l’incapacité n’est que partielle l’indemnité est réduite en conséquence. L’indemnité journalière est versée jusqu’à ce que la capacité de travail soit entièrement recouvrée ou qu’une rente soit allouée.</p>
<p><strong>Rente</strong><br class="manualbr">Elle est accordée à l’assuré dont la capacité de gain est réduite, vraisemblablement de façon permanente ou pour une longue durée. Elle est accordée à partir de la stabilisation de l’état. Elle est égale pour une incapacité totale à 80 % du gain assuré. Si l’invalidité est partielle, la rente est réduite en conséquence.<br class="manualbr">Lorsque les assurés ouvrent droit à une rente A.V.S. ou A.I., l’assurance accident attribue une rente complémentaire dont le montant correspond à la différence entre la rente A.V.S. / A.I. et 90 % du gain assuré.</p>
<p><strong>Indemnité en capital</strong><br class="manualbr">Lorsque l’on peut prévoir que l’assuré recouvrera sa capacité de gain, les prestations cessent d’être allouées et il reçoit une indemnité en capital d’un montant maximum de trois fois le gain annuel assuré.</p>
<p><strong>Indemnité pour atteinte à l’intégrité</strong><br class="manualbr">Si à la suite de l’accident, une personne souffre d’une atteinte importante et durable à son intégrité physique ou mentale, elle a droit à une indemnité équitable pour atteinte à l’intégrité. Cette indemnité versée sous forme de capital dépend de la gravité de l’atteinte à l’intégrité.</p>
<p><strong>Allocation pour impotents</strong><br class="manualbr">Versée aux assurés qui ont besoin de l’aide d’une tierce personne pour accomplir les actes ordinaires de la vie. Le montant de l’allocation est fixé en fonction du degré d’impotence.</p>
<p><strong>Prestations aux survivants</strong><br class="manualbr">La veuve et (sous certaines conditions) le veuf, les orphelins ont droit à une rente.<br class="manualbr">Si les survivants ont également droit à une rente de l’A.V.S./A.I., l’assurance accident leur attribue une rente complémentaire dont le montant correspond à la différence entre 90 % du gain assuré et la rente de l’A.V.S./A.I.</p>
<h3 class="spip"><a id="sommaire_6"></a>Prestations familiales</h3>
<p>Les prestations familiales en Suisse sont régies par la loi fédérale sur les allocations familiales (LAFam) applicable aux salariés qui n’exercent pas d’activité dans l’agriculture, aux personnes sans activité lucrative ayant un revenu modeste. Les salariés et les non salariés agricoles sont régis par la loi fédérale sur les allocations familiales dans l’agriculture (LFA) qui constitue une règlementation spécifique. Au niveau fédéral la LAFam n’est pas applicable aux travailleurs indépendants, mais certains cantons ont introduit des allocations familiales pour les indépendants. Dans ce cas, les intéressés sont assujettis de manière volontaire ou obligatoire en fonction des cantons et ils bénéficient de prestations identiques à celles des travailleurs salariés. Toutefois dans certains cantons, il existe des limites de revenus et le versement des allocations familiales aux travailleurs indépendants est soumis à une condition de revenus inférieurs à un certain plafond.</p>
<p>La LAFam prévoit le versement d’allocations familiales destinées aux parents afin de les aider à assumer l’entretien de leurs enfants. Elles comprennent les allocations pour enfants et les allocations de formation professionnelle, ainsi que dans certains cantons, les allocations de naissance et des allocations d’adoption.</p>
<p>La LAFam est une loi cadre qui se borne à fixer les dispositions générales relatives aux allocations familiales. La loi fédérale fixe les minima et les cantons peuvent compléter et améliorer la législation fédérale. Les allocations familiales sont versées à partir du premier enfant.</p>
<h4 class="spip"><strong>Allocation pour enfant</strong></h4>
<p>L’allocation pour enfant d’un montant minimum de 200 CHF par enfant et par mois est servie jusqu’aux 16 ans de l’enfant ou 20 ans pour les enfants incapables d’exercer une activité lucrative.</p>
<h4 class="spip"><strong>Allocation de formation</strong></h4>
<p>L’allocation de formation professionnelle est accordée aux enfants à partir de 16 ans et jusqu’à la fin de la formation et au plus tard 25 ans. Elle s’élève à 250 CHF par enfant et par mois.</p>
<p>Les cantons peuvent verser des prestations d’un montant plus élevé et également servir des allocations de naissance ou d’adoption.</p>
<p>Une seule allocation familiale peut être servie par enfant. Si plusieurs personnes (le père, la mère ou d’autres ayants droit potentiels) peuvent prétendre à des allocations familiales, l’ordre de priorité suivant s’applique :</p>
<ul class="spip">
<li>la personne qui exerce une activité lucrative</li>
<li>la personne qui détient l’autorité parentale</li>
<li>si l’autorité parentale est détenue conjointement, ou si aucun ayant droit ne la détient, c’est la personne qui vit principalement avec l’enfant qui perçoit les allocations familiales.</li>
<li>lorsque les deux parents vivent avec l’enfant, la priorité revient à la personne qui travaille dans le canton où vit l’enfant</li>
<li>lorsque les deux parents travaillent dans le canton où vit l’enfant, les allocations familiales sont servies à la personne qui perçoit le plus gros revenu soumis à l’AVS.</li></ul>
<p>Les allocations familiales sont versées pour les enfants résidant à l’étranger lorsque la Suisse a signé une convention de sécurité sociale avec le pays en cause.</p>
<h5 class="spip"><strong>Travailleur salarié</strong></h5>
<p>La personne qui travaille à temps partiel a droit à des allocations familiales entières si son salaire s’élève à au moins 580 CHF par mois ou 6 960 CHF par an.</p>
<p>Lorsqu’une personne a plusieurs emplois, ses revenus sont additionnés et les allocations familiales sont dues par l’employeur qui verse le salaire le plus élevé.</p>
<p>Le droit aux allocations familiales nait et s’éteint en même temps que le droit au salaire. Lorsqu’une personne ne peut pas travailler par suite de maladie ou d’accident, les allocations sont versées durant le mois où l’évènement s’est produit et durant les trois mois suivants. Elles sont également versées pendant le congé de maternité pendant 16 semaines au plus.</p>
<h5 class="spip"><strong>Personnes sans activité</strong></h5>
<p>Les personnes sans activité lucrative ont droit aux allocations familiales si leurs revenus imposables selon le droit fédéral ne dépassent pas 41 760 CHF par an et si elles ne perçoivent pas de prestations complémentaires à l’AVS/AI. Sont aussi exclues du droit, les personnes qui touchent une rente ordinaire de vieillesse ou dont le conjoint reçoit une telle rente ou exerce une activité indépendante. Certains cantons, comme celui de Vaud, ont relevé la limite de revenu à 55 680 CHF, les cantons de Genève et du Jura ont quant à eux supprimé la condition de revenu. Tous les autres cantons appliquent la limite de revenus fixée par LAFam.</p>
<h5 class="spip"><strong>Versement des allocations</strong></h5>
<ul class="spip">
<li>Les salariés reçoivent les allocations familiales en principe de leur employeur en même temps que leur salaire. Les allocations familiales versées par l’employeur sont déduites des cotisations dues à la caisse d’allocations familiales.</li>
<li>Les salariés d’employeurs non soumis à l’obligation de cotiser touchent leurs prestations directement de la caisse d’allocations familiales.</li>
<li>Les personnes qui n’exercent pas d’activité lucrative reçoivent en principe leurs prestations directement de la caisse de compensation AVS de leur canton de domicile.</li></ul>
<h5 class="spip"><strong>Age limite</strong></h5>
<p>La limite d’âge est repoussée à 25 ans pour les apprentis et les étudiants dans la majorité des cantons et à 18, 20 ou 25 ans selon les cantons, en cas de maladie ou d’infirmité.</p>
<h3 class="spip"><a id="sommaire_7"></a>Chômage (A.C.)</h3>
<h4 class="spip"><strong>Champ d’application</strong></h4>
<p>Sont assurés obligatoirement à l’assurance chômage tous les salariés assurés selon la loi sur l’assurance vieillesse survivants, qui n’ont pas encore atteint l’âge ouvrant droit à une rente de vieillesse de l’AVS et qui ne perçoivent pas de rente de vieillesse anticipée de l’AVS.</p>
<h4 class="spip"><strong>Financement</strong></h4>
<p>L’assurance chômage est financée par les cotisations des assurés et des employeurs et par une participation de la Confédération.</p>
<h4 class="spip"><strong>Organisation</strong></h4>
<p>Les caisses de compensation A.V.S. et leurs agences sont compétentes en matière de recouvrement de cotisations d’assurance chômage. S’agissant des prestations, il convient de se mettre en rapport avec les caisses d’assurance chômage ou avec les offices du travail des cantons et communes.</p>
<p>En tout état de cause, l’assurance chômage est gérée par les caisses publiques (caisses de chômage cantonales par exemple) et par les caisses privées agréées : le chômeur peut choisir celle auprès de laquelle il fera valoir son droit à prestations.</p>
<h4 class="spip"><strong>Conditions</strong></h4>
<p>Pour pouvoir prétendre aux prestations de chômage le salarié doit :</p>
<ul class="spip">
<li>être sans emploi ou partiellement sans emploi,</li>
<li>avoir subi une perte de travail d’au moins deux jours consécutifs, assortie d’un manque à gagner</li>
<li>être domicilié en Suisse</li>
<li>avoir achevé sa scolarité obligatoire</li>
<li>ne pas avoir atteint l’âge donnant droit à une rente de l’AVS et ne pas percevoir de rente anticipée de l’AVS</li>
<li>remplir les conditions relatives à la période de cotisations ou en être libéré du fait d’une période de formation, maladie, maternité, accidents,</li>
<li>être apte au travail</li>
<li>satisfaire aux exigences du contrôle : participation à la journée d’information et aux entretiens personnels de conseil et de contrôle.</li></ul>
<p>Le droit aux prestations est subordonné à l’accomplissement d’un stage de douze mois de travail au cours des deux dernières années précédant la réalisation du risque.</p>
<h4 class="spip"><strong>Montant</strong></h4>
<p>Le montant des indemnités de chômage est égal à 80 % du gain assuré dans la limite d’un plafond mensuel de 3.797 CHF. Il est de 70 % du gain assuré pour les chômeurs qui n’ont pas d’enfants à charge et ceux qui ne sont pas invalides.</p>
<p>Le gain assuré représente le salaire des 6 mois précédant l’indemnisation, dans la limite de 10 500 CHF par mois.</p>
<h4 class="spip"><strong>Service de la prestation</strong></h4>
<p>Le délai d’attente général est fixé à cinq à vingt jours. En plus de ce délai il peut y avoir un délai d’attente spécial :</p>
<ul class="spip">
<li>120 jours pour les personnes libérées des conditions relatives à la période de cotisations âgées de moins de 25 ans, qui n’ont pas d’obligations d’entretien envers des enfants et qui ne bénéficient d’aucune formation professionnelle achevée</li>
<li>5 jours pour les autres personnes libérées des conditions relatives à la période de cotisations</li>
<li>1 jour pour l’assuré qui est au terme d’une activité saisonnière ou au terme de l’exercice d’une profession dans laquelle les changements d’employeurs ou les contrats de durée limitée sont usuels.</li></ul>
<p>Le délai cadre de versement des prestations de chômage est fixé à deux ans. Le nombre d’indemnités est variable :</p>
<table class="spip">
<thead><tr class="row_first"><th id="id74dc_c0">Durée de cotisation (en mois)</th><th id="id74dc_c1">Age / Obligation d‘entretien</th><th id="id74dc_c2">Conditions</th><th id="id74dc_c3"> Allocations journalières</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="id74dc_c0">12 à 24</td>
<td headers="id74dc_c1">Jusqu’à 25 sans obligation d’entretien</td>
<td headers="id74dc_c2"></td>
<td class="numeric " headers="id74dc_c3">200</td></tr>
<tr class="row_even even">
<td headers="id74dc_c0">12 à  &lt;18</td>
<td headers="id74dc_c1">Dès 25</td>
<td headers="id74dc_c2"></td>
<td class="numeric " headers="id74dc_c3">260</td></tr>
<tr class="row_odd odd">
<td headers="id74dc_c0">12 à  &lt;18</td>
<td headers="id74dc_c1">Avec obligation d’entretien</td>
<td headers="id74dc_c2"></td>
<td class="numeric " headers="id74dc_c3">260</td></tr>
<tr class="row_even even">
<td headers="id74dc_c0">18 à 24</td>
<td headers="id74dc_c1">Dès 25</td>
<td headers="id74dc_c2"></td>
<td class="numeric " headers="id74dc_c3">400</td></tr>
<tr class="row_odd odd">
<td headers="id74dc_c0">18 à 24</td>
<td headers="id74dc_c1">Avec obligation d’entretien</td>
<td headers="id74dc_c2"></td>
<td class="numeric " headers="id74dc_c3">400</td></tr>
<tr class="row_even even">
<td headers="id74dc_c0">22 à 24</td>
<td headers="id74dc_c1">Dès 55</td>
<td headers="id74dc_c2"></td>
<td class="numeric " headers="id74dc_c3">520</td></tr>
<tr class="row_odd odd">
<td headers="id74dc_c0">22 à 24</td>
<td headers="id74dc_c1">Dès 25</td>
<td headers="id74dc_c2">Perception d’une rente AI correspondant à un taux d’invalidité d’au moins 40%</td>
<td class="numeric " headers="id74dc_c3">520</td></tr>
<tr class="row_even even">
<td headers="id74dc_c0">22 à 24</td>
<td headers="id74dc_c1">Avec obligation d’entretien</td>
<td headers="id74dc_c2">Perception d’une rente AI correspondant à un taux d’invalidité d’au moins 40%</td>
<td class="numeric " headers="id74dc_c3">520</td></tr>
<tr class="row_odd odd">
<td headers="id74dc_c0">Exonération de cotisations</td>
<td headers="id74dc_c1"></td>
<td headers="id74dc_c2"></td>
<td class="numeric " headers="id74dc_c3">90</td></tr>
</tbody>
</table>
<p><strong>Pour en savoir plus</strong></p>
<ul class="spip">
<li>Site de l’<a href="http://www.bsv.admin.ch/?lang=fr" class="spip_out" rel="external">OFAS</a> : tableau synoptique concernant la sécurité sociale suisse.</li>
<li>Site de l’<a href="http://www.bsv.admin.ch/?lang=fr" class="spip_out" rel="external">OFAS</a> : présentation des différents risques</li></ul>
<p><i>Mise à jour : février 2013</i></p>
<p><span class="spip_note_ref">[<a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/protection-sociale/article/regime-local-de-securite-sociale#nh3-1" class="spip_note" title="Notes 3-1" rev="footnote">1</a>] </span>L’assureur fixe le montant des primes qui est identique pour tous (aucune distinction n’est faite entre salariés et indépendants). Toutefois, si les coûts des prestations en nature sont différents d’un canton à un autre, l’assureur peut moduler le montant de la prime. L’employeur peut verser une contribution ou prendre en charge les primes d’assurance-maladie à titre facultatif.</p>
<p><span class="spip_note_ref">[<a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/protection-sociale/article/regime-local-de-securite-sociale#nh3-2" class="spip_note" title="Notes 3-2" rev="footnote">2</a>] </span>L’allocation pour perte de gain (APG) consiste en des indemnités journalières versées en cas de maternité.</p>
<p><span class="spip_note_ref">[<a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/protection-sociale/article/regime-local-de-securite-sociale#nh3-3" class="spip_note" title="Notes 3-3" rev="footnote">3</a>] </span>Le taux de la prime (fixé en pour mille du gain assuré) varie en fonction du classement de l’entreprise qui occupe le salarié. Le salarié travaillant moins de 8 h/semaine n’est pas obligatoirement assuré.</p>
<p><span class="spip_note_ref">[<a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/protection-sociale/article/regime-local-de-securite-sociale#nh3-4" class="spip_note" title="Notes 3-4" rev="footnote">4</a>] </span>Les entreprises sont classées en classes et degrés de tarifs selon les risques d’accidents et les conditions propres aux entreprises.</p>
<p><span class="spip_note_ref">[<a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/protection-sociale/article/regime-local-de-securite-sociale#nh3-5" class="spip_note" title="Notes 3-5" rev="footnote">5</a>] </span>Pour les revenus inférieurs à 55 700 CHF, le taux de cotisations baisse en fonction de l’échelle dégressive.</p>
<p><span class="spip_note_ref">[<a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/protection-sociale/article/regime-local-de-securite-sociale#nh3-6" class="spip_note" title="Notes 3-6" rev="footnote">6</a>] </span>Pour les revenus inférieurs à 55 700 CHF, le taux de cotisations baisse en fonction de l’échelle dégressive.</p>
<p><span class="spip_note_ref">[<a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/protection-sociale/article/regime-local-de-securite-sociale#nh3-7" class="spip_note" title="Notes 3-7" rev="footnote">7</a>] </span>La LAFam (loi fédérale sur les allocations familiales) régit les droits des salariés qui ne travaillent pas dans l’agriculture. Il s’agit d’une loi cadre qui fixe le montant minimum des allocations familiales qui doivent être servies par les cantons. Ces derniers peuvent, s’ils le souhaitent, accorder des montants plus élevés ou encore servir des prestations non prévues par la loi comme des allocations de naissance ou d’adoption. Le montant de la cotisation varie en fonction des cantons. Un seul canton (le canton du Valais) fixe une cotisation à la charge du salarié (0,3 % sur la totalité du salaire). Dans tous les autres cantons la cotisation est payée uniquement par l’employeur. La LFA (loi fédérale sur les allocations familiales dans l’agriculture) ne concerne que les travailleurs agricoles et les agriculteurs indépendants.</p>
<p><span class="spip_note_ref">[<a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/protection-sociale/article/regime-local-de-securite-sociale#nh3-8" class="spip_note" title="Notes 3-8" rev="footnote">8</a>] </span>Règlementation différente selon les cantons : cotisation obligatoire dans les cantons de BE, GL, BS, BL, SH, AR, VD et GE. Le montant de la cotisation varie selon les CAF.</p>
<p><span class="spip_note_ref">[<a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/protection-sociale/article/regime-local-de-securite-sociale#nh3-9" class="spip_note" title="Notes 3-9" rev="footnote">9</a>] </span>De plus, l’employeur et le salarié versent chacun une cotisation de solidarité égale à 0,5 % sur la tranche de salaire comprise entre 126 000 et 315000 CHF. Aucune cotisation n’est due pour les salaires excédant 315 000 CHF.</p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/protection-sociale/article/regime-local-de-securite-sociale). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
