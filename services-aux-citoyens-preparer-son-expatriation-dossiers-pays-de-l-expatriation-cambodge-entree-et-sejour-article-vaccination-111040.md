# Vaccination

<p>Aucune vaccination n’est exigée à l’entrée dans le pays.</p>
<p>Sont recommandées pour des raisons médicales :</p>
<ul class="spip">
<li>pour les adultes : mise à jour des vaccinations contre la diphtérie, le tétanos et la poliomyélite ; vaccination contre la typhoïde, l’hépatite A, l’hépatite B ;</li>
<li>pour les enfants : vaccinations recommandées en France par le Ministère de la Santé et en particulier : BCG et hépatite B dès la naissance, rougeole dès l’âge de neuf mois ; encéphalite japonaise à partir de l’âge d’un an en cas de séjours prolongés en zone rurale.</li></ul>
<p>Les vaccins sont disponibles auprès de l’Institut Pasteur du Cambodge (5 boulevard Monivong - Tél. : (855) 23 368 036).</p>
<p>Il est préférable de réaliser toutes les vaccinations nécessaires avant de partir, car une fois sur place on peut rencontrer des difficultés d’approvisionnement.</p>
<p>Pour en savoir plus, lisez notre rubrique thématique <a href="services-aux-citoyens-preparer-son-expatriation-sante.md" class="spip_in">Santé</a>.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/cambodge/entree-et-sejour/article/vaccination-111040). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
