# Haute montagne

<p><strong></strong></p>
<h5 class="spip">Avant le départ en haute montagne</h5>
<p>Informez-vous auprès de professionnels de la montagne (guides, forces de sécurité locales…) ou de services apportant des informations fiables (ambassade de France, consulat, offices de tourisme…) des conditions de sécurité entourant le projet de trek. <br class="manualbr"></p>
<p>Consultez attentivement la page Conseils aux voyageurs du pays dans le lequel vous souhaitez réaliser le trek et assurez-vous qu’il n’est pas situé dans une zone déconseillée par le ministère des Affaires étrangères et du Développement international. <br class="manualbr"></p>
<p>Vérifiez :</p>
<ul class="spip">
<li>les prévisions météorologiques locales
l’état des sentiers, des pistes ou des voies que vous souhaitez emprunter ;</li>
<li>les risques existants sur l’itinéraire envisagé selon la période de l’année ;</li>
<li>la nécessité d’être accompagné ou non d’un guide agréé (vous pouvez vous renseigner auprès du consulat ou de l’ambassade de France sur les organismes compétents pour se prononcer sur la qualité et l’éventuelle habilitation des guides locaux) ;</li>
<li>les risques sanitaires en demandant conseil à votre médecin traitant. <br class="manualbr"></li></ul>
<p>Munissez-vous des plans et des cartes à jour et aux bonnes échelles ainsi que des coordonnées nécessaires pour demander de l’aide si nécessaire. <br class="manualbr"></p>
<p>Avertissez un proche ou à défaut, les guides de haute montagne, l’ambassade ou le consulat de France de votre projet d’ascension en fournissant l’itinéraire et l’horaire prévus, la composition de l’équipe, les moyens de communication dont vous disposez. Laissez également des informations permettant de vous identifier (tenue vestimentaire, photo de vous avant le départ…) pour faciliter d’éventuelles recherches. <br class="manualbr"></p>
<p>Souscrivez une assurance rapatriement en vérifiant soigneusement les clauses du contrat. Toutes les assurances voyages ne couvrent pas les frais de recherche et d’évacuation en montagne, en particulier celles liées à une carte de crédit. De même, parfois, seuls les accidents sont couverts et non les maladies, comme le mal des montagnes.</p>
<p><br class="manualbr"><strong>Bien préparer son matériel</strong><br class="manualbr">Vous devez vous munir d’un matériel en bon état et adapté :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  vérifiez que vous disposez de chaussures adéquates, d’un casque etc. Selon les cas, des crampons, un piolet, un jeu de cordes et d’assurances, des gants sont nécessaires.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  une protection appropriée aux conditions météorologiques changeantes est indispensable : il est nécessaire de prévoir des vêtements, des couchages et des couvertures de survie adaptés qui protègent des variations de température et de temps. </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  les cartes récentes sont indispensables lors de tout déplacement en montagne. Des instruments tels qu’une boussole, un altimètre voire un système GPS sont utiles. </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  il est nécessaire de disposer dans sa trousse des produits ou des médicaments de première urgence. La composition de la trousse varie selon le lieu et le projet entrepris. </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  emporter des moyens de communication adaptés est un gage de sécurité (téléphones GSM ou satellites, émetteur radio…). Vérifiez le niveau de charge des batteries.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  prévoyez des vivres, un couteau multifonctions (à ranger dans les bagages en soute pour les voyageurs prenant l’avion en raison des contrôles effectués lors de l’embarquement), une lampe torche ou frontale, des torches fumigènes pour signaler votre présence.</p>
<p><br class="manualbr"></p>
<p><strong></strong></p>
<h5 class="spip">Pendant l’ascension ou le trek</h5>
<p>Il est important de rester vigilant de manière permanente. Ne jamais aller au delà de ses capacités physiques. Surveillez le comportement de vos équipiers, notamment les manifestations de fatigue excessive.</p>
<p>Attention au <strong>Mal Aigu des Montagnes</strong> qui se manifeste dès 2 000 mètres d’altitude et qui peut être bénin ou mortel. Toute personne, quelle que soit sa condition physique, peut être concernée par le mal des montagnes. Sa prévention repose sur trois règles essentielles :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  respecter une ascension lente (pas plus de 400 m de dénivelé positif entre deux nuits consécutives à partir de 2500 m) et bien s’hydrater ;</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  en cas de symptômes de mal des montagnes, arrêter l’ascension ;</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  si les symptômes persistent ou empirent, redescendre.
Dès qu’un cas grave est soupçonné, la descente immédiate aussi bas que possible (et au minimum en dessous de 4 000 m) est impérative.</p>
<p>Maîtrisez votre itinéraire et adaptez-vous aux différentes circonstances et aux reliefs : restez dans les sentiers balisés ou les itinéraires préparés en amont du départ. Soyez attentif aux incidents dont vous pouvez être témoin. Le non-respect des dispositions relatives à la sécurité des personnes et des biens ou à la protection de l’environnement peut engager votre responsabilité civile et pénale en fonction de la législation locale.</p>
<p>Restez attentif à l’évolution de la météo : l’augmentation brusque de la température, en particulier, accroît le risque d’avalanches ou d’éboulements. En cas d’orages, le risque d’être atteint par la foudre est réel. Soyez prudent au bord des torrents et des rivières ou en aval d’un barrage, une montée des eaux subite est toujours possible.</p>
<p><strong></strong></p>
<h5 class="spip">En cas d’accident, appliquez le plan  « P.A.S »</h5>
<ol class="spip">
<li>D’abord, <strong>Protéger la victime</strong> : mettez la à l’abri d’un nouvel accident, protégez la du vent et du froid. Toute manipulation de la victime est susceptible de mettre sa santé en danger, soyez prudent !</li>
<li>Puis, <strong>Alerter les secours</strong> : précisez votre nom, votre numéro de téléphone ou votre fréquence, le lieu de l’accident, l’heure et la nature de l’accident, la nature des blessures constatées, le nombre de personnes impliquées, les premiers soins apportés.</li>
<li>Enfin, <strong>Secourir</strong> : apportez les premiers soins si vous connaissez les gestes de secourisme adaptés ou attendez les instructions des professionnels, tout en surveillant en permanence l’évolution de l’état de la personne.</li></ol>
<p><br class="manualbr"></p>
<p><strong>En savoir plus</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/infos-pratiques/preparer-son-depart/assurances/article/assurances" class="spip_in">Assurances</a></p>
<p><strong> <i>Cette fiche-réflexe a été élaborée en collaboration avec les services de la Compagnie Républicaine de Sécurité de Briançon, spécialisée dans les secours en montagne, pour vous permettre de préparer au mieux vos projets d’ascension ou de trek.</i> </strong></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/infos-pratiques/risques/risques-naturels/article/haute-montagne-93149). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
