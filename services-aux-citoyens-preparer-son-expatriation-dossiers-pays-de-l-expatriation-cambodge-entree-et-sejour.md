# Entrée et séjour

<h2 class="rub22974">Passeport, visa, permis de travail</h2>
<p>Quel que soit le motif de votre séjour, renseignez-vous auprès de la section consulaire de l’ambassade du Cambodge à Paris qui vous informera sur la règlementation en matière d’entrée et de séjour au Cambodge, règlementation que vous devrez impérativement respecter. N’hésitez pas à consulter le site Ministère de l’Intérieur cambodgien et les services de l’Immigration afin de vous familiariser avec les modalités d’entrée et de séjour dans le pays.</p>
<p>Une fois sur place c’est le Service cambodgien de l’Immigration qui prend le relais. <strong>En aucun cas, vous n’êtes autorisé à travailler au Cambodge sans permis adéquat.</strong></p>
<p>Le Consulat de France au Cambodge n’est pas l’organisme compétent pour répondre à vos demandes d’information concernant votre séjour au Cambodge.</p>
<h4 class="spip">Les différents types de visa</h4>
<p><strong>Visa de tourisme</strong></p>
<p>Le visa est obligatoire. Il peut être délivré à l’arrivée à l’aéroport international de Phnom-Penh par les services de l’immigration (contre paiement d’une taxe de 20 USD pour les touristes). La suppression de cette facilité ayant déjà été annoncée par le ministère des Affaires étrangères, il semble plus prudent d’obtenir le visa avant le départ. Les visas de tourisme ouvrent droit à un séjour d’un mois. Ils peuvent être prorogés d’un mois maximum, mais une seule fois, par les services de l’immigration de Phnom-Penh. La durée de validité du passeport doit excéder de six mois celle du visa. Les points de passage de la frontière cambodgienne autorisés pour les étrangers sont les suivants :</p>
<p>Aéroportuaires :</p>
<ul class="spip">
<li>Phnom-Penh (Pochentong)</li>
<li>Siem Reap</li></ul>
<p>Portuaires :</p>
<ul class="spip">
<li>Sihanoukville,</li>
<li>Koh Kong (également routier)</li></ul>
<p>Fluvial :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Phnom Penh, par le Mékong (Kaoh Kok) en direction du Vietnam </p>
<p>Routiers :</p>
<ul class="spip">
<li>Thailande : Poipet (province de Banteaymeanchey), O’Smach et Chuam (province de Udormencheay), Prum (province de Pailin), Daung (province de Battambang) et Cham Yem (province de Koh Kong).</li>
<li>Vietnam : Bavet (province de Svay Rieng), Kha Orm Som Nor (province de Kandal), Phnom Den (province de Takeo), Tropiang Sre (province de Kratie), Tropiang Phlong (province de Kompong Cham), Prek Chak (province de Kampot) et Bantey Chakrey (province de Prey Veng)</li>
<li>Laos : Dong Kro Lor (province de Stung Treng) O’Yadav (province de Rattana Kiri</li></ul>
<p>Pour effectuer une mission (de moins ou de plus de trois mois), il faut obtenir auprès des services de l’immigration un visa de séjour correspondant à la durée envisagée, sur présentation des pièces justificatives (contrat de travail, ordre de mission, etc.).</p>
<p><strong>Visa d’affaires</strong></p>
<p>Pour les hommes d’affaires, un visa d’entrée est accordé à l’arrivée à l’aéroport une fois remplis les formulaires administratifs, moyennant photo d’identité et paiement d’un droit de visa de 25 USD. Le visa est accordé pour une durée d’un mois. Certaines ambassades du Cambodge délivrent également des visas. Pour la prolongation du visa, il suffit de se présenter au Ministère de l’Intérieur, Département des Etrangers, deux semaines avant la date d’expiration du visa, avec une photo d’identité et le passeport. Une prolongation de 6 mois est accordée moyennant le paiement d’un droit de 150 USD, pour une durée d’une année il faut s’acquitter de la somme de 280 USD.</p>
<p>Pour toute information actualisée, il est conseillé de prendre l’attache des services de l’immigration auprès de l’ambassade du Cambodge en France.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.ambcambodgeparis.info/?q=visa-entree-cambodge" class="spip_out" rel="external">Section consulaire de l’ambassade du Cambodge à Paris</a>.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-cambodge-entree-et-sejour-article-animaux-domestiques.md" title="Animaux domestiques">Animaux domestiques</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-cambodge-entree-et-sejour-article-vaccination-111040.md" title="Vaccination">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-cambodge-entree-et-sejour-article-demenagement-111039.md" title="Déménagement">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-cambodge-entree-et-sejour-article-passeport-visa-permis-de-travail.md" title="Passeport, visa, permis de travail">Passeport, visa, permis de travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/cambodge/entree-et-sejour/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
