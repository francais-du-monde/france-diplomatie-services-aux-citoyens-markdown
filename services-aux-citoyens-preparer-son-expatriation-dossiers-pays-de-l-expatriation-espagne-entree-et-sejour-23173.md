# Entrée et séjour

<h2 class="rub23173">Passeport, visa, permis de travail</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/entree-et-sejour-23173/#sommaire_1">Les différents types de visa</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/entree-et-sejour-23173/#sommaire_2">Travailler en Espagne</a></li></ul>
<p>Quel que soit le motif de votre séjour, renseignez-vous auprès de la section consulaire de l’ambassade d’Espagne à Paris qui vous informera sur la règlementation en matière d’entrée et de séjour en Espagne, règlementation que vous devrez impérativement respecter. N’hésitez pas à consulter le site ministère de l’Intérieur espagnol afin de vous familiariser avec les modalités d’entrée et de séjour dans le pays.</p>
<p><strong>En aucun cas, vous n’êtes autorisé à travailler en Espagne sans permis adéquat. </strong></p>
<p>Les consulats généraux de France en Espagne ne sont pas les organismes compétents pour répondre à vos demandes d’information concernant votre séjour en Espagne.</p>
<h3 class="spip"><a id="sommaire_1"></a>Les différents types de visa</h3>
<p>En tant que ressortissant d’un pays membre de l’Union européenne, vous avez le droit de vous rendre dans tout autre pays de l’Union européenne sans avoir à remplir de formalités particulières. Il vous suffit d<strong>’être en possession d’une carte nationale d’identité ou d’un passeport en cours de validité</strong>. Aussi longtemps que votre séjour dans un autre pays de l’Union Européenne ne dépasse pas trois mois, vous n’avez pas à demander de titre de séjour.</p>
<p><strong>Enregistrement auprès des autorités locales pour un séjour supérieur à trois mois</strong></p>
<p>Le décret royal 240/2007, entré en vigueur le 2 avril 2007, relatif à l’entrée et le séjour en Espagne de nationaux de pays membres de l’Union européenne a simplifié les conditions de séjour.</p>
<p><strong>Depuis juin 2007, il est délivré un certificat d’inscription en tant que résident permanent </strong>(<i>certificado de registro como residente comunitario</i>).</p>
<p>Les citoyens d’un Etat membre de l’Union européenne ont en effet désormais l’obligation de solliciter personnellement auprès du Bureau des étrangers (<i>Oficina de Extranjeros</i>) de la province où ils souhaitent s’établir, ou, le cas échéant, auprès du commissariat de police (<i>Comisaria de policia</i>) leur inscription au registre central des étrangers. Ils devront pour cela présenter un passeport ou document national d’identité en cours de validité. Un certificat d’enregistrement leur sera expédié dans les plus brefs délais.</p>
<p>Si la carte de résident en elle-même est supprimée, <strong>le N.I.E. (Numéro d’identification pour étranger) demeure en vigueur et il est nécessaire pour diverses démarches </strong>(échange ou enregistrement de permis de conduire, acquisition d’un bien immobilier, inscription dans une agence de travail temporaire, ouverture d’un compte en banque…). De plus, certains employeurs peuvent se montrer réticents à engager un ressortissant français sans ce numéro et, en tout état de cause, retenir 25 % d’impôts sur son salaire - rappelons que l’impôt est retenu à la source - au lieu du taux qui devrait s’appliquer dans le cas particulier.</p>
<p>Le N.I.E. est à solliciter via le formulaire suivant auprès du commissariat du lieu de la résidence (ou Police des étrangers de la province), l’intéressé devant présenter un passeport ou un document d’identité en cours de validité, deux photographies et un formulaire de demande. Sa validité est de cinq ans.</p>
<p>Une fois titulaires de la carte de résident, <strong>les Français ont la possibilité de s’inscrire au recensement municipal </strong>(<i>empadronamiento</i>), formalité qui donne accès aux prestations sociales espagnoles et permet de s’inscrire sur les listes électorales espagnoles pour la participation aux scrutins municipaux et européens.</p>
<p>L’inscription sur les listes complémentaires espagnoles entraîne la suspension du droit de vote pour les élections européennes en France. L’inscription se fait à la mairie (<i>Ayuntamiento</i>) ou dans les mairies annexes de quartier (<i>padrón municipal</i>) dans les grandes villes.</p>
<h3 class="spip"><a id="sommaire_2"></a>Travailler en Espagne</h3>
<p>En vertu du principe de libre circulation des travailleurs au sein des pays de l’Union européenne (UE), <strong>le visa et le permis de travail ne sont plus nécessaires pour travailler en Espagne</strong>. Les travailleurs ressortissants des pays membres de l’UE bénéficient des mêmes conditions de travail que les ressortissants espagnols.</p>
<p>L’obtention préalable du N.I.E facilitera la recherche d’emploi ou tout au moins permettra d’éviter certains écueils et une perte de temps certaine.</p>
<p><strong>Cas spécifique : les Français se rendant au Maroc via l’Espagne</strong></p>
<p>Les ressortissants français qui souhaitent se rendre au Maroc via l’Espagne doivent être en possession d’un passeport en cours de validité. Faute de pouvoir présenter ce document, les voyageurs sont refoulés à la frontière terrestre (Melilla ou Ceuta) ou réembarqués (à Tanger) à destination de l’Espagne.</p>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-documents-de-voyage.md" class="spip_in">Documents de voyage</a></li>
<li>Site du <a href="http://www.mir.es/" class="spip_out" rel="external">ministère de l’Intérieur espagnol</a></li>
<li>Section consulaire de l’<a href="http://www.exteriores.gob.es/Embajadas/PARIS/fr/Pages/inicio.aspx" class="spip_out" rel="external">ambassade d’Espagne à Paris</a></li></ul>
<p><i>Mise à jour : avril 2014</i></p>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/entree-et-sejour-23173/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
