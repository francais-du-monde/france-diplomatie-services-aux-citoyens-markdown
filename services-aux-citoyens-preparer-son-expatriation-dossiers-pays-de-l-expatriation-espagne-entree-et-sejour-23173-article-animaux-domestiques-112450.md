# Animaux domestiques

<p>Vous devrez prendre en compte la réglementation en vigueur, similaire à celle qui s’applique aux autres pays européens. Tous les animaux de compagnie doivent être accompagnés d’un certificat vétérinaire ou d’un passeport, selon leur espèce. Les propriétaires doivent pouvoir accréditer à travers un certificat écrit, rédigé en espagnol, que les animaux sont en leur possession depuis trois mois, ou depuis les premiers jours de leur naissance, si leur âge est inférieur. Dans ce certificat, ils doivent également s’engager à ne pas vendre les animaux, et à accepter les inspections que les services sanitaires espagnols jugent pertinentes de réaliser, conformément aux normes sanitaires en vigueur.</p>
<p>Pour pouvoir entrer en Espagne, votre animal (chien, chat, furet) devra respecter un certain nombre de mesures sanitaires et d’identification, susceptibles de varier selon votre pays d’origine. Dans la plupart des cas, votre animal de compagnie devra être vacciné contre la rage et identifié par une puce électronique ou un tatouage. Vous devez en outre savoir que s’il est âgé de moins de trois mois, il peut y avoir des restrictions à l’entrée en Espagne. Dans le cas où les animaux ne possèdent pas de certificat de vaccination antirabique ni de carnet sanitaire certifiant une telle vaccination, ils ne seront pas autorisés à entrer en Espagne.</p>
<p>En raison de la grippe aviaire, en Espagne, l’importation d’oiseaux et de leurs dérivés en provenance des pays concernés, qu’ils appartiennent ou non à l’UE, est interdite. En ce sens, le contrôle sur les oiseaux provenant d’Asie du Sud-est et l’inspection par les autorités sanitaires aux postes d’inspection frontaliers ont été renforcés. De même, l’entrée dans l’UE d’oiseaux ornementaux venant de tout pays tiers est interdite.</p>
<p>Si votre animal est une espèce en danger, vous ne pourrez pas emporter votre mascotte avec vous, dans la mesure où l’Espagne est signataire d’accords internationaux sur la protection des espèces en danger d’extinction.</p>
<p>Il peut être utile de disposer du numéro de téléphone des urgences vétérinaires de la localité que vous allez visiter avec votre animal de compagnie.</p>
<p>Mis à part le respect des formalités d’entrée, sachez que :</p>
<p>La muselière est obligatoire dans certaines communautés autonomes d’Espagne. L’animal doit être identifié par une micropuce ou un tatouage.</p>
<p>Pour voyager, vous devez le placer dans un conteneur pour animaux. Tous les lieux d’hébergement n’admettent pas les animaux.</p>
<p>La plupart des restaurants n’admettent pas les animaux domestiques.</p>
<p>Le nombre maximum d’animaux que l’on peut transporter sans qu’ils soient considérés comme un lot commercial varie en fonction de l’espèce.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Notre article <a href="services-aux-citoyens-preparer-son-expatriation-douanes-article-animaux-domestiques.md" class="spip_in">Animaux domestiques</a>.</p>
<p><i>Mise à jour : avril 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/entree-et-sejour-23173/article/animaux-domestiques-112450). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
