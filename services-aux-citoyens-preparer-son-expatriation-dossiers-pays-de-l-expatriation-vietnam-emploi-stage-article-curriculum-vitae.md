# Curriculum vitae

<p>Il n’y a pas de particularité propre au contexte vietnamien à dénoter en ce qui concerne la rédaction du CV. Il est recommandé d’aller à l’essentiel et de présenter son CV de manière à le rendre, clair, lisible et concis. Au Vietnam comme ailleurs, le recruteur qui recevra votre CV ne sera pas forcément aussi spécialisé que vous ; il est donc conseillé d’éviter le jargon professionnel et d’être cohérent dans le choix des informations à faire apparaître dans le CV.</p>
<p>A toutes fins utiles, pour toutes les petites et moyennes entreprises françaises et entrepreneurs individuels désireux de s’implanter au Vietnam, le Guide des affaires Vietnam publié par les bureaux Ubifrance au Vietnam présente les principales caractéristiques de l’économie du pays et propose des clés opérationnelles pour une approche pragmatique de ce marché : contexte économique et politique, secteurs porteurs, climat des affaires, réglementations à connaître, renseignements pratiques pour se rendre et s’implanter sur place.</p>
<p><i>Mise à jour : février 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/emploi-stage/article/curriculum-vitae). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
