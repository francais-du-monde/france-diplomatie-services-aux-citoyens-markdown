# Animaux domestiques

<p>Les chats et les chiens peuvent être importés d’un pays membre de l’UE ou de L’AELE sur présentation d’un <strong>passeport d’animal</strong> que vous pouvez obtenir auprès d’un vétérinaire agrée par l’administration centrale de l’inspection nationale de la santé animale. Le <strong>passeport d’animal</strong> remplace le certificat vétérinaire qui est divisé en deux parties :</p>
<ul class="spip">
<li>Partie "Certificat sanitaire"</li>
<li>Partie "Certificat de vaccination"</li></ul>
<p>Le certificat sanitaire doit avoir été <strong>rempli et signé par un vétérinaire autorisé</strong>, au plus tôt <strong>10 jours avant l’importation en Norvège</strong>. Le certificat sanitaire atteste que l’animal n’est pas suspect de pouvoir transmettre des maladies contagieuses et qu’il a été traité contre l’échinococcose. L’animal doit subir un nouveau traitement contre l’échinococcose, chez un vétérinaire, dans les sept jours suivant l’arrivée en Norvège.</p>
<p><strong>Vaccination contre la rage :</strong></p>
<p>Les chiens et chats doivent subir un test sérologique de titrage du taux d’anticorps antirabiques. Le titrage des anticorps doit être effectué au plus tôt 120 jours (et au plus tard 365 jours) après la dernière vaccination. La prise de sang doit être effectuée par un vétérinaire, analysée par un laboratoire agréé et indiquer un titre d’anticorps d’au moins 0,5 UI/ml.</p>
<p>Il est conseillé en outre de vacciner les chiens contre la maladie de Carré et la leptospirose, mais ce n’est pas une obligation.</p>
<p>Les chats et chiens doivent être identifiés par tatouage ou implant électronique. A l’arrivée au poste frontière, l’animal et son passeport ainsi que le certificat de vaccination, devront être présentés spontanément à la douane.</p>
<p><i>Source : <a href="http://www.visitnorway.fr/" class="spip_out" rel="external">Visitnorway.com</a></i></p>
<p><i>Pour en savoir plus : </i></p>
<ul class="spip">
<li><a href="http://www.mattilsynet.no/" class="spip_out" rel="external">Mattilsynet.no</a></li>
<li><a href="http://www.toll.no/templates_TAD/Article.aspx?id=176727epslanguage=en" class="spip_out" rel="external">Site des douanes norvégiennes</a></li>
<li><a href="http://www.regjeringen.no/en/dep/ud/Documents/veiledninger/2006/Diplomat-in-Norway/25.html?id=419553" class="spip_out" rel="external">Site du ministère des Affaires étrangères norvégien</a></li>
<li><a href="http://stavanger.accueil.free.fr/" class="spip_out" rel="external">Stavanger Accueil</a></li></ul>
<h4 class="spip">Information générale</h4>
<p>Certains pays réglementent l’entrée des animaux sur leur territoire (permis d’importation, quarantaine, interdiction). Prévoyez un délai d’au moins dix jours pour effectuer toutes les formalités, voire de plusieurs mois pour les pays exigeant une quarantaine.</p>
<p>Pour connaître les conditions exactes, vous devrez prendre contact :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  avec <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-etrangers-en-france/article/annuaire-des-representations-etrangeres-en-france" class="spip_in">l’ambassade en France</a> du pays de destination. Si, au cours de son transport de la France vers le pays de destination, l’animal doit transiter par un ou plusieurs pays, vous devrez également vous conformer à la réglementation du ou des pays de transit de l’animal. A noter que certains pays refusent le transit d’animaux.
</p>
<p>Des informations générales sur la réglementation de nombreux pays sont également disponibles sur le site Internet de l’<a href="http://www.iatatravelcentre.com/" class="spip_out" rel="external">Association internationale du transport aérien (AITA)</a>.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  le cas échéant, avec la ou les compagnies aériennes pour connaître les conditions de transport de l’animal (en soute ou en cabine, normes des cages, nourriture, etc.).
</p>
<p>Pour connaître les normes internationales de transport des animaux et des cages, vous pouvez consulter le site Internet de l’<a href="http://www.iata.org/" class="spip_out" rel="external">Association internationale du transport aérien (AITA)</a>. Si l’ambassade dispose d’une information particulière, celle-ci doit être communiquée à votre vétérinaire traitant, titulaire d’un mandat sanitaire, qui vous guidera pour la suite.</p>
<p>Dans le cas où l’ambassade ne disposerait pas d’informations sur la réglementation sanitaire de son pays, vous devrez respecter, par défaut, les conditions suivantes :</p>
<ul class="spip">
<li>l’animal doit être en règle vis-à-vis des exigences réglementaires françaises en matière de santé et de protection animales. Pour plus de renseignements sur la réglementation française, vous pouvez prendre contact avec votre vétérinaire traitant, titulaire d’un mandat sanitaire, ou la Direction départementale des services vétérinaires (DDSV) de votre département. Vous trouverez les coordonnées des DDSV sur le site Internet du <a href="http://agriculture.gouv.fr/" class="spip_out" rel="external">ministère de l’Agriculture</a>.</li>
<li>les documents suivants sont obligatoires pour l’animal et doivent être établis par le vétérinaire traitant, titulaire d’un mandat sanitaire :<br class="manualbr">- identification par micropuce ou tatouage ;<br class="manualbr">- certificat de vaccination contre la rage en cours de validité ;<br class="manualbr">- certificat international de bonne santé, établi par le vétérinaire traitant, titulaire du mandat sanitaire, dans la semaine précédant le départ de France.</li>
<li>Il est également conseillé de faire procéder à un titrage des anticorps anti-rabiques dans un laboratoire agréé et de se munir du carnet de vaccination tenu à jour de l’animal. Vous devrez ensuite prendre rendez-vous avec le service " santé et protection animales " de la Direction départementale des services vétérinaires (DDSV) dont relève le vétérinaire traitant, pour la validation des documents établis par celui-ci.</li></ul>
<p><strong>Légalisation des documents</strong></p>
<p>Certains pays exigent que les documents validés par la DDSV soient ensuite légalisés ou munis de l’apostille. Il convient donc de se renseigner sur ce point auprès de l’<a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-etrangers-en-france/article/annuaire-des-representations-etrangeres-en-france" class="spip_in">ambassade du pays de destination</a>.</p>
<p>Pour connaître le régime de légalisation du pays de destination, vous pouvez également consulter le site Internet du <a href="services-aux-citoyens-legalisation-et-notariat-legalisation-et-certification-de-signatures.md" class="spip_in">ministère des Affaires étrangères</a>.</p>
<p>L’apostille s’obtient auprès des cours d’appels. Vous pouvez trouver leurs coordonnées sur le site Internet du <a href="http://www.justice.gouv.fr/" class="spip_out" rel="external">ministère de la Justice</a>.</p>
<p>La légalisation est effectuée par le bureau des légalisations du ministère des affaires étrangères. Pour toute information sur les légalisations, vous pouvez consulter le site Internet du <a href="services-aux-citoyens-legalisation-et-notariat-legalisation-et-certification-de-signatures.md" class="spip_in">ministère des Affaires étrangères</a> ou contacter :</p>
<p><strong>Le bureau des légalisations</strong><br class="manualbr">57 boulevard des Invalides <br class="manualbr">75007 Paris<br class="manualbr">Téléphone (de 14 à 16 heures) : 01 53 69 38 28 / 01 53 69 38 29 <br class="manualbr">Télécopie : 01 53 69 38 31</p>
<p>Pour toute information complémentaire, vous pouvez consulter le site Internet du <a href="http://agriculture.gouv.fr/" class="spip_out" rel="external">ministère de l’Agriculture et de la Pêche</a>.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Notre article thématique <a href="services-aux-citoyens-preparer-son-expatriation-douanes-article-animaux-domestiques.md" class="spip_in">Animaux domestiques</a>.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/norvege/entree-et-sejour/article/animaux-domestiques). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
