# Protection sociale

<h2 class="rub23175">Régime local de sécurité sociale</h2>
<p>Vous trouverez une présentation détaillée du système de sécurité sociale espagnol sur le site de notre partenaire, le <a href="http://www.cleiss.fr/" class="spip_out" rel="external">CLEISS</a> (Centre des liaisons européennes et internationales de sécurité sociale). En voici la table des matières :</p>
<p><strong>Le régime des salariés</strong></p>
<ul class="spip">
<li><a href="http://www.cleiss.fr/docs/regimes/regime_espagne_salaries.html#introduction" class="spip_out" rel="external">Introduction</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_espagne_salaries.html#organisation" class="spip_out" rel="external">Organisation</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_espagne_salaries.html#financement" class="spip_out" rel="external">Financement</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_espagne_salaries.html#maladie" class="spip_out" rel="external">Maladie - maternité</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_espagne_salaries.html#incapacite" class="spip_out" rel="external">Incapacité permanente</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_espagne_salaries.html#vieillesse" class="spip_out" rel="external">Vieillesse</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_espagne_salaries.html#deces" class="spip_out" rel="external">Décès et survivants</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_espagne_salaries.html#adtmp" class="spip_out" rel="external">Assurance accidents du travail et maladies professionnelles</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_espagne_salaries.html#chomage" class="spip_out" rel="external">Chômage</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_espagne_salaries.html#pf" class="spip_out" rel="external">Prestations familiales</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_espagne_salaries.html#garantie" class="spip_out" rel="external">Garantie de ressources</a></li></ul>
<p><strong>Le régime des non-salariés</strong></p>
<ul class="spip">
<li><a href="http://www.cleiss.fr/docs/regimes/regime_espagne_nonsalaries.html#introduction" class="spip_out" rel="external">Introduction</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_espagne_nonsalaries.html#organisation" class="spip_out" rel="external">Organisation administrative</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_espagne_nonsalaries.html#financement" class="spip_out" rel="external">Financement</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_espagne_nonsalaries.html#maladie" class="spip_out" rel="external">Maladie – Maternité</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_espagne_nonsalaries.html#maternite" class="spip_out" rel="external">Maternité, paternité</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_espagne_nonsalaries.html#incapacite" class="spip_out" rel="external">Incapacité permanente</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_espagne_nonsalaries.html#vieillesse" class="spip_out" rel="external">Vieillesse</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_espagne_nonsalaries.html#deces" class="spip_out" rel="external">Décès (survivants)</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_espagne_nonsalaries.html#adtmp" class="spip_out" rel="external">Accidents du travail et maladies professionnelles</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_espagne_nonsalaries.html#chomage" class="spip_out" rel="external">Chômage</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_espagne_nonsalaries.html#pf" class="spip_out" rel="external">Prestations familiales</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_espagne_nonsalaries.html#garantiederessouces" class="spip_out" rel="external">Garantie de ressources</a></li></ul>
<p><i>Mise à jour : avril 2014</i></p>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/protection-sociale-23175/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
