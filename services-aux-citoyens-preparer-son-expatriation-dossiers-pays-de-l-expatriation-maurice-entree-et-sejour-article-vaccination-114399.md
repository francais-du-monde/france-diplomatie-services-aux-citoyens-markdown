# Vaccination

<p>Le vaccin contre la fièvre jaune n’est obligatoire que pour les personnes en provenance d’une zone infectée.</p>
<p><strong>Adultes : </strong>mise à jour des vaccinations contre la diphtérie, le tétanos et la poliomyélite ; vaccinations contre la typhoïde (longs séjours), l’hépatite A (à partir de 50 ans, une recherche préalable d’anticorps sériques totaux est justifiée), l’hépatite B (longs séjours et/ou séjours à risques).</p>
<p><strong>Enfants : </strong>vaccinations recommandées en France par le ministère de la Santé et en particulier B.C.G. et hépatite B dès le premier mois (longs séjours), rougeole dès l’âge de neuf mois, méningite à méningocoque A et C dès l’âge de trois mois, hépatite A possible à partir d’un an, typhoïde à partir de cinq ans (longs séjours).</p>
<p>Tous les vaccins commercialisés en France sont, en principe, disponibles sur place. Cependant, il est recommandé de mettre à jour ses vaccinations avant le départ en raison d’éventuelles difficultés d’approvisionnement.</p>
<p>Pour en savoir plus :</p>
<ul class="spip">
<li>notre rubrique <a href="services-aux-citoyens-preparer-son-expatriation-sante.md" class="spip_in">Santé</a></li>
<li>Santé à <a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/maurice/" class="spip_in">Maurice</a> sur le site Conseils aux voyageurs.</li></ul>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/entree-et-sejour/article/vaccination-114399). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
