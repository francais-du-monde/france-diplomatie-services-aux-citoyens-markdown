# Logement

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/vie-pratique/article/logement-108402#sommaire_1">Où se loger ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/vie-pratique/article/logement-108402#sommaire_2">Conditions de location</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/vie-pratique/article/logement-108402#sommaire_3">Hôtels</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/vie-pratique/article/logement-108402#sommaire_4">Electricité</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/vie-pratique/article/logement-108402#sommaire_5">Electroménager</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Où se loger ?</h3>
<p>On trouve assez facilement à se loger à Mexico, soit en appartements, soit en maisons individuelles dans les quartiers résidentiels :</p>
<ul class="spip">
<li>à l’ouest de la ville comme Polanco, Irrigación ou Lomas de Chapultepec qui sont peu éloignés du lycée français, ou Lomas Hipódromo ou plus excentrés : La Herradura, Tecamachalco, Interlomas, Santa Fé ;</li>
<li>au centre-sud comme Roma, La Condesa ou Del Valle ;</li>
<li>au sud comme San Angel, Coyoacan, Florida plus près de l’annexe du lycée français de Coyoacan.</li></ul>
<p>Il est possible de s’adresser à des <a href="http://guiamexico.com.mx/agencias-inmobiliarias/empresas-guia.html" class="spip_out" rel="external">agences immobilières</a> ou consulter les <strong>annonces des principaux quotidiens</strong> : <br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> <a href="http://www.aviso-oportuno.com.mx/inmuebles/" class="spip_out" rel="external">El Universal</a><br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> <a href="http://www.tuaviso.com.mx/exonline" class="spip_out" rel="external">El Excelsior</a><br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> <a href="http://www.reforma.com/" class="spip_out" rel="external">Reforma</a><br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> <a href="http://www.segundamano.com.mx/" class="spip_out" rel="external">des revues de petites annonces</a>. <br class="manualbr">Un délai de recherche de deux ou quatre semaines est nécessaire. Il est possible de trouver des appartements meublés. Les studios sont très peu courants.</p>
<p>Il est recommandé :</p>
<ul class="spip">
<li>d’opter pour un quartier sûr et bien desservi ;</li>
<li>de prendre un appartement avec parking si vous avez une voiture ;</li>
<li>de choisir un immeuble avec gardien et code d’entrée (coût d’un gardien 24h/24 : entre 2.000 et 3.000 pesos/mois) ;</li>
<li>d’équiper, si cela n’est pas le cas, la porte de l’appartement d’un judas et d’une chaîne ou d’un verrou de sécurité ;</li>
<li>si vous louez une maison de vérifier qu’elle offre toutes les garanties de sécurité.</li></ul>
<p>Il est fréquent qu’il y ait des gardiens armés dans les quartiers d’habitation où logent les expatriés ainsi que dans les rues des quartiers résidentiels. Les "cerradas" ou quartiers fermés offrent une garantie supplémentaire.</p>
<p>Le fonctionnement de ces services est correct. Toutefois, il faut vérifier avant de louer un logement si les installations sont en bon état.</p>
<h3 class="spip"><a id="sommaire_2"></a>Conditions de location</h3>
<p>Les loyers sont fréquemment libellés en dollars. La caution représente un à deux mois de loyer. La durée du bail est en général d’un an, non révocable et le contrat est renégocié tous les ans. Un fiador (garant) est généralement exigé pour tout demandeur d’appartements ou de villas en location.</p>
<p>En cas de location par une agence, une commission représentant un mois de loyer est demandée (bien se faire préciser si cette commission est à la charge du propriétaire ou du locataire).</p>
<p><strong>Exemples de coût de logement à Mexico City (loyers mensuels)</strong></p>
<p>Le prix du mètre carré à la location est, dans les quartiers proches de l’ambassade (ouest du centre ville) de 15 à 20 €.</p>
<p><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> Studio/deux piéces : de 800 € à 1.000 €<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> 3 pièces : de 1.500 € à 2.000 € <br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> 5 pièces : de 2.500€ à 4.000 €<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> Villa : de 3.000€ à 5.000 €</p>
<p>De fortes disparités de prix existent même dans un même quartier comme Polanco.</p>
<h3 class="spip"><a id="sommaire_3"></a>Hôtels</h3>
<p><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> <a href="http://www.bedandbreakfast.com/mexico.html" class="spip_out" rel="external">Bed Breakfast</a> ;<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> Autre formule d’hébergement bon marché et sympathique, la <strong>casa de huéspedes</strong> est une forme d’hébergement chez l’habitant, qui vous coûtera de 125 à 300 pesos pour une chambre double (renseignements auprès de l’office du Tourisme) ou les <strong>posadas</strong> (pensions ou petites auberges).<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> Auberges de jeunesse</p>
<ul class="spip">
<li><a href="http://www.fuaj.fr/" class="spip_out" rel="external">Fédération unie des auberges de jeunesse</a></li>
<li><a href="http://www.hihostels.com/dba/country-MX.es.htm" class="spip_out" rel="external">Auberges de jeunesse à Mexico</a></li></ul>
<p><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> Hôtels</p>
<ul class="spip">
<li><a href="http://www.turista.com.mx/hoteles-mexico%2Bdistrito%2Bfederal-20.html" class="spip_out" rel="external">Guide des hôtels</a> du District Fédéral</li>
<li><a href="http://www.hotelesenmexico.com.mx/" class="spip_out" rel="external">Association des hôtels de Mexico</a></li>
<li><a href="http://www.mexicocity.gob.mx/contenido.php?cat=10100&amp;sub=0" class="spip_out" rel="external">Rubrique « Hébergement »</a> sur la page de l’office de tourisme de la ville de Mexico.</li></ul>
<table class="spip" summary="">
<caption>Prix d’une chambre d’hôtel à Mexico</caption>
<thead><tr class="row_first"><th id="idd9b5_c0">Prix moyen d’une chambre d’hôtel
(chambre double)</th><th id="idd9b5_c1">Pesos</th><th id="idd9b5_c2">Euros</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="idd9b5_c0">Grand tourisme</td>
<td headers="idd9b5_c1">2.000 à 3.000</td>
<td headers="idd9b5_c2">120 à 180</td></tr>
<tr class="row_even even">
<td headers="idd9b5_c0">Moyen tourisme</td>
<td headers="idd9b5_c1">850 à 1.500</td>
<td headers="idd9b5_c2">50 à 90</td></tr>
</tbody>
</table>
<p>Il est à noter que peu d’hôtels acceptent les animaux domestiques.</p>
<h3 class="spip"><a id="sommaire_4"></a>Electricité</h3>
<p>Le courant électrique est monophasé (110 volts 60 Hertz). Il est parfois possible de demander, pour les maisons particulières, l’installation du courant en 220 volts. Les prises de courant sont à deux broches de type USA. Par conséquent, les appareils français de 220 V ne fonctionnent pas, un transmetteur est nécessaire.</p>
<h3 class="spip"><a id="sommaire_5"></a>Electroménager</h3>
<p>Les cuisines sont équipées la plupart du temps uniquement d’une cuisinière à gaz. L’équipement ménager, au standard USA, est disponible sur place.</p>
<p>On utilise rarement la climatisation à Mexico. La plupart des logements ne sont pas chauffés et un chauffage d’appoint est nécessaire à certains moments de la journée pendant deux à trois mois par an.</p>
<p><i>Mise à jour : septembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/vie-pratique/article/logement-108402). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
