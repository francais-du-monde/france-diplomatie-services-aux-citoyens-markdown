# Emploi, stage

<h2 class="rub23041">Marché du travail</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/emploi-stage/#sommaire_1">Secteurs à fort potentiel</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/emploi-stage/#sommaire_2">Rémunération</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Secteurs à fort potentiel</h3>
<p>Les secteurs des services, des hautes technologies, de la logistique et des transports, sont créateurs d’emplois mais les entreprises de ces secteurs recourent de plus en plus à une main d’œuvre tchèque. Les tendances 2009-2010 étant que les entreprises recrutent des nouveaux employés pour des postes déjà existants, le nombre de postes nouvellement créés reste limité.</p>
<p>Les recruteurs se trouvent plus dans le secteur de l´industrie, par contre les secteurs de l’hôtellerie, restauration, BTP, commerce de détail et secteur agricole n’ont pratiquement pas de projets de recrutement.</p>
<p>Une tendance continue de recrutement est enregistrée dans le secteur du commerce (les fonctions commerciales). Les autres secteurs où la situation reste positive sont les domaines de la mécanique, de l´administration, de la production, du secteur bancaire et des finances.</p>
<h3 class="spip"><a id="sommaire_2"></a>Rémunération</h3>
<h4 class="spip">Salaire minimum</h4>
<p>Le montant du salaire minimum est régi par le code du travail et peut être révisé tous les ans en fonction de l’évolution de la situation économique du pays. Le salaire minimum est fixé aussi bien pour le taux horaire que pour le taux mensuel. Depuis 1er août 2013, le salaire minimum légal s’élève à 8500 CZK (320 euros environ) par mois, soit <strong>50,60<strong> CZK par heure.</strong> </strong></p>
<h4 class="spip">Salaire brut et salaire net</h4>
<p>Chaque salarié doit déduire de son salaire brut les charges. Ces charges (ou retenues) correspondent, d’une part, à l’assurance maladie (4,5% du salaire brut) et aux assurances sociales (6,5% du salaire brut) et, d’autre part, à l’acompte sur l’impôt sur le revenu calculé en fin d’année (15% du salaire « super-brut »). Après déduction de ces charges, la somme restante correspond au salaire net.</p>
<p><i>Sources : <a href="http://ec.europa.eu/eures/" class="spip_out" rel="external">EURES</a> rubrique " Vivre et travailler ", <a href="http://epp.eurostat.ec.europa.eu/" class="spip_out" rel="external">EUROSTAT</a></i></p>
<p><i>Mise à jour : janvier 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-tcheque-emploi-stage-article-stages.md" title="Stages">Stages</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-tcheque-emploi-stage-article-lettre-de-motivation.md" title="Lettre de motivation">Lettre de motivation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-tcheque-emploi-stage-article-curriculum-vitae.md" title="Curriculum vitae">Curriculum vitae</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-tcheque-emploi-stage-article-recherche-d-emploi-111337.md" title="Recherche d’emploi">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-tcheque-emploi-stage-article-reglementation-du-travail.md" title="Réglementation du travail">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-tcheque-emploi-stage-article-marche-du-travail-111335.md" title="Marché du travail">Marché du travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/emploi-stage/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
