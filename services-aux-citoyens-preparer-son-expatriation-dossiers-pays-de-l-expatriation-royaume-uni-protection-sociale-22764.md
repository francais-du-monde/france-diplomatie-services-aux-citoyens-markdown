# Protection sociale

<h2 class="rub22764">Régime local de sécurité sociale</h2>
<p>Vous trouverez une présentation détaillée du système de sécurité sociale britannique sur le site de notre partenaire, le <a href="http://www.cleiss.fr/" class="spip_out" rel="external">CLEISS</a> (Centre des Liaisons Européennes et Internationales de Sécurité Sociale). En voici la table des matières :</p>
<p><a href="http://www.cleiss.fr/docs/regimes/regime_royaumeuni_s.html" class="spip_out" rel="external">Le régime des salariés</a> :</p>
<ul class="spip">
<li><a href="http://www.cleiss.fr/docs/regimes/regime_royaumeuni_s.html#a" class="spip_out" rel="external">Généralités</a> (Organisation, financement, taux de cotisation sur les salaires…)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_royaumeuni_s.html#b" class="spip_out" rel="external">Assurance maladie maternité</a> (Prestations en nature et en espèces)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_royaumeuni_s.html#c" class="spip_out" rel="external">Accidents du travail et maladies professionnelles</a> (Régime indemnitaire, y compris pour les survivants)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_royaumeuni_s.html#d" class="spip_out" rel="external">Vieillesse invalidité survivants</a> (Pensions de vieillesse, d’invalidité, de survivant, aide sociale)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_royaumeuni_s.html#e" class="spip_out" rel="external">Chômage</a> (Indemnités et assistance chômage)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_royaumeuni_s.html#f" class="spip_out" rel="external">Prestations familiales</a> (Allocations familiales, de naissance, d’adoption…)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_royaumeuni_s.html#g" class="spip_out" rel="external">Prestations accordées aux personnes disposant de faibles ressources</a></li></ul>
<p><a href="http://www.cleiss.fr/docs/regimes/regime_royaumeuni_ns.html" class="spip_out" rel="external">Le régime des non-salariés</a> :</p>
<ul class="spip">
<li><a href="http://www.cleiss.fr/docs/regimes/regime_royaumeuni_ns.html#generalites" class="spip_out" rel="external">Généralités</a> (Organisation, financement, taux de cotisation sur les salaires…)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_royaumeuni_ns.html#maladie-maternite" class="spip_out" rel="external">Assurance maladie maternité</a> (Prestations en nature et en espèces)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_royaumeuni_ns.html#vis" class="spip_out" rel="external">Vieillesse invalidité survivants</a> (Pensions de vieillesse, d’invalidité, de survivant, aide sociale)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_royaumeuni_ns.html#chomage" class="spip_out" rel="external">Chômage</a> (Indemnités et assistance chômage)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_royaumeuni_ns.html#pf" class="spip_out" rel="external">Prestations familiales</a> (Allocations familiales, de naissance, d’adoption…)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_royaumeuni_ns.html#faibles-ressources" class="spip_out" rel="external">Prestations accordées aux personnes disposant de faibles ressources</a></li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-royaume-uni-protection-sociale-22764-article-convention-de-securite-sociale.md" title="Convention de sécurité sociale">Convention de sécurité sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-royaume-uni-protection-sociale-22764-article-regime-local-de-securite-sociale-109701.md" title="Régime local de sécurité sociale">Régime local de sécurité sociale</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/protection-sociale-22764/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
