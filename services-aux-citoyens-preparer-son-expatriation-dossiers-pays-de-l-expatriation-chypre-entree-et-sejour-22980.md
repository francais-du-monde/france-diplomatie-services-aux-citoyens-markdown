# Entrée et séjour

<h2 class="rub22980">Passeport, visa, permis de travail</h2>
<p>Quel que soit le motif de votre séjour, renseignez-vous auprès de la section consulaire de l’Ambassade de Chypre à Paris, qui vous informera sur la règlementation en matière d’entrée et de séjour à Chypre, règlementation que vous devrez impérativement respecter. N’hésitez pas à consulter le site du ministère des Affaires étrangères de la République de Chypre afin de vous familiariser avec les modalités d’entrée et de séjour dans le pays.</p>
<p><strong>En aucun cas, vous n’êtes autorisé à travailler à Chypre sans permis adéquat. </strong></p>
<p>La section consulaire de l’Ambassade de France à Chypre n’est pas l’organisme compétent pour répondre à vos demandes d’information concernant votre séjour à Chypre.</p>
<h4 class="spip">Entrée </h4>
<p>Du fait de la partition de l’île, la République de Chypre considère tous les points d’entrée au Nord comme illégaux. Même si les contrôles se sont assouplis depuis 2004 (Chypre est entrée dans l’Union européenne le 1er mai 2004), il demeure formellement conseillé à nos compatriotes d’utiliser les points d’entrée officiels : aéroports internationaux de Larnaca et Paphos, ou ports de Larnaca, Limassol et Paphos.</p>
<p>Les ressortissants français désirant visiter la partie nord sont autorisés à franchir la « ligne verte » après octroi aux « points de passage » (il y en a trois à Nicosie) d’un « visa » chypriote-turc, sur présentation de leur carte d’identité ou leur passeport.</p>
<h4 class="spip">Séjour</h4>
<p>Pour des <strong>séjours inférieurs à trois mois</strong>, une carte d’identité ou un passeport en cours de validité doit être présenté, sans formalité de visa (pour les ressortissants des pays membres de l’Union européenne).</p>
<p>Pour un <strong>séjour de plus de trois mois</strong>, il faut contacter le Bureau de l’immigration au ministère de l’Intérieur qui donnera une <i>Alien Card</i> (sur présentation de la CNI pour les Français et d’une preuve de résidence).</p>
<p>Si la personne souhaite travailler, il lui faudra présenter, en plus de l’<i>Alien Card</i>, un formulaire jaune <i>yellow slip</i>, délivré également par le ministère de l’Intérieur, qui tiendra lieu de <strong>permis de travail</strong> (formulaire à remplir dans les quatre mois à compter de la date de son entrée à Chypre).</p>
<p><strong>Contact : </strong></p>
<p><a href="http://moi.gov.cy/" class="spip_out" rel="external">Ministère de l’Intérieur (Ministry of Interior)</a><br class="manualbr"><strong>Direction des étrangers et de l’immigration (Department of Aliens and Immigration)</strong><br class="manualbr">1457 Nicosie <br class="manualbr">Tél. (357) 22 80 45 02 <br class="manualbr">Fax (357) 22 80 45 87 <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/entree-et-sejour-22980/#migration#mc#crmd.moi.gov.cy#" title="migration..åt..crmd.moi.gov.cy" onclick="location.href=mc_lancerlien('migration','crmd.moi.gov.cy'); return false;" class="spip_mail">Courriel</a></p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li>Notre rubrique <a href="services-aux-citoyens-preparer-son-expatriation-documents-de-voyage.md" class="spip_in">Documents de voyage</a></li>
<li><a href="http://www.cyprus.gov.cy/" class="spip_out" rel="external">Site du gouvernement chypriote</a></li>
<li><a href="http://www.mfa.gov.cy/mfa/embassies/Embassy_Paris.nsf/index_fr/index_fr?OpenDocument" class="spip_out" rel="external">Section consulaire de l’ambassade de Chypre à Paris</a></li>
<li><a href="http://www.mfa.gov.cy/" class="spip_out" rel="external">Ministère des Affaires étrangères de la République de Chypre</a></li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chypre-entree-et-sejour-22980-article-animaux-domestiques-111066.md" title="Animaux domestiques">Animaux domestiques</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chypre-entree-et-sejour-22980-article-vaccination-111065.md" title="Vaccination">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chypre-entree-et-sejour-22980-article-demenagement-111064.md" title="Déménagement">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chypre-entree-et-sejour-22980-article-passeport-visa-permis-de-travail-111063.md" title="Passeport, visa, permis de travail">Passeport, visa, permis de travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/entree-et-sejour-22980/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
