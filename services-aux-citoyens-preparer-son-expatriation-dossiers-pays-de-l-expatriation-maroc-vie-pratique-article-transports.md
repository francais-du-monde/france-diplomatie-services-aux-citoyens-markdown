# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/vie-pratique/article/transports#sommaire_1">Importation de véhicule</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/vie-pratique/article/transports#sommaire_2">Permis de conduire</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/vie-pratique/article/transports#sommaire_3">Code de la route</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/vie-pratique/article/transports#sommaire_4">Assurances et taxes</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/vie-pratique/article/transports#sommaire_5">Achat et location</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/vie-pratique/article/transports#sommaire_6">Immatriculation</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/vie-pratique/article/transports#sommaire_7">Entretien</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/vie-pratique/article/transports#sommaire_8">Réseau routier</a></li>
<li><a id="so_9" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/vie-pratique/article/transports#sommaire_9">Transports en commun</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Importation de véhicule</h3>
<p>Il n’est pas conseillé d’importer son véhicule personnel du fait des droits de douane et d’importation pouvant atteindre 60% de la valeur du véhicule.</p>
<h3 class="spip"><a id="sommaire_2"></a>Permis de conduire</h3>
<p>Le nouveau Code de la route, entré en vigueur le 1er octobre 2010, instaure un permis à point (20 points) et une mise à niveau du contrôle routier.</p>
<p>Considérant la reconnaissance réciproque des permis de conduire entre la France et le Maroc, les résidents français au Maroc doivent échanger leur permis de conduire français en permis de conduire marocain.</p>
<p>S’ils ne le font pas, ils sont dès lors <strong>réputés conduire sans permis</strong> et passibles d’une amende, de l’immobilisation de leur véhicule et de la condamnation à la privation de la délivrance du permis de conduire pour une durée de trois mois. En outre, cette situation juridique n’est pas exclusive <strong>du refus prévisible d’indemnisation par l’assureur du conducteur incriminé en cas de sinistre.</strong></p>
<p>Par dérogation, une possibilité est donnée aux ressortissants de nationalité étrangère de conduire avec un permis étranger en cours de validité <strong>mais pour une durée maximum d’un an</strong> à compter de leur séjour temporaire au Maroc.</p>
<p><strong>Pièces à fournir pour l’échange d’un permis étranger :</strong></p>
<ul class="spip">
<li>Un imprimé spécial dit « formule PII », dûment renseigné et signé par le demandeur ;</li>
<li>Un justificatif de l’identité du demandeur et du lieu de résidence ;</li>
<li>Un reçu de paiement des droits de timbre (300 dirhams) et de rémunération de service (100 dirhams) institués par la législation en vigueur ;</li>
<li>Un certificat médical établi depuis moins de trois mois par un médecin agréé attestant l’aptitude physique et mentale du demandeur pour la catégorie de permis sollicitée ;</li>
<li>Deux photographies d’identité récentes de face, en couleur, de format 35X45 mm, sur fond blanc ;</li>
<li>L’original du permis de conduire étranger en cours de validité accompagné de la traduction en langues arabe ou française, si ce permis est rédigé dans une autre langue que ces deux langues.</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Code de la route</h3>
<p>Le code de la route est très peu respecté au Maroc et les accidents sont l’une des premières causes de mortalité. Il vous faudra être particulièrement prudent et attentif. Les feux rouges et les "stops" ne sont pas toujours respectés, surtout la nuit.</p>
<p>La conduite s’effectue à droite et la priorité est à droite. La vitesse est limitée à 40 km/h en ville, à 60 km/h ou 90 km/h sur route et à 120 km/h sur autoroute.</p>
<h3 class="spip"><a id="sommaire_4"></a>Assurances et taxes</h3>
<p>L’assurance au tiers non illimité est obligatoire. Il est préférable de prendre une assurance tous risques. Il est obligatoire de contracter une assurance auprès d’une compagnie locale, dès lors que l’on est domicilié au Maroc.</p>
<h3 class="spip"><a id="sommaire_5"></a>Achat et location</h3>
<h4 class="spip">Achat</h4>
<p>Les marques Renault, Peugeot, Citroën, Fiat, Mercedes, BMW, Toyota sont représentées au Maroc. Certains véhicules Peugeot, Renault et Fiat sont assemblés sur place. Il est conseillé d’opter pour un véhicule robuste qui sera facile à entretenir et à réparer. La climatisation est recommandée mais non indispensable. Pour les randonnées en montagne et dans le sud, un véhicule tout terrain est conseillé. Le véhicule doit être aux normes européennes, avec des phares jaunes.</p>
<p>On peut acheter un véhicule d’occasion mais les prix sont plus élevés qu’en France. L’importation d’un véhicule n’est pas conseillée aux expatriés car les droits de douane sont élevés.</p>
<h4 class="spip">Location</h4>
<p>La location ne pose pas de problèmes particuliers. Des sociétés spécialisées sont implantées au Maroc et disposent d’un parc de véhicules important.</p>
<h3 class="spip"><a id="sommaire_6"></a>Immatriculation</h3>
<p>Pour l’immatriculation d’un véhicule automobile anciennement immatriculé à l’étranger et dédouané au Maroc, le dossier de la demande d’immatriculation comprend :</p>
<ul class="spip">
<li><strong>Un imprimé spécial</strong>, suivant mode d’achat au comptant ou à crédit (formule grise n° I ou II) affranchi des droits d’enregistrements et de timbres et contenant :
<br>— La déclaration de l’acheteur du véhicule dûment renseignée et signée ;
<br>— Le contrat de vente ou de cession portant signatures légalisées du vendeur et de l’acheteur si l’immatriculation est demandée par une personne autre que celle mentionnée sur la carte grise étrangère ;
<br>— Un certificat de visite technique, délivré par un centre de visites techniques attestant que le véhicule est en bon état de marche et apte à circuler sans aucun danger ;</li>
<li><strong>Documents justificatifs de la résidence :</strong>
<br>— Pour les personnes physiques : une photocopie certifiée conforme à l’originale de la carte d’identité nationale en cours de validité et dont l’adresse relève de la juridiction du centre d’immatriculation ;
<br>— Pour les étrangers résidents au Maroc le dossier doit être complété par une copie certifiée conforme à l’originale de la carte de séjour en cours de validité ou du récépissé de dépôt de la carte de séjour portant la photo de l’intéressé et cacheté par la Sureté nationale ou la Gendarmerie Royale ;</li>
<li><strong>La carte grise étrangère</strong> ou à défaut une attestation d’immatriculation délivrée par le pays d’origine. Dans le cas d’un véhicule importé à l’état neuf par un particulier, ce dernier doit produire une facture d’achat certifiée par le garage vendeur faisant mention des caractéristiques du véhicule et la date de mise en circulation ;</li>
<li><i>Un contrat de vente à crédit </i>établi par l’organisme de financement, signé par le vendeur, l’acheteur et l’organisme de crédit, si le véhicule est acquis à crédit ;</li>
<li><strong>Une formule blanche d’immatriculation n° VII</strong> renseignée et signée par l’acheteur ;</li>
<li><strong>Le procès verbal de réception</strong> à titre isolé et le certificat d’identification, délivrés après réception du véhicule, par le centre d’immatriculation ;</li>
<li><strong>Le certificat de dédouanement</strong> portant le nom de l’acheteur du véhicule ;</li></ul>
<p>Le coût de la vignette varie de 350 à 3200 dirhams pour les véhicules essence et de 700 à 8000 dirhams pour les véhicules diesel. Un contrôle technique est obligatoire tous les ans pour les véhicules de plus de cinq ans.</p>
<h3 class="spip"><a id="sommaire_7"></a>Entretien</h3>
<p>Il est possible de faire réparer ou entretenir son véhicule à un coût moins élevé qu’en France : les pièces détachées sont plus chères, mais la main-d’œuvre meilleur-marché. La qualité du service rendu est irrégulière. Pour se procurer des pièces détachées de voiture de marque française, les délais sont variables, en fonction de la marque du véhicule.</p>
<h3 class="spip"><a id="sommaire_8"></a>Réseau routier</h3>
<p>Les routes nationales sont en bon état mais étroites et avec un trafic intense. Les routes nationales Casablanca-Marrakech et Casablanca-El Jadida sont réputées dangereuses. Toutefois, on peut dorénavant emprunter l’autoroute. En effet, il existe quatre tronçons d’autoroute où la circulation est fluide : Rabat-Fès, Larache-Rabat, Rabat-Tanger et, à un moindre degré, Rabat-Casablanca-El Jadida.</p>
<p>Les routes de montagnes ne sont pas toujours bien entretenues (région de Ouarzazate, Asni, Col du Tichka).</p>
<p><strong>Sécurité</strong></p>
<p>On peut se déplacer partout dans le pays, sans restriction à la circulation.</p>
<p>La vitesse est limitée à 120 km/h sur les autoroutes, 100km/h sur les nationales et 60km/h ou 40km/h dans les villes et villages (les radars sont très fréquents et l’amende est de 400 dirhams).</p>
<p>Il est vivement déconseillé de circuler la nuit et hors piste.</p>
<p>Dans certaines villes se côtoient voitures, bus, poids-lourds, piétons, vélos, mobylettes, véhicules attelés par des chevaux, des mulets, ou des ânes. La conduite est donc très particulière et il faut être toujours attentif.</p>
<p>Les infractions au code de la route sont nombreuses, il convient de rouler très prudemment.</p>
<p>Les stations-service sont nombreuses en ville mais ce n’est pas le cas en campagne. Dans les coins reculés, seul le gasoil est présent.</p>
<p><strong>En cas d’accident</strong></p>
<p>En cas d’accident grave, le conducteur risque le retrait du permis de conduire voire l’emprisonnement, même si sa responsabilité n’est pas établie.</p>
<h3 class="spip"><a id="sommaire_9"></a>Transports en commun</h3>
<p>Les liaisons intérieures sont assurées soit par <strong>avion </strong>(lignes sur Agadir, Marrakech, Fès, Tanger, Oujda, Ouarzazate, au départ de Casablanca et Rabat), soit par <strong>chemin de fer </strong>(il existe trois lignes principales : Tanger/Rabat, Casablanca/El Jadida/Marrakech et Kenitra/Oujda). Des navettes sont organisées toutes les demi-heures entre Rabat et Casablanca.</p>
<p>Le réseau d’<strong>autocars </strong>(CTM, SATAS, Supratours) est assez dense mais ceux-ci sont parfois vétustes et peu fiables.</p>
<p>En ville, les bus municipaux sont bondés et irréguliers. Il existe deux sortes de <strong>taxis </strong> :</p>
<ul class="spip">
<li>les "petits taxis", très peu onéreux, sont de petites berlines qui ne peuvent charger que 3 personnes et n’ont pas le droit de s’éloigner à plus de 10 km du centre des villes. Chaque ville a sa couleur de "petits taxis" ;</li>
<li>les "grands taxis" sont de grosses berlines (souvent des Mercedes) qui assurent les liaisons de ville à ville et peuvent prendre jusqu’à six personnes. Il est recommandé de fixer le prix de la course à l’avance.</li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/vie-pratique/article/transports). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
