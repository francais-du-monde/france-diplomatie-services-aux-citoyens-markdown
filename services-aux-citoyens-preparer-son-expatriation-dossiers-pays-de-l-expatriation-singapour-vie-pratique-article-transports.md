# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/vie-pratique/article/transports#sommaire_1">Importation de véhicule</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/vie-pratique/article/transports#sommaire_2">Permis de conduire</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/vie-pratique/article/transports#sommaire_3">Code de la route</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/vie-pratique/article/transports#sommaire_4">Assurances et taxes</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/vie-pratique/article/transports#sommaire_5">Achat et location</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/vie-pratique/article/transports#sommaire_6">Immatriculation</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/vie-pratique/article/transports#sommaire_7">Entretien</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/vie-pratique/article/transports#sommaire_8">Carburant</a></li>
<li><a id="so_9" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/vie-pratique/article/transports#sommaire_9">Réseau routier</a></li>
<li><a id="so_10" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/vie-pratique/article/transports#sommaire_10">Transports en commun</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Importation de véhicule</h3>
<p>L’importation de voitures est autorisée mais déconseillée en raison des difficultés pour satisfaire aux normes locales (volant à droite, normes anti-pollution très strictes, etc). Tout véhicule importé doit respecter les normes fixées par le <a href="http://www.lta.gov.sg/" class="spip_out" rel="external">Land Transport Authority (LTA)</a>.</p>
<p>Les autorités singapouriennes ont mis en place, au début des années 90, un système de régulation du marché automobile pour limiter les problèmes de circulation et de conserver la réputation d’une ville peu polluée. De plus en plus d’expatriés recourent d’ailleurs à la location longue durée.</p>
<p>Le véhicule d’occasion destiné à l’importation, outre l’obligation de respecter les règles du LTA, ne doit pas être âgé de plus de sept ans au moment de l’importation. Tout véhicule de plus de trois ans d’âge au moment de l’importation ne pourra être vendu sur le marché local et devra être détruit ou réexporté à l’issue du séjour ou dès que la limite d’âge de 10 ans sera atteinte.</p>
<h3 class="spip"><a id="sommaire_2"></a>Permis de conduire</h3>
<p>Tout conducteur dont le séjour <strong>n’excède pas 12 mois</strong> peut conduire avec un permis international, à obtenir en France uniquement, et son permis national valide, accompagné d’une traduction officielle en anglais (à obtenir auprès d’un traducteur agréé par l’Ambassade de France).</p>
<p>Cette traduction doit être légalisée par l’Ambassade de France pour être soumise auprès du ministère singapourien des transports.</p>
<p>Tout conducteur résidant <strong>plus de 12 mois (résident permanent)</strong> à Singapour doit posséder un permis de conduire singapourien. Dans ce cas, il doit convertir son permis de conduire français en permis de conduire singapourien et passer un examen théorique appelé <i>Basic Theory Test (BTT)</i>.</p>
<p><strong>L’inscription au <i>Basic Theory Test</i> </strong></p>
<p>Le coût de l’examen et de 12,90 SGD. Le candidat doit avoir au moins 18 ans. Il doit présenter son passeport et son permis de séjour à l’un des centres d’examen :</p>
<p><strong>Bukit batok Driving Centre Ltd</strong><br class="manualbr">815 Bukit Batok West Avenue 5<br class="manualbr">Singapore 659085<br class="manualbr">Tél. : 65611233 - Fax : 66656923</p>
<p><a href="http://www.cdc.com.sg/" class="spip_out" rel="external">ComfortDelGro Driving Centre Pte Ltd</a><br class="manualbr">205 Ubi Avenue 4 - Singapore 408805<br class="manualbr">Tél. : (65) 6841 8900 - Télécopie : (65) 6743 9946 / (65) 6841 7928 / (65) 6841 8913<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/vie-pratique/article/transports#info#mc#cdc.com.sg#" title="info..åt..cdc.com.sg" onclick="location.href=mc_lancerlien('info','cdc.com.sg'); return false;" class="spip_mail">Courriel</a></p>
<p><a href="http://www.ssdcl.com.sg/" class="spip_out" rel="external">Singapore Safety Driving Centre Ltd</a><br class="manualbr">3 Ang Mo Kio Street 62<br class="manualbr">Ang Mo Kio Industrial Park 3<br class="manualbr">Singapore 569139<br class="manualbr">Tél. : 64826060 - Fax : 64828808</p>
<p>Une convocation vous sera remise indiquant la date, l’heure et le centre d’examen où vous devrez vous présenter. Parfois le délai est d’un mois. Pour vous aider dans votre préparation, il est fortement recommandé de se procurer un exemplaire de la brochure <i>Basic Therory of Driving</i> auprès de son centre d’examen ou dans une librairie.</p>
<p>Après passage et réussite du <i>Basic Theory Test</i>, le conducteur doit, en personne, déposer une demande de conversion de son permis de conduire étranger au : <i>Traffic Police Headquarters.</i></p>
<p>Le permis de conduire singapourien est à renouveler au bout d’un an puis tous les un à trois ans. Les autorités vous adressent un formulaire de renouvellement. Il suffit alors de se présenter dans n’importe quel bureau de poste et de payer la somme de 20 SGD par an.</p>
<h4 class="spip">Le permis international</h4>
<p>A partir du permis singapourien, pour obtenir un permis international « International Driving License » nécessaire pour conduire dans les pays voisins, il vous faut contacter :</p>
<p><a href="http://www.aas.com.sg/" class="spip_out" rel="external">Automobile Association of Singapore</a><br class="manualbr">Tél. : 63338811 Fax : 67335094</p>
<h3 class="spip"><a id="sommaire_3"></a>Code de la route</h3>
<p>La conduite s’effectue à gauche, la priorité est à droite. La vitesse maximale autorisée est de 50/60 km/h en agglomération, 70 km/h sur route et 80-90 km/h sur autoroute. Les contrôles radars et caméras aux feux rouges sont fréquents.</p>
<p>Singapour est également un des rares pays à avoir osé remettre en cause le droit de chaque individu à posséder une voiture. L’administration a mis en place un certain nombre de règles curieuses mais efficaces. Les automobiles sont très chères à Singapour et tout le monde ne peut s’offrir ce luxe. Le gouvernement met en vente chaque mois un certain nombre de documents qui autorisent la possession d’un véhicule motorisé (l’équivalent de la carte grise). Pour les petites cylindrées, le prix peut dépasser 40 000 euros. De plus, les véhicules sont équipés d’une "Cash Card" à débit automatique pour circuler la journée en centre ville, dans une zone délimitée (ERP : Electronic Road Pricing). Le prix varie selon l’horaire de passage</p>
<p><strong>Quelques règles et conseils importants</strong></p>
<p>Il n’y a pas d’encombrement aux carrefours et pour cause : il en coûte trois points d’être responsable d’un embouteillage ! (Le permis comporte 24 points : un stop grillé coûte neuf points, un stationnement interdit trois points, une conduite en état d’ivresse 12 points…).</p>
<ul class="spip">
<li>Une action non expressément autorisée n’est pas permise, à l’inverse de la pratique française selon laquelle ce qui n’est pas interdit est autorisé (ex : les demi-tours ne peuvent être faits que lorsque le panneau l’indique).</li>
<li>Il est interdit de traverser les routes en dehors des passages piétons, les policiers pourraient vous verbaliser.</li>
<li>Le port de la ceinture de sécurité est obligatoire devant et derrière.</li>
<li>Vous devez rouler en code de 19h à 7h et lorsqu’il pleut.</li>
<li>Ne roulez pas dans les couloirs d’autobus aux heures interdites.</li>
<li>Bien que la conduite soit à gauche, la priorité est à droite.</li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Assurances et taxes</h3>
<h4 class="spip">Assurances</h4>
<p>Vous devez souscrire pour votre voiture ou moto une assurance tout risque ou au tiers pour un ou plusieurs conducteurs.</p>
<p>On assiste depuis peu à une très importante augmentation du coût des primes d’assurance (+20 à 30%). Par ailleurs les compagnies exigent la production des certificats de bonus avec une traduction en anglais pour répercuter (parfois partiellement) le bonus acquis à l’étranger et ce, dès le stade de l’établissement du devis. En revanche, certaines compagnies peuvent octroyer des rabais supplémentaires pour ancienneté d’un bonus maximal ou pour les conducteurs de plus de 40 ans. Les justificatifs traduits sont requis.</p>
<p>Chaque année, votre assureur doit réactualiser le coût de votre assurance en fonction de la dépréciation de votre voiture.</p>
<p>En cas d’accident matériel, relevez l’identité du conducteur, son numéro de permis de conduire, la plaque d’immatriculation et remplissez un constat à l’amiable « Non-Injury Motor Accident Report » (disponible auprès de la compagnie d’assurance ou au poste de police le plus proche). Envoyez-le dans les 24h à votre assureur. En cas de blessés, prévenir impérativement la police au 995.</p>
<p>Si votre voiture est en location ou en leasing, appelez votre compagnie de location. Dans tous les cas, l’assurance au tiers est obligatoire.</p>
<h4 class="spip">Vignette</h4>
<p>Le coût de la vignette auto et la taxe d’enregistrement à laquelle s’ajoute la mise aux enchères des cartes grises (valable 10 ans) rendent l’acquisition d’un véhicule environ quatre fois plus chère qu’en France.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.lta.gov.sg/content/ltaweb/en/roads-and-motoring/owning-a-vehicle/costs-of-owning-a-vehicle/tax-structure-for-cars.html" class="spip_out" rel="external">Land Transport Authority (LTA)</a>)</p>
<h3 class="spip"><a id="sommaire_5"></a>Achat et location</h3>
<h4 class="spip">Achat - occasion</h4>
<p>Le montant d’achat de votre voiture varie chaque mois en fonction du coût du COE (Certificate Of Entitlement). Le COE est appliqué à tous les véhicules avant leur mise en circulation. Cette taxe est valable pour 10 ans à partir de la date d’enregistrement de la voiture. Il faut ajouter le montant de la COE au prix d’un véhicule neuf ou ancien (après 10 ans). En ce qui concerne les voitures d’occasion, elle est généralement incluse dans le prix de vente.</p>
<p>La grande majorité des concessionnaires de marques internationales est concentrée sur Alexandra Road, Leng Kee Road et Sing Ming Road. L’achat d’un véhicule d’occasion est possible par annonces ou auprès de garages.</p>
<h4 class="spip">Location</h4>
<p>Toutes les grandes compagnies de location sont représentées (Avis, Hertz, etc). Les prix sont supérieurs à la France. Il existe également des contrats de location longue durée qui peuvent s’avérer intéressants.</p>
<h3 class="spip"><a id="sommaire_6"></a>Immatriculation</h3>
<p>Le concessionnaire se charge des démarches administratives pour que le véhicule soit enregistré au nom de son propriétaire, que le véhicule soit neuf ou d’occasion.</p>
<h3 class="spip"><a id="sommaire_7"></a>Entretien</h3>
<p>Peugeot, Citroën, Renault et de nombreuses marques étrangères sont représentées sur place. Il existe de bons garages et les délais sont rapides. Chez les concessionnaires européens, le coût des pièces et de la main d’oeuvre est très élevé. Toutefois il existe des petits garages indépendants présentant un meilleur rapport qualité-prix si le véhicule n’est plus sous garantie.</p>
<p>Un contrôle technique est effectué sur les véhicules tous les deux ans. Toutes les voitures singapouriennes possèdent l’air conditionné.</p>
<h3 class="spip"><a id="sommaire_8"></a>Carburant</h3>
<p>Il existe énormément de stations services à Singapour, appartenant à des compagnies internationales. Elles sont quasiment toutes ouvertes 24h/24h. Certaines pompes sont automatiques, avec paiement par carte bancaire. En journée, un employé s’occupe de votre plein et vous nettoie le pare brise gratuitement.</p>
<p>Le litre d’essence varie en fonction des fluctuations du marché. Il se situe en juin 2013 autour de 2,24 $ le litre.</p>
<h3 class="spip"><a id="sommaire_9"></a>Réseau routier</h3>
<p>L’état du réseau routier est excellent.</p>
<p>Singapour est une grande ville moderne mais contrairement aux autres mégalopoles, elle connaît peu les problèmes de trafic et d’embouteillage. Avenues propres, couloirs réservés aux bus, piétons prioritaires sur les passages pour piétons, taxis climatisés, quiconque venant d’une autre ville asiatique risque d’être étonné !</p>
<h3 class="spip"><a id="sommaire_10"></a>Transports en commun</h3>
<p>Des infrastructures de transports en commun modernes assurent un service rapide et efficace. Qu’il s’agisse du métro climatisé (MRT : Mass Rapid Transit), de l’autobus peu cher ou des taxis nombreux et abordables, tous sont des moyens de transport sûrs et rapides.</p>
<h4 class="spip">Bus</h4>
<p><strong>Fréquence des Bus</strong></p>
<p>Le service des bus est assuré de 6h à 23h30. Ils bénéficient de couloirs de circulation réservés, ce qui leur évite d’être bloqués dans les embouteillages et leur permet de respecter leurs horaires. En pratique, le même numéro de bus dessert un arrêt toutes les 8 à 10 minutes et souvent sur de petites distances, plusieurs bus font le même trajet donc l’arrêt est desservi toutes les deux minutes.</p>
<p><strong>Réseau des bus</strong></p>
<p>Les bus circulent sur toute l’île. Pour leur distribution et leur parcours, achetez le <i>Transit Link Guide</i>, le <i>Mighty Minds Singapore Bus Guide </i>and <i>Bus Stop Directory </i>ou le <i>Singapore Public Tranport Guide </i>en vente dans le métro (mis à jour tous les ans) qui vous permettront de connaître les horaires, arrêts et trajets de chaque bus.</p>
<p>Des panneaux indiquant la direction des bus sont consultables aux arrêts mais plusieurs arrêts peuvent porter le même nom. Faites donc bien attention à bien suivre le trajet parcouru.</p>
<p><strong>Le coût des bus</strong></p>
<p>La ville est bien desservie par les bus, peu chers, nombreux et rapides. La plupart sont climatisés, certains sont à impériale et diffusent des programmes TV. Le rapport qualité prix est très intéressant. Les bus offrent des trajets agréables et rapides facturés de 0,60 à 1,50 SGD.</p>
<p>Le prix du voyage est en fonction de la distance parcourue. Faire l’appoint en pièce avant de monter, si vous n’avez pas de carte de transport car le chauffeur ne rend pas la monnaie.</p>
<p>On peut se procurer dans les stations de métro, certains grands hôtels et les 7-Eleven (supérettes), les tickets <i>Singapore Explorer</i>. Ce billet est valable d’un à trois jours aux tarifs de 5 à 12 dollars et donne accès aux lignes desservant tous les endroits touristiques de la ville.</p>
<p>Ce ticket est valable uniquement sur le réseau de bus. Pour un séjour plus long ou une visite plus poussée de la ville, se procurer le <i>EZ-link Ticket </i>et le <i>Transitlink Guide</i>, très pratiques pour se déplacer. Cette carte coûte 15 dollars (elle est déjà chargée de sept dollars prêts à être utilisés et contient cinq dollars de caution qui vous seront rendus si vous ne vous servez plus de la carte). Elle se recharge d’un montant minimum de 10 SGD et se débite automatiquement à chaque passage en métro et bus, suivant la distance parcourue. Les tarifs sont plus avantageux avec cette carte qu’en prenant des tickets à l’unité. Cette carte ne s’utilise que pour une personne à la fois mais n’est pas personnelle.</p>
<p><strong>Les bus de nuit</strong></p>
<p>Des bus de nuit (NightRider – SMRT) sont spécialement affrétés entre 23h30 et 4h30 du matin les vendredi, samedi et jours fériés. Le trajet coûte 4,5 SGD ; les possesseurs des cartes EZ-link reçoivent un trajet gratuit.</p>
<h4 class="spip">Métro</h4>
<p>Le métro est composé de quatre lignes (Est-Ouest, Nord-Sud, Nord-Est, La Circle Line) et de 89 stations. Il dessert à l’Est l’aéroport de Changi. La Circle Line parcourt tout le pourtour du centre ville sur 33 kilomètres et comporte 29 stations.</p>
<p>Le métro est rapide, moderne, climatisé, propre et sécurisé. Une majeure partie de la population l’utilise pour se rendre en ville le soir ou au travail en journée. Passé 23h, les rames sont encore pleines de Singapouriens qui rentre chez eux. On ne se sent donc jamais isolé ou en position d’insécurité.</p>
<p>Le métro est extérieur dans la périphérie et sous terrain dans le centre ville.</p>
<p>Site des <a href="http://www.smrt.com.sg/" class="spip_out" rel="external">transports publics à Singapour</a>.</p>
<p>Le métro est gratuit en semaine (hors période de vacances locales) le matin, avant 7h45 pour les 16 stations suivantesen centre-ville : Bayfront, Bras Basah, Bugis, Chinatown, City Hall, Clarke Quay, Dhoby Ghaut, Esplanade, Lavender, Marina Bay, Orchard, Outram Park, Promenade, Raffles Place, Somerset and Tanjong Pagar.</p>
<h4 class="spip">Taxi</h4>
<p>Les taxis sont nombreux et leur usage est complètement démocratisé. Ceci est dû au faible nombre de véhicules privés sur l’île par rapport au nombre d’habitants ce qui induit un faible coût des transports.</p>
<p>Ils sont tous équipés de compteurs non trafiqués car scellés par le gouvernement. On peut, par ailleurs, y monter à quatre ou bien charger le coffre sans qu’il y ait à payer de supplément.</p>
<p>Les trajets entre l’aéroport et la ville sont majorés de 5 SGD. Des suppléments différents sont aussi appliqués aux heures de pointe le matin et le soir, la nuit et les jours fériés.</p>
<p>Il est possible de les arrêter en pleine ville en faisant un signe de la main, de les attendre aux <i>taxi stand</i>, arrêts de taxi, ou de se rendre devant un grand hôtel car plusieurs taxis attendent les clients.</p>
<p>Il est également possible de les réserver par téléphone. Un supplément sera facturé. L’attente varie généralement de 7 à 10 minutes, sauf les jours de pluie ou à certains horaires où il peut être extrêmement difficile d’en trouver de disponibles.</p>
<p>Voici les contacts des compagnies de taxi :</p>
<ul class="spip">
<li>Comfort CabLink : 6552 1111</li>
<li>Citicab : 6552 2222</li>
<li>Tibs Taxis : 6481 1211</li>
<li>Yellow-top Cab : 6552 1111</li>
<li>SMART : 6485 7777</li>
<li>Silver Cab : 6363 6888</li>
<li>Transcab : 6555 3333</li></ul>
<p>Les taxis ont un montant minimum qui varie entre 2.40 et 2.60 SGD, suivant les compagnies.</p>
<p>Les coûts des taxis sont peu élevés car leur utilisation et très répandue, mais leurs tarifs dépendent de beaucoup de conditions qu’il faut prendre en considération : Pour une réservation, du lundi au vendredi de 7h30 à 9h30 et de 17h à 23h, vous payerez une taxe de 4 dollars. Les autres jours de la semaine, incluant les samedi et dimanche, la taxe sera de 2.5 SGD.</p>
<p>A partir de 18h, les soirs de jours fériés et jusqu’à minuit le lendemain, une charge de 1 SGD est demandée.</p>
<p>Si vous prenez un taxi de Changi Airport, la taxe est de 3 SGD. Sauf les vendredi et samedi de 17h à minuit, la taxe augmente à 5 SGD. De Seletar Airport, elle est de 3 SGD et de Singapore Expo Centre, de 2 SGD.</p>
<p>Les heures où la circulation est la plus dense sont davantage taxées : 1 SGD supplémentaire de 7h30 à 9h30 et de 17h à 20h.</p>
<p>Enfin, la taxe de nuit entre 23h30 et 23h44 est 10% du prix du trajet, entre 23h45 et 23h49 de 20 %, de minuit à 00h59 de 35% et de 1h à 6 h de 50%.</p>
<h4 class="spip">Téléphérique</h4>
<p>La ville possède un téléphérique qui relie le Mont Faber, au sud de Singapour à l’île de Sentosa. Le téléphérique fonctionne tous les jours de 8h30 à 22h. Le prix est de 26 SGD pour les adultes et 25 SGD pour les enfants. Pour plus d’informations, consulter le site : <a href="http://www.singaporecablecar.com.sg/" class="spip_out" rel="external">http://www.singaporecablecar.com.sg/</a></p>
<h4 class="spip">Carte d’abonnement</h4>
<p>Il existe la carte <i>EZ-link </i>pour le bus et le métro. Ce n’est pas une carte d’abonnement mais elle permet d’avoir une réserve d’argent de minimum 10 SGD et de circuler à moindre coût sur tout le réseau de transports en commun de l’île. Cette carte coûte 15 dollars (elle est déjà chargée de 7 dollars prêts à être utilisés et contient 5 dollars de caution qui vous seront rendus si vous ne vous servez plus de la carte). Le coût des trajets est débité automatiquement en fonction de la longueur parcourue et est comptabilisé à la baisse grâce à ce système de carte. Cette carte ne s’utilise que pour une personne à la fois mais n’est pas personnelle.</p>
<h4 class="spip">Plan</h4>
<p>De petits plans de poche sont distribués à tous les guichets de métro. D’autres plus importants sont disponibles au <i>Singapore Tourism Board</i> (Office de Tourisme de Singapour) sur Orchard Road. Plusieurs éditions de guide et cartes sont en vente dans les librairies. Il n’y a pas de plan de quartier aux arrêts de bus mais les rames de métro sont équipées de plans du réseau de métro.</p>
<p>Le site Internet <a href="http://www.streetdirectory.com/" class="spip_out" rel="external">Streetdirectory.com</a> vous permet de rechercher une destination dans Singapour, de vous donner le plan exact du quartier, et si vous le désirez, le trajet vous y conduisant.</p>
<h4 class="spip">Transport en autocar</h4>
<p>Le rapport qualité-prix est intéressant et dépend de la classe de transport souhaitée. Tous les tarifs sont détaillés ci-dessous.</p>
<p>Vous trouverez des départs quotidiens pour toutes les destinations de Malaisie desservies de manière directe depuis Singapour.</p>
<p>La gare routière principale se situe au Golden Mile Complex, en face de Sultan Mosquée, dans le quartier de Bugis. Mais plusieurs autres départs existent en fonction des compagnies utilisées.</p>
<h4 class="spip">Réseau des liaisons intérieures et coûts</h4>
<p>Des bus directs partent de Singapour pour la Malaisie : Mersing (port d’embarquement pour les îles de Tioman et Rawa), Malacca, Kuala Lumpur. Pour les autres destinations comme la Thaïlande, des changements de bus sont prévus en Malaisie.</p>
<h4 class="spip">Transport maritime et ferroviaire</h4>
<h5 class="spip">Transport maritime</h5>
<p><strong>Qualité et prix</strong></p>
<p>Les transports en ferry pour accéder aux îles de Malaisie ou d’Indonésie les plus proches, sont rapides et modernes. C’est souvent le seul moyen d’accéder à ces îles et la majorité des clients sont des Singapouriens. Le service est donc proposé en fonction de cette clientèle, habituée au modernisme. Le rapport qualité-prix est très acceptable pour le service proposé.</p>
<p><strong>Liaisons avec la France</strong></p>
<p>Il n’existe pas de liaison maritime avec la France de Singapour, à part peut-être quelques bateaux de croisières (qui feraient escale à Singapour) et les porte-conteneurs.</p>
<p><strong>Compagnies maritimes locales et étrangères</strong></p>
<p>Le port principal d’embarquement des ferries se situe au <i>World Trade Centre</i>.</p>
<p>Le <i>Port of Singapore Authority</i> (au 2nd étage du World Trade Centre, 1 Maritime Square Singapore 099253, tél : +65 6273 2066) édite chaque jour un bulletin sur arrivées et départs des navires. Le trafic étant assez irrégulier, il est nécessaire de le consulter ou de les contacter au No : 63712803.</p>
<p>Pour en savoir plus :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.mpa.gov.sg/" class="spip_out" rel="external">The Maritime and Port Authority of Singapore</a></p>
<p>De nombreux ferries relient les îles de Batam et Bintan à Singapour, plusieurs fois par jour (trajet : 30-45 minutes).</p>
<p>Plusieurs compagnies proposent leurs services :</p>
<ul class="spip">
<li>Wavemaster Ferry Systems<br class="manualbr">Tel : 65468830</li>
<li><a href="http://www.penguin.com.sg/" class="spip_out" rel="external">Penguin Ferry Services</a></li>
<li><a href="http://www.brf.com.sg/" class="spip_out" rel="external">Bintan Resort Ferries</a></li></ul>
<p>Il est aussi possible de se rendre sur la côte orientale de Sumatra par bateau, en une journée. Pour cela, il faut d’abord prendre un bateau pour Batam, puis un autre bateau pour Batun (5h de trajet), puis un bus pour Pekanbaru (durée 4h). Les billets bateau + bus sont vendus par de nombreuses agences sur le port, à l’embarcadère de Batam.</p>
<h5 class="spip">Réseau maritime national</h5>
<p>Le détroit de Malacca reste une voie maritime très empruntée mais à visée surtout commerciale. Très peu de bateaux acceptent des passagers actuellement, mis à part, les bateaux de croisière de plus en plus nombreux dans cette région.</p>
<p>Les ferries directs (Penguin Fast Ferry) pour Pulau Tioman et Pulau Rawa, îles de Malaisie les plus proches de Singapour, ne fonctionnent pas actuellement, mais il est possible qu’ils reprennent du service.</p>
<p><strong>Coût</strong></p>
<p>Les ferries allant vers Bintan et Batam (îles les plus proches de Singapour) sont rapides et peu onéreux.</p>
<p>Pour Bintan, ils circulent du lundi au vendredi de 9h05 à 20h15 et les vendredi, samedi, dimanche et jours fériés de 8h05 à 20h15. Plusieurs compagnies répertoriées plus haut offrent ce service. En moyenne, les tarifs A/R sont autour de 45 dollars par adulte et 35 dollars par enfant. Pour Batam, les tarifs A/R varient autour de 28 dollars par adulte et 22 dollars par enfants.</p>
<p>Il existe des bateaux pour Jakarta et d’autres destinations en Indonésie et Malaisie mais le tarif est quasiment le même que pour l’avion et le transport long et pénible.</p>
<h5 class="spip">Transport ferroviaire</h5>
<p>La station de chemin de fer, <i>Singapore Railway Station</i>, située sur Keppel Road appartient (édifices et terrains) à la Malaisie. Quand vous arrivez sur les quais, vous êtes déjà en Malaisie. La gare est, paraît-il copiée sur celle d’Helsinki : peintures sur céramique évoquant la vie quotidienne malaise et toit voûté inspiré par le Bauhaus.</p>
<p><strong>Fréquence</strong></p>
<p>Il y a plusieurs départs quotidiens de trains pour la Malaisie.</p>
<p>Pour Bangkok, il y a un départ tous les jours tôt le matin : l’Express Rakyat, qui arrive à Butterworth 14h plus tard. Il faut coucher à Butterworth (ou à Penang, à côté) pour prendre l’Express International qui vous mènera jusqu’à Bangkok en 24h. Le départ se fait tous les jours en début d’après midi.</p>
<p>L’Orient Express propose trois départs par semaine.</p>
<p>Le réseau de chemin de fer relie Singapour à toutes les grandes villes de Malaisie et peut vous emmener jusqu’à Bangkok.</p>
<p><strong>Coût</strong></p>
<p>Le prix du trajet pour la Malaisie varie en fonction de la classe, du confort et de la distance parcourue.</p>
<h4 class="spip">Transport aérien</h4>
<p><strong>Qualité et prix</strong></p>
<p>La plupart des compagnies aériennes internationales sont représentées à Singapour. Les tarifs sont équivalents à ceux disponibles à partir de la France.</p>
<p><strong>Liaisons avec la France</strong></p>
<p>Air France, Qantas et Singapore Airlines proposent des vols directs avec la France. Les compagnies Lufthansa, British Airways, KLM et Malaysia Airlines proposent également des vols, avec escales.</p>
<p><strong>Compagnies aériennes locales et étrangères</strong></p>
<p>La plupart des compagnies aériennes possèdent une agence de représentation à Singapour. Cette île est une véritable plateforme aérienne puisque la plupart des vols à destinations de l’Asie ou de l’Océanie y font escale.</p>
<p>L’aéroport de Changi a inauguré en avril 2006 un terminal low cost (petits budgets).</p>
<p><strong>Coût</strong></p>
<p>Un billet pour la France coûte entre 1300 et 1500 SGD (à partir de 650 euros) s’il est acheté sur Internet auprès d’agences proposant des billets dégriffés sur les compagnies aériennes classiques (Air France, Singapore Airlines, KLM, Qantas…) (par exemple : <a href="http://www.zuji.com.sg/" class="spip_out" rel="external">www.zuji.com.sg</a>). Attention, ces billets ne sont souvent ni échangeables ni remboursables.</p>
<p>Un billet pour la France acheté auprès d’une agence de voyages coûtera autour de 2500 SGD soit environ 1300 euros.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/vie-pratique/article/transports). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
