# L’aide judiciaire

<p>Le bureau RCA vous assiste lors de la procédure de recouvrement de créances alimentaires à l’étranger pour bénéficier de l’aide juridictionnelle.</p>
<p>Domicilié en France ou à l’étranger, l’octroi de l’aide juridictionnelle vous permet, en cas de faibles revenus, de bénéficier d’une assistance juridique ainsi que la prise en charge par l’État des frais de justice et des honoraires des mandataires désignés pour accomplir les actes de la procédure de recouvrement à l’encontre de votre débiteur d’aliments (avocat, huissier).</p>
<p>Selon vos ressources, l’État prend en charge soit la totalité (aide totale), soit une partie des frais de justice (aide partielle).</p>
<p>L’autorité centrale du pays dans lequel vous résidez saisie de votre demande de recouvrement de créance alimentaire vous communique le formulaire de demande d’aide juridictionnelle et la liste des pièces justificatives requises. Elle vous guide pour introduire votre demande d’obtention d’aide judiciaire soit auprès du bureau d’aide juridictionnelle compétent soit auprès de l’autorité compétente désignée pour instruire ces demandes.</p>
<p><i>Mise à jour : juillet 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/conseils-aux-familles/recouvrement-de-creances/article/l-aide-judiciaire). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
