# Vie pratique

<h2 class="rub23057">Pour en savoir plus</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/vie-pratique/#sommaire_1">Cinéma</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/vie-pratique/#sommaire_2">Littérature</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/vie-pratique/#sommaire_3">Liens utiles</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/vie-pratique/#sommaire_4">Librairie spécialisée</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Cinéma</h3>
<h4 class="spip">Documentaires</h4>
<ul class="spip">
<li><i>Des racines et des ailes : Vienne</i> – documentaire télévisuel / France télévision</li>
<li><i>Autriche : le paradoxe autrichien(le dessous des cartes</i>)– documentaire télévisuel / Arte</li></ul>
<h4 class="spip">Fiction</h4>
<ul class="spip">
<li><i>Liebelei</i> de Max Öphuls – 1933</li>
<li><i>De Mayerling à Sarajevo</i> de Max Öphuls - 1940</li>
<li><i>La lettre d’une inconnue</i> de Max Öphuls – 1948</li>
<li>Le troisième homme de Carol Reed – 1949</li>
<li><i>Freud, passions secrètes</i> de John Huston - 1962</li>
<li><i>Sissi, Sissi impératrice, Sissi face à son destin</i> d’Ernst Marischka - 1955-1957</li>
<li><i>Welcome in Vienna</i> (3 parties) d’Axel Corti – 1982-1986</li>
<li><i>Colonel Redl</i> d’Istvan Szabo – 1985</li>
<li><i>Benny’s video</i> de Michael Haneke – 1993</li>
<li><i>La Marche de Radetzky</i> (série télévisée) d’Axel Corti - 1995</li>
<li><i>Funny games</i> de Michael Haneke - 1997</li>
<li><i>La pianiste</i> de Michael Haneke - 2001</li>
<li><i>Marie Bonaparte</i> de Benoit Jacquot – 2004</li>
<li><i>Before Sunrise</i> de Richard Linklater – 2004</li>
<li>A dangerous method de David Cronenberg - 2011</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Littérature</h3>
<h4 class="spip">Romans et témoignages</h4>
<ul class="spip">
<li>Heimito von Doderer : <i>Un meurtre que tout le monde commet</i></li>
<li>Adalbert Stifter : <i>L’arrière-saison</i></li>
<li>Arthur Schnitzler : <i>Vienne au crépuscule</i> <br class="manualbr"><i>Mademoiselle Else</i> <br class="manualbr"><i>Le Sous-Lieutenant Gustel</i> <br class="manualbr"><i>La nouvelle rêvée</i> <br class="manualbr"><i>Une Jeunesse viennoise</i></li>
<li>Stefan Zweig : <i>La confusion des sentiments</i><br class="manualbr"><i>Vingt-quatre heures de la vie d’une femme</i> <br class="manualbr"><i>La pitié dangereuse</i> <br class="manualbr"><i>Le joueur d‘échecs</i> <br class="manualbr"><i>Le monde d’hier. Souvenirs d’un Européen</i> <br class="manualbr"><i>Lettre d’une inconnue</i></li>
<li>Joseph Roth : <i>La marche de Radetzky</i> <br class="manualbr"><i>La crypte des Capucins</i> <br class="manualbr"><i>Une heure avant la fin du monde</i></li>
<li>Robert Musil : <i>L’homme sans qualités</i></li>
<li>Karl Kraus : <i>Aphorismes</i></li>
<li>Leopold von Sacher-Masoch : <i>La Vénus à la fourrure</i></li>
<li>Peter Handke : <i>La femme gauchère</i> <br class="manualbr"><i>Un voyage hivernal vers le Danube</i><br class="manualbr"><i>L’absence</i></li>
<li>Ingeborg Bachmann : <i>Malina</i> <br class="manualbr"><i>Le passeur</i></li>
<li>Thomas Bernhard : <i>Le neveu de Wittgenstein</i> <br class="manualbr"><i>Extinction</i> <br class="manualbr"><i>L’origine</i></li>
<li>Elfriede Jelinek : <i>La pianiste</i></li>
<li>Wolf Haas : <i>Vienne la mort</i><br class="manualbr"><i>Silentium !</i></li>
<li>Alfred Komarek : <i>Les larmes de Polt</i></li>
<li>Christoph Ransmayr : <i>La montagne volante</i></li>
<li>Daniel Glattauer : <i>Quand souffle le vent du nord</i></li>
<li>Arno Geiger : <i>Tout va bien</i></li>
<li>Eva Menasse : <i>Vienna</i></li></ul>
<h4 class="spip">Ouvrages de références</h4>
<ul class="spip">
<li>Jean Bérenger : <i>Histoire de l’empire des Habsbourg</i></li>
<li>Jacques Le Rider : <i>Les Juifs viennois à la Belle Epoque</i></li>
<li>Jean Bérenger : <i>L’Autriche-Hongrie de 1815 à 1918</i></li>
<li>Jean-Paul Bled : <i>Marie-Thérèse d’Autriche</i></li>
<li>Paul Pasteur : <i>Histoire de l’Autriche, de l’empire multinational à la nation autrichienne</i></li>
<li>Gérald Stieg : <i>L’Autriche, une nation chimérique ?</i> <br class="manualbr"><i>XVIIIème-XXème siècles</i></li>
<li>Steven Beller : <i>Histoire de l’Autriche</i></li>
<li>David Tarot : <i>Vienne et l’Europe Centrale</i> (guide culturel)</li>
<li>Brigitte Hamann : <i>Elisabeth d’Autriche</i></li>
<li>Max Schiavon : <i>L’Autriche-Hongrie, 1867-1918</i></li>
<li>Jean-Paul Bled : <i>Histoire de Vienne</i></li>
<li>Jean-Paul Bled : <i>François-Joseph</i></li>
<li>Félix Kreisler : <i>L’Autriche brûlure de l’histoire ; brève histoire de l’Autriche de 1800 à 2000</i></li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Liens utiles</h3>
<ul class="spip">
<li><a href="http://www.amb-autriche.fr/" class="spip_out" rel="external">Ambassade d’Autriche en France</a></li>
<li><a href="http://austrocult.fr/" class="spip_out" rel="external">Forum culturel autrichien</a><br class="manualbr">17 avenue de Villars, 75007 Paris<br class="manualbr">Tél. : +33 (0)1 47 05 27 10, Fax : +33 (0)1 47 05 26 42<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/vie-pratique/#paris-kf#mc#bmeia.gv.at#" title="paris-kf..åt..bmeia.gv.at" onclick="location.href=mc_lancerlien('paris-kf','bmeia.gv.at'); return false;" class="spip_mail">Courriel</a></li>
<li><a href="http://www.aauparis.fr/fr/accueil.html" class="spip_out" rel="external">Association autrichienne à Paris</a></li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Librairie spécialisée</h3>
<p><strong>Librairie Buchladen</strong><br class="manualbr">Située sur la Butte Montmartre elle offre un large choix d’ouvrages de littérature classique, poésie, psychologie, philosophie, histoire, romans policiers, méthodes d’apprentissage d’allemand, dictionnaires, DVD, auteurs allemands et autrichiens traduits en français, éditions bilingues et quelques livres jeunesse.</p>
<p>3 Rue Burq – 75018 Paris<br class="manualbr">Tel. + 33 (0) 1 42 55 42 13<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/vie-pratique/#buchladen#mc#wanadoo.fr#" title="buchladen..åt..wanadoo.fr" onclick="location.href=mc_lancerlien('buchladen','wanadoo.fr'); return false;" class="spip_mail">Courriel</a> <br class="manualbr">Ouverte du mardi au dimanche 11h00-19h30<br class="manualbr">Métro : Blanche ou Abbesses</p>
<p>Une fois en Autriche, <a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-autriche-presence-francaise-article-presence-francaise.md" class="spip_in">gardez le contact avec la France</a>.</p>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/vie-pratique/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
