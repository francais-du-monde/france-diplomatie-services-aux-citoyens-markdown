# Recherche d’emploi

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/emploi-stage/article/recherche-d-emploi-113144#sommaire_1">Outils pour la recherche d’emploi</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/emploi-stage/article/recherche-d-emploi-113144#sommaire_2">Organismes pour la recherche d’emploi</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Outils pour la recherche d’emploi</h3>
<p><strong>Journaux</strong></p>
<p>La <strong>presse locale</strong> met en ligne des sites spécialisés offrant au niveau de chaque région et Etat des offres ciblées. Pour connaitre les journaux de votre Etat, vous pouvez consulter le site internet <a href="http://www.world-newspapers.com/usa.html" class="spip_out" rel="external">World-newspapers.com</a>.</p>
<p><strong>Sites internet</strong></p>
<p>Les sites internet diffusant des offres d’emploi sont nombreux et les plus importants proposent généralement des offres du monde entier. Voici ci-dessous les sites institutionnels pour la recherche d’emploi pour la France et les Etats-Unis.</p>
<ul class="spip">
<li>Français<br class="manualbr"><a href="http://www.pole-emploi-international.fr/www.eei.com.fr/eei/accueil.jsp" class="spip_out" rel="external">Service international de Pôle Emploi</a></li>
<li>Américain<br class="manualbr"><a href="http://www.usajobs.gov/" class="spip_out" rel="external">Usajobs.gov</a></li></ul>
<p><strong>Réseaux</strong></p>
<p>Le <i>networking</i>, consiste à se constituer et entretenir un réseau relationnel grâce aux rencontres, même anodines, que vous ferez. C’est une pratique très courante et très respectée aux Etats-Unis : être recommandé est perçu comme une caution morale.</p>
<p><strong>Le réseau des chambres de commerce</strong></p>
<p>Les chambres de commerce franco-américaines ou French American Chambers of Commerce (FACC) sont présentes dans les principales villes américaines et sont très proches des acteurs économiques locaux. Elles organisent de nombreuses manifestations favorisant les échanges entre professionnels et constituent à ce titre un relais utile dans le cadre d’une recherche d’emploi.</p>
<p>Au cœur de ce réseau particulièrement dense, affilié notamment à l’Union des chambres de commerce et d’industrie françaises à l’étranger (UCCIFE), la FACC de New York joue un rôle prépondérant.</p>
<p>Les autres chambres étrangères entretenant des relations bilatérales avec les Etats-Unis peuvent représenter un appui supplémentaire si vous pratiquez plusieurs langues ; il faut noter en particulier le dynamisme des chambres liant les pays d’Amérique du Sud aux Etats-Unis.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.uccife.org/" class="spip_out" rel="external">Coordonnées des chambres de commerce franco-américaines</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Organismes pour la recherche d’emploi</h3>
<p>Pour les non résidents aux Etats-Unis, la recherche d’emploi doit s’organiser de France et ce n’est qu’après avoir décroché une proposition de travail auprès d’une entreprise <i>sponsor</i> que l’on peut entamer une procédure de demande de visa auprès des autorités américaines.</p>
<p>Pour les personnes résidentes (carte verte ou double nationalité) ou bénéficiant d’un visa leur permettant d’occuper un emploi aux Etats-Unis, les possibilités de recherche sont multiples.</p>
<p><strong>Les agences de recrutement</strong></p>
<p>Les agences de recrutement sont nombreuses et sont généralement spécialisées par domaines d’activité : secrétariat, comptabilité, informatique, finance et management…</p>
<p>On peut trouver sur Internet des listes de cabinets spécialisés. Il est préférable d’utiliser –outre le choix de l’activité- une autre clef de tri, en précisant notamment le secteur géographique.</p>
<p><strong> <i>Headhunters</i> </strong></p>
<p>Un « chasseur de tête » (<i>headhunter</i> en anglais) est un recruteur ou une agence de recrutement spécialisée pour mettre en relation des professionnels hautement qualifiés et des entreprises.</p>
<p>Ce mode de recrutement est très efficace aux Etats-Unis si l’on possède un profil susceptible d’intéresser ces professionnels. Des annuaires spécialisés sont édités chaque année avec les coordonnées de leurs cabinets ; ils sont en vente dans la plupart des grandes librairies américaines et sur internet.</p>
<p><strong>Les forums pour l’emploi</strong></p>
<p>Les forums pour l’emploi ou <i>Job Fairs</i> permettent de rencontrer les professionnels au niveau local. Ces manifestations sont organisées par les villes, les comtés, les universités et écoles. Elles sont en règle générale ouvertes à tous et gratuites pour les demandeurs d’emploi. Elles permettent de prendre la mesure de l’activité locale et souvent d’obtenir un premier contact avec un recruteur potentiel.</p>
<p>Il est fortement conseillé de se rendre régulièrement à des sessions de recrutement et des entretiens d’embauche, y compris pour des postes ne suscitant pas forcément d’intérêt. C’est en effet un bon moyen de se préparer à la technique d’entretien et d’étendre son réseau relationnel (<i>networking</i>).</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/emploi-stage/article/recherche-d-emploi-113144). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
