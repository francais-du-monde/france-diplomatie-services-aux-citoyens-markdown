# Emploi, stage

<h2 class="rub23035">Marché du travail</h2>
<p class="chapo">
    Malgré l’amélioration du marché de l’emploi, les forts taux de croissance ne permettent pas de résorber le chômage chronique et la pauvreté du pays dont le taux s’élève encore à 40%. Le taux de chômage oscille autour de 15% et le secteur informel a été le plus dynamique en terme de création d’emplois au cours des dix dernières années, aujourd’hui il représente à peu près 57% de la population active. Sur l’année 2012, les secteurs bancaires et des télécommunications ont connu les plus forts taux de croissance, mais ont créé peu d’emplois comptabilisés et fiscalisés.
</p>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/emploi-stage/#sommaire_1">Secteurs à fort potentiel</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/emploi-stage/#sommaire_2">Secteurs à faible potentiel</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/emploi-stage/#sommaire_3">Rémunération</a></li></ul>
<p>Malgré l’amélioration du marché de l’emploi, les forts taux de croissance ne permettent pas de résorber le chômage chronique et la pauvreté du pays dont le taux s’élève encore à 40%. Le taux de chômage oscille autour de 15% et le secteur informel a été le plus dynamique en terme de création d’emplois au cours des dix dernières années, aujourd’hui il représente à peu près 57% de la population active. Sur l’année 2012, les secteurs bancaires et des télécommunications ont connu les plus forts taux de croissance, mais ont créé peu d’emplois comptabilisés et fiscalisés.</p>
<h3 class="spip"><a id="sommaire_1"></a>Secteurs à fort potentiel</h3>
<p>Tous les secteurs sont accessibles à un ressortissant étranger, sauf la fonction publique. Les Français sont le plus souvent travailleurs indépendants (dans la restauration ou l’hôtellerie).</p>
<p><strong>L’économie dominicaine est relativement diversifiée</strong> pour un pays de cette taille. L’industrie, avec 20% du PIB occupe une part significative, l’agriculture, compétitive sur le marché mondial 6%, et les services 64%, en raison du poids du tourisme.</p>
<p><strong>De nouveaux secteurs se développent rapidement </strong>à côté des productions traditionnelles (sucre, café, cacao, tabac), qui ne représentent plus que 4,5% des exportations, mais emploient une main d’œuvre encore élevée en milieu rural. Les fruits tropicaux, le riz, les haricots et les cultures sous serre, le poulet complètent la production agricole. La transformation de ces produits fait partie des prochaines étapes de <strong>la construction d’une filière agroalimentaire</strong>.</p>
<p><strong>48 parcs industriels</strong> bénéficient du statut de zone franche, ils accueillent 555 sociétés et emploient plus de 120000 personnes. Dominées par l’industrie textile jusqu’à la fin des accords multifibres en 2005, les zones franches opèrent une mutation vers la confection textile de niche et la diversification vers d’autres industries comme le matériel médico-chirurgical. <strong>Elles constituent des plates-formes d’exportation privilégiées vers les Etats-Unis et l’Union européenne</strong> avec lesquels la République dominicaine <strong>a conclu des accords commerciaux</strong>. Les échanges avec l’extérieur sont en outre facilités par l’existence de 12 ports marchands et 8 aéroports internationaux.</p>
<p><strong>Premier pays touristique de la Caraïbe</strong> avec plus de 4,5 millions de touristes en 2012, la République dominicaine diversifie son offre en ajoutant à son catalogue des séjours culturels et des randonnées écologiques. <strong>Un créneau réservé au tourisme « haut de gamme » reste sans doute à créer.</strong></p>
<p>Bien que les Dominicains de la classe aisée aient coutume de faire leurs emplettes aux Etats-Unis, <strong>les centres commerciaux </strong>fleurissent, notamment dans la capitale, qui en compte 13 pour 3,5 millions d’habitants, attestant de l’appétit de consommation des habitants. <strong>De nombreuses marques européennes, françaises en particulier, sont cependant encore absentes des galeries.</strong></p>
<p>Des efforts ont été déployés au cours des dernières années pour doter le pays de grands axes routiers, une politique qui sera poursuivie par la construction de voies secondaires. Le développement des transports publics à Saint-Domingue a constitué une priorité du gouvernement précédent et la capitale dispose désormais de deux lignes de métro.</p>
<p><strong>Le pays reste cependant sous équipé en matière d’infrastructures</strong> : eau, électricité, traitement des déchets, transport publics, urbanisme, etc.</p>
<h3 class="spip"><a id="sommaire_2"></a>Secteurs à faible potentiel</h3>
<p>Le marché du travail dominicain dans sa globalité offre peu d’opportunités pour des niveaux de rémunération comparables aux standards français et ne permet pas l’accès à une santé et une éducation de qualité. En revanche, les entrepreneurs dotés d’une spécialité ou d’une compétence originale peuvent trouver des niches profitables.</p>
<p>Parmi les secteurs à faible rémunération, on peut mentionner l’agriculture, la construction, l’industrie.</p>
<h3 class="spip"><a id="sommaire_3"></a>Rémunération</h3>
<p>Les salaires sont assez bas pour la région, avec un salaire minimum compris entre 7000 DOP/mois (120 EUR) et 12 000 DOP/mois (200 EUR) selon les secteurs d’activité. Le salaire moyen est évalué à 18 000 DOP/mois (315 EUR). On note toutefois des écarts très sensibles entre les statuts : employés locaux ou expatriés, entrepreneurs ou salariés.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-dominicaine-emploi-stage-article-stages.md" title="Stages">Stages</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-dominicaine-emploi-stage-article-recherche-d-emploi.md" title="Recherche d’emploi">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-dominicaine-emploi-stage-article-reglementation-du-travail.md" title="Réglementation du travail">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-dominicaine-emploi-stage-article-marche-du-travail-111311.md" title="Marché du travail">Marché du travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/emploi-stage/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
