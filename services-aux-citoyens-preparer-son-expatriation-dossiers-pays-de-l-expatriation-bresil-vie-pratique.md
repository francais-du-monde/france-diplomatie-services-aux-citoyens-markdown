# Vie pratique

<h2 class="rub22829">Logement</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/vie-pratique/#sommaire_1">Où se loger ? </a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/vie-pratique/#sommaire_2">Conditions de location</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/vie-pratique/#sommaire_3">Hôtels</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/vie-pratique/#sommaire_4">Electricité</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/vie-pratique/#sommaire_5">Electroménager</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Où se loger ? </h3>
<p>Les loyers sont donnés à titre indicatif</p>
<p><strong>Rio de Janeiro </strong></p>
<table class="spip">
<thead><tr class="row_first"><th id="ida0fe_c0">Loyer mensuel - quartier résidentiel</th><th id="ida0fe_c1">reals</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="ida0fe_c0">Studio</td>
<td class="numeric " headers="ida0fe_c1">1 100</td></tr>
<tr class="row_even even">
<td headers="ida0fe_c0">4 pièces</td>
<td class="numeric " headers="ida0fe_c1">6 100</td></tr>
<tr class="row_odd odd">
<td headers="ida0fe_c0">5 pièces</td>
<td class="numeric " headers="ida0fe_c1">6 800</td></tr>
<tr class="row_even even">
<td headers="ida0fe_c0">Villa</td>
<td class="numeric " headers="ida0fe_c1">7 000</td></tr>
</tbody>
</table>
<p><strong>Sao Paulo</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Loyer mensuel - quartier résidentiel</strong></td>
<td>euros</td></tr>
<tr class="row_even even">
<td>3 pièces</td>
<td>1 500 à 2 500</td></tr>
<tr class="row_odd odd">
<td>5 pièces</td>
<td>2 500</td></tr>
<tr class="row_even even">
<td>Villa</td>
<td>3 000 à 4 000</td></tr>
</tbody>
</table>
<p><strong>Brasilia</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Loyer mensuel - quartier résidentiel</strong></td>
<td>euros</td></tr>
<tr class="row_even even">
<td>Studio</td>
<td>500</td></tr>
<tr class="row_odd odd">
<td>4 pièces</td>
<td>1 300</td></tr>
<tr class="row_even even">
<td>5 pièces</td>
<td>1 800</td></tr>
<tr class="row_odd odd">
<td>Villa</td>
<td>2000</td></tr>
</tbody>
</table>
<p><strong>Recife</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Loyer mensuel - quartier résidentiel</strong></td>
<td>reals</td></tr>
<tr class="row_even even">
<td>Studio</td>
<td>800</td></tr>
<tr class="row_odd odd">
<td>3 pièces</td>
<td>2 000 à 4 000</td></tr>
<tr class="row_even even">
<td>5 pièces</td>
<td>5 000 à 8 000</td></tr>
<tr class="row_odd odd">
<td>Villa</td>
<td>9 000 et +</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_2"></a>Conditions de location</h3>
<p>Le marché locatif est très ouvert. Le recours aux <strong>agences immobilières</strong> est la solution la plus courante ; la commission de l’agence est prise en charge par le propriétaire. On peut également avoir recours au "bouche à oreille" au sein de la communauté expatriée ou consulter les <strong>petites annonces </strong>paraissant dans les quotidiens comme <i>O Globo </i>ou <i>Jornal do Brasil </i>à Rio, <i>Folha de Sao Paulo </i>… plus particulièrement dans les éditions des samedi et dimanche.</p>
<p>La durée des baux varie de un à trois ans, avec réajustement du loyer tous les ans ; le loyer est payable mensuellement et d’avance. Bien qu’il soit rarement proposé par les propriétaires, un état des lieux est vivement conseillé. Il est nécessaire de disposer d’un <i>fiador</i>, personne ou organisme se portant garant du paiement du loyer. L’entreprise peut jouer le rôle de garant.</p>
<p>A <strong>Rio </strong>les quartiers résidentiels sont situés dans la zone sud et centre-sud (Ipanema, Leblon, Copacabana, Leme, Urca, Flamengo, Botafogo, Laranjeiras). L’offre locative reste limitée et le recours à un agent immobilier est indispensable pour les visites.</p>
<p>A <strong>Sao Paulo</strong>, ce sont les quartiers Paraiso, Pinheiros, Vila Madalena, Vila Mariana, Jardims, Higienopolis. L’offre d’appartements est relativement importante, mais la qualité des produits est très inégale et les biens sont souvent sur-valorisés par rapport aux prestations proposées. En général, les appartements sont loués non meublés.</p>
<p>A Rio comme à Sao Paulo, il est conseillé, pour des raisons de sécurité, d’opter pour un appartement, en étage élevé, plutôt qu’une villa. Il convient d’ailleurs de noter que, à prestations égales, les villas peuvent être moins chères à la location en raison du surcoût représenté par le gardiennage.</p>
<p>A <strong>Brasilia </strong>on peut facilement se loger en villa. Les quartiers résidentiels se situent, pour les villas, à Lago sul, Lago norte et Condominios et pour les appartements, à Asa sul, Asa norte, Sudoeste.</p>
<p>A <strong>Recife</strong>, Casa Forte, Espinheiro et Graças sont les quartiers résidentiels, ainsi que les bords de plage de Boa Viagem et Piedade. Il est plus facile de trouver un appartement vide ; les villas sont assez rares et déconseillées pour des raisons de sécurité. Il n’est pas recommandé d’habiter à la périphérie de la ville, également pour des questions de sécurité. A noter qu’il existe peu de studio vide, on trouve surtout des F3 ou plus ; les studios meublés (<i>flats</i>) sont situés dans les résidences comprenant différents services. Le délai moyen de recherche est de un mois.</p>
<p>A noter que les charges locatives sont élevées : les frais de gardiennage en représentent une part importante. Le locataire doit également acquitter la taxe d’habitation et foncière (IPTU).</p>
<h3 class="spip"><a id="sommaire_3"></a>Hôtels</h3>
<h4 class="spip">Auberges de jeunesse</h4>
<p>Se renseigner auprès de :</p>
<p><a href="http://www.hostel.org.br/" class="spip_out" rel="external">Federaçao Brasileira dos Albergues da Juventude</a><br class="manualbr">Rua da Assemblea<br class="manualbr">10 sala 1617 - Rio de Janeiro<br class="manualbr">Tél : (55 21) 2531 1085</p>
<h3 class="spip"><a id="sommaire_4"></a>Electricité</h3>
<p>Le courant est alternatif et le voltage n’est pas le même dans tous les états :</p>
<ul class="spip">
<li>60 hertz, 110/220 volts à Rio, à Sao Paulo ;</li>
<li>60 hertz, 220 volts à Brasilia et Recife.</li></ul>
<p>On trouve des prises de type européen (fiches rondes) et américain (fiches plates). Des adaptateurs sont disponibles sur place.</p>
<h3 class="spip"><a id="sommaire_5"></a>Electroménager</h3>
<p>L’électroménager est disponible sur place.</p>
<p>A noter que dans le Nordeste les appareils électroménagers ont une durée de vie inférieure à la moyenne, en raison d’un taux d’humidité très élevé. Un mobilier fragile est également déconseillé pour les mêmes raisons et à cause de la présence de termites. On trouve sur place du mobilier en bois, bambou ou rotin, ainsi que des tables en marbre et pierres diverses.</p>
<p>Les cuisines sont rarement équipées sauf dans les meublés.</p>
<h4 class="spip">Chauffage et climatisation</h4>
<p>Le chauffage existe rarement, même si dans des villes comme Sao Paulo ou Brasilia il pourrait être nécessaire certains jours d’hiver. Dans ce cas, il faut recourir à des chauffages électriques d’appoint ou des climatiseurs réversibles.</p>
<p>La climatisation est nécessaire en été mais les appartements (contrairement aux bureaux) en sont rarement équipés.</p>
<h4 class="spip">Equipement vidéo</h4>
<p>Les chaînes locales peuvent être captées seulement par les télévisions multi-systèmes compatibles avec le PAL M. Les cassettes vidéo achetées sur le marché local ne peuvent être visionnées qu’avec un lecteur de vidéo systèmes NTSC et PAL M.</p>
<p>Il est donc préférable de faire ses achats sur place.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-bresil-vie-pratique-article-loisirs-et-culture-109999.md" title="Loisirs et culture">Loisirs et culture</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-bresil-vie-pratique-article-communications.md" title="Communications">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-bresil-vie-pratique-article-transports.md" title="Transports">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-bresil-vie-pratique-article-cout-de-la-vie-109996.md" title="Coût de la vie">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-bresil-vie-pratique-article-scolarisation-109995.md" title="Scolarisation">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-bresil-vie-pratique-article-sante.md" title="Santé">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-bresil-vie-pratique-article-logement-109994.md" title="Logement">Logement</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/vie-pratique/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
