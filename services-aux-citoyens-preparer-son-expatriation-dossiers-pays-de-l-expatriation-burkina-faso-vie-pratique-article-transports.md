# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/vie-pratique/article/transports#sommaire_1">Importation de véhicule</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/vie-pratique/article/transports#sommaire_2">Permis de conduire</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/vie-pratique/article/transports#sommaire_3">Code de la route</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/vie-pratique/article/transports#sommaire_4">Assurances et taxes</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/vie-pratique/article/transports#sommaire_5">Achat et location</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/vie-pratique/article/transports#sommaire_6">Entretien</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/vie-pratique/article/transports#sommaire_7">Réseau routier</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/vie-pratique/article/transports#sommaire_8">Transports en commun</a></li></ul>
<p>Il est très fortement déconseillé de rouler de nuit sur les routes en raison de leur mauvais état, de la présence d’animaux errants et de véhicules dépourvus de feux de signalisation et de l’éventuelle présence de "coupeurs" de routes.</p>
<p><strong>En cas d’accident </strong></p>
<p>En cas d’accident, alerter le poste de gendarmerie le plus proche et contacter immédiatement le Consulat de France.</p>
<p><strong>Numéros utiles à Ouagadougou </strong></p>
<ul class="spip">
<li>Police : 17</li>
<li>Pompiers : 18</li>
<li>Gendarmerie : 30 62 71</li></ul>
<p><strong>Numéros utiles à Bobo Dioulasso </strong></p>
<ul class="spip">
<li>Police : 17</li>
<li>Pompiers : 98 26 71</li>
<li>Gendarmerie : 97 00 32</li>
<li>Burkina Secours : 97 01 43</li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Importation de véhicule</h3>
<p>L’entrée et la revente d’un véhicule ne sont pas soumises à l’application de normes locales.Les droits de douane s’élèvent en effet de 45% à 48% de la valeur du véhicule selon qu’il est neuf ou d’occasion.</p>
<h3 class="spip"><a id="sommaire_2"></a>Permis de conduire</h3>
<p>Le permis français est reconnu par les autorités burkinabé.</p>
<h3 class="spip"><a id="sommaire_3"></a>Code de la route</h3>
<p>Le code de la route, souvent méconnu, est peu respecté. Les sanctions, malgré leur codification, ne sont pas appliquées de manière stricte.</p>
<h3 class="spip"><a id="sommaire_4"></a>Assurances et taxes</h3>
<p>L’assurance au tiers est obligatoire. Il est préférable de souscrire une assurance tous risques. A titre indicatif, le coût, pour un 4x4, en est estimé à 850 000 FCFA par an.</p>
<h3 class="spip"><a id="sommaire_5"></a>Achat et location</h3>
<p>Peugeot, Renault, Mitsubishi, Nissan, Toyota sont les marques de véhicules représentées sur place. Le 4x4 est le plus prisé, particulièrement pour les déplacements en brousse.Les contrôles des services de l’Etat s’exercent annuellement sur les véhicules de tourisme ; tous les six mois sur les véhicules utilitaires. Ce contrôle, payant, se révèle particulièrement draconien pour les véhicules des étrangers.</p>
<p>L’achat d’une voiture sur place, solution à privilégier à l’importation de son véhicule personnel, s’effectue en s’adressant à l’importateur de la marque choisie.</p>
<p>A titre indicatif, le coût d’une Peugeot 307 de deux ans est estimé à 8.000.000 FCFA (environ 12 000€).</p>
<p>La location d’un véhicule avec chauffeur s’élève à 45.000 FCFA par jour (environ 68€).</p>
<h3 class="spip"><a id="sommaire_6"></a>Entretien</h3>
<p>Pour l’entretien, prévoir trois à cinq semaines de délais d’acheminement des pièces détachées.</p>
<h3 class="spip"><a id="sommaire_7"></a>Réseau routier</h3>
<p>Les grands axes routiers sont en bon état</p>
<h3 class="spip"><a id="sommaire_8"></a>Transports en commun</h3>
<p>La liberté de circulation est totale.</p>
<p>L’avion (Air Burkina), l’autocar, le taxi ou la location d’un véhicule permettent les déplacements intérieurs.En ville, le grand nombre de véhicules à deux roues rend la circulation difficile.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/vie-pratique/article/transports). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
