# L’inscription auprès des services consulaires se fait désormais en ligne !

<p class="chapo">
      Depuis le 14 juin, les Français résidant à l’étranger peuvent s’inscrire auprès des services consulaires, actualiser leurs données ou signaler leur départ directement sur le site <a href="https://www.service-public.fr/particuliers/vosdroits/F33307" class="spip_out" rel="external">service-public.fr</a>.
</p>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/inscription-consulaire-et-communaute-francaise/article/l-inscription-aupres-des-services-consulaires-se-fait-desormais-en-ligne#sommaire_1">Découvrir le service en vidéo</a></li></ul>
<p><strong>Un service ouvert 24h/24 et accessible, même lorsqu’on habite loin du consulat !</strong></p>
<p class="spip_document_93674 spip_documents spip_documents_center">
<img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L720xH360/presentation_service_inscription_registre_rs_05_cle0b73e6-71e20.png" width="720" height="360" alt=""></p>
<h4 class="spip">Pourquoi est-ce important de s’inscrire auprès des services consulaires lorsqu’on s’installe à l’étranger ?</h4>
<ul class="spip">
<li>Le consulat dispose de vos coordonnées et de celles de vos proches en France en cas d’urgence.</li>
<li>Le consulat peut vous envoyer si besoin des messages électroniques sur l’organisation des élections, les conditions de sécurité, l’organisation de tournées consulaires.</li>
<li>Les services délivrés par les consulats (renouvellement de passeport, demande de bourse scolaire, état-civil…) sont facilités.</li></ul>
<p>Pour s’inscrire, c’est très simple il suffit de se connecter sur le site <a href="https://www.service-public.fr/particuliers/vosdroits/F33307" class="spip_out" rel="external">service-public.fr</a>, de remplir le questionnaire en ligne et d’y joindre électroniquement les documents justifiant de son identité, sa nationalité française et sa résidence dans le pays.</p>
<h3 class="spip"><a id="sommaire_1"></a>Découvrir le service en vidéo</h3><iframe width="640" height="360" src="https://www.youtube.com/embed/yc6XZH0a5w0" frameborder="0" allowfullscreen></iframe><hr class="spip">
<p><strong>Lien utile :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="https://www.service-public.fr/particuliers/vosdroits/F33307" class="spip_out" rel="external">Inscription consulaire au registre des Français établis hors de France sur le site service-public.fr</a></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/inscription-consulaire-et-communaute-francaise/article/l-inscription-aupres-des-services-consulaires-se-fait-desormais-en-ligne). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
