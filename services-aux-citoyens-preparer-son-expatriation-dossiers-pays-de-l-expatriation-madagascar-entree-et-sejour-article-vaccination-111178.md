# Vaccination

<p>Aucune vaccination n’est exigée en provenance d’Europe ; la vaccination contre la fièvre jaune (antiamarile) est exigée des voyageurs en provenance d’une zone infectée.</p>
<p><strong>Sont conseillées pour des raisons médicales :</strong></p>
<ul class="spip">
<li>adultes : la mise à jour des vaccinations contre la diphtérie, le tétanos, la poliomyélite ; la vaccination contre la typhoïde, l’hépatite A, l’hépatite B.</li>
<li>enfants : les vaccinations recommandées en France par le ministère de la santé, et en particulier le BCG et l’hépatite B dès le premier mois, la rougeole dès l’âge de neuf mois.</li></ul>
<p>Il est préférable de réaliser toutes les vaccinations nécessaires avant de partir, car une fois sur place, on peut rencontrer des difficultés d’approvisionnement.</p>
<p>Pour en savoir plus :</p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/madagascar/" class="spip_in">Conseils aux voyageurs</a></li>
<li>notre thématique <a href="services-aux-citoyens-preparer-son-expatriation-sante.md" class="spip_in">Santé</a>.</li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/entree-et-sejour/article/vaccination-111178). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
