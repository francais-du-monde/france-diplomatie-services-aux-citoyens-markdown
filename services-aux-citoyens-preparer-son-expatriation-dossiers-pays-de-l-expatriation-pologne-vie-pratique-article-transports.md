# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/vie-pratique/article/transports#sommaire_1">Importation de véhicule</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/vie-pratique/article/transports#sommaire_2">Permis de conduire</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/vie-pratique/article/transports#sommaire_3">Code de la route</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/vie-pratique/article/transports#sommaire_4">Assurances et taxes</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/vie-pratique/article/transports#sommaire_5">Achat et location</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/vie-pratique/article/transports#sommaire_6">Entretien</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/vie-pratique/article/transports#sommaire_7">Réseau routier</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/vie-pratique/article/transports#sommaire_8">Transports en commun</a></li></ul>
<p>En cas d’accident mineur, établir un constat à l’amiable (modèle européen). En cas de litige grave, il est possible d’obtenir des forces de police l’assistance d’un traducteur assermenté. En pratique, au moindre accrochage, même dans un parking, la police est systématiquement appelée.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.ambafrance-pl.org/Conduite-a-tenir-en-cas-d-accident" class="spip_out" rel="external">site de l’Ambassade de France en Pologne</a></p>
<h3 class="spip"><a id="sommaire_1"></a>Importation de véhicule</h3>
<p><i>Chaque cas étant particulier, les informations ci-dessous n’ont qu’un caractère indicatif. Pour avoir des précisions, il faut toujours s’adresser aux autorités polonaises compétentes.</i></p>
<p>L’étranger qui vient en Pologne pour un séjour temporaire peut faire entrer sa voiture personnelle sur le territoire polonais sans payer les droits d’accise, pour une période ne dépassant pas les six mois pendant une année. La voiture est ré-immatriculée en Pologne mais ne peut en aucun cas être vendue en Pologne.</p>
<p>Pour les étrangers qui font venir leurs véhicules pour plus de 6 mois, les droits d’accise sont obligatoires (environ 3%).</p>
<p>Les voitures de fonction immatriculées à l’étranger au nom d’une société peuvent entrer en Pologne sous le régime de l’admission temporaire.</p>
<p>Les voitures de moins de dix ans doivent être équipées d’un pot catalytique.</p>
<p>L’immatriculation d’un véhicule est obligatoire au terme d’un délai de 30 jours suivant le dédouanement du véhicule, et au plus tard, dans le délai de six mois suivant l’installation en Pologne. Il n’est pas légal (et juridiquement risqué en cas de vol de véhicule) de conserver ses plaques d’immatriculation françaises.</p>
<p>Les services polonais compétents sont ceux du <strong>Powiat</strong> (à Varsovie : Plac Starynkiewicza 7/9, bureau 8, tél. 22.629.87.15)</p>
<h3 class="spip"><a id="sommaire_2"></a>Permis de conduire</h3>
<p>Les ressortissants des pays de l’Union européenne, titulaires de permis de conduire nationaux, résidant en Pologne, ne sont plus soumis à l’obligation de l’échange de leurs permis (loi du 29 juillet 2005, publié au JO n°180, point 1497, art.3).</p>
<h3 class="spip"><a id="sommaire_3"></a>Code de la route</h3>
<p>Le code de la route est semblable à celui en usage en France, mais les pratiques sont différentes. Les conducteurs sont parfois peu respectueux des règles élémentaires de prudence (taux très élevé d’accidents mortels). Conduite dangereuse en ville et sur route, notamment dépassements à droite, utilisation du bas-côté, dépassements sans visibilité, vitesse excessive etc Les feux de croisement doivent être allumés jour et nuit tout au long de l’année. Faire attention aux conducteurs et aux piétons en état d’ébriété.</p>
<p>La vitesse maximale est de 50 km/h en agglomération (parfois 40 km/h), 90 km/h sur route, 110 km/h sur autoroute.</p>
<p>Le taux d’alcoolémie toléré est de 0,2%. Un taux d’alcoolémie entre 0,2 % et 0,5% est passible d’une contravention. Un taux d’alcoolémie supérieur à 0,5 % est considéré comme un délit et passible d’une peine de prison.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/pologne/" class="spip_in">Conseils aux voyageurs</a> </p>
<h3 class="spip"><a id="sommaire_4"></a>Assurances et taxes</h3>
<p>Il est nécessaire d’être affilié auprès d’une compagnie d’assurance locale.</p>
<p>La responsabilité civile au tiers illimitée est obligatoire, mais il est préférable de prendre une assurance tous risques.</p>
<p>Responsabilité civile (OC) : environ 1200 PLN</p>
<p>Tous risques : 12,32 % de la valeur de la voiture entre un et trois ans.</p>
<p>Personnes transportées (NNW) : 50 PLN</p>
<p>Un contrôle technique est obligatoire au moment de l’immatriculation du véhicule importé (phares blancs, bavettes aux roues arrières). Ensuite, le premier contrôle se fait au bout de trois ans pour un véhicule neuf, puis à échéances plus rapprochées.</p>
<h3 class="spip"><a id="sommaire_5"></a>Achat et location</h3>
<p>On peut acheter sur place des véhicules français ou étrangers. Les documents nécessaires pour l’immatriculation du véhicule sont les suivants :</p>
<ul class="spip">
<li>la facture établie au nom de l’acheteur, et à son adresse privée en Pologne.</li>
<li>la copie du livret de véhicule « KARTA POJAZDU » - délivré par le vendeur</li>
<li>le certificat de conformité délivré par le vendeur.</li></ul>
<p>Les véhicules vendus en Pologne sont payables TTC.</p>
<p>La TVA s’élève à 22% de la valeur du véhicule.</p>
<p>La taxe d’accise s’élève à :</p>
<ul class="spip">
<li>3,1 % de la valeur pour les véhicules dont la cylindrée est inférieure ou égale à 2000 cm<sup>3</sup></li>
<li>18,6 % de la valeur pour les véhicules dont la cylindrée est supérieure à 2000 cm<sup>3</sup></li></ul>
<p>Il y a une légère différence avec les prix français (entre 5 et 10 %) très variable selon les marques et les modèles.</p>
<p>Les marques Renault, Citroën et Peugeot sont représentées ainsi que les marques européennes (Fiat, Volkswagen, BMW, Audi, Mercedes, Opel, Volvo, Skoda, etc.), japonaises (Toyota, Nissan, Mitsubishi, etc.) et américaines (Ford, Chevrolet, Chrysler etc.).</p>
<p>Un véhicule robuste est préférable compte tenu de l’état du réseau routier et de la longueur de l’hiver.</p>
<p>Il est obligatoire d’équiper le véhicule de pneus neige d’octobre à avril.</p>
<h3 class="spip"><a id="sommaire_6"></a>Entretien</h3>
<p>On peut faire entretenir et réparer son véhicule, à un coût moins élevé qu’en France. Les pièces détachées doivent parfois être importées.</p>
<h3 class="spip"><a id="sommaire_7"></a>Réseau routier</h3>
<p>La liberté de circulation dans le pays est totale. Un réseau autoroutier se développe rapidement. Le réseau des routes ordinaires reste parfois médiocre (10 % est conforme aux normes européennes).</p>
<h3 class="spip"><a id="sommaire_8"></a>Transports en commun</h3>
<p>Dans les principales agglomérations, le réseau de transport urbain est dense et bien organisé. A Varsovie, il n’existe qu’une ligne de métro, mais de nombreux bus et tramways desservent tous les quartiers de la capitale. Le même ticket (4,50 PLN l’unité) est valable pour les trois modes de transport. Il existe des cartes pour la journée, la semaine ou le mois.</p>
<p>Pour les taxis individuels, la prise en charge est de 8 PLN, puis 3 PLN le km. Il est recommandé d’appeler un taxi d’une compagnie ou de se rendre dans une station.</p>
<p>Pour voyager à l’intérieur du pays, le chemin de fer est le moyen le moins onéreux et le plus rapide, particulièrement conseillé en hiver. Les trains sont ponctuels.</p>
<p>Informations sur le <a href="http://www.pkp.pl/" class="spip_out" rel="external">réseau national polonais</a> ou <a href="http://www.intercity.com.pl/" class="spip_out" rel="external">trains express (réservations)</a>.</p>
<p>La flotte aérienne (LOT) est moderne ; en raison du brouillard, les aéroports peuvent être temporairement fermés l’hiver.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  office de tourisme de Pologne  <a href="http://www.pologne.travel/fr/transport-urbain/" class="spip_out" rel="external">comment voyager</a> et site de l’<a href="http://www.ambafrance-pl.org/" class="spip_out" rel="external">Ambassade de France en Pologne</a>.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/vie-pratique/article/transports). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
