# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/vie-pratique/article/transports#sommaire_1">Importation de véhicule</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/vie-pratique/article/transports#sommaire_2">Permis de conduire</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/vie-pratique/article/transports#sommaire_3">Code de la route</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/vie-pratique/article/transports#sommaire_4">Assurances et taxes</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/vie-pratique/article/transports#sommaire_5">Achat et location</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/vie-pratique/article/transports#sommaire_6">Immatriculation</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/vie-pratique/article/transports#sommaire_7">Entretien</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/vie-pratique/article/transports#sommaire_8">Réseau routier</a></li>
<li><a id="so_9" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/vie-pratique/article/transports#sommaire_9">Transports en commun</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Importation de véhicule</h3>
<p>Tout véhicule importé par un particulier doit passer par le contrôle technique du TÜV (<i>Technischer Überwachungsverein</i>, équivalent du service des Mines) avant de pouvoir être immatriculé en Allemagne (si celui-ci est en possession d’un certificat de conformité européen et que le véhicule a moins de 3ans, il n’a pas besoin du contrôle technique. Sans certificat de conformité européen le véhicule doit être présenté au TÜV qui établira un « Vollgutachten » (expertise), celui-ci incluant les contrôles technique et anti-pollution).</p>
<p>Il n’y a pas de frais de douane dans le cas de l’importation d’un véhicule en provenance d’un Etat membre de la CE.</p>
<p>La revente sur place d’un véhicule déjà immatriculé en Allemagne ne pose aucun problème particulier. Tout véhicule de plus de 3 ans d’âge, doit être soumis à l’inspection du même TÜV, tous les 2 ans, pour obtenir les vignettes de droit de circulation apposées sur les plaques d’immatriculation.</p>
<p>Dans le cas du transfert d’un véhicule en Allemagne, vous devez signaler votre déménagement à la préfecture de votre ancien domicile en France (par courrier, en joignant une copie de la carte grise), qui procédera à la radiation de votre immatriculation des registres français.</p>
<p><a href="http://www.europe-consommateurs.eu/fr/accueil/" class="spip_out" rel="external">http://www.europe-consommateurs.eu/fr/accueil/</a></p>
<h3 class="spip"><a id="sommaire_2"></a>Permis de conduire</h3>
<p>Le permis français est reconnu en Allemagne.</p>
<h3 class="spip"><a id="sommaire_3"></a>Code de la route</h3>
<p>La ceinture de sécurité est obligatoire sur l’ensemble du territoire allemand.</p>
<p>Depuis le 1er février 2001, l’usage du téléphone mobile au volant est interdit, sauf si le véhicule est à l’arrêt, moteur éteint, ou s’il s’agit d’un dispositif "mains libres".</p>
<p>Le taux d’alcoolémie maximal autorisé est de 0,5g. Un taux compris entre 0,5g et 0,8g entraîne une contravention. Un taux supérieur entraîne des poursuites pénales.</p>
<p>Hors autoroute, les vitesses maximales autorisées sont les suivantes :</p>
<ul class="spip">
<li>en agglomération : 30 à 50 km/h</li>
<li>hors agglomération : 70 à 100 km/h.</li></ul>
<p><strong>En cas d’accident</strong> entre deux véhicules sur la voie publique, il est fortement recommandé aux conducteurs, après avoir signalé l’accident par la pose du triangle réglementaire, de rester sur les lieux de l’accident et de ne pas déplacer les véhicules. En effet, pour limiter les conflits éventuels entre les parties concernées ou leurs assureurs respectifs, la police allemande a coutume de se déplacer sur les lieux d’un accident : les parties concernées et les témoins éventuels sont interrogés sur place, les circonstances exactes de l’accident sont dûment consignées par cette autorité qui est compétente pour déterminer les responsabilités de chaque conducteur.</p>
<h3 class="spip"><a id="sommaire_4"></a>Assurances et taxes</h3>
<p>L’assurance des véhicules est obligatoire. Le minimum requis est la responsabilité civile <i>(Haftpflichtversicherung)</i>, qui doit être présentée lors des démarches d’immatriculation aux autorités concernées qui transmettront le numéro et la date d’immatriculation directement à l’assurance qui démarre au moment de l’établissement de la carte grise.</p>
<p>La souscription d’une assurance "défense et recours" (<i>Rechtschutz</i>) est fortement recommandée : les recours en justice sont fréquents, longs et coûteux.</p>
<p>Le coût de l’assurance n’est pas fonction de l’ancienneté ou la puissance du véhicule mais varie en fonction d’un numéro clé d’identification du véhicule (<i>Schlüsselnummer</i>). Ce n° d’identification est indiqué selon la puissance et le respect des normes écologiques.</p>
<p>Le coût de l’assurance dépend également du bonus d’assurance acquis par le propriétaire du véhicule : il est donc conseillé de se munir d’un relevé d’informations international (formulaire commun au sein de la Communauté européenne).</p>
<p>Différentes mesures ont été prises pour améliorer la qualité de l’air dans les villes et les communes, entre autres, l’instauration de zones environnementales (<i>Umweltzonen</i>) avec l’interdiction de circuler pour les véhicules à forte émission de substances polluantes. Les zones environnementales sont signalisées par des panneaux de signalisation. Les véhicules à faible émission de substances polluantes reçoivent une « vignette écologique » (<i>Feinstaubplakette</i>) permettant l’accès aux zones environnementales. Les véhicules à forte émission de substances polluantes ne reçoivent pas de vignette et n’ont pas le droit de circuler dans les zones environnementales.</p>
<p>Les véhicules circulant sans vignette dans les zones environnementales sont passibles d’amendes. Les véhicules étrangers sont également soumis à cette réglementation. Le certificat de conformité CEE – ou en l’absence de ce document, la date de la première immatriculation – permet de déterminer si le véhicule peut recevoir la vignette. <br class="manualbr">Les vignettes (tarif de 5 à 15 Euros) peuvent s’acheter en Allemagne auprès :<br class="manualbr">• des garages agréés<br class="manualbr">• des services du TÜV et de la DEKRA<br class="manualbr">• de tous les services des immatriculations allemands<br class="manualbr">Présentation des zones environnementales protégées sur le site Internet du <a href="http://www.bmub.bund.de/" class="spip_out" rel="external">ministère fédéral de l’Environnement</a>.</p>
<h3 class="spip"><a id="sommaire_5"></a>Achat et location</h3>
<p>Toutes les grandes marques de voiture françaises et étrangères sont représentées.</p>
<p>Le marché de l’occasion permet d’acquérir des véhicules en excellent état en raison des contrôles techniques obligatoires, avec un kilométrage plutôt élevé.</p>
<p>Les tarifs de location sont sensiblement identiques à ceux qui sont pratiqués en France.</p>
<p>A titre d’exemple, pour la location d’une "Golf WW compacte", il en coûtera environ 81€ par jour, 257€ par semaine ; la location comprend l’assurance tous risques avec franchise de 153€, un kilométrage illimité et la TVA.</p>
<h3 class="spip"><a id="sommaire_6"></a>Immatriculation</h3>
<p>La réglementation allemande ne prévoit pas de délai précis concernant l’obligation d’immatriculer le véhicule. Cependant il est conseillé de faire d’immatriculer votre véhicule dès votre installation définitive en Allemagne : <strong>le véhicule doit être réimmatriculé dans un délai d’un mois suivant le déménagement, mais il est toléré un délai allant jusqu’à 6 mois.</strong></p>
<p>Au moment de l’immatriculation du véhicule, le propriétaire doit s’acquitter de la « Kraftfahrzeugsteuer » (correspondant à la vignette).</p>
<p>L’administration compétente pour vous délivrer le certificat d’immatriculation allemand est le « Landratsamt » de votre lieu de résidence, et plus précisément la « Zulassungsstelle » (service immatriculation).<br class="manualbr">Il vous faudra présenter les documents suivants :</p>
<p><strong>Pour un véhicule neuf</strong></p>
<ul class="spip">
<li>Votre carte d’identité ou votre passeport</li>
<li>Une attestation de domicile qui vous est remise lors de l’inscription obligatoire auprès des services municipaux de votre domicile allemand</li>
<li>L’original du certificat de conformité communautaire</li>
<li>L’original de la carte grise française barrée ou une carte grise export</li>
<li>Le certificat de cession ou l’original de la facture</li>
<li>L’attestation d’assurance allemande ou française.</li></ul>
<p><strong>Pour un véhicule d’occasion</strong></p>
<ul class="spip">
<li>Tous les documents cités précédemment</li>
<li>L’attestation de contrôle technique (TÜV)</li>
<li>L’attestation de contrôle d’émission des gaz d’échappement pour les véhicules de plus de 3 ans.</li></ul>
<p>S’il n’existe pas de Certificat de conformité communautaire pour votre véhicule, vous devrez présenter au service immatriculation une attestation de réception à titre isolée (Vollgutachten des TÜV). Cette attestation vaudra également certificat de passage dans un centre de contrôle technique et de contrôle d’émission des gaz d’échappement.</p>
<h3 class="spip"><a id="sommaire_7"></a>Entretien</h3>
<p>Le coût d’entretien d’une voiture est plus élevé qu’en France, d’environ 25%.</p>
<h3 class="spip"><a id="sommaire_8"></a>Réseau routier</h3>
<p>Le réseau autoroutier allemand est gratuit, au maillage certes très dense, et l’un des plus anciens d’Europe. L’état fréquemment défectueux de la chaussée est à l’origine de nombreux chantiers sur les axes principaux. Des informations très utiles et en temps réel sont données régulièrement sur l’état du réseau autoroutier (ralentissements, accidents, travaux, itinéraires de délestage) par les radios locales et sur le site internet du <a href="http://www.bmvbs.de/" class="spip_out" rel="external">ministère allemand des transports</a></p>
<p>En théorie, et sauf indication contraire, la vitesse n’est pas limitée sur les autoroutes allemandes. Toutefois, de plus en plus de portions limitées à 80 ou à 100 km/h succèdent <strong>soudainement</strong> à des portions non limitées, entraînant souvent des carambolages à la suite de freinages brutaux des conducteurs allemands soucieux d’un strict respect de la réglementation. Cette conduite "en accordéon" peut surprendre un conducteur français habitué à une vitesse plus régulée. Il est donc recommandé en règle générale de ne pas dépasser 130 km/h sur les portions d’autoroute non limitées.</p>
<h3 class="spip"><a id="sommaire_9"></a>Transports en commun</h3>
<p>Le système des transports en commun en Allemagne est très fiable. On atteint rapidement sa destination en bus, tram et métro dans les grandes villes.</p>
<p>L’excellent réseau ferroviaire allemand offre des liaisons fréquentes entre les principales villes allemandes et avec les grandes villes des régions frontalières. Les bus sont une bonne alternative au train ou à l’avion.</p>
<p><strong>A Berlin</strong>, le système intégré de transports publics (BVG) est le meilleur moyen pour se déplacer. Un système trois zones interconnecté (ABC) qui ne requiert qu’un seul billet, vous permet de passer de l’autobus au métro (U-Bahn) au S-Bahn (RER) et au tram avec votre ticket. Les tramways sont rapides et pratiques et des petits ferries vous transporteront sur les lacs de Berlin.</p>
<p>Pour plus de renseignements : <a href="http://www.bvg.de/" class="spip_out" rel="external">www.bvg.de</a></p>
<p>Les taxis sont nombreux et on en trouve (presque) tout le temps. Il y a des têtes de taxis dans toutes les gares et les aéroports mais aussi devant le centre commercial KADEWE et les hôtels.<br class="manualbr">Prix des courses : prise en charge € 3,00 et € 1.58 par km, trajet court (Kurzstrecke) € 3,50 la course pour 2 km maximum si vous hélez le taxi dans la rue.<br class="manualbr"><i>Source : <a href="http://www.berlin.de/" class="spip_out" rel="external">http://www.berlin.de</a></i></p>
<p><i>Mise à jour : juillet 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/vie-pratique/article/transports). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
