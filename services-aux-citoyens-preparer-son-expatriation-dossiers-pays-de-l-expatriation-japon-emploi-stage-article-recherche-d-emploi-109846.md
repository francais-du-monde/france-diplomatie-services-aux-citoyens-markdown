# Recherche d’emploi

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/emploi-stage/article/recherche-d-emploi-109846#sommaire_1">Outils pour la recherche d’emploi</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/emploi-stage/article/recherche-d-emploi-109846#sommaire_2">Organismes pour la recherche d’emploi</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Outils pour la recherche d’emploi</h3>
<p>L’annuaire des sociétés françaises installées au Japon est en vente en France auprès de la <a href="http://www.ubifrance.fr/" class="spip_out" rel="external">librairie du Commerce international</a> :<br class="manualbr">77 boulevard Saint-Jacques <br class="manualbr">75014 Paris<br class="manualbr">Téléphone : 01 40 73 34 60</p>
<h4 class="spip">Journaux</h4>
<ul class="spip">
<li>Yomiuri Weekly ;</li>
<li>Metropolis, hebdomadaire gratuit (arubaito ou postes de professeurs de langues). Parution le vendredi ;</li>
<li><a href="http://job.japantimes.com/index_e.php" class="spip_out" rel="external">Tokyo Notice Board</a>, hebdomadaire gratuit, pour la recherche d’emplois d’appoint et de cours de langue.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Organismes pour la recherche d’emploi</h3>
<p>La Chambre de commerce et d’industrie française du Japon (CCIFJ) édite un annuaire qui regroupe la quasi-totalité des entreprises françaises implantées au Japon, ainsi que de nombreuses entreprises japonaises intéressées par le marché français.</p>
<p>Cet annuaire peut être consulté auprès des organismes suivants :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.ccifj.or.jp/" class="spip_out" rel="external">Chambre de commerce et d’industrie française du Japon</a> (CCIFJ)<br class="manualbr">Iida Building, 5-5 Rokubancho, Chiyoda-ku, Tokyo 102-0085<br class="manualbr">Téléphone : [81] (0)3 3288 9621 <br class="manualbr">Télécopie : [81] (0)3 3288 9558</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  La plupart des <strong>chambres de commerces et d’industrie françaises à l’étranger</strong><br class="manualbr">Leurs coordonnées sont disponibles sur le site internet de l’<a href="http://www.uccife.org/" class="spip_out" rel="external">Union des chambres de commerce et d’industrie françaises à l’étranger</a>.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Les <strong>chambres régionales de commerce et d’industrie en France</strong><br class="manualbr">Leurs coordonnées sont disponibles sur le portail Internet des Chambres de Commerce en France : <a href="http://www.cci.fr/" class="spip_out" rel="external">www.cci.fr</a></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Hello Work for foreigners</strong><br class="manualbr">Les bureaux de l’agence pour l’emploi japonaise (Hello Work) gèrent les offres et demandes d’emploi. <br class="manualbr">Les résidents au Japon peuvent s’inscrire (il faut avoir son passeport et visa de travail) auprès du <a href="http://tokyo-foreigner.jsite.mhlw.go.jp/english.html" class="spip_out" rel="external">Tokyo Employment Service Center for Foreigners</a> </p>
<ul class="spip">
<li><a href="http://www.kimiwillbe.com/" class="spip_out" rel="external">Kimi Information Center</a> (emplois et travail temporaire - arubaito) ;</li>
<li><a href="http://www.fewjapan.com/" class="spip_out" rel="external">Foreign Executive Women</a> (réservé aux femmes, propose notamment des séminaires pour les femmes qui souhaitent trouver un emploi au Japon, ou en changer) ;</li>
<li><a href="http://www.femmesactivesjapon.org/" class="spip_out" rel="external">Femmes Actives Japon</a> (FAJ) : Association réservée aux femmes, pour développer un réseau féminin francophone professionnel ;</li>
<li><strong>Japan Association for Working Holiday Makers</strong> (pour titulaires du visa vacances-travail seulement).</li></ul>
<p><i>Mise à jour : juillet 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/emploi-stage/article/recherche-d-emploi-109846). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
