# Curriculum vitae

<h4 class="spip">Rédaction</h4>
<p>Le CV et la lettre de motivation sont présentés en modèle anglo-saxons. L’entretien est organisé en anglais pour déterminer les compétences du candidat en matière de gestion et ses connaissances du marché indien. Les recruteurs indiens accordent plus d’importance au CV du candidat qu’à l’entretien en lui-même.</p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/emploi-stage/article/curriculum-vitae). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
