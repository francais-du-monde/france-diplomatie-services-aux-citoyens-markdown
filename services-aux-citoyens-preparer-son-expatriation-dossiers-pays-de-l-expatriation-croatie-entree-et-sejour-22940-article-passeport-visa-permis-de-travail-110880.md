# Passeport, visa, permis de travail

<p>Quel que soit le motif de votre séjour, renseignez-vous auprès de la <a href="http://fr.mfa.hr/?mh=636&amp;mv=4564" class="spip_out" rel="external">section consulaire de l’ambassade de Croatie à Paris</a> qui vous informera sur la règlementation en matière d’entrée et de séjour en Croatie, règlementation que vous devrez impérativement respecter.</p>
<p>Vous pouvez également trouver des informations sur le site de l’<a href="http://www.ambafrance-hr.org/-Venir-en-Croatie-" class="spip_out" rel="external">ambassade de France en Croatie</a>.</p>
<p>Le Consulat de France en Croatie n’est pas l’organisme compétent pour répondre à vos demandes d’information concernant votre séjour en Croatie.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.mup.hr/" class="spip_out" rel="external">Ministère croate de l’Intérieur</a>, dans sa version anglaise (pour y accéder, cliquer sur le drapeau anglais en bas de la page d’accueil) et notamment sa rubrique « Aliens ».</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/entree-et-sejour-22940/article/passeport-visa-permis-de-travail-110880). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
