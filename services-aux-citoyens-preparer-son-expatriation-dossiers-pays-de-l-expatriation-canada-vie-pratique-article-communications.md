# Communications

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/vie-pratique/article/communications#sommaire_1">Téléphone – Internet</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/vie-pratique/article/communications#sommaire_2">Poste</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Téléphone – Internet</h3>
<p>Les liaisons téléphoniques avec la France sont excellentes.</p>
<p>Pour téléphoner de la France, vous devez composer l’indicatif pays (1), suivi de l’indicatif de la ville.</p>
<p>Pour trouver un numéro de téléphone, vous pouvez consulter les annuaires suivants :</p>
<ul class="spip">
<li><a href="http://www.canada411.ca/" class="spip_out" rel="external">Canada411.ca</a></li>
<li><a href="http://www.pagesjaunes.ca/" class="spip_out" rel="external">www.pagesjaunes.ca</a></li></ul>
<p>Pendant longtemps, Bell Canada a détenu le monopole sur les communications téléphoniques. Il existe aujourd’hui de nombreux autres opérateurs : Telus, WinTel, Sprint, Rogers, pour ne citer que les principaux. Concurrence oblige, leurs tarifs sont souvent plus avantageux que ceux de Bell Canada. Attention cependant : comme en Europe, la téléphonie recèle parfois des surprises et des coûts cachés. Certains distributeurs sont intéressants pour les communications Outre-Mer, d’autres le sont pour l’Amérique du Nord, voire pour le seul Québec. D’autres fournisseurs vous proposent des prix défiant toute concurrence lorsque vous appelez toujours le même numéro. Certains offrent un forfait valable pour un nombre illimité d’appels ou uniquement le week-end.</p>
<p>Bien entendu, tous ces tarifs évoluent selon les heures d’appel et les services annexes auxquels vous vous abonnez. Afin d’éviter une double facturation, il peut être utile de choisir un distributeur qui sera en même temps votre fournisseur Internet.</p>
<p>Le marché de la téléphonie mobile est encore plus concurrentiel que celui de la téléphonie fixe. Les principales entreprises sont Fido, Bell Mobilité, Telus et Rogers. Les Québécois utilisent souvent des cartes prépayées pour téléphoner à l’étranger.</p>
<p>La plupart des appartements sont raccordés au réseau téléphonique. Si ce n’est pas le cas, votre fournisseur de service téléphonique vous indiquera qui est responsable du câblage. A noter que l’installation d’une ligne téléphonique est aux frais de l’abonné.</p>
<p>Le site Internet du <a href="http://www.crtc.gc.ca/" class="spip_out" rel="external">Conseil de la radiodiffusion et des télécommunications canadiennes (CRTC)</a> fournit des informations pratiques pour vous aider à choisir une compagnie de téléphone. En cas de problème, vous pouvez contacter :</p>
<p><strong>Le Commissaire aux plaintes relatives aux services de télécommunications (CPRST)</strong><br class="manualbr">Case postale 81088 <br class="manualbr">Ottawa (Ontario) K1P 1B1<br class="manualbr">Numéro gratuit : 1 888 221 16 87 <br class="manualbr">Ligne ATS gratuite : 711 ou 1 800 855 0511 (serveur vocal)<br class="manualbr">Télécopie gratuite : 1 877 782 2924 <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/vie-pratique/article/communications#info#mc#ccts-cprst.ca#" title="info..åt..ccts-cprst.ca" onclick="location.href=mc_lancerlien('info','ccts-cprst.ca'); return false;" class="spip_mail">Courriel</a></p>
<p>Téléphoner gratuitement par Internet Il est généralement très avantageux d’utiliser <strong>des logiciels de téléphonie sur IP</strong> (Skype, Google talk, Live messenger, Yahoo ! messenger, etc.) car les tarifs proposés à l’international sont souvent moindre que ceux des opérateurs locaux exploitant des lignes classiques, voire gratuits dans certaines conditions.</p>
<h3 class="spip"><a id="sommaire_2"></a>Poste</h3>
<p>Les liaisons postales fonctionnent bien. L’acheminement d’une lettre par avion prend en moyenne trois à cinq jours entre la France et le Canada. Il faut compter près d’un mois pour un colis par bateau.<br class="autobr">Au Canada, la distribution du courrier s’effectue du lundi au vendredi. Le courrier est également relevé le samedi. Certains bureaux de poste sont ouverts le samedi. En 2013, l’envoi d’une lettre à l’intérieur du Canada coûtait de 0,63$ à 1,10$ et celui d’une lettre vers la France 1,80$. L’envoi d’un petit colis en recommandé à destination de l’Europe par messagerie coûte une cinquantaine de dollars.</p>
<p>Pour plus d’informations, vous pouvez contacter les informations générales de Postes Canada (1 888 550 63 33) ou <a href="http://www.postescanada.ca/cpo/mc/languageswitcher.jsf" class="spip_out" rel="external">consulter leur site Internet</a>. Vous y trouverez tous les tarifs postaux, les adresses des bureaux de poste, ainsi que tous les codes postaux.</p>
<p><i>Mise à jour : avril 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/vie-pratique/article/communications). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
