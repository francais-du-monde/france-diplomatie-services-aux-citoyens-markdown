# Convention fiscale

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/fiscalite/article/convention-fiscale-112492#sommaire_1">Champ d’application de la convention et de l’entente franco-québécoise</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/fiscalite/article/convention-fiscale-112492#sommaire_2">Règles d’imposition</a></li></ul>
<p>La France et la Canada ont signé à Paris <strong>le 2 mai 1975</strong> une convention en vue d’éviter les doubles impositions et de prévenir l’évasion fiscale en matière d’impôts sur le revenu et sur la fortune. Cette convention est <strong>entrée en vigueur le 29 juillet 1976. </strong></p>
<p>Elle a été modifiée par deux avenants :</p>
<ul class="spip">
<li><strong>un premier avenant </strong>signé à Ottawa le 16 janvier 1987, <strong>entré en vigueur le 1er octobre 1988 </strong>et publié au Journal officiel du 13 octobre 1988 (décret n°88-967 du 11 octobre 1988) ;</li>
<li><strong>un deuxième avenant </strong>signé à Ottawa le 30 novembre 1995, <strong>entré en vigueur le 1er septembre 1998 </strong>et publié au Journal officiel du 16 septembre 1998 (décret n°98-823 du 9 septembre 1998).</li></ul>
<p>Par ailleurs, la France et le gouvernement de Québec ont signé le 1er septembre 1987 à Québec une <strong>entente fiscale </strong>en vue d’éviter les doubles impositions et de prévenir l’évasion fiscale en matière d’impôts sur le revenu et sur la fortune. Cette entente a été publiée au Journal officiel du 29 octobre 1988 (décret n°88-1008 du 25 octobre 1988) et <strong>est entrée en vigueur le 19 septembre 1988</strong>. Elle a été modifiée par un avenant signé à Paris le 3 septembre 2002 qui est entré en vigueur le 1er août 2005 (décret n°2005-1080 du 23 août 2005 publié au Journal officiel du 1er septembre 2005).</p>
<p>Vous pouvez obtenir des informations précises sur son application :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  auprès du <strong>Centre des impôts des non-résidents</strong><br class="manualbr">TSA 10010 - 10 rue du Centre <br class="manualbr">93465 Noisy le Grand cedex<br class="manualbr">Téléphone : 01 57 33 83 00 <br class="manualbr">Télécopie : 01 57 33 83 50<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/fiscalite/article/convention-fiscale-112492#sip.nonresidents#mc#dgfpi.finances.gouv.fr#" title="sip.nonresidents..åt..dgfpi.finances.gouv.fr" onclick="location.href=mc_lancerlien('sip.nonresidents','dgfpi.finances.gouv.fr'); return false;" class="spip_mail">Courriel</a></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  auprès de l’<a href="http://fr.ambafrance-us.org/" class="spip_out" rel="external">Attaché fiscal pour le Canada</a> en résidence à Washington<br class="manualbr">4101 Reservoir Road <br class="manualbr">NW Washington DC 20007<br class="manualbr">Téléphone : (202) 944-6391 <br class="manualbr">Télécopie : (202) 944-6373<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/fiscalite/article/convention-fiscale-112492#impots#mc#missioneco.org#" title="impots..åt..missioneco.org" onclick="location.href=mc_lancerlien('impots','missioneco.org'); return false;" class="spip_mail">Courriel</a></p>
<p>Son texte intégral est consultable sur le <a href="http://www.impots.gouv.fr/" class="spip_out" rel="external">site Internet de l’administration fiscale</a>. Voici un résumé de ses points clés pour les particuliers :</p>
<h3 class="spip"><a id="sommaire_1"></a>Champ d’application de la convention et de l’entente franco-québécoise</h3>
<p><strong>Personnes et impôts visés (articles 1 et 2)</strong></p>
<p>L’entente s’applique aux personnes qui ont le statut de résident en France ou au Canada ou dans les deux territoires à la fois (article 1). L’article 2 précise, pour la France et le Québec, les impôts qui sont visés par l’entente.</p>
<p><strong>Domicile fiscal (article 4)</strong></p>
<p>L’article 4, paragraphe 1, précise que l’expression " résident d’une Partie contractante " désigne toute personne qui, en raison de son domicile, de sa résidence, de son siège de direction ou de tout autre critère de nature analogue, est assujettie à l’impôt dans cette Partie.</p>
<p>Le paragraphe 2 de l’article 4 énumère les critères subsidiaires permettant de résoudre les cas de double résidence si le critère de l’assujettissement à l’impôt n’est pas suffisant. Ces critères sont appliqués dans l’ordre suivant :</p>
<ul class="spip">
<li>le foyer d’habitation permanent ;</li>
<li>à défaut, le lieu où se trouve le centre des intérêts vitaux (le territoire où les liens personnels et économiques sont les plus étroits) ;</li>
<li>à défaut, le lieu de séjour habituel ;</li>
<li>à défaut, l’Etat dont la personne possède la nationalité.</li></ul>
<p>Si une personne possède à la fois la nationalité française et la nationalité canadienne ou si elle ne possède aucune de ces nationalités, la question est tranchée d’un commun d’accord par les autorités des deux Parties contractantes.</p>
<h3 class="spip"><a id="sommaire_2"></a>Règles d’imposition</h3>
<p><strong>Revenus immobiliers (article 6)</strong></p>
<p>Ces revenus sont imposables dans la Partie contractante où les biens sont situés. Cette règle s’applique également aux revenus provenant de l’exploitation et de la cession de ces biens.</p>
<p><strong>Bénéfices des entreprises (article 7)</strong></p>
<p>Aux termes de l’article 7 de l’entente, les entreprises industrielles et commerciales ne sont imposables que dans la Partie sur le territoire de laquelle se trouve un établissement stable dont la notion est déterminée à l’article 5.</p>
<p><strong>Dividendes (article 10)</strong></p>
<p>Les dividendes payés par une société qui est résidente d’une Partie contractante à une personne résidente dans l’autre Partie contractante sont imposables dans la Partie où le bénéficiaire a sa résidence.</p>
<p>L’article 10 de l’entente ne vise pas les dividendes de source québécoise car seul l’Etat canadien peut imposer à la source ces revenus. Les dividendes de source française sont également imposables en France. Cependant, si la personne physique qui perçoit ces dividendes en est le bénéficiaire effectif, la France les impose à un taux maximum de 15% du montant brut des dividendes.</p>
<p>Un paiement du Trésor français d’un montant égal au crédit d’impôt (" avoir fiscal ") peut, sous conditions, être accordé à la personne résidente au Canada qui reçoit d’une société résidente en France des dividendes dont elle est le bénéficiaire effectif et qui donneraient droit en France à un crédit d’impôt si le bénéficiaire avait été résident en France. Cette disposition ne s’applique que si la personne concernée est assujettie à l’impôt canadien à raison de ces dividendes et du paiement du Trésor français.</p>
<p><strong>Les intérêts (article 11)</strong></p>
<p>Les intérêts provenant d’une Partie contractante et payés à une personne résidente dans l’autre Partie contractante sont imposables uniquement dans le territoire où le bénéficiaire a sa résidence.</p>
<p>Ces intérêts peuvent être, sous certaines conditions, également imposables dans l’Etat d’où ils proviennent. Dans ce cas, si la personne qui reçoit les intérêts en est</p>
<p>le bénéficiaire effectif, l’impôt ne peut excéder 10 % du montant brut des intérêts. Les paragraphes 3, 4 et 6 de cet article énumèrent un certain nombre de cas dans lesquels les intérêts ne sont imposables que dans l’Etat de résidence du bénéficiaire effectif.</p>
<p>Les intérêts de source québécoise ne sont pas visés par à l’article 11 de l’entente. Seul l’Etat canadien peut les imposer.</p>
<p>Les intérêts de source française versés à un résident du Québec sont également imposables en France à un taux maximum de 10% du montant brut des intérêts. Les paragraphes 3 et 4 énumèrent les intérêts qui échappent à cette règle. Le Québec les impose également tout en accordant un crédit d’impôt. Ces intérêts sont également imposables par l’Etat canadien.</p>
<p><strong>Redevances (article 12)</strong></p>
<p>Les redevances provenant d’un Etat contractant et payées à une personne résidente de l’autre Etat contractant sont imposables uniquement dans le territoire où le bénéficiaire a sa résidence.</p>
<p>Les redevances de source québécoise ne sont pas visées par l’entente et il revient à l’Etat canadien de les imposer.</p>
<p>Les redevances de source française dont le bénéficiaire effectif est résident au Canada sont également imposables en France à un taux ne pouvant dépasser 10% du montant brut des redevances. Il existe des exceptions à cette règle qui sont énumérées aux paragraphes 3 et 4. Le Québec impose les mêmes redevances et accorde un crédit d’impôt égal à l’impôt prélevé à la source selon les dispositions de l’article 22 paragraphe 2 a) de l’entente. Ces redevances restent également imposables dans l’Etat canadien.</p>
<p>Il existe toutefois une exception à cette règle, notamment en ce qui concerne les redevances de droits d’auteur portant sur la production ou la reproduction d’une oeuvre littéraire, dramatique ou artistique qui sont uniquement imposables au Québec selon les dispositions de l’article 12, paragraphe 3 a) de l’entente.</p>
<p>Ne sont toutefois pas visées par l’exonération de l’impôt français les redevances relatives aux films et oeuvres enregistrées sur film, bande magnétoscopique ou autre moyen de reproduction destiné à la télédiffusion.</p>
<p><strong>Gains en capital (article 13)</strong></p>
<p>Les gains provenant de la cession de biens, tels que définis aux paragraphes 1 à 3 de cet article, sont imposables dans la Partie contractante où sont situés ces biens. Les autres biens ne sont imposables que dans la Partie sur le territoire de laquelle réside la personne qui cède ces biens. Cependant, les gains réalisés lors de la cession de ces biens peuvent, sous certaines conditions énumérées au paragraphe 5, être également imposables dans l’autre Partie contractante.</p>
<p><strong>Professions libérales (article 14)</strong></p>
<p>Les revenus provenant de l’exercice d’une profession indépendante ou libérale sont imposables uniquement dans la Partie sur le territoire de laquelle le travailleur est résident.</p>
<p>Si le résident d’une Partie contractante possède de façon habituelle sur le territoire de l’autre Partie contractante une base fixe pour l’exercice de son activité, les revenus imputables à cette base fixe sont imposables dans l’autre Partie contractante où est située cette base fixe.</p>
<p><strong>Salariés (article 15)</strong></p>
<p>Les salaires, traitements et autres rémunérations reçus au titre d’un emploi salarié par le résident d’un Etat contractant ne sont imposables que dans l’Etat où est exercée l’activité salariée.</p>
<p>Cependant, les rémunérations perçues par le résident d’une Partie contractante au titre d’un emploi salarié exercé dans l’autre Partie contractante ne sont imposables que dans la Partie où réside le salarié si les trois conditions suivantes sont simultanément réunies :</p>
<p>l- e séjour temporaire du salarié dans l’autre Partie contractante n’excède pas au total 183 jours au cours de toute période de douze mois ;</p>
<ul class="spip">
<li>les rémunérations sont payées par un employeur ou pour le compte d’un employeur qui n’est pas un résident de la Partie sur le territoire de laquelle est exercée l’activité salariée ;</li>
<li>la charge des rémunérations n’est pas supportée par un établissement stable ou une base fixe que l’employeur a dans la Partie sur le territoire de laquelle est exercée l’activité salariée.</li></ul>
<p>Les revenus professionnels des salariés exerçant à bord d’un navire ou d’un aéronef en trafic international ne sont imposables que dans la Partie où est située la direction effective de l’entreprise.</p>
<p><strong>Artistes et sportifs (article 17)</strong></p>
<p>Les revenus qu’un artiste du spectacle ou un sportif résident d’une Partie contractante tire de ses activités personnelles exercées dans l’autre Partie contractante en tant qu’artiste du spectacle sont imposables dans la Partie où sont exercées ces activités.</p>
<p>Cette disposition ne s’applique pas lorsque l’activité exercée dans l’autre Partie est principalement financée par des fonds publics provenant de la Partie dont la personne est résidente. Dans ce cas, les revenus tirés de cette activité sont imposables dans la Partie qui la finance sur fonds publics.</p>
<p><strong>Pensions et rentes (article 18)</strong></p>
<p>Les pensions et rentes provenant d’un Etat contractant et versées à une personne résidente de l’autre Etat contractant ne sont imposables que dans l’Etat de provenance de la pension ou de la rente.</p>
<p>Des dispositions particulières s’appliquent aux pensions et allocations de guerre, lesquelles sont susceptibles d’être exonérées d’impôt. Les pensions alimentaires ne sont imposables que dans la Partie où le bénéficiaire a sa résidence.</p>
<p><strong>Rémunérations publiques (article 19)</strong></p>
<p>Les salaires et traitements, <strong>autres que les pensions, </strong>payés par un Etat (ou une des provinces pour le Canada) ou par l’une de leurs personnes morales de droit public à une personne possédant la nationalité de cet Etat au titre de services rendus à cet Etat sont seulement imposables dans cet Etat.</p>
<p>A titre d’exemple, les rémunérations publiques versées par la France à une personne de nationalité canadienne résidente du Québec, au titre de services rendus au Québec, ne sont imposables que dans cette province si le bénéficiaire n’est pas devenu résident du Québec à seule fin de rendre les services.</p>
<p>Ces dispositions ne s’appliquent pas aux rémunérations payées au titre de services rendus dans le cadre d’une activité commerciale ou industrielle exercée par une Partie contractante.</p>
<p><strong>Etudiants, apprentis et stagiaires (article 20)</strong></p>
<p>Les sommes perçues (y compris les bourses d’études) pour couvrir les frais d’entretien, d’études ou de formation ne sont pas imposables dans la Partie contractante sur le territoire de laquelle est effectué le séjour si les conditions suivantes sont remplies :</p>
<ul class="spip">
<li>la personne doit être ou avoir été résidente de l’autre Partie contractante immédiatement avant de venir séjourner dans la Partie où elle poursuit ses études ou sa formation ;</li>
<li>le but du séjour doit être uniquement de poursuivre des études ou une formation ;</li>
<li>les sommes perçues doivent provenir de sources situées en dehors du territoire de séjour.</li></ul>
<p><strong>Imposition de la fortune (article 21 A)</strong></p>
<p>La fortune constituée par des biens immobiliers que possède un résident d’une Partie contractante et qui sont situés dans l’autre Partie contractante est imposable dans la Partie où sont situés ces biens.</p>
<p>La fortune constituée par des actions, parts ou autres droits dans une société dont l’actif est principalement composé de biens immobiliers est imposable dans la</p>
<p>Partie où sont situés ces biens.</p>
<p>La fortune constituée par des actions, parts ou autres droits (autres que ceux mentionnés ci-dessus) faisant partie d’une participation substantielle dans une société est imposable dans la Partie où cette société est résidente.</p>
<p>La fortune constituée par des biens mobiliers qui font partie de l’actif d’un établissement stable d’une entreprise ou qui appartiennent à une base fixe pour l’exercice d’une profession libérale est imposable dans la partie où est située cette entreprise ou cette base fixe.</p>
<p>Tous les autres éléments de fortune d’un résident d’une Partie contractante ne sont imposables que dans la Partie de résidence.</p>
<p>Le paragraphe 7 prévoit qu’un citoyen canadien qui n’a pas la nationalité française et qui devient résident de France n’est pas soumis à l’impôt de solidarité sur la fortune à raison des biens situés hors de France qu’il possède au 1er janvier des cinq premières années suivant l’année civile au cours de laquelle il est devenu résident de France.</p>
<p><strong>Cotisations de retraite (article 28)</strong></p>
<p>Le paragraphe 5 de l’article 28 prévoit que les personnes physiques résidentes de France et expatriées au Québec sont autorisées à déduire au Québec, pendant une durée de 60 mois, les cotisations à une caisse de retraite ou à un fonds de pension français. Cette disposition s’applique également aux résidents du Québec qui s’expatrient en France.</p>
<p><strong>Autres revenus (article 21-A)</strong></p>
<p>Les autres revenus perçus par une personne résidente d’un Etat contractant qui ne sont pas mentionnés dans cette convention sont imposables uniquement dans l’Etat de résidence de cette personne. Toutefois, si ces revenus proviennent de sources situées dans l’autre Etat contractant, ils peuvent être imposés dans l’Etat de provenance.</p>
<p><strong>Elimination de la double imposition (article 23)</strong></p>
<p>Cet article expose les modalités permettant d’éliminer la double imposition :</p>
<p>En ce qui concerne le Canada et sous réserve de certaines dispositions, l’impôt français dû conformément à la législation française et à la présente convention à raison de bénéfices, revenus ou gains provenant de France est déduit de tout impôt canadien dû à raison des mêmes bénéfices, revenus ou gains.</p>
<p>Lorsqu’un résident du Canada reçoit un revenu ou possède de la fortune exempt d’impôts au Canada, cet Etat peut néanmoins en tenir compte pour calculer l’impôt dû sur les autres éléments de revenu ou de fortune.</p>
<p>En ce qui concerne la France, les revenus qui proviennent du Canada (et qui sont imposables ou ne sont imposables qu’au Canada conformément aux dispositions de la convention) sont pris en compte pour le calcul de l’impôt français. Dans ce cas, le bénéficiaire résident en France a droit à un crédit d’impôt correspondant aux revenus canadiens, imputable sur l’impôt français.</p>
<p>La fortune possédée au Canada par un résident de France est imposable au Canada et en France. L’impôt français est calculé sous déduction d’un crédit d’impôt égal au montant de l’impôt canadien acquitté sur cette fortune.</p>
<p><i>Mise à jour : avril 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/fiscalite/article/convention-fiscale-112492). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
