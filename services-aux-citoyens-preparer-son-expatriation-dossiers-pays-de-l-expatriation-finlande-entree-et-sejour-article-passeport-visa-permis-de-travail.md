# Passeport, visa, permis de travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/entree-et-sejour/article/passeport-visa-permis-de-travail#sommaire_1">Passeport, visa, permis de travail</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Passeport, visa, permis de travail</h3>
<p>Quel que soit le motif de votre séjour, renseignez-vous auprès de la section consulaire de l’ambassade de Finlande à Paris qui vous informera sur la règlementation en matière d’entrée et de séjour en Finlande, règlementation que vous devrez impérativement respecter. N’hésitez pas à consulter le site du ministère de l’Intérieur de la Finlande afin de vous familiariser avec les modalités d’entrée et de séjour dans le pays.</p>
<p>Une fois sur place c’est la Finlande qui prend le relais. Le consulat de France en Finlande n’est pas l’organisme compétent pour répondre à vos demandes d’information concernant votre séjour en Finlande.</p>
<p>Pour toutes informations sur les conditions de séjour, il est recommandé de prendre l’attache de l’<a href="http://www.finlande.fr/Public/Default.aspx" class="spip_out" rel="external">ambassade de Finlande à Paris</a>. <strong>Pour être autorisé à travailler en Finlande, il vous faudra effectuer des démarches auprès des autorités locales.</strong></p>
<p><strong>Séjours de moins de trois mois</strong></p>
<p>Pour les courts séjours, aucune formalité n’est nécessaire. Conformément aux accords de Schengen, les ressortissants européens peuvent librement circuler et travailler en Finlande. La carte nationale d’identité suffit pour se rendre en Finlande, mais le passeport est conseillé car plus reconnu par les banques et les commerçants.</p>
<p><strong>Séjours de plus de trois mois</strong></p>
<p>Après l’expiration de la période de trois mois, les salariés ou entrepreneurs ressortissants de l’UE doivent s’inscrire au « registre des ressortissants de l´UE » auprès du commissariat de police de leur lieu de résidence. Pour toute la ville d’Helsinki, le <a href="http://www.poliisi.fi/" class="spip_out" rel="external">commissariat compétent est celui de Malmi</a>.</p>
<p>Munis d’une demande générale de résidence et de leurs papiers d’identité, d’un certificat de mariage et d’études (si besoin) et d’une photo, les ressortissants européens déclarent leur situation au commissariat. L’inscription est personnelle et coûte 50 € par personne. En outre, il n’y a pas de limitation de durée et l’inscription permet d’obtenir une carte de résident. Dans le cas d’immigration pour raisons professionnelles, le contrat de travail ainsi qu’un permis de séjour pour les travailleurs (formulaire spécifique) sont nécessaires. Pour une famille, les actes d’état civil à présenter (pour faire reconnaître un mariage ou une filiation) doivent avoir reçu l’apostille (formalité gratuite à faire, y compris par correspondance, auprès de la cour d’appel compétente en France). Chaque membre de la famille doit effectuer une déclaration individuelle.</p>
<p>Formulaires : <a href="http://www.migri.fi/" class="spip_out" rel="external">Migri.fi</a></p>
<ul class="spip">
<li>Pour les séjours supérieurs à trois mois mais inférieurs à un an, il n’est pas nécessaire de s’enregistrer au registre de la population (état civil). <strong>Néanmoins, l’inscription permet la délivrance d’un numéro d’identité finlandais qui est obligatoire pour ouvrir un compte bancaire ou percevoir un salaire</strong>. Par ailleurs, ce numéro peut également faciliter certaines démarches, telles l’ouverture d’une ligne téléphonique, l’achat d’une carte de transport, …</li>
<li><strong>Pour un séjour au-delà d’un an, l’inscription au Maistraatti est obligatoire</strong>. Le candidat à l’immigration en Finlande se présente au registre d’état civil avec ses papiers d’identité, un permis de séjour valide (ainsi qu’un certificat de mariage et les certificats de naissance des enfants si besoin), et remplit le formulaire « Enregistrement d’information d’un étranger ». Il procède aussi à une déclaration officielle d’immigration et se voit délivrer un numéro d’identité finlandais dans un délai d’une semaine. Il peut être considéré comme résident en Finlande au terme de quatre années d’inscription continue au registre de la population. Les autorités finlandaises lui délivrent alors une carte de séjour permanente.</li></ul>
<p>Il n’y aucune démarche particulière à accomplir pour pouvoir être autorisé à travailler en Finlande pour une mission de courte durée (moins de trois mois) en raison du principe de libre circulation dans l’Espace économique européen. Il suffit d’être muni d’un passeport en cours de validité ou d’une carte nationale d’identité.</p>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-documents-de-voyage-article-visas.md" class="spip_in">Visas</a></li>
<li><a href="http://www.migri.fi/" class="spip_out" rel="external">Migri.fi</a></li>
<li><a href="http://www.infopankki.fi/fr/page-accueil" class="spip_out" rel="external">Infopankki.fi</a></li>
<li><a href="http://formin.finland.fi/Public/default.aspx" class="spip_out" rel="external">Section consulaire de l’ambassade de Finlande à Paris</a></li></ul>
<p><i>Mise à jour : janvier 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/entree-et-sejour/article/passeport-visa-permis-de-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
