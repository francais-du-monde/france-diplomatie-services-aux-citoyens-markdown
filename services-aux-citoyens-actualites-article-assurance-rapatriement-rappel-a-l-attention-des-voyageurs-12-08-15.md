# Assurance-rapatriement : rappel à l’attention des voyageurs (12 août 2015)

<p>Le ministère des affaires étrangères et du développement international rappelle qu’<strong>il relève de la responsabilité individuelle de chacun de prévoir une assurance-rapatriement avant tout déplacement à l’étranger.</strong></p>
<p>Dans des situations d’urgence où un risque vital est avéré pour l’un de nos compatriotes, le ministère des affaires étrangères peut, malgré l’absence d’assurance, conduire à titre exceptionnel le rapatriement. Une dizaine d’évacuations sanitaires par affrètement d’urgence d’un avion médicalisé sont ainsi organisées chaque année.</p>
<p>Nos compatriotes et leurs familles sont informés en amont des modalités budgétaires de ces opérations, qui donnent lieu à une avance par le ministère des affaires étrangères et à un engagement de remboursement de la dette ainsi contractée envers l’Etat. Ils ont la possibilité de solliciter auprès de l’administration fiscale le rééchelonnement des remboursements, voire une remise gracieuse, partielle ou totale, de la dette.</p>
<p>Le recouvrement des frais permet de préserver notre capacité à venir en aide, dans le futur, à nos compatriotes en difficulté.</p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/actualites/article/assurance-rapatriement-rappel-a-l-attention-des-voyageurs-12-08-15). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
