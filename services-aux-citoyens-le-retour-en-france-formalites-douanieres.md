# Formalités douanières

<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-le-retour-en-france-formalites-douanieres-article-le-demenagement.md" title="Le déménagement">Le déménagement</a></li>
<li><a href="services-aux-citoyens-le-retour-en-france-formalites-douanieres-article-animaux-domestiques-retour-en-france.md" title="Animaux domestiques - Retour en France ">Animaux domestiques - Retour en France </a></li>
<li><a href="services-aux-citoyens-le-retour-en-france-formalites-douanieres-article-l-importation-en-france-d-un.md" title="L’importation en France d’un véhicule">L’importation en France d’un véhicule</a></li>
<li><a href="services-aux-citoyens-le-retour-en-france-formalites-douanieres-article-le-transfert-de-moyens-de-paiement.md" title="Le transfert de moyens de paiement">Le transfert de moyens de paiement</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/formalites-douanieres/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
