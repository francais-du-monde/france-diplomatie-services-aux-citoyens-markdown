# Convention fiscale

<p>Une convention fiscale ayant primauté sur la loi interne, les dispositions de la loi du 29 décembre 1976 modifiant les règles de territorialité et les conditions d’imposition des Français de l’étranger (J.O. du 30 décembre 1976) ne sont applicables que si elles ne contreviennent pas aux dispositions de la convention.</p>
<p>Une convention fiscale tendant à éviter les doubles impositions et à prévenir l’évasion fiscale en matière d’impôts sur le revenu a été signée le 7 novembre 1973 entre le gouvernement de la République française et l’Iran. Elle a été publiée, après ratification, au Journal Officiel du 30 avril 1975.</p>
<p>Le texte de la Convention peut être obtenu auprès de la Direction des Journaux Officiels par courrier (26 rue Desaix, 75727 Paris Cedex 15), par Fax (01 40 58 77 80), par minitel 3616 JOURNAL OFFICIEL, ou sur le site Internet du <a href="http://www.impot.gouv.fr/portal/deploiement/p1/fichedescriptive_1881/fichedescriptive_1881.pdf" class="spip_out" rel="external">ministère des Finances</a>.</p>
<h4 class="spip">Champ d’application de la convention</h4>
<p>La convention s’applique aux impôts sur le revenu (y compris les impôts sur les revenus des biens immobiliers) des résidents français ou iraniens. En Iran il s’agit de l’impôt sur le revenu ; en France il s’agit de l’impôt sur le revenu des personnes physiques et de l’impôt sur les sociétés.</p>
<h4 class="spip">Règles d’imposition</h4>
<p>Le salaire de base imposable des expatriés est déterminé par les autorités en fonction d’un barème (nationalité et fonctions) ; ce barème, en dollars des Etats-Unis, est ensuite transcrit au taux officiel (qui surévalue la valeur du rial, donc sous-évalue la valeur des salaires en devises). Le taux d’imposition se situe entre 0 (salaires mensuels inférieurs à 8,3 millions de rials soit 250 € environ début 2014) et 35% (fraction des salaires dépassant 91 millions de rials soit 2700 € environ).</p>
<p>Les salaires des expatriés étant en général payés à l’étranger, il n’y a pas de retenue à la source comme pour les contribuables iraniens : c’est l’employeur qui paye directement la totalité de l’impôt.</p>
<p><i>Mise à jour : février 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/fiscalite/article/convention-fiscale). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
