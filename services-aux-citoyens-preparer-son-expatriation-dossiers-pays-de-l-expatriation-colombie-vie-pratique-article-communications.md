# Communications

<p><strong>Téléphone - Internet </strong></p>
<p>Les services téléphoniques sont dans l’ensemble de bonne qualité. Le marché des opérateurs s’est ouvert aux opérateurs privés qui offrent toutes sortes de services pour les communications nationales et internationales.</p>
<p>Pour les appels de poste fixe à poste fixe :</p>
<ul class="spip">
<li>Appels de France vers la Colombie : 00-57 + code de la zone + numéro de votre correspondant.</li>
<li>Appels de Colombie vers la France : 007-33 + numéro de votre correspondant sans le zéro.</li>
<li>Appels depuis la Colombie vers un autre département colombien : 09 + code de la zone + numéro de votre correspondant.</li></ul>
<p>Pour les appels de poste fixe vers un portable en Colombie :</p>
<ul class="spip">
<li>depuis la France : 00-57 + le numéro de votre correspondant</li>
<li>depuis la Colombie : 03 + le numéro de votre correspondant.</li></ul>
<p>Internet est relativement répandu en Colombie. L’accès au réseau par un fournisseur colombien ne pose aucun problème. Plusieurs fournisseurs sont présents sur le marché, notamment Telmex, ETB et Telefonica.</p>
<p>Dans les grandes villes, on trouve facilement des points de connexion Internet pour un tarif d’environ 2 € de l’heure, et les hôtels proposent souvent une connexion Internet. On trouve aussi des cybercafés dans toutes les grandes villes et les villes de taille moyenne.</p>
<p><strong>Téléphoner gratuitement par Internet </strong></p>
<p>Il est généralement très avantageux d’utiliser <strong>des logiciels de téléphonie sur IP</strong> (Skype, Google talk, Live messenger, Yahoo ! messenger, etc.) car les tarifs proposés à l’international sont souvent moindre que ceux des opérateurs locaux exploitant des lignes classiques.</p>
<p><strong>Poste </strong></p>
<p>Il faut, en théorie, compter une dizaine de jours d’acheminement pour le courrier à destination ou en provenance de France. Il est conseillé d’avoir recours à la poste privée.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/vie-pratique/article/communications). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
