# Lettre de motivation

<p>Le site <a href="http://europass.cedefop.europa.eu/fr/home" class="spip_out" rel="external">Europass</a> met à votre disposition des modèles en ligne de documents pour exprimer clairement vos compétences et qualifications : CV ; passeport de compétences ; passeport de langue ; lettre de motivation.</p>
<ul class="spip">
<li><a href="https://europass.cedefop.europa.eu/editors/fr/esp/compose" class="spip_out" rel="external">Modèles de rédaction en français</a></li>
<li><a href="https://europass.cedefop.europa.eu/editors/hu/esp/compose" class="spip_out" rel="external">Modèles de rédaction en hongrois</a></li></ul>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/emploi-stage/article/lettre-de-motivation-113471). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
