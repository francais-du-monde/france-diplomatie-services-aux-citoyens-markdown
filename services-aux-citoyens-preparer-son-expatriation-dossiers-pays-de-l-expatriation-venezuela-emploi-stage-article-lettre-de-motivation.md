# Lettre de motivation

<p class="chapo">
      Qu’il s’agisse d’une réponse à une annonce ou d’une candidature spontanée, vous ne pourrez pas éviter l’exercice difficile et délicat de la lettre de motivation qui se doit d’accompagner systématiquement votre Curriculum Vitae. Les candidatures sans lettre de motivation sont généralement écartées d’office. La lettre de motivation suit globalement les mêmes règles qu’en France.
</p>
<h4 class="spip">Conseils de rédaction</h4>
<p>Personnelle, soignée, percutante et pertinente, la lettre de motivation est votre première chance d’inciter le recruteur à vous rencontrer et/ou à prendre connaissance de votre CV. La lettre de motivation n’est pas une simple lettre d’accompagnement, elle doit inscrire votre candidature dans une démarche professionnelle cohérente et être unique et ciblée pour l’offre et l’entreprise que vous visez.</p>
<p>Au Venezuela la <strong>carta de presentación</strong> correspond à une lettre de candidature spontanée, tandis que lettre de motivation en réponse à une offre sera appelée <strong>carta de acompañamiento en respuesta a un aviso</strong>. Elle peut être rédigée sur ordinateur sauf si précisée manuscrite dans l’offre d’emploi et envoyée accompagnée du CV par courriel.</p>
<h4 class="spip">Elle doit contenir trois paragraphes concentrés sur une page</h4>
<ul class="spip">
<li>Introduction (indiquer l’objet de la lettre, la référence de l’offre d’emploi, le niveau de connaissance de l’entreprise et/ou du service…) ;</li>
<li>Objectif (donner une réponse détaillée à chaque point du profil du poste illustrée par votre propre expérience et niveau de responsabilité correspondant) ;</li>
<li>Prétention salariale (si l’annonce le précise) ;</li>
<li>Enfin, indiquer le meilleur moyen de vous contacter (téléphone, e-mail).</li></ul>
<h4 class="spip">Erreur fréquente</h4>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Cher/ère = <i>Estimada/o</i> et non <i>Querida/o</i><br class="manualbr">Ex. : "Estimado Dr. Ricardo Perez, …"</p>
<h4 class="spip">Formule de politesse en fin de lettre</h4>
<ul class="spip">
<li>"Sin otro particular y a la espera de una respuesta favorable, saludos a usted,<br>Atentamente,"</li>
<li>"A la espera de sus novedades, lo/la saludo atentamente. Cordialmente."</li></ul>
<h4 class="spip">Références</h4>
<p>Les références ne doivent être fournies que sur demande (vous pouvez indiquer dans la lettre de motivation ou sur le CV que vous les tenez à disposition).</p>
<p>Les références personnelles extraprofessionnelles sérieuses sont utiles et perçues comme un plus. Elles constituent une sorte de clé qui permettra à votre interlocuteur de mieux vous situer. C’est à vous d’en faire connaître l’existence (maximum trois personnes qui vous connaissent, à condition qu’elles ne soient pas un membre de votre famille ni un ancien employeur). Indiquer leur téléphone, leur activité et depuis combien de temps vous les connaissez.</p>
<h4 class="spip">Modèles de lettres</h4>
<h4 class="spip">Exemple 1</h4>
<p>Estimados Señores,</p>
<p>Gracias a la oportunidad que me brindan con esta beca, quisiera hacer unas prácticas en el sector de ……….en (país de destino). Soy estudiante de …………. curso de …(TITULACIÓN)………….. Como ustedes pueden ver en mi Currículum Vitae, he realizado anteriormente unas prácticas en…… Actualmente asisto a cursos de mi especialidad en ………… Además, trabajo actualmente en …………como ………………</p>
<p>A continuación les quiero ofrecer a ustedes una impresión de mi persona y mis motivos para querer realizar unas prácticas en su empresa.</p>
<p>Como ya he dicho, quisiera hacer unas prácticas en el sector de marketing en (país de destino) ya que deseo adquirir más experiencia. Prefiero los trabajos en el marketing (clásico) de consumidores, pero también estaría abierto a trabajar en el departamento de gestión de la calidad del producto o de planificación estratégica de su empresa.</p>
<p>Estoy aprendiendo francés y/o portugués desde hace algún tiempo y soy consciente de que un idioma sólo se aprende profundamente cuando se está en el país donde se habla esa lengua.</p>
<p>Como no me fue posible aprender francés en la escuela, lo he aprendido por mi cuenta, ya que siempre he querido conocer mejor a los franceses y sus costumbres. Durante mi estancia quisiera con gusto llegar a ser una parte de la cultura francesa y aprender todo lo posible sobre su sociedad y cultura.</p>
<p>¿Por qué habrían de emplearme justamente a mí como persona en período de prácticas en su empresa ? En mi opinión, sé que reúno tanto cualidades profesionales como humanas para poder trabajar en su empresa.</p>
<p>Agradeciendo de antemano su colaboración, me despido atentamente,</p>
<p>Julio Pérez Rodríguez</p>
<h4 class="spip">Exemple 2 </h4>
<p>Para –….…….</p>
<p>X.Y.</p>
<p>Budapest, 24 de enero de 2008</p>
<p>Estimada Empresa,</p>
<p>Soy …………. y he leído el anuncio en el que buscan ………. para su oficina de Budapest.</p>
<p>Acabados mis estudios de Filología Española en la universidad ……………, me encuentro ahora buscando trabajo. La lengua española está presente en mi vida desde los 14 años y sigo utilizándola continuamente, día tras día.</p>
<p>Quisiera una ocupación en el marco de la cual pueda poner en práctica mis conocimientos de alto nivel del español, y de esta manera lograr éxitos tanto para la empresa como para mi persona. Me gustan las tareas que me sean un reto y requieran creatividad, por esta razón creo que pueda incorporarme perfectamente en su equipo.</p>
<p>Tengo experiencias en el terreno del comercio y la publicidad, así me comunico fácilmente con la gente y sé cómo entablar una relación ciertesa y constructiva con la clientela. Me gustaría ampliar mis conocimientos especiales, aprendo rápido y en muy poco tiempo soy capaz de asimilar la materia necesaria para una tarea determinada.</p>
<p>Espero que haya conseguido motivar su interés hacia mi persona, y que mi currículum le cause agrado ; y que también en un encuentro personal tenga la oportunidad de detallar mis conocimientos.</p>
<p>Con la certeza de recibir pronto noticias suyas, me despido muy atentamente,</p>
<p>Julio Pérez</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/emploi-stage/article/lettre-de-motivation). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
