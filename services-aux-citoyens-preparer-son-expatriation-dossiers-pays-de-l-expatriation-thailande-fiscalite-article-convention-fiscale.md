# Convention fiscale

<p>La France et la Thaïlande ont signé à Bangkok <strong>le 27 décembre 1974</strong> une convention en vue d’éviter les doubles impositions et de prévenir l’évasion fiscale en matière d’impôts sur le revenu et sur la fortune. Cette convention est <strong>entrée en vigueur le 21 novembre 1975.</strong></p>
<p>Son texte intégral est consultable sur le <a href="http://www.impots.gouv.fr/" class="spip_out" rel="external">site Internet de l’administration fiscale</a>. Voici un résumé de ses points clés pour les particuliers :</p>
<h4 class="spip">Champ d’application de la convention</h4>
<p>Ses dispositions concernant un Français expatrié en Thaïlande sont les suivantes :</p>
<p>Les salaires, traitements et autres rémunérations similaires qu’un résident d’un Etat contractant reçoit au titre d’un emploi salarié ne sont imposables que dans cet Etat, à moins que l’emploi ne soit exercé dans l’autre Etat contractant. Si l’emploi est exercé dans l’autre Etat contractant, les rémunérations reçues à ce titre sont imposables dans cet autre Etat.</p>
<p>Nonobstant les dispositions du paragraphe ci-dessus, les rémunérations qu’un résident d’un Etat contractant reçoit au titre d’un emploi salarié exercé dans l’autre Etat contractant ne sont imposables que dans le premier Etat si :</p>
<ul class="spip">
<li>le bénéficiaire séjourne dans l’autre Etat pendant une période ou des périodes n’excédant pas au total 183 jours au cours de l’année fiscale considérée ;</li>
<li>les rémunérations sont payées par un employeur ou au nom d’un employeur qui n’est pas résident de l’autre Etat ;</li>
<li>la charge des rémunérations n’est pas supportée par un établissement stable ou une base fixe que l’employeur a dans l’autre Etat.</li></ul>
<p><i>Mise à jour : juillet 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/fiscalite/article/convention-fiscale). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
