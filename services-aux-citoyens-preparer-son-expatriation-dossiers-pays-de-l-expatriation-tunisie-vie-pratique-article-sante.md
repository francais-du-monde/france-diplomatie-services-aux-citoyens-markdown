# Santé

<h4 class="spip">Etat sanitaire</h4>
<p>La Tunisie possède une infrastructure hospitalière et sanitaire satisfaisante à Tunis et Sfax. Ailleurs les conditions sont variables. Les médecins sont de bon niveau et souvent spécialisés dans les universités européennes ou américaines. Le suivi post-opératoire se révèle parfois déficient. Il est recommandé de procéder à des interventions chirurgicales de haute importance à l’étranger. Les structures d’accueil en milieu hospitalier public sont cependant médiocres. Les structures privées s’avèrent performantes.</p>
<p>On trouve des médecins, des spécialistes et des dentistes assez compétents dans toutes les grandes villes : <a href="http://www.ordre-medecins.org.tn/" class="spip_out" rel="external">site de l’ordre national des médecins de Tunisie</a>.</p>
<p>En règle générale, l’ensemble des médicaments essentiels est représenté en Tunisie. Certains médicaments devront être achetés à l’extérieur du pays. Si vous prenez un médicament spécial, assurez-vous qu’il est autorisé et emportez votre prescription médicale.</p>
<p>Le coût des soins chez un médecin généraliste est de 30 à 35 DT, de 50 à 90 DT pour un spécialiste en cabinet. Les soins dentaires s’élèvent à 40 DT (contrôle) et 75 DT (plombage). Pour les visite à domicile d’un médecin généraliste : 50 à 60 DT.</p>
<p>Pour une présentation des risques épidémiologiques ainsi que des conseils sur la médecine au quotidien, consultez les pages suivantes :</p>
<ul class="spip">
<li>Conseils, liste de médecins et hôpitaux sur le site du consulat de France à Tunis ;</li>
<li><a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/tunisie/" class="spip_in">Page dédiée à la santé en Tunisie</a> sur le site Conseils aux voyageurs ;</li>
<li>Fiche Sfax et Tunis sur le site du <a href="http://www.cimed.org" class="spip_out" rel="external">CIMED</a>.</li></ul>
<p><i>Mise à jour : novembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/vie-pratique/article/sante). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
