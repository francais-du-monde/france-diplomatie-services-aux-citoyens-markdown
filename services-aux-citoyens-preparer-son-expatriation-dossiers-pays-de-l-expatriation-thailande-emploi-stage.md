# Emploi, stage

<h2 class="rub23548">Marché du travail</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/emploi-stage/#sommaire_1">Contexte</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/emploi-stage/#sommaire_2">Secteurs à fort potentiel</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/emploi-stage/#sommaire_3">Professions règlementées</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/emploi-stage/#sommaire_4">Rémunération</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Contexte</h3>
<p>La Thaïlande est un pays aux revenus intermédiaires dont l’économie repose tout autant sur les exportations que sur la consommation intérieure.</p>
<p>La plus grande proportion des emplois est concentrée sur l’agriculture alors que certains secteurs font face à une pénurie de main d’œuvre partiellement comblée par les ressortissants des pays voisins. En outre, l’<i>ASEAN Economic Community</i> va introduire une dynamique nouvelle sur le marché du travail thaïlandais.</p>
<h3 class="spip"><a id="sommaire_2"></a>Secteurs à fort potentiel</h3>
<p>Les secteurs à fort potentiel pour un expatrié sont l’industrie manufacturière (automobile, bijouterie, machines outils, équipements à haute valeur ajoutée…) et le secteur financier (contrôle de gestion).</p>
<p>Les profils les plus recherchés sont les profils commerciaux, multilingues et mobiles à l’international. Viennent ensuite les ingénieurs formés dans les grandes écoles françaises. Enfin toutes les personnes qualifiées capables d’assurer des fonctions de direction sont également prisées.</p>
<p><strong>L’industrie manufacturière est un secteur porteur</strong></p>
<p>La filière industrielle représente environ 50% de recrutements de Français. Viennent ensuite les secteurs médicaux et cosmétiques où les français se réservent 62% des importations en Thaïlande. Il faut ensuite citer d’autres secteurs dynamiques avec par ordre d’importance l’aéronautique, la bijouterie, le conseil, les hautes technologies et le textile.</p>
<p><strong>Les transports, l’énergie et le développement durable sont de nouvelles opportunités</strong></p>
<p>Le gouvernement a récemment mis en place un vaste plan de modernisation et de développement des infrastructures. D’autre part la Thaïlande a vocation à devenir le <i>green hub energy</i> de l’Asie. Le photovoltaïque, l’éolien, la biomasse et le biogaz ainsi que l’hydroélectricité sont soutenus par des investissements publics et représente de nouvelles sources de débouchés.</p>
<h3 class="spip"><a id="sommaire_3"></a>Professions règlementées</h3>
<p>Les secteurs non accessibles à un ressortissant étranger sont, à titre d’exemple, l’artisanat traditionnel, la maçonnerie et la comptabilité/audit (sauf international). La liste des professions interdites est consultable  <a href="http://www.thaiworkpermit.com/prohibited-occupations-in-thailand.html" class="spip_out" rel="external">en ligne</a>.</p>
<h3 class="spip"><a id="sommaire_4"></a>Rémunération</h3>
<p>Le salaire moyen pour des emplois les plus couramment occupés par des Français est de 2000-3000 euros par mois. Attention cependant, les assurances santé ne sont pas toujours prises en charge par les entreprises. Le salaire minimum légal pour un étranger est de 50000 bath soit 1228 euros actuellement.</p>
<p><i>Mise à jour : juillet 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-thailande-emploi-stage-article-stages.md" title="Stages">Stages</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-thailande-emploi-stage-article-entretien-d-embauche-114433.md" title="Entretien d’embauche">Entretien d’embauche</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-thailande-emploi-stage-article-lettre-de-motivation-114432.md" title="Lettre de motivation">Lettre de motivation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-thailande-emploi-stage-article-curriculum-vitae-114431.md" title="Curriculum vitae">Curriculum vitae</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-thailande-emploi-stage-article-recherche-d-emploi.md" title="Recherche d’emploi">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-thailande-emploi-stage-article-reglementation-du-travail-114429.md" title="Réglementation du travail">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-thailande-emploi-stage-article-marche-du-travail-114428.md" title="Marché du travail">Marché du travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/emploi-stage/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
