# Convention fiscale

<p>La France et le Venezuela ont signé, le 7 mai 1992, une <a href="http://www.impots.gouv.fr/portal/deploiement/p1/fichedescriptive_1733/fichedescriptive_1733.pdf" class="spip_out" rel="external">convention en vue d’éviter les doubles impositions et de prévenir l’évasion et la fraude fiscale</a> qui régit la manière dont les différents types de revenus des personnes physiques et morales expatriées sont imposés par les deux Etats.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/fiscalite/article/convention-fiscale-103737). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
