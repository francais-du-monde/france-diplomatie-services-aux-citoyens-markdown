# Vaccination

<p>Aucune vaccination n’est exigée à l’entrée du pays pour les voyageurs en provenance de France métropolitaine.</p>
<p>Pour des raisons médicales, il est conseillé de se vacciner contre :</p>
<ul class="spip">
<li>diphtérie</li>
<li>tétanos</li>
<li>poliomyélite</li>
<li>hépatite A (à partir de 50 ans, une recherche préalable des anticorps sériques est justifiée)</li>
<li>hépatite B (pour les séjours prolongés et/ou à risques)</li>
<li>les enfants doivent avoir à jour toutes les vaccinations incluses dans le calendrier vaccinal français, en particulier pour les longs séjours : BCG dès le premier mois et rougeole dès l’âge de neuf mois.</li></ul>
<p>Pour en savoir plus, lisez notre article thématique <a href="services-aux-citoyens-preparer-son-expatriation-sante.md" class="spip_in">Santé</a>.</p>
<p><i>Mise à jour : novembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/entree-et-sejour-22878/article/vaccination-110375). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
