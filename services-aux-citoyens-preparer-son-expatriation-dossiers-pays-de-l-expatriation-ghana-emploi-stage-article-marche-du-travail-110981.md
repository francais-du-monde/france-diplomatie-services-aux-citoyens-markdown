# Marché du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/emploi-stage/article/marche-du-travail-110981#sommaire_1">Secteurs à fort potentiel</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/emploi-stage/article/marche-du-travail-110981#sommaire_2">Professions règlementées</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/emploi-stage/article/marche-du-travail-110981#sommaire_3">Rémunération</a></li></ul>
<p>L’exercice de certaines professions par des ressortissants étrangers au Ghana peut être réglementé (médecin, dentiste, pharmacien, avocat). Il importe donc de s’enquérir des conditions définies par les ordres professionnels concernés dans le pays et d’être en mesure de justifier des qualifications exigées, préalablement à la demande de visa.</p>
<p>Le secteur informel joue un rôle majeur dans le marché du travail. C’est seulement 30 à 40% du marché du travail qui effectivement ouvert tant dans les secteurs publics que privé.</p>
<h3 class="spip"><a id="sommaire_1"></a>Secteurs à fort potentiel</h3>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Pétrolier et parapétrolier<br class="manualbr">Les besoins en main d’œuvre très qualifiées existent dans ces secteurs, mais les conditions de délivrance de permis de travail, qui sont octroyées par la « Petroleum Commission » sont très restrictives. </p>
<h3 class="spip"><a id="sommaire_2"></a>Professions règlementées</h3>
<ul class="spip">
<li>Médecins</li>
<li>Pharmaciens</li>
<li>Dentiste</li>
<li>Infirmières</li>
<li>Avocats</li>
<li>Juges</li>
<li>Policiers</li>
<li>Militaires</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Rémunération</h3>
<p>Les salaires diffèrent fortement d’un secteur à l’autre. Les secteurs exposés à la concurrence internationale offrent des niveaux de rémunération meilleurs. Par exemple, le secteur bancaire et celui de l’industrie du pétrole offre des rémunérations intéressantes, à la différence du secteur public dont le niveau a cependant été réévalué de façon substantielle dans le cadre de la <i>Single spine reform</i> depuis 2010.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/emploi-stage/article/marche-du-travail-110981). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
