# L’enseignement international en France

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/enseignement/article/l-enseignement-international-en#sommaire_1">Les sections européennes</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/enseignement/article/l-enseignement-international-en#sommaire_2">Les sections internationales</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/enseignement/article/l-enseignement-international-en#sommaire_3">La filière bilingue franco-allemande </a></li></ul>
<p>Si vous souhaitez que votre enfant ne perde pas les connaissances qu’il a acquises dans une langue vivante, sachez qu’il existe en France des établissements comportant des sections européennes ou internationales.</p>
<h3 class="spip"><a id="sommaire_1"></a>Les sections européennes</h3>
<p>Les sections européennes sont implantées en collège et lycée, dans l’enseignement public et dans des établissements privés sous contrat d’association. Elles s’adressent aux jeunes désireux d’acquérir la maîtrise d’une langue étrangère à un niveau aussi proche que possible du bilinguisme.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.education.gouv.fr/cid2497/les-sections-europeennes-ou-de-langues-orientales-en-college-et-lycee.html" class="spip_out" rel="external">Ministère de l’Education nationale</a>. </p>
<h3 class="spip"><a id="sommaire_2"></a>Les sections internationales</h3>
<p>Elles accueillent entre 25 et 50% d’élèves étrangers. Ainsi elles créent un cadre propice à l’apprentissage par les élèves français d’une langue étrangère à un haut niveau. Les équipes pédagogiques sont mixtes, françaises et étrangères. Les professeurs étrangers enseignent dans leur langue en histoire, géographie et littérature de leur pays, sur des programmes établis en concertation avec les autorités éducatives des deux pays.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.education.gouv.fr/cid23092/sections-internationales-lycee.html" class="spip_out" rel="external">Ministère de l’Education nationale</a>. </p>
<h3 class="spip"><a id="sommaire_3"></a>La filière bilingue franco-allemande </h3>
<p>La filière a été créée sur la base des sections européennes des collèges et lycées. La spécificité de ce réseau de sections bilingues à profil franco-allemand réside dans le jumelage de deux établissements et dans la conception en commun de projets.</p>
<p>Cette formation conduit à la délivrance simultanée des deux diplômes de fin d’études secondaires français et allemand au terme d’un enseignement renforcé de langue et littérature et de l’enseignement de l’histoire et de la géographie dans la langue associée, sur la base de programmes définis en commun par les autorités des deux pays.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.education.gouv.fr/cid2500/les-dispositifs-franco-allemands.html" class="spip_out" rel="external">Ministère de l’Education nationale</a></p>
<p><i>Mise à jour : avril 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/enseignement/article/l-enseignement-international-en). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
