# Vaccination

<p>Aucune vaccination n’est exigée à l’entrée du pays à l’exception de la fièvre jaune pour les voyageurs en provenance d’une zone infestée (ou ayant transité par une zone infestée), mais pour des raisons médicales on peut recommander :</p>
<ul class="spip">
<li><strong>pour les adultes</strong> : mise à jour des vaccinations contre la diphtérie, le tétanos et la poliomyélite ; vaccination contre la typhoïde, l’hépatite A, l’hépatite B.</li>
<li><strong>pour les enfants</strong> : vaccinations recommandées en France par le ministère de la Santé et en particulier : BCG et hépatite B dès le premier mois (longs séjours), hépatite A (possible à partir d’un an), rougeole dès l’âge de neuf mois et typhoïde à partir de cinq ans (longs séjours).</li></ul>
<p>Les vaccins se trouvent aisément sur place, mais il est préférable de réaliser toutes les vaccinations nécessaires avant de partir, car une fois sur place, on peut rencontrer des difficultés d’approvisionnement.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Vous pouvez consulter la rubrique thématique <a href="services-aux-citoyens-preparer-son-expatriation-sante.md" class="spip_in">Santé</a>. </p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/entree-et-sejour-22968/article/vaccination-111004). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
