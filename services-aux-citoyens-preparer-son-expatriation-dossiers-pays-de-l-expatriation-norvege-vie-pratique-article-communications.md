# Communications

<h4 class="spip">Téléphone – Internet</h4>
<p>Les liaisons téléphoniques ne posent aucune difficulté.</p>
<p>On trouve des cybercafés dans de nombreuses villes du pays (dont Oslo, Bergen, Trondheim, Tromsø).</p>
<p>Pour appeler la Norvège : composer le 00 47 suivi de 8 chiffres.</p>
<p>Pour appeler la France : composer le 00 33 suivi du numéro (sans le zéro).</p>
<p>Il existe des cabines téléphoniques à pièces (1, 5, 10 ou 20 NOK), des cabines à cartes téléphoniques (les cartes s’achètent dans les kiosques à journaux et dans les bureaux de poste) et des cabines où l’on peut utiliser les principales cartes de crédit (dont Visa). L’utilisation des cabines est assez chère et il vaut mieux privilégier l’achat d’une carte SIM norvégienne.</p>
<p>Pour en savoir plus :</p>
<ul class="spip">
<li><a href="http://www.tv5.org/cms/chaine-francophone/voyageurs/Outils/p-6716-Indicatifs-telephoniques.htm" class="spip_out" rel="external">TV5.org</a></li>
<li><a href="http://stavanger.accueil.free.fr/" class="spip_out" rel="external">Stavanger Accueil</a></li></ul>
<h4 class="spip">Téléphoner gratuitement par Internet </h4>
<p>Il est généralement très avantageux d’utiliser <strong>des logiciels de téléphonie sur IP</strong> (Skype, Google talk, Live messenger, Yahoo ! messenger, etc.) car les tarifs proposés à l’international sont souvent moindre que ceux des opérateurs locaux exploitant des lignes classiques, voire gratuits dans certaines conditions.</p>
<h4 class="spip">Poste</h4>
<p>Les liaisons postales ne posent aucune difficulté. Les délais d’acheminement sont de deux à quatre jours ouvrés.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/norvege/vie-pratique/article/communications). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
