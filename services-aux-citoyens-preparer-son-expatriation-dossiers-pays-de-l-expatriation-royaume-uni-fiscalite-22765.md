# Fiscalité

<h2 class="rub22765">Fiscalité du pays</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/fiscalite-22765/#sommaire_1">Présentation</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/fiscalite-22765/#sommaire_2">Année fiscale</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/fiscalite-22765/#sommaire_3">Barème de l’impôt</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/fiscalite-22765/#sommaire_4">Coordonnées des centres d’informations fiscales</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/fiscalite-22765/#sommaire_5">Pour en savoir plus</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Présentation</h3>
<h4 class="spip">Obligations déclaratives des personnes physiques</h4>
<p>Pour les revenus ne faisant pas l’objet d’une retenue à la source (revenus des professions libérales, revenus fonciers, …), l’administration fiscale britannique adresse aux contribuables, en début d’année d’imposition, un formulaire de déclaration de revenus (<i>tax return </i>n° SA 100).</p>
<p>Les déclarations doivent être déposées :</p>
<ul class="spip">
<li>soit avant le 31 octobre qui suit la fin de la période d’imposition pour les contribuables qui souhaitent déposer une déclaration papier ;</li>
<li>soit avant le 30 décembre qui suit la fin de la période d’imposition pour les contribuables qui souhaitent que leur impôt soit calculé par les services fiscaux et recouvré dans le cadre du système PAYE. L’impôt doit être inférieur à 3000 £ ;</li>
<li>soit avant le 31 janvier qui suit la fin de la période d’imposition pour les redevables qui souhaitent déclarer par internet.</li></ul>
<p>Les déclarations sont déposées auprès du service suivant :</p>
<p>Self Assessment Return Processing Office<br class="manualbr">PO Box 6571<br class="manualbr">LIVERPOOL L75 1ZY</p>
<h4 class="spip">Modalités de paiement de l’impôt sur le revenu</h4>
<h5 class="spip">Le système PAYE</h5>
<p>Le système <i>Pay As You Earn</i> constitue une institution essentielle de la fiscalité britannique. Ce système est antérieur à la mise en place du système déclaratif. Il s’agit en fait d’un mécanisme de retenue à la source sur les salaires dont la caractéristique est de prendre directement en compte les déductions auxquelles un salarié a droit. Le montant cumulé des retenues en fin d’année correspond exactement à l’impôt dû et, dans la plupart des cas, aucune régularisation n’est nécessaire.</p>
<h5 class="spip">Coexistence du système <i>PAYE</i> et du système déclaratif</h5>
<p>La retenue à la source sur les salaires coexiste avec le système déclaratif dès lors que les revenus non salariaux ne faisant pas l’objet d’une retenue à la source libératoire doivent être déclarés dans le cadre du système de <i>self-assessment (cf. </i>ci-dessous<i>)</i>. Ainsi, un salarié disposant, par exemple, de revenus fonciers devra déposer une déclaration pour que ces revenus soient imposés à l’<i>income tax</i>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Année fiscale</h3>
<p>Au Royaume-Uni, pour les personnes physiques, l’année fiscale commence le 6 avril d’une année et se termine le 5 avril de l’année suivante.</p>
<p>L’année fiscale commence, pour les sociétés, le <strong>1er avril de l’année</strong> <i>n</i> et se termine le 31 mars de l’année suivante.</p>
<h3 class="spip"><a id="sommaire_3"></a>Barème de l’impôt</h3>
<h4 class="spip">Impôt sur le revenu (<i>Income Tax</i>)</h4>
<p><strong>L’impôt sur le revenu est calculé selon le barème suivant (2013-2014)</strong></p>
<table class="spip">
<thead><tr class="row_first"><th id="id2785_c0">Tranches d’imposition</th><th id="id2785_c1">Taux</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="id2785_c0">de 0 à 2790 £ (revenus de l’épargne)</td>
<td headers="id2785_c1">10%</td></tr>
<tr class="row_even even">
<td headers="id2785_c0">de 0 à 32 010 £</td>
<td headers="id2785_c1">20% <i>(taux de base)</i></td></tr>
<tr class="row_odd odd">
<td headers="id2785_c0">de 32 011 £ à 150 000 £</td>
<td headers="id2785_c1">40% <i>(taux supérieur)</i></td></tr>
<tr class="row_even even">
<td headers="id2785_c0">+ de 150 000 £</td>
<td headers="id2785_c1">45% (<i>taux additionnel)</i></td></tr>
</tbody>
</table>
<p><strong>Abattements applicables en 2013-2014</strong></p>
<table class="spip">
<thead><tr class="row_first"><th id="ide1f7_c0">Situation du contribuable</th><th id="ide1f7_c1">Montant</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="ide1f7_c0">Abattement individuel</td>
<td headers="ide1f7_c1">9440 £</td></tr>
<tr class="row_even even">
<td headers="ide1f7_c0">Contribuables nés entre le 06/04/1938 et le 05/04/1948</td>
<td headers="ide1f7_c1">10 500 £</td></tr>
<tr class="row_odd odd">
<td headers="ide1f7_c0">Contribuables nés avant le 06/04/1938</td>
<td headers="ide1f7_c1">10 660 £</td></tr>
<tr class="row_even even">
<td headers="ide1f7_c0">Revenu annuel maximum des personnes nées avant le 06/04/1948 pour bénéficier d’un abattement en fonction de l’âge</td>
<td headers="ide1f7_c1">26 100 £</td></tr>
<tr class="row_odd odd">
<td headers="ide1f7_c0">Couples mariés (né avant le 6 avril 1935 et âgé de 75 ans et +)</td>
<td headers="ide1f7_c1">7915 £</td></tr>
<tr class="row_even even">
<td headers="ide1f7_c0">Abattement minimum en faveur des couples mariés</td>
<td headers="ide1f7_c1">3040 £</td></tr>
<tr class="row_odd odd">
<td headers="ide1f7_c0">Abattement en faveur des personnes non voyantes</td>
<td headers="ide1f7_c1">2160 £</td></tr>
</tbody>
</table>
<h4 class="spip">Imposition des plus-values des personnes physiques (Capital Gain Tax)</h4>
<p>Les plus-values immobilières ou mobilières réalisées par les particuliers sont taxables au taux de 18% lorsque la somme des plus-values réalisées et des revenus perçus par ailleurs est inférieure à 32 010 £. Un taux de 28 % s’applique au-delà.</p>
<p><strong>Précisions</strong> :</p>
<ul class="spip">
<li>le taux de 28% s’applique à l’intégralité d’une plus-value réalisée par d’autres types de redevables (<i>trustees</i>…) ;</li>
<li>la cession de la résidence principale est exonérée.</li></ul>
<p>Au titre de l’année fiscale 2013-2014, l’abattement individuel au titre de l’impôt sur les plus-values est fixé à 10 900 £ pour les particuliers, les exécuteurs testamentaires et représentants des personnes décédées et les administrateurs de trusts constitués au profit de personnes handicapées.</p>
<h4 class="spip">Imposition des revenus financiers</h4>
<p>Les modalités d’imposition des revenus de capitaux mobiliers diffèrent selon leur nature et la situation des entités versantes :</p>
<ul class="spip">
<li>les dividendes sont versés sans retenue à la source et sont imposés ultérieurement ;</li>
<li>les intérêts font, en principe, l’objet d’une retenue à la source (toutefois, dans certains cas, ils sont versé bruts et imposés ultérieurement).</li></ul>
<p>Font l’objet d’un prélèvement à la source :</p>
<ul class="spip">
<li>les intérêts de toute nature versés à des non-résidents. Le prélèvement libératoire est effectué au taux de base de 20% (<i>cf</i>. ci-dessus : barème de l’impôt sur le revenu). Cette retenue à la source peut être réduite ou annulée par les dispositions de la convention fiscale applicable ;</li>
<li>les intérêts versés par des banques ou organismes assimilés établis au Royaume-Uni : ce prélèvement au taux de base de 20% est libératoire pour le contribuable imposable à ce taux à l’impôt sur le revenu.</li></ul>
<p>Pour les contribuables imposables au taux supérieur de 40%, l’impôt restant dû est :</p>
<ul class="spip">
<li>soit calculé et payé par le contribuable s’il dépose une déclaration de revenus ;</li>
<li>soit prélevé directement sur son salaire si le contribuable est pris en compte dans le système PAYE.</li></ul>
<p><strong>Précision</strong> : les contribuables qui ne sont pas assujettis à l’impôt sur le revenu ou sont passibles du seul taux de 10% peuvent demander le remboursement du surplus de retenue prélevée et/ou obtenir à l’avenir le versement des intérêts bruts.</p>
<p>Sont versés sans retenue à la source :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  les dividendes versés à des résidents britanniques notamment par les sociétés résidentes britanniques, les trusts en participation unitaire et certains <i>partnerships</i>.</p>
<p><strong>Taux d’imposition des dividendes (2013-2014)</strong></p>
<table class="spip">
<thead><tr class="row_first"><th id="id7b03_c0">Montant de dividends versés</th><th id="id7b03_c1">Taux appliqué après déduction de l’abattement individuel </th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="id7b03_c0">Dividendes = 32 010 £</td>
<td headers="id7b03_c1">10%</td></tr>
<tr class="row_even even">
<td headers="id7b03_c0">Dividendes = 150 000 £</td>
<td headers="id7b03_c1">32,5%</td></tr>
<tr class="row_odd odd">
<td headers="id7b03_c0">Dividendes  150 000 £</td>
<td headers="id7b03_c1">37,5%</td></tr>
</tbody>
</table>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  sur demande du contribuable à son établissement financier, les intérêts versés à des personnes physiques résidentes mais non ordinairement résidentes.</p>
<h4 class="spip">Impôt sur les sociétés (<i>Corporation Tax</i>)</h4>
<p>Seules les sociétés dont le bénéfice imposable est supérieur à 1 500 000 £ doivent verser des acomptes d’impôt sur les sociétés. En ce qui concerne les autres sociétés, l’impôt doit être acquitté en une seule fois, dans les neuf mois et un jour qui suivent la fin de l’exercice comptable (la déclaration d’IS doit être déposée dans les 12 mois qui suivent la fin de l’exercice).</p>
<p>Les sociétés résidentes du Royaume-Uni sont imposables sur les bénéfices et les plus-values qu’elles réalisent. Le résultat auquel s’applique l’impôt sur les sociétés inclue les revenus d’exploitation commerciale, les revenus fonciers, les revenus de capitaux mobiliers…</p>
<h5 class="spip">Les principaux taux en matière d’impôt sur les sociétés</h5>
<p>A compter du 1er avril 2013, le taux normal d’imposition à l’impôt sur les sociétés est de 23%. Il passera à 21% au 1er avril 2014 pour s’établir à 20% au 1er avril 2015. Les entreprises soumises à ce taux sont celles qui réalisent des bénéfices supérieurs à 1 500 000 £ ou qui sont membres d’un groupe dont les bénéfices sont supérieurs à 1 500 000 £, ou encore celles n’entrant pas dans le champ d’un autre taux.</p>
<p><strong>Remarque</strong> : le résultat des sociétés dont les bénéfices proviennent de l’extraction du pétrole et du gaz et de l’exploitation de droits pétroliers au Royaume-Uni (et sur la plate-forme continentale britannique) sont soumis à un taux d’imposition spécifique de 30 % (<i>ring fence corporation tax).</i></p>
<h5 class="spip">Le taux d’IS applicable aux petites entreprises est de 20%</h5>
<p>Ce taux est applicable aux sociétés dont le bénéfices imposable à l’impôt sur les sociétés est inférieur à 300 000 £.</p>
<p><strong>Précision</strong> : les entreprises dont le bénéfice se situe entre 300 000 et 1 500 000 £ bénéficient d’un abattement marginal de l’impôt calculé au taux normal.</p>
<p><strong>Taux d’imposition à l’IS</strong></p>
<table class="spip">
<thead><tr class="row_first"><th id="ide678_c0">Exercice du 1er avril 2013 au 31 mars 2014</th><th id="ide678_c1">Taux</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="ide678_c0">Entreprises dont le bénéfice est compris entre 0 et 300 000 £</td>
<td headers="ide678_c1">20%</td></tr>
<tr class="row_even even">
<td headers="ide678_c0">Entreprises dont le bénéfice est compris entre 300 001 £ et 1 500 000 £</td>
<td headers="ide678_c1">23% avec <i>marginal relief</i></td></tr>
<tr class="row_odd odd">
<td headers="ide678_c0">Entreprises dont le bénéfice est supérieur à 1 500 001 £</td>
<td headers="ide678_c1">23%</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_4"></a>Coordonnées des centres d’informations fiscales</h3>
<p><a href="http://search2.hmrc.gov.uk/kb5/hmrc/contactus/home.page" class="spip_out" rel="external">http://search2.hmrc.gov.uk/kb5/hmrc/contactus/home.page</a></p>
<p><a href="http://www.hmrc.gov.uk/" class="spip_out" rel="external">HM Revenue  Customs</a><br class="manualbr">Pay As You Earn<br class="manualbr">PO Box 1970<br class="manualbr">Liverpool<br class="manualbr">L75 1WX<br class="manualbr">Income tax Helpline : 0300 200 3300</p>
<h3 class="spip"><a id="sommaire_5"></a>Pour en savoir plus</h3>
<ul class="spip">
<li><a href="http://www.tresor.economie.gouv.fr/Pays/royaume-uni" class="spip_out" rel="external">Service économique français au Royaume-Uni</a></li>
<li>Le guide "Vivre et travailler en Angleterre" (éditions Lextenso) et le <a href="http://www.travailler-en-angleterre.com/category/impots" class="spip_out" rel="external">site du même nom</a> fournissent des informations utiles.</li></ul>
<p><i>Mise à jour : décembre 2013</i></p>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/fiscalite-22765/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
