# Recherche d’emploi

<p>La connaissance de la langue turque est, dans la majorité des cas, impérative.</p>
<p>La connaissance de l’anglais et de l’allemand peut être un atout.</p>
<h4 class="spip">Outils pour la recherche d’emploi</h4>
<h4 class="spip">Journaux</h4>
<p>En Turquie, des offres d’emploi sont diffusées en turc sous la rubrique des petites annonces de la presse locale et nationale. Les éditions du dimanche des journaux comme Hürriyet, Sabah et Habertürk proposent un supplément "Ressources humaines" (annonces en anglais en général).</p>
<p>Des agences spécialisées dans l’emploi offrent leurs services.</p>
<h4 class="spip">Sites internet</h4>
<ul class="spip">
<li><a href="http://www.yenibiris.com/" class="spip_out" rel="external">Yenibiris.com</a></li>
<li><a href="http://www.kariyer.net/" class="spip_out" rel="external">Kariyer.net</a></li></ul>
<h4 class="spip">Annuaires</h4>
<p>Soumettre son CV en ligne :</p>
<ul class="spip">
<li><a href="http://www.adecco.com.tr/" class="spip_out" rel="external">Adecco Türkiye</a></li>
<li><a href="http://www.amrop-tr.com/" class="spip_out" rel="external">Amrop International</a></li>
<li><a href="http://www.antal.com/" class="spip_out" rel="external">Antal International</a></li>
<li><a href="http://www.egonzehnder.com/global/ourfirm/officesregions/office/Istanbul" class="spip_out" rel="external">Egon Zehnder</a></li>
<li><a href="http://www.heidrick.com/" class="spip_out" rel="external">Heidrick  Struggles</a></li>
<li><a href="http://www.kornferry.com/" class="spip_out" rel="external">Korn Ferry</a></li>
<li><a href="http://www.manpower.com.tr/" class="spip_out" rel="external">Manpower</a></li>
<li><a href="http://www.pedersenandpartners.com/" class="spip_out" rel="external">Pederson  Partners</a></li>
<li><a href="http://www.profilinternational.com/tr" class="spip_out" rel="external">Profil International</a></li>
<li><a href="http://www.stantonchase.com/" class="spip_out" rel="external">Stanton Chase</a></li>
<li><a href="http://www.turnky.com/" class="spip_out" rel="external">Turnkey</a></li></ul>
<h4 class="spip">Organismes pour la recherche d’emploi</h4>
<p>La représentation de l’Office français de l’immigration et de l’intégration (OFII, anciennement ANAEM) en Turquie a été créée en 1969, conformément aux dispositions de la convention de main d’œuvre entre la France et la Turquie du 8 avril 1965.</p>
<p>Elle assure :</p>
<ul class="spip">
<li>le contrôle administratif et médical des bénéficiaires du regroupement familial ;</li>
<li>les formalités d’introduction des travailleurs turcs sur le sol français ;</li>
<li>la promotion des nouveaux outils de l’immigration professionnelle et plus particulièrement la carte de séjour « compétences et talents » ;</li>
<li>la mise en œuvre du « contrat d’accueil et d’intégration » en Turquie.</li></ul>
<p>Bureau Immigration : Lüleciler Caddesi no : 4 Tophane – Istanbul<br class="manualbr">Tél : +90 (212) 243 67 10 / 11<br class="manualbr">Télécopie : +90 (212) 243 52 42<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/emploi-stage/article/recherche-d-emploi#istanbul#mc#ofii.fr#" title="istanbul..åt..ofii.fr" onclick="location.href=mc_lancerlien('istanbul','ofii.fr'); return false;" class="spip_mail">Courriel</a></p>
<p>Horaires d’ouverture au public : du lundi au vendredi de 08h30 à 16h30</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/emploi-stage/article/recherche-d-emploi). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
