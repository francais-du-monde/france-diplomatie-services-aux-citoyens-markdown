# Qu’est-ce que la nationalité française ?

<p>La nationalité est le lien juridique qui relie un individu à un État déterminé. De ce lien découlent pour les personnes aussi bien des obligations (service national par exemple) que des droits politiques, civils voire professionnels. La nationalité française peut résulter :</p>
<ul class="spip">
<li>soit d’une attribution par filiation (droit du sang) ou par la naissance en France de parents nés en France (droit du sol) ;</li>
<li>soit d’une acquisition :
<br>— de plein droit (exemple : naissance et résidence en France)
<br>— par déclaration (exemple : mariage avec un conjoint français)
<br>— par décret de naturalisation</li></ul>
<p>La perte de la nationalité française peut être la conséquence d’une décision de l’autorité publique, d’un acte volontaire, d’un non-usage prolongé ou d’une déchéance.</p>
<p>Sous certaines conditions, la réintégration dans la nationalité française est prévue par le code civil.</p>
<p>La preuve matérielle de la nationalité française est constituée par le certificat de nationalité française délivré par les greffiers en chef des tribunaux d’instance.</p>
<p><i>Mise à jour : mars 2016</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/nationalite-francaise/article/la-nationalite-francaise). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
