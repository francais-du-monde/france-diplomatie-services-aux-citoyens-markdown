# Pour en savoir plus

<p><strong>Bibliographie</strong></p>
<ul class="spip">
<li><i>Histoire d’Haïti - la première république noire du Nouveau Monde</i>, Catherine EVE-ROUPERT, Ed. Perrin, 2011</li>
<li><i>Haïti – République Dominicaine : une île pour deux 1804-1916</i>, Jean-marie THEODAT, Ed. Karthala, 2003</li>
<li><i>Haïti n’existe pas – 1804-2004 : deux cents ans de solitude</i>, Christophe WARGNY, Ed. Autrement, 2008</li>
<li><i>Haïti</i>, Paul Clammer, Ed. Bradt, 2012</li>
<li><i>Le vaudou haïtien</i>, Alfred METRAUX, Ed. Gallimard, 1977</li>
<li><i>Les mystères du vaudou</i>, Laënnec HURBON, Ed. Gallimard, 1993</li></ul>
<p><i>Mise à jour : août 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/vie-pratique/article/pour-en-savoir-plus-114268). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
