# Marché du travail

<p class="chapo">
      Le Maroc a vu son taux de chômage baisser ces dernières années pour s’établir, fin 2008, légèrement en dessous de 10 %. Malgré une croissance soutenue, la création d’emploi est insuffisante avec seulement 133 000 emplois nets créés en 2008, dont à peine 28 000 dans l’industrie. Le gouvernement fixe ses objectifs à 300 000 emplois par an jusqu’en 2020.
</p>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/emploi-stage/article/marche-du-travail-110839#sommaire_1">Professions règlementées</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/emploi-stage/article/marche-du-travail-110839#sommaire_2">Rémunération</a></li></ul>
<p>Le Maroc connaît un déficit en ressources humaines qualifiées dans de nombreux secteurs du fait d’un système éducatif et de formation qui répond mal aux besoins des entreprises. Un plan d’urgence pour la formation professionnelle est en cours pour les années 2008/2012 avec comme objectif de former 750 000 jeunes notamment dans les secteurs de l’aéronautique, de l’automobile et de la mode.</p>
<p>La crise internationale a fait sentir ses premiers effets début 2009, notamment dans les secteurs du textile, de l’automobile et du tourisme qui connaissent une baisse d’activité.</p>
<p>L’accès à un emploi au Maroc est soumis à la formalité du contrat de travail pour étranger et à une préférence nationale pour les postes peu qualifiés. A partir des fonctions de cadre supérieur et de direction, la préférence nationale devient plus souple. C’est à ce niveau que les postes sont le plus ouverts aux étrangers.</p>
<p>Pour les postes qui restent ouverts aux étrangers, les secteurs les plus dynamiques offrant des possibilités d’emploi sont les transports/logistique, le tourisme, le BTP, l’environnement, la sous-traitance industrielle, l’ingénierie et les bureaux d’études, l’informatique et les technologies de l’information et de la communication, la distribution et la publicité.</p>
<h3 class="spip"><a id="sommaire_1"></a>Professions règlementées</h3>
<p>En ce qui concerne les professions libérales (dites réglementées) - médecine, architecture, expertise comptable, etc. l’accès à la profession est soumis à un ensemble de conditions (nationalité marocaine ou réciprocité d’accès dans l’Etat d’origine du candidat et avis de plusieurs institutions : secrétariat général du gouvernement, ministère technique concerné et avis de l’ordre national marocain concerné). En pratique ces professions sont largement fermées aux étrangers.</p>
<h3 class="spip"><a id="sommaire_2"></a>Rémunération</h3>
<p>Exemples de salaires de Français recrutés localement :</p>
<ul class="spip">
<li>Secrétaire : 5 000 MAD</li>
<li>Ingénieur : 19 000 MAD</li>
<li>Cadre moyen : 10 000 à 12 000 MAD</li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/emploi-stage/article/marche-du-travail-110839). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
