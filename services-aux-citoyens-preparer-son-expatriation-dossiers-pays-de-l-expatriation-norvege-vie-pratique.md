# Vie pratique

<h2 class="rub23014">Logement</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/norvege/vie-pratique/#sommaire_1">Où se loger ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/norvege/vie-pratique/#sommaire_2">Conditions de location</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/norvege/vie-pratique/#sommaire_3">Hôtels</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/norvege/vie-pratique/#sommaire_4">Electricité</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/norvege/vie-pratique/#sommaire_5">Electroménager</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Où se loger ?</h3>
<p><strong>Oslo</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Loyer mensuel - quartier résidentiel</strong></td>
<td>couronnes norvégiennes</td>
<td>euros</td></tr>
<tr class="row_even even">
<td>Studio</td>
<td>8500</td>
<td>1054</td></tr>
<tr class="row_odd odd">
<td>3 pièces</td>
<td>15 000</td>
<td>1800</td></tr>
<tr class="row_even even">
<td>5 pièces</td>
<td>25 000</td>
<td>3050</td></tr>
<tr class="row_odd odd">
<td>Villa</td>
<td>35 000</td>
<td>4270</td></tr>
</tbody>
</table>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Loyer mensuel - banlieue</strong></td>
<td>couronnes norvégiennes</td>
<td>euros</td></tr>
<tr class="row_even even">
<td>Studio</td>
<td>7500</td>
<td>915</td></tr>
<tr class="row_odd odd">
<td>3 pièces</td>
<td>12 000</td>
<td>1464</td></tr>
<tr class="row_even even">
<td>5 pièces</td>
<td>20 000</td>
<td>2440</td></tr>
<tr class="row_odd odd">
<td>Villa</td>
<td>25 000</td>
<td>3 050</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_2"></a>Conditions de location</h3>
<p>Les quartiers résidentiels sont situés à l’ouest d’Oslo, en bord de mer ou sur les collines surplombant la ville.</p>
<p>Le marché locatif est peu important. Les agences immobilières s’occupent surtout de transactions achats-ventes et proposent un choix restreint. La recherche s’effectue essentiellement sur internet. Il existe un contrat-type de location que l’on peut se procurer chez les libraires. Les avocats ont néanmoins l’habitude d’établir des contrats particuliers conformes à une législation locale assez complexe.</p>
<p>Les locations concernent surtout des logements vides, appartements dans le centre-ville et maisons dans les quartiers résidentiels proches. Le coût de la location est proportionnellement plus élevé pour les petites surfaces.</p>
<p>Selon la saison, le délai moyen pour trouver un logement peut varier d’une semaine à un mois. La durée du bail dépend de la surface et du type de contrat (comprise entre six mois et deux ans d’une façon générale).</p>
<p>Compte tenu de l’ancienneté de certains bâtiments et afin d’éviter certains malentendus à la fin du contrat, il est recommandé d’effectuer un état des lieux.</p>
<p>Le loyer est payable d’avance. En outre, une caution de trois mois est souvent pratiquée, mais une garantie bancaire peut être proposée au loueur, consistant pour le locataire à souscrire une assurance pour le paiement du loyer en cas de défaillance.</p>
<p>Le délai de résiliation d’une location par le propriétaire d’un meublé est souvent très réduit (une semaine à un mois, précisé dans le contrat, en conformité avec la législation locale). Par contre, la législation est plus contraignante (à partir de trois mois de préavis) pour le locataire qui souhaite résilier son contrat. Les usages locaux impliquant un bail consenti par le propriétaire, le contrat est rompu de plein droit en cas de vente du bien immobilier.</p>
<p>La tendance actuelle du marché immobilier incite à l’achat. Pour la procédure à suivre, il faut alors consulter les agences, lesquelles sont assez contrôlées par la législation (une école d’Etat sanctionne les études par un diplôme obligatoire pour exercer la profession).</p>
<p>La consommation d’eau, lorsqu’elle n’est pas comprise dans les charges, est minime. Le chauffage, souvent électrique, est le poste du budget le plus lourd, hors loyer et alimentation.</p>
<h3 class="spip"><a id="sommaire_3"></a>Hôtels</h3>
<p><strong>Oslo</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Prix moyen d’une chambre d’hôtel (chambre double)</strong></td>
<td>couronnes norvégiennes</td>
<td>euros</td></tr>
<tr class="row_even even">
<td>Grand tourisme</td>
<td>850</td>
<td>103</td></tr>
<tr class="row_odd odd">
<td>Moyen tourisme</td>
<td>550</td>
<td>67</td></tr>
</tbody>
</table>
<h4 class="spip">Auberges de jeunesse</h4>
<p>La Norvège compte environ 70 auberges de jeunesse y compris dans le nord du pays. Les tarifs (pour une personne) peuvent varier de 15 à 60€ la nuit.</p>
<p>Pour les renseignements et réservations :</p>
<p><strong>Norske Vandrerhjem - Hostelling international</strong><br class="manualbr">Torggata 1 - N-0181 Oslo <br class="manualbr">Tél : (47) 23 13 93 00 <br class="manualbr">Fax : (47) 23 13 93 50 <br class="manualbr">Site internet : <a href="http://www.vandrerhjem.no/" class="spip_out" rel="external">www.vandrerhjem.no</a></p>
<h3 class="spip"><a id="sommaire_4"></a>Electricité</h3>
<p>Courant électrique : 220 volts alternatif ; 50 Hz.</p>
<p>Les prises de courant sont au standard européen (les mêmes qu’en France).</p>
<h3 class="spip"><a id="sommaire_5"></a>Electroménager</h3>
<p>Les cuisines sont le plus souvent équipées des appareils électroménagers les plus courants (four ou plaque de cuisson, lave-linge, lave-vaisselle, réfrigérateur). Toutefois, il est possible de se procurer l’équipement sur place.</p>
<p>Le moyen de chauffage le plus utilisé est l’électricité ; son coût est élevé ; le marché étant libre, on peut placer les compagnies locales en concurrence. Les journaux et les sites internet fournissent régulièrement les tarifs pratiqués.</p>
<p>En raison de l’atmosphère sèche (chauffage) l’hiver, on peut prévoir un humidificateur d’air.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-norvege-vie-pratique-article-loisirs-et-culture-111222.md" title="Loisirs et culture">Loisirs et culture</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-norvege-vie-pratique-article-communications.md" title="Communications">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-norvege-vie-pratique-article-transports.md" title="Transports">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-norvege-vie-pratique-article-cout-de-la-vie-111219.md" title="Coût de la vie">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-norvege-vie-pratique-article-scolarisation.md" title="Scolarisation">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-norvege-vie-pratique-article-sante.md" title="Santé">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-norvege-vie-pratique-article-logement.md" title="Logement">Logement</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/norvege/vie-pratique/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
