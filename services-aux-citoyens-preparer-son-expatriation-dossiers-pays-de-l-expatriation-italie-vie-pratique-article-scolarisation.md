# Scolarisation

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/vie-pratique/article/scolarisation#sommaire_1">Les établissements scolaires français en Italie</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/vie-pratique/article/scolarisation#sommaire_2">Dispositif ESABAC</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/vie-pratique/article/scolarisation#sommaire_3">Enseignement supérieur</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Les établissements scolaires français en Italie</h3>
<p>Pour toute information sur la scolarisation dans le système français à l’étranger, vous pouvez consulter nos thématiques sur les <a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures.md" class="spip_in">études</a> et la <a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais.md" class="spip_in">scolarisation</a>.</p>
<p>Vous y trouverez des renseignements sur :</p>
<ul class="spip">
<li>les établissements français du primaire et du secondaire à l’étranger ;</li>
<li>les bourses scolaires et la prise en charge des frais de scolarité à l’étranger ;</li>
<li>les possibilités qui s’offrent à vous si votre enfant ne peut être scolarisé à l’étranger dans le système français (enseignement à distance par le CNED, programme français langue maternelle (FLAM), internats en France) ;</li>
<li>les épreuves du baccalauréat à l’étranger ;</li>
<li>les bourses d’études supérieures en France et à l’étranger ;</li>
<li>l’équivalence des diplômes.</li></ul>
<p>Vous trouverez ci-après la liste des établissements scolaires français en Italie et les contacts pour obtenir tous les renseignements souhaités, y compris sur les tarifs.</p>
<p>[<strong>A Rome</strong>]</p>
<table class="spip">
<thead><tr class="row_first"><th id="id95e1_c0">Etablissement</th><th id="id95e1_c1"></th><th id="id95e1_c2">Coordonnées</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="id95e1_c0"><strong>Lycée Chateaubriand</strong>
<p>Le lycée Chateaubriand est sur deux sites distincts :</p>
</td>
<td headers="id95e1_c1"><strong>de la maternelle à la 4è : Strohl-Fern</strong></td>
<td headers="id95e1_c2">Villa Strohl Fern
<p>Via Ruffo, 31 – 00196 ROMA</p>
<p>Tél. : 06/441 60 45 10</p>
<p>Fax : 06/ 321 15 75</p>
<p><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/vie-pratique/article/scolarisation#directeur#mc#lycee-chateaubriand.eu#" title="directeur..åt..lycee-chateaubriand.eu" onclick="location.href=mc_lancerlien('directeur','lycee-chateaubriand.eu'); return false;" class="spip_mail">Courriel</a></p>
</td></tr>
<tr class="row_even even">
<td headers="id95e1_c0"></td>
<td headers="id95e1_c1"><strong>de la 3ème à la terminale : Villa Patrizi</strong></td>
<td headers="id95e1_c2">Villa Patrizi
<p>Via di Villa Patrizi, 9 - 00161 ROMA</p>
<p>Tél. : 06/441 60 41</p>
<p>Fax : 06/440 26 54</p>
<p><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/vie-pratique/article/scolarisation#secdirection#mc#lycee-chateaubriand.eu#" title="secdirection..åt..lycee-chateaubriand.eu" onclick="location.href=mc_lancerlien('secdirection','lycee-chateaubriand.eu'); return false;" class="spip_mail">Courriel</a></p>
</td></tr>
<tr class="row_odd odd">
<td headers="id95e1_c0"><strong>Institut Saint-Dominique</strong></td>
<td headers="id95e1_c1"></td>
<td headers="id95e1_c2">Via Igino Lega, 5
<p>Tél. : 06/303 108 17 - 00189 ROMA</p>
<p>Fax : 06/303 113 33</p>
<p><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/vie-pratique/article/scolarisation#isddir2#mc#institutsaintdominique.it#" title="isddir2..åt..institutsaintdominique.it" onclick="location.href=mc_lancerlien('isddir2','institutsaintdominique.it'); return false;" class="spip_mail">Courriel</a></p>
</td></tr>
</tbody>
</table>
<p><strong>A Naples</strong></p>
<table class="spip">
<thead><tr class="row_first"><th id="idf609_c0">Etablissement</th><th id="idf609_c1">Coordonnées</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<th headers="idf609_c0" id="idf609_l0">Ecole Alexandre Dumas</th>
<td headers="idf609_c1 idf609_l0">Via Francesco Crispi, 86 - 80121 NAPOLI
<p>Tél. : 081/668 936 ou 081/660 169</p>
<p>Fax : 081/680 532</p>
<p><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/vie-pratique/article/scolarisation#anne.scarpa#mc#ac-grenoble.fr#" title="anne.scarpa..åt..ac-grenoble.fr" onclick="location.href=mc_lancerlien('anne.scarpa','ac-grenoble.fr'); return false;" class="spip_mail">Courriel</a></p>
</td></tr>
</tbody>
</table>
<p><strong>A Turin</strong></p>
<table class="spip">
<thead><tr class="row_first"><th id="id1905_c0">Etablissement</th><th id="id1905_c1">Coordonnées</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<th headers="id1905_c0" id="id1905_l0">Lycée Giono</th>
<td headers="id1905_c1 id1905_l0">Corso Casale, 324 10132 - TORINO
<p>Tél. : 011/660 29 55</p>
<p>Fax : 011/660 00 26</p>
<p><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/vie-pratique/article/scolarisation#proviseur#mc#lyceegionoturin.it#" title="proviseur..åt..lyceegionoturin.it" onclick="location.href=mc_lancerlien('proviseur','lyceegionoturin.it'); return false;" class="spip_mail">Courriel</a></p>
</td></tr>
</tbody>
</table>
<p><strong>A Milan</strong></p>
<table class="spip">
<thead><tr class="row_first"><th id="id10c2_c0">Etablissement</th><th id="id10c2_c1">Coordonnées</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<th headers="id10c2_c0" id="id10c2_l0">Lycée Stendhal</th>
<td headers="id10c2_c1 id10c2_l0">Via Laveno, 12 – 20148 - MILANO
<p>Tél. : 02/48 796 10</p>
<p>Fax : 02/48 700566</p>
<p><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/vie-pratique/article/scolarisation#proviseur#mc#lsmi.it#" title="proviseur..åt..lsmi.it" onclick="location.href=mc_lancerlien('proviseur','lsmi.it'); return false;" class="spip_mail">Courriel</a></p>
</td></tr>
</tbody>
</table>
<p><strong>A Florence</strong></p>
<table class="spip">
<thead><tr class="row_first"><th id="id81ed_c0">Etablissement</th><th id="id81ed_c1">Coordonnées</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<th headers="id81ed_c0" id="id81ed_l0">Ecole franco-italienne / MLF</th>
<td headers="id81ed_c1 id81ed_l0">Palazzo Venturi Ginori
<p>Via della scala 85 - 50100 FIRENZE</p>
<p>Tél. : 055/ 285538</p>
<p>Fax : 055/ 210 287</p>
<p><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/vie-pratique/article/scolarisation#secretariat#mc#vhugo.eu#" title="secretariat..åt..vhugo.eu" onclick="location.href=mc_lancerlien('secretariat','vhugo.eu'); return false;" class="spip_mail">Courriel</a></p>
</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_2"></a>Dispositif ESABAC</h3>
<p>Depuis septembre 2010, un nouveau dispositif éducatif est entré en vigueur de part et d’autre des Alpes : l’EsaBac, qui permet aux élèves italiens et français d’obtenir simultanément deux diplômes à partir d’un seul examen - l’Esame di Stato italien et le Baccalauréat français.</p>
<p>Cette double délivrance de diplômes, rendue possible grâce à l’accord signé le 24 février 2009. Il valide un parcours scolaire véritablement biculturel et bilingue.</p>
<p>La formation à l’EsaBac se place dans la continuité de l’enseignement bilingue franco-italien en place dans les sections internationales et dans les lycées classiques européens, depuis les années 1990. La double certification constitue désormais une véritable avancée pour la coopération éducative entre les deux pays : una marcia in più pour les élèves italiens et français.</p>
<p>Voir le site pédagogique : <a href="http://www.vizavi-edu.it/" class="spip_out" rel="external">VIZAVI, le site de l’enseignement bilingue français en Italie</a></p>
<p>En 2013, 210 sections EsaBac étaient ouvertes.</p>
<h3 class="spip"><a id="sommaire_3"></a>Enseignement supérieur</h3>
<p>Il est possible de poursuivre en Italie des études supérieures, dans toutes les disciplines.</p>
<p>Le système universitaire italien est composé de 77 institutions :</p>
<ul class="spip">
<li>56 Università degli Studi : universités publiques polyvalentes, dites « universités d’Etat » ;</li>
<li>3 Politecnici : universités technologiques formées de facultés d’ingénierie et d’une faculté d’architecture ;</li>
<li>13 Libere Università : universités privées ;</li>
<li>2 Università per Stranieri : universités pour étrangers (à Sienne et à Pérouse) ;</li>
<li>3 Scuole Superiori : écoles supérieures avec une législation particulière.</li></ul>
<p>A la sortie du second degré, après l’obtention de "l’esame di stato", les étudiants peuvent poursuivre leurs études dans trois branches principales : la formation universitaire, la haute formation artistique et musicale ou la formation technique supérieure.</p>
<p>Le premier échelon universitaire italien est la <i>laurea </i>(licence), qui s’obtient après trois ans d’études et ouvre la voie à la <i>laurea magistrale ou specialistica </i>(master) qui s’obtient en deux ans et donne accès au <i>dottorato </i>(doctorat).</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li>Site de l’ambassade de France en Italie : <a href="http://www.ambafrance-it.org/rubrique.php3?id_rubrique=148" class="spip_out" rel="external">Le système éducatif italien</a> ;</li>
<li><a href="http://www.studiare-in-italia.it/" class="spip_out" rel="external">Portail du ministère de l’Enseignement, de l’Université et de la Recherche</a> ;</li>
<li><a href="http://www.studenti.it/" class="spip_out" rel="external">Studenti.it</a> : principal portail italien pour les étudiants (uniquement en italien). Vous y trouverez des informations sur l’enseignement post-universitaire, les programmes de master et les bourses d’études ;</li>
<li><a href="http://www.masterin.it/" class="spip_out" rel="external">Masterin.it</a> : ce site recense tous les programmes de master pour l’Italie (en anglais, italien et allemand).</li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/vie-pratique/article/scolarisation). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
