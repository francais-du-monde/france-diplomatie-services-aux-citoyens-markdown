# Passeport, visa, permis de travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/entree-et-sejour-23154/article/passeport-visa-permis-de-travail-112182#sommaire_1">Séjourner</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/entree-et-sejour-23154/article/passeport-visa-permis-de-travail-112182#sommaire_2">Travailler</a></li></ul>
<p>Quel que soit le motif de votre séjour, renseignez-vous auprès de la section consulaire de l’ambassade d’Egypte à Paris qui vous informera sur la règlementation en matière d’entrée et de séjour en Egypte, règlementation que vous devrez impérativement respecter. N’hésitez pas à consulter le site du ministère de l’Intérieur de l’Egypte afin de vous familiariser avec les modalités d’entrée et de séjour dans le pays.</p>
<p>Une fois sur place c’est le ministère égyptien de l’Intérieur qui prend le relais. <strong>En aucun cas, vous n’êtes autorisé à travailler en Egypte sans permis adéquat.</strong></p>
<p>Le Consulat de France en Egypte n’est pas l’organisme compétent pour répondre à vos demandes d’information concernant votre séjour en Egypte.</p>
<p>De manière générale, pour toute information relative aux conditions de séjour en Egypte, il est vivement conseillé de contacter la section consulaire de l’<a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-etrangers-en-france/article/annuaire-des-representations-etrangeres-en-france" class="spip_in">ambassade d’Egypte en France</a>. Ici, les informations sont données à titre indicatif.</p>
<h3 class="spip"><a id="sommaire_1"></a>Séjourner</h3>
<p>Un séjour en Egypte nécessite un passeport dont la date d’expiration dépasse six mois ou une carte d’identité. Un visa - payant - est obligatoire. Sa validité est d’un mois. Pour les possesseurs de passeport, le visa peut être obtenu soit auprès des consulats, soit sur place, à l’arrivée à l’aéroport ; pour les porteurs de carte d’identité, le visa s’obtient uniquement sur place et nécessite deux photos d’identité. Le visa obtenu sur place coûte généralement moins cher qu’en France et peut être payé en livres égyptiennes, en dollars ou en euros (15€).</p>
<p>Un séjour prolongé requiert un <strong>visa long séjour</strong>. Il peut être obtenu soit auprès des consulats avant le départ, soit sur place, lors de la première demande de renouvellement de trois mois du visa puis du renouvellement d’un an auprès du ministère de l’Intérieur. Une amende est prévue si le visa n’a pas été renouvelé. Un test HIV est obligatoire pour obtenir un visa long séjour.</p>
<p>Toute personne souhaitant résider en Egypte doit demander un <strong>visa de résident</strong> au ministère égyptien de l’Intérieur. Le délai d’obtention est d’environ quatre semaines. Ce visa est <strong>indispensable pour pouvoir travailler en Egypte</strong> mais aussi pour pouvoir récupérer son déménagement ou acheter une voiture.</p>
<p><strong>Contacts</strong></p>
<p><strong>Le Caire :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Mogamma <br class="manualbr">Bureau de l’Immigration <br class="manualbr">Place Tahrir (centre ville) <br class="manualbr">Le Caire <br class="manualbr">Tél : 2795 63 01 / 03</p>
<p><strong>Alexandrie :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Bureau des passeports et de la nationalité <br class="manualbr">28, rue Talaat el Harb <br class="manualbr">Alexandrie <br class="manualbr">Tél : 48 60 422 </p>
<h3 class="spip"><a id="sommaire_2"></a>Travailler</h3>
<p>Pour pouvoir travailler en Egypte, il est nécessaire d’obtenir un <strong>visa de résident et un permis de travail</strong>. Il est conseillé, si vous travaillez et ne disposez que d’un visa touristique (avec la mention « work is not permitted »), d’être en possession d’une preuve que votre dossier est en cours. Les fonctionnaires des ambassades, des consulats, des services de représentation commerciale et des organismes internationaux ainsi que les correspondants de presse et les stagiaires résidents en Egypte pendant moins d’un an sont exemptés de permis de travail.</p>
<p>Le permis de travail doit être demandé par l’employeur et son obtention peut nécessiter plusieurs mois. De nombreux documents sont exigés, notamment pour l’employé :</p>
<ul class="spip">
<li>le visa touristique ou de résidence ;</li>
<li>sept photos d’identité ;</li>
<li>deux copies des certificats d’expérience ainsi que des diplômes ;</li>
<li>deux copies du contrat de mariage si l’étranger a épousé une Egyptienne ;</li>
<li>un certificat de non contamination par le SIDA. Il est possible de l’effectuer en France et de le faire légaliser par un des consulats d’Egypte en France.</li></ul>
<p>Il est conseillé de se renseigner auprès de l’un des consulats d’Egypte en France.</p>
<p><i>Mise à jour : avril 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/entree-et-sejour-23154/article/passeport-visa-permis-de-travail-112182). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
