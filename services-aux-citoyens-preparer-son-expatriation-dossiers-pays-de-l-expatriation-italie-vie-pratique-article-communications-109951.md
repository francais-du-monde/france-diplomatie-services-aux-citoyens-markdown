# Communications

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/vie-pratique/article/communications-109951#sommaire_1">Téléphone – Internet</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/vie-pratique/article/communications-109951#sommaire_2">Poste</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Téléphone – Internet</h3>
<p>Les liaisons téléphoniques sont bonnes. Depuis la France, l’indicatif de l’Italie est le 00 39, l’indicatif de Rome est le 06, Milan le 02.</p>
<p>Pour téléphoner de France en Italie, il convient de composer le 00 39 + le numéro de votre correspondant italien avec le zéro. Pour joindre de France un portable italien, il convient de composer le 00 39 + préfixe du portable sans le zéro.</p>
<p>Pour téléphoner d’Italie en France vers un poste fixe : composer le 00 33 + numéro de votre correspondant sans le zéro. Idem pour un portable.</p>
<p><a href="http://www.telecomitalia.it/" class="spip_out" rel="external">Telecom Italia</a> est le plus important opérateur. Pour ouvrir une ligne, le code fiscal et le permis de séjour seront demandés à un étranger. Il convient de compter deux semaines de délai entre la demande et l’installation.</p>
<p>Il existe quelques cabines téléphoniques, généralement en centre-ville, qui fonctionnent à cartes (schede telefoniche) vendues dans les kiosques à journaux et bureaux de tabac. Par ailleurs des cartes prépayées peuvent également être achetés dans ces commerce pour des appels à prix réduits depuis un poste fixe ou mobile.</p>
<p>L’usage du portable est très répandu en Italie. Les cartes prépayées s’avèrent très utiles et le coût de la communication est bien moins élevé qu’avec son portable étranger.</p>
<p>Les centres Internet sont généralement ouverts tous les jours ; les connexions sont de bonne qualité et à des prix raisonnables.</p>
<p>Pour l’Italie, Telecom Italia applique des tarifs préférentiels pour toutes les communications du lundi au vendredi de 18h30 à 8h00, du samedi 13h00 au lundi 8h00 et les jours fériés.</p>
<p>Pour l’étranger, tarif réduit du lundi au samedi de 22h00 à 8h00 et le dimanche, toute la journée. Les informations internationales peuvent être obtenues en composant le numéro 176.</p>
<p>Les annuaires téléphoniques, par ordre alphabétique et par profession (<i>pagine bianche</i> / <i>pagine gialle</i>), sont livrés à domicile accompagnés d’une brochure intitulée « Tutto città » qui contient de nombreux renseignements utiles.</p>
<h4 class="spip">Téléphonie fixe, mobile et Internet</h4>
<p>Ci-après une liste, non exhaustive, des principaux fournisseurs qui proposent des offres fixe + mobile + internet :</p>
<ul class="spip">
<li><a href="http://www.telecomitalia.it/" class="spip_out" rel="external">TELECOMITALIA</a> : en composant le 187 (24h/24h) vous obtiendrez tous les renseignements concernant l’installation du téléphone ;</li>
<li><a href="http://www.wind.it/" class="spip_out" rel="external">WIND</a></li>
<li><a href="http://www.fastweb.it/" class="spip_out" rel="external">FASTWEB</a></li>
<li><a href="http://www.vodafone.com/" class="spip_out" rel="external">VODAFONE</a></li>
<li><a href="http://www.tre.it/" class="spip_out" rel="external">TRE</a></li></ul>
<h4 class="spip">Utilisation de votre portable français</h4>
<p>Vous pouvez utiliser votre téléphone portable partout en Europe et dans de nombreuses autres régions du monde, grâce à la norme technique GSM de l’Union européenne. Les personnes qui utilisent leur téléphone portable à l’étranger réalisent d’importantes économies – de l’ordre de 60 % – grâce au règlement de l’UE sur l’itinérance (« roaming »), qui plafonne le coût des appels téléphoniques effectués ou reçus dans un autre pays de l’UE (« eurotarif »). De plus, une facturation à la seconde a été introduite après les premières 30 secondes pour les appels effectués, et dès la première seconde pour les appels reçus. Le coût d’envoi d’un SMS depuis l’étranger a lui aussi nettement diminué, il est désormais de 11 cents (hors TVA) maximum.</p>
<p>Les opérateurs sont libres de proposer des tarifs inférieurs. Il est donc recommandé de comparer les offres.</p>
<p>Les clients reçoivent un message automatique les informant du coût des appels, des SMS et des services de transmission de données à l’étranger (en cas de navigation sur internet ou de téléchargement d’un film, par ex.). Les prix de gros que les opérateurs se facturent mutuellement sont également plafonnés, ce qui devrait réduire les coûts supportés par les consommateurs. Un mécanisme a également été mis en place afin d’éviter aux consommateurs de devoir payer des factures astronomiques pour des services d’itinérance : après un premier avertissement, la connexion internet mobile est suspendue dès que le montant atteint un certain niveau. Une limite de 50 euros par mois s’applique par défaut si le client n’a pas défini un montant spécifique.</p>
<p>Vous trouverez sur un site web de l’UE (<a href="http://ec.europa.eu/information_society/activities/roaming/index_en.htm" class="spip_out" rel="external">Ec.europa.eu</a>, en anglais) les eurotarifs appliqués par les opérateurs des pays membres, ainsi que les liens vers leurs sites. Vous pourrez également y comparer les tarifs pratiqués pour l’envoi de SMS ou l’utilisation de services de données.</p>
<p>Source : <a href="http://europa.eu/travel/comm/index_fr.htm#phone" class="spip_out" rel="external">Europa.eu</a></p>
<h4 class="spip">Téléphoner gratuitement par Internet </h4>
<p>Il est généralement très avantageux d’utiliser <strong>des logiciels de téléphonie sur IP</strong> (Skype, Google talk, Live messenger, Yahoo ! messenger, etc.) car les tarifs proposés à l’international sont souvent moindre que ceux des opérateurs locaux exploitant des lignes classiques, voire gratuits dans certaines conditions.</p>
<h3 class="spip"><a id="sommaire_2"></a>Poste</h3>
<p>Si vous devez envoyer un courrier urgent, la poste italienne fournit un service de courrier rapide (<i>Posta Celere</i>) dans la plupart des postes, avec une garantie de livrer dans un délai de 24 à 48 heures ; pour les envois à l’étranger, cela prend généralement deux à trois jours vers les pays européens et quatre à cinq jours vers le reste du monde.</p>
<p>Les adresses des bureaux de poste sont disponibles sur le site <a href="http://www.poste.it/" class="spip_out" rel="external">Poste.it</a>. Les timbres (<i>francobolli</i>) s’achètent à la poste centrale et dans la plupart des bureaux de tabac et dans certains kiosques à journaux.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/vie-pratique/article/communications-109951). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
