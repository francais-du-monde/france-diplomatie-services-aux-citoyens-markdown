# Animaux domestiques

<p>Depuis le 3 juin 2004, l’Union européenne a adopté le " système du passeport " pour les animaux domestiques. Il permet aux ressortissants d’un certain nombre de pays, dont la France, de faire venir en Irlande des chiens ou des chats accompagnés, sans besoin de quarantaine sous certaines conditions :</p>
<ul class="spip">
<li>être en provenance d’un pays agréé ;</li>
<li>voyager sur une compagnie aérienne agréée. En l’absence de compagnies aériennes agréées, les conditions d’entrée des animaux domestiques obéissent au " système d’approbation préalable " (voir les modalités ci-dessous) ;</li>
<li>l’animal doit avoir plus de trois mois ;</li>
<li>il doit être obligatoirement accompagné de son propriétaire ou de la personne accompagnante responsable ;</li>
<li>il doit être identifié par puce électronique qui doit répondre à la norme ISO11784 ou à l’annexe A de la norme ISO11785. Si ce n’est pas le cas, le propriétaire devra fournir son propre lecteur de puce ; suite à la pose de la puce électronique, l’animal doit être vacciné contre la rage dans un pays agréé ; suite au premier vaccin antirabique (en général un mois après), une analyse de sang doit être pratiquée dans un laboratoire agréé afin de vérifier la présence d’une quantité d’anticorps neutralisants au moins égale à 0,5 IU/ml ;</li>
<li>le passeport européen de l’animal doit être complété, signé et tamponné par un vétérinaire reconnu ;</li>
<li>l’animal n’est autorisé à rentrer dans le pays que si au moins six mois se sont écoulés depuis l’analyse de sang positive. De plus, il doit être resté dans un pays agréé au cours des six derniers mois ;</li>
<li>enfin, l’animal doit recevoir un traitement contre les puces et les vers intestinaux entre 24 et 48 heures avant l’heure d’enregistrement au ferry ou à l’aéroport.Dans le cas d’un départ par avion, les transporteurs effectuent les contrôles dès l’arrivée sur le territoire irlandais. Si l’animal n’est pas en règle (notamment s’il n’a pas subi de traitement contre les puces et les vers intestinaux), il peut être placé en quarantaine pour une période de 72 heures.</li></ul>
<p>Les transporteurs effectuant les contrôles avant le départ de France, dans le cas d’un voyage par ferry, peuvent interdire le départ de l’animal en cas de manquement aux règles d’entrée en vigueur. Cependant, dans certains cas, l’animal sera autorisé à voyager, mais il sera placé directement en quarantaine à son arrivée.</p>
<p>Les coûts de transfert, de détention et de quarantaine sont à la charge du propriétaire ou de la personne accompagnante responsable de l’animal.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-douanes-article-animaux-domestiques.md" class="spip_in">Animaux domestiques</a></li>
<li><a href="http://www.agriculture.gov.ie/" class="spip_out" rel="external">Démarches à suivre pour les compagnies aériennes agréées et non agréées</a>.</li>
<li><a href="http://www.agriculture.gov.ie/" class="spip_out" rel="external">Liste des pays agrées (en anglais)</a>.</li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/entree-et-sejour/article/animaux-domestiques-110426). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
