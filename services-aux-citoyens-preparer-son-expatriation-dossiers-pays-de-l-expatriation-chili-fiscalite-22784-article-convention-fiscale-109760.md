# Convention fiscale

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/fiscalite-22784/article/convention-fiscale-109760#sommaire_1">Champ d’application de la convention</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/fiscalite-22784/article/convention-fiscale-109760#sommaire_2">Règles d’imposition</a></li></ul>
<p>La France et le Chili ont signé à Paris <strong>le 7 juin 2004</strong> une convention en vue d’éviter les doubles impositions et de prévenir l’évasion fiscale en matière d’impôts sur le revenu et sur la fortune. Cette convention est <strong>entrée en vigueur le 10 juillet 2006. </strong></p>
<p>Son texte intégral est consultable sur le <a href="http://www.impots.gouv.fr/" class="spip_out" rel="external">site Internet de l’administration fiscale</a>. Voici un résumé de ses points clés pour les particuliers :</p>
<h3 class="spip"><a id="sommaire_1"></a>Champ d’application de la convention</h3>
<p>Cette convention s’applique aux personnes résidentes en France ou au Chili ou dans les deux pays. On entend par personne résidente toute personne qui, en application des lois du pays de résidence, est assujettie à l’impôt dans ce pays en raison de son domicile ou de sa résidence. Les personnes pouvant justifier d’une résidence au Chili et en France doivent se reporter à l’article 4 de la convention pour savoir dans quel pays elles sont imposables. Par ordre de priorité ce sera le pays dans lequel la personne à son foyer d’habitation permanent, là où elle a des liens personnels et économiques les plus étroits, là où elle séjourne de façon habituelle, l’Etat dont elle possède la nationalité ou sinon, ce seront les autorités compétentes des Etats contractants qui trancheront la question dans le cadre de la procédure amiable. Les personnes exerçant une profession libérale ou toute autre activité à caractère indépendant sont considérées comme résidentes dans le pays dans lequel elles séjournent pendant une période ou des périodes excédant 183 jours par an (article 5).</p>
<h3 class="spip"><a id="sommaire_2"></a>Règles d’imposition</h3>
<p>Les impôts auxquels s’appliquent la convention sont, pour la France, les impôts sur le revenu, sur les sociétés, sur la fortune, ainsi que la taxe sur les salaires, et, pour le Chili, les impôts perçus en application de la loi relative à l’impôt sur le revenu (<i>Ley sobre impuesto a la renta</i>).</p>
<p>Pour connaître les règles d’imposition vous concernant, vous pourrez, selon votre profession, vous reporter aux articles suivants :</p>
<ul class="spip">
<li>article 14 pour les salariés ;</li>
<li>article 16 pour les artistes et les sportifs ;</li>
<li>article 17 pour les retraités ;</li>
<li>article 18 pour les fonctions publiques ;</li>
<li>article 19 pour les étudiants, les apprentis et les stagiaires.</li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/fiscalite-22784/article/convention-fiscale-109760). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
