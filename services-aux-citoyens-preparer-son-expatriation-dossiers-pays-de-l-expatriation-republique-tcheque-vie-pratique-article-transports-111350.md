# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/vie-pratique/article/transports-111350#sommaire_1">Importation de véhicule</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/vie-pratique/article/transports-111350#sommaire_2">Permis de conduire</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/vie-pratique/article/transports-111350#sommaire_3">Code de la route</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/vie-pratique/article/transports-111350#sommaire_4">Assurances et taxes</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/vie-pratique/article/transports-111350#sommaire_5">Achat et location</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/vie-pratique/article/transports-111350#sommaire_6">Immatriculation</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/vie-pratique/article/transports-111350#sommaire_7">Réseau routier</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/vie-pratique/article/transports-111350#sommaire_8">Transports en commun</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Importation de véhicule</h3>
<p>Lors de l’immatriculation, les autorités nationales sont habilitées à vérifier que la TVA a bien été payée (20%). La TVA varie selon qu’il s’agit d’une voiture <strong>neuve ou d’occasion</strong>, achetée à un <strong>vendeur professionnel</strong> ou à un <strong>particulier</strong>.</p>
<p>Pour la TVA, on considère qu’un véhicule est d’occasion s’il a été mis en circulation pour la première fois il y a plus de six mois ou s’il a parcouru plus de 6000 km.</p>
<p>Si vous achetez une voiture neuve à un <strong>concessionnaire</strong> dans un pays de l’UE, mais que vous la faites immatriculer dans un autre pays de l’UE dans lequel vous vivez ou vous emménagez, vous ne devez <strong>payer la TVA que dans votre pays de résidence</strong>.</p>
<p>Afin d’éviter de payer la TVA deux fois et de demander ensuite un remboursement, indiquez au concessionnaire que vous souhaitez faire immatriculer votre voiture dans un autre pays de l’UE.</p>
<p>Si vous achetez une voiture d’occasion à un <strong>particulier</strong>, vous <strong>ne devez pas payer la TVA</strong>, dans aucun pays.</p>
<p>Si vous achetez une voiture d’occasion à un <strong>revendeur</strong>, vous devrez payer la TVA, mais <strong>uniquement sur le bénéfice </strong>dégagé par la vente de la voiture. Cette TVA ne figurera pas sur la facture. L’achat ne sera pas soumis à la TVA dans le pays de destination.</p>
<p><i>Source : <a href="http://ec.europa.eu/youreurope/citizens/vehicles/registration/formalities/index_fr.htm" class="spip_out" rel="external">Ec.europa.eu</a></i></p>
<p>Il est donc préférable d’acheter sa voiture sur place.</p>
<p>Selon les normes locales, les véhicules doivent être équipés d’un pot catalytique. Il est interdit d’importer un véhicule de plus de huit ans d’âge. Le contrôle technique a lieu tous les deux ans.</p>
<p>Toute personne séjournant plus de 180 jours sur le territoire tchèque est tenue d’y faire immatriculer son véhicule.</p>
<p>Les formalités d’immatriculation sont :</p>
<ul class="spip">
<li>présentation d’une pièce d’identité et d’un titre de séjour</li>
<li>contrôle technique</li>
<li>contrôle anti-pollution</li>
<li>certificat de radiation du registre des véhicules du pays d’origine</li>
<li>carte grise du pays d’origine</li>
<li>certificat de conformité</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Permis de conduire</h3>
<p>Les permis de conduire français et internationaux sont reconnus en République tchèque. Depuis l’entrée du pays au sein de l’Union européenne, il n’est plus obligatoire de se faire établir un permis de conduire tchèque en échange du permis français.</p>
<h3 class="spip"><a id="sommaire_3"></a>Code de la route</h3>
<p>Le code de la route tchèque est pratiquement identique au code français en matière de sécurité (transport des enfants, ceintures de sécurité, priorité dans les ronds-points, etc…). Il faut y rajouter l’obligation d’avoir les phares allumés en plein jour toute l’année. Les routes secondaires et passages à niveau sont parfois dangereux et la signalisation parfois défectueuse.</p>
<p>La vitesse maximale autorisée est de 50 km/h en agglomération, 90 km/h sur route et 130 km/h sur autoroute (certaines sont limitées à 110 km/h). Les contrôles radar sont fréquents et les contrôles d’alcoolémie (0 mg/l) nombreux sur les routes et à la sortie des restaurants. De sévères sanctions sont appliquées : retrait de permis et amende. En cas de récidive, le conducteur est passible d’une peine de prison.</p>
<p>Les amendes pour les dépassements de vitesse (moins de 20 km/h au-dessus de la limite en ville et moins de 30 km/h au-dessus hors agglomération) varient dans la plupart des cas entre 500 et 2 000 CZK (20-70€).</p>
<p>Lors d’un contrôle policier, le chauffeur est tenu de présenter un passeport ou une autre pièce de d´identité (citoyens de l’UE), le permis de conduire (européen ou international) et les papiers de la voiture (carnet technique, assurance de la voiture obligatoire et carte verte).</p>
<p>En République tchèque, on observe <strong>zéro tolérance</strong> pour l´alcool et d´autres substances illicites lors de la conduite de la voiture. Si l’on détecte une de ces substances dans votre sang vous risquez jusqu’à trois ans de prison et une amende pouvant aller de 25 à 50 000 CZK (900-1800 €). Vous risquez la même amende en cas de refus d’alcootest ou de prise de sang.</p>
<p>Il est interdit de tenir en main un téléphone mobile pendant la conduite ni de le porter sur l’épaule. Le non-respect est puni d’une amende (50-90 €). Il est néanmoins permis de téléphoner en voiture avec un kit mains libres.</p>
<p>Les piétons en République tchèque ont une priorité absolue sur les passages piétonniers. Les voitures sont obligées de s’arrêter et de laisser les piétons traverser la route.</p>
<p>En République tchèque il y a au total 878 km d’autoroutes et de voies rapides. Si vous voulez circuler sur l´autoroute il faut mettre sur le pare-brise de votre véhicule une <strong>vignette</strong> valide. Elle est en vente dans les stations d´essence et dans les bureaux de poste.</p>
<p>Prix d’une vignette pour les voitures de moins de 3,5 tonnes :</p>
<ul class="spip">
<li>1 semaine – 310 CZK</li>
<li>1 mois – 440 CZK</li>
<li>1 an – 1500 CZK</li></ul>
<p>Prix des vignettes pour les voitures de 3,5 à 12 tonnes : un système de péage a été mis en place depuis 2010.</p>
<p>Si vous êtes victime d’un accident dont les dommages dépassent 50 000 CZK (1800 €) ou s’il y a un blessé vous devez obligatoirement appeler la police (numéro 158) qui va établir un protocole. Les accidents doivent être réglés avec la compagnie d’assurance. Dans d´autres cas un accord à l’amiable peut avoir lieu avec les tiers.</p>
<p>Les compagnies d’assurances pourvoient des services gratuits dans toute l’Europe. Tous les numéros utiles figurent dans votre livret d’assurance. Il est donc fortement recommandé de contacter le service d’assistance de la compagnie où vous êtes assurés.</p>
<h3 class="spip"><a id="sommaire_4"></a>Assurances et taxes</h3>
<p>L’assurance au tiers est obligatoire.</p>
<p>Le coût représente environ 10 000 couronnes par an (environ 400 €) mais peut varier suivant la cylindrée. Il est nécessaire de s’affilier à une compagnie d’assurance locale (nombreuses compagnies tchèques ou filiales de grands groupes internationaux).</p>
<h3 class="spip"><a id="sommaire_5"></a>Achat et location</h3>
<p>Toutes les grandes marques automobiles, françaises ou autres, sont représentées en République tchèque.</p>
<h3 class="spip"><a id="sommaire_6"></a>Immatriculation</h3>
<p><a href="http://www.france.cz/Enregistrement-de-son-vehicule-en-Republique" class="spip_out" rel="external">Consultez la page de l’ambassade de France en République tchèque</a>.</p>
<h3 class="spip"><a id="sommaire_7"></a>Réseau routier</h3>
<p>La République tchèque dispose d’un réseau routier en bon état.</p>
<h3 class="spip"><a id="sommaire_8"></a>Transports en commun</h3>
<h4 class="spip">Voie aérienne</h4>
<p>Le plus grand aéroport de République tchèque se trouve à <a href="http://www.prg.aero/" class="spip_out" rel="external">Prague-Ruzyne</a>. Les aéroports de <a href="http://www.airport-brno.cz/" class="spip_out" rel="external">Brno</a>, <a href="http://www.airport-ostrava.cz/" class="spip_out" rel="external">Ostrava</a>, <a href="http://www.airport-k-vary.cz/" class="spip_out" rel="external">Karlovy Vary</a> ou de <a href="http://www.airport-pardubice.cz/" class="spip_out" rel="external">Pardubice</a> offrent également des vols internationaux.</p>
<p>On peut se rendre à l’aéroport, de Prague, en bus, en taxi ou par ses propres moyens. L’autobus 119 part de la station de métro A - Dejvická (20 minutes de trajet), le 100 de la station de métro B – Zlicín (15 minutes). Depuis 2005, existe également un bus spécial qui part de la station Holešovice et dessert le Terminal 1 et le Terminal 2, de 5 à 22 heures chaque 30 minutes. Le trajet coûte 45 CZK.</p>
<p>La compagnie nationale d’aviation tchèque est <a href="http://www.csa.cz/" class="spip_out" rel="external">Ceské aerolinie</a>). De Prague <strong>CSA</strong> dessert la plupart des villes européennes et quelques lignes en Amérique du nord.</p>
<h4 class="spip">Voie ferroviaire</h4>
<p>La République tchèque est située au cœur de l’Europe, au carrefour d’un nœud ferroviaire qui offre de nombreuses possibilités de connexions (Euro City, Inter City, Super City). Les trains fonctionnent bien et la République tchèque fait partie des pays d’Europe où le système ferroviaire est le plus dense. La plupart des destinations sont desservies par les chemins de fers tchèques.</p>
<p>Toutes les informations sur les horaires des trains et le prix des trajets sont à consulter sur le site Internet <a href="http://www.idos.cz/" class="spip_out" rel="external">Idos.cz</a>.</p>
<p>Les <a href="http://www.cd.cz/" class="spip_out" rel="external">chemins de fer tchèques</a> ont depuis peu de nouveaux trains, les Pendolino qui relient Prague à Ostrava, Vienne et Bratislava. Ces trains à grande vitesse atteignent la vitesse de 180 km/h ce qui assure des liaisons rapides en République tchèque dotées de tout le confort.</p>
<h4 class="spip">Voie routière</h4>
<p>Les grandes villes tchèques (par exemple Prague, Brno, Ostrava) sont reliées aux autres grandes métropoles européennes par autobus. Les lignes intra République tchèque sont également assurées par des bus modernes et suivent le niveau de confort européen.</p>
<ul class="spip">
<li>Tous les horaires des transports sont disponibles sur un seul site web - <a href="http://www.idos.cz/" class="spip_out" rel="external">IDOS</a></li>
<li>Les tickets sur plus de 3000 destinations en République tchèque et pour l’étranger peuvent être <a href="http://www.amsbus.cz/" class="spip_out" rel="external">réservés en ligne</a> et ensuite être retirés dans un des bureaux de vente.</li></ul>
<h5 class="spip">En ville</h5>
<p>Toutes les grandes villes de la République tchèque disposent d’une société qui s’occupe du transport municipal (MHD). Aux arrêts vous trouverez les horaires détaillés des lignes. Sur les panneaux d’horaires vous trouverez aussi la durée du trajet entre les différents arrêts.</p>
<p>Les voyageurs dans Prague peuvent emprunter le bus, le tram ou les trois lignes de métro. Le billet de correspondance est valable pendant 30 minutes dans tous les moyens de transport (tram, bus, métro) et coûte 24 CZK. Il existe également un billet valable 90 mn pour un prix de 32 CZK. Il n´y a pas de limite de correspondances et le billet est valable pendant toute la période de validité.</p>
<p>Planifiez votre trajet à la minute près sur le site <a href="http://www.dpp.cz/idos" class="spip_out" rel="external">Dpp.cz</a>.</p>
<p>Le <strong>métro</strong> tchèque offre en totalité 54,7 km de lignes. Sur les trois lignes, qui se distinguent par des lettres et des couleurs (A – ligne verte, B – ligne jaune, C – ligne rouge) il y a 54 stations dont 3 sont des stations de correspondance (Mustek, Muzeum et Florenc).</p>
<p>Les prix des trajets en <strong>taxi </strong>à Prague sont limités par la mairie de Prague. Depuis le 1er janvier 2007 on autorise le prix maximal de 40 CZK (1,20 €) de prise en charge et 28 CZK (1 €) au kilomètre et 6 CZK par minute d´attente.</p>
<p>Des services de taxi fonctionnent sur tout le territoire de la République tchèque, surtout dans les grandes villes (Brno, Plzen, Olomouc). Les prix sont moins élevés qu´à Prague (environ de 1/3 de moins) et ne sont pas limités par les mairies.</p>
<p><i>Source : <a href="http://www.czechtourism.com/" class="spip_out" rel="external">Czechtourism.com</a></i></p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/vie-pratique/article/transports-111350). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
