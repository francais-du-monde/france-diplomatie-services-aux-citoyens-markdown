# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/vie-pratique/article/transports#sommaire_1">Importation de véhicule</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/vie-pratique/article/transports#sommaire_2">Permis de conduire</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/vie-pratique/article/transports#sommaire_3">Code de la route</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/vie-pratique/article/transports#sommaire_4">Assurances et taxes</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/vie-pratique/article/transports#sommaire_5">Achat et location</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/vie-pratique/article/transports#sommaire_6">Immatriculation</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/vie-pratique/article/transports#sommaire_7">Entretien</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/vie-pratique/article/transports#sommaire_8">Carburant</a></li>
<li><a id="so_9" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/vie-pratique/article/transports#sommaire_9">Réseau routier</a></li>
<li><a id="so_10" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/vie-pratique/article/transports#sommaire_10">Transports en commun</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Importation de véhicule</h3>
<p>L’importation d’un véhicule neuf en Afrique du Sud coûte très cher. Les droits de douane étant prohibitifs (droit de base : 42,5 % de la valeur du véhicule + droits additionnels + TVA, ce qui conduit à des droits avoisinant 85 % de la valeur du véhicule). L’importation d’un véhicule d’occasion (un an au minimum) est libre. Lors de l’importation de son véhicule personnel en Afrique du Sud l’on doit préalablement obtenir de la représentation diplomatique sud-africaine de son lieu de résidence un permis d’importation (délai de plusieurs semaines).</p>
<p>L’entrée d’un véhicule dans le pays, quand il s’agit d’une voiture d’occasion, est soumise à une inspection pour obtenir un certificat de conformité (<i>Roadworthy Certificate</i>) nécessaire à l’immatriculation. A chaque changement de propriétaire, ce certificat doit être fourni à l’acheteur au moment de l’immatriculation.</p>
<h3 class="spip"><a id="sommaire_2"></a>Permis de conduire</h3>
<p>Le permis de conduire français et le permis de conduire international sont reconnus en Afrique du Sud. Il faut toutefois s’assurer :</p>
<ul class="spip">
<li>que le permis détenu est encore valide et lisible dans une des langues officielles de l’Afrique du Sud ou bien</li>
<li>qu’il soit certifié et traduit par une autorité compétente (on peut obtenir une traduction auprès du consulat pour 14 euros)</li>
<li>que le permis de conduire présente une photographie en bon état et la signature de son détenteur</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Code de la route</h3>
<p>La liberté de circulation est totale. La conduite s’effectue à gauche et la priorité est à gauche. La vitesse maximale autorisée est de 60 km/h en agglomération, 100 km/h sur route et 120 km/h sur autoroute. Pour des raisons de sécurité, il est important de bien étudier son itinéraire afin d’éviter de se perdre. Les "Stops", parfois placés aux quatre angles d’une intersection de deux routes, requièrent une certaine vigilance (le premier à s’être arrêté repart le premier), tout comme les carrefours giratoires (la priorité n’est pas automatiquement à celui qui est déjà engagé dans le carrefour).</p>
<p>Il est interdit de téléphoner en conduisant, sauf si l’on est équipé d’un système "mains libres". En cas d’accident, les sanctions encourues sont du même type qu’en France. Le taux de mortalité dû aux accidents de la route est extrêmement élevé en Afrique du Sud.</p>
<p>Il est interdit de conduire avec un taux d’alcool pur dans le sang égal ou supérieur à 0,5 g par litre de sang.</p>
<h3 class="spip"><a id="sommaire_4"></a>Assurances et taxes</h3>
<p>Les assurances ne sont pas obligatoires, mais l’assurance au tiers (<i>Third Party</i>) est conseillée.</p>
<p>Les primes varient en fonction des marques et des modèles.</p>
<h3 class="spip"><a id="sommaire_5"></a>Achat et location</h3>
<p>De nombreux constructeurs sont représentés en Afrique du Sud : Peugeot, Renault, Audi, BMW, Ford, Honda, Mazda, Mercedes, Nissan, Opel, Toyota, Fiat, Volkswagen, Hyundai, Mitsubishi, etc. Lors de l’achat d’un véhicule, veillez à ce qu’il soit doté d’une climatisation en raison du climat et de l’insécurité (fermeture des fenêtres en ville). Il n’est pas nécessaire de disposer d’un 4x4.</p>
<p>Il y a un bon marché de l’occasion, mais les véhicules proposés ont, en règle générale, un kilométrage élevé (plus de 100 000 kms). Les prix sont très variables en fonction des marques et des modèles. Il existe un guide de l’Argus mensuel dont disposent les garages.</p>
<p>On peut louer des véhicules dans les agences de location (Avis, Hertz, Rentacar). Les prix sont variables, entre 100 et 300 rands par jour, plus kilométrage et assurance. Demander l’imprimé à utiliser en cas d’éventuel constat d’accident.</p>
<h4 class="spip">Achat d’un véhicule</h4>
<p>De nombreux concessionnaires sont installés aux abords des villes. Quelques sites internet sont spécialisés dans les annonces de véhicules d’occasion :</p>
<ul class="spip">
<li><a href="http://www.autotrader.co.za/" class="spip_out" rel="external">Autotrader.co.za</a></li>
<li><a href="http://www.cars.co.za/" class="spip_out" rel="external">Cars.co.za</a>.</li></ul>
<h3 class="spip"><a id="sommaire_6"></a>Immatriculation</h3>
<p>Le coût de la vignette est fonction du modèle du véhicule. Il n’y a pas de carte grise.</p>
<h3 class="spip"><a id="sommaire_7"></a>Entretien</h3>
<p>L’entretien des véhicules est assuré sur place sans problème. Les pièces de rechange pour Peugeot et Renault sur les modèles distribués localement sont, en général, disponibles rapidement. Pour des modèles moins fréquents, les délais peuvent être très longs (importation de France).</p>
<h3 class="spip"><a id="sommaire_8"></a>Carburant</h3>
<p>La plupart des stations-services disposent d’une centrale de paiement par carte bancaire.</p>
<p>Dans les régions rurales, les points de vente d’essence sont parfois très éloignés les uns des autres, il est donc conseillé de toujours rouler avec un réservoir plein à moitié.</p>
<h3 class="spip"><a id="sommaire_9"></a>Réseau routier</h3>
<p>Le réseau routier de 200 000 km (dont 85 000 km goudronnés) est en excellent état excepté dans certaines régions rurales. Il existe un réseau routier secondaire constitué de pistes bien entretenues.</p>
<h3 class="spip"><a id="sommaire_10"></a>Transports en commun</h3>
<h4 class="spip">Avion</h4>
<p>Il existe de nombreuses liaisons intérieures (Le Cap, Durban, Richard’s Bay, Bloemfontein, Kimberley, etc.). Le pays dispose de trois aéroports internationaux et de neuf aéroports principaux.</p>
<p>Quelques compagnies <i>lowcost</i> sont présentes : <a href="http://www.kulula.com" class="spip_out" rel="external">Kulula</a>, <a href="http://www.flymango.com/" class="spip_out" rel="external">Mango</a>).</p>
<h4 class="spip">Train</h4>
<p>Le réseau ferroviaire est peu développé et les trains circulent à vitesse réduite. Par exemple, on voyage plus de 20 heures pour faire le trajet Johannesburg-Le Cap. Il est souvent déconseillé de choisir le train comme moyen de transport pour des raisons de sécurité.</p>
<h4 class="spip">Transports urbains</h4>
<p>Les transports urbains sont sous-développés. Seuls quelques taxis individuels (coût : 3 rands/km, généralement négociables) et collectifs (auxquels ont surtout recours les populations moins fortunées ; accidents fréquents) sont disponibles. Il n’existe pas de système de bus urbain, ni de métro ou tramway. Il existe un système de liaisons entre les principales villes par autocar.</p>
<p>Depuis 2010, le <i>Gautrain</i>, train régional urbain sûr et rapide, relie Johannesburg et Pretoria.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/vie-pratique/article/transports). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
