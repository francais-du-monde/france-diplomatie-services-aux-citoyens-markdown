# Scolarisation

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/vie-pratique/article/scolarisation#sommaire_1">Les établissements scolaires français en Autriche</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/vie-pratique/article/scolarisation#sommaire_2">Enseignement supérieur</a></li></ul>
<p>Pour toute information sur la scolarisation dans le système français à l’étranger, vous pouvez consulter nos thématiques sur les <a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures.md" class="spip_in">Etudes supérieures</a> et la <a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais.md" class="spip_in">Scolarité en français</a>.</p>
<h3 class="spip"><a id="sommaire_1"></a>Les établissements scolaires français en Autriche</h3>
<p>Il existe en Autriche un établissement scolaire français, le Lycée français de Vienne, établissement dit « à gestion directe » par l’AEFE. Cet établissement public français, payant, mais disposant d’un système de bourses sur critères sociaux, permet de scolariser des enfants de la maternelle à la terminale, et même au-delà, puisqu’il abrite aussi d’une classe préparatoire économique. De plus amples renseignements peuvent être trouvés sous le lien suivant :</p>
<p><a href="http://www.aefe.fr/" class="spip_out" rel="external">Agence pour l’enseignement français à l’étranger</a>.</p>
<p>Pour les enfants germanophones, il est également possible de suivre une scolarité dans le système scolaire autrichien. Celui-ci comporte des établissements privés et publics.</p>
<p>Dans le Vorarlberg et à Vienne, des associations dispensent un enseignement complémentaire de français aux enfants inscrits dans le système autrichien :</p>
<p><a href="http://flam-vienne.at/" class="spip_out" rel="external">Verein FLAM Vienne</a><br class="manualbr">c/o Jean-Baptiste Agnès/ p.a. Aktuell Raiffeisen<br class="manualbr">F. W. Raiffeisen-Platz 1, A-1020 Wien<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/vie-pratique/article/scolarisation#jean_baptiste.agnes#mc#chello.at#" title="jean_baptiste.agnes..åt..chello.at" onclick="location.href=mc_lancerlien('jean_baptiste.agnes','chello.at'); return false;" class="spip_mail">Courriel</a>, <br class="manualbr">Tel. (+43) 699 12635854</p>
<p>Enfin, pour ceux qui sont trop éloignés du lycée français, il est toujours possible de suivre un enseignement à distance via le CNED, mais celui-ci ne dispense pas d’être inscrit dans le système national autrichien.</p>
<h3 class="spip"><a id="sommaire_2"></a>Enseignement supérieur</h3>
<p>Si un étudiant français souhaite poursuivre ou commencer ses études en Autriche</p>
<h4 class="spip">En français</h4>
<ul class="spip">
<li>le site de la <a href="http://www.fied.fr/fr/index.html" class="spip_out" rel="external">FIED (Fédération Interuniversitaire de l’Enseignement à Distance)</a> propose comme son nom l’indique, différents parcours universitaires qu’il est possible de suivre à distance et ce, notamment via l’espace Telesup ;</li>
<li>L’enseignement à distance est également possible via le <a href="http://www.cned.fr/" class="spip_out" rel="external">CNED</a>.</li></ul>
<h4 class="spip">En allemand</h4>
<p>Il est nécessaire de s’assurer de l’équivalence des diplômes. Vous trouverez les informations qui s’appliquent à votre pays dans la partie Diplômes (équivalence) du site du <a href="http://www.ciep.fr/enic-naricfr/" class="spip_out" rel="external">centre Enic-Naric</a> (vous devez vous adresser au centre NARIC d’Autriche).</p>
<p>Néanmoins, il est souvent préférable de s’adresser directement aux établissements concernés.</p>
<p>L’enseignement supérieur autrichien comprend :</p>
<ul class="spip">
<li>Les universités avec le même schéma qu’en France LMD</li>
<li>Les <i>Fachhochschulen</i></li>
<li>Les universités privées</li></ul>
<p>Pour plus de détails et la listes des établissements :</p>
<p>Consultez le site de l’<a href="http://institut-francais.at/vienne/fr/sciences-universites/etudier-en-autriche/etudes-en-autriche.html" class="spip_out" rel="external">Institut français de Vienne</a>.</p>
<p>Pour une recherche par domaine : <a href="http://www.studienwahl.at/" class="spip_out" rel="external">Studienwahl.at</a></p>
<p>Pour avoir accès à l’enseignement supérieur autrichien, comme en France, le baccalauréat est un minimum requis. De plus il est souvent demandé d’avoir un niveau d’allemand suffisant.</p>
<p>Plusieurs bourses sont proposées notamment sur le site de l’<a href="http://www.oead.ac.at" class="spip_out" rel="external">OeAd</a>.</p>
<p><i>Mise à jour : février 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/vie-pratique/article/scolarisation). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
