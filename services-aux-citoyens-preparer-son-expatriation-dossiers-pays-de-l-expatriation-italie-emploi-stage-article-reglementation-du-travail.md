# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/emploi-stage/article/reglementation-du-travail#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/emploi-stage/article/reglementation-du-travail#sommaire_2">Contrat de travail – spécificités</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/emploi-stage/article/reglementation-du-travail#sommaire_3">Fêtes légales</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/emploi-stage/article/reglementation-du-travail#sommaire_4">Emploi du conjoint</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<p>La durée légale du temps de travail est fixée à 40 heures par semaine. Il s’agit là d’un seuil à partir duquel sont calculées les heures de travail supplémentaires. Le temps de travail hebdomadaire ne peut dépasser 48 heures, y compris les heures de travail supplémentaires.</p>
<p>En règle générale, chaque période de travail donne droit à deux jours et demi de congés payés dès le premier mois de travail. La quantité de congés payés dépend aussi des contrats collectifs nationaux. Le droit à une période de congés annuels d’une durée minimale de quatre semaines est reconnu.</p>
<p>Les employeurs ont l’obligation d’assujettir leurs salariés au régime obligatoire de la sécurité sociale. L’organisme compétent est l’Istituto Nazionale della Previdenza Sociale (INPS). La protection sociale des employés comporte la retraite, la prise en charge des indemnités journalières de maladie ou de congé de maternité et le chômage. Elle est financée par les cotisations versées conjointement par les employeurs et les salariés.</p>
<p>Le système d’imposition en vigueur en Italie est généralement celui de la retenue à la source. L’impôt sur le revenu correspond à l’Imposta sul Reddito delle Persone Fisiche (IRPEF). La base d’imposition porte sur le traitement brut et ses accessoires dont est retiré la part des cotisations sociales incombant au salarié.</p>
<p>En fin de contrat (licenciement, démission, retraite) une somme égale à la moyenne du salaire perçu multiplié par le nombre d’années travaillées est versé au salarié, qu’il ait eu un contrat à durée déterminé ou indéterminé. Ce paiement s’appelle la <i>liquidazione</i>.</p>
<p><strong>La législation en matière sociale fait l’objet d’ajustements fréquents. Il est vivement recommandé de consulter les sites mentionnés ci-dessous.</strong></p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li>Site <a href="https://ec.europa.eu/eures/main.jsp?countryId=IT&amp;acro=lw&amp;lang=fr&amp;parentId=0&amp;catId=0&amp;regionIdForAdvisor=&amp;regionIdForSE=&amp;regionString=IT0%7C%20:" class="spip_out" rel="external">Eures Italie</a> ;</li>
<li>Site du <a href="http://www.cnel.it/" class="spip_out" rel="external">Conseil national de l’économie et du travail</a> (CNEL).</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Contrat de travail – spécificités</h3>
<h4 class="spip">Cadre du contrat : détachement ou expatriation</h4>
<p>Deux cas de figure peuvent se présenter. Le contrat de travail peut être fait soit dans le cadre d’un détachement, soit dans le cadre d’une expatriation.</p>
<p>Dans le cadre du <strong>détachement</strong>, l’entreprise emploie un salarié en France puis le détache à l’étranger, pour une période variable mais limitée, au terme de laquelle l’employé retrouve son poste en France. Ce contrat sera soumis au droit français.</p>
<p>Dans le cadre de l’<strong>expatriation</strong>, le salarié est recruté soit en France soit à l’étranger pour le compte d’une entreprise implantée à l’étranger ou d’une société locale. Les conditions de négociation ne seront pas les mêmes selon que l’employeur fait venir le salarié de France ou qu’il l’engage selon les modalités d’un contrat de travail local. Dans le premier cas, si les parties en décident ainsi, le contrat pourra être soumis au droit français. S’il s’agit d’un contrat local, les relations de travail seront régies par le droit local.</p>
<h4 class="spip">Types de contrats de travail en droit italien</h4>
<p>En <strong>droit italien</strong>, les principaux contrats de travail sont le contrat à durée indéterminée, le contrat à durée déterminée, le contrat temporaire, le contrat à temps partiel, le contrat d’apprentissage ou formation et le contrat de collaboration.</p>
<p>Toutefois il faut garder en vue que pour tout type de travail salarié et pour toute catégorie de travailleurs, ce sont les contrats collectifs nationaux (<i>contratti collectivi nazionali di lavoro</i>) qui définissent et régissent les droits et devoirs des salariés et des employeurs, ainsi que les rémunérations à respecter. Pour connaître les différents contrats collectifs nationaux, vous pouvez consulter le site du <a href="http://www.cnel.it/" class="spip_out" rel="external">Conseil national de l’économie et du travail (Consiglio Nazionale del Economia e del Lavoro – CNEL)</a>.</p>
<h5 class="spip">Le contrat à durée indéterminée</h5>
<p> <i>(contratto a tempo indeterminato)</i></p>
<p>Qu’il soit à temps partiel ou à temps plein, est un contrat classique, sans échéance. Il peut prévoir une période initiale d’essai à condition que ce soit précisé dans la lettre d’embauche. Pendant la période d’essai chacune des deux parties peut rompre le contrat de travail sans donner de préavis. Une fois la période passée, l’embauche devient définitive et assujettie à ce qui est prévu sur le contrat collectif national.</p>
<h5 class="spip">Le contrat à durée déterminée</h5>
<p> <i>(contratto a termine)</i></p>
<p>Comporte une échéance fixée par écrit d’un maximum de trois ans. Il peut être à temps partiel ou à temps plein. Il ne peut être prorogé qu’une seule fois, à son échéance pour une durée ne dépassant pas celle fixée dans le contrat initial et à condition que la durée totale de l’emploi n’excède pas trois ans. Un salarié embauché pour une durée déterminée bénéficie des droits sociaux dus à tous conformément au contrat collectif national.</p>
<h5 class="spip">Le contrat temporaire</h5>
<p> <i>(lavoro interinale)</i></p>
<p>Lie trois parties : l’entreprise utilisatrice, le salarié et l’agence de travail temporaire. C’est en général une bonne opportunité pour un premier contact avec le travail et offre la possibilité d’être embauché en contrat à durée indéterminée par l’entreprise utilisatrice.</p>
<h5 class="spip">Le contrat de travail à temps partiel </h5>
<p> <i>(lavoro part-time)</i></p>
<p>Etabli un rapport de travail à horaire réduit (inférieur à 40h/semaine), pour des périodes prédéterminées dans une semaine, un mois ou dans l’année.</p>
<h5 class="spip">Le contrat d’apprentissage</h5>
<p> <i>(contratto di apprendistato)</i></p>
<p>Concerne des jeunes ayant entre 18 et 29 ans. L’apprentissage peut être effectué dans toutes les entreprises pour une durée de deux et six ans, avec une obligation de 120 heures de formation par an.</p>
<h5 class="spip">Le contrat de formation travail</h5>
<p> <i>(contratto di formazione-lavoro)</i></p>
<p>Il s’agit d’un contrat à durée déterminée comprise entre 12 et 24 mois non renouvelables avec une formation de 20 à 140 heures. Ce contrat prévoit l’indemnisation d’une partie de la formation pour l’employé et des abattements sur les cotisations fiscales pour l’employeur. Il concerne les travailleurs de 16 à 32 ans.</p>
<h5 class="spip">Le contrat de collaboration</h5>
<p>Egalement appelé co-co-co (<i>collaborazione coordinata e continuativa</i>) est un contrat atypique, ne correspondant ni à un travail salarié (dependenti) ni à un travail indépendant (indipendenti). Il s’agit d’une prestation continue, à caractère personnel, inséré dans un programme d’entreprise, exercée sous l’ordre et la coordination de l’employeur, mais sans lien avec le salariat. La loi de finances de 2007 (n°296/06) contient des dispositions visant à améliorer la couverture sociale des travailleurs bénéficiant de ce type de contrat. Cette forme de contrat est de plus en plus répandue, les entreprises y trouvant un moyen de diminuer le coût du travail.</p>
<h3 class="spip"><a id="sommaire_3"></a>Fêtes légales</h3>
<ul class="spip">
<li>1er janvier (Nouvel an)</li>
<li>6 janvier (Epiphanie)</li>
<li>Lundi de Pâques</li>
<li>25 avril (anniversaire de la Libération, Fête nationale)</li>
<li>1er Mai (fête du travail)</li>
<li>2 juin (Fête de la République)</li>
<li>29 juin (à Rome uniquement : Saint-Pierre, Saint Patron de la ville de Rome)</li>
<li>15 août (Assomption)</li>
<li>1er novembre (Toussaint)</li>
<li>7 décembre (à Milan uniquement, Saint-Ambroise, Saint Patron de la ville de Milan)</li>
<li>8 décembre (Immaculée Conception)</li>
<li>25 décembre (Noël)</li>
<li>26 décembre (Saint-Etienne)</li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Emploi du conjoint</h3>
<p>S’il n’y a, en théorie, pas d’obstacle administratif, les possibilités réelles sont limitées, surtout dans le Mezzogiorno où le chômage est important.</p>
<p>Il est conseillé de prendre contact avec la <a href="http://www.chambre.it/" class="spip_out" rel="external">Chambre française de commerce et d’industrie en Italie</a>, à Milan, laquelle gère le <a href="http://impiego.chambre.it/defaultfr.asp" class="spip_out" rel="external">service de l’emploi</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/emploi-stage/article/reglementation-du-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
