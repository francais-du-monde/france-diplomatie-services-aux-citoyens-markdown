# Victimes à l’étranger

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/infos-pratiques/assistance-aux-francais/victimes-a-l-etranger-20988/#sommaire_1">Être victime à l’étranger</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/infos-pratiques/assistance-aux-francais/victimes-a-l-etranger-20988/#sommaire_2">Vol, agression, attentat</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Être victime à l’étranger</h3>
<p>Les personnes se sentent souvent démunies lorsqu’elles sont victimes d’un agissement délictueux ou d’un accident sérieux à l’étranger. En tout état de cause, les dispositifs d’aide et de recours y sont plus difficilement identifiables et accessibles.</p>
<p>Les autorités françaises entendent cependant apporter une assistance adaptée à l’ensemble de leurs ressortissants, qu’ils soient victimes à titre individuel ou dans le cadre d’un événement collectif.</p>
<p>Le guide « Être victime à l’étranger », issu d’une réflexion commune menée par le ministère des Affaires étrangères et du Développement international et le ministère de la Justice, a pour objectif de vous aider dans vos premières démarches. Organisé sous forme de fiches thématiques synthétiques, il aborde tout particulièrement les spécificités liées à la survenance d’un fait à l’étranger.</p>
<p>Il vient ainsi compléter le guide « Les droits des victimes » qui décrit les procédures tant pénales que d’indemnisation en France.</p>
<p>Le droit étant un domaine susceptible d’évolution, le guide « Être victime à l’étranger » n’est pas exhaustif. Il vous présente l’état du droit au moment de sa dernière rédaction.</p>
<p><strong>En savoir plus</strong></p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/_version_word_finale_tiree_du_PDF_valide__cle4ed49f.pdf" class="spip_in" type="application/pdf">Télécharger l’intégralité du guide "Etre victime à l’étranger. Conseils, démarches et droits" (juillet 2014) au format imprimable (PDF, 351 ko)</a></li>
<li><a href="http://www.vos-droits.justice.gouv.fr/" class="spip_out" rel="external">Accéder à la rubrique « Aide aux victimes » du Site internet du Ministère de la Justice</a></li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Vol, agression, attentat</h3>
<p><strong>En cas de vol :</strong></p>
<ul class="spip">
<li>Établissez la liste de ce qui vous a été volé.</li>
<li>Portez plainte auprès des autorités de police locales (n’hésitez pas à demander l’aide du consulat de France qui pourra, si besoin, vous accompagner dans les démarches).</li>
<li>Contactez votre assurance afin de lui déclarer le sinistre dont vous avez été victime. Votre compagnie d’assurance pourra, si cela est prévu dans votre contrat, vous apporter une aide de première nécessité.</li>
<li>Informez le consulat des circonstances et modalités du vol et déclarez-lui le vol éventuel de vos papiers d’identité.</li>
<li>Contactez :</li>
<li>votre banque afin de faire opposition sur votre carte bancaire ;</li>
<li>la compagnie aérienne pour les billets d’avion.</li></ul>
<p><strong>En cas d’agression :</strong></p>
<ul class="spip">
<li>Consultez un médecin en cas de nécessité.</li>
<li>Portez plainte auprès des autorités de police locales.</li>
<li>Prévenez le consulat de France et informez le de façon précise des circonstances et modalités de l’agression.</li></ul>
<p><strong>En cas d’agression sexuelle :</strong></p>
<ul class="spip">
<li>Consultez un médecin en urgence, le plus rapidement possible après l’agression, sans vous laver ni vous changer (le consulat dispose de listes de médecins, d’hôpitaux, services d’urgence…).</li>
<li>Le médecin constatera le viol et prescrira des prophylaxies jugées nécessaires en fonction des risques de contamination (sida et autres MST). Dans certains cas, il pourra aussi faire les prélèvements utiles à la Police et à la Justice.</li>
<li>Contactez les autorités de police locales afin de déposer plainte.</li>
<li>Sollicitez l’aide du consulat pour être orienté vers un psychologue ou vers un centre d’aide aux personnes violées. Ce soutien psychologique est tout aussi essentiel que le traitement médical. A votre retour en France, il peut vous être apporté par l’<a href="http://www.inavem.org/" class="spip_out" rel="external">INAVEM</a>.</li></ul>
<p><strong>Si l’un de vos proches est victime d’un meurtre à l’étranger :</strong></p>
<ul class="spip">
<li>Portez plainte auprès des autorités locales. Le consulat peut vous communiquer une liste d’avocats francophones, si vous désirez suivre la procédure ou vous constituer partie civile au procès. Les frais d’honoraires ne sont pas pris en charge par le Consulat.</li>
<li>Le consulat peut prévenir la famille si celle-ci est en France et prendre contact avec les autorités locales pour obtenir les rapports de police.</li>
<li>Le consulat conseillera la famille pour les démarches administratives (actes de décès, rapatriement, inhumation…).</li></ul>
<p><strong>En savoir plus</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/infos-pratiques/assistance-aux-francais/deces-a-l-etranger/article/deces-a-l-etranger" class="spip_in">Décès à l’étranger</a></p>
<p><br class="autobr"><strong>Actions en France :</strong></p>
<ul class="spip">
<li>Dans tous les cas d’agression à l’étranger, la victime ou son avocat, ou ses ayants-droits (en cas de décès de la victime), peuvent déposer un dossier d’indemnisation auprès de la Commission d’Indemnisation des Victimes d’Infraction (CIVI) du Tribunal de Grande Instance de Paris (4 boulevard du Palais 75001 PARIS) ou du domicile de la victime en France. La CIVI décide en toute indépendance de la suite à réserver aux dossiers qui lui sont transmis.</li>
<li>En cas d’attentats, la victime, ou ses ayants-droits, peuvent transmettre une demande d’indemnisation au Fonds de Garantie des Victimes d’Actes de Terrorisme et Autres Infractions (FGTI, 64 rue Defrance 94682 VINCENNES Cedex, Tél. : 01.43.98.77.00).</li>
<li>Quelle que soit la nature de l’agression, la victime, ou ses ayants-droits, peuvent s’adresser à l’<a href="http://www.inavem.org/" class="spip_out" rel="external">Institut National d’Aide aux Victimes et de Médiation</a> (INAVEM, 1 rue du Pré saint Gervais 93691 PANTIN Cedex, Tél. : 08 842 846 37, numéro d’aide ouvert 7j/7 de 9h à 21h, prix d’un appel local depuis la France métropolitaine et les DOM. Depuis l’étranger : +33 (0)1 41 83 42 08 ou par mail : 08victimes<span class="spancrypt"> [at] </span>inavem.org ) qui pourra leur accorder une assistance psychologique et juridique (informations sur ses droits, aide pour déposer le dossier d’indemnisation).</li></ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/infos-pratiques/assistance-aux-francais/victimes-a-l-etranger-20988/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
