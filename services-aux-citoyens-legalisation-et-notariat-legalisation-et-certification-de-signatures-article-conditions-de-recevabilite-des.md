# Conditions de recevabilité des actes publics par le bureau des légalisations

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/legalisation-et-notariat/legalisation-et-certification-de-signatures/article/conditions-de-recevabilite-des#sommaire_1">Les originaux</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/legalisation-et-notariat/legalisation-et-certification-de-signatures/article/conditions-de-recevabilite-des#sommaire_2">Les photocopies</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/legalisation-et-notariat/legalisation-et-certification-de-signatures/article/conditions-de-recevabilite-des#sommaire_3">Les traductions</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Les originaux</h3>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  L’acte public présenté à la légalisation doit être revêtu de la signature manuscrite et originale, du nom et de la qualité du signataire et, le cas échéant, du cachet de l’administration.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  L’acte sous seing privé présenté à la légalisation doit avoir été préalablement certifié par une mairie, un notaire -ou par la chambre de commerce et d’industrie pour les documents commerciaux et douaniers- selon la procédure suivante : signature manuscrite et originale, nom et qualité du signataire et, le cas échéant, cachet de l’administration.</p>
<p><strong>Attention - Les documents suivants ne sont recevables qu’en originaux :</strong></p>
<ul class="spip">
<li>actes d’état civil (datés de moins de trois mois)</li>
<li>actes notariés</li>
<li>extraits du casier judiciaire (de moins de six mois) - <a href="https://www.cjn.justice.gouv.fr/cjn/b3/eje20" class="spip_out" rel="external">Cliquer ici pour en faire la demande</a></li>
<li>extraits K-Bis (datés de moins de trois mois)</li>
<li>certificat de nationalité française</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Les photocopies</h3>
<p>Un acte présenté en copie doit impérativement être certifié conforme à l’original (certification apposée au recto de l’acte par la mairie du domicile ou un notaire ou, le cas échéant, directement par l’administration qui a délivré l’acte présenté).</p>
<p><strong>ATTENTION - Pour les diplômes, seule une copie certifiée conforme peut être légalisée.</strong></p>
<h3 class="spip"><a id="sommaire_3"></a>Les traductions</h3>
<p>Conformément à l’article 7 du décret précité sur la légalisation, les actes publics sont rédigés en français.</p>
<p>Les actes sous seing privé présentés en langue étrangère doivent être accompagnés d’une traduction en français effectuée par un traducteur assermenté auprès d’une Cour d’Appel française (NB : sur l’acte traduit et sa version en langue étrangère doivent apparaître le nom du traducteur, sa signature, son cachet et son n°d’enregistrement - <i>ne varietur</i>).</p>
<p>S’agissant des légalisations effectuées à l’étranger, il convient de prendre directement l’attache des missions diplomatiques et consulaires françaises pour s’assurer de l’assermentation des traducteurs auprès des autorités locales.</p>
<p><a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/article/annuaire-des-representations-francaises-a-l-etranger" class="spip_in">Coordonnées des ambassades et consulats français</a></p>
<p><strong>Attention : </strong>La qualité d’expert traducteur près la Cour d’Appel ne saurait être assimilée au statut de représentant de l’Etat. C’est la raison pour laquelle leur signature apposée sur les traductions devra faire l’objet d’une légalisation auprès d’une mairie, d’un notaire ou d’une chambre de commerce.</p>
<p><i>En vertu des dispositions de l’article L-2122-30 du code général des collectivités territoriales, les traducteurs assermentés ont la possibilité de faire légaliser la signature qu’ils apposent sur une traduction par le maire de leur commune (formalité gratuite). Cet acte sous seing privé devient alors un acte public et peut faire l’objet d’une légalisation par le Bureau des Légalisations</i></p>
<p><i>Mise à jour : juillet 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/legalisation-et-notariat/legalisation-et-certification-de-signatures/article/conditions-de-recevabilite-des). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
