# Logement

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/vie-pratique/article/logement-113154#sommaire_1">Où se loger ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/vie-pratique/article/logement-113154#sommaire_2">Prix des loyers (fourchette de prix en USD)</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/vie-pratique/article/logement-113154#sommaire_3">Conditions de location</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/vie-pratique/article/logement-113154#sommaire_4">Electricité</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/vie-pratique/article/logement-113154#sommaire_5">Electroménager</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Où se loger ?</h3>
<p><strong>Principaux quartiers résidentiels</strong></p>
<ul class="spip">
<li><strong>Atlanta</strong> : au centre et nord (Midtown, Buckhead, Virginia Highland) et en banlieue dans le Nord (Sandy Springs, Roswell, Marietta) l’Est (Decatur) ;</li>
<li><strong>Boston</strong> : <i>Downtown</i> (Beacon Hill, Southend, Black Bay) et en banlieue (Arlington, Belmont, Cambridge, Lexington, Natick, Newton, Brookline, Allston, Brighton) ;</li>
<li><strong>Chicago</strong> : au nord du centre-ville de Chicago (Glod Coast, Lincoln Park, Lakeview, Wrigleyville) et la banlieue Nord (Evanston, Wilmette) ;</li>
<li><strong>Houston</strong> : de part et d’autre d’Uptown et de Galleria ainsi que dans l’ouest de la ville où logent de nombreux Français ;</li>
<li><strong>Los Angeles</strong> : Partie ouest de la ville : West Hollywood, Bel Air, Santa Monica, Westwood, Beverly Hills Pacific Palisades, Brentwood, Venice, West Los Angeles, Culver city, Hollywood ;</li>
<li><strong>Miami</strong> : sud du centre ville (Coconut Grove, Coral Gables, Kendall, Pinecrest), Miami Beach ;</li>
<li><strong>New York</strong> : dans Manhattan (à l’exception de Harlem), ainsi que dans certains quartiers de Queens (Astoria, Forest Hills) et de Brooklyn (Brooklyn Heights, Park Slope) ;</li>
<li><strong>La Nouvelle-Orléans</strong> : Garden District, Uptown, Lake Front, Mid City et la banlieue nord (Old Metairie) ;</li>
<li><strong>San Francisco</strong> : La partie nord/nord-est de la ville et dans la Bay Area, où il existe dans chaque ville des quartiers résidentiels, en particulier au cœur de la Silicon Valley ;</li>
<li><strong>Washington</strong> : dans le district de Columbia (centre ville de Washington, Georgetown, Friendship Heights), dans le Maryland (Bethesda, Chevy Chase et Potomac) et en Virginie (Mc Lean, Arlington et Vienna).</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Prix des loyers (fourchette de prix en USD)</h3>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Ville</strong> (<i>bon quartier</i> <strong> – très bon quartier</strong>)</td>
<td><strong>Studio</strong></td>
<td><strong>3 pièces</strong></td>
<td><strong>5 pièces</strong></td>
<td><strong>Villa</strong></td></tr>
<tr class="row_even even">
<td><strong>Atlanta</strong></td>
<td><i>1200</i>
<p><strong>1600</strong></p>
</td>
<td><i>1800</i>
<p><strong>2500</strong></p>
</td>
<td><i>3000</i>
<p><strong>6000</strong></p>
</td>
<td><i>3000</i>
<p><strong>6000</strong></p>
</td></tr>
<tr class="row_odd odd">
<td><strong>Houston</strong></td>
<td><i>1200</i>
<p><strong>1600</strong></p>
</td>
<td><i>1500</i>
<p><strong>2500</strong></p>
</td>
<td>-
<p>-</p>
</td>
<td><i>2500</i>
<p><strong>5000</strong></p>
</td></tr>
<tr class="row_even even">
<td><strong>New York</strong></td>
<td><i>2500 </i>
<p><strong>4000 </strong></p>
</td>
<td><i>3300 </i>
<p><strong>6100</strong></p>
</td>
<td>-
<p>-</p>
</td>
<td>-
<p>-</p>
</td></tr>
<tr class="row_odd odd">
<td><strong>La Nouvelle-Orléans</strong></td>
<td><i>1000 </i>
<p><strong>1500</strong></p>
</td>
<td><i>1800 </i>
<p><strong>2500</strong></p>
</td>
<td>-
<p>-</p>
</td>
<td><i>2500</i>
<p><strong>4000</strong></p>
</td></tr>
<tr class="row_even even">
<td><strong>Los Angeles</strong></td>
<td><i>1450 </i>
<p><strong>2300 </strong></p>
</td>
<td><i>2250 </i>
<p><strong>3200 </strong></p>
</td>
<td><i>4550 </i>
<p><strong>5550 </strong></p>
</td>
<td><i>5000</i>
<p><strong>6500</strong></p>
</td></tr>
<tr class="row_odd odd">
<td><strong>San Francisco</strong></td>
<td><i>2500 </i>
<p><strong>3000 </strong></p>
</td>
<td><i>3100 </i>
<p><strong>4300 </strong></p>
</td>
<td><i>5200 </i>
<p><strong>7000 </strong></p>
</td>
<td><i>7000 </i>
<p><strong>11 000 </strong></p>
</td></tr>
<tr class="row_even even">
<td><strong>Washington</strong></td>
<td><i>1950 </i>
<p><strong>2300 </strong></p>
</td>
<td><i>2900 </i>
<p><strong>3250 </strong></p>
</td>
<td>-
<p>-</p>
</td>
<td><i>4600 </i>
<p><strong>5600 </strong></p>
</td></tr>
<tr class="row_odd odd">
<td><strong>Chicago</strong></td>
<td><i>1800</i>
<p><strong>2000</strong></p>
</td>
<td><i>2400</i>
<p><strong>2600</strong></p>
</td>
<td><i>3800</i>
<p><strong>4000</strong></p>
</td>
<td><i>4200</i>
<p><strong>5300</strong></p>
</td></tr>
<tr class="row_even even">
<td><strong>Miami </strong>(<i>standard décent</i> - <strong>bon standing avec piscine</strong>)</td>
<td><i>1650</i>
<p><strong>4600</strong></p>
</td>
<td><i>2500</i>
<p><strong>3700</strong></p>
</td>
<td><i>4 000</i>
<p><strong>5000</strong></p>
</td>
<td><i>3500</i>
<p><strong>5000</strong></p>
</td></tr>
<tr class="row_odd odd">
<td><strong>Boston </strong></td>
<td><i>2300</i>
<p><strong>2500</strong></p>
</td>
<td><i>2600</i>
<p><strong>3250</strong></p>
</td>
<td><i>3500</i>
<p><strong>4200</strong></p>
</td>
<td><i>4200</i>
<p><strong>6300</strong></p>
</td></tr>
</tbody>
</table>
<p><i>Estimations 2013 (Consulats de France aux Etats-Unis)</i></p>
<p>Les montants en gras concernent les loyers des quartiers résidentiels et ceux figurant en italique correspondent aux prix pratiqués en banlieue.</p>
<p>Il convient de connaître le jargon immobilier :</p>
<ul class="spip">
<li>2 1/2 = studio</li>
<li>3 1/2 = appartement avec une chambre (one bedroom)</li>
<li>junior = appartement avec un salon et deux chambres (two bedrooms)</li>
<li>4 = appartement avec une chambre et un salon qui peut être partagé pour faire une chambre supplémentaire</li></ul>
<p><strong>Pour en savoir plus</strong>, vous pouvez consulter le site du Consulat de France, par exemple pour New York : <a href="http://www.consulfrance-newyork.org/spip.php?article975" class="spip_out" rel="external">Consulfrance-newyork.org</a>, pour Washington : <a href="http://fr.ambafrance-us.org/spip.php?article328" class="spip_out" rel="external">Ambafrance-us.org</a>.</p>
<h3 class="spip"><a id="sommaire_3"></a>Conditions de location</h3>
<p><strong>Généralités</strong></p>
<p>La recherche d’un logement s’effectue en s’adressant aux agences immobilières (<i>Real Estate Agencies</i>) ou en consultant les annonces dans la presse locale (section <i>Real Estate</i>). En cas de location par une agence, le montant de la commission est à la charge du propriétaire bailleur. Le loyer est payable d’avance (un mois) et une caution représentant un ou deux mois de loyer est souvent demandée.</p>
<p>Selon l’Etat, la durée du bail peut varier d’un à trois ans. L’établissement d’un état des lieux n’est pas indispensable, mais est conseillé. Les impôts locaux et fonciers sont payés par le propriétaire. La majorité des logements ne sont pas loués meublés.</p>
<p><strong>Cas de Los Angeles et New York</strong></p>
<p>A Los Angeles, la recherche de logements à louer ou à acheter doit être circonscrite à certains quartiers. L’étendue de la ville, le manque de transports publics et l’engorgement des routes et des autoroutes font que les quartiers situés à moins d’une heure de trajet se situent pour l’essentiel dans le Westside où les conditions de sécurité minimum sont par ailleurs respectées. En raison de la crise immobilière aux Etats-Unis, les loyers, auparavant en augmentation constante, ont maintenant tendance à stagner. S’agissant de l’achat, les prix baissent mais uniquement dans les quartiers périphériques et non résidentiels où ils restent élevés.</p>
<p>A New York, il existe des variations importantes entre le cœur de Manhattan et les autres <i>boroughs</i>, ainsi qu’entre le marché de l’achat et celui de la location. Indépendamment du prix, le cadre général d’une location demeure cependant identique : versement d’une caution équivalant à un mois de loyer, versement d’un premier mois de loyer et de la commission de l’agence immobilière (15% du loyer annuel), soit un déboursement correspondant à quatre mois de loyer pour un nouveau locataire. La durée des baux est généralement courte (un ou deux ans).</p>
<p><strong>Charges</strong></p>
<ul class="spip">
<li><strong>Atlanta</strong> : les charges ne sont pas incluses dans le loyer ; la climatisation est indispensable de mai à octobre et le chauffage de décembre à mars. Le coût du gaz est très élevé (300 $ par mois l’hiver pour une villa de 3 chambres) ;</li>
<li><strong>Boston</strong> : l’eau est en principe prise en charge par le propriétaire. Les charges de chauffage peuvent varier de 100 à 400 $ ; les charges pour l’électricité de 40 à 55 $. A noter que les garages à Boston sont rares et chers ;</li>
<li><strong>Chicago</strong> : les coûts des charges sont très variables selon les prestations fournies par la copropriété (garage, ascenseur, …). En général le chauffage est individuel (gaz) et l’eau est incluse dans les charges ;</li>
<li><strong>Houston</strong> : les charges sont généralement incluses dans les locations modestes ; en revanche, les frais de climatisation sont élevés d’avril à octobre ;</li>
<li><strong>Los Angeles</strong> : les charges (gaz, électricité, eau) ne sont pas incluses dans le loyer ; la crise du secteur de l’électricité en Californie a entraîné une augmentation très sensible des tarifs d’électricité ;</li>
<li><strong>Miami</strong> : les appartements loués comportent des charges variables selon les quartiers et les conventions entre propriétaires ; dans les immeubles haut de gamme, les charges incluent le plus souvent l’eau, le câble TV et l’accès à internet ; les appartements, même loués vides, comprennent habituellement lave-linge et séchoir à linge ; la climatisation est nécessaire de juin à octobre ;</li>
<li><strong>New York</strong> : dans le cas des immeubles en copropriété ou <i>"condominiums"</i>, les charges relatives à l’eau sont généralement incluses dans le loyer ; l’électricité et le gaz sont dues par le locataire ; pour un appartement de 2 pièces, le coût des charges mensuelles est estimé à 40 $ pour le gaz, 50 à 100 $ pour l’électricité ;</li>
<li><strong>La Nouvelle-Orléans</strong> : les charges additionnelles sont l’eau, le gaz et l’électricité : il faut compter une moyenne de 200 à 400 $ par mois en plus du loyer ;</li>
<li><strong>San Francisco</strong> : les charges mensuelles supportées par le locataire varient selon l’importance de la consommation et concernent l’eau (de 70 à 200 €/mois selon la consommation et le type de logement), ainsi que l’électricité et le gaz (de 80 à 500 €/mois) ;</li>
<li><strong>Washington</strong> : selon les conditions du bail, les charges d’un appartement (eau, gaz, électricité, entretien des communs, surveillance et gardiennage) peuvent être comprises dans le montant du loyer ; les immeubles, pratiquement tous de grand standing, proposent également à leurs locataires piscines, cours de tennis et salles de fitness sans charges supplémentaires ; dans une maison, toutes les charges sont dues (eau, électricité, gaz, ordures ménagères) ; compte tenu du climat, les appartements et les villas sont pratiquement tous pourvus d’un système de chauffage et de climatisation.</li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Electricité</h3>
<p>L’électricité est de 110 volts (220 volts pour certains appareils), 60 Hz, courant monophasé, prises de courant de type américain, à deux broches plates.</p>
<p>Si l’on souhaite apporter des appareils électriques, il faut veiller à se munir d’adaptateurs car il est difficile d’en trouver sur place.</p>
<h3 class="spip"><a id="sommaire_5"></a>Electroménager</h3>
<p>Les cuisines sont la plupart du temps bien équipées (plaques de cuisson et four, réfrigérateur et, souvent, machines à laver le linge et la vaisselle, congélateur).</p>
<p>A noter que les immeubles modernes sont dotés de machines à laver le linge collectives.</p>
<p>La disponibilité locale en électroménager ne pose pas de problème particulier.</p>
<p>Les moyens de chauffage les plus utilisés sont le gaz, l’électricité et le fuel. La climatisation est nécessaire en été dans de nombreuses régions, notamment à Boston, Chicago, Honolulu, Houston, La Nouvelle-Orléans, Miami, Washington, New York, San Francisco, Porto Rico…</p>
<p>Les équipements vidéo sont de type NTSC-PAL mais il est possible de trouver des équipements multi-standard.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/vie-pratique/article/logement-113154). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
