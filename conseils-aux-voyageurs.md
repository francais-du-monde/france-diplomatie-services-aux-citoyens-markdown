# Conseils aux Voyageurs

<h2>Vous résidez à l’étranger ou comptez y voyager prochainement ?</h2>
<p class="spip_document_81920 spip_documents spip_documents_center">
<a href="conseils-aux-voyageurs-conseils-par-pays.md" class="spip_in"><img src="http://www.diplomatie.gouv.fr/fr/IMG/png/visuel_CAV_cle0abfc1.png" width="720" height="200" alt=""></a></p>
<p><strong>Conseils aux Voyageurs vise à faciliter la préparation et le bon déroulement de votre séjour à l’étranger.</strong></p>
<p>Il est fortement recommandé de suivre les conseils figurant en particulier dans la rubrique « Sécurité » afin de garantir votre sécurité personnelle. Le ministère des Affaires étrangères et du Développement international ne saurait toutefois prendre la décision finale quant à l’annulation ou au maintien de votre voyage ; cette décision vous appartient.</p>
<p>La rubrique « Dernières minutes » vous alerte sur les évènements récents qui pourraient avoir un impact sur votre sécurité. La lecture de la rubrique Sécurité, même en présence d’une « Dernière minute », reste impérative pour disposer d’un panorama des risques et connaître les recommandations afférentes.</p>
<p>En outre, entre la réservation du voyage et le départ, la situation d’un pays peut changer. Vérifiez les conditions d’annulation de votre voyage, en particulier en cas de dégradation des conditions de sécurité dans votre pays de destination.</p>
<p>En toutes circonstances, faites preuve de bon sens, soyez prudent et vigilant, observez et respectez les usages, les coutumes et la loi du pays visité.</p>
<p class="alert_cav">
<h2>ALERTES</h2>
<blockquote class="pt">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/infos-pratiques/article/securite-des-francais-a-l-etranger-115407">Sécurité des Français à l’étranger</a>
<p>
      Dans le contexte de l’intervention de la coalition internationale contre Daech et et face au risque accru d’enlèvements et d’actes hostiles contre les ressortissants des pays membres de la coalition, les Français sont invités à faire preuve de vigilance lors de leurs déplacements à l’étranger.

Il leur (...)
</p>
</blockquote>
<blockquote class="pt">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/infos-pratiques/article/securite-au-sahel">Sécurité au Sahel</a>
<p>
      La menace d’attentat et d’enlèvement visant des Occidentaux demeure élevée dans la zone sahélienne mais aussi dans les pays limitrophes. Aucune zone ne peut plus désormais être considérée comme totalement sûre.

Il est par conséquent formellement déconseillé de se rendre dans les zones rouges au Mali, au (...)
</p>
</blockquote>
<blockquote class="pt">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/infos-pratiques/article/maladie-a-virus-zika">Maladie à virus Zika</a>
<p>
      Plusieurs pays d’Amérique latine, d’Afrique et d’Asie et d’Océanie sont touchés par le virus Zika. La liste actualisée de ces pays est consultable sur le site internet de l’European Centre for Disease Prevention and Control.

Cette maladie est transmise par les piqûres de moustiques de type Aedes. Des (...)
</p>
</blockquote>
</p>
<blockquote class="titre_dernier_minutes">
<p class="home_dernieres_minutes_title">
<h2><a href="conseils-aux-voyageurs-dernieres-minutes.md" title="Dernières minutes">Dernières minutes</a></h2>
</p>
<p class="home_dernieres_minutes_img"><a href="https://twitter.com/ConseilsVoyages" class="twitter-follow-button" data-show-count="false" data-lang="fr">Suivre @ConseilsVoyages</a>
</p>
</blockquote>
<p class="items" id="pagination_dernieres_minutes">
<blockquote class="bloc_dernieres_minutes">
<span class="drapeaux"><img class="spip_logos" alt="Etats-Unis" src="http://www.diplomatie.gouv.fr/fr/IMG/rubon12311.jpg?1309771618" width="30" height="19"></span>
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/etats-unis/" title="Etats-Unis">Etats-Unis |
        16 août 2016
</a>
<p>
          Inondations en Louisiane

Suite à de fortes précipitations sur le sud-ouest et le centre de la Louisiane, une forte et rapide montée des eaux est constatée depuis le vendredi 12 août, notamment dans la région de Baton Rouge et Lafayette.

Au 16 août 2016, les alertes (...)
</p>
</blockquote>
<blockquote class="bloc_dernieres_minutes">
<span class="drapeaux"><img class="spip_logos" alt="Australie" src="http://www.diplomatie.gouv.fr/fr/IMG/rubon12202.jpg?1309531826" width="30" height="19"></span>
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/australie/" title="Australie">Australie |
        16 août 2016
</a>
<p>
          Recrudescence d’accidents de la circulation.

Des accidents graves de la route viennent d’être observés. La plus grande vigilance est appelée sur la conduite à gauche. Evitez de conduire la nuit et sous l’emprise de la fatigue. Soyez attentifs aux virages à visibilité (...)
</p>
</blockquote>
<blockquote class="bloc_dernieres_minutes">
<span class="drapeaux"><img class="spip_logos" alt="Chine" src="http://www.diplomatie.gouv.fr/fr/IMG/rubon12226.jpg?1309534692" width="30" height="19"></span>
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/chine/" title="Chine">Chine |
        16 août 2016
</a>
<p>
          Aéroports et gares de Shanghai.

Les autorités ont annoncé  des mesures de sécurité supplémentaires dans les aéroports et les gares de Shanghai dans le cadre de la préparation du sommet du G20 qui se tiendra à Hangzhou les 4 et 5 septembre. Des contrôles renforcés des (...)
</p>
</blockquote>
<span class="pagination">
<span class="on">1</span>
  |
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/?debut_dernieres_minutes=3#pagination_dernieres_minutes" class="lien_pagination" rel="nofollow">2</a>
  |
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/?debut_dernieres_minutes=6#pagination_dernieres_minutes" class="lien_pagination" rel="nofollow">3</a>

 | <a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/?debut_dernieres_minutes=3#pagination_dernieres_minutes" class="lien_pagination" rel="nofollow">&gt;</a></span>
<span class="lien_all_infos"><a href="conseils-aux-voyageurs-dernieres-minutes.md" title="Toutes les dernières minutes">Toutes les dernières minutes</a></span>
</p>
<ul class="cadre_gris">
<li><a href="conseils-aux-voyageurs-conseils-par-pays.md" title="Conseils par pays"> <img class="spip_logos" alt="Conseils par pays" src="http://www.diplomatie.gouv.fr/fr/IMG/rubon12191.jpg?1363956709" width="1000" height="419"> <span class="panneau_static"> <span class="texte_dossiers">Conseils par pays</span></span></a></li>
</ul>
<ul class="cadre_gris cols2-1024-dernier cols2-980-1024-dernier">
<li><a href="conseils-aux-voyageurs-infos-pratiques.md" title="Infos pratiques"> <img class="spip_logos" alt="Infos pratiques" src="http://www.diplomatie.gouv.fr/fr/IMG/rubon20973.jpg?1363905719" width="660" height="278"> <span class="panneau_static"> <span class="texte_dossiers">Infos pratiques</span></span></a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
