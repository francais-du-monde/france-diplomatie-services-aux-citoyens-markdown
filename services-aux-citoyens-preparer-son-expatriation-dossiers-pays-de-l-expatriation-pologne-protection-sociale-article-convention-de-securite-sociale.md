# Convention de sécurité sociale

<p><strong>La France et la Pologne sont liées par la convention générale du 9 juin 1948 entrée en vigueur le 1er mars 1949 et ayant été modifiée par avenant.</strong> Cet accord vise les ressortissants français ou polonais exerçant ou ayant exercé une activité salariée dans l’un des deux États.</p>
<p>La convention pose le principe d’assujettissement à la législation du pays d’emploi. Dans le cadre du détachement, un assuré du régime français exerçant une activité en Pologne pourra, pour une durée limitée (20 ans au maximum), sous certaines conditions, être maintenu au régime français de sécurité sociale durant son activité en Pologne et être dispensé du paiement des cotisations dans ce pays.</p>
<p>La convention contient des dispositions en matière :</p>
<ul class="spip">
<li>d’assurance maladie maternité décès,</li>
<li>d’assurance invalidité,</li>
<li>d’assurance vieillesse et survivants,</li>
<li>de prestations familiales. En application conjointe de la convention franco-polonaise et de la législation interne, les Français occupés en Pologne se trouvent généralement dans l’une des trois situations suivantes :</li></ul>
<ul class="spip">
<li>travailleurs salariés, détachés dans le cadre de la convention, ou dans celui de la législation interne ;</li>
<li>travailleurs qui ne sont plus soumis au régime français parce qu’ils ne sont pas détachés et auxquels les dispositions conventionnelles sont applicables ;</li>
<li>travailleurs soumis à la législation polonaise qui complètent leur protection sociale par une adhésion à la Caisse des Français de l’étranger.</li></ul>
<h4 class="spip">Travailleurs français exerçant une activité salariée ou assimilée et bénéficiant à ce titre des dispositions prévues par la convention franco-polonaise</h4>
<p>Les travailleurs français salariés ou assimilés occupés en Pologne sont soumis à la législation polonaise et en bénéficient dans les mêmes conditions que les ressortissants polonais.</p>
<p><strong>Maladie, maternité, décès</strong></p>
<p>Les travailleurs salariés ou assimilés qui se rendent de France en Pologne bénéficient, ainsi que leurs ayants droit résidant sous leur toit en Pologne, des prestations de l’assurance maladie de ce pays pour autant qu’ils aient effectué en Pologne un travail salarié ou assimilé, que l’affection se soit déclarée postérieurement à leur entrée dans ce pays, à moins que la législation polonaise ne prévoit des conditions plus favorables d’ouverture des droits et qu’ils remplissent les conditions requises par la législation polonaise ou justifient de celles exigées par la législation française.</p>
<p>Il en va de même pour les prestations de l’assurance maternité sous réserve qu’ils aient effectué en Pologne un travail salarié ou assimilé et qu’ils remplissent les conditions requises pour bénéficier de ces prestations au regard de la législation polonaise ou justifient de celles exigées par la législation française, compte tenu de la période d’immatriculation en France et de la période postérieure à leur immatriculation en Pologne.</p>
<p><strong>Prestations familiales</strong></p>
<p>Si la législation polonaise subordonne l’ouverture du droit aux prestations familiales à l’accomplissement de périodes de travail, d’activité professionnelle ou assimilées, il est tenu compte des périodes effectuées tant dans l’un que dans l’autre pays.</p>
<p><strong>Invalidité</strong></p>
<p>Les prestations de l’assurance invalidité sont liquidées conformément aux dispositions de la législation qui était applicable à l’intéressé à la date de la première constatation médicale de la maladie ou de l’accident.</p>
<p>Pour l’examen des droits éventuels à pension d’invalidité, les périodes d’assurance françaises et polonaises peuvent être totalisées.</p>
<p>Toutefois, si au début du trimestre civil au cours duquel est survenue la maladie, l’invalide, antérieurement soumis au régime d’invalidité français, n’était pas assujetti depuis un an au moins à la législation polonaise, il reçoit de l’organisme français la pension d’invalidité.</p>
<p>Cette disposition n’est pas applicable si l’invalidité est la conséquence d’un accident.</p>
<p><strong>Vieillesse</strong></p>
<p>Pour l’examen des droits éventuels à pension de vieillesse, les périodes d’assurance françaises et polonaises peuvent être totalisées à condition qu’elles ne se superposent pas.</p>
<p>Chaque institution française et polonaise détermine le montant de la part de pension due au titre de la législation qu’elle applique en réduisant le montant des avantages auxquels l’intéressé aurait droit si toutes les périodes d’assurance avaient été accomplies sur son territoire au prorata de la durée des périodes d’assurance effectivement accomplies sur son territoire.</p>
<p>La convention franco-polonaise ne prévoit pas le cas du travailleur français occupé en Pologne et qui, soit effectuerait un séjour temporaire en France, soit y transférerait sa résidence alors qu’il est admis aux prestations en espèces de l’assurance maladie ou de l’assurance accidents du travail (en cas de convalescence par exemple).</p>
<p>Elle prévoit, par contre, les soins de santé aux titulaires de pension de l’un des États contractants résidant sur le territoire de l’autre État contractant.</p>
<p>Pour plus d’information, consultez notre rubrique thématique <a href="services-aux-citoyens-preparer-son-expatriation-protection-sociale.md" class="spip_in">Protection sociale</a>.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/protection-sociale/article/convention-de-securite-sociale). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
