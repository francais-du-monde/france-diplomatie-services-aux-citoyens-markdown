# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/vie-pratique/article/transports#sommaire_1">Importation de véhicule</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/vie-pratique/article/transports#sommaire_2">Permis de conduire</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/vie-pratique/article/transports#sommaire_3">Code de la route</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/vie-pratique/article/transports#sommaire_4">Assurances et taxes</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/vie-pratique/article/transports#sommaire_5">Achat et location</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/vie-pratique/article/transports#sommaire_6">Immatriculation</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/vie-pratique/article/transports#sommaire_7">Entretien</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/vie-pratique/article/transports#sommaire_8">Réseau routier</a></li>
<li><a id="so_9" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/vie-pratique/article/transports#sommaire_9">Transports en commun</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Importation de véhicule</h3>
<p>Les marques françaises ne sont pas représentées aux Etats-Unis. En revanche, on peut trouver de nombreuses marques étrangères (japonaises, coréennes, allemandes, britanniques, suédoises).</p>
<p>Importer son véhicule est déconseillé : la réglementation américaine prévoit des normes très strictes (pot catalytique, phares blancs, pare-brise feuilleté, système anti-pollution, compteur en <i>miles</i>…), les formalités d’importation sont longues et compliquées et les frais élevés (transport et taxes intérieures). Par ailleurs, les véhicules importés ne peuvent pas être revendus aux Etats-Unis ; ils doivent obligatoirement être réexportés ou mis à la casse.</p>
<p>Les agences fédérales qui régulent l’importation de véhicules étrangers ou non conformes sont l’<a href="http://www.epa.gov/" class="spip_out" rel="external">Agence de Protection de l’Environnement (EPA)</a>, le <a href="https://www.transportation.gov/" class="spip_out" rel="external">Département du Transport</a>, le <a href="http://www.cbp.gov/" class="spip_out" rel="external">Service des Douanes et de la Protection des Frontière</a>, et l’<a href="http://www.irs.gov/" class="spip_out" rel="external">Internal Revue Service (IRS)</a>. Ces agences déconseillent l’importation de véhicules car la mise aux normes américaines est onéreuse, et parfois impossible ; et il est possible qu’une voiture soit conforme aux exigences d’une agence mais pas des autres.</p>
<p>L’ensemble des informations relatives à l’importation est disponible sur le site internet des <a href="http://www.cbp.gov/" class="spip_out" rel="external">douanes américaines</a>.</p>
<p>Contactez directement le <a href="http://www.cbp.gov/contact/ports" class="spip_out" rel="external">bureau des douanes</a> à chaque port d’arrivée :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Tél. : 001202325 9000</p>
<h3 class="spip"><a id="sommaire_2"></a>Permis de conduire</h3>
<p>La réglementation en matière de permis de conduire relève de la compétence de chaque État et il existe une grande variété de situations : reconnaissance pleine ou limitée du permis international auquel il faut ajouter le titre d’origine (permis de conduire français), obligation ou non de se procurer un permis local, conditions de résidence, nécessité dans certains États de se soumettre à un examen, etc.</p>
<p>Il est par conséquent impératif de <strong>se renseigner sur les règles en vigueur dans l’État de résidence</strong>, en contactant les services consulaires Français ou le service local de l’immatriculation automobile.</p>
<p>Si vous possédez un permis de conduire français : il n’est valable que pendant les trois premiers mois de votre séjour.</p>
<p>Si vous possédez un permis de conduire international : il est valable un an dans le cas où vous n’êtes que de passage. Si en revanche vous êtes résident, il ne l’est que pendant les trois premiers mois de votre séjour.</p>
<p>Il existe une pratique d’échange réciproque des permis avec la France, cependant celle-ci est limitée à certains États américains :</p>
<ul class="spip">
<li>Caroline du Sud,</li>
<li>Colorado (échange limité aux permis de catégorie B),</li>
<li>Connecticut (échange limité aux permis de catégories A et B),</li>
<li>Delaware (échange limité aux permis de catégorie B),</li>
<li>Floride (échange limité aux permis de catégories A et B),</li>
<li>Illinois (seulement si l’on a un numéro de sécurité sociale américain),</li>
<li>Kansas,</li>
<li>Maryland,</li>
<li>New Hampshire,</li>
<li>Ohio (échange limité aux permis de catégorie B),</li>
<li>Pennsylvanie (échange limité aux permis de catégories A et B),</li>
<li>Virginie (échange limité aux permis de catégorie B),</li>
<li>Virginie Occidentale.</li></ul>
<p>Chacun des Etats ayant ses propres spécificités, il convient de se renseigner auprès des bureaux locaux pour les formalités d’échange.</p>
<p><strong>Etape par étape</strong></p>
<ul class="spip">
<li>Présentez-vous au bureau du <i>Department of Motor Vehicles</i> (DMV) le plus proche de votre domicile avec votre carte de <i>Social Security</i> si vous en possédez une, votre passeport ainsi que votre visa. Si vous ne possédez pas de carte d’assuré social américaine, le DMV vous attribuera un numéro spécial.</li>
<li>Remplissez le formulaire d’inscription, et conservez le code de la route remis gratuitement.</li>
<li>Dès que vous pensez être suffisamment familiarisé avec le code de la route, contactez votre DMV afin de prendre rendez-vous pour l’examen. Les droits d’inscription varient selon les Etats mais sont généralement modestes (environ $20). Comme en France, le test se présente sous la forme d’un questionnaire à choix multiple relativement simple accompagné d’un test oculaire.</li>
<li>Pour la conduite, vous pouvez vous présenter avec votre voiture en prouvant qu’elle est assurée ou un véhicule emprunté. Vous pouvez aussi vous inscrire à une autre auto-école qui vous prêtera un véhicule, mais le passage par une auto-école n’est nullement obligatoire aux Etats-Unis.</li>
<li>Vous recevrez votre <i>driving licence</i> immédiatement ou dans le mois qui suit l’obtention de votre permis selon les Etats. Ce permis est une pièce d’identité à part entière aux Etats-Unis, qui vous facilitera la vie dans vos démarches de tous les jours (ouverture d’un compte en banque, etc…).</li></ul>
<p><strong>Pour en savoir plus :</strong></p>
<p>Vous trouverez l’adresse du site Internet de votre État de résidence sur le site du <a href="http://www.usa.gov/" class="spip_out" rel="external">portail du gouvernement américain</a>. Il est également conseillé de consulter le site du <a href="http://www.dmvusa.com" class="spip_out" rel="external">Department of Motor Vehicles</a>.</p>
<h3 class="spip"><a id="sommaire_3"></a>Code de la route</h3>
<p>La liberté de circulation est totale. La conduite s’effectue à droite. La priorité à droite n’existant pas, il faut marquer un temps d’arrêt aux carrefours en zone urbaine. Lorsque toutes les rues du carrefour ont un signal "stop", la règle de priorité est celle de l’ordre d’arrivée des véhicules.</p>
<p>Les limitations de vitesse sont les suivantes :</p>
<ul class="spip">
<li>en ville : 25 à 35 <i>miles</i>/h, soit 40 à 60 km/h,</li>
<li>hors agglomération (y compris sur autoroute) : 55 à 65 <i>miles</i>/h, soit 88 à 100 km/h.</li></ul>
<p>Le contrôle des limitations de vitesse est très strict, les sanctions sévères et applicables immédiatement. La conduite nécessite la plus grande prudence : respect de la signalisation et de la réglementation (notamment sur le port de la ceinture de sécurité, l’utilisation de sièges pour enfants, le taux d’alcoolémie). Selon l’infraction, la condamnation peut être très lourde (amende, retrait du permis de conduire, peine de prison).</p>
<p>Lorsque des enfants montent ou descendent d’un bus scolaire (jaune), il faut s’arrêter derrière lui.</p>
<p>Lorsque le conducteur est suivi par une voiture de police qui met en marche ses signaux lumineux et sonores, il doit se ranger immédiatement sur le bas-côté et rester assis dans sa voiture, les mains sur le volant, en attendant l’officier de police.</p>
<h3 class="spip"><a id="sommaire_4"></a>Assurances et taxes</h3>
<p>L’assurance au tiers est obligatoire sur la quasi-totalité du territoire américain et représente un coût moyen annuel de l’ordre de 1500 $.</p>
<p>Il convient de s’assurer auprès d’une compagnie locale. Il est pour cela préférable de se munir, avant le départ, d’une attestation de son assureur spécifiant les caractéristiques du contrat initialement souscrit et sa durée.</p>
<p>Lors de l’achat, il faut exiger l’acte de vente et la facture. Il faut en outre s’acquitter des taxes d’Etat, des frais de livraison et d’immatriculation. Les démarches s’effectuent localement auprès du <a href="http://www.dmvusa.com/" class="spip_out" rel="external">Department of Motor Vehicles</a> Licensing and Registration.</p>
<h3 class="spip"><a id="sommaire_5"></a>Achat et location</h3>
<p><strong>Achat :</strong> il est préférable d’acheter sur place une voiture de marque américaine, qui sera plus facile à entretenir et à réparer. Le marché est vaste et les prix variables. Il faut compter, par exemple, 15 000 $ pour un petit véhicule neuf. En fonction des régions, il sera préférable de s’équiper d’une voiture robuste, climatisée et adaptée à l’état des routes pour toutes les saisons.</p>
<p>Il existe par ailleurs un marché de l’occasion avec de nombreux concessionnaires et sites internet spécialistes de ce type de transaction.</p>
<p><strong>Location :</strong> certaines agences de location ne loueront pas de voiture à des personnes de moins de 25 ans. D’autres loueront une voiture à des jeunes mais feront payer un tarif plus élevé. Certaines agences ne loueront pas de voitures à des personnes de plus de 71 ans. Veillez à vérifier ces restrictions si vous vous trouvez dans ces catégories d’âge.</p>
<p>Il faut compter de 38 à 60 $ par jour selon le modèle, auxquels il est nécessaire d’ajouter 25 $/jour pour une assurance tous risques ou 18 $/jour pour une assurance au tiers.</p>
<p>Vous pouvez consulter les sites des grands loueurs de voitures tels que Avis, Hertz, Alamo, Budget, National, Dollar etc.</p>
<h3 class="spip"><a id="sommaire_6"></a>Immatriculation</h3>
<p>Les véhicules sont soumis à un contrôle technique annuel.</p>
<h3 class="spip"><a id="sommaire_7"></a>Entretien</h3>
<p>Les véhicules peuvent être réparés sur place, mais on ne trouve pas de pièces détachées pour les voitures de marque française. Celles-ci doivent être commandées en France et les délais sont longs.</p>
<p>Le coût des réparations est plus élevé qu’en France, mais le service rendu est généralement satisfaisant.</p>
<h3 class="spip"><a id="sommaire_8"></a>Réseau routier</h3>
<p>Le réseau routier est très développé, tant par le kilométrage que par l’importance de l’aménagement des voies principales. L’état des autoroutes est en général excellent et celui des routes varie selon les Etats et selon la saison (routes impraticables ou dégradées en raison des intempéries).</p>
<h3 class="spip"><a id="sommaire_9"></a>Transports en commun</h3>
<p>Pour les liaisons intérieures, la voiture et l’avion sont très utilisés contrairement au chemin de fer qui dispose d’un bon réseau, mais de trains de voyageurs plus lents et moins fréquents qu’en Europe. Le réseau de cars interurbains est en revanche plus dense, plus fréquenté et meilleur marché.</p>
<p>En ville, les moyens de transport utilisés sont le véhicule personnel ou le taxi, le métro dans certaines grandes agglomérations (notamment à New York, Chicago, Los Angeles, Miami, San Francisco, Washington), l’autobus, le tramway et, de façon plus originale, le <i>cable car</i> (à San Francisco).</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/vie-pratique/article/transports). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
