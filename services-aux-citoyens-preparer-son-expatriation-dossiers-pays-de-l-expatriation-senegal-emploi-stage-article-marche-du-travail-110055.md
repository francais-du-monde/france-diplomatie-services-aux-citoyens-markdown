# Marché du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/emploi-stage/article/marche-du-travail-110055#sommaire_1">Secteurs à fort potentiel</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/emploi-stage/article/marche-du-travail-110055#sommaire_2">Secteurs à faible potentiel</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/emploi-stage/article/marche-du-travail-110055#sommaire_3">Professions règlementées</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/emploi-stage/article/marche-du-travail-110055#sommaire_4">Rémunération</a></li></ul>
<p>Le marché du travail au Sénégal se caractérise par :</p>
<ul class="spip">
<li>une forte croissance de la demande d’emploi en lien avec la dynamique démographique (2,7% par an, ce qui entraine un doublement de la population tous les 25 ans) ;</li>
<li>une insuffisance de l’offre d’emploi en raison d’un secteur moderne en quasi-stagnation et du développement de l’économie informelle qui occupe une grande partie de la population active ;</li>
<li>des mouvements migratoires d’une grande ampleur se traduisant par une urbanisation accélérée, un déséquilibre spatial et une forte poussée de la migration internationale.</li></ul>
<p>Selon les projections de l’Organisation Mondiale du Travail, la population potentiellement en âge de travailler (15 à 64 ans) atteint environ 6,9 millions d’habitants et que celle qui déclare être active est de 5,4 millions, soit respectivement 54,1% et 42,2% de la population totale.</p>
<p>Selon les données de l’Enquête de suivi de pauvreté (2011), les taux de chômage et d’activités suivants sont respectivement enregistrés en milieu urbain et rural :</p>
<ul class="spip">
<li>Dakar-urbain : 14,1% et 45,1%</li>
<li>Autres urbains : 13,9% et 40,2%</li>
<li>Zones rurales : 7,4% et 54,2%</li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Secteurs à fort potentiel</h3>
<p>Un diagnostic effectué par la Direction de la prévision et des études économiques (DPEE) du Sénégal a identifié, comme secteurs potentiellement porteurs de croissance :</p>
<ul class="spip">
<li>les industries chimiques, et les matériaux de construction (cimenteries) ;</li>
<li>les services financiers ;</li>
<li>l’horticulture et les industries agroalimentaires ;</li>
<li>les activités immobilières ;</li>
<li>le tourisme, l’artisanat et les industries culturelles ;</li>
<li>et les télécoms.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Secteurs à faible potentiel</h3>
<p>Les secteurs à faible potentiel concernent le transport de biens et de personnes ou les sociétés de gardiennage, qui nécessitent une participation de Sénégalais dans le capital (51% au moins du capital social), ce qui peut présenter un risque pour l’investisseur français qui sera minoritaire.</p>
<h3 class="spip"><a id="sommaire_3"></a>Professions règlementées</h3>
<p>Certaines professions libérales telles que la magistrature, l’architecture, la profession d’infirmier, de notaire sont inaccessibles à des <strong>non Sénégalais</strong>. Il existe des restrictions d’accès à certaines professions pour les étrangers : transporteurs, boulangers, mareyeurs, …</p>
<p>Le français est d’usage courant ; si l’usage du wolof était requis, il est possible de prendre des cours à des tarifs abordables à l’Institut culturel et linguistique de Dakar.</p>
<h3 class="spip"><a id="sommaire_4"></a>Rémunération</h3>
<p>Le décret n°96-154 du 19 février 1996 fixe les salaires minima interprofessionnels et agricoles garantis.</p>
<p>Le salaire minimum interprofessionnel garanti (SMIG) des salariés relevant des professions soumises au régime de la durée légale hebdomadaire de travail de quarante heures est fixé à 209,10 FCFA/heure, soit un montant mensuel de 48000 FCFA.</p>
<p>Le salaire minimum agricole garanti (SMAG) pour les travailleurs relevant des entreprises agricoles et assimilés est de 182,95 FCFA/heure, soit un montant mensuel de 42000 FCFA.</p>
<p>Il n’existe en revanche pas d’équivalent du minimum vieillesse en vigueur en France ; les retraités disposant d’un nombre insuffisant de points (moins de 400) pour prétendre à une retraite complète perçoivent dès lors un versement unique, d’un montant de 200 000 FCFA.</p>
<p>Le salaire moyen demeure difficile à évaluer en raison de la forte prépondérance du secteur informel. Dans le secteur formel, le salaire moyen est estimé à environ 220 000 FCFA dans le secteur privé et à 240 000 FCFA dans la fonction publique.</p>
<p>A défaut de conventions collectives des arrêtés du ministre chargé du travail fixent :</p>
<ul class="spip">
<li>les catégories professionnelles et les salaires minima correspondants ;</li>
<li>les taux minima de majoration des heures supplémentaires effectuées de jour ou de nuit pendant les jours ouvrables, les dimanches et les jours fériés ;</li>
<li>éventuellement, les primes d’ancienneté et d’assiduité.</li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/emploi-stage/article/marche-du-travail-110055). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
