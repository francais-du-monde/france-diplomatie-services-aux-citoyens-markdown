# Vie pratique

<h2 class="rub22785">Logement</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/vie-pratique/#sommaire_1">Où se loger ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/vie-pratique/#sommaire_2">Conditions de location</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/vie-pratique/#sommaire_3">Hôtels</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/vie-pratique/#sommaire_4">Electricité</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/vie-pratique/#sommaire_5">Electroménager</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Où se loger ?</h3>
<p>L’édition du dimanche du <i>Mercurio </i>est la référence pour les annonces immobilières. On peut également utilement consulter le site Internet du <i>Mercurio</i> <a href="http://www.propiedades.emol.com/" class="spip_out" rel="external">consacré à l’immobilier</a> (annonces et liste des agences immobilières) ainsi que le site <a href="http://www.portalinmobiliario.com/" class="spip_out" rel="external">Portalinmobiliario.com</a>. Il faut compter environ un à deux mois de recherches pour trouver un logement qui corresponde à vos attentes.</p>
<p>Plusieurs universités proposent une liste de logements, résidences et chambres en location (pour un étudiant ou un jeune professionnel) :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.unab.cl/intercambio/info_util.asp" class="spip_out" rel="external">Universidad Andres Bello</a> </p>
<p>Certaines entreprises proposent, moyennant commission, des chambres chez l’habitant et des appartements meublés. Enfin, vous pouvez également passer une annonce gratuite sur le site du <a href="http://www.lepetitjournal.com/" class="spip_out" rel="external">Petit Journal</a> ou celui du <a href="http://www.routard.com/" class="spip_out" rel="external">Guide du Routard</a></p>
<p>A noter que les prix des locations à Santiago sont beaucoup plus élevés qu’en province.</p>
<p>Si vous ne possédez pas de véhicule, privilégiez les logements situés à proximité d’une station de métro.</p>
<h4 class="spip">Quartiers</h4>
<p>Santiago Centro<br class="manualbr">C’est surtout un quartier d’affaires, désert la nuit. Les loyers y sont modérés. Il est difficile d’y stationner. Les immeubles de la rue Lastarria et des alentours sont tranquilles avec de nombreux bars et restaurants. Les immeubles près du Musée des Beaux-Arts sont également bien situés.</p>
<p>Providencia<br class="manualbr">C’est un quartier d’affaires tranquille. Vous y trouverez de nombreux bars et restaurants. Les loyers sont modérés. C’est un quartier idéal pour un jeune célibataire.</p>
<p>Las Condes / Vitacura / Lo Barnechea / Los Dominicos<br class="manualbr">Ce sont des quartiers résidentiels et les loyers y sont plus élevés. Un véhicule est presque toujours obligatoire.</p>
<p>La Reina /Nuñoa<br class="manualbr">Ce sont des quartiers résidentiels plus populaires que les précédents, calmes et agréables.</p>
<p>Pour en savoir plus : <a href="http://www.ambafrance-cl.org/" class="spip_out" rel="external">site de l’ambassade de France</a></p>
<h4 class="spip">Typologie des logements</h4>
<ul class="spip">
<li>Dpto (Departamento) : appartement</li>
<li>Estudio : studio (1 chambre + cuisine et salle de bain)</li>
<li>Casa : maison</li>
<li>Pieza : chambre</li>
<li>Amoblado : meublé</li>
<li>Un ambiente : une pièce incluant cuisine et chambre</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Conditions de location</h3>
<p>Il est fortement conseillé de faire un état des lieux et de vérifier, en particulier, l’humidité des murs.</p>
<p>Les baux sont d’une durée d’un à deux ans et renouvelables tacitement (un à deux mois de préavis). Un dépôt de garantie équivalent à un ou deux mois de loyer est souvent demandé. La commission d’agence représente la moitié d’un loyer mensuel. Les propriétaires sont relativement exigeants pour les conditions de location, en particulier avec les étrangers. Sont en général exigés des feuilles de paie d’un employeur chilien, des justificatifs de solvabilité bancaire et/ou un aval chilien.</p>
<p>A noter que la signature du contrat se fait toujours au Chili devant notaire. Cette procédure est beaucoup plus courante qu’en France et les frais de l’ordre de quelques euros.</p>
<p>Les charges équivalent à environ 20% du loyer et ne comprennent ni l’eau ni l’électricité. Elles correspondent davantage aux frais d’entretien de l’immeuble. Si la climatisation n’est pas nécessaire, en revanche le chauffage est indispensable car les températures des mois d’hiver sont relativement basses. Si vous louez une maison individuelle, les barreaux, grilles et système d’alarme sont utiles. Les immeubles ont presque tous des concierges exerçant aussi les fonctions de gardiens.</p>
<table class="spip" summary="">
<caption>Prix du logement à Santiago de Chili en euros</caption>
<thead><tr class="row_first"><th id="id73f3_c0">Type de logement</th><th id="id73f3_c1">Surface (m²)</th><th id="id73f3_c2">Bons quartiers : Providencia</th><th id="id73f3_c3">Très bons quartiers : Vitacura (à prox. du lycée Français)
Las Condes</th><th id="id73f3_c4">Meilleurs quartiers : Santa Maria de Manquehue
 Quartier Club de Polo/ Alonso de Córdoba</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="id73f3_c0">Maison meublée<br class="autobr">
3 chambres</td>
<td headers="id73f3_c1">200</td>
<td headers="id73f3_c2"></td>
<td headers="id73f3_c3">3 280</td>
<td headers="id73f3_c4">3 940</td></tr>
<tr class="row_even even">
<td headers="id73f3_c0">Maison non meublée<br class="autobr">
3 chambres</td>
<td headers="id73f3_c1">200</td>
<td headers="id73f3_c2">2 460</td>
<td headers="id73f3_c3">2 790</td>
<td headers="id73f3_c4">3 450</td></tr>
<tr class="row_odd odd">
<td headers="id73f3_c0">Maison non meublée<br class="autobr">
4 chambres</td>
<td headers="id73f3_c1">250</td>
<td headers="id73f3_c2">3 120</td>
<td headers="id73f3_c3">3 280</td>
<td headers="id73f3_c4">4 100</td></tr>
<tr class="row_even even">
<td headers="id73f3_c0">Appartement meublé 2 chambres</td>
<td headers="id73f3_c1">80-120</td>
<td headers="id73f3_c2">1 390</td>
<td headers="id73f3_c3">2 100</td>
<td headers="id73f3_c4">2 210</td></tr>
<tr class="row_odd odd">
<td headers="id73f3_c0">Appartement meublé 3 chambres</td>
<td headers="id73f3_c1">120-160</td>
<td headers="id73f3_c2">2 210</td>
<td headers="id73f3_c3">2 460€)</td>
<td headers="id73f3_c4">3 000</td></tr>
<tr class="row_even even">
<td headers="id73f3_c0">Appartement non meublé 1 chambre</td>
<td headers="id73f3_c1">50-70</td>
<td headers="id73f3_c2">1 100</td>
<td headers="id73f3_c3">1 390</td>
<td headers="id73f3_c4">-</td></tr>
<tr class="row_odd odd">
<td headers="id73f3_c0">Appartement non meublé 2 chambres</td>
<td headers="id73f3_c1">80-120</td>
<td headers="id73f3_c2">1 390</td>
<td headers="id73f3_c3">1 640</td>
<td headers="id73f3_c4">2 380</td></tr>
<tr class="row_even even">
<td headers="id73f3_c0">Appartement non meublé 3 chambres</td>
<td headers="id73f3_c1">120-160</td>
<td headers="id73f3_c2">1 890</td>
<td headers="id73f3_c3">2 130</td>
<td headers="id73f3_c4">2 620</td></tr>
<tr class="row_odd odd">
<td headers="id73f3_c0">Appartement non meublé 4 chambres</td>
<td headers="id73f3_c1">160-200</td>
<td headers="id73f3_c2">2 460</td>
<td headers="id73f3_c3">2 460</td>
<td headers="id73f3_c4">3 280</td></tr>
<tr class="row_even even">
<td headers="id73f3_c0"></td>
<td headers="id73f3_c1"></td>
<td headers="id73f3_c2">NB : le prix des loyers inclus les charges communes* (environ 250 € pour les appartements de 1 à 2 pièces et 330 € pour les 3 pièces et +).</td>
<td headers="id73f3_c3">NB : le prix des loyers inclus les charges communes* (environ 250 € pour les appartements de 1 à 2 pièces et 330 € pour les 3 pièces et +)</td>
<td headers="id73f3_c4">NB : le prix des loyers inclus les charges communes* (environ 250 € pour les appartements de 1 à 2 pièces et 330 € pour les 3 pièces et +)</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_3"></a>Hôtels</h3>
<p><strong>Appart-hôtels :</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Prix moyen d’une chambre double</td>
<td>Pesos</td></tr>
<tr class="row_even even">
<td>Grand tourisme / Luxe</td>
<td>90 000</td></tr>
<tr class="row_odd odd">
<td>Moyen tourisme</td>
<td>27 500</td></tr>
</tbody>
</table>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.director.cl/" class="spip_out" rel="external">DIRECTOR Suite Hotel</a><br class="manualbr">Carmencita 45, Apoquindo alt. 3000, Las Condes <br class="manualbr">Tél. : (56-2) 233 24 23 <br class="manualbr">Réservation : (56-2) 246 65 64 ou <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/vie-pratique/#reservaciones#mc#director.cl#" title="reservaciones..åt..director.cl" onclick="location.href=mc_lancerlien('reservaciones','director.cl'); return false;" class="spip_mail">courriel</a></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.eurotel.cl/" class="spip_out" rel="external">EUROTEL</a><br class="manualbr">Guardia Vieja 285, Providencia <br class="manualbr">Tél/Fax : (56-2) 251 61 11</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>TIME SUITE</strong><br class="manualbr">Callao 2988, Las Condes<br class="manualbr">SANTIAGO RM (près du métro)<br class="manualbr">Tél. : 0056 2-757 2000</p>
<h3 class="spip"><a id="sommaire_4"></a>Electricité</h3>
<p>Le courant est de 220 volts et de 50 hertz.</p>
<p>Les prises chiliennes, à deux ou trois fiches, sont différentes des prises françaises. On trouve des adaptateurs bon marché sur le marché local. Les appareils fonctionnant sur 110 V doivent utiliser un transformateur.</p>
<p>La compagnie distributrice d’électricité est <a href="http://www.chilectra.cl/" class="spip_out" rel="external">Chilectra</a>.</p>
<h3 class="spip"><a id="sommaire_5"></a>Electroménager</h3>
<p>De nombreuses marques chiliennes et étrangères d’équipement électroménager sont disponibles sur place à un coût généralement moins élevé qu’en France.</p>
<p>Le matériel électronique est en général moins moderne qu’en France. Les modèles d’appareil photo numérique et de lecteur mp3 proposés dans la grande distribution au Chili ont environ de six mois à un an de retard sur les modèles disponibles en France.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chili-vie-pratique-article-loisirs-et-culture.md" title="Loisirs et culture">Loisirs et culture</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chili-vie-pratique-article-communications-109766.md" title="Communications">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chili-vie-pratique-article-transports.md" title="Transports">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chili-vie-pratique-article-cout-de-la-vie-109764.md" title="Coût de la vie">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chili-vie-pratique-article-scolarisation-109763.md" title="Scolarisation">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chili-vie-pratique-article-sante.md" title="Santé">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chili-vie-pratique-article-logement.md" title="Logement">Logement</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/vie-pratique/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
