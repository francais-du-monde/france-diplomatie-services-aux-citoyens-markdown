# Recherche d’emploi

<p>Le Mexique a traversé plusieurs crises avant de retrouver, en 2004, le chemin de la croissance, dynamisme économique qui s’accélère depuis. Les investisseurs ont de quoi être séduits par ce pays : en plus de sa prospérité économique actuelle, le Mexique se classe dans les 10 premières économies mondiales, son revenu par habitant est le plus élevé de toute l’Amérique latine et il est l’un des principaux pays exportateurs du monde.</p>
<p>Mais le Mexique est un pays de contrastes. 45% de la population vit dans la pauvreté et le Mexique est aussi l’un des pays du monde où la répartition des richesses est la plus inégale. Le pays souffre de l’étendue de son territoire. Beaucoup de richesses se concentrent à Mexico et dans les zones frontalières avec les Etats-Unis d’Amérique, alors que le Sud est pauvre et en retard au niveau du développement. L’économie mexicaine, très exportatrice, souffre de sa dépendance à l’égard de ses partenaires de l’ALENA (Etats-Unis et Canada) vers lesquels le Mexique exporte 80% de sa production.</p>
<p>La présence française au Mexique est importante. Elle se concentre principalement à Mexico, à Guadalajara, à Puebla, à Monterrey et dans les zones touristiques. Si la communauté française est principalement constituée de migrants ou d’émigrés de longue date, beaucoup de Français s’y installent définitivement, séduits par le charme de ce pays.</p>
<p>Les opportunités existent et les Français semblent particulièrement bien s’adapter à la réalité de ce pays.</p>
<h4 class="spip">Outils pour la recherche d’emploi</h4>
<p><i>Le signalement d’organismes sur ce site ne saurait engager la responsabilité de la MFE quant à la qualité de leurs services.</i></p>
<p><strong>Les sites internet spécialisés</strong></p>
<p>Les principaux sites de l’emploi au Mexique sont :</p>
<ul class="spip">
<li><a href="http://www.occ.com.mx/" class="spip_out" rel="external">www.occ.com.mx/</a></li>
<li><a href="http://www.zonajobs.com.mx/" class="spip_out" rel="external">www.zonajobs.com.mx</a></li>
<li><a href="http://www.bumeran.com.mx/" class="spip_out" rel="external">www.bumeran.com.mx</a></li>
<li><a href="http://www.computrabajo.com.mx/" class="spip_out" rel="external">www.computrabajo.com.mx</a></li>
<li><a href="http://empleo.trovit.com.mx/" class="spip_out" rel="external">http://empleo.trovit.com.mx/</a></li></ul>
<p>Autres sites :</p>
<ul class="spip">
<li><a href="http://www.alianzafrancesa.org.mx/Bolsa-de-trabajo" class="spip_out" rel="external">http://www.alianzafrancesa.org.mx/Bolsa-de-trabajo</a> : offres d’emploi/stage au sein des Alliances du Mexique</li>
<li><a href="http://www.lepetitjournal.com/lesbonsplans/annonces.php?city=10cat=185" class="spip_out" rel="external">http://www.lepetitjournal.com/lesbonsplans/annonces.php?city=10cat=185</a></li>
<li><a href="http://www.bolsadetrabajo.com.mx/" class="spip_out" rel="external">www.bolsadetrabajo.com.mx/</a></li>
<li><a href="http://www.acciontrabajo.com.mx/" class="spip_out" rel="external">www.acciontrabajo.com.mx/</a></li>
<li><a href="http://www.chamba.com.mx/login.asp" class="spip_out" rel="external">www.chamba.com.mx/login.asp</a></li>
<li><a href="http://www.webfrancia.com/" class="spip_out" rel="external">www.webfrancia.com</a></li></ul>
<p>Si vous avez réalisé un échange universitaire au Mexique, pensez également à consulter le site internet de l’université dans laquelle vous avez étudié car chaque école a en général sa propre bourse du travail.</p>
<p><strong>Journaux</strong></p>
<p>Les deux principaux journaux mexicains, El Universal et Reforma disposent d’une rubrique Emploi. C’est également le cas du quotidien gratuit Publimetro (équivalent du Metro français). Ces offres sont généralement disponibles sur les sites internet de ces journaux :</p>
<ul class="spip">
<li><a href="http://www.avisooportuno.mx/empleos/" class="spip_out" rel="external">El Universal</a></li>
<li><a href="http://www.avisosdeocasion.com/mex/busca_empleo.asp?marca=EMPLEOSFolioClasif=27FolioSubClasif=0" class="spip_out" rel="external">Reforma</a></li>
<li><a href="http://www.publimetro.com.mx/empleos/" class="spip_out" rel="external">Publimetro</a></li></ul>
<p><strong>Réseaux sociaux</strong></p>
<p>Il convient de ne pas négliger l’importance des réseaux sociaux, et notamment des réseaux sociaux professionnels tels que Linkedin : <a href="http://www.linkedin.com/" class="spip_out" rel="external">www.linkedin.com</a>. Cet outil est devenu incontournable et est utilisé par une grande majorité des recruteurs mexicains qui consulteront votre CV en ligne avant de vous contacter. Nous vous conseillons donc d’actualiser votre profil et de consulter les offres d’emploi publiées par les entreprises par ce biais. Certaines entreprises publient également leurs offres via leurs comptes Twitter et Facebook donc restez connectés !</p>
<p><strong>Agences de recrutement</strong></p>
<p>Les grandes agences de recrutement internationales sont présentes au Mexique. Nous vous suggérons donc de contacter :</p>
<ul class="spip">
<li><a href="http://www.adecco.com.mx/" class="spip_out" rel="external">Adecco</a></li>
<li><a href="http://www.manpower.com.mx/" class="spip_out" rel="external">Manpower</a></li>
<li>Page Group : <a href="http://www.michaelpage.com.mx/" class="spip_out" rel="external">MichaelPage</a> et <a href="http://www.pagepersonnel.com.mx/" class="spip_out" rel="external">PagePersonnel</a></li></ul>
<p><strong>Le service emploi et formation de la Chambre franco-mexicaine de commerce et d’industrie</strong></p>
<p>La Chambre franco-mexicaine de commerce et d’industrie (CFMCI) dispose d’un service de l’emploi. En la matière, la CFMCI est l’opérateur du comité consulaire pour l’emploi et la formation professionnelle (CCPEFP).</p>
<p>Contact :<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/emploi-stage/article/recherche-d-emploi#emploi2#mc#cfmci.com#" title="emploi2..åt..cfmci.com" onclick="location.href=mc_lancerlien('emploi2','cfmci.com'); return false;" class="spip_mail">Courriel</a> <br class="manualbr">Twitter : @EmpleoCFMCI<br class="manualbr">Linkedin : Servicio Empleo CFMCI<br class="manualbr">Adresse : Protasio Tagle 104, Col. San Miguel Chapultepec, Mexio Distrito Federal 11850.<br class="manualbr">Tel : +52 (55) 5272 0960 ext. 113<br class="manualbr">Horaires : du lundi au vendredi de 9h à 18h, uniquement sur rendez-vous</p>
<p>Le service emploi et formation se charge d’appuyer les Français <strong>en recherche d’emploi ou de stage</strong> au Mexique. Pour ce faire, les candidats peuvent, à leur demande, bénéficier d’un rendez-vous avec une conseillère dans les locaux de la CFMCI afin d’orienter leurs recherches (bilan de compétences, conseils sur le CV, etc.) et leur démarche d’installation au Mexique. La mission de ce service de la CFMCI est de mettre en relation les entreprises qui font partie du réseau CFMCI et les candidats en recherche d’emploi. En 2012, 160 Français ont été recrutés grâce à ce service (66 en CDI/CDD et 94 en stage).</p>
<p>Le service emploi et formation diffuse tous les 15 jours les CV des candidats français en recherche d’emploi et présents sur le territoire mexicain auprès de toutes ses entreprises membres par le biais d’une lettre d’information dédiée :</p>
<p>Empleo.com</p>
<p><a href="http://www.franciamexico.com/newsletters-mexique/empleocom-n14/" class="spip_out" rel="external">http://www.franciamexico.com/newsletters-mexique/empleocom-n14/</a></p>
<p><i>NB : La CFMCI peut recevoir des demandes d’emploi de compatriotes vivant en dehors du Mexique et notamment en France. Il est toutefois important de noter que les employeurs locaux souhaitent toujours avoir un entretien le plus rapidement possible avec les candidats proposés et vont donner la plupart du temps la préférence à un candidat déjà installé au Mexique (connaissance de la langue, adaptation à la situation et aux coutumes locales, possession d’un permis de travail). </i></p>
<p><strong>Pôle emploi international</strong></p>
<p>Pôle emploi international (PEI) est la section internationale de l’agence publique Pôle emploi. Elle propose 30 000 offres d’emploi par an et vous y trouverez des offres pour le Mexique.</p>
<p><a href="http://www.pole-emploi-international.fr/www.eei.com.fr/eei/accueil.jsp" class="spip_out" rel="external">http://www.pole-emploi-international.fr/www.eei.com.fr/eei/accueil.jsp</a></p>
<p><strong>Les contacts personnels</strong></p>
<p>Les <strong>réseaux de relation </strong>sont en effet très importants au Mexique et sont souvent la clé du succès d’une recherche d’emploi. Les contacts sont donc à développer et à multiplier.</p>
<p>Pour ce faire de nombreux événements réunissant la communauté française ou franco-mexicaine sont organisés, parmi lesquels notamment :</p>
<ul class="spip">
<li><a href="http://www.franciamexico.com/" class="spip_out" rel="external">Chambre franco-mexicaine de commerce et d’industrie</a></li>
<li><a href="http://mexicoaccueil.com/" class="spip_out" rel="external">Mexico Accueil</a></li>
<li><a href="http://www.alianzafrancesa.org.mx/" class="spip_out" rel="external">Alliance Francaise</a></li>
<li><a href="http://www.frenchtuesdays.com/index.php" class="spip_out" rel="external">Les French Tuesdays</a></li></ul>
<p><strong>Les candidatures spontanées</strong></p>
<p>Elles sont très fréquemment utilisées. Il convient de les adresser au service du recrutement. Sans relations, les chances de réussir par ce biais restent cependant limitées.</p>
<p>Vous pouvez consulter une liste des entreprises présentes au Mexique :</p>
<ul class="spip">
<li><a href="http://www.franciamexico.com/" class="spip_out" rel="external">sur le site de la CFMCI</a></li>
<li><a href="http://www.webfrancia.com/fr/categories/index.htm" class="spip_out" rel="external">sur le site Webfrancia</a></li></ul>
<p>Le site de l’<a href="http://www.alianzafrancesa.org.mx" class="spip_out" rel="external">Alliance française</a> dispose d’une rubrique "bolsa de trabajo" proposant des offres d’emploi/stages au sein des Alliances du pays.</p>
<p>Vous pouvez enfin consulter la fiche "Mexique" sur le <a href="https://www.tresor.economie.gouv.fr/pays/mexique" class="spip_out" rel="external">site de la Direction du Trésor</a>.</p>
<p><i>Mise à jour : septembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/emploi-stage/article/recherche-d-emploi). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
