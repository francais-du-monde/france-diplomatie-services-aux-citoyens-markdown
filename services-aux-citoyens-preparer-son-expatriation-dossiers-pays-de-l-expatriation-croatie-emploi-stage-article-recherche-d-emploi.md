# Recherche d’emploi

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/emploi-stage/article/recherche-d-emploi#sommaire_1">Outils pour la recherche d’emploi</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/emploi-stage/article/recherche-d-emploi#sommaire_2">Organismes pour la recherche d’emploi</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/emploi-stage/article/recherche-d-emploi#sommaire_3">Opportunités d’emploi et de stages</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Outils pour la recherche d’emploi</h3>
<h4 class="spip">Sites internet</h4>
<ul class="spip">
<li><a href="http://www.moj-posao.net/" class="spip_out" rel="external">Moj-posao.net</a></li>
<li><a href="http://www.posao.hr/" class="spip_out" rel="external">Posao.hr</a></li>
<li><a href="http://www.oglasnik.hr/" class="spip_out" rel="external">Oglasnik.hr</a></li></ul>
<h4 class="spip">Annuaires</h4>
<p><strong>Agences de recrutement :</strong></p>
<ul class="spip">
<li><a href="http://www.adecco.hr/" class="spip_out" rel="external">Adecco Hrvatska</a></li>
<li><a href="http://www.alexanderhughes.com/" class="spip_out" rel="external">Alexander Hughes d.o.o.</a></li>
<li><a href="http://www.antal.com/" class="spip_out" rel="external">Antal</a></li>
<li><a href="http://www.azz.hr/" class="spip_out" rel="external">AZZ</a></li>
<li><a href="http://www.dekra.hr/" class="spip_out" rel="external">Dekra za privremeno zapošljavanje</a></li>
<li><a href="http://www.demano.hr/" class="spip_out" rel="external">Demano privremeno zapošljavanje d.o.o.</a></li>
<li><a href="http://www.dgs.hr/" class="spip_out" rel="external">DGS</a></li>
<li><a href="http://www.pendlpiswanger.hr/" class="spip_out" rel="external">Dr. Pendl  Dr. Piswanger</a></li>
<li><a href="http://www.electus.hr/" class="spip_out" rel="external">Electus DGS</a></li>
<li><a href="http://www.hill-croatia.hr/" class="spip_out" rel="external">Hill International</a></li>
<li><a href="http://www.jurkovic.hr/" class="spip_out" rel="external">Jurkovic savjetovanje</a></li>
<li><a href="http://www.natonhrcroatia.com/" class="spip_out" rel="external">Kadrovi Naton d.o.o. za privremeno zapošljavanje</a></li>
<li><a href="http://www.lugera.com/" class="spip_out" rel="external">Lugera Makler</a></li>
<li><a href="http://www.uspinjaca.hr/" class="spip_out" rel="external">Uspinjaca</a></li>
<li><a href="http://www.niteo.hr/" class="spip_out" rel="external">Niteo - Privremeno zapošljavanje</a></li>
<li><a href="http://www.neweuroperesourcing.com/" class="spip_out" rel="external">Nova Europa Zapošljavanje d.o.o.</a></li>
<li><a href="http://www.pmc.at/" class="spip_out" rel="external">PMC Zagreb</a></li>
<li><a href="http://www.selectio.hr/" class="spip_out" rel="external">Selectio</a></li>
<li><a href="http://www.smartflex.hr/" class="spip_out" rel="external">Smart Flex za privremeno zapošljavanje</a></li>
<li><a href="http://www.trenkwalder.com/hr" class="spip_out" rel="external">Trenkwalder kadrovske usluge</a></li>
<li><a href="http://www.upsagencija.hr/" class="spip_out" rel="external">UPS</a></li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Organismes pour la recherche d’emploi</h3>
<p><a href="http://www.hzz.hr/" class="spip_out" rel="external">Agence croate pour l’emploi</a> : en croate uniquement, la version anglaise n’apparaît pas disponible bien que proposée.</p>
<h3 class="spip"><a id="sommaire_3"></a>Opportunités d’emploi et de stages</h3>
<p><a href="https://www.civiweb.com/FR/index.aspx" class="spip_out" rel="external">Civiweb</a> et <a href="http://export.businessfrance.fr/default.html" class="spip_out" rel="external">Business France</a> pour les contrats de VIE.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/emploi-stage/article/recherche-d-emploi). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
