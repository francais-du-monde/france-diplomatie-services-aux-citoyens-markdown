# Entretien d’embauche

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/emploi-stage/article/entretien-d-embauche-111290#sommaire_1">Conduite de l’entretien</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/emploi-stage/article/entretien-d-embauche-111290#sommaire_2">Apparence et attitude</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/emploi-stage/article/entretien-d-embauche-111290#sommaire_3">Négociation du salaire</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/emploi-stage/article/entretien-d-embauche-111290#sommaire_4">Questions préférées des recruteurs</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/emploi-stage/article/entretien-d-embauche-111290#sommaire_5">Erreurs à éviter</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Conduite de l’entretien</h3>
<p>En ce qui concerne l’entretien, les employeurs portugais sont sensibles tout d’abord à l’expérience professionnelle, ensuite à la formation pratique. En outre, ils ont fréquemment recours à des tests psychotechniques afin d’évaluer les compétences et la personnalité du candidat.</p>
<h3 class="spip"><a id="sommaire_2"></a>Apparence et attitude</h3>
<h4 class="spip">Entraînez-vous</h4>
<p>Pour ne pas être surpris lors de l’entretien, il est recommandé d’anticiper les questions du recruteur et de préparer des réponses (voir "les questions préférées des recruteurs" ci-dessous). Vous devez être capable de parler aussi bien de vos défauts que de vos qualités.</p>
<h4 class="spip">Soyez ponctuel</h4>
<p>Il est impératif d’arriver à l’heure à un entretien. Quelle qu’en soit la raison (embouteillage, difficulté à trouver le lieu du rendez-vous, etc.), votre retard fera mauvaise impression. Si, pour une raison particulière, votre retard est inévitable, il est recommandé de prévenir l’entreprise au préalable.</p>
<h4 class="spip">La première impression</h4>
<p>Votre apparence est primordiale : c’est la première impression que le recruteur aura de vous. Soyez discret dans votre tenue, votre maquillage, votre coiffure et votre parfum.</p>
<h4 class="spip">Soyez sûr de vous</h4>
<p>Lorsque vous vous présenterez, serrez la main du recruteur avec fermeté, souriez et maintenez le contact visuel, sans toutefois le fixer du regard. Un regard fuyant peut être perçu comme un manque de confiance en soi.</p>
<h4 class="spip">Contrôlez la fin de l’entretien</h4>
<p>N’hésitez pas à montrer votre intérêt en vous informant de la suite qui sera réservée à votre candidature.</p>
<h3 class="spip"><a id="sommaire_3"></a>Négociation du salaire</h3>
<p>Il est préférable de laisser l’employeur aborder la question du salaire lors de l’entretien. Ne soulevez le problème qu’en cas d’oubli ou d’absence de second entretien. Renseignez-vous au préalable sur le montant des salaires pratiqués au sein de l’entreprise ou sur les prix du marché au Portugal, dans la presse ou auprès d’un employé de l’entreprise : vous éviterez ainsi de surévaluer ou sous-évaluer vos compétences et de faire ainsi preuve d’orgueil ou de manque de confiance en soi. Restez néanmoins exigeant et n’hésitez pas à mettre en avant vos qualités et compétences particulières (connaissance d’une ou plusieurs langues étrangères, expériences professionnelles passées, domaine d’expertise privilégié, carnet d’adresses) lors de la négociation.</p>
<h3 class="spip"><a id="sommaire_4"></a>Questions préférées des recruteurs</h3>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Fale-me de si…</td>
<td>Parlez-moi de vous.</td></tr>
<tr class="row_even even">
<td>O que pretende fazer ?</td>
<td>Que pensez-vous faire ?</td></tr>
<tr class="row_odd odd">
<td>Porque quer trabalhar neste ramo ?</td>
<td>Pourquoi voulez-vous travailler dans ce secteur ?</td></tr>
<tr class="row_even even">
<td>O que sabe da nossa empresa ?</td>
<td>Que savez-vous de notre entreprise ?</td></tr>
<tr class="row_odd odd">
<td>Porque estudou XXX na Universidade ?</td>
<td>Pourquoi avez-vous étudié XXX à l’université ?</td></tr>
<tr class="row_even even">
<td>Qual foi a sua área favorita ? Porquê ?</td>
<td>Quel a été votre domaine de prédilection ? Pourquoi ?</td></tr>
<tr class="row_odd odd">
<td>O que é mais importante para si ? O salário ou o próprio emprego ?</td>
<td>Qu’est ce qui est le plus important pour vous ? Le salaire ou l’emploi en lui-même ?</td></tr>
<tr class="row_even even">
<td>Diga-me duas ou três experiências que lhe tenham dado maior satisfacção. Porquê ?</td>
<td>Citez-moi deux ou trois expériences vous ayant apporté satisfaction. Pourquoi ?</td></tr>
<tr class="row_odd odd">
<td>Que contribuições poderá trazer para este empresa ?</td>
<td>Que pouvez-vous apporter à l’entreprise ?</td></tr>
<tr class="row_even even">
<td>Como resolveu uma grande crise ou um grande problema ?</td>
<td>Comment avez-vous réussi à résoudre une grande crise ou un grand problème ?</td></tr>
<tr class="row_odd odd">
<td>Tem disponibilidade para deslocações ?</td>
<td>Pouvez-vous vous déplacer facilement ?</td></tr>
<tr class="row_even even">
<td>Tem outras entrevistas de emprego programadas ?</td>
<td>Avez-vous d’autres entretiens ?</td></tr>
<tr class="row_odd odd">
<td>Quais são as suas expectativas salariais ?</td>
<td>Quelles sont vos attentes en matière de salaire ?</td></tr>
<tr class="row_even even">
<td>Gostava do seu emprego anterior ?</td>
<td>Aimiez-vous votre ancien emploi ?</td></tr>
<tr class="row_odd odd">
<td>Diga me 3 qualidades e 3 defeitos</td>
<td>Citez-moi trois qualités et trois défauts</td></tr>
<tr class="row_even even">
<td>Quanto tempo pensa ficar no seu próximo emprego ?</td>
<td>Combien de temps pensez-vous rester dans votre prochain emploi ?</td></tr>
<tr class="row_odd odd">
<td>Gosta de trabalhar em equipa ?</td>
<td>Aimez-vous travailler en équipe ?</td></tr>
<tr class="row_even even">
<td>É um líder ou um seguidor ?</td>
<td>Êtes-vous un leader ou un suiveur ?</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_5"></a>Erreurs à éviter</h3>
<p>Ne laissez jamais penser que vous avez postulé dans cette entreprise par hasard. Montrez que le poste que vous entendez occuper s’inscrit de manière cohérente dans votre parcours professionnel.</p>
<p>Evitez les retards et les tenues vestimentaires excentriques ou inadéquates : ce sera perçu par l’employeur comme un signe de négligence ou un manque de respect.</p>
<p>Faites preuve d’une courtoisie équilibrée à l’égard de votre recruteur et du personnel de l’entreprise : évitez de paraître trop à votre aise comme trop distant et gardez-vous de toute posture tendant vers l’arrogance.</p>
<p>Lors de l’entretien, évitez toute forme excessive d’interventionnisme ou de digression : c’est le recruteur qui oriente le cours de l’entretien.</p>
<p>Evitez les questions hors-sujet ou inadéquates (chèque-déjeuner…), à moins que l’employeur ne vous invite à parler de sujets parallèles.</p>
<p>Ne quittez pas l’entretien sans avoir préalablement noté un certain nombre d’informations essentielles : qui devez-vous recontacter ? Quand et comment ?</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/emploi-stage/article/entretien-d-embauche-111290). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
