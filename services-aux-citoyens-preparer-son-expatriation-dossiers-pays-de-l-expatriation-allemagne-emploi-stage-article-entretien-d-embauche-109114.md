# Entretien d’embauche

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/emploi-stage/article/entretien-d-embauche-109114#sommaire_1">Conduite de l’entretien</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/emploi-stage/article/entretien-d-embauche-109114#sommaire_2">Négociation du salaire</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/emploi-stage/article/entretien-d-embauche-109114#sommaire_3">Questions préférées des recruteurs</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/emploi-stage/article/entretien-d-embauche-109114#sommaire_4">Erreurs à éviter</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Conduite de l’entretien</h3>
<h4 class="spip">Apparence et attitude</h4>
<p>L’entretien d’embauche se nomme en allemand <i>Vorstellungsgespräch</i>.</p>
<p>L’entretien est le moyen de sélection préféré des recruteurs allemands. L’entreprise vous convoquera à toute une série de rencontres. Le premier contact s’effectue en général avec le responsable du personnel et les suivants avec le ou les responsables du service dans lequel vous serez appelé à travailler.</p>
<p>L’entretien individuel est l’étape décisive à laquelle il convient de se préparer. L’employeur recherche un collaborateur qui réponde à certains critères et qui puisse s’intégrer dans l’entreprise. Pour cela, il vous jugera principalement sur trois points :</p>
<ul class="spip">
<li>votre personnalité (60%)</li>
<li>votre motivation pour le poste (25%)</li>
<li>vos compétences professionnelles (15%). Il convient de se préparer à l’entretien en tenant compte de trois aspects : professionnel, psychologique et organisationnel. L’important sera, ici encore, la cohérence de la présentation.</li></ul>
<h4 class="spip">Les compétences professionnelles</h4>
<p>On vous demandera en général de présenter votre niveau d’études et de formation, votre technicité et votre savoir-faire pratique acquis au cours de vos expériences professionnelles et éventuellement de vos formations complémentaires, ainsi que vos compétences relationnelles.</p>
<p>Les compétences personnelles devront être mises en valeur au cours de la description de votre parcours, sans les énoncer explicitement, mais en les démontrant par des exemples. Faites vous donc une liste de vos propres missions et réussites en précisant, pour chaque qualité sous-jacente, les exemples appropriés.</p>
<h4 class="spip">La préparation psychologique</h4>
<p>Si le poste à pourvoir est un poste à responsabilités, sachez que le recruteur aura pour mission, entre autres, de tester votre " résilience ", c’est-à-dire votre réactivité face aux situations difficiles. Il vérifiera si le savoir-faire dont vous faites état dans votre lettre de motivation correspond à l’impression que vous donnez en entretien. Pour les recruteurs, l’entretien d’embauche est l’occasion idéale de tester votre caractère et votre motivation. Ils chercheront à savoir quel type de personne vous êtes et comment vous vous présentez.</p>
<p>Dans les grands groupes, l’entreprise vous invitera éventuellement à participer à des journées d’évaluation dans un centre spécialisé (" assessment center "). Elles consistent en une mise en situation pendant une ou plusieurs journées dans le service recruteur. Ces journées d’évaluation sont l’occasion de vous soumettre à toute une batterie de tests psychotechniques, des jeux de rôle, des discussions de groupe, des entretiens individuels qui ont pour objectif d’évaluer vos capacités intellectuelles, sociales et émotionnelles.</p>
<h4 class="spip">La préparation organisationnelle</h4>
<p>Il est fortement recommandé de confirmer la date de l’entretien par écrit ou par téléphone.</p>
<p>Les retards ne sont généralement pas pardonnés en Allemagne. Même si vous vous croyez victime de circonstances défavorables, cet argument ne compte pas. L’idée du professionnalisme à l’allemande présuppose la qualité d’être toujours maître des circonstances. Si vous avez du retard, il est préférable d’en informer l’interlocuteur avant l’heure fixée pour l’entretien. Prévoyez des marges de temps très larges. Cela permet également d’éviter la précipitation et la fatigue avant l’entretien.</p>
<p>Les vêtements doivent être d’un style un peu plus classique qu’habituellement, tout en restant en rapport avec le type de travail demandé. Attention aux vêtements déjà un peu élimés et défraîchis. Ne courez pas non plus le risque de porter des chaussures aux semelles même légèrement abîmées. De même, tout signe qui pourrait faire soupçonner un manque d’hygiène est évidemment à éviter.</p>
<p>Enfin, il est intéressant de noter qu’en principe l’entreprise qui a invité le candidat est obligée par la loi de rembourser les frais de déplacement.</p>
<h3 class="spip"><a id="sommaire_2"></a>Négociation du salaire</h3>
<p>En ce qui concerne la question de la <strong>rémunération</strong>, ne vous avancez pas sur vos prétentions de salaire. Il est toujours préférable que ce soit la proposition de votre interlocuteur qui serve de base à la négociation.</p>
<p>Néanmoins, pouvoir évaluer et négocier ce que vous valez fait partie du professionnalisme, surtout si vous êtes un commercial. Fixez donc une fourchette en tenant compte du fait que les salaires en Allemagne, surtout pour les cadres moyens, sont plus élevés qu’en France. Pensez également au fait que compte tenu des charges sociales et de l’impôt prélevé à la source, la différence entre le salaire brut et net peut aller jusqu’à plus de 50%.</p>
<h3 class="spip"><a id="sommaire_3"></a>Questions préférées des recruteurs</h3>
<p>En règle générale, dès la première entrevue et quelle que soit l’étape du recrutement, préparez-vous à répondre, au cours d’un entretien en allemand aux <strong>cinq grandes questions</strong> suivantes :</p>
<ul class="spip">
<li>Quelles fonctions aimeriez-vous exercer dans notre entreprise ? (<i>Für welche Tätigkeit interessieren Sie sich bei uns ?</i>)</li>
<li>Avez-vous déjà travaillé à l’étranger ? (<i>Haben Sie schon im Ausland gearbeitet</i> ?)</li>
<li>Avez-vous déjà travaillé au sein d’une équipe ? (<i>Haben Sie schon im Team gearbeitet</i> ?)</li>
<li>Pour quelles raisons pensez-vous que notre entreprise pourrait vous engager pour ces fonctions ? (<i>Warum glauben Sie, dass unsere Firma Sie für diese Tätigkeit einstellen könnte </i> ?)</li>
<li>Quelles compétences professionnelles allez-vous nous apporter ? (<i>Welche Fachkenntnisse bringen Sie mit </i> ?) Répondez de manière naturelle et personnelle. Cherchez à établir un contact avec vos interlocuteurs, impliquez vous, posez des questions. Faites la preuve de vos qualités humaines. Soyez ouvert, réceptif et communicatif.</li></ul>
<p>Les recruteurs affectionnent également les « questions difficiles » du type " Qui êtes-vous ? " ou encore " Quels sont vos défauts ? ", " Avez-vous connu des échecs ? ". Il faut répondre à ces questions de manière concise, ouverte et positive. On ne vous croira pas si vous niez avoir des défauts ou avoir connu des échecs.</p>
<p>La question de savoir si une candidate est enceinte fait partie des questions interdites par la loi allemande, ainsi que toute question ne respectant pas l’intégrité de la personne. Si celles-ci vous sont posées, il est licite de répondre par un mensonge.</p>
<h3 class="spip"><a id="sommaire_4"></a>Erreurs à éviter</h3>
<ul class="spip">
<li>arriver en retard. Prévoyez donc des marges de temps très larges ;</li>
<li>se présenter dans une tenue vestimentaire laissant à désirer (vêtements élimés ou défraîchis, chaussures abîmées, etc.) ;</li>
<li>tout signe qui pourrait laisser supposer un manque d’hygiène de votre part.</li></ul>
<p><i>Mise à jour : juillet 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/emploi-stage/article/entretien-d-embauche-109114). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
