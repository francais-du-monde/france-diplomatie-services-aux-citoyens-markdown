# Protection sociale

<h2 class="rub22834">Régime local de sécurité sociale</h2>
<p>Vous trouverez une présentation détaillée du système de sécurité sociale turque sur le site de notre partenaire, le <a href="http://www.cleiss.fr/" class="spip_out" rel="external">CLEISS</a> (Centre des liaisons européennes et internationales de sécurité sociale). En voici la table des matières :</p>
<ul class="spip">
<li><a href="http://www.cleiss.fr/docs/regimes/regime_turquie.html#generalites" class="spip_out" rel="external">Généralités</a> (Organisation, financement, taux de cotisation sur les salaires…)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_turquie.html#maladie" class="spip_out" rel="external">Maladie</a> et <a href="http://www.cleiss.fr/docs/regimes/regime_turquie.html#maternite" class="spip_out" rel="external">maternité</a> (Prestations en nature et en espèces)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_turquie.html#adt" class="spip_out" rel="external">Accident du travail - maladies professionnelles</a> (Régime indemnitaire, y compris pour les survivants)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_turquie.html#invalidite" class="spip_out" rel="external">Invalidité, vieillesse, décès (survivants)</a> (Pensions de vieillesse, d’invalidité, de survivant, aide sociale)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_turquie.html#chomage" class="spip_out" rel="external">Chômage</a> (Indemnités et assistance chômage)</li></ul>
<p><strong>Système de sécurité sociale turc </strong></p>
<p>En 2007, le système turc de sécurité sociale a fait l’objet de profondes transformations avec l’entrée en vigueur de la loi no 5510. Ce dernier a ainsi gagné en efficacité et en rapidité, grâce au contrôle centralisé des différents fonds de sécurité sociale au sein d’une institution unique.</p>
<p>Dans le cadre de ce programme, les trois fonds de sécurité sociale, à savoir SSK (régime pour les salariés), Emekli Sandigi (pour les fonctionnaires) et Bag-Kur (pour les travailleurs indépendants), ont été fusionnés au sein d’un même organisme nommé Institution de sécurité sociale (SGK).</p>
<p>Employeurs et employés sont chacun redevables des cotisations de sécurité sociale (en pourcentage des revenus bruts du salarié). La part de l’employeur s’établit à 21,5 % alors que celle de l’employé est de 15 %. Les taux applicables aux employés travaillant dans des secteurs spécifiques (notamment le secteur minier, l’exploration pétrolière/gazière) peuvent varier selon la catégorie de risque à laquelle appartient l’emploi considéré.</p>
<p>Les ressortissants étrangers qui cotisent à la sécurité sociale dans leur pays d’origine ne sont pas soumis à cotisation en Turquie si les deux pays ont signé un accord mutuel à ce sujet.</p>
<p><strong>L’assurance chômage</strong></p>
<p>Les employés, les employeurs et l’État doivent verser au fonds d’assurance chômage une cotisation mensuelle obligatoire à un taux respectif de 1%, 2% et 1% du salaire brut de l’employé. Les employeurs peuvent déduire ces cotisations de leurs bénéfices imposables. Quant aux employés, ils peuvent déduire ces cotisations de leur assiette d’imposition sur le revenu.</p>
<p>Les ressortissants étrangers qui restent couverts par le régime obligatoire de sécurité sociale dans leur pays d’origine sont exemptés de cotisations en Turquie si les deux pays ont signé un accord à ce sujet. Le justificatif d’immatriculation dans un pays étranger doit être présenté au bureau local de sécurité sociale. Si l’employé n’est pas assuré à l’étranger, il devra généralement verser l’intégralité des cotisations. Les cotisations d’assurance chômage sont déclarées et acquittées auprès de l’Institution de sécurité sociale en même temps que les cotisations de sécurité sociale.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-turquie-protection-sociale-article-convention-de-securite-sociale.md" title="Convention de sécurité sociale">Convention de sécurité sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-turquie-protection-sociale-article-regime-local-de-securite-sociale-110025.md" title="Régime local de sécurité sociale">Régime local de sécurité sociale</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/protection-sociale/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
