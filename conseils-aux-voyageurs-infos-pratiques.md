# Infos pratiques

<ul class="cadre_gris cadre_gris_hauteur">
<li><a href="https://pastel.diplomatie.gouv.fr/fildariane/dyn/public/login.html;jsessionid=70E713254C8A9BE32B9026144D01E464.jvm01944-2" title="Préparer son départ">
<img class="spip_logos" alt="Préparer son départ" src="http://www.diplomatie.gouv.fr/fr/IMG/rubon20975.jpg?1309776737" width="340" height="120">
<span class="panneau_static">
<span class="texte_dossiers">Préparer son départ</span>
</span>
</a></li>
<li class="sous_rub">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/infos-pratiques/preparer-son-depart/argent/">Argent</a></li>
<li class="sous_rub">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/infos-pratiques/preparer-son-depart/assurances/">Assurances</a></li>
<li class="sous_rub">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/infos-pratiques/preparer-son-depart/ariane/">Ariane</a></li>
<li class="sous_rub">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/infos-pratiques/preparer-son-depart/formalites-administratives/">Formalités administratives</a></li>
<li class="sous_rub">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/infos-pratiques/preparer-son-depart/transport-aerien/">Transport aérien</a></li>
<li class="sous_rub">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/infos-pratiques/preparer-son-depart/avertissement/">Avertissement</a></li>
</ul>
<ul class="cadre_gris cadre_gris_hauteur cols2-1024-dernier cols2-980-1024-dernier">
<li><a href="conseils-aux-voyageurs-infos-pratiques-legislation-mineurs-a-l-etranger.md" title="Législation">
<img class="spip_logos" alt="Législation" src="http://www.diplomatie.gouv.fr/fr/IMG/rubon20980.jpg?1309776748" width="340" height="120">
<span class="panneau_static">
<span class="texte_dossiers">Législation</span>
</span>
</a></li>
<li class="sous_rub">
<a href="conseils-aux-voyageurs-infos-pratiques-legislation-mineurs-a-l-etranger.md">Mineurs à l’étranger</a></li>
<li class="sous_rub">
<a href="conseils-aux-voyageurs-infos-pratiques-legislation-legislation-locale.md">Législation locale</a></li>
<li class="sous_rub">
<a href="conseils-aux-voyageurs-infos-pratiques-legislation-actes-pedophiles-20983.md">Actes pédophiles</a></li>
<li class="sous_rub">
<a href="conseils-aux-voyageurs-infos-pratiques-legislation-contrefacon-20984.md">Contrefaçon</a></li>
</ul>
<ul class="cadre_gris cadre_gris_hauteur">
<li><a href="conseils-aux-voyageurs-infos-pratiques-assistance-aux-francais-le-role-d-un-consulat.md" title="Assistance aux Français">
<img class="spip_logos" alt="Assistance aux Français" src="http://www.diplomatie.gouv.fr/fr/IMG/rubon20985.jpg?1309776773" width="340" height="120">
<span class="panneau_static">
<span class="texte_dossiers">Assistance aux Français</span>
</span>
</a></li>
<li class="sous_rub">
<a href="conseils-aux-voyageurs-infos-pratiques-assistance-aux-francais-le-role-d-un-consulat.md">Le rôle d’un consulat</a></li>
<li class="sous_rub">
<a href="conseils-aux-voyageurs-infos-pratiques-assistance-aux-francais-incarceration-20987.md">Incarcération</a></li>
<li class="sous_rub">
<a href="conseils-aux-voyageurs-infos-pratiques-assistance-aux-francais-victimes-a-l-etranger-20988.md">Victimes à l’étranger</a></li>
<li class="sous_rub">
<a href="conseils-aux-voyageurs-infos-pratiques-assistance-aux-francais-disparitions-inquietantes.md">Disparitions inquiétantes</a></li>
<li class="sous_rub">
<a href="conseils-aux-voyageurs-infos-pratiques-assistance-aux-francais-deces-a-l-etranger.md">Décès à l’étranger</a></li>
<li class="sous_rub">
<a href="conseils-aux-voyageurs-infos-pratiques-assistance-aux-francais-mariages-forces.md">Mariages forcés</a></li>
</ul>
<ul class="cadre_gris cadre_gris_hauteur cols2-1024-dernier cols2-980-1024-dernier">
<li><a href="conseils-aux-voyageurs-infos-pratiques-risques-risques-sanitaires.md" title="Risques">
<img class="spip_logos" alt="Risques" src="http://www.diplomatie.gouv.fr/fr/IMG/rubon20992.jpg?1309776783" width="340" height="120">
<span class="panneau_static">
<span class="texte_dossiers">Risques</span>
</span>
</a></li>
<li class="sous_rub">
<a href="conseils-aux-voyageurs-infos-pratiques-risques-risques-sanitaires.md">Risques sanitaires</a></li>
<li class="sous_rub">
<a href="conseils-aux-voyageurs-infos-pratiques-risques-risque-nucleaire.md">Risque nucléaire</a></li>
<li class="sous_rub">
<a href="conseils-aux-voyageurs-infos-pratiques-risques-risques-naturels.md">Risques naturels</a></li>
<li class="sous_rub">
<a href="conseils-aux-voyageurs-infos-pratiques-risques-piraterie-maritime.md">Piraterie maritime</a></li>
<li class="sous_rub">
<a href="conseils-aux-voyageurs-infos-pratiques-risques-cybercriminalite.md">Cybercriminalité</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/infos-pratiques/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
