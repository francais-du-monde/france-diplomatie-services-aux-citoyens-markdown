# Vous revenez d’un pays de l’Union européenne

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/la-retraite/article/vous-revenez-d-un-pays-de-l-union-103674#sommaire_1">Les conditions pour liquider une pension communautaire</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/la-retraite/article/vous-revenez-d-un-pays-de-l-union-103674#sommaire_2">Les régimes de retraite complémentaire Arrco - Agirc</a></li></ul>
<p>Le règlement communautaire 1408/71 impose aux caisses de retraite, lorsqu’elles liquident la pension de base, d’intégrer les périodes d’assurance accomplies dans les pays de l’Union européenne et de l’Espace économique européen (Norvège, Islande, Liechtenstein). En application de l’accord conclu avec la Suisse, les périodes accomplies dans les Etats de l’Union européenne et de la Suisse peuvent être totalisées.</p>
<p>Il n’est toutefois pas possible de totaliser les périodes accomplies dans l’UE, l’EEE et la Suisse.</p>
<p>Avant de revenir en France, il convient de se procurer un <strong>relevé de carrière. </strong></p>
<p>L’âge à partir duquel il est possible de formuler une demande de pension de vieillesse diffère suivant l’Etat liquidateur de la pension.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.cleiss.fr/infos/pension-calcul-rgt.html" class="spip_out" rel="external">CLEISS</a> </p>
<h3 class="spip"><a id="sommaire_1"></a>Les conditions pour liquider une pension communautaire</h3>
<p>La condition essentielle pour liquider une pension est d’en faire la demande. L’organisme se charge alors de contacter les institutions des Etats dans lesquels le demandeur a effectué une activité professionnelle.</p>
<p><strong>Une année d’assurance </strong>doit avoir été accomplie dans chaque Etat de l’Union européenne où vous avez exercé une activité. Si ce n’est pas le cas, les caisses françaises prennent malgré tout en compte les quelques trimestres étrangers. Le règlement prévoit des modalités particulières de calcul de la pension communautaire lorsque la durée d’assurance accomplie dans un Etat n’atteint pas un an et qu’aucun droit n’est ouvert à ce titre.</p>
<p>Vous devez remplir, dans chacun de ces pays, les conditions pour pouvoir obtenir le versement d’une pension (atteindre l’âge légal).</p>
<h3 class="spip"><a id="sommaire_2"></a>Les régimes de retraite complémentaire Arrco - Agirc</h3>
<p><strong>Ces régimes font partie du champ d’application de la coordination communautaire.</strong></p>
<p>En application de ces règles, les caisses de retraites complémentaires sont tenues d’assurer des équivalences en reconnaissant une situation survenue dans un autre Etat de l’Union européenne comme si elle était arrivée en France.</p>
<p>Mais, en pratique, la coordination est limitée car il n’existe pas de régime de retraite complémentaire européen équivalent. En effet, la plupart sont gérés par capitalisation alors que les régimes Arrco-Agirc sont des régimes par répartition.</p>
<p>Il est donc conseillé d’adhérer volontairement auprès de la CRE (régime Arrco) et de l’IRCAFEX (régime des cadres Agirc) afin de continuer à acheter vos points de retraites complémentaires à partir d’un pays de l’Union européenne.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.info-retraite.fr/mon-parcours-retraite/je-suis-expatrie-je-lai-ete-ou-je-compte-letre" class="spip_out" rel="external">info-retraite.fr</a></li>
<li><a href="http://www.cleiss.fr/" class="spip_out" rel="external">Centre des Liaisons Européennes et Internationales de Sécurité Sociale</a> (CLEISS)<br class="manualbr">11, rue de la Tour des Dames<br class="manualbr">75436 Paris Cedex 09 <br class="manualbr">Tél : 01 45 26 33 41</li></ul>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.cnav.fr/" class="spip_out" rel="external">Caisse nationale d’assurance vieillesse (CNAV)</a> – Siège (ne reçoit pas le public) <br class="manualbr">110 avenue de Flandre <br class="manualbr">75951 Paris cedex 19 <br class="manualbr">Tél. : 3960 (serveur vocal 24H/24) <br class="manualbr">Pour appeler de l’étranger, composer le 09 71 10 39 60 </p>
<p><i>Mise à jour : avril 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/la-retraite/article/vous-revenez-d-un-pays-de-l-union-103674). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
