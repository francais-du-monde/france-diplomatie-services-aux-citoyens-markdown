# Convention de sécurité sociale

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/protection-sociale-22764/article/convention-de-securite-sociale#sommaire_1">Travailleurs non détachés</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/protection-sociale-22764/article/convention-de-securite-sociale#sommaire_2">Droits du travailleur pour lui-même et pour sa famille si elle l’accompagne</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/protection-sociale-22764/article/convention-de-securite-sociale#sommaire_3">Droits des membres de la famille demeurés en France</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/protection-sociale-22764/article/convention-de-securite-sociale#sommaire_4">Droits du travailleur en matière de chômage</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/protection-sociale-22764/article/convention-de-securite-sociale#sommaire_5">Droits des pensionnés en matière de soins de santé</a></li></ul>
<p>Le Royaume-Uni, qui est l’un des 28 États membres de l’Union européenne, est lié à la France par un certain nombre d’accords de sécurité sociale dont les principaux sont les règlements (CEE) n° 1408/71 et n° 574/72, relatifs à l’application des régimes de sécurité sociale aux travailleurs salariés et non salariés et aux membres de leur famille qui se déplacent à l’intérieur de la Communauté.</p>
<p>Dans le cadre de l’accord de l’Espace économique européen (E.E.E.) et de l’accord entre l’Union européenne et la Suisse, les règlements communautaires 1408/71 et 574/72 sont applicables aux territoires et aux ressortissants de l’Islande, de la Norvège, du Liechtenstein et de la Suisse.</p>
<p>Les Français occupés au Royaume-Uni relèvent, en principe, obligatoirement du régime britannique de protection sociale. Ils bénéficient, par ailleurs, des règlements communautaires de sécurité sociale leur permettant, en quelque sorte, le passage du régime français au régime britannique de sécurité sociale et réciproquement.</p>
<p>Les Français occupés au Royaume-Uni peuvent, s’ils le désirent, adhérer à l’assurance volontaire "expatriés" auprès de la <a href="http://www.cfe.fr/" class="spip_out" rel="external">Caisse des Français de l’étranger</a>. Il convient de préciser qu’une telle adhésion ne dispense pas les intéressés des obligations d’assurance existant dans le pays de travail.</p>
<p>Les Français travaillant au Royaume-Uni peuvent aussi être maintenus au régime français de protection sociale, c’est-à-dire détachés dans le cadre des seuls règlements communautaires de sécurité sociale.</p>
<p>Bien entendu, les Français se trouvant au Royaume-Uni en tant que touristes, étudiants, retraités ou chômeurs cherchant un emploi peuvent bénéficier également des règlements communautaires.</p>
<p>Tout renseignement complémentaire au sujet de l’application des règlements communautaires en matière de sécurité sociale peut être obtenu auprès du :</p>
<p><a href="http://www.cleiss.fr/docs/textes/rgt_index.html" class="spip_out" rel="external">Centre des liaisons européennes et internationales de sécurité sociale</a> (CLEISS)<br class="manualbr">11, rue de la Tour des Dames<br class="manualbr">75436 Paris cedex 09<br class="manualbr">Tél. : 01 45 26 33 41<br class="manualbr">Fax : 01 49 95 06 50</p>
<h3 class="spip"><a id="sommaire_1"></a>Travailleurs non détachés</h3>
<p>En vertu du principe de l’égalité de traitement posé par l’article 3, du règlement CEE 1408/71, le travailleur français occupé au Royaume-Uni est soumis au régime local comme s’il était ressortissant britannique.</p>
<h3 class="spip"><a id="sommaire_2"></a>Droits du travailleur pour lui-même et pour sa famille si elle l’accompagne</h3>
<h4 class="spip">Pendant la période d’emploi au Royaume-Uni</h4>
<p>Le travailleur est assujetti au régime britannique au titre de son activité dans ce pays. Des dispositions particulières sont toutefois applicables aux personnes qui exercent normalement une activité salariée sur le territoire de deux ou plusieurs États membres (personnel d’une entreprise effectuant des transports internationaux, V.R.P). Les prestations sont coordonnées.</p>
<h5 class="spip">Maladie, maternité</h5>
<p><strong>Totalisation</strong></p>
<p>Si l’intéressé est assuré à titre d’une activité professionnelle, les périodes d’assurance, d’emploi ou de résidence, accomplies sur le territoire d’un ou plusieurs autres.</p>
<p>États auxquels les règlements sont applicables, sont prises en compte, en tant que de besoin, par l’organisme britannique compétent pour l’examen des droits éventuels de l’intéressé aux prestations.</p>
<p>Le travailleur français non détaché aura donc intérêt à demander avant le départ à sa caisse d’affiliation, l’établissement du formulaire E 104 "Attestation concernant la totalisation des périodes d’assurance, d’emploi ou de résidence" qui sera à remettre à l’organisme britannique compétent.</p>
<h5 class="spip">Invalidité</h5>
<p>La législation française étant, sauf dans le régime minier, de type A (montant de la pension d’invalidité indépendant de la carrière d’assurance) et la législation britannique de type A, le travailleur qui aura été soumis à ces deux législations verra ses prestations liquidées conformément à la législation dont il relevait au moment de l’interruption de travail suivie d’invalidité. Pour l’ouverture des droits aux prestations il pourra éventuellement être fait appel, en cas de besoin, aux périodes d’assurance accomplies dans l’autre État.</p>
<h5 class="spip">Vieillesse</h5>
<p>Dès lors qu’une année d’assurance a été accomplie, chaque pays où le travailleur a exercé une activité professionnelle rémunère les périodes d’assurance accomplies sous sa législation.</p>
<p>Chaque institution procède à un double calcul de la pension. Elle détermine le montant de la pension du requérant en fonction des seules périodes d’assurance accomplies sous sa législation.</p>
<p>Ensuite, elle totalise les périodes d’assurance accomplies sous sa législation et sous les législations auxquelles le travailleur a été soumis. Elle détermine ainsi une pension théorique qu’elle proratise en fonction des seules périodes d’assurance accomplies sous sa législation par rapport à la totalité des périodes d’assurance accomplies sous les législations auxquelles le travailleur a été soumis.</p>
<p>Ensuite, elle compare le montant de la pension nationale et celui de la pension proratisée et verse le montant le plus avantageux des deux.</p>
<p>De cette manière, les périodes d’assurance accomplies sur le territoire d’un autre État visé dans le champ d’application du règlement pourront être prises en compte pour la détermination du taux de liquidation de la pension française, celle-ci étant ensuite calculée sur la base des périodes d’assurance effectuées en France.</p>
<h5 class="spip">Prestations familiales</h5>
<p>Le travailleur a droit pour les membres de sa famille aux prestations familiales du régime britannique.</p>
<h4 class="spip">Pendant un séjour temporaire</h4>
<p>Le travailleur et les membres de sa famille qui effectuent un séjour temporaire en France ont droit aux prestations en nature (soins) si leur état vient à nécessiter immédiatement des soins.</p>
<p>Ces prestations pourront être servies par la Caisse primaire d’assurance maladie du lieu des soins sur présentation de la carte européenne d’assurance maladie qui aura été établie avant le départ par la caisse britannique d’affiliation.</p>
<h4 class="spip">A l’occasion d’un transfert de résidence</h4>
<p>Le travailleur admis au bénéfice des prestations, sous réserve d’être autorisé par l’organisme britannique à retourner en France, conserve ses droits aux prestations. L’intéressé devra donc solliciter, avant le départ, l’établissement du formulaire E 112 "Attestation concernant le maintien des prestations en cours de l’assurance maladie-maternité" ou E 123 "Attestation de droit aux prestations en nature de l’assurance contre les accidents du travail et les maladies professionnelles".</p>
<h4 class="spip">Transfert de résidence pour se faire soigner</h4>
<p>Pour venir se faire soigner en France l’assuré ou ses ayants droits doivent obtenir l’autorisation de la caisse d’assurance maladie britannique.</p>
<p>Cette autorisation ne peut pas être refusée si les soins dont il s’agit figurent parmi les prestations prévues par la législation britannique et si ces soins ne peuvent, compte tenu de l’état de santé du requérant, être dispensés dans un délai normalement nécessaire.</p>
<h3 class="spip"><a id="sommaire_3"></a>Droits des membres de la famille demeurés en France</h3>
<h4 class="spip">Maladie, maternité</h4>
<p>Les membres de la famille auront droit aux soins de santé, pour autant qu’ils n’aient pas droit à ces prestations en vertu de la législation française.</p>
<p>Pour ce faire, le formulaire E 109 "Attestation pour l’inscription des membres de la famille du travailleur salarié ou non salarié et la tenue des inventaires" doit être établi par l’institution britannique et remis à la Caisse primaire d’assurance maladie compétente en fonction du lieu de résidence de la famille.</p>
<h4 class="spip">Prestations familiales</h4>
<p>Ils ont droit aux prestations familiales prévues par la législation britannique.</p>
<p>Toutefois, dans le cadre de la législation française, la famille peut éventuellement obtenir des allocations différentielles qui viendront donc, le cas échéant, compléter les prestations britanniques pour les porter au niveau des prestations françaises.</p>
<p>Pour bénéficier des prestations familiales, le travailleur salarié ou non salarié présentera sa demande à l’organisme britannique compétent et produira, à l’appui de celle-ci, notamment le formulaire E 401 "Attestation concernant la composition de la famille en vue de l’octroi des prestations familiales" dûment complété par la mairie du lieu de résidence de la famille ou la caisse d’allocations familiales compétente.</p>
<h3 class="spip"><a id="sommaire_4"></a>Droits du travailleur en matière de chômage</h3>
<p>L’institution de chômage britannique pourra éventuellement, si le travailleur se trouve sans emploi au Royaume-Uni après y avoir repris une activité, faire appel aux périodes de travail accomplies en France pour servir des prestations de chômage du régime britannique. Pour ce faire, un formulaire E 301 sera établi par les services pour l’emploi français.</p>
<p>De même lors d’une reprise d’activité en France après une activité au Royaume-Uni, il pourra éventuellement être fait appel en cas de besoin aux périodes d’assurance au Royaume-Uni pour servir des prestations du régime français.</p>
<h4 class="spip">Transfert de résidence pour chercher un emploi</h4>
<p>Le travailleur français qui se trouverait au chômage au Royaume-Uni où il bénéficierait de prestations pourrait revenir en France pour y chercher un emploi en conservant ses droits à prestations à condition que :</p>
<ul class="spip">
<li>avant son départ, il ait été inscrit comme demandeur d’emploi et soit resté à la disposition des services de l’emploi britannique pendant au moins quatre semaines après le début du chômage ;</li>
<li>il se soit inscrit à son arrivée en France auprès des services pour l’emploi et se soit soumis aux règles de contrôle organisées en France.</li></ul>
<p>Ce droit aux prestations pourra être maintenu pendant une période maximale de trois mois ; passé ce délai le chômeur ne pourra éventuellement continuer à bénéficier des prestations du régime britannique qu’à condition de retourner dans ce pays.</p>
<h3 class="spip"><a id="sommaire_5"></a>Droits des pensionnés en matière de soins de santé</h3>
<h4 class="spip">Résidence</h4>
<p>Pour bénéficier des prestations en nature au Royaume-Uni, le pensionné du régime français qui n’ouvre pas droit aux prestations du régime obligatoire britannique au titre d’une activité professionnelle ou d’un avantage du régime britannique devra se faire inscrire ainsi que les membres de sa famille auprès de la caisse maladie de son lieu de résidence en présentant le formulaire E 121 "Attestation pour l’inscription des titulaires de pension ou de rente et la tenue des inventaires" établi par l’organisme débiteur de la pension ou de la rente.</p>
<p>Les membres de la famille du pensionné qui ne résident pas dans le même État membre que ce dernier peuvent bénéficier des prestations de l’assurance maladie servies par l’institution du lieu de résidence.</p>
<p>Le titulaire d’une pension du régime français résidant au Royaume-Uni, bénéficiant de l’assurance maladie dans ce pays au titre de sa pension française, sera soumis au précompte de cotisations maladie sur sa pension, aux taux applicables avant le 1er janvier 1998.</p>
<p>Si les prestations d’assurance maladie qui lui sont servies au Royaume-Uni ne sont pas à la charge du régime français de sécurité sociale, le précompte de cotisations ne sera pas effectué.</p>
<h4 class="spip">Séjour temporaire</h4>
<p>Pour bénéficier de prestations de l’assurance maladie lors d’un séjour temporaire dans un Etat membre, le pensionné devra demander à l’institution britannique de son lieu de résidence une carte européenne d’assurance maladie.</p>
<p>Ce document lui permettra de bénéficier en France de prestations en nature de l’assurance maladie pour les soins nécessaires à son état servies par la Caisse primaire d’assurance maladie dans la circonscription de laquelle les soins ont été dispensés.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p>Pour vous informer sur la protection sociale des Français résidant à l’étranger, vous pouvez consulter notre thématique sur la <a href="services-aux-citoyens-preparer-son-expatriation-protection-sociale.md" class="spip_in">protection sociale</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/protection-sociale-22764/article/convention-de-securite-sociale). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
