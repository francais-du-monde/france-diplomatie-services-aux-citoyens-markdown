# Entrée et séjour

<h2 class="rub22800">Passeport, visa, permis de travail</h2>
<p>Quel que soit le motif de votre séjour, renseignez-vous auprès de la section consulaire de l’ambassade du Japon à Paris qui vous informera sur la règlementation en matière d’entrée et de séjour au Japon, règlementation que vous devrez impérativement respecter. N’hésitez pas à consulter le site ministère de l’Intérieur japonais afin de vous familiariser avec les modalités d’entrée et de séjour dans le pays.</p>
<p>Une fois sur place ce sont les services de l’immigration japonais qui prennent le relais. <strong>En aucun cas, vous n’êtes autorisé à travailler au Japon sans permis adéquat. </strong></p>
<p>Le Consulat de France au Japon n’est pas l’organisme compétent pour répondre à vos demandes d’information concernant votre séjour au Japon.</p>
<h4 class="spip">Les différents types de visa</h4>
<p>Pour un séjour inférieur à trois mois, les Français n’ont pas besoin de visa d’entrée au Japon. Ils obtiennent un titre de "visiteur temporaire", qui n’autorise pas l’exercice d’une activité rémunérée.</p>
<p>Au-delà de trois mois, la législation sur l’entrée et le séjour des étrangers au Japon distingue deux catégories : visa et statut de résidence.</p>
<p>De nouvelles dispositions concernant l’entrée sur le territoire japonais sont entrées en vigueur le 20/11/2007. Depuis cette date, tout ressortissant étranger est dans<strong> l’obligation de se soumettre à la prise de données biométriques </strong>(prise des empreintes digitales et d’une photographie du visage) sous peine de se voir refuser l’entrée au japon.</p>
<h4 class="spip">Le visa</h4>
<p>Le visa est le titre d’entrée sur le territoire. Il est délivré par les autorités consulaires japonaises et se matérialise par l’apposition d’une vignette sur le passeport.</p>
<p>La demande de visa auprès d’un poste consulaire japonais comprend l’obtention préalable d’un <strong>certificat d’admissibilité</strong> (<i>certificate of eligibility</i>) complété par l’organisme d’accueil qui se procure le dossier auprès des services de l’immigration. Cette procédure peut être longue (six mois en moyenne).</p>
<p>Enfin, si l’obtention d’un visa autorise son titulaire à résider au Japon, elle ne permet pas l’interruption du séjour pour un voyage à l’étranger. La sortie du territoire est soumise à une autorisation spéciale, dite "permis de retour" (<i>Re-entry permit</i>), délivrée au Japon par les services de l’immigration.</p>
<p>En janvier 1999, le gouvernement français a signé avec le gouvernement japonais un accord relatif au visa "vacances-travail" qui permet à ses détenteurs de découvrir le Japon tout en exerçant une activité salariée pour une durée maximale d’un an, non renouvelable. Ce visa, qui s’adresse aux personnes âgées de 18 à 30 ans, s’obtient auprès de l’<a href="http://www.fr.emb-japan.go.jp/" class="spip_out" rel="external">ambassade du Japon à Paris</a> (rubrique Aller au Japon / formalités visa / Visa vacances-travail) ou du consulat général du Japon à Marseille ou Strasbourg.</p>
<p><a href="http://www.mofa.go.jp/j_info/visit/visa/index.html" class="spip_out" rel="external">Le guide des visas japonais</a> est consultable sur le site Internet du ministère des Affaires étrangères japonais.</p>
<h4 class="spip">Le statut de résidence</h4>
<p>Le titre de séjour au Japon est délivré pour une durée précise (six mois, un ans, trois ans) et détermine les activités que son titulaire est autorisé à exercer. Il existe 27 statuts différents correspondant chacun soit à un ensemble d’activités spécifiquement autorisées, soit à un statut personnel particulier.</p>
<p>A l’exception des titulaires des titres de séjour "Temporary Visitors" (séjours jusqu’à trois mois) et "Cultural Activities", tous les statuts de résidence accordés au Japon permettent de travailler sous les diverses conditions qui les caractérisent. La procédure la plus fréquente nécessite un <strong>certificat d’admissibilité</strong> (<i>certificate of eligibility</i>) émanant de la structure d’accueil.</p>
<h4 class="spip">Programmes de séjour de longue durée</h4>
<p><a href="http://www.etp.org/" class="spip_out" rel="external">Executive Training Program</a> (ETP) est un programme s’étalant sur 12 mois avec une partie d’apprentissage de la langue japonaise, une formation en management et une période de stage en entreprises.</p>
<p><a href="http://www.etp.org/" class="spip_out" rel="external">ETP Japan ou Executive Training Programm in Japan</a> est un programme conçu et financé par la Commission européenne depuis plus de 25 ans qui consiste en un programme de formation intensive de jeunes cadres d’entreprises européennes pour les aider à développer leurs échanges commerciaux avec le Japon.</p>
<p>Ce programme se compose de trois parties :</p>
<ul class="spip">
<li>trois mois de cours intensifs de japonais en Europe ;</li>
<li>six mois de séminaires ayant pour thèmes le Japon, les affaires, pratiques commerciales mais aussi la société japonaise et sa culture (au Japon) ;</li>
<li>un stage de trois mois dans une entreprise japonaise.</li></ul>
<p>Inscriptions :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Chambre de commerce et d’industrie de Paris<br class="manualbr">Tél : 01 55 65 36 83</p>
<p><strong>Programme JET </strong>qui a pour but de développer la compréhension mutuelle entre le Japon et de nombreux pays dont la France en invitant des jeunes à venir travailler dans des collectivités locales ou dans des établissements scolaires japonais. La durée du contrat est de un an. Pour être éligible, il faut être de nationalité française, avoir moins de 35 ans et être au minimum titulaire d’une licence. Une bonne connaissance du japonais est indispensable. Pour plus d’informations contacter :</p>
<p>Informations sur le site de l’<a href="http://www.fr.emb-japan.go.jp/" class="spip_out" rel="external">ambassade du Japon en France</a> / rubrique Culture et éducation / programmes pour aller au Japon et <a href="http://www.jetprogramme.org/" class="spip_out" rel="external">Jetprogramme.org</a></p>
<p><strong>Sites à consulter :</strong></p>
<ul class="spip">
<li><a href="http://www.fr.emb-japan.go.jp/index.html" class="spip_out" rel="external">Section consulaire de l’ambassade du Japon à Paris</a></li>
<li>Pour toutes informations plus précises relatives aux conditions de séjour au Japon, il est vivement conseillé de prendre l’attache de l’un des consulats du Japon en France mentionnés ci-dessus, et de consulter la page <a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/japon/" class="spip_in">Japon du site internet Conseils aux voyageurs</a>.</li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-japon-entree-et-sejour-article-animaux-domestiques-109843.md" title="Animaux domestiques">Animaux domestiques</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-japon-entree-et-sejour-article-vaccination-109842.md" title="Vaccination">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-japon-entree-et-sejour-article-demenagement-109841.md" title="Déménagement">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-japon-entree-et-sejour-article-passeport-visa-permis-de-travail.md" title="Passeport, visa, permis de travail">Passeport, visa, permis de travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/entree-et-sejour/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
