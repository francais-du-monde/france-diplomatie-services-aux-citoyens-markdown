# Santé

<p>Pour une présentation des risques épidémiologiques ainsi que des conseils sur la médecine au quotidien, consultez les pages suivantes :</p>
<ul class="spip">
<li><a href="http://www.ambafrance-no.org/Medecins-et-dentistes-francophones" class="spip_out" rel="external">Liste de médecins</a> sur le site du consulat de France à Oslo ;</li>
<li><a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/norvege/" class="spip_in">Page dédiée à la santé en Norvège</a> sur le site Conseils aux voyageurs.</li></ul>
<h4 class="spip">Médecine de soins</h4>
<h4 class="spip">Médecins</h4>
<p>L’état sanitaire de la Norvège, ainsi que son infrastructure médicale, sont comparables à ceux des autres pays développés. Toutes les pathologies sont suivies. A noter que les médecins ne se déplacent pas à domicile et ne consultent que durant les heures de bureau. En cas d’urgence, il faut se rendre à l’hôpital.</p>
<p>Le coût d’une consultation chez un généraliste est de 250 NOK (37,50€) tarif minimum en journée. Dans les cliniques les tarifs sont nettement supérieurs (800 NOK - 100€).</p>
<p>Chaque dentiste fixe ses tarifs. Compter en moyenne 600 NOK (75€) pour une simple consultation. Pour des actes, il faut compter environ 1000 NOK minimum (125€).</p>
<h4 class="spip">Médicaments</h4>
<p>Les médicaments, même les plus courants, ne sont délivrés que sur prescription médicale.</p>
<p>Il est vivement conseillé de consulter le médecin traitant avant le départ et de souscrire une assurance rapatriement.</p>
<p><strong>Pour un court séjour, se munir de la carte européenne d’assurance maladie à demander à votre caisse d’assurance maladie au moins 15 jours avant le départ.</strong> Pour plus d’information, consultez le site de l’<a href="http://www.ameli.fr/assures/droits-et-demarches/a-l-etranger/index.php" class="spip_out" rel="external">assurance maladie en ligne</a>.</p>
<p><strong>Numéros utiles :</strong> <br class="autobr">Ambulances/Urgences : 113 <br class="manualbr">Urgences à Oslo : 22 11 80 80 <br class="manualbr">Aide aux enfants : 800 33 321 / 800 31 700. <br class="manualbr">Victimes de violences : 22 65 54 55. <br class="manualbr">Aide psychologique : 810 03 339. <br class="manualbr">Drogue anonyme : 905 29 359.</p>
<p>Pour de plus amples renseignements, vous pouvez consulter le site de l’<a href="http://www.invs.sante.fr/" class="spip_out" rel="external">INVS</a>, ainsi que celui de l’<a href="http://www.who.int/fr/index.html" class="spip_out" rel="external">OMS</a>, qui vous renseigneront sur l’état sanitaire de ce pays, ou les sites de <a href="http://www.pasteur-lille.fr/" class="spip_out" rel="external">l’institut Pasteur de Lille</a> et de <a href="http://www.pasteur.fr/" class="spip_out" rel="external">l’institut Pasteur de Paris</a>.</p>
<p><i>Dernière mise à jour : 10/01/2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/norvege/vie-pratique/article/sante). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
