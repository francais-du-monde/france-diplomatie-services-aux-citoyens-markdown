# Entretien d’embauche

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/emploi-stage/article/entretien-d-embauche#sommaire_1">Conduite de l’entretien</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/emploi-stage/article/entretien-d-embauche#sommaire_2">Apparence et attitude</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/emploi-stage/article/entretien-d-embauche#sommaire_3">Négociation du salaire</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/emploi-stage/article/entretien-d-embauche#sommaire_4">Questions préférées des recruteurs</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/emploi-stage/article/entretien-d-embauche#sommaire_5">Erreurs à éviter</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/emploi-stage/article/entretien-d-embauche#sommaire_6">Après l’entretien</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Conduite de l’entretien</h3>
<p><strong>Le recruteur examinera principalement le candidat sur les aspects suivants :</strong></p>
<ul class="spip">
<li>Ses qualifications ;</li>
<li>Sa formation ;</li>
<li>Sa motivation et son dynamisme ;</li>
<li>Sa capacité à s’exprimer clairement, tout en gardant une marque de politesse, la pertinence de ses arguments ;</li>
<li>Son sérieux et son sens des responsabilités ;</li>
<li>Son niveau de langue ;</li>
<li>Son apparence.</li></ul>
<p><strong>Pour cela, tenez compte des conseils suivants :</strong></p>
<ul class="spip">
<li>Soyez ponctuel ;</li>
<li>Préparez votre entretien. Recherchez des informations sur l’entreprise (produits vendus, chiffres clés, organigramme, principaux concurrents, fournisseurs et clients, politique de l’entreprise) et sur le poste convoité ;</li>
<li>Le recruteur sera impressionné si vous savez faire preuve d’une bonne connaissance des activités de l’entreprise et si vous posez des questions pertinentes sur la société ;</li>
<li>Préparez des questions pertinentes, mais anticipez aussi les questions qui pourront vous être posées et préparez vos réponses ; souriez, votre interlocuteur l’appréciera ;</li>
<li>Soyez prêt à illustrer vos réussites antérieures par des exemples concrets ;</li>
<li>A la fin de l’entretien, demandez à votre interlocuteur dans quel délai et de quelle manière (par exemple, vous appellera-t-il ou devez vous l’appeler) il vous fera connaître sa décision. Remerciez-le d’avoir pris du temps pour s’entretenir avec vous : « Gracias por su atención » « Fue un gusto conocerle » ;</li>
<li>80% de l’impression des recruteurs est basée sur le visuel. L’aspect soigné et professionnel est de rigueur. Le comportement et l’attitude sont privilégiés : soyez positif. Soignez particulièrement votre apparence.</li>
<li>Faites attention à votre attitude, à la façon dont vous vous tenez : épaules en arrière et tête relevée montrent que vous êtes confiant. Maintenez toujours un comportement positif et énergique. Souriez, donnez des « feedbacks » non verbaux (hochement de tête) et verbaux (« correcto », « exacto ») à votre interlocuteur.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Apparence et attitude</h3>
<p><strong>Soyez ponctuel et tenez compte des temps de transport</strong>, parfois variables. Même si les Chiliens ont en général une conception assez souple du temps, un retard à un premier entretien sera perçu de façon négative.</p>
<p>Les codes chiliens sont stricts et classiques : <strong>une touche de maquillage et un tailleur pour les dames, costumes pour les messieurs </strong>(avec cravate et chaussettes sobres).</p>
<p>Adaptez votre discours à celui du recruteur <strong>en volume, en rapidité et en politesse</strong>. L’entretien débutera par un vouvoiement réciproque (Usted). Votre interlocuteur peut ensuite décider, selon votre âge, de vous tutoyer. Ne vous vexez pas, c’est aussi une marque de sympathie.</p>
<p>Vous pouvez ensuite tutoyer votre interlocuteur si et seulement si :</p>
<ul class="spip">
<li>vous avez approximativement le même âge,</li>
<li>il vous y invite,</li>
<li>vous sentez qu’il attend de vous un tutoiement.</li></ul>
<p>Votre interlocuteur évaluera par-là votre capacité à intégrer les codes culturels chiliens qui régissent notamment les règles du tutoiement/vouvoiement. <strong>Dans le doute, restez au vouvoiement</strong>.</p>
<p>Ne vous effrayez pas des questions concernant votre vie privée (situation conjugale, nombre d’enfants, etc), mais essayez de centrer l’entretien sur l’aspect professionnel. Le recruteur ne cherchera pas à vous juger, mais à <strong>évaluer votre situation et le temps que vous êtes susceptible de rester au Chili</strong>, et donc dans l’entreprise.</p>
<p>Vous n’échapperez pas à la question : "Y porque Chile ?" (Pourquoi le Chili ?). <strong>Préparez par avance votre réponse et sachez vous justifier</strong> : je suis venu au Chili parce que … ; je compte y rester … années, etc.</p>
<p>Les Chiliens parlent beaucoup et facilement. Il est préférable de <strong>ne pas sortir d’un entretien sans avoir réussi à exposer vos motivations</strong>. Votre interlocuteur vous jugera aussi, notamment pour les commerciaux, sur votre autorité et votre capacité à intervenir dans une conversation.</p>
<p>Préparez-vous aux tests psychologiques et psychotechniques.</p>
<p>Sachez mettre en avant vos atouts tout en gardant une certaine modestie. Sinon, les employeurs pourraient vous percevoir plus comme un élément dangereux (monter en grade rapidement et prendre leur place) qu’un élément positif pour l’entreprise.</p>
<h3 class="spip"><a id="sommaire_3"></a>Négociation du salaire</h3>
<p>Dans le cadre d’un emploi ou d’un stage, vous devez anticiper les questions concernant votre salaire ou vos indemnités. Le recruteur pourra vous demander : combien gagniez-vous lors de votre emploi précédent ? Quelles sont vos prétentions salariales ?</p>
<p>La question du salaire vous sera toujours posée car c’est un critère de sélection. Pour formuler une réponse appropriée, renseignez-vous avant ou durant l’entretien sur les points suivants :</p>
<p>Quelle est la rémunération d’une personne occupant ce type de poste ?</p>
<p>S’agit-il d’un fixe, d’un fixe et de commissions, d’une avance sur commissions ?</p>
<p>Tenez compte des autres variables : ce poste est-il accompagné d’autres avantages (nourriture, transport, bonus, …) ?</p>
<p>Il faut savoir qu’en plus des indemnités de stage ou du salaire, d’autres avantages peuvent être négociés, notamment en matière de protection sociale.</p>
<p>Si vous avez un doute sur le montant, donnez une tranche de salaire qui vous semble raisonnable et qui tienne compte des caractéristiques du travail présentées par votre interlocuteur.</p>
<p>Soulignons que d’une ville à l’autre, le coût de la vie peut varier de façon importante. Un salaire de $ 400 000 pesos chiliens par mois n’a pas la même valeur à Santiago qu’à Temuco. Assurez-vous d’avoir bien compris où est basé le poste.</p>
<h3 class="spip"><a id="sommaire_4"></a>Questions préférées des recruteurs</h3>
<ul class="spip">
<li>Pourquoi le Chili ?</li>
<li>Depuis combien de temps êtes-vous au Chili ?</li>
<li>Qu´est-ce qui vous a motivé à poser votre candidature pour ce poste ? Quels sont vos projets professionnels ?</li>
<li>Quelle est votre expérience par rapport au poste proposé ? Questions techniques.</li>
<li>Test de votre niveau de langue en anglais, espagnol ou autre.</li>
<li>Qualités / Défauts.</li>
<li>Êtes-vous marié(e) ? Avez-vous des enfants ?</li>
<li>Combien de temps pensez-vous rester au Chili ?</li>
<li>Quelles sont vos prétentions salariales ?</li></ul>
<h3 class="spip"><a id="sommaire_5"></a>Erreurs à éviter</h3>
<ul class="spip">
<li>Arriver à l’entretien trop en avance ou en retard. Si vous êtes en avance, attendez l’heure de votre rendez-vous en relisant vos notes.</li>
<li>Fumer ou mâcher un chewing-gum.</li>
<li>Etre indiscret et regarder les papiers étalés sur le bureau.</li>
<li>Ne pas regarder l’interlocuteur droit dans les yeux. Répondre par des monosyllabes ou monologuer.</li>
<li>Evitez les discussions sur le salaire pendant la première partie de l’entretien. Laissez l’employeur vous en parler.</li>
<li>Ne posez pas de questions sur les jours de vacances, les congés maladie au début de l’entretien.</li>
<li>Ne vous plaignez pas, ne faites pas de demandes personnelles. Ne demandez pas à avoir des horaires ou des équipements spéciaux.</li>
<li>Ne donnez pas de liste de choses que vous ne voulez pas faire.</li>
<li>Ne demandez pas ce que l’entreprise fait d’autre. Vous êtes censé avoir pris vos renseignements.</li>
<li>A la question « Quels sont, selon vous, les points de votre parcours qui pourraient ne pas correspondre au profil demandé ? », ne répondez pas que vous ne pensez pas avoir de points négatifs, mais mettez en avant un point à améliorer (compétences techniques manquantes, …).</li>
<li>Evitez les déclarations malhonnêtes ou trompeuses.</li>
<li>Veillez à ne pas parler trop fort.</li></ul>
<h3 class="spip"><a id="sommaire_6"></a>Après l’entretien</h3>
<p>Les Chiliens sont toujours très positifs. Pour éviter le conflit et/ou par crainte de blesser leur interlocuteur, ils évitent autant que possible de dire « non » directement. <strong>Ne vous faites donc pas trop d’illusions</strong> en sortant d’un entretien qui vous a semblé positif. Il sera peut-être utile de comprendre à demi-mot ou d’interpréter un silence comme un refus.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/emploi-stage/article/entretien-d-embauche). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
