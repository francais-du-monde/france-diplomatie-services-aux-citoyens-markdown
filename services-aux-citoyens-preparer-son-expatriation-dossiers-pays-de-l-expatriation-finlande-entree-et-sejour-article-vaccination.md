# Vaccination

<p>Aucune vaccination n’est exigée à l’entrée en Finlande.</p>
<p>Il est toutefois conseillé de se faire vacciner contre l’encéphalite à tiques, en particulier en cas de séjour en zone rurale ou forestière.</p>
<p><strong>Pour en savoir plus</strong>, lisez <a href="http://www.diplomatie.gouv.fr/fr/spip.php?page=article&amp;id_article=117330" class="spip_out">notre article thématique sur la vaccination</a>.</p>
<p><i>Mise à jour : janvier 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/entree-et-sejour/article/vaccination). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
