# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/vie-pratique/article/transports-110349#sommaire_1">Importation de véhicule</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/vie-pratique/article/transports-110349#sommaire_2">Permis de conduire</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/vie-pratique/article/transports-110349#sommaire_3">Code de la route</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/vie-pratique/article/transports-110349#sommaire_4">Assurances et taxes</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/vie-pratique/article/transports-110349#sommaire_5">Achat et location</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/vie-pratique/article/transports-110349#sommaire_6">Immatriculation</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/vie-pratique/article/transports-110349#sommaire_7">Entretien</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/vie-pratique/article/transports-110349#sommaire_8">Réseau routier</a></li>
<li><a id="so_9" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/vie-pratique/article/transports-110349#sommaire_9">Transports en commun</a></li></ul>
<p>La liberté de circulation est totale. Il est recommandé de ne pas rouler de nuit sur les routes de campagne, où le réseau routier est encore en cours de réalisation, la signalisation demeurant sommaire. En cas d’accident entraînant des blessés ou des morts, un conducteur étranger peut être frappé d’interdiction de sortie du territoire jusqu’au jugement, sans même avoir la possibilité de verser une caution. Il est donc vivement recommandé, en cas d’accident, de se rendre immédiatement au commissariat de police le plus proche.</p>
<h3 class="spip"><a id="sommaire_1"></a>Importation de véhicule</h3>
<p>Il est possible d’importer un véhicule à usage personnel, sous réserve de payer des droits de douane équivalant à 15% de la valeur du véhicule, mais celui-ci doit avoir moins de deux ans. Plus généralement, tout véhicule entrant en Roumanie doit avoir moins de huit ans et répondre aux normes de pollution EUR3. Obligation également de faire immatriculer le véhicule dans un délai de 90 jours à compter de la date d’entrée sur le territoire roumain.</p>
<h3 class="spip"><a id="sommaire_2"></a>Permis de conduire</h3>
<p>La loi roumaine stipule que le permis français ou international doit être échangé contre un permis roumain dans les 90 jours qui suivent l’installation.</p>
<h3 class="spip"><a id="sommaire_3"></a>Code de la route</h3>
<p>La conduite s’effectue à droite et la priorité également. La vitesse maximale autorisée est de 50 km/h en agglomération et de 80 km/h sur route.</p>
<p>Si le taux d’alcoolémie dépasse les <strong>0,80 g/l de sang</strong>, ou <strong>0,40mg/l d’air expiré</strong>, le conducteur risque entre <strong>un an et cinq ans</strong> de prison.</p>
<h3 class="spip"><a id="sommaire_4"></a>Assurances et taxes</h3>
<p>L’assurance responsabilité civile, peu chère (50 euros en moyenne), est obligatoire. Il est fortement conseillé de prendre une assurance tous risques bien que les prix soient élevés.</p>
<h3 class="spip"><a id="sommaire_5"></a>Achat et location</h3>
<p>On peut louer des véhicules chez Avis, Budget, Europcar et Hertz à des prix semblables à ceux pratiqués en France. Le système de vignette auto n’existe pas en Roumaine. La carte grise est payante.</p>
<p>Il est possible d’acheter sur place un véhicule français ou étranger parfois 10% moins cher qu’en France. Il existe également un marché de voitures d’occasion à partir de 2000 euros.</p>
<p>Compte tenu de l’état des routes hors agglomération et de la rigueur de l’hiver, un véhicule 4X4 est conseillé même en ville. Quasiment toutes les marques françaises et étrangères sont représentées (Renault, Peugeot, Citroën, Volkswagen, BMW, marques japonaises, etc.).</p>
<h3 class="spip"><a id="sommaire_6"></a>Immatriculation</h3>
<p>Une vignette routière est délivrée pour les véhicules immatriculés à l’étranger.</p>
<p>Pour en savoir plus : <a href="http://www.roviniete.ro/" class="spip_out" rel="external">Roviniete.ro</a>.</p>
<h3 class="spip"><a id="sommaire_7"></a>Entretien</h3>
<p>Les véhicules sont soumis à un contrôle tous les trois ans et à chaque changement de propriétaire.</p>
<p>Les coûts d’entretien et de réparation sont inférieurs à ceux de la France mais le prix reste proportionnel à la qualité du travail effectué. Les pièces de rechange sont, le plus souvent, disponibles immédiatement.</p>
<h3 class="spip"><a id="sommaire_8"></a>Réseau routier</h3>
<p>Bien qu’en voie d’amélioration, le réseau routier reste inégal. Il existe deux autoroutes (Bucarest-Pitesti et Bucarest-Constanta), plusieurs tronçons étant en cours de réalisation.</p>
<h3 class="spip"><a id="sommaire_9"></a>Transports en commun</h3>
<p>Il est possible de se déplacer à l’intérieur du territoire en avion. Les liaisons aériennes entre les villes principales fonctionnent bien. La Roumanie dispose de 17 aéroports civils. Le train est le moyen de transport le plus utilisé. Le réseau est de bonne qualité et les tarifs sont peu élevés. Le confort étant variable, il est préférable d’utiliser la première classe.</p>
<p>Les conditions climatiques souvent difficiles en hiver peuvent retarder voire empêcher la circulation routière, ferroviaire ou aérienne.</p>
<p>En ville, on peut emprunter le bus, le tramway ou les taxis. Bucarest possède un métro (quatre lignes), moyen le plus commode et rapide de déplacement de la capitale</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/vie-pratique/article/transports-110349). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
