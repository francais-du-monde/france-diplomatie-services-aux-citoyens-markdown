# Santé

<h4 class="spip">Numéros d’urgence</h4>
<ul class="spip">
<li>Pompiers : 115</li>
<li>Samu : 118</li>
<li>Ambulances : 118</li>
<li>Police : 113</li>
<li>Carabinieri (gendarmes) : 112</li></ul>
<h4 class="spip">Hopitaux</h4>
<p>Tous les hôpitaux disposent d’un système d’urgences organisé en plusieurs niveaux selon la gravité de la situation.</p>
<p>Le système sanitaire public n’est pas toujours à même de répondre rapidement aux demandes, ce qui conduit les résidents à recourir à des établissements privés dont le coût est plus élevé qu’en France.</p>
<p>Les hôpitaux italiens sont conventionnés par Inter Mutuelle Assistance et les agents adhérents de la Mutuelle des Affaires étrangères n’ont pas à faire l’avance des frais hospitaliers (sous condition d’une demande préalable de prise en charge).</p>
<p>Adresses et téléphones des principaux hôpitaux à Rome :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Ospedale Pediatrico Bambino Gesù<br class="manualbr">4, p. S. Onofrio <br class="manualbr">00165 Roma (RM) <br class="manualbr">Tél. : 06 68591 </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Policlinico Umberto I<br class="manualbr">155, vl. Policlinico <br class="manualbr">00161 Roma (RM) <br class="manualbr">Tél. : 06 4465027 </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Azienda ospedaliera S. Camillo/Forlanini<br class="manualbr">87, Circonvallazione Gianicolense <br class="manualbr">00152 Roma (RM) <br class="manualbr">Tél. : 06 58701 </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Ospedale Fatebenefratelli<br class="manualbr">Piazza Fatebenefratelli <br class="manualbr">00186 Roma (RM) <br class="manualbr">Tél. : 06 68210828, 06 68892112 </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Ospedale Santo Spirito<br class="manualbr">1, Lungotevere in Sassia <br class="manualbr">00193 Roma (RM) <br class="manualbr">Tél. : 06 68351, 803 333 </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Ospedale Policlinico A. Gemelli<br class="manualbr">235, V. Pineta Sacchetti<br class="manualbr">00100 Roma (RM) <br class="manualbr">Tél. : 06 3053208, 06 3053262, 06 3053293 </p>
<h4 class="spip">Médecine de soins</h4>
<p>L’état sanitaire de l’Italie est semblable à celui des autres pays européens.</p>
<p>Des adresses de médecins et d’hôpitaux peuvent être obtenues auprès des différents <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/article/annuaire-des-representations-francaises-a-l-etranger" class="spip_in">consulats de France en Italie</a>.</p>
<p>Lors de votre arrivée en Italie, il vous faudra solliciter - gratuitement - votre inscription auprès de l’Agence sanitaire locale (A.S.L. / Azienda Sanitaria Locale) de votre quartier. Cette démarche vous permettra d’accéder à une couverture médicale et bénéficier ainsi de visites gratuites chez le médecin référent. Il vous faudra fournir une pièce d’identité valide, le code fiscal de chaque membre adulte de la famille, un certificat de résidence et la demande de choix du médecin référant (medico di famiglia).</p>
<p>La liste des médecins généralistes de votre quartier est disponible sur le <a href="http://www.ministerosalute.it/" class="spip_out" rel="external">site du ministère de la santé</a> Servizio sanitario nazionale / indirizi utili.</p>
<p>Les soins médicaux et dentaires urgents sont gratuits si vous consultez d’abord un généraliste conventionné par la USL (Unità Sanitaria Locale, la sécurité sociale). C’est lui qui vous orientera, si nécessaire, vers un spécialiste.</p>
<p>Toute personne qui se présente aux services des urgences d’un hôpital public italien est prise en charge gratuitement. Les délais d’attente dans le secteur public conduisent beaucoup d’Italiens et d’étrangers à recourir au service privé.</p>
<p>Le service privé se caractérise par des honoraires souvent élevés : le coût de la médecine privée est plus élevé qu’en France (consultation chez un généraliste : de 50 à 80€ et jusqu’à 150€ chez un spécialiste). Les soins dans le service public italien sont d’assez bon niveau, mais en raison de sa surcharge, les délais sont parfois très longs (plusieurs semaines ou mois selon le type d’examen à effectuer).</p>
<p>Les médicaments ou examens prescrits ne sont pas pris en charge, sauf par une assurance privée.</p>
<p>A Naples, les soins sont satisfaisants pour ce qui concerne la médecine de famille. Les maladies nécessitant une hospitalisation posent cependant problème, les structures d’accueil étant souvent déficientes et manquant de personnel soignant.</p>
<p>Pour ce qui concerne Turin et Gênes, en milieu hospitalier, les interventions chirurgicales sont effectuées par des chirurgiens de qualité, mais les soins post-opératoires sont médiocres (ex. nécessité de recourir à une infirmière privée pour la surveillance d’un malade de nuit).</p>
<p>Si vous êtes inscrits à l’ASL et partez en voyage à l’étranger, il convient de demander à votre bureau A.S.L. de vous délivrer la Carte européenne d’Assurance maladie <i>(tessera europea di assicurazione malattia) </i>qui atteste de vos droits et permet la prise en charge de vos soins en Europe.</p>
<p>Les pharmacies sont ouvertes du lundi au vendredi de 8h30 à 13h00 et de 16h00 à 19h30 et le samedi matin de 8h30 à 13h00. La liste des pharmacies de garde est affichée à l’extérieur de toutes les pharmacies et est publiée dans les quotidiens.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.ministerosalute.it/" class="spip_out" rel="external">Site du ministère de la santé italien</a></p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/vie-pratique/article/sante). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
