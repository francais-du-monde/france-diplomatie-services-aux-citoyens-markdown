# Scolarisation

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/vie-pratique/article/scolarisation#sommaire_1">Scolarisation dans le système français</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/vie-pratique/article/scolarisation#sommaire_2">Enseignement supérieur</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Scolarisation dans le système français</h3>
<p><strong>Etablissements scolaires français en Belgique :</strong></p>
<p><a href="http://www.lyceefrancais-jmonnet.be/" class="spip_out" rel="external">Lycée français de Belgique Jean Monnet</a><br class="manualbr">9 avenue du Lycée français, 1180 Bruxelles<br class="manualbr">Téléphone : 02 374 58 78<br class="manualbr">Télécopie : 02 374 98 43</p>
<p><a href="http://www.lfanvers.org/" class="spip_out" rel="external">Lycée international d’Anvers</a><br class="manualbr">Lamorinierestraat, 168a, 2018 ANVERS<br class="manualbr">Téléphone : 03 239 18 89<br class="manualbr">Télécopie : 03 281 07 13</p>
<p>Pour toute information sur la scolarisation dans le système français à l’étranger, vous pouvez consulter nos thématiques sur les <a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures.md" class="spip_in">études</a> et la <a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais.md" class="spip_in">scolarisation</a>. Recherchez un établissement scolaire français sur le <a href="http://www.aefe.fr/reseau-scolaire-mondial/rechercher-un-etablissement" class="spip_out" rel="external">site de l’Agence pour l’enseignement français à l’étranger (AEFE)</a>.</p>
<p>Vous y trouverez des renseignements sur :</p>
<ul class="spip">
<li>les établissements français du primaire et du secondaire à l’étranger ;</li>
<li>les bourses scolaires et la prise en charge des frais de scolarité à l’étranger ;</li>
<li>les possibilités qui s’offrent à vous si votre enfant ne peut être scolarisé à l’étranger dans le système français (enseignement à distance par le CNED, programme français langue maternelle (FLAM), internats en France) ;</li>
<li>les épreuves du baccalauréat à l’étranger ;</li>
<li>les bourses d’études supérieures en France et à l’étranger ;</li>
<li>l’équivalence des diplômes.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Enseignement supérieur</h3>
<p>La Belgique compte de nombreuses universités et établissements d’enseignement supérieur de renom. Il est possible de suivre sur place des études supérieures dans les facultés et les écoles d’ingénieurs, d’intégrer le Collège d’Europe de Bruges ou encore de poursuivre des cycles d’enseignement supérieur courts.</p>
<p>Pour plus d’informations sur l’enseignement en Belgique, vous pouvez consulter les sites suivants :</p>
<ul class="spip">
<li><a href="http://www.enseignement.be/" class="spip_out" rel="external">Portail de l’enseignement en Fédération Wallonie-Bruxelles</a></li>
<li><a href="http://www.cediep.be/" class="spip_out" rel="external">Centre de documentation et d’information sur les études et les professions</a></li>
<li><a href="http://www.unterrichtsverwaltung.be/" class="spip_out" rel="external">Etudes en communauté germanophone de Belgique - site en allemand</a></li>
<li><a href="http://www.ond.vlaanderen.be/" class="spip_out" rel="external">Ministère de l’enseignement supérieur et de la recherche scientifique de la communauté flamande de Belgique</a> - site en flamand.</li>
<li><a href="http://www.studyinflanders.be/" class="spip_out" rel="external">Etudes supérieures en Belgique flamande</a></li>
<li><a href="http://www.equivalences.cfwb.be/" class="spip_out" rel="external">Equivalences de diplômes dans la communauté française</a></li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/vie-pratique/article/scolarisation). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
