# Animaux domestiques

<p>La législation à l’ile Maurice concernant l’importation des animaux est très stricte et nécessite l’autorisation du ministère de l’Agriculture.</p>
<h4 class="spip">Chiens et chats</h4>
<p>La procédure à suivre est la suivante :</p>
<ul class="spip">
<li>Prendre connaissance de la législation en vigueur concernant l’exportation des animaux en France.</li>
<li>Effectuer la demande de permis d’importation au service vétérinaire du ministère de l’Agriculture.</li>
<li>Fournir les documents nécessaires pour l’obtention du Permis d’Importation et du certificat sanitaire (copie du carnet de santé et vaccinations).</li>
<li>Confirmer son arrivée au minimum deux jours avant le départ auprès du service de quarantaine pour la prise en charge de son animal.</li></ul>
<p>L’animal est placé en quarantaine pour une période d’un mois et les frais d’hébergement et de nourriture sont à prendre en charge.</p>
<p><strong>Important </strong> : l’importation de certains chiens de race est interdite sur le territoire mauricien et ce pour une durée indéterminée. La liste des <strong>chiens dangereux</strong> établie par le gouvernement est actuellement la suivante : Fila Brasileiro, Tosa-Inu Japonais, Pitbull Terrier, Pitbull Américain et Dogue Argentin, American Staffordshire Terrier, Blue Rose Pitbull, Red Nose Pitbull, Boerboel.</p>
<p>Contact des services vétérinaires <i>(Veterinary ServicesDivision)</i> sur place :<br class="autobr">Dr. D.Meenowa</p>
<p>Tel : (230) 454 1016, 454 1017, 466 6662<br class="manualbr">Fax : (230) 464-2210<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/entree-et-sejour/article/animaux-domestiques#moa-dvs#mc#mail.gov.mu#" title="moa-dvs..åt..mail.gov.mu" onclick="location.href=mc_lancerlien('moa-dvs','mail.gov.mu'); return false;" class="spip_mail">Courriel</a></p>
<p>Pour connaître les conditions d’exportation des animaux domestiques, vous pouvez consulter les ressources suivantes :</p>
<ul class="spip">
<li>Notre article <a href="services-aux-citoyens-preparer-son-expatriation-douanes-article-animaux-domestiques.md" class="spip_in">Animaux domestiques</a></li>
<li><a href="http://ambafrance-mu.org/" class="spip_out" rel="external">Ambassade en France à Maurice</a></li>
<li><a href="http://www.iatatravelcentre.com/" class="spip_out" rel="external">Association internationale du transport aérien (AITA)</a></li>
<li><a href="http://www.vet-alfort.fr/" class="spip_out" rel="external">Ecole nationale vétérinaire de Maisons-Alfort</a></li>
<li><a href="http://agriculture.gouv.fr/" class="spip_out" rel="external">Ministère de l’Agriculture</a></li>
<li>Convention sur le commerce international des espèces de faune et de flore sauvages menacées d’extinction : <a href="http://cites.org/" class="spip_out" rel="external">Cites.org</a></li></ul>
<p>Certains pays exigent que les documents validés par la DDSV soient ensuite légalisés ou munis de l’apostille. Il convient donc de se renseigner sur ce point auprès de l’<a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-etrangers-en-france/article/annuaire-des-representations-etrangeres-en-france" class="spip_in">ambassade du pays de destination</a>.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/entree-et-sejour/article/animaux-domestiques). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
