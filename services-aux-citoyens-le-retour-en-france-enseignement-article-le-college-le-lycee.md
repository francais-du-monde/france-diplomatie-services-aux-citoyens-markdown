# Le collège, le lycée

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/enseignement/article/le-college-le-lycee#sommaire_1">Le collège</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/enseignement/article/le-college-le-lycee#sommaire_2">Le lycée</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/enseignement/article/le-college-le-lycee#sommaire_3">L’inscription</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Le collège</h3>
<p>Le collège comprend les classes suivantes : 6ème (choix d’une première langue vivante), 5ème (latin facultatif), 4ème (choix d’une seconde langue vivante), 3ème (grec facultatif, Brevet des collèges).</p>
<p>Pour en savoir plus, vous pouvez consulter le site du <a href="http://www.education.gouv.fr/cid214/le-college-enseignements-organisation-et-fonctionnement.html" class="spip_out" rel="external">ministère de l’Education nationale</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Le lycée</h3>
<p>Les classes du lycée sont au nombre de 3 : Seconde (orientation dans un lycée général, technologique ou professionnel), Première (pour le lycée d’enseignement général, orientation vers une section scientifique, littéraire, sciences économiques), Terminale (Baccalauréat).</p>
<p>Pour en savoir plus, vous pouvez consulter le site du <a href="http://www.education.gouv.fr/cid215/le-lycee-enseignements-organisation-et-fonctionnement.html" class="spip_out" rel="external">ministère de l’Education nationale</a>.</p>
<h3 class="spip"><a id="sommaire_3"></a>L’inscription</h3>
<p>Pour l’inscription dans l’enseignement privé, vous prendrez directement contact avec l’établissement que vous avez choisi.</p>
<p>Dans l’enseignement public, vous devez, en principe, inscrire votre enfant dans le collège du secteur géographique de votre domicile. Du fait de votre retour en France, vous êtes en situation de changement de domicile. Vous vous adresserez au service de la scolarité du <strong>rectorat de votre future Académie </strong>qui vous communiquera le dossier d’inscription.</p>
<p>On vous demandera de choisir :</p>
<ul class="spip">
<li>le régime de votre enfant (internat, demi-pension, externat) ;</li>
<li>la première langue étrangère ;</li>
<li>si vous désirez lui faire suivre un enseignement facultatif de langue et culture régionales.</li></ul>
<p><strong>N’oubliez pas de confirmer son inscription auprès de l’établissement</strong>, dès que vous connaîtrez le collège où votre enfant est admis.</p>
<p>Une dérogation peut être accordée par l’inspecteur d’académie quand le collège du secteur de rattachement n’offre pas certains enseignements (langues étrangères, section internationale).</p>
<p>L’inscription au lycée dépend des décisions d’orientation et d’affectation prises en fin de 3ème par une commission, ainsi que des possibilités d’accueil des lycées du district scolaire. Après avoir pris connaissance de la décision d’affectation, vous devez prendre contact avec le lycée pour la constitution du dossier d’inscription.</p>
<p><strong>Votre enfant a suivi sa scolarité à l’étranger dans un établissement français reconnu par le ministère français de l’Education nationale</strong></p>
<p>Les décisions d’orientation prises par cet établissement sont valables de plein droit en France.</p>
<p>Avant votre retour en France, vous devez demander un certificat de radiation à l’ancienne école. Les démarches à effectuer pour l’inscription de votre enfant sont les mêmes que celles prévues pour un changement de domicile. Vous devez vous adresser :</p>
<ul class="spip">
<li>à la mairie de votre nouveau domicile pour une inscription dans une école primaire ;</li>
<li>au rectorat de votre Académie pour une inscription dans un collège ou un lycée. Reportez-vous aux rubriques ci-dessus (« L’école maternelle, l’école primaire » et « Le collège, le lycée »).</li></ul>
<p><strong>Votre enfant a suivi l’enseignement par correspondance du CNED pendant son séjour à l’étranger</strong></p>
<p>Les décisions d’orientation prises par le Centre national d’enseignement à distance (CNED) sont valables de plein droit en France.</p>
<p>Les démarches à effectuer pour l’inscription de votre enfant sont les mêmes que celles prévues pour un changement de domicile. Reportez-vous aux rubriques ci-dessus (« L’école maternelle, l’école primaire » et « Le collège, le lycée »).</p>
<p><strong>Votre enfant a suivi sa scolarité à l’étranger dans un établissement non reconnu par le ministère français de l’Education nationale</strong></p>
<p>Pour l’inscription dans l’enseignement public secondaire (collège, lycée), un examen d’admission est généralement demandé. Renseignez-vous sur la date de cet examen auprès de l’Inspection académique qui vous orientera vers le service chargé de l’accueil des enfants revenant de l’étranger (CASNAV - Centre académique pour la scolarisation des enfants nouvellement arrivés et des enfants du voyage). Le Centre d’Information et d’Orientation (CIO) de votre ville fait passer ces tests. Pour savoir si des tests sont exigés pour l’inscription en primaire, il convient de contacter la mairie de votre domicile.</p>
<p><i>Mise à jour : avril 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/enseignement/article/le-college-le-lycee). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
