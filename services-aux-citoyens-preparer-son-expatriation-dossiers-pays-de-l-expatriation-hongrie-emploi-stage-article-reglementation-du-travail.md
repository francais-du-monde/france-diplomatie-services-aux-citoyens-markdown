# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/emploi-stage/article/reglementation-du-travail#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/emploi-stage/article/reglementation-du-travail#sommaire_2">Contrat de travail – spécificités</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/emploi-stage/article/reglementation-du-travail#sommaire_3">Fêtes légales</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/emploi-stage/article/reglementation-du-travail#sommaire_4">Emploi du conjoint</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/emploi-stage/article/reglementation-du-travail#sommaire_5">Création d’entreprises – spécificités</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<p><strong>Temps de travail</strong></p>
<p>D’après le code du travail (<i>Munka Törvénykönyve</i>), la durée légale du travail est de <strong>8 heures par jour et de 40 heures par semaine</strong>. Une durée inférieure peut être établie sur la base d’une règlementation du travail ou sur la base d’un accord entre les parties. Une durée supérieure peut également être fixée, dans le cas d’un travail exigeant une permanence ou dans le cas où l’employé est un proche collaborateur de l’employeur ou du dirigeant, à la condition que cette durée ne dépasse pas 12 heures de travail par jour et tout au plus 60 heures de travail hebdomadaire.</p>
<p>Afin de prévenir des problèmes de santé ou des dangers, la durée maximale du temps de travail d’une activité donnée est établie par texte de loi ou par convention collective.</p>
<p>La durée du temps de travail de l’employé ne peut pas dépasser 12 heures pour la journée de travail, 48 heures pour la semaine de travail et dans le cas d’un travail exigeant une permanence 24 heures pour la journée et 72 heures pour la semaine. Il convient de considérer comme temps de travail la durée complète de la permanence si la durée du travail lui-même n’est pas mesurable. Les différents cadres et périodes de l’organisation du temps de travail sont définis par convention collective ou, à défaut, par l’employeur. Le travail peut avoir lieu par roulement, en cycle continu ou par relais. Le travail par roulement permet de faire travailler les salariés différents jours ouvrables de façon à ce que le travail ne soit jamais interrompu au cours de la semaine. Le travail en cycle continu est similaire, à la différence que le travail se poursuit également le week-end. Dans le cas de travail par relais, les salariés exercent une activité identique à tour de rôle pendant une journée et le temps de fonctionnement journalier dépasse leur durée de travail.</p>
<p>L’employeur doit communiquer à l’employé l’organisation du temps de travail lors de la conclusion du contrat de travail, à la date où le contrat prend effet et lors du changement des périodes d’organisation du travail.</p>
<p>La durée du travail définie en jour, semaine ou année doit être répartie en journées de travail. Dans le cas de répartition égale du temps de travail, la durée quotidienne du travail est toujours la même. Cependant, la loi autorise la répartition inégale du temps de travail. Dans ce cas, la loi définit, en vue de protéger le salarié, une durée minimale et maximale du temps de travail quotidien. Ainsi, la durée quotidienne du travail ne peut être inférieure à quatre heures. À l’exception des emplois exigeant une permanence, la durée du temps de travail quotidien ne peut pas dépasser 12 heures car la répartition inégale du temps de travail ne peut imposer des charges disproportionnées à l’employé. Enfin, pour les travaux dangereux ou nuisibles à la santé faisant l’objet d’une répartition inégale du temps de travail, la durée maximale quotidienne du travail est de six heures.</p>
<p>Est considéré comme travail de nuit le travail effectué entre vingt-deux heures et six heures, alors que le régime de nuit est le travail effectué durant la nuit dans le cadre d’une organisation du temps de travail par roulement.</p>
<p>Durant le temps de repos, le salarié ne travaille pas et l’employeur n’est pas soumis à l’obligation de fournir du travail au salarié. On peut distinguer différents types de temps de repos :</p>
<ul class="spip">
<li>la pause</li>
<li>le repos quotidien,</li>
<li>le jour de repos hebdomadaire,</li>
<li>le jour de pause.</li></ul>
<p><strong>La pause</strong> constitue le temps de repos le plus court. Il s’agit de permettre au salarié de se reposer et de se restaurer au cours de la journée de travail. Lorsque le temps de travail quotidien atteint six heures, une pause minimale de 20 minutes doit être accordée au salarié. Si le travail n’est pas de nature à pouvoir être interrompu, par exemple en raison d’une organisation du temps de travail en cycle continu ou par relais, ou parce que le travail est composé d’heures d’astreinte, la pause doit être prise au cours du temps de travail. Cela signifie que la pause de 20 minutes est alors incluse dans le temps de travail. Une pause doit également être accordée pour chaque période ininterrompue de trois heures supplémentaires travaillées. L’employeur a pour obligation d’accorder ces pauses même si le salarié n’en fait pas la demande.</p>
<p>Entre deux journées de travail, le salarié doit bénéficier d’un <strong>repos quotidien</strong> d’une durée minimale de 11 heures. Des dispositions différentes peuvent être prévues par convention collective ou par accord conclu entre les parties, mais le repos quotidien ne doit pas être inférieur à huit heures. Lorsque le salarié réside loin de son lieu de travail, cette durée ne comprend pas le temps du trajet.</p>
<p><strong>Le jour de repos hebdomadaire</strong> a pour fonction de permettre au salarié de se reposer entre deux semaines de travail. Le salarié bénéficie de deux journées de repos hebdomadaire, dont l’une doit être donnée le dimanche.</p>
<p>Le code du travail prévoit des réductions du temps de travail. Par exemple, les délégués syndicaux, les membres du comité d’entreprise, le travailleur qui suit des études de niveau élémentaire et la mère qui allaite ont droit à des congés payés supplémentaires ; en outre, un père reçoit cinq jours de congé lors de la naissance de son enfant. Une taxe d’absence doit être payée au titre de cette réduction du temps de travail.</p>
<p><strong>Congés</strong></p>
<p>Pour chaque année de travail, les salariés ont droit à des congés payés, lesquels ont pour fonction de leur permettre de se reposer. Ont également droit aux congés payés les salariés à temps partiels et les retraités exerçant une activité salariée. Les congés payés annuels sont composés de congés de base et de congés supplémentaires. Les congés de base sont fonction de l’âge du salarié. Pour chaque tranche d’âge, la durée des congés payés de base s’applique l’année au cours de laquelle le salarié atteint l’âge correspondant à la tranche d’âge. Les durées des congés payés de base sont les suivantes :</p>
<ul class="spip">
<li>20 jours jusqu’à l’âge de 25 ans</li>
<li>21 jours à partir de l’âge de 25 ans</li>
<li>un jour supplémentaire tous les trois ans jusqu’à l’âge de 31 ans</li>
<li>un jour supplémentaire tous les deux ans jusqu’à l’âge de 45 ans</li>
<li>30 jours à partir de l’âge de 45 ans.</li></ul>
<p>En plus des congés payés de base, le salarié peut bénéficier de jours de congés payés supplémentaires dans certains cas. Le code du travail prévoit des congés supplémentaires dans les quatre cas suivants : les salariés mineurs, les salariés qui ont des enfants de moins de 16 ans (deux jours de congés par enfant, pour chaque parent), les salariés aveugles et enfin les salariés qui travaillent en sous-sol de façon habituelle ou qui sont exposés à des rayons ionisants au moins trois heures par jour.</p>
<p>L’employeur doit accorder au salarié un quart des congés payés pour la période qu’il a demandée. Le salarié doit quant à lui faire part à l’employeur de sa demande de congés 15 jours avant leur début effectif. La fixation des autres congés, décidée après consultation du salarié, relève du droit et du devoir de l’employeur. Il lui est également possible d’accorder le report de congés annuels sur l’année suivante jusqu’au 31 mars et pour un maximum d’un quart des congés annuels. Les congés peuvent être divisés en plus de deux blocs à la seule demande du salarié. L’employeur est autorisé à interrompre les congés du salarié pour une raison impérieuse ou dans un intérêt économique exceptionnel où ses compétences sont directement requises. Le salarié sera alors dédommagé pour les frais occasionnés et le préjudice subi.</p>
<p>Les congés sans solde, que la loi prévoit dans trois cas :</p>
<ul class="spip">
<li>La femme salariée enceinte a droit à 24 semaines de congés maternité (<strong>sans solde</strong>). En qualité de parent, le salarié a également droit à un congé sans solde :
<br>— jusqu’à ce que l’enfant ait atteint l’âge de trois ans dans le cadre d’un congé parental ;
<br>— jusqu’à ce que l’enfant ait atteint l’âge de 14 ans si l’employé bénéficie d’allocations de garde d’enfants ;
<br>— jusqu’à ce que l’enfant ait atteint l’âge de 12 ans, en cas de maladie de l’enfant, pour les soins de celui-ci au domicile.</li>
<li>L’employé a droit au congé sans solde pour soigner l’un de ses proches lorsque ce dernier nécessite des soins de longue durée.</li>
<li>Un congé sans solde d’une durée d’un an doit être accordé au salarié qui construit lui-même son habitation. L’employé en incapacité de travailler a droit à 15 jours de congés de maladie par année calendaire.</li></ul>
<p>La femme salariée enceinte a droit à 24 semaines de congé maternité, réparties de façon à ce qu’elle puisse bénéficier de quatre semaines de congé avant la date présumée de l’accouchement.</p>
<p>Le fonctionnaire peut recevoir sur demande un congé, tout en conservant son salaire moyen, afin de rédiger sa thèse, ainsi que dans les cas où il participe à un travail de recherche à la suite d’un appel d’offres, part en voyage d’études ou rédige un manuel scolaire (de spécialité).</p>
<p>Le congé octroyé à ce titre ne peut excéder un an.</p>
<p>Les jours fériés reconnus par la loi sont chômés par les salariés. Les jours fériés officiels sont le 1er janvier, le 15 mars, le lundi de Pâques, le 1er mai, le lundi de Pentecôte, le 20 août, le 23 octobre, le 1er novembre et les 25 et 26 décembre.</p>
<h3 class="spip"><a id="sommaire_2"></a>Contrat de travail – spécificités</h3>
<p>Le contrat de travail peut être conclu pour une période déterminée (toujours inférieure à cinq ans) ou pour une durée indéterminée. Il ne peut être modifié que par consentement mutuel. Le contrat de travail peut être rompu unilatéralement moyennant un préavis qui ne peut être inférieur à 30 jours et qui varie en fonction de l’ancienneté du salarié dans l’entreprise.</p>
<p>L’indemnité de licenciement est prévue par le code du travail et augmente avec l’ancienneté. Elle est inexistante pour une présence dans l’entreprise de moins de trois ans. Elle peut atteindre six mois de salaire pour une ancienneté de 25 ans. Le licenciement d’un salarié est interdit dans certaines situations (invalidité, maternité, etc.). Le licenciement d’un salarié est également interdit durant la période de cinq ans précédant son départ à la retraite sauf motifs exceptionnels et argumentés de la part de l’employeur.</p>
<h3 class="spip"><a id="sommaire_3"></a>Fêtes légales</h3>
<ul class="spip">
<li>1er janvier</li>
<li>15 mars : fête nationale (soulèvement de 1848)</li>
<li>Pâques et lundi de Pâques</li>
<li>1er mai : fête du travail</li>
<li>dimanche et lundi de Pentecôte</li>
<li>20 août : fête nationale - Saint-Etienne</li>
<li>23 octobre : anniversaire de l’insurrection de 1956</li>
<li>25 et 26 décembre</li></ul>
<p>Pour en savoir plus :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.tv5.org/TV5Site/voyageurs/accueil.php" class="spip_out" rel="external">TV5.org</a></p>
<h3 class="spip"><a id="sommaire_4"></a>Emploi du conjoint</h3>
<p>Les possibilités d’emploi sont limitées, compte tenu de la barrière linguistique et des salaires peu élevés.</p>
<h3 class="spip"><a id="sommaire_5"></a>Création d’entreprises – spécificités</h3>
<p>Comme en France, il existe de nombreux statuts juridiques d’entreprise, s’inscrivant globalement dans une nomenclature, des conditions et modalités similaires.</p>
<p>Pour connaître les spécificités et choisir le type de structures qui correspond à un projet identifié, il convient de prendre conseil auprès d’un avocat (liste consultable sur le site de l’<a href="http://www.ambafrance-hu.org/Avocat-conseil-aupres-de-la" class="spip_out" rel="external">Ambassade de France à Budapest</a>), qui est en mesure également d’établir les documents nécessaires et d’effectuer certaines démarches.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/emploi-stage/article/reglementation-du-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
