# Stages

<p><strong>La connaissance de la langue grecque est importante dans la recherche d’un emploi en Grèce</strong>. S’il s’agit d’un stage, l’employeur acceptera une certaine imperfection dans la pratique de la langue. Pour un emploi, des notions de grec, y compris dans le domaine technique, sont nécessaires.</p>
<p><strong>La connaissance de la langue anglaise est également importante et peut parfois pallier un faible niveau en grec</strong>. Il est fortement conseillé de s’inscrire à des cours de grec dès votre arrivée, même si votre emploi ne le requiert pas absolument. En effet, il est essentiel, pour pouvoir s’intégrer, de posséder un minimum de connaissances en grec.</p>
<p><strong>Certains emplois destinés aux Français ne requièrent pas une grande connaissance du grec</strong>. Il s’agit d’emplois dans l’enseignement de la langue française (cours particuliers) ou d’emplois saisonniers dans le tourisme (accompagnateur touristique). Ces emplois sont cependant synonyme d’insécurité, voire de précarité.</p>
<p>La recherche d’un emploi peut s’avérer difficile. Des cabinets de recrutement ont constaté que seul un tiers des employeurs publiaient des annonces ou faisaient appel à des cabinets de recrutement pour la recherche de personnel. En effet, un employeur privilégiera tout d’abord son environnement proche en utilisant le bouche à oreille avant d’avoir recours à une agence de recrutement. Cette voie informelle présente des avantages. En rencontrant une personne qui peut appuyer votre candidature, vous avez l’occasion de prouver vos qualités dans un contexte qui n’est pas permis par le curriculum vitae ou un entretien formel.</p>
<p>Il est évidemment très difficile au départ de se créer un réseau de connaissances. Il est, par conséquent, vivement conseillé de s’impliquer dès l’arrivée en Grèce dans des associations afin de multiplier les chances de rencontres. La communauté franco-hellénique est très organisée à Athènes. Elle permet, par le biais de diverses organisations, de rencontrer des gens, de s’informer sur les tendances actuelles du marché du travail et de se créer son propre réseau personnel.</p>
<p><strong>Avez-vous pensé au volontariat international (V.I.E ou V.I.A.) ?</strong></p>
<p>Le V.I.E. permet aux entreprises françaises de confier à un jeune ressortissant de l’espace économique européen, âgé de 18 à 28 ans, une mission professionnelle (rémunérée) à l’étranger durant une période modulable de 6 à 24 mois.</p>
<p>Pour en savoir plus :</p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/emplois-stages-concours/volontaires-internationaux/" class="spip_in">Volontaires internationaux</a></li>
<li><a href="http://www.civiweb.com/FR/index.aspx" class="spip_out" rel="external">Civiweb.com</a></li>
<li><a href="http://www.businessfrance.fr/" class="spip_out" rel="external">Business France.fr</a> - <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/emploi-stage/article/stages#infoVIE#mc#ubifrance.fr#" title="infoVIE..åt..ubifrance.fr" onclick="location.href=mc_lancerlien('infoVIE','ubifrance.fr'); return false;" class="spip_mail">Courriel</a></li></ul>
<p><i>Mise à jour : novembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/emploi-stage/article/stages). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
