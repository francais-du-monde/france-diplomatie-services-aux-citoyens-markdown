# La communauté française inscrite au registre des Français établis hors de France

<p class="chapo">
      Pour mémoire, on estime le nombre de Français vivant à l’étranger, y compris ceux qui ne sont pas inscrits au registre entre 2 et 2,5 millions.
</p>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/inscription-consulaire-et-communaute-francaise/article/la-communaute-francaise-inscrite-au-registre-des-francais-etablis-hors-de#sommaire_1">1/ Une croissance régulière de la communauté française inscrite au registre</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/inscription-consulaire-et-communaute-francaise/article/la-communaute-francaise-inscrite-au-registre-des-francais-etablis-hors-de#sommaire_2">2/ Les pays où la communauté française est la plus nombreuse restent les mêmes qu’en 2014.</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/inscription-consulaire-et-communaute-francaise/article/la-communaute-francaise-inscrite-au-registre-des-francais-etablis-hors-de#sommaire_3">3/ Répartition par âge et par genre des inscrits au Registre des Français établis hors de France en 2015</a></li></ul><blockquote class="texteencadre-spip spip"><strong class="caractencadre2-spip spip"><i>Les statistiques par pays sont consultables sur le site</i> <a href="https://www.data.gouv.fr/fr/datasets/chiffres-du-registre-des-francais-etablis-hors-de-france-pour-lannee-2015/" class="spip_out" rel="external">data.gouv.fr</a>.</strong></blockquote>
<h3 class="spip"><a id="sommaire_1"></a>1/ Une croissance régulière de la communauté française inscrite au registre</h3>
<p><strong>Le nombre d’inscrits au registre au 31 décembre 2015 s’élève à 1 710 945</strong>, soit une progression de 1,8% par rapport à 2014. Le taux de croissance était de 2,4% entre 2013 et 2014.<br class="autobr">Pour mémoire, on estime le nombre de Français vivant à l’étranger, y compris ceux qui ne sont pas inscrits au registre entre 2 et 2,5 millions.</p>
<p>La part des binationaux dans la population française inscrite au registre reste stable autour de 42%.</p>
<p>63,3% des inscrits au registre le sont depuis plus de 5 ans et 9,6%, depuis moins d’un an.</p>
<h3 class="spip"><a id="sommaire_2"></a>2/ Les pays où la communauté française est la plus nombreuse restent les mêmes qu’en 2014.</h3>
<p><strong>Les 5 premiers  pays d’accueil de la communauté inscrite au registre restent les mêmes qu’en 2014 :</strong> Suisse, Etats-Unis, Royaume-Uni, Belgique, Allemagne. Ils concentrent près de 40% de la communauté française inscrite au registre. <br class="autobr">Près de 50% de nos compatriotes résident en Europe et 13,7% en Amérique du nord.</p>
<p class="spip_document_91992 spip_documents spip_documents_center">
<img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L858xH819/inscrits_au_registre_160310_cle056187-a7626.png" width="858" height="819" alt=""></p>
<h3 class="spip"><a id="sommaire_3"></a>3/ Répartition par âge et par genre des inscrits au Registre des Français établis hors de France en 2015</h3>
<p><strong>La répartition des Français inscrits par genre est stable sur les dernières années.</strong> En 2015, avec 50,2% de femmes parmi les inscrits au registre, la population française établie à l’étranger a sensiblement le même profil que la population française métropolitaine qui en compte 51,5%.</p>
<p><strong>Cependant il existe des différences selon les zones géographiques.</strong> Ainsi, les femmes sont toujours moins présentes en Asie-Océanie (42,54%) et plus présentes dans les Etats membres de l’Union européenne (53,51%).</p>
<p>La structure par âge de la communauté française établie à l’étranger est stable par rapport à l’année précédente. Plus de 74% des Français inscrits au registre ont plus de 18 ans.</p>
<p><strong>Inscrits de moins de 18 ans :</strong> 25,08<br class="autobr"><strong>Inscrits entre 18 et 25 ans :</strong>   9,3<br class="autobr"><strong>Inscrits entre 26 et 60 ans :</strong>   50,4<br class="autobr"><strong>Inscrits de + de 60 ans : </strong>      15,13</p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/inscription-consulaire-et-communaute-francaise/article/la-communaute-francaise-inscrite-au-registre-des-francais-etablis-hors-de). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
