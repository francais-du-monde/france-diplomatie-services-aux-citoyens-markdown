# Communications

<h4 class="spip">Téléphone – Internet</h4>
<p><strong>Les liaisons téléphoniques </strong>ont un coût relativement moins élevé qu’en France. Des téléphones portables peuvent être loués le temps d’un séjour pour environ 3 NZD (1,77€) la journée. Il est aussi possible d’acheter un téléphone bas de gamme pour une quinzaine d’euros.</p>
<p>Le réseau téléphonique néo-zélandais fonctionne très bien dans les zones urbaines. Il arrive cependant de ne pas avoir de réseau dans les endroits plus reculés et au relief plus escarpé.</p>
<p>Les principaux opérateurs sont 2degrees, NZ Telecom et Vodafone.</p>
<p>Depuis la France, pour appeler sur une ligne fixe, il faut composer le <strong>00 64</strong>, suivi de l’indicatif de la région :</p>
<ul class="spip">
<li><strong>4</strong> pour Wellington,</li>
<li><strong>9</strong> pour Auckland,</li>
<li><strong>3</strong> pour Christchurch</li></ul>
<p>et des chiffres du numéro de téléphone. Pour les téléphones portables, il faut composer le 0064, ôter le 0 initial du numéro (les numéros de portable commencent souvent par 021 ou 027) et taper le reste du numéro de téléphone.</p>
<p><strong>Le réseau Internet </strong>est constitué par 44 fournisseurs différents. Les huit fournisseurs principaux sont Telecom NZ, Vodafone NZ, Orcon, Slingshot,Snap, World Net, Woosh et MegaTel.</p>
<p>Les abonnements « illimités » ne sont pas toujours disponibles et la majorité des Néo-Zélandais ont un abonnement limité à internet : les fournisseurs comptabilisent le nombre de gigabytes utilisés et imposent généralement une limite, qui varie selon l’abonnement (entre 10 et 100 par mois, le plus souvent).</p>
<p>Cependant, les derniers gouvernements ont accordé une attention particulière au développement du réseau internet en Nouvelle-Zélande.</p>
<p>De nombreux cybercafés sont répartis sur le territoire. Compter entre 4 NZD et 8 NZD de l’heure.</p>
<p><strong>Téléphoner gratuitement par Internet</strong></p>
<p>Il est généralement très avantageux d’utiliser <strong>des logiciels de téléphonie</strong> (Skype, Google talk, WhatsApp Messenger, etc.) car les tarifs proposés à l’international sont souvent moindre que ceux des opérateurs locaux exploitant des lignes classiques, voire gratuits dans certaines conditions.</p>
<h4 class="spip">Poste</h4>
<p>Les liaisons postales sont fiables. Les colis en provenance de l’étranger sont régulièrement ouverts et inspectés par les services sanitaires.</p>
<p>Une réforme des services postaux réduira la livraison du courrier à trois jours par semaine à partir de 2014-2015.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/vie-pratique/article/communications). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
