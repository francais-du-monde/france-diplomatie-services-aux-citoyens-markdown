# Entretien d’embauche

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/emploi-stage/article/entretien-d-embauche-112456#sommaire_1">Conduite de l’entretien</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/emploi-stage/article/entretien-d-embauche-112456#sommaire_2">Apparence et attitude</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/emploi-stage/article/entretien-d-embauche-112456#sommaire_3">Négociation du salaire</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/emploi-stage/article/entretien-d-embauche-112456#sommaire_4">Questions préférées des recruteurs</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/emploi-stage/article/entretien-d-embauche-112456#sommaire_5">Erreurs à éviter</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/emploi-stage/article/entretien-d-embauche-112456#sommaire_6">Après l’entretien</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Conduite de l’entretien</h3>
<p>Après un bref entretien initial, des tests psychologiques constituent généralement la première étape de la procédure de sélection chez la plupart des employeurs, qui sera suivie de toute une série d’entretiens.</p>
<p>L’employeur peut être amené à s’interroger sur les motivations qui poussent le candidat à chercher un emploi en Espagne. Dans la mesure où il est très facile pour un employé de quitter rapidement son entreprise (les délais de préavis sont très courts, et les formalités liées à la rupture d’un CDD peu contraignantes) le recruteur va vouloir s’assurer de sa volonté de s’installer à long terme en Espagne.</p>
<p>A la fin de l’entretien, il est bien vu de poser des questions sur le poste ou l’organisation afin de montrer son intérêt.</p>
<p>Le candidat doit également s’attendre à devoir subir toute une série de tests, les tests psychotechniques, psychologiques et de compétences techniques étant très courants en Espagne.</p>
<h3 class="spip"><a id="sommaire_2"></a>Apparence et attitude</h3>
<p>Les entretiens peuvent se dérouler de manière un peu moins formaliste qu’en France. Ainsi, le candidat ne doit pas être surpris si le recruteur le tutoie, lui devra néanmoins utiliser le vouvoiement.</p>
<h3 class="spip"><a id="sommaire_3"></a>Négociation du salaire</h3>
<p>Faire des recherches afin de savoir si le salaire offert est compétitif sur le marché pour des positions similaires.</p>
<p>Il faut prendre en compte certains bénéfices comme assurance vie, mutuelle santé, vacances, épargne retraite.</p>
<h3 class="spip"><a id="sommaire_4"></a>Questions préférées des recruteurs</h3>
<ul class="spip">
<li>Hábleme de Usted ?</li>
<li>Por qué quiere trabajar con nosotros ?</li>
<li>Qué espera de este puesto de trabajo ?</li>
<li>Qué puede aportarnos profesionalmente ?</li>
<li>Cómo ve su futuro profesional ?</li>
<li>Cuáles son sus puntos fuertes y sus puntos débiles ?</li>
<li>A qué dedica su tiempo libre ?</li>
<li>Tiene alguna pregunta ?</li></ul>
<h3 class="spip"><a id="sommaire_5"></a>Erreurs à éviter</h3>
<ul class="spip">
<li><strong>Arriver en retard</strong>. Le manque de ponctualité donne une mauvaise image. Il est préférable d’arriver 5 à 10 minutes en avance.</li>
<li><strong>Etre mal habillé</strong>. Il n’est pas toujours nécessaire d’être en costume-cravate, cela dépend de chaque entreprise, mais il faut montrer une image professionnelle et fiable.</li>
<li><strong>Ne pas oublier d’éteindre son téléphone portable</strong></li>
<li><strong>Centrer l’entretien sur le salaire.</strong> Le premier entretien n’est pas le moment pour négocier la rémunération.</li></ul>
<h3 class="spip"><a id="sommaire_6"></a>Après l’entretien</h3>
<p>Prendre des notes sur l’entretien peut éviter d’oublier des points importants.</p>
<p>Notre les noms et postes des personnes rencontrées pendant l’entretien.</p>
<p>Il est possible d’appeler ou d’envoyer un courrier au recruteur afin de savoir où en est le processus, dans un laps de temps d’une quinzaine de jours après l’entretien.</p>
<p><i>Mise à jour : avril 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/emploi-stage/article/entretien-d-embauche-112456). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
