# Les équivalences de diplômes

<p>En France, il n’existe pas d’équivalence juridique entre les diplômes obtenus à l’étranger et les diplômes français.</p>
<p>Le Centre ENIC-NARIC France (European Network of Information Centres-National Academic Recognition Information Centres) est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes. Il établit des attestations de comparabilité pour un/des diplôme(s) obtenu(s) à l’étranger, informe sur les procédures à suivre pour exercer une profession réglementée et renseigne sur la procédure de reconnaissance des diplômes français à l’étranger.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p><a href="http://www.ciep.fr/enic-naricfr/" class="spip_out" rel="external">ENIC-NARIC France - CIEP</a><br class="manualbr">1 avenue Léon Journault<br class="manualbr">92318 Sèvres Cedex<br class="manualbr">Tél. : 01 70 19 30 31 - Fax : 01 45 07 63 02 <br class="manualbr">Courriel : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/emploi/article/les-equivalences-de-diplomes#enic-naric#mc#ciep.fr#" title="enic-naric..åt..ciep.fr" onclick="location.href=mc_lancerlien('enic-naric','ciep.fr'); return false;" class="spip_mail">enic-naric<span class="spancrypt"> [at] </span>ciep.fr</a></p>
<p>Centre international d’études pédagogiques (CIEP)<br class="autobr">1 avenue Léon Journault <br class="manualbr">92318 Sèvres Cedex <br class="manualbr">33 (0)1 45 07 60 00</p>
<p><i>Mise à jour : octobre 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/emploi/article/les-equivalences-de-diplomes). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
