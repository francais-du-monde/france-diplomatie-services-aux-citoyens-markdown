# Convention fiscale

<p>La France et l’Irlande ont signé, le 21 mars 1968, une convention en matière de fiscalité publiée au Journal Officiel du 10 septembre 1971.</p>
<p>Les dispositions conventionnelles qui ont primauté sur les dispositions du droit interne, selon l’article 55 de la Constitution française, répartissent entre les deux Etats, le droit d’imposer les revenus perçus par leurs résidents respectifs.</p>
<p><a href="http://www.diplomatie.gouv.fr/fr/" class="spip_url spip_out"></a>Cette convention tend à éviter les doubles impositions en matière d’impôts sur les revenus qui pourraient résulter de l’application des législations de ces deux Etats.</p>
<h4 class="spip">Champ d’application de la convention</h4>
<p>Cet accord a pour objet de protéger <strong>les résidents de chacun des Etats contractants</strong> en matière d’impôts prélevés directement sur le revenu ou sur le bénéfice des sociétés.</p>
<p>La convention trouve donc à s’appliquer aux résidents de ces Etats.</p>
<p>Notion de résidence : l’article 2 paragraphe 7 de la convention s’applique aux personnes qui sont considérées comme "résidents d’un Etat contractant" ou de chacun de ces deux Etats.</p>
<p>D’après ce même article de la convention, est considérée comme <strong>"résident d’Irlande"</strong> toute personne qui est résidente d’Irlande pour l’application de l’impôt irlandais et qui n’est pas résidente de France pour l’application de l’impôt français ; et comme <strong>"résident de France"</strong> toute personne qui est résidente de France pour l’application de l’impôt français et qui n’est pas résidente d’Irlande pour l’application de l’impôt irlandais.</p>
<p>Par conséquent, une personne est considérée comme résident d’un Etat contractant lorsque, en vertu de la législation dudit Etat, elle se trouve assujettie à l’impôt à raison de son domicile, de sa résidence ou de critères analogues.</p>
<p>Ce paragraphe ne fournissant pas les critères subsidiaires permettant de résoudre le cas de double résidence si l’assujettissement à l’impôt ne pouvait suffire, il faut se référer au droit interne de chacun des deux pays.</p>
<p>Ces critères sont :</p>
<p><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> pour l’Irlande :</p>
<ul class="spip">
<li>l’Etat dans lequel la personne séjourne de façon habituelle (il s’agit de la notion de 183 jours de présence physique sur le territoire au cours d’une année fiscale) ;</li>
<li>la mise à disposition d’un lieu de séjour réservé à l’usage d’une personne et qui, de plus, réside en Irlande pendant une période quelconque au cours de l’année d’imposition ;</li>
<li>enfin, lorsque la personne se rend chaque année en Irlande pour une période de trois mois ou plus par an.</li></ul>
<p><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> pour la France :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  cela répond à la définition conventionnelle de "résident de France" la personne qui entre dans le champ d’application de l’article 4 B du Code général des impôts. </p>
<p>Pour qu’un contribuable soit domicilié en France, il suffit que l’un des critères suivants soit rempli :</p>
<ul class="spip">
<li>avoir en France son foyer ou le lieu de son séjour principal ;</li>
<li>exercer en France une activité professionnelle, salariée ou non, à moins que la personne justifie que cette activité y est exercée à titre accessoire ;</li>
<li>avoir en France le centre de ses intérêts économiques.</li></ul>
<h4 class="spip">Elimination de la double imposition </h4>
<p>La double imposition est évitée selon un dispositif prévu par l’article 21 de la convention.</p>
<p><strong>En ce qui concerne la France</strong>, les revenus autres que les dividendes sont exonérés des impôts français lorsqu’ils sont, en vertu de la convention, imposables en Irlande. Dans les cas non réglés par les dispositions de la convention, les revenus d’une personne résidant habituellement en France, sont exonérés des impôts français s’ils ont leur source en Irlande et sont imposables d’après la législation de cet Etat.</p>
<p><strong>En ce qui concerne l’Irlande</strong>, sous certaines réserves, l’impôt français perçu sur des revenus ayant leur source en France, est considéré comme un crédit déductible de tout impôt irlandais exigible sur ces revenus.</p>
<p>Pour en savoir plus :</p>
<ul class="spip">
<li><a href="http://www.impots.gouv.fr/portal/deploiement/p1/fichedescriptive_1806/fichedescriptive_1806.pdf" class="spip_out" rel="external">Texte intégral de la convention fiscale entre la France et l’Irlande</a></li>
<li>Notre rubrique thématique <a href="services-aux-citoyens-preparer-son-expatriation-fiscalite.md" class="spip_in">Fiscalité</a></li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/fiscalite/article/convention-fiscale). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
