# Entretien d’embauche

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/emploi-stage/article/entretien-d-embauche-108664#sommaire_1">Conduite de l’entretien</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/emploi-stage/article/entretien-d-embauche-108664#sommaire_2">Apparence et attitude</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/emploi-stage/article/entretien-d-embauche-108664#sommaire_3">Négociation du salaire</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/emploi-stage/article/entretien-d-embauche-108664#sommaire_4">Questions préférées des recruteurs</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/emploi-stage/article/entretien-d-embauche-108664#sommaire_5">Erreurs à éviter</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/emploi-stage/article/entretien-d-embauche-108664#sommaire_6">Après l’entretien</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Conduite de l’entretien</h3>
<p>Le processus d’entretien d’embauche nécessite une très bonne préparation et de la patience afin de se soumettre aux différentes étapes du recrutement.</p>
<p>Avant l’entretien d’embauche (<i>job interview</i>), il faut s’attendre à être d’abord convoqué pour un test d’admission (<i>entrance test</i>). Ce dernier est déterminant pour la suite qui sera réservée à votre candidature. En Australie, les entretiens comprennent généralement un panel de deux ou trois intervenants. L’entretien commence par un échange informel. Le temps de réponse, à l’issue d’un entretien, peut être long.</p>
<p>Quel que ce soit le type d’entretien, voici quelques conseils :</p>
<ul class="spip">
<li>La préparation de l’entretien ne doit pas être bâclée. Une des conditions pour une bonne préparation repose sur la connaissance de l’entreprise et de l’emploi auquel vous postulez. Il faut rechercher des informations concrètes pour être en mesure de parler de l’entreprise et de l’emploi et de donner votre avis si on vous le demande ;</li>
<li>S’il s’agit de votre premier entretien d’embauche, il est préférable de vous entraîner ;</li>
<li>Soyez ponctuel, le mieux étant d’arriver au moins cinq minutes en avance, temps pendant lequel vous pouvez vous relaxer ;</li>
<li>La coutume veut que l’on présente une carte de visite lors du premier entretien. Les documents tels que les lettres de référence, les certificats ou diplômes sont remis ultérieurement ;</li>
<li>Prévoir une ou deux copies de votre curriculum vitae (y compris une pour vous), ainsi que de tous les documents relatifs aux diplômes et autres certificats qui figurent dans votre curriculum vitae ;</li>
<li>Lorsque vous parlez, ne perdez jamais de vue l’objectif et présentez de manière démonstrative vos connaissances sur le secteur industriel et/ou l’entreprise en question. Ne faites jamais part de votre opinion sauf si l’on vous demande vos impressions. Enfin ne cherchez pas à trop en dire. Soyez sincère et mettez-y du cœur.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Apparence et attitude</h3>
<p>Bien qu’un entretien d’embauche suive au niveau international globalement les mêmes règles et exigences, il est important de donner la meilleure image de vous-même le plus naturellement possible et en respectant certains codes de conduite culturels propres à l’Australie :</p>
<ul class="spip">
<li>Même si l’entreprise vous semble décontractée, vous ne devez pas négliger votre tenue vestimentaire. Il est conseillé de s’habiller avec soin et de façon plutôt traditionnelle. C’est une marque de votre sérieux. Pendant l’été, les vestes sont souvent ôtées. Les femmes portent une jupe et un chemisier ou une robe classique accompagnée d’accessoires discrets. Le but est aussi d’être à l’aise dans les habits que vous portez ;</li>
<li>Serrez la main de votre ou de vos interlocuteurs avant l’entretien et lorsque vous prenez congé d’eux ; ne vous asseyez que lorsqu’on vous y invite ;</li>
<li>Ne coupez jamais la parole à votre interlocuteur ;</li>
<li>Lorsque vous parlez, pensez à regarder votre interlocuteur dans les yeux et au moment des présentations adressez-vous à la ou les personnes présentes en utilisant le titre : Monsieur, Madame ou Mademoiselle. Les titres liés à la fonction professionnelle ou au niveau universitaire sont habituellement minimisés ;</li>
<li>Evitez de regarder votre montre ;</li>
<li>Bien plus que l’emploi en tant que tel, votre personnalité, votre confiance, votre enthousiasme, votre attitude positive et votre capacité à bien communiquer sont déterminants dans le processus de sélection ;</li>
<li>Votre capacité linguistique est également essentielle. Mesurez bien les conséquences d’un anglais oral insuffisant.</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Négociation du salaire</h3>
<p>Il est important que vous connaissiez le salaire moyen de l’emploi que vous recherchez. En cas de négociation, il vous sera plus facile d’argumenter. Toutefois, il vous appartient d’être en accord avec vos prétentions salariales. Le fait d’accepter une rémunération inférieure à celle du marché n’est pas une bonne stratégie. L’expérience laisse à penser que vous vous démotiverez assez rapidement.</p>
<p>A la question « précisez-moi vos attentes en matière de salaire ? », vous pouvez répondre :</p>
<p>« D’après une publication des salaires par xxxx (citer la source) et la tendance du marché, le montant annuel varie de …. à …… . Par conséquent, et compte tenu de ma valeur professionnelle, le salaire souhaité se situe au dessus de la moitié supérieure de la fourchette nationale. »</p>
<h3 class="spip"><a id="sommaire_4"></a>Questions préférées des recruteurs</h3>
<p>Soyez prêt à répondre à de nombreuses questions concrètes permettant de vous évaluer lors de mises en situation. Vos réponses doivent être franches, complètes et précises (<i>yes</i> ou <i>no</i>). D’une manière générale, préparez-vous à être interrogé et à formuler des arguments qui vous valoriseront. Si vous êtes amené à relater un vécu professionnel compliqué, faites-le sans chercher à vous défendre, ni à critiquer vos anciens collègues. Il est préférable de dire comment vous l’avez résolu. Cela indique votre capacité d’analyse et de management.</p>
<p>Voici quelques questions clé courantes :</p>
<ul class="spip">
<li><strong>Points faibles personnels (<i>personal weakness</i>)</strong><br class="manualbr">Quels sont vos points faibles par rapport à l’emploi proposé ? Ce à quoi vous pouvez répondre : " je pense que mes compétences et mes atouts correspondent au poste ". Vous pouvez également demander à votre interlocuteur s’il y a quelque chose qu’il aimerait aborder avec vous sur ce sujet ou de façon plus " frontale " quelles sont, à son avis, vos points faibles ?</li>
<li><strong>Ethique professionnelle (<i>hard work ethics</i>)</strong><br class="manualbr">Est-ce que votre dernier ou actuel employeur vous décrirait comme quelqu’un qui atteint son but et poursuit ses objectifs ?</li>
<li><strong>Se démarquer (<i>standing out</i>)</strong><br class="manualbr">Très souvent, il est demandé de démontrer ses différences par rapport aux autres employés : quelles idées ou compétences nouvelles pensez-vous apporter à l’emploi que les autres employés n’offrent pas ?</li>
<li><strong>Etre déterminé (<i>being specific</i>)</strong><br class="manualbr">Pourquoi avez-vous choisi cette carrière ou branche professionnelle ?</li>
<li><strong>Votre passé (<i>your past</i>)</strong><br class="manualbr">L’employeur pourra, par exemple, vous demander de justifier une interruption ou un changement dans vos études.</li>
<li><strong>Votre contribution à l’entreprise (<i>contribute to the company)</i> </strong><br class="manualbr">Présentez les contributions particulières dont vous avez fait bénéficier votre dernier emploi et indiquez comment vous pourriez servir l’entreprise en question ?</li>
<li><strong>Projection (<i>in five years…)</i> </strong><br class="manualbr">Où aimeriez-vous être dans cinq ans ?</li>
<li><strong>Derniers employeurs (<i>previous bosses</i>)</strong><br class="manualbr">Parlez-moi de ou des relations professionnelles avec vos différents employeurs ? Un conseil : ne mentionnez que ceux avec lesquels vous êtes certain d’avoir entretenu de bonnes relations et qui pourraient être des référents fiables.</li>
<li><strong>Mises en situation concrètes</strong></li>
<li><strong>Démonstration d’un problème solutionné</strong><br class="manualbr">Décrivez-moi une situation difficile que vous avez résolue ? Comment avez-vous identifié le problème et êtes-vous parvenu à tout mettre en œuvre pour le résoudre ?</li>
<li><strong>Démonstration en tant que manager</strong><br class="manualbr">Décrivez une situation où vous avez tenté de persuader une personne de faire quelque chose qu’elle ne souhaitait pas faire.</li>
<li><strong>Démonstration de prise d’initiative</strong><br class="manualbr">Décrivez une situation qui montre votre aptitude à prendre des initiatives et à mener à bien cette tâche supplémentaire.</li></ul>
<h3 class="spip"><a id="sommaire_5"></a>Erreurs à éviter</h3>
<p>Il est inutile d’accepter un entretien d’embauche si l’emploi ne vous attire pas vraiment. Votre désintérêt se verra très vite. A l’inverse, ne soyez pas trop sûr de vous, ni arrogant.</p>
<h3 class="spip"><a id="sommaire_6"></a>Après l’entretien</h3>
<p>N’oubliez pas d’envoyer, les jours suivants l’entretien, une lettre de remerciement (<i>follow-up thank you letter</i>) qui témoignera définitivement de votre intérêt pour l’emploi.</p>
<p><i>Mise à jour : octobre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/emploi-stage/article/entretien-d-embauche-108664). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
