# Loisirs et culture

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/vie-pratique/article/loisirs-et-culture-109952#sommaire_1">Tourisme</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/vie-pratique/article/loisirs-et-culture-109952#sommaire_2">Activités culturelles</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/vie-pratique/article/loisirs-et-culture-109952#sommaire_3">Sports</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/vie-pratique/article/loisirs-et-culture-109952#sommaire_4">Télévision – Radio</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/vie-pratique/article/loisirs-et-culture-109952#sommaire_5">Presse française</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Tourisme</h3>
<p>L’Italie est l’un des pays les plus riches du monde en sites touristiques, naturels et artistiques. Toutes les époques sont représentées : antiquité, période romane, renaissance, baroque, etc.</p>
<p>Pour tout renseignement d’ordre touristique s’adresser à :</p>
<p>Office du tourisme italien<br class="manualbr">23, rue de la Paix - 75002 PARIS <br class="manualbr">Tél. : 01.42.66.66.68.</p>
<p><a href="http://www.enit.it/" class="spip_out" rel="external">L’Agence nationale italienne pour le tourisme</a> vous informera notamment sur les régions, les moyens de transport, les manifestations et événements dans le pays et l’hébergement sur place.</p>
<p>Le site francophone <a href="http://www.italie1.com/" class="spip_out" rel="external">Italie1.com</a> aborde entre autre l’histoire, l’art, le tourisme, les musées de toutes les régions italiennes et donne des informations utiles.</p>
<h3 class="spip"><a id="sommaire_2"></a>Activités culturelles</h3>
<h4 class="spip">Activités culturelles en français</h4>
<p>Le cinéma français est surtout représenté dans le cadre des institutions culturelles françaises, lors des rétrospectives et des festivals (Mostra de Venise). Les grands films français qui sont distribués dans les circuits commerciaux sont programmés en version italienne.</p>
<p>De nombreuses manifestations artistiques françaises sont présentées dans les grandes villes (Rome, Venise, Milan, Turin, Gênes, Naples) et dans le cadre des festivals (Venise, Bologne, Spoleto, Taormina, etc.).</p>
<p>Il y a régulièrement des expositions d’artistes français dans les grands centres artistiques : académie de France à Rome, musée Rivoli à Turin, Palazzo Real à Milan, à Venise…</p>
<p>En outre les services et centres culturels de Rome, Florence, Milan, Naples, Turin, Gênes et Palerme, et les nombreuses Alliances françaises réparties dans les différentes capitales régionales du pays, proposent des programmes très riches d’activités culturelles : cinéma, théâtre, musique, danse, conférences, expositions, colloques, etc.</p>
<h5 class="spip">Alliance française</h5>
<p>Le <a href="http://www.alliancefr.it/" class="spip_out" rel="external">réseau des Alliances françaises</a> est particulièrement bien implanté en Italie de nombreuses succursales sont à la disposition des francophiles et/ou des francophones. Pour plus de renseignements, consulter la <a href="http://annuaire.fondation-alliancefr.org/?action=recherche_avancee&amp;is_enseignante=off&amp;nom=&amp;pays_id=122&amp;statut_id=off&amp;zonegeo_id=0" class="spip_out" rel="external">liste des structures existantes</a>.</p>
<h5 class="spip">L’Institut français d’Italie</h5>
<p>L’Institut français d’Italie (IFI) est né le 1er janvier 2012. Il regroupe le service de coopération et d’action culturelle de l’Ambassade de France en Italie ainsi que les centres et instituts culturels français de Florence, Milan, Naples et Palerme.</p>
<p><a href="http://institutfrancais-italia.com/" class="spip_out" rel="external">Institut francais d’Italie</a><br class="manualbr">Ambassade de France en Italie<br class="manualbr">Piazza Farnese, 67 – 00186 Roma</p>
<p><strong>Les Missions de l’IFI</strong></p>
<p>L’Institut français d’Italie se consacre aux relations franco-italiennes dans tous les domaines : culturel, éducatif, scolaire et universitaire au sens large. Il agit dans le cadre de l’accord culturel bilatéral signé par la France et l’Italie le 4 novembre 1949 et régulièrement mis à jour.</p>
<p>Ses missions s’articulent autour des cinq secteurs suivants :</p>
<ul class="spip">
<li>la coopération linguistique et éducative ;</li>
<li>l’action culturelle, à travers la création artistique et la collaboration pour le patrimoine et les musées ;</li>
<li>la diffusion du cinéma et de l’audiovisuel ;</li>
<li>le livre et le débat d’idées ;</li>
<li>la coopération universitaire.</li></ul>
<p><strong>Les réalisations de l’IFI</strong></p>
<p>L’Institut français d’Italie se distingue par la richesse des grands projets culturels qu’il mène sur le territoire italien.</p>
<p>Chaque année depuis 2008, le festival Suona Francese promeut la création musicale française en Italie et le développement de la collaboration franco-italienne dans le domaine pédagogique et musical avec plus de 140 manifestations dans 40 villes différentes.</p>
<p>Le festival <strong>Rendez-vous</strong> s’impose en 2011 comme un événement majeur de l’action de l’Ambassade de France en Italie. Il propose de faire découvrir au public italien le cinéma français contemporain avec plus de trente projections réparties sur cinq jours de festival à Rome au mois d’avril, repris ensuite dans différentes villes italiennes.</p>
<p><strong>France Danse</strong> est le rendez vous avec la création chorégraphique et a lieu tous les deux ans de juillet à novembre pour plus de 100 jours de rendez-vous avec la danse.</p>
<p>Avec le projet <strong>Face à Face</strong>, la scène théâtrale française en Italie a été à l’honneur dès 2006 à chaque printemps.</p>
<p>Le Festival de la Fiction française accueille et fait connaître au public italien les romanciers français contemporains. Il s’appuie sur les sorties italiennes les plus récentes de façon à présenter au public des auteurs immédiatement disponible en traduction.</p>
<p>Enfin, à l’automne 2012, l’Institut français d’Italie inaugure sous le nom d’<strong>Inter(ri)viste</strong> une nouvelle programmation centrée sur le débat intellectuel. Pour chaque conférence, une revue française et une revue italienne collaborent en circonscrivant le thème proposé par l’IFI.</p>
<p><strong>L’Italie, un pays au patrimoine artistique et culturel exceptionnel</strong></p>
<p>L’Italie compte de très nombreux musées, renfermant des œuvres d’une rare beauté. Comme le dit Antonio Paolucci, directeur des Musées du Vatican, « sous le ciel de l’Italie, le Musée est partout ». Parmi les musées les plus visités on peut citer la Galerie des Offices de Florence, le Palais des Doges de Venise, la Galerie Borghèse de Rome.</p>
<p>En ce qui concerne le patrimoine culturel italien, toutes les régions italiennes possèdent des villes d’art et des trésors architecturaux. La cité de Venise ne ressemble à aucune autre et attire chaque année 20 millions de visiteurs venus du monde entier ; les vestiges romains de Rome ou de Pompéi rendent compte de la grandeur de l’Empire romain à l’Antiquité.</p>
<p>La culture en Italie est également très présente avec la musique et les opéras, dont l’origine revient à la ville de Naples. La Scala de Milan, Les Arènes de Vérone, le Théâtre de l’opéra de Rome et l’Opéra San Carlo de Naples bénéficient d’une réputation internationale et constituent l’une des richesses de ce pays.</p>
<h4 class="spip">Activités culturelles locales</h4>
<h5 class="spip">Les grands événements culturels italiens</h5>
<p>La Biennale d’Art de Venise, se déroulera du 1er juin au 24 novembre 2013 et la Mostra du Cinéma de Venise à la fin du mois d’août 2013 ; la Triennale du design, de l’architecture et des arts décoratifs de Milan, le Salon du livre de Turin et le Festival international du film de Rome sont également très suivis par le grand public.</p>
<h5 class="spip">Cinéma et théâtre </h5>
<p>Les salles de cinéma sont nombreuses et confortables dans l’ensemble. Elles programment une majorité de films américains, doublés en italien (peu de films en V.O.), ainsi que la production italienne.</p>
<p>Dans toutes les grandes villes, la vie culturelle est extrêmement riche. A Rome, Naples, Milan, Venise (avec la Biennale du théâtre), Turin, le théâtre offre des programmations variées, avec des spectacles classiques et modernes, italiens et étrangers.</p>
<p>Les concerts et les représentations d’opéra sont très nombreux à se produire dans des lieux historiques : Scala de Milan, Teatro San Carlo de Naples, et nombreuses églises.</p>
<h3 class="spip"><a id="sommaire_3"></a>Sports</h3>
<p>On peut pratiquer les mêmes sports qu’en France et plus particulièrement les sports de mer (voile, pêche sous-marine) et de montagne (nombreuses stations de ski). Il existe beaucoup d’installations sportives où l’on peut exercer un sport à des conditions raisonnables (football, tennis), ainsi que de nombreux clubs privés, plus onéreux. Le football peut être considéré comme le "sport national".</p>
<p>De nombreuses courses hippiques ont lieu dans tout le pays, notamment à Rome (Cappanelle pour le galop, Tor di Valle pour le trot), à Milan (San Siro pour le trot), à Turin et à Naples.</p>
<p>La chasse et la pêche sont autorisées si l’on possède un permis, délivré par les autorités régionales, après vérification des aptitudes des candidats. Les dates d’ouverture et de fermeture sont fixées par les régions.</p>
<h3 class="spip"><a id="sommaire_4"></a>Télévision – Radio</h3>
<p>La télévision italienne propose de nombreuses chaînes ; trois chaînes publiques (RAI-1, RAI-2, RAI-3) et de nombreuses chaînes privées, nationales et locales. La qualité des programmes est variable. Il est possible de capter des chaînes étrangères avec une antenne parabolique. Le système adopté est PAL.</p>
<p>Une grande quantité de radios libres est relayée par des radios relevant du service public.</p>
<p>Plusieurs chaînes et radios publiques françaises sont présentes en Italie. C’est le cas de :</p>
<ul class="spip">
<li>TV5 : la chaîne francophone, qui peut être captée en réception directe analogique à partir d’Eutelsat, est proposée également par la plate-forme satellitaire « SkyItalia »</li>
<li>France 24, la chaîne française d’information internationale, émet sur le numérique terrestre italien. Elle est aussi disponible sur le satellite (Sky-italia canal 538) et en clair sur Hot Bird et Astra 1.</li>
<li>La chaîne européenne d’information en continu Euronews, qui diffuse en sept langues dont l’italien, figure dans le bouquet Sky et est reprise partiellement par la Rai Uno (le matin).</li>
<li>Arte France et la chaîne italienne RaiSat (diffusée sur l’offre de base du bouquet satellite SKY ) ont conclu un accord prévoyant la diffusion (trois heures par semaine) depuis janvier 2005 des programmes d’Arte, des documentaires essentiellement, par Raisat Premium.</li></ul>
<p>La diffusion de RFI en ondes courtes a été supprimée en direction de l’Europe occidentale depuis 1997. Les programmes de RFI sont accessibles par satellite :</p>
<ul class="spip">
<li>en direct et analogique sur Astra IC 19° Est : RFI 1 en français et RFI 2 ;</li>
<li>dans le bouquet numérique Canal Satellite (Astra) : RFI 1 stéréo et RFI Musique stéréo ;</li>
<li>à destination des relais de diffusion sur Eutelsat II Fu : RFI 1, RFI 2 et RFI Musique.</li></ul>
<p>En outre, neuf programmes de Radio France (France Musique, Hector, FIP, Radio Bleue, Elisa, France Inter, France Info, France Culture et le Mouv’) sont également présents sur Astra 19,2° Est dans le bouquet de Canal Satellite en clair, en numérique, norme de qualité du disque laser. Ils sont accessibles en Europe en réception directe individuelle ou collective.</p>
<p>Sans installation spécifique, vous pourrez recevoir les chaînes italiennes Raiuno, Raidue, Raitre, Canale 5, Italia 1, Rete 4, la 7. Le câble et l’ADSL proposent sur abonnement des bouquets numériques internationaux (comprenant France 2). Renseignements auprès des compagnies <a href="http://www.fastweb.it/" class="spip_out" rel="external">Fastweb</a> et <a href="http://www.sky.it/" class="spip_out" rel="external">Sky</a>. A savoir que l’installation d’antennes et paraboles nécessite l’autorisation de la co-propriété.</p>
<p><strong>Redevance audiovisuelle (Canone)</strong></p>
<p>Si vous avez acheté votre téléviseur en Italie, ou s’il a figuré sur votre inventaire de déménagement, la taxe annuelle vous sera envoyée automatiquement. Elle s’élève à 113,5€/an en 2013.</p>
<h3 class="spip"><a id="sommaire_5"></a>Presse française</h3>
<p>La plupart des publications françaises se trouvent facilement dans les kiosques des grandes villes. Les quotidiens arrivent le jour même.</p>
<p>La librairie "La Procure" assure la diffusion d’ouvrages français à Rome. Dans les autres villes, on trouve des ouvrages en français dans les librairies suivantes : à Naples, la "Librairie Française", et la Librairie "Feltrinelli" ; à Turin, "la Librairie Française" ; à Milan, la "Librairie française" ; à Venise la "Librairie Franco-italienne" et la "Librairie San Giovanni e Poalo".</p>
<p>Vous pouvez également consulter le site <a href="http://www.libreriafrancese.it/" class="spip_out" rel="external">Libreriafrancese.it</a></p>
<p>Le Petit Journal, journal des Français et francophones à l’étranger, (éditions à Milan et Rome) proposent des rubriques pratiques et d’information sur ces deux villes.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/vie-pratique/article/loisirs-et-culture-109952). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
