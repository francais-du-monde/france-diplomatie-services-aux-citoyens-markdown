# Curriculum vitae

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/emploi-stage/article/curriculum-vitae#sommaire_1">Rédaction</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/emploi-stage/article/curriculum-vitae#sommaire_2">Diplômes</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/emploi-stage/article/curriculum-vitae#sommaire_3">Modèles de CV</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Rédaction</h3>
<p>En Australie, le curriculum vitae le plus usité est celui qui est organisé selon un ordre rétro chronologique (<i>chronological résumé</i>). L’expérience professionnelle et les études poursuivies sont présentées en commençant par les évènements les plus récents.</p>
<p>Le curriculum vitae fonctionnel (<i>functional résumé</i>) est également répandu. Il est cependant déconseillé si vous avez fréquemment changé d’emploi ou avez connu d’importantes périodes sans activité au cours de vos parcours professionnel et universitaire.</p>
<p>Le curriculum vitae compte généralement deux à trois pages. Celui-ci doit être rédigé en anglais et dactylographié. La qualité rédactionnelle est tout aussi importante que le contenu, étant donné que votre maîtrise de l’anglais sera déterminante pour l’obtention d’un emploi permanent en Australie.</p>
<p>Le curriculum vitae chronologique comporte huit sections essentielles :</p>
<p><strong>Section 1 - L’essentiel ou coordonnées personnelles (<i>the basics </i>ou <i>personal contacts</i>)</strong></p>
<ul class="spip">
<li>Nom</li>
<li>Adresse</li>
<li>Ville</li>
<li>Etat</li>
<li>Pays</li>
<li>Téléphone fixe et/ou mobile</li>
<li>Courriel</li></ul>
<p>Les indications relatives à l’état civil, à l’âge et à la nationalité, ainsi que la photographie du postulant ne doivent pas figurer dans le CV.</p>
<p><strong>Section 2 - Objectif (<i>objective</i>)</strong></p>
<p>L’information qu’un employeur lira en priorité est celle qui concerne votre objectif. C’est elle qui l’amènera ou non à poursuivre la lecture de votre curriculum vitae. Vous devez expliquer de façon claire et concise les raisons qui font de vous le meilleur candidat pour l’emploi en question.</p>
<p><strong>Section 3 - Expérience professionnelle (<i>experience </i>ou <i>work experience </i>or <i>work history</i>)</strong></p>
<p>Il suffit de lister les emplois occupés les plus significatifs, sans oublier les dates, et en détaillant vos responsabilités.</p>
<p><strong>Section 4 – Etudes / diplômes (<i>education </i>ou <i>qualifications</i>)</strong></p>
<p>En conservant l’ordre rétro chronologique, vous devez indiquer les périodes d’études en indiquant précisément :</p>
<ul class="spip">
<li>les dates,</li>
<li>le nom des établissements fréquentés,</li>
<li>la ville et le pays</li>
<li>les diplômes obtenus et leur équivalence en Australie.</li></ul>
<p>Vous devez avoir également une idée claire de ces équivalences et être en mesure d’en parler lors de l’entretien d’embauche.</p>
<p><strong>Section 5 - Formation (<i>related coursework </i>ou <i>training</i>)</strong></p>
<p>Ce point concerne les cours et formations spécifiques suivis en dehors ou parallèlement à vos études, sans oublier d’indiquer les diplômes ou certificats obtenus. Les périodes de stage, de bénévolat et de cours de langue figureront dans cette section.</p>
<p>Cette section peut également s’intituler réalisations spécifiques (<i>special achievements</i>). Vous y indiquerez les actions individuelles ou collectives menées, ainsi que votre investissement dans un ou plusieurs projets associatifs.</p>
<p><strong>Section 6 - Synthèse de mot-clé (<i>keyword summary</i>)</strong></p>
<p>Ce point vous permet de spécifier vos principaux atouts pour l’emploi auquel vous postulez, en veillant à indiquer, parmi ces atouts, ceux qui sont susceptibles de faire la différence par rapport à un autre candidat.</p>
<p><strong>Section 7- Centres d’intérêt (<i>interest</i>)</strong></p>
<p>Ce point n’est pas obligatoire. Mais noter quelques-uns de vos centres d’intérêt peut permettre à votre employeur de se faire une meilleure idée de vous.</p>
<p><strong>Section 8 - Références (<i>references </i>ou <i>referees</i>)</strong></p>
<p>Les références sont communiquées à l’employeur afin qu’il se fasse une meilleure idée de vous dans le monde du travail. Vous devez indiquer le nom des personnes dont vous êtes certain qu’elles feront l’éloge de votre travail. Si vous ne souhaitez pas indiquer de noms, il vous est possible d’ajouter la formule suivante : références fournies à la demande (<i>references supplied upon request</i>).</p>
<h3 class="spip"><a id="sommaire_2"></a>Diplômes</h3>
<p>Un accord de reconnaissance mutuelle des diplômes a été signé par le président de <i>Universities Australia</i> (l’association des 38 universités australiennes), le président de la Conférence des présidents des universités (CPU) et le président de l’association des écoles d’ingénieurs français (CDEFi) en octobre 2009. Cet accord encourage la reconnaissance mutuelle des périodes d’études et des diplômes de l’enseignement supérieur afin de faciliter la poursuite des études dans le pays partenaire. Il est recommandé de regarder le site du ministère australien, l‘<a href="https://internationaleducation.gov.au/" class="spip_out" rel="external">Australian Education International</a> (AEI NOOSR) pour vérifier que l’équivalence existe bien.</p>
<p>Pour en savoir plus, vous pouvez consulter le site de l’<a href="http://www.ambafrance-au.org/Reconnaissance-des-diplomes" class="spip_out" rel="external">ambassade de France en Australie</a>.</p>
<p>Les universités australiennes se conforment généralement au tableau d’équivalence ci-dessous. Elles sont toutefois libres de l’appliquer ou non en fonction de l’appréciation de chaque dossier.</p>
<table class="spip">
<thead><tr class="row_first"><th id="idb591_c0">Diplôme français</th><th id="idb591_c1">Diplôme australien jugé comme équivalent</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="idb591_c0">Baccalauréat</td>
<td headers="idb591_c1">High School Diploma</td></tr>
<tr class="row_even even">
<td headers="idb591_c0">Bac + 1</td>
<td headers="idb591_c1">Diploma</td></tr>
<tr class="row_odd odd">
<td headers="idb591_c0">Bac + 2</td>
<td headers="idb591_c1">Advanced Diploma ou Associate Degree</td></tr>
<tr class="row_even even">
<td headers="idb591_c0">DEUG / DUT (2 années d’études supérieures)</td>
<td headers="idb591_c1">Associate diploma ou Associate Degree</td></tr>
<tr class="row_odd odd">
<td headers="idb591_c0">Licence</td>
<td headers="idb591_c1">Bachelor’s degree</td></tr>
<tr class="row_even even">
<td headers="idb591_c0">Maîtrise</td>
<td headers="idb591_c1">Bachelor’s degree</td></tr>
<tr class="row_odd odd">
<td headers="idb591_c0">Magistère</td>
<td headers="idb591_c1">Bachelor’s degree</td></tr>
<tr class="row_even even">
<td headers="idb591_c0">Master Pro</td>
<td headers="idb591_c1">Coursework Master’s Degree</td></tr>
<tr class="row_odd odd">
<td headers="idb591_c0">Master Recherche</td>
<td headers="idb591_c1">Research Master’s Degree</td></tr>
<tr class="row_even even">
<td headers="idb591_c0">DESS</td>
<td headers="idb591_c1">Australian Postgraduate Diploma</td></tr>
<tr class="row_odd odd">
<td headers="idb591_c0">DEA</td>
<td headers="idb591_c1">Australian Postgraduate Diploma</td></tr>
<tr class="row_even even">
<td headers="idb591_c0">Diplôme d’ingénieur</td>
<td headers="idb591_c1">Bachelor’s degree</td></tr>
<tr class="row_odd odd">
<td headers="idb591_c0">Mastère spécialisé</td>
<td headers="idb591_c1">Australian Master’s Degree</td></tr>
<tr class="row_even even">
<td headers="idb591_c0">Diplôme d’Etat de Docteur (dentiste / pharmacien / vétérinaire)</td>
<td headers="idb591_c1">Bachelor’s degree</td></tr>
<tr class="row_odd odd">
<td headers="idb591_c0">Doctorat d’université</td>
<td headers="idb591_c1">Australian Master’s Degree</td></tr>
<tr class="row_even even">
<td headers="idb591_c0">Docteur-ingénieur</td>
<td headers="idb591_c1">Professional Doctorate</td></tr>
<tr class="row_odd odd">
<td headers="idb591_c0">Doctorat de 3ème cycle / Doctorat</td>
<td headers="idb591_c1">Australian PhD ou Doctorate</td></tr>
<tr class="row_even even">
<td headers="idb591_c0">Doctorat d’Etat</td>
<td headers="idb591_c1">Australian PhD ou Doctorate</td></tr>
</tbody>
</table>
<p>S’il n’existe pas d’équivalent exact dans le système éducatif australien, il est parfois plus prudent de mentionner sur son curriculum vitae son diplôme en français suivi d’une explication succincte en anglais. Lors de l’entretien d’embauche, vous devrez de même être capable de présenter oralement vos diplômes. De plus, les Australiens ne connaissent pas nécessairement les différents types d’établissements ou d’écoles en France.</p>
<p><strong>Quelques exemples</strong></p>
<p><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> Maîtrise : <i>fourth year of university study in</i>…<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> CAPES/Agrégation : <i>fifth year of university study in preparation for teaching in French high schools ;</i> <br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> Classes préparatoires : <i>undergraduate courses to prepare for entry exams to top universities </i> ;<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> Ecole supérieure des sciences économiques et commerciales : <i>top French business school</i>.</p>
<p>Des organismes gouvernementaux sont chargés de l’examen des diplômes étrangers pour les personnes souhaitant travailler dans un autre pays. En Australie, le NOOSR est une sous-direction, au sein du ministère de l’Education, chargée d’évaluer les diplômes étrangers.</p>
<p>Leurs coordonnées sont les suivantes :</p>
<p><a href="http://aei.gov.au/" class="spip_out" rel="external">Australian Education International</a>-National Office of Overseas Skills Recognition (AEI-NOOSR) <br class="manualbr">GPO Box 1407 <br class="manualbr">Canberra ACT 2601<br class="manualbr">Téléphone : 1800 020086 <br class="manualbr">Télécopie : (02) 62407636<br class="manualbr">Courriel : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/emploi-stage/article/curriculum-vitae#aei-noosr#mc#dest.gov.au#" title="aei-noosr..åt..dest.gov.au" onclick="location.href=mc_lancerlien('aei-noosr','dest.gov.au'); return false;" class="spip_mail">aei-noosr<span class="spancrypt"> [at] </span>dest.gov.au</a></p>
<h3 class="spip"><a id="sommaire_3"></a>Modèles de CV</h3>
<p>Exemples de CV australiens sur <a href="http://www.easy-cv.com/banque-de-cv/australia/" class="spip_out" rel="external">easy-CV</a>.</p>
<p><i>Mise à jour : octobre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/emploi-stage/article/curriculum-vitae). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
