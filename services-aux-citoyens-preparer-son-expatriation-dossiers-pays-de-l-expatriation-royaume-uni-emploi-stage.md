# Emploi, stage

<h2 class="rub22763">Marché du travail</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/emploi-stage/#sommaire_1">Secteurs à fort potentiel</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/emploi-stage/#sommaire_2">Secteurs à faible potentiel</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/emploi-stage/#sommaire_3">Rémunération</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Secteurs à fort potentiel</h3>
<ul class="spip">
<li>Hôtellerie et restauration ;</li>
<li>Enseignement ;</li>
<li>Recherche scientifique (chimie, pétrochimie) ;</li>
<li>Commerce ;</li>
<li>Informatique et nouvelles technologies de l’information ;</li>
<li>Finances.</li></ul>
<h4 class="spip">Exemples de professions recherchées</h4>
<ul class="spip">
<li><strong>Cuisinier </strong> : traditionnellement, les cuisiniers français sont appréciés.</li>
<li><strong>Hôtellerie </strong> : secteur porteur (notamment avec la construction de structures hôtelières dans le cadre des JO de 2012).</li>
<li><strong>Professeur de français </strong> : l’enseignement est un secteur qui connaît une pénurie de main d’œuvre au Royaume-Uni, aussi bien dans les instituts privés que dans les écoles publiques. En général, le professeur de français langue étrangère (FLE) a un statut de freelance. La difficulté consiste donc à assurer un nombre d’heures suffisant pour disposer d’un revenu convenable et régulier.</li>
<li><strong>Secrétaire bilingue - assistant(e) de direction </strong> : les emplois administratifs peuvent s’avérer très intéressants pour les Français possédant une expérience appropriée, à condition de bien maîtriser la langue anglaise, tant au niveau de l’écrit que de l’oral.</li>
<li><strong>Comptable </strong> : la comptabilité anglo-saxonne, bien que présentant des différences avec la comptabilité française, peut offrir des opportunités à un comptable qualifié français (possédant un DPECF), à condition de maîtriser correctement la langue anglaise. Les candidats avec une formation comptable peuvent se renseigner auprès des organismes suivants : <a href="http://www.accaglobal.com/" class="spip_out" rel="external">ACCA</a> et <a href="http://www.cimaglobal.com/" class="spip_out" rel="external">CIMA</a>.</li>
<li><strong>Assistant(e) de recherche </strong> : des postes d’assistant de recherche (niveau DESS) ou de chargé de recherche (Doctorat) sont publiés régulièrement dans la presse universitaire.</li>
<li><strong>Informaticien </strong> : l’informatique de haut niveau est un secteur favorable aux Français qui bénéficient de la très bonne réputation des formations françaises ; des opportunités peuvent se présenter, à condition toutefois de pratiquer l’anglais couramment.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Secteurs à faible potentiel</h3>
<ul class="spip">
<li><strong>Professions juridiques </strong> : il n’existe pas d’équivalence entre le statut d’avocat en France et celui de <i>Lawyer </i>au Royaume-Uni, les deux systèmes juridiques présentant de grandes différences. Pour exercer en tant que <i>Lawyer</i>, il est nécessaire d’être reconnu par la <i>Law Society</i>, ce qui suppose de posséder les diplômes britanniques.</li>
<li><strong>Autres secteurs </strong> : marketing, publicité, communication.</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Rémunération</h3>
<h4 class="spip">Salaire minimum national</h4>
<p>La quasi totalité des travailleurs ont droit au Royaume-Uni au salaire minimum national (<i>National Minimum Wage</i>), quels que soit la profession, le type de contrat de travail (temps partiel, contrat à durée déterminée ou indéterminée, etc.) ou la taille et le type de l’entreprise. Ne peuvent prétendre au salaire minimum légal, les personnes suivantes : les travailleurs à leur compte, les membres du gouvernement, les directeurs (sauf s’ils disposent d’un contrat de travail), les enfants encore en âge de scolarisation (âgés de moins 16 ans), les militaires, les employé(e)s au pair, les bénévoles, les détenus, certains apprentis, etc. Le salaire minimum des travailleurs agricoles est fixé par le Conseil des salaires de l’agriculture (<i>Agricultural Wages Board</i>) et diffère du salaire minimum national. Le salaire minimum national est fixé chaque année au mois d’octobre par le gouvernement sur les recommandations de la commission indépendante pour les bas salaires (<i>Low Pay Commission</i> ou LCP). Le salaire minimum national est fonction de l’âge du travailleur.</p>
<p>Salaire minimum horaire à compter du 01/10/2013</p>
<table class="spip">
<thead><tr class="row_first"><th id="id507f_c0">Âge</th><th id="id507f_c1">Salaire minimum horaire</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="id507f_c0">21 ans et plus</td>
<td headers="id507f_c1">6,31 £</td></tr>
<tr class="row_even even">
<td headers="id507f_c0">18 à 20 ans</td>
<td headers="id507f_c1">5,03 £</td></tr>
<tr class="row_odd odd">
<td headers="id507f_c0">16 à 17 ans</td>
<td headers="id507f_c1">3,72 £</td></tr>
<tr class="row_even even">
<td headers="id507f_c0">apprenti</td>
<td headers="id507f_c1">2,68 £</td></tr>
</tbody>
</table>
<p><i>Source : <a href="https://www.gov.uk/national-minimum-wage-rates" class="spip_out" rel="external">https://www.gov.uk/national-minimum-wage-rates</a></i></p>
<p>Le salaire est souvent viré directement sur votre compte bancaire.</p>
<p>Les cotisations de sécurité sociale et l’impôt sur le revenu selon le système <i>Pay as You Earn</i> de retenue à la source sont directement déduits de votre salaire. Les autres déductions éventuelles, telles que les cotisations à un régime de retraite, devront faire l’objet d’un accord l’employeur et le salarié.</p>
<p>(Voir aussi : <a href="http://ec.europa.eu/eures/" class="spip_out" rel="external">EURES</a> / rubrique " Vivre et travailler ")</p>
<h4 class="spip">Quelques exemples de salaires (chiffres de 2013)</h4>
<ul class="spip">
<li><strong>Cuisinier qualifié et expérimenté </strong> : le salaire annuel moyen est de 28 000 livres.</li>
<li><strong>Vendeur(euse) en magasin </strong> : beaucoup de magasins offrent le minimum légal, en général à partir de 6,31 livres/heure avec parfois des commissions sur les ventes. Le salaire annuel moyen est de 17 000 livres. <br class="manualbr"><strong>A savoir</strong> : les magasins sont ouverts 7/7, sans que cela ait une répercussion sur le salaire !</li>
<li><strong>Professeur de français qualifié FLE (français langue étrangère) </strong> : salaire horaire à partir de 18 livres.</li>
<li><strong>Secrétaire bilingue </strong> : salaire moyen de 20 000 livres/an pour une personne qui arrive de France sans expérience en Grande-Bretagne. Cependant les salaires augmentent rapidement après deux ans d’expérience.</li>
<li><strong>Poste en comptabilité/contrôle de gestion </strong> : ces emplois peuvent être très bien rémunérés, à condition de posséder une qualification anglaise. Un assistant comptable peut prétendre à un salaire annuel de 21 000 à 27 000 livres, un comptable diplômé, à partir de 30 000 livres.</li>
<li><strong>Informaticien </strong> : les rémunérations varient selon le degré de compétence et de responsabilités. De 22 000 livres par an pour un débutant (administration et support technique) à 70 000 livres par an pour un informaticien confirmé (chef de projet senior).</li>
<li><strong>Commercial : </strong>les salaires débutent à 18 000 livres avec commissions.</li>
<li><strong>Finances : </strong>à moins d’avoir une expérience en Grande-Bretagne, peu de postes sont proposés actuellement, du fait d’un nombre important de personnes expérimentées sur le marché.</li></ul>
<p>A titre indicatif, quelques exemples de salaires mensuels bruts moyens, par catégories socio-professionnelles (employés à temps plein) :</p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Cadres dirigeants</td>
<td>4 500 £</td></tr>
<tr class="row_even even">
<td>Professions libérales, ingénieurs</td>
<td>3 500 £</td></tr>
<tr class="row_odd odd">
<td>Techniciens assistants</td>
<td>3 000 £</td></tr>
<tr class="row_even even">
<td>Artisanat</td>
<td>2 500 £</td></tr>
<tr class="row_odd odd">
<td>Machinistes</td>
<td>1 800 £</td></tr>
<tr class="row_even even">
<td>Secrétariat et employés de l’administration</td>
<td>1 600 £</td></tr>
<tr class="row_odd odd">
<td>Service aux personnes</td>
<td>1 200 £</td></tr>
<tr class="row_even even">
<td>Ouvriers</td>
<td>1 250 £</td></tr>
<tr class="row_odd odd">
<td>Commerce de détail et services clients</td>
<td>1 000 £</td></tr>
</tbody>
</table>
<p>De manière générale, une bonne maîtrise de l’anglais ainsi qu’une première expérience sur le territoire sont importantes avant d’espérer trouver un travail qualifié. A Londres, le salaire moyen d’un jeune diplômé (hors banque et droit) se situe entre 20 000 et 22 000 livres/an. Les postes en province sont moins bien rémunérés mais le coût de la vie est moins important.</p>
<p><strong>Pour plus d’informations :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="https://ec.europa.eu/eures/main.jsp?countryId=UKacro=lwlang=frparentId=0catId=0regionIdForAdvisor=regionIdForSE=regionString=UK0%7C%20:" class="spip_out" rel="external">EURES, le portail européen sur la mobilité et l’emploi</a></p>
<p><i>Mise à jour : décembre 2013</i></p>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/emploi-stage/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
