# Retraite

<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-retraite-article-calculer-sa-retraite-lorsqu-on-a.md" title="Calculer sa retraite lorsque l’on part travailler à l’étranger">Calculer sa retraite lorsque l’on part travailler à l’étranger</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-retraite-article-vivre-sa-retraite-a-l-etranger.md" title="Vivre sa retraite à l’étranger">Vivre sa retraite à l’étranger</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/retraite/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
