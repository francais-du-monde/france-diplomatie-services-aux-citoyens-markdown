# La délivrance de certificat de nationalité française

<p>Les certificats de nationalité française sont délivrés par les greffiers en chef des tribunaux d’instance, sous l’autorité du ministre de la Justice.</p>
<p>Les personnes qui résident :</p>
<ul class="spip">
<li>en France doivent solliciter leur certificat de nationalité française auprès du greffier en chef du tribunal d’instance compétent à raison de leur domicile, qu’elles soient nées en France ou à l’étranger ;</li>
<li>à l’étranger et y sont nées sont invitées à s’adresser au directeur de greffe du Service de la nationalité des Français nés et établis hors de France (30 rue du Château des Rentiers, 75013, Paris) ;</li>
<li>à l’étranger et sont nées en France (Paris excepté) doivent s’adresser au greffier en chef du tribunal d’instance compétent à raison de leur lieu de naissance ;</li>
<li>à l’étranger et sont nées à Paris sont invitées à s’adresser au Pôle de la nationalité française de Paris (28 rue du Château des Rentiers, 75013, Paris).</li></ul>
<p>Afin de faciliter la preuve de la nationalité française, la loi du 16 mars 1998, entrée en vigueur le 1er septembre 1998, prévoit la mention en marge de l’acte de naissance de l’intéressé de toute première délivrance d’un certificat de nationalité française.</p>
<p>Plus d’informations sur les documents à fournir pour obtenir un certificat de nationalité française sur <a href="http://vosdroits.service-public.fr/particuliers/F1051.xhtml" class="spip_out" rel="external">Service-public.fr</a>.</p>
<p><i>Mise à jour : mars 2016</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/nationalite-francaise/article/la-delivrance-de-certificat-de-nationalite-francaise). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
