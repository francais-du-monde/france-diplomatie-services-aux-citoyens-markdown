# Communications

<h4 class="spip">Téléphone – Internet</h4>
<p>Les liaisons téléphoniques sont bonnes depuis les postes privés ou les cabines à cartes dans les principales grandes villes</p>
<ul class="spip">
<li>Pour joindre la Thaïlande depuis la France : composer le 00 66 + les chiffres de l’abonné.</li>
<li>Pour joindre la France depuis la Thaïlande : composer le numéro à 9 chiffres de votre correspondant (sans le premier 0) précédés du 001 (préfixe de l’international) et du 33 (préfixe de la France).</li></ul>
<p>La couverture du réseau pour les téléphones portables est très bonne en Thaïlande.</p>
<p>La qualité des connexions internet est liée au réseau téléphonique. L’ADSL est présent dans la plupart des grandes villes et des lieux plus ou moins touristiques.</p>
<p><strong>Téléphoner gratuitement par Internet</strong></p>
<p>Il est généralement très avantageux d’utiliser <strong>des logiciels de téléphonie sur IP</strong> car les tarifs proposés à l’international sont souvent moindre que ceux des opérateurs locaux exploitant des lignes classiques, voire gratuits dans certaines conditions.</p>
<h4 class="spip">Poste</h4>
<p>Les liaisons postales sont satisfaisantes, elles s’effectuent en quelques jours. Il vaut mieux envoyer lettres et colis en recommandé.</p>
<p><i>Mise à jour : juillet 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/vie-pratique/article/communications). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
