# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/vie-pratique/article/transports#sommaire_1">Importation de véhicule</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/vie-pratique/article/transports#sommaire_2">Permis de conduire</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/vie-pratique/article/transports#sommaire_3">Code de la route</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/vie-pratique/article/transports#sommaire_4">Assurances et taxes</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/vie-pratique/article/transports#sommaire_5">Achat et location</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/vie-pratique/article/transports#sommaire_6">Entretien</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/vie-pratique/article/transports#sommaire_7">Réseau routier</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/vie-pratique/article/transports#sommaire_8">Transports en commun</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Importation de véhicule</h3>
<p>La conduite étant à gauche, il n’est pas recommandé d’importer des véhicules ne disposant pas d’un volant à droite. L’importation d’un véhicule via le Nord de l’île est considérée comme une importation illégale en République de Chypre.</p>
<h3 class="spip"><a id="sommaire_2"></a>Permis de conduire</h3>
<p>Le permis de conduire international n’est pas obligatoire : un permis français valide est suffisant s’il correspond au type de véhicule que l’on souhaite conduire.</p>
<p>Au-delà de six mois, on peut se voir délivrer un permis de conduire valable uniquement à Chypre, avec conservation du permis français.</p>
<p>Le permis chypriote coûte environ 60 € (35 CYP) et peut être obtenu en 10 jours.</p>
<h3 class="spip"><a id="sommaire_3"></a>Code de la route</h3>
<p>Pour toute information :</p>
<p>Department of Road Transport<br class="manualbr">Vasileos Pavlou 27, 2412 Egkomi<br class="manualbr">Tél. : +35722807000 <br class="manualbr">Fax : +35722354030<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/vie-pratique/article/transports#roadtransport#mc#rtd.mcw.gov.cy#" title="roadtransport..åt..rtd.mcw.gov.cy" onclick="location.href=mc_lancerlien('roadtransport','rtd.mcw.gov.cy'); return false;" class="spip_mail">Courriel</a></p>
<h3 class="spip"><a id="sommaire_4"></a>Assurances et taxes</h3>
<p>L’assurance au tiers est obligatoire. Elle n’est pas valable dans la zone nord (nécessité de contracter une assurance complémentaire « chypriote-turque », des points de vente se trouvent aux principaux points de passage).</p>
<p>Prix indicatifs en janvier 2013 :</p>
<ul class="spip">
<li>véhicule jusqu’à 1600cc, assurance tous risques : entre 300 et 400 euros/an selon bonus et l’âge du véhicule.</li>
<li>véhicule de 1600cc à 1900cc, assurance tous risques : entre 400 et 600 euros/an selon bonus et l’âge du véhicule.</li></ul>
<h3 class="spip"><a id="sommaire_5"></a>Achat et location</h3>
<p>De nombreux constructeurs automobiles sont représentés à Chypre : Peugeot, Renault, Citroën, BMW, Mercedes, ainsi que toutes les marques japonaises.</p>
<p>Il est recommandé d’utiliser un véhicule climatisé avec volant à droite.</p>
<p>Le service des mines contrôle les voitures importées, en particulier les phares à cause de la conduite à gauche, volant à droite.</p>
<p>Compte tenu des prix à l’achat pratiqués localement et de la conduite à gauche, il n’est pas utile d’importer son véhicule.</p>
<p>Il est possible de louer un véhicule à partir de 23 ans pour les titulaires du permis depuis plus de quatre ans. De nombreuses sociétés de location existent. Les prix de location varient en fonction de la saison, de la marque et du nombre de jours utilisés.</p>
<h3 class="spip"><a id="sommaire_6"></a>Entretien</h3>
<p>L’entretien des véhicules ne pose aucune difficulté sur place, aussi bien en ce qui concerne les réparations que l’approvisionnement en pièces détachées. Il existe de nombreux garages dans la capitale.</p>
<h3 class="spip"><a id="sommaire_7"></a>Réseau routier</h3>
<p>L’état du réseau routier est satisfaisant.</p>
<h4 class="spip">Infrastructure routière</h4>
<p>Pour visiter certains villages et des sites intéressants, le recours à un véhicule 4X4 peut se révéler nécessaire. Les conducteurs chypriotes prennent parfois des libertés avec le code de la route. La conduite est à gauche et appelle un surcroît de vigilance. La conduite en véhicule à deux roues se révèle assez dangereuse. En cas d’accident même bénin, il convient d’attendre l’arrivée de la police pour le constat. Il n’y a aucune difficulté pour les approvisionnements en carburant.</p>
<h3 class="spip"><a id="sommaire_8"></a>Transports en commun</h3>
<p>En dehors des restrictions de passage en zone nord, la circulation ne pose pas de problème à l’intérieur de Chypre. Les transports urbains sont peu développés, à l’exception de quelques lignes de bus et des taxis collectifs. Une course en taxi dans la capitale ne dépasse guère 10 euros.</p>
<p><strong>Autobus</strong></p>
<p>Il existe trois sortes d’<a href="http://www.cyprusbybus.com/" class="spip_out" rel="external">autobus à Chypre</a> pour faciliter vos déplacements :</p>
<ul class="spip">
<li>Les autobus interurbains qui relient quotidiennement et régulièrement les villes entre elles.</li>
<li>Les autobus ruraux qui relient presque tous les villages aux grandes villes proches, à raison d’un ou deux par jour, sauf le dimanche.</li>
<li>Les bus urbains qui sillonnent les villes avec des horaires réguliers et fréquents le jour. Dans certains quartiers touristiques et pendant la saison d’été, ils fonctionnent jusque tard le soir.</li></ul>
<p>Depuis quelques années, un service de bus (bus bleus) relie divers quartiers de l’extérieur de Nicosie à la gare routière (centre ville). Le ticket est à 1.50 € et les enfants scolarisés ont une carte qui leur permet de voyager gratuitement toute l’année.</p>
<p><strong>Vélos</strong></p>
<p>La ville de Nicosie s’est dotée à différents endroits stratégiques (musée – théâtre – etc…) de stations de location de vélo sur le modèle du Vélib parisien.</p>
<p><strong>Les Taxis</strong></p>
<p>Les taxis interurbains que l’on peut partager avec quatre à sept autres passagers. Ils offrent des liaisons régulières entre toutes les grandes villes de Chypre, toutes les demi-heures, du lundi au vendredi de 6 h à 18 h, et jusqu’à 17 h le samedi et le dimanche. On peut réserver sa place par téléphone ou par internet auprès des différentes compagnies.</p>
<p>Les taxis ruraux ne fonctionnent que depuis ou vers leur base. Ces taxis ne sont pas équipés de compteurs et le tarif dépend du prix par kilomètre.</p>
<p>Le service de taxi est assuré 24 h/24 dans les villes. Les taxis peuvent être réservés à l’avance ou hélés directement dans la rue. Les taxis urbains sont obligatoirement équipés d’un compteur qui commence à tourner dès l’entrée à bord du passager.</p>
<p>Le transport d’animaux de compagnie n’est autorisé que s’ils sont enfermés dans un panier.</p>
<p><strong>Transport aérien</strong></p>
<p>Chypre est desservi par deux aéroports internationaux, Larnaca et Paphos.</p>
<p>L’aéroport d’Ercan (Tymbou), desservant directement Chypre-nord depuis la Turquie, a été déclaré illégal par décision du gouvernement de la République de Chypre et n’est pas agréé par l’organisation de l’aviation civile internationale (OACI). Il est néanmoins utilisé par des compagnies turques ou chypriotes turques ainsi que pour des vols charters, notamment en provenance ou à destination du Royaume-Uni. Les voyageurs qui utiliseraient cet aéroport doivent savoir qu’en cas d’accident, de litige ou de contentieux, ils ne bénéficient pas des conventions internationales protectrices des droits du passager.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/vie-pratique/article/transports). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
