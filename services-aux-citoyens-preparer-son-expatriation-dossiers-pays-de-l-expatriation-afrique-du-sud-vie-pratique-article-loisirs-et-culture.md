# Loisirs et culture

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/vie-pratique/article/loisirs-et-culture#sommaire_1">Tourisme</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/vie-pratique/article/loisirs-et-culture#sommaire_2">Activités culturelles</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/vie-pratique/article/loisirs-et-culture#sommaire_3">Sports</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/vie-pratique/article/loisirs-et-culture#sommaire_4">Télévision – Radio</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/vie-pratique/article/loisirs-et-culture#sommaire_5">Presse française</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/vie-pratique/article/loisirs-et-culture#sommaire_6">Librairies</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Tourisme</h3>
<p>L’expression <i>Rainbow Nation</i> pourrait aussi s’appliquer au patrimoine naturel de l’Afrique du Sud tant celui-ci est diversifié. D’une province à l’autre, les parcs nationaux et les réserves, nombreux et souvent immenses, permettent d’appréhender la spectaculaire variété des sites, des cultures, des paysages, des écosystèmes : réserves littorales (De Hoop, Cederberg Wilderness), parcs de montagne (Karoo, Drakensberg), réserves fauniques, parc semi-désertique du Kalahari qui se prolonge au Botswana, canyon de Blyde River (dans la province de Mpumalanga), parc volcanique du Pilanesberg (dans la province du Nord-ouest). Le plus connu, le parc national Kruger (frontalier avec le Mozambique), couvre un territoire de la taille du pays de Galles.</p>
<p>Pour plus d’information, on peut consulter le site Internet des <a href="http://www.parks-sa.co.za/" class="spip_out" rel="external">South African National Parks</a>.</p>
<p>La seule province du Cap Ouest suffit à donner une indication des possibilités de découvertes insolites ou grandioses avec le Mont de la Table (<i>Table Moutain</i>, parc regroupant 1400 espèces végétales) dominant la ville du Cap (où se trouve le South Africa Museum, le plus grand et le plus ancien musée du pays), la région des vignobles autour de Stellenbosch, la route côtière appelée Route des Jardins, la route panoramique creusée à flanc de corniche de Chapman’s Peak, le Cap de Bonne Espérance, les fermes dédiées à l’élevage d’autruches dans la région de Little Karoo.</p>
<h3 class="spip"><a id="sommaire_2"></a>Activités culturelles</h3>
<h4 class="spip">Activités culturelles en français</h4>
<p>Le cinéma se fait de plus en plus présent en version originale sous-titrée pour la diffusion en salles. Les cinémas Ster Kinekor proposent régulièrement des films français sous-titrés en anglais, avec le soutien de l’Institut français d’Afrique du Sud (IFAS).</p>
<p>Il y a peu de théâtres en langue française, sauf à l’IFAS. Des expositions sont organisées par l’IFAS et les Alliances françaises.</p>
<p><strong>Institut Français d’Afrique du Sud (IFAS)</strong><br class="manualbr">Phenyo House – 62 Juta Street – Braamfontein - Johannesburg <br class="manualbr">P.O. Box 542 - 2113 Newtown <br class="manualbr">Téléphone : (011) 403 0458 - Télécopie : (011) 403 0465 <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/vie-pratique/article/loisirs-et-culture#ifas#mc#ifas.org.za#" title="ifas..åt..ifas.org.za" onclick="location.href=mc_lancerlien('ifas','ifas.org.za'); return false;" class="spip_mail">Courriel</a></p>
<p><strong>Alliances françaises</strong></p>
<p>19 Alliances françaises dispensent des cours de français, gèrent des bibliothèques-médiathèques et accueillent des manifestations culturelles. Consulter le site des <a href="http://www.alliance.org.za/" class="spip_out" rel="external">Alliances françaises en Afrique du Sud</a>.</p>
<h4 class="spip">Activités culturelles locales</h4>
<h5 class="spip">Télévision</h5>
<p>La <i>South African Broadcasting Corporation</i> (SABC) dispose de trois chaînes qui diffusent toutes en anglais, outre les autres langues sud-africaines. Une chaîne câblée, à péage, équivalent de Canal +, propose beaucoup de films. Les chaînes étrangères peuvent par ailleurs être captées avec une antenne parabolique. Les programmes très variés se révèlent par ailleurs d’inégale qualité. L’influence américaine est très marquée.</p>
<h5 class="spip">Cinéma et théâtre </h5>
<p>Les salles de cinéma, nombreuses et très confortables, projettent les meilleurs films étrangers grand public ainsi que le nouveau cinéma (films européens, australiens, scandinaves, asiatiques, etc.). Il existe peu de productions sud-africaines. Les projections en plein air sont également très fréquentées.</p>
<p>Les théâtres proposent des pièces américaines ou anglaises, des pièces en afrikaans et des pièces "alternatives" plutôt en anglais :</p>
<ul class="spip">
<li>à Johannesburg : le Civic Theatre, le Market Theatre, le Teatro au sein du complexe de loisirs Montecasino ;</li>
<li>à Pretoria : State Theatre, Brooklyn Theatre ;</li>
<li>au Cap, les offres de spectacles sont également nombreuses et diversifiées.</li></ul>
<h5 class="spip">Musique et musées</h5>
<p>Les clubs de jazz abondent à Durban et Johannesburg.</p>
<p>De très nombreux musées d’Afrique du Sud, les principaux sont recensés sur Internet à l’adresse suivante : <a href="http://www.museumsonline.co.za/" class="spip_out" rel="external">Museums Online South Africa</a>.</p>
<h3 class="spip"><a id="sommaire_3"></a>Sports</h3>
<p>Le climat, l’étendue et la diversité du pays permettent de disposer d’un large spectre d’activités sportives : activités nautiques (surf, plongée sous-marine), randonnée (innombrables circuits des parcs et réserves), escalade, sports aériens, golf et équitation. Les clubs et les installations sportives sont très nombreux pour toutes activités et les équipements disponibles sur le marché local. Les sports les plus populaires chez les Sud-Africains demeurent le rugby, le cricket, le football, l’athlétisme et donnent lieu à des rencontres fréquentes.</p>
<p>Pour chasser en Afrique du Sud, il faut être accompagné d’un <i>Registered Professional Hunter</i>. L’importation d’une arme de chasse est soumise à déclaration. Le permis de chasse est délivré par le <i>Receiver of Revenues</i>. Les périodes d’ouverture et de fermeture de la chasse varient selon les espèces.</p>
<p>Le permis pour la pêche en eau douce s’obtient également auprès du <i>Receiver of Revenues</i>. Les périodes autorisées diffèrent selon les types de pêche.</p>
<h3 class="spip"><a id="sommaire_4"></a>Télévision – Radio</h3>
<p>Radio France Internationale peut être captée tous les jours avec une excellente réception si l’on dispose d’une antenne parabolique adéquate. La qualité en ondes courtes est par contre médiocre.</p>
<p>Par le satellite avec le bouquet "parabole Réunion", il est possible de capter 23 chaînes françaises avec une antenne adéquate.</p>
<h3 class="spip"><a id="sommaire_5"></a>Presse française</h3>
<p>Les journaux et livres en français sont difficiles à trouver même s’il n’y a ni censure, ni restrictions à l’importation. Il existe des amorces de distribution dans quelques librairies. Les bibliothèques des Alliances françaises proposent généralement de nombreux titres de presse, romans, essais, livres pour enfants etc.</p>
<h3 class="spip"><a id="sommaire_6"></a>Librairies</h3>
<h4 class="spip">Johannesburg </h4>
<p>Exclusive Books (à Village Walk en particulier), Facts and Fiction (Rosebank en particulier), les grands CNA - Central News Agency (Sandton, Rosebank, Eastgate).</p>
<h4 class="spip">Le Cap</h4>
<p>I.D. Booksellers (122 Longmarket Street - Téléphone : 021 423 9104), CNA au centre commercial de Waterfront (seulement quelques journaux) et Juta Booksellers (1 Bree Street, pour les livres et dictionnaires seulement).</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/vie-pratique/article/loisirs-et-culture). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
