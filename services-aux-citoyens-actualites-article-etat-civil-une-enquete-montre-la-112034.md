# État civil : une enquête montre la qualité du service rendu par le ministère des Affaires étrangères (01.04.14)

<p>Soucieux d’améliorer autant que possible la qualité de ses prestations, <strong>le service central d’état civil (SCEC) du ministère des Affaires étrangères a réalisé une enquête de satisfaction sur la qualité du service offert aux usagers pour la délivrance de copies ou d’extraits d’actes de l’état civil.</strong></p>
<p>Pour mémoire, le SCEC délivre chaque année autour de 2 millions de documents d’état civil.</p>
<p><strong>Pendant 5 semaines, 58 000 usagers ont ainsi été interrogés par messagerie électronique,</strong> 12 jours après avoir formulé leur demande d’un document d’état civil.</p>
<p>Ce délai tient compte du temps de traitement des demandes (2 jours en moyenne), de la mise sous pli puis surtout de l’acheminement par voie postale, l’envoi sous format électronique n’étant pas accepté pour les documents d’état civil.</p>
<p>Près d’une personne sur trois a répondu aux questions posées, un chiffre qui reflète en lui-même l’attachement des usagers au bon fonctionnement de leur service public.</p>
<p><strong><br class="autobr">Les résultats de l’enquête, très positifs, mettent en avant une grande qualité de service : </strong></p>
<ul class="spip">
<li>90% des usagers se disent très satisfaits ou satisfaits, et le taux s’élève à 95% lorsque les usagers ont déjà reçu un document d’état civil ou un courrier du SCEC au moment de l’enquête.</li>
<li>les commentaires sont eux aussi en grande majorité très positifs, voire élogieux : plus de 30% d’entre eux contiennent les mots "parfait", "efficace", "très satisfaisant ou satisfaisant", "très bien ou bien", "bon service".</li></ul>
<p>Cette appréciation globale très positive montre qu’en dépit d’un contexte budgétaire contraint, <strong>les efforts mis en œuvre par le service central d’état civil pour assurer un service de qualité portent leurs fruits.</strong></p>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/photos-videos-publications-infographies/publications/brochures-institutionnelles/article/le-service-central-d-etat-civil" class="spip_in">Présentation du service central d’état civil</a></li>
<li><a href="services-aux-citoyens.md" class="spip_in">La rubrique "Vivre à l’étranger"</a></li></ul>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/actualites/article/etat-civil-une-enquete-montre-la-112034). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
