# Curriculum vitae

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/emploi-stage/article/curriculum-vitae#sommaire_1">Présentation</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/emploi-stage/article/curriculum-vitae#sommaire_2">Contenu</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/emploi-stage/article/curriculum-vitae#sommaire_3">Modèles de CV</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Présentation</h3>
<ul class="spip">
<li>Une page est la norme, ne jamais dépasser deux pages même en cas de longue carrière, éviter les mises en page trop denses ;</li>
<li>Il est recommandé de traduire (ou faire traduire) votre CV en espagnol ;</li>
<li>Police de caractère doit être sobre et lisible ;</li>
<li>Papier blanc ou discrètement coloré ;</li>
<li>Puces et gras (utilisé avec parcimonie pour marquer uniquement les points clés) ;</li>
<li>L’essentiel doit apparaître au début ;</li>
<li>Se concentrer sur la mise en valeur des thèmes correspondant à l’objectif ;</li>
<li>Dire uniquement la vérité ;</li>
<li>Supprimer toute information inutile ;</li>
<li>Veiller à l’orthographe.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Contenu</h3>
<ul class="spip">
<li>Nom ;</li>
<li>Profession ;</li>
<li>Téléphone (y compris le téléphone portable) ;</li>
<li>Courriel ;</li>
<li>Etat-civil ;</li>
<li>Date et lieu de naissance ;</li>
<li>Brève description personnelle et de vos intérêts en général ;</li>
<li>Photo portrait de qualité ;</li>
<li>Statut migratoire (essentiel).</li></ul>
<h4 class="spip">Objectif professionnel </h4>
<ul class="spip">
<li>Type d’emploi ;</li>
<li>Domaine d’intérêt ;</li>
<li>Parcours professionnel et sites internet des entreprises concernées ;</li>
<li>Lieu géographique d’implantation.</li></ul>
<h4 class="spip">Scolarité</h4>
<ul class="spip">
<li>Uniquement le diplôme le plus élevé (et son équivalence) ;</li>
<li>Indiquer la période correspondant aux études (dates début et fin) ;</li>
<li>Indiquer le nom des écoles, universités et l’intitulé exact de la filière ainsi que leur site internet ;</li>
<li>Cours/stages/formations annexes ou spécifiques en relation avec le poste.</li></ul>
<h4 class="spip">Langues </h4>
<ul class="spip">
<li>Précisez toujours votre niveau d’espagnol ;</li>
<li>L’anglais est un atout important.</li></ul>
<h4 class="spip">Connaissances informatiques et techniques</h4>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Privilégier celles en lien direct avec l’emploi visé.</p>
<h4 class="spip">Trajectoire professionnelle </h4>
<ul class="spip">
<li>Entreprise : commencer par présenter la plus récente, puis remonter dans le temps ;</li>
<li>Taille de l’entreprise, activité, chiffre d’affaire, effectif et site internet ;</li>
<li>Fonction exercée : pensez à bien décrire précisément les fonctions réelles exercées et les compétences gagnées, surtout pour les expériences les plus récentes ;</li>
<li>Références : nom, fonction et coordonnées de chaque employeur (ou site internet si les coordonnées y figurent). Privilégier les références locales de nature à rassurer les employeurs sur l’adaptation du candidat au marché du travail vénézuélien ;</li>
<li>Période (en années de préférence) ;</li>
<li>Emploi et fonction occupés ;</li>
<li>Principales tâches professionnelles accomplies ;</li>
<li>Raison du départ de l’entreprise.</li></ul>
<h4 class="spip">Autres informations qui peuvent être fournies</h4>
<ul class="spip">
<li>Activités de loisirs ;</li>
<li>Mobilité ;</li>
<li>Domaine professionnel de préférence ;</li>
<li>Date et signature.</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Modèles de CV</h3>
<h4 class="spip">Apellido y Nombre</h4>
<p>Diploma</p>
<p>Dirreccion, Tél, Email</p>
<p>Objetivo profesional</p>
<h4 class="spip">Experiencia laboral</h4>
<h5 class="spip">Freelancer</h5>
<p>Desde Agosto 2011.</p>
<p><strong>Misión : </strong>creación de programas de escritorio y páginas web corporativas, personales y de estudiantes</p>
<h5 class="spip">Técnico de Servicio</h5>
<p>Mayo 2009- Mayo 2011.</p>
<p><strong>Misión : </strong>Instalación, desinstalación, reprogramación, mantenimiento correctivo y preventivo de puntos de venta (POS) para Banesco</p>
<h5 class="spip">Bibliotecario</h5>
<p>Enero 2008-Enero 2009. Escuela Bolivariana "Dr. Ramón Gualdrón"</p>
<p><strong>Misión : </strong>Del cargo :</p>
<ul class="spip">
<li>Orientar alumnos, docentes y usuarios de la biblioteca</li>
<li>Elaborar inventario mensual de libros, para su mejor control</li></ul>
<p><strong>Adicional :</strong></p>
<ul class="spip">
<li>Desarrollo de un programa para notas finales de los alumnos</li>
<li>Implementación y mantenimiento de Red interna en la Institución</li>
<li>Mantenimiento a computadoras</li>
<li>Participación en cursos de informática básica a los alumnos del plantel</li></ul>
<h4 class="spip">Títulos y formaciones</h4>
<p><strong>Universidad Fermin Toro</strong><br class="manualbr">Desde Marzo 2010 : Ingeniero en computación</p>
<p><strong>Instituto Universitario Tecnológico Industrial Rodolfo Loero Arismendi núcleo Barquisimeto</strong> <br class="manualbr">Marzo 2006 Enero 2009 : Técnico Superior Universitario en Informática</p>
<p><strong>Instituto Diocesano Barquisimeto</strong> <br class="manualbr">Septiembre 1999 Julio 2004- Bachiller en ciencias</p>
<p><strong>Unidad Educativa Colegio "La Milagrosa"</strong><br class="manualbr">Septiembre 1993 Julio 1999 Educación Básica</p>
<h4 class="spip">Competencias informáticas</h4>
<p><strong>Programas informáticos</strong><br class="manualbr">Visual Basic 6.0, java, lenguaje C, C++, HTML, CSS, PHP, JavaScript, jQuery, Python, MySQL.</p>
<p><strong>Lenguas informáticas</strong><br class="manualbr">Office, Windows, Linux, Photoshop, Illustrator, Firework</p>
<p><strong>Informaciones complementarias</strong><br class="manualbr">Implementación de Redes, Creación de páginas Web, ensamblaje y mantenimiento de computadoras, desarrollo de Base de datos</p>
<h4 class="spip">Conocimientos idiomáticos</h4>
<p><strong>Alemán</strong><br class="manualbr"><strong>Nivel oral</strong> : Básico  <strong>Nivel escrito</strong> : Básico</p>
<p><strong>Inglés</strong><br class="manualbr"><strong>Nivel oral</strong> : Avanzado <strong>Nivel escrito</strong> : Avanzado</p>
<p><strong>Español</strong><br class="manualbr"><strong>Nivel oral</strong> : Nativo <strong>Nivel escrito</strong> : Nativo</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/emploi-stage/article/curriculum-vitae). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
