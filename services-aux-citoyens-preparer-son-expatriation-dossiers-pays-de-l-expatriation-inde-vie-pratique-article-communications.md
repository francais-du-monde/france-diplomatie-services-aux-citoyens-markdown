# Communications

<h4 class="spip">Téléphone – Internet</h4>
<p>Le fonctionnement du réseau téléphonique est satisfaisant. Il existe de nombreuses boutiques faisant office de "cabines téléphoniques" d’où l’on peut appeler la France et envoyer des télécopies.</p>
<p>Pour appeler l’Inde depuis la France, composez le 00 91, suivi de l’indicatif de la ville : <strong>11</strong> pour New-Delhi, <strong>22</strong> pour Bombay, <strong>33</strong> à Calcutta, <strong>44</strong> à Chennai, <strong>80</strong> à Bangalore, <strong>20</strong> à Pune…</p>
<p>Pour appeler la France, composer le 00 33 et l’indicatif régional sans le 0.</p>
<p>Les téléphones portables sont très répandus. Les différents opérateurs proposent divers systèmes prépayés ou d’abonnement. Les réseaux couvrent l’essentiel du territoire, et les communications sont très bon marché.</p>
<p>L’usage du courrier électronique est très développé. De nombreux cyber-cafés proposent des accès à environ 60 roupies la demi-heure. La majorité des foyers dispose d’un abonnement internet ADSL ou par câble.</p>
<h4 class="spip">Téléphoner gratuitement par Internet </h4>
<p>Il est généralement très avantageux d’utiliser <strong>des logiciels de téléphonie sur IP</strong> (Skype, Google talk, Live messenger, Yahoo ! messenger, etc.) car les tarifs proposés à l’international sont souvent moindre que ceux des opérateurs locaux exploitant des lignes classiques, voire gratuits dans certaines conditions.</p>
<h4 class="spip">Poste</h4>
<p>Le courrier est acheminé dans un délai moyen d’une semaine, qui peut aller jusqu’à 15 jours (plus rapide dans le sens France-Inde). Les services des postes sont lents mais relativement fiables. Toutefois, la poste retient les colis si le contenu est taxable ou soumis à restrictions et le destinataire devra payer les taxes à l’importation.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/vie-pratique/article/communications). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
