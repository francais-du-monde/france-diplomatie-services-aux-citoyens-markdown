# Emploi

<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-le-retour-en-france-emploi-article-la-reinsertion.md" title="La recherche d’emploi">La recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-le-retour-en-france-emploi-article-la-validation-des-acquis-de-l-experience-vae.md" title="La validation des acquis de l’expérience (VAE)">La validation des acquis de l’expérience (VAE)</a></li>
<li><a href="services-aux-citoyens-le-retour-en-france-emploi-article-les-equivalences-de-diplomes.md" title="Les équivalences de diplômes">Les équivalences de diplômes</a></li>
<li><a href="services-aux-citoyens-le-retour-en-france-emploi-article-le-chomage.md" title="Le chômage">Le chômage</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/emploi/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
