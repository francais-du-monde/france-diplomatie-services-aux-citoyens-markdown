# Entretien d’embauche

<h4 class="spip">Préparer l’entretien</h4>
<h4 class="spip">Gestion des informations </h4>
<p>Le candidat doit se préparer à l’entretien, cibler, analyser, et organiser les informations disponibles sur l’entreprise et son secteur d’activité. Pour ce faire, il utilisera les données fournies disponibles sur Internet.</p>
<h4 class="spip">Communication</h4>
<p>Le candidat doit s’informer sur l’état du marché du travail et sur les exigences du poste pour lequel il postule. Il doit répondre de manière claire et précise aux questions du recruteur et doit en contrepartie lui poser des questions pertinentes sur le poste et son évolution au sein de l’entreprise.</p>
<h4 class="spip">Comportement durant l’entretien</h4>
<h4 class="spip">Image positive</h4>
<p>Le candidat doit avoir confiance en lui-même et se présenter au recruteur sans essayer de cacher certains aspects de sa personnalité. Certains recruteurs n’hésitent pas à utiliser l’analyse du langage du corps pour interpréter certains comportements ou postures.</p>
<h4 class="spip">Responsabilité</h4>
<p>La capacité à fixer des objectifs et des priorités qui tiennent compte des équilibres entre les vies professionnelle et privée est un critère qui permettra au recruteur d’apprécier le pragmatisme du candidat. Il ne faut pas vouloir trop en faire ou trop promettre en espérant être le meilleur.</p>
<h4 class="spip">Capacité à s’adapter</h4>
<p>La période d’adaptation du nouvel employé doit être courte. Il doit être rapidement opérationnel. Le candidat doit donc montrer son ouverture d’esprit, sa capacité à travailler en équipe et à s’intégrer pour apprendre rapidement, mais il doit en même temps convaincre qu’il peut être indépendant et autonome quand cela est nécessaire.</p>
<h4 class="spip">Volonté d’accéder à plus de connaissances</h4>
<p>La formation continue est une priorité pour qui veut se perfectionner et parfaire ses connaissances dans un environnement en constante évolution. Lors de l’entretien, le candidat doit préciser au recruteur ses intentions de <strong>participer aux séminaires de formation de l’entreprise</strong>.</p>
<p><i>Mise à jour : novembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/emploi-stage/article/entretien-d-embauche). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
