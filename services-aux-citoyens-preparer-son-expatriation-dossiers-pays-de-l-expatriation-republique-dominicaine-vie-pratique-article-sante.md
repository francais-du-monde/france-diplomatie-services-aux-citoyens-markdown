# Santé

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/vie-pratique/article/sante#sommaire_1">Avant le départ</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/vie-pratique/article/sante#sommaire_2">Sur place</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/vie-pratique/article/sante#sommaire_3">En cas d’urgence</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/vie-pratique/article/sante#sommaire_4">Retour en France</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Avant le départ</h3>
<p><strong>Consultez</strong> vos médecins (généraliste, dentiste).</p>
<p><strong>Souscrivez</strong> à une <a href="https://pastel.diplomatie.gouv.fr/fildariane/dyn/public/login.html;jsessionid=70E713254C8A9BE32B9026144D01E464.jvm01944-2" class="spip_in">assurance</a> : plusieurs cas ont été signalés de malades n’ayant pu être hospitalisés faute d’avoir acquitté, avant tout soin, des sommes très élevées (en dollars américains) auprès d’établissements hospitaliers. Ces pratiques sont courantes dans tout le pays lorsque les honoraires des médecins sont libres, surtout dans les zones les plus touristiques. Il convient donc de se renseigner auprès de son centre de sécurité sociale et de sa mutuelle avant de partir, et de souscrire une bonne assurance pour la durée prévue du séjour.</p>
<p><strong>Aucune vaccination</strong> n’est obligatoire pour entrer en République Dominicaine, cependant :</p>
<ul class="spip">
<li>la mise à jour de la vaccination diphtérie-tétanos-poliomyélite est recommandée,</li>
<li>d’autres vaccinations peuvent être conseillées (selon conditions d’hygiène et durée du séjour) : fièvre typhoïde, hépatites virales A et B.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Sur place</h3>
<p>Prévention notamment des maladies transmises par les piqûres d’insectes :</p>
<ul class="spip">
<li><strong>Paludisme</strong> : maladie parasitaire transmise par les piqûres de moustiques, elle impose le recours à des mesures de protection individuelle (sprays, crèmes, diffuseurs électriques, moustiquaires…). Il convient de s’adresser à votre médecin habituel ou à un centre de conseils aux voyageurs pour recevoir des informations adaptées avant un déplacement en République Dominicaine. <i>Classification</i> : le pays est classé en zone 1.</li>
<li><strong>Dengue</strong> : cette maladie virale est transmise par les piqûres de moustiques : il convient donc de respecter les mesures habituelles de protection (produits anti-moustiques à utiliser sur la peau et sur les vêtements, diffuseurs électriques). La dengue pouvant prendre une forme potentiellement grave (forme hémorragique) il est vivement recommandé de consulter un médecin en cas de fièvre et d’éviter la prise d’aspirine.</li>
<li><strong>H1N1</strong> : il vous est recommandé de respecter les règles d’hygiène élémentaires suivantes :<br class="manualbr">- Lavage des mains régulier<br class="manualbr">- Aération régulière des pièces</li>
<li><strong>Choléra</strong> : Une vague de choléra a touché la République dominicaine au cours de l’hiver 2010 - 2011. L’épidémie n’est pas encore totalement enrayée malgré les mesures prises par les autorités locales.</li></ul>
<p><strong>Afin de diminuer les risques, il convient d’observer des règles d’hygiène alimentaire rigoureuses :</strong></p>
<ul class="spip">
<li>Il est formellement déconseillé de boire l’eau du robinet. Il est possible de trouver de l’eau en bouteille capsulée dans l’ensemble du pays, ainsi que l’eau filtrée des <i>botellones</i>.</li>
<li>se laver régulièrement les mains (à l’eau savonneuse ou avec des gels hydro-alcooliques), en particulier avant tous les repas,</li>
<li>laver soigneusement (avec de l’eau filtrée des <i>botellones</i>) ou peler les fruits et légumes,</li>
<li>éviter la consommation de poissons, coquillages, ou fruits de mer autrement que bien cuits ou frits,</li>
<li>s’assurer d’une cuisson suffisante des aliments en général,</li>
<li>éviter les lieux (« cantines ambulantes ») ne garantissant pas toutes les conditions d’hygiène requises,</li>
<li>éviter de boire à même les canettes.</li></ul>
<p>A signaler, la présence en République Dominicaine – comme dans toutes les îles des Caraïbes – de la <i>cigüatera</i>, intoxication alimentaire par la chair des poissons et crustacés, contaminés par une micro-algue.</p>
<p>Cette maladie se traduit par des troubles digestifs, nerveux et cardio-vasculaires, ainsi que des démangeaisons. La consultation d’un médecin est formellement recommandée.</p>
<h3 class="spip"><a id="sommaire_3"></a>En cas d’urgence</h3>
<p><strong>Particularités locales :</strong> en cas de consultation médicale, et tout particulièrement avec un médecin contacté par un hôtel, il est impératif au préalable de bien <strong>faire spécifier le prix de la consultation</strong>.</p>
<p>De même, en cas d’hospitalisation, il est indispensable de <strong>demander un devis</strong> avant toute intervention et de vérifier auprès de votre assurance les modalités de prise en charge des soins (remboursement ou prise en charge directe). Il est fréquent que les hôpitaux empêchent la sortie des personnes n’ayant pas réglé l’intégralité de leurs frais médicaux.</p>
<h3 class="spip"><a id="sommaire_4"></a>Retour en France</h3>
<p>Il est vivement recommandé de consulter un médecin en cas de fièvre pendant le voyage ou dans les semaines qui suivent le retour en France.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Fiche Saint-Domingue sur le site du <a href="http://www.cimed.org" class="spip_out" rel="external">CIMED</a>.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/vie-pratique/article/sante). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
