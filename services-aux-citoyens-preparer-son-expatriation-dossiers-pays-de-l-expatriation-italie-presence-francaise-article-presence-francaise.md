# Présence française

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/presence-francaise/article/presence-francaise#sommaire_1">Ambassade et consulat de France</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/presence-francaise/article/presence-francaise#sommaire_2">Autorités françaises dans le pays</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/presence-francaise/article/presence-francaise#sommaire_3">Associations dans le pays</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Ambassade et consulat de France</h3>
<p>Consultez notre article <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/article/annuaire-des-representations-francaises-a-l-etranger" class="spip_in">Annuaire des représentations françaises à l’étranger</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Autorités françaises dans le pays</h3>
<h4 class="spip">Réseau français de coopération et d’action culturelle</h4>
<p>Les coordonnées des services du réseau de coopération et d’action culturelle français en Italie ainsi que la description de l’activité de ces services se trouvent dans la rubrique culture du site internet de l’Ambassade ou sur le site internet du réseau culturel : <a href="http://latitudefrance.diplomatie.gouv.fr/AnnuaireCarto.html" class="spip_out" rel="external">Latitude France</a>.</p>
<h4 class="spip">Economie : réseau mondial</h4>
<p>Business France, l’Agence française pour le développement international des entreprises, <a href="http://export.businessfrance.fr/italie/export-italie-avec-notre-bureau.html" class="spip_out" rel="external">est présente en Italie</a>. Sa mission est d’accompagner les entreprises françaises dans leur développement à l’international.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.youbuyfrance.com/it/" class="spip_out" rel="external">Site de la Mission économique Business France en Italie</a> </p>
<p>Les services économiques sont également présents. Ils sont une émanation de la Direction générale du Trésor et ont pour missions principales l’analyse macroéconomique et l’animation des relations économiques bilatérales entre la France et les pays où ils sont implantés.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.tresor.economie.gouv.fr/se/italie" class="spip_out" rel="external">Site internet du service économique français en Italie</a> </p>
<h4 class="spip">Vos élus à l’Assemblée des Français de l’étranger</h4>
<p>Pour toute information sur l’Assemblée des Français de l’étranger et pour connaître les conseillers et les Sénateurs qui représentent les Français établis hors de France dans votre circonscription, vous pouvez consulter les sites Internet suivants :</p>
<ul class="spip">
<li><a href="http://www.assemblee-afe.fr/" class="spip_out" rel="external">Assemblée des Français de l’étranger (AFE)</a> ;</li>
<li>Sénat au service des Français de l’étranger : <a href="http://www.senat.fr/expatries/dossiers_pays/italie.html" class="spip_out" rel="external">dossier spécifique à l’Italie sur le site du Sénat</a> .</li></ul>
<h4 class="spip">Députés des Français de l’étranger</h4>
<p>La révision constitutionnelle du 23 juillet 2008, publiée au Journal officiel du 19 avril 2011, introduit la représentation à l’Assemblée nationale des Français établis hors de France. La création de onze circonscriptions législatives à l’étranger permet aux Français expatriés d’élire leur député à l’Assemblée nationale à partir du scrutin de 2012.</p>
<p>Pour plus d’information : <a href="http://www.votezaletranger.gouv.fr/" class="spip_out" rel="external">Votez à l’étranger</a></p>
<h3 class="spip"><a id="sommaire_3"></a>Associations dans le pays</h3>
<h4 class="spip">Associations françaises</h4>
<h5 class="spip">Rome</h5>
<p>Vous trouverez sur le site du consulat de France à Rome la <a href="http://www.ambafrance-it.org/Associations-francaises,1897" class="spip_out" rel="external">liste des associations françaises de Rome, Naples et Palerme</a>.</p>
<h5 class="spip">Milan</h5>
<p>Vous trouverez sur le site du consulat général de France à Milan la <a href="http://www.ambafrance-it.org/-Associations-francaises-" class="spip_out" rel="external">liste des associations françaises de la circonscription consulaire de Milan</a>.</p>
<h5 class="spip">Florence</h5>
<ul class="spip">
<li><strong>Association des Français de Toscane </strong><br class="manualbr">Via E. Rossi, 15 - 50139 Florence <br class="manualbr">Téléphone : 055.48.24.53</li>
<li><strong>Associations des parents d’élèves (A.P.E)</strong><br class="manualbr">Ecole Française de Florence<br class="manualbr">Via Gioberti, 67 - 50121 Florence<br class="manualbr">Téléphone : 055 67 71 10 – Télécopie : 05 562 35 749</li>
<li><a href="http://www.aaiff.it/" class="spip_out" rel="external">Association des Amis de l’Institut français de Florence</a></li>
<li>Centro Romantico - Palazzo Strozzi, 50123 Firenze</li>
<li><a href="http://www.florence-accueil.org" class="spip_out" rel="external">Florence Accueil</a><br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/presence-francaise/article/presence-francaise#floacc#mc#yahoo.fr#" title="floacc..åt..yahoo.fr" onclick="location.href=mc_lancerlien('floacc','yahoo.fr'); return false;" class="spip_mail">Courriel</a></li></ul>
<h5 class="spip">Brescia</h5>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.fiafe.org/index.php?module=accueilsamp;ville=BRESCIA" class="spip_out" rel="external">Brescia contact</a><br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/presence-francaise/article/presence-francaise#venturap#mc#unifi.it#" title="venturap..åt..unifi.it" onclick="location.href=mc_lancerlien('venturap','unifi.it'); return false;" class="spip_mail">Courriel</a></p>
<h5 class="spip">Verone</h5>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.fiafe.org/index.php?module=accueilsamp;ville=VERONE" class="spip_out" rel="external">Vérone contact</a><br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/presence-francaise/article/presence-francaise#eragris#mc#libero.it#" title="eragris..åt..libero.it" onclick="location.href=mc_lancerlien('eragris','libero.it'); return false;" class="spip_mail">Courriel</a></p>
<p>Il existe de nombreuses autres associations de Français en Italie. Leurs coordonnées peuvent être obtenues auprès des consulats.</p>
<h4 class="spip">Associations franco-italiennes</h4>
<ul class="spip">
<li>Associazone "Italia-Francia"<br class="manualbr">Largo Fontanella di Borghese, 19 - 00186 Rome<br class="manualbr">Tél. : 06-687-89-26</li>
<li>Associations culturelles franco-italiennes (A.C.I.F.) <br class="manualbr">S.Marco 4939- 30124 Venezia<br class="manualbr">Tél. : 04 15 22 70 79</li></ul>
<p><i>Mise à jour : septembre 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/presence-francaise/article/presence-francaise). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
