# Communications

<h4 class="spip">Téléphone – Internet</h4>
<p>Les liaisons téléphoniques avec la France sont bonnes.</p>
<p>Coût d’un appel téléphonique vers la France (postes fixes) : 160 ou 180 FCFA la minute (soit 0,24€ ou 0,27€ selon l’heure).</p>
<p>L’indicatif du Sénégal est le 221 : depuis la France, composer 00 221 puis le numéro du correspondant (n° à neuf chiffres).</p>
<p>Le réseau ADSL a fait de grands progrès. Les connections au réseau sont rapides.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Voir aussi : <a href="http://www.tv5.org/TV5Site/voyageurs/accueil.php" class="spip_out" rel="external">Tv5.org</a> </p>
<h4 class="spip">Téléphoner gratuitement par Internet</h4>
<p>Il est généralement très avantageux d’utiliser <strong>des logiciels de téléphonie sur IP</strong> (Skype, Google talk, FaceTime, etc.) car les tarifs proposés à l’international sont souvent moindre que ceux des opérateurs locaux exploitant des lignes classiques, voire gratuits dans certaines conditions.</p>
<h4 class="spip">Poste</h4>
<p>Le courrier est acheminé de façon régulière. A Dakar, le délai moyen de réception d’un envoi effectué de France est d’une semaine pour une lettre et jusqu’à deux semaines pour un colis. La garantie de réception est satisfaisante. Une boîte postale est conseillée. DHL existe mais à un tarif relativement élevé.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/vie-pratique/article/communications). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
