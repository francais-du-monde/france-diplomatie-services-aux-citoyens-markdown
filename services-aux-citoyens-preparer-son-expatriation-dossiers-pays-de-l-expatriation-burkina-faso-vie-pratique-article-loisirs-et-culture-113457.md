# Loisirs et culture

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/vie-pratique/article/loisirs-et-culture-113457#sommaire_1">Tourisme</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/vie-pratique/article/loisirs-et-culture-113457#sommaire_2">Activités culturelles</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/vie-pratique/article/loisirs-et-culture-113457#sommaire_3">Sports</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/vie-pratique/article/loisirs-et-culture-113457#sommaire_4">Télévision – Radio</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/vie-pratique/article/loisirs-et-culture-113457#sommaire_5">Presse française</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Tourisme</h3>
<p>Des trois parcs nationaux, le parc du W, tout à l’est du pays, est le plus grand, partagé entre le Bénin, le Niger et le Burkina. Le parc d’Arly, au sud-est, (plus près de la capitale, à 520 km) abrite notamment les grands mammifères et une avifaune très variée. Le parc de Pô (Kaboré Tambi), à 120 km au sud de la capitale, est le moins vaste.</p>
<p>A noter que les conditions d’hébergement sont spartiates dès que l’on s’éloigne de Ouagadougou et Bobo-Dioulasso.</p>
<p>Le travail du bronze et du cuir, la sculpture du bois, la poterie et la vannerie sont les fers de lance de l’artisanat.</p>
<h3 class="spip"><a id="sommaire_2"></a>Activités culturelles</h3>
<p><strong>Activités culturelles en français</strong></p>
<p>Les Centres culturels français animent en presque totalité la vie culturelle (concerts, spectacles, expositions, bibliothèques).</p>
<p><a href="http://www.institutfrancais-burkinafaso.com/" class="spip_out" rel="external">Institut français Georges Méliès</a><br class="manualbr">01 BP 561 - Ouagadougou 01 <br class="manualbr">Tél : (226) 50 30 60 97/98/99<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/vie-pratique/article/loisirs-et-culture-113457#contact-ouaga#mc#institutfrancais-burkinafaso.com#" title="contact-ouaga..åt..institutfrancais-burkinafaso.com" onclick="location.href=mc_lancerlien('contact-ouaga','institutfrancais-burkinafaso.com'); return false;" class="spip_mail">Courriel</a></p>
<p><a href="http://www.institutfrancais-burkinafaso.com/" class="spip_out" rel="external">Institut français Henri Matisse</a><br class="manualbr">BP 293 - Bobo-Dioulasso <br class="manualbr">Tél : (226) 20 97 39 79 /80<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/vie-pratique/article/loisirs-et-culture-113457#contact-bobo#mc#institutfrancais-burkinafaso.com#" title="contact-bobo..åt..institutfrancais-burkinafaso.com" onclick="location.href=mc_lancerlien('contact-bobo','institutfrancais-burkinafaso.com'); return false;" class="spip_mail">Courriel</a></p>
<p><strong>Activités culturelles locales</strong></p>
<p>Tous les vendredis soirs, à l’Institut français de Ouagadougou, a lieu un café-concert qui permet de découvrir les talents de la scène musicale régionale en toute convivialité.</p>
<p>L’institut Français du Burkina Faso accueille et soutient de nombreux festivals dont une partie se produit aussi à Bobo-Dioulasso :</p>
<ul class="spip">
<li>Rendez-vous chez nous (Arts de la Rue)</li>
<li>Festival International de danse de Ouagadougou</li>
<li>Dialogues de corps (Biennale de danse)</li>
<li>Rock à Ouaga</li>
<li>Waga festival</li>
<li>Jazz à Ouaga</li>
<li>Ciné droit libre</li>
<li>Rencontres Sobaté (Cinéma documentaire)</li>
<li><a href="http://www.fespaco.bf/" class="spip_out" rel="external">Fespaco</a></li>
<li>Journées cinématographiques de femmes africaines</li>
<li>Semaine du cinéma européen</li>
<li>Semaine du cinéma américain</li></ul>
<p>A Bobo-Dioulasso, la Semaine nationale de la culture initiée en 1983, propose des activités autour de pratiques traditionnelles, danse, musique et arts plastiques y sont l’honneur.</p>
<h3 class="spip"><a id="sommaire_3"></a>Sports</h3>
<p>Les principaux sports collectifs et individuels peuvent être pratiqués dans les clubs de la capitale et de Bobo-Dioulasso, ainsi que la natation dans les piscines des hôtels.</p>
<p>La chasse se pratique du 15 décembre au 30 mars. Le permis nécessaire s’obtient auprès du ministère de l’Environnement. La pêche est autorisée toute l’année sans permis. Toutefois, les lieux de pêche sont éloignés de la capitale.</p>
<h3 class="spip"><a id="sommaire_4"></a>Télévision – Radio</h3>
<p>Toutes les radios françaises et internationales sont accessibles via le Net.</p>
<p>TV5 peut être captée voie hertzienne et par satellite. Des opérateurs locaux proposent un abonnement à un "bouquet" de chaînes reçues au moyen d’antennes paraboliques.</p>
<h3 class="spip"><a id="sommaire_5"></a>Presse française</h3>
<p>De nombreuses revues mensuelles et quotidiens français sont disponibles à la consultation au sein de la médiathèque de l’Institut Français de Ouagadougou. Par ailleurs un pôle Etudes, un pôle Jeunesse et un pôle Arts et loisirs recèlent plus de 25 000 documents (livres, DVD, CD…) disponibles à l’emprunt sur abonnement.</p>
<p>A Bobo- Dioulasso, une bibliothèque numérique donne l’accès à de nombreux documents sur abonnement.</p>
<p>La presse française se trouve aussi dans les centres de presse des hôtels et dans certaines librairies. Les principales revues françaises sont vendues, à Ouagadougou, à la librairie DIACFA (choix de livres également), à Marina Market et SCIMAS (rue Yenenga).</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/vie-pratique/article/loisirs-et-culture-113457). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
