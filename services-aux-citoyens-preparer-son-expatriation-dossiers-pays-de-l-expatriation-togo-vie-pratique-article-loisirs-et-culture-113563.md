# Loisirs et culture

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/vie-pratique/article/loisirs-et-culture-113563#sommaire_1">Tourisme</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/vie-pratique/article/loisirs-et-culture-113563#sommaire_2">Activités culturelles</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/vie-pratique/article/loisirs-et-culture-113563#sommaire_3">Sports</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/vie-pratique/article/loisirs-et-culture-113563#sommaire_4">Télévision – Radio</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/vie-pratique/article/loisirs-et-culture-113563#sommaire_5">Presse française</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Tourisme</h3>
<p>Pour toute information, consultez le site de <a href="http://www.togo-tourisme.com/" class="spip_out" rel="external">Togo tourisme</a>.</p>
<p>La baignade le long du littoral togolais est particulièrement dangereuse.</p>
<h3 class="spip"><a id="sommaire_2"></a>Activités culturelles</h3>
<p><strong>Activités culturelles en français</strong></p>
<p>L’Institut français du Togo est provisoirement fermé en raison de sa prochaine relocalisation prévue en 2014.</p>
<p>Lomé concentre les centres culturels les plus importants. De nombreux artistes et troupes locales s’y expriment chaque semaine : musique, danse, théâtre, expositions…</p>
<p>Parmi les centres plus importants citons l’Institut français, Le Goethe Institut, le 54, l’espace Arema ou encore le Centre culturel Denyigba et le "brin de chocolat".</p>
<p><strong>Activités culturelles locales</strong></p>
<p>Le Togo dispose d’une très grande diversité ethnique et culturelle. Cette richesse s’exprime au travers des fêtes et des rites traditionnels qui rythment la vie de chacune des communautés. Celles-ci sont généralement liées aux croyances et aux traditions les plus anciennes et les plus ancrées au sein des populations locales.</p>
<p>Le Togo propose quelques fêtes traditionnelles d’exceptions :</p>
<ul class="spip">
<li><strong>Epe-ekpe</strong> (prise de la pierre sacrée) : nouvel an chez les Guins,</li>
<li><strong>Gadao-Adossa</strong> : fête des couteaux chez les TEM,</li>
<li><strong>Evala</strong> : les luttes traditionnelles kabyés…</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Sports</h3>
<p>Il est possible de pratiquer de nombreux sports collectifs ou individuels : volley-ball, tennis, natation, pétanque, basket-ball, golf, musculation, sports nautiques, équitation, tir à l’arc, aéronautisme, pêche sous-marine, judo, aïkido, karaté et rugby. Il existe à Lomé plusieurs clubs permettant la pratique d’une bonne variété de sports. La pêche peut se pratiquer sans permis. La chasse est autorisée, à condition de posséder un permis de port d’armes, un permis d’importation d’arme et de munitions ainsi qu’une autorisation de chasse. Aucune vente d’armes ne s’effectue sur place. La chasse est ouverte du 1er janvier au 30 avril.</p>
<h3 class="spip"><a id="sommaire_4"></a>Télévision – Radio</h3>
<p>Il est possible de capter des chaînes étrangères avec abonnement et décodeur ou grâce aux paraboles (France 2, RTL 9, Euronews, CFI, Canal Horizon, TV5, CNN…). On peut capter Radio-France Internationale en FM (91,5 MHZ) avec une qualité de réception excellente.</p>
<h3 class="spip"><a id="sommaire_5"></a>Presse française</h3>
<p>Les journaux, livres, périodiques en provenance de France sont disponibles sur place mais à un prix supérieur au prix français.</p>
<p>Les journaux locaux sont en français.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/vie-pratique/article/loisirs-et-culture-113563). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
