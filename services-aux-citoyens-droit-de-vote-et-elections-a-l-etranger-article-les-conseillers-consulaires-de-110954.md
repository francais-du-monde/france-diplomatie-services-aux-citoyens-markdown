# Les conseillers consulaires, de nouveaux représentants pour les Français de l’étranger

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/droit-de-vote-et-elections-a-l-etranger/article/les-conseillers-consulaires-de-110954#sommaire_1">Infographie "Des conseillers consulaires : pour quoi faire ?"</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/droit-de-vote-et-elections-a-l-etranger/article/les-conseillers-consulaires-de-110954#sommaire_2">Quel est le rôle de ces conseillers consulaires ?</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/droit-de-vote-et-elections-a-l-etranger/article/les-conseillers-consulaires-de-110954#sommaire_3">Comment sont-ils élus ? Pour combien de temps ?</a></li></ul>
<p><strong>Depuis 2014, les Français établis hors de France élisent de nouveaux représentants : les conseillers consulaires. </strong></p>
<p>Avant 2012, les Français de l’étranger étaient représentés par 12 sénateurs et 155 conseillers à l’Assemblée des Français de l’étranger. Lors des élections législatives de 2012, les Français établis hors de France ont pu, pour la première fois, élire 11 députés pour les représenter à l’Assemblée nationale.</p>
<p><strong>Depuis 2014, l’élection des conseillers consulaires complètera la représentation des Français de l’étranger par un dispositif de proximité.</strong></p>
<h3 class="spip"><a id="sommaire_1"></a>Infographie "Des conseillers consulaires : pour quoi faire ?"</h3>
<p class="spip_document_78570 spip_documents spip_documents_center">
<img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L660xH933/conseillers_consulaires_role_660_cle037b2c-d73de.jpg" width="660" height="933" alt='Infographie "Des conseillers consulaires : pour quoi faire ?"'></p>
<p class="document_doc">
<a class="spip_in" title="Doc:Version imprimable de l'infographie &quot;Des conseillers consulaires : pour quoi faire ?&quot; , 630.3 ko, 0x0" href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/conseillers_consulaires_role_PRINT_cle8a288c.pdf"><img width="18" height="21" alt="Doc:Version imprimable de l’infographie " des conseillers consulaires pour quoi faire src="http://www.diplomatie.gouv.fr/fr/plugins/fdiplo_squelettes/prive/vignettes/pdf.png" style="vertical-align: middle;"></a>
<a href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/conseillers_consulaires_role_PRINT_cle8a288c.pdf">Version imprimable de l’infographie "Des conseillers consulaires : pour quoi faire ?" - (PDF, 630.3 ko)</a>
</p>
<h3 class="spip"><a id="sommaire_2"></a>Quel est le rôle de ces conseillers consulaires ?</h3>
<p>Les conseillers consulaires sont des élus de proximité, <strong>représentant les Français établis hors de France auprès des ambassades et des consulats, au sein des conseils consulaires.</strong></p>
<p>Ils siègeront dans des conseils consulaires réunis sous la présidence du chef de poste diplomatique ou consulaire au moins deux fois par an, en fonction du volume de sujets à traiter. <strong>Les conseils consulaires participeront à la mise en place des politiques conduites pour les Français de l’étranger</strong> (enseignement, aides sociales, emploi et formation professionnelle, etc.).</p>
<p>Les 443 conseillers consulaires constitueront la majeure partie du collège électoral chargé d’élire les sénateurs des Français de l’étranger.</p>
<h3 class="spip"><a id="sommaire_3"></a>Comment sont-ils élus ? Pour combien de temps ?</h3>
<p>Les 443 conseillers consulaires seront <strong>élus au suffrage universel direct.</strong></p>
<p>Le nombre de conseillers consulaires sera variable en fonction du nombre de Français inscrits dans la circonscription consulaire concernée (il pourra varier de 1 à 9).</p>
<p>Les modalités de vote proposées pour ce scrutin seront le vote à l’urne, en personne ou par procuration, et le vote par internet.</p>
<p>Le mandat des conseillers consulaires sera de 6 ans, tout comme celui des élus à l’Assemblée des Français de l’étranger.</p>
<p><i>Mise à jour : février 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/droit-de-vote-et-elections-a-l-etranger/article/les-conseillers-consulaires-de-110954). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
