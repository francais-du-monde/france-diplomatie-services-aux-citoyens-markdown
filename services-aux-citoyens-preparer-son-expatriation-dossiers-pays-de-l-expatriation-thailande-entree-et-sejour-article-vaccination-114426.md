# Vaccination

<p>Aucune vaccination n’est exigée à l’entrée en Thaïlande pour les personnes en provenance de France. La vaccination contre la fièvre jaune est obligatoire pour les personnes en provenance d’une zone infectée.</p>
<p><strong>Vaccinations recommandées :</strong></p>
<p><strong>Adultes </strong> : mise à jour des vaccinations contre la diphtérie, le tétanos et la poliomyélite ; vaccinations contre la typhoïde, l’hépatite A, l’hépatite B.</p>
<p>Sont recommandées pour des séjours prolongés et/ou à risques : typhoïde, hépatite B, rage (situation d’isolement ou accès au traitement sur place difficile), encéphalite japonaise.</p>
<p>Veuillez également vérifier l’immunité antituberculeuse.</p>
<p><strong>Enfants </strong> : les enfants doivent mettre à jour toutes les vaccinations incluses dans le calendrier vaccinal français, en particulier pour les longs séjours : B.C.G. (dès le premier mois), rougeole dès l’âge de 9 mois.</p>
<p>Tous les vaccins commercialisés en France sont disponibles sur place. Il est conseillé néanmoins de réaliser toutes les vaccinations nécessaires avant de partir.</p>
<p>Pour en savoir plus, lisez notre article thématique <a href="services-aux-citoyens-preparer-son-expatriation-sante.md" class="spip_in">Santé</a>.</p>
<p><i>Mise à jour : juillet 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/entree-et-sejour/article/vaccination-114426). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
