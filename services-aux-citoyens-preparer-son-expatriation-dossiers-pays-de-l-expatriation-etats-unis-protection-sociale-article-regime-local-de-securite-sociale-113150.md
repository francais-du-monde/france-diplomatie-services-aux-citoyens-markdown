# Régime local de sécurité sociale

<p>Vous trouverez une présentation détaillée du système de sécurité sociale américain sur le site de notre partenaire, le <a href="http://www.cleiss.fr/" class="spip_out" rel="external">CLEISS</a> (Centre des liaisons européennes et internationales de sécurité sociale). En voici la table des matières :</p>
<p><a href="http://www.cleiss.fr/docs/regimes/regime_etatsunis.html#socialinsurance" class="spip_out" rel="external">La sécurité sociale au sens large "<i>Social Insurance</i></a></p>
<ul class="spip">
<li><a href="http://www.cleiss.fr/docs/regimes/regime_etatsunis.html#generalites" class="spip_out" rel="external">Généralités</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_etatsunis.html#obamacare" class="spip_out" rel="external">Obama Care</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_etatsunis.html#vs" class="spip_out" rel="external">Vieillesse et survivants ("Old Age and Survivors Insurance" - O.A.S.I.)</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_etatsunis.html#invalidite" class="spip_out" rel="external">Invalidité ("Disabled insurance" - D.I.)</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_etatsunis.html#medicare" class="spip_out" rel="external">Medicare</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_etatsunis.html#chomage" class="spip_out" rel="external">Chômage ("Unemployment Insurance" - U.I.)</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_etatsunis.html#adtmp" class="spip_out" rel="external">Accident du travail - maladies professionnelles</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_etatsunis.html#it" class="spip_out" rel="external">Incapacité temporaire ("State Temporary Disability Coverage")</a></li></ul>
<p><a href="http://www.cleiss.fr/docs/regimes/regime_etatsunis.html#programmes" class="spip_out" rel="external">Programmes d’assistance</a></p>
<ul class="spip">
<li><a href="http://www.cleiss.fr/docs/regimes/regime_etatsunis.html#ssi" class="spip_out" rel="external">Supplemental security income</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_etatsunis.html#afdc" class="spip_out" rel="external">Aide aux familles ayant des enfants à charge (A.F.D.C.)</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_etatsunis.html#medicaid" class="spip_out" rel="external">Assistance médicale (Medicaid)</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_etatsunis.html#fs" class="spip_out" rel="external">Tickets pour l’achat de nourrriture (Food Stamps)</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_etatsunis.html#ag" class="spip_out" rel="external">Assistance générale</a></li></ul>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/protection-sociale/article/regime-local-de-securite-sociale-113150). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
