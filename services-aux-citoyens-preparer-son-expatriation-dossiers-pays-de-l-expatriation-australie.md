# Australie

<p><strong>Au 31 décembre 2014</strong>, 22 539 Français étaient inscrits au registre des Français établis hors de France tenu par le consulat général de France à Sydney. Le nombre de Français non-inscrits est évalué à 55 000.</p>
<p>La communauté française installée en Australie est estimée à 75 000 personnes dont les deux tiers d’entre elles possèdent la double nationalité. Ces Français s’y sont installés lors des différentes vagues d’immigration qui se sont succédées depuis 1950. La communauté française est majoritairement composée de personnes résidentes (90 %) installées principalement dans les zones urbaines de Sydney, Melbourne et les autres capitales d’Etat. Les deux tiers de la communauté française résident en Nouvelle-Galles du Sud. Plus de 22 000 jeunes Français séjournent en Australie au titre du programme " visa vacances-travail " (VVT) instauré en 2004.</p>
<p>En 2011, avec 6,8 Mds AUD (5,04 Mds €) la France est le 11e investisseur direct étranger en Australie (part de 1,3%). Il y a 462 implantations françaises (220 sièges sociaux de filiales d’entreprises françaises et 237 établissements secondaires), qui emploient plus de 74 729 salariés (dont 1 021 expatriés français) et réalisent un chiffre d’affaires estimé à plus de 12 Mds €. La quasi-totalité des entreprises du CAC 40 est présente en Australie, marché qu’elles considèrent comme stratégique dans leur développement international (test de nouvelles formules commerciales ou de nouveaux produits, pays pilote pour des expériences managériales) et pour leur positionnement en Asie-Pacifique (projection sur le pourtour asiatique).</p>
<p>L’Australie, terre lointaine et encore préservée, fait beaucoup rêver. Connue aussi pour son dynamisme et son caractère cosmopolite, elle ne cesse d’attirer les Français qui sont chaque année plus nombreux à partir pour cette destination. Le nombre d’expatriés français a en effet presque doublé en quelques années. Avec le Canada, elle est la destination la plus prisée au monde. Cependant, si l’Australie mène toujours une politique ouverte d’attraction de main d’œuvre qualifiée, d’étudiants et de jeunes travailleurs, l’assouplissement de sa politique migratoire est actuellement remise en cause dans le débat politique.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/dossiers-pays/australie/" class="spip_in">Une description de l’Australie, de sa situation politique et économique</a></li>
<li><a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/australie/" class="spip_in">Des informations actualisées sur les<strong> conditions locales de sécurité </strong>en Australie</a></li>
<li><a href="http://www.ambafrance-au.org/" class="spip_out" rel="external">Ambassade de France en Australie</a></li></ul>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-australie-entree-et-sejour.md">Entrée et séjour</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-australie-entree-et-sejour-article-passeport-visa-permis-de-travail-108654.md">Passeport, visa, permis de travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-australie-entree-et-sejour-article-demenagement.md">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-australie-entree-et-sejour-article-vaccination-108656.md">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-australie-entree-et-sejour-article-animaux-domestiques.md">Animaux domestiques</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-australie-emploi-stage.md">Emploi, stage</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-australie-emploi-stage-article-marche-du-travail-108659.md">Marché du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-australie-emploi-stage-article-reglementation-du-travail.md">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-australie-emploi-stage-article-recherche-d-emploi.md">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-australie-emploi-stage-article-curriculum-vitae.md">Curriculum vitae</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-australie-emploi-stage-article-lettre-de-motivation-108663.md">Lettre de motivation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-australie-emploi-stage-article-entretien-d-embauche-108664.md">Entretien d’embauche</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-australie-emploi-stage-article-stages.md">Stages</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-australie-protection-sociale-22707.md">Protection sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-australie-protection-sociale-22707-article-regime-local-de-securite-sociale-108666.md">Régime local de sécurité sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-australie-protection-sociale-22707-article-convention-de-securite-sociale-108667.md">Convention de sécurité sociale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-australie-fiscalite-22708.md">Fiscalité</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-australie-fiscalite-22708-article-fiscalite-du-pays-108668.md">Fiscalité du pays</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-australie-fiscalite-22708-article-convention-fiscale-108669.md">Convention fiscale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-australie-vie-pratique.md">Vie pratique</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-australie-vie-pratique-article-logement.md">Logement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-australie-vie-pratique-article-sante.md">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-australie-vie-pratique-article-scolarisation.md">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-australie-vie-pratique-article-cout-de-la-vie-108673.md">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-australie-vie-pratique-article-transports.md">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-australie-vie-pratique-article-communications-108671.md">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-australie-vie-pratique-article-loisirs-et-culture.md">Loisirs et culture</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-australie-presence-francaise.md">Présence française</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-australie-presence-francaise-article-presence-francaise.md">Présence française</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
