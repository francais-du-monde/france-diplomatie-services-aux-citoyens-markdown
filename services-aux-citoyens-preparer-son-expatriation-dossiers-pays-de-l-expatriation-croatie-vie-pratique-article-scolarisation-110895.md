# Scolarisation

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/vie-pratique/article/scolarisation-110895#sommaire_1">Les établissements scolaires français en Croatie</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/vie-pratique/article/scolarisation-110895#sommaire_2">Enseignement supérieur</a></li></ul>
<p>Pour toute information sur la scolarisation dans le système français à l’étranger, vous pouvez consulter notre thématique sur les études et la scolarisation.</p>
<p>Vous y trouverez des renseignements sur :</p>
<ul class="spip">
<li>les établissements français du primaire et du secondaire à l’étranger ;</li>
<li>les bourses scolaires et la prise en charge des frais de scolarité à l’étranger ;</li>
<li>les possibilités qui s’offrent à vous si votre enfant ne peut être scolarisé à l’étranger dans le système français (enseignement à distance par le CNED, programme français langue maternelle (FLAM), internats en France) ;</li>
<li>les épreuves du baccalauréat à l’étranger ;</li>
<li>les bourses d’études supérieures en France et à l’étranger ;</li>
<li>l’équivalence des diplômes.</li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Les établissements scolaires français en Croatie</h3>
<p>L’<a href="http://www.efz.hr/" class="spip_out" rel="external">Ecole française de Zagreb</a> – Eurocampus accueille plus de cent élèves, de la maternelle à la fin du collège. Avec l’Ecole allemande de Zagreb, elle constitue un des cinq Eurocampus dans le monde. L’Eurocampus de Zagreb accueille des élèves de 16 nationalités différentes pour un enseignement en français, en allemand et en croate. Il contribue à faire de Zagreb une capitale européenne attractive.</p>
<p>Pour en savoir plus :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.aefe.fr/" class="spip_out" rel="external">Agence pour l’enseignement français à l’étranger</a>. </p>
<h3 class="spip"><a id="sommaire_2"></a>Enseignement supérieur</h3>
<p>Il est possible de suivre des études supérieures en Croatie.</p>
<p>Les élèves peuvent s’inscrire à l’université après l’examen d’entrée, un test de langue croate et l’établissement d’un certificat d’équivalence de diplôme secondaire délivré par le ministère de l’Education croate.</p>
<p>Il n’existe pas en Croatie de Grandes écoles, l’enseignement supérieur étant presque intégralement assuré par les universités. Pour les étudiants étrangers, les frais de scolarité annuels sont très variables selon les universités (de 1000 euros par an à la faculté des lettres de Zagreb à 6000 euros par an à la faculté d’économie de Zagreb). Les prix les plus élevés concernent les formations partiellement ou intégralement dispensées en anglais (en économie, et médecine, notamment).</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/vie-pratique/article/scolarisation-110895). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
