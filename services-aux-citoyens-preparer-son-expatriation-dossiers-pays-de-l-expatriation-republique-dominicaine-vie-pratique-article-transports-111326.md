# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/vie-pratique/article/transports-111326#sommaire_1">Importation de véhicule</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/vie-pratique/article/transports-111326#sommaire_2">Permis de conduire</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/vie-pratique/article/transports-111326#sommaire_3">Code de la route</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/vie-pratique/article/transports-111326#sommaire_4">Assurances et taxes</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/vie-pratique/article/transports-111326#sommaire_5">Achat et location</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/vie-pratique/article/transports-111326#sommaire_6">Immatriculation</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/vie-pratique/article/transports-111326#sommaire_7">Entretien</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/vie-pratique/article/transports-111326#sommaire_8">Réseau routier</a></li>
<li><a id="so_9" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/vie-pratique/article/transports-111326#sommaire_9">Transports en commun</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Importation de véhicule</h3>
<p>Il est interdit de faire importer un véhicule de plus de cinq ans. Pour le reste, aucune norme n’est exigée. Les droits de douane sont multiples et varient avec la cylindrée et la valeur réelle ou présumée du véhicule. Ils peuvent dépasser les 80 %.</p>
<p>Les particuliers doivent renouveler leur vignette automobile (« Marbete ») tous les ans : en 2014, elle coûte 1200 DOP pour les voitures de plus de cinq ans et 2200 DOP pour les autres.</p>
<h3 class="spip"><a id="sommaire_2"></a>Permis de conduire</h3>
<p>Le permis de conduire dominicain ne sanctionne aucune réelle formation. Le permis de conduire français (et non le permis l’international) est reconnu sur place durant trois mois.</p>
<p>Les résidents doivent échanger leur permis contre un permis local auprès de la <i>Dirección General de Tránsito Terrestre</i> (les délais sont extrêmement variables).</p>
<h3 class="spip"><a id="sommaire_3"></a>Code de la route</h3>
<p>La vitesse est limitée à 60 km/h en agglomération et à 80 km/h sur la route.</p>
<p>Cependant, force est de constater que le code de la route est très peu respecté. La République Dominicaine possède l’un des taux de mortalité sur les routes les plus élevés au monde.</p>
<p>Il est donc vivement conseillé d’être extrêmement prudent et vigilant au volant pour être en mesure d’anticiper les réactions des autres conducteurs.</p>
<h3 class="spip"><a id="sommaire_4"></a>Assurances et taxes</h3>
<p>Les véhicules ne sont pas toujours en bon état. L’existence d’une franchise d’assurance élevée peut inciter le loueur à réclamer une forte somme à son client pour des dommages matériels parfois très anciens.</p>
<p>En cas de location, l’assurance des dommages causés aux tiers est obligatoire.</p>
<p>Les primes d’assurances "tous risques" sont aussi élevées qu’en France, et il peut être intéressant de faire appel à un courtier pour sélectionner la solution la plus avantageuse.</p>
<p>Aucune statistique sur les prestations obtenues n’est disponible.</p>
<h3 class="spip"><a id="sommaire_5"></a>Achat et location</h3>
<p>Pratiquement toutes les marques sont importées mais représentées avec plus ou moins d’efficacité. Un véhicule tout terrain ou SUV est conseillé pour sa solidité et pour l’assurance qu’il peut donner dans le flux urbain. D’autre part, ce type de voiture se revend plus facilement.</p>
<p>On peut acheter sur place des voitures d’occasion, dans des garages ou auprès de particuliers.</p>
<h3 class="spip"><a id="sommaire_6"></a>Immatriculation</h3>
<p>La carte grise est remise avec les plaques minéralogiques. Elle s’obtient auprès de la <i>Dirección General de los Impuestos Internos</i>.</p>
<h3 class="spip"><a id="sommaire_7"></a>Entretien</h3>
<p>Il existe des garages fiables qui effectuent l’entretien régulièrement à moindres frais car, si les pièces peuvent revenir cher, la main d’œuvre est bon marché.</p>
<h3 class="spip"><a id="sommaire_8"></a>Réseau routier</h3>
<p>L’état général du réseau est bon et de nouvelles infrastructures routières sont construites en permanence.</p>
<p>A noter, l’ouverture de nouvelles autoroutes reliant Saint-Domingue à Las Terrenas ou encore à l’Est du pays (Punta Cana – Bávaro), et réduisant considérablement le temps de trajet depuis la capitale.</p>
<p><strong>Sécurité</strong></p>
<p>La prudence au volant est de rigueur. Le non respect du code de la route et une signalisation déficiente augmentent les risques d’accident. Il est conseillé, au volant, de verrouiller les portes de son véhicule, et cela, même en cas de stationnement de courte durée.</p>
<p>Il est déconseillé de conduire la nuit.</p>
<p><strong>En cas d’accident</strong></p>
<p>Les secours routiers, en cas d’accident, sont quasi inexistants. Les sanctions sont sévères et tout conducteur reconnu responsable d’un accident de circulation ayant entraîné des dommages corporels peut être détenu quelques jours au commissariat.</p>
<p>Il est conseillé, lors de la location du véhicule, de faire ôter par le loueur tout signe indiquant que le véhicule utilisé est loué (logo de la compagnie notamment), de ne présenter aux policiers qu’une copie du permis de conduire.</p>
<h3 class="spip"><a id="sommaire_9"></a>Transports en commun</h3>
<p><strong>Modes de transport préconisés localement</strong></p>
<p>En ville, l’usage du véhicule personnel est recommandé.</p>
<p>Il existe quelques autobus, de nombreux taxis collectifs (« guagua »), en mauvais état, bondés mais bon marché et des taxis individuels sans compteur, mais également bon marché (environ 150 – 200 pesos la course, soit 2,63 € - 3,50 €).</p>
<p>Il est conseillé d’utiliser les services de compagnies de taxis qui disposent de centres d’appel et permettent une identification.</p>
<p>Deux lignes de métro ont été inaugurées récemment (janvier 2009 pour la première et avril 2013 pour la seconde), mais le maillage n’est pas encore suffisant pour une utilisation intensive.</p>
<p>Les déplacements à l’intérieur du pays peuvent aussi s’effectuer par autocar : des lignes régulières desservent la province.</p>
<p>Il s’agit d’un moyen de transport relativement fiable, confortable et peu onéreux.</p>
<p>La liberté de circulation est totale, hors des zones militaires.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/vie-pratique/article/transports-111326). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
