# Fiscalité

<h2 class="rub22983">Fiscalité du pays</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/fiscalite-22983/#sommaire_1">Présentation</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/fiscalite-22983/#sommaire_2">Année fiscale</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/fiscalite-22983/#sommaire_3">Barème de l’impôt</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/fiscalite-22983/#sommaire_4">Quitus fiscal</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/fiscalite-22983/#sommaire_5">Solde du compte en fin de séjour</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/fiscalite-22983/#sommaire_6">Coordonnées des centres d’information fiscale</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Présentation</h3>
<h4 class="spip">Impôt sur les bénéfices des sociétés</h4>
<p>Les sociétés considérées comme ayant leur résidence à Chypre sont assujetties à l’impôt sur les bénéfices dégagés de leurs revenus obtenus soit à Chypre, soit à l’extérieur. Pour des sociétés non-résidentes, seuls sont imposables à Chypre les revenus tirés d’activités exercées à l’intérieur de l’île. Depuis 2013, le taux d’imposition est fixé à 12,5%. Les déficits antérieurs peuvent faire l’objet d’un report en avant sans limitation de durée.</p>
<p>Les établissements publics autonomes sont imposables au taux de 25%. Une taxe supplémentaire de 5% est applicable aux bénéfices des sociétés ou des établissements publics dès lors qu’ils dépassent un plafond fixé à environ 1 720 000 €.</p>
<h4 class="spip">Taxes retenues à la source</h4>
<h5 class="spip">Taxe sur la valeur ajoutée (T.V.A.)</h5>
<p>La T.V.A. a été portée à 19% à compter du 1er janvier 2014.</p>
<p>Ce taux de TVA est applicable aux importations et livraisons de produits et prestations de services qui ont lieu à Chypre, avec certaines exceptions :</p>
<ul class="spip">
<li>taux de 0% : exportations, livraisons d’aliments, commissions depuis l’étranger pour les services d’intermédiation concernant l’import/export depuis et vers Chypre, transport international aérien/maritime de biens et personnes, prestations de gestion de navires etc.,</li>
<li>taux réduit de 5% : livraisons d’aliments pour animaux, engrais, pesticides, boissons industrialisées, livres, journaux, revues, médicaments, vaccins etc.</li>
<li>taux réduit de 9% : restauration, hôtellerie, transport de passagers sur le territoire chypriote, transport maritime interne.</li></ul>
<p>Les personnes présentes plus de 183 jours, de façon cumulée au cours d’une année fiscale, sont considérées comme résidentes fiscales à Chypre.</p>
<h5 class="spip">Taxe exceptionnelle</h5>
<p>Une taxe exceptionnelle est appliquée à compter de 2012 et jusqu’en 2016, sur toutes les rémunérations brutes et les pensions de retraite des salariés, les bénéfices des travailleurs indépendants et sur l’ensemble des pensions de retraites du secteur public et privé.</p>
<h5 class="spip">Taxes spéciales</h5>
<ul class="spip">
<li>10% sur les redevances au titre de l’usage de droits de propriété intellectuelle, mais avec exemption dans le cas où ils auraient été accordés pour une zone géographique non circonscrite à Chypre.</li>
<li>5% sur la location de films.</li>
<li>10% sur les revenus tirés par des non-résidents d’activités artistiques, sportives (matches de football par exemple) ou de loisir.</li></ul>
<h4 class="spip">Autres impôts et taxes</h4>
<h5 class="spip">Cohésion sociale</h5>
<p>Il s’agit d’une contribution nouvelle pour alimenter le "fonds de cohésion sociale". Dans le secteur privé, elle est acquittée à moitié par les employeurs et à moitié par les employés ; les taux applicables sont :</p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Emoluments bruts mensuel / pensions de retraite €</td>
<td>Secteur public</td>
<td>Secteur privé</td></tr>
<tr class="row_even even">
<td><strong>2013</strong></td></tr>
<tr class="row_odd odd">
<td>Jusqu’à 2500</td>
<td>0</td>
<td>0</td></tr>
<tr class="row_even even">
<td>de 2501 à 3500</td>
<td>2,5%</td>
<td>2,5% (minimum €10)</td></tr>
<tr class="row_odd odd">
<td>de 3501 à 4500</td>
<td>3,0%</td>
<td>3,0%</td></tr>
<tr class="row_even even">
<td>à partir de 4501</td>
<td>3,5%</td>
<td>3,5%</td></tr>
<tr class="row_odd odd">
<td><strong>2014</strong></td></tr>
<tr class="row_even even">
<td>Jusqu’à 1500</td>
<td>0</td>
<td>0</td></tr>
<tr class="row_odd odd">
<td>de 1501 à 2500</td>
<td>2,5%</td>
<td>2,5% (minimum €10)</td></tr>
<tr class="row_even even">
<td>de 2501 à 3500</td>
<td>3,0%</td>
<td>3,0%</td></tr>
<tr class="row_odd odd">
<td>à partir de 3501</td>
<td>3,5%</td>
<td>3,5%</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_2"></a>Année fiscale</h3>
<p>L’exercice fiscal correspond à une année calendaire (1er janvier-31 décembre).</p>
<h3 class="spip"><a id="sommaire_3"></a>Barème de l’impôt</h3>
<h4 class="spip">Impôt sur le revenu des personnes physiques (I.R.P.P.)</h4>
<p>Les tranches d’imposition ont été révisées à la hausse.</p>
<p><strong>Barème d’imposition sur le revenu des personnes physiques en 2013</strong></p>
<table class="spip">
<thead><tr class="row_first"><th id="iddde1_c0">Montant imposable (en €)  </th><th id="iddde1_c1">%  </th><th id="iddde1_c2">Impôt maximum cumulé  </th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="iddde1_c0">de 0 à 19 500</td>
<td headers="iddde1_c1">0</td>
<td headers="iddde1_c2">0</td></tr>
<tr class="row_even even">
<td headers="iddde1_c0">de 19 501 à 28 000</td>
<td headers="iddde1_c1">20</td>
<td headers="iddde1_c2">1700</td></tr>
<tr class="row_odd odd">
<td headers="iddde1_c0">de 28 001 à 36 300</td>
<td headers="iddde1_c1">25</td>
<td headers="iddde1_c2">+2075 soit 3775 au total (1700+2075)</td></tr>
<tr class="row_even even">
<td headers="iddde1_c0">de 36 301 à 60 000</td>
<td headers="iddde1_c1">30</td>
<td headers="iddde1_c2">+ 7110 soit 10 885 au total (3775+7110)</td></tr>
<tr class="row_odd odd">
<td headers="iddde1_c0">à partir de 60 001</td>
<td headers="iddde1_c1">35%</td>
<td headers="iddde1_c2"></td></tr>
</tbody>
</table>
<p>Les époux sont imposés séparément en fonction de leurs revenus respectifs.</p>
<p>Les traitements et salaires font l’objet d’une <strong>retenue à la source</strong> prélevée par l’employeur sur la base du système PAYE (Pay As You Earn).</p>
<p>Les autres catégories de revenus encaissés durant l’année écoulée donnent lieu à déclaration avant le 30 avril, à l’exception du cas des personnes appelées à produire des états financiers, pour lesquels la date limite est reportée au 31 décembre. L’administration compétente est le ministère des Finances (<i>Ministry of Finances - Department of Inland Revenue</i>).</p>
<p>Un mécanisme de versements par tiers provisionnels à échéance les 1er août, 30 septembre et 31 décembre a été instauré. Au 1er août, il est procédé à un ajustement visant à solder la situation fiscale consolidée des contribuables au regard de leurs obligations au titre de l’exercice antérieur.</p>
<p>Le régime préférentiel accordé aux employés étrangers des sociétés <i>offshore</i> a été supprimé. Tous les résidents à Chypre (plus de 183 jours par an), quel que soit leur statut, sont désormais assujettis à l’I.R.P.P. selon le barème en vigueur.</p>
<h4 class="spip">Contribution spécifique à l’effort de défense nationale</h4>
<p>Cette taxe spéciale est appliquée sur les intérêts perçus sur les dépôts bancaires (à hauteur de 10%), les intérêts perçus et qui ne proviennent pas des activités courantes d’une société (30%) et sur les dividendes (à hauteur de 15%) .</p>
<h5 class="spip">Dividendes</h5>
<p>Les résidents bénéficiaires de dividendes provenant de sociétés établies à Chypre ou hors de Chypre, sont tenus d’acquitter une taxe de 17% sur ces revenus.</p>
<p>Une société résidente est réputée par l’administration fiscale avoir versé sous forme de dividendes 70% de ses bénéfices après impôt dans les deux ans à partir de la fin de l’exercice fiscal auquel ces bénéfices se rattachent ; le montant ainsi obtenu est imposable à 15% au titre de la contribution à l’effort de défense nationale.</p>
<p>Sont exonérés :</p>
<ul class="spip">
<li>les dividendes versés par une société résidente à une autre société résidente ;</li>
<li>sous certaines conditions, les dividendes perçus par une société résidente ou par une société non-résidente ayant un établissement stable à Chypre, lorsqu’ils proviennent d’une société non-résidente dont elle détient au moins 1% du capital ;</li>
<li>intérêts</li></ul>
<h5 class="spip">Intérêts</h5>
<p>Le taux d’imposition est de 30% retenus à la source. Sont exemptés les intérêts encaissés par des sociétés dans le cadre de leurs activités courantes.</p>
<p>Les intérêts sur les bons du Trésor et autres titres émis par l’Etat sont imposables au taux antérieur de 3%.</p>
<p>Les intérêts perçus par une société dans le cadre de l’exercice de ses activités courantes ne sont pas considérés comme tels au titre de la contribution à l’effort de défense nationale. Ils relèvent de l’imposition sur les bénéfices des sociétés.</p>
<h5 class="spip">Loyers</h5>
<p>Le taux est de 3% sur le montant des loyers bruts, après abattement de 25%.</p>
<h5 class="spip">Bénéfices des établissements publics autonomes ou semi-autonomes</h5>
<p>Ils sont assujettis à une contribution de 3%.</p>
<p><i>Source : l’essentiel d’un marché Chypre- éditions Ubifrance</i></p>
<h3 class="spip"><a id="sommaire_4"></a>Quitus fiscal</h3>
<p>Un quitus fiscal est exigé avant de quitter le pays.</p>
<h3 class="spip"><a id="sommaire_5"></a>Solde du compte en fin de séjour</h3>
<p>Un expatrié peut solder son compte en fin de séjour à Chypre.</p>
<h3 class="spip"><a id="sommaire_6"></a>Coordonnées des centres d’information fiscale</h3>
<p><a href="http://www.mof.gov.cy/mof/ird/ird.nsf/dmlindex_en/dmlindex_en?OpenDocument" class="spip_out" rel="external">Ministry of Finance - Inland Revenue Department</a><br class="manualbr">Michael Karaoli &amp; Gregori Afxentiou<br class="manualbr">1439 Nicosia<br class="manualbr">Cyprus<br class="manualbr">Tél. : +357 22602723</p>
<p><i>Mise à jour : janvier 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chypre-fiscalite-22983-article-convention-fiscale-111078.md" title="Convention fiscale">Convention fiscale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chypre-fiscalite-22983-article-fiscalite-du-pays-111077.md" title="Fiscalité du pays">Fiscalité du pays</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/fiscalite-22983/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
