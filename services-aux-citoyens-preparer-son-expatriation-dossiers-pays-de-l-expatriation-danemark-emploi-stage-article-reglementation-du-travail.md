# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/emploi-stage/article/reglementation-du-travail#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/emploi-stage/article/reglementation-du-travail#sommaire_2">Les règles fondamentales du marché du travail danois</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/emploi-stage/article/reglementation-du-travail#sommaire_3">Contrat de travail – spécificités</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/emploi-stage/article/reglementation-du-travail#sommaire_4">Fêtes légales</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/emploi-stage/article/reglementation-du-travail#sommaire_5">Création d’entreprises</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<h4 class="spip">La règlementation locale</h4>
<p>Les lois en matière de droit du travail sont peu nombreuses dans la législation danoise. Le marché du travail danois est principalement régi par des accords collectifs entre les partenaires sociaux ; l’Etat n’intervient dans ce processus qu’en dernier recours, quand tous les efforts de conciliation ont été épuisés. Le processus de négociation se déroule entre les principaux syndicats des travailleurs et ceux du patronat. Si les partenaires sociaux n’arrivent pas à un accord, un médiateur public intervient en proposant aux deux parties un arrangement à l’amiable. Au cas où cet arrangement n’est pas satisfaisant, le parlement danois peut intervenir en adoptant la motion de conciliation et en lui donnant force de loi.</p>
<p>Ce système de droit du travail remonte à la conclusion de la " Grande Convention " en 1899 (suite à un conflit social majeur). Cette même convention reconnaissait, sans en donner la définition exacte, les prérogatives patronales, affirmait le droit des partenaires sociaux à s’organiser et à mener des conflits sociaux et énonçait le devoir de paix sociale. Cette dernière clause précise que les arrêts de travail concertés sont contraires à la loi pendant la durée de la convention si l’objet du litige porte sur un aspect déjà mentionné dans l’accord collectif. Cet accord fut négocié entre les deux principaux syndicats : LO (<i>Landsorganisationen</i> : confédération des salariés) et DA (<i>Dansk Arbejdsgiverforening</i> : confédération du patronat), qui existent encore aujourd’hui et regroupent diverses entités plus petites. La " Grande Convention " est toujours d’actualité et a subi plusieurs modifications depuis lors. La dernière modification date de 1993 et précise qu’il est souhaitable que toutes les questions relatives aux conditions de travail et aux rémunérations soient déterminées par des accords collectifs.</p>
<p>Ainsi, la législation dans ce domaine, contrairement à la France, se limite à un petit nombre de lois adoptées, d’une part à l’initiative du <i>Folketing</i> (le parlement danois) et d’autre part suite aux directives communautaires (celles-ci tentent de limiter la décentralisation du processus de négociation collective). Ce système, communément dénommé " le modèle danois ", repose sur la confiance et le dialogue entre les parties en négociation. Le taux moyen de syndicalisation est très élevé au Danemark (près de 80% des salariés du secteur privé et 100% des salariés du secteur public). Les règles régissant le marché du travail sont assez uniformes étant donné que la majorité des conventions collectives adoptées dans les entreprises (on parle de " convention d’adhésion " quand l’employeur n’est pas affilié à une organisation patronale) sont très proches de l’accord de base entre DA et LO. Cet accord détermine, entre autre, les règles générales en cas de licenciement abusif, ainsi que celles qui protègent les délégués du personnel. Traditionnellement, ces conventions collectives sont conclues au niveau national pour une branche donnée et chaque convention collective précise la durée (de deux à quatre ans en général) et l’étendue des accords. Cependant, la tendance de ces vingt dernières années est plutôt à négocier des accords au niveau local (il est fréquent que les salaires soient négociés au sein des entreprises même) qui viennent compléter les accords généraux.</p>
<h3 class="spip"><a id="sommaire_2"></a>Les règles fondamentales du marché du travail danois</h3>
<p><i>Source : <a href="http://ec.europa.eu/eures/" class="spip_out" rel="external">EURES</a></i></p>
<h4 class="spip">Durée de travail</h4>
<p>La durée de travail est généralement de 37 heures hebdomadaires, du lundi au vendredi. A cet usage s’associe la " loi des onze heures " qui impose une période de repos de 11 heures consécutives entre deux jours de travail. En règle générale, les heures supplémentaires sont majorées de 50% pour les trois premières heures et de 100% pour les heures suivantes.</p>
<h4 class="spip">Congés annuels</h4>
<p>La loi danoise qui régit le droit aux congés annuels (Ferielov) précise que les salariés bénéficient de 25 jours ouvrables de congés payés par année de travail effectif (soit 2,08 jours par mois de travail effectif à terme échu), s’y ajoute cinq jours obtenus par la plupart des accords collectifs. Chaque convention collective établit le fonctionnement des congés annuels, il suffit de s’y référer. Une prime de vacances de 1% minimum du salaire annuel est versée lors du départ en congés. Ces dernières années, les négociations portent aussi sur une possible sixième semaine de congés payés.</p>
<h4 class="spip">Formation professionnelle</h4>
<p>Pour bénéficier du droit aux indemnités journalières pour la formation professionnelle, il faut avoir résidé, travaillé et cotisé à une caisse de chômage au Danemark pendant au moins un an. De plus, tout salarié doit avoir travaillé pendant au moins trois ans au cours des cinq années précédentes et doit obtenir l’accord préalable de son employeur. Le congé de formation s’adresse aux personnes de plus de 25 ans et peut s’étaler de 1 à 52 semaines. Il est rémunéré sur la base du taux maximum de l’allocation chômage.</p>
<p>Il existe plusieurs filières de formation professionnelle et seuls les « jobcenter » (l’équivalent danois des Pôles Emploi) sont compétents pour conseiller et orienter. La formation " ouverte ", organisée par les écoles professionnelles, les universités, les écoles…, offre aux adultes actifs comme aux chômeurs des programmes de formation à temps complet ou partiel.</p>
<h4 class="spip">Salaire minimum</h4>
<p>Le salaire mensuel minimum n’existe pas dans la législation danoise. La rémunération est fixée soit individuellement soit par le biais de conventions collectives. Il existe trois différents systèmes de minimum salarial : le traditionnel salaire standard qui est utilisé principalement dans le secteur public à destination des emplois peu rémunérés et peu qualifiés, le salaire minimum qui définit une base pécuniaire pour le personnel jeune et peu expérimenté, et le taux horaire qui, instauré depuis peu, commence à se substituer aux deux autres systèmes.</p>
<p>La base de rémunération varie en fonction de la convention collective applicable dans l’entreprise. Il peut donc exister des disparités au niveau de la rémunération selon le secteur d’activité, l’entreprise et la zone géographique. Néanmoins, il est possible de dégager des tendances assez homogènes. Le salaire horaire brut moyen varie généralement de 120 à 140 DKK.</p>
<h4 class="spip">Egalité des chances sur le marché du travail </h4>
<p>Depuis 1989, la législation danoise prescrit le salaire égal entre les hommes et les femmes (<i>Ligelonsloven</i>). Toute discrimination sexuelle dans l’attribution des fonctions ou dans les conditions de travail est interdite.</p>
<h3 class="spip"><a id="sommaire_3"></a>Contrat de travail – spécificités</h3>
<h4 class="spip">Les différents types de contrat</h4>
<p><i>Source : Eures</i></p>
<h5 class="spip">Le contrat de travail </h5>
<p>Un contrat de travail devrait toujours être conclu avec l’employeur et de préférence avant le premier jour de travail. Selon la loi, l’employeur est censé fournir au salarié un contrat de travail ou tout autre document écrit attestant de la relation professionnelle, et ce dans un délai maximum d’un mois après le commencement effectif du travail.</p>
<p>Au Danemark, pour tout type de prestation salariale, il faut se référer à la convention collective de branche applicable à l’entreprise. Les droits et les devoirs des salariés et des employeurs ainsi que les rémunérations à respecter, y sont ainsi détaillés. Généralement, la plupart des syndicats proposent des modèles standards de contrats de travail pour les employés qui ne seraient pas couverts par une convention collective. Ces modèles peuvent ainsi servir de base pour formuler un contrat individuel.</p>
<p>Il est fortement recommandé qu’un certain nombre de points figure dans le contrat de travail et tout changement dans les conditions de travail doit faire l’objet d’un document écrit remis par l’employeur à l’employé dans un délai maximum d’un mois après la date effective du changement.</p>
<p>Pour les contrats internationaux (il y a contrat international dès lors que la prestation de travail s’exécute à l’étranger), plusieurs modalités sont proposées :</p>
<p><strong>Le contrat local</strong> : le salarié est assujetti à la législation danoise. Il a en principe les mêmes droits et obligations que les autres salariés nationaux.</p>
<p><strong>Le détachement</strong> : un salarié déjà employé au sein de l’entreprise est envoyé en mission à l’étranger pour une période déterminée pour le compte de son employeur (souvent moins d’un an). Il signe deux contrats différents : un avec l’entreprise française et l’autre avec l’entreprise locale. Le détaché conserve ses liens avec la France (retraite, protection sociale…).</p>
<p><strong>La mise à disposition internationale</strong> : le salarié est mis à disposition d’une autre société du groupe (filiale ou société sœur). Un avenant est ajouté au contrat de travail et précise les conditions d’exécution du travail dans le pays d’accueil.</p>
<p><strong>L’expatriation </strong> : dans la plupart des cas, le salarié est recruté spécialement pour un poste à l’étranger ou s’il est déjà en poste peut y être envoyé pour une durée indéterminée (majoritairement de trois à cinq ans). Selon la formule choisie, le contrat de travail est soumis soit à la loi française, soit à la législation danoise, tout dépend s’il prend la forme d’un contrat local. En général, l’employeur prend en charge tous les frais liés à l’expatriation (scolarité des enfants, logement…) et peut offrir des avantages supplémentaires tels que des primes d’expatriation ou autres (des billets d’avion…etc.).</p>
<p>Pour la plupart des contrats mentionnés précédemment (hormis les contrats locaux), il est bon de vérifier que certains paramètres ne soient pas omis tels que :</p>
<ul class="spip">
<li>les avantages éventuellement accordés (logement de fonction, voiture de fonction…).</li>
<li>le régime de prévoyance, de chômage et de retraite, sans oublier les régimes de retraites complémentaires.</li>
<li>le cas échéant la prise en charge des frais de scolarité des enfants.</li>
<li>la prise en charge des dépenses de déménagement à l’aller et au retour sont à préciser. Il en va de même pour le mode de voyage et sa prise en charge (toujours aller-retour) pour l’intéressé et sa famille, plus les voyages en France à l’occasion des congés ou en cas de maladie grave ou de décès de l’intéressé ou d’un membre de sa famille.</li></ul>
<p><strong>Il faut veiller aussi à observer certaines clauses recommandées dans un contrat à l’international : </strong></p>
<ul class="spip">
<li>la reprise de l’ancienneté (surtout dans le cas d’une suspension de contrat)</li>
<li>la clause d’égalisation fiscale</li>
<li>l’intéressement et la participation</li>
<li>les conditions de réintégration du salarié</li></ul>
<p>Enfin, les contrats de travail peuvent contenir des clauses spécifiques qui peuvent, dans certains cas, limiter les futures possibilités professionnelles du salarié (clause de non concurrence, de résultats et autres). Il est donc fortement recommandé <strong>de bien étudier son contrat de travail avant de le signer.</strong></p>
<h4 class="spip">Les différentes formes de contrats de travail</h4>
<p>Au Danemark, les différentes formes de contrats sont assez similaires aux contrats français dans leur essence. Les règles du marché du travail sont assez homogènes, néanmoins il peut exister des disparités selon le secteur d’activité, l’entreprise, la région…etc. Le système de négociation collective a pour but d’adapter ces mêmes règles en fonction de tous ces paramètres et le nombre de lois dans ce domaine est très limité (un nombre important de ces lois sont le fruit de l’adaptation des directives européennes). C’est pourquoi il est préconisé, avant tout, de <strong>se reporter aux conventions collectives</strong> en vigueur pour toute question concernant les conditions de travail.</p>
<h5 class="spip">Le contrat à durée indéterminée</h5>
<p>C’est un contrat classique et comme son nom l’indique sans échéance. En général, une période initiale d’essai est fixée à 3 mois. Durant cette phase, chacune des deux parties peut rompre le contrat de travail à condition, pour l’employeur, de donner un préavis de 14 jours pendant les 3 premiers mois. Pour plus de précisions, il est fortement recommandé de se référer à la convention collective applicable. Une fois passée la période d’essai, l’embauche devient définitive. Ce contrat peut être à temps plein ou à temps partiel.</p>
<p>Le contrat à durée indéterminée est beaucoup plus fréquent au Danemark que le contrat à durée déterminée et ce en raison des règles de licenciement beaucoup plus souples qu’en France.</p>
<h5 class="spip">Le contrat à durée déterminée</h5>
<p>Ce contrat écrit a une durée préalablement fixée. Il peut s’effectuer sur la base d’un horaire à temps plein ou à temps partiel. Il peut être prorogé à son échéance.</p>
<p>Le salarié ayant un contrat à durée déterminée jouit des mêmes droits économiques et sociaux que les autres salariés, peu importe la nature du contrat. Tout comme les autres employés, il est soumis aux règles figurant dans la convention collective en vigueur dans la société ou la branche d’activité.</p>
<h5 class="spip">Le contrat temporaire (Intérim)</h5>
<p>Un contrat de travail temporaire associe trois partenaires : l’agence de travail temporaire, le travailleur intérimaire et une entreprise utilisatrice.</p>
<p>Le travailleur intérimaire conclut un contrat avec l’agence de travail temporaire et est mis à disposition d’une entreprise utilisatrice. Le travailleur temporaire exerce ses fonctions au sein de l’entreprise utilisatrice qui contrôle sa prestation de travail.</p>
<p>Il constitue en général une bonne opportunité pour le premier contact avec le poste occupé et peut déboucher sur une possibilité d’être embauché en contrat à durée indéterminée par l’entreprise qui a fait appel au travailleur intérimaire.</p>
<p>Au Danemark, ce type de contrat est particulièrement utilisé dans le secteur tertiaire.</p>
<h5 class="spip">Le contrat de travail à temps partiel</h5>
<p>Un salarié ayant un contrat à temps partiel effectue un horaire de travail inférieur à la durée hebdomadaire normale de travail. Au Danemark, la plupart des conventions collectives instaurent une durée moyenne de travail hebdomadaire de 37 heures (avec un maximum de 48 heures suite aux directives européennes). Cet horaire réduit peut être prédéterminé par des périodes comprises dans une semaine (moins de 37 heures par semaines), dans un mois (moins de 148 heures par mois), ou encore dans une année (moins de 1776 heures par an).</p>
<p>La politique du ministère danois pour l’emploi met un point d’honneur à favoriser la formation tout au long du parcours professionnel. Le Danemark est le deuxième pays européen (après la Finlande) dont les entreprises font de la formation des salariés une des priorités. On estime qu’une partie significative des effectifs d’une entreprise participe à des programmes de formation financés par le ministère de l’emploi, les entreprises et les salariés. Ceci explique que le Danemark dispose d’une main d’œuvre très qualifiée.</p>
<p>Cependant, il convient de nuancer ce propos car, à l’image de la France, les possibilités de formation des salariés sont la plupart du temps adressées au personnel encadrant. Ainsi, l’accès à la formation dépend en grande partie du niveau du poste, de la taille de l’entreprise, de la politique de l’entreprise en matière de carrières etc.</p>
<p><strong>Pour en savoir plus</strong> :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="https://www.workindenmark.dk/" class="spip_out" rel="external">Work in Denmark</a>.</p>
<h3 class="spip"><a id="sommaire_4"></a>Fêtes légales</h3>
<h4 class="spip">Jours chômés</h4>
<ul class="spip">
<li><strong>Janvier</strong> :
<br>— 1er janvier</li>
<li><strong>Mars</strong> :
<br>— Jeudi saint
<br>— Vendredi saint
<br>— Lundi de Pâques</li>
<li><strong>Avril</strong> :
<br>— Jour des prières Store Bedebag (4ème vendredi après Pâques)</li>
<li><strong>Mai</strong> :
<br>— Ascension
<br>— Pentecôte</li>
<li><strong>Juin</strong> :
<br>— Jour de la Constitution "Grundlovsdag" (fête nationale)</li>
<li><strong>Décembre</strong> :
<br>— Noël
<br>— lendemain de Noël</li></ul>
<h4 class="spip">Jours non chômés</h4>
<ul class="spip">
<li>16 avril : anniversaire de la reine Marguerite II</li>
<li>5 mai : reddition de l’armée allemande au Danemark en 1945</li></ul>
<h3 class="spip"><a id="sommaire_5"></a>Création d’entreprises</h3>
<p>Si vous entendez créer une entreprise au Danemark, certaines formalités sont requises :</p>
<ul class="spip">
<li>L’acquisition des permis nécessaires, et pour le management et pour les employés (permis de séjour et/ou de travail).</li>
<li>L’immatriculation au Danemark de la société.</li>
<li>L’enregistrement de la société auprès des autorités fiscales.</li>
<li>Il est recommandé d’ouvrir un compte bancaire.</li>
<li>Il est conseillé de consulter un expert-comptable.</li></ul>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.virk.dk/" class="spip_out" rel="external">Direction danoise des Industries et des Sociétés</a>.<br class="manualbr">Sur ce site vous trouverez en anglais tous les renseignements relatifs à la création de votre entreprise que vous pouvez également enregistrer en ligne.</li>
<li><a href="https://www.tresor.economie.gouv.fr/pays/danemark" class="spip_out" rel="external">Service économique de l’Ambassade de France au Danemark</a><br class="manualbr">Kongens Nytorv 4 <br class="manualbr">1050 Copenhague K,<br class="manualbr">Tél : (+45) 33 67 01 00</li>
<li><a href="http://export.businessfrance.fr/danemark/export-danemark-avec-notre-bureau.html" class="spip_out" rel="external">Business France au Danemark</a><br class="manualbr">Knabrostræde 3,2 TV. <br class="manualbr">1210 Copenhague K<br class="manualbr">Tél : (+45) 33 37 61 74</li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/emploi-stage/article/reglementation-du-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
