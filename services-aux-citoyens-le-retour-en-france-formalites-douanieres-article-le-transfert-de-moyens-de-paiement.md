# Le transfert de moyens de paiement

<p>A l’occasion de votre transfert de résidence en France vous devez :</p>
<ul class="spip">
<li>déclarer auprès de l’administration fiscale les comptes ouverts, utilisés ou clos à l’étranger ;</li>
<li>déclarer à la douane, lors de votre passage à la frontière, les transferts physiques des sommes (espèces ou chèques), titres (actions, obligations, etc.) ou valeurs en provenance de l’étranger, lorsque leur montant est égal ou supérieur à 10 000 euros (ou son équivalent en devises).</li></ul>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://douanes.gouv.fr/page.asp?id=79" class="spip_out" rel="external">Douanes.gouv.fr</a></p>
<p><i>Mise à jour : mars 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/formalites-douanieres/article/le-transfert-de-moyens-de-paiement). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
