# Etudier à distance

<h4 class="spip">Etudier à distance</h4>
<p>Si vous résidez à l’étranger, vous avez la possibilité de poursuivre des études en France en vous inscrivant directement auprès d’une université française possédant un service de télé-enseignement.</p>
<p>Le site internet <a href="http://www.formasup.fr/" class="spip_out" rel="external">formasup</a>  et celui de la <a href="http://www.fied.fr/fr/index.html" class="spip_out" rel="external">Fédération interuniversitaire de l’enseignement à distance (FIED)</a> référencent les établissements supérieurs (notamment les universités) proposant ce type de formation.</p>
<p><strong>Pour en savoir plus</strong> :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Le <a href="http://www.cned.fr/" class="spip_out" rel="external">CNED</a> (Centre National d’Enseignement à distance) propose également des formations de niveau supérieur à distance.</p>
<p><i>Mise à jour : février 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/etudes-superieures/s-informer-sur-les-etudes-a-l-etranger/article/etudier-a-distance). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
