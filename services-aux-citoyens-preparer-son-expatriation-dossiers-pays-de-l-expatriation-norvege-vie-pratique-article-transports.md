# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/norvege/vie-pratique/article/transports#sommaire_1">Importation de véhicule</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/norvege/vie-pratique/article/transports#sommaire_2">Permis de conduire</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/norvege/vie-pratique/article/transports#sommaire_3">Code de la route</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/norvege/vie-pratique/article/transports#sommaire_4">Assurances et taxes</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/norvege/vie-pratique/article/transports#sommaire_5">Achat et location</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/norvege/vie-pratique/article/transports#sommaire_6">Immatriculation</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/norvege/vie-pratique/article/transports#sommaire_7">Entretien</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/norvege/vie-pratique/article/transports#sommaire_8">Réseau routier</a></li>
<li><a id="so_9" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/norvege/vie-pratique/article/transports#sommaire_9">Transports en commun</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Importation de véhicule</h3>
<p>Si l’on vient en Norvège avec son propre véhicule, il faut s’acquitter d’une taxe en fonction de l’âge, de la puissance et du taux de pollution du véhicule (pour une voiture d’occasion achetée en France cela revient souvent au prix d’achat).</p>
<p>Vous pouvez <a href="http://www.dinside.no/104524/importkalkulator" class="spip_out" rel="external">calculer le prix d’importation</a> en ligne.</p>
<p>Aucun délai particulier n’est imposé pour réexporter ou revendre un véhicule acheté en Norvège. Il faudra cependant s’acquitter de taxes au moment de la revente : 55 % de taxes si le véhicule a été acheté entre deux et trois ans auparavant, 35 % de taxes si le véhicule a été acheté plus de trois ans auparavant.</p>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li><a href="http://toll.no/templates_TAD/Article.aspx?id=146861epslanguage=en" class="spip_out" rel="external">Site des douanes norvégiennes</a></li>
<li><a href="http://stavanger.accueil.free.fr/" class="spip_out" rel="external">Stavanger.accueil.free.fr</a></li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Permis de conduire</h3>
<p>Depuis 1996 la Norvège applique la réglementation européenne en matière de reconnaissance de permis de conduire et il n’est donc plus nécessaire d’échanger son permis français contre un permis norvégien.</p>
<h3 class="spip"><a id="sommaire_3"></a>Code de la route</h3>
<p>La conduite s’effectue à droite ; la priorité est à droite, à gauche aux ronds-points.</p>
<p>Les limitations de vitesse doivent être impérativement respectées : jusqu’à 100 km/h sur autoroute, 80 km/h sur route, 50 voire 30 km/h en agglomération. Les contrôles de vitesse et les radars sont fréquents. En cas d’infraction, les amendes sont perçues immédiatement par les policiers. L’alcoolémie est sévèrement sanctionnée à partir de 0,2g/l : amendes élevées, retrait du permis de conduire, voire peine de prison. Une infraction au stationnement, y compris le non-respect des cinq mètres à une intersection ou un passage pour piéton, coûte environ 500 NOK. Le port de la ceinture de sécurité est obligatoire à l’avant comme à l’arrière. Toute infraction à cette règle est passible d’une amende.</p>
<p>Les amendes pour le stationnement sont également élevées et sont gérées par des entreprises privées dont les agents travaillent de jour comme de nuit !</p>
<p><strong>Signalisation</strong> : la plus grande vigilance est recommandée lorsque des panneaux signalent la traversée d’animaux (moutons, rennes…).</p>
<p>Les feux de croisement doivent être allumés de jour comme de nuit. Les pneus d’hiver sont obligatoires du 1er novembre au 15 avril et sont autorisés en dehors de ces dates si les conditions météorologiques le justifient.</p>
<p>La Norvège compte environ 50 postes de péage, dont la plupart sont automatiques.</p>
<h3 class="spip"><a id="sommaire_4"></a>Assurances et taxes</h3>
<p>L’assurance au tiers est obligatoire. A titre d’exemple, une assurance "tous risques " revient de 7000 à 20 000 NOK (de 800 à 2260 €) en fonction du véhicule, du "bonus" de l’assuré et des résultats des négociations entreprises avec l’assureur.</p>
<p>Tous les véhicules âgés de plus de quatre ans doivent passer un contrôle technique tous les deux ans. Ces contrôles peuvent être effectués chez les garagistes, les concessionnaires ou centres NAF (réductions pour les membres Norges Automobil-Forbund).</p>
<p>L’immatriculation d’un véhicule en Norvège est assujettie à la preuve de l’assurance.</p>
<p>En cas d’accident, il est souhaitable d’établir un constat à l’amiable, en anglais.</p>
<h3 class="spip"><a id="sommaire_5"></a>Achat et location</h3>
<p>Pratiquement toutes les marques de voitures sont disponibles sur le marché local.</p>
<p>Compte tenu des taxes élevées (TVA, circulation, pollution), le prix d’un véhicule neuf peut se situer entre 100 et 125% de plus que son prix en France.</p>
<h3 class="spip"><a id="sommaire_6"></a>Immatriculation</h3>
<p>La procédure de mise aux normes norvégiennes (sécurité, pollution) étant compliquée et surtout onéreuse, on se renseignera sur le montant des réparations ou travaux à envisager, sur la taxe à acquitter avant d’importer son véhicule personnel (droits de douanes de 100 à 125% de la valeur du véhicule au moment de son importation). La preuve de l’acquittement des taxes de douanes est exigée lors de la revente d’un véhicule importé.</p>
<ul class="spip">
<li>Coût de la vignette auto : 2790 NOK pour une voiture normale en 2010. Tout propriétaire d’un véhicule doit s’acquitter du paiement annuel de la vignette. Contrairement au système français ou le montant de la vignette dépend du type du véhicule, en Norvège le tarif est unique. La vignette est payée un an à l’avance (l’année x on paye la vignette pour l’année x+1).</li>
<li>Coût de la carte grise, très variable : de 2000 à 22 000 NOK environ.</li></ul>
<p>Procédure d’obtention : <br class="autobr">Bureau de contrôle des véhicules <br class="manualbr">Biltilsynet à Oslo <br class="manualbr">Tél : 22 49 64 87.</p>
<h3 class="spip"><a id="sommaire_7"></a>Entretien</h3>
<p>On peut faire effectuer des réparations sur place pour un coût de main d’œuvre supérieur de 40% aux prix pratiqués en France pour une qualité de service rendu comparable. Les pièces nécessaires se trouveront au tarif norvégien d’importation.</p>
<p>Pour se procurer des pièces détachées de voitures de marques françaises il faudra compter deux semaines de délai en moyenne.</p>
<h3 class="spip"><a id="sommaire_8"></a>Réseau routier</h3>
<p>Le réseau routier, en bon état, ne laisse aucune ville inaccessible ; il est à noter que des routes de montagnes peuvent être fermées une partie de l’année (les périodes peuvent varier d’une année à l’autre). Les pneus d’hiver sont obligatoires du 1er novembre au 15 avril (15 mai dans les régions du nord).</p>
<p>Les routes sont essentiellement à une voie - dans chaque sens - sauf aux abords des principales villes. Il faut acquitter environ 20 NOK pour l’entrée dans Oslo, Bergen et Trondheim.</p>
<p>Les pistes encore nombreuses dans les campagnes et le nord du pays (seul 60 % du réseau est asphalté) sont souvent soumises à péage (10 à 20 NOK).</p>
<h3 class="spip"><a id="sommaire_9"></a>Transports en commun</h3>
<p>Oslo et sa périphérie sont desservies par un réseau de tramway et de métro. Un réseau de bus complète la desserte du tramway. Pour une course en taxi il faut compter environ 800 NOK pour le trajet ville/aéroport et 100 NOK environ pour une course en ville.</p>
<p>Dans les campagnes, et au nord après Bodø jusqu’à Kirkenes, l’autocar assure les liaisons internes.</p>
<p>Une cinquantaine d’aéroports et plusieurs compagnies aériennes maillent le territoire.</p>
<p>L’aéroport international d’Oslo (<i>Gardermoen</i>) se trouve à 50 km au nord de la ville. Un train-navette le relie à la capitale toutes les 20 à 25 minutes (170 NOK aller simple).</p>
<p>Le chemin de fer norvégien, à vocation plutôt touristique, relie les principales villes du pays et s’arrête à Bodø au nord.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.visitnorway.com/" class="spip_out" rel="external">Visitnorway.com</a></p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/norvege/vie-pratique/article/transports). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
