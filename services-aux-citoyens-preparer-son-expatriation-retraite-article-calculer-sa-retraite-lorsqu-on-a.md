# Calculer sa retraite lorsque l’on part travailler à l’étranger

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/retraite/article/calculer-sa-retraite-lorsqu-on-a#sommaire_1">Dans le cas d’un détachement</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/retraite/article/calculer-sa-retraite-lorsqu-on-a#sommaire_2">Dans le cas d’une expatriation</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Dans le cas d’un détachement</h3>
<p><strong>Vous êtes maintenus aux régimes obligatoires français. Vous continuez à relever du régime de la sécurité sociale française et </strong>votre période de travail effectuée à l’étranger sera validée pour la liquidation de votre retraite, comme si vous étiez resté en activité professionnelle en France.</p>
<h3 class="spip"><a id="sommaire_2"></a>Dans le cas d’une expatriation</h3>
<p><strong>1- Vous travaillez ou avez travaillé dans les différents Etats de l’Union européenne, la Suisse, le Liechtenstein, l’Islande ou la Norvège.</strong></p>
<p>Vos droits à pension de vieillesse seront déterminés de la manière suivante : chaque institution nationale d’assurance vieillesse calculera le montant de la pension nationale en fonction de la durée d’assurance dans son pays. Elle calculera également le montant de la pension théorique comme si toutes les périodes d’assurance avaient été accomplies dans son pays. Cette pension théorique sera réduite au prorata des seules périodes d’assurance effectivement accomplies dans le pays. Le montant ainsi déterminé est la pension proportionnelle. La plus élevée des deux pensions, pension nationale ou pension proportionnelle, vous sera alors attribuée. Vous recevrez directement de chacun des États votre pension de vieillesse.</p>
<p><strong>Attention</strong> : l’âge auquel il est possible d’obtenir une pension est différent selon les Etats et chacun fixe le point de départ de la retraite en fonction de cet âge.</p>
<p><strong>2- Vous travaillez ou avez travaillé dans un Etat ayant signé une convention avec la France</strong></p>
<p>Dans les conventions incluant l’assurance vieillesse, le mode de calcul de votre pension se fera :</p>
<ul class="spip">
<li>en totalisant vos périodes d’assurance et en les proratisant en fonction de la durée de travail effectué dans les deux Etats contractants ;</li>
<li>au choix, suivant ce premier système ou par liquidation séparée, si vous avez exercé votre activité en Croatie, au Gabon, dans les îles anglo-normandes, en Israël, en Macédoine, au Mali, en Mauritanie, au Niger, à Saint-Marin, au Sénégal ou au Togo ;</li>
<li>selon des dispositions identiques à celles figurant dans les règlements communautaires dans la plupart des autres pays liés à la France par une convention.</li></ul>
<p><strong>3- Vous travaillez ou avez travaillé dans un Etat qui n’est pas lié à la France par un accord de sécurité sociale</strong></p>
<p>Votre demande de retraite française n’entraine pas l’examen de vos droits à retraite dans l’autre pays.</p>
<p>Si vous avez cotisé dans le pays d’accueil, le calcul de votre retraite se fera en fonction de la législation locale.</p>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li><a href="http://www.cleiss.fr/pdf/guide_inforetraite.pdf" class="spip_out" rel="external">Information retraite des futurs expatriés</a> du <a href="http://www.cleiss.fr/" class="spip_out" rel="external">CLEISS</a></li>
<li>Site de la <a href="http://www.lassuranceretraite.fr/" class="spip_out" rel="external">CNAV</a></li>
<li>La rubrique <a href="services-aux-citoyens-preparer-son-expatriation-protection-sociale.md" class="spip_in">Protection sociale</a></li></ul>
<p><i>Mise à jour : mars 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/retraite/article/calculer-sa-retraite-lorsqu-on-a). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
