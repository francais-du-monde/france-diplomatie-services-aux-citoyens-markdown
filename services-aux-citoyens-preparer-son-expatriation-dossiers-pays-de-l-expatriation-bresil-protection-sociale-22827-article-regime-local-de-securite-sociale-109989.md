# Régime local de sécurité sociale

<p>Vous trouverez une présentation détaillée du système de sécurité sociale au Brésil sur le site de notre partenaire, le <a href="http://www.cleiss.fr/" class="spip_out" rel="external">CLEISS</a> (Centre des liaisons européennes et internationales de sécurité sociale). En voici la table des matières :</p>
<ul class="spip">
<li><a href="http://www.cleiss.fr/docs/regimes/regime_bresil.html#generalites" class="spip_out" rel="external">Généralités</a> (Organisation, financement, taux de cotisation sur les salaires…)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_bresil.html#financement" class="spip_out" rel="external">Financement</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_bresil.html#maternite" class="spip_out" rel="external">Assurance maladie maternité</a> (Prestations en nature et en espèces)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_bresil.html#atmp" class="spip_out" rel="external">Accident du travail et maladie professionnelle</a> (Régime indemnitaire, y compris pour les survivants)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_bresil.html#invalidite" class="spip_out" rel="external">Pension d’invalidité</a>, <a href="http://www.cleiss.fr/docs/regimes/regime_bresil.html#vieillesse" class="spip_out" rel="external">pension de vieillesse</a>, <a href="http://www.cleiss.fr/docs/regimes/regime_bresil.html#survivants" class="spip_out" rel="external">pension de survivants</a> (Pensions de vieillesse, d’invalidité, de survivant, aide sociale)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_bresil.html#pf" class="spip_out" rel="external">Prestations familiales</a> (Allocations familiales, de naissance, d’adoption…)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_bresil.html#assistance" class="spip_out" rel="external">Assistance sociale</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_bresil.html#chomage" class="spip_out" rel="external">Chômage</a> (Indemnités et assistance chômage)</li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/protection-sociale-22827/article/regime-local-de-securite-sociale-109989). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
