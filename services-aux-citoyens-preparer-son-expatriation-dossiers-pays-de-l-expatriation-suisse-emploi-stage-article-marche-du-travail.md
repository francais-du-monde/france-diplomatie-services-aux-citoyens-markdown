# Marché du travail

<p>Le taux de chômage moyen était de 3,1% en 2011. Toutefois, la répartition régionale du chômage montre d’importantes disparités. Ainsi, en 2011, le taux de chômage de la Suisse latine atteignait 4,6%, contre 2,5% en Suisse alémanique. Le chômage frappe près de trois fois plus la population étrangère (6,2%) que les Suisses (2,2%).</p>
<p>En 2011, la Suisse comptait 4,632 millions d’actifs avec une participation des personnes âgées (76%) relativement élevée. La tendance à l’augmentation du nombre de retraites anticipées, qu’on observait encore il y a quelques années, s’est affaiblie. Toujours plus de personnes en Suisse restent actives au-delà de l’âge légal de la retraite. Les travailleurs âgés font ce choix surtout lorsque le travail ménage la santé et que les conditions d’engagement et l’ambiance de travail sont bonnes. Néanmoins, la majorité des employeurs ne jugent utile et nécessaire d’employer des collaborateurs âgés que pour certaines fonctions et ne pratiquent pas de façon systématique une politique du personnel favorisant l’emploi des seniors.</p>
<p>Le <strong>travail à temps partiel</strong> est également plus répandu que dans la plupart des pays de l’UE (14% chez les hommes et 58,6% chez les femmes).</p>
<p>Le temps de travail hebdomadaire, d’une durée de 41,7 heures, dépasse d’une bonne heure la moyenne de l’UE.</p>
<p>71% des salariés sont occupés dans le secteur des services, 25% dans le secteur secondaire et environ 4% dans l’agriculture. La Suisse apparaît un peu plus axée sur les services que la moyenne de l’UE.</p>
<p>La <strong>population étrangère</strong> revêt une grande importance pour le marché suisse de l’emploi. Les ressortissants étrangers représentent plus de 27% de la population active permanente (1,25 million de personnes). Environ deux tiers sont des ressortissants de pays de l’UE. Les plus nombreux sont les ressortissants italiens et ceux des pays des Balkans occidentaux (près de 4% de la population active chacun), suivis des Allemands (2,7%) et des Portugais (2,6%). En plus des étrangers résidant de manière permanente, la Suisse accueille plus de 46000 travailleurs en séjour de courte durée et plus de 248000 travailleurs frontaliers, pour la plupart originaires des pays voisins.</p>
<p>Le nombre des migrants exerçant une activité lucrative a fortement augmenté au cours des dernières années. C’est particulièrement le cas des ressortissants de pays de l’UE qui, depuis 2002 et l’application progressive de l’accord sur la libre circulation des personnes, sont de plus en plus nombreux à venir travailler en Suisse (d’Allemagne et du Portugal, notamment).</p>
<p>Sources utilisées (OFS/Seco/CS)</p>
<p><strong>Pour en savoir plus</strong> :</p>
<p><a href="http://www.frontalier.org/index.htm" class="spip_out" rel="external">Site internet des travailleurs frontaliers</a> (les frontaliers nécessitent un <a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-suisse-entree-et-sejour-article-passeport-visa-permis-de-travail-105239.md" class="spip_in">permis de travail</a>).</p>
<p><strong>Les secteurs à fort potentiel</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span> <strong>Secteur médical</strong>
<br>— Infirmiers(ères) généralistes ou spécialisé(e)s en gériatrie et psychiatrie ; anesthésistes ;
<br>— instrumentistes.<br class="manualbr">Les besoins dans ce secteur sont si importants que les établissements de soins suisses, lorsqu’ils n’arrivent plus à recruter parmi les ressortissants de l’Union européenne, vont chercher du personnel infirmier extracommunautaire qu’ils forment.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span> <strong>Industrie horlogère</strong><br class="manualbr">Ce secteur a connu récemment des années exceptionnelles et a créé beaucoup d’emplois. Les besoins en spécialistes sont toujours très élevés. En effet, en dépit de la crise actuelle, et pour faire face à l’augmentation des exportations (+15,4% en juillet 2012), les entreprises horlogères comptent former ou embaucher plus de 3 000 collaborateurs, d’ici 2016 (horlogers, monteurs, spécialistes de complications, etc.). Les postes sont à pourvoir principalement dans les cantons du Jura, Neuchâtel, Berne et Genève.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span> <strong>Industrie chimique et pharmaceutique</strong><br class="manualbr">L’industrie chimique et pharmaceutique suisse est la deuxième branche industrielle de Suisse. Branche en plein essor, l’industrie chimique et pharmaceutique est un des plus grands employeurs industriels en Suisse. Elle se compose d’entreprises aux dimensions les plus variées, présentes aux quatre coins de la Suisse.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span> <strong>Informatique / Téléphonie</strong><br class="manualbr">Les besoins en personnel qualifié sont traditionnellement importants (Microsoft, Swisscom, Sunrise…).</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span> <strong>Conseil</strong><br class="manualbr">Consultants, services aux entreprises.<br class="manualbr">Les " big 4 " (PwC, Ernst  Young, KPMG et Deloitte) recrutent régulièrement des spécialistes.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span> <strong>Agroalimentaire</strong><br class="manualbr">Nestlé, COOP, Migros, Lidl etc. offrent de nombreux emplois.</p>
<p><strong>Les secteurs à faible potentiel</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span> <strong>Banque et finance</strong><br class="manualbr">D’après une enquête menée par Ernst Young, les bouleversements qui affectent la finance suisse depuis la crise de 2008 l’encouragent à tailler radicalement dans ses dépenses ; plus pour les banques que pour les assureurs : nombreuses suppressions de postes attendues.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span> <strong>Hôtellerie et restauration</strong><br class="manualbr">Contrairement aux idées reçues, le secteur recherche principalement des employés qualifiés et des cadres et pratiquement pas d’employés sans qualification. C’est par ailleurs l’un des secteurs d’activité qui propose la rémunération moyenne la plus faible (4106 CHF bruts mensuels selon l’Office fédéral de la statistique).</p>
<h4 class="spip">Barèmes de rémunération</h4>
<p><strong>Détermination du salaire</strong></p>
<p>La question du salaire est d’autant plus primordiale qu’en général les travailleurs français qui postulent pour la première fois pour un emploi en Suisse ignorent les " prix " du marché.</p>
<p>En Suisse, le salaire brut moyen s’élevait, en 2010, à 5979 CHF, toutes fonctions et tous secteurs confondus (source : Office fédéral de la statistique rubrique Thèmes / Travail, rémunération).</p>
<p>Il existe néanmoins des différences significatives en fonction :</p>
<ul class="spip">
<li>du secteur d’activité : par exemple, le secteur bancaire et financier était celui qui payait en moyenne le mieux ses employés (environ 9357 CHF bruts mensuels) et le secteur de l’hôtellerie et de la restauration l’un qui payait le moins bien (environ 4106 CHF)</li>
<li>des cantons : en moyenne, les cantons de la région de Zurich sont ceux qui rémunèrent le mieux leurs employés. Les salaires les plus bas se trouvent dans le canton du Tessin.</li></ul>
<p>Pour déterminer une fourchette de salaire qu’il est possible de demander, il est recommandé de se rendre sur le site <a href="http://www.lohnrechner.bfs.admin.ch/" class="spip_out" rel="external">salarium</a> qui permet de calculer son salaire en fonction de certains paramètres : âge, expérience, profession, formation, sexe…</p>
<p><strong>Calculateurs de salaire</strong></p>
<ul class="spip">
<li><a href="http://www.geneve.ch/ogmt/" class="spip_out" rel="external">Observatoire genevois</a> du marché du travail</li>
<li><a href="http://www.lohnrechner.ch/" class="spip_out" rel="external">Union syndicale suisse</a></li>
<li><a href="http://www.fr.ch/sstat/fr/pub/calculateur_des_salaires.htm" class="spip_out" rel="external">Service de la statistique</a> du canton de Fribourg</li></ul>
<p><strong>Exemples de salaires (à titre indicatif)</strong></p>
<p>Les exemples de salaires ci-dessous sont des salaires bruts mensuels (1/12ème du 13ème mois compris) pour une durée hebdomadaire de 42h. Ils peuvent varier en fonction du profil du travailleur, du secteur d’activité et du canton :</p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Emploi</td>
<td>Genève</td>
<td>Berne</td>
<td>Zürich</td></tr>
<tr class="row_even even">
<td>Ingénieur recherche et développement (cadre débutant en chimie) :</td>
<td>8453 CHF</td>
<td>7 922 CHF</td>
<td>8358 CHF</td></tr>
<tr class="row_odd odd">
<td>Serveur</td>
<td>5203 CHF</td>
<td>4984 CHF</td>
<td>5223 CHF</td></tr>
<tr class="row_even even">
<td>Secrétaire</td>
<td>5755 CHF</td>
<td>5306 CHF</td>
<td>5577 CHF</td></tr>
<tr class="row_odd odd">
<td>Puéricultrice</td>
<td>4500 CHF</td>
<td></td>
<td></td></tr>
<tr class="row_even even">
<td>Infirmière</td>
<td>6599 CHF</td>
<td>6357 CHF</td>
<td>6732 CHF</td></tr>
</tbody>
</table>
<p><i>Mise à jour : février 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/emploi-stage/article/marche-du-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
