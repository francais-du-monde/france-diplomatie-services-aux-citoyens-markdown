# Passeport, visa, permis de travail

<p>Quel que soit le motif de votre séjour, renseignez-vous auprès de la section consulaire de l’<a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-etrangers-en-france/article/annuaire-des-representations-etrangeres-en-france" class="spip_in">ambassade du Ghana à Paris</a> qui vous informera sur la règlementation en matière d’entrée et de séjour au Ghana, règlementation que vous devrez impérativement respecter.</p>
<p>L’Ambassade du Ghana à Paris ne possède pas de site internet.</p>
<p>N’hésitez pas à consulter le site du <a href="http://www.ghanaimmigration.org/" class="spip_out" rel="external">Ghana Immigration Service</a> afin de vous familiariser avec les modalités d’entrée et de séjour dans le pays.</p>
<p>Pour une installation professionnelle, c’est votre employeur (ou futur employeur) qui prendra le relais. <strong>En aucun cas, vous n’êtes autorisé à travailler au Ghana sans permis adéquat.</strong></p>
<h4 class="spip">Court séjour </h4>
<p>Un visa de court séjour doit être demandé à l’ambassade du Ghana à Paris avant le départ. Les autorités ghanéennes ne délivrent pas de visa à l’arrivée au Ghana sauf aux personnes arrivant d’un pays où il n’y a pas d’ambassade/consulat du Ghana et qui ont préalablement prévenu les services ghanéens de l’immigration. Coût indicatif de ce visa à l’arrivée : 150 dollars.</p>
<p>L’obtention du visa court séjour se fait par comparution personnelle ou par correspondance depuis la province. A titre indicatif le tarif d’un visa de court séjour d’un mois est de 50 euros.</p>
<h4 class="spip">Long séjour</h4>
<p>Une fois sur place, pour un séjour de plus de trois mois, un permis de séjour et un permis de travail sont obligatoires. Ceux-ci doivent faire l’objet d’une demande auprès du <a href="http://www.ghanaimmigration.org/" class="spip_out" rel="external">Ghana Immigration Service</a>. La section consulaire de l’Ambassade de France au Ghana n’est pas l’organisme compétent pour répondre à vos demandes d’information concernant votre long séjour au Ghana.</p>
<p>Par ailleurs, vous devrez impérativement prendre l’attache de la National Identification Authority qui vous délivrera une carte d’identité pour étrangers (« Non-citizen Ghanacard ») Cette démarche est obligatoire. Elle vous permettra entre autres démarches de faire la demande du permis de conduire ghanéen. L’obtention de cette carte est payante : 120 USD ou l’équivalent en cedis ghanéens.</p>
<p>Vous pouvez consulter le site du <a href="http://www.fims.org.gh/" class="spip_out" rel="external">Foreign Management Identification System</a> pour plus d’informations sur cette démarche ou contacter cet organisme au (0501) 267262 ou (0501) 267280.</p>
<p>Vous pouvez également vous rendre sur le site de l’<a href="http://www.ambafrance-gh.org/" class="spip_out" rel="external">Ambassade de France</a> / rubrique « Enregistrement des ressortissants étrangers ».</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/entree-et-sejour/article/passeport-visa-permis-de-travail-110977). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
