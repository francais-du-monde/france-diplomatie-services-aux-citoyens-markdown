# Vie pratique

<h2 class="rub22984">Logement</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/vie-pratique/#sommaire_1">Conditions de location</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/vie-pratique/#sommaire_2">Loyers</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/vie-pratique/#sommaire_3">Hôtels</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/vie-pratique/#sommaire_4">Electricité</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/vie-pratique/#sommaire_5">Electroménager</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Conditions de location</h3>
<p>Les quartiers résidentiels se situent dans le proche sud-ouest (Ayios Andreas/ Ayios Dometios/ Ayios Pavlos), au sud-ouest (Makedonitissa, Parissinos) et au sud de la vieille ville (Acropoli, Dassoupoli).</p>
<h3 class="spip"><a id="sommaire_2"></a>Loyers</h3>
<p>En règle générale, les loyers ne comprennent pas les charges. La durée des baux est souvent de deux ans, renouvelables. L’augmentation du loyer au bout de deux ans est négociable dès le premier contact et se situe en moyenne à 7%. Le loyer est payable d’avance, d’un à trois mois. Les agences sont nombreuses et le marché locatif varié, à l’exception des studios pour lesquels il n’existe pas d’offre. La commission d’agence est, en principe, versée par le propriétaire.</p>
<p>Les prix des loyers peuvent varier considérablement d’une maison ou d’un appartement à un autre. Tout dépend de la prestation offerte (avec ou sans jardin, vue, cuisine moderne, piscine, etc.) et de l’état général du logement.</p>
<h3 class="spip"><a id="sommaire_3"></a>Hôtels</h3>
<p><strong>Prix moyen d’une chambre d’hôtel, petit déjeuner inclus (à titre indicatif) :</strong></p>
<ul class="spip">
<li>Hôtel 2 à 3 étoiles (Larnaca) : 40 à 50 €</li>
<li>Hôtel 2 à 3 étoiles (Nicosie) : 70 à 80 €</li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Electricité</h3>
<p>L’alimentation électrique se fait sur du courant 240 V, avec des prises aux normes britanniques. Il est donc nécessaire de se munir d’un adaptateur pour les appareils électriques importés de France.</p>
<p>Pour en savoir plus :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.visitcyprus.com/wps/portal/getting_to_cyprus/practical_info/%21ut/p/c5/04_SB8K8xLLM9MSSzPy8xBz9CP0os3hXN0fHYE8TIwN3A2MDAyNXI2cPE48gA28jA6B8JG75YCMCusNB9pnFG-AAjgYQeXzmo8hbGHpZAOW9Tb28jEyMDC2M9P088nNT9QtyIwwyA9IVAR2e0gg%21/dl3/d3/L2dJQSEvUUt3QS9ZQnZ3LzZfRUZBQVNJNDIwMDJROTAyMTAySlZOMzIySDc%21/" class="spip_out" rel="external">Office de tourisme de Chypre</a> </p>
<h3 class="spip"><a id="sommaire_5"></a>Electroménager</h3>
<p>S’agissant de matériel électroménager, tout l’équipement est disponible sur place.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chypre-vie-pratique-article-loisirs-et-culture.md" title="Loisirs et culture">Loisirs et culture</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chypre-vie-pratique-article-communications.md" title="Communications">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chypre-vie-pratique-article-transports.md" title="Transports">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chypre-vie-pratique-article-cout-de-la-vie-111082.md" title="Coût de la vie">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chypre-vie-pratique-article-scolarisation.md" title="Scolarisation">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chypre-vie-pratique-article-sante.md" title="Santé">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chypre-vie-pratique-article-logement-111079.md" title="Logement">Logement</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/vie-pratique/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
