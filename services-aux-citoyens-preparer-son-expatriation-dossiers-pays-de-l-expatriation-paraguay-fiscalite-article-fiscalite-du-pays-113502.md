# Fiscalité du pays

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/paraguay/fiscalite/article/fiscalite-du-pays-113502#sommaire_1">Présentation</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/paraguay/fiscalite/article/fiscalite-du-pays-113502#sommaire_2">Année fiscale</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/paraguay/fiscalite/article/fiscalite-du-pays-113502#sommaire_3">Quitus fiscal</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/paraguay/fiscalite/article/fiscalite-du-pays-113502#sommaire_4">Solde du compte en fin de séjour</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Présentation</h3>
<p><strong>Revenus personnels</strong></p>
<p>Depuis 2012 (Décret 9.371/2012 du 30 juillet 2012), les personnes physiques et certaines sociétés sont soumises à l’Impôt sur le revenu, plus connu sous le nom d’IRP (Impuesto a la Renta Personal).</p>
<p>Sont imposables : les prestataires de services, les métiers de la fonction publique et les gains issus de la vente occasionnelle de biens immobiliers.</p>
<p>Le taux d’imposition sur le salaire net imposable est de 10% pour les revenus supérieurs à 10 salaires minimums mensuels, de 8% pour les revenus égaux ou inférieurs à 10 salaires minimums mensuels, de 20% pour les personnes ne résidant pas au Paraguay mais percevant des revenus dans le pays.</p>
<p>Sont déductibles :</p>
<ul class="spip">
<li>les dons à l’Etat, aux communes, aux entités religieuses reconnues par la loi, aux entités éducatives, sociales, culturelles ;</li>
<li>pour les personnes physiques, les dépenses effectuées à l’étranger en relation avec l’activité professionnelle ; les dépenses et investissements réalisés et justifiés ; les cotisations à la sécurité sociale ;</li>
<li>pour les personnes ne cotisant pas à la sécurité sociale, 15% du salaire ;</li>
<li>pour certaines sociétés, tous les investissements en relation avec l’obtention de revenus imposables.</li></ul>
<p>Sont exonérés :</p>
<ul class="spip">
<li>les pensions des anciens combattants ;</li>
<li>les indemnités des membres des corps diplomatique et consulaire ;</li>
<li>les indemnités perçues pour cause de décès ou de maladie ;</li>
<li>les pensions de retraite ;</li>
<li>les intérêts, commissions ou rendements de capitaux des entités bancaires et financières.</li></ul>
<p><strong>Impôt sur les sociétés</strong></p>
<p>Tout revenu découlant d’une activité commerciale, de production ou de service est assujetti au régime fiscal. L’impôt sur les bénéfices des sociétés est de 30% et de 25% pour les revenus des activités agricoles. Il existe par ailleurs une législation particulière pour le secteur des mines et du pétrole.</p>
<p><strong>Taxe sur la valeur ajoutée</strong></p>
<p>L’impôt sur la valeur ajoutée (IVA) a été introduit au Paraguay en 1992 et constitue l’une des principales sources de revenu de l’Etat.</p>
<p>Le taux appliqué aux produits fabriqués localement ou importés est de 10%. Des dispositions particulières peuvent toutefois s’appliquer à certains produits, comme l’exonération des ventes de produits agricoles.</p>
<p>Par ailleurs, l’impôt sélectif à la consommation (ISC) est appliqué à certains produits importés (cigarettes, boissons alcoolisées, alcools et combustibles dérivés du pétrole). Le taux de cet impôt varie de 5 à 50% selon les produits.</p>
<p><strong>Droits de douane</strong></p>
<p>Les droits de douane sont applicables à l’importation de biens et services.</p>
<p>Leur taux varie de 0 à 20% selon la nature des produits.</p>
<h3 class="spip"><a id="sommaire_2"></a>Année fiscale</h3>
<p>L’année fiscale au Paraguay, comme l’année comptable, correspond à l’année civile.</p>
<h3 class="spip"><a id="sommaire_3"></a>Quitus fiscal</h3>
<p>Il n’est pas exigé de quitus fiscal avant de quitter le Paraguay.</p>
<h3 class="spip"><a id="sommaire_4"></a>Solde du compte en fin de séjour</h3>
<p>Un expatrié français peut solder son compte en fin de séjour.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/paraguay/fiscalite/article/fiscalite-du-pays-113502). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
