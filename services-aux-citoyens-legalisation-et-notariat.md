# Légalisation et notariat

<p class="spip_document_84791 spip_documents spip_documents_center">
<img src="http://www.diplomatie.gouv.fr/fr/IMG/jpg/rubrique_legalisation_cle4fd18e-1.jpg" width="660" height="140" alt=""></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-legalisation-et-notariat-legalisation-et-certification-de-signatures.md">Légalisation et certification de signatures</a></li>
<li><a href="services-aux-citoyens-legalisation-et-notariat-legalisation-et-certification-de-signatures-article-la-legalisation-de-documents-publics-francais-destines-a-une-autorite-etrangere.md">La légalisation de documents publics français destinés à une autorité étrangère</a></li>
<li><a href="services-aux-citoyens-legalisation-et-notariat-legalisation-et-certification-de-signatures-article-autorites-competentes-et-nature.md">Autorités compétentes et nature des documents</a></li>
<li><a href="services-aux-citoyens-legalisation-et-notariat-legalisation-et-certification-de-signatures-article-cout-de-la-legalisation.md">Coût de la légalisation</a></li>
<li><a href="services-aux-citoyens-legalisation-et-notariat-legalisation-et-certification-de-signatures-article-conditions-de-recevabilite-des.md">Conditions de recevabilité des actes publics par le bureau des légalisations</a></li>
<li><a href="services-aux-citoyens-legalisation-et-notariat-legalisation-et-certification-de-signatures-article-legalisation-par-correspondance.md">Légalisation par correspondance</a></li>
<li><a href="services-aux-citoyens-legalisation-et-notariat-legalisation-et-certification-de-signatures-article-principaux-cas-particuliers.md">Principaux cas particuliers</a></li>
<li><a href="services-aux-citoyens-legalisation-et-notariat-legalisation-et-certification-de-signatures-article-adresses-et-liens-utiles.md">Adresses et liens utiles</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-legalisation-et-notariat-actes-notaries-a-l-etranger.md">Actes notariés à l’étranger</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/legalisation-et-notariat/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
