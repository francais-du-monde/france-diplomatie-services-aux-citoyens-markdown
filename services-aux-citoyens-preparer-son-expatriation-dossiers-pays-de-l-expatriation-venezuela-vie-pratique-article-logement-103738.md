# Logement

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/vie-pratique/article/logement-103738#sommaire_1">Où se loger ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/vie-pratique/article/logement-103738#sommaire_2">Conditions de location </a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/vie-pratique/article/logement-103738#sommaire_3">Hôtels</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Où se loger ?</h3>
<p>A Caracas, les quartiers résidentiels se trouvent au nord-est de la ville : Los Palos Grandes, Altamira, La Castellana, Country club…</p>
<table class="spip" summary="">
<caption>Coût locatif moyen par type d’habitation et de quartier en euros</caption>
<thead><tr class="row_first"><th id="idfa62_c0">Type de logement</th><th id="idfa62_c1">Quartiers résidentiels </th><th id="idfa62_c2">Très bons quartiers</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="idfa62_c0">Maison, 3 chambres</td>
<td headers="idfa62_c1">2400€</td>
<td headers="idfa62_c2">6240€</td></tr>
<tr class="row_even even">
<td headers="idfa62_c0">Maison, 4 chambres</td>
<td headers="idfa62_c1">5760€</td>
<td headers="idfa62_c2">10008€</td></tr>
<tr class="row_odd odd">
<td headers="idfa62_c0">Appartement, 2 chambres</td>
<td headers="idfa62_c1">2528€</td>
<td headers="idfa62_c2">5280€</td></tr>
<tr class="row_even even">
<td headers="idfa62_c0">Appartement, 3 chambres</td>
<td headers="idfa62_c1">5950€</td>
<td headers="idfa62_c2">7980€</td></tr>
<tr class="row_odd odd">
<td headers="idfa62_c0">Studio</td>
<td headers="idfa62_c1">2400€</td>
<td headers="idfa62_c2">4320€</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_2"></a>Conditions de location </h3>
<p>La durée des baux est en principe d’un an mais les pratiques divergent énormément selon les propriétaires. Il est conseillé d’avoir recours à une agence pour trouver un logement une maison ou un appartement. Compte tenu des conditions d’insécurité régnantes, il est préférable de louer un appartement. L’agence immobilière fait généralement payer l’équivalent d’un mois de loyer à la signature du contrat.</p>
<p>Les charges à payer en supplément du loyer correspondent principalement aux frais d’électricité. Le gaz est très peu onéreux et l’eau est souvent comprise dans les charges communes.</p>
<h3 class="spip"><a id="sommaire_3"></a>Hôtels</h3>
<p>Si vous cherchez un hébergement typique optez pour les <i>posadas</i> (pensions familiales) où, même parmi les moins chères et les plus basiques, le caractère, l’originalité ou au moins l’accueil sont au rendez-vous. Si vous voyagez en pleine nature vous trouverez des <i>campamentos</i> : des hébergements qui peuvent varier de la cabane rustique avec quelques hamacs aux superbes auberges de campagne (piscine, climatisation…).</p>
<p>La plupart des hôtels proposent également des chambres relativement confortables avec salle de bains privés pour les petits budgets.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/vie-pratique/article/logement-103738). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
