# Santé

<p>Précautions à prendre :</p>
<p>Aucune vaccination n’est exigée pour l’entrée sur le territoire luxembourgeois. Il est toutefois recommandé d’être à jour dans ses vaccins (diphtérie, poliomyélite et tétanos).</p>
<p>Pour les enfants, veuillez surveiller le calendrier vaccinal.</p>
<p>Il est vivement conseillé de consulter un médecin traitant avant le départ et de souscrire une assurance rapatriement.</p>
<p>En ce qui concerne le SIDA, il est rappelé que le risque existe partout dans le monde et que les mesures de prévention doivent être observées où que l’on soit.</p>
<p>La carte européenne d’assurance maladie atteste de vos droits à l’assurance maladie en Europe. Elle est valable un an. Pour plus d’informations, veuillez consulter notre rubrique thématique <a href="services-aux-citoyens-preparer-son-expatriation-protection-sociale.md" class="spip_in">Protection sociale</a>.</p>
<p>Pour une présentation des risques épidémiologiques ainsi que des conseils sur la médecine au quotidien, consultez les pages suivantes :</p>
<ul class="spip">
<li><a href="http://www.sante.public.lu/" class="spip_out" rel="external">Conseils, liste de médecins et hôpitaux</a></li>
<li>Page dédiée à la <a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/luxembourg/" class="spip_in">santé au Luxembourg sur le site Conseils aux voyageurs</a></li></ul>
<p><strong>Pour en savoir plus :</strong></p>
<p>Les recommandations par pays de l’Institut Pasteur :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.pasteur.fr/" class="spip_out" rel="external">Pasteur.fr</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/vie-pratique/article/sante). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
