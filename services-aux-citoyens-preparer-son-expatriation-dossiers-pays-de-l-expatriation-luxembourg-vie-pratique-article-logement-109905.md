# Logement

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/vie-pratique/article/logement-109905#sommaire_1">Où se loger ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/vie-pratique/article/logement-109905#sommaire_2">Conditions de location</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/vie-pratique/article/logement-109905#sommaire_3">Hôtels</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/vie-pratique/article/logement-109905#sommaire_4">Electricité</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/vie-pratique/article/logement-109905#sommaire_5">Electroménager</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Où se loger ?</h3>
<p>Au Luxembourg, on peut trouver un logement à louer en consultant les annonces immobilières des journaux comme le Luxemburger Wort, Tageblatt, Le Quotidien ou le site internet <a href="http://www.athome.lu/" class="spip_out" rel="external">Athome.lu</a>.</p>
<p>A Luxembourg-Ville, les quartiers résidentiels sont Merl, Bel-Air, Limperstberg, Kirchberg, Bonnevoie.</p>
<p>La formule "appart-hôtel" peut constituer une solution d’attente. Le prix des locations oscillent entre 750 et 1000 euros pour la semaine et entre 1230 et 2500 euros pour le mois</p>
<p>Quelques exemples de sites proposant des locations de ce type :</p>
<ul class="spip">
<li><a href="http://www.domus.lu/" class="spip_out" rel="external">Domus.lu</a></li>
<li><a href="http://www.key-inn.com/fr/accueil" class="spip_out" rel="external">Key-inn.com</a></li>
<li><a href="http://www.senator.lu/contact.html" class="spip_out" rel="external">Senator.lu</a></li></ul>
<p>Il existe également des studios à louer auprès de particuliers ou des chambres à louer chez l’habitant.</p>
<p><strong>Exemples de prix de location en euros</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Lieu</td>
<td>Studio</td>
<td>3 pièces</td>
<td>5 pièces</td>
<td>Villa</td></tr>
<tr class="row_even even">
<td>Quartier résidentiel</td>
<td>900</td>
<td>1700</td>
<td>2500-3000</td>
<td>Plus de 3000</td></tr>
<tr class="row_odd odd">
<td>Banlieue</td>
<td>700</td>
<td>1300</td>
<td>1750-2000</td>
<td>Au moins 2500</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_2"></a>Conditions de location</h3>
<p>L’offre de logements est relativement variée mais les loyers sont assez élevés. Il est possible de trouver des loyers un peu moins élevés dans les villes proches de Luxembourg ; celles-ci sont souvent bien desservies par le chemin de fer ou les bus et les temps de transport pour rejoindre la capitale sont souvent raisonnables.</p>
<p>Les propriétaires proposent un bail de trois ans, sans révocation possible, sauf clause particulière à négocier, inscrite dans le bail, pour raison de mutation. Il est donc souhaitable de demander un bail d’un an renouvelable.</p>
<p>Les logements sont loués vides. Les charges représentent environ 10% du loyer et comprennent l’éclairage et l’entretien des parties communes. Il est nécessaire de prévoir :</p>
<ul class="spip">
<li>les frais d’agence (environ 1 mois de loyer) + 15% TVA ;</li>
<li>une caution de deux voire trois mois de loyer (garantie bancaire) ;</li></ul>
<p>Un état des lieux est à effectuer à la remise des clés et sera repris avant le départ du locataire.</p>
<h3 class="spip"><a id="sommaire_3"></a>Hôtels</h3>
<p>A titre indicatif, voici les prix pour une chambre d’hôtel (double) à Luxembourg et ses environs :</p>
<ul class="spip">
<li>Hôtel 2 étoiles : de 60 à 110 euros ;</li>
<li>Hôtel 3 étoiles : de 90 à 270 euros ;</li>
<li>Hôtel 4 étoiles : de 90 à 480 euros ;</li>
<li>Hôtel 5 étoiles : de 110 à 550 euros</li></ul>
<p>Les prix des hôtels varient aussi en fonction de leur localisation.</p>
<h3 class="spip"><a id="sommaire_4"></a>Electricité</h3>
<p>Courant électrique : 220 volts, fréquence : 50 Hz. Les prises de courant sont aux normes européennes DIN. Les prises de téléphone sont différentes de celles utilisées en France.</p>
<h3 class="spip"><a id="sommaire_5"></a>Electroménager</h3>
<p>Il existe une grande diversité d’appareils électroménagers disponibles sur le marché local. Les cuisines sont souvent partiellement équipées.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/vie-pratique/article/logement-109905). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
