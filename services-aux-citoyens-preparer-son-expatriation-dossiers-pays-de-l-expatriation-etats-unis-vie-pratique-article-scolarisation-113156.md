# Scolarisation

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/vie-pratique/article/scolarisation-113156#sommaire_1">Les établissements scolaires français en Etats-Unis</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/vie-pratique/article/scolarisation-113156#sommaire_2">Enseignement supérieur</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Les établissements scolaires français en Etats-Unis</h3>
<p>Pour toute information sur la scolarisation dans le système français à l’étranger, vous pouvez consulter notre thématique sur les <a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures.md" class="spip_in">Etudes supérieures</a> et la <a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais.md" class="spip_in">Scolarité en français</a>.</p>
<p>Vous y trouverez des renseignements sur :</p>
<ul class="spip">
<li>les établissements français du primaire et du secondaire à l’étranger ;</li>
<li>les bourses scolaires et la prise en charge des frais de scolarité à l’étranger ;</li>
<li>les possibilités qui s’offrent à vous si votre enfant ne peut être scolarisé à l’étranger dans le système français (enseignement à distance par le CNED, programme français langue maternelle (FLAM), internats en France) ;</li>
<li>les épreuves du baccalauréat à l’étranger ;</li>
<li>les bourses d’études supérieures en France et à l’étranger ;</li>
<li>l’équivalence des diplômes.</li></ul>
<p>Pour en savoir plus :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.aefe.fr/" class="spip_out" rel="external">Agence pour l’enseignement français à l’étranger</a></p>
<h3 class="spip"><a id="sommaire_2"></a>Enseignement supérieur</h3>
<p>Les études supérieures aux Etats-Unis nécessitent l’obtention du <i>High School Diploma</i> et, pour les élèves étrangers, du diplôme marquant la fin des études secondaires. Cependant de très nombreux établissements, universités ou <i>colleges</i>, ne se contentent pas de ce diplôme et exigent que les candidats présentent également leurs résultats aux tests d’évaluation de niveau qui ont une valeur nationale : les SAT (<i>Scholastic Assessment Test</i>) ou ACT.</p>
<p>Les établissements d’enseignement supérieur sont libres de fixer leurs propres critères d’admission. Les titulaires du baccalauréat français, selon les établissements, pourront être admis directement en deuxième année de <i>College</i> américain.</p>
<p>L’accès en <i>graduate school</i> (après l’obtention du <i>Bachelor’s Degree</i> ou B.A.) est possible pour les diplômés étrangers. Les diplômés du système universitaire français, bien qu’il n’existe pas d’équivalence automatique entre la France et les Etats-Unis pour les titres et diplômes universitaires, peuvent en principe s’inscrire à ce cycle (préparation d’un <i>Master’s Degree</i>), s’ils sont titulaires d’une licence. Les titulaires d’une maîtrise, d’un DESS ou d’un DEA peuvent également accéder à un niveau de <i>graduate studies</i> et, à terme, à un PhD ou un titre doctoral équivalent. Le diplôme d’ingénieur français a été reconnu en mai 2013 comme équivalent au <i>US Masters degree in Engineering</i> par l’AACRAO (Association regroupant les responsables des admissions dans les universités américaines).</p>
<p>Le système d’enseignement supérieur américain comprend essentiellement quatre types d’établissements :</p>
<ul class="spip">
<li>Les <i>Community</i> et <i>Junior Colleges</i> (formation en deux ans), établissements généralement publics qui proposent un enseignement de courte durée, à faible coût et de proximité.</li>
<li>Les <i>Colleges</i> (formation en quatre ans) sont des établissements autonomes qui assurent un enseignement au niveau undergraduate.</li>
<li><strong>Les universités</strong> (qautre ans et plus) se composent généralement de plusieurs collèges, parfois également appelés <i>schools</i>. (<i>Illinois Institute of Technology</i>, <i>California Institute of Technology</i>, etc.) ; elles peuvent être publiques ou privées.</li>
<li>Les établissements d’enseignement technique (<i>vocational schools</i> et <i>technical schools</i>) dispensent en général un enseignement non universitaire et proposent une formation professionnelle dans le domaine des arts et métiers.</li></ul>
<p>Les États-Unis comptent plus de 4000 établissements d’enseignement supérieur et plus de 7500 <i>vocational schools</i>. Le coût moyen des études (qui inclut les frais de scolarité, le logement et le fonctionnement du bureau des élèves) varie pour l’année scolaire 2011-2012 de 15 000 USD pour les établissements publics à 35 000 USD pour les établissements privés. Les études au sein d’un <i>Community College</i> sont cependant moins onéreuses, soit environ 4000 USD par an.</p>
<p>Les universités américaines pratiquent en général une sélection assez rigoureuse à l’admission et tiennent compte, en plus des résultats scolaires, de la personnalité des candidats et de leurs activités péri-scolaires.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li>la <a href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/Fiche_CURIE_Etats-Unis_2013-_cle8711d5.pdf" class="spip_out">fiche Curie</a></li>
<li><a href="http://www.educationusa.state.gov/" class="spip_out" rel="external">Bureau of Educational and Cultural Affairs</a> du Département d’Etat. Ce site fournit des informations utiles aux étudiants étrangers désireux de poursuivre un cycle d’études aux Etats-Unis.</li>
<li>Sites de l’Ambassade des Etats-Unis en France : <a href="http://www.ambafrance-us.org/" class="spip_out" rel="external">Ambafrance-us.org</a> et <a href="http://frenchculture.org/front" class="spip_out" rel="external">Frenchculture.org</a>.</li>
<li><a href="http://www.micefa.org/" class="spip_out" rel="external">Mission interuniversitaire de coordination des échanges franco-américains</a>. La MICEFA est un organisme, association loi 1901, qui regroupe de nombreuses universités d’Ile-de-France, ainsi que d’autres institutions. Elle a signé des accords d’échanges avec de nombreuses universités et consortium sur tout le territoire américain. Les étudiants inscrits dans l’un des établissements membres de la MICEFA ont la possibilité de bénéficier d’une des quelque 200 places proposées chaque année. Pour s’informer sur les universités et les cours disponibles, consultez le site de la MICEFA et contactez le service des relations internationales de votre université.</li>
<li><a href="http://www.fulbright-france.org/" class="spip_out" rel="external">Commission franco-américaine d’échanges universitaires et culturels (<i>Fulbright commission</i>)</a>. La Commission franco-américaine contribue à développer les liens d’amitié et de coopération entre la France et les États-Unis par l’intermédiaire d’échanges éducatifs. Elle offre des bourses à des étudiants, à des jeunes professionnels et à des chercheurs français et américains.</li></ul>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/vie-pratique/article/scolarisation-113156). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
