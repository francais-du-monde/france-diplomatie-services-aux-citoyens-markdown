# Passeport, visa, permis de travail

<p>Les ressortissants de l’Union européenne peuvent s’établir librement en Belgique, sous réserve de justifier des ressources suffisantes.</p>
<p>Tout citoyen de l’Union a le droit de séjourner sur le territoire de l’Etat membre d’accueil pendant une période allant jusqu’à trois mois, sans autres conditions ou formalités que l’obligation d’être en possession d’une carte d’identité ou d’un passeport en cours de validité.</p>
<p>Pour un séjour de plus de trois mois, un Français désirant s’installer en Belgique est tenu d’effectuer, <strong>dans les huit jours </strong>suivant son arrivée, une <strong>demande d’établissement auprès de sa commune de résidence (mairie)</strong>, afin d’obtenir un <strong>permis de séjour</strong>. Il doit fournir les documents suivants :</p>
<ul class="spip">
<li>carte nationale d’identité ou passeport en cours de validité,</li>
<li>preuve de domicile,</li>
<li>attestation d’emploi (annexe 19b) ou justification de ressources,</li>
<li>3 photographies d’identité.</li></ul>
<p><strong>Attention</strong> : pour effectuer un séjour de plus de trois mois, vous devez être en mesure de justifier de ressources suffisantes sous peine de risquer d’être obligé de quitter le territoire belge.</p>
<p>Des frais de dossier, variables selon les communes, sont à acquitter pour la confection de votre permis de séjour.</p>
<p>Les autorités belges délivreront dans un premier temps un <strong>permis de séjour de trois mois</strong> (carte violette) renouvelable, puis si vous disposez d’une résidence fixe et d’un revenu régulier, vous pouvez demander un <strong>permis de séjour permanent</strong> (carte bleue), <strong>valable cinq ans</strong>.</p>
<p>Les ressortissants de l’UE sont dispensés de permis de travail et jouissent des mêmes opportunités, droits et obligations que les travailleurs belges.</p>
<p>Pour toute information complémentaire relative aux conditions de séjour en Belgique, il est vivement conseillé de contacter le <a href="http://www.consulfrance-bruxelles.org/" class="spip_out" rel="external">consulat général de France à Bruxelles</a>).</p>
<p><strong>Pour en savoir plus</strong> :</p>
<ul class="spip">
<li>Vous pouvez consulter la thématique <a href="services-aux-citoyens-preparer-son-expatriation-documents-de-voyage.md" class="spip_in">Permis de séjour – permis de travail</a></li>
<li>Le site internet de l’Ambassade de France en Belgique, rubrique <a href="http://www.ambafrance-be.org/-S-installer-vivre-et-etudier-en-" class="spip_out" rel="external">S’installer, vivre et étudier en Belgique</a></li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/entree-et-sejour/article/passeport-visa-permis-de-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
