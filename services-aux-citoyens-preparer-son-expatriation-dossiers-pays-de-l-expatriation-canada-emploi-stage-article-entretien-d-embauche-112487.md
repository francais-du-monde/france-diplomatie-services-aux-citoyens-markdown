# Entretien d’embauche

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/emploi-stage/article/entretien-d-embauche-112487#sommaire_1">Conduite de l’entretien</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/emploi-stage/article/entretien-d-embauche-112487#sommaire_2">Apparence et attitude</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/emploi-stage/article/entretien-d-embauche-112487#sommaire_3">Négociation du salaire</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/emploi-stage/article/entretien-d-embauche-112487#sommaire_4">Questions préférées des recruteurs</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/emploi-stage/article/entretien-d-embauche-112487#sommaire_5">Erreurs à éviter</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/emploi-stage/article/entretien-d-embauche-112487#sommaire_6">Après l’entretien</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Conduite de l’entretien</h3>
<p>Si vous vous trouvez au Canada et que votre candidature est retenue, vous serez convoqué pour un entretien d’embauche au sein de l’entreprise.</p>
<p>Cet entretien peut se dérouler par téléphone si vous n’êtes pas au Canada. Il permettra de cerner précisément votre profil et vos compétences. Il sera déterminant pour la suite réservée à votre candidature. <strong>Il est important de ne pas le négliger.</strong></p>
<p>Si l’entretien se déroule en anglais, il est fortement conseillé de bien maîtriser la langue.</p>
<p>Utilisez une ligne téléphonique fixe dans un lieu tranquille et à l’abri de toutes nuisances sonores. L’utilisation du portable est à proscrire.</p>
<p>L’entretien d’embauche téléphonique est pratiqué au sein des entreprises. Il ne faut, par conséquent, jamais l’exclure des étapes à franchir pour décrocher un emploi.</p>
<p>Au Québec, l’entretien d’embauche par téléphone n’est pas très développé. Dans le cadre d’une recherche d’emploi au Québec à partir de la France, il est possible d’être convié à un pré-entretien téléphonique. C’est une première étape, certes importante, dans le processus d’embauche, mais insuffisante. Si le candidat est retenu au cours de cet entretien téléphonique, il sera invité à faire le déplacement au Québec.</p>
<p>La webcam n’est pas encore utilisée dans ce type d’entretien téléphonique.</p>
<p>Au Québec, <strong>l’entretien s’appelle souvent entrevue</strong>. Celle-ci est très différente de son équivalent français et se déroule dans une ambiance décontractée.</p>
<p>Très souvent, le candidat aura l’impression que l’entrevue s’est bien déroulée et qu’il a donc toutes les chances d’être embauché. L’employeur, toujours très courtois, voire chaleureux, et enthousiaste semblera avoir porté un réel intérêt aux arguments avancés par le candidat.</p>
<p>A l’exception de certains secteurs d’activité comme la vente ou l’assurance, où le recruteur poussera beaucoup plus le candidat dans ses derniers retranchements, jamais ce dernier ne se sentira piégé, voire humilié.</p>
<p>L’objectif du recruteur est de mettre le candidat en confiance et d’avoir avec lui un entretien harmonieux et courtois, qui lui permettra de mieux connaître son futur employé. Ce comportement ne signifie cependant pas systématiquement que vous serez engagé. Par conséquent, l’impression positive que vous aurez retenue d’une entrevue ne doit pas vous empêcher de poursuivre vos recherches d’emploi.</p>
<p><strong>Avant l’entretien</strong></p>
<p>Pour passer avec succès l’entretien d’embauche, il est nécessaire de s’y préparer soigneusement :</p>
<ul class="spip">
<li>Effectuez des recherches sur l’entreprise ;</li>
<li>N’hésitez pas à vous informer de la durée de l’entrevue, des personnes présentes et de la manière dont se déroulera l’entretien (y aura-t-il des tests, par exemple).</li>
<li>Renseignez-vous sur les tâches et les responsabilités précises de l’emploi auquel vous postulez ;</li>
<li>Renseignez-vous auprès du bureau emploi de Ressources humaines et Développement social Canada (RHDSC) de votre ville afin de bénéficier d’une simulation d’entretien d’embauche ;</li>
<li>Demandez le lieu et l’horaire précis de l’entretien car celui-ci ne se déroulera pas forcément au siège de l’entreprise ;</li>
<li>Arrivez dix minutes avant l’heure car les Canadiens sont ponctuels. Si vous devez arriver en retard à votre rendez-vous, prévenez votre interlocuteur de ce contretemps ;</li>
<li>Préparez une série de questions par écrit ;</li>
<li>Ne perdez pas de vue que le Canada appartient au continent nord-américain. Une connaissance même élémentaire du pays vous évitera de nombreuses désillusions ;</li></ul>
<p>Certains organismes peuvent vous assister lorsque l’entrevue se déroule loin de votre domicile.</p>
<p><strong>Pendant l’entretien</strong></p>
<ul class="spip">
<li>Optez pour une poignée de main franche ;</li>
<li>Maîtrisez vos gestes et ayez une attitude sereine ;</li>
<li>Favorisez une stratégie de communication " non-verbale " qui met en avant votre image professionnelle. Parler trop peut jouer en votre défaveur ;</li>
<li>Utilisez le silence et évitez de répondre par des onomatopées. Réfléchissez à la question posée et organisez vos pensées avant de répondre ;</li>
<li>Contrairement à la France où les études et les diplômes sont très valorisés, les employeurs canadiens s’intéressent plus aux capacités et aux compétences des candidats, sans leur coller une étiquette. Il est ainsi possible de faire carrière dans un secteur différent de sa formation.</li>
<li>Centrez votre entretien sur vos compétences, votre facilité d’adaptation, votre mobilité, votre capacité à travailler en équipe, votre souhait, le cas échéant, de faire bénéficier l’entreprise de votre expérience et votre volonté d’apprendre, l’humilité étant une force au Canada.</li>
<li>Le recruteur vous demandera d’indiquer vos défauts : ne vous dérobez pas et ne vous inventez pas de défaut (par exemple : trop perfectionniste). Proposez toujours une solution au défaut mentionné.</li></ul>
<p><strong>Documents à apporter pour l’entrevue</strong></p>
<ul class="spip">
<li>Des curriculum vitae (vous aurez peut-être plusieurs interlocuteurs) ;</li>
<li>Des cartes de visite (indispensable au Québec) ;</li>
<li>L’offre d’emploi (elle vous servira d’aide-mémoire) ;</li>
<li>De quoi prendre des notes ;</li>
<li>Une feuille sur laquelle vous aurez rédigé des questions à poser pendant l’entrevue ;</li>
<li>Une liste de références (ancien employeur, collègues de travail, ancien professeur, conseiller en emploi).</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Apparence et attitude</h3>
<p>Les Canadiens sont généralement décontractés (<i>casual</i>). Privilégiez cependant une tenue classique. Le jean est à proscrire.</p>
<p>Votre apparence et votre attitude seront fonction du poste auquel vous postulez. Pour les métiers de la finance ou du service à la clientèle, les hommes devront être en costume et les femmes en tailleur.</p>
<p>Les entreprises nord-américaines ont souvent adopté le principe du <i>Casual Friday</i>, jour, généralement le vendredi, où les employés s’habillent comme bon leur semble. Cette règle ne s’applique cependant pas aux candidats à un emploi. Par conséquent, si votre entrevue se déroule un vendredi, ne vous présentez pas en tenue décontractée, mais en costume.</p>
<p>A votre arrivée, il est préférable d’attendre que le recruteur vous tende la main avant de la lui serrer. Généralement, on se serre la main uniquement lors de la première rencontre. Il est également important de regarder son interlocuteur dans les yeux et de soutenir son regard.</p>
<p>Vous ne devrez pas couper la parole à votre interlocuteur. Chacun parle à son tour, sans interrompre l’autre personne.</p>
<p>Enfin, tenez compte du climat. En hiver, prévoyez deux paires de chaussures ou une paire de « surchausses », afin de ne pas arriver dans les locaux de l’entreprise avec des chaussures sales ou mouillées, ce qui serait du plus mauvais effet.</p>
<p>Nos compatriotes sont généralement bien acceptés. Certains clichés sont cependant susceptibles de les desservir :</p>
<ul class="spip">
<li>ton plaintif ;</li>
<li>comportement exigeant ;</li>
<li>lacunes linguistiques ;</li>
<li>attitude conquérante.</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Négociation du salaire</h3>
<p>Avant d’aborder la question des avantages sociaux et du salaire, il est impératif de faire des recherches sur le système de protection sociale de la province où vous travaillerez, sur les avantages offerts tels que la couverture médicale, les vacances, les primes, et sur le salaire minimum qui varie selon les Provinces.</p>
<p>Les employeurs canadiens privilégient le dialogue. Il est par conséquent préférable de parfaitement s’entendre sur les conditions et de les avoir bien comprises. Il ne faut jamais perdre de vue que le système canadien est libéral et qu’il applique des méthodes américaines (licenciement immédiat sans indemnités, peu de vacances annuelles).</p>
<p>La question du salaire est très importante. Elle ne devra être abordée qu’à la fin de l’entrevue et, si possible, par l’employeur. N’hésitez pas à répondre franchement à cette question.</p>
<p>Votre employeur aura une idée très claire de la valeur du poste qu’il souhaite vous confier. L’absence de toute expérience professionnelle au Canada et de qualifications sera certainement prise en compte. Ainsi, le montant que vous suggérerez lui permettra de voir si vos prétentions sont réalistes.</p>
<p>Donnez une fourchette de salaire, plutôt qu’un chiffre précis. La fourchette que vous indiquerez sera de l’ordre de 20 %. Sachez que vous négocierez un salaire annuel et non mensuel ou hebdomadaire. Pour connaître le salaire auquel vous pouvez prétendre, vous devez auparavant vous renseigner sur les grilles salariales pratiquées dans votre profession. Voici quelques pistes :</p>
<ul class="spip">
<li>le site Internet québécois "Information sur le marché du travail" (IMT) est une mine de renseignements. Il indique les rémunérations pour chaque profession.</li>
<li>vous pouvez vous renseigner auprès de personnes exerçant le même métier que vous ou dans le même secteur. Le salaire n’étant pas un tabou au Canada, la plupart des Canadiens vous indiqueront assez librement leur rémunération.</li>
<li>les organisations syndicales.</li>
<li>en vous renseignant au préalable sur votre future entreprise, vous connaîtrez peut-être son chiffre d’affaires, ses perspectives de croissance et l’état du marché. Toutes choses utiles pour négocier votre rémunération.</li></ul>
<p>Pour justifier le salaire que vous proposerez, préparez une liste de vos accomplissements professionnels qui ont permis à votre précédent employeur de progresser.</p>
<p>Votre salaire sera fonction du marché, de vos compétences, mais aussi des compétences requises pour le poste auquel vous postulez.</p>
<p>Chaque branche sectorielle, chaque entreprise dispose de ses propres avantages. Votre salaire se négociera aussi en fonction des prestations auxquelles vous aurez droit (mutuelles, nombre de semaines de congés payés, etc.). Si la rémunération ne vous satisfait pas, convenez de revoir votre futur patron pour une revalorisation dans quelques mois.</p>
<p>Il peut être également intéressant de se renseigner sur les prélèvements sociaux et les impôts afin de ne pas avoir de surprise. Pour un salaire annuel brut de 18 000 dollars, le salaire net après impôts et prélèvements sociaux sera de 14 900 dollars. Pour un salaire de 30 000 dollars, la rémunération nette sera de 22 800 dollars. Enfin, pour un salaire de 50 000 dollars, le salaire annuel net sera d’environ 34 100 dollars.</p>
<h3 class="spip"><a id="sommaire_4"></a>Questions préférées des recruteurs</h3>
<p>Les organismes d’aide à l’emploi vous indiqueront les thèmes préférés des recruteurs. Ce seront vos meilleurs conseillers sur le sujet. En connaissant à l’avance ces questions, vous pourrez préparer vos réponses et mettre ainsi toutes les chances de votre côté.</p>
<p>Parmi la cinquantaine de questions que peut vous poser un employeur, les cinq questions suivantes reviennent fréquemment :</p>
<ul class="spip">
<li>Parlez-moi un peu de vous ;</li>
<li>Quels sont vos principaux défauts ?</li>
<li>Quels sont vos points forts ?</li>
<li>Quel salaire demandez-vous ?</li>
<li>Pourquoi devrais-je vous engager ?</li></ul>
<p>D’autres thèmes seront évoqués :</p>
<ul class="spip">
<li>votre statut au Canada. Votre employeur souhaitera savoir si vous avez légalement le droit de travailler. Par ailleurs, un candidat titulaire d’un seul permis de travail temporaire sera moins intéressant qu’une personne en possession d’un visa de résident permanent.</li>
<li>votre expérience au Canada. N’omettez rien, notamment tout ce qui a trait au bénévolat si vous n’avez jamais travaillé au Canada.</li>
<li>votre accomplissement professionnel le plus important.</li>
<li>vos emplois antérieurs : ceux que vous avez préférés, ceux que vous avez le moins aimés, ce que vous pensez de vos précédents employeurs et de vos anciens supérieurs hiérarchiques.</li>
<li>la formation reçue et ce à quoi elle correspond au Canada. Vous devrez prendre le temps d’expliquer les tenants et les aboutissants de votre formation afin de rassurer votre employeur.</li>
<li>les raisons de votre installation au Canada et vos objectifs à long terme.</li></ul>
<h3 class="spip"><a id="sommaire_5"></a>Erreurs à éviter</h3>
<p>Voici quelques exemples de questions :</p>
<ul class="spip">
<li>Quand puis-je espérer une réponse ?</li>
<li>Sous l’autorité de qui serai-je placé(e) et à quel échelon hiérarchique ?</li>
<li>Pourriez-vous m’indiquer le contenu de la période d’essai et/ou d’apprentissage ?</li>
<li>Offrez-vous un programme de formation ?</li></ul>
<p>Vous trouverez ci-dessous une liste des erreurs à éviter :</p>
<ul class="spip">
<li>Evitez le bavardage et ne vous sentez pas obligé de poser toutes les questions que vous avez préparées.</li>
<li>Axez plutôt l’entretien sur vos compétences, votre facilité d’adaptation, votre mobilité, votre capacité à travailler en équipe, vos accomplissements professionnels et, le cas échéant, votre souhait de faire bénéficier l’entreprise de votre expérience.</li>
<li>La modestie est une qualité. Faites très attention à ne jamais paraître prétentieux sur votre savoir-faire ou à exagérer vos compétences professionnelles et linguistiques. Le but recherché est d’établir un dialogue avec votre ou vos interlocuteur(s) qui vous soit favorable.</li>
<li>Veillez à ne pas faire étalage de clichés sur le Canada et en particulier sur la province de Québec. De même, il ne faut pas sous-estimer les différences linguistiques et d’accent, ainsi que les préjugés qu’entretiennent parfois les Québécois à leur égard : ainsi, l’accent français pourra être perçu comme arrogant par votre interlocuteur québécois.</li>
<li>Faites preuve de retenue et de discrétion y compris à l’égard de votre propre pays. Il est inutile de dénigrer la France. Vos problèmes n’intéresseront pas votre interlocuteur.</li>
<li>Ne vous laissez pas intimider.</li>
<li>Elever la voix et parler fort est très mal considéré.</li>
<li>Arriver en retard. L’absence de ponctualité est mal vue au Québec.</li>
<li>Mettre l’accent sur votre individualisme et votre indépendance dans le travail. Au Canada, il est important d’avoir le sens du travail en équipe.</li>
<li>Ne pas interrompre votre interlocuteur</li>
<li>Etre trop décontracté. La décontraction avec laquelle se déroule l’entretien peut en effet être trompeuse : il ne faut pas oublier qu’il s’agit d’une entrevue d’embauche.</li></ul>
<h3 class="spip"><a id="sommaire_6"></a>Après l’entretien</h3>
<p>L’entrevue est terminée. Quelle que soit votre impression quant à son déroulement, demandez à votre interlocuteur la manière dont vous pourrez reprendre contact avec lui. N’hésitez pas à demander le délai de réponse. Sinon, manifestez-vous 10 à 15 jours après l’entretien.</p>
<p>Vous devrez rester en contact avec la personne qui vous a reçu. Arrivé chez vous, envoyez un courriel de remerciement. Ce courriel est très important : il doit être envoyé moins de 24 heures après l’entrevue. <strong>Ne pas envoyer de mot de remerciement pourra être perçu comme une marque de désintérêt pour l’entreprise.</strong></p>
<p>Ne vous attendez pas à être contacté par la personne qui vous a reçue si vous ne vous manifestez pas et encore moins si vous n’avez pas envoyé de courriel. Le recruteur s’attend (à moins qu’il n’indique expressément le contraire) à ce que vous le contactiez pour vous renseigner sur le processus d’embauche.</p>
<p>Au Canada, la boîte vocale est un instrument de communication et une méthode de filtrage très développés au sein des entreprises. Il est très difficile d’obtenir immédiatement un interlocuteur. La standardiste vous redirigera vers la boîte vocale de l’interlocuteur que vous cherchez à joindre. Laissez un message court et clair. Il est inutile de répéter l’opération. Les relations professionnelles étant moins hiérarchisées qu’en France, vous pouvez joindre directement votre interlocuteur sans passer par son secrétariat.</p>
<p><i>Mise à jour : avril 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/emploi-stage/article/entretien-d-embauche-112487). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
