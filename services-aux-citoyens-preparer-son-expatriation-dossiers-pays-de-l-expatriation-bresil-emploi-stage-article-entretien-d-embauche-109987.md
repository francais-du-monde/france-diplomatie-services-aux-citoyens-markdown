# Entretien d’embauche

<p>La convivialité est de mise lors des entretiens d’embauche, toutefois, même si l’ambiance est plus détendue qu’en France, il convient de montrer de ce que vous apporterez concrètement à l’entreprise. Il est important de soigner votre tenue vestimentaire.</p>
<p>Plusieurs entretiens peuvent être organisés avant que l’entreprise décide de vous recruter.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/emploi-stage/article/entretien-d-embauche-109987). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
