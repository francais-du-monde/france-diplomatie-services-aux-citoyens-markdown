# Recherche d’emploi

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/emploi-stage/article/recherche-d-emploi-111337#sommaire_1">Outils pour la recherche d’emploi</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/emploi-stage/article/recherche-d-emploi-111337#sommaire_2">Sites d’intérêt </a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/emploi-stage/article/recherche-d-emploi-111337#sommaire_3">Organismes pour la recherche d’emploi</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Outils pour la recherche d’emploi</h3>
<h4 class="spip">Journaux</h4>
<ul class="spip">
<li><a href="http://www.idnes.cz/" class="spip_out" rel="external">Mlada Fronta Dnes</a> (tous les mardis et jeudis)</li>
<li><a href="http://hn.ihned.cz/" class="spip_out" rel="external">Hospodarske Noviny</a> (suppl. Kariera - tous les jeudis)</li>
<li><a href="http://www.lidovky.cz/" class="spip_out" rel="external">Lidove Noviny</a></li>
<li><a href="http://www.annonce.cz/" class="spip_out" rel="external">Annonce</a></li></ul>
<h4 class="spip">Sites internet</h4>
<ul class="spip">
<li><a href="http://kariera.ihned.cz/" class="spip_out" rel="external">Kariera.ihned.cz</a> (en tchèque)</li>
<li><a href="http://hotjobs.cz/" class="spip_out" rel="external">Hotjobs.cz</a> (en tchèque)</li>
<li><a href="http://www.jobpilot.cz/" class="spip_out" rel="external">Jobpilot.cz</a> (en tchèque)</li>
<li><a href="http://www.cvonline.cz/" class="spip_out" rel="external">CVonline.cz</a> (en anglais)</li>
<li><a href="http://www.jobs.cz/" class="spip_out" rel="external">Jobs.cz</a> (en tchèque ou en anglais)</li>
<li><a href="http://www.annonce.cz/prace-obchod-a-sluzby.html" class="spip_out" rel="external">Annonce.cz</a></li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Sites d’intérêt </h3>
<p>Une liste des principales entreprises françaises implantées en République tchèque est disponible sur le site de l’<a href="http://www.france.cz/" class="spip_out" rel="external">ambassade de France</a>.</p>
<h3 class="spip"><a id="sommaire_3"></a>Organismes pour la recherche d’emploi</h3>
<p><strong>Le comité consulaire pour l’emploi et la formation professionnelle (CCPEFP),</strong> dont les bureaux sont installés dans les locaux de la Chambre de commerce franco-tchèque (CCFT), aide et conseille gratuitement les Français dans leur recherche d’emploi en République tchèque. Il recueille les demandes d’emploi et de stage, ainsi que les offres transmises par les entreprises.</p>
<p>Le CCPEFP offre les services suivants :</p>
<ul class="spip">
<li>informations sur le marché de l’emploi et les procédures d’embauche en République tchèque ;</li>
<li>communication des coordonnées d’entreprises sous forme de listes sectorielles ;</li>
<li>publication d’une annonce sur son site Internet, ainsi que dans la revue " Contact " de la CCFT ;</li>
<li>mise en relation des demandes avec les offres d’emploi transmises par les entreprises (dont certaines sont disponibles sur le site de la CCFT). Pour que le CCPEFP puisse enregistrer votre demande d’emploi ou de stage, vous devez envoyez par <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/emploi-stage/article/recherche-d-emploi-111337#emploi#mc#ccft-fcok.cz#" title="emploi..åt..ccft-fcok.cz" onclick="location.href=mc_lancerlien('emploi','ccft-fcok.cz'); return false;" class="spip_mail">courriel</a> les documents suivants :</li></ul>
<ul class="spip">
<li>votre curriculum vitae (au moins en français) ;</li>
<li>votre lettre de motivation ;</li>
<li>le texte de l’annonce à publier (400 caractères maximum, ex. sur le site <a href="http://www.ccft-fcok.cz/fr/index.asp?p=kandidati-inzeraty" class="spip_out" rel="external">de la chambre de commerce franco-tchèque</a>)</li></ul>
<p>Pour plus d’information, vous pouvez contacter le CCPEFP à l’adresse suivante :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  IBC Building - Pobrezni 3 <br class="manualbr">18600 Praha 8 - Karlin<br class="manualbr">Téléphone : [420] 224 83 30 90 - Télécopie : [420] 224 83 30 93 <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/emploi-stage/article/recherche-d-emploi-111337#emploi#mc#ccft-fcok.cz#" title="emploi..åt..ccft-fcok.cz" onclick="location.href=mc_lancerlien('emploi','ccft-fcok.cz'); return false;" class="spip_mail">Courriel</a></p>
<p>L´<a href="http://portal.mpsv.cz/upcr/" class="spip_out" rel="external">Office tchèque du travail</a>, correspond à Pôle emploi (<i>Úrad práce</i>)</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://portal.mpsv.cz/sz/zahr_zam/prociz/vmciz" class="spip_out" rel="external">moteur de recherche d’offres de travail destinées aux étrangers</a> (« Zamestnavatel je ochoten zamestnat cizince / Employer is willing to employ foreigners »).</p>
<p>Les candidats peuvent également s’enregistrer sur le site et <a href="http://portal.mpsv.cz/sz/zahr_zam/prociz/cv" class="spip_out" rel="external">déposer leur CV en ligne</a>.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/emploi-stage/article/recherche-d-emploi-111337). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
