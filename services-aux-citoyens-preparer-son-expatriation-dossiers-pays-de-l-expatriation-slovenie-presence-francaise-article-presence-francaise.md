# Présence française

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/presence-francaise/article/presence-francaise#sommaire_1">Ambassade et consulat de France</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/presence-francaise/article/presence-francaise#sommaire_2">Autorités françaises dans le pays</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/presence-francaise/article/presence-francaise#sommaire_3">Associations dans le pays</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Ambassade et consulat de France</h3>
<p>Consulter notre <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/article/annuaire-des-representations-francaises-a-l-etranger" class="spip_in">annuaire des ambassades et consulats de France à l’étranger</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Autorités françaises dans le pays</h3>
<p><strong>Réseau français de coopération et d’action culturelle</strong></p>
<p>Les coordonnées des services du réseau de coopération et d’action culturelle français en Slovénie ainsi que la description de l’activité de ces services se trouvent dans la rubrique culture du site internet de l’<a href="http://www.institutfrance.si/linstitut.2.html" class="spip_out" rel="external">Institut français</a> ou sur le site internet du réseau culturel : <a href="http://latitudefrance.diplomatie.gouv.fr/AnnuaireCarto.html" class="spip_out" rel="external">Latitude France</a>.</p>
<p><strong>Economie : réseau mondial</strong></p>
<p>Business France, l’Agence française pour le développement international des entreprises, <a href="http://export.businessfrance.fr/autriche/export-autriche-slovenie-avec-notre-bureau.html" class="spip_out" rel="external">est présente en Slovénie</a>. Sa mission est d’accompagner les entreprises françaises dans leur développement à l’international.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Site de la <a href="http://www.ambafrance-si.org/Business-France-Ljubljana" class="spip_out" rel="external">mission économique Business France en Slovénie</a></p>
<p>Les services économiques sont également présents. Ils sont une émanation de la Direction générale du Trésor et ont pour missions principales l’analyse macroéconomique et l’animation des relations économiques bilatérales entre la France et les pays où ils sont implantés.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Site internet du <a href="http://www.ambafrance-si.org/Service-economique-de-Ljubljana" class="spip_out" rel="external">service économique français en Slovénie</a> </p>
<p><strong>Vos élus à l’Assemblée des Français de l’étranger</strong></p>
<p>Pour toute information sur l’Assemblée des Français de l’étranger et pour connaître les conseillers et les Sénateurs qui représentent les Français établis hors de France dans votre circonscription, vous pouvez consulter les sites Internet suivants :</p>
<ul class="spip">
<li><a href="http://www.assemblee-afe.fr/" class="spip_out" rel="external">Assemblée des Français de l’étranger (AFE)</a></li>
<li>Sénat au service des Français de l’étranger : <a href="http://www.senat.fr/expatries/vos_senateurs.html" class="spip_out" rel="external">sénateurs des Français de l’étranger</a></li></ul>
<p><strong>Députés des Français de l’étranger</strong></p>
<p>La révision constitutionnelle du 23 juillet 2008, publiée au Journal officiel du 19 avril 2011, introduit la représentation à l’Assemblée nationale des Français établis hors de France. La création de onze circonscriptions législatives à l’étranger a permis aux Français expatriés d’élire leur député à l’Assemblée nationale depuis 2012. M. Pierre-Yves Leborgn’ est le député pour la 7ème circonscription (Europe centrale, orientale et du Sud-Est).</p>
<p>Pour plus d’information :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.votezaletranger.gouv.fr/" class="spip_out" rel="external">Votez à l’étranger</a> </p>
<h3 class="spip"><a id="sommaire_3"></a>Associations dans le pays</h3>
<p><strong>Associations franco-slovènes</strong></p>
<ul class="spip">
<li><strong>Le Cercle des Illyriens</strong><br class="manualbr">Le Cercle des Illyriens est une association autonome culturelle à but non lucratif dont l’activité principale est de favoriser les rencontres entre francophones et francophiles résidant en Slovénie.</li>
<li><strong>Association Slovénie-France</strong><br class="manualbr">Dunajska cesta 65, 1000 LJUBLJANA</li>
<li><a href="http://associationfranceslovenie.blogspot.fr/" class="spip_out" rel="external">Association France-Slovénie</a><br class="manualbr">2, rue de Lille, 75 007 PARIS</li>
<li><a href="http://www.petermartinc.org/?lang=fr" class="spip_out" rel="external">Association Peter Martinc</a><br class="manualbr">Garibaldijeva 18, 6000 KOPER<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/presence-francaise/article/presence-francaise#info#mc#petermartinc.org#" title="info..åt..petermartinc.org" onclick="location.href=mc_lancerlien('info','petermartinc.org'); return false;" class="spip_mail">Courriel</a>
<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> <strong> Amicale franco-slovène</strong><br class="manualbr">Ob studencnici 9, 2250 Ptuj<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/presence-francaise/article/presence-francaise#info#mc#afsp.si#" title="info..åt..afsp.si" onclick="location.href=mc_lancerlien('info','afsp.si'); return false;" class="spip_mail">Courriel</a></li></ul>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li><a href="http://www.ambafrance-si.org/Adresses-utiles" class="spip_out" rel="external">Liste des associations françaises</a> maintenue à jour par la section consulaire de l’ambassade de France à Ljubljana</li>
<li>Notre article dédié <a href="services-aux-citoyens-preparer-son-expatriation-en-savoir-plus-article-associations-des-francais-de-l-etranger.md" class="spip_in">Associations des Français de l’étranger</a></li></ul>
<p><i>Mise à jour : septembre 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/presence-francaise/article/presence-francaise). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
