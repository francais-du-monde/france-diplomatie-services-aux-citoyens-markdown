# Animaux domestiques

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/entree-et-sejour/article/animaux-domestiques#sommaire_1">Chiens, chats et furets</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/entree-et-sejour/article/animaux-domestiques#sommaire_2">Autres animaux</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/entree-et-sejour/article/animaux-domestiques#sommaire_3">Pour en savoir plus : </a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Chiens, chats et furets</h3>
<p>L’Allemagne faisant partie de l’Union européenne, ce sont les dispositions communautaires qui s’appliquent en matière d’importation d’animaux domestiques (chats, chiens et furets). A noter que le nombre d’animaux est limité à cinq. Au-delà de cinq animaux, ce sont d’autres dispositions communautaires (directive <a href="http://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=CELEX%3A31992L0065%3AFR%3AHTML" class="spip_out" rel="external">92/65/CEE</a>) relatives aux importations commerciales qui sont appliquées.</p>
<p>Les chiens, les chats et les furets doivent satisfaire aux conditions suivantes :</p>
<ul class="spip">
<li>Etre âgées de plus de trois mois ;</li>
<li>Etre identifiés par tatouage ou par puce électronique ;</li>
<li>Etre valablement vaccinés contre la rage ;</li>
<li>Être titulaires d’un passeport conforme au modèle européen délivré par un vétérinaire habilité attestant de l’identification et de la vaccination contre la rage de l’animal ;</li>
<li>Etre munis d’un carnet de vaccination. Le système d’identification électronique, ainsi que la reconnaissance de la validité de la vaccination contre la rage, peuvent varier d’un Etat membre à l’autre. Il est donc vivement recommandé de prendre contact avec <a href="http://www.paris.diplo.de/Vertretung/paris/fr/Startseite.html" class="spip_out" rel="external">l’ambassade d’Allemagne en France</a>.</li></ul>
<p>A noter qu’il existe en Allemagne un impôt sur les chiens ("Hundesteuer"). Cet impôt concerne les animaux âgés de plus de 3 mois. Il est fixé par les länder et perçu par les communes. Dans le mois suivant votre arrivée, vous devrez déclarer votre animal auprès de la commune de votre résidence. Le montant de l’impôt dépend du nombre de chiens que vous possédez et de la race de l’animal. Ainsi, la taxe est plus élevée pour les chiens de combat et les chiens dangereux.</p>
<p>Pour plus d’informations :</p>
<ul class="spip">
<li><a href="http://www.allemagne.diplo.de/contentblob/3444780/Daten/2442465/02haustiereeinfuhrBRDdatei.pdf" class="spip_out" rel="external">Ambassade d’Allemagne en France</a></li>
<li><a href="http://www.bundesfinanzministerium.de/" class="spip_out" rel="external">Ministère fédéral des Finances</a></li></ul>
<p><strong>Chiens dangereux</strong></p>
<p>La loi fédérale dispose que les races de chiens suivantes ne doivent plus être introduits ou importés sur le territoire allemand : pit-bull terrier, american Staffordshire terrier, Staffordshire bull terrier, bull terrier, ainsi que les croisements de ces races entre elles ou avec d’autres races.</p>
<p>Les chiens d’autres races ainsi que leurs croisements entre elles ou avec d’autres chiens, représentant un danger aux termes des dispositions définies par le land de résidence du chien, ne doivent pas être introduits ou importés depuis l’étranger dans ce land. La violation de cette loi peut conduire à une peine d’emprisonnement pouvant aller jusqu’à 2 ans.</p>
<p>Pour les touristes accompagnés de leur chien, le règlement sur les exceptions à l’introduction et à l’importation de chiens dangereux sur le territoire fédéral autorise le voyage dans les cas suivants :</p>
<ul class="spip">
<li>La personne qui fait entrer un chien dangereux sur le territoire fédéral séjourne moins de quatre semaines en Allemagne.</li>
<li>Le chien dangereux fait actuellement partie du contingent allemand, est introduit à l’étranger puis sera réimporté en Allemagne.</li>
<li>Le chien dangereux est autorisé dans un autre land.</li>
<li>Le chien dangereux est un chien de service, de sauvetage, de catastrophe ou d’assistance aux personnes handicapées.</li>
<li>Les propriétaires sont tenus de justifier l’identité de leur chien à l’aide des documents adéquats. Par ailleurs, ces chiens doivent être tenus en laisse et porter une muselière dans tous les länder.</li>
<li>La démarche à suivre pour les races de chiens jugées dangereuses sur le site internet de <a href="http://www.allemagne.diplo.de/contentblob/3444782/Daten/2442466/02haustierehundedatei.pdf" class="spip_out" rel="external">l’ambassade d’Allemagne en France</a>.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Autres animaux</h3>
<p>Le nombre d’oiseaux de compagnie pouvant être importés à des fins non commerciales est limité à trois, conformément au paragraphe 38 du règlement allemand relatif à la lutte contre les épizooties dans l’Union européenne (BmTierSSchV). Un certificat sanitaire est exigé uniquement pour les perroquets et perruches.</p>
<p>Avant d’introduire ou de faire transiter des animaux autres que chiens, chats, furets ou oiseaux de compagnie, doivent être respectées :</p>
<p><strong>Conditions de police sanitaire</strong></p>
<p>La loi allemande s’applique. Si un certificat sanitaire est exigé, vous devrez le solliciter auprès de la plus haute autorité vétérinaire du land par lequel vous entrerez sur le territoire de la République fédérale d’Allemagne.</p>
<p>Lapins domestiques : en cas de séjour ou d’élection de domicile en Allemagne, le nombre de lapins domestiques pouvant être importés sans avoir à solliciter d’autorisation spécifique est limité à trois.</p>
<p>Poissons d’aquarium : une autorisation d’importation conforme aux conditions de police sanitaire est obligatoire pour les poissons d’aquarium, quel que soit le nombre.</p>
<p>L’importation de toutes les autres espèces d’animaux domestiques (par exemple hamsters, cochons d’Inde, souris, tortues, reptiles et amphibiens) n’est soumise à aucune condition particulière de police sanitaire. Toutefois, le terme d’« animal domestique » étant sujet à interprétation et certaines espèces d’animaux exotiques pouvant difficilement être considérées comme domestiques – malgré la perception que peut en avoir le propriétaire – il est recommandé, en cas de doute, de vérifier si votre animal doit satisfaire des conditions de police sanitaire particulières.</p>
<p><strong>Conditions relatives à la protection des espèces</strong></p>
<p>Pour en savoir plus sur ce type de condition :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.allemagne.diplo.de/contentblob/3444778/Daten/2442464/02haustiereeinfuhralledatei.pdf" class="spip_out" rel="external">Ambassade d’Allemagne en France</a></p>
<h3 class="spip"><a id="sommaire_3"></a>Pour en savoir plus : </h3>
<ul class="spip">
<li><a href="http://agriculture.gouv.fr/" class="spip_out" rel="external">Ministère de l’Agriculture, de l’agro-alimentaire et de la forêt</a> ;</li>
<li>Bureau de l’identification et du contrôle des mouvements d’animaux du ministère de l’Agriculture : <br class="manualbr">Tél. : 01 49 55 84 59<br class="manualbr">Courriel : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/entree-et-sejour/article/animaux-domestiques#bicma.sdspa.dgal#mc#agriculture.gouv.fr#" title="bicma.sdspa.dgal..åt..agriculture.gouv.fr" onclick="location.href=mc_lancerlien('bicma.sdspa.dgal','agriculture.gouv.fr'); return false;" class="spip_mail">bicma.sdspa.dgal<span class="spancrypt"> [at] </span>agriculture.gouv.fr</a></li>
<li><a href="http://www.bmel.de/DE/Startseite/startseite_node.html" class="spip_out" rel="external">Ministère fédéral de l’Alimentation, de l’Agriculture et de la protection des consommateurs</a></li></ul>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/entree-et-sejour/article/animaux-domestiques). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
