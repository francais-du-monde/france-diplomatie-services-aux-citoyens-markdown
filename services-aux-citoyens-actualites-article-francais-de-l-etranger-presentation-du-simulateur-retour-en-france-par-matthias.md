# Français de l’étranger - Présentation du simulateur « Retour en France » par Matthias Fekl (3 février 2016)

<p class="chapo">
      Dans le cadre de la présentation des nouvelles mesures de simplification par le Premier ministre, Matthias Fekl, secrétaire d’État chargé du commerce extérieur, de la promotion du tourisme, et des Français de l’étranger, a présenté le 3 février dix mesures qui faciliteront le retour en France de nos compatriotes établis à l’étranger.
</p>
<p class="spip_document_91397 spip_documents spip_documents_center">
<img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L720xH360/simplification_twitter_cle0badbd-afc3e.jpg" width="720" height="360" alt=""></p>
<p>Le gouvernement a ainsi conçu <a href="http://retour.apps.simplicite.io/ext/REFFront" class="spip_out" rel="external">un simulateur en ligne « Retour en France »</a> qui permet aux Français de l’étranger de connaître, en fonction de leur situation particulière, les démarches à accomplir au moment de leur retour.</p>
<p>C’est l’une des mesures que le gouvernement met en œuvre suite au rapport "Retour en France des Français de l’étranger" de la sénatrice Hélène Conway-Mouret remis au Premier ministre. D’autres mesures assurant une meilleure continuité administrative entre les périodes d’expatriation et de vie en France ont été annoncées dans le domaine du logement, de l’assurance maladie, de la fiscalité ou des retraites.</p>
<p class="spip_document_91396 spip_documents spip_documents_center">
<a href="http://www.modernisation.gouv.fr/retour-en-france" class="spip_out"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L720xH360/simplification_retour_twitter2_0_cle0b6111-65391.jpg" width="720" height="360" alt=""></a></p>
<p class="document_doc">
<a class="spip_in" title="Doc:Télécharger le dossier de presse 80 nouvelles mesures pour simplifier la vie des Français , 812 ko, 0x0" href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/dp_particuliers_cle0b311d.pdf"><img width="18" height="21" alt="Doc:Télécharger le dossier de presse 80 nouvelles mesures pour simplifier la vie des Français , 812 ko, 0x0" src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L18xH21/pdf-7444b.png" style="vertical-align: middle;"></a>
<a href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/dp_particuliers_cle0b311d.pdf">Télécharger le dossier de presse 80 nouvelles mesures pour simplifier la vie des Français - (PDF, 812 ko)</a>
</p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/actualites/article/francais-de-l-etranger-presentation-du-simulateur-retour-en-france-par-matthias). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
