# Fiscalité du pays

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/fiscalite-23001/article/fiscalite-du-pays-111151#sommaire_1">Présentation </a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/fiscalite-23001/article/fiscalite-du-pays-111151#sommaire_2">Anné fiscale </a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/fiscalite-23001/article/fiscalite-du-pays-111151#sommaire_3">Quitus fiscal </a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/fiscalite-23001/article/fiscalite-du-pays-111151#sommaire_4">Solde du compte en fin de séjour </a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/fiscalite-23001/article/fiscalite-du-pays-111151#sommaire_5">Coordonnées des centres d’information fiscale </a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/fiscalite-23001/article/fiscalite-du-pays-111151#sommaire_6">Impôts directs et indirects</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Présentation </h3>
<p>Les personnes qui travaillent en Lituanie paient l’impôt sur le revenu.</p>
<p>Le taux de l’impôt sur le revenu est de 15% :</p>
<p>Le taux de 15% est également appliqué aux revenus tirés de la redistribution des profits, aux revenus des marins, aux revenus des sportifs, aux honoraires reçus d’après un contrat de droits d’auteur, aux revenus tirés de la création, aux locations de biens, à la vente ou tout autre transfert de propriété, aux revenus d’une activité individuelle si la personne a décidé de ne pas régler une somme forfaitaire pour l’attestation d’activité, les retraites de tout fonds de pension lituanien, les cotisations pour un contrat d’assurance vie, les remboursements d’un contrat d’assurance vie résilié.</p>
<p>Un impôt sur les revenus d’un montant fixé par les conseils municipaux est payé sur les revenus provenant d’une activité pour laquelle une personne a une attestation d’activité.</p>
<p>La première période fiscale des revenus, obtenus à un domicile fiscal permanent en Lituanie, d’une personne résidant temporairement en Lituanie est l’année civile pour laquelle le domicile fiscal permanent a été ou devait être enregistré.</p>
<h3 class="spip"><a id="sommaire_2"></a>Anné fiscale </h3>
<p>La période fiscale de l’impôt sur les revenus est l’année civile.</p>
<h3 class="spip"><a id="sommaire_3"></a>Quitus fiscal </h3>
<p>Un quitus fiscal est exigé avant de quitter le pays.</p>
<h3 class="spip"><a id="sommaire_4"></a>Solde du compte en fin de séjour </h3>
<p>Un expatrié peut solder son compte en fin de séjour.</p>
<h3 class="spip"><a id="sommaire_5"></a>Coordonnées des centres d’information fiscale </h3>
<p><a href="http://www.finmin.lt/" class="spip_out" rel="external">Ministère des Finances - service des politiques fiscales</a><br class="manualbr">J. Tumo-Vaizganto 8a/2<br class="manualbr">LT-01512 Vilnius<br class="manualbr">Tél. : (370) 52 39 00 00<br class="manualbr">Fax : (370) 52 79 14 81<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/fiscalite-23001/article/fiscalite-du-pays-111151#finmin#mc#finmin.lt#" title="finmin..åt..finmin.lt" onclick="location.href=mc_lancerlien('finmin','finmin.lt'); return false;" class="spip_mail">Courriel</a></p>
<p><a href="http://www.vmi.lt/lt/" class="spip_out" rel="external">Inspectorat de la taxe</a><br class="manualbr">Vasario 16-osios g. 15,<br class="manualbr">LT-01514 Vilnius<br class="manualbr">Tel. (370) 52 66 82 00<br class="manualbr">Fax (370) 52 12 56 04,<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/fiscalite-23001/article/fiscalite-du-pays-111151#vmi#mc#vmi.lt#" title="vmi..åt..vmi.lt" onclick="location.href=mc_lancerlien('vmi','vmi.lt'); return false;" class="spip_mail">Courriel</a></p>
<p>Par ailleurs, le texte de la loi encadrant l’impôt sur le revenu est disponible, en anglais, sur le <a href="http://www.lrs.lt/" class="spip_out" rel="external">site internet du Parlement lituanien</a></p>
<h3 class="spip"><a id="sommaire_6"></a>Impôts directs et indirects</h3>
<ul class="spip">
<li>Impôt sur le revenu</li>
<li>Impôt sur les sociétés</li>
<li>Impôt sur la terre</li>
<li>Impôt sur les ressources naturelles nationales</li>
<li>Impôt sur le pétrole et les ressources de gaz</li>
<li>Impôt sur la fortune</li>
<li>TVA et accises</li>
<li>Taxe sur les valeurs mobilières</li></ul>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="https://www.tresor.economie.gouv.fr/Pays/lituanie" class="spip_out" rel="external">Service économique français en Lituanie</a></p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/fiscalite-23001/article/fiscalite-du-pays-111151). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
