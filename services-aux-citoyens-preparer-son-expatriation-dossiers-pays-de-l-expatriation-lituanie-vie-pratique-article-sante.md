# Santé

<p>L’infrastructure hospitalière locale est en mesure de répondre à toutes situations d’urgence. Il existe plusieurs hôpitaux publics et cliniques privées. Les soins classiques sont de bonne qualité ainsi que dans certaines spécialités (cardiologie, ophtalmologie). La consultation en médecine libérale est quasiment inconnue. Les médecins sont regroupés dans des centres de santé privés ou publics. La consultation d’un médecin généraliste en centre varie de 26 à plus de 30€.</p>
<p>Des cas isolés de rage ont été signalés ces dernières années. Les animaux de compagnie sont soumis en conséquence à la règlementation européenne (vaccinations, tatouage, carnet d’identité européen visé par un vétérinaire avant chaque déplacement, etc.).</p>
<p>Les canalisations d’eau pouvant receler des métaux lourds, notamment dans les quartiers anciens, il est préférable de consommer de l’eau en bouteille ou filtrée.</p>
<p>En raison d’une recrudescence des cas d’encéphalites à tique, il convient d’être particulièrement prudent lors des promenades en forêt. Il devient nécessaire, pour les personnes se rendant de manière habituelle en forêt, d’envisager la vaccination des adultes et des enfants.</p>
<p>Pour un court séjour, quinze jours avant la date du départ au minimum, il faut demander à sa caisse d’assurance maladie, la délivrance d’une carte européenne d’assurance maladie.</p>
<p>Les personnes suivant un traitement médical consulteront leur médecin traitant et se muniront de leurs médicaments habituels avant de partir. Bien que la pharmacopée lituanienne comporte un grand nombre de médicaments usités en France, le dosage et le nom commercial peuvent varier.</p>
<p>Tous les voyageurs devraient contracter une assurance rapatriement.</p>
<p><strong>Numéros utiles :</strong></p>
<ul class="spip">
<li>Pompiers : 01.</li>
<li>Médecin et ambulance : 03.</li>
<li>Numéro international d’urgence : 112</li></ul>
<p>Pour les urgences médicales, s’adresser à l’hôpital Santariškes à Vilnius : +(370)5 236 51 13.</p>
<p>Une liste des médecins est disponible sur le <a href="http://www.ambafrance-lt.org/" class="spip_out" rel="external">site de l’ambassade de France en Lituanie</a></p>
<p>Pour de plus amples renseignements, vous pouvez consulter le site de l’<a href="http://www.invs.sante.fr/" class="spip_out" rel="external">INVS</a>, ainsi que celui de l’<a href="http://www.who.int/fr/index.html" class="spip_out" rel="external">OMS</a>, qui vous renseigneront sur l’état sanitaire de ce pays, ou les sites de <a href="http://www.pasteur-lille.fr/" class="spip_out" rel="external">l’institut Pasteur de Lille</a> et de <a href="http://www.pasteur.fr/" class="spip_out" rel="external">l’institut Pasteur de Paris</a>.</p>
<p>Pour une présentation des risques épidémiologiques ainsi que des conseils sur la médecine au quotidien, consultez les pages suivantes :</p>
<ul class="spip">
<li><a href="http://www.ambafrance-lt.org/Medecins-accredites-par-l" class="spip_out" rel="external">Conseils, liste de médecins et hôpitaux</a> sur le site de l’Ambassade de France en Lituanie</li>
<li><a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/lituanie/" class="spip_in">Page dédiée à la santé en Lituanie</a> sur le site Conseils aux voyageurs</li>
<li>Fiche Vilnius sur le site du <a href="http://www.cimed.org/" class="spip_out" rel="external">CIMED</a>.</li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/vie-pratique/article/sante). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
