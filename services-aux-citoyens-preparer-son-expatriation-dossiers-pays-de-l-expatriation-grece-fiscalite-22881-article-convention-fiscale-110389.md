# Convention fiscale

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/fiscalite-22881/article/convention-fiscale-110389#sommaire_1">Champ d’application de la convention</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/fiscalite-22881/article/convention-fiscale-110389#sommaire_2">Dispositions conventionnelles sur certaines catégories de revenus</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/fiscalite-22881/article/convention-fiscale-110389#sommaire_3">Elimination de la double imposition</a></li></ul>
<p>La France et la Grèce ont signé, le 21 août 1963, une convention en matière de fiscalité publiée au Journal Officiel du 2 février 1965.</p>
<p>Cette convention tend à éviter les doubles impositions en matière d’impôts sur les revenus qui pourraient résulter de l’application des législations de ces deux Etats.</p>
<p>Le texte de la convention peut être obtenu auprès de la Direction des Journaux Officiels, 26 rue Desaix 75727 Paris cedex 15, par courrier, par fax (01 40 58 77 80), ou sur le <a href="http://www.impots.gouv.fr/" class="spip_out" rel="external">site Internet du ministère des Finances</a>.</p>
<p>Les dispositions conventionnelles qui ont primauté sur les dispositions du droit interne, selon l’article 55 de la Constitution française répartissent entre les deux Etats, le droit d’imposer les revenus perçus par leurs résidents respectifs.</p>
<h3 class="spip"><a id="sommaire_1"></a>Champ d’application de la convention</h3>
<p>Cet accord a pour objet de protéger <strong>les résidents de chacun des Etats contractants</strong> en matière d’impôts prélevés directement sur le revenu ou sur le bénéfice des sociétés.</p>
<p>La convention trouve donc à s’appliquer aux résidents de ces Etats.</p>
<p>L’article 2 de la convention s’applique aux personnes qui sont considérées comme "résidents d’un Etat contractant".</p>
<p>D’après le paragraphe 3-b de ce même article, une personne est considérée comme "résident d’un Etat contractant" lorsque, en vertu de la législation dudit Etat, elle se trouve assujettie à l’impôt à raison de son domicile, de sa résidence ou de critères analogues.</p>
<p>Au paragraphe 3-b, l’article 2 fournit des critères subsidiaires permettant de résoudre le cas de double résidence si l’assujettissement à l’impôt ne pouvait suffire.</p>
<p>Ces critères sont :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  un foyer d’habitation permanent (il s’agit, par exemple, du lieu de situation du conjoint ou des enfants) ;<br class="manualbr">- l’Etat où la personne possède le centre de ses intérêts vitaux (tant professionnels que privés) ;<br class="manualbr">- l’Etat dans lequel elle séjourne de façon habituelle (notion de 183 jours de présence physique sur le territoire au cours d’une année fiscale) ;<br class="manualbr">- à défaut, l’Etat dont elle possède la nationalité.</p>
<h3 class="spip"><a id="sommaire_2"></a>Dispositions conventionnelles sur certaines catégories de revenus</h3>
<h4 class="spip">Traitements, salaires, pensions et rentes</h4>
<h5 class="spip">Rémunérations privées</h5>
<p>L’article 13, paragraphe 1 précise que les traitements et salaires d’origine privée ne sont, en règle générale, imposables que dans l’Etat où <strong>s’exerce l’activité personnelle</strong>.</p>
<p>Le maintien de l’imposition dans l’Etat de résidence du bénéficiaire est prévu par le paragraphe 2 du même article sous réserve de trois conditions simultanément remplies :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  le séjour temporaire du bénéficiaire dans l’autre Etat ne dépasse pas une durée totale de 183 jours ;<br class="manualbr">- la rémunération est payée par un employeur qui n’est pas résident de l’Etat d’exercice ;<br class="manualbr">- la rémunération ne doit pas être à la charge d’un établissement stable ou d’une base fixe de l’employeur dans l’Etat.</p>
<p>Exemple : Monsieur X est envoyé trois mois à Athènes, soit quatre-vingt-dix jours (mai, juin, juillet de l’année n) par une PME située en France, fabricant d’articles de maroquinerie, en vue de prospecter le marché hellène. Cette entreprise ne dispose d’aucune succursale ni bureau en Grèce et rémunère l’intéressé. Dans un tel cas, Monsieur X devra déclarer ses revenus en France.</p>
<p>Au contraire, si Monsieur X est envoyé du mois de février inclus au mois de novembre inclus, son séjour de plus de 183 jours en Grèce entraîne son imposition dans ce pays.</p>
<h5 class="spip">Rémunérations publiques</h5>
<p>L’article 14, paragraphe 1, que les traitements, salaires et rémunérations analogues ainsi que les <strong>pensions de retraite</strong> payés par un Etat ou une personne morale de droit public de cet Etat restent imposables dans cet Etat.</p>
<p>Exemple : Monsieur X est fonctionnaire de l’Etat français. Il résidait pendant son activité en France et décide d’aller prendre sa retraite chez l’un de ses enfants en Grèce. Les montants de ses pensions resteront imposés en France ; son dossier est alors pris en charge par un centre des impôts spécial, le Centre des Impôts des Non-résidents.</p>
<p><strong>Toutefois</strong>, en vertu de l’article 14, paragraphe 2, 1er alinéa, cette règle ne s’applique pas lorsque le bénéficiaire possède <strong>la nationalité de l’autre Etat</strong> sans être en même temps ressortissant de l’Etat payeur ; l’imposition est réservée à l’Etat dont l’intéressé est le résident.</p>
<p>Exemple : Monsieur X, <strong>citoyen français</strong>, était fonctionnaire à l’Ambassade de Grèce en France ; il prend sa retraite en France. Dans ce cas, il sera imposable en France et devra déclarer ses revenus au centre des impôts dont dépend son domicile.</p>
<p><strong>D’autre part</strong>, en vertu des dispositions du paragraphe 2, 2ème alinéa, du même article, les règles fixées au paragraphe 1 dudit article ne sont pas applicables aux rémunérations ou aux pensions versées au titre <strong>de services rendus dans le cadre d’une activité industrielle ou commerciale exercée par un Etat ou une personne morale de droit public.</strong></p>
<p>Les sommes versées à ce titre sont imposées soit dans l’Etat d’exercice de l’activité (article 13 de la convention), soit dans l’Etat de résidence du bénéficiaire (article 12 de la convention).</p>
<p>Exemples :</p>
<ul class="spip">
<li>Monsieur X, agent E.D.F., est envoyé en Grèce afin d’effectuer des travaux de conception avec les services locaux grecs. Monsieur X est rémunéré par E.D.F. France. E.D.F. étant un établissement de droit public à caractère industriel et commercial, les rémunérations allouées à Monsieur X seront imposées en Grèce.</li>
<li>Monsieur X, retraité de la S.N.C.F., perçoit une pension de la caisse vieillesse de la S.N.C.F. Or, Monsieur X, résident en France, a décidé de vivre à Athènes. Cette retraite se verra ainsi imposable en Grèce puisque la S.N.C.F. a le caractère d’un établissement public, industriel et commercial.</li></ul>
<h5 class="spip">Pensions et rentes</h5>
<p>L’article 12 prévoit que les pensions de retraite de source privée ainsi que les rentes viagères restent imposables dans l’Etat dont le bénéficiaire est un résident.</p>
<p>Cette disposition couvre également les pensions publiques auxquelles les règles de l’article 14 ne sont pas applicables.</p>
<p>En outre, la convention ne comportant aucune disposition particulière concernant les prestations de sécurité sociale, il en résulte que les pensions privées versées par la Sécurité sociale française à des résidents de Grèce échappent à l’impôt français et sont par conséquent imposées en Grèce.</p>
<p>Exemple : Monsieur X, citoyen français qui a exercé une activité salariée en Grèce, décide de venir prendre sa retraite en France. Les pensions versées par la Grèce au titre de cette activité sont imposables en France.</p>
<h5 class="spip">Etudiants, stagiaires, enseignants</h5>
<p>L’article 17 de la convention prévoit que les étudiants, les stagiaires d’un Etat qui séjournent dans l’autre Etat à seule fin d’y poursuivre leurs études ou leur formation et qui perçoivent des subsides d’origine étrangère à cet Etat sont exonérés d’impôt par ce dernier Etat.</p>
<p>L’article 19 précise que les rémunérations versées aux enseignants résidents d’un Etat se rendant temporairement dans l’autre Etat en vue d’y exercer une activité pédagogique pendant une période <strong>ne dépassant pas deux ans</strong>, dans une université, un collège, une école ou autre établissement restent imposables dans l’Etat de résidence.</p>
<h4 class="spip">Autres catégories de revenus</h4>
<h5 class="spip">Bénéfices industriels et commerciaux</h5>
<p>L’article 4, paragraphe 1, dispose que les entreprises industrielles et commerciales sont imposables sur le territoire où se trouve un établissement stable.</p>
<h5 class="spip">Bénéfices des professions non commerciales et des revenus non commerciaux</h5>
<p>L’article 16, paragraphe 1, stipule que les revenus provenant de l’exercice d’une profession libérale ou d’autres activités indépendantes sont imposables dans l’Etat sur le territoire duquel se trouve l’installation permanente ou base fixe où s’exerce de façon régulière l’activité personnelle.</p>
<p>Les revenus que les professionnels du spectacle ainsi que les sportifs réalisent en cette qualité dans l’un des deux Etats restent imposables dans l’Etat d’exercice de l’activité selon les dispositions de l’article 18 de la convention.</p>
<p>L’article 11, paragraphe 2, pose en principe que les revenus non commerciaux (redevances et droits d’auteur) sont imposables dans l’Etat d’où proviennent ces revenus à un taux qui ne peut excéder 5%.</p>
<h5 class="spip">Revenus immobiliers</h5>
<p>L’article 3, paragraphe 1, dispose que les revenus des biens immobiliers y compris les bénéfices des exploitations agricoles sont imposables dans l’Etat où ils sont situés.</p>
<h5 class="spip">Revenus de capitaux mobiliers</h5>
<p><strong>Les dividendes</strong></p>
<p>Ce terme désigne les revenus provenant d’actions, bons de jouissance, parts de fondateur ou autres parts bénéficiaires.</p>
<p>De manière générale, l’article 9 reprend d’une part la règle suivant laquelle les dividendes payés par une société qui est un résident d’un Etat contractant à un résident de l’autre Etat sont imposables dans cet autre Etat suivant les règles de sa loi interne, hormis le cas où le bénéficiaire possède un établissement stable dans l’Etat de la source et où la participation génératrice des dividendes fait partie de cet établissement.</p>
<p><strong>Les intérêts</strong></p>
<p>Ce terme désigne les revenus des fonds publics, des obligations d’emprunts, des obligations d’emprunts et des créances de toute nature ainsi qu’aux intérêts produits par des titres négociables, les bons de caisse et les intérêts de créances ordinaires.</p>
<p>L’article 10 précise que les intérêts provenant d’un Etat et payés à un résident de l’autre Etat restent imposables dans l’Etat de la source aux taux prévus au paragraphe 3 dudit article. Ainsi, l’imposition en France des intérêts versés à des résidents de Grèce ne peut excéder 12% et les intérêts de source grecque versés à des résidents de France sont imposables en Grèce.</p>
<h3 class="spip"><a id="sommaire_3"></a>Elimination de la double imposition</h3>
<p>L’élimination de la double imposition pour les résidents de France qui perçoivent des revenus de source grecque s’opère aux termes du paragraphe 1 de l’article 21-A selon le régime de l’imposition exclusive ou de l’imputation.</p>
<p>Selon le ministère grec des Finances, « l’article 14 §1 n’établit pas de droit exclusif en faveur de l’Etat de la source, tandis que l’Etat de résidence conserve son pouvoir fiscal, si un tel pouvoir est établi par son droit interne. L’élimination de la double imposition s’effectue conformément à l’article 21, aux termes duquel l’impôt versé en France est déduit de l’impôt grec, à concurrence du montant correspondant à l’impôt dudit revenu en Grèce ».</p>
<p>Le régime de l’imposition exclusive est celui prévu pour la totalité des revenus à l’exception des dividendes, des intérêts et des redevances.</p>
<p>Le régime de l’imputation concerne les revenus visés aux articles 9 à 11 de la convention (redevances, intérêts et dividendes). Il s’agit en fait de la règle du partage du droit d’imposition entre l’Etat de la source des revenus et l’Etat de résidence du bénéficiaire.</p>
<p><i>Mise à jour : novembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/fiscalite-22881/article/convention-fiscale-110389). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
