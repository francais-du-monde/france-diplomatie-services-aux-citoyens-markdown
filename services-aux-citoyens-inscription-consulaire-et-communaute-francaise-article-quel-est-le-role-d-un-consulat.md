# Quel est le rôle d’un consulat ?

<p class="spip_document_93699 spip_documents spip_documents_center">
<a href="services-aux-citoyens-publications-article-que-font-les-consulats-pour-vous.md" class="spip_in"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L720xH360/que_fait_un_consulat_maj_juin2016_rs_cle4ae586-d2be4.png" width="720" height="360" alt=""></a></p>
<p><i>(Cliquez sur l’image pour voir l’intégralité de l’infographie)</i></p>
<p>Si vous êtes résident dans un pays étranger : pensez à vous faire enregistrer, dès votre installation, auprès du consulat de France ou de la section consulaire de l’ambassade de France.</p>
<p><strong>Ce qu’un consulat de France peut faire :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  En cas de perte ou de vol de document le consulat pourra : </p>
<p>Vous procurer des attestations en cas de perte ou de vol de documents, passeport, carte nationale d’identité ou permis de conduire sur présentation d’une déclaration faite préalablement auprès des autorités locales de police.</p>
<p>Il pourra aussi :</p>
<ul class="spip">
<li>après vérification, vous délivrer un laissez-passer pour permettre votre seul retour en France <br class="manualbr">ou</li>
<li>après consultation de l’autorité émettrice (préfecture ou consulat ayant établi votre passeport) vous délivrer un nouveau passeport.</li></ul>
<ul class="spip">
<li>En cas de difficultés financières, le Consul pourra vous indiquer le moyen le plus efficace pour que des proches puissent vous faire parvenir rapidement la somme d’argent dont vous avez besoin.</li>
<li>En cas d’arrestation ou d’incarcération, vous pouvez demander que le consulat soit informé. Le Consul pourra faire savoir aux autorités locales que vous êtes sous la protection consulaire de la France et s’enquérir du motif de votre arrestation. Si vous en êtes d’accord, il préviendra votre famille et sollicitera les autorisations nécessaires pour pouvoir vous rendre visite. Il s’assurera ainsi des conditions de détention et du respect des lois locales. Pour vous aider judiciairement, le consul vous proposera le choix d’un avocat qui pourra vous défendre (vous devrez rémunérer les services de cet avocat).</li>
<li>En cas de maladie, le consulat pourra vous mettre en relation avec un médecin agréé par ses services et tiendra à votre disposition, dans la mesure du possible, une liste de médecins spécialisés. Dans tous les cas, les honoraires restent à votre charge.</li>
<li>En cas d’accident grave, le consulat pourra prévenir votre famille et envisager avec elle les mesures à prendre : hospitalisation ou rapatriement (les frais engagés demeurant à votre charge, il est vivement conseillé se souscrire une assurance rapatriement).</li>
<li>En cas de décès, le consulat prend contact avec la famille pour l’aviser et la conseiller dans les formalités légales de rapatriement ou d’inhumation de la dépouille mortelle ou de ses cendres. Les frais sont assumés soit par la famille, soit par l’organisme d’assurance du défunt.</li>
<li>En cas de difficultés diverses avec les autorités locales ou des particuliers, le consulat pourra vous conseiller, vous fournir des adresses utiles (administrations locales, avocats, interprètes, etc.).</li></ul>
<p><strong>Ce qu’un consulat de France ne peut pas faire :</strong></p>
<ul class="spip">
<li>Vous rapatrier aux frais de l’État, sauf dans le cas d’une exceptionnelle gravité et sous réserve d’un remboursement ultérieur.</li>
<li>Régler une amende, votre note d’hôtel, d’hôpital ou toute autre dépense engagée par vous.</li>
<li>Vous avancer de l’argent sans la mise en place préalable d’une garantie.</li>
<li>Vous délivrer un passeport dans la minute.</li>
<li>Intervenir dans le cours de la justice pour obtenir votre libération si vous êtes impliqué dans une affaire judiciaire ou accusé d’un délit commis sur le territoire d’un pays d’accueil.</li>
<li>Se substituer aux agences de voyage, au système bancaire ou aux compagnies d’assurance.</li>
<li>Assurer officiellement votre protection consulaire si vous possédez aussi la nationalité du pays dans lequel vous voyagez.</li></ul>
<p><i>Mise à jour : juin 2016</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/inscription-consulaire-et-communaute-francaise/article/quel-est-le-role-d-un-consulat). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
