# Communications

<p><strong>Téléphone – Internet</strong></p>
<p>Les communications téléphoniques et télégraphiques sont excellentes et rapides.</p>
<p>Les principaux fournisseurs (les mêmes compagnies offrent les deux services) sont :</p>
<ul class="spip">
<li><a href="http://www.saunalahti.fi/" class="spip_out" rel="external">Saunalahti</a></li>
<li><a href="http://www.dna.fi/" class="spip_out" rel="external">Dna</a></li>
<li><a href="http://www.sonera.fi/" class="spip_out" rel="external">Sonera</a></li>
<li><a href="http://www.elisa.fi/" class="spip_out" rel="external">Elisa</a></li></ul>
<p>L’indicateur téléphonique de la Finlande à partir de la France est le 00 358 (Finlande) et le 9 pour Helsinki.</p>
<p>Téléphoner gratuitement par Internet Il est généralement très avantageux d’utiliser <strong>des logiciels de téléphonie sur IP</strong> (Skype, Google talk, Live messenger, Yahoo ! messenger, etc.) car les tarifs proposés à l’international sont souvent moindre que ceux des opérateurs locaux exploitant des lignes classiques, voire gratuits dans certaines conditions.</p>
<p><strong>Poste</strong></p>
<p>Le délai pour les liaisons postales est de deux à trois jours pour le courrier, 4 à 10 jours pour les colis. La distribution est quotidienne.</p>
<p>Site de la poste finlandaise : <a href="http://www.posti.fi/index-en.html" class="spip_out" rel="external">http://www.posti.fi/english/</a></p>
<p><i>Mise à jour : janvier 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/vie-pratique/article/communications). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
