# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/vie-pratique/article/cout-de-la-vie-109812#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/vie-pratique/article/cout-de-la-vie-109812#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/vie-pratique/article/cout-de-la-vie-109812#sommaire_3">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>La Belgique appartient à la zone Euro. Il n’existe pas de contrôle des changes. Plus de 150 banques étrangères sont installées en Belgique. Les quelques banques françaises établies dans le pays n’ont qu’une activité de banque d’affaires.</p>
<p>Les principaux groupes en Belgique sont Fortis Banque, KBC, Dexia, ING.</p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<p>Les chèques sont peu utilisés. On leur préfère les virements bancaires et les cartes de paiement (Bancontact/Mister Cash ou Proton, système de porte-monnaie électronique). Les cartes de crédit sont largement répandues (Carte Visa, Master Card).</p>
<p>Les banques sont ouvertes du lundi au vendredi de 9h00 à 15h30 au plus tôt ou 17h00 au plus tard. Certaines sont ouvertes le samedi matin.</p>
<h3 class="spip"><a id="sommaire_3"></a>Evolution des prix</h3>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/vie-pratique/article/cout-de-la-vie-109812). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
