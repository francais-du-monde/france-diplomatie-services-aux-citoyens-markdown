# Stages

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/emploi-stage/article/stages#sommaire_1">Stage professionnel</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/emploi-stage/article/stages#sommaire_2">Avez-vous pensé au Volontariat International (V.I.E ou V.I.A.) ?</a></li></ul>
<p>Différents moyens de recherche s’offrent à vous, lesquels passent principalement par :</p>
<ul class="spip">
<li>Le réseau relationnel personnel ;</li>
<li>Le réseau des anciens de votre université ;</li>
<li>Les répertoires d’entreprises et d’organismes (et leurs sites web) comme Kompass ;</li>
<li>Les candidatures spontanées auprès d’organismes bien déterminés ;</li>
<li>Les chambres de commerce ;</li>
<li>Les nombreux sites d’offres de stage.</li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Stage professionnel</h3>
<p>Attention à ne pas confondre le stage obligatoire avec le stage volontaire :</p>
<ul class="spip">
<li>le stage obligatoire est un stage de formation obligatoire effectué auprès d’un établissement ou d’une entreprise d’accueil au Luxembourg dans le cadre d’un enseignement ou d’une formation dispensés par un établissement d’enseignement secondaire ou supérieur.<br class="manualbr">L’établissement ou l’entreprise d’accueil au Luxembourg signe avec le stagiaire une <a href="http://www.guichet.public.lu/citoyens/fr/travail-emploi/types-contrat-travail/contrat-jeunes-actifs/convention-stage/index.html" class="spip_out" rel="external">convention ou un contrat de stage</a> qui <strong>ne peut pas comporter un salaire</strong>. Cependant la convention peut prévoir des avantages matériels comme le logement gratuit, les frais de repas ou de l’argent de poche. Ce type de stage obligatoire est aussi dénommé "stage non rémunéré" ;</li>
<li>le stage volontaire est un stage qui comporte un contrat de travail et un salaire. Il est de ce fait assimilé à un emploi. Il est aussi dénommé stage rémunéré.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Avez-vous pensé au Volontariat International (V.I.E ou V.I.A.) ?</h3>
<p>Le V.I.E. permet aux entreprises françaises de confier à un jeune ressortissant de l’espace économique européen, âgé de 18 à 28 ans, une mission professionnelle (rémunérée) à l’étranger durant une période modulable de 6 à 24 mois.</p>
<p>Pour en savoir plus :</p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/emplois-stages-concours/volontaires-internationaux/" class="spip_in">Page thématique sur le volontariat international sur notre site</a></li>
<li><a href="http://www.civiweb.com/FR/index.aspx" class="spip_out" rel="external">Civiweb.com</a></li>
<li><a href="http://export.businessfrance.fr/default.html" class="spip_out" rel="external">Business France.fr</a> - <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/emploi-stage/article/stages#infoVIE#mc#ubifrance.fr#" title="infoVIE..åt..ubifrance.fr" onclick="location.href=mc_lancerlien('infoVIE','ubifrance.fr'); return false;" class="spip_mail">courriel</a></li>
<li><a href="http://www.guichet.public.lu/citoyens/fr/enseignement-formation/etudes-superieures/activite-professionnelle/convention-stage/index.html" class="spip_out" rel="external">Guichet public - études supérieures - convention-stage</a></li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/emploi-stage/article/stages). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
