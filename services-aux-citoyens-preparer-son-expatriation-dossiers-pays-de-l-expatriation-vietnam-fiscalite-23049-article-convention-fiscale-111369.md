# Convention fiscale

<p>La France et le Vietnam ont signé le 10 février 1993 une convention en matière de fiscalité, publiée au Journal officiel du 20 décembre 1994.</p>
<p>Cette convention tend à éviter les doubles impositions en matière d’impôts sur les revenus et sur la fortune qui pourraient résulter de l’application des législations de ces deux Etats.</p>
<p>En vertu de l’article 55 de la Constitution, les conventions fiscales, qui répartissent entre les Etats le droit d’imposer, prévalent sur les dispositions fiscales de droit interne.</p>
<p>Le texte de la convention fiscale franco-vietnamienne est disponible sur le site de l’<a href="http://www.impots.gouv.fr" class="spip_out" rel="external">administration fiscale</a></p>
<p><i>Mise à jour : février 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/fiscalite-23049/article/convention-fiscale-111369). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
