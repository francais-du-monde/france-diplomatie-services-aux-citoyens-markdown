# Curriculum vitae

<p><strong>Rédaction</strong></p>
<p>Le format recommandé à adopter est le modèle anglo-saxon : pragmatique, tourné vers les réalisations professionnelles, pouvant dépasser une page.</p>
<p>Il devra ad minima être rédigé en anglais, et éventuellement en français si le candidat postule auprès d’une entreprise française implantée en Thaïlande.</p>
<p><strong>Construire son CV</strong></p>
<p>Outre les règles habituelles s’appliquant au CV, veillez particulièrement aux points suivants :</p>
<ul class="spip">
<li>Rédiger un objectif professionnel en en tête (ex : <i>professional statement</i> ou <i>professional objective</i>) intégrant également un résumé en une ou deux lignes de son expérience professionnelle</li>
<li>Illustrer ses responsabilités passées à travers des exemples de réalisations concrètes (chiffrées si possible) et significatives par rapport à l’objectif professionnel</li>
<li>Mettre en avant et expliciter les diplômes obtenus et les compétences linguistiques, éléments clés différenciateurs</li>
<li>Prévoir et faire mention de références (anciens employeurs pouvant être contactés par une entreprise pour appuyer votre profil)</li></ul>
<p><strong>Envoi du CV</strong></p>
<p>En Thaïlande où la dimension de réseau est très ancrée, il est fortement conseillé de faire parvenir sa candidature en bénéficiant de la recommandation ou de l’introduction d’une personne connaissant le destinataire de la candidature (salarie, ex-salarié, client, fournisseur, connaissances personnelles).</p>
<p>A défaut, sachez à qui vous envoyez votre candidature, en la nommant précisément.</p>
<p><strong>Contact utile :</strong></p>
<p>La Chambre de commerce franco-thaïe (CCFT) dispose d’un département Emploi dont la mission est notamment, d’accompagner les ressortissants français dans leur recherche d’emploi.</p>
<ul class="spip">
<li>Des offres d’emploi d’entreprises françaises sont <a href="http://www.francothaicc.com/" class="spip_out" rel="external">publiées sur son site internet</a></li>
<li>Des ateliers sont régulièrement organisés chaque moisdans les locaux de la CCFT à Bangkok :
<br>— Techniques et outils de recherche d’emploi + atelier pratique autour du CV
<br>— Le réseau, porte d’entrée clé dans sa recherche d’emploi en Thaïlande
<br>— Réussir ses entretiens de recrutement</li></ul>
<p>Pour toute information complémentaire, merci de prendre contact avec le département Emploi de la CCFT : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/emploi-stage/article/curriculum-vitae-114431#employment#mc#francothaicc.com#" title="employment..åt..francothaicc.com" onclick="location.href=mc_lancerlien('employment','francothaicc.com'); return false;" class="spip_mail">employment<span class="spancrypt"> [at] </span>francothaicc.com</a></p>
<p><i>Mise à jour : juillet 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/emploi-stage/article/curriculum-vitae-114431). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
