# Règlementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/emploi-stage/article/reglementation-du-travail#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/emploi-stage/article/reglementation-du-travail#sommaire_2">Emploi du conjoint</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/emploi-stage/article/reglementation-du-travail#sommaire_3">Cotisations sociales</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/emploi-stage/article/reglementation-du-travail#sommaire_4">Fêtes légales</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<p>En Suisse, le droit du travail est régi par le principe d’égalité inscrit à l’article 8 de la Constitution suisse. En matière de droit du travail, le code des Obligations, les conventions collectives de travail (CCT) et les règlements du personnel peuvent intervenir. Cela dépend de l’entreprise, de la branche d’activité, etc.</p>
<p>S’il n’existe pas de convention collective de travail pour la branche d’activité ou l’entreprise, c’est le code des Obligations qui s’applique. Dans beaucoup de cas, les points importants du contrat (par exemple, le temps de travail) sont définis dans le cadre d’une CCT ou d’un règlement d’entreprise.</p>
<p>En général, l’entreprise vous indiquera, lors de la signature du contrat de travail, dans quel cadre juridique elle se trouve (CCT de branche, CCT d’entreprise, règlement d’entreprise ou code des obligations).</p>
<p>L’employé doit, en plus des assurances sociales obligatoires dont les cotisations sont prélevées à la source, être assuré contre la maladie. Contrairement à ce qui se passe en France, l’employeur ne prend pas systématiquement à sa charge une partie des cotisations à l’assurance maladie, celle-ci restant à la charge intégrale de l’employé, sauf négociation contraire. Dans certains cantons, l’employeur s’assure que cette condition est remplie lorsqu’il procède à un recrutement.</p>
<p>Il n’y a pas de salaire minimum légal au niveau national ou cantonal. Certaines conventions collectives prévoient cependant des salaires minimum.</p>
<p>Le droit du travail est en Suisse plus souple qu’en France, notamment en matière de résiliation du contrat de travail. Pour un contrat à durée indéterminée, la période d’essai est de trois mois au maximum, sans durée minimum imposée, avec un préavis de sept jours. La durée hebdomadaire de travail est en moyenne de 42 heures et ne peut dépasser 45 à 50 heures selon le secteur d’activité.</p>
<p>Le délai de congé (ou préavis) dépend de votre ancienneté et du cadre juridique de l’entreprise. Le code des Obligations prévoit un préavis maximum de trois mois à partir de neuf ans d’ancienneté.</p>
<p>Sauf en cas de faute grave, le délai de congé est systématiquement appliqué et il est particulièrement mal vu de vouloir y déroger. A l’inverse de ce qui se passe en France, il n’est pas habituel, sauf cas particulier, de demander des indemnités de licenciement.</p>
<p><strong>Pour en savoir plus</strong></p>
<ul class="spip">
<li>Le <a href="http://www.seco.admin.ch/" class="spip_out" rel="external">secrétariat d’Etat à l’Economie</a> (SECO) vous informe sur le droit du travail : rubrique Thèmes / Travail.</li>
<li><a href="http://www.travailler-en-suisse.ch/" class="spip_out" rel="external">Travailler en Suisse</a></li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Emploi du conjoint</h3>
<p>Dans le cadre du rapprochement familial, l’étranger obtient un permis de séjour du même type que celui de son conjoint qui travaille. Le permis de séjour permet de résider en Suisse et d’y chercher du travail. Si le conjoint trouve un emploi, le permis de séjour est alors transformé en permis de travail.</p>
<p>Les personnes qui vivent en concubinage peuvent également se prévaloir du rapprochement familial, mais sous conditions de ressources financières. Selon un barème établi par les cantons, les revenus du ménage doivent être supérieurs à un certain seuil, faute de quoi le permis de séjour peut être refusé. Le logement doit également être adapté à la taille de la famille.</p>
<h3 class="spip"><a id="sommaire_3"></a>Cotisations sociales</h3>
<p>Les cotisations sociales sont de plusieurs sortes :</p>
<ul class="spip">
<li>certaines, comme l’assurance vieillesse et survivants (AVS), l’assurance invalidité (AI) et l’assurance perte de gain (APG), ont le même mode de calcul pour tout le monde, quels que soient l’entreprise et le canton ;</li>
<li>certaines sont propres à l’entreprise. C’est le cas de la prévoyance professionnelle (2ème pilier) qui représente en général la partie la plus importante des charges.</li></ul>
<p>Le mode de calcul de chacune des charges, ainsi que le taux de cotisation appliqué, sont très différents. Il est par conséquent difficile de donner une moyenne. On estime que, pour un travailleur âgé de 30 ans, les charges sociales représentent entre 12 et 14% du salaire brut.</p>
<p>Selon le type de permis de travail, le travailleur étranger est imposé ou non à la source sur ses revenus. L’impôt est ainsi directement prélevé sur le salaire par l’employeur, lequel reverse ensuite le montant à l’administration fiscale du canton.</p>
<p>Le salaire net versé sur le compte bancaire est donc le salaire net duquel a été déduit l’impôt si le travailleur étranger y est soumis.</p>
<p>Le site <a href="http://www.expatwire.fr/" class="spip_out" rel="external">Expatwire</a> propose un outil gratuit permettant de calculer son salaire net. Il explique également en détail les différents prélèvements sur votre salaire.</p>
<h3 class="spip"><a id="sommaire_4"></a>Fêtes légales</h3>
<p>Les jours fériés en Suisse sont toujours cantonaux, à l’exception du 1er août qui est la Fête nationale. Tous les cantons ont cependant désigné comme jours de repos officiels le Nouvel An, l’Ascension et Noël.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.fcal.ch/index.php?hl=fr&amp;geo=3056" class="spip_out" rel="external">Liste des jours fériés par canton sur le site Feiertagskalender</a></p>
<p><i>Mise à jour : février 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/emploi-stage/article/reglementation-du-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
