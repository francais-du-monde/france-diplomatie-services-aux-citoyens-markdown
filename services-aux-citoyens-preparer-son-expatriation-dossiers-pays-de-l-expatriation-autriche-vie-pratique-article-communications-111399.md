# Communications

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/vie-pratique/article/communications-111399#sommaire_1">Téléphone – Internet</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/vie-pratique/article/communications-111399#sommaire_2">Poste</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Téléphone – Internet</h3>
<p>Les liaisons téléphoniques ne posent aucun problème.</p>
<p>Les cartes téléphoniques s’achètent dans les bureaux de poste, les gares et dans les bureaux de tabac.</p>
<p>Pour appeler de France en Autriche, il convient de composer le 0043 + l’indicatif de la ville puis le numéro de téléphone. Les indicatifs des villes sont : Vienne – 1 ; Graz – 316 ; Innsbruck – 512 ; Salzbourg – 662 ; Linz – 732. Pour appeler d’Autriche en France, composez le 0033 + le numéro du correspondant sans le 0 initial.</p>
<p>L’Autriche est un des pays d’Europe présentant le plus grand nombre de foyers connectés à internet.</p>
<h4 class="spip">Utilisation de votre portable français</h4>
<p>Vous pouvez utiliser votre téléphone portable partout en Europe et dans de nombreuses autres régions du monde, grâce à la norme technique GSM de l’Union européenne. Les personnes qui utilisent leur téléphone portable à l’étranger réalisent d’importantes économies – de l’ordre de 60 % – grâce au règlement de l’UE sur l’itinérance (« roaming »), qui plafonne le coût des appels téléphoniques effectués ou reçus dans un autre pays de l’UE (« eurotarif »). De plus, une facturation à la seconde a été introduite après les premières 30 secondes pour les appels effectués, et dès la première seconde pour les appels reçus.</p>
<p>Les clients reçoivent un message automatique les informant du coût des appels, des SMS et des services de transmission de données à l’étranger (en cas de navigation sur internet ou de téléchargement d’un film, p. ex.). Les prix de gros que les opérateurs se facturent mutuellement sont également plafonnés, ce qui devrait réduire les coûts supportés par les consommateurs. Un mécanisme a également été mis en place afin d’éviter aux consommateurs de devoir payer des factures astronomiques pour des services d’itinérance : après un premier avertissement, la connexion internet mobile est suspendue dès que le montant atteint un certain niveau. Une limite de 50 euros par mois s’applique par défaut si le client n’a pas défini un montant spécifique.</p>
<p>Vous trouverez sur un <a href="http://ec.europa.eu/information_society/activities/roaming/index_en.htm" class="spip_out" rel="external">site web de l’UE</a>, en anglais) les eurotarifs appliqués par les opérateurs des 28 pays membres, ainsi que les liens vers leurs sites. Vous pourrez également y comparer les tarifs pratiqués pour l’envoi de SMS ou l’utilisation de services de données.</p>
<p>Source : <a href="http://europa.eu/travel/comm/index_fr.htm#phone" class="spip_out" rel="external">http://europa.eu/travel/comm/index_fr.htm#phone</a></p>
<h4 class="spip">Téléphoner gratuitement par Internet </h4>
<p>Il est généralement très avantageux d’utiliser <strong>des logiciels de téléphonie sur IP</strong> (Skype, Google talk, Live messenger, Yahoo ! messenger, etc.) car les tarifs proposés à l’international sont souvent moindre que ceux des opérateurs locaux exploitant des lignes classiques, voire gratuits dans certaines conditions.</p>
<h3 class="spip"><a id="sommaire_2"></a>Poste</h3>
<p>Les liaisons postales sont quotidiennes. Il faut compter deux à cinq jours d’acheminement pour le courrier entre la France et l’Autriche.</p>
<p>Site internet de la <a href="http://www.post.at/" class="spip_out" rel="external">poste autrichienne</a></p>
<p><i>Mise à jour : février 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/vie-pratique/article/communications-111399). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
