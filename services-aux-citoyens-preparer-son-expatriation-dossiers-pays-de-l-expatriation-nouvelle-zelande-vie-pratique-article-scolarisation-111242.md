# Scolarisation

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/vie-pratique/article/scolarisation-111242#sommaire_1">Les établissements scolaires français en Nouvelle-Zélande</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/vie-pratique/article/scolarisation-111242#sommaire_2">Enseignement supérieur</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Les établissements scolaires français en Nouvelle-Zélande</h3>
<p>Pour toute information sur la scolarisation dans le système français à l’étranger, vous pouvez consulter nos thématiques sur les <a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures.md" class="spip_in">Etudes supérieures</a> et la <a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais.md" class="spip_in">Scolarité en français</a>.</p>
<p>Vous y trouverez des renseignements sur :</p>
<ul class="spip">
<li>les établissements français du primaire et du secondaire à l’étranger ;</li>
<li>les bourses scolaires et la prise en charge des frais de scolarité à l’étranger ;</li>
<li>les possibilités qui s’offrent à vous si votre enfant ne peut être scolarisé à l’étranger dans le système français (enseignement à distance par le CNED, programme français langue maternelle (FLAM), internats en France) ;</li>
<li>les épreuves du baccalauréat à l’étranger ;</li>
<li>les bourses d’études supérieures en France et à l’étranger ;</li>
<li>l’équivalence des diplômes.</li></ul>
<p>Pour en savoir plus :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.aefe.fr/" class="spip_out" rel="external">Agence pour l’enseignement français à l’étranger</a></p>
<h3 class="spip"><a id="sommaire_2"></a>Enseignement supérieur</h3>
<p>Le NCEA level 3 permet de poursuivre des études universitaires en Nouvelle-Zélande.</p>
<p>La Nouvelle-Zélande compte <strong>huit universités</strong> situées dans les principales villes du pays :</p>
<table class="spip">
<thead><tr class="row_first"><th id="idf12c_c0">Universités </th><th id="idf12c_c1">Localisation </th><th id="idf12c_c2">Nombre d’étudiants (2011) </th><th id="idf12c_c3">Fondée en… </th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="idf12c_c0">University of Auckland</td>
<td headers="idf12c_c1">Auckland</td>
<td class="numeric " headers="idf12c_c2">32 193</td>
<td class="numeric " headers="idf12c_c3">1883</td></tr>
<tr class="row_even even">
<td headers="idf12c_c0">University of Otago</td>
<td headers="idf12c_c1">Dunedin</td>
<td class="numeric " headers="idf12c_c2">21 728</td>
<td class="numeric " headers="idf12c_c3">1869</td></tr>
<tr class="row_odd odd">
<td headers="idf12c_c0">Massey University</td>
<td headers="idf12c_c1">Palmerston North</td>
<td class="numeric " headers="idf12c_c2">19 519</td>
<td class="numeric " headers="idf12c_c3">1927</td></tr>
<tr class="row_even even">
<td headers="idf12c_c0">Auckland University of Technology</td>
<td headers="idf12c_c1">Auckland</td>
<td class="numeric " headers="idf12c_c2">18 518</td>
<td class="numeric " headers="idf12c_c3">2000</td></tr>
<tr class="row_odd odd">
<td headers="idf12c_c0">Victoria University of Wellington</td>
<td headers="idf12c_c1">Wellington</td>
<td class="numeric " headers="idf12c_c2">16 871</td>
<td class="numeric " headers="idf12c_c3">1897</td></tr>
<tr class="row_even even">
<td headers="idf12c_c0">University of Canterbury</td>
<td headers="idf12c_c1">Christchurch</td>
<td class="numeric " headers="idf12c_c2">13 553</td>
<td class="numeric " headers="idf12c_c3">1873</td></tr>
<tr class="row_odd odd">
<td headers="idf12c_c0">University of Waikato</td>
<td headers="idf12c_c1">Hamilton</td>
<td class="numeric " headers="idf12c_c2">10 348</td>
<td class="numeric " headers="idf12c_c3">1964</td></tr>
<tr class="row_even even">
<td headers="idf12c_c0">Lincoln University</td>
<td headers="idf12c_c1">Lincoln</td>
<td class="numeric " headers="idf12c_c2">3 843</td>
<td class="numeric " headers="idf12c_c3">1878</td></tr>
</tbody>
</table>
<p>Elles offrent des formations selon le système éducatif anglo-saxon Bachelor-Master-Doctorate, comparable au nouveau modèle commun européen LMD.</p>
<p>Un niveau d’anglais au-dessus de 550 au TOEFL, de 80 au TOEFL IBT ou de 6.0 au IELTS est nécessaire pour pouvoir étudier à l’université en Nouvelle-Zélande en tant que<i>undergraduate </i>(premières années de licence).</p>
<p>À la différence de la France, en Nouvelle-Zélande toutes les matières (y compris la musique, l’architecture et les arts) sont enseignées dans les universités. Les « Polytechs » ou « Institutes of Technology » sont des établissements qui offrent des formations à vocation professionnelle.</p>
<p>Les droits d’inscription pour les étrangers y sont très élevés (à partir de 15 000 - 20 000$ NZ environ,soit 8875 - 11 835 € par an). Le statut de résident permanent permet de bénéficier du tarif appliqué aux Néo-Zélandais (domestic students).</p>
<p><strong>Diplômes</strong></p>
<ul class="spip">
<li>Bachelor’s ou Bachelor’s with Honours : niveau équivalent au DEUG ou à la licence, en trois ou quatre ans.</li>
<li>Master’s ou Master’s with Honours : niveau équivalent à la Maîtrise ou au Master, en 5 ans (un Master’s degree with Honours se fait en deux ans après un Bachelor’s degree ; un simple Master’s degree peut se faire en un an après un Bachelor’s degree with honours).</li>
<li>Doctoral Degree : niveau équivalent au Doctorat, souvent en trois ans</li></ul>
<p><strong>Pour en savoir plus :</strong></p>
<p><a href="http://www.nzvcc.ac.nz/" class="spip_out" rel="external">Universités de la Nouvelle-Zélande</a>.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/vie-pratique/article/scolarisation-111242). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
