# Lettre de motivation

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/emploi-stage/article/lettre-de-motivation#sommaire_1">Rédaction</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Rédaction</h3>
<p>Les lettres de motivation sont moins utilisées au Chili qu’en France. S’il s’agit d´une offre publiée sur Internet, elle n´est que rarement demandée. Vous pouvez détailler vos motivations dans le courriel d’accompagnement.</p>
<p>S’il s’agit d’une candidature spontanée, la lettre de motivation peut être importante et expliquer votre démarche et votre intérêt pour cette entreprise. Les Chiliens peuvent être sensibles à une lettre rédigée par un Français dans un bon espagnol.</p>
<p>Au Chili, la lettre de motivation est dactylographiée et non pas manuscrite. Faites-la corriger afin de vous assurer qu’elle ne contient pas d’erreurs.</p>
<p>Elle doit être rédigée en espagnol et adressée à la bonne personne. Si vous disposez du nom de la personne, assurez-vous de bien l’orthographier et vérifiez son poste. Idéalement, vous devez contacter la personne chargée de votre recrutement : ce peut être le directeur technique ou le directeur commercial. Si vous ne savez pas à qui adresser votre lettre de motivation, envoyez votre courrier à la <i>Gerencia de Recursos Humanos</i> ou au <i>Gerente General</i>.</p>
<p><strong>Votre lettre de motivation ne doit pas dépasser une page, être brève, claire et directe.</strong> Utilisez un ton professionnel et profitez-en pour mettre en relief vos qualités ou ce que vous pourriez apporter à l’entreprise. Évitez un texte monotone et soyez créatif afin d´attirer l’attention du lecteur. Dans la mesure du possible, signez manuellement vos lettres, en bleu ou noir.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/emploi-stage/article/lettre-de-motivation). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
