# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/emploi-stage/article/reglementation-du-travail-111007#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/emploi-stage/article/reglementation-du-travail-111007#sommaire_2">Contrat de travail – spécificités</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/emploi-stage/article/reglementation-du-travail-111007#sommaire_3">Fêtes légales</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/emploi-stage/article/reglementation-du-travail-111007#sommaire_4">Emploi du conjoint</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/emploi-stage/article/reglementation-du-travail-111007#sommaire_5">Création d’entreprises – spécificités</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<p>La législation du travail sud-africaine a connu de nombreux changements à la fin des années 1990, qui en font aujourd’hui l’une des législations les plus avancées du monde. Conforme aux règlements édictés par l’Organisation Internationale du Travail, elle assure une protection et de nombreux droits sociaux aux employés.</p>
<p>Les principaux textes applicables sont les suivants :</p>
<ul class="spip">
<li><i>Occupational Health and Safety Act</i> (1993) : il fait autorité en matière de santé et de sécurité sur le lieu de travail. Cette loi est valable pour tous les secteurs, sauf celui de l’industrie minière qui est régi par le <i>Mines Health and Safety Act</i> ;</li>
<li><i>Labour Relations Act</i> (1995) : il régit les rapports entre les partenaires sociaux et régule le droit de grève en accord avec la Constitution, le recours au <i>lock-out</i> et le licenciement des employés ;</li>
<li><i>Basic Conditions of Employment Act</i> (1997) : il statue sur les conditions de base de l’emploi pour tous les employés et employeurs en Afrique du Sud. Il définit les droits fondamentaux relatifs à la durée maximale du travail, à la protection des employés de nuit et aux congés. Il interdit le travail des enfants de moins de 15 ans. Il détermine ainsi les termes de tout contrat d’embauche ;</li>
<li><i>Employment Equity Act</i> (1998) : il comporte deux volets distincts qui correspondent au même objectif, à savoir l’adéquation entre la composition de la population active et la structure de la main d’oeuvre. Le premier volet vise l’interdiction des discriminations, tandis que le second est relatif à la discrimination positive et prévoit l’élaboration par tout employeur de plus de 50 salariés d’un plan d’égalisation des chances ;</li>
<li><i>Unemployment Insurance Act</i> (n° 63 de 2001) : prévoit l’obligation pour tous les employés de cotiser à l’<i>Unemployment Insurance Fund</i>, quel que soit le montant du salaire ;</li>
<li><i>Skills Development Act</i> (1998) et <i>Skills Development Levies Act</i> (1999) : ces textes manifestent la volonté des pouvoirs publics d’améliorer la qualification de la main d’oeuvre sud-africaine. Ils instaurent une taxe dont l’employeur peut récupérer une partie pour financer un plan de formation interne au sein de l’entreprise.</li>
<li><i>Broad-Based Black Economic Empowerment Act</i> (2003) : l’objectif de ce texte est de s’assurer de l’équité de la distribution des richesses, par la mise en place d’un programme de discrimination positive vis-à-vis de la population noire sud-africaine. Ces textes sont consultables sur le site Internet du <a href="http://www.labour.gov.za/" class="spip_out" rel="external">ministère du Travail</a>.</li></ul>
<p>La durée maximale de travail est de <strong>neuf</strong> heures par jour, si l’employé travaille cinq jours par semaine ou moins, <strong>huit</strong> heures par jour si l’employé travaille plus de cinq jours par semaine, 45 heures par semaine. A cela peuvent être ajoutées 15 minutes quotidiennes, dans la limite d’une heure par semaine. Les heures supplémentaires ne sont permises que si un accord préalable entre l’employeur et l’employé a été conclu à ce sujet. Elles ne peuvent pas dépasser trois heures par jour et 10 heures par semaine.</p>
<p>Le régime des congés : un employé peut prétendre à 21 jours de congés annuels (1 jour pour 17 jours travaillés), non cumulables d’une année sur l’autre ; un nombre de jours de congés maladie rémunérés qui, sur une période de trois ans, est égal au nombre de jours habituellement travaillés en six semaines ; un congé maternité dont la durée maximale est de quatre mois ; trois jours de congés payés pour responsabilité familiale pour les employés travaillant au moins quatre jours par semaine (naissance d’un enfant, maladie de son enfant, décès d’un ascendant, etc.).</p>
<h3 class="spip"><a id="sommaire_2"></a>Contrat de travail – spécificités</h3>
<p><strong>La rupture du contrat de travail</strong></p>
<p>Les trois motifs valables de licenciement reconnus par la loi sont : la faute professionnelle, l’incapacité physique ou professionnelle et le licenciement pour raison économique. L’employeur ou l’employé souhaitant rompre un contrat de travail doit donner un préavis qui n’est pas obligatoire en cas de licenciement pour faute ou incapacité. Le préavis est à donner une semaine à l’avance pour des contrats de durée inférieure ou égale à six mois, de deux semaines pour des contrats de durée comprise entre six mois et un an, de quatre semaines pour des contrats de durée supérieure à un an.</p>
<ul class="spip">
<li>Seuls les employés licenciés pour incapacité peuvent prétendre à une indemnité pouvant aller jusqu’à 12 mois de salaire. Le licenciement pour faute professionnelle ne donne pas droit à cette indemnité sauf si le tribunal administratif (C.C.M.A. - <i>Commission for Conciliation, Mediation and Arbitration</i>) décidait, pour des raisons humanitaires ou juridiques (s’il estime que la faute professionnelle n’est pas prouvée mais que l’employeur ne veut, malgré tout, plus recruter l’employé) que celui-ci a droit à une indemnisation.</li>
<li>En cas de licenciement économique, seule la compensation égale à une semaine de salaire par année d’activité est payée en plus du dernier salaire et des droits à congé.</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Fêtes légales</h3>
<ul class="spip">
<li>1er janvier - Nouvel An</li>
<li>21 mars - Journée des Droits de l’Homme</li>
<li>18 avril - Vendredi Saint (vendredi précédent le dimanche de Pâques)</li>
<li>21 avril - Fête de la Famille (lundi suivant le dimanche de Pâques)</li>
<li>27 avril - Fête de la Liberté (fête nationale)</li>
<li>28 avril - Jour férié</li>
<li>1 mai - Fête du Travail</li>
<li>16 juin - Journée de la Jeunesse</li>
<li>9 août - Journée nationale des Femmes</li>
<li>24 septembre - Journée du Patrimoine</li>
<li>16 décembre - Fête de la Réconciliation</li>
<li>25 décembre - Noël</li>
<li>26 décembre - Journée de la Bonne Volonté</li></ul>
<p>Lorsqu’un jour férié tombe un dimanche, le lundi suivant est férié.</p>
<h3 class="spip"><a id="sommaire_4"></a>Emploi du conjoint</h3>
<p>Les lois sud-africaines régissant le travail (politique de discrimination positive "affirmative action") et l’immigration (permis de travail accordés seulement aux étrangers venant occuper un emploi qui n’a pas pu être pourvu par un Sud-Africain ou un étranger titulaire d’une autorisation permanente de séjour) restreignent les possibilités d’emplois pour les expatriés et leurs conjoints.</p>
<p>Pour en savoir plus, vous pouvez consulter le site de l’<a href="http://www.services.gov.za/" class="spip_out" rel="external">administration sud-africaine</a>.</p>
<h3 class="spip"><a id="sommaire_5"></a>Création d’entreprises – spécificités</h3>
<p>Consulter la page <a href="http://www.dti.gov.za/trade_investment/how_todo_business_insa.jsp" class="spip_out" rel="external">How to do business in SA</a> sur le site du ministère sud-africain du Commerce et de l’Industrie :</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/emploi-stage/article/reglementation-du-travail-111007). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
