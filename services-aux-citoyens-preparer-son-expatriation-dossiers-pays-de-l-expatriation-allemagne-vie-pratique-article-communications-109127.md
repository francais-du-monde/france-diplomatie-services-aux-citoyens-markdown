# Communications

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/vie-pratique/article/communications-109127#sommaire_1">Téléphone – Internet</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/vie-pratique/article/communications-109127#sommaire_2">Poste</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Téléphone – Internet</h3>
<p>Les communications urbaines et interurbaines peuvent être obtenues à partir des bureaux de poste et des cabines publiques. Les cartes téléphoniques sont en vente dans tous les bureaux de poste et parfois dans les kiosques, au prix de 5 et 10 euros.</p>
<p><strong>Indicatifs téléphoniques</strong></p>
<ul class="spip">
<li><strong>Vers l’Allemagne</strong> : 00 49 (indicatif pour l’Allemagne) + indicatif de la ville (sans le 0) + numéro de votre correspondant ;</li>
<li><strong>Vers la France</strong> : 00 33 (indicatif pour la France) + numéro de votre correspondant sans le 0.</li></ul>
<p>Il existe en Allemagne de très nombreux opérateurs en téléphonie et en connexion Internet. Pour n’en citer que quelques-uns :</p>
<ul class="spip">
<li><a href="http://www.telekom.de/" class="spip_out" rel="external">Deutsche Telekom</a></li>
<li>le site <a href="http://www.billigertelefonieren.de/" class="spip_out" rel="external">www.billigertelefonieren.de</a>recense un vaste choix d’offres pour la téléphonie fixe et mobile, ainsi que pour Internet.</li></ul>
<p><strong>Utilisation de votre portable français</strong></p>
<p>Vous pouvez utiliser votre téléphone portable partout en Europe et dans de nombreuses autres régions du monde, grâce à la norme technique GSM de l’Union européenne. Les personnes qui utilisent leur téléphone portable à l’étranger réalisent d’importantes économies – de l’ordre de 60 % – grâce au règlement de l’UE sur l’itinérance (« roaming »), qui plafonne le coût des appels téléphoniques effectués ou reçus dans un autre pays de l’UE (« eurotarif »). De plus, une facturation à la seconde a été introduite après les premières 30 secondes pour les appels effectués, et dès la première seconde pour les appels reçus. Le coût d’envoi d’un SMS depuis l’étranger a lui aussi nettement diminué. Renseignez-vous auprès de votre opérateur français pour les tarifs.</p>
<p>Les clients reçoivent un message automatique les informant du coût des appels, des SMS et des services de transmission de données à l’étranger (en cas de navigation sur internet ou de téléchargement d’un film, p. ex.). Les prix de gros que les opérateurs se facturent mutuellement sont également plafonnés, ce qui devrait réduire les coûts supportés par les consommateurs. Un mécanisme a également été mis en place afin d’éviter aux consommateurs de devoir payer des factures astronomiques pour des services d’itinérance : après un premier avertissement, la connexion internet mobile est suspendue dès que le montant atteint un certain niveau. Une limite de 50 euros par mois s’applique par défaut si le client n’a pas défini un montant spécifique.</p>
<p>Vous trouverez sur le site de l’<a href="http://ec.europa.eu/information_society/activities/roaming/tariffs/in_ms/index_en.htm" class="spip_out" rel="external">Europe’s Information Society</a>  (Commission européenne) les eurotarifs appliqués par les opérateurs des pays membres, ainsi que les liens vers leurs sites :</p>
<p>Source : <a href="http://europa.eu/travel/comm/index_fr.htm#phone" class="spip_url spip_out auto" rel="nofollow external">http://europa.eu/travel/comm/index_fr.htm#phone</a></p>
<p>Téléphoner gratuitement par Internet Il est généralement très avantageux d’utiliser <strong>des logiciels de téléphonie sur IP</strong> (Skype, Google talk, Live messenger, Yahoo ! messenger, etc.) car les tarifs proposés à l’international sont souvent moindre que ceux des opérateurs locaux exploitant des lignes classiques, voire gratuits dans certaines conditions.</p>
<p><strong>Sites Internet à consulter</strong></p>
<ul class="spip">
<li>Vous trouverez les indicatifs des principales villes d’Allemagnesur les sites suivants :</li>
<li><a href="http://www.tv5.org/cms/chaine-francophone/voyageurs/Outils/p-6716-Indicatifs-telephoniques.htm" class="spip_out" rel="external">TV5MONDE</a></li>
<li><a href="http://www.dastelefonbuch.de/" class="spip_out" rel="external">www.dastelefonbuch.de/</a></li>
<li><a href="http://www.teleauskunft.de/" class="spip_out" rel="external">Pages jaunes et blanches allemandes</a></li>
<li><a href="http://www.deutschland.de/" class="spip_out" rel="external">Portail de l’Allemagne</a></li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Poste</h3>
<p>Les liaisons postales fonctionnent parfaitement. Il faut compter en moyenne deux jours de délai postal entre l’Allemagne et la France pour une lettre ordinaire.</p>
<p>A noter que les tarifs sont en fonction de la taille de l’enveloppe. Pour connaître les tarifs en détail, vous pouvez consulter le site de la <a href="http://www.deutschepost.de/" class="spip_out" rel="external">Poste allemande</a> (Deutsche Post).</p>
<p>Les bureaux de poste sont ouverts en général du lundi au vendredi de 8h à 18h et le samedi jusqu’à 12h. Dans les gares des grandes villes et dans les aéroports, ils restent parfois ouverts plus longtemps.</p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/vie-pratique/article/communications-109127). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
