# Déménagement

<p>Par bateau, il faut compter environ quarante-cinq jours (formalités de douane comprises). Par avion, ce délai peut être ramené à dix jours (compte tenu de l’informatisation du système, les formalités de douane prennent environ dix jours quel que soit le mode de transport choisi).</p>
<p>A titre indicatif, le coût d’acheminement (Paris-Tananarive) d’un container de 20 pieds est estimé entre 6000 et 8500 €.</p>
<p>Lorsque vous déménagez définitivement à Madagascar, vous avez droit à une franchise des droits et taxes à l’importation pour les effets et objets personnels (linges de maison, meubles, vêtements et chaussures, électroménagers, vaisselles, livres, jouets,…) qui vous ont appartenu depuis plus de six mois, ainsi qu’à une voiture particulière et à une moto par famille qui vous ont appartenu depuis plus de un an, leurs cartes grises faisant foi (voir arrêté 16 152 / 2007 du 21/09/07).</p>
<p>Les pièces à fournir pour une demande de franchise sont précisées sur le site des douanes locales.</p>
<p>S’agissant d’un déménagement international, il est primordial de s’entourer d’un maximum de garanties en faisant appel à un professionnel disposant de certifications reconnues (marque NF Service, ISO 9002, etc.)</p>
<p><a href="http://www.csdemenagement.fr/" class="spip_out" rel="external">Chambre syndicale du déménagement</a><br class="manualbr">Téléphone : 01 49 88 61 40 <br class="manualbr">Télécopie : 01 49 88 61 46<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/entree-et-sejour/article/demenagement-111177#contact#mc#csdemenagement.fr#" title="contact..åt..csdemenagement.fr" onclick="location.href=mc_lancerlien('contact','csdemenagement.fr'); return false;" class="spip_mail">Courriel</a></p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/entree-et-sejour/article/demenagement-111177). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
