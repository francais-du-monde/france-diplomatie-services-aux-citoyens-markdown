# Emploi, stage

<h2 class="rub22724">Marché du travail</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/emploi-stage/#sommaire_1">Secteurs porteurs</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/emploi-stage/#sommaire_2">Rémunération</a></li></ul>
<p>Les chiffres du chômage en 2012 : 6,8%Malgré une baisse de 5% du PIB allemand, les effets de la crise sur le marché de l’emploi ont été moindres que celles attendues. Néanmoins, ces chiffres sont à nuancer : le recours au chômage partiel a beaucoup participé à la stabilité du taux de chômage, les offres d’emploi sont en baisse ainsi que les contrats à durée indéterminée.</p>
<p>En janvier 2010, on constate toujours de fortes disparités sur le marché du travail entre les nouveaux länder de l´Est et l´ex-Allemagne de l´Ouest. Le taux de chômage dans la fédération est de 6,8% contre 7,1% l´année précédente (2011) à la même période avec la répartition suivante :</p>
<ul class="spip">
<li>Ouest : 5,7%</li>
<li>Est : 9,9 %</li></ul>
<p>Les länder les moins touchés par le chômage sont la Bavière et la Bade-Wuttemberg avec des taux respectivement de 3,7% et de 3,9%%, pratiquement en situation de plein emploi pour la population active qualifiée .</p>
<h3 class="spip"><a id="sommaire_1"></a>Secteurs porteurs</h3>
<p>Il existe des secteurs à fort potentiel pour les Français, particulièrement à l’export et dans les biens de grande consommation (marketing et distribution).</p>
<p>Les scientifiques et les ingénieurs français de haut vol bénéficient d’une excellente image dans tous les secteurs de la haute technologie, des télécommunications, de l’audiovisuel, de la mécanique de précision, de l’aéronautique et dans le secteur spatial.</p>
<p>Les "Wirtschaftsingenieure" - interface entre ingénierie et gestion des entreprises - sont très recherchés avec cependant une possible réserve des entreprises allemandes face aux jeunes diplômés français, plus jeunes que leurs homologues allemands.</p>
<p><strong>Secteurs porteurs</strong></p>
<ul class="spip">
<li>Énergie : le besoin en ingénieurs, diplômés en sciences naturelles et d’économistes est énorme. Ex : Eon, RWE, Areva, Shell Deutschland Oil GmbH recherchent des ingénieurs, économistes, informaticiens ;</li>
<li>Agro-alimentaire et la distribution : Edeka, REWE, Lidl et Aldi recherchent des économistes, juristes, spécialistes en agro-alimentaire, informaticiens ;</li>
<li>IT (en particulier les secteurs information, automation, microtechnique, technique de l’énergie) : Datev, Hewlett Packard ;</li>
<li>Pharmacie : Merck (controlling).</li></ul>
<p><strong>Métiers porteurs</strong></p>
<ul class="spip">
<li>Ingénieur technico-commercial / Commercial</li>
<li>Ingénieur développement, Supply-Chain, achat, qualité</li>
<li>Comptabilité : controlling  accounting</li>
<li>IT : business analyst, ingénieur développement, software etc.</li>
<li>Online et accès Internet : sales manager, business developpement</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Rémunération</h3>
<p>En Allemagne, il n’existe pas de <strong>salaire minimum légal</strong>, équivalent au SMIC en France. Néanmoins si une convention collective s’applique, l’employé peut prétendre au salaire conventionnel.</p>
<p>S’il s’agit d’un contrat hors convention collective, les deux parties conviennent librement du montant du salaire, des primes supplémentaires et des éventuelles réévaluations.</p>
<p>Toute discrimination est interdite entre les salariés hommes et femmes, ainsi qu’entre les salariés à temps plein et ceux à temps partiel.</p>
<p><strong>L’impôt sur le revenu est retenu à la source</strong>, c’est à dire prélevé chaque mois par l’employeur sur le salaire. Celui-ci doit donc verser son salaire à l’employé à la date prévue dans le contrat et lui remettre un bulletin de paie, mentionnant le salaire brut et les retenues effectuées au titre de l’impôt sur le revenu et des cotisations sociales.</p>
<p>Afin d’évaluer le niveau des salaires en Allemagne, vous pouvez consulter les sites suivants :</p>
<ul class="spip">
<li><a href="http://www.gehalts-check.de/" class="spip_out" rel="external">www.gehalts-check.de</a> (rubrique « Gehaltsdatenbank ») indiquant pour plus de 600 professions le salaire annuel brut moyen par secteur d’activité ;</li>
<li><a href="http://www.arbeitsrecht.de/" class="spip_out" rel="external">www.arbeitsrecht.de</a> / rubrique "Lohnspiegel" ;</li>
<li><a href="http://www.lohnspiegel.de/main/" class="spip_out" rel="external">www.lohnspiegel.de/main/</a> / rubrique "Lohn- und Gehaltscheck" ;</li>
<li><a href="http://www.boeckler.de/" class="spip_out" rel="external">Hans Böckler Stiftung</a> dans la liste "Hauptbereiche" choisir "WIS - Tarifarchiv" ;</li>
<li>Site du syndicat <a href="http://www.igmetall.de/" class="spip_out" rel="external">IG Metall</a> / rubrique "Tarife" ;</li>
<li>Site du quotidien <a href="http://www.sueddeutsche.de/" class="spip_out" rel="external">Süddeutsche Zeitung</a> / rubrique "Karriere  Gehälter ABC".</li></ul>
<p>A titre indicatif, voici quelques exemples de salaires :</p>
<table class="spip">
<thead><tr class="row_first"><th id="id84ed_c0">Profession  </th><th id="id84ed_c1">Echelle des salaires annuels bruts moyens (en euros)</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="id84ed_c0">Standardiste / Accueil / Call Center</td>
<td headers="id84ed_c1">24 575 euros</td></tr>
<tr class="row_even even">
<td headers="id84ed_c0">Secrétaire</td>
<td headers="id84ed_c1">26 356 euros</td></tr>
<tr class="row_odd odd">
<td headers="id84ed_c0">Assistant de direction / marketing</td>
<td headers="id84ed_c1">34 385 euros</td></tr>
<tr class="row_even even">
<td headers="id84ed_c0">Comptable</td>
<td headers="id84ed_c1">30 981 euros</td></tr>
<tr class="row_odd odd">
<td headers="id84ed_c0">Acheteur</td>
<td headers="id84ed_c1">31 878 euros</td></tr>
<tr class="row_even even">
<td headers="id84ed_c0">Responsable des ventes</td>
<td headers="id84ed_c1">44 727 euros</td></tr>
<tr class="row_odd odd">
<td headers="id84ed_c0">Programmeur</td>
<td headers="id84ed_c1">37 682 euros</td></tr>
<tr class="row_even even">
<td headers="id84ed_c0">Commercial itinérant</td>
<td headers="id84ed_c1">42 363 euros</td></tr>
<tr class="row_odd odd">
<td headers="id84ed_c0">Chef de produit (marketing)</td>
<td headers="id84ed_c1">44 433 euros</td></tr>
<tr class="row_even even">
<td headers="id84ed_c0">Ingénieur commercial</td>
<td headers="id84ed_c1">56 898 euros</td></tr>
</tbody>
</table>
<p>Source : <a href="http://www.emploi-allemagne.de/" class="spip_out" rel="external">www.emploi-allemagne.de</a></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-emploi-stage-article-stages.md" title="Stages">Stages</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-emploi-stage-article-entretien-d-embauche-109114.md" title="Entretien d’embauche">Entretien d’embauche</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-emploi-stage-article-lettre-de-motivation-109113.md" title="Lettre de motivation">Lettre de motivation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-emploi-stage-article-curriculum-vitae.md" title="Curriculum vitae">Curriculum vitae</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-emploi-stage-article-recherche-d-emploi-109111.md" title="Recherche d’emploi">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-emploi-stage-article-reglementation-du-travail.md" title="Réglementation du travail">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-emploi-stage-article-marche-du-travail-109109.md" title="Marché du travail">Marché du travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/emploi-stage/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
