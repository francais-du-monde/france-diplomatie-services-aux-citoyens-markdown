# Santé

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/vie-pratique/article/sante#sommaire_1">Médecine de soins</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/vie-pratique/article/sante#sommaire_2">Informations générales</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Médecine de soins</h3>
<p>L’état sanitaire est convenable, s’améliorant progressivement, les soins dispensés, malgré le manque de crédits, sont de bonne qualité et les spécialistes sont compétents. Toutefois pour des interventions très spécialisées, il convient d’envisager dans certains cas un rapatriement vers la France. Le parc hospitalier est dans son ensemble en mauvais état (quelques cliniques privées de bonne qualité), un hôpital public (dit hôpital militaire) est flambant neuf.</p>
<p>Le coût des soins est un peu moins élevé qu’en France, sauf pour les cliniques privées.</p>
<p>La proportion de médecins exerçant dans le privé est de plus en plus importante et les consultations s’effectuent au sein de cabinet et non pas dans les hôpitaux. Comme les médecins francophones sont peu nombreux, il est conseillé de contacter le Consulat, afin d’obtenir les coordonnées des médecins habituellement consultés par les Français. Vous aurez ainsi les données les plus actuelles, les plus justes et les plus précises.</p>
<p>En cas de difficulté pouvant nécessiter une hospitalisation, il faut contacter le médecin traitant local ou le SAMU hongrois en composant le 104. Les ambulances sont dirigées automatiquement vers l’établissement hospitalier disposant des meilleures conditions d’accueil. Budapest comprend une vingtaine d’hôpitaux comportant tous des services spécialisés. L’orientation se fait en fonction de la pathologie et de l’hôpital de garde.</p>
<p>La carte européenne d’assurance maladie est reconnue et donc conseillée en cas d’urgence et dans les établissements publics seulement. La carte européenne n’est pas acceptée dans les institutions privées.</p>
<p>Si vous avez contracté une assurance de rapatriement sanitaire, il convient d’appeler sa compagnie ou son service d’assistance, ou encore de se renseigner auprès du consulat.</p>
<p>Outre la garantie de rapatriement, il convient de bien vérifier la clause concernant les frais d’hospitalisation engagés à l’étranger.</p>
<h3 class="spip"><a id="sommaire_2"></a>Informations générales</h3>
<p><strong>Grippe aviaire</strong></p>
<p>La direction générale de la Santé recommande aux voyageurs d’éviter tout contact avec les volailles et les oiseaux, c’est-à-dire de ne pas se rendre dans des élevages ni sur les marchés aux volatiles. Les recommandations générales d’hygiène, qui visent à se protéger des infections microbiennes, sont préconisées :</p>
<ul class="spip">
<li>éviter de consommer des produits alimentaires crus ou peu cuits, en particulier les viandes et les œufs ;</li>
<li>se laver régulièrement les mains à l’eau et au savon ou une solution hydro-alcoolique. Le virus se transmet par voie aérienne (voie respiratoire) soit par contact direct, notamment avec les sécrétions respiratoires et les matières fécales des animaux malades, soit de façon indirecte par l’exposition à des matières contaminées (par l’intermédiaire de la nourriture, de l’eau, du matériel et des mains ou des vêtements souillés). Les espaces confinés favorisent la transmission du virus.</li></ul>
<p><strong>Les tiques</strong></p>
<p>Les tiques sont nombreuses en Hongrie. Il est recommandé de vérifier le corps régulièrement et éventuellement de demander conseil à votre médecin référent pour la vaccination.</p>
<p><strong>Conseils et autres informations</strong></p>
<p>Pour les courts séjours, se munir de la carte européenne d’assurance maladie à demander à votre caisse d’assurance maladie au moins 15 jours avant le départ. Pour plus d’information, consultez la page de l’<a href="http://www.ameli.fr/assures/droits-et-demarches/a-l-etranger/index.php" class="spip_out" rel="external">Assurance Maladie</a>.</p>
<blockquote class="texteencadre-spip spip"><strong>Consulter le médecin traitant avant le départ et contracter une assurance de rapatriement sanitaire.</strong></blockquote>
<p><strong>Numéros utiles :</strong><br class="manualbr">Hôpital Mav : +36 1 269.55.80 <br class="manualbr">Hôpital Szent Janos : +36 1 458.45.00 <br class="manualbr">SOS dentiste (urgences 24h/24) : +36 1 267.9602</p>
<p>Pour de plus amples renseignements, vous pouvez consulter le site de l’<a href="http://www.invs.sante.fr/" class="spip_out" rel="external">INVS</a>, ainsi que celui de l’<a href="http://www.who.int/fr/index.html" class="spip_out" rel="external">OMS</a>, qui vous renseigneront sur l’état sanitaire de ce pays, ou les sites de <a href="http://www.pasteur-lille.fr/" class="spip_out" rel="external">l’institut Pasteur de Lille</a> et de <a href="http://www.pasteur.fr/" class="spip_out" rel="external">l’institut Pasteur de Paris</a>.</p>
<p>Pour une présentation des risques épidémiologiques ainsi que des conseils sur la médecine au quotidien, consultez les pages suivantes :</p>
<ul class="spip">
<li>Page dédiée à la <a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/hongrie/" class="spip_in">santé en Hongrie</a> sur le site Conseils aux voyageurs</li>
<li>Fiche Budapest sur le site du <a href="http://www.cimed.org" class="spip_out" rel="external">CIMED</a></li></ul>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/vie-pratique/article/sante). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
