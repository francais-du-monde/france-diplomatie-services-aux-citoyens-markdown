# Dernières minutes

<blockquote class="home_dernieres_minutes_img_main">
<a href="https://twitter.com/ConseilsVoyages" class="twitter-follow-button" data-show-count="false" data-lang="fr">Suivre @ConseilsVoyages</a>
</blockquote>
<p class="items" id="pagination_dernieres_minutes">
<blockquote class="bloc_dernieres_minutes">
<span class="drapeaux"><img class="spip_logos" alt="Etats-Unis" src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L30xH19/rubon12311-6b67c.jpg" width="30" height="19"></span>
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/etats-unis/" title="Etats-Unis">Etats-Unis |
        16 août 2016
</a>
<p>
          Inondations en Louisiane

Suite à de fortes précipitations sur le sud-ouest et le centre de la Louisiane, une forte et rapide montée des eaux est constatée depuis le vendredi 12 août, notamment dans la région de Baton Rouge et Lafayette.

Au 16 août 2016, les alertes (...)
</p>
</blockquote>
<blockquote class="bloc_dernieres_minutes">
<span class="drapeaux"><img class="spip_logos" alt="Australie" src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L30xH19/rubon12202-365a8.jpg" width="30" height="19"></span>
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/australie/" title="Australie">Australie |
        16 août 2016
</a>
<p>
          Recrudescence d’accidents de la circulation.

Des accidents graves de la route viennent d’être observés. La plus grande vigilance est appelée sur la conduite à gauche. Evitez de conduire la nuit et sous l’emprise de la fatigue. Soyez attentifs aux virages à visibilité (...)
</p>
</blockquote>
<blockquote class="bloc_dernieres_minutes">
<span class="drapeaux"><img class="spip_logos" alt="Chine" src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L30xH19/rubon12226-477e1.jpg" width="30" height="19"></span>
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/chine/" title="Chine">Chine |
        16 août 2016
</a>
<p>
          Aéroports et gares de Shanghai.

Les autorités ont annoncé  des mesures de sécurité supplémentaires dans les aéroports et les gares de Shanghai dans le cadre de la préparation du sommet du G20 qui se tiendra à Hangzhou les 4 et 5 septembre. Des contrôles renforcés des (...)
</p>
</blockquote>
<blockquote class="bloc_dernieres_minutes">
<span class="drapeaux"><img class="spip_logos" alt="Mali" src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L30xH19/rubon12287-922ba.png" width="30" height="19"></span>
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/mali/" title="Mali">Mali |
        15 août 2016
</a>
<p>
          Plusieurs véhicules ont été attaqués le 8 août 2016 par des coupeurs de route sur l’axe Bamako-Siby ; cette attaque a fait plusieurs victimes. Il est recommandé d’éviter de se déplacer dans cette zone jusqu’à nouvel ordre.

En raison des menaces terroristes prévalant dans la (...)
</p>
</blockquote>
<blockquote class="bloc_dernieres_minutes">
<span class="drapeaux"><img class="spip_logos" alt="République centrafric" src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L30xH19/rubon12224-4ecba.jpg" width="30" height="19"></span>
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/republique-centrafricaine/" title="République centrafricaine">République centrafricaine |
        12 août 2016
</a>
<p>
          Recrudescence de cas de paludisme

Avec le retour de la saison pluvieuse, une nette recrudescence de cas de paludisme est observée dans le pays, y compris avec des formes graves et potentiellement mortelles.

Sur place, il est fortement recommandé de se protéger des (...)
</p>
</blockquote>
<blockquote class="bloc_dernieres_minutes">
<span class="drapeaux"><img class="spip_logos" alt="Thaïlande" src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L30xH19/rubon12322-f64cb.jpg" width="30" height="19"></span>
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/thailande/" title="Thaïlande">Thaïlande |
        12 août 2016
</a>
<p>
          Série d’explosions.

Une série d’explosions se sont produites dans plusieurs stations balnéaires de Thaïlande les 11 et 12 août (Patong, Trang, Hua Hin, Bang Niang, Krabi, Nakhon Si Thammarat et Surat Thani). Il est recommandé de faire preuve de vigilance et d’éviter les (...)
</p>
</blockquote>
<blockquote class="bloc_dernieres_minutes">
<span class="drapeaux"><img class="spip_logos" alt="Portugal" src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L30xH19/rubon12348-59a25.jpg" width="30" height="19"></span>
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/portugal/" title="Portugal">Portugal |
        10 août 2016
</a>
<p>
          Plusieurs régions du Portugal sont actuellement touchées par de nombreux incendies de grande ampleur, en raison de la chaleur, de la sécheresse et de vents de forte intensité.

La moitié nord du pays est placée en alerte maximale. Les autorités portugaises ont mis en (...)
</p>
</blockquote>
<blockquote class="bloc_dernieres_minutes">
<span class="drapeaux"><img class="spip_logos" alt="Pérou" src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L30xH19/rubon12345-06562.jpg" width="30" height="19"></span>
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/perou/" title="Pérou">Pérou |
        9 août 2016
</a>
<p>
          Mise en garde contre la consommation d’Ayahuasca

L’ Ayahuasca, parfois utilisée en Amazonie au cours de processus d’initiation au chamanisme, est une plante hallucinogène inscrite en France au registre des stupéfiants. Sa consommation peut avoir des conséquences (...)
</p>
</blockquote>
<blockquote class="bloc_dernieres_minutes">
<span class="drapeaux"><img class="spip_logos" alt="Brésil" src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L30xH19/rubon12219-a847a.jpg" width="30" height="19"></span>
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/bresil/" title="Brésil">Brésil |
        29 juillet 2016
</a>
<p>
          Jeux Olympiques et Paralympiques

Le Brésil accueillera les Jeux Olympiques du 5 au 21 août 2016 et Paralympiques du 7 au 18 septembre 2016. Conformément à ses traditions d’hospitalité, le pays s’attache à réserver le meilleur accueil aux touristes. Le renforcement (...)
</p>
</blockquote>
<blockquote class="bloc_dernieres_minutes">
<span class="drapeaux"><img class="spip_logos" alt="Zambie" src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L30xH19/rubon12303-59d4c.png" width="30" height="19"></span>
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/zambie/" title="Zambie">Zambie |
        27 juillet 2016
</a>
<p>
          Des élections générales (présidentielle, législatives, provinciales et municipales) se tiendront en Zambie le 11 août 2016. Un éventuel deuxième tour pourrait se tenir ultérieurement à une date non encore précisée. Il est recommandé de se tenir à l’écart des rassemblements ou (...)
</p>
</blockquote>
<span class="pagination" style="width: 100%; text-align: center;">
<span class="on">1</span>
  |
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/dernieres-minutes/?debut_dernieres_minutes=10#pagination_dernieres_minutes" class="lien_pagination" rel="nofollow">2</a>
  |
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/dernieres-minutes/?debut_dernieres_minutes=20#pagination_dernieres_minutes" class="lien_pagination" rel="nofollow">3</a>
  |
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/dernieres-minutes/?debut_dernieres_minutes=30#pagination_dernieres_minutes" class="lien_pagination" rel="nofollow">4</a>
  |
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/dernieres-minutes/?debut_dernieres_minutes=40#pagination_dernieres_minutes" class="lien_pagination" rel="nofollow">5</a>
  |
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/dernieres-minutes/?debut_dernieres_minutes=50#pagination_dernieres_minutes" class="lien_pagination" rel="nofollow">6</a>

 | <a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/dernieres-minutes/?debut_dernieres_minutes=10#pagination_dernieres_minutes" class="lien_pagination" rel="nofollow">&gt;</a></span>
</p>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/dernieres-minutes/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
