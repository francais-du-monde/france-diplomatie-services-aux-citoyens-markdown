# Régime local de sécurité sociale

<p>Vous trouverez une présentation détaillée du système de sécurité sociale luxembourgeois sur le site de notre partenaire, le <a href="http://www.cleiss.fr/" class="spip_out" rel="external">CLEISS</a> (Centre des liaisons européennes et internationales de sécurité sociale). En voici la table des matières :</p>
<ul class="spip">
<li><a href="http://www.cleiss.fr/docs/regimes/regime_luxembourg.html#generalites" class="spip_out" rel="external">Généralités</a> (Organisation, financement, taux de cotisation sur les salaires…)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_luxembourg.html#maladie" class="spip_out" rel="external">Maladie - maternité</a> (Prestations en nature et en espèces)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_luxembourg.html#dependance" class="spip_out" rel="external">Prestations en cas de dépendance</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_luxembourg.html#pensions" class="spip_out" rel="external">Pensions</a> (Pensions de vieillesse, d’invalidité, de survivant, aide sociale)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_luxembourg.html#adtmp" class="spip_out" rel="external">Accidents du travail et maladies professionnelles</a> (Régime indemnitaire, y compris pour les survivants)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_luxembourg.html#pf" class="spip_out" rel="external">Prestations familiales</a> (Allocations familiales, de naissance, d’adoption…)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_luxembourg.html#chomage" class="spip_out" rel="external">Chômage</a> (Indemnités et assistance chômage)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_luxembourg.html#rmg" class="spip_out" rel="external">Revenu minimum garanti</a></li></ul>
<p>Vous pouvez également consulter le site : <a href="http://www.guichet.public.lu/citoyens/fr/sante-social/index.html" class="spip_out" rel="external">Guichet public - Sante / social/</a> qui développe les rubriques suivantes :</p>
<ul class="spip">
<li><a href="http://www.guichet.public.lu/citoyens/fr/sante-social/affiliation-remboursement/index.html" class="spip_out" rel="external">Affiliation et remboursement des frais de soins de santé</a></li>
<li><a href="http://www.guichet.public.lu/citoyens/fr/sante-social/action-sociale/index.html" class="spip_out" rel="external">Mesures d’action sociale</a></li>
<li><a href="http://www.guichet.public.lu/citoyens/fr/sante-social/chomage/index.html" class="spip_out" rel="external">Chômage</a></li>
<li><a href="http://www.guichet.public.lu/citoyens/fr/sante-social/maternite-prestations-familiales/index.html" class="spip_out" rel="external">Maternité, congés et prestations familiales</a></li>
<li><a href="http://www.guichet.public.lu/citoyens/fr/sante-social/accident-maladie/index.html" class="spip_out" rel="external">Prestations de l’assurance maladie et de l’assurance accident</a></li>
<li><a href="http://www.guichet.public.lu/citoyens/fr/sante-social/assurance-dependance/index.html" class="spip_out" rel="external">Prestations de l’assurance dépendance</a></li>
<li><a href="http://www.guichet.public.lu/citoyens/fr/sante-social/assurance-pension/index.html" class="spip_out" rel="external">Assurance pension</a></li>
<li><a href="http://www.guichet.public.lu/citoyens/fr/sante-social/fin-vie/index.html" class="spip_out" rel="external">Fin de vie</a></li>
<li><a href="http://www.guichet.public.lu/citoyens/fr/sante-social/metiers-sante/index.html" class="spip_out" rel="external">Métiers de la santé et du social</a></li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/protection-sociale-22810/article/regime-local-de-securite-sociale-109901). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
