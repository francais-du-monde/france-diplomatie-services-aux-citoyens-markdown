# Scolarisation

<p>Pour toute information sur la scolarisation dans le système français à l’étranger, vous pouvez consulter notre thématique sur les <a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures.md" class="spip_in">Etudes supérieures</a> et la <a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais.md" class="spip_in">Scolarité en français</a>.</p>
<p>Vous y trouverez des renseignements sur :</p>
<ul class="spip">
<li>les établissements français du primaire et du secondaire à l’étranger ;</li>
<li>les bourses scolaires et la prise en charge des frais de scolarité à l’étranger ;</li>
<li>les possibilités qui s’offrent à vous si votre enfant ne peut être scolarisé à l’étranger dans le système français (enseignement à distance par le CNED,</li>
<li>programme français langue maternelle (FLAM), internats en France) ;</li>
<li>les épreuves du baccalauréat à l’étranger ;</li>
<li>les bourses d’études supérieures en France et à l’étranger ;</li>
<li>l’équivalence des diplômes.</li></ul>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.aefe.fr/" class="spip_out" rel="external">Agence pour l’enseignement français à l’étranger</a></p>
<h4 class="spip">Les établissements scolaires français en Haïti</h4>
<p>Le <a href="http://www.lycee-a-dumas-pap-aefe.org/" class="spip_out" rel="external">Lycée Alexandre Dumas</a> est le lycée français de Port-au-Prince. Vous trouverez plus d’information sur ce lycée sur le <a href="http://www.ambafrance-ht.org/Scolariser-son-enfant-au-Lycee" class="spip_out" rel="external">site Internet de l’Ambassade</a>.</p>
<p>Plusieurs établissements sont également partenaires du LAD :</p>
<ul class="spip">
<li>Le centre <strong>Alcibiade Pommeyrac</strong> à Jacmel ;</li>
<li>Le collège <strong>Les Oliviers</strong> à Delmas ;</li>
<li>L’école <strong>Sainte Thérèse de l’enfant Jésus</strong> à Port-au-Prince ;</li>
<li>Les cours privés <strong>Edmé</strong> à Port-au-Prince ;</li>
<li>L’institution <strong>Saint Rose de Lima</strong> à Port-au-Prince ;</li>
<li>L’institution mixte <strong>EPI</strong> aux Gonaïves ;</li>
<li>L’institution <strong>EPI Eben Ezer</strong> aux Gonaïves ;</li>
<li>L’institution <strong>Les parents réunis</strong> au Cap Haïtien.</li></ul>
<p><i>Mise à jour : août 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/vie-pratique/article/scolarisation). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
