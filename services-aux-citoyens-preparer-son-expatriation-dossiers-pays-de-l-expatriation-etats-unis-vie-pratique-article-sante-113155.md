# Santé

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/vie-pratique/article/sante-113155#sommaire_1">Précautions à prendre</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/vie-pratique/article/sante-113155#sommaire_2">Accès aux soins</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/vie-pratique/article/sante-113155#sommaire_3">Coût des soins</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/vie-pratique/article/sante-113155#sommaire_4">Carte de sécurité sociale</a></li></ul>
<p>Le système de santé aux États-Unis apparaît différent du système de santé français et des dispositifs en place dans les autres pays de l’Union européenne en raison notamment de la prédominance des acteurs privés. Deux Américains sur trois, âgés de moins de 65 ans, sont ainsi couverts par le biais d’une assurance privée liée à l’emploi. La prise en charge publique concerne principalement, les plus de 65 ans à travers le programme fédéral <i>Medicare</i>, et les familles pauvres éligibles au programme <i>Medicaid</i>. En 2010, le système de santé des Etats-Unis a fait l’objet d’une importante réforme, avec l’adoption par le Congrès du <i>Patient Protection and Affordable Care Act</i>. Cette loi rend notamment obligatoire, depuis le 1er janvier 2014, pour les personnes ne disposant d’aucune couverture en matière de santé (financée par un employeur ou fournie par un programme d’assurance public), la souscription d’une assurance maladie privée, laquelle peut être subventionnée pour les personnes à faibles revenus.</p>
<h3 class="spip"><a id="sommaire_1"></a>Précautions à prendre</h3>
<p>Sur le plan médical, aucune vaccination n’est exigée pour l’entrée sur le territoire américain. Il convient toutefois, comme en toute circonstance, d’être à jour de son carnet de vaccinations.</p>
<p>S’agissant du SIDA, il est rappelé que le risque existe partout dans le monde et que les mesures de prévention doivent être observées où que l’on soit.</p>
<h3 class="spip"><a id="sommaire_2"></a>Accès aux soins</h3>
<p>L’infrastructure médicale est excellente et la qualité des soins exceptionnelle mais d’un coût très élevé (jusqu’à cinq fois supérieur à celui constaté en France).</p>
<p>Hors situation d’urgence, il est possible de s’adresser au consulat général de la circonscription de résidence pour obtenir la liste des médecins francophones. Des coordonnées de praticiens sont par ailleurs disponibles sur la plupart des sites <i>internet</i> des consulats généraux.</p>
<p>En situation d’urgence, seule une ambulance assure la priorité lors de l’arrivée au service des urgences de l’hôpital où l’admission est conditionnée par une garantie financière (carte de crédit).</p>
<p>S’agissant des médicaments, il est important de préciser qu’une ordonnance établie en France ne permet pas de se procurer les produits aux Etats-Unis. Il est donc conseillé de se constituer, le cas échéant, un stock suffisant des médicaments nécessaires et de conserver une copie de l’ordonnance correspondante et il convient de se renseigner avant le départ sur les conditions d’approvisionnement des spécialités pharmaceutiques pour les traitements chroniques.</p>
<p><strong>Il est vivement conseillé de consulter le médecin traitant avant le départ et de souscrire une assurance rapatriement.</strong></p>
<h3 class="spip"><a id="sommaire_3"></a>Coût des soins</h3>
<p>Aux Etats-Unis la médecine est essentiellement basée sur un système d’assurances privées compétitives et concurrentielles entre elles. Par conséquent les prix sont très élevés. A titre d’exemple :</p>
<ul class="spip">
<li>consultation mineure chez un <strong>généraliste</strong> : de 80 à 100 USD</li>
<li>consultation chez un <strong>dentiste</strong> : de 105 à 250 USD</li>
<li>consultation chez un <strong>spécialiste</strong> : à partir de 150 USD</li>
<li>consultation au <strong>service des urgences d’un hôpital</strong> : très élevée, varie selon l’acte. Le coût de certaines opérations chirurgicales (rares, spécialisées et difficiles) dans les meilleurs établissements hospitaliers peut atteindre des sommes exorbitantes : plusieurs centaines de milliers, voire de millions de dollars.</li></ul>
<p>Le coût des médicaments est également très élevé et ces derniers sont délivrés à l’unité.</p>
<p>Il est ainsi recommandé de s’assurer :</p>
<ul class="spip">
<li><i>pour les résidents</i> : contracter une assurance locale, un contrat "standart" (<i>Blue Cross</i>) est disponible pour un montant d’environ 500 USD par personne et par mois, mais il varie en fonction de l’âge du souscripteur.</li>
<li><i>pour les étudiants</i> : contracter une assurance monde/USA auprès d’une mutuelle étudiante, qui coûte environ 350 euros par personne pour 6 mois en couverture tous risques.</li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Carte de sécurité sociale</h3>
<p>Obtenir une carte de sécurité sociale (<i>Social Security Number</i>) est une démarche prioritaire à entreprendre dès votre arrivée. Ce sera votre numéro d’identification valable en permanence auprès de l’administration américaine. Que vous exerciez ou non une activité professionnelle, elle vous sera demandée en de nombreuses occasions (permis de conduire, ouverture d’un compte bancaire, obtention d’un crédit à la consommation, etc).</p>
<p>Toutefois, pour ceux qui ne peuvent travailler ou pour les enfants, il vous faudra trouver une raison valable à cette demande (ce peut être l’obtention du permis de conduire ou une demande de l’école). La carte qui vous sera alors remise portera la mention " Not valid for employment ".</p>
<p>Pour l’obtenir, adressez-vous au bureau le plus proche de votre domicile <a href="http://www.ssa.gov/" class="spip_out" rel="external">Social Security Administration</a></p>
<p>Vous devez vous y rendre personnellement pour remplir le formulaire SS-5 avec votre passeport et celui de vos enfants, votre visa américain et un document de l’I.N.S. tels que les formulaires I-94, I-551, I-688B ou I-766.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/vie-pratique/article/sante-113155). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
