# Emploi, stage

<h2 class="rub23540">Marché du travail</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/emploi-stage/#sommaire_1">Contexte</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/emploi-stage/#sommaire_2">Secteurs à fort potentiel</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/emploi-stage/#sommaire_3">Secteurs à faible potentiel</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/emploi-stage/#sommaire_4">Professions règlementées</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/emploi-stage/#sommaire_5">Rémunération</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Contexte</h3>
<p>Il est préférable de trouver un emploi avant d’arriver à Maurice. L’employeur fera les démarches pour l’obtention du permis de travail et du permis de résidence. Il est cependant très difficile d’obtenir un emploi car les sociétés savent que les démarches sont plus longues pour obtenir les permis.</p>
<h3 class="spip"><a id="sommaire_2"></a>Secteurs à fort potentiel</h3>
<p>Les secteurs à fort potentiel sont :</p>
<ul class="spip">
<li>les technologies de l’information et de la communication</li>
<li>toute activité liée à la formation</li>
<li>l’industrie bio-médicale</li>
<li>l’industrie/transformation de produits de la mer, et aquaculture</li>
<li>l’énergie durable</li>
<li>le développement immobilier</li>
<li>l’architecture</li>
<li>l’industrie océanique</li>
<li>l’agro-alimentaire</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Secteurs à faible potentiel</h3>
<p>Les secteurs à déconseiller sont le commerce et la restauration.</p>
<h3 class="spip"><a id="sommaire_4"></a>Professions règlementées</h3>
<p>Les activités non accessibles à un ressortissant étranger sont celles liées aux sports nautiques, la location de voitures et l’industrie de la canne à sucre. L’emploi des travailleurs étrangers dans le secteur de la construction est également contrôlé.</p>
<h3 class="spip"><a id="sommaire_5"></a>Rémunération</h3>
<p>Le salaire minimum pour un étranger est fixé à 45 000 MRU (environ 1125 €) à l’exception du secteur des TIC où il est fixé à 30 000 MRU (environ 750€).</p>
<p>Les salaires et les conditions d’emploi d’un expatrié doivent respecter les dispositions prévues dans les législations du travail.</p>
<p>Les salaires sont payés exclusivement à l’île Maurice et en roupies mauriciennes (MUR).</p>
<p>Les heures supplémentaires sont rémunérées à hauteur de 50% à 100% du salaire horaire du salarié, en fonction de la durée et du jour.</p>
<p><i>Mise à jour : juin 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-maurice-emploi-stage-article-stages.md" title="Stages">Stages</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-maurice-emploi-stage-article-curriculum-vitae.md" title="Curriculum vitae">Curriculum vitae</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-maurice-emploi-stage-article-recherche-d-emploi.md" title="Recherche d’emploi">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-maurice-emploi-stage-article-reglementation-du-travail.md" title="Réglementation du travail">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-maurice-emploi-stage-article-marche-du-travail-114401.md" title="Marché du travail">Marché du travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/emploi-stage/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
