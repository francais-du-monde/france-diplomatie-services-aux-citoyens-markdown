# Convention fiscale

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/fiscalite/article/convention-fiscale-110467#sommaire_1">Champ d’application de la convention</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/fiscalite/article/convention-fiscale-110467#sommaire_2">Dispositions conventionnelles concernant certaines catégories de revenus</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/fiscalite/article/convention-fiscale-110467#sommaire_3">Elimination de la double imposition</a></li></ul>
<p>La France et Israël ont signé à Jérusalem le 31 juillet 1995 une convention en vue d’éviter les doubles impositions et de prévenir l’évasion et la fraude fiscale en matière d’impôts sur le revenu et sur la fortune. Cette convention a été publiée au Journal officiel du 12 juin 1996 et est <strong>entrée en vigueur le 18 juillet 1996.</strong></p>
<p><a href="http://www.impots.gouv.fr/portal/deploiement/p1/fichedescriptive_1910/fichedescriptive_1910.pdf" class="spip_out" rel="external">Texte de la convention sur le site du ministère des Finances</a></p>
<p>Les dispositions conventionnelles, qui ont primauté sur les dispositions du droit interne selon l’article 55 de la Constitution Française, répartissent entre les deux Etats le droit d’imposer les revenus perçus par leurs résidents respectifs.</p>
<h3 class="spip"><a id="sommaire_1"></a>Champ d’application de la convention</h3>
<p>La convention s’applique aux résidents de chacun des Etats en matière d’impôts sur le revenu ou sur la fortune.</p>
<h4 class="spip">Impôts concernés (article 2)</h4>
<p>En France, cette convention concerne l’impôt sue le revenu, l’impôt sur les sociétés, la taxe sur les salaires et l’impôt de solidarité sur la fortune.</p>
<p>En Israël, cette convention concerne l’impôt sur le revenu, l’impôt sur les plus-values provenant des cessions immobilières, l’impôt foncier et l’impôt sur les employeurs.</p>
<h4 class="spip">Notion de résidence (article 3)</h4>
<p>Est "résident d’un Etat" toute personne qui, en vertu de la législation de cet Etat, est assujettie à l’impôt dans cet Etat, en raison de son domicile, de sa résidence, de son siège de direction, ou de tout autre critère de nature analogue.</p>
<p>Certaines personnes peuvent être déclarées résidentes des deux Etats, la convention définit alors les règles qui s’appliquent dans ce cas.</p>
<h3 class="spip"><a id="sommaire_2"></a>Dispositions conventionnelles concernant certaines catégories de revenus</h3>
<h4 class="spip">Revenus immobiliers (article 6)</h4>
<p>Les revenus provenant de biens immobiliers (y compris les revenus des exploitations agricoles ou forestières) ne sont imposables que dans l’Etat contractant où ces biens immobiliers sont situés.</p>
<h4 class="spip">Bénéfices des entreprises (article 7)</h4>
<p>Les bénéfices d’une entreprise d’un Etat ne sont imposables que dans cet Etat.</p>
<p>Toutefois, si une entreprise d’un Etat exerce son activité dans l’autre Etat par l’intermédiaire d’un établissement stable qui y est situé (notion définie à l’article 5 de la convention), les bénéfices imputables à cet établissement seront alors imposables dans l’Etat où il est situé.</p>
<h4 class="spip">Dividendes (article 10)</h4>
<p>Au sens de cette convention, le terme "dividende" désigne les revenus provenant d’actions, actions ou bons de jouissance, parts de mine, parts de fondateur ou autres parts bénéficiaires à l’exception des créances, ainsi que les revenus soumis au régime des distributions par la législation fiscale de l’Etat dont la société distributrice est un résident. Le terme <i>dividende</i> ne comprend pas les jetons de présence.</p>
<p>Le principe général est que les dividendes payés par une société résidente de l’un des deux Etats à un résident de l’autre Etat sont imposables dans cet autre Etat. Toutefois, ces dividendes sont aussi imposables dans l’Etat dans lequel réside la société qui les paie (cas détaillés dans les paragraphes 2 et suivants de l’article 10). Cette imposition n’affecte pas l’imposition de la société au titre des bénéfices qui servent au paiement des dividendes.</p>
<h4 class="spip">Intérêts (article 11)</h4>
<p>Au sens de cette convention, le terme <i>intérêts</i> désigne les revenus des créances de toute nature et notamment les revenus des fonds publics et des obligations d’emprunts, y compris les primes et lots attachés à ces titres.</p>
<p>Les intérêts provenant de l’un des Etats et payés à un résident de l’autre Etat sont imposables dans cet autre Etat, cependant ces intérêts sont aussi imposables dans l’Etat dont ils proviennent dans la limite de 10%. Certaines dispositions particulières existent, elles sont détaillées dans les paragraphes 2 et suivants de l’article 11 de la convention.</p>
<h4 class="spip">Redevances (article 12)</h4>
<p>Au sens de cette convention, le terme <i>redevances</i> désigne les rémunérations de toute nature payées pour l’usage, la concession de l’usage ou, par extension, la cession d’un droit de propriété intellectuelle.</p>
<p>Les redevances provenant de l’un des Etat et payées à un résident de l’autre Etat sont imposables dans cet autre Etat, cependant ces redevances sont aussi imposables dans l’Etat dont elles proviennent dans la limite de 10%. Certaines dispositions particulières existent, elles sont détaillées dans les paragraphes 2 et suivants de l’article 12 de la convention.</p>
<h4 class="spip">Gains en capital (article 13)</h4>
<p>Dans le cas général, les gains provenant de la cession d’un bien ne sont imposables que dans l’Etat dont le cédant est un résident.</p>
<p>Cependant des exceptions existent en fonction des situations et des biens cédés (liste non exhaustive, voir paragraphes 2 et suivants de l’article 13 de la convention) :</p>
<ul class="spip">
<li>Gains provenant de la cession de biens immobiliers : imposables dans l’Etat où ces biens immobiliers sont situés ;</li>
<li>Gains provenant de la cession d’actions ou parts qui font partie d’une participation substantielle d’une société résidente de l’un des Etats : imposables dans cet Etat dans la limite de 18 %.</li></ul>
<h4 class="spip">Professions indépendantes (article 14)</h4>
<p>Les revenus que tire d’une activité indépendante un résident d’un Etat ne sont imposables que dans cet Etat.</p>
<p>Cependant, si le professionnel concerné exerce aussi habituellement son activité dans l’autre Etat (+ de six mois par an, ou dans une « base fixe » établie dans l’autre Etat), il devra alors s’acquitter dans l’autre Etat de l’impôt sur les revenus concernant la fraction d’activité qu’il y a exercée.</p>
<h4 class="spip">Professions salariées (article 15)</h4>
<p>Les salaires, traitements et autres rémunérations similaires qu’un résident d’un Etat reçoit au titre d’un emploi salarié ne sont imposables que dans cet Etat.</p>
<p>Si l’activité est exercée dans l’autre Etat, ces revenus sont imposables uniquement dans cet autre Etat (sauf exceptions, voir article 15 paragraphes 2 et 3 de la convention).</p>
<h4 class="spip">Jetons de présence (article 16)</h4>
<p>Les jetons de présence et autres rétributions similaires qu’un résident d’un Etat reçoit en sa qualité de membre du conseil d’administration ou de surveillance d’une société résidente de l’autre Etat sont imposables dans cet autre Etat.</p>
<h4 class="spip">Artistes et sportifs (article 17)</h4>
<p>Les revenus qu’un résident d’un Etat tire de ses activités exercées dans l’autre Etat en tant qu’artiste ou sportif sont imposables dans cet autre Etat, sauf dans le cas où ces revenus sont financés principalement par des fonds publics de son Etat de résidence, auquel cas ils seront imposables uniquement dans ce dernier.</p>
<p>Ces dispositions s’appliquent aussi si le revenus ne sont pas versés directement à l’artiste ou au sportif mais à un intermédiaire, quel que soit son Etat de résidence.</p>
<h4 class="spip">Pensions (article 18)</h4>
<p>Les pensions, rentes viagères et autres rémunérations similaires ne sont imposables que dans l’Etat de résidence (sauf cas particulier des pensions payées par un Etat ou l’une de ses entités de droit public détaillé dans le paragraphe 2 de l’article 19 de la convention).</p>
<p>Rémunérations publiques (article 19)Les rémunérations, autres que les pensions, payées par un Etat (ou l’une de ses entités de droit public) à une personne au titre de services rendus ne sont imposables que dans cet Etat (sauf exceptions dans certains cas où les services ont été rendus dans l’autre Etat).</p>
<p>Si la rémunération publique au titre du service rendu intervient dans le cadre d’une activité industrielle ou commerciale, ce sont les dispositions générales des articles 15 (salariés), 16 (jetons de présences) et 18 (pensions) qui s’appliquent.</p>
<h3 class="spip">Enseignants et étudiants (article 20)</h3>
<p>Un résident d’un Etat qui séjourne dans l’autre Etat à seule fin d’enseignement ou de recherche d’intérêt public dans un établissement d’enseignement officiellement reconnu de cet autre Etat n’est imposable que dans son Etat de résidence initial sur les rémunérations correspondant à ces travaux. Cette disposition s’applique pendant une période allant jusqu’à deux ans après la première arrivée de cette personne dans l’Etat pour ces travaux.</p>
<p>Un résident d’un Etat qui séjourne dans l’autre Etat à seule fin d’y poursuivre ses études ou sa formation (y compris en tant que stagiaire) n’est imposable sur les sommes qu’il reçoit pour couvrir ses frais d’entretien, d’études ou de formation que dans son Etat de résidence initial à conditions qu’elles en proviennent.</p>
<h4 class="spip">Autres revenus (article 21)</h4>
<p>Les éléments du revenu, d’où qu’ils proviennent, qui ne sont pas traités spécifiquement par un article ci-dessus ne sont imposables que dans l’Etat de résidence de la personne qui les reçoit.</p>
<h4 class="spip">Fortune (article 22)</h4>
<p>La fortune d’un résident de l’un des Etats n’est imposable que dans cet Etat, à l’exception de celle constituée par les biens suivants :</p>
<ul class="spip">
<li>les biens immobiliers sont imposables uniquement dans l’Etat où ils sont situés, y compris si ces biens sont détenus par l’intermédiaire d’une société dont ils sont l’actif principal</li>
<li>les actions, parts et autres droits qui font partie d’une participation substantielle dans une société résidente d’un Etat sont imposables dans cet Etat</li>
<li>les biens mobiliers qui font partie de l’actif d’un établissement stable qu’une entreprise d’un Etat a dans l’autre Etat est imposable dans l’Etat où se situe l’établissement (de même pour les bases fixes utilisées pour l’exercice d’une professions indépendantes dans l’autre Etat).</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Elimination de la double imposition</h3>
<p>L’élimination de la double imposition pour les résidents en France s’opère au moyen du crédit d’impôt (plafonné) selon les modalités du paragraphe 1 de l’article 23 de la convention.</p>
<p>En Israël, sous réserve de dispositions contraires dans certains cas de la législation locale, l’impôt français payé à raison des revenus provenant de France ou de la fortune située en France est admis en déduction (plafonnée, voir paragraphe 2 de l’article 23 de la convention) de l’impôt israélien dû sur ces revenus ou cette fortune.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/fiscalite/article/convention-fiscale-110467). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
