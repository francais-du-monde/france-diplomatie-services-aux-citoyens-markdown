# Loisirs et culture

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/vie-pratique/article/loisirs-et-culture#sommaire_1">Tourisme</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/vie-pratique/article/loisirs-et-culture#sommaire_2">Sports</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/vie-pratique/article/loisirs-et-culture#sommaire_3">Télévision – Radio</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/vie-pratique/article/loisirs-et-culture#sommaire_4">Presse française</a></li></ul>
<p>La <a href="http://www.rts.sn/" class="spip_out" rel="external">Radiodiffusion Télévision du Sénégal</a> (RTS) est la chaîne nationale mais on compte plus d’une dizaine de stations de radio sur la bande FM (Sud FM, Nostalgie Dakar, Africa N°1, Radio Dunyaa …)</p>
<p>Sans parabole, la télévision se limite à la RTS, la chaîne officielle qui diffuse des programmes internationaux, parmi lesquels des émissions des chaînes françaises. Avec une antenne parabolique ou une antenne MMDS, et en fonction des abonnements, on peut recevoir une série de chaînes françaises (TV5, Canal + Horizon, Canal France International, TV5) et internationales (MCM, Euronews …).</p>
<p>La vidéo est répandue et il est conseillé d’apporter son magnétoscope (système SECAM-K.)</p>
<p>A Dakar, l’Institut Léopold Sédar Senghor propose des spectacles musicaux, chorégraphiques, des pièces de théâtre et des films. Des spectacles intéressants sont proposés au théâtre Sorano. Des expositions d’art sénégalais sont organisées à la Galerie nationale sénégalaise et dans des galeries privées. Il n’existe plus de cinémas à Dakar.</p>
<p>A Saint Louis, l’animation culturelle est assurée par l’Institut culturel français (diffusion de films récents, expositions, concerts). Des associations locales proposent de nombreuses manifestations musicales, sportives.</p>
<p><strong>Pour en savoir plus</strong> :</p>
<ul class="spip">
<li><a href="http://www.ambafrance-sn.org/-Francais-" class="spip_out" rel="external">Site de l’Ambassade de France au Sénégal</a> ;</li>
<li><a href="http://www.au-senegal.com/Agenda-culturel,1476.html" class="spip_out" rel="external">Au-senegal.com</a>.</li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Tourisme</h3>
<p>Au Sénégal, les centres d’intérêt sont variés, grâce à la présence d’un climat favorable, de la mer, des coutumes et traditions nationales.</p>
<p>Il existe à Dakar quelques sites pittoresques : le quartier de la Médina et son marché de Tylène, la Grande Mosquée, la place de l’Indépendance et le village artisanal de Soumbedioune où exercent les sculpteurs sur bois, tisserands et bijoutiers.</p>
<p>L’île de Gorée, à trois kilomètres de Dakar, ancienne plaque tournante de la traite des Noirs, permet d’aborder l’histoire africaine. On y trouve la célèbre Maison des Esclaves, ainsi qu’un musée historique, un musée de la femme et un musée océanographique d’un haut intérêt.</p>
<p>Les plages de sable fin du Sénégal s’étendent de la frontière de la Mauritanie à celle de la Guinée. De nombreux hôtels ont été construits au Cap Skirring et sur la petite Côte (Saly) à 83 km de Dakar.</p>
<p>Les parcs nationaux (Niokolo Koba, Sine Saloum) permettent l’observation de la faune dans un cadre parfois luxuriant.</p>
<p>Saint-Louis, ancienne capitale du Sénégal à 265 km de Dakar, offre le charme de son patrimoine architectural.</p>
<p>A voir également : les digues du delta, Richard Toll, à 100 km de Saint-Louis (rizières, champs de canne à sucre et territoire de chasse de 50.000 ha), le lac de Guiers et les parcs ornithologiques du Djoujd et de la Langue de Barbarie.</p>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li><a href="http://www.tourisme.gouv.sn/" class="spip_out" rel="external">Le site du ministère du Tourisme et des transports aériens sénégalais</a></li>
<li><a href="http://www.culture.gouv.sn/" class="spip_out" rel="external">Le site du ministère de la Culture du Sénégal</a></li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Sports</h3>
<p>De nombreux sports peuvent être pratiqués au sein de clubs sportifs : tous sports d’extérieur avec les limitations dues à la chaleur, tennis, golf, voile, ski nautique, équitation, natation. Il est possible de se procurer les équipements nécessaires à Dakar. Les rencontres sportives entre des équipes françaises et sénégalaises sont assez fréquentes.</p>
<p>Les plages de l’océan Atlantique ne sont pas aménagées et la natation peut se révéler dangereuse du fait des courants et de la force des vagues.</p>
<p>On peut pêcher toute l’année sans permis, en mer et en rivière. La pêche sportive nécessite la détention d’un permis de pêche (carte fédérale) délivré par la Fédération sénégalaise de pêche sportive.</p>
<p>La chasse est réglementée afin de protéger les espèces. Un permis de chasse ainsi qu’un permis de port d’arme sont indispensables (la déclaration des armes à l’entrée du pays est obligatoire). Il s’obtient auprès du bureau des affaires administratives du ministère de l’Intérieur du Sénégal. La chasse est ouverte de décembre à avril.</p>
<p>Pour en savoir plus :</p>
<ul class="spip">
<li><a href="http://www.ambafrance-sn.org/-Francais-" class="spip_out" rel="external">Le site du ministère du Tourisme et des transports aériens sénégalais</a></li>
<li><a href="http://www.au-senegal.com/-Sports-et-loisirs-au-Senegal-.html" class="spip_out" rel="external">Sports et loisirs au Sénégal</a></li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Télévision – Radio</h3>
<h4 class="spip">Radio</h4>
<p>Radio France Internationale (RFI) est accessible 24h/24 avec une bonne qualité de réception :</p>
<ul class="spip">
<li>92FM à Dakar</li>
<li>99.7 FM à Saint Louis</li>
<li>91.4 FM à Kaolack</li>
<li>87.6 FM à Ziguinchor</li>
<li>100.2 FM à Thiès</li>
<li>88.9 FM à Tambacounda</li></ul>
<h4 class="spip">Télévision</h4>
<p>Des abonnements à des bouquets satellites sont faciles à souscrire, avec une réception de qualité et à un coût raisonnable (Canal Horizon).</p>
<h3 class="spip"><a id="sommaire_4"></a>Presse française</h3>
<p>La quasi totalité de la presse française est disponible sur place ; les journaux quotidiens sont distribués avec 24h de délai.</p>
<p>Plusieurs librairies assurent la diffusion de la presse et des ouvrages français :</p>
<h4 class="spip">Dakar</h4>
<p>Librairie <strong>Aux quatre vents</strong><br class="manualbr">55, rue Felix Faure - BP 18 20, Dakar <br class="manualbr">Tél : 821 80 83 / 822 13 46</p>
<p>Mermoz : Pyrotechnie n°6 (route de Ouakam) <br class="manualbr">Tél : 869 10 37</p>
<p><a href="http://www.clairafrique.net/" class="spip_out" rel="external">Librairie Clair-Afrique</a> (dispose de trois points de vente au Sénégal)</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Place de l’Indépendance<br class="manualbr">B.P. 2005 - Quartier de Plateau - DAKAR <br class="manualbr">Tél : (+221) 33 822 21 69 frasl ; (+221) 33 849 49 99<br class="manualbr">Fax : (+221) 33 842 77 58 </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Av. Cheikh Anta DIOP - Rte de l’Université prolongée<br class="manualbr">B.P. 2005 - Quartier de Fann - DAKAR <br class="manualbr">Tél : (+221) 33 864 44 29 frasl ; (+221) 33 869 49 59<br class="manualbr">Fax : (+221) 33 864 58 54 </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Daniel Brottier - 116, Rue de Verdun<br class="manualbr">B.P. 3004 - Quartier de Aiglon - THIÈS <br class="manualbr">Tél : (+221) 33 951 24 33<br class="manualbr">Fax : (+221) 33 952 27 84 </p>
<h4 class="spip">Saint-Louis </h4>
<p><strong>Librairie Nord-Africa</strong><br class="manualbr">Rue Blanchot, BP 307 - tél. (221) 961-13-40</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.culture.gouv.sn/" class="spip_out" rel="external">site du ministère sénégalais de la Culture</a>. </p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/vie-pratique/article/loisirs-et-culture). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
