# Marché du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/emploi-stage/article/marche-du-travail#sommaire_1">Contexte</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/emploi-stage/article/marche-du-travail#sommaire_2">Secteurs à fort potentiel</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/emploi-stage/article/marche-du-travail#sommaire_3">Secteurs à faible potentiel</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/emploi-stage/article/marche-du-travail#sommaire_4">Professions règlementées</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/emploi-stage/article/marche-du-travail#sommaire_5">Rémunération</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Contexte</h3>
<h4 class="spip">Les perspectives</h4>
<p>En raison de la position stratégique et centrale de Singapour en Asie du Sud-Est et de ses innovations permanentes, le marché de l’emploi singapourien est ancré dans une position dynamique.</p>
<p>Malgré un taux de chômage très bas, le marché de l’emploi reste relativement hermétique. Le gouvernement tient à conserver les emplois pour les résidents locaux. Lorsqu’on est expatrié, il faut pouvoir justifier de ses compétences pour occuper un poste à Singapour et souvent prouver qu’un étranger est recherché pour ce type d’emploi.<br class="autobr">Il existe des secteurs d’emploi à Singapour relevant de législations particulières vis à vis des étrangers.</p>
<p>Comme cela est abordé dans le paragraphe "entrée, séjour", une prise de poste par un Français est conditionnée par l’obtention du permis de travail, demandé par l’entreprise-"sponsor" et délivré par le <i>Ministry of Manpower</i>, quel que soit le métier et l’environnement professionnel.</p>
<h3 class="spip"><a id="sommaire_2"></a>Secteurs à fort potentiel</h3>
<h4 class="spip">Les biotechnologies</h4>
<p>Singapour mène une politique volontariste et ambitieuse afin d’imposer le secteur des biotechnologies comme le quatrième pilier du secteur manufacturier du pays. L’objectif principal avoué du gouvernement est de développer la production et le nombre d’emploi.</p>
<p>Il faut noter que dans la zone Asie, Singapour est en forte compétition avec ses voisins (Japon, Corée, Taiwan, Chine, Thaïlande) dont les gouvernements sont tout aussi impliqués dans ce processus de développement des biotechnologies, principalement pour des applications liées à la santé humaine.</p>
<p>Les chiffres du secteur biomédical comprennent les secteurs pharmaceutiques, technologies médicales (équipements médico-hospitaliers), biotechnologies et services de santé (laboratoires, essais cliniques). Le secteur biomédical représente 10% de la production industrielle et le quart de sa valeur ajoutée, presque entièrement réalisée par l’industrie pharmaceutique.</p>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li><a href="http://www.biomed-singapore.com/" class="spip_out" rel="external">Biomed-Singapore.com</a> - site officiel singapourien de biotechnologie ;</li>
<li><a href="http://www.svca.org.sg/" class="spip_out" rel="external">Singapore Venture Capital Association</a> ;</li>
<li><a href="http://www.asiabiotech.com.sg/" class="spip_out" rel="external">Asiabiotech.com.sg</a> - publication bi-mensuelle des biotechs en Asie.</li></ul>
<h4 class="spip">Les TIC (technologies de l’information et de la communication)</h4>
<p>Le secteur des technologies de l’information reste, pour le gouvernement singapourien, le fer de lance de l’industrie nationale. C’est l’industrie électronique qui a tiré la croissance de l’Etat pendant les deux dernières décennies et une diversification s’est effectuée ces dernières années dans les autres secteurs des technologies de l’information, plus particulièrement le domaine du logiciel.</p>
<h4 class="spip">Restauration et hôtellerie</h4>
<p>Tous les professionnels du secteur s’accordent à dire que le recrutement est le principal problème de Singapour. La restauration n’attire pas les vocations chez les jeunes Singapouriens, préférant les emplois mieux rémunérés dans d’autres secteurs.</p>
<p>Les restaurants asiatiques dominent et sont segmentés par cuisine. On compte une centaine de restaurants de type occidental, dont une vingtaine de « français ». Le développement des restaurants français est freiné par le prix des cartes, leur lisibilité par le consommateur et le service jugé trop rigide. La tendance est au pot pourri de cuisines (« fusion »), avec cependant une nette avance pour du pseudo italien. Dans l’ensemble cependant, la clientèle des restaurants français a l’habitude de voyager et connaît cuisine et vins. Pour les autres consommateurs cependant, le premier modèle de cuisine occidental connu est le modèle anglo-saxon.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p><a href="http://www.stb.com.sg/" class="spip_out" rel="external">Singapore Tourism Board</a> ;<br class="autobr"><a href="http://www.sha.org.sg/" class="spip_out" rel="external">Singapore Hotel Association</a>.</p>
<h3 class="spip"><a id="sommaire_3"></a>Secteurs à faible potentiel</h3>
<ul class="spip">
<li>Distribution / prêt-à-porter</li>
<li>Textile</li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Professions règlementées</h3>
<h4 class="spip">Les professions médicales</h4>
<p>Les professions telles que médecins, infirmières, sages femmes, dentistes, pharmaciens et kinésithérapeutes sont normalement réservées aux ressortissants de Singapour. Il existe cependant des possibilités d’obtenir une reconnaissance de qualification et d’expérience professionnelle. Ces formalités doivent être effectuées par l’employeur. Il faut être conscient qu’il sera souvent impossible d’ouvrir un cabinet à son compte. Il faut au préalable trouver un employeur, généralement un médecin déjà agréé, qui emploiera le médecin ou spécialiste sous sa direction.</p>
<h5 class="spip">Enregistrement pour les médecins généralistes</h5>
<p>Le seul moyen d’exercer est de trouver un hôpital ou une institution prête à procéder à votre embauche. L’employeur fera alors une demande "d’accréditation" et de <i>registration</i> qui seront valables au maximum 12 mois.</p>
<p>Il n’est pas possible pour un médecin français d’exercer dans un cabinet privé sans être passé par le dispositif public et sans avoir commencé par exercer à Singapour sous la tutelle d’un médecin agréé.</p>
<p>Une liste des universités de médecine reconnues à Singapour a été établie. En France, les universités parisiennes Louis Pasteur et Pierre et Marie Curie délivrent des diplômes directement reconnus par le Conseil des médecins de Singapour. Ces médecins peuvent directement commencer à travailler sous une <i>conditional registration</i> pendant les six premiers mois, sous la tutelle d’un médecin déjà agréé, et ensuite passer en <i>full registration</i>.</p>
<p>Les personnes qui ne sont pas titulaires d’un diplôme délivré par l’une de ces universités françaises doivent d’abord commencer par s’enregistrer sous une <i>temporary registration</i> et répondre aux critères ci-dessous :</p>
<ul class="spip">
<li>le médecin doit soumettre un certificat du Conseil de l’Ordre des Médecins français prouvant qu’il a exercé durant les trois dernières années. Le certificat ne doit pas avoir plus de trois mois ;</li>
<li>le médecin doit obtenir un certificat de pratique dont le montant est de 250 SGD pour un an et de 500 SGD pour deux ans.</li></ul>
<p>Les documents à fournir, en original ou en copie, sont : diplôme, internats, pratique libérale, certificats des derniers emplois occupés, etc. Tous les documents doivent être traduits en anglais et certifiés conformes.</p>
<p>Il existe plusieurs types d’accréditations médicales pour les médecins généralistes expatriés qui souhaitent pratiquer la médecine à Singapour :</p>
<p><strong>Full registration</strong></p>
<p>Cette accréditation (250SGD) autorise les médecins à pratiquer à leur compte.</p>
<p>Le médecin doit être titulaire du MMBS décerné par the National University of Singapore et d’un certificat prouvant son expérience ou de qualifications reconnues par le <i>Medical Council</i> et posséder un certificat prouvant ses compétences. Il doit également prouver au <i>Medical Council</i> ses connaissances médicales, son expérience et la reconnaissance par ses pairs. Il doit avoir exercé au moins six mois à Singapour sous une <i>conditional registration</i>.</p>
<p><strong>Conditional registration</strong></p>
<p>Cette accréditation (125 SGD) autorise un médecin étranger à travailler dans un établissement de santé singapourien sous la supervision d’un médecin enregistré à Singapour.</p>
<p>Le médecin doit être titulaire d’un diplôme médical décerné par une école enregistrée au <i>medical registration</i> (les deux universités parisiennes Louis Pasteur et Pierre et Marie Curie) ou être titulaire de qualifications reconnues par le conseil des spécialistes</p>
<p>Le médecin doit être sélectionné par un hôpital ou une institution singapourienne approuvée par le conseil médical et doit également apporter la preuve au <i>Medical Council</i> de ses connaissances médicales, de son expérience et de la reconnaissance par ses pairs.</p>
<p>Au terme de de six mois d’un contrat en <i>conditional registration</i>, le médecin peut demander une <i>full registration</i>.</p>
<p><strong>Temporary registration</strong></p>
<p>Cette accréditation (150 SGD) autorise un médecin étranger à exercer pour une courte période (environ un an) renouvelable, auprès d’un établissement public singapourien :</p>
<ul class="spip">
<li>dans le but d’enseigner, d’élaborer une recherche ou de valider une thèse ;</li>
<li>si le médecin n’a pas été diplômé des deux universités françaises reconnues ;</li>
<li>si le médecin étranger souhaite exercer dans le cadre d’une rencontre professionnelle pour une durée très courte ;</li>
<li>si le médecin souhaite exercer dans un établissement médical pour une période prédéterminée et dans le cadre d’un exercice spécifique.</li></ul>
<p>Le médecin doit :</p>
<ul class="spip">
<li>avoir une offre d’emploi émanant d’une institution singapourienne accréditée par le <a href="http://www.smc.gov.sg/" class="spip_out" rel="external">Medical Singapore Council</a> ;</li>
<li>être enregistré auprès du Conseil de l’Ordre des Médecins en France ;</li>
<li>posséder au moins trois ans d’expérience à l’étranger ;</li>
<li>prouver que ses qualifications ou spécialités sont en adéquation avec sa demande de poste ;</li>
<li>passer un test d’anglais s’il n’a pas étudié en langue anglaise (score de 7 au IELTS et score de 250 pour la version électronique ou de 600 pour la version papier, au TOEFL).</li></ul>
<p>Les informations, formulaires et documents requis sont disponibles sur le site Internet du <a href="http://www.smc.gov.sg/" class="spip_out" rel="external">Medical Singapore Council</a>.</p>
<p>Des informations supplémentaires sont disponibles auprès du <a href="http://www.singhealth.com.sg/" class="spip_out" rel="external">Medical Manpower Singapore Health Services</a>.</p>
<h5 class="spip">Enregistrement pour les médecins spécialistes</h5>
<p>Le médecin spécialiste doit s’enregistrer auprès du registre des spécialistes avant de pouvoir être désigné comme spécialiste et de pouvoir utiliser ce titre.</p>
<p>Pour cela, il faut tout d’abord que le futur employeur obtienne pour le médecin une accréditation de spécialiste (<i>Certificate of Specialist Accreditation</i>) auprès du <i>Specialist Accreditation Board</i> établi par le <i>Medical Registration Act.</i></p>
<p>La demande ne peut se faire que pour une seule spécialité par médecin.</p>
<p>Après avoir obtenu cette accréditation, le futur employeur du praticien, maintenant reconnu officiellement en tant que spécialiste, doit faire la demande d’enregistrement (<i>Certificate of Specialist Registration</i>) auprès du <i>Singapore Medical Council </i>afin qu’il puisse exercer à Singapour.</p>
<p>Toutes ces informations sont disponibles sur le site Internet des professionnels de la santé : <a href="http://www.hpp.moh.gov.sg/" class="spip_out" rel="external">health professionals’ portal</a>.</p>
<p>Soyez attentif aux papiers à fournir afin d’être accrédité. Tout est détaillé sur le site, aux pages indiquées.</p>
<h5 class="spip">Les infirmières et sages femmes</h5>
<p>Les infirmières cherchant un emploi à Singapour doivent s’adresser aux institutions de santé locales qui leur permettront d’avoir accès aux offres d’emploi et qui les assistera dans leurs démarches d’enregistrement auprès du <i>Singapore Nursing board</i>.</p>
<p>Dans le cadre de la recherche d’emploi, le site du <a href="http://www.moh.gov.sg/" class="spip_out" rel="external">ministère de la Santé</a> répertorie tous les contacts des centres médicaux de Singapour.</p>
<p>Le <a href="http://www.snb.gov.sg/" class="spip_out" rel="external">Singapore Nursing Board</a> est chargé d’approuver les demandes d’enregistrement des infirmières et sages femmes afin de leur permettre, par la suite, de pouvoir exercer dans un cabinet, en tant qu’employées.</p>
<p>Les demandes d’accréditations sont étudiées tous les trois mois.</p>
<p>Pour s’enregistrer, il sera demandé de fournir, traduits en anglais, un certificat d’enregistrement <i>(certification of registration)</i> demandé auprès du Conseil national de l’Ordre des sages-femmes en France et du conseil des infirmiers en France, les certificats d’expérience, les contrats des emplois précédents, tous les certificats disponibles relatifs à ces métiers.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.hpp.moh.gov.sg/" class="spip_out" rel="external">Health professionals portal</a></p>
<h5 class="spip">Les dentistes</h5>
<p>Le <i>Singapore Dental Council </i>est chargé d’approuver les demandes d’enregistrement des dentistes étrangers qui souhaitent exercer à Singapour.</p>
<p>Pour cela, le dentiste doit préalablement trouver un employeur. Une fois la proposition d’embauche établie, l’employeur doit faire une demande d’enregistrement (<i>registration as a dentist</i>) auprès du <i>Singapore Dental Council</i>. Avec le formulaire, le dentiste devra transmettre la copie de ses diplômes, attestations d’expérience, numéros d’enregistrement du pays d’origine, lettre du futur employeur, frais d’enregistrement (liste non exhaustive), etc… La liste détaillée concernant cette procédure, ainsi que le formulaire à télécharger, sont disponibles sur le site du <a href="http://www.sdc.gov.sg/" class="spip_out" rel="external">Singapore Dental Council</a>.</p>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li><a href="http://www.aomss.org.sg/" class="spip_out" rel="external">Association of Oral and Maxillofacial Surgeons (Singapore)</a>.</li>
<li><a href="http://www.aos.org.sg/" class="spip_out" rel="external">Association of Orthodontists (Singapore)</a></li>
<li><a href="http://www.dentalhealth.org.sg/" class="spip_out" rel="external">Singapore Dental Health Foundation</a></li>
<li><a href="http://www.sda.org.sg/" class="spip_out" rel="external">Singapore Dental Association</a></li></ul>
<h5 class="spip">Les pharmaciens</h5>
<p>Pour pratiquer à Singapour, le pharmacien doit préalablement trouver un employeur. Suite à cette démarche, il doit être enregistré auprès du <i>Singapore pharmacy council </i>pour pouvoir exercer. Pour cela, il doit remplir un formulaire, disponible sur le site Internet du Singapore pharmacy council (<a href="http://www.spc.gov.sg/" class="spip_out" rel="external">www.spc.gov.sg/</a>) et fournir tous les justificatifs demandés ainsi que les droits d’enregistrement détaillés sur cette page.</p>
<p>Si l’enregistrement est approuvé par le conseil, le pharmacien doit alors exercer pendant une période considérée comme un stage, afin de valider ses compétences auprès du conseil singapourien. Cette période varie entre trois et douze mois. Tout dépend de la validation d’acquis obtenue ainsi que du besoin ou non de repasser des modules à Singapour.</p>
<p>Au terme de cette période, le pharmacien est alors officiellement enregistré (cotisation de 200 SGD) et peut travailler dans toute institution ou compagnie qui souhaite l’employer.</p>
<h5 class="spip">Les psychologues</h5>
<p>Pour exercer à Singapour, il n’y a pas de licence obligatoire ou d’enregistrement auprès des autorités.</p>
<p>Il est cependant conseillé de s’inscrire auprès de la <i>Singapore Psychological</i><i> Society </i>afin d’être reconnu. Pour être éligible, le futur membre doit se soumettre à différentes requêtes :</p>
<ul class="spip">
<li>posséder les diplômes de psychologie requis pour exercer dans son pays d’origine ;</li>
<li>être reconnu en tant que tel par le Conseil des Psychologues en France.</li></ul>
<p>Si la candidature pour être membre du <a href="http://www.singaporepsychologicalsociety.org/" class="spip_out" rel="external">Singapore Psychological Society</a><br class="autobr"> est acceptée, le psychologue sera, suivant sa spécialité, reconnu officiellement sous le titre de <i>clinical psychologist, counselling psychologist, educational psychologist, industrial</i> ou <i>occupational psychologist</i>, etc. et inscrit au registre des psychologues singapouriens.</p>
<h5 class="spip">Les avocats</h5>
<p>Dans tous les cas, les avocats étrangers <strong>ne sont pas autorisés</strong> à plaider devant les cours singapouriennes.</p>
<p>S’agissant des avocats français qui souhaiteraient travailler à Singapour, leur diplôme français n’étant pas reconnu, ils ne peuvent qu’être consultants dans des cabinets locaux ou étrangers.</p>
<p>En revanche, Singapour reconnaît les diplômes d’avocat délivrés dans les Etats membres du Commonwealth. Les avocats titulaires de tels diplômes peuvent être admis au Barreau de Singapour. Les diplômes d’avocat délivrés par quatre universités américaines (Harvard et Yale notamment) sont aussi reconnus à Singapour.</p>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li><a href="https://www.mlaw.gov.sg/content/minlaw/en.html" class="spip_out" rel="external">Ministry of Law</a></li>
<li><a href="http://www.lawsociety.org.sg/" class="spip_out" rel="external">Law Society of Singapore</a></li>
<li><a href="http://www.lawonline.com.sg/" class="spip_out" rel="external">Law Online, Singapore Legal portal</a></li></ul>
<h5 class="spip">Les architectes</h5>
<p>Pour pratiquer l’architecture à Singapour, une licence est requise. Seules les personnes enregistrées au <i>Board of Architects </i>de Singapour peuvent avoir le titre d’architecte. Pour cela, l’architecte doit avoir, au préalable, obtenu dans son pays d’origine une licence d’architecture dans une école reconnue internationalement. Si le <i>Board of Architect </i>décide que la personne est éligible pour obtenir le titre, elle doit ensuite avoir une expérience pratique dans un cabinet d’architecture de Singapour. La durée de cette pratique va dépendre de l’expérience passée.</p>
<p>Si la personne a peu d’expérience : il faut au moins qu’elle ait deux ans de pratique dont au minimum 12 mois consécutifs à Singapour, plus un examen pratique à passer.</p>
<p>Si la personne possède au moins trois ans d’expérience, il lui faudra en plus valider deux ans à Singapour dont 12 mois consécutifs au cours des cinq années précédant la demande d’application.</p>
<p>Les personnes ayant plus de 10 ans d’expérience peuvent être reconnues sans le passage obligé de la pratique locale.</p>
<p>Cependant, un architecte français peut être embauché directement et sans accréditation dans un cabinet et exercer l’architecture s’il travaille sous la direction d’un architecte reconnu par le <i>Board of Architects</i>. Il ne pourra simplement pas utiliser le titre officiel d’architecte pour lui.</p>
<p>Toutes les informations détaillées sont disponibles auprès du <a href="http://www.boa.gov.sg/" class="spip_out" rel="external">Board of Architects</a>.</p>
<h3 class="spip"><a id="sommaire_5"></a>Rémunération</h3>
<p>Le salaire est le fruit de la négociation entre employeur et employé. Il n’existe pas de salaire minimum à Singapour.</p>
<p>Si les salariés singapouriens acceptent que leurs salaires ne soient pas augmentés en période de marasme économique, ils s’attendent à être davantage rémunérés en période de croissance économique.</p>
<p>A Singapour les rémunérations sont nettes, puisqu’il n’existe pas de charges sociales, un contrat signé à 5000 SGD/mois équivaut donc à percevoir 5000 SGD.</p>
<p>La part variable du salaire est très importante pour les employés singapouriens. Le versement d’un treizième mois est un usage répandu à Singapour. Par ailleurs, les entreprises versent des bonus de manière presque systématique, qui varient en principe d’un à quatre mois de salaire.</p>
<p>Les employés singapouriens se tournent fréquemment vers des propositions financièrement plus attrayantes. Pour fidéliser leurs employés, les entreprises ont intérêt à les retenir en leur offrant des promotions, des augmentations de salaires, des stages à l’étranger, des bonus, des plans de carrières, des avantages en nature, des prêts, etc.</p>
<p>Cette flexibilité des salaires est relativisée par l’existence des recommandations émises par le <i>National Wage Council </i>(NWC). Ce comité tripartite rassemble des représentants syndicaux d’entreprises et du gouvernement. Il publie par voie de presse, une fois par an, les directives qui serviront de base aux négociations salariales entre employeurs et employés.</p>
<p>Les recommandations du NWC pour l’année prennent en considération plusieurs facteurs :</p>
<ul class="spip">
<li>les performances économiques de l’année précédente ;</li>
<li>les conditions du marché du travail, l’augmentation des salaires, de la productivité et des prix, la compétitivité des coûts, les perspectives de croissance de l’année.</li></ul>
<p>Le Ministry Of Manpower publie chaque année, au mois de mai, un rapport sur les salaires pratiqués à Singapour (<i>Report on Wages in Singapore</i>). Les salaires mentionnés ci-dessous proviennent de ce rapport. Il s’agit des salaires avant déductions de la part salariale des charges sociales (pour les singapouriens) et avant paiement de l’impôt sur les revenus. Ces salaires n’incluent pas les charges patronales, les bonus, le paiement des heures supplémentaires et les avantages en nature.</p>
<p>Il convient de souligner que les salaires mentionnés dans le rapport du <i>Ministry Of Manpower </i>représentent une moyenne et qu’ils varient en fonction de divers facteurs (secteur d’activité, chiffre d’affaire, performance de l’entreprise).</p>
<p><strong>Salaires moyens en 2010</strong></p>
<table class="spip">
<thead><tr class="row_first"><th id="id0396_c0">Fonctions</th><th id="id0396_c1">Salaires mensuels en SGD</th></tr></thead>
<thead><tr class="row_first"><th id="id0396_c0">Fonctions d’encadrement</th><th id="id0396_c1"></th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="id0396_c0">Directeur administratif</td>
<td class="numeric " headers="id0396_c1">4460</td></tr>
<tr class="row_even even">
<td headers="id0396_c0">Directeur des relations publiques et publicité</td>
<td class="numeric " headers="id0396_c1">4665</td></tr>
<tr class="row_odd odd">
<td headers="id0396_c0">Directeur financier</td>
<td class="numeric " headers="id0396_c1">6013</td></tr>
<tr class="row_even even">
<td headers="id0396_c0">Responsable projet dans le BTP</td>
<td class="numeric " headers="id0396_c1">4489</td></tr>
<tr class="row_odd odd">
<td headers="id0396_c0">Business Development Manager</td>
<td class="numeric " headers="id0396_c1">6216</td></tr>
<tr class="row_even even">
<td headers="id0396_c0">PDG</td>
<td class="numeric " headers="id0396_c1">9785</td></tr>
<tr class="row_odd odd">
<td headers="id0396_c0">Corporate Planning Manager</td>
<td class="numeric " headers="id0396_c1">6986</td></tr>
<tr class="row_even even">
<td headers="id0396_c0">Directeur publicité</td>
<td class="numeric " headers="id0396_c1">7728</td></tr>
<tr class="row_odd odd">
<td headers="id0396_c0">Responsable service clientèle</td>
<td class="numeric " headers="id0396_c1">5453</td></tr>
<tr class="row_even even">
<td headers="id0396_c0">Directeur général</td>
<td class="numeric " headers="id0396_c1">11000</td></tr>
<tr class="row_odd odd">
<td headers="id0396_c0">Managing director</td>
<td class="numeric " headers="id0396_c1">15000</td></tr>
<tr class="row_even even">
<td headers="id0396_c0">Responsable site de production</td>
<td class="numeric " headers="id0396_c1">5952</td></tr>
<tr class="row_odd odd">
<td headers="id0396_c0">Directeur des opérations (commerce)</td>
<td class="numeric " headers="id0396_c1">4409</td></tr>
<tr class="row_even even">
<td headers="id0396_c0">Directeur des opérations (finance)</td>
<td class="numeric " headers="id0396_c1">8022</td></tr>
<tr class="row_odd odd">
<td headers="id0396_c0">Directeur des ressources humaines</td>
<td class="numeric " headers="id0396_c1">6033</td></tr>
<tr class="row_even even">
<td headers="id0396_c0">Directeur commercial et marketing</td>
<td class="numeric " headers="id0396_c1">8952</td></tr>
<tr class="row_odd odd">
<td headers="id0396_c0"><strong>Autres Professions</strong></td>
<td class="numeric " headers="id0396_c1"></td></tr>
<tr class="row_even even">
<td headers="id0396_c0">Comptable</td>
<td class="numeric " headers="id0396_c1">3824</td></tr>
<tr class="row_odd odd">
<td headers="id0396_c0">Avocat</td>
<td class="numeric " headers="id0396_c1">7000</td></tr>
<tr class="row_even even">
<td headers="id0396_c0">Ingénieur aéronautique</td>
<td class="numeric " headers="id0396_c1">3784</td></tr>
<tr class="row_odd odd">
<td headers="id0396_c0">Auditeur</td>
<td class="numeric " headers="id0396_c1">3024</td></tr>
<tr class="row_even even">
<td headers="id0396_c0">Architecte</td>
<td class="numeric " headers="id0396_c1">5258</td></tr>
<tr class="row_odd odd">
<td headers="id0396_c0">Business Management Consultant</td>
<td class="numeric " headers="id0396_c1">5858</td></tr>
<tr class="row_even even">
<td headers="id0396_c0">Ingénieur informatique</td>
<td class="numeric " headers="id0396_c1">5874</td></tr>
<tr class="row_odd odd">
<td headers="id0396_c0">Analyste crédit</td>
<td class="numeric " headers="id0396_c1">4424</td></tr>
<tr class="row_even even">
<td headers="id0396_c0">Ingénieur électronique</td>
<td class="numeric " headers="id0396_c1">3560</td></tr>
<tr class="row_odd odd">
<td headers="id0396_c0">Analyste financier</td>
<td class="numeric " headers="id0396_c1">4741</td></tr>
<tr class="row_even even">
<td headers="id0396_c0">Journaliste</td>
<td class="numeric " headers="id0396_c1">3984</td></tr>
<tr class="row_odd odd">
<td headers="id0396_c0">Ingénieur mécanique</td>
<td class="numeric " headers="id0396_c1">3702</td></tr>
<tr class="row_even even">
<td headers="id0396_c0">Ingénieur biomédical</td>
<td class="numeric " headers="id0396_c1">3767</td></tr>
<tr class="row_odd odd">
<td headers="id0396_c0"><strong>Professions techniques et professions assimilées</strong></td>
<td class="numeric " headers="id0396_c1"></td></tr>
<tr class="row_even even">
<td headers="id0396_c0">Technicien informatique</td>
<td class="numeric " headers="id0396_c1">1911</td></tr>
<tr class="row_odd odd">
<td headers="id0396_c0">Technicien en ingénierie électronique</td>
<td class="numeric " headers="id0396_c1">2070</td></tr>
<tr class="row_even even">
<td headers="id0396_c0">Vendeur</td>
<td class="numeric " headers="id0396_c1">2677</td></tr>
<tr class="row_odd odd">
<td headers="id0396_c0">Technicien du bâtiment</td>
<td class="numeric " headers="id0396_c1">2343</td></tr>
<tr class="row_even even">
<td headers="id0396_c0">Réceptionniste</td>
<td class="numeric " headers="id0396_c1">1486</td></tr>
<tr class="row_odd odd">
<td headers="id0396_c0">Assistant (secrétariat)</td>
<td class="numeric " headers="id0396_c1">2470</td></tr>
<tr class="row_even even">
<td headers="id0396_c0">Chauffeur</td>
<td class="numeric " headers="id0396_c1">1471</td></tr>
</tbody>
</table>
<p><strong>Source</strong> : <a href="http://www.mom.gov.sg/" class="spip_out" rel="external">Ministry Of Manpower</a></p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/emploi-stage/article/marche-du-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
