# Lancement de l’inscription en ligne au registre des Français établis à l’étranger (17 juin 2016)

<p class="chapo">
      Matthias Fekl, secrétaire d’Etat chargé du commerce extérieur, de la promotion du tourisme et des Français de l’étranger, a présenté hier le nouveau service d’inscription en ligne au registre des Français établis à l’étranger.
</p>
<p><strong>L’inscription, qui s’effectuait jusqu’à présent sur place dans nos consulats, se fait depuis le 15 juin en ligne sur le site service-public.fr.</strong></p>
<p>Notre objectif est de faciliter les démarches administratives des Français vivant à l’étranger, comme par exemple les demandes de passeport ou de bourse scolaire. L’inscription sur les registres des Français établis à l’étranger permettra également, en particulier avant les échéances électorales prévues en 2017, de s’inscrire en ligne sur la liste électorale consulaire ou de demander la radiation d’une liste en cas de retour prévu en France.</p>
<p>Aujourd’hui, sur les quelque 2,5 millions de Français vivant à l’étranger, 1,7 million sont inscrits au registre des Français établis à l’étranger auprès de nos consulats. Cette inscription permet aussi de mieux informer nos compatriotes vivant à l’étranger, notamment sur les conditions de sécurité, les modalités d’organisation des élections ou encore le calendrier des tournées consulaires. Ce service bénéficiera aussi aux Français qui vont s’installer à l’étranger ou qui rentrent en France.</p>
<p><strong>Pour plus d’informations : </strong></p>
<p><a href="services-aux-citoyens-inscription-consulaire-et-communaute-francaise-article-pourquoi-et-comment-s-inscrire-au-registre-des-francais-etablis-hors-de-france.md" class="spip_out">http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/inscription-consulaire-et-communaute-francaise/article/pourquoi-et-comment-s-inscrire-au-registre-des-francais-etablis-hors-de-france</a></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/inscription-consulaire-et-communaute-francaise/article/lancement-de-l-inscription-en-ligne-au-registre-des-francais-etablis-a-l). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
