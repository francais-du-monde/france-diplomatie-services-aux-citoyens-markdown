# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/emploi-stage/article/reglementation-du-travail#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/emploi-stage/article/reglementation-du-travail#sommaire_2">Contrat de travail – spécificités</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/emploi-stage/article/reglementation-du-travail#sommaire_3">Fêtes légales</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/emploi-stage/article/reglementation-du-travail#sommaire_4">Emploi du conjoint</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<p>Le droit du travail au Togo est régi par :</p>
<ul class="spip">
<li>La loi n° 2006 portant Code du travail</li>
<li>La Convention collective interprofessionnelle du Togo (décembre 2011)</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Contrat de travail – spécificités</h3>
<p>En matière de contrat de travail, le régime de droit commun est le contrat à durée indéterminée ; le contrat à durée déterminée constitue l’exception.</p>
<p>L’engagement individuel des travailleurs a lieu par écrit conformément aux dispositions légales et réglementaires en vigueur.</p>
<p>Tout engagement effectué en l’absence d’un acte écrit constitue un contrat de travail en bonne et due forme.</p>
<p>Sauf dispositions contraires stipulées par écrit, le contrat est réputé à durée indéterminée.</p>
<h3 class="spip"><a id="sommaire_3"></a>Fêtes légales</h3>
<p><strong>Fêtes légales locales ouvrées et chômées</strong></p>
<ul class="spip">
<li>1er janvier : Nouvel an</li>
<li>27 avril : Fête Nationale</li>
<li>1er mai : Fête du Travail</li>
<li>21 juin : Fête des Martyrs</li>
<li>15 août : Assomption</li>
<li>1er novembre : Toussaint</li>
<li>25 décembre : Noël</li></ul>
<p><strong>Fêtes légales à date variable</strong></p>
<ul class="spip">
<li>Pâques</li>
<li>Pentecôte</li>
<li>Ascension</li>
<li>Ramadan</li>
<li>Tabaski</li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Emploi du conjoint</h3>
<p>Il existe peu de possibilités d’emploi sur place pour le conjoint.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/emploi-stage/article/reglementation-du-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
