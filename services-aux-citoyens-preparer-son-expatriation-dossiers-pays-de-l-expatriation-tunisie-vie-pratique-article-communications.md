# Communications

<h4 class="spip">Téléphone – Internet</h4>
<p><strong>Téléphone</strong></p>
<p>Les liaisons téléphoniques sont bonnes.</p>
<p>L’indicatif téléphonique pour la Tunisie est le 216 ; 216 71 pour Tunis (ainsi que pour Carthage et La Marsa), 216 74 pour Sfax.</p>
<p><strong>Internet</strong></p>
<p>Il existe une dizaine de fournisseurs d’accès. Il est parfois difficile de se connecter à certains sites Internet et les courriels sont contrôlés.</p>
<h4 class="spip">Téléphoner gratuitement par Internet </h4>
<p>Il est généralement très avantageux d’utiliser <strong>des logiciels de téléphonie sur IP</strong> (Skype, Google talk, Live messenger, Yahoo ! messenger, etc.) car les tarifs proposés à l’international sont souvent moindre que ceux des opérateurs locaux exploitant des lignes classiques, voire gratuits dans certaines conditions.</p>
<h4 class="spip">Poste</h4>
<p>Les liaisons postales offrent de bonnes garanties de réception.</p>
<p>La durée d’acheminement d’une lettre ordinaire varie de trois à six jours.</p>
<p>Pour les colis, les délais peuvent atteindre huit jours.</p>
<p><i>Mise à jour : novembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/vie-pratique/article/communications). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
