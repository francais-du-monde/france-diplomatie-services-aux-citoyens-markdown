# L’importation en France d’un véhicule

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/formalites-douanieres/article/l-importation-en-france-d-un#sommaire_1">Définitions</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/formalites-douanieres/article/l-importation-en-france-d-un#sommaire_2">Importation d’un véhicule en provenance d’un pays situé hors de l’Union européenne</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/formalites-douanieres/article/l-importation-en-france-d-un#sommaire_3">Importation d’un véhicule en provenance d’un état membre de l’Union européenne</a></li></ul>
<p>Cet article ne traite que des formalités relatives à l’importation en France d’un <strong>véhicule non utilitaire destiné à l’usage</strong> <strong>privé</strong> (voiture, voiturette, autocaravane, caravane, remorque et moto) doté d’un équipement standard, soumis à l’immatriculation et importé à titre personnel par un particulier.</p>
<p>Pour l’importation de tout autre type de véhicule, vous pouvez vous renseigner à la direction régionale des douanes de votre domicile. Pour connaître ses coordonnées, consultez le site Internet des <a href="http://www.douane.gouv.fr/" class="spip_out" rel="external">douanes françaises</a>.</p>
<h3 class="spip"><a id="sommaire_1"></a>Définitions</h3>
<p><strong>Droit(s) de douane</strong></p>
<p>Impôt qui frappe les marchandises à l’entrée ou à la sortie du territoire douanier. Il peut être calculé d’après un pourcentage appliqué à la valeur de la marchandise ou d’après la quantité de marchandises.</p>
<p>Taxe(s) Il s’agit de la taxe sur la valeur ajoutée (TVA) qui est actuellement de 19,6 % sur les véhicules.</p>
<h3 class="spip"><a id="sommaire_2"></a>Importation d’un véhicule en provenance d’un pays situé hors de l’Union européenne</h3>
<p><strong>Importation sans paiement des droits et taxes</strong></p>
<p>Si vous fixez <strong>votre résidence principale</strong> en France, vous pouvez bénéficier d’une franchise, c’est à dire du non-paiement des droits et taxes normalement exigibles, pour l’importation de votre véhicule si les conditions suivantes sont remplies et sous réserve que le véhicule soit mentionné sur la liste, établie en deux exemplaires, de vos biens personnels :</p>
<ul class="spip">
<li>avoir résidé <strong>au moins 12 mois</strong> dans un pays situé hors de l’Union européenne ;</li>
<li>avoir utilisé le véhicule à titre privé depuis <strong>au moins six mois</strong> avant le transfert de résidence ;</li>
<li>avoir acquitté toutes les taxes douanières et/ou fiscales exigibles dans le pays de provenance ou d’origine du véhicule. A noter que le véhicule doit être importé en France dans les 12 mois suivant la date de transfert de votre résidence principale en France. Par ailleurs, vous ne pourrez vous dessaisir du véhicule admis en franchise de droits et taxes avant un délai de 12 mois suivant la date de son entrée en France.</li></ul>
<p>Vous devrez présenter au bureau des douanes compétent à raison de votre nouveau domicile, les documents suivants :</p>
<ul class="spip">
<li>un inventaire détaillé et estimatif, daté et signé, établi en deux exemplaires de vos biens personnels sur lequel sera mentionné votre véhicule ;</li>
<li>le formulaire (Cerfa n°10070*02), établi en deux exemplaires, de déclaration d’entrée en France en franchise de biens personnels en provenance de pays tiers à l’UE ; ce formulaire est disponible sur le site Internet des <a href="http://www.douane.gouv.fr/" class="spip_out" rel="external">douanes françaises</a> ;</li>
<li>tout document attestant de votre résidence principale dans un pays situé hors de l’Union européenne et du transfert définitif de résidence en France ; vous pourrez, notamment, demander, <strong>avant le départ du pays de résidence</strong>, un certificat de changement de résidence auprès du consulat français dont vous dépendez ;</li>
<li>le certificat d’immatriculation dans le pays de provenance ou tout document officiel équivalent. En cas de besoin, il vous sera demandé une traduction de ce document ;</li>
<li>la facture d’achat du véhicule. En cas de besoin, il vous sera demandé une traduction de ce document. A noter que le véhicule doit être muni de plaques d’immatriculation étrangères valides.</li></ul>
<p>Les services douaniers vous remettront un exemplaire visé de votre inventaire, ainsi qu’un certificat n° 846 A que vous devrez présenter, dans un délai maximum de quatre mois, à la préfecture pour l’immatriculation de votre véhicule dans une série normale.</p>
<p><strong>Importation avec paiement des droits et taxes</strong></p>
<p>Le paiement des droits et taxes auprès d’un bureau de douane, en vue de l’immatriculation du véhicule pour un usage privé, sera exigé dans les cas suivants :</p>
<ul class="spip">
<li>si une des conditions indiquées n’est pas remplie ;</li>
<li>S’il s’agit d’un achat en dehors de l’Union européenne, dans un DOM ou dans une collectivité territoriale d’Outre-Mer ; le service des douanes établira, sur vos indications, une taxation d’office payable en espèces ou par chèque. Au-delà d’un montant de 1 524,49 euros, votre chèque devra être certifié. Vous pouvez, néanmoins, remplacer le chèque bancaire certifié par un chèque de banque (chèque tiré par l’établissement bancaire sur lui-même).</li></ul>
<p>Après le dédouanement, le service vous remettra :</p>
<ul class="spip">
<li>un exemplaire de la déclaration de douane ;</li>
<li>une quittance attestant du paiement des droits et taxes ;</li>
<li>un certificat n° 846 A que vous devrez présenter, dans un délai maximum de quatre mois, à la préfecture où sera immatriculé votre véhicule. Règles communes à l’immatriculation. Après avoir effectué les opérations de dédouanement, vous devrez faire immatriculer votre véhicule dans une série française normale auprès d’une préfecture.</li></ul>
<p>Dans tous les cas, pour pouvoir être immatriculé, votre véhicule sera soumis à un contrôle de réception à titre isolé (identification du véhicule, examen de bon état général et de la conformité aux normes) par la direction régionale de l’environnement, de l’aménagement et du logement (DREAL, ex DRIRE) compétente à raison de votre domicile.</p>
<p>Pour en savoir plus concernant les formalités à effectuer, les modalités de taxation et de paiement, vous pouvez consulter le site Internet des <a href="http://www.douane.gouv.fr/" class="spip_out" rel="external">douanes françaises</a>. Concernant les formalités d’immatriculation de votre véhicule, vous pouvez consulter les sites Internet suivants :</p>
<ul class="spip">
<li>le <a href="http://www.service-public.fr/" class="spip_out" rel="external">portail de l’administration française</a> ;</li>
<li>La <a href="http://www.developpement-durable.gouv.fr/" class="spip_out" rel="external">liste des DREAL</a></li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Importation d’un véhicule en provenance d’un état membre de l’Union européenne</h3>
<p>Ne sont traitées ci-dessous que les formalités concernant l’importation en France d’un véhicule à moteur, ainsi que les remorques destinées à leur être attachées.</p>
<p><strong>Définitions</strong></p>
<p>En matière d’échanges intracommunautaires est considéré comme <strong>véhicule neuf</strong> un véhicule ayant moins de 6 mois ou ayant parcouru moins de 6 000 km au jour de son importation en France et comme <strong>véhicule d’occasion</strong> un véhicule qui a plus de 6 mois et a parcouru plus de 6 000 km au jour de son introduction en France.</p>
<p>Sont considérés comme <strong>communautaires </strong>les véhicules qui ne sont pas passibles des droits et taxes dus à l’importation, soit parce qu’ils sont d’origine communautaire, soit parce que les droits et taxes ont été acquittés à l’entrée dans l’Union européenne.</p>
<p><strong>Les principes</strong></p>
<p>L’acquisition d’un véhicule acheté neuf ou d’occasion dans un autre Etat membre de l’Union européenne ne fait pas l’objet de formalités douanières.</p>
<p><strong>Vous devez acquitter la TVA en France</strong> si vous avez acheté un véhicule neuf (voir définition ci-dessus) dans un autre état membre.</p>
<p>Par conséquent, et afin de ne pas acquitter 2 fois cette taxe, les véhicules achetés neufs dans un autre état membre doivent l’être hors TVA.</p>
<p>Dans le cas où la TVA a été acquittée dans le pays d’achat, vous devez demander son remboursement auprès des autorités compétentes du pays d’achat.</p>
<p>Le paiement de cette taxe s’effectue auprès des services de la Direction générale des Finances publiques (recette des impôts).</p>
<p><strong>Vous n’aurez pas à acquitter la TVA en France : </strong></p>
<ul class="spip">
<li>dans le cas d’un véhicule d’occasion (vois définition ci-dessus)</li>
<li>dans le cas de l’introduction en France d’un véhicule neuf effectuée dans le cadre d’un transfert de résidence.</li></ul>
<p>Dans tous les cas (acquittement ou non de la TVA), les services des impôts vous délivreront un certificat fiscal que vous devrez présenter aux services préfectoraux pour pouvoir procéder à l’immatriculation de votre véhicule.</p>
<p>Ce certificat fiscal (modèle 1993 VT REC) doit être demandé auprès du service des impôts des entreprises de votre domicile dans un délai de 15 jours suivant la livraison. Les documents à présenter au service des impôts sont les suivants :</p>
<ul class="spip">
<li>original et copie de la facture, ou du document en tenant lieu, remis par le vendeur ;</li>
<li>original ou copie du certificat d’immatriculation délivré à l’étranger ;</li>
<li>le cas échéant, lorsque les documents présentés sont rédigés dans une autre langue que le français, une traduction certifiée conforme pourra être exigée en cas de doute ou d’incompréhension quant à leur contenu. A noter que le véhicule doit être équipé de plaques d’immatriculation étrangères valides.</li></ul>
<p><strong>Conformité technique et contrôle technique du véhicule</strong></p>
<p>Avant d’immatriculer votre véhicule, l’administration vérifiera sa conformité technique (procédure dite de "réception"). Elle s’assurera également de son bon état de fonctionnement en procédant à un contrôle technique.</p>
<p>La procédure de "réception" permet de vérifier si un véhicule est conforme aux caractéristiques techniques prévues par la loi. Les véhicules réceptionnés doivent être accompagnés d’un certificat de conformité.</p>
<p>Depuis le 1er janvier 1996, les procédures de réception sont harmonisées dans toute l’Union européenne en ce qui concerne les véhicules particuliers. Si votre véhicule est d’un modèle qui a obtenu une réception CE, le certificat de conformité qui l’accompagne et qui est délivré par le constructeur est valable dans tous les États membres de l’Union européenne. Si votre véhicule a reçu une réception seulement à portée nationale, les autorités françaises peuvent refuser le certificat de conformité national de votre véhicule pour des raisons de protection de la sécurité ou de l’environnement. Ce refus doit être dûment motivé.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li>site du <a href="http://www.ants.interieur.gouv.fr/siv/-siv-.html" class="spip_out" rel="external">système d’immatriculation des véhicules (SIV)</a></li>
<li>les <a href="http://www.developpement-durable.gouv.fr/Liste-des-22-DREAL.html" class="spip_out" rel="external">directions régionales de l’environnement, de l’aménagement et du logement (DREAL)</a></li>
<li>le <a href="http://www.service-public.fr/" class="spip_out" rel="external">portail de l’administration française</a> ;</li>
<li>le portail <a href="http://ec.europa.eu/youreurope/" class="spip_out" rel="external">L’Europe est à vous</a> de la Commission européenne.</li></ul>
<p><i>Mise à jour : mars 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/formalites-douanieres/article/l-importation-en-france-d-un). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
