# Légalisation et certification de signatures

<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-legalisation-et-notariat-legalisation-et-certification-de-signatures-article-la-legalisation-de-documents-publics-francais-destines-a-une-autorite-etrangere.md" title="La légalisation de documents publics français destinés à une autorité étrangère">La légalisation de documents publics français destinés à une autorité étrangère</a></li>
<li><a href="services-aux-citoyens-legalisation-et-notariat-legalisation-et-certification-de-signatures-article-autorites-competentes-et-nature.md" title="Autorités compétentes et nature des documents">Autorités compétentes et nature des documents</a></li>
<li><a href="services-aux-citoyens-legalisation-et-notariat-legalisation-et-certification-de-signatures-article-cout-de-la-legalisation.md" title="Coût de la légalisation">Coût de la légalisation</a></li>
<li><a href="services-aux-citoyens-legalisation-et-notariat-legalisation-et-certification-de-signatures-article-conditions-de-recevabilite-des.md" title="Conditions de recevabilité des actes publics par le bureau des légalisations">Conditions de recevabilité des actes publics par le bureau des légalisations</a></li>
<li><a href="services-aux-citoyens-legalisation-et-notariat-legalisation-et-certification-de-signatures-article-legalisation-par-correspondance.md" title="Légalisation par correspondance">Légalisation par correspondance</a></li>
<li><a href="services-aux-citoyens-legalisation-et-notariat-legalisation-et-certification-de-signatures-article-principaux-cas-particuliers.md" title="Principaux cas particuliers">Principaux cas particuliers</a></li>
<li><a href="services-aux-citoyens-legalisation-et-notariat-legalisation-et-certification-de-signatures-article-adresses-et-liens-utiles.md" title="Adresses et liens utiles">Adresses et liens utiles</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/legalisation-et-notariat/legalisation-et-certification-de-signatures/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
