# Santé

<p>En général, l’état sanitaire est bon. La qualité des soins est bonne dans les cliniques privées, très moyennes dans les hôpitaux publics, même si le nouvel hôpital central de Nicosie est de très bon niveau.</p>
<p>Le coût des soins varient selon les prestations médicales.</p>
<p>Il est préférable d’éviter de boire l’eau du robinet.</p>
<p>Une attestation de non-séropositivité peut être demandée aux travailleurs immigrés.</p>
<p><strong>Contracter une assurance rapatriement sanitaire.</strong></p>
<p>Pour obtenir des soins médicaux à Chypre, les citoyens de l’Union européenne doivent être en possession d’un « E-formulaire » délivré par les autorités de leur propre Etat (sécurité sociale pour la France).</p>
<p>Si vous souhaitez effectuer un séjour à Chypre <strong>inférieur à un an</strong> (touristes, travailleurs ou étudiants), une <strong>carte européenne d’assurance maladie</strong> (<i>European Health Insurance Card</i>) devra être demandée à votre Caisse primaire d’assurance maladie avant votre départ. Ce document individuel (<strong>qui remplace, depuis le 1er juin 2004, l’ancien formulaire E-111</strong>) vous sera délivré sur présentation de votre carte vitale. La carte européenne est valable un an et vous permettra de bénéficier de soins médicaux ou hospitaliers à Chypre, sans avoir à faire l’avance des frais.</p>
<p>Pour tout séjour <strong>supérieur à un an</strong>, un formulaire <strong>E-106</strong> (pour les travailleurs, expatriés) ou <strong>E-121</strong> (pour les retraités) devra être demandé à la Caisse primaire d’assurance maladie avant le départ.</p>
<p>Il sera alors <strong>obligatoire</strong>, dès votre arrivée sur le territoire chypriote, de vous rendre au ministère de la Santé (<i>Ministry of Health</i>, 10 Markou Drakou, 1040 Nicosia / Tel : 22 40 01 28) ou au dans n’importe quel hôpital public, muni de ce formulaire E-106 ou E-121, afin que les autorités chypriotes vous délivrent une carte d’assurance maladie locale (<i>Cyprus Medical Card</i>).</p>
<p><strong>Numéros utiles</strong></p>
<ul class="spip">
<li>Hôpital général de Larnaca : 24 630 300.</li>
<li>Hôpital de Limassol : 25 330 777.</li>
<li>Hôpital général de Nicosie : 22 451 111.</li>
<li>Hôpital général de Paphos : 26 240 100.</li>
<li>Clinique privée Apollonio 22469000</li>
<li>Clinique privée Evangelistria 22410100</li>
<li>Clinique privée Hippokration 22356565</li></ul>
<p>Pour de plus amples renseignements, vous pouvez consulter le site de l’<a href="http://www.invs.sante.fr/" class="spip_out" rel="external">Institut de veille sanitaire (InVS)</a>, ainsi que celui de l’<a href="http://www.who.int/fr/index.html" class="spip_out" rel="external">OMS</a>, qui vous renseigneront sur l’état sanitaire de ce pays, ou le site de <a href="http://www.pasteur.fr/" class="spip_out" rel="external">l’institut Pasteur de Paris</a>.</p>
<p>Le site du<a href="http://www.cimed.org" class="spip_out" rel="external">CIMED</a> publie une fiche sur les villes de Nicosie et Limassol.</p>
<p>Pour une présentation des risques épidémiologiques ainsi que des conseils sur la médecine au quotidien, consultez les pages suivantes :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.ambafrance-cy.org/Liste-de-medecins" class="spip_out" rel="external">Conseils, liste de médecins et hôpitaux sur le site de l’Ambassade de France à Chypre</a>.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/vie-pratique/article/sante). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
