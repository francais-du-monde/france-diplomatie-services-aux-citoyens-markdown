# Communications

<h4 class="spip">Téléphone – Internet</h4>
<h4 class="spip">Téléphone</h4>
<p>Les liaisons téléphoniques sont instantanées et bonnes, mais relativement chères à l’international.</p>
<p>L’indicatif téléphonique du Vietnam à partir de la France est le - 00.84 - suivi de l’indicatif régional. Les délais pour les liaisons postales aériennes varient de six à dix jours.</p>
<h4 class="spip">Internet</h4>
<p>Les liaisons Internet sont satisfaisantes, possibilité de se connecter en ADSL.</p>
<p>Des cybercafés  sont présents à Hanoï, Ho Chi Minh-Ville, Nha Trang, Hoi An (Danang) et dans la plupart des agglomérations.</p>
<p>Pour plus d’information sur les télécommunications et Internet : <a href="http://www.consulfrance-hcm.org/article.php3?id_article=49" class="spip_out" rel="external">Consulat de France à Ho Chi Minh Ville</a> et <a href="http://www.ambafrance-vn.org/" class="spip_out" rel="external">Ambassade de France au Vietnam</a>.</p>
<h4 class="spip">Téléphoner gratuitement par Internet </h4>
<p>Il est généralement très avantageux d’utiliser <strong>des logiciels de téléphonie sur IP</strong> (Skype, Google talk, Live messenger, Yahoo ! messenger, etc.) car les tarifs proposés à l’international sont souvent moindre que ceux des opérateurs locaux exploitant des lignes classiques, voire gratuits dans certaines conditions.</p>
<p><i>Mise à jour : février 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/vie-pratique/article/communications). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
