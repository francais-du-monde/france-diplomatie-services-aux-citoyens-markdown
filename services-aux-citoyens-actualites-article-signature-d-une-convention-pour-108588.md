# Signature d’une convention pour une plateforme internationale d’échanges de données d’état civil  (4 octobre 2013)

<p>La France vient de signer une convention internationale dans le domaine de l’état civil.</p>
<p>Grâce à cette convention,<strong> les démarches administratives devraient être rendues plus accessibles pour l’ensemble des citoyens.</strong> En outre, les échanges internationaux devraient être facilités et sécurisés par le support électronique prévu.</p>
<p>C’est l’ambassadeur de France en Suisse, Michel Duclos, qui le 20 septembre 2013, a procédé à la signature de cette convention internationale de la Commission internationale de l’état civil (CIEC)  sur l’utilisation de la plateforme de la CIEC de communication internationale de données d’état civil par voie électronique.</p>
<p>Il s’agit là d’une étape importante dans le processus de modernisation, au niveau international, des échanges d’informations en matière d’état civil. <strong>La signature de cette convention s’inscrit, plus largement, dans le cadre des efforts de dématérialisation de l’état civil géré par le ministère des Affaires étrangères</strong> et d’une coopération accrue avec les pays membres de la CIEC, organisation internationale intergouvernementale fondée en 1948 et composée de 15 États membres dont 12 États membres de l’Union européenne.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.ciec1.org/" class="spip_out" rel="external">Site internet de la Commission Internationale de l’État Civil (CIEC)</a></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/actualites/article/signature-d-une-convention-pour-108588). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
