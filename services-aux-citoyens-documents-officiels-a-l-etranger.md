# Documents officiels à l’étranger

<p class="spip_document_84783 spip_documents spip_documents_center">
<img src="http://www.diplomatie.gouv.fr/fr/IMG/jpg/rubrique_docs-officiels_cle84de9f.jpg" width="660" height="140" alt=""></p>
<p>Vous trouverez dans cette rubrique les modalités de demandes de documents officiels  pouvant être délivrés par les services consulaires ainsi que des renseignements sur le permis de conduire à l’étranger.</p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-documents-officiels-a-l-etranger-article-comment-faire-une-demande-de-passeport.md" title="Comment faire une demande de passeport ?">Comment faire une demande de passeport ?</a></li>
<li><a href="services-aux-citoyens-documents-officiels-a-l-etranger-article-carte-nationale-d-identite.md" title="Carte nationale d’identité">Carte nationale d’identité</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-permis-de-conduire.md" title="Permis de conduire">Permis de conduire</a></li>
<li><a href="services-aux-citoyens-documents-officiels-a-l-etranger-article-attestation-de-recensement-et-de-la-participation-aux-journees-defense-et.md" title="Attestation de recensement et de la participation aux journées défense et citoyenneté">Attestation de recensement et de la participation aux journées défense et citoyenneté</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/documents-officiels-a-l-etranger/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
