# Communications

<h4 class="spip">Téléphone – Internet</h4>
<p>Le code téléphonique pour la Suède est le 00 + 46.</p>
<p><strong>Indicatifs des villes </strong></p>
<table class="spip">
<thead><tr class="row_first"><th id="id0c6e_c0">Ville  </th><th id="id0c6e_c1">Indicatif  </th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="id0c6e_c0">Borås</td>
<td class="numeric " headers="id0c6e_c1">33</td></tr>
<tr class="row_even even">
<td headers="id0c6e_c0">Falkenberg</td>
<td class="numeric " headers="id0c6e_c1">346</td></tr>
<tr class="row_odd odd">
<td headers="id0c6e_c0">Falun</td>
<td class="numeric " headers="id0c6e_c1">23</td></tr>
<tr class="row_even even">
<td headers="id0c6e_c0">Göteborg</td>
<td class="numeric " headers="id0c6e_c1">31</td></tr>
<tr class="row_odd odd">
<td headers="id0c6e_c0">Jönköping</td>
<td class="numeric " headers="id0c6e_c1">36</td></tr>
<tr class="row_even even">
<td headers="id0c6e_c0">Karlstad</td>
<td class="numeric " headers="id0c6e_c1">54</td></tr>
<tr class="row_odd odd">
<td headers="id0c6e_c0">Kristianstad</td>
<td class="numeric " headers="id0c6e_c1">44</td></tr>
<tr class="row_even even">
<td headers="id0c6e_c0">Malmö</td>
<td class="numeric " headers="id0c6e_c1">40</td></tr>
<tr class="row_odd odd">
<td headers="id0c6e_c0">Norrköping</td>
<td class="numeric " headers="id0c6e_c1">11</td></tr>
<tr class="row_even even">
<td headers="id0c6e_c0">Norrköping</td>
<td class="numeric " headers="id0c6e_c1">63</td></tr>
<tr class="row_odd odd">
<td headers="id0c6e_c0">Skövde</td>
<td class="numeric " headers="id0c6e_c1">500</td></tr>
<tr class="row_even even">
<td headers="id0c6e_c0">Södertälje</td>
<td class="numeric " headers="id0c6e_c1">755</td></tr>
<tr class="row_odd odd">
<td headers="id0c6e_c0">Stockholm (et sa banlieue)</td>
<td class="numeric " headers="id0c6e_c1">8</td></tr>
<tr class="row_even even">
<td headers="id0c6e_c0">Strömstad</td>
<td class="numeric " headers="id0c6e_c1">526</td></tr>
<tr class="row_odd odd">
<td headers="id0c6e_c0">Trelleborg</td>
<td class="numeric " headers="id0c6e_c1">410</td></tr>
<tr class="row_even even">
<td headers="id0c6e_c0">Uddevalla</td>
<td class="numeric " headers="id0c6e_c1">522</td></tr>
<tr class="row_odd odd">
<td headers="id0c6e_c0">Uppsala</td>
<td class="numeric " headers="id0c6e_c1">18</td></tr>
<tr class="row_even even">
<td headers="id0c6e_c0">Varberg</td>
<td class="numeric " headers="id0c6e_c1">340</td></tr>
<tr class="row_odd odd">
<td headers="id0c6e_c0">Västerås</td>
<td class="numeric " headers="id0c6e_c1">21</td></tr>
<tr class="row_even even">
<td headers="id0c6e_c0">Ystad</td>
<td class="numeric " headers="id0c6e_c1">411</td></tr>
</tbody>
</table>
<p>Pour téléphoner d’une région à l’autre en Suède, ajouter le 0 devant l’indicatif.</p>
<p>Pour des informations sur l’<a href="http://www.se.all.biz/guide-phonecodes" class="spip_out" rel="external">indicatif d’autres villes de Suède</a>.</p>
<p>Le téléphone mobile est très largement utilisé en Suède. Il existe un choix important d’opérateurs pratiquant des tarifs variables (Telia, Telenor, 3, Tele2, Halebop, Comviq, Hallon…Glocalnet, Telia, Telenor, Com-Hem, etc.) avec des offres intéressantes pour les communications avec l’étranger. L’abonnement au téléphone peut être groupé avec internet et la télévision par câble.</p>
<p><strong>Utilisation de votre portable français</strong></p>
<p>Les clients reçoivent un message automatique les informant du coût des appels, des SMS et des services de transmission de données à l’étranger (en cas de navigation sur internet ou de téléchargement d’un film, p. ex.). Les prix de gros que les opérateurs se facturent mutuellement sont également plafonnés, ce qui devrait réduire les coûts supportés par les consommateurs. Un mécanisme a également été mis en place afin d’éviter aux consommateurs de devoir payer des factures astronomiques pour des services d’itinérance : après un premier avertissement, la connexion internet mobile est suspendue dès que le montant atteint un certain niveau. Une limite de 50 euros par mois s’applique par défaut si le client n’a pas défini un montant spécifique.</p>
<p>Vous trouverez sur un <a href="http://ec.europa.eu/information_society/activities/roaming/index_en.htm" class="spip_out" rel="external">site web de l’UE</a> (en anglais) les eurotarifs appliqués par les opérateurs des 27 pays membres, ainsi que les liens vers leurs sites. Vous pourrez également y comparer les tarifs pratiqués pour l’envoi de SMS ou l’utilisation de services de données.</p>
<p><i>Source : <a href="http://europa.eu/travel/comm/index_fr.htm#phone" class="spip_out" rel="external">Europa.eu</a></i></p>
<p><strong>Internet</strong> est très développé, même à l’extrême nord du pays. Il convient de présenter un <i>personnumer</i> pour s’abonner.</p>
<p><strong>Téléphoner gratuitement par Internet</strong></p>
<p>Il est généralement très avantageux d’utiliser <strong>des logiciels de téléphonie sur IP</strong> (Skype, Google talk, Live messenger, Yahoo ! messenger, etc.) car les tarifs proposés à l’international sont souvent moindre que ceux des opérateurs locaux exploitant des lignes classiques, voire gratuits dans certaines conditions.</p>
<h4 class="spip">Poste</h4>
<p>Les délais postaux varient entre deux et cinq jours (très bonne garantie de réception).</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/vie-pratique/article/communications). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
