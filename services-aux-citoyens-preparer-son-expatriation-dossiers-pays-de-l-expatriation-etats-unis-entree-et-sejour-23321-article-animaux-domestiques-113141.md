# Animaux domestiques

<p>L’importation d’animaux domestiques aux Etats-Unis est soumise aux dispositions réglementaires définies par les <a href="http://www.cdc.gov/" class="spip_out" rel="external">Centers for Disease Control and Prevention</a> (CDC) qui constituent une agence fédérale sous tutelle du ministère américain de la Santé.</p>
<p>En principe, les autorités fédérales ne réclament pas de certificats de santé pour les animaux de compagnie ; toutefois, ces documents sont régulièrement exigés pour entrer dans certains Etats et par les compagnies aériennes qui assurent la liaison vers les Etats-Unis. En conséquence, l’ambassade des Etats-Unis à Paris les inscrit dans la liste des documents dont doit se munir le voyageur le cas échéant.</p>
<p>Le site de l’<a href="http://www.usda-france.fr/media/amener-un-animal-aux-etats-unis.pdf" class="spip_out" rel="external">ambassade des Etats-Unis à Paris</a> détaille très clairement les conditions d’importation d’animaux domestiques aux Etats-Unis.</p>
<p>Il convient par ailleurs de s’informer au niveau local sur la réglementation de l’Etat de destination, qui peut contenir des dispositions particulières en matière de vaccination.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p> - Lisez notre article <a href="services-aux-citoyens-preparer-son-expatriation-douanes-article-animaux-domestiques.md" class="spip_in">Animaux domestiques</a>.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/entree-et-sejour-23321/article/animaux-domestiques-113141). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
