# Logement

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/vie-pratique/article/logement-110029#sommaire_1">Où se loger ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/vie-pratique/article/logement-110029#sommaire_2">Conditions de location</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/vie-pratique/article/logement-110029#sommaire_3">A Istanbul </a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/vie-pratique/article/logement-110029#sommaire_4">Electricité</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/vie-pratique/article/logement-110029#sommaire_5">Electroménager</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Où se loger ?</h3>
<p><strong>Ankara</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Prix moyen d’une chambre d’hôtel (chambre double)</strong></td>
<td>Euros</td></tr>
<tr class="row_even even">
<td>Grand tourisme</td>
<td>285</td></tr>
<tr class="row_odd odd">
<td>Moyen tourisme</td>
<td>200</td></tr>
</tbody>
</table>
<p><strong>Istanbul</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Prix moyen d’une chambre d’hôtel (chambre double)</strong></td>
<td>Euros</td></tr>
<tr class="row_even even">
<td>Grand tourisme</td>
<td>300</td></tr>
<tr class="row_odd odd">
<td>Moyen tourisme</td>
<td>180</td></tr>
</tbody>
</table>
<p><i>Les prix à Istanbul peuvent sensiblement augmenter en période de salons ou de séminaires internationaux</i>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Conditions de location</h3>
<p>L’agence immobilière est le meilleur moyen, pour les étrangers, de trouver un logement à Ankara. Toutefois, il est également possible de consulter les annonces dans la presse locale ou sur certains sites Internet.</p>
<p>Les loyers sont la plupart du temps, pour les expatriés, exprimés et payables en devises (euros ou dollars). A noter que la loi turque oblige les propriétaires à percevoir leurs loyers sur un compte bancaire.</p>
<p>Les charges immobilières (entretien des bâtiments, du jardin, chauffage collectif, …) sont réglées en monnaie locale. Le loyer et les charges immobilières ne comprennent généralement pas les frais d’électricité, d’eau, de gaz et bien sûr de téléphone ou d’Internet.</p>
<p>Il est habituellement demandé un mois de loyer d’avance, mais certains propriétaires peuvent demander davantage.</p>
<p>Si vous passez par une agence, le contrat de location sera établi en turc et en anglais. Il y sera joint un état des lieux établi à l’arrivée du locataire. Attention, il est difficile de rompre un bail sans préavis.</p>
<p>En ce qui concerne la sécurité, il convient d’être prudent. S’il y a peu de vols sur Ankara, la situation est plus délicate à Istanbul.</p>
<h4 class="spip">A Ankara </h4>
<p>Les quartiers de Cankaya, Kavaklidere et de Gaziosmanpasa sont les plus demandés par la communauté d’expatriés et les plus proches du centre-ville. Le quartier de Beysukent, neuf et résidentiel mais éloigné du centre-ville, offre de nombreuses possibilités de logements en maisons individuelles avec jardin. En règle générale, les possibilités de logements sont nombreuses.</p>
<p>A noter que le lycée français est situé à Incek à environ 25 minutes du centre-ville en voiture.</p>
<p>Ci-dessous quelques prix pratiqués en 2010 pour des biens de bon standing à luxueux :</p>
<ul class="spip">
<li>Appartement de deux chambres, non meublés, entre 80 et 120 m2 : 500 € à 1200 €</li>
<li>Appartement de trois chambres, non meublés, entre 120 et 160 m2 : 700 € à 3000 €</li>
<li>Appartement de quatre chambres, non meublés, entre 160 et 300 m2 : 1000 € à 8000 €</li>
<li>Maison de quatre chambres, environ 250 m2 : 1400 € à 8000 €.</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>A Istanbul </h3>
<p>Les quartiers résidentiels se situent sur la rive européenne (Bebek, Etiler, Yeniköy, Tarabya).</p>
<p>Les prix des loyers sont généralement fixés et payés en dollars US ou en euros. Le marché de l’immobilier est beaucoup plus difficile à Istanbul qu’à Ankara, le prix des loyers sensiblement plus élevé.</p>
<p>Les propriétaires Turcs sont attachés à la clientèle étrangère qui offre de nombreuses garanties. Toutefois, notez que le marché est très volatil et que les prix semblent fortement varier selon la demande, notamment durant l’été : ainsi un appartement proposé à 3500 <strong>USD </strong> la première semaine de juillet peut grimper à 3500 <strong>euros </strong> la semaine suivante, face à la forte demande d’expatriés.</p>
<p>Dans ces conditions, il est difficile d’indiquer le coût des loyers par catégorie d’appartement. On a pu relever qu’un appartement comportant trois chambres et d’une superficie d’environ 160m2, était proposé à la location entre 3500 et 6000 euros suivant les quartiers.</p>
<h3 class="spip"><a id="sommaire_4"></a>Electricité</h3>
<p>Le courant est alternatif, 220 volts, 50 Hertz. Les prises sont de type européen, avec des prises de terre spéciales.</p>
<h3 class="spip"><a id="sommaire_5"></a>Electroménager</h3>
<p>Les cuisines des appartements non meublés ne sont généralement pas équipées. Dans les appartements meublés on trouve four et plaques de cuisson, lave-linge, réfrigérateur, plus rarement lave-vaisselle et congélateur.</p>
<p>L’équipement ménager est disponible sur place. Les appareils de fabrication locale sont de qualité très correcte. Il est également possible de trouver du matériel d’importation dans les grandes surfaces ; les prix en sont évidemment supérieurs.</p>
<p>Le chauffage central au charbon (lignite) est remplacé par le fioul et le gaz.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/vie-pratique/article/logement-110029). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
