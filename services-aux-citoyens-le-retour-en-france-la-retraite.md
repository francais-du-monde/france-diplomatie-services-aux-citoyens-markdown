# Retraites

<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-le-retour-en-france-la-retraite-article-vous-revenez-d-un-pays-de-l-union-103674.md" title="Vous revenez d’un pays de l’Union européenne">Vous revenez d’un pays de l’Union européenne</a></li>
<li><a href="services-aux-citoyens-le-retour-en-france-la-retraite-article-vous-revenez-d-un-pays-hors-de-l-103673.md" title="Vous revenez d’un pays hors de l’Union européenne">Vous revenez d’un pays hors de l’Union européenne</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/la-retraite/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
