# Santé

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/vie-pratique/article/sante#sommaire_1">Médecine de soins</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/vie-pratique/article/sante#sommaire_2">Numéros utiles</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/vie-pratique/article/sante#sommaire_3">Carte européenne d’assurance maladie</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Médecine de soins</h3>
<h4 class="spip">Le système de soin </h4>
<p>De manière générale, le système belge est de très bonne qualité. Afin de bénéficier du remboursement des soins de santé et du paiement des indemnités d’incapacité de travail, tout bénéficiaire de l’assurance maladie doit s’affilier ou s’inscrire auprès d’un organisme assureur (mutualité, service régional de la Caisse nationale auxiliaire d’assurance maladie invalidité). Le choix de l’organisme assureur est libre. Vous avez le choix de souscrire une assurance complémentaire, qui offre une garantie spécifique selon l’organisme qui le propose et le type de contrat souscrit.</p>
<p>Le choix d’un médecin, comme en France, est totalement libre. En Belgique, 30 spécialités de médecine sont reconnues. Il existe, dans chaque spécialité, différentes catégories de médecins, avec des honoraires variables.</p>
<p>Le montant de remboursement par l’assurance soins de santé est identique, que le médecin soit conventionné ou pas (le surplus d’honoraire généralement pratiqué par le médecin non conventionné reste entièrement à charge du patient).</p>
<p>Il faut en principe faire l’avance des frais, en contrepartie d’une attestation de soins donnés. Cette attestation doit ensuite être transmise à l’organisme assureur (mutualité), qui effectue le remboursement.</p>
<p>Le montant de l’intervention de l’assurance dans le coût des prestations varie principalement en fonction de la nature de la prestation, du statut du bénéficiaire et de la qualité du dispensateur de soins. Dans la plupart des cas, il faut payer une quote-part personnelle ou « ticket modérateur » (25%).</p>
<p>Tous les assurés sociaux qui tombent dans le champ d’application de la sécurité sociale reçoivent une <strong>carte SIS</strong> (<i>Système Information Sociale</i>).</p>
<p>Cette carte peut être obtenue via la Caisse auxiliaire d’assurance maladie-invalidité ou auprès d’une mutuelle.</p>
<p>Vous devrez présenter votre carte SIS lors de l’achat de médicaments remboursables chez le pharmacien, lors de l’admission ou de soins à l’hôpital, lors de chaque contact avec la mutualité. Au recto de votre carte figure votre numéro de registre national.</p>
<p><i>Source : Guide la santé transfrontalière Nord Pas de Calais- Belgique</i></p>
<p>Les pharmacies sont fermées du samedi midi au lundi. Si les médecins généralistes et spécialistes consultent en général dans la journée, il est en revanche quasiment impossible d’obtenir un rendez-vous pour une consultation en soirée ou le samedi (hors services d’urgence hospitaliers).</p>
<p>Pour en savoir plus :</p>
<ul class="spip">
<li><a href="http://www.statbel.fgov.be/" class="spip_out" rel="external">Données statistiques sur la santé en Belgique</a></li>
<li><a href="http://www.sante-belgique.com/" class="spip_out" rel="external">Annuaire des professionnels de santé</a></li>
<li>Portail <a href="http://www.belgium.be/fr/sante/index.jsp" class="spip_out" rel="external">Belgium.be / Santé</a>.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Numéros utiles</h3>
<h4 class="spip">Sur l’ensemble du territoire </h4>
<ul class="spip">
<li>Service médical d’urgence et Pompiers : 100</li>
<li>Croix Rouge : 105</li>
<li>Centre Anti-Poison : 070.245.245</li></ul>
<h4 class="spip">Circonscription consulaire de Bruxelles</h4>
<ul class="spip">
<li>Centre des brûlés : (02) 268.62.00</li>
<li>Centre hospitalier Saint-Pierre : (02) 535.31.11</li>
<li>Hôpital Saint-Jean : (02) 221.91.11</li>
<li>Hôpital Erasme à Anderlecht : (02) 555.34.05</li>
<li>Hôpital Reine Elizabeth à Kokelberg : (02) 482.40.00</li>
<li>Universitar Ziekenhuis à Gand : (09) 240.21.11</li>
<li>Saint-Jean Hôpital à Bruges : (050) 45.21.11</li></ul>
<h4 class="spip">Circonscription consulaire d’Anvers</h4>
<ul class="spip">
<li>Centre des brûlés : (03) 217.75.95</li>
<li>Hôpital Middleheim : (03) 280.31.11</li>
<li>Centre hospitalier universitaire à Edegem : (03) 821.31.00</li>
<li>Virga Jesse Hopital à Hasselt : (011) 30.81.11</li>
<li>Salvator Hopital à Hasselt : (011) 28.91.11</li></ul>
<h4 class="spip">Circonscription consulaire de Liège</h4>
<ul class="spip">
<li>Centre des brûlés : (04) 366.72.94</li>
<li>Centre hospitalier universitaire : (04) 366.71.11</li>
<li>Hôpital de la Citadelle : (04) 225.61.11</li>
<li>CHR de Namur : (081) 72.61.11</li>
<li>Centre hospitalier Peltzer à Verviers : (087) 21.21.11</li>
<li>Clinique St Joseph à Arlon : (063) 23.11.11</li></ul>
<p><strong>Il est conseillé de consulter le médecin traitant avant le départ et de souscrire une assurance rapatriement.</strong></p>
<h3 class="spip"><a id="sommaire_3"></a>Carte européenne d’assurance maladie</h3>
<p><strong>La carte européenne d’assurance maladie</strong> (CEAM) atteste de vos droits à l’assurance maladie en Europe. Lors d’un séjour temporaire dans un Etat membre de l’Espace économique européen(EEE) ou en Suisse, elle vous permet de bénéficier de la prise en charge des soins médicalement nécessaires.</p>
<p><strong>Elle a une durée de validité maximale d’un an.</strong></p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li>Consultez le site internet du <a href="http://www.cleiss.fr/" class="spip_out" rel="external">Centre des liaisons européennes et internationales de sécurité sociale</a> (CLEISS).</li>
<li>Consultez la thématique <a href="http://www.ameli.fr/assures/droits-et-demarches/index.php" class="spip_out" rel="external">A l’étranger</a> sur le site internet de l’Assurance maladie en France.</li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/vie-pratique/article/sante). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
