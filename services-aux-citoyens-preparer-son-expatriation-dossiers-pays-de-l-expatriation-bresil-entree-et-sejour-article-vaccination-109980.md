# Vaccination

<p>Pour entrer sur le territoire brésilien, il convient d’être à jour pour les vaccins suivants : diphtérie, poliomyélite et tétanos. Les vaccinations contre les maladies suivantes sont également recommandées :</p>
<ul class="spip">
<li>Hépatite Virale A</li>
<li>Typhoïde</li>
<li>Hépatite Virale B (pour des séjours longs et répétés)</li>
<li>Rage (pour les séjours en dehors des grandes villes)</li>
<li>Fièvre jaune.</li></ul>
<p>Pour le Brésil, la vaccination contre la fièvre jaune est exigée pour les personnes se rendant dans de nombreux états fédéraux (Acre, Amapa, Amazonas, Distrito Federal, Goias, Maranhao, Mato Grosso, Mato Grosso do Sul, Minais Gerais, Para, Rondônia, Roraima, Tocatins, quelques parties des états de Bahia, Espiritu Santo, Piaui, Parana, Rio Grande do Sul, Santa Catarina et Sao Paulo). Pas de transmission de la maladie dans les zones côtières.</p>
<p>Même si la vaccination contre la fièvre jaune n’est pas exigée pour tous les voyageurs à destination du Brésil, elle reste nécessaire partout où le risque existe. Enfin, les enfants doivent être à jour dans leur calendrier vaccinal.</p>
<p>La dengue, maladie virale transmise par les piqûres de moustiques, est également présente au Brésil et impose le respect des mesures habituelles de protection (vêtements longs, produits anti-moustiques à utiliser sur la peau et sur les vêtements, diffuseurs électriques) et l’attention des personnes les plus vulnérables). Cette affection pouvant prendre une forme potentiellement grave (forme hémorragique) il convient, en cas de fièvre, de consulter un médecin et d’éviter la prise d’aspirine.</p>
<p>Pour en savoir plus, lisez <a href="services-aux-citoyens-preparer-son-expatriation-sante.md" class="spip_in">notre article thématique sur la vaccination</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/entree-et-sejour/article/vaccination-109980). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
