# Lettre de motivation

<h4 class="spip">Rédaction</h4>
<p>La lettre de motivation, de préférence manuscrite, doit être particulièrement soignée dans sa présentation (paragraphes clairs), intéresser le recruteur et susciter son désir de rencontrer le rédacteur.</p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/emploi-stage/article/lettre-de-motivation). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
