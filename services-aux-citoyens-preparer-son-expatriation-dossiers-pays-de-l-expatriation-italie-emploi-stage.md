# Emploi, stage

<h2 class="rub22817">Marché du travail</h2>
<p>Selon l’ISTAT, le taux de chômage en Italie a atteint 11.7 % au 1er trimestre 2013. Les jeunes demeurent les plus touchés avec un taux à 36.9 %.</p>
<p>Le taux d’emploi est particulièrement bas chez les femmes et les séniors (de 55 à 64 ans).</p>
<p>L’Italie connaît les disparités régionales plus importantes. Les niveaux d’emploi et d’activité stagnent dans le sud, particulièrement chez les femmes et les travailleurs âgés, et s’accompagnent de taux plus élevés de travail non déclaré.</p>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/emploi-stage/#sommaire_1">Secteurs à fort potentiel</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/emploi-stage/#sommaire_2">Secteurs à faible potentiel</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/emploi-stage/#sommaire_3">Professions règlementées</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/emploi-stage/#sommaire_4">Rémunération</a></li></ul>
<p>Six régions du Centre et du Nord (Lombardie, Piémont, Vénétie, Emilie-Romagne, Marches et Toscane) regroupent les principaux foyers de croissance industrielle du pays. Quatre sont très actives dans les services marchands (Lombardie pour les services financiers, Toscane et Latium pour le tourisme, Ligurie pour les activités portuaires).</p>
<h3 class="spip"><a id="sommaire_1"></a>Secteurs à fort potentiel</h3>
<p>Les métiers de bouche (pâtissiers, boulangers), l’enseignement, l’informatique, le commerce et le secrétariat sont les principaux secteurs d’emploi. Les ingénieurs, techniciens, contrôleurs de gestion, comptables, assistants à la clientèle, opérateurs call-center (en français), professionnels du tourisme et de la grande distribution sont recherchés. La maîtrise de l’italien s’avère indispensable dans la plupart de ces professions.</p>
<h3 class="spip"><a id="sommaire_2"></a>Secteurs à faible potentiel</h3>
<p>Les secteurs de la culture, du tourisme, de l’art, le journalisme et les formations trop généralistes (lettres, droit, communication si l’italien n’est pas parfait) offrent assez peu de débouchés. Il faut offrir des compétences bien précises et une très bonne connaissance de la langue.</p>
<h3 class="spip"><a id="sommaire_3"></a>Professions règlementées</h3>
<p>Les citoyens de l’Union européenne exerçant une profession libérale dans leur pays doivent s’adresser aux ordres professionnels italiens pour connaître les modalités leur permettant d’accéder à leur profession en Italie. Pour certaines professions réglementées, une reconnaissance professionnelle est nécessaire préalablement à l’exercice de la profession en Italie. La reconnaissance professionnelle consiste à reconnaître un diplôme délivré dans un état de l’Union européenne, dans le but de permettre à son titulaire d’exercer sa profession dans un autre état membre.</p>
<p><strong>Quelques exemples des professions réglementées en Italie </strong> :</p>
<ul class="spip">
<li>Secteur juridique : avocat, greffier…</li>
<li>Secteur paramédical : orthopédiste ; diététicien ; psychologue ; opticien ; podologue ; assistant d’hygiène dentaire.</li>
<li>Secteur technique : ingénieur ; biologiste ; chimiste ; géologue ; conseiller commercial ; agent de change.</li>
<li>Secteur socio-culturel : enseignant ; assistant social.</li></ul>
<p>La liste de ces professions est consultable sur le site Internet du <a href="http://www.cimea.it/" class="spip_out" rel="external">centre d’information sur la mobilité et les équivalences académiques</a> (CIMEA), qui est le bureau en Italie du réseau européen <a href="http://www.ciep.fr/enic-naricfr/" class="spip_out" rel="external">ENIC-NARIC</a> d’informations sur la reconnaissance des diplômes et la législation relative à l’enseignement supérieur :</p>
<p>Pour en savoir plus sur la reconnaissance des diplômes français en Italie, consultez le <a href="http://www.ambafrance-it.org/spip.php?article1018" class="spip_out" rel="external">site de l’ambassade de France en Italie</a>.</p>
<h3 class="spip"><a id="sommaire_4"></a>Rémunération</h3>
<p>L’article 36 de la Constitution italienne dispose que tout travailleur a droit à une rémunération proportionnée à la quantité et à la qualité de son travail. La loi ne fixe pas de salaire minimum garanti à tous les travailleurs, mais la pratique veut que la fixation du revenu économique minimum se base sur des conventions collectives de travail nationales (CCNL), qui s’appliquent dès lors également aux travailleurs qui ne sont membres d’aucun syndicat signataire. La somme de tous les éléments qui composent le salaire correspond à la rémunération brute, de laquelle sont déduites les cotisations à la charge du travailleur. Les cotisations sociales sont obligatoires, conformément à la loi, et sont calculées en pourcentage sur la rémunération : une partie est à la charge de l’entreprise et l’autre est assumée par le travailleur. La rémunération se compose de tout ce que le travailleur perçoit, en argent ou en nature, avant déduction des diverses retenues. Toutefois, certains éléments sont exclus de la rémunération et ne sont pas soumis aux contributions, par exemple : les allocations familiales, les sommes dépensées pour les bourses d’étude, les crèches et les colonies en faveur des membres de la famille des salariés. Les cotisations doivent être versées et déclarées chaque mois par l’entreprise à l’Istituto Nazionale della Previdenza Sociale (INPS).</p>
<p>Après déduction des cotisations, il reste la rémunération imposable, de laquelle sont déduites les retenues fiscales. Le résultat final est la rémunération nette. La rémunération se compose de différents éléments, certains fixes et d’autres variables.</p>
<h4 class="spip">Les éléments fixes </h4>
<ul class="spip">
<li>la <i>paga base</i> ou <i>minimo tabellare</i> (rémunération de base ou minimum barémique), qui tient lieu de rémunération du profil professionnel. Toute qualification différente est couverte par un niveau déterminé, auquel correspond un minimum barémique ;</li>
<li>l<i>’indennità di contingenza </i>(indemnité de vie chère), qui représentait le mécanisme d’adaptation automatique à la hausse de l’inflation. Depuis le protocole d’accord du 23/07/1993 entre le gouvernement et les partenaires sociaux, cette indemnité est intégrée dans la rémunération, laquelle est négociée tous les deux ans ;</li>
<li>l’<i>E.D.R. </i>(élément distinct de la rémunération) ;</li>
<li>les <i>scatti di anzianità </i>(primes d’ancienneté), qui constituent la partie de la rémunération liée à la durée de travail du travailleur dans une même entreprise, dans la même catégorie professionnelle ;</li>
<li>les <i>superminimi</i>, qui englobent les compensations dérivant de pourparlers au niveau de l’entreprise ou d’accords individuels et liées à la capacité professionnelle du travailleur.</li></ul>
<h4 class="spip">Les éléments variables </h4>
<ul class="spip">
<li>les majorations en cas de travail extraordinaire, la nuit ou les jours fériés ;</li>
<li>les indemnités légales, comme la non-utilisation des vacances ;</li>
<li>les indemnités contractuelles, telles que les primes de production/de résultat, les indemnités de table, de traçabilité, de travail désagréable, de transfert, de caisse ;</li>
<li>les pourboires ;</li>
<li>les mensualités supplémentaires (treizième et quatorzième mois).</li></ul>
<p>Le paiement du salaire a obligatoirement lieu par la remise du bordereau de salaire. La fiche de salaire doit indiquer l’identité et la qualification professionnelle du travailleur, la période à laquelle se réfère la rémunération, les allocations familiales et tous les autres éléments qui composent la rémunération ainsi que toutes les retenues. Le lieu de paiement est généralement le lieu où est effectuée la prestation et l’employeur doit régler le montant du salaire par chèque ou virement sur un compte bancaire ou postal.</p>
<p>(Source : <a href="http://ec.europa.eu/eures/" class="spip_out" rel="external">EURES</a> rubrique Vivre et travailler).</p>
<p><strong>Les revenus</strong></p>
<p>La rémunération doit être déterminée par les conventions collectives ou par les accords conclus entre les parties. Les conventions collectives sont des accords conclus entre les syndicats des travailleurs et ceux des employeurs, dans le but de réglementer les aspects économiques (salaire, indemnités et mensualités supplémentaires, primes, etc.) et légaux (horaire, congés, encadrement, sécurité, hygiène du travail, etc.) de la relation de travail.</p>
<p>Les éléments de la rémunération peuvent être répartis comme suit : les éléments permanents, payés systématiquement à chaque période de paye (utiles pour rémunérer les périodes lors desquelles aucune prestation de travail n’a été effectuée), et les éléments occasionnels ou non permanents, payés seulement une fois ou à titre exceptionnel.</p>
<p>Pour les employés, les conventions collectives établissent une rémunération mensuelle fixe. Les ouvriers sont généralement rémunérés à l’heure travaillée.</p>
<p>La Constitution et des articles de loi disposent qu’à tâches égales, les travailleuses et les travailleurs reçoivent la même rémunération.</p>
<p><strong>En Italie, le revenu mensuel moyen s’élève à 1 410 € (nets), soit 47 € par jour.</strong></p>
<p>Le revenu annuel moyen brut des Italiens ne dépassent pas 22 000 € (21 933 €) tandis que 78% des contribuables gagnent moins de 28 000 € par an. La <strong>Lombardie</strong> enregistre le <strong>revenu moyen le plus élevé</strong>, avec <strong>23 930 €</strong>. Le <strong>revenu moyen le plus bas</strong> est celui des habitants des <strong>Pouilles : 16 763 €</strong>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-italie-emploi-stage-article-stages.md" title="Stages">Stages</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-italie-emploi-stage-article-entretien-d-embauche-109940.md" title="Entretien d’embauche">Entretien d’embauche</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-italie-emploi-stage-article-lettre-de-motivation-109939.md" title="Lettre de motivation">Lettre de motivation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-italie-emploi-stage-article-curriculum-vitae.md" title="Curriculum vitae">Curriculum vitae</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-italie-emploi-stage-article-recherche-d-emploi.md" title="Recherche d’emploi">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-italie-emploi-stage-article-reglementation-du-travail.md" title="Réglementation du travail">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-italie-emploi-stage-article-marche-du-travail.md" title="Marché du travail">Marché du travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/emploi-stage/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
