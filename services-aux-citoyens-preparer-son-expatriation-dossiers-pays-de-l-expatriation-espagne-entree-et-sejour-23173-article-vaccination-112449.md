# Vaccination

<p>Comme pour tous les pays d’Europe de l’Ouest, il n’y a pas de risque sanitaire particulier en Espagne. Aucune vaccination n’est exigée à l’entrée du pays.</p>
<p>Pour des raisons médicales, il peut être recommandé :</p>
<ul class="spip">
<li>Diphtérie, tétanos, poliomyélite (mise à jour),</li>
<li>Hépatite B (pour de longs séjours ou séjours à risques).</li></ul>
<p>NB : les enfants doivent avoir à jour toutes les vaccinations incluses dans le calendrier vaccinal français, en particulier pour les longs séjours : BCG dès le premier mois, rougeole-oreillons-rubéole dès l’âge de neuf mois.</p>
<p>Pour en savoir plus :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  notre article thématique sur la <a href="services-aux-citoyens-preparer-son-expatriation-sante.md" class="spip_in">Santé</a>.</p>
<p><i>Mise à jour : avril 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/entree-et-sejour-23173/article/vaccination-112449). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
