# Royaume-Uni

<p>Le 31 décembre 2014, 126 804 Français étaient inscrits au Registre des français établis hors de France (5636 à Edimbourg et 121 168 à Londres).</p>
<p>La communauté française au Royaume-Uni est cependant évaluée à plus de 300 000 personnes en prenant en compte les personnes non inscrites.</p>
<p>La communauté française résidant au Royaume-Uni est jeune, la tranche des 25-40 ans étant particulièrement bien représentée (un tiers des inscrits aux Registres des consulats généraux à Londres et Edimbourg). Elle est composée de 55% de femmes et 45% d’hommes.</p>
<p>17,6% des inscrits au Registre sont binationaux (dont près de 50% de britanniques).</p>
<p>63% des Français inscrits au Registre de la circonscription de Londres résident dans le Grand Londres. Le reste de la communauté française est concentré dans les principaux centres urbains du pays : Birmingham et Coventry pour les Midlands, Leeds et Manchester pour le nord et, surtout, les villes universitaires (Oxford et Cambridge), ainsi que dans les régions les plus dynamiques du sud-est de l’Angleterre (le Surrey, l’Essex, le Kent et l’Hampshire).</p>
<p>La durée moyenne de séjour au Royaume-Uni est de 5,7 ans (calculée sur la durée d’inscription au Registre du Consulat général de Londres).</p>
<p>De très nombreuses entreprises françaises sont implantées dans tous les secteurs d’activité sur l’ensemble du territoire.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/dossiers-pays/royaume-uni/" class="spip_in">Une description du Royaume-Uni, de sa situation politique et économique</a> ;</li>
<li><a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/royaume-uni/" class="spip_in">Des informations actualisées sur les <strong>conditions locales de sécurité</strong> au Royaume-Uni</a> ;</li>
<li><a href="http://www.ambafrance-uk.org/" class="spip_out" rel="external">Ambassade de France au Royaume-Uni</a>.</li></ul>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-royaume-uni-entree-et-sejour-22762.md">Entrée et séjour</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-royaume-uni-entree-et-sejour-22762-article-passeport-visa-permis-de-travail-109690.md">Passeport, visa, permis de travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-royaume-uni-entree-et-sejour-22762-article-demenagement-109691.md">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-royaume-uni-entree-et-sejour-22762-article-vaccination.md">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-royaume-uni-entree-et-sejour-22762-article-animaux-domestiques-109693.md">Animaux domestiques</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-royaume-uni-emploi-stage.md">Emploi, stage</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-royaume-uni-emploi-stage-article-marche-du-travail-109694.md">Marché du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-royaume-uni-emploi-stage-article-reglementation-du-travail-109695.md">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-royaume-uni-emploi-stage-article-recherche-d-emploi.md">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-royaume-uni-emploi-stage-article-curriculum-vitae.md">Curriculum vitae</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-royaume-uni-emploi-stage-article-lettre-de-motivation.md">Lettre de motivation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-royaume-uni-emploi-stage-article-entretien-d-embauche.md">Entretien d’embauche</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-royaume-uni-emploi-stage-article-stages.md">Stages</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-royaume-uni-protection-sociale-22764.md">Protection sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-royaume-uni-protection-sociale-22764-article-regime-local-de-securite-sociale-109701.md">Régime local de sécurité sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-royaume-uni-protection-sociale-22764-article-convention-de-securite-sociale.md">Convention de sécurité sociale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-royaume-uni-fiscalite-22765.md">Fiscalité</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-royaume-uni-fiscalite-22765-article-fiscalite-du-pays-109703.md">Fiscalité du pays</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-royaume-uni-fiscalite-22765-article-convention-fiscale-109704.md">Convention fiscale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-royaume-uni-vie-pratique.md">Vie pratique</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-royaume-uni-vie-pratique-article-logement.md">Logement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-royaume-uni-vie-pratique-article-sante.md">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-royaume-uni-vie-pratique-article-scolarisation-109707.md">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-royaume-uni-vie-pratique-article-cout-de-la-vie.md">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-royaume-uni-vie-pratique-article-transports-109709.md">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-royaume-uni-vie-pratique-article-communications.md">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-royaume-uni-vie-pratique-article-loisirs-et-culture.md">Loisirs et culture</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-royaume-uni-presence-francaise.md">Présence française</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-royaume-uni-presence-francaise-article-presence-francaise.md">Présence française</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
