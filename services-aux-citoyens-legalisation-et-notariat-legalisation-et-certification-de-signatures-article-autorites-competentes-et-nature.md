# Autorités compétentes et nature des documents

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/legalisation-et-notariat/legalisation-et-certification-de-signatures/article/autorites-competentes-et-nature#sommaire_1">En France</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/legalisation-et-notariat/legalisation-et-certification-de-signatures/article/autorites-competentes-et-nature#sommaire_2">A l’étranger</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>En France</h3>
<p>Le Bureau des légalisations du ministère des Affaires étrangères (57 bd. des Invalides 75007 PARIS) est compétent pour légaliser les documents français suivants :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Les actes publics :
<br>— les expéditions des décisions des juridictions judiciaires ou administratives, les actes émanant de ces juridictions et des ministères publics institués auprès d’elles ;
<br>— les actes établis par les greffiers ;
<br>— les actes établis par des huissiers de justice ;
<br>— les expéditions des actes de l’état civil établis par les officiers de l’état civil ;
<br>— les actes établis par les autorités administratives ;
<br>— les actes notariés.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Les actes originellement sous seing privé que la Convention de La Haye fait entrer dans la catégorie des actes publics dès lors qu’ils ont servi de support à l’exécution d’une formalité administrative :
<br>— les certificats médicaux visés par l’ordre national des médecins ;
<br>— les actes à caractère commercial et douanier, visés par une chambre de commerce et d’industrie.</p>
<p><strong>Attention : </strong></p>
<p>Le Bureau des légalisations n’est pas compétent pour légaliser un document émanant d’un État étranger.</p>
<p>Si un acte public étranger doit produire des effets en France, vous devez vous adresser à l’autorité consulaire de l’État dont émane l’acte.</p>
<p>S’il doit être présenté dans un État tiers, le document devra être légalisé par l’autorité consulaire de l’État dont émane l’acte puis être surlégalisé par l’autorité consulaire de l’État dans lequel l’acte doit produire ses effets.</p>
<h3 class="spip"><a id="sommaire_2"></a>A l’étranger</h3>
<p>Les ambassadeurs et les consuls sont compétents pour légaliser :</p>
<ul class="spip">
<li>les actes publics d’une autorité française destinés à être produits à l’étranger ;</li>
<li>les actes publics de l’État étranger de leur résidence destinés à être produits en France ou devant un autre ambassadeur ou consul français ;</li>
<li>les actes publics établis par les agents diplomatiques et consulaires étrangers dans l’État de leur résidence destinés à être produits soit en France soit devant un autre ambassadeur ou consul français ;</li>
<li>sous certaines conditions, les actes sous-seing privé au sens de l’article 5 du titre II du décret sur la légalisation.</li></ul>
<p><i>Mise à jour :  juillet 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/legalisation-et-notariat/legalisation-et-certification-de-signatures/article/autorites-competentes-et-nature). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
