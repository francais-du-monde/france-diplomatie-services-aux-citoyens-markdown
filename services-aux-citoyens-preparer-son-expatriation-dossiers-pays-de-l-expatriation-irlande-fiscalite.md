# Fiscalité

<h2 class="rub22893">Fiscalité du pays</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/fiscalite/#sommaire_1">Impôt sur le revenu des personnes physiques (<i>Income Tax</i>) </a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/fiscalite/#sommaire_2">Taxes</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/fiscalite/#sommaire_3">Année fiscale</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/fiscalite/#sommaire_4">Barème de l’impôt</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/fiscalite/#sommaire_5">Quitus fiscal</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Impôt sur le revenu des personnes physiques (<i>Income Tax</i>) </h3>
<h4 class="spip">Assujettissement à l’impôt</h4>
<p>Pour apprécier dans quelle mesure une personne physique est assujettie à l’impôt en Irlande, il convient de considérer à la fois la notion de résidence et de domicile.</p>
<p>Ainsi, toute personne est considérée résidente en Irlande, durant un exercice fiscal donné, si elle passe 183 jours ou plus en Irlande au cours de cet exercice ou si elle passe un total cumulé de 280 jours en Irlande durant cet exercice et l’exercice fiscal précédent.</p>
<p>Compte tenu de cette définition, un ressortissant français peut se retrouver dans l’un des trois cas suivants :</p>
<ul class="spip">
<li>s’il est résident en Irlande et domicilié dans ce pays, il est soumis à l’impôt sur le revenu en Irlande à raison de l’ensemble de ses revenus, qu’ils soient de source irlandaise ou étrangère</li>
<li>s’il est résident en Irlande mais non domicilié dans ce pays, il est assujetti à l’impôt sur le revenu en Irlande au titre de ses revenus ayant leur source en Irlande, ainsi que de ses revenus de source étrangère transférés en Irlande <i>(remitted revenue)</i></li>
<li>s’il est non résident en Irlande, il reste soumis à l’impôt sur le revenu en Irlande à raison de ses seuls revenus de source irlandaise.</li></ul>
<h4 class="spip">Modalités de paiement de l’impôt</h4>
<p>Pour les personnes ne percevant que des revenus salariés, un système de retenue à la source est appliqué à chaque salaire. Les salariés payent leurs impôts dans le cadre du régime PAYE (<i>"Pay As You Earn"</i> :  acquittez votre impôt directement sur votre salaire). Ce système dispense les personnes qui y sont assujetties de remplir une déclaration de revenus.</p>
<p>Dans le cas d’une activité non salariée ou lorsque d’autres types de revenus sont perçus, une déclaration fiscale doit être souscrite auprès de l’Administration fiscale et des douanes (<i>Revenue Commissioners)</i>, avant le 31 octobre suivant l’année d’imposition.</p>
<p>Une activité non salariée donne lieu au paiement provisionnel, au 31 octobre de chaque année, de 90% de l’impôt estimé sur l’année en cours ou de 100% de l’impôt de l’année précédente. Le solde doit être payé au plus tard le 31 octobre de l’année suivante.</p>
<h3 class="spip"><a id="sommaire_2"></a>Taxes</h3>
<h4 class="spip">Taxe sur la valeur ajoutée (<i>Value added tax</i>)</h4>
<p>C’est une taxe sur les dépenses de consommation. Elle est ajoutée au prix de la plupart des biens et services. Les taux de TVA sont plus élevés qu’en France : le taux normal s’élève à 23% depuis janvier 2012.</p>
<h4 class="spip">Taxe d’immatriculation des véhicules (<i>Vehicle registration tax</i>)</h4>
<p>Depuis le 1er juillet 2008, le montant de la VRT des voitures correspond au niveau d’émission CO2.</p>
<h4 class="spip">Taxe annuelle sur les véhicules à moteur (<i>Motor tax</i>)</h4>
<p>Cette taxe est applicable sur les véhicules individuels en fonction de la cylindrée, mais aussi sur les véhicules industriels, en fonction de la capacité de charge du véhicule. Elle est reversée aux collectivités locales et est notamment destinée au financement des routes autres que les nationales.</p>
<p>Pour une voiture enregistrée après 2008, la taxe est proportionnelle aux émissions de CO2.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.revenue.ie/fr/index.html" class="spip_out" rel="external">Guides en français édités par l’Administration fiscale en Irlande</a></li>
<li>Notre rubrique thématique <a href="services-aux-citoyens-preparer-son-expatriation-fiscalite.md" class="spip_in">Fiscalité</a></li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Année fiscale</h3>
<p>L’année fiscale en Irlande débute le 1er janvier et se termine le 31 décembre.</p>
<h3 class="spip"><a id="sommaire_4"></a>Barème de l’impôt</h3>
<h4 class="spip">Impôt sur le revenu des personnes physiques (<i>Income Tax</i>)</h4>
<p>Les personnes de plus de 65 ans dont le revenu est inférieur au seuil d’imposition sont exonérées de l’impôt sur le revenu. Ce seuil est déterminé en fonction de la situation familiale :</p>
<table class="spip">
<thead><tr class="row_first"><th id="id8b6a_c0"> </th><th id="id8b6a_c1">Seuil d’imposition (taux 2011)  </th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="id8b6a_c0">Célibataire ou veuf(ve) âgé de 65 ans ou plus</td>
<td headers="id8b6a_c1">18 000 €</td></tr>
<tr class="row_even even">
<td headers="id8b6a_c0">Couple marié âgé de 65 ans ou plus</td>
<td headers="id8b6a_c1">36 000 €</td></tr>
<tr class="row_odd odd">
<td headers="id8b6a_c0">Majoration par enfant à charge jusqu’au 2 ème enfant</td>
<td headers="id8b6a_c1">575 €</td></tr>
<tr class="row_even even">
<td headers="id8b6a_c0">Majoration par enfant à charge à partir du 3 ème enfant</td>
<td headers="id8b6a_c1">830 €</td></tr>
</tbody>
</table>
<p>Il existe deux tranches d’imposition en fonction du niveau de revenus. Les taux appliqués sont respectivement de 20% pour la première tranche et de 41% pour la portion de revenus au-delà.</p>
<p>En fonction la situation familiale, le plafond de la première tranche est fixé selon le barème suivant :</p>
<p><strong>Taux applicable selon la tranche de revenus</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td></td>
<td>1ère tranche <strong>20%</strong></td></tr>
<tr class="row_even even">
<td>Célibataire ou Veuf(ve)</td>
<td>32 800 €</td></tr>
<tr class="row_odd odd">
<td>Parent seul</td>
<td>36 800 €</td></tr>
<tr class="row_even even">
<td>Couple marié avec 1 salaire</td>
<td>41 800 €</td></tr>
<tr class="row_odd odd">
<td>Couple marié avec 2 salaires</td>
<td>65 600 €</td></tr>
</tbody>
</table>
<p>La loi fiscale irlandaise prévoit de nombreux cas d’abattement pouvant prendre la forme de déductions d’impôt ou de dégrèvements (situation de famille, personnes à charge, invalidité, abattements spécifiques au régime de retenue à la source, aux primes d’assurance médicale et aux cotisations de retraite, logement, etc.). Il est donc important de se référer, pour chaque année d’imposition, aux notices disponibles auprès des autorités fiscales afin de déterminer les abattements applicables à chaque situation personnelle.</p>
<h4 class="spip">Taxe sur la valeur ajoutée (<i>Value-Added Tax</i>)</h4>
<p>Toute personne physique ou morale qui assure la fourniture de biens ou de services doit être enregistrée à la TVA irlandaise si son chiffre d’affaires annuel est supérieur à 37 500 euros (pour les services) ou 75 000 euros (pour les biens).</p>
<p>Certains services sont exonérés de la TVA (écoles, universités, hôpitaux, etc.).</p>
<p>Selon les produits ou services, le taux de TVA est fixé à 0%, 4,8%, 5.2%, 9%, 13,5% ou 23%.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.revenue.ie/en/tax/vat/rates/current-historic-rates-vat.html" class="spip_out" rel="external">Article dédié à la TVA sur le site de l’Administration fiscale et des douanes irlandaise</a></p>
<h3 class="spip"><a id="sommaire_5"></a>Quitus fiscal</h3>
<p>Un quitus fiscal est exigé avant de quitter le pays.</p>
<p>Il est possible pour un expatrié français de solder son compte en fin de séjour.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p><a href="http://www.revenue.ie/" class="spip_out" rel="external">Revenue Commissioners</a> <br class="manualbr">1st floor, CRIO<br class="manualbr">Cathedral Street<br class="manualbr">Dublin 1 - DX 192<br class="manualbr">Tél. : 01 865 50 00 - Lo-Call : 1 890 201 104<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/fiscalite/#captax#mc#revenue.ie#" title="captax..åt..revenue.ie" onclick="location.href=mc_lancerlien('captax','revenue.ie'); return false;" class="spip_mail">Courriel</a><br class="manualbr">Ouverture au public du lundi au vendredi de 8h30 à 16h sauf les jours fériés.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.revenue.ie/" class="spip_out" rel="external">Site de l’Administration fiscale et des douanes Irlandaise</a></p>
<p><i>Mise à jour : décembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-irlande-fiscalite-article-convention-fiscale.md" title="Convention fiscale">Convention fiscale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-irlande-fiscalite-article-fiscalite-du-pays-110436.md" title="Fiscalité du pays">Fiscalité du pays</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/fiscalite/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
