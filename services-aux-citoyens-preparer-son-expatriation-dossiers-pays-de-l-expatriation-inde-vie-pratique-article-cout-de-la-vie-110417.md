# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/vie-pratique/article/cout-de-la-vie-110417#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/vie-pratique/article/cout-de-la-vie-110417#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/vie-pratique/article/cout-de-la-vie-110417#sommaire_3">Budget</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/vie-pratique/article/cout-de-la-vie-110417#sommaire_4">Alimentation</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/vie-pratique/article/cout-de-la-vie-110417#sommaire_5">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>L’unité monétaire est la roupie indienne (INR).</p>
<p>Au 24 janvier 2014, la roupie indienne vaut 0,0118 euro, c’est-à-dire qu’un euro équivaut à 84,75 INR.</p>
<p>Modalités d’approvisionnement en monnaie locale : il est possible de se procurer des roupies soit par le change sur place, soit par la voie bancaire, notamment par l’ouverture d’un compte en devises ou en INR auprès d’un établissement bancaire (ceci à condition d’être résident).</p>
<p>Les principales banques françaises implantées à New-Delhi et Bombay sont la BNP Paribas, Caylon et la Société Générale mais ces banques n’assurent pas la gestion des comptes de particuliers.</p>
<p>Pour les établissements bancaires étrangers : HSBC, Barclay’s Bank, Citibank, ABN, Standart Chartered, Deutsche bank.</p>
<p>L’usage des chèques est assez répandu mais les paiements se font de plus en plus par carte bancaire de crédit (attention toutefois aux fraudes). Les chèques de voyage sont convertibles auprès de quelques banques.</p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<p>On note un certain progrès vers la convertibilité de la monnaie nationale pour les transactions commerciales, mais le contrôle des changes reste très strict. On ne peut exporter ou transférer des roupies, à l’exception des expatriés qui investissent localement dans le cadre d’une joint-venture franco-indienne. Ces derniers, qui participent au capital de la société, sont autorisés par la Reserve Bank of India, à l’issue d’une longue procédure, à convertir en devises la rémunération de leurs investissements.</p>
<h3 class="spip"><a id="sommaire_3"></a>Budget</h3>
<p>Le candidat à l’expatriation devra se garder de chercher à apprécier sa situation en convertissant tous les chiffres en euros, ou en tentant de déterminer le pouvoir d’achat en France du montant obtenu. Cette appréciation doit se faire avant tout sur le montant des dépenses dans le pays d’expatriation, qu’elles soient exprimées en monnaie locale ou en euros.</p>
<p>Des erreurs d’arrondis peuvent survenir.</p>
<h3 class="spip"><a id="sommaire_4"></a>Alimentation</h3>
<h4 class="spip">Prix moyen d’un repas dans un restaurant</h4>
<p><strong>Bombay</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td></td>
<td>roupies (sans ou avec vin)</td></tr>
<tr class="row_even even">
<td>Restaurant de qualité supérieure</td>
<td>2500 à 4500</td></tr>
<tr class="row_odd odd">
<td>Restaurant de qualité moyenne</td>
<td>700</td></tr>
</tbody>
</table>
<p><strong>Calcutta</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td></td>
<td>roupies</td></tr>
<tr class="row_even even">
<td>Restaurant de qualité supérieure</td>
<td>1500</td></tr>
<tr class="row_odd odd">
<td>Restaurant de qualité moyenne</td>
<td>500</td></tr>
</tbody>
</table>
<p><strong>New Delhi</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td></td>
<td>roupies (sans ou avec vin)</td></tr>
<tr class="row_even even">
<td>Restaurant de qualité supérieure</td>
<td>2500 à 4500</td></tr>
<tr class="row_odd odd">
<td>Restaurant de qualité moyenne</td>
<td>700</td></tr>
</tbody>
</table>
<p><strong>Pondichéry</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td></td>
<td>roupies</td></tr>
<tr class="row_even even">
<td>Restaurant de qualité supérieure</td>
<td>1000</td></tr>
<tr class="row_odd odd">
<td>Restaurant de qualité moyenne</td>
<td>400</td></tr>
</tbody>
</table>
<p><strong>Bangalore</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td></td>
<td>roupies (sans ou avec vin)</td></tr>
<tr class="row_even even">
<td>Restaurant de qualité supérieure</td>
<td>2500 à 4500</td></tr>
<tr class="row_odd odd">
<td>Restaurant de qualité moyenne</td>
<td>700</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_5"></a>Evolution des prix</h3>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/vie-pratique/article/cout-de-la-vie-110417). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
