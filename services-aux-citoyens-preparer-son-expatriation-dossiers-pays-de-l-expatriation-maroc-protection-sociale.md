# Protection sociale

<h2 class="rub22935">Régime local de sécurité sociale</h2>
<h4 class="spip">Régime local de sécurité sociale</h4>
<p>Vous trouverez une présentation détaillée du système de sécurité sociale marocaine sur le site de notre partenaire, le <a href="http://www.cleiss.fr/" class="spip_out" rel="external">CLEISS</a> (Centre des liaisons européennes et internationales de sécurité sociale). En voici la table des matières :</p>
<ul class="spip">
<li><a href="http://www.cleiss.fr/docs/regimes/regime_maroc.html#I" class="spip_out" rel="external">Généralités</a>
<br>— <a href="http://www.cleiss.fr/docs/regimes/regime_maroc.html#IA" class="spip_out" rel="external">Structure</a>
<br>— <a href="http://www.cleiss.fr/docs/regimes/regime_maroc.html#IB" class="spip_out" rel="external">Organisation</a>
<br>— <a href="http://www.cleiss.fr/docs/regimes/regime_maroc.html#IC" class="spip_out" rel="external">Affiliation</a> D. <a href="http://www.cleiss.fr/docs/regimes/regime_maroc.html#ID" class="spip_out" rel="external">Financement</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_maroc.html#II" class="spip_out" rel="external">Prestations</a>
<br>— <a href="http://www.cleiss.fr/docs/regimes/regime_maroc.html#IIA" class="spip_out" rel="external">Maladie - Maternité</a>
<br>— <a href="http://www.cleiss.fr/docs/regimes/regime_maroc.html#IIB" class="spip_out" rel="external">Allocations familiales</a>
<br>— <a href="http://www.cleiss.fr/docs/regimes/regime_maroc.html#IIC" class="spip_out" rel="external">Accident du travail et maladies professionnelles</a>
<br>— <a href="http://www.cleiss.fr/docs/regimes/regime_maroc.html#IID" class="spip_out" rel="external">Décès</a>
<br>— <a href="http://www.cleiss.fr/docs/regimes/regime_maroc.html#IIE" class="spip_out" rel="external">Invalidité</a>
<br>— <a href="http://www.cleiss.fr/docs/regimes/regime_maroc.html#IIF" class="spip_out" rel="external">Vieillesse</a>
<br>— <a href="http://www.cleiss.fr/docs/regimes/regime_maroc.html#IIG" class="spip_out" rel="external">Pension de survivant</a></li></ul>
<p><strong>Présentation du régime marocain</strong></p>
<p>Le régime marocain de protection sociale couvre les salariés du secteur public et ceux du secteur privé. Il assure aux intéressés une protection contre les risques de maladie maternité, invalidité, vieillesse, survie, décès et il sert les prestations familiales.</p>
<p>Depuis novembre 2002, l’assurance accident du travail-maladies professionnelles est devenue obligatoire pour tous. Les entreprises doivent souscrire une police d’assurances pour le compte de leurs employés auprès d’une société d’assurance et de réassurances.</p>
<p>Les salariés du régime public sont gérés par la caisse nationale des organismes de prévoyance sociale (CNOPS) et ceux du régime privé par la caisse nationale de sécurité sociale (CNSS).</p>
<p><strong>Les prestations</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<th id="id3e52_l0">Assurance</th>
<td headers="id3e52_l0"><strong>Nature des prestations</strong> (indemnités journalières, pension, capital…)</td>
<td headers="id3e52_l0"><strong>Base de calcul</strong></td>
<td headers="id3e52_l0"><strong>Montant maximum de l’avantage</strong> (Indiquez la devise : MAD)</td></tr>
<tr class="row_even even">
<th id="id3e52_l1">Maladie</th>
<td headers="id3e52_l1">- indemnités journalières CNSS</td>
<td headers="id3e52_l1">- trois jours de carence ;
<p>- pour un salaire moyen supérieur au plafond légal de 6000 MAD : montant forfaitaire = 2/3 du salaire journalier moyen plafonné à 200 MAD soit 133,33 MAD</p>
</td>
<td headers="id3e52_l1">- montant journalier max = 133,33 MAD</td></tr>
<tr class="row_odd odd">
<th id="id3e52_l2">Matérnite</th>
<td headers="id3e52_l2">- indemnités journalières CNSS</td>
<td headers="id3e52_l2">- justifier 54 jours continus ou discontinus de cotisation durant les 10 mois précédent l’arrêt ;
<p>- durée indemnisation : 14 semaines dont sept semaines au minimum après la date de l’accouchement ;</p>
<p>- montant : 100% du salaire mensuel moyen plafonné déclaré pendant les six derniers mois servi pendant 14 semaines.</p>
<p>Le montant minimum de ces indemnités ne peut, en aucun cas, être inférieur au SMIG.</p>
</td>
<td headers="id3e52_l2">- pour un salaire moyen supérieur au plafond légal de 6000 MAD : montant indemnité journalière maternité max = 200 MAD</td></tr>
<tr class="row_even even">
<th id="id3e52_l3">Accidents du travail – maladies professionnelles</th>
<td headers="id3e52_l3">- indemnités journalières ;
<p>- allocation de produits pharmaceutiques</p>
<p>Assurance privée de l’employeur</p>
</td>
<td headers="id3e52_l3"></td>
<td headers="id3e52_l3"></td></tr>
<tr class="row_odd odd">
<th id="id3e52_l4">Invalidité</th>
<td headers="id3e52_l4">- pension
<p>CNSS</p>
</td>
<td headers="id3e52_l4">- justifier 1080 jours d’assurance dont 108 jours pendant les 12 mois civils qui précèdent le défaut d’incapacité de travail ;
<p>- être rendu incapable d’exercer une activité lucrative quelconque ;</p>
<p>- ne pas avoir atteint l’âge d’admissibilité à la pension de vieillesse ;</p>
<p>- minimum : 50% du salaire mensuel moyen pour l’assuré qui compte 1080 à 3240 jours d’assurance, +1% pour chaque période de 216 jours de cotisation accomplie en + des 3240 jours.</p>
</td>
<td headers="id3e52_l4">- maximum : 70% du salaire moyen</td></tr>
<tr class="row_even even">
<th id="id3e52_l5">Décès – veuvage – survie</th>
<td headers="id3e52_l5">- allocation au décès
<p>- CNSS</p>
</td>
<td headers="id3e52_l5">- 9250 MAD si le décès fait suite à un accident de travail
<p>- montant en fonction des ayants droit : 10 000 à 12 000 MAD</p>
</td>
<td headers="id3e52_l5">- montant max : 12 000 MAD</td></tr>
<tr class="row_odd odd">
<th id="id3e52_l6">Vieillesse</th>
<td headers="id3e52_l6">- CNSS (assurance obligatoire) : pension
<p>- CIMR (assurance volontaire) : capital</p>
</td>
<td headers="id3e52_l6">- avoir atteint 60 ans et justifier 3240 jours d’assurance
<p>- montant de la pension minimale : 1000 MAD par mois</p>
<p>- montant : 50% du salaire mensuel plafonné (à 6000 MAD) si l’assuré totalise au moins 3240 jours de cotisation, + 1% pour chaque période d’assurance de 216 jours accomplie en + des 3240 jours</p>
<p>- CIMR : taux appliqué à la partie du salaire supérieure au plafond du régime de base de la CNSS</p>
<p>- taux CIMR : 3,90 à 15,60% en part patronale, 3 à 12% en part salariale</p>
</td>
<td headers="id3e52_l6">- montant max : 70% du salaire moyen</td></tr>
<tr class="row_even even">
<th id="id3e52_l7">Chômage</th>
<td headers="id3e52_l7">0</td>
<td headers="id3e52_l7">0</td>
<td headers="id3e52_l7">0</td></tr>
<tr class="row_odd odd">
<th id="id3e52_l8">Prestations familiales</th>
<td headers="id3e52_l8">- allocation
<p>CNSS</p>
</td>
<td headers="id3e52_l8">- à la charge exclusive de l’employeur, prélèvement sur la masse salariale brute, sans limite de plafond
<p>- justifier 108 jours continus ou discontinus de cotisations pendant six mois</p>
<p>- justifier un salaire mensuel supérieur ou égal à 60% du SMIG</p>
<p>- enfant de moins de 21 ans et scolarisés</p>
<p>- allocation mensuelle de 200 MAD par enfant pour les trois premiers enfants, 36 MAD pour les trois suivants</p>
</td>
<td headers="id3e52_l8">- six enfants max :
<p>allocation mensuelle de 200 MAD par enfant pour les trois premiers enfants, 36 MAD pour les trois suivants</p>
</td></tr>
</tbody>
</table>
<p><i>Mise à jour : janvier 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-maroc-protection-sociale-article-convention-de-securite-sociale.md" title="Convention de sécurité sociale">Convention de sécurité sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-maroc-protection-sociale-article-regime-local-de-securite-sociale.md" title="Régime local de sécurité sociale">Régime local de sécurité sociale</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/protection-sociale/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
