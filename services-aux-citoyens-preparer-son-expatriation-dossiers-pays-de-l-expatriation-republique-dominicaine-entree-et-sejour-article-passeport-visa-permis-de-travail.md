# Passeport, visa, permis de travail

<p>Quel que soit le motif de votre séjour (autre que touristique) il est recommandé de prendre l’attache de la section consulaire de l’ambassade de la République Dominicaine à Paris qui vous informera sur la règlementation à respecter en matière d’entrée et de séjour en République Dominicaine.</p>
<p>Une fois sur place ce sont les autorités locales (<i>Migracion</i>) qui prennent le relais. <strong>En aucun cas, vous n’êtes autorisé à résider et travailler en République dominicaine sans les autorisations adéquates.</strong></p>
<p>La section consulaire de l’Ambassade de France en République Dominicaine n’est pas l’organisme compétent pour répondre à vos demandes d’information concernant votre séjour en République Dominicaine.</p>
<h4 class="spip">Visa de tourisme</h4>
<p>Conformément à l’article 87 section XIV de la loi migratoire dominicaine, tout étranger arrivant en République Dominicaine doit être en possession d’un passeport ayant une <strong>durée de validité au moins égale à six mois</strong> après la date d’arrivée.</p>
<p>Les citoyens des pays d’Europe peuvent voyager ou résider dans le pays pour une durée n’excédant pas 90 jours, moyennant l’acquisition d’une carte de tourisme d’une valeur de 10 USD. On peut se procurer celle-ci dès l’arrivée dans l’aéroport de destination, et le cas échéant auprès de l’agence touristique en charge du voyage.</p>
<h4 class="spip">Visa de long séjour et résidence</h4>
<p>Pour les ressortissants français souhaitant s’établir de façon temporaire ou permanente, un visa de long séjour doit être obtenu préalablement à l’arrivée, auprès du consulat de la République Dominicaine en France. Le visa sera délivré conformément au motif du séjour envisagé.</p>
<p>Après leur arrivée en République Dominicaine, les Français titulaires d’un tel visa devront procéder aux démarches d’obtention de la carte de résidence.</p>
<p>Faire appel aux services d’un avocat pour satisfaire à ces démarches peut s’avérer nécessaire.</p>
<p>Le site Internet de la Dirección General de Migración dominicaine présente les différentes catégories de visa, et les pièces requises pour la constitution du dossier de régularisation après votre arrivée dans le pays.</p>
<p><strong>NB</strong> : Quelle que soit sa nationalité, tout enfant de moins de 13 ans quittant la République Dominicaine, seul ou non accompagné de ses parents, doit <strong>impérativement</strong> être muni d’une autorisation spéciale de sortie du territoire signée du père et/ou de la mère, légalisée.</p>
<p>La légalisation peut être effectuée soit au préalable par un consulat dominicain en France, soit par le service des migrations à Saint-Domingue. Sans ce document, l’enfant ne pourra quitter le pays.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/entree-et-sejour/article/passeport-visa-permis-de-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
