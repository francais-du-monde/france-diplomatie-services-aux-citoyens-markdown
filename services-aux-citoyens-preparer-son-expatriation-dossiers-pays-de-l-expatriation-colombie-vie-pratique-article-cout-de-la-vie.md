# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/vie-pratique/article/cout-de-la-vie#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/vie-pratique/article/cout-de-la-vie#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/vie-pratique/article/cout-de-la-vie#sommaire_3">Budget</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/vie-pratique/article/cout-de-la-vie#sommaire_4">Évolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>L’unité monétaire est le peso colombien, son code ISO est COP.</p>
<p>Un peso comprend 100 centavos de pesos. Les différentes pièces sont de 50, 100, 200 et 500 pesos. Les billets sont au nombre de six (1000, 2000, 5000, 10000 et 50000 pesos).</p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<p>La carte de crédit est acceptée dans les plus grands établissements (hôtels, restaurants, loueurs de voitures etc.), et se généralise progressivement ailleurs. Les distributeurs de billets (cajeros automáticos) acceptent les cartes Visa, MasterCard, mais les cartes American Express et Diners Club sont moins souvent acceptées. L’utilisation des chèques de voyage est loin d’être courante, même dans les hôtels (se renseigner avant).</p>
<h3 class="spip"><a id="sommaire_3"></a>Budget</h3>
<p>Le coût de la vie varie selon les départements, les villes et les quartiers de Bogota. Ces derniers sont divisés en « strates » qui correspondent au niveau de confort, de sécurité et à la catégorie sociale. Il existe 6 strates, les quartiers les plus pauvres appartenant aux 1 et 2, les plus aisés aux 5 et 6. Le coût de la vie dans les quartiers aisés appartenant aux « strates » 5 et 6 est élevé. La majorité des expatriés français vivent dans ces zones. A Bogota il s’agit du nord de la ville.</p>
<p>Le candidat à l’expatriation devra se garder de chercher à apprécier sa situation en convertissant tous les chiffres en euros, ou en tentant de déterminer le pouvoir d’achat en France du montant obtenu. Cette appréciation doit se faire avant tout sur le montant des dépenses dans le pays d’expatriation, qu’elles soient exprimées en monnaie locale ou en euros, en regard du salaire qui est susceptible d’y être perçu.</p>
<p>Pour connaître les prix des denrées alimentaires, consultez les sites internet des supermarchés en ligne :</p>
<ul class="spip">
<li><a href="http://www.exito.com" class="spip_url spip_out" rel="external">http://www.exito.com</a></li>
<li><a href="http://www.carulla.com" class="spip_url spip_out" rel="external">http://www.carulla.com</a></li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Évolution des prix</h3>
<p>La politique de ciblage de l’inflation menée par la banque centrale a permis de ramener l’inflation de 25% dans les années 90 à un niveau inférieur à 4% au cours des dernières années.</p>
<p>L’indice des prix à la consommation (IPCH) en 2010, tous postes de dépenses confondues, est de 125 (base=100).</p>
<p>Sources : <a href="http://donnees.banquemondiale.org/pays/colombie" class="spip_out" rel="external">Banque Mondiale</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/vie-pratique/article/cout-de-la-vie). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
