# Présence française

<h4 class="spip">Ambassade et consulat de France</h4>
<p>Consulter notre annuaire des <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/" class="spip_in">représentations françaises à l’étranger</a>.</p>
<h4 class="spip">Autorités françaises dans le pays</h4>
<h5 class="spip">Réseau français de coopération et d’action culturelle</h5>
<p>Les coordonnées des services du réseau de coopération et d’action culturelle français en Allemagne ainsi que la description de l’activité de ces services se trouvent dans la rubrique culture du site internet de l’ambassade ou sur le site internet du réseau culturel : <a href="http://www.latitudefrance.org/" class="spip_out" rel="external">Latitude France</a>.</p>
<h5 class="spip">Economie : réseau mondial</h5>
<p>Business France, l’Agence française pour le développement international des entreprises, <a href="http://export.businessfrance.fr/allemagne/export-allemagne-avec-notre-bureau.html" class="spip_out" rel="external">est présente en Allemagne</a>. Sa mission est d’accompagner les entreprises françaises dans leur développement à l’international.</p>
<p>Les services économiques sont également présents. Ils sont une émanation de la Direction générale du Trésor et ont pour missions principales l’analyse macroéconomique et l’animation des relations économiques bilatérales entre la France et les pays où ils sont implantés.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.ambafrance-de.org/Service-economique-regional" class="spip_out" rel="external">Site internet du service économique français en Allemagne</a></p>
<h5 class="spip">Vos élus à l’Assemblée des Français de l’étranger</h5>
<p>Pour toute information sur l’Assemblée des Français de l’étranger et pour connaître les conseillers et les Sénateurs qui représentent les Français établis hors de France dans votre circonscription, vous pouvez consulter les sites Internet suivants :</p>
<ul class="spip">
<li><a href="http://www.assemblee-afe.fr/" class="spip_out" rel="external">Assemblée des Français de l’étranger</a> (AFE)</li>
<li>Sénat au service des Français de l’étranger : <a href="http://www.senat.fr/expatries/dossiers_pays/allemagne.html" class="spip_out" rel="external">dossier spécifique à l’Allemagne</a> sur le site du Sénat.</li></ul>
<h5 class="spip">Députés des Français de l’étranger</h5>
<p>La révision constitutionnelle du 23 juillet 2008, publiée au Journal officiel du 19 avril 2011, introduit la représentation à l’Assemblée nationale des Français établis hors de France. La création de onze circonscriptions législatives à l’étranger permet aux Français expatriés d’élire leur député à l’Assemblée nationale à partir du scrutin de 2012.</p>
<p>Pour plus d’information : <a href="http://www.votezaletranger.gouv.fr/" class="spip_out" rel="external">Votez à l’étranger</a></p>
<h4 class="spip">Communauté française</h4>
<p>De manière générale, il s’agit d’une communauté jeune (moyenne d’âge inférieure à 40 ans), citadine, bien intégrée, résidant depuis longtemps en Allemagne et comportant une part importante de bi-nationaux (environ 40%).</p>
<h4 class="spip">Associations dans le pays</h4>
<p>Les associations de Français en Allemagne sont nombreuses. Il est conseillé de prendre contact avec l’ambassade de France à Berlin et les consulats généraux de France en Allemagne afin d’obtenir des listes d’associations plus complètes.</p>
<h5 class="spip">Association démocratique des Français à l’étranger - Français du Monde (ADFE - FdM)</h5>
<p><strong>Pour connaître les coordonnées de la section la plus proche, consultez le <a href="http://www.francais-du-monde.org/nous-rejoindre/nos-sections-dans-le-monde/" class="spip_out" rel="external">site internet de l’association</a>.</strong></p>
<h5 class="spip">Union des Français de l’Etranger (UFE)</h5>
<p>Pour connaître les coordonnées de la section la plus proche, consultez le <a href="http://www.ufe.org/fr/representations-par-pays/77_allemagne.html" class="spip_out" rel="external">site internet de l’association</a>.</p>
<h5 class="spip">Fédération internationale des accueils français et francophones à l’Etranger (FIAFE)</h5>
<p>Pour connaître les coordonnées de la section la plus proche, consultez le <a href="http://www.fiafe.org/index.php?module=recherche" class="spip_out" rel="external">site internet de l’association</a>.</p>
<p>Il est possible d’obtenir les coordonnées d’associations franco-allemandes auprès de :</p>
<p><a href="http://www.vdfg.de/" class="spip_out" rel="external">Vereinigung Deutsch-Französischer Gesellschaften in Deutschland und Frankreich</a><br class="manualbr">Platanenweg 4 - 53619 Rheinbreitbach<br class="manualbr">Téléphone : 02224 35 68 6 <br class="manualbr">Télécopie : 02224 - 78 613<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/presence-francaise/#wf.Linckelmann#mc#t-online.de#" title="wf.Linckelmann..åt..t-online.de" onclick="location.href=mc_lancerlien('wf.Linckelmann','t-online.de'); return false;" class="spip_mail">Courriel</a></p>
<p>L’ambassade de France à Berlin et les consulats généraux de France en Allemagne proposent les coordonnées d’associations franco-allemandes sur leurs sites Internet.</p>
<p>Pour en savoir plus :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="services-aux-citoyens-preparer-son-expatriation-en-savoir-plus-article-associations-des-francais-de-l-etranger.md" class="spip_in">Notre article dédié aux associations</a>.</p>
<p><i>Mise à jour : juillet 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-presence-francaise-article-presence-francaise.md" title="Présence française">Présence française</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/presence-francaise/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
