# Argentine

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/argentine/#sommaire_1">Entrée et séjour</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/argentine/#sommaire_2">Santé et protection sociale</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/argentine/#sommaire_3">Fiscalité</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/argentine/#sommaire_4">Emploi, stage</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/argentine/#sommaire_5">Logement</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/argentine/#sommaire_6">Scolarisation</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/argentine/#sommaire_7">Transports</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/argentine/#sommaire_8">Vie pratique</a></li>
<li><a id="so_9" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/argentine/#sommaire_9">Culture et médias français</a></li>
<li><a id="so_10" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/argentine/#sommaire_10">Présence française</a></li></ul>
<ul class="spip">
<li><strong>Monnaie : </strong>peso argentin</li>
<li><strong>Coût de la vie</strong> : <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">indice des prix à la consommation (IPCH)</a></li>
<li><strong>Le décalage horaire : </strong>- 4 heures (hiver) ou 5 heures (été)</li>
<li><strong>Indicatif téléphonique :</strong> +54</li>
<li><strong>Documents de voyage : </strong>passeport en cours de validité durant la totalité de séjour + 6 mois</li>
<li><strong>Visa :</strong> non (pour un séjour inférieur ou égal à 90 jours)</li>
<li><strong>Vaccins exigés à l’entrée dans le pays : </strong><a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/argentine/" class="spip_out">dossier Argentine sur Conseils aux voyageurs</a></li>
<li><strong>Conventions :</strong> icônes pour les conventions signées (protection sociale, fiscalité, programme vacances-travail)</li>
<li><strong>Conditions de sécurité :</strong> <a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/argentine/" class="spip_out">dossier Argentine sur Conseils aux voyageurs</a></li></ul>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Ambassade</strong></td>
<td><a href="http://www.embafrancia-argentina.org/" class="spip_out" rel="external">Buenos Aires</a></td></tr>
<tr class="row_even even">
<td><strong>Consulat général</strong></td>
<td>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.embafrancia-argentina.org/-Consulat-general-a-Buenos-Aires-" class="spip_out" rel="external">Buenos Aires</a> </p></td></tr>
<tr class="row_odd odd">
<td><strong>Nombre de Français inscrits</strong> au registre consulaire</td>
<td><strong>13 456</strong></td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_1"></a>Entrée et séjour</h3>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Documents</strong> de voyages reconnus</td>
<td>Passeport couvrant le séjour + 6 mois.</td></tr>
<tr class="row_even even">
<td><strong>Enregistrement auprès des autorités locales </strong></td>
<td>Enregistrement auprès des autorités locales dès l’arrivée. <br> <br>
<ul class="spip">
<li><a href="http://www.efran.mrecic.gov.ar/fr" class="spip_out" rel="external">Ambassade d’Argentine à Paris</a></li>
<li><a href="http://www.migraciones.gov.ar/" class="spip_out" rel="external">Site de la direction nationale des migrations</a></li></ul></td></tr>
<tr class="row_odd odd">
<td><strong>Déménagement</strong></td>
<td>Pour trouver un professionnel disposant de certifications reconnues (marque NF Service, ISO 9002, etc.), contacter <a href="http://www.csdemenagement.fr/" class="spip_out" rel="external">La Chambre syndicale du déménagement</a> <br class="autobr">
Téléphone : 01 49 88 61 40 <br class="autobr">
Télécopie : 01 49 88 61 46 <br class="autobr">
Courriel : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/argentine/#contact#mc#csdemenagement.fr#" title="contact..åt..csdemenagement.fr" onclick="location.href=mc_lancerlien('contact','csdemenagement.fr'); return false;" class="spip_mail">contact<span class="spancrypt"> [at] </span>csdemenagement.fr</a></td></tr>
<tr class="row_even even">
<td><strong>Formalités douanières</strong></td>
<td><a href="http://aduanaargentina.com/" class="spip_out" rel="external">Douanes argentines</a></td></tr>
<tr class="row_odd odd">
<td><strong>Animaux domestiques</strong></td>
<td>
<ul class="spip">
<li><a href="http://www.senasa.gov.ar/" class="spip_out" rel="external">Site des services vétérinaires argentins (SENASA)</a></li>
<li>Notre article thématique <a href="services-aux-citoyens-preparer-son-expatriation-douanes-article-animaux-domestiques.md" class="spip_in">Animaux domestiques</a></li>
<li>Contacter l’<a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-etrangers-en-france/" class="spip_out">ambassade d’Argentine en France</a></li></ul></td></tr>
<tr class="row_even even">
<td>- Documents valables pour importation</td>
<td>
<ul class="spip">
<li>Carnet de santé (absence de maladies parasitaires, infectieuses, contagieuses) ;</li>
<li>Certificat de vaccin contre la rage, établi par un vétérinaire</li></ul></td></tr>
<tr class="row_odd odd">
<td>- Vaccin obligatoire</td>
<td>Antirabique</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_2"></a>Santé et protection sociale</h3>
<p class="spip_document_94329 spip_documents">
<img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L1000xH191/sante_protection_sociale_cle8d58a9-624a6.jpg" width="1000" height="191" alt=""></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>- Vaccins recommandés</td>
<td><a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/argentine/" class="spip_out">Dossier Argentine sur Conseils aux voyageurs</a></td></tr>
<tr class="row_even even">
<td>- Numéros utiles</td>
<td>
<ul class="spip">
<li>Urgences secours/pompiers : <strong>100</strong></li>
<li>Urgences police : <strong>911</strong></li>
<li>Argentine SAME : <strong>107</strong></li>
<li>Numéro d’urgence d’assistance aux touristes : <strong>0800 999 28 38</strong></li></ul></td></tr>
<tr class="row_odd odd">
<td>- Praticiens francophones</td>
<td>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.embafrancia-argentina.org/Medecins-et-hopitaux,2546" class="spip_out" rel="external">Buenos Aires</a> </p></td></tr>
<tr class="row_even even">
<td>- Régime local de sécurité sociale</td>
<td>
<ul class="spip">
<li>Voir les informations en français sur le <a href="http://www.cleiss.fr/docs/regimes/regime_argentine.html" class="spip_out" rel="external">Régime argentin de sécurité sociale sur le site du CLEISS</a></li>
<li>Informations en espagnol sur le site de <a href="http://www.anses.gov.ar/" class="spip_out" rel="external">Administration nationale de la Sécurité sociale argentine (ANSES)</a></li>
<li><a href="http://www.ilo.org/dyn/natlex/natlex4.byCountry?p_lang=fr" class="spip_out" rel="external">Législation nationale</a> sur le droit du travail, la sécurité sociale et les droits de la personne</li></ul></td></tr>
<tr class="row_odd odd">
<td>- Convention de sécurité sociale</td>
<td><a href="http://www.cleiss.fr/pdf/liste_accords_internationaux.pdf?bcsi_scan_1fe59ba8c561fa18=0bcsi_scan_filename=liste_accords_internationaux.pdf" class="spip_out" rel="external">Liste des accords internationaux</a></td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_3"></a>Fiscalité</h3>
<p class="spip_document_94327 spip_documents">
<img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L1000xH190/fiscalite_cle87785b-6b3a0.jpg" width="1000" height="190" alt=""></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>- Fiscalité du pays</td>
<td>La période de l’année fiscale correspond à l’année civile (1er janvier – 31 décembre). <br class="autobr">
Les personnes physiques et morales qui débutent leur activité (y compris les sociétés de fait) doivent s’inscrire auprès de la <a href="http://www.afip.gov.ar/" class="spip_out" rel="external">direction générale des Impôts (AFIP-DGI)</a> compétente pour leur domicile (particulier ou commercial). Elles obtiendront alors une <i>Clave Única de Identificación Tributaria</i> (CUIT, clé unique d’identification fiscale). <br class="autobr">
En Argentine, l’impôt sur le revenu des <strong>travailleurs salariés</strong> est <strong>prélevé à la source mensuellement</strong>, ils n’ont donc pas besoin d’établir une déclaration de revenus. <br> <br><strong>Pour en savoir plus :</strong><br> <br>
<ul class="spip">
<li><a href="http://www.tresor.economie.gouv.fr/pays/argentine" class="spip_out" rel="external">Service économique français en Argentine</a></li>
<li>Site de l’administration fiscale fédérale argentine : <a href="http://www.afip.gov.ar/futCont/otros/sistemaTributarioArgentino/" class="spip_out" rel="external">présentation du système d’imposition argentin</a> (<i>sistema tributario argentino</i>)</li></ul></td></tr>
<tr class="row_even even">
<td>- Convention fiscale</td>
<td>Consulter la <a href="http://www.impots.gouv.fr/portal/dgi/public/documentation.impot;jsessionid=WS5KW31XDMEUFQFIEIPSFFA?espId=-1pageId=docu_internationalsfid=440" class="spip_out" rel="external">convention fiscale franco-argentine</a>.</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_4"></a>Emploi, stage</h3>
<p class="spip_document_94326 spip_documents">
<img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L1000xH190/emploi_stage_2_cle8d9e56-32814.jpg" width="1000" height="190" alt=""></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>- Administration compétente</td>
<td><a href="http://www.trabajo.gov.ar/" class="spip_out" rel="external">Ministère du Travail</a> (<i>Ministerio de Trabajo, Empleo y Seguridad Social</i>)</td></tr>
<tr class="row_even even">
<td>- Réglementation du travail</td>
<td>
<ul class="spip">
<li><a href="http://www.infoleg.gov.ar/" class="spip_out" rel="external">Informations législatives (site du ministère argentin de l’Economie)</a></li>
<li><a href="http://www.ilo.org/dyn/natlex/natlex4.byCountry?p_lang=fr" class="spip_out" rel="external">Législation nationale</a> sur le droit du travail, la sécurité sociale et les droits de la personne</li></ul></td></tr>
<tr class="row_odd odd">
<td><strong>Recherche d’emploi</strong></td>
<td></td></tr>
<tr class="row_even even">
<td>- Organismes de recherche d’emploi</td>
<td>
<ul class="spip">
<li><a href="http://www.ccifa.com.ar/" class="spip_out" rel="external">Chambre de commerce et d’industrie franco-argentine</a></li>
<li><a href="http://www.trabajo.gov.ar/buscastrabajo/" class="spip_out" rel="external">¿Buscás trabajo ?</a>, guide du demandeur d’emploi sur le site ministère argentin du Travail</li></ul></td></tr>
<tr class="row_odd odd">
<td><strong>Curriculum vitae </strong></td>
<td><a href="http://www.trabajo.gov.ar/buscastrabajo/cv/" class="spip_out" rel="external">Guide de rédaction du CV sur le site du ministère argentin du travail</a>. Pour les filiales françaises, privilégiez la structure habituelle du CV français.</td></tr>
<tr class="row_even even">
<td><strong>Lettre de motivation</strong></td>
<td>La <i>carta de presentación</i> correspond à une lettre de candidature spontanée, tandis que lettre de motivation en réponse à une offre sera appelée <i>carta de acompañamiento en respuesta a un aviso</i>. Elle peut être rédigée sur ordinateur sauf si précisée manuscrite dans l’offre d’emploi et envoyée accompagnée du CV par courriel. <a href="http://www.trabajo.gov.ar/buscastrabajo/cv/#ancla2" class="spip_out" rel="external">Modèle de lettre et conseils sur le site du ministère argentin du travail</a></td></tr>
<tr class="row_odd odd">
<td><strong>Stages et volontariat international</strong></td>
<td>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-emploi-article-stage-117331.md" class="spip_in">Stages</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-emploi-article-volontariat.md" class="spip_in">Volontariat</a></li>
<li><a href="http://www.civiweb.com/FR/index.aspx" class="spip_out" rel="external">Civiweb.com</a></li>
<li><a href="http://www.businessfrance.fr/" class="spip_out" rel="external">Business France.fr</a> - <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/argentine/#buenosaires#mc#businessfrance.fr#" title="buenosaires..åt..businessfrance.fr" onclick="location.href=mc_lancerlien('buenosaires','businessfrance.fr'); return false;" class="spip_mail">buenosaires<span class="spancrypt"> [at] </span>businessfrance.fr</a></li></ul></td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_5"></a>Logement</h3>
<p class="spip_document_94328 spip_documents">
<img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L1000xH191/logement_cle8d8344-0c805.jpg" width="1000" height="191" alt=""></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>- Conditions de location</td>
<td>Contrat de location d’une durée de deux ans minimum. Payé en pesos, le loyer peut connaître une réévaluation de 15% tous les six mois ainsi qu’à l’occasion du renouvellement du bail (forte augmentation de 35 à 50%). Il peut être négocié pour la durée du bail si payé en dollars américains. <br> <br>
<ul class="spip">
<li>Préavis : <strong>3 mois </strong></li>
<li>Caution : <strong>1 ou 2 mois</strong></li>
<li>Frais d’agence : commission équivalente à 5% du total du bail de deux ans ou 1 ou 2 mois de loyer</li>
<li>Etat des lieux : <strong>conseillé </strong></li></ul></td></tr>
<tr class="row_even even">
<td>- Electricité</td>
<td>Tension (220 v), fréquence (50Hz), prises aux normes argentines.</td></tr>
<tr class="row_odd odd">
<td>- Electroménager</td>
<td>Pas difficultés à signaler</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_6"></a>Scolarisation</h3>
<p class="spip_document_94330 spip_documents">
<img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L1000xH191/scolarisation_cle8d878a-b1491.jpg" width="1000" height="191" alt=""></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>- Les établissements scolaires français dans le pays</td>
<td><a href="http://www.aefe.fr/reseau-scolaire-mondial/rechercher-un-etablissement" class="spip_out" rel="external">Agence pour l’enseignement français à l’étranger</a></td></tr>
<tr class="row_even even">
<td>- Enseignement supérieur</td>
<td><a href="http://portal.educacion.gov.ar/mapa/" class="spip_out" rel="external">Renseignements sur le système national sur le site du ministère de l’éducation argentin</a></td></tr>
<tr class="row_odd odd">
<td>Garde d’enfants</td>
<td>Jardins d’enfants / garde d’enfants possible (nourrice).</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_7"></a>Transports</h3>
<p class="spip_document_94331 spip_documents">
<img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L1000xH191/transports_cle0be2ad-eec64.jpg" width="1000" height="191" alt=""></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>- Permis de conduire</td>
<td>Les touristes français ont la possibilité de conduire un véhicule loué ou acheté sur place sur présentation du permis français et du passeport (avec ou sans visa). Nous vous recommandons toutefois de vous procurer un <strong>permis international</strong> auprès de la préfecture de votre lieu de résidence avant votre voyage en Argentine. Si vous êtes un Français résident, vous devez passer le permis de conduire argentin. Vous pouvez contacter la <i>Dirección general adjunta de Gestión de transito y transporte</i> (direction générale du transport) pour plus de précisions ainsi que <a href="http://www.embafrancia-argentina.org/Permis-de-conduire" class="spip_out" rel="external">le site du consulat général</a>.</td></tr>
<tr class="row_even even">
<td>- Code de la route</td>
<td>Port de la ceinture de sécurité : obligatoire à l’avant comme à l’arrière. Usage au volant d’un téléphone interdit ; taux d’alcool dans le sang à partir duquel la conduite est interdite : 0,5 g/l. Taux d’alcool dans l’air expiré à partir duquel la conduite est interdite : 0,25 g/l. <br> <br>Les vitesses maximales autorisées sont les suivantes :  <br> <br>
<ul class="spip">
<li>en agglomération : 60 km/h (avenue) et 40 km/h (rue)</li>
<li>sur route : 100 km/h</li>
<li>sur autoroute : 130 km/h et 100 km/h à Buenos Aires</li></ul></td></tr>
<tr class="row_odd odd">
<td>- Réseau routier</td>
<td>Les routes sont généralement asphaltées et en bon état sur les principaux axes (parfois à péage). L’état des routes secondaires est très variable.</td></tr>
<tr class="row_even even">
<td>- Importation de véhicule</td>
<td>
<ul class="spip">
<li><a href="http://aduanaargentina.com/" class="spip_out" rel="external">Douanes argentines</a></li>
<li><a href="http://www.afip.gob.ar/" class="spip_out" rel="external">Site internet du ministère des Finances argentin</a>, qui consacre <a href="http://www.afip.gob.ar/turismo/#exenciones" class="spip_out" rel="external">un chapitre aux différents droits de douanes</a>.</li></ul></td></tr>
<tr class="row_odd odd">
<td>- Immatriculation</td>
<td><a href="http://www.dnrpa.gov.ar/portal_dnrpa/" class="spip_out" rel="external">DNRPA</a> (<i>Dirección Nacional de los Registros Nacionales de la Propiedad del Automotor y de Créditos Prendarios</i>)</td></tr>
<tr class="row_even even">
<td>- En cas d’accident</td>
<td>En cas d’accident de la circulation mettant en cause un tiers, un constat de police est obligatoire. Attendre la police pour faire le constat, même pour un petit accrochage sinon les assurances ne remboursent pas.</td></tr>
<tr class="row_odd odd">
<td>- Assurances et taxes</td>
<td>Il est obligatoire de souscrire au minimum une assurance au tiers pour tout type de voiture (ne couvre ni les vols ni les incendies). Il est également possible de s’assurer tous risques mais les tarifs sont élevés.</td></tr>
<tr class="row_even even">
<td>- Achat et location</td>
<td>Aucune difficulté</td></tr>
<tr class="row_odd odd">
<td>- Entretien</td>
<td>Aucune difficulté</td></tr>
<tr class="row_even even">
<td>- Transports en commun</td>
<td><strong>Le réseau ferroviaire</strong> interurbain est pratiquement inexistant et a été remplacé par des bus ou des <i>combis. </i> Pour se déplacer <strong>à Buenos Aires et dans la banlieue</strong>, il est préférable d’emprunter les transports en commun (bus, train, métro) ou les taxis, nombreux et peu coûteux, car le stationnement dans le centre-ville est pratiquement impossible. Méfiez-vous cependant des taxis illégaux, en particulier à l’aéroport, pour éviter les mauvaises surprises. Le système de radiotaxis ou de <i>remises</i> (taxis privés qu’il faut appeler par téléphone) est à privilégier. <br> <br>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.transporte.gov.ar/" class="spip_out" rel="external">Ministère des transports argentin</a> </p></td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_8"></a>Vie pratique</h3>
<p class="spip_document_94332 spip_documents">
<img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L1000xH190/vie_pratique_cle0c829d-1d85c.jpg" width="1000" height="190" alt=""></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Coût de la vie</strong></td>
<td><a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Indice des prix à la consommation (IPCH)</a></td></tr>
<tr class="row_even even">
<td>- Monnaie et change</td>
<td>
<ul class="spip">
<li>Symbole : $</li>
<li>Code ISO : ARS</li>
<li>Sous-unité : cents</li>
<li>Banque centrale : <a href="http://www.bcra.gov.ar/" class="spip_out" rel="external">Banque centrale de l’Argentine</a></li></ul></td></tr>
<tr class="row_odd odd">
<td>- Opérations bancaires</td>
<td>Les retraits de liquide dans un guichet d’une autre banque que la vôtre entrainent des frais. Les horaires d’ouverture des banques sont assez réduits (entre 10h et 15h, éviter l’heure du déjeuner). Les banques sont fermées le samedi.  Depuis peu, les transferts de fonds sont possibles. Vous devez vous renseigner auprès de votre banque en France et en Argentine afin de compléter les documents nécessaire à ce transfert.</td></tr>
<tr class="row_even even">
<td><strong>Communications</strong></td>
<td></td></tr>
<tr class="row_odd odd">
<td>- Téléphone – internet</td>
<td>
<ul class="spip">
<li>Les indicatifs vers l’Argentine : 00 54</li>
<li>depuis l’Argentine vers la France : 00 33.</li></ul>
<p><br> <br>Les opérateurs proposant les packs télé-téléphone fixe-internet sont nombreux.</p>
</td></tr>
<tr class="row_even even">
<td>- Poste</td>
<td>Les liaisons postales sont souvent lentes (8 à 15 jours) mais présence de l’opérateur (OCA) pour envois au niveau national. <br> <br>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.correoargentino.com.ar/" class="spip_out" rel="external">Site Internet de la poste argentine</a>  </p></td></tr>
<tr class="row_odd odd">
<td><strong>Aménagements pour handicapés dans les lieux publics</strong></td>
<td>Les aménagements sont de bonne qualité, notamment l’accès aux moyens de transport en commun, magasins, salles de spectacles, trottoirs, etc.</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_9"></a>Culture et médias français</h3>
<table class="spip">
<thead><tr class="row_first"><th id="iddb7c_c0">Culture  </th><th id="iddb7c_c1"></th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="iddb7c_c0">- Tourisme</td>
<td headers="iddb7c_c1">
<ul class="spip">
<li><a href="http://www.turismo.gov.ar/eng/Information/fsinformations.htm" class="spip_out" rel="external">Site du ministère argentin du Tourisme</a></li>
<li><a href="http://www.camaraargentinadeturismo.travel/" class="spip_out" rel="external">Chambre argentine de tourisme</a></li>
<li><a href="http://www.transporte.gov.ar/" class="spip_out" rel="external">Ministère des transports argentin</a></li>
<li><a href="http://www.defensoriaturista.org.ar/" class="spip_out" rel="external">Défenseur du touriste ville de Buenos Aires</a></li></ul></td></tr>
<tr class="row_even even">
<td headers="iddb7c_c0">- Activités culturelles en français</td>
<td headers="iddb7c_c1">
<ul class="spip">
<li><a href="http://www.alianzafrancesa.org.ar/" class="spip_out" rel="external">Alliance Française</a></li>
<li><a href="http://www.embafrancia-argentina.org/agenda/" class="spip_out" rel="external">Service culturel de l’Ambassade de France</a></li>
<li><a href="http://www.embafrancia-argentina.org/-Version-Francaise-" class="spip_out" rel="external">Ambassade de France en Argentine</a></li></ul></td></tr>
<tr class="row_odd odd">
<td headers="iddb7c_c0"><strong>Médias français</strong></td>
<td headers="iddb7c_c1"></td></tr>
<tr class="row_even even">
<td headers="iddb7c_c0">Télévision</td>
<td headers="iddb7c_c1">Certains câblo-opérateurs régionaux diffusent des programmes français ou francophones, notamment <a href="http://www.tv5monde.com/" class="spip_out" rel="external">TV5MONDE</a> et France 24.</td></tr>
<tr class="row_odd odd">
<td headers="iddb7c_c0">- Presse française</td>
<td headers="iddb7c_c1">Une vingtaine de correspondants de la presse française sont établis en Argentine. Retrouvez leurs noms et leurs coordonnées sur le <a href="http://www.embafrancia-argentina.org/Correspondants-des-medias-francais-en-Argentine" class="spip_out" rel="external">site de l’ambassade de France</a></td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_10"></a>Présence française</h3>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>- Réseau français de coopération et d’action culturelle</td>
<td>
<ul class="spip">
<li><a href="http://www.embafrancia-argentina.org/-Version-Francaise-" class="spip_out" rel="external">Ambassade de France en Argentine</a></li>
<li><a href="http://www.latitudefrance.org/" class="spip_out" rel="external">Latitude France</a></li></ul></td></tr>
<tr class="row_even even">
<td>- Réseau économique</td>
<td>
<ul class="spip">
<li><a href="http://export.businessfrance.fr/default.html" class="spip_out" rel="external">Mission économique BusinessFrance</a></li>
<li><a href="http://www.tresor.economie.gouv.fr/Pays" class="spip_out" rel="external">Service économique français</a></li>
<li><a href="http://www.ccifa.com.ar/" class="spip_out" rel="external">Chambre de commerce et d’industrie France-Argentine</a></li>
<li><a href="http://www.cnccef.org/" class="spip_out" rel="external">Comité national des conseils du Commerce extérieur de la France</a></li></ul></td></tr>
<tr class="row_odd odd">
<td>- Les élus</td>
<td>
<ul class="spip">
<li><a href="http://www.assemblee-afe.fr/" class="spip_out" rel="external">Assemblée des Français de l’étranger</a></li>
<li><a href="http://www.senat.fr/expatries/vos_senateurs.html" class="spip_out" rel="external">Sénateurs des Français de l’étranger</a></li>
<li><a href="services-aux-citoyens-droit-de-vote-et-elections-a-l-etranger-article-les-elus-des-francais-de-l-etranger.md" class="spip_in">Députés des Français de l’étranger</a></li>
<li><a href="services-aux-citoyens-droit-de-vote-et-elections-a-l-etranger-article-les-elus-des-francais-de-l-etranger.md" class="spip_in">Conseillers consulaires</a></li></ul></td></tr>
<tr class="row_even even">
<td>- Associations françaises ou francophones</td>
<td>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.embafrancia-argentina.org/Associations-francaises-en" class="spip_out" rel="external">Argentine</a> </p></td></tr>
</tbody>
</table>
<p><i>Mise à jour : juin 2016</i></p>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/argentine/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
