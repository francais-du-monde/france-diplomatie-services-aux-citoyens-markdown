# Enseignement

<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-le-retour-en-france-enseignement-article-l-ecole-maternelle-et-l-ecole.md" title="L’école maternelle, l’école primaire/élémentaire">L’école maternelle, l’école primaire/élémentaire</a></li>
<li><a href="services-aux-citoyens-le-retour-en-france-enseignement-article-le-college-le-lycee.md" title="Le collège, le lycée">Le collège, le lycée</a></li>
<li><a href="services-aux-citoyens-le-retour-en-france-enseignement-article-l-enseignement-international-en.md" title="L’enseignement international en France">L’enseignement international en France</a></li>
<li><a href="services-aux-citoyens-le-retour-en-france-enseignement-article-l-enseignement-superieur-103684.md" title="L’enseignement supérieur">L’enseignement supérieur</a></li>
<li><a href="services-aux-citoyens-le-retour-en-france-enseignement-article-les-aides-financieres.md" title="Les aides financières">Les aides financières</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/enseignement/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
