# Protection sociale

<h2 class="rub22802">Régime local de sécurité sociale</h2>
<p>Vous trouverez une présentation détaillée du système de sécurité sociale japonaise sur le site de notre partenaire, le <a href="http://www.cleiss.fr/" class="spip_out" rel="external">CLEISS</a> (Centre des liaisons européennes et internationales de sécurité Sociale). En voici la table des matières :</p>
<p><a href="http://www.cleiss.fr/docs/regimes/regime_japon_salaries.html" class="spip_out" rel="external">Le régime des salariés</a> :</p>
<ul class="spip">
<li><a href="http://www.cleiss.fr/docs/regimes/regime_japon_salaries.html#generalites" class="spip_out" rel="external">Généralités</a> (Organisation, financement, taux de cotisation sur les salaires…)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_japon_salaries.html#financement" class="spip_out" rel="external">Financement</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_japon_salaries.html#maladie" class="spip_out" rel="external">Assurance maladie</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_japon_salaries.html#pension" class="spip_out" rel="external">Assurance pension</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_japon_salaries.html#adtmp" class="spip_out" rel="external">Assurance accidents du travail et maladie professionnelle</a> (Régime indemnitaire, y compris pour les survivants)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_japon_salaries.html#chomage" class="spip_out" rel="external">Assurance Chômage "koyo hoken"</a> (Indemnités et assistance chômage)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_japon_salaries.html#pf" class="spip_out" rel="external">Prestations familiales</a> (Allocations familiales, de naissance, d’adoption…)</li></ul>
<p><a href="http://www.cleiss.fr/docs/regimes/regime_japon_independants.html" class="spip_out" rel="external">Le régime des indépendants</a> :</p>
<ul class="spip">
<li><a href="http://www.cleiss.fr/docs/regimes/regime_japon_independants.html#generalites" class="spip_out" rel="external">Généralités</a> (Organisation, financement, taux de cotisation sur les salaires…)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_japon_independants.html#financement" class="spip_out" rel="external">Financement</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_japon_independants.html#maladie" class="spip_out" rel="external">Assurance maladie</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_japon_independants.html#pension" class="spip_out" rel="external">Assurance pension</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_japon_independants.html#adtmp" class="spip_out" rel="external">Assurance accidents du travail et maladie professionnelle</a> (Régime indemnitaire, y compris pour les survivants)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_japon_independants.html#pf" class="spip_out" rel="external">Prestations familiales</a> (Allocations familiales, de naissance, d’adoption…)</li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-japon-protection-sociale-22802-article-convention-de-securite-sociale-109852.md" title="Convention de sécurité sociale">Convention de sécurité sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-japon-protection-sociale-22802-article-regime-local-de-securite-sociale-109851.md" title="Régime local de sécurité sociale">Régime local de sécurité sociale</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/protection-sociale-22802/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
