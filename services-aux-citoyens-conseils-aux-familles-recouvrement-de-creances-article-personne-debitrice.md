# Personne débitrice

<p>Vous êtes une personne débitrice d’une obligation alimentaire.</p>
<p>Vous avez <strong>l’obligation légale</strong> de payer à votre créancier une somme d’argent fixée par décision de justice au titre des aliments dus. <br class="autobr">Votre résidence ou celle de votre créancier est située à l’étranger.</p>
<p>Si vous n’exécutez pas volontairement le paiement de cette créance, le créancier peut engager <a href="services-aux-citoyens-conseils-aux-familles-recouvrement-de-creances-article-la-procedure-de-recouvrement-de.md" class="spip_in">la procédure de recouvrement de créances alimentaires à l’étranger</a>.</p>
<p>Celui-ci dispose en principe de cinq années pour procéder au recouvrement de chaque mois de pension alimentaire impayée.</p>
<p>Par ailleurs, le non paiement de la pension alimentaire due est sanctionné pénalement par le délit d’abandon de famille passible d’une peine de deux ans d’emprisonnement et de 1 500 euros d’amende.</p>
<p>Il est par conséquent toujours préférable de trouver un arrangement amiable avec le créancier d’aliments afin de payer sa dette.</p>
<p>Enfin, le règlement (CE) n°4/2009 du Conseil du 18 décembre 2008 relatif à la compétence, la loi applicable, la reconnaissance et l’exécution des décisions et la coopération en matière d’obligations alimentaires, entré en vigueur le 18 juin 2011, vous permet d’effectuer des demandes, notamment solliciter la modification de la décision de justice existante fixant la pension alimentaire. <br class="autobr">Il convient de saisir, vous-même ou par le biais de votre avocat, le bureau RCA, lequel vous orientera dans les démarches à effectuer auprès des autorités judiciaires compétentes.</p>
<p><i>Mise à jour : juin 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/conseils-aux-familles/recouvrement-de-creances/article/personne-debitrice). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
