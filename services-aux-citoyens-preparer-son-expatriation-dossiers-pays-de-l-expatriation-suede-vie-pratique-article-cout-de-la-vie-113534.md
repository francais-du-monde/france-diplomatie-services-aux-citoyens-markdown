# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/vie-pratique/article/cout-de-la-vie-113534#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/vie-pratique/article/cout-de-la-vie-113534#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/vie-pratique/article/cout-de-la-vie-113534#sommaire_3">Alimentation</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/vie-pratique/article/cout-de-la-vie-113534#sommaire_4">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>L’unité monétaire est la couronne suédoise (SEK). La couronne est subdivisée en 100 öre (la plus petite unité monétaire est la pièce de 50 öre). La couronne suédoise ne fait pas parti de l’Euroland.</p>
<p>Toutes les cartes de crédit sont largement acceptées. Les chéquiers n’existent plus. Les banques sont en général ouvertes du lundi au vendredi de 9h30 à 15h.</p>
<p>Les principales banques suédoises sont Handelsbanken, SEB, Sparbanken, Nordea.</p>
<p>Il est assez simple d’ouvrir un compte bancaire en Suède : il convient de présenter un justificatif de domicile, un passeport biométrique et un justificatif d’emploi.</p>
<p>Il existe des banques françaises installées en Suède, mais elles n’offrent pas leurs services à la clientèle particulière.</p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<p>Les transferts de fonds sont libres.</p>
<p>Selon le règlement (CE) NO 1889/2005 du parlement européen et du Conseil du 26 octobre 2005 relatif aux contrôles de l’argent liquide entrant ou sortant de la Communauté, toute personne physique entrant ou sortant de la Communauté avec au moins <strong>10 000 euros en argent liquide</strong> déclare la somme transportée aux autorités compétentes de l’État membre par lequel elle entre ou sort de la Communauté.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://ec.europa.eu/taxation_customs/customs/customs_controls/cash_controls/index_fr.htm" class="spip_out" rel="external">Ec.europa.eu</a></p>
<h3 class="spip"><a id="sommaire_3"></a>Alimentation</h3>
<p>Les conditions d’approvisionnement sont excellentes. Aucun bien particulier ne doit être acheté à l’extérieur du pays.</p>
<p><strong>Prix moyen d’un repas dans un restaurant</strong></p>
<p>Un repas à Stockholm dans un restaurant de qualité supérieure coûte environ 450 SEK et dans un restaurant de qualité moyenne environ 200 SEK (vin exclu). Le pourboire est facultatif.</p>
<h3 class="spip"><a id="sommaire_4"></a>Evolution des prix</h3>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/vie-pratique/article/cout-de-la-vie-113534). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
