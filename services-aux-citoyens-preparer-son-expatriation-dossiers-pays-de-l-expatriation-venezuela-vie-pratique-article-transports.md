# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/vie-pratique/article/transports#sommaire_1">Importation de véhicule</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/vie-pratique/article/transports#sommaire_2">Permis de conduire</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/vie-pratique/article/transports#sommaire_3">Code de la route</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/vie-pratique/article/transports#sommaire_4">Assurances et taxes</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/vie-pratique/article/transports#sommaire_5">Achat et location</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/vie-pratique/article/transports#sommaire_6">Entretien</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/vie-pratique/article/transports#sommaire_7">Transports en commun</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Importation de véhicule</h3>
<p>L’importation d’un véhicule français dans le pays est fortement déconseillée : les démarches administratives sont difficiles et la détaxe est refusée sur certains modèles en fonction de la puissance du moteur.</p>
<h3 class="spip"><a id="sommaire_2"></a>Permis de conduire</h3>
<p>Les français titulaires d’un permis international peuvent l’utiliser durant leur première année de résidence au Venezuela. Ils doivent ensuite échanger leur permis français contre un permis local pour pouvoir circuler dans le pays. Il convient, pour ce faire, de produire les documents suivants :</p>
<ul class="spip">
<li>Photocopie du passeport ou de la "cédula" vénézuélienne ;</li>
<li>Photocopie du permis de conduire français ;</li>
<li>Examen médical en cours de validité ;</li>
<li>Examen théorique délivré par la "Inspectoria de transito del Llanito" ;</li>
<li>deux photographies d’identité.</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Code de la route</h3>
<p>Les automobilistes locaux respectent rarement le code de la route. De plus, en dehors des grands axes, les routes sont mal entretenues et le réseau a beaucoup souffert des pluies diluviennes de fin 2010. Il est donc préférable d’utiliser un véhicule tout terrain.</p>
<h3 class="spip"><a id="sommaire_4"></a>Assurances et taxes</h3>
<p>La responsabilité civile limitée n’existe pas. Le risque obligatoire est couvert avec une limitation légale inférieure à 12.000 € en cas de dommages corporels au tiers. La prime annuelle s’élève environ à 220 €. Il convient toutefois de souscrire une assurance complémentaire couvrant le vol ainsi que les dommages causés au conducteur, passagers et véhicules. Le montant de la prime annuelle se situe entre 10 et 15 % de la valeur vénale du véhicule et varie selon le type de véhicule.</p>
<h3 class="spip"><a id="sommaire_5"></a>Achat et location</h3>
<p>La plupart des marques européennes, asiatiques et américaines sont disponibles sur le territoire vénézuélien. Vous trouverez sans difficulté des concessionnaires de toutes marques dans les grandes villes.</p>
<p>La location de véhicules sur place est possible mais les tarifs sont plus élevés qu’en France.</p>
<h3 class="spip"><a id="sommaire_6"></a>Entretien</h3>
<p>Pour les véhicules achetés sur place les réparations sont effectuées sans difficultés mais restent assez coûteuses pour un service sans garantie. L’importation de pièces détachées pour les véhicules de marque française est possible mais est soumis à des droits de douanes très élevés (120% en moyenne).</p>
<h3 class="spip"><a id="sommaire_7"></a>Transports en commun</h3>
<p>Le pays dispose d’un très large <strong>réseau aérien</strong>, desservant presque toutes les villes et permettant de rejoindre les régions les plus reculées. Pour se déplacer dans tout le pays l’avion est donc le mode de transport le plus simple. Les vols nationaux sont assurés par plusieurs compagnies vénézuéliennes, dont <i>Avensa</i> et <i>Avior Airline</i>.</p>
<p>Le réseau ferroviaire étant inexistant, le bus est le moyen le plus utilisé par les vénézuéliens pour parcourir le pays. Les compagnies d’autobus proposent différents types de véhicules selon les tarifs : les « bus cama » sont les plus confortables (sièges inclinables, climatisation…). Attention, cependant, il est rare que les horaires soient respectés et les trajets sont souvent très longs en l’absence de voie rapide.</p>
<p>Vous trouverez aussi des <strong>taxis</strong> à tous les coins de rue. Pour reconnaitre les compagnies fiables, repérez-vous aux lignes noires et blanches en quadrillé sur les portières ou aux pancartes « taxi » et « libre ». Dans tout les cas il n’y a pas de compteur, les prix sont fixés par zones et sont négociables avec le chauffeur. Il faut compter entre 70 à 200 bolivars pour une course en journée dans Caracas.</p>
<p>Dans Caracas même, vous pouvez opter pour le <strong>métro</strong>. Le réseau, moderne et climatisé, fonctionne très bien. Les quatre lignes sont ouvertes de 05h30 à 23h et le billet coûte 0.30 cts d’euros. Il existe également des <strong>métrobus</strong>, autobus qui desservent les quartiers périphériques au départ des stations de métro. Pour plus d’informations rendez-vous sur le site du <a href="http://www.metrodecaracas.com.ve/" class="spip_out" rel="external">métro de Caracas</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/vie-pratique/article/transports). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
