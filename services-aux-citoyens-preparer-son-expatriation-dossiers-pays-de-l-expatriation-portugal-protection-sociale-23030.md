# Protection sociale

<h2 class="rub23030">Régime local de sécurité sociale</h2>
<p>Vous trouverez une présentation détaillée du système de sécurité sociale portugais sur le site de notre partenaire, le <a href="http://www.cleiss.fr/" class="spip_out" rel="external">CLEISS</a> (Centre des liaisons européennes et internationales de sécurité sociale). En voici la table des matières :</p>
<p><a href="http://www.cleiss.fr/docs/regimes/regime_portugal_s.html" class="spip_out" rel="external">Le régime des salariés</a> :</p>
<ul class="spip">
<li><a href="http://www.cleiss.fr/docs/regimes/regime_portugal_s.html#generalites" class="spip_out" rel="external">Généralités</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_portugal_s.html#maladie" class="spip_out" rel="external">Maladie, maternité</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_portugal_s.html#adt" class="spip_out" rel="external">Accidents du travail et maladies professionnelles</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_portugal_s.html#pensions" class="spip_out" rel="external">Pensions</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_portugal_s.html#chomage" class="spip_out" rel="external">Chômage</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_portugal_s.html#pf" class="spip_out" rel="external">Prestations familiales</a></li></ul>
<p><a href="http://www.cleiss.fr/docs/regimes/regime_portugal_ns.html" class="spip_out" rel="external">Le régime des non salariés</a> :</p>
<ul class="spip">
<li><a href="http://www.cleiss.fr/docs/regimes/regime_portugal_ns.html#generalites" class="spip_out" rel="external">Généralités</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_portugal_ns.html#maladie" class="spip_out" rel="external">Maladie, maternité</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_portugal_ns.html#adt" class="spip_out" rel="external">Accidents du travail et maladies professionnelles</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_portugal_ns.html#pensions" class="spip_out" rel="external">Pensions</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_portugal_ns.html#pf" class="spip_out" rel="external">Prestations familiales</a></li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-protection-sociale-23030-article-convention-de-securite-sociale-111293.md" title="Convention de sécurité sociale">Convention de sécurité sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-protection-sociale-23030-article-regime-local-de-securite-sociale-111292.md" title="Régime local de sécurité sociale">Régime local de sécurité sociale</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/protection-sociale-23030/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
