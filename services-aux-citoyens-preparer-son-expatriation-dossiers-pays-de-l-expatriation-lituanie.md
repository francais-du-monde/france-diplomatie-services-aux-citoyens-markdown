# Lituanie

<p>Au 31 décembre 2014, la communauté française en Lituanie inscrite au registre des Français établis hors de France était de <strong>418</strong> personnes.</p>
<p>Il s’agit d’une communauté assez jeune, résidant à Vilnius et travaillant essentiellement dans le secteur tertiaire et industriel. On trouve également des Français dans le secteur de l’enseignement et de la coopération, aussi bien sous l’égide des services de l’ambassade de France que, plus récemment, dans le cadre d’un volontariat conduit par l’Union européenne. La majorité des ressortissants français expatriés en Lituanie s’y installent de manière indépendante ; ils suivent un conjoint lituanien ou créent leur société sur place.</p>
<p>Parallèlement, le nombre d’entreprises tricolores bénéficiant d’une implantation locale est stable. Il s’établissait fin 2012 à plus de 65 sociétés, filiales de grands groupes traduisant ainsi l’intérêt toujours plus marqué de la part des investisseurs français pour ce petit marché très dynamique, qui présente des atouts incontestables et des opportunités d’affaires plurielles. Il est en effet relativement facile d’accès pour des PME et constitue aussi un véritable tremplin vers les marchés russe (enclave de Kaliningrad) et biélorusse.</p>
<p>On citera notamment ici l’exemple de DALKIA, qui gère le chauffage urbain de la capitale et d’une dizaine de villes de province, EUROVIA, BULL, JC DECAUX, GEOPOST, TOTAL, BUREAU VERITAS, SANOFI AVENTIS, SCHNEIDER ELECTRIC, ALSTOM, SAINT GOBAIN etc.</p>
<p>Les domaines d’activité ralliant la majorité des filiales françaises en Lituanie restent l’industrie (production d’énergie, textile, bois), le transport, le secteur agricole et agroalimentaire (produits laitiers, spiritueux), les biens de consommation (produits cosmétiques et pharmaceutiques) ainsi que les services (grande distribution, hôtellerie-restauration, publicité, tourisme, immobilier, décoration).</p>
<p>A ces sociétés vient s’ajouter également une vingtaine de petites entreprises créées localement par des Français établis en Lituanie.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/dossiers-pays/lituanie/" class="spip_in">Une description de la Lituanie, de sa situation politique et économique</a></li>
<li><a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/lituanie/" class="spip_in">Des informations actualisées sur les conditions locales de sécurité en Lituanie</a></li>
<li><a href="http://www.ambafrance-lt.org/" class="spip_out" rel="external">Ambassade de France en Lituanie</a>.</li></ul>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-lituanie-entree-et-sejour.md">Entrée et séjour</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-lituanie-entree-et-sejour-article-passeport-visa-permis-de-travail.md">Passeport, visa, permis de travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-lituanie-entree-et-sejour-article-demenagement-111139.md">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-lituanie-entree-et-sejour-article-vaccination-111140.md">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-lituanie-entree-et-sejour-article-animaux-domestiques.md">Animaux domestiques</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-lituanie-emploi-stage.md">Emploi, stage</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-lituanie-emploi-stage-article-marche-du-travail.md">Marché du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-lituanie-emploi-stage-article-reglementation-du-travail-111143.md">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-lituanie-emploi-stage-article-recherche-d-emploi.md">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-lituanie-emploi-stage-article-stages.md">Stages</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-lituanie-protection-sociale.md">Protection sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-lituanie-protection-sociale-article-regime-local-de-securite-sociale-111149.md">Régime local de sécurité sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-lituanie-protection-sociale-article-convention-de-securite-sociale.md">Convention de sécurité sociale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-lituanie-fiscalite-23001.md">Fiscalité</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-lituanie-fiscalite-23001-article-fiscalite-du-pays-111151.md">Fiscalité du pays</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-lituanie-fiscalite-23001-article-convention-fiscale-111152.md">Convention fiscale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-lituanie-vie-pratique.md">Vie pratique</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-lituanie-vie-pratique-article-logement-111153.md">Logement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-lituanie-vie-pratique-article-sante.md">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-lituanie-vie-pratique-article-scolarisation.md">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-lituanie-vie-pratique-article-cout-de-la-vie-111156.md">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-lituanie-vie-pratique-article-transports-111157.md">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-lituanie-vie-pratique-article-communications.md">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-lituanie-vie-pratique-article-loisirs-et-culture.md">Loisirs et culture</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-lituanie-presence-francaise.md">Présence française</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-lituanie-presence-francaise-article-presence-francaise.md">Présence française</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
