# Communications

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/vie-pratique/article/communications#sommaire_1">Téléphone – Internet</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/vie-pratique/article/communications#sommaire_2">Internet</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/vie-pratique/article/communications#sommaire_3">Poste</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Téléphone – Internet</h3>
<p>Les liaisons téléphoniques avec la France sont très bonnes.</p>
<h4 class="spip">Indicatifs</h4>
<ul class="spip">
<li><strong>Depuis la France vers l’Irlande</strong> : 00 353 + le numéro d’abonné sans le " 0 " local.<br class="manualbr">Le numéro d’appel peut comporter un nombre variable de chiffres selon la région.</li>
<li><strong>Depuis l’Irlande vers la France</strong> : 00 33 + le numéro d’abonné sans le " 0 " local (soit 9 chiffres).<br class="manualbr">Certains numéros ne peuvent être obtenus depuis l’étranger (numéros spéciaux, numéros verts…).<br class="manualbr"><i>Meteor</i>, <i>Vodafone </i>et <i>02 </i>sont les principaux fournisseurs de téléphones mobiles.</li></ul>
<h4 class="spip">Utilisation de votre portable français</h4>
<p>Vous pouvez utiliser votre téléphone portable partout en Europe et dans de nombreuses autres régions du monde, grâce à la norme technique GSM de l’Union européenne. Les personnes qui utilisent leur téléphone portable à l’étranger réalisent d’importantes économies – de l’ordre de 60 % – grâce au règlement de l’UE sur l’itinérance (« roaming »), qui plafonne le coût des appels téléphoniques effectués ou reçus dans un autre pays de l’UE (« eurotarif »). De plus, une facturation à la seconde a été introduite après les premières 30 secondes pour les appels effectués, et dès la première seconde pour les appels reçus. Le coût d’envoi d’un SMS depuis l’étranger a lui aussi nettement diminué, il est désormais de 11 cents (hors TVA) maximum.</p>
<p>Les clients reçoivent désormais un message automatique les informant du coût des appels, des SMS et des services de transmission de données à l’étranger (en cas de navigation sur internet ou de téléchargement d’un film, p. ex.). Les prix de gros que les opérateurs se facturent mutuellement sont également plafonnés, ce qui devrait réduire les coûts supportés par les consommateurs. Un mécanisme a également été mis en place afin d’éviter aux consommateurs de devoir payer des factures astronomiques pour des services d’itinérance : après un premier avertissement, la connexion internet mobile est suspendue dès que le montant atteint un certain niveau. Une limite de 50 euros par mois s’applique par défaut si le client n’a pas défini un montant spécifique.</p>
<p>Un site web dédié de l’UE publie les <a href="http://ec.europa.eu/information_society/activities/roaming/index_en.htm" class="spip_out" rel="external">eurotarifs appliqués par les opérateurs des 28 pays membres</a>, ainsi que les liens vers leurs sites.</p>
<p><strong>Autre sites utiles : </strong></p>
<ul class="spip">
<li><a href="http://www.eirphonebook.ie/" class="spip_out" rel="external">Les pages blanches en Irlande</a></li>
<li><a href="http://www.goldenpages.ie/" class="spip_out" rel="external">Les pages jaunes en Irlande</a></li>
<li><a href="http://www.11850.ie/" class="spip_out" rel="external">11850 Directory Enquiries</a></li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Internet</h3>
<p>L’accès à Internet est très répandu dans le pays. On trouve de nombreux cybercafés à Dublin, Cork, Galway, Waterford…</p>
<h3 class="spip"><a id="sommaire_3"></a>Poste</h3>
<p>Les liaisons postales fonctionnent normalement, les délais de réception variant de 1 à 10 jours selon la nature de l’envoi et sa destination.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://postage.anpost.ie/" class="spip_out" rel="external">Calculateur des frais d’envoi sur le site de la poste en Irlande</a></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/vie-pratique/article/communications). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
