# Vaccination

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/entree-et-sejour/article/vaccination#sommaire_1">Vaccination contre la méningo-encéphalite</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Vaccination contre la méningo-encéphalite</h3>
<p> (FSME - Zeckenimpfung)</p>
<p>Le vaccin n’est pas obligatoire mais fortement recommandé.</p>
<p>Vous avez la possibilité de vous faire vacciner par votre médecin généraliste traitant. Il conviendra de vous procurez le sérum en pharmacie au préalable.</p>
<p><strong>Pour en savoir plus : </strong> <br class="autobr">Vous pouvez consulter les sites suivants :</p>
<ul class="spip">
<li>Notre rubrique thématique <a href="services-aux-citoyens-preparer-son-expatriation-sante.md" class="spip_in">Santé</a> ;</li>
<li><a href="https://www.wien.gv.at/" class="spip_out" rel="external">https://www.wien.gv.at/</a> (en allemand) ;</li>
<li><a href="http://zecken.at/" class="spip_out" rel="external">Zecken.at</a> (en allemand) ;</li></ul>
<p>Pour en savoir plus, lisez <a href="http://www.mfe.org/index.php/Thematiques/Sante/Vaccinations" class="spip_out" rel="external">notre article thématique sur la vaccination</a>.</p>
<p><i>Mise à jour : février 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/entree-et-sejour/article/vaccination). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
