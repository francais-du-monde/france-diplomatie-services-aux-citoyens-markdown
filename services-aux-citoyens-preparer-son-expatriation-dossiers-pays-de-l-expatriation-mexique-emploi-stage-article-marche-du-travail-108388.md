# Marché du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/emploi-stage/article/marche-du-travail-108388#sommaire_1">Secteurs à fort potentiel</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/emploi-stage/article/marche-du-travail-108388#sommaire_2">Taux de chômage</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/emploi-stage/article/marche-du-travail-108388#sommaire_3">Professions règlementées</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/emploi-stage/article/marche-du-travail-108388#sommaire_4">Rémunération</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Secteurs à fort potentiel</h3>
<p><strong>Répartition de la population occupant un emploi en fonction des secteurs d’activités au 3ème trimestre 2012 :</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Population totale</td>
<td class="numeric ">115 296 767</td>
<td></td></tr>
<tr class="row_even even">
<td>Population active</td>
<td class="numeric ">51 378 927</td>
<td></td></tr>
<tr class="row_odd odd">
<td>Population occupant un emploi</td>
<td class="numeric ">48 732 252</td>
<td></td></tr>
<tr class="row_even even">
<td>Secteur primaire</td>
<td class="numeric ">6 737 884</td>
<td>14%</td></tr>
<tr class="row_odd odd">
<td>Secteur secondaire</td>
<td class="numeric ">11 504 587</td>
<td>24%</td></tr>
<tr class="row_even even">
<td>Secteur tertiaire</td>
<td class="numeric ">30 171 041</td>
<td>62%</td></tr>
<tr class="row_odd odd">
<td>Non spécifié</td>
<td class="numeric ">318 740</td>
<td>1%</td></tr>
</tbody>
</table>
<p>Les types de postes les plus recherchés sont les postes de commerciaux et particulièrement de technico-commerciaux ainsi que les ingénieurs qualifiés. En termes d’embauche, les secteurs les plus dynamiques sont les communications, le transport et le tourisme.</p>
<p>D’une manière générale les profils les plus recherchés concernent les postes techniques (techniciens dans l’industrie, technicien en maintenance automobile, ingénieur qualité, responsable de maintenance raffinerie, technicien maintenance export, ingénieur superviseur en électronique, responsable essai et montage d’usine…), les postes de commerciaux, souvent dans le commerce international, les postes dans l’enseignement du français, le secteur de la mode et des produits de luxe ainsi que pour tous les métiers du tourisme (restauration, hôtellerie, sports nautiques).</p>
<p>Certaines zones du Mexique sont très dynamiques dans des secteurs particuliers, c’est le cas par exemple du bassin Querétaro – Léon - San Luis Potosi (Bajío), en pleine expansion dans les secteurs de l’automobile et de l’aéronautique, avec un fort recrutement de techniciens et d’ingénieurs spécialisés dans ces domaines. Dans ce cas précis, le Mexique manque d’ingénieurs industriels de haut niveau (le pays forme plutôt des techniciens de maintenance par exemple) ce qui peut représenter une belle opportunité pour des candidats français au niveau ingénieur Bac+5 (versus niveau licence au Mexique).</p>
<h3 class="spip"><a id="sommaire_2"></a>Taux de chômage</h3>
<p>Au 3ème trimestre 2012, la population active représentait 44,56% de la population mexicaine (soit 51 378 927 personnes) selon l’INEGI. Le taux de chômage national s’élevait à 5,2% de la population économiquement active à cette même date ce qui représente environ 2,646 millions d’individus sans emploi. Ce taux est similaire à celui observé l’an dernier à la même période puisqu’en octobre 2011 le taux de chômage était de 5,0%. Pour 2013, il est prévu la création de 500,000 nouveaux emplois (assurés à l’IMSS) et un taux de chômage moyen de 4,6% sur l’année.</p>
<h3 class="spip"><a id="sommaire_3"></a>Professions règlementées</h3>
<p>Certains secteurs ne sont pas accessibles aux ressortissants étrangers (déterminés par la loi sur l’investissement étranger), tels que le pétrole et autres hydrocarbures, l’électricité, la production d’énergie nucléaire, les minéraux radioactifs et les services télégraphiques.</p>
<h3 class="spip"><a id="sommaire_4"></a>Rémunération</h3>
<p>La rémunération du salarié est composée du salaire journalier (au moins égal au salaire minimum défini par la Commission nationale des salaires minimum ou par les dispositions de la convention collective applicable), des gratifications, primes, commissions, prestations en espèce ou avantages réservés au salarié en échange de son travail. Selon la loi (art. 88 LFT), le versement du salaire, en liquide ou par chèque, peut se faire suivant deux modalités : les salariés effectuant des tâches « matérielles » sont payés de manière hebdomadaire ; les autres tous les quinze jours. Dans la pratique, on observe une tendance à la mensualisation de la rémunération du personnel d’encadrement.</p>
<p>Le montant du salaire minimum journalier est fixé chaque année pour trois zones géographiques. Ce salaire, qui peut être général ou catégoriel, ne s’applique qu’à un nombre restreint de salariés mais son évolution sert de référence aux négociations collectives sur la revalorisation des salaires. A partir du 27 novembre 2012, le salaire minimum légal journalier a été fixé à 62,33 pesos pour la zone de Mexico DF soit environ 3,72 €. A titre comparatif, il était de 59,82 pesos en 2011.</p>
<p>En 2012, selon l’INEGI, le salaire moyen mensuel était de 7,809.75 pesos soit environ 467 € (en effet, au cours du premier trimestre 2012, 64,4% des salariés gagnaient moins de 10.000 pesos (environ 600 €) soit cinq fois le salaire minimum par mois).Les salariés ne peuvent pas effectuer plus de trois heures supplémentaires en une seule journée et ce seulement trois fois par semaine. Ces heures supplémentaires doivent être payées le double du salaire normal. Si les employés travaillent pendant leur jour de repos, ils ont droit à l’équivalent de trois fois le salaire normal correspondant à ce jour. S’ils travaillent le dimanche et même s’ils se voient attribuer un jour de repos pendant la semaine suivante, l’employeur devra leur verser une prime de 25 % en supplément du salaire journalier normal.Lorsque vous consultez une offre d’emploi, il convient de vérifier, outre le salaire brut comparé au salaire net, les prestations offertes à savoir :</p>
<ul class="spip">
<li><strong>Aguinaldo</strong>(prime annuelle, 13ème mois) : minimum 15 jours de salaire, payable normalement le 20 décembre ;</li>
<li><strong>IMSS</strong> (sécurité sociale) ;</li>
<li><strong>Seguro de Gastos Medicos Mayores</strong> (équivalent d’une mutuelle) : <strong>non obligatoire</strong> ;</li>
<li><strong>Vales de despensa</strong> (tickets restaurant, carte d’achat pour nourriture, essence, etc…) ;</li>
<li><strong>Fondo de ahorro</strong> : chaque mois l’entreprise prélève 1 % de votre salaire et vous le restitue au double en fin de l’année ;</li>
<li><strong>Vacaciones</strong> : la loi autorise 6 jours de congés payés à partir de la première année puis ce nombre va en augmentant chaque année ;</li>
<li><strong>Prima vacacional</strong> : prime que l’on vous verse pour chaque jour de congés payés.</li></ul>
<p><i>Mise à jour : septembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/emploi-stage/article/marche-du-travail-108388). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
