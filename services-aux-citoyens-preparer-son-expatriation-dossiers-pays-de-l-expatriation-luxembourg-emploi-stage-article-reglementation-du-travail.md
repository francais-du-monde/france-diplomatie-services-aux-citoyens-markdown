# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/emploi-stage/article/reglementation-du-travail#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/emploi-stage/article/reglementation-du-travail#sommaire_2">Contrat de travail – spécificités</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/emploi-stage/article/reglementation-du-travail#sommaire_3">Fêtes légales</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/emploi-stage/article/reglementation-du-travail#sommaire_4">Emploi du conjoint</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/emploi-stage/article/reglementation-du-travail#sommaire_5">Création d’entreprises – spécificités</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<p>Depuis le 1er janvier 2009, la distinction entre les catégories socioprofessionnelles des personnels du secteur public et du secteur privé n’existe plus, en droit du travail ni en matière de sécurité sociale.</p>
<h4 class="spip">Durée du travail</h4>
<p>La durée normale de travail des salariés est de <strong>huit</strong> heures par jour et <strong>40</strong> heures par semaine.</p>
<p>L’employeur peut déterminer une période de référence (généralement d’un mois) au cours de laquelle la durée normale de travail est appliquée de façon plus souple. Pour ce faire, l’employeur doit obligatoirement établir :</p>
<ul class="spip">
<li>soit un plan d’organisation du travail (POT) ;</li>
<li>soit un horaire mobile. Quoiqu’il en soit, la durée maximale de travail ne peut dépasser 10 heures par jour, ni 48 heures par semaine.</li></ul>
<p>L’employeur doit garantir à ses salariés des périodes de repos de minimum :</p>
<ul class="spip">
<li>11 heures consécutives au cours de chaque période de 24 heures ;</li>
<li>44 heures consécutives par période de sept jours. Dès lors que la durée de travail journalière est supérieure à <strong>six heures</strong>, tout salarié a droit à un ou de plusieurs temps de repos, rémunérés ou non.</li></ul>
<p>Une convention collective peut cependant fixer des limites inférieures à ces seuils.</p>
<p>Toute heure de travail dépassant la durée normale de travail est considérée comme heure supplémentaire. Les heures supplémentaires prestées peuvent être soit récupérées par le salarié, soit rémunérées. La rémunération des heures supplémentaires ainsi prestées est exempte d’impôt et partiellement exonérée de charges sociales.</p>
<p>Le travail de dimanche (de minuit à minuit) est en principe interdit pour tous les salariés et apprentis. Il est toutefois autorisé pour certaines catégories de salariés, dans certains secteurs d’activité et pour certains travaux spécifiques qui ne peuvent pas être réalisés en semaine. Le travail dominical fait l’objet d’un repos compensatoire et d’une rétribution spécifiques.</p>
<h4 class="spip">Période de préavis</h4>
<p>L’employeur qui licencie un salarié pour une raison autre qu’une <a href="http://www.guichet.public.lu/entreprises/fr/ressources-humaines/fin-relation-travail/licenciement-resiliation/licencier-faute-grave/index.html" class="spip_out" rel="external">faute grave</a> doit lui accorder un <strong>préavis</strong> ainsi qu’une <strong>indemnité de licenciement</strong> en cas d’ancienneté de service de cinq ans ou plus. Si l’entreprise compte plus de 150 salariés, le licenciement doit être précédé d’un entretien préalable avec le salarié à licencier. Si elle compte au moins 15 salariés, elle doit par ailleurs notifier au Comité de conjoncture chaque licenciement effectué pour des raisons non inhérentes à la personne du salarié. Le salarié licencié qui preste son préavis peut par ailleurs demander à son employeur un congé pour la recherche d’un nouvel emploi au cours de sa période de préavis.</p>
<p>Le préavis commence à courir le 15 du mois en cours, si la lettre de licenciement est notifiée avant le 15, le 1er jour du mois suivant, si la lettre de licenciement est notifiée entre le 15e et le dernier jour du mois.</p>
<p>Si l’employeur ne respecte pas le délai de préavis, il doit verser au salarié une <strong>indemnité compensatoire de préavis</strong> correspondant à la rémunération due pour la période de préavis non respectée.</p>
<h5 class="spip">Durée du préavis</h5>
<p>La durée du préavis dépend de l’ancienneté du salarié au moment de la notification du licenciement.</p>
<p><strong>Délai de préavis en fonction de l’ancienneté du salarié</strong></p>
<table class="spip">
<thead><tr class="row_first"><th id="id8915_c0">Ancienneté de service du salarié </th><th id="id8915_c1">Délai de préavis à respecter </th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="id8915_c0">moins de 5 ans</td>
<td headers="id8915_c1">2 mois</td></tr>
<tr class="row_even even">
<td headers="id8915_c0">de 5 à moins de 10 ans</td>
<td headers="id8915_c1">4 mois</td></tr>
<tr class="row_odd odd">
<td headers="id8915_c0">10 ans et plus</td>
<td headers="id8915_c1">6 mois</td></tr>
</tbody>
</table>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.guichet.public.lu/entreprises/fr/ressources-humaines/fin-relation-travail/index.html" class="spip_out" rel="external">Guichet public - Fin de la relation de travail</a>.</p>
<h4 class="spip">Rémunération</h4>
<p>Le salaire est <strong>librement déterminé</strong> par les deux parties au moment de la signature du <a href="http://www.guichet.public.lu/entreprises/fr/ressources-humaines/contrat-convention/contrat-travail/duree-indeterminee/index.html" class="spip_out" rel="external">contrat de travail</a>. Néanmoins, tout employeur doit respecter le salaire social minimum applicable en fonction de la qualification du salarié. Les salaires à verser peuvent être impactés par deux types d’ajustement :</p>
<ul class="spip">
<li>l’augmentation des minima sociaux ;</li>
<li>l’indexation des salaires.</li></ul>
<p><strong>NB : Détachement :</strong> l’employeur qui <a href="http://www.guichet.public.lu/entreprises/fr/ressources-humaines/mobilite/detachement/detacher-personnel/index.html" class="spip_out" rel="external">détache un salarié dans un des pays de l’union européenne</a> doit s’assurer que le salaire de ce dernier est au moins égal au salaire social minimum du pays où le salarié prestera temporairement son travail.</p>
<p>Le salaire minimum applicable dépend du niveau de <strong>qualification</strong> professionnelle du salarié. Pour être considéré comme <strong>qualifié</strong>, le salarié doit :</p>
<ul class="spip">
<li>soit disposer, pour la profession exercée, d’un <a href="http://www.guichet.public.lu/home/fr/index.html" class="spip_out" rel="external">certificat officiel reconnu</a> au moins équivalent au <strong>certificat d’aptitude technique et professionnelle</strong> (CATP) ou d’un <strong>diplôme d’aptitude professionnelle</strong> (DAP) de l’enseignement technique luxembourgeois ;</li>
<li>soit disposer d’un <strong>certificat de capacité manuelle</strong> (CCM) ou d’un <strong>certificat de capacité professionnelle</strong> (CCP) et justifier d’une pratique d’au moins deux années dans le métier concerné ;</li>
<li>soit disposer d’un <strong>certificat d’initiation technique et professionnelle</strong> (CITP) et justifie d’une <strong>pratique d’au moins cinq années</strong> dans le métier ou la profession concernés ;</li>
<li>soit, s’il ne dispose pas de certificat, justifier d’une <strong>pratique professionnelle minimale de 10 ans</strong> (lorsqu’il existe un certificat sanctionnant la qualification requise) ;</li>
<li>soit justifier d’une <strong>pratique minimale de six années</strong> dans un métier exigeant une certaine capacité technique et pour lequel la formation n’est pas sanctionnée par la délivrance d’un certificat officiel.S’il existe une <a href="http://www.guichet.public.lu/entreprises/fr/ressources-humaines/contrat-convention/convention-collective/appliquer-conventions/index.html" class="spip_out" rel="external">convention collective</a>, le salaire applicable ainsi que ses modalités d’évolution sont déterminées par la <strong>grille de salaire</strong> définie dans cette convention en fonction du <a href="http://www.guichet.public.lu/entreprises/fr/ressources-humaines/recrutement/recherche-candidat/besoins-recrutement/index.html" class="spip_out" rel="external">poste et des qualifications requises</a> pour l’occuper.</li></ul>
<h5 class="spip">Salaire social minimum</h5>
<p>Le salaire ne peut <strong>en aucun cas être inférieur</strong> au <a href="http://www.guichet.public.lu/entreprises/fr/ressources-humaines/remuneration/paiement-remunerations/salaire/index.html#panel-10" class="spip_out" rel="external">salaire social minimum</a>.</p>
<p>Le salaire social minimum applicable est :</p>
<ul class="spip">
<li>majoré de 20 % pour un <strong>salarié qualifié</strong> ;</li>
<li>diminué de 20 % à 25 % dans le cas d’un <strong>travailleur adolescent</strong>.</li></ul>
<p><strong>Salaire minimum applicable</strong> (salaire brut mensuel pour 40h/semaine)</p>
<table class="spip">
<thead><tr class="row_first"><th id="id5ca3_c0">Age et qualification</th><th id="id5ca3_c1">% du salaire social minimum</th><th id="id5ca3_c2">Montants bruts mensuels au 1er janvier 2013</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="id5ca3_c0">18 ans et plus qualifié</td>
<td headers="id5ca3_c1">120 %</td>
<td class="numeric virgule" headers="id5ca3_c2">2249,03</td></tr>
<tr class="row_even even">
<td headers="id5ca3_c0">18 ans et plus non qualifié</td>
<td headers="id5ca3_c1">100 %</td>
<td class="numeric virgule" headers="id5ca3_c2">1874,19</td></tr>
<tr class="row_odd odd">
<td headers="id5ca3_c0">17 à 18 ans</td>
<td headers="id5ca3_c1">80 %</td>
<td class="numeric virgule" headers="id5ca3_c2">1499,35</td></tr>
<tr class="row_even even">
<td headers="id5ca3_c0">15 à 17 ans</td>
<td headers="id5ca3_c1">75 %</td>
<td class="numeric virgule" headers="id5ca3_c2">1405,64</td></tr>
</tbody>
</table>
<h5 class="spip">Ajustement des minima sociaux</h5>
<p>Le salaire social minimum, les pensions, les rentes accident et le RMG peuvent être <a href="http://www.guichet.public.lu/entreprises/fr/ressources-humaines/remuneration/paiement-remunerations/salaire/index.html#panel-11" class="spip_out" rel="external">ajustés tous les deux ans</a> en fonction de l’évolution du niveau moyen des rémunérations.</p>
<p>Lorsque le niveau moyen des rémunérations a augmenté par rapport au salaire social minimum, le niveau du salaire social minimum peut être relevé pour combler partiellement ou intégralement cet écart.</p>
<h5 class="spip">Indexation des salaires</h5>
<p>Les salaires, traitements et prestations sociales (y compris le salaire social minimum), sont d’autre part <a href="http://www.guichet.public.lu/entreprises/fr/ressources-humaines/remuneration/paiement-remunerations/salaire/index.html#panel-12" class="spip_out" rel="external">indexés à l’évolution du coût de la vie</a>.</p>
<p>Lorsque l’indice des prix à la consommation augmente ou diminue de 2,5 % au cours du semestre précédent, les traitements sont en principe adaptés dans les mêmes proportions.</p>
<p>Toutefois, pour l’année 2012, l’adaptation des salaires, traitements et prestations a été reportée au 1er octobre 2012. Pour 2013 et 2014, les nouvelles adaptations éventuelles s’effectueront <strong>au moins 12 mois</strong> après l’adaptation précédente.</p>
<p>Pour les <a href="http://www.guichet.public.lu/entreprises/fr/ressources-humaines/mobilite/detachement/detacher-personnel/index.html" class="spip_out" rel="external">salariés détachés au Luxembourg</a>, l’indexation ne s’applique qu’au salaire minimum et non aux salaires d’un montant supérieur.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.guichet.public.lu/entreprises/fr/ressources-humaines/remuneration/paiement-remunerations/salaire/index.html" class="spip_out" rel="external">Guichet public - rémunerations</a></p>
<h3 class="spip"><a id="sommaire_2"></a>Contrat de travail – spécificités</h3>
<h4 class="spip">Cadre du contrat : détachement ou expatriation</h4>
<p>Deux cas de figure peuvent se présenter. Le contrat de travail peut être fait soit dans le cadre d’un détachement, soit dans le cadre d’une expatriation.</p>
<p>Dans le cadre du détachement, l’entreprise emploie un salarié en France puis le détache à l’étranger, pour une période variable mais limitée, au terme de laquelle l’employé retrouve son poste en France. Ce contrat sera soumis au droit français.</p>
<p>Dans le cadre de l’expatriation, le salarié est recruté soit en France, soit à l’étranger pour le compte d’une entreprise implantée à l’étranger ou d’une société locale. Les conditions de négociation ne seront pas les mêmes selon que l’employeur fait venir le salarié de France ou qu’il l’engage selon les modalités d’un contrat de travail local. Dans le premier cas, si les parties en décident ainsi, le contrat pourra être soumis au droit français. S’il s’agit d’un contrat local, les relations de travail seront régies par le droit local.</p>
<h4 class="spip">Règles locales applicables en matière de contrat</h4>
<p>L’<strong>employeur</strong> qui souhaite embaucher un salarié doit <strong>conclure un contrat de travail</strong>, soit avant, soit au moment même de l’entrée en service du salarié. Plusieurs types de contrats existent : le contrat à durée indéterminée, à durée déterminée, à temps partiel, saisonnier, étudiant, enfin, le contrat de stage ou d’initiation à l’emploi.</p>
<p>Le contenu du contrat doit avoir comme base trois éléments indispensables pour former un contrat de travail :</p>
<ul class="spip">
<li>la prestation d’un travail ;</li>
<li>la rémunération ou le salaire en compensation de cette activité ;</li>
<li>le lien de subordination entre l’employeur et le salarié.</li></ul>
<p>Le contrat est remis au plus tard au moment de l’entrée en activité du salarié. Il se remplit avec les indications suivantes, pour un contrat à durée indéterminée :</p>
<ul class="spip">
<li>l’<strong>identité des parties</strong> qui concluent le contrat (noms, adresses) ;</li>
<li>la <strong>date de l’entrée en service effective</strong> du salarié ;</li>
<li>le <strong>lieu de travail</strong> ;</li>
<li>le <strong>siège de l’entreprise</strong> ou, le cas échéant, le domicile de l’employeur ;</li>
<li>la <strong>nature de l’emploi</strong> occupé, et le cas échéant, la description des fonctions et des tâches assignées au salarié au moment de son engagement ;</li>
<li>la <a href="http://www.guichet.public.lu/entreprises/fr/ressources-humaines/temps-travail/gestion/index.html" class="spip_out" rel="external">durée de travail</a> journalière ou hebdomadaire normale du travailleur salarié ;</li>
<li>l’<strong>horaire</strong> normal de travail ;</li>
<li>la <a href="http://www.guichet.public.lu/entreprises/fr/ressources-humaines/remuneration/paiement-remunerations/salaire/index.html" class="spip_out" rel="external">rémunération de base et l’indice en vigueur</a> à la signature du contrat, ainsi que les compléments éventuels de salaires (gratification, 13ème mois, prime de ménage, bonus,…) ;</li>
<li>la durée du <a href="http://www.guichet.public.lu/entreprises/fr/ressources-humaines/conges/annuel/conge-annuel/index.html" class="spip_out" rel="external">congé annuel payé</a> ;</li>
<li>la <a href="http://www.guichet.public.lu/entreprises/fr/ressources-humaines/fin-relation-travail/licenciement-resiliation/licencier-preavis/index.html" class="spip_out" rel="external">durée des délais de préavis</a> à observer par l’employeur et le salarié en cas de rupture du contrat etc.</li></ul>
<p><strong>Pour en savoir plus : </strong></p>
<p><a href="http://www.guichet.lu/entreprises/fr/ressources-humaines/contrat-convention/index.html" class="spip_out" rel="external">Guichet.lu - contrat-convention</a>.</p>
<h4 class="spip"><strong>Convention collective</strong></h4>
<p>La convention collective est un accord conclu entre employeurs et salariés d’une entreprise ou d’un secteur d’activité. Elle permet d’adapter les règles du Code du travail aux besoins et spécificités d’une entreprise ou d’un secteur. Toute convention collective doit être négociée entre partenaires sociaux selon un certain formalisme et déposée auprès de l’<a href="http://www.guichet.public.lu/entreprises/fr/organismes/itm/index.html" class="spip_out" rel="external">Inspection du travail et des mines</a> (ITM) pour validation par le ministre du Travail et de l’Emploi.</p>
<p>Il existe deux types de conventions collectives de travail :</p>
<ul class="spip">
<li>les conventions collectives <strong>ordinaires</strong> négociées entre un employeur ou un groupe d’entreprises (ou leurs représentants) et une organisation syndicale ;</li>
<li>les conventions collectives <strong>déclarées d’obligation générale</strong> par un règlement grand-ducal et applicables à l’ensemble des employeurs et salariés de la profession concernée.</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Fêtes légales</h3>
<ul class="spip">
<li>Nouvel an (1er janvier) ;</li>
<li>lundi de Pâques ;</li>
<li>1er mai (fête du Travail) ;</li>
<li>Ascension ;</li>
<li>lundi de Pentecôte ;</li>
<li>Fête nationale (célébration publique de l’anniversaire du Grand-Duc, 23 juin) ;</li>
<li>Assomption (15 août) ;</li>
<li>Toussaint (1er novembre) ;</li>
<li>1er jour de Noël (25 décembre) ;</li>
<li>2ème jour de Noël (26 décembre).</li></ul>
<p>Chaque salarié <a href="http://www.itm.lu/droit-du-travail/remuneration-des-jours-feries-legaux" class="spip_out" rel="external">bénéficie de 10 jours fériés légaux</a>. Lorsqu’un jour férié tombe un jour non travaillé, l’employeur doit accorder un jour de congé compensatoire au salarié. Si les conditions spéciales de l’entreprise ne permettent pas de chômer un jour férié, l’employeur doit verser une majoration de salaire.</p>
<h3 class="spip"><a id="sommaire_4"></a>Emploi du conjoint</h3>
<p>Le Grand-Duché de Luxembourg est membre à part entière de l’Union européenne et de l’Espace économique européen. De ce fait, depuis le 1er janvier 1992, tous les ressortissants des Etats membres de l’UE et de l’EEE peuvent travailler dans n’importe quel pays membre.</p>
<p>Ainsi, les citoyens européens jouissent des mêmes droits professionnels que les citoyens luxembourgeois.</p>
<p>Pour trouver un emploi au Luxembourg, il existe des moyens similaires à ceux des autres Etats membres de l’Union européenne. Vous pouvez naturellement adresser des candidatures spontanées aux entreprises ou organismes qui vous intéressent mais aussi répondre aux offres d’emploi publiées dans la presse. Cependant, et à côté des agences intérimaires, des cabinets de recrutement et des centres d’orientation professionnelle, les services publics de l’emploi constituent des intermédiaires spécialisés susceptibles d’aider dans la recherche.</p>
<p>Les services publics de l’emploi sont administrés par l’administration de l’emploi (ADEM) placé sous l’autorité du ministre du travail et de l’emploi et sous les ordres d’un directeur, chef d’administration. Son siège est à Luxembourg, sa compétence s’étend sur tout le territoire du Grand-Duché, enfin, elle compte trois agences régionales (Esch-sur-Alzette, Diekirch et Wiltz). Parmi ses missions, on retrouve notamment la promotion de l’utilisation optimale du potentiel de travail, en coordination avec la politique économique et sociale ainsi que le recrutement des travailleurs à l’étranger.</p>
<p><a href="http://www.adem.public.lu/" class="spip_out" rel="external">Administration de l’emploi</a><br class="manualbr">10 rue Bender<br class="manualbr">L-1229 Luxembourg<br class="manualbr">Tél. : +352 2478-88 88 <br class="manualbr">Fax : +352 40 61 40<br class="autobr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/emploi-stage/article/reglementation-du-travail#info#mc#adem.public.lu#" title="info..åt..adem.public.lu" onclick="location.href=mc_lancerlien('info','adem.public.lu'); return false;" class="spip_mail">Courriel</a></p>
<h3 class="spip"><a id="sommaire_5"></a>Création d’entreprises – spécificités</h3>
<p>Voir <a href="http://www.guichet.public.lu/home/fr/index.html" class="spip_out" rel="external">Guichet public - création-développement</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/emploi-stage/article/reglementation-du-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
