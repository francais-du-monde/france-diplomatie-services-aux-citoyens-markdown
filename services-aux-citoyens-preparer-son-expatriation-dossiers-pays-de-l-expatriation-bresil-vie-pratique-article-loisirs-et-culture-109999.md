# Loisirs et culture

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/vie-pratique/article/loisirs-et-culture-109999#sommaire_1">Tourisme</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/vie-pratique/article/loisirs-et-culture-109999#sommaire_2">Activités culturelles</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/vie-pratique/article/loisirs-et-culture-109999#sommaire_3">Sports</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/vie-pratique/article/loisirs-et-culture-109999#sommaire_4">Télévision - Radio</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/vie-pratique/article/loisirs-et-culture-109999#sommaire_5">Presse française</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Tourisme</h3>
<p>Le Brésil offre une très grande variété d’attraits touristiques, qu’il s’agisse de sites naturels (plages de sable blanc, îles à la végétation tropicale, forêt amazonienne, montagne, chutes d’Iguaçu…), du patrimoine architectural des villes du nord et du Nordeste (architecture coloniale, églises baroques) ou encore du seul site de Rio.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://paris.itamaraty.gov.br/fr/" class="spip_out" rel="external">Le site de l’Ambassade du Brésil en France</a> ;</li>
<li><a href="http://www.braziltour.com/" class="spip_out" rel="external">Le portail brésilien du tourisme</a>.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Activités culturelles</h3>
<h4 class="spip">Activités culturelles en français</h4>
<p><strong>Alliance française</strong></p>
<p>Le réseau des Alliances françaises présentes sur le territoire brésilien offrent, en plus des cours de langue, de nombreuses manifestations culturelles.</p>
<p><a href="http://www.aliancafrancesabrasil.com.br/" class="spip_out" rel="external">Délégation générale de l’Alliance française au Brésil</a><br class="manualbr">Rua Uruguaiana, 55 - Salas 501 a 518 Centro<br class="manualbr">CEP 20050-090 Rio de Janeiro<br class="manualbr">Tél : (55) 21 32 99 20 00 - Fax : (55) 21 32 99 200 05<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/vie-pratique/article/loisirs-et-culture-109999#dgafbr#mc#rioaliancafrancesa.com.br#" title="dgafbr..åt..rioaliancafrancesa.com.br" onclick="location.href=mc_lancerlien('dgafbr','rioaliancafrancesa.com.br'); return false;" class="spip_mail">Courriel</a></p>
<p>L’Espace culturel <i>Le Corbusier</i> à Brasilia, la <i>Casa França-Brasil</i> à Rio de Janeiro programment de nombreuses manifestations.</p>
<h4 class="spip">Activités culturelles locales</h4>
<p>Les grandes villes disposent de nombreuses salles de cinéma, en général de bon confort. La majorité des films présentés sont américains, en version originale sous-titrée en portugais.</p>
<p>L’activité théâtrale est intense, surtout à Rio de Janeiro et Sao Paulo. Le répertoire, classique et moderne, est principalement composé d’auteurs brésiliens. Les productions, de qualité inégale, voient leur succès souvent lié à la présence d’artistes de télévision.</p>
<p>Concerts et spectacles musicaux abondent : classique, jazz, musique populaire, danse, sans oublier le carnaval. Dans les grandes villes, les nombreux musées et galeries d’art présentent des expositions de qualité.</p>
<h3 class="spip"><a id="sommaire_3"></a>Sports</h3>
<p>A l’exclusion des sports d’hiver, tous les sports, individuels et collectifs, peuvent être pratiqués au Brésil : tennis, golf, volley-ball, basket-ball, football, foot-volley, delta plane, ainsi que les sports nautiques (surf, planche à voile, natation, nautisme, plongée sous-marine). Tout l’équipement est disponible sur place. Les clubs sont nombreux, mais quelquefois chers et sélectifs. Il existe également de nombreuses salles de gymnastique et de musculation.</p>
<p>Les rencontres sportives sont fréquentes, football et Formule 1 sont les sports de prédilection des Brésiliens. La chasse est officiellement interdite au Brésil.</p>
<p>La pêche est autorisée toute l’année ; elle nécessite un permis dans la plupart des Etats. Il peut être obtenu auprès du Departamento Estadual de Proteçao de Recursos Naturais (DEPRN) à Sao Paulo, de l’Institut brésilien pour la protection de l’environnement (IBAMA) à Brasilia.</p>
<h3 class="spip"><a id="sommaire_4"></a>Télévision - Radio</h3>
<p>Les chaînes câblées, ainsi que les bouquets satellitaires Directv et Skynet des opérateurs TVA et NET diffusent des programmes et des films français. Par ailleurs TV5 est sur tous les réseaux, diffusée 24h/24.</p>
<p>Il est possible de capter RFI, relayé par la Guyane. La qualité de réception est variable selon les régions : bonne à Recife et Brasilia, mauvaise à Rio et Sao Paulo.</p>
<h3 class="spip"><a id="sommaire_5"></a>Presse française</h3>
<p>La presse française est disponible dans les grandes villes : kiosques des aéroports, des centres commerciaux et des grands hôtels.</p>
<p>On peut trouver des livres français dans les librairies suivantes : Sodiler et la FNAC à Brasilia, Nova Livraria Leonardo da Vinci à Rio de Janeiro, la Librairie française et la FNAC à Sao Paulo.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/vie-pratique/article/loisirs-et-culture-109999). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
