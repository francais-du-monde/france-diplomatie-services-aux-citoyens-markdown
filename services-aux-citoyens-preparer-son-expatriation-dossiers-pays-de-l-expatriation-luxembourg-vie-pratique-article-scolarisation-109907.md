# Scolarisation

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/vie-pratique/article/scolarisation-109907#sommaire_1">Les établissements scolaires français au Luxembourg</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/vie-pratique/article/scolarisation-109907#sommaire_2">Le système scolaire luxembourgeois</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Les établissements scolaires français au Luxembourg</h3>
<p>Pour toute information sur la scolarisation dans le système français à l’étranger, vous pouvez consulter notre thématique sur <a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures.md" class="spip_in">les études</a> et la <a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais.md" class="spip_in">scolarisation</a>.</p>
<p>Vous y trouverez des renseignements sur :</p>
<ul class="spip">
<li>les établissements français du primaire et du secondaire à l’étranger ;</li>
<li>les bourses scolaires et la prise en charge des frais de scolarité à l’étranger ;</li>
<li>les possibilités qui s’offrent à vous si votre enfant ne peut être scolarisé à l’étranger dans le système français (enseignement à distance par le CNED, programme français langue maternelle (FLAM), internats en France)</li>
<li>les épreuves du baccalauréat à l’étranger ;</li>
<li>les bourses d’études supérieures en France et à l’étranger ;</li>
<li>l’équivalence des diplômes.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Le système scolaire luxembourgeois</h3>
<h4 class="spip">L’éducation précoce</h4>
<p>Elle est destinée aux enfants de t182rois ans et sa <strong>fréquentation est facultative</strong>.</p>
<p>Son objectif est de faciliter l’intégration sociale et scolaire de l’enfant y compris ceux d’origine étrangère tout en se familiarisant avec la langue et la culture luxembourgeoises.</p>
<p>Chaque groupe est encadré par un(e) instituteur(trice) de l’éducation précoce (fonctionnaires de l’Etat) ainsi qu’un(e) éducateur(trice). Les activités se déclinent entre autres sous forme de discussions, de jeux de rôle et d’imitation, des sorties et des fêtes en classe.</p>
<h4 class="spip">L’éducation préscolaire</h4>
<p>Elle est destinée aux enfants âgés de 4 à 6 ans. Contrairement à l’éducation précoce, <strong>l’éducation préscolaire est obligatoire</strong>.</p>
<p>Elle comprend deux années. Poursuivant toujours son objectif d’intégration sociale et scolaire, l’éducation préscolaire prépare au développement mental, cognitif, langagier (apprentissage de la langue luxembourgeoise), créateur et moteur des enfants. Elle intègre également des activités spécifiques préparant à l’alphabétisation en allemand ainsi qu’à l’apprentissage des mathématiques.</p>
<p>Chaque groupe est encadré par des instituteurs(trices) de l’éducation préscolaire (fonctionnaires de l’Etat).</p>
<h4 class="spip">L’enseignement primaire</h4>
<p>L’éducation primaire a pour objectif de développer les habiletés et les comportements de l’enfant en lui faisant acquérir les compétences et les connaissances fondamentales indispensables à toute formation ultérieure.</p>
<p>L’enseignement comporte 6 années regroupées en trois niveaux :</p>
<ul class="spip">
<li>le degré inférieur ;</li>
<li>le degré moyen ;</li>
<li>le degré supérieur.</li></ul>
<p>Les instituteurs(trices) enseignent la langue allemande, française (2ème année) et luxembourgeoise mais aussi les mathématiques, l’éveil aux sciences, l’histoire et la géographie, l’éducation artistique et musicale, etc… selon le plan d’études défini tous les ans par le ministère de l’Education nationale.</p>
<p>Pour les élèves ayant acquis les connaissances et les compétences de base de l’enseignement primaire, un certificat de fin d’études primaires est délivré.</p>
<h4 class="spip">L’enseignement post-primaire</h4>
<p>Il est dispensé dans les lycées et comporte deux ordres d’affectation selon l’avis délivré par le conseil d’orientation en fin de 6ème année :</p>
<p><strong>L’enseignement secondaire </strong>(formation générale qui prépare aux études supérieures et universitaires) : sept années réparties en deux divisions (la division inférieure en trois ans : classe de 7ème, 6ème et 5ème ; la division supérieure en quatre ans : classe polyvalente de 4ème, cycle de spécialisation : 3ème, 2ème et 1ère) ;</p>
<p><strong>L’enseignement secondaire technique </strong>prépare à la vie professionnelle mais aussi à l’enseignement supérieur. Il varie entre six et huit ans selon les régimes d’études et les degrés de spécialisation (le régime technique, le régime de la formation de technicien et le régime professionnel).</p>
<h4 class="spip">Enseignement supérieur</h4>
<p>Les formations supérieures ou post-secondaires au Luxembourg ne sont pas dispensées dans toutes les disciplines. Certains étudiants luxembourgeois poursuivent leurs études à l’étranger, notamment à partir du 2ème cycle. Il existe des possibilités d’études supérieures et un premier cycle complet au Centre universitaire de Luxembourg. Presque toutes les disciplines y sont enseignées.</p>
<p>En règle générale, le français est la langue d’enseignement dans les instituts d’enseignement supérieur. Toutefois, les langues allemande et anglaise peuvent figurer au programme d’un certain nombre de formations. Pour les études de technologie et d’ingénierie, l’allemand est la langue dominante de l’enseignement.</p>
<p>L’Université de Luxembourg comporte trois unités de recherche au sein de trois facultés :</p>
<ul class="spip">
<li>la faculté des sciences, de la technologie et de la communication ;</li>
<li>la faculté des lettres, des sciences humaines, des arts et des sciences de l’éducation ;</li>
<li>la faculté de droit, d’économie et de finance.</li></ul>
<p>Elles sont réparties sur trois campus :</p>
<p><strong>Campus Kirchberg</strong><br class="manualbr">6, rue Richard Coudenhove-Kalergi <br class="manualbr">L-1359 Luxembourg<br class="manualbr">Tél. : (+352) 46 66 44 5000</p>
<p><strong>Campus Walferdange</strong><br class="manualbr">Route de Diekirch - BP2 <br class="manualbr">L-7220 Walferdange<br class="manualbr">Tél. : (+352) 46 66 44 9000</p>
<p>Campus Limpertsberg-&gt;<a href="http://www.uni.lu/" class="spip_url spip_out auto" rel="nofollow external">http://www.uni.lu/</a>]<br class="manualbr">162A avenue de la Faïencerie <br class="manualbr">L-1511 Luxembourg<br class="manualbr">Tél : (+352) 46 66 44 6000 / 6611 <br class="manualbr">Fax : (+352) 46 66 44 6760<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/vie-pratique/article/scolarisation-109907#seve.infos#mc#uni.lu#" title="seve.infos..åt..uni.lu" onclick="location.href=mc_lancerlien('seve.infos','uni.lu'); return false;" class="spip_mail">Courriel</a></p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.guichet.public.lu/citoyens/fr/enseignement-formation/index.html" class="spip_out" rel="external">Guichet public - enseignement-formation</a></li>
<li><a href="http://www.institutfrancais-luxembourg.lu/" class="spip_out" rel="external">Institut français du Luxembourg.lu</a></li>
<li><a href="http://www.men.public.lu/" class="spip_out" rel="external">Le site du ministère de l’Education nationale et de la Formation professionnelle</a></li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/vie-pratique/article/scolarisation-109907). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
