# Vaccination

<p>Il est toutefois recommandé d’effectuer les vaccinations suivantes ou de s’assurer qu’elles sont à jour :</p>
<ul class="spip">
<li>Hépatite virale B,</li>
<li>Diphtérie,</li>
<li>Tétanos,</li>
<li>Poliomyélite,</li>
<li>Typhoïde,</li>
<li>Hépatite A</li></ul>
<p>A partir de l’âge de 50 ans, une recherche préalable des anticorps sériques (Ig G) est justifiée.</p>
<p>Les autorités sanitaires australiennes recommandent en général un programme d’immunisation des jeunes enfants contre la diphtérie, le tétanos, la poliomyélite, ainsi que contre la rougeole et les oreillons quand ils ont entre 12 et 15 mois. Les enfants doivent être à jour des vaccinations du calendrier vaccinal français, en particulier pour les longs séjours : BCG dès le premier mois, rougeole dès l’âge de 9 mois.</p>
<p>Une autre différence avec la France concerne l’espacement des vaccinations, plus long en Australie (notamment les toutes premières vaccinations du nourrisson) et celle concernant la vaccination contre la varicelle qui est "obligatoire" ici alors qu’elle ne se pratique pas en France.</p>
<p>Pour plus d’informations sur le programme vaccinal australien : <a href="http://www.immunise.health.gov.au/" class="spip_out" rel="external">http://www.immunise.health.gov.au/</a></p>
<p>Pour en savoir plus, lisez <a href="services-aux-citoyens-preparer-son-expatriation-sante.md" class="spip_in">notre article thématique sur la vaccination</a>.</p>
<p><i>Mise à jour : octobre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/entree-et-sejour/article/vaccination-108656). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
