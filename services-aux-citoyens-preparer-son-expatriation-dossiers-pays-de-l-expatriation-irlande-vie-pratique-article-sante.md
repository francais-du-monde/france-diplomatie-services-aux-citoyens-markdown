# Santé

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/vie-pratique/article/sante#sommaire_1">Précautions à prendre</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/vie-pratique/article/sante#sommaire_2">Système de santé publique et accès aux soins</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Précautions à prendre</h3>
<p>Sur le plan médical, aucune vaccination n’est exigée pour l’entrée sur le territoire irlandais. Il convient toutefois d’être à jour dans ses vaccins (diphtérie, poliomyélite et tétanos). Pour les enfants, il est important de surveiller le calendrier vaccinal.</p>
<p>L’avortement est illégal en Irlande.</p>
<p>S’agissant du SIDA, il est rappelé que le risque existe partout dans le monde et que les mesures de prévention doivent être observées où que l’on soit.</p>
<h3 class="spip"><a id="sommaire_2"></a>Système de santé publique et accès aux soins</h3>
<p>Que vous soyez assuré ou pas en Irlande (pays de résidence), vous avez droit à toutes les prestations en nature proposées en application de la législation de ce pays. Ces prestations sont servies pas l’institution d’assurance maladie de votre lieu de résidence dans les mêmes conditions que pour toutes les autres personnes assurées dans ce pays.</p>
<p>En Irlande, les services de la santé sont gérés au niveau local par huit administrations sanitaires régionales.</p>
<p>Le secteur de la santé se caractérise par la coexistence de deux systèmes parallèles et inégaux :</p>
<ul class="spip">
<li>un secteur public qui s’adresse aux personnes à revenus modestes et comporte de nombreuses carences ;</li>
<li>un secteur privé offrant une meilleure qualité de prise en charge médicale pour un coût nettement plus élevé.</li></ul>
<p>Compte tenu des insuffisances du système public (gratuité des soins réservée à quelques catégories de personnes, listes d’attente pour l’accès aux traitements médicaux et chirurgicaux, absence de certaines spécialités, etc.), il est préférable, en cas d’installation durable en Irlande, de souscrire à une assurance médicale privée. Les cotisations sont parfois élevées, mais elles peuvent dans certains cas faire l’objet d’une prise en charge partielle ou totale par l’employeur.</p>
<p>Il est possible d’obtenir tous les renseignements sur l’accès aux soins auprès du centre de Dublin.</p>
<p><strong>Eastern Regional Health Authority</strong><br class="manualbr">Customer Services Department<br class="manualbr">Dr. Steevens’ Hospital - Dublin 8<br class="manualbr">Tél. (Locall) : 1 800 520 520<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/vie-pratique/article/sante#customer.services#mc#erha.ie#" title="customer.services..åt..erha.ie" onclick="location.href=mc_lancerlien('customer.services','erha.ie'); return false;" class="spip_mail">Courriel</a></p>
<p>Il est vivement conseillé de consulter son médecin traitant avant le départ et de souscrire à une assurance rapatriement.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.hse.ie/eng" class="spip_out" rel="external">Site du service de la santé en Irlande</a> ;</li>
<li><a href="http://www.ambafrance-ie.org/Systeme-de-sante-irlandais" class="spip_out" rel="external">Article sur le système de soin Irlandais sur le site de l’Ambassade de France à Dublin</a>.</li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/vie-pratique/article/sante). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
