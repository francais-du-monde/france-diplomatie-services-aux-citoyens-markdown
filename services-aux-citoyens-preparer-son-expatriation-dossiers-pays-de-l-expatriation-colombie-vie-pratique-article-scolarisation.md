# Scolarisation

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/vie-pratique/article/scolarisation#sommaire_1">Les établissements scolaires français en Colombie</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/vie-pratique/article/scolarisation#sommaire_2">Enseignement supérieur</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/vie-pratique/article/scolarisation#sommaire_3">Pour en savoir plus : </a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Les établissements scolaires français en Colombie</h3>
<p>Plusieurs établissements dispensent un enseignement en français en Colombie : <a href="http://www.lfbogota.com/" class="spip_out" rel="external">Lycée français Louis Pasteur à Bogota</a>, Lycée français de Pereira et le <a href="http://lfcali.edu.co/" class="spip_out" rel="external">Lycée français Paul Valéry</a>. Ces trois établissements couvrent toutes les sections, depuis l’école maternelle jusqu’au baccalauréat.</p>
<p>Pour toute information sur la scolarisation dans le système français à l’étranger, vous pouvez également consulter nos rubriques thématiques : <a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures.md" class="spip_in">Etudes supérieures</a> et <a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais.md" class="spip_in">Scolarité en français</a>.</p>
<p>Vous y trouverez des renseignements sur :</p>
<ul class="spip">
<li>les établissements français du primaire et du secondaire à l’étranger ;</li>
<li>les bourses scolaires et la prise en charge des frais de scolarité à l’étranger ;</li>
<li>les possibilités qui s’offrent à vous si votre enfant ne peut être scolarisé à l’étranger dans le système français (enseignement à distance par le CNED, programme français langue maternelle (FLAM), internats en France) ;</li>
<li>les épreuves du baccalauréat à l’étranger ;</li>
<li>les bourses d’études supérieures en France et à l’étranger ;</li>
<li>l’équivalence des diplômes.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Enseignement supérieur</h3>
<p>L’enseignement supérieur en Colombie comprend deux étapes :</p>
<ul class="spip">
<li><strong>"le Pregrado"</strong>, où on peut choisir entre : l’éducation technique professionnelle, l’éducation technologique et la formation professionnelle universitaire.</li>
<li><strong>le "Posgrado"</strong>, où on retrouve : les spécialisations, les masters d’approfondissement ou de recherche, les doctorats et post-doctorats.</li></ul>
<p>La particularité des universités colombiennes repose dans leur fort degré d’autonomie qui leur permet de créer des formations universitaires à la carte et répondant à leur profil et à leur exigence d’internationalisation.</p>
<p>Il existe actuellement plus de 90 programmes de doubles diplômes ainsi que de nombreux programmes de coopération avec des établissements d’enseignement supérieur français. A titre d’exemple un programme d’échange pour les jeunes ingénieurs a été mis en place depuis janvier 2011. D’autres programmes de ce type, notamment dans les domaines de l’agriculture et de la médecine, devraient rapidement voir le jour. Ces programmes sont articulés avec la création de filières francophones introduites dans 16 universités colombiennes.</p>
<p><strong>A noter que depuis le 1er févier 2010 la France et la Colombie ont signée une convention de reconnaissance mutuelle des diplômes universitaires.</strong></p>
<h3 class="spip"><a id="sommaire_3"></a>Pour en savoir plus : </h3>
<ul class="spip">
<li><a href="http://www.aefe.fr/" class="spip_out" rel="external">Agence pour l’enseignement français à l’étranger</a></li>
<li><a href="http://www.ambafrance-co.org/" class="spip_out" rel="external">Ambassade de France en Colombie, rubrique Présence française</a></li>
<li><a href="http://www.mineducacion.gov.co/1621/w3-propertyvalue-43808.html" class="spip_out" rel="external">Ministère colombien de l’Éducation nationale</a></li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/vie-pratique/article/scolarisation). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
