# La reconstitution d’actes d’état civil

<p>Le Service central d’état civil ne dispose pas de la totalité des registres établis dans les États anciennement soumis à la souveraineté française.</p>
<p>Afin que les personnes concernées puissent disposer d’actes de naissance et de mariage et en obtenir des copies et extraits, le Service central d’état civil est habilité, en application des dispositions de la <a href="http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000692958fastPos=1fastReqId=191820324categorieLien=cidoldAction=rechTexte" class="spip_out" rel="external">loi 68-671 du 25 juillet 1968</a> à reconstituer les actes manquants. La reconstitution permet également de mettre à jour ces actes par l’apposition de mentions.</p>
<p>L’article 4 de la loi dispose que les actes « sont établis, soit par reproduction des registres originaux, soit au vu de copies ou extraits d’actes de l’état civil, soit, à défaut, au vu de tous documents judiciaires ou administratifs ou même sur des déclarations de témoins recueillis sans frais par le juge d’instance. »</p>
<p>Les demandes doivent ainsi être accompagnées d’un certain nombre de justificatifs tels que :</p>
<ul class="spip">
<li>une photocopie de la carte d’identité ou un certificat de nationalité française délivré par le tribunal d’instance.</li>
<li>une copie ou un extrait, même de date ancienne, de l’acte à reconstituer ou, à défaut, un acte de notoriété délivré, sans frais, par le tribunal d’instance.</li>
<li>le cas échéant, une photocopie du livret de famille et une copie de l’acte de mariage délivrée par la mairie (s’il a été célébré en France).</li>
<li>tout document judiciaire ou administratif concernant l’état civil (adoption, divorce…)</li>
<li>dans la mesure du possible, tous documents indiquant les nom, prénom(s), date et lieu de naissance des parents (copies d’actes de naissance ou de mariage, livrets de famille, etc.).D’une manière générale, les personnes concernées peuvent solliciter le Service central d’état civil pour la délivrance d’une copie ou d’un extrait de leur acte de naissance ou de mariage. Si le Service ne détient pas l’acte demandé dans ses registres, il leur indiquera la marche à suivre et les justificatifs nécessaires pour procéder à l’établissement de l’acte demandé.</li></ul>
<p><i>Mise à jour : mars 2016</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/etat-civil/article/reconstitution-d-actes-d-etat). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
