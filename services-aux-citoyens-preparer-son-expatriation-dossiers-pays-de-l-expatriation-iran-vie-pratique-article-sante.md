# Santé

<p>Pour une présentation des risques épidémiologiques ainsi que des conseils sur la médecine au quotidien, consultez les pages suivantes :</p>
<ul class="spip">
<li>Conseils, liste de médecins et hôpitaux sur le site de l’Ambassade de France en Iran <a href="http://www.ambafrance-ir.org" class="spip_url spip_out auto" rel="nofollow external">www.ambafrance-ir.org</a></li>
<li>Page dédiée à la santé en Iran sur le site Conseils aux voyageurs</li>
<li>Fiche Téhéran sur le site du <a href="http://ww.cimed.org" class="spip_out" rel="external">CIMED</a>.</li></ul>
<p><i>Mise à jour : février 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/vie-pratique/article/sante). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
