# Déclarer la naissance d’un enfant à l’étranger

<p class="chapo">
      Dans nombre de pays, la législation locale oblige les ressortissants étrangers à déclarer les naissances à l’officier de l’état civil local.
</p>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/etat-civil/article/les-naissances-a-l-etranger#sommaire_1">La déclaration ou la transcription</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/etat-civil/article/les-naissances-a-l-etranger#sommaire_2">Le cas particulier de l’Algérie, du Maroc et de la Tunisie</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/etat-civil/article/les-naissances-a-l-etranger#sommaire_3">La reconnaissance</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/etat-civil/article/les-naissances-a-l-etranger#sommaire_4">Le choix de nom</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/etat-civil/article/les-naissances-a-l-etranger#sommaire_5">La reconnaissance d’un jugement d’adoption étranger</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/etat-civil/article/les-naissances-a-l-etranger#sommaire_6">Délivrance ou mise à jour du livret de famille</a></li></ul>
<p>Cette procédure n’est pas obligatoire mais elle est indispensable pour obtenir un acte de naissance français.</p>
<h3 class="spip"><a id="sommaire_1"></a>La déclaration ou la transcription</h3>
<p>Pour faire enregistrer la naissance de votre enfant dans l’état civil français, deux procédures sont possibles :</p>
<p><strong>La déclaration de la naissance à l’ambassade ou au consulat de votre pays de résidence</strong></p>
<p>Dans les pays où que la loi locale ne s’y oppose pas, la déclaration de naissance peut être reçue par l’officier de l’état civil consulaire territorialement compétent au même titre que par les services de l’état civil du pays de résidence.</p>
<p>Dans ce cas, l’ambassade ou le consulat vont aussitôt établir l’acte de naissance et le conserver dans leurs registres. La déclaration doit être faite dans les quinze jours de l’accouchement ; ce délai est porté à 30 jours hors d’Europe et, en Europe, dans les pays suivants : Albanie, Arménie, Azerbaïdjan, Biélorussie, Bosnie-Herzégovine, Croatie, Espagne, Estonie, Finlande, Géorgie, Grèce, Kazakhstan, Kirghizstan, Kosovo, Lettonie, Lituanie, Macédoine, Moldavie, Monténégro, Norvège, Ouzbékistan, Pologne, Portugal, République tchèque, Roumanie, Russie, Serbie, Slovaquie, Slovénie, Suède, Tadjikistan, Turkménistan, Turquie, Ukraine.</p>
<p><strong>La transcription par l’officier d’état civil consulaire de l’acte de naissance local</strong></p>
<p>Dans les pays où la législation oblige les ressortissants étrangers à déclarer les naissances à l’officier de l’état civil local, les parents auront recours à la transcription de l’acte de naissance local par l’officier d’état civil de l’ambassade ou du consulat.</p>
<p>La demande de transcription d’un acte de naissance doit être accompagnée :</p>
<ul class="spip">
<li>de la copie de l’acte de naissance étranger et sa traduction ;</li>
<li>d’un justificatif de nationalité française pour l’un des parents au moins ;</li>
<li>du livret de famille pour mise à jour ;</li>
<li>pour les enfants nés hors mariage, d’une copie de l’acte relatif à la reconnaissance souscrite par le père, lorsque ce dernier est français ;</li>
<li>l’ambassade ou le consulat peut demander des documents supplémentaires selon le contexte.</li></ul>
<p>Nous vous recommandons de prendre l’attache de la <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/article/annuaire-des-representations-francaises-a-l-etranger" class="spip_in">représentation diplomatique ou consulaire de votre pays de résidence</a> pour faire les vérifications nécessaires préalables à la naissance.</p>
<h3 class="spip"><a id="sommaire_2"></a>Le cas particulier de l’Algérie, du Maroc et de la Tunisie</h3>
<p>Les demandes de transcription pour les actes dressés en Algérie, au Maroc ou en Tunisie doivent être envoyées uniquement par courrier postal au Bureau des transcriptions pour le Maghreb (BTM) du Service central d’état civil à l’adresse suivante :</p>
<p>Service central d’état civil<br class="manualbr">BTM<br class="manualbr">11 rue de la Maison Blanche<br class="manualbr">44941 NANTES CEDEX 9</p>
<p>La liste des pièces à fournir ainsi que le formulaire de demande de transcription sont à télécharger sur le site du <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/article/annuaire-des-representations-francaises-a-l-etranger" class="spip_in">Consulat général de France compétent dans le pays concerné</a>.</p>
<p>Tout dépôt de dossier ou demande se fait exclusivement par voie postale à l’adresse ci-dessus. Aucune demande de renseignement ne peut être effectuée par téléphone mais le Bureau de transcription du Maghreb peut être joint <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/etat-civil/article/les-naissances-a-l-etranger#bta.scec#mc#diplomatie.gouv.fr#" title="bta.scec..åt..diplomatie.gouv.fr" onclick="location.href=mc_lancerlien('bta.scec','diplomatie.gouv.fr'); return false;" class="spip_mail">par courriel</a>.</p>
<h3 class="spip"><a id="sommaire_3"></a>La reconnaissance</h3>
<p>Si votre enfant est né hors mariage, <a href="https://www.service-public.fr/particuliers/vosdroits/F887" class="spip_out" rel="external">la filiation s’établit différemment à l’égard du père et de la mère</a>. Elle peut, par ailleurs, intervenir avant la naissance, au moment de la déclaration de naissance où après celle-ci.</p>
<p>Renseignez-vous auprès de la <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/article/annuaire-des-representations-francaises-a-l-etranger" class="spip_in">représentation française de votre pays de résidence</a> pour déterminer si une reconnaissance par le père doit être souscrite en sa faveur et, éventuellement, par la mère lorsqu’elle est étrangère.</p>
<h3 class="spip"><a id="sommaire_4"></a>Le choix de nom</h3>
<p>La <a href="https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000593399&amp;dateTexte=&amp;categorieLien=id" class="spip_out" rel="external">loi n°2002-304</a> du 4 mars 2002 modifiée par la <a href="https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000778913" class="spip_out" rel="external">loi n°2003-516</a> du 18 juin 2003 a changé en profondeur le dispositif en matière de dévolution du nom de famille.</p>
<p><a href="https://www.service-public.fr/particuliers/vosdroits/N151" class="spip_out" rel="external">En vertu de la nouvelle procédure</a>, les parents peuvent choisir par déclaration conjointe pour leur premier enfant commun (légitime ou naturel) né à compter du 1er janvier 2005, soit le nom du père, soit le nom de la mère, soit leurs deux noms accolés dans l’ordre choisi par eux dans la limite d’un nom de famille pour chacun d’eux.</p>
<p>En l’absence de déclaration de choix de nom, c’est le nom du père qui est transmis. Le nom choisi pour le premier enfant est irrévocable et vaut pour les enfants communs suivants.</p>
<p>La demande de choix de nom doit être présentée au moment de la déclaration de naissance ou de la demande de transcription d’un acte de naissance local.</p>
<h3 class="spip"><a id="sommaire_5"></a>La reconnaissance d’un jugement d’adoption étranger</h3>
<p><a href="http://www.diplomatie.gouv.fr/fr/adopter-a-l-etranger/comment-adopter-a-l-etranger/les-fiches-pratiques-pour-adopter-a-l-etranger/article/nationalite-de-l-enfant-adopte-14520" class="spip_in">L’attribution de la nationalité française</a> dépend du type d’adoption poursuivi. L’adoption simple ne confère pas la nationalité française de plein droit à l’adopté.</p>
<p>La demande de <a href="http://www.diplomatie.gouv.fr/fr/adopter-a-l-etranger/comment-adopter-a-l-etranger/les-fiches-pratiques-pour-adopter-a-l-etranger/article/transcription-d-un-jugement-d-adoption" class="spip_in">vérification d’opposabilité d’une décision étrangère d’adoption</a> relève du Procureur de la République du Tribunal de Grande Instance de Nantes. Après instruction du dossier et reconnaissance d’opposabilité par le Procureur, le Service Central de l’État Civil procèdera à l’établissement de l’acte de naissance français de l’enfant et complètera le livret de famille des parents.</p>
<h3 class="spip"><a id="sommaire_6"></a>Délivrance ou mise à jour du livret de famille</h3>
<p>Le livret de famille est délivré, selon les situations, par l’officier de l’état civil qui célèbre le mariage ou par celui qui dresse l’acte de naissance du premier enfant.</p>
<p>Dans le premier cas, la mise à jour du livret de famille ne peut intervenir qu’après que l’acte de naissance ou sa transcription aient été enregistrés par l’officier d’état civil compétent.</p>
<p>Pour les événements d’état civil survenus à l’étranger, cette mise à jour peut être assurée par le Service central d’état civil ou par l’ambassade ou le consulat qui détient l’acte dans ses registres.</p>
<p>Pour en savoir plus sur le livret de famille :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="services-aux-citoyens-etat-civil-et-nationalite-francaise-etat-civil-article-livrets-de-famille.md" class="spip_in">Actualiser son livret de famille </a></p>
<p><i>Mise à jour : mars 2016</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/etat-civil/article/les-naissances-a-l-etranger). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
