# Fiscalité du pays

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/fiscalite/article/fiscalite-du-pays#sommaire_1">Année fiscale </a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/fiscalite/article/fiscalite-du-pays#sommaire_2">Barème de l’impôt </a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/fiscalite/article/fiscalite-du-pays#sommaire_3">Quitus fiscal </a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Année fiscale </h3>
<p>L’impôt est retenu à la source et payé mensuellement par l’employeur. Deux types de formulaire de déclaration d’impôts existent, l’un pour l’employé, l’autre pour l’employeur. Les déclarations doivent être faites en février-mars.</p>
<h3 class="spip"><a id="sommaire_2"></a>Barème de l’impôt </h3>
<h4 class="spip">L’impôt sur le revenu applicable aux personnes résidentes physiques de Roumanie</h4>
<p>Le revenu annuel soumis à l’impôt sur le revenu comprend principalement :</p>
<ul class="spip">
<li>les salaires ;</li>
<li>les revenus d’activités indépendantes ;</li>
<li>les revenus obtenus suite à la cession de l’usage de biens ;</li>
<li>les revenus obtenus grâce à des investissements ;</li>
<li>les pensions ;</li>
<li>les revenus d’activités agricoles.</li></ul>
<p>Le code fiscal roumain prévoit un taux unique de 16% sur le revenu.</p>
<p>Exceptions :</p>
<ul class="spip">
<li>Un taux de 25% pour les revenu supérieurs à 600 lei et provenant des jeux de hasard ;</li>
<li>Déductions personnelles en montants fixes pour les salaires bruts des personnes physiques inférieurs à 3000 lei ;</li>
<li>Les transactions immobilières :
<br>— Si l’immeuble a été obtenu à une date inférieure à trois ans par rapport au moment de la vente, taux de 3% jusqu’à la valeur de 200 000 lei ; si la valeur de l’immeuble dépasse 200 000 lei, 6000 lei + 2% sur la valeur qui dépasse 200 000 lei
<br>— Si l’immeuble a été obtenu à une date supérieure à trois ans par rapport au moment de la vente, taux de 2% jusqu’à la valeur de 200 000 lei ; si la valeur de l’immeuble dépasse 200 000 lei, 4000 lei + 1% sur la valeur qui dépasse 200 000 lei</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Quitus fiscal </h3>
<p>Un quitus fiscal est exigé avant de quitter le pays.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/fiscalite/article/fiscalite-du-pays). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
