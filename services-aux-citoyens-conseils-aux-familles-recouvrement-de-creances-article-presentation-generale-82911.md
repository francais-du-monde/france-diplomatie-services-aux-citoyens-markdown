# Présentation générale

<p class="chapo">
      Avertissement : les informations publiées sont données sous réserve des modifications législatives et réglementaires pouvant intervenir en France et à l’étranger.
</p>
<p><strong>Pour rappel</strong>, le ministère des Affaires étrangères <strong>n’est pas compétent pour connaître des dossiers de recouvrement de créance alimentaire lorsque le créancier et le débiteur d’aliments résident en France</strong> ; dans ce cas, les autorités judiciaires françaises (tribunaux, huissiers de justice, avocats) doivent être saisies.</p>
<p>L’augmentation du nombre de couples séparés et la grande mobilité des personnes impliquent la multiplication des litiges transfrontaliers en matière de créances alimentaires.</p>
<p>En effet, l’éclatement des cellules familiales au-delà des frontières rend complexe le recouvrement d’une obligation alimentaire fixée par une décision de justice lorsque le créancier et le débiteur d’aliments résident dans deux pays distincts, quelle que soit leur nationalité.</p>
<p>Dans ce cas, se posent des difficultés d’ordre pratique et financier : localisation du débiteur, constitution du dossier, choix d’un avocat à l’étranger, obstacle de la distance et de la langue.</p>
<p>La France a adhéré à divers instruments internationaux et communautaires en matière d’aliments (convention de New York du 20 juin 1956, convention de La Haye du 23 novembre 2007, règlement (CE) n°4/2009 du 18 décembre 2008), et désigné pour leur application en qualité d’autorité centrale le :</p>
<blockquote class="texteencadre-spip spip"><br class="autobr">
<p class="spip" style="text-align:center;"><br class="autobr">
<strong>Ministère des Affaires étrangères </strong> <br class="manualbr">Direction des Français à l’étranger et de l’administration consulaire<br class="manualbr">Service des conventions, des affaires civiles et de l’entraide judiciaire<br class="manualbr">Sous-direction de la Protection des droits des personnes<br class="manualbr">Bureau du recouvrement des créances alimentaires (RCA)<br class="manualbr">27, rue de la convention – CS 91 533<br class="manualbr">75 732 PARIS Cedex 15<br class="manualbr">01 43 17 90 01<br class="autobr">
</p>
<p><br class="autobr"></p>
</blockquote>
<p>Le bureau du recouvrement des créances alimentaires (RCA) de la sous-direction de la protection des droits des personnes de la direction des Français à l’étranger et de l’administration consulaire est l’autorité centrale française compétente pour mettre en œuvre <a href="services-aux-citoyens-conseils-aux-familles-recouvrement-de-creances-article-la-procedure-de-recouvrement-de.md" class="spip_in">la procédure de recouvrement de créances alimentaires à l’étranger</a>.</p>
<p>Si vous êtes une <a href="services-aux-citoyens-conseils-aux-familles-recouvrement-de-creances-article-personne-creanciere.md" class="spip_in">personne créancière</a> d’une <a href="services-aux-citoyens-conseils-aux-familles-recouvrement-de-creances-article-l-obligation-alimentaire.md" class="spip_in">obligation alimentaire</a> et que vous souhaitez poursuivre l’action en recouvrement de votre créance à l’étranger, le bureau RCA vous oriente pour mener la procédure de recouvrement selon votre situation.</p>
<p>Si vous êtes une <a href="services-aux-citoyens-conseils-aux-familles-recouvrement-de-creances-article-personne-debitrice.md" class="spip_in">personne débitrice</a> d’une obligation alimentaire, vous êtes tenu au paiement de votre dette d’aliments ; le bureau RCA peut vous soutenir dans vos différentes démarches.</p>
<p>Ce même bureau peut également vous fournir des informations utiles en lien avec le recouvrement des créances alimentaires, notamment la possibilité de bénéficier d’une <a href="services-aux-citoyens-conseils-aux-familles-recouvrement-de-creances-article-l-aide-judiciaire.md" class="spip_in">aide juridictionnelle</a>.</p>
<p><i>Mise à jour : juin 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/conseils-aux-familles/recouvrement-de-creances/article/presentation-generale-82911). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
