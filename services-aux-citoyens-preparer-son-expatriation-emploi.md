# Emploi

<p>Quels organismes contacter lorsque l’on cherche du travail à l’étranger ? Quelles sont les conditions à remplir pour postuler aux différents stages et emplois que proposent les organismes gouvernementaux et non gouvernementaux ? Que faut-il savoir sur les volontariats à l’international et les « programmes vacances travail » ?</p>
<p>Vous trouverez, dans ces rubriques, des conseils et des contacts qui vous aideront à réaliser votre projet professionnel à l’étranger.</p>
<p class="spip_document_84251 spip_documents spip_documents_center">
<img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L1000xH190/VIE-EAU_2-1000x190_cle8af464-e3517.jpg" width="1000" height="190" alt=""></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-emploi-article-programme-vacances-travail.md" title="Programme Vacances-Travail">Programme Vacances-Travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-emploi-article-stage-117331.md" title="Stage">Stage</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-emploi-article-rechercher-un-emploi-a-l-etranger-117334.md" title="Rechercher un emploi à l’étranger">Rechercher un emploi à l’étranger</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-emploi-article-volontariat.md" title="Volontariat">Volontariat</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/emploi/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
