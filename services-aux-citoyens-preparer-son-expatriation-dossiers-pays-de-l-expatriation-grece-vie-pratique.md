# Vie pratique

<h2 class="rub22882">Logement</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/vie-pratique/#sommaire_1">Où se loger ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/vie-pratique/#sommaire_2">Conditions de location</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/vie-pratique/#sommaire_3">Hôtels</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/vie-pratique/#sommaire_4">Electricité</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/vie-pratique/#sommaire_5">Electroménager</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Où se loger ?</h3>
<p>Il n’y a pas à proprement parler d’agences immobilières telles que nous les connaissons en France : les agents immobiliers travaillent par annonces dans la presse. A Athènes, le centre-ville manque de logements modernes. La chaleur, alliée à la pollution, y rend en outre le séjour assez fatiguant. Aussi les français préfèrent-ils s’éloigner à des distances de l’ordre de 19 à 25 kms du centre-ville.</p>
<h4 class="spip">Acheter en Grèce</h4>
<p>L’acquisition en Grèce d’un bien immobilier se révèle souvent être un véritable parcours du combattant.</p>
<p>Que votre choix se porte sur un terrain à construire ou sur une construction existante il conviendra de prendre un certain nombre de précautions.</p>
<p>Un citoyen communautaire peut acquérir sans restriction un bien immobilier en Grèce. Il devra au préalable obtenir un numéro d’immatriculation fiscale de non résident (AFM). D’une manière pratique nous recommandons également l’ouverture d’un compte bancaire en Grèce au nom de l’acheteur, par lequel transitera le prix de la vente. Cette précaution pourra faciliter par la suite les démarches fiscales.</p>
<h5 class="spip">Comment rechercher</h5>
<p>Dans un premier temps, il est possible de se faire une idée du marché en consultant les petites annonces disponible dans <strong>la presse spécialisée</strong>. Il est notable que la majorité des sources peuvent aujourd’hui être directement consultées sur la toile en langue grecque.</p>
<p>La profession <strong>d’agent immobilier</strong> s’est beaucoup développée ces dernières années en voyant notamment l’implantation en Grèce de professionnels souvent bilingues. Recourir à un professionnel bien implanté dans la région concernée pourra être un atout. L’intervention d’un agent se fait sur la base d’un mandat de recherche. Il est important d’en comprendre le détail. D’une manière générale, la pratique veut que l’agent ne soit rémunéré qu’au résultat<span class="spip_note_ref"> [<a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/vie-pratique/#nb3-1" class="spip_note" rel="footnote" title="sur la base d’une commission de 2 % de la valeur du bien à la charge de (...)" id="nh3-1">1</a>]</span>.</p>
<p>Les agents sont souvent également mandatés par les vendeurs. L’assistance d’un <strong>avocat</strong> lors de la négociation du mandat garantira un mandat équilibré.</p>
<h5 class="spip">Comment acquérir</h5>
<p>Une fois le bien identifié se pose un certain nombre de difficultés d’ordre pratique et légal :</p>
<p>A ce stade il est souhaitable de nommer un avocat<span class="spip_note_ref"> [<a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/vie-pratique/#nb3-2" class="spip_note" rel="footnote" title="Le montant des honoraires d’un avocat est fixé librement. Dans la pratique (...)" id="nh3-2">2</a>]</span> dont le rôle sera d’analyser les éventuelles difficultés inhérentes au bien. (Vérification des titres de propriété auprès du registre des hypothèques et du cadastre, du permis de construire, voire de la constructibilité du terrain en coopération avec un ingénieur civil…..). <strong>La Loi oblige de toute façon les partis à être assistés par un avocat lors de la signature du contrat de vente</strong>.</p>
<p>Une fois l’acheteur satisfait sur les caractéristiques du bien, une <strong>promesse de vente</strong> peut être signée sous seing privé entre l’acheteur et le vendeur. Une somme symbolique sera versée et un délai limite pour la signature du contrat authentique devant notaire seront fixés. Si cette pratique a sans conteste un impact d’ordre psychologique, il est notable, que sur le plan légal, elle ne peut en aucun cas garantir la vente définitive. Alternativement, un précontrat d’achat pourra être signé devant notaire éventuellement assorti de conditions suspensives.</p>
<p>Le transfert de propriété ne se fera qu’à la signature du contrat définitif, lequel devra ensuite faire l’objet d’un enregistrement par le registre des hypothèques compétent. Il ne pourra être signé que si un certain nombre de certificats ont été fournis et les frais de mutation préalablement réglés.</p>
<p>Il convient de ne pas ignorer la pratique qui consiste à ce que le contrat authentique soit établi sur la base de la valeur fiscale du bien (« antikeminki axia »), cela dans le but de réduire les frais de mutation (+/- 12%). Cette pratique n’est pas sans risques pour l’acheteur qui devra s’informer auprès de son conseil.</p>
<p>Pour les personnes ne parlant pas la langue grecque, l’assistance d’un interprète lors de la signature du contrat est également obligatoire.</p>
<h3 class="spip"><a id="sommaire_2"></a>Conditions de location</h3>
<p>Le délai moyen de recherche est de un à deux mois. Le mobilier des locations dites meublées est souvent sommaire. Les cuisines ne sont pas toujours équipées.</p>
<p>Les baux sont fixés pour une durée de deux ans minimum. Le loyer est payable d’avance : un mois de caution (parfois deux) et un mois d’avance (pouvant aller jusqu’à 12). En cas de location par une agence, la commission représente 2 % sur les loyers de la durée du bail ou parfois un mois de loyer. Les logements sont relativement abordables.</p>
<p>Beaucoup de propriétaires refusent de louer aux possesseurs d’un animal domestique, surtout s’il s’agit d’un chien.</p>
<p>Le moyen de chauffage le plus utilisé est le mazout. La climatisation se révèle nécessaire pendant les plus fortes chaleurs. Les services tels que l’eau, l’électricité et le téléphone fonctionnent correctement. Les charges comprennent en général le chauffage (souvent collectif), concierge, ascenseur, petites dépenses de l’immeuble.</p>
<p>Il est conseillé d’établir un état des lieux.</p>
<p>Pour en savoir plus : <a href="http://athenesaccueil-preparersonarrivee.blogspot.com/p/se-loger.html#choisir" class="spip_out" rel="external">Site d’Athenes accueil</a></p>
<p>Les bons quartiers sont Alimos et Néa Smyrni, les très bons quartiers sont en banlieue Sud proches de la mer Palio Faliro, Glyfada, Voula et Kavouri, les meilleurs quartiers sont Vouliagmeni, Kifisia, Ekali, Pendeli, Maroussi. Un certain nombre de Français résident en banlieue Nord-Est du fait de la présence à Aghia Paraskevi du Lycée franco-hellénique Eugène Delacroix.</p>
<h4 class="spip">Exemples de coûts de logement à Athènes (loyers mensuels) </h4>
<p><strong>Maisons</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td></td>
<td>Bons quartiers</td>
<td>Très bons quartiers</td>
<td>Meilleurs quartiers</td></tr>
<tr class="row_even even">
<td>3 chambres (200 m², meublées)</td>
<td>3000 euros</td>
<td>4000</td>
<td>6000</td></tr>
<tr class="row_odd odd">
<td>3 chambres (200 m², non-meublées)</td>
<td>1900 euros</td>
<td>2700 euros</td>
<td>3500 euros</td></tr>
<tr class="row_even even">
<td>4 chambres (250 m², non-meublées)</td>
<td>2200 euros</td>
<td>3000 euros</td>
<td>4000 euros</td></tr>
</tbody>
</table>
<p><strong>Appartements</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td></td>
<td>Bons quartiers</td>
<td>Très bons quartiers</td>
<td>Meilleurs quartiers</td></tr>
<tr class="row_even even">
<td>2 chambres (80-120 m², meublées)</td>
<td>1300 euros</td>
<td>1700 euros</td>
<td>2200 euros</td></tr>
<tr class="row_odd odd">
<td>3 chambres (120-160 m², meublées)</td>
<td>1550 euros</td>
<td>2000 euros</td>
<td>3000 euros</td></tr>
<tr class="row_even even">
<td>1 chambre (50-70 m², non-meublée)</td>
<td>600 euros</td>
<td>900 euros</td>
<td>1000 euros</td></tr>
<tr class="row_odd odd">
<td>2 chambres (80-120 m², non-meublées)</td>
<td>800 euros</td>
<td>1300 euros</td>
<td>1500 euros</td></tr>
<tr class="row_even even">
<td>3 chambres (120-160 m², non-meublées)</td>
<td>1200 euros</td>
<td>1650 euros</td>
<td>2000 euros</td></tr>
<tr class="row_odd odd">
<td>4 chambres (160-200 m², non-meublées)</td>
<td>1400 euros</td>
<td>2000 euros</td>
<td>3000 euros</td></tr>
</tbody>
</table>
<p>L’offre est très inégale selon les quartiers, la recherche se fait via des agences immobilières ou petites annonces.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.visitgreece.gr/" class="spip_out" rel="external">site de l’office de tourisme grec</a></p>
<h3 class="spip"><a id="sommaire_3"></a>Hôtels</h3>
<p><strong>Athènes</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Prix moyen d’une chambre d’hôtel (chambre double)</td>
<td>Euros</td></tr>
<tr class="row_even even">
<td>Grand tourisme</td>
<td>270</td></tr>
<tr class="row_odd odd">
<td>Moyen tourisme</td>
<td>150</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_4"></a>Electricité</h3>
<p>Le courant électrique est de type alternatif (220 volts, 50 périodes). Les prises de courant respectent les normes européennes.</p>
<h3 class="spip"><a id="sommaire_5"></a>Electroménager</h3>
<p>Le matériel électroménager de marques françaises ou étrangères est disponible, mais plus cher qu’en France à qualité égale.</p>
<p><i>Mise à jour : novembre 2013</i></p>
<p><span class="spip_note_ref">[<a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/vie-pratique/#nh3-1" class="spip_note" title="Notes 3-1" rev="footnote">1</a>] </span>sur la base d’une commission de 2 % de la valeur du bien à la charge de l’acheteur</p>
<p><span class="spip_note_ref">[<a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/vie-pratique/#nh3-2" class="spip_note" title="Notes 3-2" rev="footnote">2</a>] </span>Le montant des honoraires d’un avocat est fixé librement. Dans la pratique ils évoluent entre 1,5 et 5% de la valeur du bien acquis dans le cas de la conclusion de l’affaire.</p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-grece-vie-pratique-article-loisirs-et-culture.md" title="Loisirs et culture">Loisirs et culture</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-grece-vie-pratique-article-communications.md" title="Communications">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-grece-vie-pratique-article-transports.md" title="Transports">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-grece-vie-pratique-article-cout-de-la-vie-110393.md" title="Coût de la vie">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-grece-vie-pratique-article-scolarisation-110392.md" title="Scolarisation">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-grece-vie-pratique-article-sante.md" title="Santé">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-grece-vie-pratique-article-logement.md" title="Logement">Logement</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/vie-pratique/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
