# Coronavirus MERS

<p>Plusieurs dizaines de cas d’infection virale à Coronavirus (MERS CoV) ont été constatés ces derniers mois dans les pays de la Péninsule arabique ou dans les pays limitrophes.</p>
<p>Les zones géographiques identifiées à risque ou potentiellement à risque incluent, à présent, l’Arabie Saoudite, le Bahreïn, les Emirats arabes unis, l’Iran, l’Irak, Israël, la Jordanie, le Koweït, le Liban, Oman, le Qatar, les Territoires palestiniens, la Syrie et le Yémen. Des cas d’infection par le coronavirus du syndrome respiratoire du Moyen-Orient sont également apparus en Corée du Sud. A ce jour, aucune mesure de restriction des voyages en direction ou en provenance de ces pays en raison du coronavirus n’est justifiée.</p>
<p>La source de transmission de ce nouveau Coronavirus est toujours à l’étude (les chameaux pourraient jouer un rôle de réservoir).</p>
<p>Les coronavirus sont une vaste famille de virus susceptibles de provoquer un large éventail de maladies chez l’homme, qui vont du rhume banal jusqu’à une atteinte respiratoire sévère. La présentation la plus fréquente associe de la fièvre et une infection pulmonaire.</p>
<p>Les mesures classiques d’hygiène sont recommandées pour limiter les risques de transmission de ce virus, en particulier le lavage régulier des mains à l’eau et au savon ou avec une solution hydro-alcoolique. Il convient également, dans la mesure du possible, d’éviter les foules et grands rassemblements ainsi qu’un contact rapproché avec des personnes malades. Le nettoyage et la désinfection réguliers de surfaces telles que les poignées de porte, les interrupteurs, les rambardes ou les jouets sont recommandés.</p>
<p>Éviter tout contact avec les animaux en particulier avec les chameaux et les dromadaires.</p>
<p>Il est très fortement déconseillé aux voyageurs se rendant dans la Péninsule arabique de consommer de la viande de chameau ou du lait de chamelle.</p>
<p>Il est fortement recommandé aux personnes âgées fragiles, aux personnes atteintes d’une pathologie chronique évolutive, entrainant notamment une baisse de l’immunité (diminution des défenses contre les infections) aux enfants et aux femmes enceintes de consulter leur médecin traitant avant d’envisager un déplacement dans une zone touchée pour l’épidémie.</p>
<p>Sur place, en cas de forte fièvre, toux et/ou de difficultés respiratoires, il convient de consulter un médecin sans délai.</p>
<p>Une surveillance particulière a été mise en place en France. En cas de fièvre ou de symptômes respiratoires dans les jours qui suivent le retour en France, il convient d’appeler le centre 15 (téléphone : 15) en signalant ce voyage.</p>
<p>Pour toute information complémentaire, consulter le <a href="http://www.sante.gouv.fr/" class="spip_out" rel="external">site internet du ministère français de la santé</a> et <a href="http://www.who.int/fr/" class="spip_out" rel="external">celui de l’Organisation mondiale de la Santé</a>.</p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/infos-pratiques/risques/risques-sanitaires/article/coronavirus-mers). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
