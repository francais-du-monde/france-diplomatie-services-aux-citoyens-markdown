# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/vie-pratique/article/cout-de-la-vie-114294#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/vie-pratique/article/cout-de-la-vie-114294#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/vie-pratique/article/cout-de-la-vie-114294#sommaire_3">Alimentation</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/vie-pratique/article/cout-de-la-vie-114294#sommaire_4">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>L’unité monétaire est le Zloty (PLN) et groszy (1 zloty = 100 groszys). Le zloty polonais n’est convertible qu’en Pologne.</p>
<p>Les principales devises, dont l’euro (liquide ou chèques de voyage), peuvent être échangées contre des zlotys polonais dans les banques, les bureaux de change (en polonais <i>Kantor</i>) et les hôtels.</p>
<p>La majorité des hôtels, restaurants, stations d’essence et magasins de luxe acceptent les cartes de crédit les plus répandues (Visa, American Express et Mastercard, etc.). Les distributeurs bancaires (en polonais : Bankomat) sont fréquents dans la capitale et les grandes villes de Pologne.</p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<p>Les salariés peuvent recevoir le montant de leur rémunération soit en Pologne soit sur un compte bancaire à l’étranger.</p>
<p>Il existe des banques françaises : DNB Nord, Société Générale, CALYON, BNP (excepté DNB Nord, ces banques sont réservées aux entreprises) et des banques étrangères (75 %) : Citibank, ABN-Amro bank, Deutsche Bank, HSBC…</p>
<p>Les chèques ne sont pas utilisés en Pologne.</p>
<p>Paiements par carte bancaire ou virements (à la banque ou à la poste).</p>
<p>Pour ouvrir un compte bancaire en Pologne, vous devez présenter un passeport valide (certaines banques exigent un autre document avec une photo, par exemple un permis de conduire) et un document certifiant votre statut (étudiant, salarié, etc). Vous pouvez par ailleurs effectuer un retrait d’argent sur votre compte ouvert dans votre pays d’origine via un distributeur automatique. Il est possible d’ouvrir un compte privé en devise polonaise ou étrangère (<i>Source : site de la commission européenne</i>).</p>
<p>Banques polonaises : <a href="http://www.skarbiec.biz/banki/" class="spip_out" rel="external">www.skarbiec.biz/banki</a></p>
<h3 class="spip"><a id="sommaire_3"></a>Alimentation</h3>
<p>Présence de nombreuses grandes surfaces (Auchan, Carrefour, Réal, Leclerc) et de centres commerciaux modernes.</p>
<p>Prix moyen d’un repas dans un restaurant : 60 PLN</p>
<h3 class="spip"><a id="sommaire_4"></a>Evolution des prix</h3>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/vie-pratique/article/cout-de-la-vie-114294). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
