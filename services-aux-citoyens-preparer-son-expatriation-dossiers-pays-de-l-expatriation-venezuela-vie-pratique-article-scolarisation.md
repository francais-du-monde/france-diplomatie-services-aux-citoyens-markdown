# Scolarisation

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/vie-pratique/article/scolarisation#sommaire_1">Les établissements français au Venezuela</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/vie-pratique/article/scolarisation#sommaire_2">Enseignement supérieur</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/vie-pratique/article/scolarisation#sommaire_3">Pour en savoir plus</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Les établissements français au Venezuela</h3>
<p>Il n’y a qu’un établissement français au Venezuela : le <a href="http://www.colegiofrancia.edu.ve/" class="spip_out" rel="external">lycée français (Colegio Francia) de Caracas</a>. Cet établissements couvre toutes les sections, depuis l’école maternelle jusqu’au baccalauréat.</p>
<p>Pour toute information sur la scolarisation dans le système français à l’étranger, vous pouvez également consulter notre rubrique thématique : <a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais.md" class="spip_in">Scolarité en français</a>.</p>
<p>Vous y trouverez des renseignements sur :</p>
<ul class="spip">
<li>les établissements français du primaire et du secondaire à l’étranger ;</li>
<li>les bourses scolaires et la prise en charge des frais de scolarité à l’étranger ;</li>
<li>les possibilités qui s’offrent à vous si votre enfant ne peut être scolarisé à l’étranger dans le système français (enseignement à distance par le CNED, programme français langue maternelle (FLAM), internats en France) ;</li>
<li>les épreuves du baccalauréat à l’étranger ;</li>
<li>les bourses d’études supérieures en France et à l’étranger ;</li>
<li>l’équivalence des diplômes.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Enseignement supérieur</h3>
<p>L’enseignement supérieur au Venezuela regroupe des institutions qui se répartissent de façon très inégale dans les 8 régions géographique du pays avec une plus forte concentration sur Caracas, Maracaibo et Mérida. Il y a dans le pays 51 universités, 130 instituts universitaires de technologie, des collèges universitaires ainsi que plusieurs établissements spécialisés.</p>
<p>C’est le ministère du pouvoir populaire pour l’éducation qui est en charge de l’enseignement primaire et secondaire. L’enseignement supérieur est pris en charge par le ministère du pouvoir populaire pour l’éducation supérieure. La recherche est, elle, confiée au ministère du pouvoir populaire pour la science, la technologie et les industries intermédiaires. De nombreux organismes publics ou parapublics viennent compléter le dispositif ministériel ; on peut notamment citer la Fondation Gran Mariscal de Ayacucho qui a pour mission de démocratiser l’enseignement supérieur au travers de programmes de bourses.</p>
<p>La durée des études est longue, mais le « Bachillerato » est obtenu avec une année de moins d’apprentissage que le baccalauréat, et son niveau est généralement plus faible. La licence est obtenue en 5 ans, la maîtrise demande 2 ans d’études et enfin le doctorat nécessite de 2 à 4 ans.</p>
<p>Le coût des études est très fluctuant. Dans les universités publiques, il est peu élevé pour les étudiants venant du système public d’enseignement secondaire, et calculé d’après les revenus des familles pour les étudiants en provenance de l’enseignement secondaire privé. Dans les universités privées, il est totalement variable.</p>
<h4 class="spip">Équivalence avec le système français</h4>
<p>Un accord a été signé en 1995 pour la reconnaissance réciproque du baccalauréat. Pour les autres diplômes, il n’y a pas d’équivalence automatique. Un remaniement du système d’enseignement supérieur est en pleine gestation avec une réorganisation des études se rapprochant davantage du processus de Bologne en vigueur en Europe.</p>
<p>Il existe plusieurs accords inter-universitaires entre des établissements supérieurs français et leurs partenaires vénézuéliens.</p>
<h3 class="spip"><a id="sommaire_3"></a>Pour en savoir plus</h3>
<ul class="spip">
<li><a href="http://www.aefe.fr/" class="spip_out" rel="external">Agence pour l’enseignement français à l’étranger</a></li>
<li><a href="http://www.ambafrance-ve.org/" class="spip_out" rel="external">Ambassade de France au Venezuela, rubrique Présence française</a></li>
<li><a href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/VENEZUELA_19-11-09.pdf" class="spip_out">France diplomatie, rubrique Enseignement supérieur</a></li>
<li><a href="http://www.me.gob.ve/" class="spip_out" rel="external">Ministère du pouvoir populaire pour l’enseignement supérieur</a></li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/vie-pratique/article/scolarisation). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
