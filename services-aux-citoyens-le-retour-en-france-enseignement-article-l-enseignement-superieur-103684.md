# L’enseignement supérieur

<p><strong>Si vos enfants sont titulaires ou futurs titulaires du baccalauréat français obtenu à l’étranger,</strong> qu’ils soient ou non de nationalité française, leur admission en premier cycle dans une université française s’effectue selon la procédure APB (Admission Post Bac) disponible <a href="http://www.admission-postbac.fr/" class="spip_out" rel="external">en ligne</a>.</p>
<p><strong>Si vos enfants préparent le baccalauréat européen, le baccalauréat franco-allemand ou le baccalauréat international de Genève,</strong> leur admission en premier cycle universitaire s’effectue <a href="http://www.admission-postbac.fr/" class="spip_out" rel="external">selon la même procédure</a>. Ces baccalauréats sont en effet valables de plein droit sur le territoire français et sont assimilés au baccalauréat français.</p>
<p><strong>Si vos enfants sont scolarisés dans le système éducatif du pays où vous résidez,</strong> ils peuvent s’inscrire en premier cycle dans une université française à condition que le diplôme qu’ils préparent confère la qualification requise pour être admis dans les établissements analogues à ceux du pays où le diplôme est délivré. Il leur est toutefois recommandé de se signaler, dès le mois d’avril, à l’attention de l’université dans laquelle ils envisagent de demander leur inscription afin d’obtenir la validation de leur diplôme.</p>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li>Consulter notre article <a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures-s-informer-sur-les-etudes-a-l-etranger-article-reconnaissance-des-diplomes-etrangers-en-france.md" class="spip_in">Reconnaissance des diplômes étrangers en France</a></li>
<li><a href="http://www.ciep.fr/enic-naric-france" class="spip_out" rel="external">Ciep.fr</a></li></ul>
<p><i>Mise à jour : avril 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/enseignement/article/l-enseignement-superieur-103684). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
