# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/vie-pratique/article/transports-110997#sommaire_1">Importation de véhicule</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/vie-pratique/article/transports-110997#sommaire_2">Permis de conduire</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/vie-pratique/article/transports-110997#sommaire_3">Code de la route</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/vie-pratique/article/transports-110997#sommaire_4">Assurances et taxes</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/vie-pratique/article/transports-110997#sommaire_5">Achat et location</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/vie-pratique/article/transports-110997#sommaire_6">Immatriculation</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/vie-pratique/article/transports-110997#sommaire_7">Entretien</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/vie-pratique/article/transports-110997#sommaire_8">Réseau routier</a></li>
<li><a id="so_9" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/vie-pratique/article/transports-110997#sommaire_9">Transports en commun</a></li></ul>
<p>La conduite de nuit est à éviter en dehors des principales villes en raison de l’état du réseau routier et de l’éclairage défaillant.</p>
<h3 class="spip"><a id="sommaire_1"></a>Importation de véhicule</h3>
<p>Importer son véhicule est possible avec des droits de douane d’environ 75%. Le coût du transport est variable selon les sociétés de déménagement.</p>
<h3 class="spip"><a id="sommaire_2"></a>Permis de conduire</h3>
<p>Le permis de conduire français ou international est reconnu, mais il faut se faire délivrer un permis local par la police (avec un délai d’environ un mois). Toutefois, le permis français devra être authentifié par les services de l’ambassade de France pour être accepté par les autorités ghanéennes (excepté pour les statuts officiels et diplomatiques).</p>
<h3 class="spip"><a id="sommaire_3"></a>Code de la route</h3>
<p>La conduite s’effectue à droite et la priorité est à gauche sauf aux ronds-points. La vitesse est limitée en ville à 50 km/h, et à 90 km/h sur route et autoroute.</p>
<h3 class="spip"><a id="sommaire_4"></a>Assurances et taxes</h3>
<p>Le coût de la carte grise et de la vignette est d’environ 50 euros. <br class="manualbr">Elles sont obligatoires pour le risque "tiers collision". Il est conseillé de coupler son assurance locale avec celle d’une compagnie étrangère.</p>
<h3 class="spip"><a id="sommaire_5"></a>Achat et location</h3>
<p>Les principales marques étrangères sont représentées de même que la marque Renault et prochainement Citroën. Un véhicule tout-terrain climatisé est conseillé.</p>
<p>Le coût et les conditions de location, de même que les prix des véhicules vendus sur place sont sensiblement identiques à ce que l’on rencontre en Europe. Il faudra par ailleurs compter entre 80 et 100 euros par jour pour la location d’un véhicule avec chauffeur.</p>
<h3 class="spip"><a id="sommaire_6"></a>Immatriculation</h3>
<p>A noter que les véhicules immatriculés avec une plaque étrangère ne sont pas autorisés à circuler au Ghana entre 18h et 6h (sous peine d’amende et d’immobilisation du véhicule durant la nuit).</p>
<h3 class="spip"><a id="sommaire_7"></a>Entretien</h3>
<p>L’entretien d’un véhicule sur place est effectué dans de petits garages (ateliers) avec une qualité de service très moyenne. Se procurer des pièces détachées peut être difficile et les délais de livraison peuvent être longs (un mois minimum). Il est donc judicieux de prévoir un lot de pièces de rechange, de l’outillage et des écrous "antivol" pour les roues.</p>
<h3 class="spip"><a id="sommaire_8"></a>Réseau routier</h3>
<p>La liberté de circulation est totale.</p>
<p>L’état du réseau routier, médiocre, est en voie d’amélioration. Accra et Tema sont reliées par 30 km d’autoroute. La route côtière transafricaine de Cape Coast à la frontière ivoirienne et le triangle Cape Coast/Kumasi/Accra sont en bon état. En revanche, la route de Lomé est très dégradée et la route du Burkina Faso via Tamale est inachevée. L’état des pistes varie selon les saisons. Pour ses déplacements à l’intérieur du pays, un véhicule neuf ou en très bon état et de préférence climatisé, est recommandé.</p>
<h3 class="spip"><a id="sommaire_9"></a>Transports en commun</h3>
<p>Les liaisons internes sont assurées par avion, par autocar (peu confortable) ou par train (rare). Les taxis collectifs et tro-tro demeurent risqués (accidents fréquents rapportés). En ville, l’utilisation de taxis et tro-tro est courante. Le véhicule personnel reste le moyen de transport privilégié chez les étrangers.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/vie-pratique/article/transports-110997). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
