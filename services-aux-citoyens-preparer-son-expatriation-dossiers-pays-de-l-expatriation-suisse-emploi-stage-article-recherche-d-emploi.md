# Recherche d’emploi

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/emploi-stage/article/recherche-d-emploi#sommaire_1">Réseaux</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/emploi-stage/article/recherche-d-emploi#sommaire_2">Média</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/emploi-stage/article/recherche-d-emploi#sommaire_3">Organisations et organismes internationaux établis en Suisse</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/emploi-stage/article/recherche-d-emploi#sommaire_4">Organismes sur place pour la recherche d’emploi</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Réseaux</h3>
<p>Il existe un important réseau de cabinets de recrutements dans tout le pays, bien que ce soit Genève qui compte le plus grande nombre d’agences de placement. 63% des RH suisses les mandatent.</p>
<p>Le site <a href="http://www.travailler-en-suisse.ch/" class="spip_out" rel="external">Travailler en Suisse</a> rubrique Travailler liste, par canton, les cabinets de recrutement en Suisse et à Genève.</p>
<p>Le bouche-à-oreille permet également de se faire connaître et d’être informé(e) plus rapidement des emplois vacants. C’est le cas au sein des organisations internationales. Il ne faut pas hésiter à contacter les associations ou unions professionnelles et les clubs d’entrepreneurs du ou des secteurs qui vous intéressent et de parcourir leur site internet. L’appartenance à un réseau professionnel se fait au travers d’une adhésion en tant que membre.</p>
<p><strong>Les offices du travail</strong></p>
<p>Au niveau des administrations cantonales, les offices du travail sont généralement intégrés au Département cantonal de l’économie et s’appellent, selon les cantons :</p>
<ul class="spip">
<li>Office de l’emploi ;</li>
<li>Office cantonal de l’emploi ;</li>
<li>Office public de l’emploi ;</li>
<li>Office cantonal du travail ;</li>
<li>Service des arts et métiers et du travail ;</li>
<li>Service de l’industrie, du commerce et du travail.</li></ul>
<p>Les offices régionaux de placement (ORP), et en règle générale, les caisses de chômage publiques, y sont directement subordonnés.</p>
<p>Les offices régionaux de placement (ORP) sont des centres de prestations spécialisés dans les domaines du marché du travail, du placement et du chômage. Au nombre de 130, ils représentent la plus grande plate-forme de l’emploi en Suisse.</p>
<h3 class="spip"><a id="sommaire_2"></a>Média</h3>
<p><strong>Les journaux</strong></p>
<p>La plus grande partie des offres d’emploi sont disponibles sur Internet (90% des RH y « surfent ») celles publiées dans les journaux et les magazines " papiers " étant de moins en moins nombreuses.</p>
<p>Vous trouverez des offres d’emploi sur les sites Internet des journaux suivants :</p>
<ul class="spip">
<li><a href="http://www.letemps.ch/" class="spip_out" rel="external">Le Temps</a> rubrique Les rendez-vous / carrières  (s’adresse plutôt à des cadres)</li>
<li><a href="http://www.nzz.ch/" class="spip_out" rel="external">Neue Zürcher Zeitung</a> (site en allemand) rubrique " Marktplätze "</li>
<li><a href="http://www.tagesanzeiger.ch/" class="spip_out" rel="external">Tagesanzeiger</a> / rubrique " Marktplatz "</li></ul>
<p><strong>Sites officiels</strong></p>
<ul class="spip">
<li><a href="http://www.espace-emploi.ch/" class="spip_out" rel="external">SECO - Direction du travail - Espace emploi</a> (pour toute la Suisse)</li>
<li><a href="http://www.portail-emploi.ch/" class="spip_out" rel="external">Portail romand de l’emploi</a></li></ul>
<p>Les administrations suisses, qu’elles soient fédérales, cantonales ou communales, proposent des postes, accessibles également aux travailleurs étrangers.</p>
<p>Pour accéder à ces offres d’emploi, vous pouvez consulter le <a href="http://www.publicjobs.ch.ch/" class="spip_out" rel="external">Portail suisse de l’emploi public</a>.</p>
<p><strong>Sites privés</strong></p>
<p>Les sites portail d’offres d’emploi, appelés aussi <i>Jobboards</i>, sont nombreux en Suisse. Par ailleurs, diverses associations, qu’elles soient françaises ou suisses, proposent des offres d’emplois.</p>
<ul class="spip">
<li>Le site <a href="http://www.travailler-en-suisse.ch/" class="spip_out" rel="external">Travailler en Suisse</a> rubrique  Travailler recense certains de ces sites.</li>
<li><a href="http://www.avg-seco.admin.ch/" class="spip_out" rel="external">Un site officiel</a> liste les différentes agences de placement et cabinets de recrutement.</li>
<li><a href="http://www.efinancialcareers.ch/" class="spip_out" rel="external">Financial Careers</a> (site en allemand) :  emplois dans la banque et la finance.</li></ul>
<p>S’agissant du recrutement via les réseaux sociaux, une récente étude a démontré que 54% des RH suisses sont sceptiques à cet égard.</p>
<p><strong>Annuaires d’entreprises</strong></p>
<p>Dans certains cas, il peut être plus utile de contacter directement l’entreprise qui vous intéresse, en particulier si vous êtes un professionnel de son domaine. L’envoi de candidature spontanée est donc une option à prendre en compte.</p>
<p>Les pages jaunes de l’annuaire téléphonique suisse fournissent les entreprises répertoriées par rubrique. L’<a href="http://www.kompass.com" class="spip_out" rel="external">annuaire mondial Kompass</a>, leader de l’information des entreprises est consultable gratuitement par pays et par catégories.</p>
<p>Le site <a href="http://www.travailler-en-suisse.ch" class="spip_out" rel="external">Travailler en Suisse</a> liste, par canton, sous la rubrique " listes entreprises suisses " les 450 plus grandes entreprises en Suisse. Ce site fournit aussi les coordonnées des banques suisses ainsi que des agences de placement en Suisse.</p>
<h3 class="spip"><a id="sommaire_3"></a>Organisations et organismes internationaux établis en Suisse</h3>
<ul class="spip">
<li>Accord général sur les tarifs douaniers et le commerce (GATT) (géré par la « Commission intérimaire de l’Organisation internationale du commerce » = ICITO)</li>
<li>Association des pays exportateurs de minerai de fer (APEF)</li>
<li>Association européenne de libre-échange (AELE)</li>
<li>Banque asiatique de développement, pour son bureau à Zurich</li>
<li>Banque de règlements internationaux (BRI)</li>
<li>Bureau de l’Organisation des Nations Unies pour le développement industriel (ONUDI) à Zurich</li>
<li>Bureau international de l’éducation (BIE)</li>
<li>Comité intergouvernemental pour les migrations (CIM)</li>
<li>Commission du droit international (organe de l’Assemblée générale de l’Organisation des Nations Unies)</li>
<li>ICITO, voir Accord général sur les tarifs douaniers et le commerce (GATT)</li>
<li>Organisation des Nations Unies (ONU) (Office des Nations Unies à Genève)</li>
<li>Organisation européenne pour la recherche nucléaire (CERN)</li>
<li>Organisation intergouvernementale pour les transports internationaux ferroviaires (OTIF)</li>
<li>Organisation internationale de protection civile (OIPC)</li>
<li>Organisation internationale du travail (OIT)</li>
<li>Organisation météorologique mondiale (OMM)</li>
<li>Organisation mondiale de la propriété intellectuelle (OMPI)</li>
<li>Organisation mondiale de la santé (OMS)</li>
<li>Union internationale des télécommunications (UIT)</li>
<li>Union interparlementaire (UIP)</li>
<li>Union postale universelle (UPU)</li>
<li>Union européenne de radiotélévision (UER)</li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Organismes sur place pour la recherche d’emploi</h3>
<p>Pour augmenter vos chances de trouver un emploi, vous devez utiliser différentes méthodes de recherche d’emploi :</p>
<ul class="spip">
<li>exploiter votre réseau personnel de relations ;</li>
<li>recourir aux agences de placement et aux cabinets de recrutement. Les profils techniques ou atypiques auront parfois intérêt à passer par ces intermédiaires ;</li>
<li>contacter les offices cantonaux de l’emploi. Les offres d’emploi ne sont pas accessibles aux personnes n’ayant jamais travaillé en Suisse. Elles peuvent cependant être consultées sur le site Internet suivant : <a href="http://www.espace-emploi.ch" class="spip_out" rel="external">www.espace-emploi.ch</a> ;</li>
<li>envoyer des candidatures spontanées.</li></ul>
<p>Depuis le 1er juin 2004, il n’y a plus de protectionnisme concernant l’accès au marché du travail et les ressortissants de l’Union européenne sont considérés, sur le plan légal, comme des travailleurs locaux. Auparavant, les entreprises devaient prouver qu’elles n’avaient pas trouvé en Suisse de candidat correspondant au profil recherché.</p>
<p>Certaines entreprises ont des besoins en personnel qui ne font l’objet d’aucune publication dans la presse, ni d’aucune autre forme de publicité. Ce que l’on appelle " le marché caché " représenterait plus de 50% du marché de l’emploi et n’est accessible que grâce au <i>Networking</i> ou techniques de réseautage.</p>
<p><i>Mise à jour : février 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/emploi-stage/article/recherche-d-emploi). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
