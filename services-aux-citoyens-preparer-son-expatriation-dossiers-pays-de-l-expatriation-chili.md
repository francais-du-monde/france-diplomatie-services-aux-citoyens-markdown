# Chili

<p><strong>31 décembre 2014, 10 936 Français</strong> étaient inscrits au registre des Français établis hors de France.</p>
<p>Estimée à quelques 12 000 personnes, la communauté française est principalement composée d’expatriés de longue date (entre 60 et 70%) et réside majoritairement dans le "Grand Santiago" (77 %). La région de Vina del Mar-Valparaiso (Vème région) et les régions qui s’étirent tout au long du Chili au sud de Santiago (VIIIème à XIIème régions) ne viennent que largement après. Peu de Français habitent dans les régions du Nord.</p>
<p>On dénombre en 2007 près de 122 établissements français au Chili créateurs d’emplois. Ils occupent une place notable dans les secteurs hautement stratégiques de l’économie locale tels que l’agriculture, l’énergie ou les transports.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/dossiers-pays/http-publication-diplomatie-gouv-fr-fr-dossiers-pays-chili/" class="spip_in">Des informations générales sur le Chili, sa situation politique et économique</a> ;</li>
<li><a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/chili/" class="spip_in">Des informations actualisées sur les <strong>conditions locales de sécurité</strong> au Chili</a> ;</li>
<li><a href="http://www.ambafrance-cl.org/" class="spip_out" rel="external">Ambassade de France au Chili</a>.</li></ul>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chili-entree-et-sejour-22781.md">Entrée et séjour</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chili-entree-et-sejour-22781-article-passeport-visa-permis-de-travail-109746.md">Passeport, visa, permis de travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chili-entree-et-sejour-22781-article-demenagement-109747.md">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chili-entree-et-sejour-22781-article-vaccination-109748.md">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chili-entree-et-sejour-22781-article-animaux-domestiques-109749.md">Animaux domestiques</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chili-emploi-stage.md">Emploi, stage</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chili-emploi-stage-article-marche-du-travail.md">Marché du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chili-emploi-stage-article-reglementation-du-travail.md">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chili-emploi-stage-article-recherche-d-emploi.md">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chili-emploi-stage-article-curriculum-vitae-109753.md">Curriculum vitae</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chili-emploi-stage-article-lettre-de-motivation.md">Lettre de motivation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chili-emploi-stage-article-entretien-d-embauche.md">Entretien d’embauche</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chili-emploi-stage-article-stages.md">Stages</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chili-protection-sociale-22783.md">Protection sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chili-protection-sociale-22783-article-regime-local-de-securite-sociale-109757.md">Régime local de sécurité sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chili-protection-sociale-22783-article-convention-de-securite-sociale-109758.md">Convention de sécurité sociale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chili-fiscalite-22784.md">Fiscalité</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chili-fiscalite-22784-article-fiscalite-du-pays-109759.md">Fiscalité du pays</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chili-fiscalite-22784-article-convention-fiscale-109760.md">Convention fiscale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chili-vie-pratique.md">Vie pratique</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chili-vie-pratique-article-logement.md">Logement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chili-vie-pratique-article-sante.md">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chili-vie-pratique-article-scolarisation-109763.md">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chili-vie-pratique-article-cout-de-la-vie-109764.md">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chili-vie-pratique-article-transports.md">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chili-vie-pratique-article-communications-109766.md">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chili-vie-pratique-article-loisirs-et-culture.md">Loisirs et culture</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chili-presence-francaise.md">Présence française</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chili-presence-francaise-article-presence-francaise.md">Présence française</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
