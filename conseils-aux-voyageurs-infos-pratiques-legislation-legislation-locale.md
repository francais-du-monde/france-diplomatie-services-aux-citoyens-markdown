# Législation locale

<p>Respectez la ou les religions pratiquée(s) dans le pays de destination, et plus généralement les usages locaux ainsi que les comportements et les règles vestimentaires ou alimentaires qui s’y attachent.</p>
<p>Respectez également de façon scrupuleuse les lois locales notamment celles relatives :</p>
<ul class="spip">
<li>à l’alcool (importation, achat et consommation, notamment au volant),</li>
<li>aux mœurs,</li>
<li>aux stupéfiants.</li></ul>
<p>Les peines encourues peuvent être très lourdes dans certains pays, allant jusqu’à la réclusion à perpétuité voire la peine de mort.</p>
<p>Pour en savoir plus, consultez la <a href="conseils-aux-voyageurs-conseils-par-pays.md" class="spip_in">fiche "Conseils aux voyageurs"</a> de votre pays de destination.</p>
<p class="spip_document_66149 spip_documents spip_documents_left">
<a href="conseils-aux-voyageurs-conseils-par-pays.md" class="spip_in"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L423xH250/Carte_CAV-b0dce.jpg" width="423" height="250" alt="Carte CAV"></a></p>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/infos-pratiques/legislation/legislation-locale/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
