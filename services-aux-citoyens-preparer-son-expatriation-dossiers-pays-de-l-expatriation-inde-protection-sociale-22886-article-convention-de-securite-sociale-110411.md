# Convention de sécurité sociale

<p><a href="http://www.cleiss.fr/docs/textes/conv_inde.html" class="spip_out" rel="external">L’accord de sécurité sociale franco indien signé le 30 septembre 2008</a> est entré en vigueur le 1er juillet 2011.</p>
<p>Cet accord vise les travailleurs salariés et non salariés, de nationalité française ou indienne ou ressortissants d’État tiers.</p>
<p>Comme tous les textes internationaux il prévoit l’assujettissement du travailleur dans le pays où est exercée l’activité salariée ou non salariée. Son originalité tient aux conditions particulières du détachement du travailleur salarié (disposition dérogeant à la règle d’assujettissement dans le pays de travail). C’est ainsi que le travailleur salarié détaché dans l’autre État contractant par son employeur pourra être maintenu pendant une durée de 5 ans au régime d’assurance vieillesse de son pays d’origine et être exonéré durant la même période du paiement des cotisations au titre de la vieillesse dans la pays d’accueil. Par contre, il sera affilié dans le pays d’accueil pour les risques de sécurité sociale suivants : maladie, invalidité, accidents du travail et prestations familiales.</p>
<p>A côté des dispositions relatives à la législation applicable, ce texte coordonne les prestations d’invalidité, de vieillesse et de survivants.</p>
<p>Les personnes qui au cours de leur carrière ont été assujetties à la législation des deux États contractants pourront voir leurs droits à pension examinés dans le cadre de l’accord qui prévoit que chaque État rémunère les périodes d’assurance accomplies sous sa législation.</p>
<p>Pour la liquidation de la pension l’institution compétente de chacun des États contractants procèdera de la manière suivante :</p>
<p>examen des droits à pension nationale en fonction des seules périodes accomplies sous sa législation,</p>
<p>Totalisation des périodes accomplies sous sa législation avec celles accomplies sous la législation de l’autre État contractant, détermination d’une pension théorique établie comme si toutes les périodes d’assurance avaient été réalisées sous la législation de l’institution qui examine les droits, proratisation de la pension théorique en fonction des périodes d’assurance accomplies sous la législation de l’institution qui liquide par rapport à la totalité des périodes sous les législations des deux États contractants (pension proratisée).</p>
<p>Comparaison de la pension nationale et de la pension proportionnelle et versement du montant le plus avantageux des deux.</p>
<p>Les prestations acquises sur le territoire de l’un des États contractants sont versées sur le territoire de l’autre État contractant à tous les titulaires, quelle que soit leur nationalité. En cas de résidence sur le territoire d’un État tiers, les institutions nationales serviront les prestations aux ressortissants indiens ou français dans les mêmes conditions que pour leurs nationaux.</p>
<p>Enfin la partie IV de l’Accord contient un certain nombre de dispositions prévoyant une coopération entre les institutions des États contractants, destinées à permettre notamment le recouvrement des cotisations et des prestations indues en vue de mieux lutter contre la fraude et les erreurs.</p>
<p><i>Source : <a href="http://www.cleiss.fr/" class="spip_out" rel="external">CLEISS</a></i></p>
<p><strong>Pour en savoir plus :</strong></p>
<p>Pour vous informer sur la protection sociale des Français résidant à l’étranger, vous pouvez consulter notre thématique sur la <a href="services-aux-citoyens-preparer-son-expatriation-protection-sociale.md" class="spip_in">protection sociale</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/protection-sociale-22886/article/convention-de-securite-sociale-110411). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
