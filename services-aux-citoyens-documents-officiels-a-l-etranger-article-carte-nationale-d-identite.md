# Carte nationale d’identité

<p>La carte nationale d’identité est un document officiel qui permet à tout citoyen de justifier de son identité et, lorsque sa durée de validité n’est pas expirée, de sa nationalité française.</p>
<p>Elle n’est pas obligatoire ; elle est gratuite et valable quinze ans. En cas de perte ou de vol, un droit de 25 euros devra être payé pour l’obtention d’une nouvelle carte.</p>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/documents-officiels-a-l-etranger/article/carte-nationale-d-identite#sommaire_1">La CNIS permet-elle de voyager ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/documents-officiels-a-l-etranger/article/carte-nationale-d-identite#sommaire_2">Auprès de quelle autorité une demande de CNIS est-elle déposée ?</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/documents-officiels-a-l-etranger/article/carte-nationale-d-identite#sommaire_3">Quelles sont les pièces à fournir ?</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/documents-officiels-a-l-etranger/article/carte-nationale-d-identite#sommaire_4">Comment demander une CNIS pour un enfant ?</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/documents-officiels-a-l-etranger/article/carte-nationale-d-identite#sommaire_5">Quel est le délai de délivrance ?</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/documents-officiels-a-l-etranger/article/carte-nationale-d-identite#sommaire_6">Que faire en cas de perte ou de vol de la CNIS à l’étranger ?</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>La CNIS permet-elle de voyager ?</h3>
<p>La carte nationale d’identité sécurisée permet de se déplacer à l’étranger :</p>
<ul class="spip">
<li>dans tous les pays de l’Union européenne ;</li>
<li>dans les pays limitrophes de la France (Andorre, Monaco, Suisse) ;</li>
<li>dans les Etats qui  l’acceptent comme document de voyage. Pour plus de renseignements, <a href="conseils-aux-voyageurs.md" class="spip_in">consulter la page "Entrée et séjour" de la rubrique "Conseils aux voyageurs"</a>. <br class="manualbr">
<p class="spip_vignette"><img width="300" height="219" title="Illust: © AFP, 27.7 ko, 300x219" alt="Illust: © AFP, 27.7 ko, 300x219" src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L300xH219/cnis2-de63f.jpg"><br>© AFP</p>
<p><br class="manualbr"></p></li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Auprès de quelle autorité une demande de CNIS est-elle déposée ?</h3>
<p><strong>En France : </strong></p>
<p>Elle est délivrée dans <a href="http://www.interieur.gouv.fr/sections/a_l_interieur/le_ministere/organisation/les_prefectures" class="spip_out" rel="external">les préfectures et sous-préfets (voir la liste sur le site du ministère de l’Intérieur</a>.</p>
<p><strong>A l’étranger : </strong></p>
<p>Elle est délivrée dans tous les <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/article/annuaire-des-representations-francaises-a-l-etranger" class="spip_in">postes consulaires</a>.</p>
<h3 class="spip"><a id="sommaire_3"></a>Quelles sont les pièces à fournir ?</h3>
<p>D’abord <strong>il faut être inscrit au registre des Français établis hors de France</strong>. Cette formalité peut être effectuée lors de la demande de CNIS, elle facilitera vos démarches ultérieures (<a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/inscription-consulaire-et-communaute-francaise/article/comment-s-inscrire-et-mettre-ses" class="spip_in">consulter la page de la rubrique sur l’inscription au registre des Français établis hors de France</a>)</p>
<p><strong>Il est obligatoire de se présenter personnellement</strong> pour déposer une demande et recueillir l’empreinte digitale de l’index gauche ou à défaut de l’index droit.</p>
<ul class="spip">
<li>Pièces à fournir en cas de <a href="http://vosdroits.service-public.fr/particuliers/F1341.xhtml" class="spip_out" rel="external">première demande</a></li>
<li>Pièces à fournir en cas de <a href="http://vosdroits.service-public.fr/particuliers/F21089.xhtml" class="spip_out" rel="external">renouvellement</a></li>
<li>Pièces à fournir suite à une <a href="http://vosdroits.service-public.fr/particuliers/F1344.xhtml" class="spip_out" rel="external">perte</a> ou à un <a href="http://vosdroits.service-public.fr/particuliers/F1759.xhtml" class="spip_out" rel="external">vol</a>.</li></ul>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.interieur.gouv.fr/sections/a_votre_service/vos_demarches/passeport-biometrique/differencier-titre-securise" class="spip_out" rel="external">Rubrique "Passeport" sur le site du ministère de l’Intérieur</a></li>
<li><a href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/depliant_norme_photo-2.pdf" class="spip_in" type="application/pdf">Brochure sur les normes des photographies d’identité (PDF, 1,17 Mo)</a></li>
<li><a href="services-aux-citoyens-etat-civil-et-nationalite-francaise-etat-civil-article-demander-la-copie-d-un-acte-d-etat-civil.md" class="spip_in">Informations sur l’obtention d’une copie ou d’un extrait d’acte d’état civil</a></li>
<li><a href="http://vosdroits.service-public.fr/particuliers/F18713.xhtml" class="spip_out" rel="external">Informations sur le justificatif de nationalité</a></li></ul>
<p><strong>(*) Si ces justificatifs ont été produits et enregistrés lors de votre inscription au registre des Français établis hors de France, il ne sera pas nécessaire de les fournir à nouveau.</strong></p>
<h3 class="spip"><a id="sommaire_4"></a>Comment demander une CNIS pour un enfant ?</h3>
<p>Toute personne peut <a href="http://vosdroits.service-public.fr/particuliers/F1342.xhtml" class="spip_out" rel="external">demander une CNIS</a> pour un enfant sur lequel elle exerce l’autorité parentale.</p>
<h3 class="spip"><a id="sommaire_5"></a>Quel est le délai de délivrance ?</h3>
<p>Le délai de délivrance d’une CNIS à l’étranger est d’environ six semaines.</p>
<h3 class="spip"><a id="sommaire_6"></a>Que faire en cas de perte ou de vol de la CNIS à l’étranger ?</h3>
<p>La perte ou le vol d’une CNIS doit immédiatement être déclarée par son titulaire :</p>
<ul class="spip">
<li>en cas de vol : aux autorités de police locales et déclaration de vol au poste diplomatique ou consulaire le plus proche ;</li>
<li>en cas de perte : au poste diplomatique ou consulaire le plus proche.</li></ul>
<p><i>Mise à jour : septembre 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/documents-officiels-a-l-etranger/article/carte-nationale-d-identite). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
