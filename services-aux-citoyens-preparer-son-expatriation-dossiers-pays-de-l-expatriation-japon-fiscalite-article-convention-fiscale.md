# Convention fiscale

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/fiscalite/article/convention-fiscale#sommaire_1">Champ d’application de la convention</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/fiscalite/article/convention-fiscale#sommaire_2">Règles d’imposition</a></li></ul>
<p>La France et le Japon ont signé <strong>le 3 mars 1995</strong> une convention en vue d’éviter les doubles impositions et de prévenir l’évasion fiscale en matière d’impôts sur le revenu et sur la fortune. Cette convention est <strong>entrée en vigueur le 21 mars 1996. </strong></p>
<p>Un avenant à cette convention a été signé à Paris le 11 janvier 2007 et est entré en vigueur le 1er décembre 2007.</p>
<p>En vertu de l’article 55 de la Constitution, les conventions fiscales, qui répartissent entre les Etats le droit d’imposer, prévalent sur les dispositions fiscales de droit interne.</p>
<p>Son texte intégral est consultable sur le site Internet de l’administration fiscale. Voici un résumé de ses points clés pour les particuliers.</p>
<h3 class="spip"><a id="sommaire_1"></a>Champ d’application de la convention</h3>
<p>La convention franco-japonaise ne s’applique qu’aux personnes résidant dans l’un des Etats contractants ou dans les deux.</p>
<p>En vertu de l’article 4-1 de la convention, une personne est considérée comme résidente d’un Etat contractant si elle assujettie à l’impôt dans cet Etat en raison de son domicile, de sa résidence, de son siège social, de son siège de direction ou de tout autre critère analogue. N’est pas considérée comme résidente d’un Etat contractant la personne qui n’est assujettie à l’impôt dans cet Etat que pour les revenus de sources situées dans cet Etat.</p>
<p>Lorsqu’une personne est considérée comme résidente dans chacun des deux Etats contractants, l’article 4-2 prévoit des critères subsidiaires permettant de déterminer une résidence unique au regard de la convention.</p>
<p>Une personne est considérée résidente de l’Etat contractant où elle dispose d’un foyer d’habitation permanent. Si cette personne dispose d’un foyer d’habitation permanent dans chacun des deux Etats contractants, elle est considérée comme résidente de l’Etat contractant avec lequel ses liens personnels et économiques sont les plus étroits (centre des intérêts vitaux).</p>
<p>Si le centre des intérêts vitaux ne peut pas être déterminé, une personne est considérée comme résidente de l’Etat contractant où elle séjourne de façon habituelle. Si cette personne séjourne de façon habituelle dans chacun des Etats contractants ou dans aucun d’eux, elle est considérée résidente de l’Etat contractant dont elle possède la nationalité.</p>
<p>Enfin, si cette personne possède la nationalité de chacun des Etats contractants ou d’aucun d’eux, sa résidence est alors déterminée d’un commun accord entre les autorités françaises et japonaises.</p>
<h3 class="spip"><a id="sommaire_2"></a>Règles d’imposition</h3>
<h4 class="spip">Elimination des doubles impositions (article 23)</h4>
<h5 class="spip">Du côté français</h5>
<p>Les revenus provenant du Japon et imposables au Japon en vertu de la convention sont pris en compte pour le calcul de l’impôt français. L’impôt japonais ouvre droit à un crédit d’impôt imputable sur l’impôt français.</p>
<h5 class="spip">Du côté japonais</h5>
<p>Lorsqu’un résident du Japon reçoit des revenus provenant de France et imposables en France en vertu de la convention, le montant de l’impôt français dû à raison de ces revenus constitue en principe un crédit admis en déduction de l’impôt japonais à la charge de ce résident.</p>
<h4 class="spip">Modalités d’imposition des revenus catégoriels</h4>
<h5 class="spip">Traitements, salaires, pensions et rentes</h5>
<p><strong>Rémunérations privées (article 15)</strong></p>
<p>Sous réserve des dispositions plus spécifiques de la convention, les salaires, traitements et autres rémunérations similaires qu’un résident d’un Etat contractant reçoit au titre d’un emploi salarié ne sont imposables que dans cet Etat à moins que l’emploi ne soit exercé dans l’autre Etat contractant. Si tel est le cas, les rémunérations reçues à ce titre sont en principe imposables dans cet autre Etat.</p>
<p>Toutefois, les rémunérations perçues par une personne résidente d’un Etat contractant au titre d’un emploi salarié exercé dans l’autre Etat contractant restent imposables par l’Etat de résidence de cette personne si :</p>
<ul class="spip">
<li>cette personne séjourne dans l’autre Etat pendant une période ou des périodes n’excédant pas au total 183 jours durant toute période de douze mois commençant ou se terminant au cours de l’année fiscale concernée ;</li>
<li>les rémunérations sont payées par un employeur qui n’est pas résident de l’autre Etat ;</li>
<li>les rémunérations ne sont pas à la charge d’un établissement stable que l’employeur a dans l’autre Etat.</li></ul>
<p>En tout état de cause, les rémunérations au titre d’un emploi salarié exercé à bord d’un navire ou d’un aéronef exploités en trafic international par une entreprise d’un Etat contractant sont imposables dans cet Etat.</p>
<p><strong>Rémunérations publiques (article 19)</strong></p>
<p>Les rémunérations, autres que les pensions, payées par un Etat contractant ou l’une de ses collectivités locales à une personne physique au titre de services rendus à cet Etat ou à cette collectivité dans l’exercice de fonctions de caractère public, ne sont imposables que dans cet Etat.</p>
<p>Toutefois, ces rémunérations ne sont imposables que dans l’autre Etat contractant si les services sont rendus dans cet Etat et si la personne physique est un résident de cet Etat qui :</p>
<ul class="spip">
<li>possède la nationalité de cet Etat ou</li>
<li>n’est pas devenu un résident de cet Etat à seule fin de rendre les services.</li></ul>
<p>Les pensions payées par un Etat contractant ou l’une de ses collectivités locales à une personne physique au titre de services rendus à cet Etat ou cette collectivité ne sont imposables que dans cet Etat.</p>
<p>Toutefois, ces pensions ne sont imposables que dans l’autre Etat contractant si la personne physique est un résident de cet Etat et en possède la nationalité. Lorsque les rémunérations et les pensions sont payées au titre de services rendus dans le cadre d’une activité industrielle ou commerciale exercée par un Etat contractant ou l’une de ses collectivités locales, ce sont alors les dispositions conventionnelles relatives aux rémunérations privées (cf. supra) et aux pensions (cf. infra) qui s’appliquent.</p>
<p><strong>Pensions (article 18)</strong></p>
<p>Les pensions privées et autres rémunérations similaires payées à un résident d’un Etat contractant au titre d’un emploi antérieur ne sont imposables que dans cet Etat.</p>
<p><strong>Etudiants et stagiaires (article 20)</strong></p>
<p>Les sommes qu’un étudiant ou un stagiaire qui est, ou qui était immédiatement avant de se rendre dans un Etat contractant, un résident de l’autre Etat contractant et qui séjourne dans le premier Etat à seule fin d’y poursuivre ses études ou sa formation, reçoit pour couvrir ses frais d’entretien, d’études ou de formation ne sont pas imposables dans cet Etat, à condition qu’elles proviennent de sources situées en dehors de cet Etat.</p>
<p>Une personne physique, qui est ou qui était, immédiatement avant de se rendre dans un Etat contractant, un résident de l’autre Etat contractant et qui séjourne dans le premier Etat à titre temporaire pendant une période n’excédant pas deux ans en tant que bénéficiaire d’une bourse, allocation ou récompense ayant pour objet principal le financement d’études ou de recherches et qui lui est versée par une organisation gouvernementale, religieuse, charitable, scientifique, artistique, culturelle ou éducative est exonérée d’impôt dans le premier Etat sur le montant de cette bourse, allocation ou récompense.</p>
<p>Une personne physique, qui est ou qui était, immédiatement avant de se rendre dans un Etat contractant, un résident de l’autre Etat contractant, et qui est un employé ou un contractuel d’une entreprise de cet autre Etat ou d’une organisation gouvernementale, religieuse, charitable, scientifique, artistique, culturelle ou éducative de cet autre Etat et qui séjourne dans le premier Etat à titre temporaire pendant une période n’excédant pas un an à seule fin d’acquérir une expérience en matière technique, professionnelle ou commerciale auprès d’une personne autre que cette entreprise ou cette organisation est exonérée d’impôt dans le premier Etat sur les sommes provenant de l’autre Etat et destinées à couvrir ses frais d’entretien.</p>
<p><strong>Enseignants et chercheurs (article 21)</strong></p>
<p>Une personne physique qui séjourne dans un Etat contractant pendant une période ne dépassant pas deux ans afin d’enseigner ou d’effectuer des travaux de recherche dans une université, un lycée, une école ou un autre établissement d’enseignement officiellement reconnu situé dans cet Etat et qui est, ou qui était immédiatement avant ce séjour, un résident de l’autre Etat contractant n’est imposable que dans cet autre Etat sur les rémunérations qui proviennent de cet enseignement ou de ces travaux de recherche et à raison desquelles il est soumis à l’impôt dans cet autre Etat.</p>
<p>Cette règle n’est toutefois applicable que si les travaux de recherches ne sont pas entrepris principalement en vue de la réalisation d’un avantage particulier bénéficiant à une ou plusieurs personnes déterminées.</p>
<h5 class="spip">Autres catégories de revenus</h5>
<p><strong>Revenus et gains immobiliers (articles 6 et 13)</strong></p>
<p>Les revenus et gains provenant de l’exploitation directe de biens immobiliers, y compris les revenus des exploitations agricoles ou forestières et ceux provenant des biens immobiliers d’une entreprise, sont imposables dans l’Etat contractant où ces biens sont situés.</p>
<p><strong>Bénéfices des entreprises (articles 7, 8 et 9)</strong></p>
<p>Sous réserve des dispositions plus spécifiques de la convention, les bénéfices d’une entreprise d’un Etat contractant ne sont imposables que dans cet Etat, à moins que l’entreprise n’exerce son activité dans l’autre Etat contractant par l’intermédiaire d’un établissement stable qui y est situé. Dans ce dernier cas, les bénéfices de l’entreprise sont imposables dans l’autre Etat uniquement dans la mesure où ils sont imputables à l’établissement stable.</p>
<p>En vertu de l’article 5 de la convention, un établissement stable désigne une installation fixe d’affaires par l’intermédiaire de laquelle l’entreprise exerce tout ou partie de son activité. Il peut s’agir d’un siège de direction, d’une succursale, d’un bureau, d’une usine, d’un atelier et d’un lieu d’extraction de ressources naturelles (mine, puits de pétrole ou de gaz, etc.)</p>
<p><strong>Dividendes (article 10)</strong></p>
<p>On entend par "dividendes" les revenus provenant d’actions ou bons de jouissance, parts de mine, parts de fondateur ou autres parts bénéficiaires à l’exception des créances, ainsi que les revenus d’autres parts sociales soumis au même régime fiscal que les revenus d’actions par la législation fiscale de l’Etat contractant dans lequel est située la société distributrice.</p>
<p>Les dividendes payés par une société résidente d’un Etat contractant à un résident de l’autre Etat contractant sont imposables dans cet autre Etat. Ces dividendes peuvent toutefois être aussi imposés dans l’Etat de résidence de la société (imposition maximale de 5% ou 10% suivant les cas).</p>
<p><strong>Intérêts (article 11)</strong></p>
<p>Les intérêts provenant d’un Etat contractant et payés à un résident de l’autre Etat contractant sont imposables dans cet autre Etat.</p>
<p>Toutefois, ces intérêts sont aussi imposables dans l’Etat contractant d’où ils proviennent et selon la législation de cet Etat contractant, mais si le bénéficiaire effectif des intérêts est un résident de l’autre Etat contractant, l’impôt ainsi établi ne peut excéder 10 % du montant brut des intérêts.</p>
<p><strong>Jetons de présence (article 16)</strong></p>
<p>Les jetons de présence et autres rétributions similaires qu’un résident d’un Etat contractant reçoit en sa qualité de membre du conseil d’administration ou de surveillance d’une société résidente de l’autre Etat contractant sont imposables dans cet autre Etat.</p>
<p><strong>Artistes et sportifs (article 17)</strong></p>
<p>Les revenus que les sportifs et professionnels du spectacle (par exemple, artiste de théâtre, de cinéma, de la radio ou de la télévision) résident d’un Etat contractant retirent de leurs activités personnelles exercées dans l’autre Etat contractant sont imposables dans cet autre Etat contractant.</p>
<p>Toutefois, ces revenus sont exonérés d’impôt dans cet autre Etat contractant lorsque ces activités sont financées, pour une part importante, par des fonds publics d’un Etat contractant ou de ses collectivités locales ou par des fonds de leurs personnes morales de droit public ou de leurs organismes sans but lucratif.</p>
<p><strong>Autres revenus (article 22)</strong></p>
<p>Les éléments du revenu d’un résident d’un Etat contractant, d’où qu’ils proviennent, dont ce résident est le bénéficiaire effectif et qui ne sont pas traités dans la convention fiscale ne sont imposables que dans cet Etat contractant.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/fiscalite/article/convention-fiscale). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
