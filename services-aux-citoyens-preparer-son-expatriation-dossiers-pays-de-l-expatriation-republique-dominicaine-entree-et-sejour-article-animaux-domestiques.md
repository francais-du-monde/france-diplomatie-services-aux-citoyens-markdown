# Animaux domestiques

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/entree-et-sejour/article/animaux-domestiques#sommaire_1">Importation de chiens et chats en République Dominicaine</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/entree-et-sejour/article/animaux-domestiques#sommaire_2">Retour en France de chiens, chats, ou autres animaux de compagnie</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Importation de chiens et chats en République Dominicaine</h3>
<p><strong>Exigences zoo sanitaires pour l’importation de chien et de chats en République Dominicaine</strong></p>
<p>Les animaux doivent être accompagnés :</p>
<ul class="spip">
<li><strong>d’un certificat de vaccination</strong><br class="manualbr">L’animal est à jour d’un vaccin antirabique à base de virus inactif. Ce vaccin doit avoir été administré au cours des 12 derniers mois, mais plus de 30 jours avant la date de départ.</li>
<li><strong>d’un certificat de santé établi et signé par un vétérinaire officiel</strong><br class="manualbr">L’animal doit avoir été examiné durant les 72 heures précédant le départ, et déclaré non-porteur de maladie infectieuse ou contagieuse. Il doit être traité contre les parasites externes et internes, et provenir d’une zone où aucune maladie affectant l’espèce n’a été constatée au cours des 60 derniers jours.</li></ul>
<p>Les données suivantes doivent apparaitre sur ces documents :</p>
<ul class="spip">
<li>Nom du propriétaire</li>
<li>Adresse</li>
<li>Nom, race, sexe, couleur et âge de l’animal</li></ul>
<p><strong>Ces documents devront être traduits en espagnol (une traduction libre est possible).</strong></p>
<p>D’autre part :</p>
<ul class="spip">
<li>Les animaux âgés de moins de trois mois nécessitent seulement un certificat de santé établi par un vétérinaire officiel.</li>
<li>L’import de chiens et de chats dans un but commercial requiert l’autorisation de la direction générale de l’élevage de République Dominicaine relative à l’analyse des risques zoo sanitaires.</li></ul>
<p>Pour toute information, n’hésitez pas à consulter le site du <a href="http://www.cnmsf.gob.do/Generalidades/CuarentenaAnimal/RequisitosZoosanitariosdeImportac%C3%B3n/tabid/128/Default.aspx" class="spip_out" rel="external">Comité national pour l’application de mesures sanitaires et phyto-sanitaires (CNMSF)</a></p>
<h3 class="spip"><a id="sommaire_2"></a>Retour en France de chiens, chats, ou autres animaux de compagnie</h3>
<p> Il existe différents cas de figure, en fonction de la nature et de la race de votre animal de compagnie, de son origine, etc. Il est recommandé avant votre départ de consulter les instructions spécifiques établies par les <a href="http://www.douane.gouv.fr/" class="spip_out" rel="external">autorités douanières françaises</a></p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/entree-et-sejour/article/animaux-domestiques). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
