# Déménagement

<p>La voie maritime, la moins coûteuse, est la meilleure solution pour acheminer un déménagement volumineux. En deçà de 500 kilos, l’avion peut être intéressant.</p>
<p>Le délai d’acheminement est de six semaines environ par voie maritime. Pour un container de 20 pieds (25 m3), le coût varie de 8000 à 13 000 euros.</p>
<p><strong>Il est nécessaire de prévoir l’expédition du déménagement après l’arrivée sur place </strong> : en effet le permis de séjour et la carte d’identité sont nécessaires au dédouanement. Un délai d’un mois est à prévoir pour l’obtention de ces documents. Un connaissement et un inventaire détaillé sont également nécessaires.</p>
<p>Pour le retour en France, vous pouvez faire appel à plusieurs transporteurs.</p>
<p>S’agissant d’un déménagement international, il est primordial de s’entourer d’un maximum de garanties en faisant appel à un professionnel disposant de certifications reconnues (marque NF Service, ISO 9002, etc.)</p>
<p><a href="http://www.csdemenagement.fr/" class="spip_out" rel="external">Chambre syndicale du déménagement</a><br class="manualbr">Téléphone : 01 49 88 61 40 <br class="manualbr">Télécopie : 01 49 88 61 46<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/entree-et-sejour/article/demenagement#contact#mc#csdemenagement.fr#" title="contact..åt..csdemenagement.fr" onclick="location.href=mc_lancerlien('contact','csdemenagement.fr'); return false;" class="spip_mail">Courriel</a></p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/entree-et-sejour/article/demenagement). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
