# Animaux domestiques

<p>Pour faire voyager votre animal en Suisse, vous avez besoin des documents suivants :</p>
<ul class="spip">
<li>une <strong>vaccination antirabique </strong>valable</li>
<li>un <a href="http://www.blv.admin.ch/index.html?lang=fr" class="spip_out" rel="external">certificat vétérinaire</a> attestant que, depuis qu’ils sont nés, les jeunes animaux ont été détenus sur leur lieu de naissance, sans avoir de contact avec des animaux sauvages. Le certificat n’est pas nécessaire si les animaux sont accompagnés de leur mère dont ils dépendent encore. Les chiots âgés de moins de 56 jours ne peuvent être importés que s’ils sont accompagnés de leur mère ou de leur nourrice.</li>
<li>une <strong>micro puce ou un tatouage</strong></li>
<li>un <strong>passeport officiel </strong>pour animaux de compagnie de l’UE</li></ul>
<p>Enregistrez votre animal ou vos animaux à la douane suisse et conservez soigneusement les justificatifs relatifs à votre déclaration effectuée en bonne et due forme et, le cas échéant, à l’acquittement de la taxe sur la valeur ajoutée.</p>
<p>L’importation en Suisse de chiens à la queue ou aux oreilles coupées est interdite. Des dérogations sont accordées s’il s’agit d’un déménagement ou de vacances en Suisse. La douane décide si les critères de dérogation sont remplis ou non.</p>
<p>En tant que propriétaire de chien, vous devez déclarer votre animal auprès de votre commune de résidence. Votre vétérinaire doit en outre enregistrer vos animaux dans la base suisse de données sur les chiens. Veuillez également vous renseigner sur les <a href="http://www.blv.admin.ch/index.html?lang=fr" class="spip_out" rel="external">obligations en matière de formation pour les chiens et pour les détenteurs de chiens</a> en Suisse.</p>
<p>Cette réglementation ne s’applique qu’aux animaux <strong>de compagnie</strong> qui sont accompagnés par leur propriétaire. L’introduction sur le territoire suisse d’un animal dans le seul but de le vendre est interdite car il s’agirait alors d’une <a href="http://www.bvet.admin.ch/ein_ausfuhr/?lang=fr" class="spip_out" rel="external">importation commerciale</a>.</p>
<p><i>Mise à jour : février 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/entree-et-sejour/article/animaux-domestiques). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
