# Lettre de motivation

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/emploi-stage/article/lettre-de-motivation-113146#sommaire_1">Rédaction</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/emploi-stage/article/lettre-de-motivation-113146#sommaire_2">Modèles de lettre de motivation</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Rédaction</h3>
<p>Le but de la lettre de motivation est d’expliquer pourquoi vous désirez travailler aux Etats-Unis et pourquoi vous tenez à travailler pour telle ou telle entreprise.</p>
<ul class="spip">
<li>Personnalisez votre lettre autant que possible : faites des recherches sur l’entreprise visée et adressez votre lettre à une personne précise.</li>
<li>La lettre de motivation doit être dactylographiée : une lettre manuscrite pourrait être interprétée comme un manque de rigueur</li>
<li>La lettre doit être succincte : Soyez bref, clair, concis et utilisez des verbes d’action.</li>
<li>Précisez le type de visa que vous avez l’intention de décrocher : ne laissez surtout pas supposer que vous ne vous êtes pas souciés de ces détails administratifs car les lois américaines en la matière sont assez strictes.</li></ul>
<p>La lettre de motivation peut être construite selon le plan suivant :</p>
<ul class="spip">
<li>Présentez-vous brièvement et expliquez comment vous avez connu l’entreprise ou l’emploi souhaité</li>
<li>Montrez votre intérêt dans l’organisation et expliquez comment vos qualifications et votre expérience sera au service du poste recherché</li>
<li>Détaillez vos compétences en évitant les redites avec le CV : mettez en avant vos points forts en vous appuyant ci-possible sur des exemples concrets.</li>
<li>Sollicitez un rendez-vous auprès de votre interlocuteur. Ecrivez votre lettre dans un anglais parfait : attention aux tournures de phrases, à la grammaire, aux faux amis et aux termes britanniques.</li></ul>
<p>Après l’envoi des candidatures, relancez les entreprises par téléphone ou courrier électronique pour vous assurer de la bonne réception du dossier et de jauger l’intérêt qu’il a pu susciter.</p>
<h3 class="spip"><a id="sommaire_2"></a>Modèles de lettre de motivation</h3>
<p>Vous trouverez sur les sites suivants des modèles de lettres qui pourront vous aider dans votre rédaction :</p>
<ul class="spip">
<li><a href="http://jobsearch.about.com/od/coverlettersamples/Sample_Cover_Letters.htm" class="spip_out" rel="external">Jobsearch.about.com</a></li>
<li><a href="http://reslady.com/coverletters.html" class="spip_out" rel="external">Reslady.com</a> (service payant)</li>
<li><a href="http://www.iagora.com/iwork/resumes/letter_model_usa.html" class="spip_out" rel="external">Iagora.com</a></li></ul>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/emploi-stage/article/lettre-de-motivation-113146). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
