# Permis de conduire

<p>Le permis de conduire français est reconnu par convention dans tous les Etats membres de l’Union européenne (UE) ou de l’Espace économique européen (EEE). Par conséquent, vous pourrez circuler dans ces pays avec votre seul permis de conduire français et ce, quelle que soit la durée de votre séjour. Depuis le 1er juillet 1996, l’échange du permis français en permis local n’est plus obligatoire.</p>
<p><strong>Attention </strong> :</p>
<p>Un permis de conduire français obtenu par échange d’un permis de conduire émis par un État hors Union européenne n’est pas automatiquement reconnu par les autres États membres de l’Union européenne. Il convient donc de se rapprocher de l’autorité compétente pour savoir si votre permis de conduire est valable.</p>
<p>Si vous êtes titulaire d’un permis de conduire français en cours de validité et fixez votre "résidence normale" dans un autre État membre de l’Union européenne, celui-ci peut inscrire sur le permis les mentions indispensables à sa gestion et peut appliquer ses dispositions nationales en matière de :</p>
<ul class="spip">
<li>durée de validité du permis de conduire ;</li>
<li>contrôle médical (même périodicité qu’aux détenteurs des permis qu’il délivre) ;</li>
<li>mesures fiscales (liées à la détention d’un permis) ;</li>
<li>sanctions (permis à points, par exemple) ;</li>
<li>restriction, suspension, retrait ou annulation du permis de conduire et peut donc, si nécessaire, procéder à l’échange du permis d’origine.</li></ul>
<p>Le permis de conduire international n’est valable à l’intérieur de l’Union européenne<span class="spip_note_ref"> [<a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/permis-de-conduire/#nb1" class="spip_note" rel="footnote" title="Allemagne, Autriche, Belgique, Bulgarie, Chypre (partie grecque), Croatie, (...)" id="nh1">1</a>]</span> que si vous pouvez produire votre permis de conduire français.</p>
<p>Dans les pays situés hors de l’Espace économique européen (qui comprend l’Union européenne, l’Islande, le Liechtenstein et la Norvège), et à l’exception de la Chine, vous pourrez être autorisé à conduire <strong>temporairement </strong> avec votre permis de conduire français valide <strong>pendant une période allant de trois mois à un an</strong> selon les pays, dès lors que votre titre français est assorti d’un permis de conduire international traduisant en différentes langues les informations qui figurent sur votre titre. Passée cette période transitoire, et pour pouvoir continuer à conduire, vous devrez obtenir le permis local, ou bien par examen, ou bien par échange – si votre pays de résidence figure sur la liste des pays avec lesquels la France pratique l’échange des permis de conduire.</p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/Liste_permis_de_conduire_valables_a_l_echange_01_2014_cle8cc6c4.pdf" class="spip_in" type="application/pdf">Liste des États et autorités dont les permis de conduire sont susceptibles de faire l’objet d’un échange contre un titre français (PDF - 68 ko)</a></li></ul>
<p><retourligne></retourligne></p>
<p><strong>Attention : c’est souvent la nationalité du permis de conduire qui est prise en considération par les autorités du pays d’accueil et non celle de son titulaire</strong>. A titre d’exemple, si vous avez obtenu votre permis de conduire français par échange d’un permis de conduire étranger, il est possible que les conditions de reconnaissance ou d’échange de votre permis de conduire français se réfèrent à la nationalité du permis de conduire étranger d’origine.</p>
<p>Il est vivement conseillé de se renseigner <strong>avant le départ</strong> de France auprès de l’<a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/" class="spip_in">Ambassade ou du consulat</a> du pays d’accueil sur les points suivants :</p>
<ul class="spip">
<li>conditions de reconnaissance dans le pays d’accueil du permis de conduire français, voire du permis de conduire international français ;</li>
<li>conditions d’échange du permis de conduire français en un permis de conduire local ;</li>
<li>le cas échéant, conditions d’échange ou de reconnaissance d’un permis de conduire français obtenu par échange d’un permis de conduire étranger ;</li>
<li>les documents à se procurer avant le départ de France auprès des autorités françaises, à faire légaliser (ou munir de l’apostille) et, le cas échéant, à faire traduire dans la langue du pays d’accueil.</li></ul>
<p>La procédure pour obtenir le <strong>permis de conduire international</strong> est relativement simple. Il convient de se rendre à la préfecture ou sous-préfecture de son domicile muni :</p>
<ul class="spip">
<li>du permis de conduire français,</li>
<li>de deux photos d’identité,</li>
<li>d’une pièce d’identité,</li>
<li>d’un justificatif de domicile.</li></ul>
<p>Le permis de conduire international est délivré gratuitement pour une <strong>durée de trois ans</strong>. Conservez le permis français car sans ce document officiel, le permis international n’a aucune valeur.</p>
<p><strong>Attention : le permis de conduire international n’est délivré qu’aux personnes pouvant justifier d’un domicile en France. </strong></p>
<p><strong>Pour en savoir plus</strong> :</p>
<ul class="spip">
<li><a href="http://vosdroits.service-public.fr/particuliers/N19126.xhtml" class="spip_out" rel="external">Service-public.fr</a></li></ul>
<p><i>Mise à jour : février 2015</i></p>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/permis-de-conduire/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
