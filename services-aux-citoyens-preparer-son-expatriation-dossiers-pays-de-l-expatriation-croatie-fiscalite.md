# Fiscalité

<h2 class="rub22943">Fiscalité du pays</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/fiscalite/#sommaire_1">Présentation </a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/fiscalite/#sommaire_2">Année fiscale</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/fiscalite/#sommaire_3">Coordonnées des centres d’information fiscale</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Présentation </h3>
<p>Depuis le 1er juillet 2013, la Croatie fait partie des Etats membres de l’Union européenne.</p>
<p>La devise locale est la kuna (HRK). Son taux nominal de change est d’environ HRK 7,6 pour 1 euro à fin 2013.</p>
<h4 class="spip">L’impôt sur le revenu des personnes physiques</h4>
<p>En droit interne croate, un résident est imposable sur l’ensemble de ses revenus mondiaux (revenus d’activité salariée, revenus de biens mobiliers et immobiliers, etc.), un non résident n’est assujetti à l’impôt en Croatie qu’à raison de ses revenus de source croate.</p>
<p>Les résidents sont définis comme toute personne physique résidant en permanence en Croatie ou présente en Croatie pendant une période ininterrompue de plus de 183 jours. Aucun régime particulier n’est prévu pour le personnel étranger expatrié en Croatie par des sociétés étrangères.</p>
<p>Tous les contribuables ont droit à un abattement de HRK 2200 par mois, soit HRK 26400 par an. Les <strong>revenus des salaires</strong> sont taxés à la source selon le barème suivant :</p>
<ul class="spip">
<li>taux de départ : 12% Tranche de revenus comprise entre 0 et 26 400 HRK</li>
<li>taux de base : 25% Tranche de revenus comprise entre 26 400 et 105 600 HRK</li>
<li>taux supérieur : 40% Tranche au-dessus de 105 600 HRK</li></ul>
<p>Le salaire mensuel minimal pour un emploi à temps plein (40 heures par semaine) sur la période du 1er juin 2013 au 31 décembre 2013 s’élève à HRK 2985.</p>
<p>Les autres impôts comprennent :</p>
<ul class="spip">
<li>12% pour le <strong>revenu généré par les dividendes</strong> et les parts bénéficiaires de valeurs mobilières, payé après le 1er mars 2012 (sauf pour les dividendes et plus-values obtenus avant le 31 décembre 2000 et pendant la période du 1er janvier 2005 au 28 février 2012, ou pour les dividendes et plus-values obtenus au travers des programmes de distribution d’actions aux employés). Un seuil de non-taxation de HRK 12 000 par an peut être demandé sous forme d’un crédit d’impôt annuel sur l’IRPP ;</li>
<li>12% sur les <strong>revenus issus de la location ou mise à bail</strong> de biens immobiliers (une déduction de 30% est allouée sur le calcul du revenu imposable) ;</li>
<li>12% sur les <strong>revenus d’assurance</strong> (avec des exemptions sur certains schémas) ;</li>
<li>25% sur les <strong>revenus de propriété</strong> et de droits de propriété (ceci comprend les droits d’auteur, les droits industriels, ou le revenu de plus de trois propriétés du même type, ou de plus de trois droits de propriété de même type sur une période de cinq ans) ;</li>
<li>25% sur le revenu issu du don ou des stock-options des actions de sa propre société ; 40% pour les revenus sous forme d’intérêts (toutefois les intérêts perçus des comptes bancaires en devises ou en HRK sont exemptés) ;</li>
<li>Les plus-values réalisées à partir de la vente de valeurs mobilières (actions et autres types de valeurs) sont exemptées de l’IRPP si elles ne sont pas considérées comme l’activité principale de l’assujetti.</li></ul>
<p>Si un contribuable obtient un crédit d’impôt annuel, le total de son revenu est soumis au barème de l’impôt.</p>
<p>Sauf les cas d’exemptions spécifiques, tous les <strong>revenus</strong> <strong>en nature</strong> sont imposables à leur valeur de marché (y compris avec 25% de TVA). Ceci comprend l’utilisation des véhicules professionnels (des règles particulières s’appliquent), les facilités de logement, les prêts dont l’intérêt est inférieur à 3% l’an, les repas. Le montant des revenus de salaire en nature doivent faire l’objet d’une évaluation consolidée pour le calcul de l’IRPP.</p>
<h5 class="spip">Les surtaxes municipales</h5>
<p>Les municipalités peuvent imposer une taxe additionnelle. Pour la capitale, Zagreb, elle s’élève à 18%, et dépend du lieu de résidence principal du contribuable. Elle est calculée sur le montant de l’IRPP.</p>
<h5 class="spip">Les abattements et déductions</h5>
<ul class="spip">
<li>L’abattement personnel de base annuel est de HRK 26 400</li>
<li>L’abattement pour épou(x)se dépendant (e) est de HRK 13 200</li>
<li>Abattement pour le 1er enfant dépendant HRK 13 200</li>
<li>Dons en Croatie à hauteur de 2% des revenus de l’année précédente</li></ul>
<h4 class="spip">Les retraites et pensions</h4>
<p>Les retraites et pensions de source croate sont considérées comme un revenu salarié et imposées en conséquence. Les pensions de source étrangère sont imposables en Croatie comme un revenu salarié, en fonction des dispositions de l’existence éventuelle d’un accord de non-double imposition entre la Croatie et le pays d’origine</p>
<p>A partir du 1er janvier 2013, de nouvelles dispositions sont en place au sein du système des retraites. Les paiements effectués par les employeurs au titre des annuités de retraite de leurs employés au moment de leur retraite ne sont pas considérés comme un revenu, mais les annuités de retraite payées par les compagnies d’assurance seront considérées comme un revenu imposable si les primes d’assurance étaient exemptes d’IRPP.</p>
<h4 class="spip">Les cotisations sociales</h4>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td></td>
<td><strong>Salarié</strong></td>
<td><strong>Employeur </strong><span class="spip_note_ref"> [<a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/fiscalite/#nb3-1" class="spip_note" rel="footnote" title="Basé sur le salaire brut et payé par l’employeur en plus du salaire (...)" id="nh3-1">1</a>]</span></td></tr>
<tr class="row_even even">
<td><strong>Pension de retraite </strong> <strong>(1er pilier) </strong><span class="spip_note_ref"> [<a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/fiscalite/#nb3-2" class="spip_note" rel="footnote" title="Les plafonds salariaux maximaux pour les deux piliers s’appliquent sur une (...)" id="nh3-2">2</a>]</span></td>
<td>15%</td>
<td>-</td></tr>
<tr class="row_odd odd">
<td><strong>Pension de retraite </strong> <strong>(2ème pilier) </strong><span class="spip_note_ref"> [<a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/fiscalite/#nb3-3" class="spip_note" rel="footnote" title="Les plafonds salariaux maximaux pour les deux piliers s’appliquent sur une (...)" id="nh3-3">3</a>]</span></td>
<td>5%</td>
<td>-</td></tr>
<tr class="row_even even">
<td><strong>Assurance santé</strong></td>
<td>-</td>
<td>13%</td></tr>
<tr class="row_odd odd">
<td><strong>Assurance chômage</strong></td>
<td>-</td>
<td>1,5%</td></tr>
<tr class="row_even even">
<td><strong>Assurance accidents du travail</strong></td>
<td>-</td>
<td>0,7%</td></tr>
<tr class="row_odd odd">
<td><strong>TOTAL</strong></td>
<td>20%</td>
<td>15,2%</td></tr>
</tbody>
</table>
<h4 class="spip">La taxe sur la valeur ajoutée</h4>
<ul class="spip">
<li>Le taux standard de la taxe sur la valeur ajoutée est de 25%. Il existe des taux réduits à 10% et 5%.</li>
<li>Le taux à 10% concerne notamment les services de logement, certains journaux et magazines, les huiles et graisses comestibles, les aliments pour nouveau-nés, le sucre blanc raffiné, l’eau (à l’exception des eaux en bouteille), les services de préparation et de service d’aliments, les boissons non alcooliques, le vin et la bière dans les services de restauration.</li>
<li>Le taux à 5% s’applique au pain et au lait, à certains livres et magazines, aux spectacles, aux films, et aux journaux (dans certaines conditions).</li></ul>
<p>La TVA d’entrée ne peut être récupérée sur :</p>
<ul class="spip">
<li>L’achat ou la location de voitures, d’avions, de bateaux ou d’autres moyens de transport personnel et les dépenses afférentes.</li>
<li>Les frais de divertissement ou de spectacle.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Année fiscale</h3>
<p>L’année fiscale s’étend du 1er janvier au 31 décembre. Le délai pour la déclaration des revenus est fixé au dernier jour de février de l’année N pour les revenus de l’année N – 1.</p>
<p>Les impôts imputables aux salariés sont retenus à la source par l’employeur. Pour les non salariés et les sociétés, il convient de remplir une déclaration et de prendre l’attache des services compétents.</p>
<h3 class="spip"><a id="sommaire_3"></a>Coordonnées des centres d’information fiscale</h3>
<p><strong>Bureau Régional de Zagreb « PODRUZNI URED ZAGREB »</strong><br class="manualbr">Avenija Dubrovnik n° 22, Zagreb<br class="manualbr">Téléphone : 01 650 11 11</p>
<p><strong>Ministère des Finances</strong><br class="manualbr">Rue Katanciceva n° 5, Zagreb<br class="manualbr">Téléphone : 01 459 13 33</p>
<p><strong>Pour en savoir plus :</strong></p>
<p>Service économique de l’ambassade de France<br class="manualbr">Preradoviceva 35/1<br class="manualbr">10000 Zagreb <br class="autobr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/fiscalite/#zagreb#mc#dgtresor.gouv.fr#" title="zagreb..åt..dgtresor.gouv.fr" onclick="location.href=mc_lancerlien('zagreb','dgtresor.gouv.fr'); return false;" class="spip_mail">Courriel</a></p>
<p><i>Mise à jour : janvier 2014</i></p>
<p><span class="spip_note_ref">[<a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/fiscalite/#nh3-1" class="spip_note" title="Notes 3-1" rev="footnote">1</a>] </span>Basé sur le salaire brut et payé par l’employeur en plus du salaire brut.</p>
<p><span class="spip_note_ref">[<a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/fiscalite/#nh3-2" class="spip_note" title="Notes 3-2" rev="footnote">2</a>] </span>Les plafonds salariaux maximaux pour les deux piliers s’appliquent sur une base mensuelle et pour le Pilier 1 sur une base annuelle.</p>
<p><span class="spip_note_ref">[<a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/fiscalite/#nh3-3" class="spip_note" title="Notes 3-3" rev="footnote">3</a>] </span>Les plafonds salariaux maximaux pour les deux piliers s’appliquent sur une base mensuelle et pour le Pilier 1 sur une base annuelle.</p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-croatie-fiscalite-article-convention-fiscale.md" title="Convention fiscale">Convention fiscale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-croatie-fiscalite-article-fiscalite-du-pays-110893.md" title="Fiscalité du pays">Fiscalité du pays</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/fiscalite/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
