# Communications

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/vie-pratique/article/communications#sommaire_1">Téléphone – Internet</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/vie-pratique/article/communications#sommaire_2">Poste</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Téléphone – Internet</h3>
<p>Depuis la France, cela coûte le même prix de téléphoner vers un téléphone portable singapourien que vers un téléphone fixe singapourien. Le prix varie en fonction de l’opérateur.</p>
<ul class="spip">
<li>Pour les personnes ne souscrivant pas de contrat de téléphone, mais utilisant une carte prépayée, la carte sera également débitée quand des appels seront reçus.</li>
<li>Pour les personnes en <i>Long Term Social Visit Pass</i>, il n’est pas possible de souscrire un contrat de téléphone à son nom, il faut utiliser une carte prépayée.</li></ul>
<h4 class="spip">Téléphone fixe</h4>
<p><strong>Indicatif téléphonique :</strong> 00[65] + N° de téléphone à huit chiffres</p>
<h5 class="spip">Ouverture d’une ligne de téléphone</h5>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.singtel.com/" class="spip_out" rel="external">Singapore Telecom (SingTel)</a> est l’organe national auprès duquel vous demandez l’ouverture des lignes de téléphone.</p>
<p>Mais SingTel n’a plus le monopole du téléphone international. La concurrence étant importante, il propose des programmes variés avec des prix différents.</p>
<p>Pour faire une demande, composez le 169 pour une ligne privée ou le 166 pour une ligne professionnelle. Depuis le 1er mars 2002, Singapour est passé à des numéros de téléphone à huit chiffres.</p>
<p>Pour installer une ligne téléphonique, vous devez vous rendre dans un bureau SingTel, muni de votre passeport et de votre visa de travail. Il vous sera demandé une caution. Pour l’ouverture de lignes supplémentaires, la caution n’est pas nécessaire.</p>
<p>Il existe de nombreux opérateurs de téléphonie offrant des services à prix réduits pour les appels vers l’étranger. Il est conseillé de comparer les offres qui changent régulièrement. Les principaux opérateurs sont :</p>
<ul class="spip">
<li><a href="http://www.singtel.com/" class="spip_out" rel="external">Singtel</a></li>
<li>GlobalCom - Tél. pour renseignements : 64822900</li>
<li><a href="http://www.sunpage.com.sg/" class="spip_out" rel="external">Sunpage</a></li></ul>
<p>Vous pouvez utiliser ces compagnies pour vos communications internationales (pour appeler mais aussi pour se faire appeler à Singapour).</p>
<h5 class="spip">Prises téléphoniques</h5>
<p>Les prises de téléphone singapouriennes sont identiques aux prises de téléphone françaises. Vous pouvez donc, si vous le souhaitez, apporter votre téléphone de France. Attention, s’il nécessite aussi un branchement électrique, vous aurez besoin d’un adaptateur mural.</p>
<h5 class="spip">Lignes individuelles ou partagées</h5>
<p>Toutes les lignes au sein d’un même domicile sont au nom du détenteur du permis de travail. Il est possible d’installer plusieurs lignes de téléphone dans un même domicile mais le détenteur du permis de travail donc le détenteur également des lignes est le seul à pouvoir faire la demande de lignes multiples.</p>
<h4 class="spip">Téléphone mobile</h4>
<p>Si vous importez votre téléphone portable de France, vous devez au préalable le faire débloquer afin qu’il puisse fonctionner avec les puces singapouriennes.</p>
<p>Il existe plusieurs opérateurs de téléphonie mobile. Les plus importants sont :</p>
<ul class="spip">
<li><a href="http://www.singtel.com/" class="spip_out" rel="external">Singapore Telecom</a></li>
<li><a href="http://www.starhub.com/" class="spip_out" rel="external">StarHub Pte Ltd</a></li>
<li><a href="http://www.m1.com.sg/" class="spip_out" rel="external">MobileOne (M1)</a></li></ul>
<h4 class="spip">Réseau Internet</h4>
<p>Les fournisseurs d’accès à Internet sont très nombreux.</p>
<p>Le réseau Internet à haut débit est de très bonne qualité. Il est souvent possible de se connecter sans fil (wireless) dans les lieux publics, les cafés ou les restaurants. Il existe également des cafés Internet bon marché au centre ville ainsi que quelques ordinateurs en libre accès à l’Aéroport International Changi.</p>
<p>Téléphoner gratuitement par Internet Il est généralement très avantageux d’utiliser <strong>des logiciels de téléphonie sur IP</strong> (Skype, Google talk, Live messenger, Yahoo ! messenger, etc.) car les tarifs proposés à l’international sont souvent moindre que ceux des opérateurs locaux exploitant des lignes classiques, voire gratuits dans certaines conditions.</p>
<h3 class="spip"><a id="sommaire_2"></a>Poste</h3>
<p>Il existe une soixantaine de bureaux de poste, au moins un par quartier et dans les centres commerciaux. La liste peut être consultée dans l’annuaire téléphonique « Business ».</p>
<p>Les <strong>heures d’ouverture</strong> sont en général :</p>
<ul class="spip">
<li>Du lundi au vendredi : 8h30 - 17h.</li>
<li>Le samedi : 8h30 – 13h.</li></ul>
<p>Certains bureaux ont des horaires particuliers (Killiney road est ouvert du lundi au samedi de 9h à 21h et le dimanche de 9h à 16h30. A l’aéroport, le bureau de poste est ouvert en continu).</p>
<p>Le courrier est distribué deux fois par jour en centre ville et dans la région industrielle de Jurong, mais une seule fois ailleurs.</p>
<p>On trouve de nombreuses <strong>boîtes aux lettres</strong> en ville. Elles sont blanches ou grises métallisées.</p>
<p>En plus des services postaux classiques, les bureaux vendent : cartes postales, petits cadeaux, cartes téléphoniques, coupons de parking publics, renouvellement de permis, tout le nécessaire pour les colis et les emballages (boîtes, ficelles, adhésifs).</p>
<p>Vous pouvez vous procurer la liste des <strong>tarifs d’affranchissement </strong>à la poste.</p>
<ul class="spip">
<li>Le tarif normal pour une lettre pour Singapour est : 0,26 SGD.</li>
<li>Le tarif normal pour une lettre pour la France est : 1,10 dollar.</li></ul>
<p>Les <strong>liaisons postales</strong> sont assurées avec de bonnes garanties de réception, dans un délai de 5 à 10 jours dans le sens France-Singapour et un délai de 7 à 10 jours dans le sens Singapour-France.</p>
<p>Vous pouvez aussi faire appel aux <strong>services de courrier express</strong>.</p>
<p>Pour rechercher un numéro de <strong>code postal ou tout renseignement relatif aux services postaux de Singapour </strong> :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.singpost.com/" class="spip_out" rel="external">Singpost.com</a></p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/vie-pratique/article/communications). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
