# Scolarisation

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/vie-pratique/article/scolarisation#sommaire_1">Le primaire et le secondaire</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/vie-pratique/article/scolarisation#sommaire_2">L’enseignement supérieur</a></li></ul>
<p>En tant qu’expatrié au Portugal, il est possible de scolariser ses enfants dans l’un des deux établissements français du pays faisant partie du réseau mondial de l’Agence pour l’enseignement français à l’étranger.</p>
<p>Pour toute information sur la scolarisation dans le système français à l’étranger, vous pouvez consulter nos thématiques sur les <a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures.md" class="spip_in">Etudes supérieures</a> et la <a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais.md" class="spip_in">Scolarité en français</a>.</p>
<h3 class="spip"><a id="sommaire_1"></a>Le primaire et le secondaire</h3>
<h4 class="spip">Les lycées français</h4>
<p>A Lisbonne, le <a href="http://lfcl-lisbonne.eu/" class="spip_out" rel="external">lycée français Charles Lepierre</a>, fondé en 1952, accueille aujourd’hui plus de 1750 élèves de la petite section de maternelle à la terminale (950 d’entre eux fréquentent le primaire, et 800 le secondaire). La part des élèves de nationalité française est de 63 %, et celle des élèves disposant de la double nationalité portugaise et française est de 10 %.</p>
<p>Créé en 1963 à Porto, le <a href="http://www.lyceefrancaisdeporto.pt/fr/" class="spip_out" rel="external">lycée Marius Latour</a> reçoit pour sa part plus de 900 élèves de tous les âges (de la maternelle à la terminale).</p>
<p>Ces institutions appliquent le contenu du programme français et suivent un rythme scolaire identique à celui de notre pays. Les cours débutent ainsi au mois de septembre et s’achèvent à la fin du mois de juin.</p>
<h5 class="spip">Enseignements</h5>
<p>Les enseignements sont dispensés en français et suivent les mêmes lignes directrices que les cours dont bénéficient les élèves français. Des cours de portugais sont prodigués. La pluralité des langages utilisés vise à permettre l’acquisition d’une double culture et prépare efficacement à l’intégration des systèmes d’enseignement supérieur français, portugais, ou internationaux.</p>
<p>Les établissements français sont des établissements d’excellence, où l’attention portée aux élèves permet un apprentissage dans les meilleures conditions. Ils préparent leurs élèves au passage des examens nationaux français que sont le brevet des collèges et le baccalauréat. Les élèves des classes de première et de terminale retrouveront les mêmes filières que leurs homologues nationaux.</p>
<h5 class="spip">Frais de scolarité</h5>
<p>Ces établissements sont payants. Les frais de scolarité demandés varient en fonction de l’âge, de la classe et de la nationalité de l’enfant. Ils sont réclamés de manière trimestrielle, et oscillent entre 1000 euros (pour un enfant de maternelle de nationalité communautaire) et 2000 euros (pour un élève de terminal de nationalité extracommunautaire).</p>
<p>Le <a href="http://lfcl-lisbonne.eu/" class="spip_out" rel="external">lycée Charles Lepierre de Lisbonne</a> prévoit le paiement de 800 euros non remboursables pour toute première inscription. Il interdit aussi à un élève quittant la structure de s’y réinscrire avant la deuxième année suivant son départ.</p>
<h5 class="spip">Services</h5>
<p>Des services de demi-pension existent dans les deux lycées français au Portugal, de même que plusieurs activités extrascolaires (sports, arts, etc.).</p>
<p>Le <a href="http://www.lyceefrancaisdeporto.pt/fr/" class="spip_out" rel="external">lycée Marius Latour de Porto</a> offre des services de ramassage scolaire à ses étudiants. Il signe aussi des accords avec des familles qui peuvent accueillir un élève contre rémunération pour une durée déterminée.</p>
<p>Le lycée français de Lisbonne offre quant à lui des services de garderie et d’étude surveillée payants. La structure veille ainsi sur les élèves de maternelle en dehors des horaires scolaires et accueille tous les autres étudiants après les cours.</p>
<h5 class="spip">Inscription</h5>
<p>Pour scolariser vos enfants dans l’un de ces deux établissements, il convient de présenter une demande aux services administratifs compétents. Un dossier est à remplir et des justificatifs de résidence ou de résidence, de nationalité et de situation professionnelle parentale sont à fournir. Le tout devra être remis aux services administratifs des lycées, idéalement avant le début de chaque année scolaire. Des inscriptions en cours d’années, bien que plus compliquées, restent possibles.</p>
<h5 class="spip">Bourses</h5>
<p>Des bourses octroyées par l’Etat français peuvent être allouées aux familles au regard de critères sociaux et économiques. Il incombe à chaque famille de se renseigner et de remplir les dossiers en fournissant les justificatifs nécessaires. Pour plus de renseignements :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.ambafrance-pt.org/" class="spip_out" rel="external">Ambassade de France au Portugal</a> </p>
<h4 class="spip">L’enseignement à distance</h4>
<p>Il est possible de suivre le programme scolaire français par correspondance. Le <a href="http://www.cned.fr/" class="spip_out" rel="external">Centre national des études à distance (CNED)</a> permet à tout élève, quel que soit son niveau scolaire, de suivre les enseignements français grâce à des supports écrits et multimédias. Les formations sont payantes et reconnues par le système scolaire français.</p>
<h3 class="spip"><a id="sommaire_2"></a>L’enseignement supérieur</h3>
<h4 class="spip">Structure de l’enseignement supérieur au Portugal</h4>
<p>L’enseignement supérieur au Portugal repose sur deux types d’institutions publiques ou privées : les Universités et les Instituts supérieurs polytechniques. Le Portugal compte aujourd’hui :</p>
<ul class="spip">
<li><strong>14 universités publiques</strong></li>
<li><strong>2 établissements supérieurs à statut spécifique</strong> : l’Université catholique portugaise, établissement concordataire, et l’Institut supérieur des sciences du travail et de l’entreprise, spécialisé en économie, sciences humaines et sociales et gestion, qui siègent au Conseil des recteurs des universités portugaises avec les universités publiques</li>
<li><strong>17 universités privées</strong></li>
<li>un réseau dense d’<strong>instituts polytechniques</strong>, qui proposent des formations spécialisées dans différents domaines tels que l’ingénierie, l’agriculture, la gestion, l’éducation.</li></ul>
<p>Le cadre de Bologne (LMD) est effectif pour l’ensemble des cursus universitaires et polytechniques à partir de l’année universitaire 2009/2010, s’articulant entre :</p>
<ul class="spip">
<li>La licence (licenciatura) - trois ans</li>
<li>Le master (mestrado) - cinq ans</li>
<li>Le doctorat (doutoramento) - huit ans</li></ul>
<p>Le cadre traditionnel de formation perdure toutefois dans les domaines de la santé (médecine, dentaire, vétérinaire) et de l’architecture. Une formation intégrée « Licence + Master » est mise en place dans certaines filières de formation d’ingénieur.</p>
<h4 class="spip">Admission et inscription</h4>
<p>L’accès à l’enseignement supérieur se fait à l’issue des études secondaires (12ème année, équivalent de la terminale). Il n’est pas automatique et reste conditionné à un concours obligatoire, comprenant une à trois matières selon la filière. En effet, les places dans chaque filière et université sont limitées et attribuées en fonction des résultats à ce concours (et aux moyennes obtenues en 12e année).</p>
<p><strong>A noter</strong> : les filières les plus compétitives sont médecine (une moyenne générale de plus de 18/20 est demandée), droit, architecture et études d’ingénierie.</p>
<h4 class="spip">Coûts de scolarité</h4>
<p>Les frais d’inscription sont déterminés par chaque établissement et varient d’une filière d’études à l’autre. Il faut compter de 700 à 1000 euros de droits d’inscription dans les établissements publics et de 3500 à 5000 euros dans les établissements privés.</p>
<h4 class="spip">Aides financières</h4>
<p>Des aides financières existent. Pour en prendre connaissance, voici une liste de liens utiles :</p>
<ul class="spip">
<li><a href="http://www.instituto-camoes.pt/" class="spip_out" rel="external">Institut Camões</a></li>
<li><a href="http://www.gulbenkian-paris.org/" class="spip_out" rel="external">Fondation Calouste Gulbenkian</a></li></ul>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://alliancefr.pt/" class="spip_out" rel="external">Alliance française au Portugal</a> ;</li>
<li><a href="http://www.epefrance.org/component/option,com_deeppockets/task,catShow/id,1/Itemid,38/" class="spip_out" rel="external">Apprendre le portugais en France</a> ;</li>
<li><a href="http://lfcl-lisbonne.eu/" class="spip_out" rel="external">Lycée français Charles Lepierre</a> ;</li>
<li><a href="http://www.lyceefrancaisdeporto.pt/fr/" class="spip_out" rel="external">Lycée français Marius Latour</a> ;</li>
<li><a href="http://www.dges.mctes.pt/DGES/pt" class="spip_out" rel="external">Direction générale de l’enseignement supérieur</a> ;</li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/vie-pratique/article/scolarisation). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
