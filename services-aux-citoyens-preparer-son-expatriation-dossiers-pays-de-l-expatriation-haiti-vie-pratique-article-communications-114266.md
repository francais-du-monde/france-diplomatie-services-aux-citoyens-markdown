# Communications

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/vie-pratique/article/communications-114266#sommaire_1">Téléphone – Internet</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/vie-pratique/article/communications-114266#sommaire_2">Poste</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Téléphone – Internet</h3>
<p>Les liaisons téléphoniques sont aléatoires entre la France et Haïti.</p>
<p>On trouve des <a href="http://www.cybercafe.com/" class="spip_out" rel="external">cybercafés</a> dans la capitale. Les liaisons Internet, bien qu’en amélioration, restent lentes.</p>
<p>Il existe deux opérateurs principaux de téléphonie mobile auprès desquels vous pouvez souscrire des abonnements Internet :</p>
<ul class="spip">
<li>Digicel ;</li>
<li>Natcom.</li></ul>
<p><strong>Téléphoner gratuitement par Internet </strong></p>
<p>Il est généralement très avantageux d’utiliser <strong>des logiciels de téléphonie sur IP</strong> (Skype, Google talk, Live messenger, Yahoo ! messenger, etc.) car les tarifs proposés à l’international sont souvent moindre que ceux des opérateurs locaux exploitant des lignes classiques, voire gratuits dans certaines conditions.</p>
<h3 class="spip"><a id="sommaire_2"></a>Poste</h3>
<p>L’acheminement du courrier requiert au minimum deux voire trois semaines. La poste se montre peu fiable pour l’international, notamment concernant les colis.</p>
<p>Le recours à des prestataires privés peut être utile. Compter trois semaines pour un chronopost international normal et une semaine pour un chronopost international express.</p>
<p><i>Mise à jour : août 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/vie-pratique/article/communications-114266). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
