# Convention fiscale

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/fiscalite-22794/article/convention-fiscale-109807#sommaire_1">Analyse de la convention franco-belge</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/fiscalite-22794/article/convention-fiscale-109807#sommaire_2">Modalités d’imposition des revenus catégoriels</a></li></ul>
<p>La France et la Belgique ont signé, le 10 mars 1964, une convention en vue d’éviter les doubles impositions et d’établir des règles d’assistance administrative et juridique réciproque en matière d’impôts sur les revenus. Elle a été publiée au Journal Officiel le 15 août 1965.</p>
<p>Cette convention a été modifiée par l’avenant du 15 février 1971 publié au Journal Officiel du 6 décembre1973 et par l’avenant du 8 février 1999 publié par décret du 16 juin 2000.</p>
<p>Une convention internationale ayant primauté sur la loi interne, les dispositions de la loi du 29 décembre 1976 modifiant les règles de territorialité et les conditions d’imposition des Français de l’étranger (Journal Officiel du 30 décembre 1976) ne sont applicables que si elles ne contreviennent pas aux dispositions de la convention.</p>
<p>Avant l’analyse de ce texte et les modalités d’imposition des différents revenus catégoriels, vous trouverez ci-après des informations relatives à vos obligations vis-à-vis du service des impôts français. La dernière partie de ce chapitre est consacrée à la fiscalité applicable en Belgique.</p>
<h3 class="spip"><a id="sommaire_1"></a>Analyse de la convention franco-belge</h3>
<p>Cette convention tend à éviter les doubles impositions qui pourraient résulter de l’application des législations des deux Etats, d’établir des règles d’assistance administrative et juridique réciproque en matière d’impôts sur les revenus et de régler certaines autres questions en matière fiscale.</p>
<p>Le texte de la convention et de son avenant peut être obtenu à la Direction des Journaux Officiels :</p>
<ul class="spip">
<li>par courrier : 26 rue Desaix, 75727 PARIS Cedex 15 ;</li>
<li>ou sur le site Internet du <a href="http://www.impots.gouv.fr/portal/deploiement/p1/fichedescriptive_1425/fichedescriptive_1425.pdf" class="spip_out" rel="external">ministère des finances</a></li></ul>
<p>Les dispositions conventionnelles qui ont primauté sur les dispositions du droit interne, selon l’article 55 de la Constitution française répartissent entre les deux Etats, le droit d’imposer les revenus pour leurs résidents respectifs.</p>
<h4 class="spip">Champ d’application de la convention</h4>
<p>Cet accord a pour objet de protéger les résidents de chacun des Etats contractants contre les doubles impositions.</p>
<p>La convention trouve donc à s’appliquer aux résidents de ces Etats.</p>
<h5 class="spip">Notion de résidence</h5>
<p>L’article 1, paragraphe 2 de la convention précise qu’une personne physique est réputée résident de l’Etat contractant où elle dispose d’un foyer permanent d’habitation.</p>
<p>Le paragraphe 2 du même article énumère des critères subsidiaires permettant de résoudre le cas de double résidence si l’alinéa précédent ne pouvait suffire.</p>
<p>Ces critères sont :</p>
<ul class="spip">
<li>le foyer d’habitation permanent ;</li>
<li>le centre de ses intérêts vitaux ;</li>
<li>le lieu de séjour habituel ;</li>
<li>à défaut, l’Etat dont elle possède la nationalité.</li></ul>
<p>Si une personne possède la double nationalité ou si elle ne possède la nationalité d’aucun d’eux, la question est tranchée d’un commun accord par les autorités des deux Etats contractants (article 1, paragraphe 2d).</p>
<h4 class="spip">Elimination de la double imposition</h4>
<p>La double imposition est évitée selon un dispositif prévu par l’article 19 de la Convention.</p>
<p>L’article A2 exonère d’impôts belges les revenus, autres que ceux cités au paragraphe A1, lorsque la convention en attribue exclusivement l’imposition à la France. Cependant, la Belgique peut calculer les revenus imposables en Belgique au taux correspondant à l’ensemble des revenus (belges et français).</p>
<p>L’article B2 exonère d’impôts français les revenus, autres que ceux cités au paragraphe B1, lorsque la convention en attribue exclusivement l’imposition à la Belgique. Cependant, la France peut calculer les revenus imposables en France au taux correspondant à l’ensemble des revenus (français et belges).</p>
<p>Aux résultats ainsi obtenus peuvent être appliquées les réfactions prévues par la loi interne de chaque pays.</p>
<h3 class="spip"><a id="sommaire_2"></a>Modalités d’imposition des revenus catégoriels</h3>
<h4 class="spip">Traitements, salaires, pensions et rentes</h4>
<h5 class="spip">Rémunérations privées</h5>
<p><strong>Principe</strong></p>
<p>L’article 11, paragraphe 1, précise que les traitements et salaires d’origine privée ne sont, en règle générale, imposables que dans l’Etat où s’exerce l’activité personnelle.</p>
<p><strong>Exceptions à cette règle générale</strong></p>
<p><strong>1</strong>) Le maintien de l’imposition dans l’Etat de résidence du bénéficiaire est prévu par le paragraphe 2-a du même article sous réserve de trois conditions simultanément remplies :</p>
<ul class="spip">
<li>le séjour temporaire du bénéficiaire dans l’autre Etat ne dépasse pas une durée totale de 183 jours au cours de l’année civile ;</li>
<li>la rémunération est payée par un employeur qui est résident de l’Etat de résidence ;</li>
<li>la rémunération ne doit pas être à la charge d’un établissement stable ou d’une installation fixe de l’employeur dans l’Etat d’exercice.</li></ul>
<p><strong>2</strong>) Il résulte des dispositions du paragraphe 2-B de l’article 11 de la convention que les rémunérations des salariés autres qu’intérimaires employés à bord d’un navire, d’un aéronef en trafic international ou à bord d’un bateau servant à la navigation intérieure sur le territoire des deux Etats contractants ne sont imposables que dans l’Etat où se trouve le siège de la direction effective de l’entreprise.</p>
<p><strong>3</strong>) Cas des frontaliers</p>
<p>Il s’agit de salariés justifiant de cette qualité qui ont leur foyer permanent d’habitation dans la zone frontalière d’un Etat, exercent leur activité professionnelle dans la zone frontalière de l’autre Etat et retournent <strong>chaque jour</strong> dans le premier Etat.</p>
<p>La zone frontalière de chaque Etat comprend les communes situées à une distance de la frontière n’excédant pas 20 km.</p>
<p><strong>Frontaliers demeurant en Belgique et travaillant en France</strong></p>
<p>Pour obtenir l’exonération de l’impôt français, les travailleurs frontaliers résidant en Belgique doivent souscrire une demande formulée sur un imprimé bilingue n° 5206.</p>
<p>Cet imprimé peut être obtenu au siège de chaque Direction des services fiscaux intéressée.</p>
<p>Le travailleur frontalier remplissant sa déclaration doit certifier réunir les conditions requises pour bénéficier de l’exonération de l’impôt français. Il doit obtenir le visa de son employeur ainsi qu’une attestation. Le premier exemplaire est remis aux services belges dont il relève, le deuxième à son employeur français.</p>
<p>La demande est renouvelée chaque année.</p>
<p><strong>Frontaliers demeurant en France et travaillant en Belgique</strong></p>
<p>Pour obtenir l’exonération de l’impôt belge, les travailleurs frontaliers résidant en France doivent souscrire une demande sur l’imprimé portant le numéro 276 Front (F).</p>
<p>Ces imprimés peuvent être obtenus auprès des services de taxation belges.</p>
<p><strong>Régime d’imposition</strong></p>
<p>Selon les dispositions de l’article 11, paragraphe 2-c, les revenus provenant de l’activité salariée des travailleurs frontaliers ne sont imposables que dans l’Etat dont ils sont résidents. Ce régime fiscal doit être accordé à tout titulaire de la carte frontalière en vigueur dans le cadre de ces dispositions.</p>
<h5 class="spip">Rémunérations publiques</h5>
<p><strong>Principe</strong></p>
<p>L’article 10, paragraphe 1, indique que les traitements, salaires et rémunérations analogues ainsi que les pensions de retraite payés par un Etat ou une personne morale de droit public de cet Etat restent imposables dans cet Etat.</p>
<p><strong>Exceptions</strong></p>
<p>Toutefois, en vertu de l’article 10, paragraphe 3, cette règle ne s’applique pas lorsque le bénéficiaire possède la nationalité de l’autre Etat sans être en même temps ressortissant de l’Etat payeur ; l’imposition est réservée à l’Etat dont l’intéressé est le résident.</p>
<p>D’autre part, en vertu des dispositions du paragraphe 2 du même article, les règles fixées aux paragraphes 1 et 4 dudit article ne sont pas applicables aux rémunérations ou aux pensions versées au titre de services rendus dans le cadre d’une activité industrielle ou commerciale exercée par un Etat ou une personne morale de droit public, en principe et sous réserve d’accords particuliers.</p>
<p>Les sommes versées à ce titre sont imposées soit dans l’Etat d’exercice de l’activité (article 11 de la convention), soit dans l’Etat de résidence du bénéficiaire (article 12 de la convention).</p>
<h5 class="spip">Pensions et rentes</h5>
<p>L’article 12 prévoit que les pensions de retraite de source privée ainsi que les rentes viagères restent imposables dans l’Etat dont le bénéficiaire est un résident. Cette disposition couvre également les pensions publiques auxquelles les règles de l’article 10 ne sont pas applicables.</p>
<h5 class="spip">Etudiants, apprentis</h5>
<p>L’article 14 de la convention prévoit que les étudiants, les apprentis d’un Etat qui séjournent dans l’autre Etat à seule fin d’y poursuivre leurs études ou leur formation sont exonérés d’impôt par ce dernier Etat si les sommes reçues sont de source étrangère.</p>
<h5 class="spip">Enseignants</h5>
<p>L’article 13 précise que les rémunérations versées aux enseignants résidents d’un Etat se rendant temporairement dans l’autre Etat exclusivement en vue d’y exercer une activité pédagogique pendant une période <strong>ne dépassant pas deux ans</strong>, dans une université, un collège, une école ou autre établissement d’enseignement restent imposables dans l’Etat de résidence.</p>
<h4 class="spip">Autres catégories de revenus</h4>
<h5 class="spip">Bénéfices industriels et commerciaux</h5>
<p>L’article 4, paragraphe 1, dispose que les bénéfices industriels et commerciaux ne sont imposables que dans l’Etat où se trouve l’établissement stable dont ils proviennent.</p>
<p>Bénéfices des professions libérales et des revenus non commerciaux</p>
<p>L’article 7, paragraphe 1, dispose que les revenus provenant de l’exercice d’une profession libérale ou d’autres activités indépendantes sont imposables dans l’Etat sur le territoire duquel se trouve l’installation fixe où s’exerce de façon régulière l’activité personnelle.</p>
<p>L’article 8, paragraphe 1, pose en principe que les revenus non commerciaux (redevances et droits d’auteur) sont imposables dans l’Etat de résidence du bénéficiaire, sous couvert de la production de formulaires spécifiques remis auprès des autorités fiscales de l’Etat dont relève le créancier des revenus.</p>
<h5 class="spip">Revenus immobiliers</h5>
<p>L’article 3, paragraphe 1, dispose que les revenus des biens immobiliers y compris les bénéfices des exploitations agricoles sont imposables dans l’Etat où ils sont situés.</p>
<h5 class="spip">Revenus de capitaux mobiliers</h5>
<p><strong>Les dividendes</strong></p>
<p>L’article 15, paragraphe 5, définit les dividendes comme les revenus provenant d’actions, actions ou bons de jouissance, parts de mines, parts de fondateur ou autres parts bénéficiaires ainsi que les revenus d’autres parts sociales soumis au même régime que les revenus d’actions par la législation fiscale de l’Etat dont la société distributrice est un résident.</p>
<p>En principe, le paragraphe 1 du même article dispose que les dividendes payés par une société d’un Etat à un résident de l’autre Etat sont imposables dans l’Etat de résidence du bénéficiaire.</p>
<p>Toutefois et sous réserve du paragraphe 3, le paragraphe 2 maintient au profit de l’Etat de résidence de la société qui paie les dividendes, le droit d’imposer à un taux ne pouvant dépasser 15%. Ce taux est limité à 10% si le bénéficiaire des dividendes est une société qui a la propriété exclusive d’au moins 10% du capital de la société distributrice depuis le début du dernier exercice social de celle-ci clos avant la distribution.</p>
<p>Le paragraphe 3 prévoit que le paiement de l’avoir fiscal attaché aux dividendes distribués par les sociétés françaises est accordé aux personnes physiques résidentes de la Belgique, après déduction de la retenue à la source au taux de 15% sur les dividendes distribués augmentés de l’avoir fiscal.</p>
<p>Le paragraphe 4 précise que le précompte mobilier acquitté par la société distributrice résidente de France, est remboursé aux sociétés bénéficiaires résidentes de Belgique qui n’ont pas droit à l’avoir fiscal après déduction de la retenue à la source dont le taux est fixé pour ces dividendes au paragraphe 2 (cf. ci-dessus).</p>
<p>Les dispositions des paragraphes 1 à 4 ne s’appliquent pas conformément aux termes du paragraphe 6, lorsque le bénéficiaire des dividendes résident d’un Etat a dans l’autre Etat d’où proviennent les dividendes, un établissement stable auquel se rattache la distribution de ces revenus.</p>
<p><strong>Les intérêts</strong></p>
<p>L’article 16, paragraphe 1, dispose que les intérêts et produits d’obligations, ou autres titres d’emprunts négociables, de bons de caisse, de prêts, dépôts et toutes autres créances, sont imposables dans l’Etat dont le bénéficiaire est résident.</p>
<p>Toutefois, le paragraphe 3 prévoit que l’Etat, source des revenus, conserve le droit de les imposer à un taux ne pouvant excéder 15%.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/fiscalite-22794/article/convention-fiscale-109807). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
