# Transports

<p class="chapo">
      En cas d’accident grave, il convient de composer le 100 (pompiers), le 101 (police) ou le 112, numéro d’urgence européen.
</p>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/vie-pratique/article/transports#sommaire_1">Importation </a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/vie-pratique/article/transports#sommaire_2">Permis de conduire</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/vie-pratique/article/transports#sommaire_3">Code de la route </a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/vie-pratique/article/transports#sommaire_4">Assurances et taxes </a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/vie-pratique/article/transports#sommaire_5"> Achat </a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/vie-pratique/article/transports#sommaire_6">Immatriculation</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/vie-pratique/article/transports#sommaire_7">Entretien</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/vie-pratique/article/transports#sommaire_8">Réseau routier </a></li>
<li><a id="so_9" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/vie-pratique/article/transports#sommaire_9">Transport en commun</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Importation </h3>
<p>Pour importer un véhicule de France, il convient de présenter un certificat de conformité établi par le constructeur et réclamer une vignette 705 (attestation de dédouanement) au service des Douanes belge, permettant l’immatriculation des véhicules neufs et/ou importés.</p>
<p><a href="http://fiscus.fgov.be/interfdanl/fr/index.htm" class="spip_out" rel="external">Administration des douanes</a> / Citoyens</p>
<h3 class="spip"><a id="sommaire_2"></a>Permis de conduire</h3>
<p>Le permis de conduire français est reconnu en Belgique. Il n’est plus nécessaire de l’échanger contre un permis belge.</p>
<p><a href="http://www.belgium.be/fr/mobilite/permis_de_conduire/index.jsp" class="spip_out" rel="external">Informations sur le permis belge</a></p>
<h3 class="spip"><a id="sommaire_3"></a>Code de la route </h3>
<p>La conduite s’effectue à droite. Le respect de la priorité à droite est appliqué avec rigueur. La vitesse maximale autorisée est de 50km/h en agglomération, 90 km/h sur route et 120 km/h sur autoroute. La signalisation routière est en néerlandais dans la région flamande. Les sanctions encourues en cas d’infraction sont de même ordre que celles appliquées en France. Le port de la ceinture de sécurité et du casque est obligatoire. Le triangle de pré-signalisation est également obligatoire. Le taux d’alcoolémie maximum autorisé au volant est de 0,5g/l. Faute de paiement immédiat des amendes, les véhicules immatriculés à l’étranger sont immobilisés.</p>
<p>Pour en savoir plus :</p>
<ul class="spip">
<li><a href="http://www.code-de-la-route.be/index.php" class="spip_out" rel="external">Guide des réglementations routières belges</a> (brochures utiles / circuler en Belgique)</li>
<li><a href="http://www.belgium.be/fr/mobilite/securite_routiere/index.jsp" class="spip_out" rel="external">Portail Belgium.be</a></li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Assurances et taxes </h3>
<p>En Belgique, tous les véhicules à moteur mis en circulation doivent être obligatoirement assurés, au minimum, en responsabilité civile.</p>
<p>Une assurance en responsabilité civile intervient uniquement pour les frais occasionnés à une tierce partie. Elle ne couvre pas les frais liés aux dégâts occasionnés à votre véhicule. Si vous voulez que ces frais soient couverts, il vous faut souscrire une police d’assurance complémentaire, comme une assurance <i>omnium</i>. Cette assurance complémentaire n’est pas obligatoire.</p>
<p>La tarification se détermine en fonction de votre compagnie d’assurance, de votre âge et de la puissance du véhicule. Le nombre éventuel d’accidents dans lesquels vous avez été en tort est également pris en compte. Pour obtenir un tarif intéressant, il est important d’avoir une attestation de l’assureur précédent.</p>
<p>Le certificat d’assurance, également appelé <i>carte verte</i>, doit obligatoirement se trouver à bord de votre véhicule. Ne pas être assuré constitue un acte punissable.</p>
<p><i>Source : Portail <a href="http://www.belgium.be" class="spip_out" rel="external">Belgium.be</a> / Mobilité / Véhicules / Taxes de circulation et assurances</i></p>
<p>Quand vous achetez et immatriculez une voiture en Belgique, vous devez payer :</p>
<ul class="spip">
<li><strong>la TVA</strong> (21% de TVA sur le prix de vente pour l’achat d’une voiture neuve, pas de TVA pour l’achat d’une voiture d’occasion à un particulier) ;</li>
<li><strong>la taxe de mise en circulation</strong> : cette taxe porte sur les véhicules neufs ou d’occasion. Il s’agit d’une taxe unique que vous devez payer si le véhicule a été enregistré à votre nom. Cet impôt est dû dès l’instant où vous utilisez pour la première fois votre véhicule sur la voie publique ;</li>
<li><strong>la taxe de circulation</strong> : dans le mois qui suit l’inscription de votre véhicule en Belgique, vous recevrez, en principe, de la part du Service contribution auto du SFP Finances, une invitation à payer la taxe de circulation, dont le montant varie en fonction de l’âge du véhicule et de sa puissance fiscale (entre 66 € /4CV et 1 702 € /20 CV). Vous devez payer cette taxe chaque année ;</li>
<li>pour une <strong>voiture roulant au LPG : la taxe de circulation complémentaire.</strong> Vous devrez payer cette taxe en même temps que la taxe de circulation ;</li>
<li>pour une <strong>voiture avec radiocassette ou lecteur cd en région wallonne : la redevance radio</strong>. Vous devez signaler que votre véhicule est équipé d’une radio au service Radio Télévision Redevances, lequel vous enverra une invitation à régler la redevance radio. Cette taxe a été supprimée en région flamande et en région de Bruxelles-Capitale.</li></ul>
<p>Pour en savoir plus sur les taxes :</p>
<ul class="spip">
<li><a href="http://minfin.fgov.be/portail2/fr/index.htm" class="spip_out" rel="external">Guide fiscal de votre voiture</a> (Publications / Guide fiscal de votre voiture)</li>
<li><a href="http://minfin.fgov.be/portail2/fr/index.htm" class="spip_out" rel="external">Service public fédéral Finances</a> (Thèmes / Transport)<br class="manualbr">SPF Finances, contributions autos Bruxelles <br class="manualbr">North Galaxy (bâtiment A- 18ème étage) Boulevard du Roi Albert II 33 _  1030 Bruxelles<br class="manualbr">Tél : 0257 257 57</li></ul>
<p>Un <strong>contrôle technique</strong> est obligatoire chaque année pour les véhicules de quatre ans et plus, à l’importation d’un véhicule ou pour la revente de celui-ci. Vous trouverez sur le <a href="http://www.goca.be/fr/" class="spip_out" rel="external">site du GOCA</a> (Groupement des entreprises agréées de contrôle automobile et du permis de conduire) de nombreuses informations sur le contrôle technique. Le site reprend les conditions liées à chaque type de véhicule et indique les tarifs et adresses des centres de contrôle technique de Belgique.</p>
<p>Pour en savoir plus : <a href="http://www.belgium.be/fr/mobilite/vehicules/modalites_techniques/controle_technique/" class="spip_out" rel="external">Contrôle technique sur Belgium.be/</a></p>
<h3 class="spip"><a id="sommaire_5"></a> Achat </h3>
<p>Toutes les marques de véhicules sont représentées en Belgique. Il est également possible d’acheter sur place un véhicule d’occasion.</p>
<p>Il est préférable d’acheter une voiture en Belgique plutôt que de l’importer, les prix pratiqués étant souvent inférieurs aux prix français.</p>
<p>Pour en savoir plus sur l’achat d’un véhicule en Belgique / à l’étranger :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://minfin.fgov.be/portail2/fr/themes/transport.htm" class="spip_out" rel="external">Service public fédéral Finances</a> </p>
<h3 class="spip"><a id="sommaire_6"></a>Immatriculation</h3>
<p>Après l’achat de votre voiture, vous devez l’immatriculer auprès de la Direction de l’immatriculation des véhicules (DIV) du SPF Mobilité et Transport. L’immatriculation de la DIV vous donne droit à un certificat d’immatriculation et une plaque minéralogique.</p>
<p>Vous devez demander l’immatriculation de votre véhicule au moyen du formulaire rose "demande d’immatriculation" délivré gratuitement par la firme qui livre le véhicule, par la compagnie d’assurance qui couvre la responsabilité civile, par les stations de contrôle technique, ou par la D.I.V. elle-même.</p>
<p>Il convient de contacter votre assureur afin d’y faire apposer une vignette d’assurance ; ce dernier immatricule immédiatement votre véhicule par internet. Vous recevrez le lendemain par la poste le certificat d’immatriculation et votre plaque.</p>
<p>Vous pouvez également transmettre par courrier le formulaire et la vignette d’assurance à la DIV ou vous rendre, muni de votre carte d’identité, à l’un de ses guichets.</p>
<p>A Bruxelles, les guichets de cette Direction se trouvent dans le bâtiment :</p>
<p>City Atrium<br class="manualbr">Rue du Progrès, 56<br class="manualbr">1210 Bruxelles.</p>
<p>Les villes les plus importantes de province disposent également d’une <a href="http://www.mobilit.belgium.be/language_selection?destination=%3Cfront%3E" class="spip_out" rel="external">antenne DIV</a>.</p>
<p><strong>Attention à votre immatriculation française en Belgique</strong> : toutes les personnes étrangères ayant établi leur résidence en Belgique sont tenues de respecter la <a href="http://www.consulfrance-bruxelles.org/Immatriculer-son-vehicule-en" class="spip_out" rel="external">règlementation belge en matière d’immatriculation de véhicules</a>.</p>
<p>Pour en savoir plus sur l’immatriculation :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Portail <a href="http://www.belgium.be/fr/mobilite/vehicules/immatriculation/" class="spip_out" rel="external">Belgium.be</a> </p>
<p>En tant que propriétaire d’un véhicule, il convient, pour circuler en Belgique, d’acquitter un certain nombre de taxes dont le montant dépend de la puissance du moteur, du carburant utilisé, etc.</p>
<h3 class="spip"><a id="sommaire_7"></a>Entretien</h3>
<p>Un contrôle technique est obligatoire chaque année pour les véhicules de quatre ans et plus, à l’importation d’un véhicule ou pour la revente de celui-ci (voir rubrique immatriculation).</p>
<p>L’entretien du véhicule ne pose aucun problème. Les pièces détachées peuvent être obtenues sans délai à un coût légèrement supérieur toutefois à celui de la France.</p>
<p>Le site <a href="http://www.carbu.be/" class="spip_out" rel="external">Carbu.be</a> permet de connaître gratuitement les prix des carburants pratiqués dans les stations services en Belgique.</p>
<h3 class="spip"><a id="sommaire_8"></a>Réseau routier </h3>
<p>Le réseau routier, en très bon état, compte 1700 km d’autoroutes gratuites.</p>
<h3 class="spip"><a id="sommaire_9"></a>Transport en commun</h3>
<p>Outre l’<a href="http://www.brusselsairport.be/" class="spip_out" rel="external">aéroport international de Bruxelles</a>, la Belgique dispose d’aéroports à Anvers, Charleroi, Liège et Ostende. Vous trouverez sur le site de l’aéroport les moyens de transport entre l’aéroport et le centre de Bruxelles.</p>
<p>Avec 3500 km de lignes, le réseau ferroviaire belge est l’un des plus denses en Europe. Par ailleurs, des liaisons ferroviaires rapides sont assurées avec la France (Thalys, Eurostar, TGV), les Pays-Bas, l’Allemagne et le Royaume-Uni.</p>
<p>SNCB - <a href="http://www.b-rail.be/" class="spip_out" rel="external">Société nationale des chemins de fer belges</a></p>
<p>A Bruxelles, les renseignements concernant le <i>réseau de transports collectifs</i> (métro, bus, tramway) peuvent être obtenus auprès de la STIB.</p>
<p><a href="http://www.stib.be/" class="spip_out" rel="external">Société des transports intercommunaux de Bruxelles</a></p>
<p>Il existe également un site pour tous renseignements sur les <a href="http://www.taxi.irisnet.be/" class="spip_out" rel="external">taxis bruxellois</a></p>
<p>Pour en savoir plus sur les transports en Belgique :</p>
<ul class="spip">
<li><a href="http://www.wallonie.be/" class="spip_out" rel="external">Portail de la région wallone</a></li>
<li><a href="http://www.belgium.be/fr/mobilite/transports_en_commun/" class="spip_out" rel="external">Portail fédéral belge</a></li>
<li><a href="http://www.bruxelles.irisnet.be/mobilite-et-transports" class="spip_out" rel="external">Portail de la région de Bruxelles-Capitale</a></li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/vie-pratique/article/transports). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
