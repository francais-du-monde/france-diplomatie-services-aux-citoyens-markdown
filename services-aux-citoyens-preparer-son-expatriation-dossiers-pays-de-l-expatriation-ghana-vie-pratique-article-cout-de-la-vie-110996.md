# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/vie-pratique/article/cout-de-la-vie-110996#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/vie-pratique/article/cout-de-la-vie-110996#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/vie-pratique/article/cout-de-la-vie-110996#sommaire_3">Alimentation</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/vie-pratique/article/cout-de-la-vie-110996#sommaire_4">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>L’unité monétaire est le cédi ghanéen (GHS) lequel a remplacé l’ancien cédi (GHC) en 2007.</p>
<p>Au 1er janvier 2014, le cédi vaut 0,312 euros, c’est-à-dire qu’un euro équivaut à 3,2 cédis*. Les divisions du cedi s’appellent les pesewas.</p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<p>La carte de crédit internationale "visa" est aujourd’hui largement acceptée dans les grandes villes du pays, notamment à Accra, dans les hôtels, les supermarchés et centres commerciaux, les restaurants, et dans les guichets automatiques des banques. Les frais bancaires occasionnés sont généralement de l’ordre de 3%. La Mastercard est moins répandue. Le règlement des dépenses de consommation courante en espèces reste néanmoins le plus fréquent.</p>
<p>La Société Générale y est représentée.</p>
<p><strong>Transfert de fonds :</strong></p>
<p>Les transferts de fonds peuvent s’effectuer à partir d’un compte en devises étrangères. La monnaie nationale n’est pas convertible à l’étranger.</p>
<h3 class="spip"><a id="sommaire_3"></a>Alimentation</h3>
<p>Les conditions d’approvisionnement sont bonnes, mais le coût des produits alimentaires importés est élevé (compter un triplement du prix, au minimum).</p>
<p>Le prix d’un repas au restaurant peut varier de 10 à 25€ environ (de 20 à 100€ dans un restaurant occidental).</p>
<h3 class="spip"><a id="sommaire_4"></a>Evolution des prix</h3>
<p>Le Ghana a connu ces dernières années une inflation de l’ordre de 10% l’an (13% en 2013).</p>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/vie-pratique/article/cout-de-la-vie-110996). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
