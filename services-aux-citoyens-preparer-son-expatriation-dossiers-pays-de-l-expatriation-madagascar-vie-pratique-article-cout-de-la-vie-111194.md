# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/vie-pratique/article/cout-de-la-vie-111194#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/vie-pratique/article/cout-de-la-vie-111194#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/vie-pratique/article/cout-de-la-vie-111194#sommaire_3">Budget</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/vie-pratique/article/cout-de-la-vie-111194#sommaire_4">Alimentation</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/vie-pratique/article/cout-de-la-vie-111194#sommaire_5">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>L’unité monétaire de Madagascar est l’ariary (MGA).</p>
<p>Au 12 décembre 2013, le taux de change est 1 EUR = 3066,12 MGA</p>
<p>Relativement stable ces derniers mois, on assiste à une dépréciation de l’ariary depuis mi-novembre 2013.</p>
<p>Pour suivre l’évolution du taux de change de l’ariary :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Consultez le site de la <a href="http://www.banque-centrale.mg/" class="spip_out" rel="external">Banque centrale de Madagascar</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<p>Il existe 11 banques territoriales à Madagascar, dont trois sont des filiales de banques françaises : la BNI/Crédit Agricole, la BFV-SG/Société Générale et la BMOI/Groupe BPCE. Des distributeurs automatiques sont disponibles dans toutes les grandes agglomérations du pays. Cependant, dans les zones reculées, il est prudent de disposer de liquidités. La carte VISA internationale est acceptée dans tout le pays, contrairement à la MASTERCARD qui ne l’est pour l’instant que par un nombre réduit d’établissements bancaires (BNI, BOA).</p>
<h3 class="spip"><a id="sommaire_3"></a>Budget</h3>
<p>L’estimation du budget mensuel peut bien entendu varier de façon importante suivant le lieu de résidence et le mode de vie. Les chiffres ci-après sont communiqués à titre purement indicatif :</p>
<ul class="spip">
<li>Pour un célibataire : à partir de 1800 €</li>
<li>Pour un couple : à partir de 2500 €</li>
<li>Pour un couple avec deux enfants : à partir de 3500 €</li></ul>
<p>Par ailleurs, en raison du coût élevé du transport, les prix pratiqués dans les supermarchés pour les produits non achetés localement sont, par rapport à Tananarive (Antananarivo), de +5% à Tamatave (Toamasina), entre +5 et 10% à Majunga (Mahajanga) et de +15% à Diégo Suarez (Antsiranana) et Tuléar (Toliara).</p>
<p>Selon le Fonds Monétaire International (FMI), Madagascar a enregistré une variation des prix à la consommation de 10% en 2011 et de 5,8% en 2012. Pour 2013 et 2014, les projections s’établissent à respectivement 6,9% et 7,3%.</p>
<h3 class="spip"><a id="sommaire_4"></a>Alimentation</h3>
<p><strong>Conditions d’approvisionnement</strong></p>
<p>Dans les régions du pays exposées aux cyclones (notamment côte Est), prévoir des réserves de conserves, d’eau en bouteille, de lait stérilisé et de produits d’entretien.</p>
<p><strong>Prix moyen d’un repas dans un restaurant</strong></p>
<p>Le prix moyen d’un repas au restaurant peut varier de 11 à 35€ environ. Le pourboire représente 10 à 15% du montant de l’addition.</p>
<p>Les produits alimentaires locaux sont généralement bon marché (ex. : viande et poisson : 9000 mga / kg, fruits et légumes : entre 500 et 2000 mga / kg). En revanche les produits importés (laitages, produits d’épicerie manufacturés, produits pour bébé, vin et alcools…) sont vendus à des prix 50 à 250 % plus élevés qu’en France.</p>
<p>Le taux d’inflation annuel est estimé entre 5 et 10%.</p>
<h3 class="spip"><a id="sommaire_5"></a>Evolution des prix</h3>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/vie-pratique/article/cout-de-la-vie-111194). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
