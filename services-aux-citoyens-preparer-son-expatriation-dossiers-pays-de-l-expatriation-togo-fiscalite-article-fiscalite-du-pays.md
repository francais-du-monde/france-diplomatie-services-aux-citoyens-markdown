# Fiscalité du pays

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/fiscalite/article/fiscalite-du-pays#sommaire_1">Généralités</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/fiscalite/article/fiscalite-du-pays#sommaire_2">Déclarations de revenus et modalités de paiement de l’impôt</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/fiscalite/article/fiscalite-du-pays#sommaire_3">Année fiscale</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/fiscalite/article/fiscalite-du-pays#sommaire_4">Assiette et barème de l’impôt</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/fiscalite/article/fiscalite-du-pays#sommaire_5">Quitus fiscal </a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/fiscalite/article/fiscalite-du-pays#sommaire_6">Inscription auprès du fisc </a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/fiscalite/article/fiscalite-du-pays#sommaire_7">Droits et taxes sur le commerce international</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Généralités</h3>
<p>Placé sous l’autorité du ministère de l’Economie et des Finances, le Commissariat des impôts est désormais l’organe chargé de la liquidation et du recouvrement de l’impôt. Il succède à la Direction générale des impôts qui a fusionné, début 2014, avec la Direction générale des douanes en vue de la création de l’Office togolais des recettes (OTR).</p>
<p>Les lois de finances sont consultables à :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.togoreforme.com/" class="spip_out" rel="external">Togoreforme.com</a></p>
<p>L’imposition des entreprises est de type déclaratif, et comporte deux régimes fiscaux : le régime du forfait (pour les artisans et les micro-entreprises du secteur informel), et le régime du réel (pour les contribuables qui tiennent une comptabilité régulière et complète).</p>
<p>La France et le Togo ont conclu <a href="http://www.impots.gouv.fr/portal/deploiement/p1/fichedescriptive_1969/fichedescriptive_1969.pdf" class="spip_out" rel="external">une convention fiscale</a> qui évite la double imposition des revenus des ressortissants des deux pays. Elle est consultable sur le site de l’<a href="http://www.impots.gouv.fr/" class="spip_out" rel="external">administration fiscale</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Déclarations de revenus et modalités de paiement de l’impôt</h3>
<p>Les salariés, dont l’employeur est domicilié ou établi au Togo, sont soumis à une retenue à la source prélevée mensuellement. L’employeur doit reverser le montant des retenues au commissariat des impôts au plus tard le 15 du mois suivant. Cette retenue à la source ne dispense pas les salariés du dépôt d’une déclaration pour l’ensemble de leurs revenus.</p>
<p>Les déclarations de revenus sont à déposer au plus tard le 31 mars de chaque année pour les entreprises individuelles et les professions libérales, le 30 avril pour les sociétés. Les contribuables doivent se rendre :</p>
<ul class="spip">
<li>à leur centre des impôts de rattachement si le chiffre d’affaires est inférieur à 30 M FCFA ;</li>
<li>à la Direction des petites et moyennes entreprises si le chiffre d’affaires est compris entre 30 M FCFA et 100 M FCFA ;</li>
<li>à la Direction des grandes entreprises si le chiffre d’affaires est supérieur à 100 M FCFA.</li></ul>
<p>Le paiement de l’impôt donne lieu à quatre versements d’acomptes provisionnels, les 31 janvier, 31 mai, 31 juillet et 31 octobre de l’année suivant celle au cours de laquelle sont réalisés les revenus servant de base au calcul de l’impôt. Le premier acompte est égal au quart de l’impôt acquitté au titre de l’année précédente, tandis que les trois suivants sont égaux au solde dû pour l’année écoulée.</p>
<p>Les contribuables soumis au régime du forfait doivent pour leur part payer la taxe professionnelle unique (TPU), également en quatre acomptes,aux dates suivantes : 15 janvier, 15 avril, 15 juillet, 15 octobre.</p>
<h3 class="spip"><a id="sommaire_3"></a>Année fiscale</h3>
<p>L’année fiscale correspond à l’année civile et s’étend du 1er janvier au 31 décembre.</p>
<h3 class="spip"><a id="sommaire_4"></a>Assiette et barème de l’impôt</h3>
<p><strong>Impôt sur le revenu des personnes physiques (IRPP) :</strong> le revenu net imposable des salariés est déterminé en considérant leur revenu brut imposable, duquel on déduit les retenues faites par l’employeur. De ce montant, on défalque 10% au titre des frais professionnels, puis on retranche 15% du montant obtenu (5% pour les sommes excédant 10 M FCFA par an). On obtient ainsi le revenu net taxable pour une personne célibataire.</p>
<p>A noter que le revenu net taxable est diminué de 72 000 FCFA par personne à charge et par an, et plafonné à 432 000 FCFA par an.</p>
<p>Le barème appliqué au revenu net taxable est le suivant pour 2014 :</p>
<table class="spip">
<thead><tr class="row_first"><th id="id90f7_c0">Revenu imposable par part </th><th id="id90f7_c1">Taux applicable </th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="id90f7_c0">de 0 à 900 000</td>
<td headers="id90f7_c1">Exonération</td></tr>
<tr class="row_even even">
<td headers="id90f7_c0">de 900 001 à 4 000 000</td>
<td headers="id90f7_c1">7%</td></tr>
<tr class="row_odd odd">
<td headers="id90f7_c0">de 4 000 001 à 6 000 000</td>
<td headers="id90f7_c1">15%</td></tr>
<tr class="row_even even">
<td headers="id90f7_c0">de 6 000 001 à 10 000 000</td>
<td headers="id90f7_c1">25%</td></tr>
<tr class="row_odd odd">
<td headers="id90f7_c0">de 10 000 001 à 15 000 000</td>
<td headers="id90f7_c1">30%</td></tr>
<tr class="row_even even">
<td headers="id90f7_c0">Plus de 15 000 000</td>
<td headers="id90f7_c1">35%</td></tr>
</tbody>
</table>
<p>Les pensions de toute nature perçues par les français résidents au Togo <a href="http://www.impots.gouv.fr/portal/deploiement/p1/fichedescriptive_5349/fichedescriptive_5349.pdf" class="spip_out" rel="external">ne sont pas imposables en France</a>.</p>
<p><strong>Taxe complémentaire à l’impôt sur le revenu (TC) :</strong> les contribuables doivent par ailleurs cotiser à la taxe complémentaire à l’impôt sur le revenu, qui représente 25% du total de l’IRPP. Le produit de la taxe comporte un minimum fixé à 6000 FCFA par redevable et un plafond de 200 000 FCFA, sauf dispositions particulières.</p>
<p><strong>Impôt sur les sociétés (IS) :</strong> il frappe le bénéfice des personnes morales passibles de l’IS ainsi que celui des sociétés de personnes ayant opté pour l’IS. Le taux de l’impôt sur les sociétés est fixé à 29% du bénéfice imposable.</p>
<p><strong>Impôt minimum forfaitaire (IMF) :</strong> les entreprises relevant de l’impôt sur le revenu et les personnes morales relevant de l’impôt sur les sociétés sont assujetties, quels que soient les résultats d’exploitation, au paiement de l’impôt minimum forfaitaire. Le taux fixé est de 1% du chiffre d’affaires annuel avec un minimum de 50 000 FCFA et un maximum de 500 M FCFA.</p>
<p><strong>Taxe sur les salaires :</strong> cet impôt est assis sur les traitements, salaires, et sur la valeur des avantages en nature versés aux employés d’une entreprise. La taxe sur les salaires est à la charge de l’entreprise et est déductible de la base imposable à l’IRPP ou à l’IS. Son taux est égal à 5% du montant total des rémunérations et des avantages en nature alloués ; dont 1% doit être affecté à un fonds national d’apprentissage, de formation, et de perfectionnement professionnels, et 1% doit être reversé à un fonds spécial de développement de l’habitat.</p>
<p><strong>Taxe professionnelle :</strong> elle est due chaque année par les personnes physiques et morales qui exercent à titre habituel une activité professionnelle non salariée, sous réserve des exemptions prévues. L’impôt est calculé à partir du chiffre d’affaires hors TVA et de la valeur locative des locaux professionnels, les taux appliqués étant fonction des branches d’activité.</p>
<p><strong>Taxe professionnelle unique :</strong> impôt synthétique applicable aux petites entreprises du secteur informel dont le chiffre d’affaire est inférieur à 30 M FCFA. Son taux est de 2,5% pour les activités de production et/ou de commerce et de 8,5% pour les activités de prestations de services.</p>
<p><strong>Taxe sur les plus-values :</strong> les plus-values réalisées par les personnes physiques et les sociétés lors de la cession à titre onéreux de biens ou de droits sont passibles de l’impôt sur le revenu, sous réserve de certaines dispositions particulières. Ce régime d’imposition ne concerne pas les cessions consenties par les sociétés passibles de l’impôt sur les sociétés.</p>
<p>Sont exonérées de l’impôt sur le revenu les plus-values résultant de la cession :</p>
<ul class="spip">
<li>de l’immeuble constituant la résidence principale du contribuable lorsqu’il s’agit d’une première mutation ;</li>
<li>d’immeubles autres et de droits immobiliers dont le prix de cession n’excède pas 2M FCFA ;</li>
<li>de terrains agricoles ou destinés à la culture ;</li>
<li>de meubles meublants, appareils ménagers et voitures automobiles ;</li>
<li>de meubles autres dont le prix de cession n’excède pas 1M FCFA.</li></ul>
<p>Pour les immeubles, la plus-value est intégralement taxable dès lors que le bien est cédé moins de cinq ans après son acquisition ; et pour les biens mobiliers, moins de deux ans après leur acquisition.</p>
<p><strong>Imposition des revenus financiers :</strong> les revenus des capitaux mobiliers sont pris en compte dans le revenu global et soumis à l’impôt sur le revenu lorsqu’ils sont encaissés par des personnes physiques ayant leur domicile fiscal au Togo.</p>
<p>Lorsque ces revenus sont compris dans les recettes d’une entreprise, ils interviennent pour la détermination du résultat imposable à l’impôt sur les sociétés.</p>
<p><strong>Taxe d’habitation :</strong> la taxe d’habitation est liquidée forfaitairement par tarif selon le type d’habitation : studio, villa, étage…et est établie pour l’année civile. Les montants à payer varient de 4 000 à 75 000 FCFA.</p>
<h3 class="spip"><a id="sommaire_5"></a>Quitus fiscal </h3>
<p>Les autorités togolaises exigent la présentation d’un quitus fiscal avant d’autoriser le départ de toute personne ayant son domicile fiscal au Togo et quittant le pays définitivement.</p>
<h3 class="spip"><a id="sommaire_6"></a>Inscription auprès du fisc </h3>
<p>Les salariés sont inscrits auprès du FISC par leur employeur, qui a la charge de prélever une retenue mensuelle sur leur salaire qu’il reverse auprès des administrations fiscales.</p>
<p>L’inscription auprès du FISC pour les entreprises individuelles s’effectue automatiquement lors de l’immatriculation au Centre de formalités des entreprises, situé à la Chambre de commerce et d’industrie.</p>
<h3 class="spip"><a id="sommaire_7"></a>Droits et taxes sur le commerce international</h3>
<p>Les produits importés et exportés sont soumis au paiement de droits fiscaux d’entrée ou de sortie, lesquels sont fonctions du type de produit échangé. Le tarif extérieur commun de l’UEMOA actuellement applicable sera remplacé à compter du 1 er janvier 2015 par un nouveau tarif extérieur commun applicable à l’ensemble des pays de la CEDEAO.</p>
<p>Il existe en outre des taxes fixes, à savoir :</p>
<ul class="spip">
<li>les timbres douaniers, dont le taux est fixé à 4% de la valeur des biens échangés ;</li>
<li>la taxe de statistique, dont le taux est de 3% ;</li>
<li>la TVA, au taux de 18% ;</li>
<li>la taxe de péage, qui s’élève à 200 FCFA/ kg.</li></ul>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Commissariat des Impôts<br class="manualbr">Tel : +228 22 61 51 23 ou +228 22 61 51 24<br class="manualbr">Fax : +228 22 61 26 67<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/fiscalite/article/fiscalite-du-pays#contact#mc#dgitogo.tg#" title="contact..åt..dgitogo.tg" onclick="location.href=mc_lancerlien('contact','dgitogo.tg'); return false;" class="spip_mail">Courriel</a></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Service économique près l’ambassade de France au Togo<br class="manualbr">Tel : +228 22 23 46 60<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/fiscalite/article/fiscalite-du-pays#lome#mc#dgtresor.gouv.fr#" title="lome..åt..dgtresor.gouv.fr" onclick="location.href=mc_lancerlien('lome','dgtresor.gouv.fr'); return false;" class="spip_mail">Courriel</a></p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/fiscalite/article/fiscalite-du-pays). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
