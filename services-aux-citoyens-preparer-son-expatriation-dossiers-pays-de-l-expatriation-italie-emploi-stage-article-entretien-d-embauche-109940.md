# Entretien d’embauche

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/emploi-stage/article/entretien-d-embauche-109940#sommaire_1">Conduite de l’entretien</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/emploi-stage/article/entretien-d-embauche-109940#sommaire_2">Apparence et attitude</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/emploi-stage/article/entretien-d-embauche-109940#sommaire_3">Questions préférées des recruteurs</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/emploi-stage/article/entretien-d-embauche-109940#sommaire_4">Après l’entretien</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Conduite de l’entretien</h3>
<p>En Italie, l’entretien d’embauche prévaut très largement dans la sélection des candidats. Il vous faudra parfois passer entre deux et cinq entretiens dans certaines entreprises. L’entretien dure en général une heure, et est mené dans une ambiance assez informelle.</p>
<p>Dans un premier temps, le recruteur va s’attacher à cerner la personnalité du candidat en posant de nombreuses questions sur sa personnalité avec des questions sur sa situation familiale, sa formation, ses expériences professionnelles (réalisations, missions, responsabilités…). Il n’abordera pas les questions relatives à la religion, l’origine…</p>
<p>Dans un second temps, le recruteur va chercher à savoir si votre profil correspond au poste proposé. Le candidat doit être ponctuel et se montrer honnête, dynamique, positif lors des entretiens.</p>
<p>On ne parle pas d’emblée de salaire (ce sera l’objet éventuellement du deuxième entretien). Posez des questions sur la structure de la société, l’organigramme et les fonctions que vous devrez occuper. Souvent vous aurez à faire à des petites structures (80% des entreprises sont des PME ou des TPE) où le poste n’est pas toujours très clairement défini. A vous de mettre en avant vos compétences et d’expliquer le contenu de vos études et de vos stages. Demandez si vous pouvez téléphoner pour avoir des nouvelles et tâchez de vous faire donner une date de réponse de la part de l’entreprise. Il arrive souvent que les entreprises ne donnent aucune nouvelle aux candidats qui n’ont pas été retenus.</p>
<h3 class="spip"><a id="sommaire_2"></a>Apparence et attitude</h3>
<p>En général une présentation correcte est nécessaire. Une apparence négligée peut en effet vous desservir auprès de recruteurs qui attachent généralement de l’importance à la présentation. La cravate est recommandée.</p>
<h3 class="spip"><a id="sommaire_3"></a>Questions préférées des recruteurs</h3>
<p>Les questions possibles à l’entretien d’embauche sont :</p>
<ul class="spip">
<li>Perché vuole lavorare nella nostra sociétà ?</li>
<li>Puo parlarmi di Lei…</li>
<li>Che progetti di lavoro ha per il futuro ?</li>
<li>Perché ha lasciato il suo ultimo lavoro ?</li>
<li>Come si vede tra dieci anni ?</li>
<li>Che cosa non sopporta negli altri sul lavoro ?</li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Après l’entretien</h3>
<p>Il est d’usage en Italie de serrer la main de son interlocuteur à la fin d’un entretien d’embauche.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/emploi-stage/article/entretien-d-embauche-109940). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
