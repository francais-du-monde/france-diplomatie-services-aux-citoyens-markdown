# Loisirs et culture

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/vie-pratique/article/loisirs-et-culture#sommaire_1">Tourisme</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/vie-pratique/article/loisirs-et-culture#sommaire_2">Activités culturelles</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/vie-pratique/article/loisirs-et-culture#sommaire_3">Sports</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Tourisme</h3>
<p>Le tourisme est très développé en Israël, à Jérusalem et dans les territoires palestiniens qui disposent d’atouts touristiques évidents. Tous les sites touristiques sont ouverts au public, à l’exception de l’esplanade des mosquées dont l’accès est réglementé.</p>
<p>Il convient d’être prudent pour se rendre à Hébron et plus généralement en Cisjordanie. L’entrée à Gaza est impossible pour les touristes.</p>
<p>Pour plus de renseignements, s’adresser à :</p>
<p><strong>Office national israélien du tourisme </strong><br class="manualbr">94 rue St Lazare (fermé au public) - 75009 Paris <br class="manualbr">Téléphone : 01 42 61 01 97 Télécopie : 01 49 27 09 46<br class="manualbr">Courriel : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/vie-pratique/article/loisirs-et-culture#infos#mc#otisrael.com#" title="infos..åt..otisrael.com" onclick="location.href=mc_lancerlien('infos','otisrael.com'); return false;" class="spip_mail">infos<span class="spancrypt"> [at] </span>otisrael.com</a></p>
<p>Cet organisme a pour vocation de promouvoir et informer sur le tourisme, l’hébergement et les manifestations culturelles en Israël.</p>
<p>Pour en savoir plus, vous pouvez consulter le site en français du <a href="http://www.goisrael.com/Tourism_Fra" class="spip_out" rel="external">ministère du Tourisme israélien</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Activités culturelles</h3>
<h4 class="spip">Activités culturelles en français</h4>
<p>Les Instituts français et centres culturels français proposent diverses activités culturelles ainsi que des spectacles.</p>
<ul class="spip">
<li><a href="http://www.ambafrance-il.org/-Institut-de-Beer-Sheva-.html" class="spip_out" rel="external">Institut français de Beer Sheva</a></li>
<li><a href="http://www.ccfgary-jerusalem.org.il/" class="spip_out" rel="external">Centre culturel français de Jérusalem</a></li>
<li><a href="http://www.ambafrance-il.org/-Centre-de-Haifa-.html" class="spip_out" rel="external">Centre culturel français de Haïfa</a></li></ul>
<h4 class="spip">Activités culturelles locales</h4>
<p>De mi-mai à mi-juin se déroule le Festival d’Israël qui regroupe tous les styles de musique, théâtre et danse. Des festivals de film et de musique ont lieu chaque année. Les films projetés sont généralement des films américains grand public en version originale sous-titrée en hébreu. Les grands films français sont régulièrement à l’affiche, toujours en version française. Quelques films européens anciens et récents (français, espagnol, anglais, italiens) sont projetés régulièrement à la cinémathèque de Jérusalem en version originale sous-titrée, en anglais ou en hébreu. Pour le théâtre, chaque année on peut assister à de nombreuses créations et pièces du répertoire classique surtout à Jérusalem-Ouest, principalement en hébreu, mais quelques représentations sont données en anglais.</p>
<p>Les radios diffusent tous les jours des émissions en anglais, en arabe, en français, en yiddish, en russe et dans d’autres langues. Les flashes d’information en anglais et en français durent un quart d’heure et sont très détaillés. Ils peuvent être utiles avant de prendre la route vers des zones sensibles. La bande FM a été libéralisée et diffuse beaucoup de musique.</p>
<p>La chaîne de télévision <i>Israël TVI</i> diffuse tous les jours un journal d’informations en anglais, de 18 h 15 à 18 h 30. Les programmes télévisés sont souvent en anglais, avec sous-titrages en hébreu, arabe et russe.</p>
<p>La presse française, de même que celle d’autres pays, est largement diffusée en Israël dans les centres commerciaux ou dans des librairies mais elle reste très chère. Le Jérusalem Post présente une édition hebdomadaire en langue française. Il n’existe aucune restriction à l’importation de journaux, livres, cassettes.</p>
<h3 class="spip"><a id="sommaire_3"></a>Sports</h3>
<p>Tous les sports pratiqués en Europe existent également en Israël. On y trouve des clubs sportifs locaux et des centres de sport.</p>
<p>Les sports nautiques comme la planche à voile et le ski nautique se pratiquent sur la côte méditerranéenne. La mer Rouge offre un vaste terrain d’exploration pour les plongeurs : les coraux sont nombreux, les espèces de poissons variées par leurs formes et leurs couleurs.</p>
<p><strong>Sources : </strong> <a href="http://www.ambafrance-il.org/-Accueil-Francais-.html" class="spip_out" rel="external">Ambassade de France en Israël</a>, <a href="http://www.israelvalley.com/" class="spip_out" rel="external">CCFI</a></p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/vie-pratique/article/loisirs-et-culture). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
