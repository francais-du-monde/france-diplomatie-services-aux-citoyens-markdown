# Convention fiscale

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/fiscalite/article/convention-fiscale-110992#sommaire_1">Champ d’application de la convention</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/fiscalite/article/convention-fiscale-110992#sommaire_2">Dispositions conventionnelles sur certaines catégories de revenus</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/fiscalite/article/convention-fiscale-110992#sommaire_3">Elimination de la double imposition</a></li></ul>
<p>La France et la République du Ghana ont signé, le 5 avril 1993, une convention en matière de fiscalité publiée au Journal Officiel du 14 mai 1997.</p>
<p>Cette convention tend à éviter les doubles impositions en matière d’impôts sur les revenus et sur les gains en capital qui pourraient résulter de l’application des législations de ces deux Etats.</p>
<p>Le texte de la convention et de ses avenants peut être obtenu à la Direction des Journaux Officiels, par courrier (26 rue Desaix, 75727 PARIS Cedex 15), par fax (01 40 58 77 80), par minitel 36 16 JOURNAL OFFICIEL ou sur le <a href="http://www.impots.gouv.fr/portal/deploiement/p1/fichedescriptive_1837/fichedescriptive_1837.pdf" class="spip_out" rel="external">site Internet du ministère des Finances</a>.</p>
<p>Les dispositions conventionnelles qui ont primauté sur les dispositions du droit interne, selon l’article 55 de la Constitution française répartissent entre les deux Etats, le droit d’imposer les revenus perçus par leurs résidents respectifs.</p>
<h3 class="spip"><a id="sommaire_1"></a>Champ d’application de la convention</h3>
<p>Cet accord a pour objet de protéger <strong>les résidents de chacun des Etats contractants</strong> en matière d’impôts prélevés directement sur le revenu ou sur les gains en capital.</p>
<h4 class="spip">Personnes concernées</h4>
<p>L’article 1 de la Convention s’applique aux personnes qui sont des résidents d’un Etat contractant ou des deux Etats contractants.</p>
<h5 class="spip">Notion de résidence</h5>
<p>Au termes de l’article 4, paragraphe 1, de la convention, une personne est considérée comme "résident d’un Etat contractant" lorsque, en vertu de la législation dudit Etat, elle se trouve assujettie à l’impôt à raison de son domicile, de sa résidence ou de critères analogues.</p>
<p>Le paragraphe 2, du même article fournit des critères subsidiaires permettant de résoudre le cas de double résidence si l’assujettissement à l’impôt ne pouvait suffire.</p>
<p>Ces critères sont :</p>
<ul class="spip">
<li>un foyer d’habitation permanent (il s’agit, par exemple, du lieu de situation du conjoint ou des enfants) ;</li>
<li>l’Etat où la personne possède le centre de ses intérêts vitaux (tant professionnels que privés) ;</li>
<li>l’Etat dans lequel elle séjourne de façon habituelle (notion de 183 jours de présence physique sur le territoire au cours d’une année fiscale) ;</li>
<li>à défaut, l’Etat dont elle possède la nationalité.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Dispositions conventionnelles sur certaines catégories de revenus</h3>
<h4 class="spip">Traitements, salaires, pensions et rentes</h4>
<h5 class="spip">Rémunérations privées</h5>
<p><strong>Principe</strong></p>
<p>L’article 16, paragraphe 1 précise que les traitements et salaires d’origine privée ne sont, en règle générale, imposables que dans l’Etat où <strong>s’exerce l’activité personnelle</strong>.</p>
<p><strong>Exceptions à cette règle générale</strong></p>
<p>Le maintien de l’imposition dans l’Etat de résidence du bénéficiaire est prévu par le paragraphe 2 du même article sous réserve de trois conditions simultanément remplies :</p>
<ul class="spip">
<li>le séjour temporaire du bénéficiaire dans l’autre Etat ne dépasse pas une durée totale de 183 jours au cours de l’année civile.</li>
<li>la rémunération est payée par un employeur qui n’est pas résident de l’Etat d’exercice ;</li>
<li>la rémunération ne doit pas être à la charge d’un établissement stable ou d’une base fixe de l’employeur dans l’Etat.</li></ul>
<p><strong>Exemple</strong> : Monsieur X est envoyé trois mois, soit quatre-vingt-dix jours (mai, juin, juillet de l’année n) par une PME située en France, fabriquant d’articles de maroquinerie, en vue de prospecter le marché ghanéen. Cette entreprise ne dispose d’aucune succursale ni bureau au Ghana et rémunère l’intéressé. Dans un tel cas, Monsieur X devra déclarer ses revenus en France.</p>
<p>Au contraire, si Monsieur X est envoyé du mois de février inclus au mois de novembre inclus, son séjour de plus de 183 jours au Ghana entraîne son imposition dans ce pays.</p>
<p>Il résulte des dispositions du paragraphe 3 de l’article 16 de la convention que les rémunérations des salariés, autres qu’intérimaires employés à bord d’un navire, d’un aéronef en trafic international ou à bord d’un bateau servant à la navigation intérieure, ne sont imposables que dans l’Etat où se trouve la direction effective de l’entreprise.</p>
<h5 class="spip">Rémunérations publiques</h5>
<p><strong>Principe</strong></p>
<p>L’article 20, paragraphes 1-a et 2-a, indique que les traitements, salaires et rémunérations analogues ainsi que les <strong>pensions de retraite</strong> payés par un Etat ou une personne morale de droit public de cet Etat restent imposables dans cet Etat.</p>
<p><strong>Exemple</strong> : Monsieur X est fonctionnaire de l’Etat français. Il résidait pendant son activité en France et décide d’aller prendre sa retraite chez l’un de ses enfants à Accra. Les montants de ses pensions resteront imposés en France ; son dossier est alors pris en charge par un Centre des impôts spécial, le Centre des Impôts des Non-résidents.</p>
<p><strong>Exceptions</strong></p>
<p><strong>Toutefois</strong>, en vertu de l’article 20, paragraphes 1-b et 2-b, cette règle ne s’applique pas lorsque le bénéficiaire possède <strong>la nationalité de l’autre Etat</strong> sans être en même temps ressortissant de l’Etat payeur ; l’imposition est réservée à l’Etat dont l’intéressé est le résident.</p>
<p><strong>Exemple</strong> : Monsieur X, <strong>citoyen français</strong>, était fonctionnaire à l’Ambassade de la République du Ghana en France ; il prend sa retraite en France. Dans ce cas, il sera imposable en France et devra déclarer ses revenus au centre des impôts dont dépend son domicile.</p>
<p><strong>D’autre part</strong>, en vertu des dispositions du paragraphe 3 du même article, les règles fixées aux paragraphes 1 et 2 dudit article ne sont pas applicables aux rémunérations ou aux pensions versées au titre <strong>de services rendus dans le cadre d’une activité industrielle ou commerciale exercée par un Etat ou une personne morale de droit public.</strong></p>
<p>Les sommes versées à ce titre sont imposées soit dans l’Etat d’exercice de l’activité (article 16 de la convention), soit dans l’Etat de résidence du bénéficiaire (article 19 de la convention).</p>
<p><strong>Exemples</strong> :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Monsieur X, agent E.D.F., est envoyé à Accra afin d’effectuer des travaux de conception avec les services locaux ghanéens. </p>
<p>Monsieur X est rémunéré par E.D.F. France.</p>
<p>E.D.F. étant un établissement de droit public à caractère industriel et commercial, les rémunérations allouées à Monsieur X seront imposées au Ghana.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Monsieur X, retraité de la S.N.C.F., perçoit une pension de la caisse vieillesse de la S.N.C.F. Or, Monsieur X, résident en France, a décidé de vivre à Accra. Cette retraite se verra ainsi imposable au Ghana puisque la S.N.C.F. a le caractère d’un établissement public, industriel et commercial. </p>
<h5 class="spip">Pensions et rentes</h5>
<p>L’article 19, paragraphe 1, prévoit que les pensions de retraite de source privée ainsi que les rentes viagères restent imposables dans l’Etat dont le bénéficiaire est un résident.</p>
<p>Cette disposition couvre également les pensions publiques auxquelles les règles de l’article 20 ne sont pas applicables.</p>
<p>En outre, la convention comporte une disposition particulière concernant les prestations de sécurité sociale et précise au paragraphe 2 de l’article 19 que les pensions privées versées par la Sécurité sociale française à des résidents du Ghana restent imposables dans l’Etat d’origine de ces revenus, au cas précis la France ainsi que dans l’Etat du domicile du bénéficiaire. Toutefois, l’article 24 de la convention élimine la double imposition en incluant ces revenus dans la base de l’impôt ghanéen mais l’impôt déjà payé en France est déductible de cet impôt dans la limite de la fraction de l’impôt ghanéen correspondant au rapport entre le montant de ces pensions et le montant total des revenus imposables au Ghana.</p>
<p>Dans l’exemple contraire, pour les pensions de source ghanéenne versées dans le cadre de législation sur la sécurité sociale à un résident de France, la France accorde un crédit d’impôt égal au montant de l’impôt payé au Ghana dans la limite de l’impôt français correspondant.</p>
<p><strong>Exemple </strong> : Monsieur X, citoyen français qui a exercé une activité salariée à Accra, décide de venir prendre sa retraite en France. Les pensions versées par les caisses de sécurité sociale du Ghana au titre de cette activité sont imposables dans ce pays. Mais il devra également déclarer ces revenus en France.</p>
<h5 class="spip">Etudiants, stagiaires, professeurs, chercheurs</h5>
<p><strong>Etudiants, stagiaires</strong></p>
<p>L’article 21 de la convention prévoit que les étudiants, les stagiaires d’un Etat qui séjournent dans l’autre Etat à seule fin d’y poursuivre leurs études ou leur formation et qui perçoivent des subsides d’origine étrangère à cet Etat sont exonérés d’impôt par ce dernier Etat.</p>
<p><strong>Professeurs, chercheurs</strong></p>
<p>L’article 22, paragraphes 1 et 2, précise que les rémunérations versées aux enseignants ou aux chercheurs résidents d’un Etat se rendant temporairement dans l’autre Etat en vue d’y exercer une activité pédagogique ou de recherches pendant une période <strong>ne dépassant pas deux ans</strong>, restent imposables dans l’Etat de résidence.</p>
<h4 class="spip">Autres catégories de revenus</h4>
<h5 class="spip">Bénéfices industriels et commerciaux</h5>
<p>L’article 7, paragraphe 1, dispose que les entreprises industrielles et commerciales sont imposables sur le territoire où se trouve un établissement stable.</p>
<p>L’article 5 définit la notion d’établissement stable.</p>
<h5 class="spip">Bénéfices des professions non commerciales et des revenus non commerciaux</h5>
<p>L’article 15, paragraphe 1, stipule que les revenus provenant de l’exercice d’une profession libérale ou d’autres activités indépendantes sont imposables dans l’Etat sur le territoire duquel se trouve l’installation permanente ou base fixe où s’exerce de façon régulière l’activité personnelle.</p>
<p>Les revenus que les artistes réalisent en cette qualité dans l’un des deux Etats restent imposables dans l’Etat de situation de leur activité selon les dispositions de l’article 18 paragraphe 1 de la convention.</p>
<p>L’article 12, pose en principe que les revenus non commerciaux (redevances et droits d’auteur) sont imposables dans l’Etat de résidence du bénéficiaire.</p>
<p>Toutefois, le droit d’imposer ces revenus revient exclusivement à l’Etat, source de ceux-ci, à un taux qui ne peut excéder 12,5% de leur montant brut lorsque l’Etat est le Ghana, et 10% lorsqu’il s’agit de la France.</p>
<h5 class="spip">Revenus immobiliers</h5>
<p>L’article 6, paragraphe 1, dispose que les revenus des biens immobiliers y compris les bénéfices des exploitations agricoles sont imposables dans l’Etat où ils sont situés.</p>
<p>Cette règle s’applique également aux gains provenant de la cession ou de l’échange desdits biens ou droits selon les dispositions de l’article 14, paragraphe 1-a.</p>
<p>En ce qui concerne les gains provenant de l’aliénation de tous biens autres que mobiliers ou immobiliers, le paragraphe 4 de l’article 14 précise qu’ils restent imposables dans l’Etat de résidence du cédant.</p>
<h5 class="spip">Revenus de capitaux mobiliers</h5>
<p><strong>Les dividendes</strong></p>
<p>Ce terme désigne les revenus provenant d’actions, actions ou bons de jouissance, parts de mines, parts de fondateur ou autres parts bénéficiaires, à l’exception des créances et les revenus d’autres parts sociales assimilés aux revenus d’actions.</p>
<p>De manière générale, l’article 10 prévoit le principe de la répartition du droit d’imposer entre l’Etat dont la société distributrice est un résident et l’Etat de résidence du bénéficiaire.</p>
<p>Ainsi, les dividendes de source ghanéenne reçus par des résidents de France sont imposables en France. Mais le Ghana peut taxer ces revenus au taux maximum de 15%. Ce taux plafond est toutefois ramené à 7,5% du montant brut des dividendes lorsque leur bénéficiaire effectif est une société mère, résidente de France, détenant au moins directement ou indirectement 10% du capital de la société distributrice. Dans le même cas de figure les dividendes de source française versés à des résidents du Ghana seront imposés au taux de 5%.</p>
<p><strong>Les intérêts</strong></p>
<p>Ce terme désigne les revenus des fonds publics, des obligations d’emprunts, des obligations d’emprunts et des créances de toute nature ainsi qu’aux intérêts produits par des titres négociables, les bons de caisse et les intérêts de créances ordinaires.</p>
<p>L’article 11 précise que les intérêts provenant d’un Etat et payés à un résident de l’autre Etat restent imposables dans l’Etat de résidence du bénéficiaire effectif lorsque ces intérêts sont versés en liaison avec la vente à crédit de marchandises ou la fourniture de services par une entreprise à une autre entreprise. Sinon, l’imposition revient à l’Etat, source des revenus, et ne peut excéder 10% du montant brut des intérêts lorsque cet Etat est la France et 12,5% lorsqu’il s’agit du Ghana.</p>
<h5 class="spip">Rémunérations de services de direction</h5>
<p>Cette catégorie de revenus comprend les rémunérations de toute nature payées à toute personne autre qu’un employé du débiteur, en contrepartie de services de direction ou de gestion, de services techniques ou de services de conseil.</p>
<p>L’article 13 de la convention prévoit le principe de la répartition du droit d’imposer entre l’Etat dont le débiteur est un résident et l’Etat de résidence du bénéficiaire.</p>
<p>Les rémunérations de services de direction de source ghanéenne reçues par des résidents de France sont imposables en France. Le Ghana peut également imposer ces revenus au taux maximum de 10% de leur montant brut.</p>
<p>Cet article, aux paragraphes 4-5-6, permet aux bénéficiaires de telles rémunérations d’opter pour l’application à celles-ci, selon le cas, du régime des bénéfices des entreprises ou des revenus des professions indépendantes.</p>
<h3 class="spip"><a id="sommaire_3"></a>Elimination de la double imposition</h3>
<p>L’élimination de la double imposition pour les résidents de France qui perçoivent des revenus de source ghanéenne s’opère aux termes de l’article 24 selon le régime de l’imputation</p>
<p>Le montant de l’impôt français sur lequel le crédit est imputable est déterminé de la manière suivante :</p>
<ul class="spip">
<li>si l’impôt français est proportionnel, l’impôt à retenir est celui résultant de l’application du taux proportionnel aux revenus ghanéens visés ;</li>
<li>s’il s’agit d’un barème progressif d’imposition, il convient de les prendre en compte pour la détermination du taux effectif.</li></ul>
<p>Le calcul du taux effectif consiste à calculer l’impôt sur les seuls revenus imposables en France en appliquant le taux d’imposition correspondant à l’ensemble des revenus de sources françaises et étrangères.</p>
<p>Au résultat ainsi obtenu peuvent être appliquées les réfactions prévues par la loi interne (crédit ou réduction d’impôt).</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/fiscalite/article/convention-fiscale-110992). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
