# Demander sa radiation d’une liste électorale consulaire

<p>En quittant définitivement votre actuel pays de résidence, n’oubliez pas de signaler votre départ au consulat, par écrit. Une attestation de radiation vous sera remise ou adressée.</p>
<p>En l’absence de radiation de la liste électorale consulaire vous ne pourrez voter en France pour les scrutins nationaux.</p>
<p>Votre demande peut se faire sur papier libre, par courrier électronique ou en utilisant <a href="https://www.formulaires.modernisation.gouv.fr/gf/cerfa_14040.do" class="spip_out" rel="external">le formulaire Cerfa n° 14040*03.</a></p>
<p><i>Mise à jour : janvier 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/droit-de-vote-et-elections-a-l-etranger/article/demander-sa-radiation-d-une-liste). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
