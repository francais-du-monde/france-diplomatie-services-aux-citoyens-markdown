# Stages

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/emploi-stage/article/stages#sommaire_1">Contexte </a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/emploi-stage/article/stages#sommaire_2">Recherche de stage</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/emploi-stage/article/stages#sommaire_3">Volontariat international (V.I.E ou V.I.A.)</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Contexte </h3>
<p>Le stage est très répandu au Sénégal et la plupart des petites, moyennes et grandes entreprises de tous les secteurs ont systématiquement recours à des stagiaires. Cependant, la plupart des entreprises n’offre aucun salaire, ni aucune indemnité ; même si certaines offrent des aides pour l’hébergement. En cas d’avis positif, les frais durant la période en entreprises sont ainsi à la charge du stagiaire. Seules les entreprises ayant leur siège dans les pays occidentaux ou les institutions internationales implantées dans le pays offrent des indemnités, dans la mesure de la réglementation en vigueur dans leur pays d’origine. Cependant, ces dernières ont une culture de recrutement de stagiaires qui dépend des dispositions définies au niveau de leur siège. Elles sont donc moins enclines à donner un avis positif sur des demandes spontanées de stage.</p>
<p>Les structures sénégalaises émettent rarement des offres de stages. Il n’existe d’ailleurs pas de plateforme centralisé pour de telles offres. Par contre, il existe bien de blogs et de sites internet pour faire des demandes à l’endroit des entreprises. Toutefois, aucune réponse de la part d’entreprises n’est garantie. Ces sites peuvent constituer dans certains cas une source d’arnaque.</p>
<h3 class="spip"><a id="sommaire_2"></a>Recherche de stage</h3>
<p>Compte tenu du fait qu’il n’existe pas de plateforme d’annonces d’offres de stage au Sénégal, la méthode la plus courante est la demande spontanée de stage. Elle peut être adressée aussi bien à des entreprises sénégalaises qu’aux entreprises expatriées ou aux institutions internationales. Dans le second cas, la demande pourrait restée sans réponse ou donner lieu à un avis négatif, compte tenu des dispositions particulières de recrutement de stagiaires dans ces structures.</p>
<p>En ce qui concerne les entreprises sénégalaises, la difficulté réside dans leur identification et même dans l’introduction des demandes. Il n’existe pas au Sénégal un catalogue gratuit des entreprises ; et de façon générale, les recrutements se font par cooptation. Aussi la préférence pour des nationaux est très prononcée.</p>
<p>Pour les entreprises sénégalaises appartenant à des expatriés européens, le Conseils des investisseurs européens au Sénégal (<a href="http://www.cies.sn/" class="spip_out" rel="external">CIES</a>) propose une liste de ces entreprises avec leur contact.</p>
<p>Concernant les entreprises sénégalaises détenues par des nationaux, il faut s’adresser aux associations professionnelles, qui pourraient communiquer les contacts des entreprises suivant les secteurs d’activités ; ou se procurer le catalogue de toutes les entreprises inscrites au registre du commerce de la Chambre du commerce de Dakar (CCIAD) pour un montant de 25000 FCFA (soit 38 EUR) :</p>
<p><strong>Service de la documentation du CCIAD</strong><br class="manualbr">1, Place de l’Indépendance - Dakar (Sénégal) - BP 118 Dakar<br class="manualbr">Tel : (+221) 33 823 71 89 <br class="manualbr">Fax : (+221) 33 823 93 63 <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/emploi-stage/article/stages#cciad#mc#orange.sn#" title="cciad..åt..orange.sn" onclick="location.href=mc_lancerlien('cciad','orange.sn'); return false;" class="spip_mail">Courriel</a></p>
<p>Dans les deux cas, il faut une prise de contact téléphonique avant d’envoyer tout document de demande de stage.</p>
<h3 class="spip"><a id="sommaire_3"></a>Volontariat international (V.I.E ou V.I.A.)</h3>
<p>Le V.I.E. permet aux entreprises françaises de confier à un jeune ressortissant de l’espace économique européen, âgé de 18 à 28 ans, une mission professionnelle (rémunérée) à l’étranger durant une période modulable de 6 à 24 mois.</p>
<p>Pour en savoir plus :</p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/emplois-stages-concours/volontaires-internationaux/" class="spip_in">Page thématique sur le volontariat international sur notre site</a></li>
<li><a href="http://www.civiweb.com/FR/index.aspx" class="spip_out" rel="external">Civiweb.com</a></li>
<li><a href="http://export.businessfrance.fr/default.html" class="spip_out" rel="external">Business France.fr</a> - <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/emploi-stage/article/stages#infoVIE#mc#ubifrance.fr#" title="infoVIE..åt..ubifrance.fr" onclick="location.href=mc_lancerlien('infoVIE','ubifrance.fr'); return false;" class="spip_mail">courriel</a></li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/emploi-stage/article/stages). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
