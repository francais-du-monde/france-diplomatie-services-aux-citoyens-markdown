# Fiscalité

<h2 class="rub23007">Fiscalité du pays</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/fiscalite/#sommaire_1">Présentation</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/fiscalite/#sommaire_2">Modalités d’imposition des revenus catégoriels</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/fiscalite/#sommaire_3">Rémunérations publiques</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/fiscalite/#sommaire_4">Pensions et rentes</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/fiscalite/#sommaire_5">Etudiants, stagiaires</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/fiscalite/#sommaire_6">Autres catégories de revenus</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/fiscalite/#sommaire_7">Barème de l’impôt</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/fiscalite/#sommaire_8">Quitus fiscal</a></li>
<li><a id="so_9" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/fiscalite/#sommaire_9">Solde du compte en fin de séjour</a></li>
<li><a id="so_10" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/fiscalite/#sommaire_10">Coordonnées des centres d’information fiscale</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Présentation</h3>
<h3 class="spip"><a id="sommaire_2"></a>Modalités d’imposition des revenus catégoriels</h3>
<p>La détermination de l’impôt sur les revenus salariaux et assimilés (IRSA) est effectuée selon le barème ci-après :</p>
<ul class="spip">
<li>Jusqu’à 250 000 AR : 0%</li>
<li>Au-dessus de 250 000 AR : 21%</li></ul>
<p>Des informations complémentaires sont disponibles sur le site de l’<a href="http://www.impots.mg/index_fr.php" class="spip_out" rel="external">administration fiscale malgache</a>.</p>
<p><strong>Principe</strong></p>
<p>L’article 15, paragraphe 1 de la Convention franco-malgache de non double imposition précise, sous réserve des dispositions des articles suivants, que les traitements et salaires d’origine privée qu’un résident d’un Etat reçoit au titre d’un emploi salarié ne sont, en règle générale, imposables que dans cet Etat. Toutefois, si l’emploi est exercé dans l’autre Etat contractant, les rémunérations perçues sont imposables dans cet autre Etat.</p>
<p><strong>Exceptions à cette règle générale</strong></p>
<ul class="spip">
<li>Le maintien de l’imposition dans l’Etat de résidence du bénéficiaire est prévu par le paragraphe 2 du même article sous réserve de trois conditions simultanément remplies : le séjour temporaire du bénéficiaire dans l’autre Etat ne dépasse pas une durée totale de 183 jours au cours de l’année fiscale considérée ; la rémunération est payée par un employeur qui n’est pas résident de l’Etat d’exercice et, la rémunération ne doit pas être à la charge d’un établissement stable ou d’une base fixe de l’employeur dans l’Etat d’exercice.</li>
<li>Il résulte des dispositions du paragraphe 3 de l’article 15 de la convention que les revenus professionnels des salariés employés à bord d’un navire, d’un aéronef en trafic international ne sont imposables que dans l’Etat où se trouve la direction effective de l’entreprise.</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Rémunérations publiques</h3>
<p><strong>Principe</strong></p>
<p>L’article 19, paragraphes 1 et 2, indique que les traitements, salaires et rémunérations analogues ainsi que les <strong>pensions de retraite </strong>payés par un Etat ou une personne morale de droit public de cet Etat sont imposables dans l’Etat qui les verse.</p>
<p><strong>Exception</strong></p>
<p>Ne sont pas concernées par cette règle les rémunérations ou les pensions versées au titre <strong>de services rendus dans le cadre d’une activité industrielle ou commerciale exercée par un Etat ou une personne morale de droit public.</strong></p>
<p>Les sommes versées à ce titre sont imposées soit dans l’Etat d’exercice de l’activité (article 15 de la convention), soit dans l’Etat de résidence du bénéficiaire (article 18 de la convention).</p>
<h3 class="spip"><a id="sommaire_4"></a>Pensions et rentes</h3>
<p>L’article 18 prévoit que les pensions de retraite de source privée ainsi que les rémunérations similaires sont imposables dans l’Etat dont le bénéficiaire est un résident.</p>
<p>Cette disposition couvre également les pensions publiques auxquelles les règles de l’article 19 ne sont pas applicables.</p>
<h3 class="spip"><a id="sommaire_5"></a>Etudiants, stagiaires</h3>
<p>L’article 20, paragraphe 1, de la Convention prévoit que les étudiants ou les stagiaires d’un Etat qui séjournent dans l’autre Etat à seule fin d’y poursuivre leurs études ou leur formation et qui perçoivent des subsides d’origine étrangère pour couvrir leurs frais d’entretien sont exonérés d’impôt par l’Etat d’accueil.</p>
<h3 class="spip"><a id="sommaire_6"></a>Autres catégories de revenus</h3>
<p><strong>Bénéfices industriels et commerciaux</strong></p>
<p>L’article 7, paragraphe 1, dispose que les entreprises industrielles et commerciales sont imposables sur le territoire où se trouve l’établissement stable.</p>
<p>La notion d’établissement stable est définie à l’article 5 de la Convention.</p>
<p><strong>Bénéfices des professions non commerciales et des revenus non commerciaux</strong></p>
<p>L’article 14, paragraphe 1, stipule que les revenus provenant de l’exercice d’une profession libérale ou d’autres activités indépendantes sont imposables dans l’Etat sur le territoire duquel se trouve l’installation permanente où s’exerce de façon régulière l’activité personnelle. Les revenus que les professionnels du spectacle ainsi que les sportifs réalisent, en cette qualité ou par l’intermédiaire d’une personne autre, dans l’un des deux Etats, restent imposables dans l’Etat d’exercice de l’activité selon les dispositions de l’article 17 de la convention.</p>
<p><strong>Revenus immobiliers</strong></p>
<p>Les revenus des biens immobiliers, y compris les bénéfices des exploitations agricoles, sont imposables dans l’Etat où ils sont situés (article 6 de la Convention).</p>
<p>Cette règle s’applique également aux gains provenant de la cession ou de l’échange desdits biens ou droits selon les dispositions de l’article 13, paragraphe 1.</p>
<p><strong>Revenus de capitaux mobiliers - les dividendes</strong></p>
<p>Ce terme désigne les revenus provenant d’actions, actions ou bons de jouissance, parts de mines, parts de fondateur ou autres parts bénéficiaires à l’exception des créances, ainsi que les revenus d’autres parts sociales soumis au même régime fiscal que les revenus d’actions.</p>
<p>De manière générale, l’article 10 pose le principe de l’imposition des dividendes dans l’Etat contractant où la société distributrice a son domicile fiscal.</p>
<p>Le paragraphe 2 de l’article 10 dispose que les dividendes distribués donnent lieu à un prélèvement à la source au taux de 25%, ramené à 15% lorsque le bénéficiaire est une société qui détient directement une participation représentant au moins 25% du capital de la société distributrice. Par ailleurs, le bénéfice de l’avoir fiscal n’est pas accordé aux résidents de Madagascar.</p>
<p><strong>Revenus de capitaux mobiliers - les intérêts</strong></p>
<p>Ce terme désigne les revenus des fonds publics, des obligations d’emprunts, des obligations d’emprunts et des créances de toute nature ainsi qu’aux intérêts produits par des titres négociables, les bons de caisse et les intérêts de créances ordinaires.</p>
<p>Le paragraphe 1 de l’article 11 précise que les intérêts provenant d’un Etat et payés à un résident de l’autre Etat sont imposables dans le premier Etat. Le taux d’imposition est limité à 15% du revenu brut si le résident est le bénéficiaire effectif.</p>
<h3 class="spip"><a id="sommaire_7"></a>Barème de l’impôt</h3>
<p><strong>L’impôt sur le revenu applicable aux personnes physiques résidentes de Madagascar</strong></p>
<p>Vous trouverez sous le présent titre, le dernier barème d’imposition des revenus ainsi que certaines déductions du revenu imposable admises.</p>
<p><strong>Remarques</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  L’impôt général sur les revenus salariaux est <strong>retenu à la source par l’employeur </strong>ou l’organisme payeur lors de chaque paiement mensuel et versé au Trésor.</p>
<p>Principal dans les 15 premiers jours du mois suivant au moyen d’un bordereau fourni par l’administration.</p>
<ul class="spip">
<li>L’employeur ou l’organisme payeur est également tenu de déposer au Bureau des Contributions Directes un état nominatif des sommes payées aux salariés.</li>
<li>Les contribuables dont l’employeur est hors du territoire national doivent déterminer et verser eux-mêmes l’impôt correspondant au cours du mois considéré.</li>
<li>De plus, les bénéficiaires des revenus salariaux doivent avant le 15 janvier de chaque année déclarer au Bureau des Contributions Directes dont ils dépendent le montant des revenus qu’ils perçoivent.</li></ul>
<h3 class="spip"><a id="sommaire_8"></a>Quitus fiscal</h3>
<p>Avant de quitter le pays, l’expatrié devra fournir un quitus fiscal exigé par le ministère de l’Intérieur.</p>
<h3 class="spip"><a id="sommaire_9"></a>Solde du compte en fin de séjour</h3>
<p>Pour solder son compte en fin de séjour, un expatrié doit également obtenir l’autorisation du même ministère.</p>
<h3 class="spip"><a id="sommaire_10"></a>Coordonnées des centres d’information fiscale</h3>
<p><a href="http://www.impots.mg/" class="spip_out" rel="external">Direction générale des impôts</a><br class="manualbr">Immeuble des Finances et du Budget<br class="manualbr">Antaninarenina<br class="manualbr">Antananarivo(101)-BP 863<br class="manualbr">Tél. : 22 335 50/22 287 08<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/fiscalite/#dgimpots#mc#moov.mg#" title="dgimpots..åt..moov.mg" onclick="location.href=mc_lancerlien('dgimpots','moov.mg'); return false;" class="spip_mail">Courriel</a></p>
<p><i>Mise à jour : janvier 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-madagascar-fiscalite-article-convention-fiscale.md" title="Convention fiscale">Convention fiscale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-madagascar-fiscalite-article-fiscalite-du-pays.md" title="Fiscalité du pays">Fiscalité du pays</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/fiscalite/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
