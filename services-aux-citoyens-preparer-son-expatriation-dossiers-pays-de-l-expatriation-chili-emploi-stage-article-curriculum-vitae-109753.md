# Curriculum vitae

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/emploi-stage/article/curriculum-vitae-109753#sommaire_1">Rédaction</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/emploi-stage/article/curriculum-vitae-109753#sommaire_2">Diplômes</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Rédaction</h3>
<p>Le curriculum vitae est un document qui reflète le parcours personnel et professionnel d’une personne. Le but est de décrocher un entretien. Pour ce faire, il faut tenir compte des points suivants lors de son élaboration :</p>
<p><strong>Les CV adressés aux entreprises, même françaises, doivent toujours être rédigés en espagnol.</strong></p>
<ul class="spip">
<li>Créer un curriculum vitae personnalisé et spécifique pour chaque emploi auquel vous souhaitez postuler.</li>
<li>Actualiser constamment les informations de votre curriculum vitae.</li>
<li>La lettre de motivation n’est pas nécessaire au Chili. Elle peut cependant faire la différence.</li>
<li>Faire ressortir les points les plus importants : les connaissances, l’expérience, tout ce qui a un rapport avec le poste proposé et représente un avantage important par rapport aux autres candidats.</li>
<li>Ne pas avancer des éléments que vous ne pourrez pas expliquer verbalement et qui pourraient vous mettre en difficulté lors de l’entretien.</li></ul>
<p>Les curriculum vitae chiliens sont généralement plus longs que les CV français. Il y a encore quelques années, tout y était mentionné de la maternelle à l’université. Aujourd’hui, les recruteurs apprécient de plus en plus la capacité des candidats à résumer leur parcours et à synthétiser leurs principaux points forts. Privilégiez donc un CV court : une page pour les débutants et trois pages (grand maximum) pour les personnes disposant d’une longue expérience. Par ailleurs, il faut toujours joindre une photographie récente à son CV.</p>
<h4 class="spip">Le CV doit contenir les informations suivantes</h4>
<p><strong>1. Antecedentes Personales</strong><br class="manualbr">Informations personnelles :</p>
<ul class="spip">
<li>nom, prénom,</li>
<li>âge et date de naissance,</li>
<li>adresse et téléphone,</li>
<li>état civil,</li>
<li>nationalité,</li>
<li>numéro de RUT (carte d’identité chilienne) ou, à défaut, du passeport.</li></ul>
<p><strong>2. Antecedentes Académicos</strong><br class="manualbr">Formation :</p>
<ul class="spip">
<li>lieu et dates des études secondaires et universitaires.</li>
<li>Diplômes obtenus.</li>
<li>On peut également indiquer les formations complémentaires (séminaires, conférences).</li></ul>
<p><strong>3. Idiomas</strong><br class="manualbr">Langues parlées.</p>
<p><strong>4. Antecedentes Laborales</strong><br class="manualbr">Expérience professionnelle (stages inclus).</p>
<p><strong>5. Otros</strong><br class="manualbr">Connaissances en informatique, permis de conduire, etc.</p>
<p><strong>6. Referencias</strong><br class="manualbr">Recommandations et références d’anciens postes occupés (nom de l’entreprise, personne à contacter et numéro de téléphone). Pour les postes à responsabilité, les recruteurs chiliens contacteront la personne référence.</p>
<p><strong>7. Pretensiones salariales</strong><br class="manualbr">Si vous répondez à une annonce, il vous sera demandé d´indiquer vos « prétentions salariales » sur votre CV. Dans le cas contraire, vous devrez les communiquer lors du premier entretien. Attendez donc de connaître vos fonctions et responsabilités et renseignez-vous auparavant sur le montant exigé en moyenne pour le poste auquel vous postulez.</p>
<h4 class="spip">Format du CV</h4>
<p>Au Chili, les CV sont généralement présentés sous la forme inversée afin de remonter de votre expérience la plus récente à votre formation d’origine.</p>
<h3 class="spip"><a id="sommaire_2"></a>Diplômes</h3>
<p>Il n’existe pas d’équivalence officielle entre les diplômes français et chiliens.</p>
<p>L´<a href="http://www.uchile.cl/rrii" class="spip_out" rel="external">Université du Chili</a>  est chargée d´établir l´équivalence et le niveau des diplômes au cas par cas. Pour plus d´informations et pour connaître les documents requis, vous pouvez consultez leur <a href="http://www.uchile.cl/rrii" class="spip_out" rel="external">site internet</a> / rubrique <i>Relaciones internacionales</i>.</p>
<p>Vos diplômes pourront vous être demandés pour un emploi et vous seront utiles pour faire une demande de visa temporaire.</p>
<p>Il est conseillé de faire [légaliser-rub1499], par le ministère français des Affaires étrangères, puis par le <a href="http://www.cgparis.cl/" class="spip_out" rel="external">Consulat général du Chili en France</a>, vos certificats et diplômes d´études <strong>avant votre départ de France</strong>. Si cette formalité n’a pas été effectuée en France, l’<a href="http://www.france.cl/" class="spip_out" rel="external">Ambassade de France au Chili</a> pourra procéder à cette légalisation. Le document devra ensuite être légalisé au Chili par le <a href="http://www.minrel.gob.cl/legalizacion-de-documentos/minrel/2008-06-19/154031.html" class="spip_out" rel="external">ministère chilien des Relations extérieures</a> (Ministerio de Relaciones Exteriores, Departamento de Legalizaciones).</p>
<p>Une fois légalisés, les diplômes français doivent être traduits en espagnol par un traducteur assermenté au Chili. Vous pouvez obtenir la liste de ces traducteurs auprès du ministère chilien des Relations extérieures ou auprès de l’Ambassade de France au Chili.</p>
<p>Les systèmes éducatifs chiliens et français sont suffisamment proches pour que votre interlocuteur puisse comprendre votre parcours avec ses références.</p>
<ul class="spip">
<li><strong>CAP/BEP</strong> : "Diploma técnico en … (2 años de estudios)"</li>
<li><strong>Brevet des collèges</strong> : "Diploma de enseñanza básica completa"</li>
<li><strong>Baccalauréat </strong> : "Prueba de Selección Universitaria (PSU)"</li>
<li><strong>DEUG / DUT / DU</strong> : "Estudios de pregrado en… (indiquer le nombre d´années d´études)"</li>
<li><strong>Licence en … (préciser la matière)</strong> : "Diploma universitario en…………" ou "Diploma en….de la Universidad…." (Dans les deux cas, indiquez le nombre d´années d´études)</li>
<li><strong>Master Professionnel / Master de recherche</strong> : "Estudios de postgrado / Master"</li>
<li><strong>Doctorat </strong> : "Doctorado"</li></ul>
<p>Les noms des diplômes français ne signifient pas grand chose pour les Chiliens. Détaillez, par conséquent, chaque lettre composant le nom de votre diplôme et précisez les cours suivis et la formation obtenue.</p>
<p>Voici différentes possibilités pour présenter vos diplômes :</p>
<p><strong>Apprentissage</strong><br class="manualbr">Formación alternando estudios teóricos y práctica en empresas.</p>
<p><strong>BTS : Brevet de technicien supérieur</strong><br class="manualbr">Programa de estudios de pregrado de dos años especializado en …</p>
<p><strong>DUT : Diplôme universitaire de technologie</strong><br class="manualbr">Programa de estudios universitarios de dos años especializado en …</p>
<p><strong>Licence</strong> : Programa de estudios universitarios de tres años enfocado en …</p>
<p><strong>Classes preparatoires HEC</strong><br class="manualbr">Dos años de preparación a los concursos nacionales de Prestigiosas Escuelas de Comercio francesas (Graduate Business Schools).</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/emploi-stage/article/curriculum-vitae-109753). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
