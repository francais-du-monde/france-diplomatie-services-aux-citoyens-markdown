# Communications

<h4 class="spip">Téléphone - Internet</h4>
<p>Les services téléphoniques sont dans l’ensemble de bonne qualité. Le téléphone est distribué sous l’enseigne CANTV. Le marché des opérateurs s’est ouvert aux opérateurs privés qui offrent toutes sortes de services pour les communications nationales et internationales. On trouve facilement des cabines téléphoniques et des centres de téléphonie. Des cartes de téléphone sont en vente dans les kiosques de presse et on peut louer un téléphone cellulaire à l’aéroport.</p>
<p>Pour rappel :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>De la France vers le Venezuela</strong> : 00-58 + indicatif de la ville à deux chiffres (212 pour Caracas) + numéro du correspondant.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Du Venezuela vers la France</strong> : 00-33 + code opérateur + numéro du correspondant (à 9 chiffres, sans le 0).</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Appels intérieurs</strong> : composer le 0, puis le code de la ville.</p>
<p>Vous pouvez également utiliser des logiciels de <strong>téléphonie sur IP</strong> (Skype, Google talk, Live Messenger, Yahoo ! Messenger, etc.) car les tarifs proposés à l’international sont souvent moindre que ceux des opérateurs locaux exploitant des lignes classiques.</p>
<p>Internet est bien répandu au Venezuela. Dans les grandes villes, on trouve facilement des points de connexion Internet pour un tarif d’environ 2 € de l’heure, et les hôtels proposent souvent une connexion Internet. On trouve aussi des cybercafés dans toutes les grandes villes et les villes de taille moyenne.</p>
<h4 class="spip">Poste</h4>
<p>La poste vénézuélienne fonctionne mal. L’acheminement est beaucoup plus long qu’en France : il faut compter entre 4 jours et 3 semaines pour qu’un courrier arrive à destination, même s’il provient d’une autre ville vénézuélienne. Il est conseillé d’avoir recours à des services privés type DHL ou FEDEX.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/vie-pratique/article/communications). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
