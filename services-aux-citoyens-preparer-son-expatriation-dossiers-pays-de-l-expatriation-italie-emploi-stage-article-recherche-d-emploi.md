# Recherche d’emploi

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/emploi-stage/article/recherche-d-emploi#sommaire_1">Outils pour la recherche d’emploi</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/emploi-stage/article/recherche-d-emploi#sommaire_2">Organismes pour la recherche d’emploi</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Outils pour la recherche d’emploi</h3>
<h4 class="spip">Journaux</h4>
<ul class="spip">
<li><a href="http://www.corriere.it/" class="spip_out" rel="external">Corriere della Sera</a> qui publie chaque vendredi le <a href="http://lavoro.corriere.it/" class="spip_out" rel="external">Corriere&amp;Lavoro</a> ;</li>
<li><a href="http://www.repubblica.it/" class="spip_out" rel="external">La Repubblica</a> publie des pages emploi chaque jeudi ;</li>
<li>"Il Giornale" publie des offres d’emploi chaque jeudi ;</li>
<li><a href="http://www.ilsole24ore.com/" class="spip_out" rel="external">Il sole 24 Ore</a> publie des pages emploi chaque lundi ;</li>
<li><a href="http://www.lastampa.it/" class="spip_out" rel="external">La Stampa</a> publie des offres d’emploi tous les jours et le vendredi à Turin ;</li>
<li><a href="http://www.ilmessaggero.it/" class="spip_out" rel="external">Il Messaggero</a> publie des offres d’emploi chaque vendredi (Rome) ;</li>
<li><a href="http://www.lavorare.net/" class="spip_out" rel="external">Lavorare a Roma e nel Lazio</a> chaque mardi ;</li>
<li><a href="http://www.obiettivolavoro.it/" class="spip_out" rel="external">Obiettivo Lavoro" pour Rome et le Latium</a> ;</li>
<li><a href="http://www.bollettinodellavoro.it/" class="spip_out" rel="external">Bollettino del lavoro</a> (magazine bimensuel donnant des informations sur l’emploi, publication des avis de concours, offres d’embauche des entreprises privées, etc).</li></ul>
<h4 class="spip">Sites internet</h4>
<ul class="spip">
<li><a href="http://www.emploi-international.org/" class="spip_out" rel="external">Emploi international</a></li>
<li><a href="http://www.lavoro.gov.it/Pages/default.aspx" class="spip_out" rel="external">Site du ministère du travail</a></li>
<li><a href="http://www.job-chambre.it/" class="spip_out" rel="external">Site de la Chambre française de commerce</a></li>
<li><a href="http://www.provincia.fi.it/lavoro/" class="spip_out" rel="external">Province de Florence</a></li>
<li><a href="http://www.provincia.bologna.it/probo/Engine/RAServePG.php" class="spip_out" rel="external">Province de Bologne</a></li>
<li><a href="http://www.provincia.torino.it/" class="spip_out" rel="external">Province de Turin</a></li>
<li><a href="http://www.romalavoro.net/" class="spip_out" rel="external">Rome</a></li>
<li><a href="http://www.comune.milano.it/" class="spip_out" rel="external">Milan</a></li></ul>
<h4 class="spip">Réseaux</h4>
<p>La prospection se fait au travers des petites annonces de la presse ou des sites Internet. Cependant, c’est le réseau relationnel qui est la principale source de contacts pour trouver un emploi. Cultivez vos connaissances et parlez de vos recherches à votre entourage.</p>
<h3 class="spip"><a id="sommaire_2"></a>Organismes pour la recherche d’emploi</h3>
<p>Si vous avez une bonne connaissance de l’italien vous pouvez vous renseigner auprès des organismes suivants :</p>
<ul class="spip">
<li><a href="http://www.chambre.it/" class="spip_out" rel="external">Service de l’emploi de la Chambre française de commerce et d’industrie en Italie de Milan</a><br class="manualbr">Tél. : (+39) 02 72 53 72 01. <br class="manualbr">A Rome, un conseiller emploi tiendra désormais une permanence pour informer les demandeurs sur les possibilités de formations et d’emploi (02 72 53 01).</li>
<li>Les <a href="http://www.informagiovani.it/" class="spip_out" rel="external">Informagiovani</a> sont des centres publics ou privés, présents dans plusieurs villes italiennes, qui offrent une quantité d’informations aux jeunes sur la recherche d’emploi, le logement, la formation, les loisirs.</li>
<li>Le <strong>réseau Eures</strong> (European Employment Services) est présent dans toute l’Europe et relie les Euroconseillers qui mettent en relation des offres d’emploi avec des demandes au niveau européen. Ces Euroconseillers peuvent aussi donner des renseignements sur le marché du travail local.</li>
<li>Des <strong>associations </strong>diverses publiques ou privées, ont pour fonction l’appariement des offres et des demandes d’emploi. C’est le cas d’<a href="http://www.unimpiego.it/" class="spip_out" rel="external">Unimpiego</a> (dans le Piemont). Vous pouvez également voir le <a href="http://www.lavoro.gov.it/Pages/default.aspx" class="spip_out" rel="external">site du ministère du travail</a> ou consulter le site des <a href="http://www.paginegialle.it/" class="spip_out" rel="external">Pages jaunes</a> / rubrique "Ricerca e selezione di personale" pour avoir des adresses de cabinets de recrutement.</li></ul>
<p>Beaucoup de <strong>sociétés d’intérim</strong> ont vu le jour. Les plus importantes : Adecco, Kelly, Manpower, Temporary, Ad Interim, Obiettivo Lavoro, Sinterim, Italia Lavoro, Quandoccorre… presque toutes ont un site internet. Pour cela voir dans l’<a href="http://www.paginegialle.it/" class="spip_out" rel="external">annuaire électronique italien</a> et rechercher « genzia interinale ». Vous pouvez également contacter des <strong>cabinets de recrutement</strong> tels que <a href="http://www.aimsitalia.com/home.aspx" class="spip_out" rel="external">AIMS Italia</a>.</p>
<p>Enfin, les <strong>organisations syndicales</strong> peuvent offrir des informations sur les offres d’emploi, le droit du travail ou encore les formations professionnelles :</p>
<ul class="spip">
<li><a href="http://www.cgil.it/" class="spip_out" rel="external">Confederazione Generale Italiana del Lavoro</a> (CGIL) ;</li>
<li><a href="http://www.cisl.it/" class="spip_out" rel="external">Confederazione Italiana Sindicati Lavoratori</a> (CISL) ;</li>
<li><a href="http://www.uil.it/" class="spip_out" rel="external">Unione Italiana del Lavoro</a> (UIL)</li></ul>
<p><strong>L’agence nationale pour l’emploi (Sezione circoscrizionale per l’impiego)</strong></p>
<p><a href="http://www.centroimpiego.it/" class="spip_out" rel="external">Les bureaux de l’emploi</a>) sont les ex-<i>uffici del collocamento</i>. Le demandeur d’emploi s’y inscrit comme personne à la recherche d’un emploi immédiatement disponible, en présentant le permis de séjour et le code fiscal. Il peut y trouver des informations sur l’emploi, des offres, ainsi que des mini-stages sur les techniques de recherche d’emploi.</p>
<p>Il est à noter que même si l’inscription n’est plus nécessaire pour pouvoir accéder à un travail salarié, certains employeurs l’exigent. Elle prouve que l’on est sans emploi. Elle se fait auprès de la sezione circoscrizionale du lieu de résidence.</p>
<p>Différents documents sont à fournir :</p>
<ul class="spip">
<li>demande sur imprimé (remis sur place) ;</li>
<li>fiche familiale d’état civil (stato di famiglia) délivrée par la mairie ou autocertificazione (déclaration sur l’honneur) ;</li>
<li>numéro d’identification fiscal (codice fiscale) ;</li>
<li>passeport ou carte nationale d’identité en cours de validité.</li></ul>
<p>La <i>sezione circoscrizionale per l’impiego</i> remet une attestation d’inscription (<i>tesserino rosa</i>) qui certifie l’inscription au chômage. Un tampon annuel doit confirmer la validité de cette carte.</p>
<p><strong>Formation professionnelle continue</strong></p>
<p>La formation continue est peu pratiquée en Italie.</p>
<p>L’Etat, par l’intermédiaire des régions, organise des cours de formation pour les chômeurs, avec des financements du Fond social européen et du ministère du Travail. Ces formations sont gratuites mais non rémunérées, elles s’adressent aux chômeurs de longue durée inscrits au chômage depuis au moins un an.</p>
<p>Les organismes de formation qui proposent une formation formelle sont principalement les écoles et centres de formation professionnels, les centres territoriaux permanents, les centres municipaux et les universités ; l’organisme le plus connu est l’ENAIP. Les employeurs sont assez peu sensibles à la formation qui reste entièrement à leur charge car aucun fond de formation n’est prévu actuellement (sauf dans le secteur intérimaire et pour les cadres supérieurs).</p>
<p>Les cours de formation et masters organisés par les organismes privés et les universités ont un coût élevé.</p>
<p>A l’issue de ces formations sont délivrés des <i>attestati regionali</i> n’équivalant pas à un diplôme d’état (<i>diploma di stato</i>).</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li>Site du <a href="http://www.cedefop.europa.eu/" class="spip_out" rel="external">Centre européen pour le développement de la formation professionnelle</a> (CEDEFOP)</li>
<li>Site de l’<a href="http://www.isfol.it/" class="spip_out" rel="external">Institut pour le développement de la formation professionnelle et du travail</a> (ISFOL)</li></ul>
<p><strong>Ce que recherchent les recruteurs</strong></p>
<p>Il est indispensable de bien parler l’italien. La connaissance de l’anglais est souvent demandée.</p>
<p>Quelques professions comme les ingénieurs spécialisés ou les télé-vendeurs, peuvent se contenter du français lors des premières semaines de contrat à condition de maîtriser l’anglais ou une autre langue étrangère (l’allemand est recherché dans le nord de l’Italie). Dans ce cas, une formation à l’apprentissage de l’italien est souvent financée par l’entreprise ou par une structure locale associative à but non lucratif.</p>
<p>Pour les postes de secrétariat, le niveau d’italien écrit doit être également excellent.</p>
<p>Beaucoup de postes commerciaux, y compris ceux de la grande distribution, exigent une parfaite connaissance de l’environnement italien, voire des dialectes de certaines régions lorsque la clientèle est composée de petits utilisateurs (carrossiers, mécaniciens …)</p>
<p>L’inspection d’académie (<i>Provveditorato</i>) organise des cours d’italien pour étrangers, dispensés gratuitement dans les écoles. Il existe de nombreuses écoles privées qui offrent des cours d’italien pour adultes, dont les prix varient.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/emploi-stage/article/recherche-d-emploi). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
