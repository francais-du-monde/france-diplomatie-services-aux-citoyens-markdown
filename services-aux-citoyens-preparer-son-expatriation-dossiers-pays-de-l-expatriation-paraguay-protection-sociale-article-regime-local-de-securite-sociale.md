# Régime local de sécurité sociale

<h4 class="spip">Financement</h4>
<p>La sécurité sociale est financée grâce à des cotisations de 25,5% sur les salaires versées à l’Institut de prévision sociale (IPS) par les employeurs (16,5%) et les employés (9%).</p>
<h4 class="spip">Principales prestations</h4>
<table class="spip">
<thead><tr class="row_first"><th id="ide56a_c0">Assurance  </th><th id="ide56a_c1">Nature des prestations  </th><th id="ide56a_c2">Base de calcul  </th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="ide56a_c0">Maladie</td>
<td headers="ide56a_c1">indemnités journalières</td>
<td headers="ide56a_c2">50% du salaire pendant 6 mois</td></tr>
<tr class="row_even even">
<td headers="ide56a_c0">Maternité</td>
<td headers="ide56a_c1">indemnités journalières</td>
<td headers="ide56a_c2">50% du salaire</td></tr>
<tr class="row_odd odd">
<td headers="ide56a_c0">Accidents du travail  et maladies professionnelles</td>
<td headers="ide56a_c1">indemnités journalières</td>
<td headers="ide56a_c2">75% du salaire</td></tr>
<tr class="row_even even">
<td headers="ide56a_c0">Invalidité</td>
<td headers="ide56a_c1">pension</td>
<td headers="ide56a_c2">42,5% du salaire mensuel</td></tr>
<tr class="row_odd odd">
<td headers="ide56a_c0">Décès – veuvage</td>
<td headers="ide56a_c1">-pension pour la veuve
<p>-pension pour les enfants jusqu’à 16 ans</p>
</td>
<td headers="ide56a_c2">- 40% de la pension due à l’assuré
<p>- 20% de la pension due à l’assuré</p>
</td></tr>
<tr class="row_even even">
<td headers="ide56a_c0">Vieillesse</td>
<td headers="ide56a_c1">pension</td>
<td headers="ide56a_c2">100% du salaire mensuel calculé  sur les cinq dernières années</td></tr>
<tr class="row_odd odd">
<td headers="ide56a_c0">Prestations familiales</td>
<td headers="ide56a_c1">allocation</td>
<td headers="ide56a_c2">5% du salaire minimum par enfant</td></tr>
</tbody>
</table>
<p><strong>Il n’existe pas de couverture sociale pour le chômage.</strong></p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/paraguay/protection-sociale/article/regime-local-de-securite-sociale). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
