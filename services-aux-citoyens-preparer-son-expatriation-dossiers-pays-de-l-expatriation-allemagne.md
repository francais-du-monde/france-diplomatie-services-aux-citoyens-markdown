# Allemagne

<p>La communauté française en Allemagne est estimée à plus de <strong>160 000 personnes</strong>.</p>
<p><strong>Au 31 décembre 2014</strong>, <strong>112 879 Français</strong> étaient inscrits au registre des Français établis hors de France (46 202 personnes inscrites auprès de notre consulat à <a href="http://www.ambafrance-de.org/-Munich-" class="spip_out" rel="external">Munich</a>, 43 176 à <a href="http://www.ambafrance-de.org/-Francfort-" class="spip_out" rel="external">Francfort</a>, 23 501 à <a href="http://www.ambafrance-de.org/-Francais-" class="spip_out" rel="external">l’ambassade de France à Berlin</a>).</p>
<p>Les Français du Bade-Wurtemberg sont établis dans les principales villes de la zone frontalière (Fribourg en Brisgau, Karlsruhe, Baden-Baden, Offenbourg et Mannheim), à Stuttgart, capitale du land, et au sud, dans la région de Donaueschingen, Immendingen et Villingen-Schwenningen.</p>
<p>En Bavière, la population française se concentre dans le sud du land, principalement à Munich.</p>
<p>Dans les länder du Nord et de l’Est du pays (Berlin, Hambourg, Basse-Saxe, Brandenbourg, Brême, Mecklembourg-Poméranie, Saxe, Saxe-Anhalt, Schleswig-Holstein et Thuringe), la majorité des Français sont installés dans la capitale et les grandes villes (Hambourg, Hanovre, Brême, Leipzig, Dresde, Rostock, Kiel et Halle).</p>
<p>En Rhénanie-Palatinat, les Français sont établis dans les anciennes villes de garnison françaises (Kaiserslautern, Ludwigshafen, Trèves, Mayence, Landau et Coblence). En Hesse, la population française est implantée dans la région "Rhein-Main" et principalement dans les villes de Francfort sur le Main, Wiesbaden et Darmstadt. Enfin, en Rhénanie du Nord - Westphalie, la communauté française se concentre dans les grandes agglomérations de Düsseldorf, Cologne, Bonn, Arnsberg, Münster et Detmold.</p>
<p>On compte <strong>près de 1600 d’entreprises</strong> ou succursales françaises implantées en Allemagne (au 31 décembre 2011).</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/dossiers-pays/allemagne/" class="spip_in">Une description de l’Allemagne, de sa situation politique et économique</a></li>
<li>Des informations actualisées sur les <a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/allemagne/" class="spip_in">conditions locales de sécurité en Allemagne</a></li>
<li><a href="http://www.ambafrance-de.org/-Francais-" class="spip_out" rel="external">Ambassade de France en Allemagne</a></li></ul>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-entree-et-sejour.md">Entrée et séjour</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-entree-et-sejour-article-passeport-visa-permis-de-travail-109105.md">Passeport, visa, permis de travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-entree-et-sejour-article-demenagement.md">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-entree-et-sejour-article-vaccination.md">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-entree-et-sejour-article-animaux-domestiques.md">Animaux domestiques</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-emploi-stage.md">Emploi, stage</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-emploi-stage-article-marche-du-travail-109109.md">Marché du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-emploi-stage-article-reglementation-du-travail.md">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-emploi-stage-article-recherche-d-emploi-109111.md">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-emploi-stage-article-curriculum-vitae.md">Curriculum vitae</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-emploi-stage-article-lettre-de-motivation-109113.md">Lettre de motivation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-emploi-stage-article-entretien-d-embauche-109114.md">Entretien d’embauche</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-emploi-stage-article-stages.md">Stages</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-protection-sociale-22725.md">Protection sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-protection-sociale-22725-article-regime-local-de-securite-sociale-109116.md">Régime local de sécurité sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-protection-sociale-22725-article-convention-de-securite-sociale-109117.md">Convention de sécurité sociale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-fiscalite.md">Fiscalité</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-fiscalite-article-fiscalite-du-pays.md">Fiscalité du pays</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-fiscalite-article-convention-fiscale-109120.md">Convention fiscale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-vie-pratique.md">Vie pratique</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-vie-pratique-article-logement-109121.md">Logement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-vie-pratique-article-sante-109122.md">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-vie-pratique-article-scolarisation-109123.md">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-vie-pratique-article-cout-de-la-vie-109124.md">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-vie-pratique-article-transports.md">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-vie-pratique-article-communications-109127.md">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-vie-pratique-article-loisirs-et-culture.md">Loisirs et culture</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-presence-francaise.md">Présence française</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-presence-francaise-article-presence-francaise.md">Présence française</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
