# Recherche d’emploi

<p><strong>Organismes pour la recherche d’emploi</strong></p>
<ul class="spip">
<li>Il n’existe pas au Ghana d’organisme équivalent à l’ANPE ;</li>
<li>Il n’y a pas de structure particulière à l’Ambassade de France pour la recherche d’emploi ;</li>
<li>La Chambre de commerce est en cours de formation.</li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/emploi-stage/article/recherche-d-emploi). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
