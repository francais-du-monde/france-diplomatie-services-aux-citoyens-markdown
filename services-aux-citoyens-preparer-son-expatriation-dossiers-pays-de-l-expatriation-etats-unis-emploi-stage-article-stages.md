# Stages

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/emploi-stage/article/stages#sommaire_1">Stage professionnel</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/emploi-stage/article/stages#sommaire_2">Stage d’étude</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/emploi-stage/article/stages#sommaire_3">Avez-vous pensé au volontariat international (V.I.E ou V.I.A.) ?</a></li></ul>
<p>Un visa de type J-1 est obligatoire pour effectuer un stage aux Etats-Unis. Vous trouverez toutes les informations relatives au visa J-1 dans la rubrique <a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-etats-unis-entree-et-sejour-23321.md" class="spip_in">Entrée et séjour</a>.</p>
<p>Un stage aux Etats-Unis n’est pas autorisé dans le cadre du programme d’exemption de visa. Même si vous partez moins de 90 jours, vous devez obtenir un visa J-1, que vous soyez rémunéré ou non.</p>
<p>Pour pouvoir participer au programme J-1 en tant que stagiaire il faut correspondre à l’un des deux profils suivants : le profil <i>Trainee</i> ou le profil <i>Intern</i>.</p>
<p>Les stages sont autorisés dans les secteurs suivants : arts et culture, médias et communication, éducation, sciences sociales, sciences de l’information, conseil et services sociaux, gestion, commerce et finance, hôtellerie et tourisme, aviation, sciences et ingénierie, industrie, architecture, bâtiment, agriculture, sylviculture et pêche, administration et droit.</p>
<p>Le gouvernement américain n’autorise pas les stagiaires étrangers à exercer un emploi qui ne nécessite pas de qualification particulière, par exemple : barman ou barmaid, technicien(ne) de surface, caissier(e), chauffeur de taxi, etc … (voir le site Internet du <a href="http://www.state.gov/" class="spip_out" rel="external">Département d’Etat</a> pour connaître les professions qui ne peuvent pas admettre de stagiaires étrangers).</p>
<p>Le candidat-stagiaire a la possibilité de chercher son futur employeur par ses propres moyens mais ne peut obtenir de visa J-1 sans passer par un organisme agréé souvent appelé <i>organisme sponsor</i>. Cet organisme doit avoir reçu l’agrément du Département d’Etat américain.</p>
<p>L’organisme sponsor a l’autorisation de vous fournir un certificat d’éligibilité appelé certificat DS-2019.</p>
<p>Nouvelle réglementation : depuis le 19 juillet 2007, le candidat-stagiaire doit en plus du DS-2019 présenter au service consulaire de l’ambassade des Etats-Unis pour l’obtention de son visa J-1 un nouveau document appelé <i>Training / Internship Placement Plan – T/IPP</i> DS-7002 – Form. Ce formulaire est en quelque sorte une convention de stage, remplie et signée par l’employeur, le stagiaire et l’organisme sponsor. Ce dernier est obligé de rendre visite à l’entreprise et de vérifier qu’elle est prête à respecter les différentes réglementations édictées par le Département d’Etat. <a href="http://www.state.gov/documents/organization/84240.pdf" class="spip_out" rel="external">Le formulaire DS-7002 peut être téléchargé en ligne</a>. Le DS-2019 ne peut désormais être délivré par l’organisme sponsor sans le <i>Training / Internship Placement Plan – T/IPP</i> DS-7002.</p>
<p>Niveau d’anglais requis : le candidat-stagiaire doit avoir un niveau d’anglais qui lui permet de communiquer aisément à l’intérieur d’une entreprise. Contrairement au programme Au Pair par exemple, aucun cours d’anglais n’est prévu pour le stagiaire pendant son séjour aux Etats-Unis. Le niveau d’anglais du candidat sera vérifié par l’organisme sponsor soit par un test d’anglais soit par un entretien oral. Si votre niveau d’anglais s’avère insuffisant, le DS-2019 peut vous être refusé.</p>
<p>L’<a href="http://french.france.usembassy.gov/" class="spip_out" rel="external">Ambassade des Etats-Unis en France</a> communique sur son site Internet une liste d’organismes proposant des stages aux Etats-Unis.</p>
<p><i>Source : <a href="http://www.fulbright-france.org/gene/main.php" class="spip_out" rel="external">Commission franco-américaine d’échanges universitaires et culturels</a></i></p>
<h3 class="spip"><a id="sommaire_1"></a>Stage professionnel</h3>
<p>Les <i>Trainees</i> ont soit un diplôme de l’enseignement supérieur plus au moins un ans d’expérience professionnelle, soit au moins cinq ans d’expérience professionnelle. La durée maximum autorisée pour un stage est de 18 mois pour un <i>Trainee</i>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Stage d’étude</h3>
<p>Les <i>Interns</i> sont soit encore inscrits dans un établissement d’enseignement supérieur dans leur pays d’origine, soit titulaire d’un diplôme de l’enseignement supérieur depuis moins de 12 mois. Durée maximum de 12 mois pour un <i>Intern</i>.</p>
<h3 class="spip"><a id="sommaire_3"></a>Avez-vous pensé au volontariat international (V.I.E ou V.I.A.) ?</h3>
<p>Le V.I.E. permet aux entreprises françaises de confier à un jeune ressortissant de l’espace économique européen, âgé de 18 à 28 ans, une mission professionnelle (rémunérée) à l’étranger durant une période modulable de 6 à 24 mois.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/emplois-stages-concours/volontaires-internationaux/" class="spip_in">Notre thématique sur le volontariat international</a></li>
<li><a href="http://www.civiweb.com/FR/index.aspx" class="spip_out" rel="external">Civiweb.com</a></li>
<li><a href="http://www.businessfrance.fr/" class="spip_out" rel="external">Business France.fr</a> - <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/emploi-stage/article/stages#infoVIE#mc#ubifrance.fr#" title="infoVIE..åt..ubifrance.fr" onclick="location.href=mc_lancerlien('infoVIE','ubifrance.fr'); return false;" class="spip_mail">Courriel</a></li></ul>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/emploi-stage/article/stages). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
