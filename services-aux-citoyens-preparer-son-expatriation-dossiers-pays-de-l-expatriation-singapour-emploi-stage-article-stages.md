# Stages

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/emploi-stage/article/stages#sommaire_1">Stages professionnels</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/emploi-stage/article/stages#sommaire_2">Stages d’étude</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/emploi-stage/article/stages#sommaire_3">Avez-vous pensé au volontariat international (V.I.E ou V.I.A.) ?</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Stages professionnels</h3>
<p>Il convient de visiter le site du <a href="http://www.mom.gov.sg/FOREIGN-MANPOWER/PASSES-VISAS/Pages/default.aspx" class="spip_out" rel="external">Ministry of Manpower</a> pour se renseigner sur les modalités d’obtention du <i>Work Holiday Pass</i> et des visas pour stages. A noter, des règles plus restrictives pour la délivrance des <i>Work Holiday Passes</i> sont entrées en vigueur en décembre 2012.</p>
<p>La Chambre de commerce française à Singapour recense des <a href="http://www.fccsingapore.com/index.php?id=251" class="spip_out" rel="external">offres de stage</a> dans les entreprises françaises basées à Singapour.</p>
<p><strong>Attention :</strong> les règles singapouriennes d’immigration définies par le Ministry of Manpower  ne permettent pas aux ressortissants français d’effectuer de stages en entreprise sous couvert d’un « visa touristique ». Les Français dans cette situation doivent demander à leur employeur régulariser leur situation au plus vite sous peine d’être poursuivis par la justice locale.</p>
<h3 class="spip"><a id="sommaire_2"></a>Stages d’étude</h3>
<p>Les étudiants intéressés par un stage au ministère des Affaires étrangères et dans les ambassades de France à l’étranger doivent se rendre sur le site France Diplomatie (<a href="http://www.diplomatie.gouv.fr/fr/emplois-stages-concours/" class="spip_out">rubrique "Emplois, stages et concours"</a>), où sont détaillées les conditions et la procédure pour postuler à un stage, et où sont publiées toutes les offres de stages du ministère.</p>
<p>En ce qui concerne les stages dans des universités, les étudiants peuvent se référer en priorité au site des principales universités singapouriennes, notamment la <a href="http://www.nus.edu.sg/" class="spip_out" rel="external">National University of Singapore</a>, la <a href="http://www.ntu.edu.sg/" class="spip_out" rel="external">Nanyang Technological University</a> ou la <a href="http://www.smu.edu.sg/" class="spip_out" rel="external">Singapore Management University</a>).</p>
<h3 class="spip"><a id="sommaire_3"></a>Avez-vous pensé au volontariat international (V.I.E ou V.I.A.) ?</h3>
<p>Le V.I.E. permet aux entreprises françaises de confier à un jeune ressortissant de l’espace économique européen, âgé de 18 à 28 ans, une mission professionnelle (rémunérée) à l’étranger durant une période modulable de 6 à 24 mois.</p>
<p>Pour en savoir plus :</p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/emplois-stages-concours/volontaires-internationaux/" class="spip_in">Page thématique sur le volontariat international sur notre site</a></li>
<li><a href="http://www.civiweb.com/FR/index.aspx" class="spip_out" rel="external">Civiweb.com</a></li>
<li><a href="http://export.businessfrance.fr/default.html" class="spip_out" rel="external">Business France.fr</a> - <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/emploi-stage/article/stages#infoVIE#mc#ubifrance.fr#" title="infoVIE..åt..ubifrance.fr" onclick="location.href=mc_lancerlien('infoVIE','ubifrance.fr'); return false;" class="spip_mail">Courriel</a></li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/emploi-stage/article/stages). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
