# Curriculum vitae

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/emploi-stage/article/curriculum-vitae-110492#sommaire_1">Rédaction</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/emploi-stage/article/curriculum-vitae-110492#sommaire_2">Diplômes</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Rédaction</h3>
<p>Le CV et la lettre de motivation doivent impérativement être rédigés en anglais. La lettre de motivation doit être courte et dactylographiée. Pour le CV, précisez :</p>
<ul class="spip">
<li>le type de poste recherché,</li>
<li>les langues parlées,</li>
<li>les diplômes en français et leurs équivalences dans le système anglo-saxon.</li></ul>
<p>Insistez sur les séjours à l’étranger.</p>
<p>Si possible, présentez un dossier avec une adresse à Singapour, ce qui supprime la réticence liée à l’éloignement du candidat.</p>
<p>Pour les candidats dans les entreprises locales, joindre une copie des diplômes (avoir les originaux sur soi pour les montrer lors de l’entretien) et leurs traductions rédigées par un traducteur assermenté par l’Etat singapourien et certifiées conformes par le Consulat de France, ainsi que des lettres de références, traduites en anglais.</p>
<p>Afin de proposer votre candidature à une entreprise singapourienne ou étrangère, les étapes sont similaires à celles que vous pourriez entreprendre avec une entreprise française. Il suffit de réaliser un CV ainsi qu’une lettre de motivation. A Singapour, on utilise communément le terme de « resume » au lieu de CV. Ce n’est paradoxalement pas pour autant une version condensée du CV. Mais l’expression anglo-saxonne s’est imposée dans la terminologie utilisée.</p>
<p>Le CV est considéré comme un document marketing qui fournit des informations clés concernant vos connaissances, vos expériences, votre cursus et vos qualités personnelles. Il détaille très précisément votre cursus et vos expériences.</p>
<p>La particularité de Singapour est de le rédiger sur plusieurs pages : chaque expérience et formation sont décrites. Les candidats n’hésitent pas à fournir des chiffres, des statistiques…</p>
<p><strong>Pour postuler, plusieurs possibilités s’offrent à vous :</strong></p>
<ul class="spip">
<li>Vous répondez à une offre de stage ou d’emploi</li>
<li>Vous faites une candidature spontanée</li>
<li>Vous postulez directement sur le site Internet de l’entreprise</li>
<li>Vous déposez votre <i>resume</i> sur un site de recherche d’emploi</li>
<li>Vous rencontrez un chasseur de tête <i>head hunter</i> qui vous mettra en contact avec des employeurs intéressés.</li></ul>
<p>Si vous envoyez votre <i>resume</i> par la poste, envoyez-le aussi par messagerie électronique. Mais privilégiez toujours l’envoi par la poste. Il est plus facile de supprimer un message électronique que de jeter une lettre déjà matérialisée sous format papier.</p>
<p>Avant de rédiger votre <i>resume</i>, faites votre propre évaluation. Il faut penser à votre projet professionnel et orienter votre profil en fonction de l’objectif défini par avance. Si votre intérêt porte sur différents postes, dans différents secteurs, créez un <i>resume</i> pour chacun d’entre eux, orienté en fonction du poste et de la demande.</p>
<p><strong>Si vous répondez à une offre de stage ou d’emploi, vous pouvez suivre ces quelques étapes :</strong></p>
<ul class="spip">
<li>Repérez les mots clés de l’offre dans la présentation des fonctions à assumer, des produits ou services vendus ;</li>
<li>Prenez des renseignements sur l’entreprise, réalisez quelques recherches sur Internet ou utilisez des annuaires d’entreprises telles que le Kompass ;</li>
<li>Collectez l’ensemble de ces informations et faites une synthèse ;</li>
<li>Observez sous cet angle votre parcours professionnel, vos activités extra-professionnelles, vos formations. Déduisez-en vos compétences et aptitudes pour mettre en lumière les similitudes entre ce que vous avez réalisé et ce que vous serez amené à faire ;</li>
<li>Prouvez que vous êtes le ou la candidat(e) idéal(e) pour le poste.</li></ul>
<p>Sur cette base, revoyez votre CV afin de l’adapter si nécessaire et rédigez votre lettre de motivation.</p>
<p><strong>Pour être efficace et représentatif, le CV doit être :</strong></p>
<ul class="spip">
<li><strong>Détaillé</strong> : au moins deux pages d’explications. Pour les juniors qui ont peu d’expérience, détaillez celles que vous avez déjà eues, en précisant leurs durées respectives, même si ce n’est que quelques mois.</li>
<li><strong>Direct</strong> : faites apparaître clairement ce que vous proposez à l’entreprise et présenter également votre motivation professionnelle par une ou deux lignes en guise « d’accroche ».</li>
<li><strong>Lisible</strong> : la présentation doit être claire. Indiquez les points les plus importants en les listant plutôt que d’écrire de longues phrases ou paragraphes.</li>
<li><strong>Stylé</strong> : il n’y a pas de présentation type, elle se fait selon vos goûts, soyez original pour vous faire remarquer mais sans être extravagant.</li></ul>
<p><strong>Tenez compte des points suivants :</strong></p>
<ul class="spip">
<li>Un CV est toujours en anglais, même à l’attention d’une filiale française à Singapour et n’est jamais manuscrit.</li>
<li>Mettez votre photo afin que l’on se souvienne plus facilement de vous.</li>
<li>Imprimez seulement le recto, jamais de recto-verso.</li>
<li>Utilisez une taille de police entre 10 et 14.</li>
<li>Utilisez une seule et même police (Times New Roman, Arial…).</li>
<li>Restez sobre, pas de décor (sauf dans certains cas précis : domaines artistiques…), pas de graphique…</li>
<li>Présentez dans l’ordre chronologique décroissant : commencez par les dates les plus récentes pour finir par les dates les plus anciennes. Ceci est valable pour les rubriques « Expérience professionnelle » et « Formation ».</li>
<li>Explicite : faites figurer en caractères gras, soulignés et /ou encadrés les fonctions exercées, les diplômes obtenus…</li>
<li>Ne mettez pas d’abréviation ou d’initiale.</li>
<li>Ne mentionnez pas des éléments tels que CDD, vacataire. En revanche, il est possible de mettre <i>Trainee</i> ou <i>Internship</i> lorsque vous avez fait un stage.</li>
<li>N’utilisez jamais le « Je » ou ne vous référez pas à vous-même. Par exemple, au lieu de dire « J’étais responsable de la communication » dire « Responsable communication ».</li>
<li>Si vous venez de terminer vos études, mettez en valeur vos diplômes, stages, études, rapports et mémoires.</li></ul>
<h4 class="spip">Le haut du CV : l’état civil</h4>
<ul class="spip">
<li><strong>Prénom </strong>(<i>first name</i>)<strong> et Nom </strong>(<i>last name</i>). Pas de surnom. Vous avez la possibilité d’apposer la première lettre de votre deuxième prénom, ex : Sylvia V. Sudrés.</li>
<li><strong>Adresse à Singapour </strong>(<i>address</i>). Utilisez une adresse permanente (celle d’une connaissance ou celle que vous comptez utiliser après l’obtention de votre emploi).</li>
<li><strong>Téléphone personnel</strong> (<i>Home Phone number</i>), professionnel (<i>Office Phone number</i>), mobile (<i>Cell Phone</i>). Utilisez un numéro de téléphone permanent, incluant le code pays. Si vous avez un répondeur, enregistrez une annonce neutre.</li>
<li><strong>Numéro de fax : </strong>(<i>Fax number</i>).</li>
<li><strong>Email</strong> (choisissez une adresse email à but professionnel, évitez les adresses fantaisistes).</li>
<li><strong>L’adresse de votre site web</strong> uniquement si les pages web reflètent vos ambitions professionnelles.</li>
<li><strong>La nationalité</strong>. Si vous détenez déjà un permis de séjour ou un visa, mentionnez-le.</li>
<li><strong>Le nom de votre métier ou de celui que vous souhaitez obtenir </strong>(si vous en avez les compétences) afin que le recruteur comprenne immédiatement votre profil.</li></ul>
<h4 class="spip">Le projet professionnel : <i>Objective</i> </h4>
<p>Accorder cette expression à votre objectif du moment : <i>Internship objective, Professional objective, Career objective</i>. Cette rubrique est destinée à développer ce que vous proposez à l’entreprise en soulignant :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Les compétences que l’on peut apporter à cette société (synthétisées en 3-4 points forts) en soulignant ses qualités et en définissant son profil.</p>
<p><strong>Exemple</strong> <br class="manualbr">Pour les fonctions commerciales : une bonne sociabilité, de la combativité, du dynamisme, le goût de la vente. Pour les fonctions administratives et comptables : une aptitude au calcul, de la rigueur, de la précision et de la méthode.<br class="manualbr">Plus larges seront vos compétences, plus votre valeur sur le marché de l’emploi sera appréciée. Les débutants devront puiser dans leurs expériences scolaires, leurs travaux saisonniers, leurs stages, leurs loisirs.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  L’objectif professionnel fixé : spécifiez le titre du poste ou faites-en une description. Si vous ne savez pas exactement quel type d’emploi vous aimeriez occuper, faites une description de poste plus large. </p>
<p><strong>Exemple</strong> <br class="manualbr">« To obtain an entry-level position within a financial institution requiring strong analytical and organizational skills ». Ajustez votre objectif à chaque employeur, à chaque job que vous recherchez.<br class="manualbr">Préférez des <i>Broad categories</i> à des titres spécifiques (<i>Office management</i> plutôt que <i>Secretary</i>). Soyez clair, précis et direct.</p>
<h4 class="spip">L’expérience professionnelle : <i>Work Experience</i> </h4>
<p>L’objectif est de mettre en valeur vos responsabilités, vos succès et de détailler votre profil clairement. Ne vous inventez pas des jobs et n’amplifiez pas vos expériences :</p>
<p>Il est important de mentionner :</p>
<ul class="spip">
<li>Les dates : mois et années, pas de jour</li>
<li>Mettez votre dernière expérience en premier</li>
<li>Indiquez s’il s’agit d’un stage ou d’un emploi</li>
<li>Le nom de la société et son secteur d’activité</li>
<li>Le lieu : ne pas indiquer l’adresse entière mais seulement la ville et le pays</li>
<li>La fonction : essayez de donner un titre aux tâches que vous avez effectuées, même si elles sont diverses.</li>
<li>Les missions et tâches accomplies : évitez le jargon, les termes techniques, les abréviations hormis si le recruteur est censé les connaître. N’hésitez pas à détailler les tâches accomplies, les missions réalisées, à donner des chiffres et des résultats. Les CV singapouriens sont très étoffés et contiennent souvent plusieurs pages.</li>
<li>Les stages qui sont une part de votre expérience professionnelle.</li>
<li>Des chiffres pour quantifier, des exemples pour être concret et mettez en valeur les bénéfices de vos actions : les réalisations professionnelles quantifiables par exemple.</li>
<li>Employez et diversifiez les verbes d’action pour décrire ce qui a été réalisé, effectué, gagné ou économisé. Ils dynamisent votre présentation :</li>
<li>Pour ceux qui ont une formation technique (commerciale, marketing, ingénierie…) utilisez des « tirets » afin que le lecteur puisse rapidement identifier votre profil, vos expériences.</li>
<li>Pour ceux qui ont une formation plus littéraire (enseignement, LEA, journalisme…) et qui postulent à des postes requérant certaines qualités rédactionnelles, il est possible de présenter ses expériences de façon plus rédigée.</li></ul>
<p><strong>Quelques exemples pour vous aider :</strong></p>
<ul class="spip">
<li>Une amélioration de performance</li>
<li>Une diminution de coûts de …euros</li>
<li>Une augmentation de profits et/ou de ventes de …% Un gain de temps</li>
<li>Un accroissement d’efficacité</li>
<li>Un développement de clientèle</li>
<li>Une certitude quant à un meilleur contrôle</li>
<li>Une aide à l’amélioration de la prise de décision</li>
<li>Une contribution à l’amélioration esthétique et fonctionnelle d’un produit</li>
<li>Une progression dans les relations avec le personnel</li>
<li>Une amélioration dans la fiabilité d’un produit, d’un service ou d’un outil</li>
<li>La mise en place d’une nouvelle technologie, d’un nouveau processus ou d’un nouveau design</li>
<li>Une rénovation des systèmes</li>
<li>Un perfectionnement des stratégies</li>
<li>Une rentabilisation des opérations jusque là déficitaires</li>
<li>Une amélioration des conditions de travail</li>
<li>Une identification et une résolution des problèmes</li>
<li>Un accès à un nouveau service</li>
<li>Une gestion d’un budget à hauteur de….. L’obtention d’un prix</li>
<li>L’atteinte d’objectifs prédéfinis</li></ul>
<h4 class="spip">La formation : <i>Education</i> </h4>
<p>Indiquez dans cette rubrique :</p>
<ul class="spip">
<li>Les diplômes obtenus,</li>
<li>les enseignements suivis du plus récent au plus ancien, en donnant leurs équivalences dans le système singapourien donc anglo-saxon (voir tableau ci-après).(Facultatif : votre niveau d’études, la matière la plus étudiée, votre spécialisation ainsi que les mentions ou récompenses obtenues).</li></ul>
<ul class="spip">
<li>Le nom de l’établissement, école ou université.</li>
<li>Le lieu (ville ou pays).</li></ul>
<p>N’hésitez pas à indiquer vos scores à des examens tels que le TOEFL, TOIEC, GPA…</p>
<p>Les titres des diplômes français ne signifient pas grand chose pour les singapouriens, par conséquent, détaillez chaque étape de votre diplôme et tentez de donner les correspondances ou équivalences singapouriennes.</p>
<p><strong>Attention :</strong> ne citez pas le Bac si vous êtes titulaire d’un diplôme de niveau Bac + 4 et ne citez pas le BEPC si vous êtes titulaire du Baccalauréat. N’utilisez pas de sigle pour nommer votre école qui, aussi renommée soit-elle, n’est pas forcément connue à l’échelon international.</p>
<h4 class="spip">Autres informations : <i>Additional Information/Miscellaneous</i> </h4>
<h5 class="spip">Langues (languages)</h5>
<p>Afin de qualifier votre niveau, vous pouvez utiliser les termes suivants :</p>
<ul class="spip">
<li><i>Mother tongue or native language</i> (langue maternelle)</li>
<li><i>2nd Native tongue</i></li>
<li><i>Bilingual</i> : maîtrise parfaite de la langue même s’il s’agit d’une seconde langue.</li>
<li><i>Fluent (capable of flowing)</i> : vous vous exprimez aisément.</li>
<li><i>Proficient</i> : votre niveau est bon mais vous manquez de pratique et ne pouvez converser à propos de tous les sujets de façon aisée.</li>
<li><i>Conversational/conversant (moyen)</i> : vous comprenez et vous vous faites comprendre mais votre conversation est limitée.</li>
<li><i>Good Knowledge</i> : ce niveau fait référence à un niveau scolaire bon mais où la pratique manque.</li>
<li><i>Working Knowledge</i> : la conversation touchant à un secteur donné, celui du travail exercé, est moyenne à bonne, elle est limitée par ailleurs.</li>
<li><i>Beginner or Notions</i> : niveau débutant ou « faux » débutant dans le cas d’une langue non pratiquée depuis longtemps.</li></ul>
<p>La maîtrise de l’anglais est essentielle mais la pratique d’autres langues est également très utile et valorisante. N’hésitez pas à mettre en valeur vos connaissances. Surtout si vous parlez des langues asiatiques car ces compétences chez les expatriés sont rares et très recherchées.</p>
<h5 class="spip">Informatique : <i>Computer skills</i> </h5>
<p>Citez les logiciels maîtrisés (software) et précisez votre niveau, par exemple :</p>
<ul class="spip">
<li>Proficient with Microsoft Office</li>
<li>Proficient in both P.C.  Mac environments</li>
<li>Computer literate</li>
<li>Internet : proficient in Internet research</li></ul>
<p><strong>Attention :</strong> faites en sorte de ne pas multiplier les termes à haute technicité, inabordables pour la plupart des personnes qui seraient amenées à lire votre <i>resume</i>, excepté si vous postulez dans une <i>IT Company</i> (<i>Information Technology</i> : nouvelles technologies de l’information et de la communication)</p>
<h4 class="spip">Activités extra professionnelles : <i>Extra-professional activities</i> </h4>
<p>Sports, associations, clubs, activités, passions : citez-les si cela vous distingue des autres (expérience en tant que leader/<i>leadership experience</i>), si l’activité traduit des qualités appréciables requises pour le poste à pourvoir.</p>
<p>Soyez originaux mais sincères. Ceux-ci peuvent-être des détails anodins qui feront la différence entre des candidats de même niveau. De plus, ce sont des informations que l’ont retient et qui vous distinguent plus largement.</p>
<h4 class="spip">Références : <i>References</i> </h4>
<p><strong>Important </strong> : il est d’usage de prouver ses qualifications et donc de fournir des photocopies traduites et certifiées conformes des diplômes. De même, il est fréquent d’attester de son passé professionnel par des références et des lettres de recommandations rédigées par vos anciens employeurs, précisant vos qualités et les missions menées.</p>
<p>Souvent, les références ainsi que les lettres de recommandations sont données lors du premier entretien.</p>
<p>Les références consistent à fournir les noms et les coordonnées de vos anciens employeurs et/ou professeurs, ainsi que le lien professionnel qu’ils avaient avec vous, après leur avoir demandé leur autorisation.</p>
<p>Si vos anciens employeurs ne parlent pas anglais, demandez leur plutôt de vous écrire une lettre de recommandation en français que vous ferez traduire ou traduirez en anglais.</p>
<p>La pratique la plus courante est d’indiquer en bas de votre <i>Resume</i> : <i>References available upon request</i>. Vous pouvez aussi mettre vos références directement à la fin de votre <i>Resume</i>.</p>
<h4 class="spip">Resume « Checkup »</h4>
<p>Votre <i>resume</i> est terminé, c’est l’heure des dernières vérifications !</p>
<p>Il ne doit y avoir aucune erreur grammaticale, faute d’orthographe ou incohérence.</p>
<p>Si vous en avez la possibilité, faites-le corriger par un recruteur ou une personne travaillant dans le secteur d’activité que vous souhaitez intégrer. Vous pouvez également suivre les étapes suivantes :</p>
<ul class="spip">
<li>Faites une vérification orthographique et grammaticale par le biais de votre ordinateur avant que quelqu’un ne le relise.</li>
<li>Demandez à une personne parlant couramment l’anglais de vérifier la grammaire.</li>
<li>Faites-le relire et corriger par des professionnels compétents (professeur de langues, personnes s’occupant de l’insertion professionnelle). Chacun d’entre eux vous donnera son point de vue et fera évoluer votre <i>resume</i>.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Diplômes</h3>
<p>Il n’existe pas d’équivalence officielle entre les diplômes français et singapouriens, on parle davantage de correspondances. Ainsi chaque université et école décident de l’équivalence à accorder. Les correspondances proposées ici ne sont qu’indicatives et dépendent aussi de votre propre appréciation.</p>
<table class="spip">
<thead><tr class="row_first"><th id="idfea6_c0">France</th><th id="idfea6_c1">Singapore</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="idfea6_c0">Brevet des collèges</td>
<td headers="idfea6_c1">GCSE</td></tr>
<tr class="row_even even">
<td headers="idfea6_c0">Certificat d’aptitudes professionnelles (CAP)</td>
<td headers="idfea6_c1">CGLI Craft Certificate</td></tr>
<tr class="row_odd odd">
<td headers="idfea6_c0">Brevet d’aptitudes professionnelles (BEP)</td>
<td headers="idfea6_c1">CGLI Craft Certificate</td></tr>
<tr class="row_even even">
<td headers="idfea6_c0">Baccalauréat</td>
<td headers="idfea6_c1">"A" Level / High School Graduation</td></tr>
<tr class="row_odd odd">
<td headers="idfea6_c0">Baccalauréat technologique</td>
<td headers="idfea6_c1">BETEC National Diploma</td></tr>
<tr class="row_even even">
<td headers="idfea6_c0">BTS / DUT</td>
<td headers="idfea6_c1">HND</td></tr>
<tr class="row_odd odd">
<td headers="idfea6_c0">IUT</td>
<td headers="idfea6_c1">Polytechnics</td></tr>
<tr class="row_even even">
<td headers="idfea6_c0"><strong>Université</strong></td>
<td headers="idfea6_c1"><strong>University</strong></td></tr>
<tr class="row_odd odd">
<td headers="idfea6_c0">DEUST</td>
<td headers="idfea6_c1">2 years University Diploma / Associate Degree</td></tr>
<tr class="row_even even">
<td headers="idfea6_c0">Classes Préparatoires</td>
<td headers="idfea6_c1">2 years selective, Preparatory</td></tr>
<tr class="row_odd odd">
<td headers="idfea6_c0">Licence 1 / DEUG 1</td>
<td headers="idfea6_c1">First Year of Bachelor</td></tr>
<tr class="row_even even">
<td headers="idfea6_c0">Licence 2 / DEUG</td>
<td headers="idfea6_c1">Second Year of Bachelor / 2 First year of University</td></tr>
<tr class="row_odd odd">
<td headers="idfea6_c0">Licence 3 / Licence</td>
<td headers="idfea6_c1">Bachelor Degree</td></tr>
<tr class="row_even even">
<td headers="idfea6_c0">Ingénieur Maître des instituts universitaires professionnels (IUP)</td>
<td headers="idfea6_c1">Professional Bachelor Degree</td></tr>
<tr class="row_odd odd">
<td headers="idfea6_c0">Master 1 / Maîtrise</td>
<td headers="idfea6_c1">First Year Of European Master Degree</td></tr>
<tr class="row_even even">
<td headers="idfea6_c0">Master 2 / DEA / DESS</td>
<td headers="idfea6_c1">European Master Degree</td></tr>
<tr class="row_odd odd">
<td headers="idfea6_c0">Institut d’administration des entreprises (IAE)</td>
<td headers="idfea6_c1">School of Business and Administration</td></tr>
<tr class="row_even even">
<td headers="idfea6_c0">Ecole supérieure de commerce (ESC)</td>
<td headers="idfea6_c1">School of Business and Management</td></tr>
<tr class="row_odd odd">
<td headers="idfea6_c0">Ecoles d`ingénieurs</td>
<td headers="idfea6_c1">Engineering School / University of Science  Technology</td></tr>
<tr class="row_even even">
<td headers="idfea6_c0">Ingénieur</td>
<td headers="idfea6_c1">Master of Engineering Degree</td></tr>
<tr class="row_odd odd">
<td headers="idfea6_c0">Doctorat</td>
<td headers="idfea6_c1">PHD</td></tr>
<tr class="row_even even">
<td headers="idfea6_c0">Post Doctorat</td>
<td headers="idfea6_c1">Post Doctorate</td></tr>
<tr class="row_odd odd">
<td headers="idfea6_c0">Stage</td>
<td headers="idfea6_c1">Training course / Internship</td></tr>
</tbody>
</table>
<p>Les titres des diplômes français ne signifient pas grand chose pour les singapouriens, par conséquent, détaillez chaque étape de votre diplôme et tentez de donner les correspondances ou équivalences singapouriennes.</p>
<p><strong>Remarques :</strong></p>
<p>N’utilisez pas de sigle pour nommer votre école qui, aussi renommée soit-elle, n’est pas forcément connue à l’échelon international</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/emploi-stage/article/curriculum-vitae-110492). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
