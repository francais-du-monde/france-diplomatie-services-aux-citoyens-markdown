# Loisirs et culture

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/vie-pratique/article/loisirs-et-culture-114267#sommaire_1">Tourisme</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/vie-pratique/article/loisirs-et-culture-114267#sommaire_2">Activités culturelles</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/vie-pratique/article/loisirs-et-culture-114267#sommaire_3">Sports</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/vie-pratique/article/loisirs-et-culture-114267#sommaire_4">Médias</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/vie-pratique/article/loisirs-et-culture-114267#sommaire_5">Presse française</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Tourisme</h3>
<p>Le tourisme est une activité encore peu développée mais qui fait l’objet d’un investissement important de la part du gouvernement haïtien.</p>
<p>Pour en savoir plus :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.haititourisme.gouv.ht/" class="spip_out" rel="external">Ministère haïtien du Tourisme</a></p>
<h3 class="spip"><a id="sommaire_2"></a>Activités culturelles</h3>
<p>Une programmation riche et variée, y compris pour les enfants et les jeunes, concerts, spectacles de danse et théâtre, films, expositions, conférences et débats, ateliers, etc. est offerte par l’Institut français en Haïti (IFH), à Port-au-Prince.</p>
<p>Six Alliances françaises sont présentes en Haïti : Cap Haïtien, Gonaïves, Jacmel, Jérémie, Les Cayes et Port-de-Paix. Elles proposent de nombreuses activités culturelles.</p>
<p>Il existe enfin plusieurs associations ou café haïtiens qui proposent des événements culturels variés à Port-au-Prince (FOKAL, Centre culturel Anne-Marie Morrisset, Pen club, Café philo etc.) et en province.</p>
<h3 class="spip"><a id="sommaire_3"></a>Sports</h3>
<p>Différents sports, aussi bien collectifs qu’individuels, peuvent être pratiqués dans les clubs existants : football, tennis, natation, sport en salle, danse, badminton etc.</p>
<p>Les côtes offrent des possibilités de sports nautiques. Toutefois, Haïti ne dispose pas de port de plaisance et les structures sont souvent précaires.</p>
<h3 class="spip"><a id="sommaire_4"></a>Médias</h3>
<p><strong>Télévision – Radio</strong></p>
<p>La radio est le principal media d’information en Haïti, il en existe plus de 300 à travers tout le territoire.</p>
<p>RFI est présent en Haïti, sur la grille internationale. La chaîne dispose de 6 émetteurs à travers tout le territoire : Cap-Haïtien (100,5 FM), Gonaïves (90,5 FM), Jacmel (96,9 FM), Jérémie (92,7 FM), Les Cayes (106,9 FM) et Port-au-Prince (89,3 FM).</p>
<p>Il existe également de nombreuses chaînes de télévision, souvent adossées à une radio locale. Canal Sat propose en outre plusieurs bouquets satellites en Haïti.</p>
<p>Les médias diffusent surtout en créole, mais le français est également bien présent.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.ambafrance-ht.org/Medias-haitiens" class="spip_out" rel="external">Ambassade de France en Haïti</a> </p>
<h3 class="spip"><a id="sommaire_5"></a>Presse française</h3>
<p>Certains supermarchés et librairies de la capitale et de Pétion-Ville proposent la vente de journaux et magazines français de façon irrégulière.</p>
<p><i>Mise à jour : août 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/vie-pratique/article/loisirs-et-culture-114267). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
