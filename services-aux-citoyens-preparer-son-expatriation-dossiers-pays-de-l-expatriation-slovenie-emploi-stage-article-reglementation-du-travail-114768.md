# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/emploi-stage/article/reglementation-du-travail-114768#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/emploi-stage/article/reglementation-du-travail-114768#sommaire_2">Contrat de travail – spécificités</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/emploi-stage/article/reglementation-du-travail-114768#sommaire_3">Fêtes légales (non travaillées)</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/emploi-stage/article/reglementation-du-travail-114768#sommaire_4">Emploi du conjoint</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<p><strong>La durée légale du travail est de huit heures par jour (40 heures par semaine).</strong> Une pause de 30 minutes par jour est comprise dans les heures de travail. Le travail des ouvriers et des employés commence généralement très tôt le matin (entre 7h et 8h).</p>
<p><strong>Les salariés ont droit à au moins quatre semaines de congés payés annuels.</strong> Un jour supplémentaire est accordé pour chaque enfant à charge de moins de 15 ans. Au moins trois jours supplémentaires sont accordés aux femmes de plus de 55 ans, aux hommes de plus de 58 ans, aux personnes handicapées et aux personnes ayant un enfant handicapé à charge. L’année compte 12 jours fériés payés.</p>
<p><strong>La durée des congés maladie</strong> est au maximum de 120 jours ouvrables par an. Les 30 premiers sont à la charge de l’employeur. Le congé maternité dure 356 jours (congé de maternité et congé pour soins et garde de l’enfant sont pris ensemble).</p>
<p><strong>Le salaire minimum en Slovénie</strong> est de 783,66 euros brut au 1er janvier 2013. Les heures supplémentaires de travail lors d’une journée normale et lors d’un dimanche ou d’un jour férié sont majorées en fonction de la convention collective de l’entreprise.</p>
<p><strong>La durée de la période d’essai</strong> est de six mois au maximum. En cas de rupture de contrat à l’initiative du salarié, la durée du préavis est de 15 jours minimum, si l’employé a un an d’ancienneté et de 30 jours minimum, si l’employé a plus d’un an d’ancienneté. En cas de rupture de contrat à l’initiative de l’employeur la durée du préavis est de :</p>
<ul class="spip">
<li>15 jours pour une période de travail inférieure à un an ;</li>
<li>30 jours pour une période de travail d’un à deux ans ;</li>
<li>pour une ancienneté de plus de deux ans : 30 jours (pour deux ans) + deux jours pour chaque année travaillée supplémentaire ;</li>
<li>80 jours à partir de 25 ans de services.</li></ul>
<p><strong>Il n’existe pas d’âge légal de départ à la retraite proprement dit</strong>. L’âge auquel les salariés peuvent faire valoir leur droit à la retraite varie en fonction de la durée de cotisation, du nombre d’enfants élevés et, pour les femmes, de l’âge d’entrée sur le marché du travail (avant ou après 18 ans).</p>
<p>Conformément à la législation locale, il existe en 2013 trois cas de figure :</p>
<ul class="spip">
<li>l’assuré acquiert le droit à la retraite à l’âge de 58 ans et 4 mois pour les hommes et 58 ans pour les femmes s’il justifie respectivement de 40 ans de cotisations pour les hommes et 38 ans et 4 mois pour les femmes ;</li>
<li>cet âge est de 63 ans et 6 mois pour les hommes et de 61 ans et 6 mois pour les femmes si l’intéressé a au moins 20 ans d’annuités ;</li>
<li>il est de 65 ans pour les hommes et 63 ans et 6 mois pour les femmes au cas où l’intéressé aurait cotisé au moins pendant 15 ans.</li></ul>
<p>Même au cas où la personne serait largement au-dessus des conditions légales pour le départ à la retraite, l’employeur est obligé de respecter le choix du salarié de poursuivre son activité s’il s’affranchit correctement de ses obligations professionnelles et est considérée apte physiquement par le médecin du travail.</p>
<h3 class="spip"><a id="sommaire_2"></a>Contrat de travail – spécificités</h3>
<p>Le taux des cotisations sociales est de 16,1% pour les employeurs et de 22,1% pour les salariés.</p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Cotisations sociales (en % du salaire)</td>
<td>Part Patronale</td>
<td>Part Salariale</td></tr>
<tr class="row_even even">
<td>Maladie</td>
<td>6,56 %</td>
<td>6,36 %</td></tr>
<tr class="row_odd odd">
<td>Maternité</td>
<td>0,10 %</td>
<td>0,10 %</td></tr>
<tr class="row_even even">
<td>Retraite / Invalidité</td>
<td>8,85 %</td>
<td>15,5 %</td></tr>
<tr class="row_odd odd">
<td>Accidents du travail, maladies professionnelles</td>
<td>0,53 %</td>
<td>-</td></tr>
<tr class="row_even even">
<td>Chômage</td>
<td>0,06 %</td>
<td>0,14 %</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_3"></a>Fêtes légales (non travaillées)</h3>
<ul class="spip">
<li>1er janvier : nouvel An</li>
<li>8 février : fête de la Culture slovène</li>
<li>27 avril : célébration du soulèvement contre l’occupant (lors de la Deuxième Guerre mondiale)</li>
<li>1er - 2 mai : fête du Travail</li>
<li>25 juin : Jour de la Proclamation (fête nationale)</li>
<li>15 août : Assomption</li>
<li>31 octobre : souvenir de la Réforme</li>
<li>1er novembre : Toussaint</li>
<li>25 décembre : Noël</li>
<li>26 décembre : fête de l’Indépendance</li></ul>
<p>Il faut y ajouter les fêtes religieuses à dates variables : Pâques et Pentecôte.</p>
<h3 class="spip"><a id="sommaire_4"></a>Emploi du conjoint</h3>
<p>Pas de restriction pour les ressortissants de l’Espace économique européen (obligation de déclaration de séjour).</p>
<p><i>Mise à jour : septembre 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/emploi-stage/article/reglementation-du-travail-114768). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
