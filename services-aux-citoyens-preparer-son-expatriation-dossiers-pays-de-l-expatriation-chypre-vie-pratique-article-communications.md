# Communications

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/vie-pratique/article/communications#sommaire_1">Téléphone – Internet</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/vie-pratique/article/communications#sommaire_2">Poste</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Téléphone – Internet</h3>
<h4 class="spip">Téléphone</h4>
<p>Indicatifs :</p>
<ul class="spip">
<li>depuis la France vers le Chypre <br class="manualbr">00 357 + numéro à 8 chiffres</li>
<li>depuis le Chypre vers la France <br class="manualbr">00 33 + le numéro d’abonné, en ôtant le "0" de l’indicatif régional</li></ul>
<p>Des instructions pour la composition des numéros ainsi que les tarifs en vigueur sont affichés dans toutes les cabines publiques. Pour un appel à l’intérieur de Chypre, vous n’aurez qu’à composer un numéro de téléphone à 8 chiffres.</p>
<p><strong>Utilisation de votre portable français</strong></p>
<p>Les téléphones portables français fonctionnent sans difficulté sur toute l’île. Il convient de vérifier auprès de son fournisseur les conditions tarifaires appliquées.</p>
<p>Les tarifs des appels lors des déplacements à l’intérieur de l’Union européenne sont plafonnés (les réseaux téléphoniques au Nord sont de facto reliés au réseau turc et appliquent des tarifs en conséquence). Pour en savoir plus, vous pouvez consulter le site Internet suivant : <a href="http://ec.europa.eu/information_society/activities/roaming/index_en.htm" class="spip_out" rel="external">http://ec.europa.eu/information_society/activities/roaming/index_en.htm</a> (site uniquement en anglais). La rubrique « tariffs » vous informe sur les prix pratiqués pour les services en itinérance (appels téléphoniques, SMS, services de données) à Chypre par les opérateurs locaux. Vous trouverez à la rubrique « find your operator », des liens vers les sites Internet des opérateurs de téléphonie à Chypre.</p>
<h4 class="spip">Internet</h4>
<p>De nombreux établissements dont les centres d’appel téléphonique, les cafés Internet et les hôtels disposent de services de connexion à Internet variables en fonction de la rapidité de la connexion et du type de réseau installé. En fonction de l’établissement, la connexion sera avec ou sans fil, et généralement facturée à l’heure.</p>
<p>La qualité des fournisseurs locaux est bonne, mais les tarifs assez élevés.</p>
<p>Téléphoner gratuitement par Internet Il est généralement très avantageux d’utiliser <strong>des logiciels de téléphonie sur IP</strong> (Skype, Google talk, Live messenger, Yahoo ! messenger, etc.) car les tarifs proposés à l’international sont souvent moindre que ceux des opérateurs locaux exploitant des lignes classiques, voire gratuits dans certaines conditions.</p>
<h3 class="spip"><a id="sommaire_2"></a>Poste</h3>
<p>Les liaisons postales entre la France et Chypre sont bonnes. Il faut compter un délai d’acheminement moyen de trois ou quatre jours pour une lettre postée à Nicosie.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.mcw.gov.cy/" class="spip_out" rel="external">Ministère des Communications et du Travail</a>.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/vie-pratique/article/communications). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
