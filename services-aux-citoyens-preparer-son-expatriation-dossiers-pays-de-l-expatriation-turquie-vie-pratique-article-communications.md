# Communications

<h4 class="spip">Téléphone – Internet</h4>
<h4 class="spip">Téléphone</h4>
<p>Les liaisons téléphoniques et télégraphiques sont bonnes.</p>
<p>Pour appeler la Turquie depuis la France :</p>
<ul class="spip">
<li>indicatif du pays : 90</li>
<li>indicatif Ankara : 312</li>
<li>indicatif Istanbul : 212 partie Europe et 216 partie Asie</li></ul>
<p>Voir aussi : <a href="http://www.tv5.org/TV5Site/voyageurs/accueil.php" class="spip_out" rel="external">Tv5.org/</a> et le site de l’<a href="https://goturkey.com/fr" class="spip_out" rel="external">office de tourisme de Turquie</a></p>
<p>Importer son propre cellulaire en Turquie vous oblige soit à le déclarer à l’entrée dans le pays (à l’aéroport) sur présentation de la facture d’achat ou à le faire enregistrer auprès de l’Agence des télécommunications dans un délai d’un mois, faute de quoi votre ligne sera bloquée.</p>
<p>Vous trouverez toutefois localement un grand choix de magasins de téléphones cellulaires.</p>
<p>Par ailleurs, l’obtention d’une ligne téléphonique ou d’une ligne Internet à domicile est conditionnée à la présentation de la carte de résident auprès des services de Turktélécom.</p>
<p>Des fournisseurs de réseaux téléphoniques se sont installés récemment en Turquie et offrent des prix concurrentiels, notamment sur l’étranger. Le marché est toutefois en développement et de nouvelles offres apparaissent régulièrement.</p>
<h4 class="spip">Internet</h4>
<p>Aucun problème pour se connecter à Internet. Les centres Internet abondent dans toutes les villes, dans les centres commerciaux et dans les hôtels.</p>
<h5 class="spip">Téléphoner gratuitement par Internet </h5>
<p>Il est généralement très avantageux d’utiliser <strong>des logiciels de téléphonie sur IP</strong> (Skype, Google talk, Live messenger, Yahoo ! messenger, etc.) car les tarifs proposés à l’international sont souvent moindre que ceux des opérateurs locaux exploitant des lignes classiques, voire gratuits dans certaines conditions.</p>
<h4 class="spip">Poste</h4>
<p>Le délai de réception du courrier est de 5 à 10 jours, avec une assez bonne garantie de réception. Ne pas omettre d’indiquer le numéro de la boîte postale lorsqu’elle existe. La qualité du service postal à l’intérieur de la Turquie est cependant incertaine.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/vie-pratique/article/communications). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
