# Entrée et séjour

<h2 class="rub22968">Passeport, visa, permis de travail</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/entree-et-sejour-22968/#sommaire_1">Séjour de moins de trois mois</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/entree-et-sejour-22968/#sommaire_2">Séjour de plus de trois mois </a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/entree-et-sejour-22968/#sommaire_3">Permis de travail</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/entree-et-sejour-22968/#sommaire_4">Les différents types de visa</a></li></ul>
<p>Quel que soit le motif de votre séjour, renseignez-vous auprès de la section consulaire de l’ambassade d’Afrique du Sud à Paris qui vous informera sur la règlementation en matière d’entrée et de séjour en Afrique du Sud, règlementation que vous devrez impérativement respecter. N’hésitez pas à consulter le site du <a href="http://www.dha.gov.za/" class="spip_out" rel="external">ministère de l’Intérieur sud-africain</a> afin de vous familiariser avec les modalités d’entrée et de séjour dans le pays.</p>
<p><strong>En aucun cas, vous n’êtes autorisé à travailler en Afrique du Sud sans permis adéquat. </strong></p>
<p>Les consulats de France en Afrique du Sud ne sont pas compétents pour répondre à vos demandes d’information concernant votre séjour en Afrique du Sud.</p>
<h3 class="spip"><a id="sommaire_1"></a>Séjour de moins de trois mois</h3>
<p>Il faut être muni d’un passeport en cours de validité et d’un billet de retour. Le visa est délivré gratuitement à l’arrivée aux touristes et aux personnes en voyage d’affaires. Tout voyageur quittant le pays après la date d’expiration de son visa encourt l’arrestation, la détention et la comparution devant un juge, ainsi que le paiement d’une forte amende. Si vous souhaitez prolonger votre séjour, il convient de s’adresser, avant échéance du visa, au bureau des <i>Home Affairs</i>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Séjour de plus de trois mois </h3>
<p>Pour effectuer un séjour de plus de trois mois ou si le motif du séjour est un stage, des études, un événement sportif…, il faut déposer une demande de visa au moins deux mois avant le départ, auprès de l’<a href="http://www.afriquesud.net/index.php?lang=fr" class="spip_out" rel="external">ambassade d’Afrique du Sud à Paris</a>.</p>
<p>Pour l’obtention d’un permis de résidence ou de travail, la demande est à formuler à l’ambassade d’Afrique du Sud à Paris avant le départ, à l’aide du formulaire BI 159 E. Certaines conditions sont requises et le délai peut varier de plusieurs semaines à trois mois. Le dossier de demande de permis de travail doit être très bien argumenté en faisant apparaître qu’aucun Sud-africain (ou étranger titulaire d’un permis de séjour permanent en Afrique du Sud) ne peut occuper l’emploi trouvé par le demandeur français.</p>
<p>Les formalités d’entrée pour le conjoint ou le concubin sont identiques à celles du chef de famille.</p>
<p>La durée des permis est d’un an maximum (renouvelable), même pour les expatriés travaillant dans le cadre d’un projet bilatéral à long terme. Le renouvellement est soumis à conditions.</p>
<p>Pour de plus amples informations, prendre l’attache de l’<a href="http://www.afriquesud.net/index.php?lang=fr" class="spip_out" rel="external">ambassade d’Afrique du Sud à Paris</a>.</p>
<h3 class="spip"><a id="sommaire_3"></a>Permis de travail</h3>
<p>Il est impératif d’obtenir un permis de travail (<i>work permit</i>) avant le départ pour l’Afrique du Sud. Hormis pour les conjoints de Sud-Africains, on ne peut obtenir sur place, à partir de son visa de tourisme, une autorisation de travail. Un retour dans le pays de résidence habituelle sera obligatoire.</p>
<p>La demande du <i>work permit</i> doit être déposée à l’ambassade (ou au consulat) d’Afrique du Sud du pays où le requérant réside. Le dépôt du dossier complet s’accompagne d’un versement d’environ 185 euros ; cette somme est conservée par les autorités sud-africaines, même en cas de refus de <i>work permit</i>. Le dossier est ensuite envoyé en Afrique du Sud, à Pretoria, au ministère de l’Intérieur pour décision et renvoyé ensuite à la représentation sud-africaine, qui informe le requérant du résultat. La procédure, dès lors que le dossier a été accepté par l’ambassade (ou le consulat) d’Afrique du Sud, prend un minimum de deux mois (mais les délais oscillent généralement entre trois et cinq mois, voire plus). Un <i>work permit</i> est délivré pour trois ans maximum et peut être renouvelé en Afrique du Sud (une seule taxe payée pour les trois ans). Le conjoint pourra bénéficier d’un visa d’accompagnement qui ne donne pas automatiquement droit au travail.</p>
<p>Coordonnées des bureaux de <i>Home Affairs</i> disponibles sur le site internet du <a href="http://www.dha.gov.za/index.php/contact-us" class="spip_out" rel="external">ministère de l’Intérieur</a>.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p>[Vous pouvez consulter l’article <a href="services-aux-citoyens-preparer-son-expatriation-documents-de-voyage.md" class="spip_in">Documents de voyage</a>.</p>
<h3 class="spip"><a id="sommaire_4"></a>Les différents types de visa</h3>
<p>Le ministère de l’Intérieur sud-africain (<i>Departement of Home Affairs - DHA</i>) est habilité pour la délivrance de :</p>
<p><strong>Permis de séjour temporaire :</strong></p>
<ul class="spip">
<li><i>Visitors permit</i></li>
<li><i>Study permit</i></li>
<li><i>Treaty permit</i></li>
<li><i>Business permit</i></li>
<li><i>Medical treatment permit</i></li>
<li><i>Relative permit</i></li>
<li><i>Retired person permit</i></li>
<li><i>Corporate permit</i></li>
<li><i>Exchange permit</i></li></ul>
<p><strong>Permis de travail :</strong></p>
<ul class="spip">
<li><i>Quota work permit</i></li>
<li><i>General work permit</i></li>
<li><i>Exceptional Skills work permit</i></li>
<li><i>Intra-company transfer work permit</i></li></ul>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.afriquesud.net/index.php?lang=fr" class="spip_out" rel="external">Section consulaire de l’ambassade d’Afrique du Sud à Paris</a></li>
<li><a href="http://www.dha.gov.za/" class="spip_out" rel="external">Site du ministère de l’Intérieur sud-africain (Home Affairs)</a></li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-entree-et-sejour-22968-article-animaux-domestiques-111005.md" title="Animaux domestiques">Animaux domestiques</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-entree-et-sejour-22968-article-vaccination-111004.md" title="Vaccination">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-entree-et-sejour-22968-article-demenagement-111003.md" title="Déménagement">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-entree-et-sejour-22968-article-passeport-visa-permis-de-travail-111002.md" title="Passeport, visa, permis de travail">Passeport, visa, permis de travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/entree-et-sejour-22968/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
