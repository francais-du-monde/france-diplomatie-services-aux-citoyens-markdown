# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/vie-pratique/article/transports#sommaire_1">Transport routier</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/vie-pratique/article/transports#sommaire_2">Transports en commun</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/vie-pratique/article/transports#sommaire_3">Transports urbains</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/vie-pratique/article/transports#sommaire_4">Annexe</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Transport routier</h3>
<h4 class="spip">Infrastructures</h4>
<p>Le réseau routier est vaste et de bonne qualité. Les diverses voies carrossables maillent convenablement le territoire portugais, ce qui rend le déplacement par véhicule routier très commode.</p>
<p>Des autoroutes (autopistas) permettent, comme en France, une circulation rapide entre les principales villes du pays. Le Portugal a mis en place un nouveau système de péage exclusivement électronique, qui facture les passages sur ce type de voies en photographiant les plaques minéralogiques. Le paiement se fait par prélèvement automatique ou dans les bureaux de poste portugais (« Correios »).</p>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li><a href="http://www.portugaltolls.pt/" class="spip_out" rel="external">Portugaltolls.pt</a></li>
<li><a href="http://www.estradasdeportugal.pt/" class="spip_out" rel="external">Estradasdeportugal.pt</a></li></ul>
<h4 class="spip">Immatriculation du véhicule</h4>
<p>En tant que résident local au Portugal, l’immatriculation d’un véhicule doit se faire au plus tard <strong>dans les six mois</strong>. Ce délai est de rigueur pour tout type d’acquisition de véhicule, comme pour le changement d’enregistrement de votre véhicule français.</p>
<p><strong>Pour faire enregistrer votre véhicule immatriculé dans un Etat membre de l’Union européenne au Portugal :</strong></p>
<p>La procédure d’enregistrement de votre véhicule comporte six étapes qu’il vous appartiendra de suivre pour que l’opération soit validée par l’administration portugaise :</p>
<ul class="spip">
<li>réunir les documents prouvant votre acquisition légale du véhicule (facture, déclaration de vente, certificat de conformité communautaire, carte grise…) ;</li>
<li>prendre contact avec l’Institut de la mobilité et des transports terrestres pour obtenir de leur part le formulaire Modelo 9 IMTT (également disponible sur le site internet de l’IMTT, <i>cf. liens utiles ci-dessous</i>) ;</li>
<li>prendre contact avec le centre d’inspection (CITV) pour obtenir le formulaire Modelo 112 et faire procéder à l’inspection du véhicule. Demander au CITV la mesure du taux d’émission du dioxyde de carbone ;</li>
<li>dans le délai de 20 jours après l’entrée sur le territoire, se présenter à la douane (Alfândega), pour y procéder à la déclaration du véhicule (Declaração Aduaneira do Veiculo – DAV) muni des documents mentionnés au cours des étapes précédentes. Le paiement de l’impôt sur les véhicules (ISV) est à effectuer auprès de cette administration. Une exemption de paiement de cet impôt peut être octroyée. En faire la demande sur place ;</li>
<li>solliciter de l’IMTT l’émission d’un certificat d’immatriculation. Pour cela, présenter à l’IMTT un exemplaire de la DAV obtenue et une copie du livret d’origine du véhicule, authentifié par les douanes ;</li>
<li>se présenter à une Conservatoria do Registo Automovel pour y faire inscrire votre véhicule.</li></ul>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li>Site de l’<a href="http://www.imtt.pt/sites/IMTT/Portugues/Veiculos/Matriculas/Paginas/Matriculas.aspx" class="spip_out" rel="external">IMTT</a> ;</li>
<li><a href="http://www.imtt.pt/sites/imtt/portugues/veiculos/inspecao/AprovacaoCITVs/Paginas/CITV.aspx" class="spip_out" rel="external">CITV</a>.</li></ul>
<p><strong>NB</strong> : vous trouverez une liste des points d’accueil de l’IMTT en fin de ce document.</p>
<h4 class="spip">Permis de conduire</h4>
<p>Le permis de conduire français "modèle européen" étant reconnu par les autres Etats membres de l’Union européenne, il n’est plus nécessaire de l’échanger contre un permis portugais, mais il reste possible d’en faire la demande.</p>
<p>Toutefois, les conducteurs qui s’établissent au Portugal doivent en informer l’IMTT dans un délai de 60 jours, sous peine de sanctions.</p>
<p>Pour tout renseignement sur le permis de conduire ou l’immatriculation des véhicules, l’autorité compétente est l’IMTT, qui dispose d’un certain nombre de bureaux locaux dans chaque district du territoire continental.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.ambafrance-pt.org/" class="spip_out" rel="external">Ambassade de France au Portugal</a></p>
<p><strong>NB</strong> : vous trouverez une liste des points d’accueil de l’IMTT en fin de ce document.</p>
<h4 class="spip">Code de la route</h4>
<p>Le code de la route et la signalisation sont les mêmes qu’en France, mais les styles de conduite diffèrent largement. Les conducteurs portugais sont souvent moins disciplinés que leurs pendants français et les règles de la circulation sans danger sont souvent mises à mal par des comportements périlleux. Une vigilance redoublée s’impose en conséquence en voiture, puisque le nombre d’accidents de la route reste en proportion bien plus élevé au Portugal qu’en France.</p>
<p>La priorité est à droite, hormis aux ronds-points. Les limitations de vitesse sont de 120 km/h sur autoroute, 100km/h sur les voies rapides, 90km/h sur route et 50 km/h en agglomération. En outre, sur certaines routes à plusieurs voies peuvent exister des vitesses minimales en dessous desquelles il est interdit de rouler.</p>
<p>Comme en France, sont obligatoires à bord de tous les véhicules (y compris étrangers) circulant au Portugal un gilet fluorescent (à conserver dans l’habitacle de la voiture et non dans le coffre) et un triangle de signalisation.</p>
<p>La conduite en état d’ivresse (le taux d’alcoolémie maximum toléré au volant est de 0,5g/l) et l’usage du téléphone portable au volant sont réprimés par la police.</p>
<p>Sur certaines portions de route, notamment l’A25 entre Aveiro et Vilar Formoso, il est obligatoire de rouler avec les feux de croisement allumés, même de jour.</p>
<p>En cas d’infraction au code de la route, le contrevenant étranger doit immédiatement payer l’amende correspondante, voire, en cas de faute grave justifiant la comparution ultérieure devant un tribunal, une caution équivalant au montant maximal de l’amende. L’impossibilité de payer l’amende peut entraîner la confiscation immédiate du véhicule ou des documents afférents à celui-ci. Dans ce dernier cas, une attestation provisoire de circulation est délivrée à l’auteur de l’infraction.</p>
<h4 class="spip">Assurance responsabilité civile automobile</h4>
<p>Elle concerne la branche automobile, ainsi que d’autres types de véhicules tels que motos et cyclomoteurs. On peut y ajouter des couvertures facultatives afin d’améliorer les performances des contrats souscrits. On trouve au Portugal le système de "packs" de couverture, proposé par les sociétés d’assurances afin de satisfaire les nécessités spécifiques de chaque client, proposant en contrepartie des prix intéressants pour les adhésions automatiques.</p>
<p>Les assurances obligatoires sont les mêmes qu’en France. Certaines assurances françaises ont une faible couverture en dehors du territoire et il est préférable de les coupler avec une assurance étrangère.</p>
<h3 class="spip"><a id="sommaire_2"></a>Transports en commun</h3>
<h4 class="spip">Transport aérien</h4>
<p>Le territoire portugais a une superficie de 92000 km², pour une distance nord-sud équivalant à 550 kilomètres. La majorité des déplacements entre les grandes villes se fait longitudinalement, et l’avion ne génère que peu souvent un gain de temps par rapport aux autres moyens de locomotion.</p>
<p>Il constitue toutefois un mode de transport essentiel pour se rendre à l’étranger. Du fait de l’isolement géographique du pays et du manque d’infrastructures ferroviaires de grande vitesse, le Portugal reste en effet difficilement accessible depuis l’étranger autrement qu’en avion.</p>
<p>Le Portugal dispose d’aéroports dans ses villes principales, ainsi que sur ses îles. L’<a href="http://www.ana.pt/" class="spip_out" rel="external">ANA Aeroportos de Portugal</a> centralise leur gestion. Vous pourrez, par l’intermédiaire de son site Internet, accéder aux différentes informations concernant les vols au Portugal et l’accès aux aéroports.</p>
<p>Plusieurs compagnies aériennes portugaises effectuent des liaisons régulières nationales et internationales :</p>
<ul class="spip">
<li><a href="http://www.flytap.com/" class="spip_out" rel="external">TAP Portugal (<i>Transportes Aéreos Portugueses</i>)</a> assure des vols réguliers vers plus de 50 destinations internationales et des vols intérieurs entre Lisbonne, Porto, Faro et les régions autonomes de Madère et des Açores, tout comme entre les Îles de Madère et de Porto Santo.</li>
<li><a href="http://www.sata.pt/" class="spip_out" rel="external">SATA</a> assure des vols réguliers entre toutes les îles des Açores et des Açores vers Madère et le Portugal continental, ainsi que quelques vols réguliers vers certains aéroports internationaux.</li>
<li>Aerocondor Transports Aériens (ATA) effectue des vols internes réguliers et des vols charter internationaux.</li></ul>
<h4 class="spip">Transport ferroviaire</h4>
<p>Le réseau ferroviaire, peu développé, est en bon état. Il permet des liaisons rapides et fréquentes, à prix modéré, entre Lisbonne, Coimbra, Porto et les autres villes du Portugal. Des liaisons internationales sont aussi possibles (notamment avec l’Espagne).</p>
<p>Il existe différents types de trains :</p>
<ul class="spip">
<li>les trains <i>Alfa Pendular</i> assurent les liaisons les plus rapides et les plus confortables entre Lisbonne et l’Algarve de même que, pour le nord du pays avec Porto, Braga ou Guimarães ;</li>
<li>le service <i>Intercidades</i> (inter-villes) offre des liaisons sur les axes Lisbonne-Porto-Guimarães, Lisbonne-Guarda, Lisbonne-Covilhã, Lisbonne-Alentejo et Lisbonne-Algarve. Le Sud-Express et le train-hôtel Lusitânia assurent la liaison internationale au départ de Lisbonne respectivement vers Paris et Madrid ;</li>
<li>un vaste réseau de trains régionaux, interrégionaux et suburbains couvre tout le territoire national.</li></ul>
<p>Les titres de transport sont à acheter dans les gares. Des bornes automatiques et des guichets sont à disposition. Ces derniers offrent un accueil par un agent spécialisé qui sera capable de vous informer et de vous orienter.</p>
<h4 class="spip">Autocars</h4>
<p>Le Portugal possède un bon réseau d’autocars desservi par plusieurs compagnies, parmi lesquelles Eva transportes et Rede expressos.</p>
<p>Les destinations proposées sont bien plus nombreuses que celles desservies par les compagnies de transport ferroviaire. Les sites Internet des différents opérateurs proposant des transports en bus permettent d’effectuer des réservations en ligne.</p>
<p>Des terminaux routiers existent dans la majorité des villes. Ils rassemblent la majorité des lignes de cars en un seul endroit.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.cp.pt/" class="spip_out" rel="external">Compagnie nationale ferroviaire</a> ;</li>
<li><a href="http://www.carris.pt/" class="spip_out" rel="external">Compagnie de transport de la métropole lisboète</a>.</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Transports urbains</h3>
<p>Les prix des transports urbains avoisinent ceux de nos réseaux français. Il vous appartient de payer un supplément pour le premier achat de billet rechargeable.</p>
<h4 class="spip">Métro</h4>
<p>Lisbonne et Porto disposent d’un métro. Le métro de Lisbonne est pourvu de quatre lignes, et fonctionne entre 6 heures 30 et 1 heure du matin. Le métro de Porto dispose quant à lui de cinq lignes et fonctionne entre 6 heures et 1 heure du matin. Les deux réseaux sont découpés en plusieurs zones tarifaires, fonctions de l’éloignement du centre-ville.</p>
<p><strong>A savoir</strong> : conservez bien votre titre de transport lors de vos différents voyages en métro ou en train. Durant le trajet, il est probable que vous soyez contrôlé par un agent de la compagnie empruntée. Arrivé à votre destination, vous aurez besoin de votre titre de transport pour sortir de la gare.</p>
<h4 class="spip">Tramways et autobus</h4>
<p>Lisbonne et Porto disposent d’un important réseau de tramways et d’autobus. Les arrêts de bus sont nombreux et irriguent bien les diverses zones de ces conurbations.</p>
<p>La ville de Lisbonne propose aussi des funiculaires qui aident à accéder à certains points escarpés de la ville.</p>
<p>Les autres villes portugaises, de taille moindre, sont aussi bien desservies par les services de transport en autobus. Les tickets peuvent la plupart du temps être achetés lors de la montée dans le véhicule. Il suffit pour cela d’indiquer la destination au chauffeur. Les lignes d’autocar relient souvent plusieurs municipalités pouvant être distantes d’une vingtaine de kilomètres, et les noms des arrêts n’apparaissent pas à l’intérieur des bus. N’hésitez donc pas à demander au chauffeur de vous indiquer que vous avez atteint votre destination.</p>
<h4 class="spip">Taxis</h4>
<p>Le prix est indiqué dans le taximètre et les tarifs sont affichés à l’intérieur du véhicule ou peuvent être demandés au chauffeur.</p>
<p>En dehors des agglomérations, le transport en taxi est payé au kilomètre, le prix étant préalablement convenu entre le chauffeur et le passager. Lorsqu’il y en a, les péages sont à régler par le client.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.stcp.pt/" class="spip_out" rel="external">Compagnie de transport de Porto</a></p>
<h3 class="spip"><a id="sommaire_4"></a>Annexe</h3>
<h4 class="spip">Liste des points d’accueil de l’IMTT</h4>
<p><strong>Direcção Regional de Mobilidade e Transportes do Norte</strong><br class="manualbr">Av. Fontes Pereira de Melo, 485/527<br class="manualbr">4149-015 PORTO<br class="manualbr">22 619 64 00</p>
<p><strong>DRMT NORTE - Delegação Distrital de Viação de Braga</strong><br class="manualbr">R. do Poente, 66/70 Stª Tecla<br class="manualbr">4715-043 BRAGA<br class="manualbr">253 612 441</p>
<p><strong>DRMT NORTE - Delegação Distrital de Viação de Bragança</strong><br class="manualbr">Av. Sá Carneiro, 175<br class="manualbr">5301-853 BRAGANÇA<br class="manualbr">273 310 100</p>
<p><strong>DRMT NORTE - Delegação Distrital de Viação de Viana do Castelo</strong><br class="manualbr">R. Carolino Ramos, 2 – R/C<br class="manualbr">4904-852 VIANA DO CASTELO<br class="manualbr">258 806 720</p>
<p><strong>DRMT NORTE - Delegação Distrital de Viação de Vila Real</strong><br class="manualbr">Av. Aureliano Barrigas – Bloco C – R/C Esq.<br class="manualbr">5000-413 VILA REAL<br class="manualbr">259 326 131</p>
<p><strong>Direcção Regional de Mobilidade e Transportes do Centro</strong><br class="manualbr">Av. Fernão de Magalhães, 511-513<br class="manualbr">3000-177 COIMBRA<br class="manualbr">239 856 900</p>
<p><strong>DRMT Centro - Delegação Distrital de Viação da Guarda</strong><br class="manualbr">Rua 4 de Outubro, Guarda Gare<br class="manualbr">6300-878 GUARDA<br class="manualbr">271 205 130</p>
<p><strong>DRMT Centro - Delegação Distrital de Viação de Aveiro</strong><br class="manualbr">Estrada da Cidadela de Aveiro, 33<br class="manualbr">3800-371 AVEIRO<br class="manualbr">234 302190</p>
<p><a href="http://www.imtt.pt/sites/IMTT/Portugues/Contactos/Paginas/DelegacaoDistritaldeViacaodeCasteloBranco043.aspx?servico=0" class="spip_out" rel="external">DRMT Centro - Delegação Distrital de Viação de Castelo Branco</a><br class="manualbr">Av. da Carapalha, 45 <br class="manualbr">6000-320 CASTELO BRANCO <br class="manualbr">272 348 100</p>
<p><a href="http://www.imtt.pt/sites/IMTT/Portugues/Contactos/Paginas/DelegacaoDistritaldeViacaodeLeiria044.aspx?servico=0" class="spip_out" rel="external">DRMT Centro - Delegação Distrital de Viação de Leiria</a><br class="manualbr">R. da Assunção, 25 – Guimarota <br class="manualbr">2410-068 LEIRIA <br class="manualbr">244 800 460</p>
<p><strong>DRMT Centro - Delegação Distrital de Viação de Viseu</strong><br class="manualbr">R. A. Herculano, Edif. Paulo VI – Lote 201 CV<br class="manualbr">3510-035 VISEU<br class="manualbr">232 419360</p>
<p><a href="http://www.imtt.pt/sites/IMTT/Portugues/Contactos/Paginas/DireccaoRegionaldeMobilidadeeTransportesdeLisboaeValedoTejo009.aspx?servico=0" class="spip_out" rel="external">Direção Regional de Mobilidade e Transportes de Lisboa e Vale do Tejo</a><br class="manualbr">Av. Elias Garcia, n.º 103 <br class="manualbr">1050-098 LISBOA <br class="manualbr">21 791 30 00</p>
<p><a href="http://www.imtt.pt/sites/IMTT/Portugues/Contactos/Paginas/DelegacaoDistritaldeViacaodeSantarem007.aspx?servico=0" class="spip_out" rel="external">DRMT Lisboa e V.T. - Delegação Distrital de Viação de Santarém</a><br class="manualbr">Praceta Alves Redol, 7/9 <br class="manualbr">2000-182 SANTARÉM <br class="manualbr">243 305 030</p>
<p><a href="http://www.imtt.pt/sites/IMTT/Portugues/Contactos/Paginas/DelegacaoDistritaldeViacaodeSetubal008.aspx?servico=0" class="spip_out" rel="external">DRMT Lisboa e V.T. - Delegação Distrital de Viação de Setúbal</a><br class="manualbr">Av. Dr. António Rodrigues Manito, 92-R/C <br class="manualbr">2900-062 SETÚBAL <br class="manualbr">265 548800</p>
<p><a href="http://www.imtt.pt/sites/IMTT/Portugues/Contactos/Paginas/DireccaoRegionaldeMobilidadeeTransportesdoAlentejo003.aspx?servico=0" class="spip_out" rel="external">Direção Regional de Mobilidade e Transportes do Alentejo</a><br class="manualbr">Parque Industrial e Tecnológico Rua Arquiminio Caeiro <br class="manualbr">7000-171 ÉVORA <br class="manualbr">266 745 650</p>
<p><a href="http://www.imtt.pt/sites/IMTT/Portugues/Contactos/Paginas/DireccaoRegionaldeMobilidadeeTransportesdoAlentejoNucleodeActividadesdeTransporte%28Evora%29004.aspx?servico=0" class="spip_out" rel="external">Direção Regional de Mobilidade e Transportes do Alentejo - Núcleo de Actividades de Transporte (Évora)</a><br class="manualbr">Av. Túlio Espanca<br class="manualbr">7005-840 ÉVORA <br class="manualbr">266 750 480</p>
<p><a href="http://www.imtt.pt/sites/IMTT/Portugues/Contactos/Paginas/DelegacaoDistritaldeViacaodeBeja001.aspx?servico=0" class="spip_out" rel="external">DRMT Alentejo - Delegação Distrital de Viação de Beja</a><br class="manualbr">R. D. Nuno Álvares Pereira – Edif. Governo Civil<br class="manualbr">7800-054 BEJA <br class="manualbr">284 313 690/1</p>
<p><a href="http://www.imtt.pt/sites/IMTT/Portugues/Contactos/Paginas/DelegacaoDistritaldeViacaodePortalegre002.aspx?servico=0" class="spip_out" rel="external">DRMT Alentejo - Delegação Distrital de Viação de Portalegre</a><br class="manualbr">Travessa da R. do Comércio, 4<br class="manualbr">7300-857 PORTALEGRE <br class="manualbr">245 331 574</p>
<p><a href="http://www.imtt.pt/sites/IMTT/Portugues/Contactos/Paginas/DireccaoRegionaldeMobilidadeeTransportesdoAlgarve006.aspx?servico=0" class="spip_out" rel="external">Direção Regional de Mobilidade e Transportes do Algarve</a><br class="manualbr">R. Aboim Ascensão, 10/14<br class="manualbr">8004-025 FARO <br class="manualbr">289 870 130 / 28 989 51 80</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/vie-pratique/article/transports). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
