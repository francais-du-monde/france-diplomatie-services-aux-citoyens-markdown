# Convention de sécurité sociale

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/protection-sociale/article/convention-de-securite-sociale#sommaire_1">Convention de sécurité sociale</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/protection-sociale/article/convention-de-securite-sociale#sommaire_2">Travailleurs exerçant une activité salariée en Roumanie et relevant à ce titre de la convention</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Convention de sécurité sociale</h3>
<p>La France et la Roumanie sont liées par la Convention du 16 décembre 1976 entrée en vigueur le 1er février 1978.</p>
<p>En application conjointe de la législation interne et de la convention franco-roumaine, les Français occupés en Roumanie se trouvent généralement dans l’une des trois situations suivantes :</p>
<ul class="spip">
<li>travailleurs détachés dans le cadre conventionnel ou dans le cadre de la législation française ;</li>
<li>travailleurs qui ne sont plus soumis à la législation française parce qu’ils ne sont pas détachés et qui, exerçant une activité salariée en Roumanie, entrent dans le champ d’application de la Convention ;</li>
<li>travailleurs soumis à la législation roumaine et qui complètent leur protection par une assurance volontaire en France.</li></ul>
<p>Bien entendu, il ne s’agit là que d’exemples puisque les Français se trouvant en Roumanie peuvent être des non-salariés, des étudiants, des retraités en vacances.</p>
<p>Tout renseignement complémentaire en matière de sécurité sociale peut être obtenu auprès du :</p>
<p><a href="http://www.cleiss.fr/docs/textes/index.html" class="spip_out" rel="external">Centre des liaisons européennes et internationales de sécurité sociale</a><br class="manualbr">11, rue de la tour des Dames<br class="manualbr">75436 PARIS Cedex 09<br class="manualbr">Tél : 01 45 26 33 41<br class="manualbr">Télécopie : 01 49 95 06 50</p>
<h3 class="spip"><a id="sommaire_2"></a>Travailleurs exerçant une activité salariée en Roumanie et relevant à ce titre de la convention</h3>
<p>Les ressortissants français exerçant en Roumanie une activité salariée ou assimilée sont, ainsi que leurs ayants droit résidant avec eux, soumis aux législations de sécurité sociale de l’Etat sur le territoire duquel ils exercent leur activité dans les mêmes conditions que les nationaux dudit Etat, sous les réserves et modalités particulières convenues d’un commun accord.</p>
<h4 class="spip">Droits du travailleur pour lui-même et pour les membres de sa famille qui l’accompagnent</h4>
<h5 class="spip">Au cours de la période de travail en Roumanie</h5>
<p><strong>Maladie-maternité</strong></p>
<p>Les personnes qui se rendent d’un Etat dans l’autre pour y exercer une activité salariée bénéficient, ainsi que les membres de leurs familles qui les accompagnent, des prestations maladie maternité prévues par la législation de l’état du nouveau lieu de travail pour autant notamment qu’elles remplissent les conditions requises pour l’ouverture du droit aux prestations, compte tenu, en tant que de besoin, des périodes d’assurance ou équivalentes antérieurement accomplies au titre de la législation de l’autre Etat.</p>
<p><strong>Invalidité</strong></p>
<p>Pour les travailleurs salariés qui se rendent d’un pays dans l’autre, les périodes d’assurance ou équivalentes accomplies sous le régime de sécurité sociale du premier Etat sont totalisées en tant que de besoin et à la condition qu’elles ne se superposent pas avec les périodes d’assurance ou équivalentes accomplies sous le régime de l’autre Etat, mais seulement pour l’ouverture du droit aux prestations de ce dernier Etat. La Convention ne prévoit pas l’exportation des prestations : une pension d’invalidité roumaine ne saurait donc être transférée en France.</p>
<p><strong>Vieillesse</strong></p>
<p>Les périodes d’assurance accomplies sous chacune des législations des deux parties contractantes, de même que les périodes reconnues équivalentes à des périodes d’assurance, sont totalisées en tant que de besoin à la condition qu’elles ne se superposent pas, en vue seulement de l’appréciation des conditions d’ouverture du droit aux prestations. La Convention ne prévoit pas l’exportation des prestations : une pension de vieillesse roumaine ne saurait donc être transférée en France.</p>
<h5 class="spip">Au cours d’un séjour temporaire ou d’un transfert de résidence (retour temporaire à l’occasion d’une convalescence par exemple)</h5>
<p><strong>Séjour temporaire : droit aux prestations de l’assurance maladie maternité</strong></p>
<p>Les travailleurs français, salariés en Roumanie bénéficient, lors d’un séjour temporaire effectué en France à l’occasion d’un congé payé ou d’une absence autorisée, des prestations maladie maternité lorsque des soins médicaux immédiats, y compris l’hospitalisation, leur sont nécessaires, sous réserve que l’institution d’affiliation ait attesté que le droit aux prestations est ouvert. Cette attestation qui vaut autorisation n’est valable que pour une durée maximale de trois mois. Toutefois, ce délai peut être prorogé pour une nouvelle période de trois mois par décision de l’institution d’affiliation après avis favorable de son contrôle médical. Ces dispositions valent pour les membres de la famille qui accompagnent le travailleur.</p>
<p><strong>Transfert de résidence en cas de maladie</strong></p>
<p>Dans l’hypothèse d’une maladie présentant un caractère d’exceptionnelle gravité, le travailleur salarié, admis au bénéfice des prestations maladie-maternité à la charge de l’institution compétente du pays où il est occupé, conserve ce bénéfice à la charge de ladite institution lorsqu’il est autorisé à poursuivre son traitement sur le territoire de l’autre Etat.</p>
<p>Cette autorisation ne peut être refusée que s’il est établi que le déplacement de l’intéressé est de nature à compromettre son état de santé ou l’application du traitement médical.</p>
<p>Ces dispositions s’appliquent aux membres de la famille qui accompagnent le travailleur.</p>
<p><strong>Transfert de résidence en cas d’accident du travail ou de maladie professionnelle</strong></p>
<p>Un travailleur salarié français victime d’un accident du travail ou atteint d’une maladie professionnelle sur le territoire de l’une des parties contractantes et admis au bénéfice des prestations dues pendant la période d’incapacité temporaire, conserve le bénéfice desdites prestations à la charge de l’institution compétente lorsqu’il demande à poursuivre son traitement sur le territoire de l’autre Etat, à condition que préalablement à son départ, le travailleur ait obtenu l’autorisation de l’institution roumaine ou française à laquelle il est affilié.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/protection-sociale/article/convention-de-securite-sociale). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
