# Dossiers pays de l’expatriation

<p class="spip_document_78542 spip_documents spip_documents_left">
<img src="http://www.diplomatie.gouv.fr/fr/IMG/jpg/dossiers_pays_expatriation_cle44d318.jpg" width="660" height="125" alt="Dossiers pays de l'expatriation"></p>
<p>Bien préparer son expatriation, c’est collecter des informations sur le pays d’accueil.</p>
<p>Afin de vous aider à préparer votre expatriation, le Ministère des Affaires étrangères et du Développement International a confectionné des dossiers qui reprennent les éléments essentiels à connaitre :</p>
<ul class="spip">
<li>Les modalités d’entrée et de séjours, la protection sociale, la fiscalité du pays d’accueil et d’autres domaines intéressants la vie quotidienne à l’étranger.</li></ul>
<p>Vous trouverez aussi tous les liens utiles vers les organismes compétents de votre pays d’accueil afin de vous aider à organiser votre expatriation.</p>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/#sommaire_1">Afrique</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/#sommaire_2">Afrique du Nord / Moyen Orient</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/#sommaire_3">Amérique</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/#sommaire_4">Asie</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/#sommaire_5">Europe</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/#sommaire_6">Océanie</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Afrique</h3>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud.md" class="spip_in">Afrique du Sud</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-burkina-faso.md" class="spip_in">Burkina Faso</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-ghana.md" class="spip_in">Ghana</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-madagascar.md" class="spip_in">Madagascar</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-maurice.md" class="spip_in">Maurice</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-senegal.md" class="spip_in">Sénégal</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-togo.md" class="spip_in">Togo</a></li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Afrique du Nord / Moyen Orient</h3>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-egypte.md" class="spip_in">Egypte</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-iran.md" class="spip_in">Iran</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-israel.md" class="spip_in">Israël</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-maroc.md" class="spip_in">Maroc</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-tunisie.md" class="spip_in">Tunisie</a></li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Amérique</h3>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-argentine.md" class="spip_in">Argentine</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-bresil.md" class="spip_in">Brésil</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-canada.md" class="spip_in">Canada</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chili.md" class="spip_in">Chili</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-colombie.md" class="spip_in">Colombie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-etats-unis.md" class="spip_in">Etats-Unis</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-haiti.md" class="spip_in">Haïti</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-mexique.md" class="spip_in">Mexique</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-paraguay.md" class="spip_in">Paraguay</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-perou.md" class="spip_in">Pérou</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-dominicaine.md" class="spip_in">République Dominicaine</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-venezuela.md" class="spip_in">Venezuela</a></li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Asie</h3>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-cambodge.md" class="spip_in">Cambodge</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-inde.md" class="spip_in">Inde</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-japon.md" class="spip_in">Japon</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-singapour.md" class="spip_in">Singapour</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-thailande.md" class="spip_in">Thaïlande</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-vietnam.md" class="spip_in">Vietnam</a></li></ul>
<h3 class="spip"><a id="sommaire_5"></a>Europe</h3>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne.md" class="spip_in">Allemagne</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-autriche.md" class="spip_in">Autriche</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-belgique.md" class="spip_in">Belgique</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chypre.md" class="spip_in">Chypre</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-croatie.md" class="spip_in">Croatie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-danemark.md" class="spip_in">Danemark</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-espagne.md" class="spip_in">Espagne</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-finlande.md" class="spip_in">Finlande</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-grece.md" class="spip_in">Grèce</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-hongrie.md" class="spip_in">Hongrie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-irlande.md" class="spip_in">Irlande</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-italie.md" class="spip_in">Italie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-lituanie.md" class="spip_in">Lituanie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-luxembourg.md" class="spip_in">Luxembourg</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-norvege.md" class="spip_in">Norvège</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-pologne.md" class="spip_in">Pologne</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal.md" class="spip_in">Portugal</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-tcheque.md" class="spip_in">République Tchèque</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-roumanie.md" class="spip_in">Roumanie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-royaume-uni.md" class="spip_in">Royaume-Uni</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-slovenie.md" class="spip_in">Slovénie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-suede.md" class="spip_in">Suède</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-suisse.md" class="spip_in">Suisse</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-turquie.md" class="spip_in">Turquie</a></li></ul>
<h3 class="spip"><a id="sommaire_6"></a>Océanie</h3>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-australie.md" class="spip_in">Australie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-nouvelle-zelande.md" class="spip_in">Nouvelle-Zélande</a></li></ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
