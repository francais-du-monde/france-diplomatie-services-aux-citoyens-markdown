# Régime local de sécurité sociale

<p>Vous trouverez une présentation détaillée du système de sécurité sociale Indien<i> </i>sur le site de notre partenaire, le <a href="http://www.cleiss.fr/" class="spip_out" rel="external">CLEISS</a> (Centre des liaisons européennes et internationales de sécurité Sociale). En voici la table des matières :</p>
<ul class="spip">
<li><a href="http://www.cleiss.fr/docs/regimes/regime_inde.html#generalites" class="spip_out" rel="external">Généralités</a> (Organisation, financement, taux de cotisation sur les salaires…) ;</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_inde.html#mm" class="spip_out" rel="external">Maladie-maternité</a> (Prestations en nature et en espèces) ;</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_inde.html#atmp" class="spip_out" rel="external">Accidents du travail - maladies professionnelles</a> (Régime indemnitaire, y compris pour les survivants) ;</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_inde.html#vis" class="spip_out" rel="external">Vieillesse, Invalidité, Survivants</a> (Pensions de vieillesse, d’invalidité, de survivant, aide sociale) ;</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_inde.html#chomage" class="spip_out" rel="external">Chômage</a> (Indemnités et assistance chômage).</li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/protection-sociale-22886/article/regime-local-de-securite-sociale-110410). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
