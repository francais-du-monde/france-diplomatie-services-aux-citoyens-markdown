# Présence française

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/presence-francaise/article/presence-francaise#sommaire_1">Ambassade et consulat de France</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/presence-francaise/article/presence-francaise#sommaire_2">Autorités françaises dans le pays</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/presence-francaise/article/presence-francaise#sommaire_3">Associations dans le pays</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Ambassade et consulat de France</h3>
<p>Consultez notre <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a/article/annuaire-des-representations-108230" class="spip_out">Annuaire des représentations françaises à l’étranger</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Autorités françaises dans le pays</h3>
<p><strong>Réseau français de coopération et d’action culturelle</strong></p>
<p>Les coordonnées des services du réseau de coopération et d’action culturelle français en Suède ainsi que la description de l’activité de ces services se trouvent dans la rubrique culture du <a href="http://www.ambafrance-se.org/Service-de-cooperation-et-d-action,5647" class="spip_out" rel="external">site internet de l’Ambassade</a> ou sur le site internet du réseau culturel : <a href="http://latitudefrance.diplomatie.gouv.fr/AnnuaireCarto.html" class="spip_out" rel="external">Latitude France</a>.</p>
<p><strong>Economie : réseau mondial</strong></p>
<p>Business France, l’Agence française pour le développement international des entreprises, est présente en Suède. Sa mission est d’accompagner les entreprises françaises dans leur développement à l’international.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Site de la <a href="http://export.businessfrance.fr/suede/contact-et-plan-d-acces.html" class="spip_out" rel="external">mission économique Business France en Suède</a></p>
<p>Le service économique régional est également présent. Il est une émanation de la Direction générale du Trésor et a pour mission principale l’analyse macroéconomique et l’animation des relations économiques bilatérales entre la France et la Suède.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Site internet du <a href="http://www.tresor.economie.gouv.fr/Pays/suede" class="spip_out" rel="external">service économique français en Suède</a></p>
<p><strong>Vos élus à l’Assemblée des Français de l’étranger</strong></p>
<p>Pour toute information sur l’Assemblée des Français de l’étranger et pour connaître les conseillers et les Sénateurs qui représentent les Français établis hors de France dans votre circonscription, vous pouvez consulter les sites Internet suivants :</p>
<ul class="spip">
<li><a href="http://www.assemblee-afe.fr/" class="spip_out" rel="external">Assemblée des Français de l’étranger (AFE)</a> ;</li>
<li><a href="http://www.ambafrance-se.org/Conseillers-a-l-Assemblee-des" class="spip_out" rel="external">Les élus en Suède</a> ;</li>
<li>Sénat au service des Français de l’étranger : <a href="http://www.senat.fr/expatries/dossiers_pays/suede.html" class="spip_out" rel="external">dossier spécifique à la Suède</a> sur le site du Sénat.</li></ul>
<p><strong>Députés des Français de l’étranger</strong></p>
<p>La révision constitutionnelle du 23 juillet 2008, publiée au Journal officiel du 19 avril 2011, introduit la représentation à l’Assemblée nationale des Français établis hors de France. La création de onze circonscriptions législatives à l’étranger permet aux Français expatriés d’élire leur député à l’Assemblée nationale à partir du scrutin de 2012.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p><a href="http://www.votezaletranger.gouv.fr/" class="spip_out" rel="external">Votez à l’étranger</a></p>
<h3 class="spip"><a id="sommaire_3"></a>Associations dans le pays</h3>
<p><strong>Associations françaises</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Association démocratique des Français à l’étranger - Français du Monde (ADFE-FdM)</strong><br class="manualbr">Selmedalsvägen 78 - I TR - 129 37 HäGERSTEN<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/presence-francaise/article/presence-francaise#jbdbonnard#mc#yahoo.se#" title="jbdbonnard..åt..yahoo.se" onclick="location.href=mc_lancerlien('jbdbonnard','yahoo.se'); return false;" class="spip_mail">Courriel</a></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Union des Français de l’étranger (UFE)</strong><br class="manualbr">Cervinsväg 14<br class="manualbr">163 42 Spånga<br class="manualbr">Téléphones : 00 46 46 76 241 81 10 <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/presence-francaise/article/presence-francaise#ufesuede#mc#gmail.com#" title="ufesuede..åt..gmail.com" onclick="location.href=mc_lancerlien('ufesuede','gmail.com'); return false;" class="spip_mail">Courriel</a> </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.stockholmaccueil.se" class="spip_out" rel="external">Stockholm Accueil</a><br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/presence-francaise/article/presence-francaise#secretaire#mc#stockholmaccueil.se#" title="secretaire..åt..stockholmaccueil.se" onclick="location.href=mc_lancerlien('secretaire','stockholmaccueil.se'); return false;" class="spip_mail">Courriel</a> </p>
<p>Le guide "Stockholm pas à pas" apporte une aide pratique aux nouvelles familles francophones qui s’installent à Stockholm</p>
<p><strong>Associations franco-suédoises</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.ccfs.se/" class="spip_out" rel="external">Chambre de commerce française en Suède</a> <br class="manualbr">Grev Turegatan 10 E <br class="manualbr">114 46 Stockholm <br class="manualbr">Téléphone : [46] (0) 8 442 54 41 ou 44 <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/presence-francaise/article/presence-francaise#info#mc#ccfs.se#" title="info..åt..ccfs.se" onclick="location.href=mc_lancerlien('info','ccfs.se'); return false;" class="spip_mail">Courriel</a></p>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li><a href="http://www.ambafrance-se.org/Associations-francaises-en-Suede" class="spip_out" rel="external">Liste des associations françaises</a> maintenue à jour par l’ambassade de France en Suède ;</li>
<li>Notre article <a href="services-aux-citoyens-preparer-son-expatriation-en-savoir-plus-article-associations-des-francais-de-l-etranger.md" class="spip_in">Associations des Français de l’étranger</a></li></ul>
<p><i>Mise à jour : septembre 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/presence-francaise/article/presence-francaise). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
