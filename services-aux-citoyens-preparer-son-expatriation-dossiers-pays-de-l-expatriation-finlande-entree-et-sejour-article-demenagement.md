# Déménagement

<p>Les effets et objets personnels usagés peuvent être importés sans paiement de droit si le propriétaire a obtenu une autorisation de séjour en Finlande. Un inventaire précis des objets et du mobilier est obligatoire.</p>
<p>Un déménagement par voie routière est la meilleure solution pour une arrivée en Finlande (camion + bateau depuis Tallinn) en raison des infrastructures routières excellentes, des délais (environ cinq jours) et de la situation géographique du port à Helsinki (centre ville). Par avion, le délai est d’environ huit jours.</p>
<p><strong>Pour en savoir plus</strong> :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  notre rubrique <a href="services-aux-citoyens-preparer-son-expatriation-douanes.md" class="spip_in">Douanes</a></p>
<p>S’agissant d’un déménagement international, il est primordial de s’entourer d’un maximum de garanties en faisant appel à un professionnel disposant de certifications reconnues (marque NF Service, ISO 9002, etc.).</p>
<p><strong><a href="http://www.csdemenagement.fr/" class="spip_out" rel="external">Chambre syndicale du déménagement</a></strong><br class="manualbr">Téléphone : 01 49 88 61 40 <br class="manualbr">Télécopie : 01 49 88 61 46<br class="manualbr">Courriel : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/entree-et-sejour/article/demenagement#contact#mc#csdemenagement.fr#" title="contact..åt..csdemenagement.fr" onclick="location.href=mc_lancerlien('contact','csdemenagement.fr'); return false;" class="spip_mail">contact<span class="spancrypt"> [at] </span>csdemenagement.fr</a></p>
<p><i>Mise à jour : janvier 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/entree-et-sejour/article/demenagement). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
