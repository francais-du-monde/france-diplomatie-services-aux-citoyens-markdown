# Animaux domestiques

<p>Pour entrer sur le territoire hongrois, les chiens, les chats et les furets doivent satisfaire aux conditions suivantes :</p>
<ul class="spip">
<li>porter une identification par puce électronique ;</li>
<li>être à jour du vaccin contre la rage ;</li>
<li>être titulaires d’un passeport (<i>pet passport</i>) délivré par un vétérinaire habilité attestant de l’identification et de la vaccination de l’animal contre la rage ;</li>
<li>être âgé de trois mois et plus</li></ul>
<p>Il est conseillé également de se munir du carnet de vaccinations de l’animal domestique et avant le départ de consulter le vétérinaire.</p>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li><a href="http://www.vet-alfort.fr/" class="spip_out" rel="external">Ecole Nationale vétérinaire d’Alfort</a></li>
<li><a href="http://agriculture.gouv.fr/" class="spip_out" rel="external">Ministère de l’Agriculture</a></li></ul>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/entree-et-sejour/article/animaux-domestiques). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
