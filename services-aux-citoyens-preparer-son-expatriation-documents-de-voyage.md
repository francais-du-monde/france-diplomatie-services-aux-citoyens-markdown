# Documents de voyage

<p>Au sein de l’Union Européenne  vous pouvez voyager avec une carte nationale d’identité ou avec un passeport en cours de validité. Cependant, dès lors que vous quittez l’Union Européenne il vous faut votre passeport.</p>
<p>Dans certains cas, vous devez  également solliciter un visa auprès du consulat ou de l’ambassade en France de votre pays de destination.</p>
<p>Quand et comment demander un visa, un permis de séjour ou un permis de travail ? Comment renouveler son passeport lorsqu’on vit à l’étranger ?</p>
<p><strong>Ces démarches peuvent prendre plusieurs semaines : il est donc important d’anticiper !</strong></p>
<p><strong>Attention</strong> : Pensez à vérifier la validité de vos documents de voyage régulièrement</p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-documents-de-voyage-article-documents-d-identite.md" title="Documents d’identité">Documents d’identité</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-documents-de-voyage-article-visas.md" title="Visas">Visas</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/documents-de-voyage/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
