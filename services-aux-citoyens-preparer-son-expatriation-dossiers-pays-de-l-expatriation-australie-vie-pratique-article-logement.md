# Logement

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/vie-pratique/article/logement#sommaire_1">Où se loger ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/vie-pratique/article/logement#sommaire_2">Différents types de logements</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/vie-pratique/article/logement#sommaire_3">Conditions de location</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/vie-pratique/article/logement#sommaire_4">Hôtels</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/vie-pratique/article/logement#sommaire_5">Electricité</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/vie-pratique/article/logement#sommaire_6">Eau</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/vie-pratique/article/logement#sommaire_7">Electroménager</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Où se loger ?</h3>
<p><strong>Canberra</strong></p>
<p>Canberra est une ville bien organisée et très facile d’accès. Elle n’est cependant pas aussi vivante que Sydney ou Melbourne. Les environs de Canberra sont très beaux, à condition de ne pas rechercher la proximité des plages. Les stations de ski sont situées à quelques heures au sud de la ville.</p>
<p>A Canberra, vous aurez le choix entre un logement en appartement ou dans une maison individuelle au cœur d’un espace naturel. Le délai de recherche, selon la période, est d’une semaine à un mois. Le prix des loyers varie en fonction de la distance par rapport au centre ville. La plupart des personnes vivent à environ 20 minutes du centre ville. Les quartiers les plus recherchés sont :</p>
<ul class="spip">
<li>Forrest,</li>
<li>Griffith,</li>
<li>Red Hill,</li>
<li>Yarramula,</li>
<li>Manuka,</li>
<li>Deakin,</li>
<li>Kingston</li>
<li>Barton</li></ul>
<p>Kingston et Barton ont l’avantage d’être situés à proximité de l’école franco-australienne, du Parlement et de l’ambassade de France.</p>
<p>Aux abords de Canberra, les villes de Woden, Tuggeranong, Belconnen et Gungahlin sont des lieux de vie agréables et bien desservis. Elles possèdent leur propre périphérie et offrent un choix de centres commerciaux, d’écoles et d’équipements tels qu’hôpitaux, stades, parcs, etc.</p>
<p><strong>Adelaide</strong></p>
<p>Jolie ville possédant de magnifiques plages, Adelaide est cependant restée en retrait par rapport aux autres villes australiennes. Le coût de la vie y est peu élevé et le marché de l’immobilier vraiment abordable comparé au reste de l’Australie. L’Etat du Sud offre une bonne qualité de logements de type maison. En moyenne, on compte trois personnes par logement et deux tiers des maisons possèdent au minimum trois chambres. Environ 80% des propriétaires possèdent une maison individuelle. L’accès à la propriété est plutôt élevé avec un taux d’environ 70% de propriétaires ou d’acheteurs.</p>
<p>Les quartiers à éviter sont Salisbury et Elizabeth. Il est préférable de se loger à Burnside, Beaumont, Leabrook ou encore West Lakes. Les lieux tels que Norwood, North Adelaide et Chinatown méritent le détour.</p>
<p><strong>Sydney</strong></p>
<p>Sydney jouit d’un très bon climat et d’une économie dynamique. Le coût de l’immobilier y est beaucoup plus élevé que partout ailleurs en Australie. Si vous ne travaillez pas dans le centre des affaires de Sydney, il faudra prendre en considération la durée et le coût des déplacements quotidiens entre votre domicile et votre travail. Certaines familles choisissent de vivre à Newcastle ou Wollongong.</p>
<p>Les services immobiliers à la location ne sont pas toujours très performants. En effet, le marché, quoiqu’important, est orienté principalement à la vente, ce qui rend les baux précaires. Les agences immobilières sont très nombreuses, mais pratiquement aucune n’est compétente pour l’ensemble de l’agglomération de Sydney. A Sydney, il faut prendre le temps de découvrir le quartier choisi et ne pas hésiter à le visiter à différentes heures de la journée, ni à se renseigner sur les équipements mis à disposition (écoles, supermarchés, hôpitaux, etc.). Les chauffeurs de taxi pourront également vous fournir de précieuses informations. Enfin, Il est plus aisé de trouver un logement vide que meublé. Le délai de recherche, selon la période, est d’une semaine à trois mois.</p>
<p>A noter que les logements individuels ne seraient pas attribués en priorité aux résidents temporaires possédant un contrat de travail à durée déterminée. Ces personnes doivent donc rechercher une colocation.</p>
<p>Sydney compte cinq quartiers résidentiels chics :</p>
<ul class="spip">
<li><strong>North Sydney </strong>- Cette agglomération comprend trois portions listées ci-dessous par ordre décroissant du prix de vente moyen d’une maison :
<br>— Lower North
<br>— Northern Beaches
<br>— Upper North Shore
Les bons quartiers incluent également McMahons Point, Kirribilli, Mosman et Hunters. Ceux de Double Bay, Neutral Bay et Lavender Bay sont également recommandés. Le quartier <i>Lower North Shore </i>compte de nombreux expatriés.</li>
<li><strong>Western Sydney</strong> - Ce quartier est traditionnellement celui des " <i>bogans</i>", expression utilisée par les Australiens et les Néo-Zélandais pour désigner les habitants de ce quartier.</li>
<li><strong>Inner West Sydney</strong> - Ce quartier accueille une population très " branchée ".</li>
<li><strong>East Sydney</strong> - Ce quartier se définit comme un lieu " bobo ". Le prix moyen d’une maison s’élève à un million de dollars australiens. Les autres endroits " bobo " sont Paddington, Surry Hills et Balmain (anciens quartiers ouvriers rénovés et proches du centre ville).</li>
<li><strong>South Sydney (Sutherland Shire) </strong>- Ce quartier regroupe des familles nombreuses et très pratiquantes ayant adopté un style de vie rappelant celui des années 50 et centré uniquement sur la valeur du travail. Leur mode de vie est insulaire. Elles ne franchissent jamais les limites de leur quartier.</li></ul>
<p>Pour les personnes souhaitant habiter Sydney, les quartiers les plus recherchés sont ceux de Vaucluse, Bellevue Hill, North Shore et en général tout emplacement disposant d’une vue sur la baie. De nombreuses familles françaises habitent néanmoins dans les quartiers plus proches de l’école française (Randwick, Maroubra, Centenial Park, Coogee et Bondi). Dans le centre historique, le quartier de Leichardt a bonne réputation et dispose de nombreux commerces.</p>
<p><strong>Melbourne</strong></p>
<p>Le climat de Melbourne est très différent de celui de Sydney et n’est pas sans effet sur le mode de vie local. Melbourne a la réputation d’être plus vivable que Sydney et le prix de l’immobilier y est de 30 à 35% plus bas qu’à Sydney.</p>
<p>Les bons quartiers à Melbourne sont South Yarra, Kew et Elsternwick et les très bons quartiers sont Armadale, Hawthorn, Eaglemont et South Yarra. Les meilleurs quartiers se trouvent à Toorak, Camberwell et Brighton. De nombreux appartements et villas sont régulièrement mis sur le marché, généralement vides. Le délai de recherche est d’environ deux mois. Beaucoup d’expatriés ont élu domicile dans les quartiers résidentiels situés à la périphérie de Melbourne.</p>
<p><strong>Brisbane</strong></p>
<p>La région dispose d’un excellent climat avec cependant des étés pluvieux et humides. Son économie diversifiée connaît une croissance rapide et l’immobilier y est quasiment aussi compétitif qu’à Sydney ou Melbourne. Toutefois, le centre ville de Brisbane est défraîchi et certains quartiers périphériques (South West et ceux voisins du Port) ont été récemment infectés par des fourmis rouges. Ce dernier problème a eu des répercussions très négatives sur le marché de l’immobilier. Il est possible de se loger sur la <i>Gold Coast</i><i> </i>située à une heure de Brisbane. La <i>Sunshine Coast</i><i> </i>vers le nord est trop éloignée si l’on travaille à Brisbane.</p>
<p><strong>Perth</strong></p>
<p>Semblable à une petite Californie, Perth possède de nombreux atouts (plages et parcs grandioses). Elle dispose de toute évidence du meilleur climat de toute l’Australie et d’une économie qui croît rapidement. Le secteur immobilier y est relativement bon marché. L’inconvénient de Perth est son isolement géographique, Adelaïde située à trois heures d’avion étant la ville la plus proche. Toutefois, on trouve de nombreuses petites villes à la périphérie de Perth. Les meilleurs endroits pour se loger ne sont pas nécessairement les plus chers. Le centre des affaires étant situé dans le centre-ville, il est important, avant de s’installer en périphérie, de se pencher sur le problème des voies d’accès par route ou par train.</p>
<p><strong>Recherche de logement</strong></p>
<p>Trouver un logement confortable et à un prix abordable n’est pas toujours facile, surtout dans les grandes villes. Sydney et Melbourne ne disposent pas d’une offre importante de logements à louer. Il est préférable de faire appel à une agence immobilière répertoriée dans les <a href="http://www.yellowpages.com.au/" class="spip_out" rel="external">pages jaunes</a> (rubrique " <i>Real Estate Agent </i>") ou de consulter des sites internet spécialisés.</p>
<p>Les offres sont classées par Etat, puis par région ou par quartier. Dans les offres immobilières, le montant indiqué du loyer est toujours à la semaine. Le journal du samedi permet d’avoir un aperçu du marché immobilier. Les agences immobilières proposent également le samedi matin des visites collectives de logements. Il est aussi possible de visiter le bien moyennant la présentation d’une pièce d’identité et le dépôt de 50 AUD auprès de l’agence. Cette somme est restituée lorsque vous rendez les clés du logement à l’agence.</p>
<p>Les annonces de colocation sont publiées dans les journaux (<i>the Sydney Morning Herald </i>et <i>the Melbourne Age</i>). Dans les grandes villes, des agents immobiliers se chargent, moyennant un prix allant de 100 à 200 AUD, de trouver des colocataires qui partagent les mêmes intérêts.</p>
<p>La commission versée à l’agence est à la seule charge du propriétaire du logement.</p>
<p><strong>Quelques termes à connaître</strong></p>
<ul class="spip">
<li>Marché locatif : <i>rental market</i></li>
<li>Marché de la vente : <i>property market</i></li>
<li>Agence immobilière : <i>real estate agency</i>, <i>letting agency</i></li>
<li>Agent immobilier : <i>real-estate agent</i></li>
<li>Bail : <i>lease</i></li>
<li>Contrat de location : <i>tenancy agreement</i>, <i>rental contract</i></li>
<li>Locataire : <i>tenant</i></li>
<li>Propriétaire : <i>landlord, owner </i></li>
<li>Quartier : <i>neighbourhood </i></li>
<li>Studio : <i>studio/bedsitter</i></li>
<li>Appartement une chambre : <i>1 bedroom apartment</i></li>
<li>Maison avec 2 chambres : <i>2 bedroom house</i></li>
<li>Co-locataire : <i>flat-sharer, flatmate </i></li>
<li>Dépôt de garantie : <i>bond, deposit </i></li>
<li>Meublé : <i>furnished</i></li>
<li>Non-meublé : <i>unfurnished </i></li>
<li>Bien, propriété : <i>property </i></li>
<li>Crédit immobilier : <i>mortgage</i></li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Différents types de logements</h3>
<p><strong>Logement à la location (maison et appartement)</strong></p>
<p>Les loyers sont moins élevés que ceux pratiqués dans la plupart des pays européens. Le loyer est fonction du type de bien (maison individuelle, mitoyenne ou semi-mitoyenne, studio, appartement), de l’ancienneté, de la surface, du nombre de pièces et de l’ameublement (meublé / vide - <i>furnished / unfurnished</i>).</p>
<p>Se loger en centre ville n’est pas nécessairement moins cher qu’en périphérie. Les facteurs déterminants sont la renommée du quartier et l’effet de mode. Les modes changeant parfois très vite, il est conseillé de se renseigner avant de se mettre à la recherche d’un logement.</p>
<p>Le loyer est payé à la semaine.</p>
<ul class="spip">
<li>Studio : 180 - 300 AUD par semaine</li>
<li>Appartement d’une chambre : 150 - 275 AUD par semaine</li>
<li>Appartement de deux chambres : 200 - 400 AUD par semaine</li>
<li>Appartement de trois chambres : 275 - 500 AUD par semaine</li>
<li>Maison de deux chambres : 225 - 500 AUD par semaine</li>
<li>Maison de trois chambres : 325 - 650 AUD par semaine</li></ul>
<p><strong>Homestay / farmstay</strong></p>
<p>De nombreux propriétaires, en ville comme à la campagne, mettent à disposition une chambre dans leur maison afin de payer leur crédit immobilier. Le logement chez l’habitant s’apparente à la formule <i>Bed and Breakfast. </i>Vous êtes généralement traité comme un membre de la famille. Le prix comprend le petit-déjeuner et le dîner à horaires fixes. Cette formule entraîne quelques contraintes qu’il faut bien évaluer avant de se lancer dans ce type d’habitat.</p>
<p>Il faut compter entre 110 et 270 AUD par semaine.</p>
<p><strong>Colocation</strong> (<i>shared accommodation</i>)</p>
<p>La colocation permet non seulement de pallier le coût élevé des loyers dans les grandes villes, notamment à Sydney, mais aussi favorise l’intégration. Particulièrement prisée des jeunes et des étudiants, la colocation est de plus en plus répandue parmi les salariés. Les frais fixes (électricité, gaz, téléphone, etc.) sont généralement répartis équitablement entre tous les colocataires. Mais certains propriétaires les incluent dans le prix du loyer.</p>
<p>Avant d’opter pour la colocation, demandez à vérifier le contrat de location. Il est plus simple, au regard de la loi, d’établir le contrat de colocation au nom d’une seule personne qui ensuite sous-louera le logement à d’autres personnes.</p>
<p>Le prix d’une colocation est fixé en fonction de l’ameublement du logement, de sa superficie, de sa situation géographique et des équipements ménagers. Le loyer se règle à la semaine.</p>
<p>Il faut compter, pour une chambre simple individuelle, de 80 à 140 dollars australiens (250 dollars si l’on dispose d’une salle de bains privée) et, pour une chambre double partagée, de 60 à 175 AUD.</p>
<p><strong>Résidence universitaire</strong> (<i>campus accommodation</i>)</p>
<p>Le logement en résidence universitaire est très demandé, mais peu d’universités en possèdent. L’offre est donc très réduite. Dès votre admission dans une université, vous devez rapidement déposer votre demande auprès du bureau du logement de l’établissement (<i>housing office</i>). Le nombre de demandeurs est très élevé, notamment à Sydney et Melbourne. Bien que les étudiants étrangers soient prioritaires, vous n’avez aucune garantie que votre demande aboutisse.</p>
<p>Le coût s’élève à 90 AUD par semaine sans les repas et à 280 AUD en pension complète.</p>
<p>Pour en savoir plus, vous pouvez consulter le site Internet <a href="http://www.studyinaustralia.gov.au/global/live-in-australia/accommodation" class="spip_out" rel="external">Study in Australia</a>.</p>
<h3 class="spip"><a id="sommaire_3"></a>Conditions de location</h3>
<p><strong>Contrat de location</strong></p>
<p>Il est impératif d’établir par écrit un contrat de location (<i>tenancy agreement</i>) entre l’agent immobilier ou le propriétaire et vous-même. Evitez les ententes verbales. A l’exception des Etats de Nouvelle-Galles du Sud (<i>New South Wales</i>) et de Victoria, où la loi prévoit un contrat type de location, il est important d’exiger un contrat de location clair et détaillé. Lisez-le attentivement avant de le signer. Soyez particulièrement attentif aux clauses de reprise du logement par le propriétaire et de départ du locataire. Vérifiez également si le loyer inclut les taxes, le chauffage et la climatisation. Si tel est le cas, il faut que cela figure dans le contrat.</p>
<p>A l’exception des locations saisonnières, le contrat de bail est conclu pour une durée de 6 ou 12 mois avec une clause de renouvellement. Il est également possible de demander un bail plus long (<i>long lease</i>) pour 2 ou 3 ans, afin de bénéficier d’un loyer fixe garanti pendant toute cette période.</p>
<p>Le locataire doit respecter un préavis de 21 jours pour rompre le bail. Si le propriétaire ne souhaite pas renouveler le bail, il devra respecter un préavis de 60 jours à compter de la date d’expiration du bail. Il est obligatoire de dresser un état des lieux. Dès lors que vous avez signé un contrat de location, le propriétaire ne peut vous forcer à quitter le logement ou vous expulser, sauf s’il est en possession d’un mandat d’expulsion. Néanmoins, les dégâts causés au logement, le retard ou l’absence de paiement du loyer, la rénovation sans autorisation ou la sous-location constituent des motifs d’expulsion. Le propriétaire n’a toutefois pas le droit de changer les serrures, d’enlever vos effets personnels ou de couper l’alimentation électrique ou de gaz.</p>
<p>Pour en savoir plus, vous pouvez consulter le site Internet du <a href="http://www.tenants.org.au/" class="spip_out" rel="external">syndicat des locataires de Nouvelle-Galles du Sud</a>. Vous y trouverez des informations sur vos droits en cas de litige avec votre propriétaire.</p>
<p><strong>Caution</strong></p>
<p>La majorité des agences immobilières exigent le versement de deux semaines de loyer pour un bail (<i>lease</i>) d’un an et d’une semaine pour un bail de six mois. La caution peut s’élever à un mois de loyer selon le type de bien loué et du contrat de location établi.</p>
<p>Vous devrez également verser un dépôt de garantie (<i>bond </i>ou <i>deposit</i>) pour le gaz et l’électricité, ainsi qu’en cas de dégâts. Son montant s’élève de 4 à 6 semaines (pour un logement meublé) de loyer.</p>
<p>Dans certains cas, des frais administratifs sont facturés pour la rédaction du contrat.</p>
<p>Bien qu’il y ait des règles à respecter et que les propriétaires fassent preuve d’une certaine prudence, ceux-ci n’en demeurent pas moins assez flexibles.</p>
<p>Les étrangers titulaires d’un visa temporaire d’une validité supérieure à 12 mois ont le droit d’acquérir un bien immobilier en Australie, sous réserve que le bien acquis soit destiné à la résidence principale et non pas à des fins lucratives (location). Par ailleurs, à l’expiration du visa ou en cas de départ définitif du propriétaire, le bien doit être impérativement revendu.</p>
<p>Pour en savoir plus, vous pouvez consulter le site Internet du <a href="https://firb.gov.au/" class="spip_out" rel="external">Foreign Investment Review Board</a> (FIRB) situé à Canberra.</p>
<p>L’acquisition d’un bien immobilier entraîne, pour le vendeur et pour l’acheteur, un certain nombre de frais qui correspondent à un pourcentage calculé sur le prix de vente du bien. Il s’agit de :</p>
<ul class="spip">
<li>Droits de timbre (<i>stamp duty</i>) : cette taxe est fixée en fonction de la valeur du bien et du type d’acheteur. Dans certains Etats, les droits de timbre sont moins élevés lorsqu’il s’agit d’un premier achat et que le prix du bien est situé entre 500 000 et 600 000 AUD.</li>
<li><i>Land Transfer Registration </i> : ces frais sont liés au changement de propriétaire et versés au <i>Land Titles Office </i>(bureau du cadastre). Ils sont fixés en fonction du montant de la transaction et de la législation de l’Etat.</li>
<li>Frais de justice <i>(legal fees) </i> : ceux-ci représentent entre 1 et 2 % du prix d’achat et varient considérablement d’un Etat à l’autre.</li>
<li><i>Solicitor’s fees / Conveyancer’s Fees </i> : frais de rédaction de l’acte. Ceux-ci sont compris entre 500 et 1 700 AUD.</li>
<li>Taxes gouvernementales (<i>Government taxes</i>) : il s’agit des taxes fédérales et territoriales appliquées à toute transaction immobilière.</li>
<li>Frais d’emprunt immobilier (<i>mortgage fees</i>) : c’est l’ensemble des frais liés au crédit immobilier.</li>
<li>Contrôle des termites et des insectes nuisibles (<i>termite  pest inspection</i>) : si certains Etats sont plus souples que d’autres, ce contrôle pourra être exigé, même s’il n’est pas prévu par la loi. Son coût est compris entre 150 et 250 AUD et est supporté soit par le vendeur ou l’acheteur, soit par les deux.</li>
<li><i>Strata Inspection </i> : ce contrôle n’est obligatoire que lors de l’achat d’un appartement. Il consiste en un état des lieux de l’immeuble et de sa gestion administrative. Son coût est compris entre 180 et 250 AUD.</li>
<li><i>Inspection fee / Survey fee </i> : il est conseillé de faire procéder à une inspection lors de l’achat d’une maison individuelle ancienne. Son coût varie entre 400 et 500 AUD.</li>
<li>Frais d’assurance <i>(building insurance) </i> : Il est souhaitable de faire assurer le bien dès la signature de l’acte de vente.</li>
<li>Frais divers : architecte, installations et connexions diverses, etc.</li></ul>
<p><strong>Achat</strong></p>
<p>La plupart des banques, des promoteurs immobiliers et des organismes de crédit publient des brochures gratuites dispensant des conseils judicieux et objectifs. L’édition du samedi des principaux journaux contient des offres de ventes immobilières. Le prix indiqué correspond parfois uniquement à celui du terrain dont la superficie est calculée en <i>squares </i>ou en <i>acres</i>. La surface des pièces, exprimée en <i>feet </i>ou <i>inches</i>, n’est généralement pas indiquée dans l’annonce. Quant à celle de la maison, elle est exprimée en <i>square.</i></p>
<p>A noter qu’un <i>square</i> équivaut à environ 9,3 m2.</p>
<h3 class="spip"><a id="sommaire_4"></a>Hôtels</h3>
<p>Ils sont nombreux et certains sont bon marché : auberge de jeunesse (<i>youth hostel</i>), pension (<i>guesthouse</i>), motel, camping, etc. On y trouve des chambres pour une, deux, trois et quatre personnes. Il existe aussi des appartements familiaux à deux chambres.</p>
<p>Vous pouvez consulter les sites Internet suivants :</p>
<ul class="spip">
<li><i><a href="http://www.yha.com.au/" class="spip_out" rel="external">Youth Hostels Australia</a> </i>(YHA). Les auberges sont généralement très bien tenues et le personnel est accueillant. On en trouve dans presque toutes les grandes villes australiennes.</li>
<li><i><a href="http://www.ymca.org.au/" class="spip_out" rel="external">Young Men’s Christian Association</a> </i>(YMCA)</li>
<li><i><a href="http://www.ywca.org.au/" class="spip_out" rel="external">Young women’s Christian association</a> </i>(YWCA)</li></ul>
<h3 class="spip"><a id="sommaire_5"></a>Electricité</h3>
<p>En Australie, 80% de l’électricité est produite par le charbon. L’utilisation du gaz naturel pour fournir de l’électricité est de plus en plus répandue, notamment dans les régions du sud et de l’ouest.</p>
<p>Le coût de l’électricité est assez élevé pour les particuliers. Le courant est de 240 volt et la fréquence de 50 hertz. Les prises de courant de type I ont trois branches plates et sont spécifiques à l’Australie. Des adaptateurs peuvent être achetés en France ou sur place.</p>
<h3 class="spip"><a id="sommaire_6"></a>Eau</h3>
<p>Certaines régions sont soumises à des restrictions en matière d’utilisation de l’eau. Le changement climatique et la gestion de l’eau sont des enjeux importants pour ce pays où la sécheresse dans certaines zones inhabitées est l’une des plus sévères du monde. Une politique de sensibilisation sur le bon usage de l’eau a été mise en place dans de nombreuses régions et villes. Elle concerne tous les gestes quotidiens et occasionnels qui nécessitent une importante quantité d’eau comme l’arrosage des pelouses et des jardins, le lavage des véhicules, le remplissage des piscines, le nettoyage des sols, douches, etc. L’alerte est régulièrement donnée aux habitants au moyen de panneaux d’information (par exemple : restrictions de l’usage de l’eau en vigueur – niveau d’eau extrêmement bas / <i>water restrictions now in place – water level now extremely low</i>) en s’appuyant sur une échelle composée de niveaux (<i>stages</i>) allant de 1 à 8. A noter que chaque Etat attribue un sens bien précis à chaque niveau.</p>
<p>La consommation d’eau est normalement comprise dans le loyer. L’eau du robinet est potable, mais il est recommandé de la filtrer.</p>
<h3 class="spip"><a id="sommaire_7"></a>Electroménager</h3>
<p>Les logements vides possèdent la plupart du temps une cuisine sommairement équipée : plaques de cuisson et four, parfois réfrigérateur et machine à laver le linge ou la vaisselle et plus rarement un micro-onde.</p>
<p>Il existe un important marché de l’occasion (<i>second hand shops</i>) proposant du matériel de qualité avec jusqu’à 50 % de réduction. Pour connaître leurs coordonnées, vous pouvez consulter les <a href="http://www.yellowpages.com.au/" class="spip_out" rel="external">pages jaunes</a> australiennes.</p>
<p>La proximité des marchés asiatiques permet une variété d’équipements à des prix concurrentiels. Il est conseillé de n’apporter avec soi que les appareils électriques dont le voltage est international, sinon il faudra prévoir de nombreux adaptateurs de prise électrique.</p>
<p>Si vous souhaitez emporter avec vous vos appareils électroménagers, il est à noter que tout appareil, ainsi que ses composants susceptibles de dépasser les limites de plomb, de mercure ou d’arsenic autorisées, peut faire l’objet d’une importation restreinte ou d’une interdiction d’entrée sur le territoire australien. Par conséquent, il est important de fournir aux douanes australiennes la notice d’utilisation lisible et en anglais des appareils et le pays de fabrication. La marque de fabrication doit également apparaître clairement et de manière permanente sur l’appareil. Dans tous les cas, veillez à fournir un inventaire le plus détaillé possible à l’entreprise de déménagement international chargée du transport de vos effets personnels. Pour plus d’informations, vous pouvez consulter le site Internet des <a href="http://www.customs.gov.au/" class="spip_out" rel="external">douanes australiennes</a>.</p>
<p><strong>Chauffage / climatisation</strong></p>
<p>Il existe différents systèmes de chauffage, principalement au gaz et à l’électricité, mais aussi solaire. La qualité du chauffage dépend beaucoup de la vétusté du logement et de son isolation. La plupart des logements ne possédant pas de chauffage central, il est souvent nécessaire de prévoir des chauffages d’appoint. Dans l’habitat neuf, le chauffage par le sol ou au gaz est le plus courant. L’entretien de la chaudière doit toujours être effectué par un professionnel agréé.</p>
<p>Les frais de chauffage sont généralement à la charge du locataire, sauf en cas de location de courte durée. Dans tous les cas, il est préférable de se le faire préciser avant la signature du contrat.</p>
<p>Environ 45% des foyers australiens sont équipés d’un climatiseur. Selon la région, il faudra prévoir un climatiseur supplémentaire qui n’est pas nécessairement fourni avec le logement. Il vous appartient d’en parler au propriétaire. Il existe de nombreux types de climatiseurs. Avant tout achat, évaluez la taille de votre logement et vos besoins. Il est possible de se procurer des climatiseurs d’occasion.</p>
<p><strong>Sécurisation</strong></p>
<p>Le taux de criminalité en Australie est plus faible que celui de la majorité des grands pays industrialisés.</p>
<p>Dans tous les cas, il est préférable d’installer un système d’alarme et de protection contre le vol et de souscrire une assurance. Il existe une grande gamme d’équipements de surveillance du domicile.</p>
<p>Pour joindre la police, composez le 000 depuis un téléphone fixe et le 112 ou le 000 depuis un téléphone portable. N’oubliez pas de préciser l’endroit exact où vous vous trouvez.</p>
<p>A noter que le port et l’utilisation d’armes à feu sont strictement réglementés en Australie. Pour en savoir plus, vous pouvez consulter le site Internet de <a href="http://www.guncontrol.org.au/" class="spip_out" rel="external">l’association à but non-lucratif du contrôle des armes en Australie</a> (<i>gun-control Australia</i>).</p>
<p>Enfin, l’importation d’armes à feu, ainsi que leurs composants et accessoires, les articles, les magazines d’armurerie et de chasse, les munitions et les répliques d’armes, est soumise à un contrôle très strict à l’entrée en Australie. La demande d’importation de ces articles doit être effectuée avant l’entrée sur le territoire australien. Pour plus d’information, vous pouvez consulter le site Internet des <a href="http://www.customs.gov.au/" class="spip_out" rel="external">douanes australiennes</a>.</p>
<p><strong>Equipement vidéo</strong></p>
<p>Le système de télévision adopté en Australie est le NTSC, alors que celui utilisé en France est le système SECAM.</p>
<p>En ce qui concerne les DVD, l’Australie se trouve en zone 4 et la France en zone 2. Il est donc conseillé d’acquérir un lecteur permettant de lire les disques de toutes les zones.</p>
<p><i>Mise à jour : octobre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/vie-pratique/article/logement). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
