# Convention fiscale

<p>La France et le Grand-Duché de Luxembourg ont signé à Paris <strong>le 1er avril 1958</strong> une convention en vue d’éviter les doubles impositions et de prévenir l’évasion fiscale en matière d’impôts sur le revenu et sur la fortune. Cette convention est <strong>entrée en vigueur le 9 avril 1960. </strong>Elle a été modifiée par trois fois :</p>
<ul class="spip">
<li>par un premier avenant en date du 8 septembre 1970 publié au Journal Officiel du 8 janvier 1972 (décret n°71-1145)</li>
<li>par un deuxième avenant en date du 24 novembre 2006 publié au Journal Officiel du 16 janvier 2008 (décret n°2008-43) et entré en vigueur le 27 décembre 2007</li>
<li>par un troisième avenant en date du 3 juin 2009 publié au J.O. du 29 janvier 2011 (décret n°2011-109) et entré en vigueur le 29 octobre 2010.</li></ul>
<p>Son texte intégral est consultable sur le site de la <a href="http://www.impots.gouv.fr/" class="spip_out" rel="external">direction générale des Finances publiques</a>.</p>
<p>Voici un résumé de ses points clés pour les particuliers.</p>
<h4 class="spip">Champ d’application de la convention</h4>
<h4 class="spip">Règles d’imposition</h4>
<h5 class="spip">Définition du domicile</h5>
<p>Article 2 point 4, premier aliéna de la convention : « Le domicile fiscal des personnes physiques est au lieu de la résidence normale entendue dans le sens du foyer permanent d’habitation ou, à défaut, au lieu du séjour principal ».</p>
<h5 class="spip">Biens immobiliers</h5>
<p>Les revenus des biens immobiliers et de leurs accessoires, y compris les bénéfices des exploitations agricoles et forestières, ne sont imposables que dans l’Etat où les biens sont situés (article 3).</p>
<h5 class="spip">Salariés</h5>
<p>Les traitements, salaires et autres rémunérations analogues ne sont imposables que dans l’Etat sur le territoire duquel s’exerce l’activité personnelle source de ces revenus (article 14, point 1). Ainsi une personne exerçant une activité professionnelle au Luxembourg y sera imposée à raison de ses revenus.</p>
<p>Toutefois, ses rémunérations ne seront imposables qu’en France (article 14, point 2, 1er alinéa) si le salarié d’une entreprise située en France effectue au</p>
<p>Luxembourg une mission temporaire pendant un séjour n’excédant pas 183 jours, à condition qu’il continue à être payé par l’entreprise située en France.</p>
<p>SI la mission au Luxembourg excède une durée totale de 183 jours, le salarié est imposé au Luxembourg sur l’ensemble des rémunérations qu’il perçoit au titre de l’activité exercée au Luxembourg depuis le début de la mission (article 14, point 2, 2ème alinéa).</p>
<h5 class="spip">Professions libérales (article 15)</h5>
<p>Les revenus provenant de l’exercice d’une profession libérale et, d’une manière générale, tous revenus du travail autres que ceux qui sont visés aux articles 11 (tantièmes, jetons de présence, etc.), 12 (revenus à caractère public), 13 (pensions privées) et 14 (salariés) de la convention sont imposables seulement dans l’Etat où s’exerce l’activité personnelle et où elle a un point d’attache fixe.</p>
<p>Sont considérées comme professions libérales l’activité scientifique, artistique, littéraire, enseignante ou pédagogique, ainsi que celle des médecins, avocats, architectes ou ingénieurs.</p>
<p>En revanche, les revenus provenant de l’activité professionnelle indépendante exercée dans l’un des deux Etats par les artistes dramatiques, lyriques et chorégraphiques, ainsi que par les chefs d’orchestre et les musiciens, y sont imposables, même si cette activité n’a pas de point d’attache fixe dans l’Etat où elle est exercée.</p>
<h5 class="spip">Enseignants</h5>
<p>Les enseignants d’un Etat contractant qui vont professer dans un établissement d’enseignement (université, lycée, collège, école, etc.) situé dans l’autre Etat pour une période de deux années au plus sont exemptés d’impôt dans l’Etat où ils enseignent pour la rémunération qu’ils y perçoivent, pendant cette période de deux ans, au titre de leur enseignement (article 16).</p>
<h5 class="spip">Etudiants et apprentis</h5>
<p>Les étudiants et les apprentis de l’un des deux Etats contractants qui séjournent dans l’autre Etat exclusivement pour y faire leurs études ou y acquérir une formation professionnelle ne sont soumis à aucune imposition dans l’Etat de séjour pour les subsides qu’ils reçoivent de provenance étrangère (article 17).</p>
<h5 class="spip">Traitements et salaires à caractère public</h5>
<p>Les rémunérations allouées par un des Etats signataires, les départements, les communes ou autres personnes morales de droit public, en vertu d’une prestation de service ou de travail actuelle ou antérieure, sous forme de traitements, pensions, salaires et autres appointements sont imposables seulement dans l’Etat du débiteur (article 12).</p>
<h5 class="spip">Pensions et rentes viagères</h5>
<p>Les pensions et rentes viagères à caractère privé provenant d’un des Etats contractants et payées à une personne ayant son domicile fiscal dans l’autre Etat ne sont imposables que dans l’Etat où le bénéficiaire a son domicile fiscal (article 13).</p>
<p>En revanche, les pensions de nature publique, ainsi que les prestations servies dans le cadre d’un régime obligatoire de sécurité sociale, ne sont imposables que dans l’Etat du débiteur (article 12).</p>
<h5 class="spip">Autres revenus</h5>
<p>Ils ne sont imposables que dans l’Etat où est situé le domicile fiscal du bénéficiaire (article 19).</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/fiscalite/article/convention-fiscale). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
