# Déménagement

<p class="chapo">
      Bien que la Suisse fasse désormais partie de l’<a href="http://vosdroits.service-public.fr/F2712.xhtml" class="spip_out" rel="external">Espace Schengen</a>, elle n’est pas membre de l’union européenne douanière. Par conséquent, le contrôle douanier subsiste, en particulier à l’égard de l’ensemble des marchandises et des biens qui entrent sur le territoire.
</p>
<p>Le transfert légal de votre domicile en Suisse constitue la condition essentielle pour pouvoir exporter l’ensemble de vos biens personnels (éventuellement biens de collection, animaux et véhicules motorisés). Ces derniers peuvent être admis en exonération totale de redevances en Suisse s’ils ont été acquis au moins six mois avant le déménagement. Vous devrez également continuer de les utiliser personnellement en Suisse.</p>
<p>Toutefois bien que les animaux achetés à l’étranger et importés en Suisse soient admis en franchise de droits de douane, la taxe sur la valeur ajoutée doit être payée au taux de 8 %. La présentation d’une quittance facilite le dédouanement.</p>
<p>Les denrées alimentaires (à l’exception des boissons alcoolisées et du tabac) sont en principe exonérées de redevance au titre d’une franchise en valeur de 300 CHF.</p>
<p>Le transfert des biens personnels doit s’effectuer, au plus tard, dans les douze mois qui suivent la date du transfert de résidence en Suisse.</p>
<p>L’importation peut avoir lieu en une ou plusieurs fois. Dans ce dernier cas, l’inventaire remis au service des douanes lors de la première importation doit comprendre la totalité des biens pour lesquels la franchise est demandée.</p>
<p>La Suisse ne faisant pas partie de l’Union européenne ni de l’Union européenne douanière, il est nécessaire de fournir les trois documents suivants pour le passage en douane :</p>
<ul class="spip">
<li>preuve du changement de résidence : contrat de travail ou contrat de bail (bail à loyer)</li>
<li>inventaire détaillé et chiffré du déménagement en double exemplaire ;</li>
<li>demande de franchise douanière à la direction générale des douanes (formulaire 18.44 - disponible sur le site internet de l’<a href="http://www.ezv.admin.ch" class="spip_out" rel="external">Administration fédérale des douanes</a>).</li></ul>
<p>Le paiement des droits et de douane et autres taxes s’effectue uniquement en Francs suisses.</p>
<p>S’agissant d’un déménagement international, il est primordial de s’entourer d’un maximum de garanties en faisant appel à un professionnel disposant de certifications reconnues (marque NF Service, ISO 9002, etc.)</p>
<p><a href="http://www.csdemenagement.fr/" class="spip_out" rel="external">Chambre syndicale du déménagement</a><br class="manualbr">Téléphone : 01 49 88 61 40 <br class="manualbr">Télécopie : 01 49 88 61 46<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/entree-et-sejour/article/demenagement#contact#mc#csdemenagement.fr#" title="contact..åt..csdemenagement.fr" onclick="location.href=mc_lancerlien('contact','csdemenagement.fr'); return false;" class="spip_mail">Courriel</a></p>
<h4 class="spip">Pour en savoir plus</h4>
<ul class="spip">
<li><a href="http://www.ezv.admin.ch" class="spip_out" rel="external">Administration fédérale des douanes</a></li>
<li>Notre article thématique : <a href="services-aux-citoyens-preparer-son-expatriation-douanes-article-demenagement.md" class="spip_in">Déménagement</a></li></ul>
<p><i>Mise à jour : février 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/entree-et-sejour/article/demenagement). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
