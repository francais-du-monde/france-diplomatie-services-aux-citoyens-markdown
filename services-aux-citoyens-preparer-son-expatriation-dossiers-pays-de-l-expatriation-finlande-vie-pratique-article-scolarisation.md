# Scolarisation

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/vie-pratique/article/scolarisation#sommaire_1">Les établissements scolaires français en Finlande</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/vie-pratique/article/scolarisation#sommaire_2">Autres établissements francophones</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/vie-pratique/article/scolarisation#sommaire_3">Enseignement supérieur</a></li></ul>
<p>Pour toute information sur la scolarisation dans le système français à l’étranger, vous pouvez consulter notre thématique sur les <a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures.md" class="spip_in">Etudes supérieures</a> et la <a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais.md" class="spip_in">Scolarité en français</a>.</p>
<p>Vous y trouverez des renseignements sur :</p>
<ul class="spip">
<li>les établissements français du primaire et du secondaire à l’étranger ;</li>
<li>les bourses scolaires et la prise en charge des frais de scolarité à l’étranger ;</li>
<li>les possibilités qui s’offrent à vous si votre enfant ne peut être scolarisé à l’étranger dans le système français (enseignement à distance par le CNED, programme français langue maternelle (FLAM), internats en France) ;</li>
<li>les épreuves du baccalauréat à l’étranger ;</li>
<li>les bourses d’études supérieures en France et à l’étranger ;</li>
<li>l’équivalence des diplômes.</li>
<li><a href="http://www.aefe.fr/" class="spip_out" rel="external">Agence pour l’enseignement française à l’étranger (AEFE)</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Les établissements scolaires français en Finlande</h3>
<p><strong>Ecole Jules Verne</strong></p>
<p>L’<a href="http://www.ecolejulesverne.fi/" class="spip_out" rel="external">école Jules Verne</a> est <strong>une école française</strong>, offrant un enseignement en langue française, avec des enseignants français diplômés de l’Education nationale, pour des enfants français et non-français à partir de l’âge de deux ans. Elle appartient au réseau de l’<a href="http://www.aefe.fr/" class="spip_out" rel="external">Agence pour l’enseignement français à l’étranger (AEFE)</a> et dispense le programme officiel de l’Education Nationale française. A la rentrée 2014, elle accueille 130 enfants.</p>
<h3 class="spip"><a id="sommaire_2"></a>Autres établissements francophones</h3>
<p><strong>Lycée franco-finlandais d’Helsinki</strong></p>
<p>La fonction spécifique du <a href="http://www.hrsk.fi/fr/accueil/" class="spip_out" rel="external">Lycée franco-finlandais d’Helsinki</a> est de donner à ses élèves une bonne connaissance du français et de la culture francophone. Selon ses possibilités, le Lycée coopère avec les écoles de différents pays francophones, organise des voyages d’études et reçoit des visiteurs des pays francophones et d’autres pays.</p>
<p><strong>Ecole européenne</strong></p>
<p>La vocation des Écoles européennes est de dispenser un enseignement multilingue et multiculturel à des enfants des cycles maternel, primaire et secondaire. L’école européenne d’Helsinki dispose d’une section linguistique française.</p>
<h3 class="spip"><a id="sommaire_3"></a>Enseignement supérieur</h3>
<p>Les universités sont actuellement au nombre de 14, réparties sur tout le territoire finlandais. Parmi elles, huit sont des universités multidisciplinaires, trois des universités artistiques, deux des universités de technologie, et une université de sciences économiques. Il convient d’y ajouter l’École supérieure de la défense nationale (<i>Maanpuolustuskorkeakoulu, Försvarshögskolan</i>), rattachée au ministère de la défense.</p>
<p>L’accès à l’université est principalement ouvert aux titulaires du baccalauréat finlandais (<i>ylioppilastutkinto, studentexamen</i>), notamment parce qu’il suppose la réussite à un examen d’entrée (<i>pääsykoe, inträdesprov</i>) qui n’est accessible qu’en finnois et suédois (à quelques exceptions près, comme les formations en langue étrangère par exemple). Le nombre d’étudiants est encadré au niveau national par un <i>numerus clausus </i>fixé par chaque établissement sur la base d’une recommandation du ministère de l’éducation. L’accès est aussi possible, après examen, aux titulaires d’un diplôme de fin d’études secondaires professionnelles sanctionnant au moins trois années d’étude. La procédure de candidature à l’université est désormais unique et nationale, les lycéens émettant des vœux quant à l’université et à la discipline choisies.</p>
<p>Le français est enseigné en majeure dans les universités d’Helsinki, Turku (universités finnophones et suédophones), Jyväskylä et Tampere. Les sections de français sont intégrées dans des départements plus vastes dédiés à l’étude des langues romanes ou modernes. On compte près de 150 nouveaux inscrits en majeure chaque année, pour environ 750 étudiants ayant le français en majeure, tous cycles confondus. Chaque université accepte de surcroît annuellement entre 6 et 15 étudiants de français en mineure.</p>
<p>Les universités finlandaises délivrent des diplômes de premier, deuxième et troisième cycles, côtés en crédits ECTS <i>(European Credit Transfer System) </i>appelés points (<i>opintopiste, studiepoäng</i>) et conformes au schéma européen (processus de Bologne). Le premier cycle conduit à l’obtention de la licence (<i>kandidaattitutkinto, kandidatexamen</i>), le deuxième au master (<i>maisteritutkinto, magisterexamen</i>). Le troisième cycle, qui conduit au doctorat (<i>tohtoritutkinto, doktorsexamen</i>), pour lequel les universités disposent du monopole de délivrance, comporte un niveau intermédiaire inconnu en France et dont le nom (<i>lisensiaattitutkinto, licentiatexamen</i>) prête souvent à confusion pour les Français. Ce diplôme est généralement obtenu un ou deux ans après le master.</p>
<p>L’une des caractéristiques du système finlandais est sa très grande flexibilité et le caractère progressif des études. Les étudiants s’inscrivent dans la filière qui correspond à leur majeure et la souplesse de construction des parcours intégrant les mineures ne les oblige pas à se déterminer sur la spécialité du master visé. C’est la combinaison de la majeure et des mineures tout au long des cinq ans du cycle licence-master qui, progressivement, conduit à la spécialisation de l’étudiant. Il en résulte que le niveau licence est en fait rarement un niveau de sortie. L’examen d’entrée à l’université vaut d’ailleurs automatiquement pour les deux cycles (même s’il existe une procédure de sélection pour une entrée au niveau licence).</p>
<ul class="spip">
<li><a href="http://www.studyinfinland.fi/" class="spip_out" rel="external">Study in Finland</a></li>
<li><a href="http://www.infopankki.fi/fr/vie-en-finlande/-ducation" class="spip_out" rel="external">Infopankki.fi</a></li></ul>
<p><i>Mise à jour : janvier 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/vie-pratique/article/scolarisation). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
