# Lettre de motivation

<p><strong>Forme et style</strong></p>
<p>La lettre doit être dactylographiée, rédigée en anglais et signée.</p>
<p>Elle occupera une, voire deux, pages au plus et accompagnera votre curriculum vitae.</p>
<p>Elle mentionnera vos nom et prénom, votre adresse, votre téléphone et mobile (y compris le code international si elle est envoyée depuis la France) et votre courriel.</p>
<p>En cas de doute, n’hésitez pas à appeler l’entreprise pour connaître les nom et prénom de votre interlocuteur. N’envoyez jamais une lettre commençant par la formule d’appel " <i>Dear Madam, Dear Sir </i>".</p>
<p>Privilégiez un style formel, affirmé, mais respectueux de l’employeur. Evitez le jargon et les clichés, ainsi que les expressions toutes faites ou recopiées. La lettre est le reflet de votre personnalité.</p>
<p>Utilisez des mots forts et des verbes d’action afin de souligner votre détermination et votre énergie ; soyez clair, objectif et persuasif et ne vous contentez pas de seulement décrire votre parcours ; soyez positif dans le ton, le contenu et dans vos attentes.</p>
<p>N’ajoutez pas de détails inutiles sur vous-même et votre expérience passée. Cela risquerait de vous desservir.</p>
<p>Soyez synthétique et regroupez les idées logiquement dans un unique paragraphe. Agrémentez toujours vos propos d’exemples.</p>
<p><strong>Contenu</strong></p>
<p>Il faut compter en moyenne trois paragraphes et une conclusion.</p>
<p>Vous devez mentionner le poste pour lequel vous postulez, ainsi que vos objectifs concernant cet emploi. Il est essentiel de retenir l’attention de l’employeur dès le premier paragraphe. C’est donc l’occasion de vous présenter brièvement, ainsi que vos objectifs de carrière professionnelle.</p>
<p>Rappelez les compétences recherchées par l’employeur et n’hésitez pas à vous vendre en expliquant de façon précise, optimiste et honnête les raisons qui vous ont amené à postuler et qui font de vous le candidat idéal pour cet emploi. Mettez en évidence les réalisations et compétences professionnelles qui feront la différence et vous permettront de vous démarquer par rapport aux autres candidats. Ce point est d’autant plus important que vous êtes un candidat étranger.</p>
<p>A partir de quelques exemples, montrez comment vous pouvez être une valeur ajoutée pour cet emploi.</p>
<p>Terminez votre lettre de motivation d’une façon personnelle non seulement pour souligner que votre candidature peut être prise en considération pour l’emploi en question, mais également pour laisser une dernière impression positive dans la manière de vous présenter.</p>
<p><i>Mise à jour : octobre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/emploi-stage/article/lettre-de-motivation-108663). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
