# Entretien d’embauche

<h4 class="spip">Conduite de l’entretien</h4>
<p>Si l’entreprise norvégienne fait une recherche en France à travers un cabinet mandaté, le candidat suit un schéma classique d’entretiens.</p>
<h4 class="spip">Apparence et attitude</h4>
<p>Les Norvégiens sont très attentifs à ce que l’on pense de leur pays. Il est important d’exprimer son intérêt pour la Norvège, montrer sa motivation et argumenter les raisons de son installation. Le futur employeur voudra certainement s’assurer que le candidat va rester plusieurs années dans le pays. Si l’on ne parle pas le norvégien, il faut montrer que l’on est prêt à faire de gros efforts pour l’apprendre. Seule la possession de la langue permettra au candidat de s’intégrer pleinement. Il faut également être très ponctuel, ne pas afficher une ambition trop forte et affirmer ses capacités d’intégration dans la société et dans l’équipe.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/norvege/emploi-stage/article/entretien-d-embauche). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
