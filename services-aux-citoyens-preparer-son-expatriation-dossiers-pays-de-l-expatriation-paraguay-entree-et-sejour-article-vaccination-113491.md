# Vaccination

<p>Aucune vaccination particulière n’est exigée à l’entrée du pays.</p>
<p>Il faut cependant être à être à jour de ses vaccinations en France.</p>
<p>Certaines vaccinations sont conseillées :</p>
<ul class="spip">
<li>la fièvre jaune en cas de séjour en zone frontalière avec le Brésil en période chaude</li>
<li>Hépatite A et B pour les personnes qui travaillent en milieu hospitalier ou communautaire insalubre.</li></ul>
<p>Observation : vaccinations obligatoires pour les paraguayens : tuberculose, diphtérie, tétanos, coqueluche, poliomyélite, rubéole, rougeole, oreillons, fièvre jaune, influenza B, hépatite B. Et les recommandées : hépatite A, varicelle, papilloma virus pour les jeunes filles.</p>
<p>Pour en savoir plus, lisez notre rubrique thématique <a href="services-aux-citoyens-preparer-son-expatriation-sante.md" class="spip_in">Santé</a>.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/paraguay/entree-et-sejour/article/vaccination-113491). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
