# Programmes proposés par les Organismes publics français

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/etudes-superieures/financer-ses-etudes/article/programmes-proposes-par-les-organismes-publics-francais#sommaire_1">I. Centre de Coopération Internationale en Recherche Agronomique pour le Développement (CIRAD)</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/etudes-superieures/financer-ses-etudes/article/programmes-proposes-par-les-organismes-publics-francais#sommaire_2">II. Centre National de la Recherche Scientifique (CNRS)</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/etudes-superieures/financer-ses-etudes/article/programmes-proposes-par-les-organismes-publics-francais#sommaire_3">III. Institut de Recherche pour le Développement (IRD)</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/etudes-superieures/financer-ses-etudes/article/programmes-proposes-par-les-organismes-publics-francais#sommaire_4">IV. Institut national de la Santé et de la Recherche médicale (INSERM)</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>I. Centre de Coopération Internationale en Recherche Agronomique pour le Développement (CIRAD)</h3>
<p>Dans le souci de faciliter les échanges internationaux, le CIRAD offre à des étudiants des possibilités d’accueil par une de ses équipes à l’étranger pour y effectuer un stage, une thèse ou un séjour d’études post-doctorales. Les candidatures doivent être enregistrées sur internet ou, à défaut, transmises par télécopie.</p>
<blockquote class="texteencadre-spip spip"><br class="autobr">
<strong>Où s’adresser ?</strong> <br class="manualbr">CIRAD <br class="manualbr">Direction Générale Déléguée à la Recherche et à la Stratégie <br class="manualbr">TA 279/04, Avenue Agropolis <br class="manualbr">34398 Montpellier Cedex 5 <br class="manualbr">Tél. 04 67 61 58 00 - Fax : 04 67 61 59 86 <br class="manualbr">Site : <a href="http://www.cirad.fr/" class="spip_out" rel="external">www.cirad.fr</a> <br class="manualbr">Courriel : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/etudes-superieures/financer-ses-etudes/article/programmes-proposes-par-les-organismes-publics-francais#dgd_rs#mc#cirad.fr#" title="dgd_rs..åt..cirad.fr" onclick="location.href=mc_lancerlien('dgd_rs','cirad.fr'); return false;" class="spip_mail">dgd_rs<span class="spancrypt"> [at] </span>cirad.fr</a><br class="autobr">
</blockquote>
<h3 class="spip"><a id="sommaire_2"></a>II. Centre National de la Recherche Scientifique (CNRS)</h3>
<p>Par l’intermédiaire du CNRS,  l’organisme de recherche japonais JSPS (<i>Japan Society for the Promotion of Science</i>)  propose des bourses à des chercheurs français pour un séjour de recherche au Japon.</p>
<p><strong>Bourses de séjour au Japon</strong></p>
<ul class="spip">
<li>Bourses standard JSPS pour post-doctorants</li>
<li>Bourses courtes JSPS pour doctorants et post-doctorants</li>
<li>Bourses de la JSPS pour chercheurs confirmés</li>
<li>Bourses d’été JSPS pour doctorants et post-doctorants</li></ul>
<blockquote class="texteencadre-spip spip"><br class="autobr">
<strong>Où s’adresser ?</strong> <br class="manualbr">Centre National de la Recherche Scientifique <br class="manualbr">Direction des Relations Internationales <br class="manualbr">M<sup>me</sup> Monique BENOIT <br class="manualbr">3, rue Michel-Ange <br class="manualbr">75 794 Paris Cedex 16 <br class="manualbr">Fax : 01 44 96 46 95/48 56<br class="manualbr">Site : <a href="http://www.cnrs.fr/derci" class="spip_out" rel="external">www.cnrs.fr/derci</a> <br class="manualbr">Courriel : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/etudes-superieures/financer-ses-etudes/article/programmes-proposes-par-les-organismes-publics-francais#Monique.Benoit#mc#cnrs-dir.fr#" title="Monique.Benoit..åt..cnrs-dir.fr" onclick="location.href=mc_lancerlien('Monique.Benoit','cnrs-dir.fr'); return false;" class="spip_mail">Monique.Benoit<span class="spancrypt"> [at] </span>cnrs-dir.fr</a><br class="autobr">
</blockquote>
<h3 class="spip"><a id="sommaire_3"></a>III. Institut de Recherche pour le Développement (IRD)</h3>
<p>L’Institut de recherche pour  le Développement offre à des étudiants des possibilités d’accueil à l’étranger dans le cadre  de stages, thèses, contrat post-doctoraux  ou volontariats internationaux. <br class="autobr">Les demandes sont à effectuer directement auprès des unités de recherche.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Consulter la liste des unités de recherche de l’IRD sur <a href="http://www.ird.fr/la" class="spip_url spip_out auto" rel="nofollow external">www.ird.fr/la</a> recherche/unites-de-recherche</p>
<p>Pour les doctorants, chercheurs, ingénieurs et techniciens originaires des pays du Sud, l’IRD propose des financements pour des thèses ou des échanges scientifiques</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Pour en savoir plus, consulter <a href="http://www.ird.fr/programmes-dsf" class="spip_url spip_out auto" rel="nofollow external">www.ird.fr/programmes-dsf</a> </p>
<blockquote class="texteencadre-spip spip"><br class="autobr">
<strong>Où s’adresser ?</strong> <br class="manualbr">Institut de Recherche pour le Développement <br class="manualbr">Direction des programmes de recherche  et de la formation au Sud (DPF)<br class="manualbr">44 Bd de Dunkerque<br class="manualbr">CS 90009<br class="manualbr">13572 Marseille cedex 02<br class="manualbr">Tél. : 33 (0)4 91 99 92 00 - Télécopie : 33 (0)4 91 99 92 22 <br class="manualbr">Site : <a href="http://www.ird.fr/" class="spip_out" rel="external">www.ird.fr</a> <br class="manualbr">Courriel : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/etudes-superieures/financer-ses-etudes/article/programmes-proposes-par-les-organismes-publics-francais#webmaster#mc#ird.fr#" title="webmaster..åt..ird.fr" onclick="location.href=mc_lancerlien('webmaster','ird.fr'); return false;" class="spip_mail">webmaster<span class="spancrypt"> [at] </span>ird.fr</a><br class="autobr">
</blockquote>
<h3 class="spip"><a id="sommaire_4"></a>IV. Institut national de la Santé et de la Recherche médicale (INSERM)</h3>
<p><strong>Programme Projets Avenir</strong></p>
<ul class="spip">
<li><strong>Public concerné</strong> : les chercheurs statutaires, universitaires, hospitaliers ou hospitalo-universitaires. Également, les post-doctorants, les chefs de clinique assistants et les assistants hospitalo-universitaires.</li>
<li><strong>Dépôt des dossiers</strong> : au mois de mars. Le dossier est téléchargeable sur le serveur <a href="http://www.inserm.fr/fr" class="spip_out" rel="external">www.inserm.fr</a> ou <a href="http://www.eva.inserm.fr/" class="spip_out" rel="external">www.eva.inserm.fr</a>. Le dossier est à déposer sur le site EVA.</li></ul>
<blockquote class="texteencadre-spip spip"><br class="autobr">
<strong>Où s’adresser ?</strong> <br class="manualbr">Institut National de la Santé et de la Recherche Médicale <br class="manualbr">Département des Ressources humaines <br class="manualbr">À l’attention de M<sup>me</sup> Anne-Marie Laffaye <br class="manualbr">101, rue de Tolbiac <br class="manualbr">75654 Paris Cedex 13 <br class="manualbr">Tél. 01 44 23 62 14 <br class="manualbr">Site : <a href="http://www.inserm.fr/" class="spip_out" rel="external">www.inserm.fr</a> <br class="manualbr">Courriel : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/etudes-superieures/financer-ses-etudes/article/programmes-proposes-par-les-organismes-publics-francais#laffaye.avenir#mc#tolbiac.inserm.fr#" title="laffaye.avenir..åt..tolbiac.inserm.fr" onclick="location.href=mc_lancerlien('laffaye.avenir','tolbiac.inserm.fr'); return false;" class="spip_mail">laffaye.avenir<span class="spancrypt"> [at] </span>tolbiac.inserm.fr</a><br class="autobr">
</blockquote>
<p><i>Mise à jour : février 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/etudes-superieures/financer-ses-etudes/article/programmes-proposes-par-les-organismes-publics-francais). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
