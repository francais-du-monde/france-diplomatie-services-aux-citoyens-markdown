# Animaux domestiques

<h4 class="spip">Importation depuis un pays de l’UE</h4>
<p><strong>Chiens et chats et furets</strong></p>
<p>Pour entrer en Suède, les chiens et les chats doivent satisfaire aux conditions suivantes :</p>
<ul class="spip">
<li>être âgés d’au moins trois mois ;</li>
<li>être identifiés par puce électronique. La Suède reconnaît également la méthode d’identification par tatouage (si effectué avant 03/07/2011) et être lisible et accompagné d’un certificat du vétérinaire comportant la date du tatouage ;</li>
<li>être valablement vaccinés contre la rage ; en cas de primovaccination, l’animal est seulement autorisé à voyager 21 jours après le avoir subi un titrage sérique des anticorps antirabiques, sauf pour les furets (examen de laboratoire effectué sur un prélèvement sanguin et permettant de s’assurer de la validité de la vaccination de l’animal contre la rage) dans un <a href="http://ec.europa.eu/food/animal/liveanimals/pets/approval_en.htm" class="spip_out" rel="external">laboratoire agréé par l’Union européenne</a>. Le résultat doit être supérieur ou égal à 0,5 UI/ml ;</li>
<li>être titulaire d’un passeport délivré par un vétérinaire titulaire d’un mandat sanitaire ;</li>
<li>avoir subi un traitement contre les tiques et l’échinococcose.</li></ul>
<p><strong>Vaccins valables en Suède :</strong></p>
<ul class="spip">
<li>Rabisin chiens et chats, valable 365 jours</li>
<li>Nobivac Rage chiens, chats et furets, valable 3x365 jours pour chiens et chats et 1x365 jours pour les furets</li>
<li>Purevax Rage pour chats, avec une durée de validité de 365 jours après le premier vaccin, et de 3x365 jours après revaccination</li></ul>
<p>Il faut signaler l’import de son animal de compagnie aux <a href="http://www.tullverket.se/en" class="spip_out" rel="external">douanes suédoises</a>.</p>
<p>Lors d’une installation en Suède, les chiens doivent être inscrits au Registre central des chiens, ou <i>Hundregistret</i>, qui est accessible en ligne.</p>
<p>Vous trouverez en ligne <a href="http://www.jordbruksverket.se/amnesomraden/djur/olikaslagsdjur/hundarochkatter/hundregistret.4.207049b811dd8a513dc8000442.html" class="spip_out" rel="external">les instructions</a> concernant l’identification et l’enregistrement des chiens.</p>
<p><strong>Autres animaux</strong></p>
<p>Les informations sur l’importation de chevaux, lapins domestiques, rongeurs domestiques, reptiles et amphibiens et toutes espèces dangereuses figurent sur le site internet du ministère de l’agriculture suédois.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li>Notre article thématique <a href="services-aux-citoyens-preparer-son-expatriation-douanes-article-animaux-domestiques.md" class="spip_in">Animaux domestiques</a></li>
<li>Il est conseillé de prendre contact avec l’<a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-etrangers-en-france/article/annuaire-des-representations-etrangeres-en-france" class="spip_in">ambassade du pays de destination</a></li>
<li><a href="http://www.jordbruksverket.se/swedishboardofagriculture/engelskasidor/animals/import.4.6621c2fb1231eb917e680002950.html" class="spip_out" rel="external">Ministère de l’Agriculture (<i>Jordbruksverket</i>)</a><br class="manualbr">Tél : +46 771 223223</li>
<li><a href="http://www.jordbruksverket.se/2.5abb9acc11c89b20e9e800057.html" class="spip_out" rel="external">Informations du ministère suédois de l’Agriculture</a> sur l’importation de chatons, chiots, et bébés furets</li>
<li><a href="http://agriculture.gouv.fr/" class="spip_out" rel="external">Fiche du ministère français de l’Agriculture</a> sur les mouvements des animaux domestiques au sein de l’UE.</li></ul>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/entree-et-sejour/article/animaux-domestiques). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
