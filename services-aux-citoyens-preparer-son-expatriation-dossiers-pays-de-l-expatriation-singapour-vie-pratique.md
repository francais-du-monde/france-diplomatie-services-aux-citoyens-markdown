# Vie pratique

<h2 class="rub22906">Logement</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/vie-pratique/#sommaire_1">Où se loger ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/vie-pratique/#sommaire_2">Conditions de location</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/vie-pratique/#sommaire_3">Hôtels</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/vie-pratique/#sommaire_4">Electricité</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/vie-pratique/#sommaire_5">Eau</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/vie-pratique/#sommaire_6">Electroménager</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Où se loger ?</h3>
<h4 class="spip">Comment chercher un logement</h4>
<p>La situation du secteur immobilier résidentiel à Singapour est très particulière. Une grande majorité des habitants logent dans des appartements d’Etat, dont ils sont locataires ou qu’ils peuvent acquérir au bout d’un certain nombre d’années.</p>
<p>La recherche d’un logement est relativement aisée et passe par les agences de location ou les petites annonces. Le délai moyen de recherche est de deux à trois semaines. Le choix de logements est important, tant au niveau des appartements que des villas. Un état des lieux est conseillé. Un dépôt de garantie de deux mois est demandé. Le loyer est payable avec un à deux mois d’avance. Il comprend les charges de copropriété. Les autres charges (eau, électricité, gaz, téléphone, câble…) dues par le locataire sont distinctes du loyer.</p>
<p>Il existe un programme particulier d’offres de locations d’appartements appelé <i>shift programm</i> destiné à aider les jeunes professionnels ou étudiants à se loger à moindre prix à Singapour.</p>
<p>Si vous souhaitez vous inscrire à ce programme, vous devez contactez :</p>
<p><a href="http://www.hdb.gov.sg/" class="spip_out" rel="external">Housing Development Group</a><br class="manualbr">Tél. : [65] 68 85 53 15 / 16 / 17 / 25 / 26 / 27<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/vie-pratique/#hdg#mc#jtc.gov.sg#" title="hdg..åt..jtc.gov.sg" onclick="location.href=mc_lancerlien('hdg','jtc.gov.sg'); return false;" class="spip_mail">Courriel</a></p>
<p>Il est possible de négocier le montant du loyer, de demander certains travaux ou aménagements (peinture, rideaux neufs, matériels électroménager, meubles…). En principe, les contrats de location durent deux ans et peuvent être renouvelés à l’issue de cette période pour un ou deux ans supplémentaires.</p>
<p>En principe, les agences demandent un demi mois de loyer de rémunération si le bail est de un an et un mois de loyer si le bail est de deux ans. Cependant il apparaît que diverses pratiques ont cours, notamment en fonction du niveau de loyer versé. Les cautions sont versées au propriétaire. Elles s’élèvent généralement à deux mois de loyer.</p>
<p>Vous trouverez ci-dessous les coordonnées d’agences immobilières offrant leurs services à la communauté française. Cette liste n’est pas exhaustive.</p>
<h5 class="spip">Annonces</h5>
<p>Vous trouverez des annonces sur le site Internet de l’<a href="http://www.afsingapour.com/" class="spip_out" rel="external">Association française de Singapour</a>.</p>
<p>Vous trouverez également des informations concernant la location immobilière sur le site Internet <a href="https://www.contactsingapore.sg/" class="spip_out" rel="external">Contact Singapore</a> et des offres et une liste d’agences sur le site Internet <a href="http://www.expatsingapore.com/" class="spip_out" rel="external">Expatsingapore.com</a>.</p>
<p>Quelques annonces paraissent également dans le supplément quotidien du <a href="http://www.straitstimes.com/" class="spip_out" rel="external">Straits Times</a>, principal journal local, et sur les panneaux prévus à cet effet à l’Alliance française et dans les grandes surfaces.</p>
<h4 class="spip">Typologie des logements</h4>
<h5 class="spip">Studios</h5>
<p>Il existe des studios à louer à Singapour mais le choix est plus restreint que pour les appartements. La plupart des logements ouverts à la location sont des appartements et des villas. La majorité des jeunes gens arrivant seuls sur Singapour optent plutôt pour la colocation. Il y a davantage de choix de logements et le loyer est moins onéreux. Mais il existe tout de même des studios. Il faut savoir que la plupart de ceux qui sont proposés à la location sont en fait des T1. La chambre est une pièce séparée du salon et de la cuisine.</p>
<h5 class="spip">Appartements</h5>
<p>Deux types de logements en appartements sont proposés à Singapour : les logements en HDB (l’équivalent des HLM français) et les logements en condominiums (résidences privées avec piscine, tennis, salle de sports, barbecues…). Les appartements en HDB sont d’un standing moins élevé que ceux des condominiums. Dans les deux cas, vous pouvez trouver des T1, T2, T3 et T4, meublés ou non. On trouve ces deux types de logement dans le centre ville et en périphérie avec une prédominance de condominiums en centre ville et de HDB en périphérie.</p>
<h5 class="spip">Condominiums</h5>
<p>Les condominiums sont des résidences privées de haut standing, habitées, pour la majorité, par des expatriés ou des personnes aisées. Les condominiums sont souvent équipés d’une piscine, d’un court de tennis, d’une salle de sport et d’espaces pour barbecues. Une équipe de garde et de personnel d’entretien s’occupent de la résidence. Les baux sont en général de deux ans, mais il est possible d’en trouver pour un an. Les appartements à louer sont meublés ou non et peuvent comporter jusqu’à cinq chambres.</p>
<h5 class="spip">Villas</h5>
<p>Il existe deux types de villas : les villas traditionnelles et les villas coloniales appelées <i>black and white</i>. Ce sont les anciennes villas de l’époque coloniale, situées en centre ville et sur l’ancien camp militaire britannique de Seletar. Elles appartiennent toutes à l’Etat. Il n’est donc pas possible de les acheter. Pour en devenir locataire, il faut s’inscrire sur une liste d’attente et participer à une mise aux enchères. Il existe d’autres villas, en centre ville comme à la périphérie, offertes à la location ou à la vente pour les Singapouriens ou les personnes ayant le statut de <i>permanent resident</i>.</p>
<h4 class="spip">Types d’hébergements</h4>
<h5 class="spip">Locations</h5>
<p>La plupart des expatriés vivent en location dans des <i>condominums</i> ou villas. Peu décident d’acheter un bien immobilier à Singapour car le séjour pour un temps limité et la législation ne s’y prêtent pas forcément. Les personnes ayant le statut de <i>permanent resident</i> ont plus de droits que les personnes titulaires d’un <i>employment pass</i> car elles sont censées résider plus longtemps à Singapour.</p>
<p>Le dynamisme économique des années 2005 à 2007 a entraîné une forte hausse des loyers. Pour des informations à jour, vous pouvez vous reporter au site Internet de l’<a href="http://www.ura.gov.sg/" class="spip_out" rel="external">Urban Redevelopment Authority</a>, qui publie chaque trimestre le prix moyen par mètre carré dans tous les condominiums pour lesquels au moins dix contrats ont été signés dans le trimestre.</p>
<h5 class="spip">Colocations</h5>
<p>Les personnes en colocation sont pour la plupart de jeunes diplômés, stagiaires ou étudiants, arrivant seuls à Singapour et prévoyant de rester pour une durée limitée. Ils partagent, en général, le logement avec d’autres personnes se trouvant dans la même situation, toutes nationalités confondues. Les baux sont, dans ce cas, souvent au nom d’une seule personne et les propriétaires sont en principe arrangeants et établissent les contrats pour de courtes durées (quelques mois).</p>
<p>Dans le cas des colocations, les logements sont pour la majorité déjà meublés.</p>
<p>Les appartements typiques ont en général trois chambres (une grande avec salle de bain privée et le plus souvent deux autres chambres partageant une salle de bain).</p>
<p>Pour connaître les annonces de colocation, regarder tous les sites d’annonces immobilières ainsi que les panneaux d’annonces dans les universités et écoles.</p>
<h5 class="spip">Meublés</h5>
<p>Les appartements meublés sont très courants à Singapour. Beaucoup d’expatriés choisissent de ne pas déménager avec des conteneurs. Cette tendance se retrouve surtout chez les jeunes. Les tarifs sont donc un peu plus élevés. Les meubles sont souvent simples (mobilier Ikea) mais corrects et suffisants. Plus le standing d’ameublement est élevé, plus le montant du loyer augmentera. Il est par ailleurs souvent plus facile de négocier un bail d’un an plutôt que de deux pour les appartements meublés car les locataires restent en général moins longtemps.</p>
<h3 class="spip"><a id="sommaire_2"></a>Conditions de location</h3>
<h4 class="spip">Justificatifs à fournir</h4>
<ul class="spip">
<li>Si le bail est au nom de votre entreprise qui prend en charge votre logement, les propriétaires souhaiteront certainement consulter les passeports des occupants du logement ainsi que "l’employment pass" de la personne travaillant dans l’entreprise locataire.</li>
<li>Si le bail est au nom des locataires, les propriétaires demanderont à consulter les passeports des locataires ainsi que leurs permis de séjour et parfois une attestation de l’employeur afin d’être certain que les occupants sont en situation régulière à Singapour.</li></ul>
<h4 class="spip">Caution</h4>
<p>Si vous passez par une agence immobilière, vous aurez à participer aux frais d’agence (entre un et deux mois de loyer) si votre loyer est inférieur à 2500 SGD/mois.</p>
<p>Par ailleurs, on vous demandera également deux mois de caution. C’est à vous ensuite d’essayer de négocier avec l’agent immobilier ou le propriétaire, mais les règles en vigueur sont celles-ci.</p>
<h4 class="spip">Paiement d’avance</h4>
<p>Pour confirmer la location, quand le locataire présente sa lettre d’intention (Letter of Intent) pour occuper ce logement, il est d’usage qu’il verse également un mois de loyer par avance (en plus des deux mois de caution).</p>
<h4 class="spip">Durée du bail</h4>
<p>Le bail est en principe de deux ans à Singapour, mais il est possible de négocier pour qu’il soit raccourci à un an. Peu d’agences ou de propriétaires l’acceptent mais certaines conditions peuvent être valables. Par exemple, il est prévu que certains condominiums subissent des travaux à une date donnée ou que d’autres travaux bruyants aient lieu dans le quartier. Dans ces cas, il est possible de raccourcir la période du bail à un an.</p>
<p>Attention, en général, pour les baux d’un an le loyer est plus élevé que pour les baux de deux ans.</p>
<h4 class="spip">Délais de préavis</h4>
<p>Si le locataire souhaite quitter l’appartement, il doit en informer son propriétaire par lettre deux mois avant son départ.</p>
<p>Si le locataire quitte l’appartement pour « raisons professionnelles », il est nécessaire pour l’entreprise du locataire (que le bail soit à son nom ou au nom de la compagnie) d’informer le propriétaire par lettre que leur employé est muté dans un autre pays ou qu’il n’est plus employé par cette entreprise. Pour cela, il faut qu’au préalable, lors de la signature du bail, que la cause diplomatique ait été acceptée par le propriétaire.</p>
<p>Cette clause permet habituellement de signer des baux de 12 mois + 2 mois. Ainsi, à n’importe quel moment après la période des 12 mois, le locataire peut quitter le logement en donnant un préavis de 2 mois. Le loyer minimum à verser est donc de 14 mois, quoi qu’il arrive.</p>
<h4 class="spip">Loyer</h4>
<p>Le montant du loyer dépend de la localisation géographique du logement. Il sera plus élevé si l’habitation est au centre ville, proche d’une école internationale (car la demande est élevée), ou si elle est proche d’une station de métro.</p>
<p>Dans le centre ville, les prix des appartements à la vente sont très élevés, c’est pourquoi les loyers sont habituellement plus onéreux à cet endroit.</p>
<p>Singapour possède des quartiers « résidentiels » plus adaptés aux expatriés dans diverses zones :</p>
<ul class="spip">
<li>Le « cœur historique » des expatriés : Tanglin, Orchard</li>
<li>Les zones voisines de ce « cœur historique », le long des axes Bukit Timah et Thomson road</li>
<li>Certaines rues de la City</li>
<li>le quartier du Lycée français (Ang Mo Kio) moins onéreux</li>
<li>« East Coast » en allant vers l’aéroport</li></ul>
<p>La très forte hausse du marché locatif pousse les étrangers à quitter les zones « traditionnelles » ci-dessus, et il n’est plus rare de voir des Français s’installer dans le nord de l’île : Sembawang, Seletar…</p>
<h4 class="spip">Charges</h4>
<p>L’électricité, l’eau et le gaz sont facturés par la société SP Services qui envoie ses factures mensuellement. Ces notes sont à la charge des locataires.</p>
<p>La plupart des logements sont proposés partiellement équipés en appareils électroménagers : air conditionné, plafonnier, frigidaire, cuisinière et four incorporés, mais c’est aux locataires de veiller à leur maintenance.</p>
<h4 class="spip">Modalités de paiement</h4>
<p>Le montant peut être réglé en liquide, par carte NETS, par chèque prélèvement automatique (GIRO) ou par « Internet Banking (IB) ».</p>
<h4 class="spip">Assurance habitation</h4>
<p>Les propriétaires prennent, en général, en charge l’assurance contre les incendies et selon leur bon vouloir, l’assurance pour les biens mobiliers du logement.</p>
<h4 class="spip">Taxe d’habitation</h4>
<p>Ce n’est pas au locataire de payer la taxe d’habitation. Un seul cas est cependant à souligner : si le bien immobilier est un local commercial, alors la GST (TVA) devra être payée en plus du loyer.</p>
<h3 class="spip"><a id="sommaire_3"></a>Hôtels</h3>
<p>Pour tout type de logement sur Singapour, il est recommandé de réserver sa chambre 1 ou 2 jours à l’avance. Sinon, commencez à chercher le plus tôt possible dans la matinée pour le soir même.</p>
<p>Il devient difficile de se loger pour un prix modique à Singapour. La rénovation urbaine fait disparaître un grand nombre de petits hôtels chinois et de <i>guest houses</i>. Les hôtels d’un luxe souvent tapageur, autrefois cantonnés au secteur d’Orchard Road, s’installent dans Chinatown ou Little India.</p>
<p>Dernière précision : si dans les adresses bon marché, les prix sont toutes taxes comprises, il faut, dans les autres catégories, ajouter un peu plus de 15% de taxes aux prix annoncés. De plus, le paiement est souvent demandé en liquide.</p>
<h4 class="spip">Hôtels</h4>
<p>Dans la plupart des hôtels, une avance vous sera demandée si vous réservez une chambre pour une arrivée après 16h.</p>
<p>Les hôtels sont classés par le ministère du Tourisme (HLB). Mais ce n’est pas pour autant un gage de qualité. Pour un prix moyen local (100 dollars), vous aurez une chambre sans grande originalité mais confortable. N’hésitez pas à entrer dans l’hôtel, et à demander à voir une chambre. On est parfois agréablement surpris.</p>
<p>La plupart des hôtels proposent des équipements standard : téléphone à numérotation internationale directe, Internet et câble, service en chambre, mini-bar, ports de données pour modem, chambres ou étages non fumeurs, et centres d’affaires et de remise en forme disposant des tout derniers équipements.</p>
<p>Il est conseillé de marchander le prix d’une chambre, dans les grands hôtels, surtout en période creuse. Pour disposer de l’air conditionné, dites "with air con". Si l’on vous propose une chambre moins chère "with fan", sachez qu’il s’agit de ventilateurs. Beaucoup de chambres bon marché ne disposent pas de fenêtres.</p>
<p>Si vous arrivez à Singapour sans avoir effectué de réservation hôtelière, les comptoirs du SHA à l’aéroport Changi de Singapour pourront vous assister pour votre réservation.</p>
<p>Heures d’ouverture quotidiennes des comptoirs du SHA au Terminal 1<br class="manualbr">Comptoir Est : 10 h – 23 h 30<br class="manualbr">Comptoir Ouest : 24 heures</p>
<p>Heures d’ouverture quotidiennes des comptoirs du SHA au Terminal 2<br class="manualbr">Comptoir Nord : 7 h – 23 h<br class="manualbr">Comptoir Sud : 24 heures</p>
<p>Vous pouvez vous procurer auprès du Singapore Tourist Board les deux livrets « Hôtels » et « Budget Hôtels » ou bien de consulter la liste des hôtels sur le <a href="http://www.visitsingapore.com/" class="spip_out" rel="external">site Internet officiel du Singapore Tourist Board</a>. Ces listes sont très complètes, classées par lieux et par prix. Vous trouverez une sélection allant du très haut de gamme avec le « Raffles », hôtel colonial mythique, au petit hôtel pour routard, sans climatisation.</p>
<p>Pour une situation très centrale, vous pouvez loger autour d’Orchard Road. Vous y trouverez une multitude de chaînes d’hôtels.</p>
<h4 class="spip">Auberges de jeunesse</h4>
<p>Quelques <i>Youth hostels</i>, YWCA et YMCA existent. L’entretien y est assez aléatoire et ce n’est pas toujours aussi avantageux que les adresses spécialisées dans l’hébergement des petits budgets, de type <i>guesthouse</i>. Question de prix surtout, mais aussi de situation géographique, de services et d’ambiance, tout simplement…</p>
<p>Pour les auberges de jeunesse à Singapour, vous pouvez contacter :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Youth Hostels Association</strong><br class="manualbr">20 Kramat Lane - #04-12 United House Singapore 228773<br class="manualbr">Tél. : [65] 733 67 53 - Télécopie : [65] 733 67 54</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Hostelling International Hi Singapore</strong><br class="manualbr">1 Pasir Ris Close Singapore 519599</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>YMCA of Singapore</strong><br class="manualbr">1 Orchard Road Singapore 228823<br class="manualbr">Tél. : [65] 63373444 et 63366000 - Télécopie : [65] 63373140</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.fuaj.org/" class="spip_out" rel="external">Fédération unie des auberges de jeunesse</a></p>
<h4 class="spip">Bed and breakfast</h4>
<p>Il n’existe pas à proprement parler d’hébergement "Bed and Breakfast" à Singapour. Les <i>guesthouses</i> font office de type d’hébergement « moyen de gamme » entre auberges et hôtels. Elles indiquent en principe des chambres d’hôtes mais on trouve un peu de tout sous cette appellation, du dortoir à la chambre climatisée. C’est en général moins cher qu’un véritable hôtel mais elles sont quand même bien tenues. On doit enlever ses chaussures avant d’entrer, cafés et thés sont offerts, de nombreux services sont proposés (réservation de visites, laverie, Internet…) et des informations sur la ville et la région sont souvent disponibles. Vous trouverez beaucoup de <i>guesthouses</i> dans le quartier de Little India (les petites rues perpendiculaires à Serangoon Road) et dans le quartier d’Arab Street (autour de Beach Road).</p>
<h4 class="spip">Serviced apartments</h4>
<p>Les <i>serviced apartments</i> sont des appartements disponibles en résidences hôtelières pour les familles recherchant un logement pour un temps limité. Ces personnes sont souvent en attente de leur propre lieu de résidence à leur arrivée ou sont sur le point de quitter Singapour, leur conteneur étant déjà parti avec leurs affaires personnelles et leur mobilier. Il est possible de louer ces logements pour une période très courte comme pour une période plus longue, de 6 à 12 mois. Ces résidences permettent de vivre « comme en appartement », avec une cuisine, tout le nécessaire quotidien avec les services d’un hôtel en plus (personnel d’entretien par exemple). Les <i>serviced apartments</i> sont situés en plein centre ville. Leurs superficies s’étendent de 50 m2 pour un studio à 135 m2 en moyenne pour un appartement de trois chambres. Les tarifs varient de S$4000/mois pour un studio jusqu’à S$18 000/mois pour un appartement haut standing.</p>
<p>Plusieurs grands groupes hôteliers internationaux proposent ce type de logement. Vous trouverez la liste sur le site : <a href="http://www.expatsingapore.com/" class="spip_out" rel="external">Expatsingapore.com</a>.</p>
<h3 class="spip"><a id="sommaire_4"></a>Electricité</h3>
<p><strong>Voltage</strong></p>
<p>Le voltage de Singapour est 220-240 V AC 50 cycles par seconde.</p>
<p><strong>Types de prises</strong></p>
<p>Il vous faudra parfois changer les prises des appareils importés de France afin de les adapter aux prises singapouriennes. Sinon, vous trouverez des adaptateurs pour éviter de bricoler. Les prises sont sur le modèle anglais, avec <strong>trois broches </strong>disposées triangulairement pour les prises mâles et trois entrées formant un triangle pour les prises murales.</p>
<p>Les transformateurs ne sont pas nécessaires à Singapour car les voltages restent les mêmes que ceux de France.</p>
<h3 class="spip"><a id="sommaire_5"></a>Eau</h3>
<p><strong>Alimentation en eau froide/eau chaude</strong></p>
<p>Singapour est alimenté en eau potable par la Malaisie. La Cité-Etat possède en complément plusieurs réservoirs d’eau potable sur l’île. Il n’y a donc pas de problème d’alimentation en eau potable. Tous les appartements et maisons ont des arrivées d’eau chaude et d’eau froide.</p>
<p>Attention, la plupart du temps, il faut actionner un interrupteur pour que la production d’eau chaude soit enclenchée. Il faut aussi vérifier qu’il y ait une arrivée d’eau chaude dans votre cuisine, ce qui n’est pas systématique contrairement aux salles de bains qui en sont toutes équipées.</p>
<h3 class="spip"><a id="sommaire_6"></a>Electroménager</h3>
<p>Les cuisines sont généralement équipées de plaques de cuisson et four intégré, d’un réfrigérateur, de machine à laver et parfois d’un sèche-linge. Beaucoup de lave-linge locaux ne possèdent que des programmes pour laver à l’eau froide.</p>
<p>Peu de personnes disposent d’un lave-vaisselle et beaucoup de familles investissent dans un réfrigérateur à vins, compte tenu du climat du pays.</p>
<h4 class="spip">Chauffage / climatisation</h4>
<p>Tous les appartements en condominiums et la plupart des maisons possèdent la climatisation. Elle est recommandée pour le bien-être des habitants. De plus, elle permet d’éloigner les moustiques et permet aux habitations d’éviter un trop fort taux d’humidité en intérieur qui détériorerait cuirs, livres, documents papiers et appareils électroniques.</p>
<h4 class="spip">Equipement vidéo</h4>
<p>La vidéo (système PAL) et le DVD sont répandus et sont soumis à une censure officielle. Singapour possède un marché de l’électronique très performant. Un grand choix de lecteurs est disponible sur place.</p>
<p><strong>Attention</strong> : il est formellement interdit d’acheter ou d’importer à Singapour des vidéos piratées. Des spots publicitaires informant de cette interdiction et sensibilisant les citoyens singapouriens sont diffusés quotidiennement sur les chaînes nationales et au cinéma.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-singapour-vie-pratique-article-loisirs-et-culture.md" title="Loisirs et culture">Loisirs et culture</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-singapour-vie-pratique-article-communications.md" title="Communications">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-singapour-vie-pratique-article-transports.md" title="Transports">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-singapour-vie-pratique-article-cout-de-la-vie-110503.md" title="Coût de la vie">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-singapour-vie-pratique-article-scolarisation.md" title="Scolarisation">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-singapour-vie-pratique-article-logement.md" title="Logement">Logement</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/vie-pratique/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
