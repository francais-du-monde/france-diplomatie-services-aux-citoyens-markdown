# Communications

<h4 class="spip">Téléphone – Internet</h4>
<p>Les liaisons téléphoniques et postales fonctionnement normalement. L’accès à Internet ne pose pas de difficulté.</p>
<ul class="spip">
<li>Indicatifs depuis la France vers la Lituanie<br class="manualbr">00 370 + le numéro d’abonné</li>
<li>depuis la Lituanie vers la France<br class="manualbr">00 33 + le numéro d’abonné sans le "0" local (soit 9 chiffres)</li></ul>
<h4 class="spip">Téléphoner gratuitement par Internet </h4>
<p>Il est généralement très avantageux d’utiliser <strong>des logiciels de téléphonie sur IP</strong> (Skype, Google talk, Live messenger, Yahoo ! messenger, etc.) car les tarifs proposés à l’international sont souvent moindre que ceux des opérateurs locaux exploitant des lignes classiques, voire gratuits dans certaines conditions.</p>
<h4 class="spip">Poste</h4>
<p>Les liaisons postales fonctionnement normalement.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/vie-pratique/article/communications). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
