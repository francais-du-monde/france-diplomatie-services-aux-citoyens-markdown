# Entrée et séjour

<h2 class="rub23028">Passeport, visa, permis de travail</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/entree-et-sejour/#sommaire_1">Titre de séjour</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/entree-et-sejour/#sommaire_2">Permis de travail</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/entree-et-sejour/#sommaire_3">Démarches diverses</a></li></ul>
<p>Quel que soit le motif de votre séjour, renseignez-vous auprès de la section consulaire de l’ambassade du Portugal à Paris qui vous informera sur la règlementation en matière d’entrée et de séjour au Portugal, règlementation que vous devrez impérativement respecter. N’hésitez pas à consulter le site ministère de l’Intérieur portugais afin de vous familiariser avec les modalités d’entrée et de séjour dans le pays.</p>
<p><strong>En aucun cas, vous n’êtes autorisé à travailler au Portugal sans permis adéquat. </strong></p>
<p>Le consulat de France à Lisbonne n’est pas l’organisme compétent pour répondre à vos demandes d’information concernant votre séjour au Portugal.</p>
<h3 class="spip"><a id="sommaire_1"></a>Titre de séjour</h3>
<p>Le Portugal étant membre de l’Union européenne, les conditions de séjour et de travail sont celles prévues par la législation communautaire.</p>
<p>Tout ressortissant français qui a l’intention de séjourner <i>plus de trois mois et moins d’un an </i>au Portugal doit demander un titre de séjour provisoire, ou bien une carte de séjour s’il souhaite rester pour une <i>durée supérieure à un an</i>. La demande doit être faite dans les trois mois de son arrivée au <a href="http://www.sef.pt/" class="spip_out" rel="external">Service des étrangers et des frontières (<i>Serviço de Estrangeiros e Fronteiras</i>)</a> de son lieu de résidence :</p>
<p><strong>Lisbonne</strong></p>
<p>Avenida Antonio Augusto de Aguiar 20<br class="manualbr">1069-118 Lisboa<br class="manualbr">Tél. : (351) 21 358 55 00</p>
<p><strong>Porto</strong></p>
<p>Rua D. Joao IV 536<br class="manualbr">4013 Porto<br class="manualbr">Tél. : (351) 22 589 87 10</p>
<p>Il suffit de présenter la demande du titre de séjour, la carte d’identité ou le passeport, une photo d’identité ainsi que divers justificatifs selon votre statut économique (étudiant, salarié, demandeur d’emploi, retraité, indépendant, etc.). Si le résident est salarié, une déclaration de l’employeur indiquant la durée prévue de l’emploi pourra être demandée.</p>
<p>Pour plus de détails, il convient de consulter le site Internet de l’<a href="http://www.ambafrance-pt.org/" class="spip_out" rel="external">Ambassade de France au Portugal</a> qui détaille les documents à fournir en fonction de votre statut (salarié, retraité, étudiant)</p>
<h3 class="spip"><a id="sommaire_2"></a>Permis de travail</h3>
<p>Pour l’obtention du <strong>permis de travail</strong>, il est nécessaire de présenter une pièce d’identité en cours de validité et un contrat de travail (pour les salariés) ou des justificatifs de ressources. Les délais de délivrance sont longs, mais il est généralement permis de commencer à travailler avant d’avoir obtenu le permis.</p>
<h3 class="spip"><a id="sommaire_3"></a>Démarches diverses</h3>
<p>Il convient par ailleurs d’obtenir un <strong>numéro de Contribuinte</strong> (contribuable) lequel vous identifie auprès de l’administration fiscale et s’avère nécessaire pour entamer la plupart des démarches administratives.</p>
<p>Le dossier "S’installer" du site associatif <a href="http://www.livinginlisbon.com/dossiers/content.php?id=3" class="spip_out" rel="external">LivingInLisbon.com</a> vous apportera des conseils utiles.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.embaixada-portugal-fr.org/" class="spip_out" rel="external">Ambassade du Portugal en France</a> ;</li>
<li><a href="http://www.ambafrance-pt.org/" class="spip_out" rel="external">Ambassade de France au Portugal</a> ;</li>
<li><a href="http://www.sef.pt/" class="spip_out" rel="external">Service des étrangers et des frontières</a> ;</li>
<li>Notre article thématique <a href="services-aux-citoyens-preparer-son-expatriation-documents-de-voyage.md" class="spip_in">Documents de voyage</a>.</li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-entree-et-sejour-article-animaux-domestiques.md" title="Animaux domestiques">Animaux domestiques</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-entree-et-sejour-article-vaccination-111283.md" title="Vaccination">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-entree-et-sejour-article-demenagement-111282.md" title="Déménagement">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-entree-et-sejour-article-passeport-visa-permis-de-travail.md" title="Passeport, visa, permis de travail">Passeport, visa, permis de travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/entree-et-sejour/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
