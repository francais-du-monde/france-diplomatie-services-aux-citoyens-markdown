# Entretien d’embauche

<h4 class="spip">Conduite de l’entretien</h4>
<p>L’objectif du <i>job interview</i> est de convaincre que vous possédez les compétences et la motivation pour l’emploi auquel vous postulez. N’oubliez-pas de vous renseigner sur l’entreprise, de revoir vos points forts. Les entretiens sont souvent accompagnés de tests psycho-techniques (en fonction du secteur d’activité recherché).</p>
<p>Le site suivant pourra vous aider à préparer votre entretien (questions posées par l’employeur, etc) :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.careers.govt.nz/" class="spip_out" rel="external">Careers.govt.nz</a></p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/emploi-stage/article/entretien-d-embauche). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
