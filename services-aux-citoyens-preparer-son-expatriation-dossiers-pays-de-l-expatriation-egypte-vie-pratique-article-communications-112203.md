# Communications

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/vie-pratique/article/communications-112203#sommaire_1">Téléphone – Internet</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/vie-pratique/article/communications-112203#sommaire_2">Poste</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Téléphone – Internet</h3>
<p>Les liaisons téléphoniques sont bonnes mais le coût des communications vers l’étranger est élevé. L’installation d’une ligne internationale à domicile est nécessaire. Il existe également des possibilités d’abonnement à des standards privés qui servent de relais. Il est possible de téléphoner depuis les grands hôtels. Le téléphone mobile est répandu et la compatibilité entre les normes françaises et égyptiennes est désormais assurée.</p>
<p>Pour appeler depuis la France, composer l’indicatif du pays - 00 20 - puis l’indicatif du gouvernorat à un ou deux chiffres (2 pour Le Caire, 3 pour Alexandrie) suivi du numéro du correspondant.</p>
<p>Depuis l’Egypte, un appel vers la France est composé de l’indicatif 00 33 suivi du numéro du correspondant sans le 0.</p>
<p>En Egypte, pour téléphoner vers un autre gouvernorat, composer le 0 puis l’indicatif du gouvernorat suivi du numéro du correspondant ; pour un appel à l’intérieur d’un gouvernorat ou vers n’importe quel portable, faire directement le numéro du correspondant.</p>
<p>Plusieurs fournisseurs d’accès proposent des forfaits de connexion Internet à bon marché et de bonne qualité.</p>
<p><strong>Téléphoner gratuitement par Internet </strong></p>
<p>Il est généralement très avantageux d’utiliser <strong>des logiciels de téléphonie sur IP</strong> (Skype, Google talk, Live messenger, Yahoo ! messenger, etc.) car les tarifs proposés à l’international sont souvent moindre que ceux des opérateurs locaux exploitant des lignes classiques, voire gratuits dans certaines conditions.</p>
<h3 class="spip"><a id="sommaire_2"></a>Poste</h3>
<p>Les liaisons postales sont lentes et aléatoires. Les lettres et les paquets sont souvent ouverts et la réception n’est pas garantie. Il est préférable d’utiliser les services de courrier rapide plus chers mais plus sûrs. Les colis sont remis à leur destinataire après le paiement de taxes douanières qui peuvent être très élevées s’il s’agit de produits dont l’importation est réglementée.</p>
<p><i>Mise à jour : avril 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/vie-pratique/article/communications-112203). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
