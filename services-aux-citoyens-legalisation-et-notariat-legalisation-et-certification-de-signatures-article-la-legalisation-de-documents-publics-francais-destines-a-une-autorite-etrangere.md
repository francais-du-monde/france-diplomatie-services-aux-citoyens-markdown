# La légalisation de documents publics français destinés à une autorité étrangère

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/legalisation-et-notariat/legalisation-et-certification-de-signatures/article/la-legalisation-de-documents-publics-francais-destines-a-une-autorite-etrangere#sommaire_1">Définition, principe</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/legalisation-et-notariat/legalisation-et-certification-de-signatures/article/la-legalisation-de-documents-publics-francais-destines-a-une-autorite-etrangere#sommaire_2">Base juridique</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/legalisation-et-notariat/legalisation-et-certification-de-signatures/article/la-legalisation-de-documents-publics-francais-destines-a-une-autorite-etrangere#sommaire_3">Régime de légalisation selon le pays</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Définition, principe</h3>
<p>La légalisation est la formalité par laquelle est attestée la véracité de la signature, la qualité en laquelle le signataire de l’acte a agi et, le cas échéant, l’identité du sceau ou timbre dont cet acte est revêtu.</p>
<p><strong>La légalisation correspond exclusivement à une certification matérielle de signature et non pas à un certificat de conformité à la loi française</strong>.</p>
<p>Elle donne lieu à l’apposition d’un cachet spécifique sur tous <strong>les actes publics français </strong>destinés à être produits à l’étranger, sous réserve du régime juridique en vigueur entre la France et le pays destinataire. Le document légalisé par le ministère des affaires étrangères sera ensuite légalisé par l’ambassade ou le consulat de l’État étranger sur le territoire duquel l’acte doit produire ses effets.</p>
<p><strong>Important : </strong></p>
<p>Avant de présenter votre document à la légalisation, il est impératif de lire attentivement toutes les rubriques consacrées aux opérations de légalisation (autorités compétentes, conditions de recevabilité, coût, légalisation par correspondance, principaux cas particuliers, adresses et liens utiles) et de vous conformer notamment aux deux règles suivantes :</p>
<p><strong>Sur l’acte à légaliser doivent figurer, en plus de sa signature, le nom et la qualité du signataire de l’acte</strong>. Sans ces indications, l’acte ne pourra pas être légalisé par le Bureau des légalisations du ministère des Affaires étrangères. Il convient de <strong>vous assurer que l’autorité ayant délivré l’acte a bien porté le nom et la qualité du signataire sur le document</strong>. Vous êtes en droit d’exiger la présence de ces indications dans l’acte.</p>
<p>En cas de traduction, la signature du traducteur assermenté doit être préalablement authentifiée (légalisée) par une mairie, une chambre de commerce ou un notaire. Le Bureau des légalisations ne pourra légaliser la traduction sans cette authentification préalable de la signature du traducteur.</p>
<h3 class="spip"><a id="sommaire_2"></a>Base juridique</h3>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/01_-_Decret_no2001-899.pdf" class="spip_in" type="application/pdf">Décret n°2001-899 :abrogation des dispositions relatives à la certification conforme des copies de documents</a></li>
<li><a href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/02_-_Circulaire_pour_l_application_du_decret.pdf" class="spip_in" type="application/pdf">Circulaire du 01/10/01 pour l’application du décret n°2001-899</a></li>
<li><a href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/03_-_Decret_no2007-1205.pdf" class="spip_in" type="application/pdf">Décret n°2007-1205 du 10 août 2007 relatif aux attributions du ministère des affaires étrangères, des ambassadeurs et des chefs de poste consulaire en matière de légalisation d’actes</a></li>
<li><a href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/04_-_Arrete_du_3_septembre_2007.pdf" class="spip_in" type="application/pdf">Arrêté du 3 septembre 2007 relatif aux conditions d’application du décret n°2007-1205 du 10 août 2007</a></li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Régime de légalisation selon le pays</h3>
<p>Selon les différents accords internationaux conclus entre la France et le pays destinataire de votre document, la procédure peut revêtir plusieurs formes. L’acte public pourra :</p>
<ul class="spip">
<li>être légalisé par le ministère des Affaires étrangères ; ou,</li>
<li>recevoir l’apostille au titre de la convention de La Haye du 5 octobre 1961 ;</li>
<li>être dispensé de légalisation.</li></ul><a class="spip_in" title="Doc:Tableau récapitulatif de l'état actuel du droit conventionnel en matière de légalisation  , 418 ko, 0x0" href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/tableau_regime_lega_par_pays_-_pour_site_internet_-_oct_2015_cle0bb665.pdf"><img width="18" height="21" alt="Doc:Tableau récapitulatif de l’état actuel du droit conventionnel en matière de légalisation  , 418 ko, 0x0" src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L18xH21/pdf-7444b.png" style="vertical-align: middle;"></a>
<a href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/tableau_regime_lega_par_pays_-_pour_site_internet_-_oct_2015_cle0bb665.pdf">Tableau récapitulatif de l’état actuel du droit conventionnel en matière de légalisation  - (PDF, 418 ko)</a>
<p>Mise à jour : novembre 2015</p>
<p><i>Ce tableau concerne les documents établis par une autorité française qui doivent être présentés à l’étranger.</i></p>
<a class="spip_in" title="Doc:Etats parties à la Convention de La Haye du 5 octobre 1961  , 44.3 ko, 0x0" href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/liste_des_pays_convention_de_la_haye_-_pour_site_internet_-_oct_2015_cle83e83e.pdf"><img width="18" height="21" alt="Doc:Etats parties à la Convention de La Haye du 5 octobre 1961  , 44.3 ko, 0x0" src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L18xH21/pdf-7444b.png" style="vertical-align: middle;"></a>
<a href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/liste_des_pays_convention_de_la_haye_-_pour_site_internet_-_oct_2015_cle83e83e.pdf">Etats parties à la Convention de La Haye du 5 octobre 1961  - (PDF, 44.3 ko)</a>
<p>Mise à jour : novembre 2015</p>
<p class="document_doc">
<a class="spip_in" title="Doc:Etats parties à la Convention Européenne du 7 juin 1968 , 9.7 ko, 0x0" href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/ConvEUROPE_7juin1968_cle07e429.pdf"><img width="18" height="21" alt="Doc:Etats parties à la Convention Européenne du 7 juin 1968 , 9.7 ko, 0x0" src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L18xH21/pdf-7444b.png" style="vertical-align: middle;"></a>
<a href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/ConvEUROPE_7juin1968_cle07e429.pdf">Etats parties à la Convention Européenne du 7 juin 1968 - (PDF, 9.7 ko)</a>
</p>
<p class="document_doc">
<a class="spip_in" title="Doc:Conventions bilatérales , 58.1 ko, 0x0" href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/CONVBILATERALE3-2.pdf"><img width="18" height="21" alt="Doc:Conventions bilatérales , 58.1 ko, 0x0" src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L18xH21/pdf-7444b.png" style="vertical-align: middle;"></a>
<a href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/CONVBILATERALE3-2.pdf">Conventions bilatérales - (PDF, 58.1 ko)</a>
</p>
<p class="document_doc">
<a class="spip_in" title="Doc: Etats parties à la Convention CIEC , 40.9 ko, 0x0" href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/CONVCIEC4.pdf"><img width="18" height="21" alt="Doc: Etats parties à la Convention CIEC , 40.9 ko, 0x0" src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L18xH21/pdf-7444b.png" style="vertical-align: middle;"></a>
<a href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/CONVCIEC4.pdf"> Etats parties à la Convention CIEC - (PDF, 40.9 ko)</a>
</p>
<p class="document_doc">
<a class="spip_in" title="Doc:Etats parties à la Convention CIEC du 15 septembre 1977 , 142 ko, 0x0" href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/tableau_5_-_Conv_CIEC_15_sept_1977_-_pour_site_internet_-_sept_2014_cle049692.pdf"><img width="18" height="21" alt="Doc:Etats parties à la Convention CIEC du 15 septembre 1977 , 142 ko, 0x0" src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L18xH21/pdf-7444b.png" style="vertical-align: middle;"></a>
<a href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/tableau_5_-_Conv_CIEC_15_sept_1977_-_pour_site_internet_-_sept_2014_cle049692.pdf">Etats parties à la Convention CIEC du 15 septembre 1977 - (PDF, 142 ko)</a>
</p>
<p class="document_doc">
<a class="spip_in" title="Doc:Etats parties à la Convention des Communautés européennes du 25 mai 1987 , 9.3 ko, 0x0" href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/Tableau6CONV_ComEU1987_cle03cea2-1.pdf"><img width="18" height="21" alt="Doc:Etats parties à la Convention des Communautés européennes du 25 mai 1987 , 9.3 ko, 0x0" src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L18xH21/pdf-7444b.png" style="vertical-align: middle;"></a>
<a href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/Tableau6CONV_ComEU1987_cle03cea2-1.pdf">Etats parties à la Convention des Communautés européennes du 25 mai 1987 - (PDF, 9.3 ko)</a>
</p>
<p>Mise à jour : juillet 2015</p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/legalisation-et-notariat/legalisation-et-certification-de-signatures/article/la-legalisation-de-documents-publics-francais-destines-a-une-autorite-etrangere). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
