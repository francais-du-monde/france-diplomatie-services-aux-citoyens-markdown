# Scolarisation

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/vie-pratique/article/scolarisation#sommaire_1">Les établissements scolaires français au Danemark</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/vie-pratique/article/scolarisation#sommaire_2">Enseignement supérieur</a></li></ul>
<p>Pour toute information sur la scolarisation dans le système français à l’étranger, vous pouvez consulter notre thématique sur les <a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures.md" class="spip_in">Etudes supérieures</a> et la <a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais.md" class="spip_in">Scolarité en français</a>.</p>
<p>Vous y trouverez des renseignements sur :</p>
<ul class="spip">
<li>les établissements français du primaire et du secondaire à l’étranger ;</li>
<li>les bourses scolaires et la prise en charge des frais de scolarité à l’étranger ;</li>
<li>les possibilités qui s’offrent à vous si votre enfant ne peut être scolarisé à l’étranger dans le système français (enseignement à distance par le CNED, programme français langue maternelle (FLAM), internats en France) ;</li>
<li>les épreuves du baccalauréat à l’étranger ;</li>
<li>les bourses d’études supérieures en France et à l’étranger ;</li>
<li>l’équivalence des diplômes.</li></ul>
<p>Pour en savoir plus :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.aefe.fr/" class="spip_out" rel="external">Agence pour l’enseignement français à l’étranger</a></p>
<h3 class="spip"><a id="sommaire_1"></a>Les établissements scolaires français au Danemark</h3>
<p><a href="http://www.lfph.dk/" class="spip_out" rel="external">Lycee Prins Henrik</a><br class="manualbr"><strong>Cycle d´enseignement de la Maternelle à la Terminale</strong><br class="manualbr">Frederiksberg Allé 22 A <br class="manualbr">1820 Frederiksberg C<br class="manualbr">Tél. : 33 21 20 48<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/vie-pratique/article/scolarisation#lfph#mc#lfph.dk#" title="lfph..åt..lfph.dk" onclick="location.href=mc_lancerlien('lfph','lfph.dk'); return false;" class="spip_mail">Courriel</a></p>
<h3 class="spip"><a id="sommaire_2"></a>Enseignement supérieur</h3>
<h4 class="spip">Etudier au Danemark</h4>
<p>Nombreux sont les étudiants étrangers à vouloir se rendre au Danemark pour un temps défini, afin de compléter un parcours d’études spécifiques ou élargir leurs expériences internationales. Pour les aider, la Direction pour les universités et l’internationalisation (<i>Styrelsen for universiteter oginternationalisering</i>) est une formidable plateforme qui donne des informations essentielles sur le système danois d’éducation, les programmes d’échanges scolaires et universitaires, et aide les étudiants à concrétiser leur projet de séjour.</p>
<p>La Direction pour les universités et l’internationalisation est aussi un organisme qui propose aux étudiants de niveau avancé de bénéficier de bourses d’études et de recherche.</p>
<p>Ces pages permettent également d’accéder à l’information relative à <a href="http://frankrig.um.dk/fr/le-danemark/etudes2/apprendre-le-danois-en-france/" class="spip_out" rel="external">l’apprentissage du danois en France</a>.<br class="manualbr">Bredgade 43<br class="manualbr">1260 Copenhague K<br class="manualbr">Tél. : 33 95 12 00<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/vie-pratique/article/scolarisation#iu#mc#iu.dk#" title="iu..åt..iu.dk" onclick="location.href=mc_lancerlien('iu','iu.dk'); return false;" class="spip_mail">Courriel</a></p>
<h4 class="spip">Bourses d’études</h4>
<p>Pour toute demande de bourses, il convient de s’adresser directement à la Direction pour les universités et l’internationalisation (Styrelsen for Universiteter ogInternationalisering).</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://studyindenmark.dk/" class="spip_out" rel="external">Site gouvernemental sur l’enseignement supérieur danois</a> à l´usage des étudiants étrangers.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/vie-pratique/article/scolarisation). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
