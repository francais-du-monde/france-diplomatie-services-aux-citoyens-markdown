# Déménagement

<p>S’agissant d’un déménagement international, il est primordial de s’entourer d’un maximum de garanties en faisant appel à un professionnel disposant de certifications reconnues (marque NF Service, ISO 9002, etc.)</p>
<p><a href="http://www.csdemenagement.fr/" class="spip_out" rel="external">Chambre syndicale du déménagement</a> <br class="manualbr">Téléphone : 01 49 88 61 40 <br class="manualbr">Télécopie : 01 49 88 61 46<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/paraguay/entree-et-sejour/article/demenagement#contact#mc#csdemenagement.fr#" title="contact..åt..csdemenagement.fr" onclick="location.href=mc_lancerlien('contact','csdemenagement.fr'); return false;" class="spip_mail">Courriel</a></p>
<h4 class="spip">Par bateau </h4>
<p>Via Montevideo (Uruguay) ou Buenos Aires (Argentine) : il est préférable de choisir un déménagement de porte à porte. Plusieurs entreprises locales peuvent assurer un service convenable : Benkovics, Servimex et Van Pack.</p>
<p>Le délai d’acheminement par bateau est de six à dix semaines.</p>
<h4 class="spip">Par avion</h4>
<p>Via Rio ou São Paulo (Brésil), Buenos Aires (Argentine) ou Miami (Etats-Unis). Sur le parcours via Miami, les connaissements aériens ne sont pas toujours respectés, les compagnies devant attendre qu’un avion fret soit plein avant de le faire partir.</p>
<p>Le délai d’acheminement par avion est de huit à dix jours.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="services-aux-citoyens-preparer-son-expatriation-douanes.md" class="spip_in">Douanes</a></p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/paraguay/entree-et-sejour/article/demenagement). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
