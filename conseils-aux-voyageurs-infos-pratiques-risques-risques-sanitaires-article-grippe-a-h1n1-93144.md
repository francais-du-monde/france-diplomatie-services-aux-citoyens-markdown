# Grippe A (H1N1)

<p><strong>Les premiers cas de grippe A/H1N1 ont été rapportés par l’Organisation Mondiale de la Santé (OMS) en avril 2009, débouchant sur une pandémie qui a touché le monde entier pendant l’hiver 2009-2010. Quelques cas ont resurgi à l’hiver 2010-2011.</strong></p>
<p>Les syndromes sont ceux d’une grippe saisonnière : fièvre, toux, écoulement nasal, douleurs articulaires et/ou musculaires.</p>
<p>Avant un séjour à l’étranger, il est recommandé :</p>
<ul class="spip">
<li>de prendre contact avec son médecin traitant afin de vérifier les mesures de prévention sanitaires et les vaccinations nécessaires pour votre voyage ;</li>
<li>de s’informer sur la situation sanitaire locale en consultant les sites du <a href="http://www.pandemie-grippale.gouv.fr/" class="spip_out" rel="external">ministère de la Santé</a> et de l’<a href="http://www.who.int" class="spip_out" rel="external">Organisation Mondiale de la Santé</a>.</li></ul>
<p>Pendant le voyage, en cas de présence du virus dans le pays de séjour, il est conseillé :</p>
<ul class="spip">
<li>de se conformer aux recommandations émises par les autorités sanitaires locales, notamment :</li>
<li>d’éviter les lieux de rassemblement public ;</li>
<li>de respecter les règles d’hygiène élémentaire (lavage soigneux et régulier des mains avec de l’eau savonneuse ou des solutions hydro-alcooliques et aération des pièces de vie notamment) ;</li>
<li>de porter une attention toute particulière aux personnes les plus vulnérables (les enfants et personnes âgées ou fragiles en particulier) ;</li>
<li>d’éviter le contact avec les personnes malades ;</li>
<li>de consulter un médecin en cas de fièvre ou de symptômes grippaux (courbatures, douleurs musculaires notamment) ;</li>
<li>de se tenir informé de la situation sanitaire locale.</li></ul>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  de contacter le poste diplomatique en cas de difficulté à obtenir les soins ou le traitement prescrit par un médecin local ou par le médecin conseil de l’ambassade. </p>
<p>En cas de syndrome grippal dans les sept jours suivant le retour, contactez votre médecin traitant ou le Centre 15 de votre département (Tél. : 15).</p>
<p><strong> <i>Document réalisé en partenariat avec la Direction Générale de la Santé.</i> </strong></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/infos-pratiques/risques/risques-sanitaires/article/grippe-a-h1n1-93144). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
