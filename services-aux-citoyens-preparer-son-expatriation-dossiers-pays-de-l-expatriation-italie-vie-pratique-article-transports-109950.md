# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/vie-pratique/article/transports-109950#sommaire_1">Importation de véhicule</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/vie-pratique/article/transports-109950#sommaire_2">Permis de conduire</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/vie-pratique/article/transports-109950#sommaire_3">Code de la route</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/vie-pratique/article/transports-109950#sommaire_4">Assurances et taxes</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/vie-pratique/article/transports-109950#sommaire_5">Achat et location</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/vie-pratique/article/transports-109950#sommaire_6">Immatriculation</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/vie-pratique/article/transports-109950#sommaire_7">Réseau routier</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/vie-pratique/article/transports-109950#sommaire_8">Transports en commun</a></li></ul>
<p>En Italie, il est impossible d’assurer les véhicules en plaques étrangères. Pour son importation, en l’absence de plaques et/ou d’assurance, le véhicule devra être acheminé à l’aide d’un camion afin d’effectuer les démarches nécessaires auprès de la Douane compétente et aux Mines (Motorizzazione civile).</p>
<h3 class="spip"><a id="sommaire_1"></a>Importation de véhicule</h3>
<p>L’importation d’un véhicule en provenance d’un des pays de l’Union européenne ne pose pas de problème. Même pour un véhicule provenant d’un pays de l’UE, il est nécessaire de contacter le "Motorizzazione civile", équivalent du service des Mines français, pour l’homologation du véhicule. <strong>Au delà de six mois de résidence, le véhicule doit être immatriculé en Italie</strong>. Les démarches à effectuer sont longues.</p>
<p>Il convient de fournir le certificat de résidence, le code fiscal, le certificat d’immatriculation original (carte grise), le certificat de conformité délivré par le fabricant, la déclaration des caractéristiques techniques - à demander au fabricant - et les timbres fiscaux. Ces documents doivent être accompagnés de leur traduction certifiée conforme en italien.</p>
<p>Afin de simplifier ces démarches, il est conseillé de s’adresser à une agence ACI (Automobile Club Italia) qui s’occupera également de l’inscription au PRA (Pubblico Registro Automobilistico, coût entre 500 et 800 €).</p>
<p>Si votre véhicule provient d’un pays hors UE, il convient de prévoir en plus les droits de douane (environ 30% de la valeur indiquée par l’argus).</p>
<p>Il n’est pas conseillé d’importer un véhicule depuis la France, les prix en Italie, pour des voitures neuves comme pour les occasions, étant généralement inférieurs aux prix français.</p>
<p>Les véhicules achetés en Italie <strong>avec exemption d’IVA</strong> peuvent être revendus, sur place, six mois au moins après l’achat, sans avoir à payer l’IVA. Les frais de réimmatriculation en plaques ordinaires, à la charge du vendeur, s’élèvent à environ 1 000 € et les frais de passage de propriété, à la charge de l’acheteur, à environ 600 €.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li>Bureau de Motorizzazione civile :
<br>— à <a href="http://www.motorizzazioneroma.it/" class="spip_out" rel="external">Rome</a>
<br>— à <a href="http://www.motorizzazionetorino.it/" class="spip_out" rel="external">Turin</a></li>
<li><a href="http://www.aci.it/" class="spip_out" rel="external">Automobile Club Italie</a></li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Permis de conduire</h3>
<p>Un permis de conduire, conforme au modèle européen, est valable en Italie en application de la directive communautaire n°91/439/CEE du 29 juillet 1991 et de la circulaire n°31 du 28 mai 1999 du ministère italien des Transports. <strong>Il n’est donc pas obligatoire de convertir son permis français</strong>. Toutefois, une reconnaissance (<i>procedura di riconoscimento</i>) du permis auprès de la préfecture peut être utile en cas de vol ou de perte de document en Italie.</p>
<p>Si le permis n’est pas conforme au modèle européen ou s’il a déjà été changé, il fera l’objet d’une conversion conformément au décret d’application. Le délai pour accomplir ces formalités est d’une année après l’installation en Italie. Pour plus d’informations, consulter la Motorizazzione du lieu de résidence.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li>Site de l’Ambassade de France en Italie : <a href="http://www.ambafrance-it.org/spip.php?article398" class="spip_out" rel="external">Vivre en Italie</a></li>
<li><a href="http://www.motorizzazioneroma.it/" class="spip_out" rel="external">Motorizzazione (Rome)</a></li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Code de la route</h3>
<p>La conduite et la priorité sont à droite. La vitesse est limitée à 50 km/h en agglomération, 90 km/h sur route et 110 à 130 km/h sur autoroute. Le permis à points (20 points) a été instauré en juillet 2003 en Italie.</p>
<p>La circulation est réglementée dans les centres historiques de certaines villes dont Rome, Milan et Florence ; il faut alors un permis <i>(contrassegno</i>) pour y accéder. Elle peut être interdite ou limitée certains jours pour limiter la pollution.</p>
<p>Le port de la ceinture de sécurité est obligatoire à l’avant et à l’arrière. Le taux d’alcoolémie maximum autorisé est de 0,5 g/l. L’usage du téléphone portable est interdit au volant.</p>
<p>Les feux de croisement sont obligatoires pour tous les véhicules, de jour comme de nuit, sur les autoroutes ainsi que sur les routes principales.</p>
<p>Le port du casque est obligatoire pour un deux-roues. Le conducteur doit être majeur pour transporter un passager. Les feux de croisement sont obligatoires pour les cyclomoteurs et motocyclettes sur tout le réseau routier et autoroutier ainsi que dans les villes, de jour comme de nuit.</p>
<p>Un gilet de sécurité fluorescent homologué est obligatoire à bord du véhicule. Vous devez également posséder un triangle de signalisation.</p>
<p>Les sanctions encourues en cas d’infraction sont les mêmes qu’en France : amendes (parfois élevées), retrait du permis de conduire. L’impossibilité de payer une amende en cas d’infraction peut entraîner le retrait immédiat du permis de conduire.</p>
<p>La conduite à Rome et dans le sud nécessite un certain temps d’adaptation.</p>
<h3 class="spip"><a id="sommaire_4"></a>Assurances et taxes</h3>
<p>Assez semblables aux assurances françaises, les assurances en Italie sont beaucoup plus onéreuses pour certaines, c’est le cas de l’assurance automobile. L’assurance "responsabilité civile automobile" est obligatoire. L’assurance personnelle de garantie pour le conducteur n’est pas prévue dans la police automobile de base. Il faut donc la solliciter.</p>
<p>Les prix des assurances ont été libérés le 1er juillet 1994 et augmentent depuis d’environ 5% par an. Il n’existe pas, pour le moment, de grilles tarifaires aussi détaillées que les grilles des compagnies françaises.</p>
<p>Le montant de la prime dépend du bonus/malus de l’assuré, du type et de l’âge du véhicule. Elle est plus élevée (de l’ordre de 50%) qu’en France. En Italie, il existe 18 classes de mérite. La classe d’entrée est la classe de mérite 14. Sur justificatif d’un bonus de votre précédant assureur, l’assureur italien vous appliquera la classe de mérite équivalente.</p>
<p>Le coût de l’assurance dépend aussi de la ville de résidence du conducteur. A Naples, par exemple, le prix d’une assurance contre le vol est prohibitif.</p>
<h3 class="spip"><a id="sommaire_5"></a>Achat et location</h3>
<h4 class="spip">Achat</h4>
<p>Toutes les marques de voitures, françaises et étrangères sont représentées en Italie, à des prix plus ou moins identiques à ceux pratiqués en France. Lors de l’achat d’un véhicule d’occasion, il convient de prévoir les frais de transfert de propriété (entre 350 et 750€, suivant la puissance du véhicule).</p>
<h3 class="spip"><a id="sommaire_6"></a>Immatriculation</h3>
<p>Le coût de la vignette (appelée <i>"Tassa di proprietà"</i>) varie en fonction de la cylindrée, de l’année du véhicule et du type du carburant (la vignette d’un véhicule diesel est nettement plus chère). Elle s’acquitte auprès de l’Automobile club d’Italie (A.C.I.) ou de certaines banques. Le paiement est annuel ou peut être fractionné pour certaines catégories de véhicule.</p>
<p>Des contrôles techniques doivent avoir lieu quatre ans après l’immatriculation pour un véhicule neuf puis tous les deux ans. Les contrôles de l’émission des gaz d’échappement sont annuels. Ces contrôles s’effectuent chez la plupart des garagistes.</p>
<p>Lorsque les démarches d’importation (voir rubrique précédente) sont terminées, de nouvelles plaques d’immatriculation sont obtenues auprès de la Motorizzazione ou de l’agence, ainsi que le livret de circulation et le certificat de propriété, ces deux derniers documents tenant lieu de carte grise.</p>
<h3 class="spip"><a id="sommaire_7"></a>Réseau routier</h3>
<p>L’état général du réseau routier est bon. Cependant l’étroitesse des voies et les méandres sur certaines autoroutes (Salerne-Reggio de Calabre) peuvent les rendre dangereuses en cas de vitesse excessive. Les routes secondaires dont l’entretien n’est pas parfaitement assuré peuvent réserver des surprises.</p>
<p>Le réseau autoroutier permet de se rendre dans chaque ville importante facilement, y compris en Sicile. Les temps de trajet sont cependant considérables sauf pour les liaisons avec la capitale et les métropoles du nord.</p>
<h3 class="spip"><a id="sommaire_8"></a>Transports en commun</h3>
<p>Il est possible de se déplacer à l’intérieur du pays en <a href="http://www.bus.it/" class="spip_out" rel="external">autocar</a>.</p>
<h4 class="spip">Transport maritime</h4>
<p>Grâce à un <a href="http://www.traghetti.com/CompagnieMarittime.html" class="spip_out" rel="external">réseau maritime étoffé</a>, l’accès aux îles (Sicile, Sardaigne, îles de la baie de Naples, îles Eoliennes), à la Grèce et l’Afrique du Nord est aisé. Les principales villes portuaires offrent un accès vers ces destinations :</p>
<ul class="spip">
<li>au départ de Naples : les îles de la baie, les îles Eoliennes, la Sicile, la Sardaigne, la Tunisie ;</li>
<li>au départ de Palerme : le nord de l’Italie, les îles avoisinantes, l’Afrique du Nord ;</li>
<li>au départ de Salerne : la Sicile, la Tunisie ;</li>
<li>au départ de Milazzo (près de Messine) : les îles Eoliennes ;</li>
<li>au départ de Marsala, Trapani, Agrigente : les îles de Lampedusa et Pantelleria, la Tunisie ;</li>
<li>au départ de Bari, Brindisi : l’Albanie, la Grèce et la Turquie ;</li>
<li>au départ de Livourne : la Corse, la Sardaigne ;</li>
<li>au départ de Civitavecchia : la Sardaigne.</li></ul>
<h4 class="spip">Transport aérien</h4>
<p>Le <a href="http://www.aeroporti.com/aeroporti.html" class="spip_out" rel="external">réseau</a> est également très étendu. Les aéroports de Rome et de Naples assurent des vols quotidiens vers les capitales européennes ainsi que des vols intérieurs. Les aéroports de Palerme et de Catane desservent l’Italie et reçoivent également de nombreux charters internationaux.</p>
<h4 class="spip">Transport ferroviaire</h4>
<p>Pour se déplacer en Italie, on peut utiliser la formule <a href="http://www.interrailnet.com/" class="spip_out" rel="external">InterRail</a>. Le <i>One Country Pass</i> permet par exemple de choisir trois, quatre, six ou huit jours de libre circulation dans un mois.</p>
<ul class="spip">
<li><a href="http://www.ferroviedellostato.it/" class="spip_out" rel="external">Chemins de fer italiens</a> ou <a href="http://www.trenitalia.it/" class="spip_out" rel="external">Trenitalia.it</a></li>
<li><a href="http://www.atac.roma.it/" class="spip_out" rel="external">Plan des réseaux ferroviaires de banlieue (Rome)</a>.</li></ul>
<h4 class="spip">Transports urbains</h4>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Permis d’accès au centre historique de Rome</strong><br> <br>Le centre historique de Rome est une « ZTL » (zone à trafic limité). Il est fermé à la circulation du lundi au vendredi, entre 6h30 et 18h00 et le samedi entre 14h00 et 18h00. Des signaux lumineux à l’entrée de la ZTL indiquant <i>Varco attivo</i> indiquent la limitation de circulation. Ces horaires peuvent être modifiés par la commune, il est donc souhaitable de les vérifier avant d’utiliser votre véhicule.</td></tr>
</tbody>
</table>
<p>La société chargée des transports en commun à Rome est l’<a href="http://www.atac.roma.it/" class="spip_out" rel="external">ATAC</a>.</p>
<p>Les renseignements sur les tarifs et les trajets peuvent être obtenus sur le site <a href="http://www.agenziamobilita.roma.it/" class="spip_out" rel="external">Agenziamobilita.roma.it</a></p>
<h5 class="spip">Taxis</h5>
<p>Numéros :</p>
<ul class="spip">
<li>06 3570</li>
<li>06 88177</li>
<li>06 5551</li>
<li>06 4994</li>
<li>06 6645</li></ul>
<p>Les taxis à Rome sont blancs et affichent sur le toit l’inscription TAXI, éviter les chauffeurs abusifs qui attendent les passagers aux sorties de gare et aéroport.</p>
<p><strong>A Milan</strong>, le <a href="http://www.atm-mi.it/" class="spip_out" rel="external">métro</a> est composé de trois lignes.</p>
<p>Depuis janvier 2008, Milan a mis en place dans le centre, du lundi au vendredi de 7h30 à 19h30 un péage urbain : l’ "Ecopass". Les véhicules à essence répondant aux normes Euro 3 et 4 sont exemptés. Pour les véhicules diesel, seuls ceux répondant aux normes Euro 4 FAP sont exemptés de péage. Les véhicules étrangers y sont assujettis et il faut donc se procurer une carte dans un kiosque à journaux et l’activer en obtenant le code confidentiel par l’intermédiaire d’un numéro vert.</p>
<p><strong>A Turin</strong>, le <a href="http://www.comune.torino.it/gtt/" class="spip_out" rel="external">réseau de transports urbains</a> est de bonne qualité.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/vie-pratique/article/transports-109950). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
