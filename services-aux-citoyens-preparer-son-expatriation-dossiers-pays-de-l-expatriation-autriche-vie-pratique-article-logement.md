# Logement

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/vie-pratique/article/logement#sommaire_1">Où se loger ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/vie-pratique/article/logement#sommaire_2">Conditions de location</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/vie-pratique/article/logement#sommaire_3">Electricité</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/vie-pratique/article/logement#sommaire_4">Electroménager</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Où se loger ?</h3>
<p>Les quartiers résidentiels se situent en centre-ville, à l’intérieur du Ring (1er arrondissement) dans les quartiers proches de la vieille ville (3ème, 4ème, 7ème, 8ème, 9ème) et dans les 13ème, 18ème, 19ème arrondissements de Vienne. A noter : le lycée français de Vienne est situé dans le 9ème arrondissement. De fait, une partie importante de la communauté française y réside.</p>
<p>Sur le site <a href="http://www.wohnnet.at/" class="spip_out" rel="external">Wohnnet.at</a>, vous pouvez consultez en détail les prix actualisés selon les arrondissements.</p>
<h4 class="spip">Colocation</h4>
<p>Pour les étudiants ou les personnes effectuant un court séjour en Autriche, il est aussi possible de trouver une collocation (<i>WG : Wohngemeinschaft</i>) sur internet. Les offres à Vienne sont nombreuses. Il est souvent plus facile de trouver un logement une fois sur place.</p>
<ul class="spip">
<li><a href="http://www.jobwohnen.at/" class="spip_out" rel="external">Jobwohnen.at</a></li>
<li><a href="http://www.wg-gesucht.de/" class="spip_out" rel="external">Wg-gesucht.de</a></li></ul>
<h4 class="spip">Auberges de jeunesse </h4>
<p>Il est conseillé de se procurer la carte internationale des auberges de jeunesse. Les personnes, qui en seraient démunies, seront dans l’obligation de s’acquitter partout d’un surcoût d’environ 3€.</p>
<p><a href="http://www.oejhv.or.at/" class="spip_out" rel="external">Österreichischer Jugendherbergsverband / Austrian Youth Hostel Association</a> <br class="manualbr">Gonzagagasse 221010 Wien<br class="manualbr">Tel. : 01/533 53 53<br class="manualbr">Fax : 01/533 53 53 50 <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/vie-pratique/article/logement#office#mc#oejhv.at#" title="office..åt..oejhv.at" onclick="location.href=mc_lancerlien('office','oejhv.at'); return false;" class="spip_mail">Courriel</a></p>
<p>Il est possible de réserver à l’avance des chambres chez l’habitant en contactant la centrale de réservation :</p>
<p><strong>Mitwohnzentrale</strong><br class="manualbr">Laudongasse, 7<br class="manualbr">1080 Wien<br class="manualbr">Tel. : (0043) (1) 402 60 61<br class="manualbr">Fax. : (0043) (1) 402 60 62</p>
<p>De plus, l’office du tourisme de Vienne offre des possibilités de réserver des chambres à l’avance par <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/vie-pratique/article/logement#rooms#mc#info.wien.at#" title="rooms..åt..info.wien.at" onclick="location.href=mc_lancerlien('rooms','info.wien.at'); return false;" class="spip_mail">courriel</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Conditions de location</h3>
<p>Les prix de location d’appartements dans la ville de Vienne, en général très disparates, ne sont pas seulement fonction de la situation géographique dans la ville, du quartier avoisinant et de l’état de l’immeuble, mais aussi de la réglementation qui s’applique au bien en question. Le marché immobilier viennois s’examine donc plutôt au regard de son cadre légal, qui présente un grand nombre de particularités.</p>
<p>Afin de définir la situation réglementaire d’un immeuble, il convient tout d’abord de distinguer entre les immeubles dont le permis de construire date d’avant 1945 et ceux construits après cette date. Pour la première catégorie d’immeubles, la réglementation applicable est de manière générale très favorable aux locataires. Cette situation est particulièrement prononcée pour les appartements ou bureaux qui n’ont pas fait l’objet d’un nouveau bail conclu après le 1er mars 1994 (date à laquelle une nouvelle loi dite “ sur les valeurs de référence ” est entrée en vigueur) et qui se caractérisent par un loyer extrêmement bas, une durée indéterminée du contrat et par un droit de reprise du bail par la proche famille. Malgré des aménagements, les immeubles construits avant 1945 restent toujours soumis à un cadre réglementaire relativement plus favorable aux locataires que les immeubles plus récents pour lesquels le loyer peut être fixé selon accord mutuel sans restriction légale.</p>
<p>Les modifications réglementaires intervenues depuis 1994 avaient pour but d’établir un juste équilibre entre les différents intérêts en jeu. Ainsi, pour les baux conclus après le 1er mars 1994 et concernant les appartements dont les équipements (notamment sanitaires) sont aux normes actuelles, un “ loyer équitable ” défini selon la loi peut être exigé.</p>
<p>D’une manière générale et à première vue, les prix de location à Vienne sont inférieurs à ceux pratiqués dans d’autres capitales européennes, notamment à Paris. Les particularités du marché de location autrichien peuvent cependant avoir pour effet d’alourdir considérablement les charges, surtout s’il s’agit d’un loyer à durée déterminée. Ainsi, il convient d’ajouter au loyer les frais d’entretien de l’immeuble (approvisionnement en eau, enlèvement des ordures ménagères, service de nettoiement de l’immeuble, etc.), que le locataire doit verser, en sus du loyer, au gestionnaire de l’immeuble. A cela s’ajoutent les frais pour le chauffage et l’électricité de l’appartement qui sont, dans la plupart des cas, à la charge du locataire.</p>
<p>De plus, lors de la conclusion d’un bail, il convient d’ajouter :</p>
<ul class="spip">
<li>La commission de l’agence immobilière, qui peut s’élever, pour un contrat à durée indéterminée, à trois mois de loyer, auxquels s’ajoutent les frais d’entretien et la TVA de 20%. Pour un contrat d’une durée inférieure à deux ans, la commission peut s’élever à un loyer mensuel ; pour un contrat d’une durée entre deux et trois ans, elle peut s’élever à deux loyers mensuels, auxquels s’ajoutent les frais d’entretien et la TVA de 20%.</li>
<li>Une taxe pour la conclusion du bail, qui s’élève, pour les contrats à durée déterminée (jusqu’à trois ans) à 1% du total du loyer et, pour les contrats à durée indéterminée, à 1% des trois années de loyer.</li>
<li>Une caution qui s’élève dans la plupart des cas à trois loyers mensuels (mais qui est en principe restituée à la fin du contrat).</li>
<li>Des frais divers (téléphone, gaz, électricité…).</li></ul>
<p><strong>Au total, l’entrée dans un logement de location constitue, dans la plupart des cas, une dépense conséquente pour le locataire. </strong></p>
<p>A Vienne, la quasi-totalité des transactions immobilières passe par les agences immobilières. Les prix moyens des locations, publiés dans une brochure intitulée <i>Immobilienpreisspiegel</i>, éditée par la Chambre économique fédérale, sont donc bien représentatifs des loyers actuellement en vigueur. Il faut cependant mentionner que pour les locataires disposant d’anciens contrats de location, les loyers sont beaucoup plus modérés et ne peuvent pas être relevés aux niveaux actuels.</p>
<p>Principales difficultés à signaler : la recherche d’un logement devient très difficile pour des familles de plus de deux enfants compte tenu de la rareté des logements de plus de quatre pièces et de leur coût.</p>
<p>Il est conseillé de consulter le site Internet <a href="http://www.wohnservice-wien.at/" class="spip_out" rel="external">Wohnservice-wien.at</a> pour la recherche sur Vienne.</p>
<p>Il est recommandé de souscrire une assurance responsabilité civile <i>Haftpflichtversicherung</i> assortie de l’assistance juridique <i>Rechtsschutzversicherung</i>. Une assurance habitation <i>Haushaltsversicherungen</i> est dépendante de la valeur du mobilier.</p>
<p>Pour chercher un appartement, vous pouvez consulter les journaux :</p>
<ul class="spip">
<li><a href="http://www.kurier.at/" class="spip_out" rel="external">Kurier.at</a></li>
<li><a href="http://www.krone.at/" class="spip_out" rel="external">Krone.at</a></li>
<li><a href="http://www.derstandard.at/" class="spip_out" rel="external">Derstandard.at</a></li>
<li><a href="http://www.diepresse.at/" class="spip_out" rel="external">Diepresse.at</a></li></ul>
<p>Pour en savoir plus : <a href="http://www.ambafrance-at.org/spip.php?rubrique621" class="spip_out" rel="external">site de l’ambassade de France à Vienne</a></p>
<h3 class="spip"><a id="sommaire_3"></a>Electricité</h3>
<p>Le courant électrique est livré sous une tension de 220 volts, à une fréquence de 50 Hz. Les prises de courant sont de type européen.</p>
<h3 class="spip"><a id="sommaire_4"></a>Electroménager</h3>
<p>Les cuisines sont le plus souvent équipées (plaques de cuisson et four, machine à laver le linge, machine à laver la vaisselle, réfrigérateur et congélateur).</p>
<p>On trouve en Autriche les mêmes équipements qu’en France. Attention, toutefois, aux téléviseurs ou lecteurs DVD achetés localement qui ne reçoivent pas les standards SECAM L.</p>
<p><i>Dernière mise à jour : 10/01/2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/vie-pratique/article/logement). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
