# Contrefaçon

<p>L’achat de produits contrefaits est sanctionné en France (délit pénal) comme dans les pays de l’Union Européenne et de nombreux pays étrangers.<br class="autobr"><strong>Le ministère des Affaires étrangères et du Développement international déconseille formellement l’achat de produits contrefaits lors de séjours à l’étranger.</strong></p>
<p>Une marchandise contrefaite présente les caractéristiques suivantes :</p>
<ul class="spip">
<li>elle est généralement liée au crime organisé et aux réseaux de travail clandestin et d’immigration illégale ;</li>
<li>elle nuit au développement durable en marginalisant les produits artisanaux locaux au profit de copies importées ;</li>
<li>elle est fréquemment liée à l’exploitation humaine (absence de législation du travail, travail forcé, travail des enfants) ;</li></ul>
<p>Les produits contrefaits échappent à toute norme de sécurité et à tout contrôle de qualité. Ils peuvent s’avérer dangereux pour la santé (risques d’allergies ou d’intoxication) et la sécurité, ce qui est notamment le cas des contrefaçons de médicaments, de boissons, de parfums et cosmétiques, de pièces automobiles et de jouets pour enfants.</p>
<h5 class="spip"> <strong>Au plan juridique</strong> </h5>
<p>La présence de produits contrefaits sur des marchés ou dans des boutiques peut vous donner l’impression que ces produits sont tolérés voire acceptés. Ce n’est généralement pas le cas et vous pouvez encourir des sanctions pénales en achetant ces produits de bonne foi.</p>
<p>Des contrôles réguliers sont effectués aux ports et aéroports internationaux d’arrivée en France. Si vous détenez des contrefaçons, deux types de procédures peuvent être engagés contre vous (les sanctions correspondantes pouvant être cumulatives, si le nombre de produits détenus est important) :</p>
<ul class="spip">
<li>Procédure douanière : les marchandises contrefaisantes sont saisies puis détruites. L’amende encourue est fonction du nombre d’objets détenus et calculée (selon la valeur authentique) des produits saisis. A noter que l’intervention de la douane est possible au premier objet de contrefaçon, même s’il s’agit d’un objet porté. Il appartient au voyageur de justifier de l’origine licite de ce qu’il transporte ;</li>
<li>Procédure pénale : dans les mêmes circonstances que ci-dessus, la peine encourue est de 3 ans d’emprisonnement et 300 000 euros d’amende, soit une sanction similaire à celle retenue pour le vol. La contrefaçon est donc un délit justifiant, si nécessaire, la garde à vue de la personne impliquée, la possibilité d’utiliser à son encontre les modes de comparution rapides devant la justice et, en cas de condamnation, l’inscription de celle-ci au casier judiciaire.</li></ul>
<p><strong>Pour en savoir plus</strong></p>
<ul class="spip">
<li><a href="http://www.unifab.com" class="spip_out" rel="external">Site Internet de l’Union des Fabricants</a></li>
<li><a href="http://www.contrefacon-danger.com" class="spip_out" rel="external">Site Internet du comité national anti-contrefaçon</a></li>
<li><a href="http://www.comitecolbert.com" class="spip_out" rel="external">Site Internet du Comité Colbert</a></li></ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/infos-pratiques/legislation/contrefacon-20984/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
