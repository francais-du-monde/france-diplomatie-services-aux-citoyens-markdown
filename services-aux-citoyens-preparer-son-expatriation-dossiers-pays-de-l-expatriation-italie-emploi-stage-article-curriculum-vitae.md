# Curriculum vitae

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/emploi-stage/article/curriculum-vitae#sommaire_1">Rédaction</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/emploi-stage/article/curriculum-vitae#sommaire_2">Diplômes</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/emploi-stage/article/curriculum-vitae#sommaire_3">Modèles de CV</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Rédaction</h3>
<p>Le CV italien diffère peu du CV français.</p>
<p>Le recruteur italien ayant besoin de savoir rapidement s’il peut vous convoquer pour passer un entretien ou pas, en fonction des critères qu’il s’est fixés, il convient d’éviter toute longueur ou originalité.</p>
<p>En Italie, les candidats envoient généralement deux CV : le premier, relativement succinct, a pour but de laisser à l’employeur la liberté de statuer rapidement. Le second CV, plus détaillé, est demandé par l’employeur au candidat, s’il est intéressé par le profil de ce dernier.</p>
<p>Les Italiens attachent assez peu d’importance à la rédaction de leur CV, privilégiant davantage le "parlé" sur l"écrit". Un employeur préfèrera rencontrer un candidat pour cerner sa personnalité et ses compétences.</p>
<p>Le candidat adressera un CV qui doit être dactylographié, signé, sans photographie. Il ne convient pas de joindre des lettres de références ou de recommandations. Celles-ci seront plutôt fournies lors de l’entretien. Le CV peut occuper deux pages.</p>
<p>Le CV comprendra les rubriques suivantes :</p>
<ul class="spip">
<li><strong>L’état civil (dati personali) </strong> : le candidat indique simplement son nom, adresse et ses coordonnées téléphoniques, adresse e-Mail et nationalité ;</li>
<li><strong>La formation</strong><strong> (studi e formazione) </strong> : le candidat présente par ordre chronologique sa formation. Il commence à partir du baccalauréat ou BEP/CAP, il mentionne les diplômes et certificats obtenus, les années d’obtention, le nom des écoles, etc ;</li>
<li><strong>L’expérience professionnelle (esperienze professionali) </strong> : le candidat mentionne selon un ordre chronologique, les postes occupés, le nom et la localisation de ses précédents employeurs et la durée du contrat. Il n’est pas nécessaire de décrire les missions confiées, elles seront développées lors de l’entretien plus approfondi avec l’employeur ;</li>
<li><strong>Les langues étrangères (lingue) </strong> : le candidat mentionne les langues étrangères qu’il pratique, y compris le français, et le niveau qu’il détient dans chacune d’elles. Cette rubrique englobe les certificats et tests d’évaluation lui reconnaissant un niveau linguistique, les voyages effectués à l’étranger, etc. Il convient d’être concis et précis dans l’énoncé de ces informations, sans entrer dans le détail (style télégraphique) ;</li>
<li>Rubrique <strong>"Divers" ("Altre informazioni)</strong> : le candidat y indique son niveau d’informatique, les permis détenus en particulier. En revanche les passions ou sports pratiqués ne sont pas abordés dans un CV italien. Ces questions seront évoquées lors de l’entretien d’embauche si l’employeur souhaite en savoir plus sur la personnalité du candidat.</li></ul>
<p>A la fin de votre CV, il convient d’ajouter la formule "autorizzo il trattamento dei miei dati personali ai sensi della legge 675/96".</p>
<h3 class="spip"><a id="sommaire_2"></a>Diplômes</h3>
<p>Il est nécessaire de vérifier la reconnaissance professionnelle de votre diplôme français.</p>
<p>La page internet <a href="http://www.ambafrance-it.org/La-reconnaissance-des-dipl%C3%B4mes" class="spip_out" rel="external">de l’Ambassade de France en Italie</a> peut vous y aider.</p>
<p>Des informations peuvent être obtenues sur le réseau Centre national d’information sur la reconnaissance des diplômes des Etats membres de l’UE (NARIC) et plus spécifiquement en Italie auprès du <a href="http://www.cimea.it/" class="spip_out" rel="external">Centre d’information sur la mobilité et les équivalences académiques</a> (CIMEA) :</p>
<p>Viale XXI aprile 36 <br class="manualbr">00162 Roma<br class="manualbr">Tel : +39.06.86.32.12.81</p>
<h3 class="spip"><a id="sommaire_3"></a>Modèles de CV</h3>
<p>Des modèles de CV sont disponibles sur le site du service emploi de la Chambre française de commerce et d’industrie de Milan : <a href="http://www.chambre.it/fr/frontpage" class="spip_out" rel="external">http://www.chambre.it/fr/frontpage</a> Job Chambre  News / Informations / Présentation du service de l’emploi.</p>
<p>Le site <strong>Europass</strong>, initiative de l’UE visant à aider les citoyens à présenter leurs compétences et qualifications de manière claire et logique (notamment auprès des employeurs) et à favoriser la mobilité transfrontalière, propose des modèles de CV adaptés à chaque pays.</p>
<p>Toute l’information sur <a href="http://www.europass.cedefop.europa.eu/" class="spip_out" rel="external">Europass</a> rubrique "Bienvenue" pour accéder aux pages en français et Europass Italia.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/emploi-stage/article/curriculum-vitae). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
