# 50e anniversaire de l’implantation à Nantes du ministère des affaires étrangères et du développement international et de la création du service central d’état civil (4 juin 2015)

<iframe width="640" height="380" src="https://www.youtube.com/embed/wBAp3SX4T5Q" frameborder="0" allowfullscreen></iframe>
<p><strong>Avec 15 millions d’actes conservés depuis sa création en 1965, le service central d’état civil est le plus important service d’état civil de France.</strong> Il délivre copies et extraits d’actes d’état civil aux Français dont un des épisodes de la vie civile s’est déroulé à l’étranger (naissance, mariage, reconnaissance, adoption, décès). Il est également compétent pour établir les actes d’état civil français de toutes les personnes qui acquièrent la nationalité française.</p>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/actualites/article/50e-anniversaire-de-l-implantation-a-nantes-du-ministere-des-affaires#sommaire_1">Infographie : l’Etat civil au MAEDI : la 1re mairie de France se modernise</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/actualites/article/50e-anniversaire-de-l-implantation-a-nantes-du-ministere-des-affaires#sommaire_2">La semaine des métiers des Affaires étrangères (du 8 au 12 juin 2015)</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Infographie : l’Etat civil au MAEDI : la 1re mairie de France se modernise</h3>
<p class="spip_document_85861 spip_documents spip_documents_center">
<img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L858xH2402/scec_1re_mairie_de_france_statique_juin_2015_cle81ad12-cc429.png" width="858" height="2402" alt=""></p>
<h3 class="spip"><a id="sommaire_2"></a>La semaine des métiers des Affaires étrangères (du 8 au 12 juin 2015)</h3>
<p>A l’occasion du 50ème anniversaire de la présence du Ministère des Affaires étrangères et du Développement international à Nantes, le Centre des Archives diplomatiques de Nantes et le Service central d’état civil ont le plaisir de vous inviter à La semaine des métiers des Affaires étrangères, du 8 au 12 juin 2015, du lundi au vendredi de 13h30 à 18h.</p>
<p>De l’action de la France à l’ONU à l’action consulaire, en passant par la gestion de crises sécuritaires et la diplomatie économique, découvrez les missions de ce ministère atypique. Les archives diplomatiques seront présentes durant toute la semaine, sur le stand consacré aux métiers des archives : rapatriement d’archives venues du monde entier, traitement et restauration des documents. Le Service central d’état civil sera également présent pour exposer son rôle en tant que « Mairie des Français de l’étranger », chargé principalement de l’établissement d’actes d’état civil et de leur exploitation.</p>
<p>De nombreuses animations sont au programme : dialogue avec des agents qui feront part de leur expérience, simulation d’un point de presse, conférences, tables rondes et projections vidéo.</p>
<p>À découvrir également, l’exposition photo de Yann Arthus-Bertrand, 60 solutions face au changement climatique, qui illustre l’engagement de la France en matière de développement durable et le rôle du ministère dans l’organisation de la conférence internationale Paris 2015 sur le climat - COP21.</p>
<p>***<br class="autobr">Espace international Cosmopolis<br class="autobr">18 rue Scribe - 44000 Nantes<br class="autobr">Entrée libre</p>
<p><strong>Renseignements : 02.51.84.36.70</strong></p>
<p>Vous pouvez télécharger le programme ici :</p>
<p class="document_doc">
<a class="spip_in" title="Doc:Semaine des métiers du MAE , 1 Mo, 0x0" href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/semaine_metiers-depliant-pages_cle09698f.pdf"><img width="18" height="21" alt="Doc:Semaine des métiers du MAE , 1 Mo, 0x0" src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L18xH21/pdf-7444b.png" style="vertical-align: middle;"></a>
<a href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/semaine_metiers-depliant-pages_cle09698f.pdf">Semaine des métiers du MAE - (PDF, 1 Mo)</a>
</p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/actualites/article/50e-anniversaire-de-l-implantation-a-nantes-du-ministere-des-affaires). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
