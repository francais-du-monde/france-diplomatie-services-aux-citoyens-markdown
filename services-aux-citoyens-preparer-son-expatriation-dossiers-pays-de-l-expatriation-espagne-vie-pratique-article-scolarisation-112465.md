# Scolarisation

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/vie-pratique/article/scolarisation-112465#sommaire_1">Les établissements scolaires français en Espagne</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/vie-pratique/article/scolarisation-112465#sommaire_2">Enseignement supérieur</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Les établissements scolaires français en Espagne</h3>
<p>Pour toute information sur la scolarisation dans le système français à l’étranger, vous pouvez consulter notre thématique sur les <a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures.md" class="spip_in">Etudes supérieures</a> et la <a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais.md" class="spip_in">Scolarité en français</a>.</p>
<p>Vous y trouverez des renseignements sur :</p>
<ul class="spip">
<li>les établissements français du primaire et du secondaire à l’étranger ;</li>
<li>les bourses scolaires et la prise en charge des frais de scolarité à l’étranger ;</li>
<li>les possibilités qui s’offrent à vous si votre enfant ne peut être scolarisé à l’étranger dans le système français (enseignement à distance par le CNED, programme français langue maternelle (FLAM), internats en France) ;</li>
<li>les épreuves du baccalauréat à l’étranger ;</li>
<li>les bourses d’études supérieures en France et à l’étranger ;</li>
<li>l’équivalence des diplômes.</li></ul>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.aefe.fr/" class="spip_out" rel="external">Agence pour l’enseignement français à l’étranger</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Enseignement supérieur</h3>
<p>Si vous souhaitez vous inscrire en première année de premier cycle universitaire, vous devez vous adresser directement à <strong>l’UNED</strong> (Universidad Nacional de Educación a Distancia). Cet organisme se charge d’étudier les dossiers des nouveaux bacheliers.</p>
<p><strong>Les établissements d’enseignement supérieur</strong></p>
<p>Depuis 1985, année de début du processus de décentralisation de l’enseignement supérieur, le nombre d’universités en Espagne a plus que doublé, passant de 35 à l’époque à 79 pour l’année universitaire 2012-2013. Si l’on se réfère aux données du recensement de population 2008, il y a en Espagne 1,7 universités par million d’habitants et une université pour chaque 48 500 jeunes en âge universitaire (18-24 ans).</p>
<p>Les 50 universités publiques, dont l’Université internationale Menéndez Pelayo et l’Université internationale d’Andalousie, dispensent des enseignements sur 154 campus. Près de 60 % du réseau universitaire public se concentre dans les Communautés autonomes d’Andalousie (10 universités, 34 campus), Catalogne (7 universités, 28 campus), Madrid (6 universités, 17 campus) et Valence (5 universités, 13 campus).</p>
<p>Les 29 universités privées, dont sept relèvent de l’église catholique et cinq sont « non présentielles » concernent 69 campus et sont majoritairement présentes dans les Communautés autonomes de Madrid (9 universités, 16 campus), Catalogne (5 universités, 16 campus) et Castille et Léon (5 universités, 7 campus).</p>
<p>Les universités dépendant de l’église catholique sont créées par décision conjointe de l’Etat espagnol et du Saint Siège et peuvent également délivrer des diplômes ecclésiastiques. Il existe par ailleurs des établissements privés ;</p>
<p><strong>L’accès à l’enseignement supérieur</strong></p>
<p>En application de LOMCE 8/2013, l’accès à l’enseignement supérieur pour les élèves des systèmes éducatifs des établissements des Etats membres de l’Union européenne, ou d’autres Etats avec lesquels il existe des accords de réciprocité en la matière, est modifié. Pour les élèves français seul le baccalauréat est requis. Les universités peuvent néanmoins mettre en place des modalités spécifiques d’entrée dans les filières les plus sélectives (épreuves, entretiens….). Jusqu’à 2017, date à laquelle la loi s’appliquera aussi aux élèves du système espagnol, les élèves qui le souhaitent pourront continuer à présenter, tout comme les élèves du système national, les épreuves spécifiques de l’actuelle <i>selectividad</i>. Ce dispositif transitoire fera l’objet de précisions dans les mois qui viennent.</p>
<p><strong>Les formations</strong></p>
<p>L’organisation des études supérieures officielles est désormais régie par les décrets royaux RD 56/2005 et RD 1393/2007. Ces derniers prévoient la mise en œuvre de l’architecture du processus de Bologne et officialisent donc trois cycles, chacun d’entre eux étant sanctionné par un diplôme officiel reconnu sur l’ensemble du territoire :</p>
<p><strong>Le grado : </strong>d’une durée de quatre ans, il correspond à un minimum de 240 ECTS et peut atteindre 300 ECTS dans certaines disciplines. Dès l’obtention des premiers 120 crédits est délivré un certificat d’études initiales validant la moitié du cursus sans être un diplôme. Ce <i>grado</i> doit faciliter l’homologation vers d’autres systèmes non européens, comme le système américain. Les étudiants sont incités à réaliser au moins un semestre à l’étranger au cours de ce cursus ;</p>
<p><strong>Le master : </strong>accessible après le <i>grado</i>, il valide entre 60 et 120 ECTS. Il correspond à une spécialisation dans un domaine donné et inclut des activités de recherche. Le master se conclut par une période de stage et un exposé oral devant un jury ;</p>
<p><strong>Le doctorat </strong> : accessible après le master, son nouveau statut vient d’être promulgué par le décret royal DR 99/2011. Il est d’une durée normale de trois ans, pouvant s’étendre à quatre ans au maximum, et valide jusque 30 ECTS complémentaires, selon le sujet étudié et l’origine des doctorants.</p>
<p><strong>Le coût des études</strong></p>
<p>En ce qui concerne les universités publiques, les fourchettes de coût des formations sont établies annuellement par la Conférence générale de politique universitaire, discipline par discipline et selon les niveaux des enseignements. Chaque Communauté autonome, se cadrant sur ces fourchettes, décide alors du coût final des formations et ce pour l’ensemble des universités dont elle a la charge.</p>
<p>Pour l’année universitaire 2012-2013, et à titre d’exemple, le coût moyen d’une première année de <i>grado</i> est de 1074 € ; celui d’un master est de 1650 € (professionnalisant) ou 2400 € (non professionnalisant). A noter que les coûts sont plus élevés pour les étudiants qui redoublent et varient également en fonction des communautés (Madrid et Barcelone sont les plus onéreuses en <i>grado</i>, l’Andalousie et la Galice les plus accessibles).</p>
<p>S’agissant des universités privées, les coûts sont libres et peuvent atteindre jusqu’à 18 000 € pour une année de formation en management des entreprises. Une année de master se situe en moyenne autour de 8000 €.</p>
<p><i>Mise à jour : avril 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/vie-pratique/article/scolarisation-112465). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
