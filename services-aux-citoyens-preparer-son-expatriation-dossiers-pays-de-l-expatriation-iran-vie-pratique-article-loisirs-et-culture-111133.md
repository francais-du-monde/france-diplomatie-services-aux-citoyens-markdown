# Loisirs et culture

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/vie-pratique/article/loisirs-et-culture-111133#sommaire_1">Tourisme</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/vie-pratique/article/loisirs-et-culture-111133#sommaire_2">Activités culturelles</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/vie-pratique/article/loisirs-et-culture-111133#sommaire_3">Sports</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/vie-pratique/article/loisirs-et-culture-111133#sommaire_4">Télévision – Radio</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/vie-pratique/article/loisirs-et-culture-111133#sommaire_5">Presse française</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Tourisme</h3>
<p>L’Iran dispose de très nombreux sites touristiques et naturels ; des périodes achéménide, parthe ou sassanide, jusqu’à la dynastie Qadjar, l’histoire a légué nombre de vestiges renommés : Persépolis, Bishâpur, Ispahan, Yazd… Les sites naturels (montagne, déserts etc.) présentent une variété inépuisable.</p>
<p>En revanche les infrastructures sont peu développées (hôtellerie, restauration, équipement des sites).</p>
<p>Pour toute information d’ordre touristique, on peut s’adresser en France à :</p>
<p><strong>Maison de l’Iran</strong><br class="manualbr">71 Ave des Champs Elysées <br class="manualbr">75008 Paris <br class="manualbr">Tél. : 01 42 25 99 42</p>
<p><a href="http://www.paris.icro.ir/index.aspx?siteid=189" class="spip_out" rel="external">Centre Culturel d’Iran à Paris</a><br class="manualbr">6, rue Jean Bart - 75006 Paris <br class="manualbr">Tél. : 01 45 49 19 20<br class="manualbr">Télécopie : 01 45 49 31 34 <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/vie-pratique/article/loisirs-et-culture-111133#cci#mc#cciran.com#" title="cci..åt..cciran.com" onclick="location.href=mc_lancerlien('cci','cciran.com'); return false;" class="spip_mail">Courriel</a></p>
<p>Le Centre dispose d’une bibliothèque (5000 ouvrages) et organise des cours de langue ainsi que des expositions.</p>
<p>et en Iran à :</p>
<p><a href="http://www.irpedia.com/" class="spip_out" rel="external">Iran tourism and travel organization</a></p>
<h3 class="spip"><a id="sommaire_2"></a>Activités culturelles</h3>
<h4 class="spip">Activités culturelles en français</h4>
<p>Pendant les festivals locaux, des films français sont présentés en français. Quelques films achetés par l’Iran sont doublés en persan.</p>
<h4 class="spip">Activités culturelles locales</h4>
<p>La télévision iranienne compte six chaînes nationales, de qualité inégale et huit chaînes par satellite. Il est possible de capter les chaînes étrangères avec une antenne satellite. La vidéo est répandue.</p>
<p>Téhéran compte de nombreuses de salles de cinéma confortables, Les films projetés sont iraniens, et de médiocre qualité, sauf exception. L’Iran compte des réalisateurs de renom international, tels Abbas KIAROSTAMI, Asghar FARHADI, MAKHMALBAF, Jafar PANAHIDariush MEHRJUI, Masoud KIMIAI.</p>
<p>Des pièces de théâtre sont présentées régulièrement. Il s’agit pour l’essentiel de pièces écrites par des dramaturges iraniens et présentées en persan ou des pièces étrangères traduites en persan.</p>
<p>Plusieurs salles programment régulièrement des spectacles de musique folklorique ou religieuse.</p>
<p>Tous les vendredis des expositions de peintres iraniens sont organisées dans les différentes galeries de Téhéran. Elles présentent surtout l’art local : peinture, calligraphie, poterie, dessin, et sont en général de bonne qualité.</p>
<p>La radio propose des programmes islamiques, poésie, musique traditionnelle, questions de société.</p>
<h3 class="spip"><a id="sommaire_3"></a>Sports</h3>
<p>Il est possible de pratiquer de nombreux sports : équitation, tennis, golf, squash, basket-ball, arts martiaux, culturisme, sports nautiques. Des stations de ski sont accessibles facilement depuis Téhéran. Les activités physiques s’exercent séparément pour les hommes et pour les femmes. Il existe des clubs sportifs accessibles aux étrangers. Ces clubs ne sont pas mixtes. Les piscines individuelles ou collectives sont convenablement contrôlées.</p>
<p>L’organisation de l’Environnement, service dépendant d’une vice-présidence de la République, gère la chasse et la pêche. L’autorisation d’importer une arme de chasse est très longue à obtenir (environ deux ans) et il n’est pas autorisé de chasser avec une arme appartenant à un tiers. Un permis officiel doit être délivré par l’Organisation de la chasse et de la pêche.</p>
<p>Le permis de pêche est délivré pour un an pour les rivières non protégées, et pour un jour pour les rivières protégées. La pêche est autorisée toute l’année.</p>
<h3 class="spip"><a id="sommaire_4"></a>Télévision – Radio</h3>
<p>Le personnel diplomatique est autorisé à détenir une parabole pour obtenir les chaînes télévisées étrangères. Il est possible de capter France 24 et TV5 au moyen d’une antenne satellite, ainsi que ABSAT avec un abonnement et un décodeur.</p>
<p>RFI peut être captée, avec une qualité de réception moyenne.</p>
<h3 class="spip"><a id="sommaire_5"></a>Presse française</h3>
<p>La presse française est très peu diffusée en Iran. Les abonnements, acheminés par voie postale, sont soumis à la censure. Les voyageurs n’ont pas le droit d’apporter avec eux des journaux et revues à l’arrivée en Iran, il arrive que les revues soient saisies ou censurées à l’aéroport. Quelques librairies vendent des ouvrages en français.</p>
<p><i>Mise à jour : février 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/vie-pratique/article/loisirs-et-culture-111133). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
