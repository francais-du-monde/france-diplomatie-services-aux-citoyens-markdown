# Emploi, stage

<h2 class="rub23478">Marché du travail</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/emploi-stage/#sommaire_1">Secteurs à fort potentiel</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/emploi-stage/#sommaire_2">Secteurs à faible potentiel</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/emploi-stage/#sommaire_3">Professions règlementées</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/emploi-stage/#sommaire_4">Rémunération</a></li></ul>
<p>Le chômage touche officiellement 6,5% de la population active togolaise, mais ce chiffre doit être pris avec précaution en l’absence de dispositif d’assurance chômage source de statistiques fiables. Le taux de chômage est le plus élevé à Lomé où il atteint 10%, contre 2,8% en moyenne en milieu rural. Les populations les plus touchées sont les femmes (9%), les diplômés de l’enseignement supérieur (7,1%), et les jeunes (8,1% de chômage chez les 15-35 ans).</p>
<p>La lutte contre le chômage menée par les autorités s’est traduit par la création d’une Agence Nationale Pour l’Emploi (ANPE) en 2008, par l’élaboration d’une stratégie de croissance accélérée et de promotion de l’emploi (SCAPE) sur la période 2013-2017, et par la mise en place d’un comité sectoriel « promotion, protection sociale et emploi » qui assure le suivi et l’évaluation des politiques publiques en la matière, et de leur cohérence avec la SCAPE.</p>
<p>Le Togo est caractérisé par l’importance de son secteur informel, qui joue un rôle majeur dans le marché du travail. Selon les derniers chiffres connus par la direction nationale de la statistique et de la comptabilité nationale, il a généré une valeur ajoutée de 426 Mds FCFA en 2001, soit une contribution au PIB de 44%. Face à l’essor du secteur informel, les autorités ont mis en place une Délégation à l’Organisation du secteur informel (DOSI) dont l’objectif est d’organiser, de régir, et de contrôler le secteur.</p>
<p>L’étroitesse du marché du travail au Togo limite fortement les possibilités d’embauche sur place des ressortissants français, les recrutements d’expatriés se faisant dans la majeure partie des cas depuis la France.</p>
<h3 class="spip"><a id="sommaire_1"></a>Secteurs à fort potentiel</h3>
<ul class="spip">
<li>Activité portuaire</li>
<li>Agronomie</li>
<li>BTP</li>
<li>Commerce</li>
<li>Tourisme, hôtellerie, restauration</li>
<li>Banque (le Togo compte de nombreuses banques représentées dans plusieurs pays d’Afrique qui offrent des débouchés intéressants, généralement pour les personnes ayant la nationalité d’un de leurs pays d’implantation)</li>
<li>Extraction (sous réserve des conclusions de l’actualisation en cours des données minières)</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Secteurs à faible potentiel</h3>
<ul class="spip">
<li>Télécom (dans l’attente d’une ouverture du marché)</li>
<li>Industrie manufacturière</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Professions règlementées</h3>
<p>Le droit togolais étant d’inspiration française, les professions réglementées au Togo sont sensiblement les mêmes qu’en France (avocat, notaire, huissier de justice, courtier en assurance, pharmacien, médecin, …).</p>
<h3 class="spip"><a id="sommaire_4"></a>Rémunération</h3>
<p>Les salaires sont très variables en fonction des emplois, des secteurs, des qualifications requises, et de la nature du contrat (local ou d’expatrié). Le salaire minimum légal est de 35 000 FCFA par mois pour 40 heures de travail par semaine, ce qui correspond à environ 53 EUR.</p>
<p><i>Mise à jour : juin 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-togo-emploi-stage-article-stages.md" title="Stages">Stages</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-togo-emploi-stage-article-recherche-d-emploi.md" title="Recherche d’emploi">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-togo-emploi-stage-article-reglementation-du-travail.md" title="Réglementation du travail">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-togo-emploi-stage-article-marche-du-travail.md" title="Marché du travail">Marché du travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/emploi-stage/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
