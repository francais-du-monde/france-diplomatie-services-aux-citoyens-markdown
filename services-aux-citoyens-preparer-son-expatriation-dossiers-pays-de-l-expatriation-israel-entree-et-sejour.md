# Entrée et séjour

<h2 class="rub22896">Passeport, visa, permis de travail</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/entree-et-sejour/#sommaire_1">Touristes </a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/entree-et-sejour/#sommaire_2">Etudiants </a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/entree-et-sejour/#sommaire_3">Visas de travail</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/entree-et-sejour/#sommaire_4">Les conjoints </a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/entree-et-sejour/#sommaire_5">La <i>loi du retour</i> </a></li></ul>
<p>Quel que soit le motif de votre séjour, renseignez-vous auprès de la section consulaire de l’ambassade d’Israël à Paris qui vous informera sur la règlementation en matière d’entrée et de séjour en Israël, règlementation que vous devrez impérativement respecter. N’hésitez pas à consulter le site ministère de l’Intérieur israélien afin de vous familiariser avec les modalités d’entrée et de séjour dans le pays.</p>
<p>Une fois sur place c’est le ministère israélien de l’Immigration et de l’Intégration qui prend le relais. <strong>En aucun cas, vous n’êtes autorisé à travailler en Israël sans permis adéquat. </strong></p>
<p>Le Consulat de France en Israël n’est pas l’organisme compétent pour répondre à vos demandes d’information concernant votre séjour sur le territoire israélien.</p>
<h3 class="spip"><a id="sommaire_1"></a>Touristes </h3>
<p>Les ressortissants français peuvent entrer en Israël pour une visite touristique sans visa pour une durée de trois mois maximum (avec un passeport ayant une validité de plus de six mois). A la fin du séjour de trois mois, une extension du séjour peut être sollicitée auprès du ministère israélien de l’Intérieur, sans assurance d’accord des autorités israéliennes.</p>
<h3 class="spip"><a id="sommaire_2"></a>Etudiants </h3>
<p>Un visa d’un an est accordé sur production d’un certificat de scolarité (renouvelable chaque année).</p>
<p><strong>Attention</strong> : ces deux types de visa n’octroient pas le droit de travailler.</p>
<h3 class="spip"><a id="sommaire_3"></a>Visas de travail</h3>
<p>L’octroi d’un permis de séjour et de travail est limité par la loi et par une décision gouvernementale. Il revient à l’employeur d’adresser au bureau du travail israélien une demande de visa de travail pour étranger. Après les vérifications d’usage, il transmet son accord au ministère de l’Intérieur qui délivre le visa dont la durée ne peut dépasser 27 mois. Après cette période, l’intéressé doit quitter le pays et ne peut revenir en Israël qu’après avoir déposé une nouvelle demande de visa. Les travailleurs étrangers ne peuvent prétendre à l’assurance chômage, à la retraite et à la formation professionnelle par le biais de l’Etat. En revanche, ils sont soumis aux cotisations sociales leur permettant de bénéficier du droit au congé de maternité et de l’assurance contre les accidents du travail et la faillite.</p>
<h3 class="spip"><a id="sommaire_4"></a>Les conjoints </h3>
<p>Les conjoints étrangers de citoyens israéliens ou de travailleurs étrangers se voient accorder un visa renouvelable chaque année et qui ne leur accorde pas le droit de travailler.</p>
<p>Cas particulier des conjoints d’agents diplomatiques et consulaires. Par échange de notes verbales entre les autorités françaises et les autorités israéliennes, les deux pays ont signé un accord pour faciliter l’emploi des conjoints d’agents diplomatiques et consulaires nommés pour effectuer une mission officielle dans l’Etat d’accueil.</p>
<h3 class="spip"><a id="sommaire_5"></a>La <i>loi du retour</i> </h3>
<p>En vertu de la <i>loi du retour</i>, tout individu dont l’un des grands parents est juif se voit octroyer automatiquement la nationalité israélienne lors de la demande d’immigration. Il a toutefois la possibilité de refuser la nationalité israélienne en optant pour le statut de résident temporaire (cinq ans) ou permanent (illimité) lui octroyant le droit de séjourner et travailler en Israël et de jouir de l’ensemble de la législation sociale du pays. Les résidents permanents sont soumis aux obligations militaires israéliennes.</p>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li><a href="http://www.ambafrance-il.org/Formalites-de-sejour-en-Israel.html" class="spip_out" rel="external">Rubrique décrivant les formalités de séjour en Israël sur le site de l’Ambassade de France en Israël</a> ;</li>
<li><a href="http://www.moia.gov.il/Moia_fr/" class="spip_out" rel="external">Pages en français du ministère israélien de l’Immigration et de l’Intégration</a> (en particulier la rubrique de la <i>loi du retour</i>).</li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-israel-entree-et-sejour-article-animaux-domestiques.md" title="Animaux domestiques">Animaux domestiques</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-sante.md" title="Vaccination">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-israel-entree-et-sejour-article-demenagement-110454.md" title="Déménagement">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-israel-entree-et-sejour-article-passeport-visa-permis-de-travail.md" title="Passeport, visa, permis de travail">Passeport, visa, permis de travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/entree-et-sejour/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
