# Recherche d’emploi

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/emploi-stage/article/recherche-d-emploi#sommaire_1">Outils pour la recherche d’emploi</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/emploi-stage/article/recherche-d-emploi#sommaire_2">Organismes pour la recherche d’emploi</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Outils pour la recherche d’emploi</h3>
<h4 class="spip">Journaux</h4>
<p>Tous les journaux (de diffusion nationale ou locale, ainsi que les " gratuits ") diffusent des offres d’emploi chaque jour, et ont un cahier hebdomadaire " carrières". Leurs sites internet permettent de consulter les offres d’emploi en ligne :</p>
<ul class="spip">
<li><a href="http://www.nzherald.co.nz/" class="spip_out" rel="external">Nzherald.co.nz</a></li>
<li><a href="http://www.stuff.co.nz/dominion-post/" class="spip_out" rel="external">Stuff.co.nz/dominion-post</a></li>
<li><a href="http://www.edgazette.govt.nz/" class="spip_out" rel="external">Edgazette.govt.nz</a> (pour les emplois dans l’enseignement)</li></ul>
<h4 class="spip">Sites internet</h4>
<p><strong>Sites gouvernementaux néo-zélandais proposant des offres d’emploi :</strong></p>
<ul class="spip">
<li><a href="http://www.careers.govt.nz/" class="spip_out" rel="external">Site officiel du gouvernement sur l’emploi</a> ;</li>
<li><a href="http://www.immigration.govt.nz/?bcsi_scan_96404f7f6439614d=DMWxQwj+7Yhv0b5RLslRdcMb6i0FAAAAvMMwCw==:1" class="spip_out" rel="external">Site du ministère de l’immigration</a> ;</li>
<li><a href="http://www.newkiwis.co.nz/" class="spip_out" rel="external">Site du service de l’immigration administré par la chambre de commerce d’Auckland</a> ;</li>
<li><a href="https://jobs.govt.nz/" class="spip_out" rel="external">Jobs.govt.nz</a>.</li></ul>
<p><strong>Autres sites d’emploi :</strong></p>
<ul class="spip">
<li><a href="http://www.seek.co.nz/" class="spip_out" rel="external">Seek.co.nz</a></li>
<li><a href="http://www.trademe.co.nz/jobs" class="spip_out" rel="external">Trademe.co.nz/jobs</a></li>
<li><a href="http://www.workingin.com/" class="spip_out" rel="external">Workingin.com</a></li></ul>
<h4 class="spip">Cabinets de recrutement</h4>
<ul class="spip">
<li><a href="http://www.workandincome.govt.nz/" class="spip_out" rel="external">Agence pour l’emploi néo–zélandaise</a> (structure publique fournissant assistance financière et services de recherche d’emploi aux citoyens et résidents néo-zélandais)</li>
<li><a href="http://www.adecco.co.nz/default.aspx" class="spip_out" rel="external">Adecco.co.nz</a></li>
<li><a href="http://nz.hudson.com/" class="spip_out" rel="external">NZ.hudson.com</a></li>
<li><a href="https://www.manpower.co.nz/" class="spip_out" rel="external">Manpower.co.nz</a></li>
<li><a href="http://nz.drakeintl.com/" class="spip_out" rel="external">NZ.drakeintl.com</a></li></ul>
<p>Les personnes ne maitrisant pas bien l’anglais pourront trouver des petits boulots dans la restauration ou la cueillette des fruits, activité souvent exercée par les détenteurs du <i>working holiday visa</i>. Les principaux sites d’emplois saisonniers sont :</p>
<ul class="spip">
<li><a href="http://www.seasonalwork.co.nz/online/welcome.csn" class="spip_out" rel="external">Seasonalwork.co.nz</a></li>
<li><a href="http://www.picknz.co.nz/" class="spip_out" rel="external">Picknz.co.nz</a></li>
<li>Le site de <a href="http://www.frogs-in-nz.com/" class="spip_out" rel="external">Frogs in New Zealand</a> fournit des conseils sur l’emploi et une liste de liens utiles.</li></ul>
<p><i>Le signalement d’organismes sur ce site ne saurait engager la responsabilité de la MFE quant à la qualité de leurs services.</i></p>
<h3 class="spip"><a id="sommaire_2"></a>Organismes pour la recherche d’emploi</h3>
<p><a href="http://www.fnzcci.org.nz/" class="spip_out" rel="external">French New Zealand Chamber of Commerce and Industry</a><br class="manualbr">Hosted by Auckland Chamber of Commerce<br class="manualbr">FNZCCI<br class="manualbr">P O Box 47<br class="manualbr">Shortland Street, Auckland 1140<br class="manualbr">New Zealand<br class="manualbr">Tél. : [64] (0)9 302 99 32 <br class="manualbr">Télécopie : [64] (0)9 309 0081<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/emploi-stage/article/recherche-d-emploi#info#mc#fnzcci.org.nz#" title="info..åt..fnzcci.org.nz" onclick="location.href=mc_lancerlien('info','fnzcci.org.nz'); return false;" class="spip_mail">Courriel</a></p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/emploi-stage/article/recherche-d-emploi). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
