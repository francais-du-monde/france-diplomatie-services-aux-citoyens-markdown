# Curriculum vitae

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/emploi-stage/article/curriculum-vitae#sommaire_1">Rédaction</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/emploi-stage/article/curriculum-vitae#sommaire_2">Diplômes</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/emploi-stage/article/curriculum-vitae#sommaire_3">Organismes compétents en Espagne</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/emploi-stage/article/curriculum-vitae#sommaire_4">Modèles de CV</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Rédaction</h3>
<p><strong>Il n’existe pas de différence fondamentale avec le curriculum vitae français</strong>. Il ne doit pas dépasser deux pages. Toutefois, il peut être demandé de joindre au CV le « dossier académique » avec le détail des notes obtenues chaque année.</p>
<p>Le CV doit être bref, concis, dactylographié et présenté dans l’ordre chronologique inverse : les données les plus récentes, qu’on suppose être les plus significatives doivent être présentées en premier. Il est bon d’indiquer en titre un objectif professionnel, ainsi que ses principaux atouts pour le poste indiqué. Les annonces d’emploi exigent souvent de joindre une photo au CV.</p>
<p><strong>Les différentes rubriques</strong></p>
<ul class="spip">
<li><strong>Nom et prénom. </strong>Il peut être recommandé de mentionner deux noms de famille (celui du père et de la mère) afin de calquer la présentation sur la présentation espagnole.</li>
<li><strong>Adresse en Espagne</strong>. Une adresse en France diminuera considérablement les chances du candidat d’être contacté par un employeur.</li>
<li><strong>Numéro de téléphone </strong>personnel, téléphone portable et fixe</li>
<li><strong>Courriel</strong></li>
<li><strong>Titre de résidence avec N.I E</strong>, numéro de sécurité sociale</li>
<li><strong>Permis de conduire</strong>, voiture personnelle le cas échéant</li></ul>
<p>A noter que l’état civil, la date et lieu de naissance sont fréquemment indiqués.</p>
<p><strong>La formation (<i>Formacion académica</i>)</strong></p>
<p>Il suffit de mentionner les diplômes les plus importants (inutile par exemple de mentionner le Baccalauréat si vous avez un niveau d’études plus élevé, ou votre licence si vous avez obtenu par la suite une maîtrise).</p>
<p><strong>Expérience professionnelle (<i>Experienca profesional</i>)</strong></p>
<p>Doivent figurer : les dates d’embauche, l’intitulé du poste, le nom de l’entreprise, son secteur, ainsi qu’un bref descriptif des fonctions occupées afin de faire ressortir les compétences acquises. Il est important de faire apparaître les mots clés de son activité, et, en particulier dans le cas d’un poste commercial, les résultats obtenus.</p>
<p>A partir de cinq ans d’expérience environ, l’expérience primant sur la formation, cette rubrique doit apparaître en premier lieu.</p>
<p><strong>Langues et informatique (<i>Idiomas e Informatica</i>)</strong></p>
<p>Il est bon de justifier le niveau de langues et d’informatique indiqué. Par exemple, en ce qui concerne le niveau de langue, il est possible d’indiquer les séjours effectués à l’étranger ou les diplômes obtenus (TOEFL, DELE…).</p>
<p><strong>Centres d’intérêt (Varios)</strong></p>
<p>Cette rubrique est facultative. Il peut être intéressant d’indiquer ses centres d’intérêt et ses activités associatives si elles mettent en valeur un point fort de sa personnalité et peuvent, de ce fait, éveiller l’attention de l’employeur.</p>
<h3 class="spip"><a id="sommaire_2"></a>Diplômes</h3>
<p>Les procédures de reconnaissance universitaire et professionnelle des diplômes sont complexes en Espagne.</p>
<p>Vous trouverez des informations complètes sur ce sujet sur le site du consulat de France à Séville :</p>
<ul class="spip">
<li><a href="http://www.ambafrance-es.org/france_espagne/spip.php?article2832" class="spip_out" rel="external">Ambassade de France en Espagne</a> ;</li>
<li><a href="http://www.mecd.gob.es/portada-mecd/" class="spip_out" rel="external">Ministère espagnol de l’Education, de la Culture et des Sports</a>.</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Organismes compétents en Espagne</h3>
<p><strong>Homologation de diplôme (<i>homologacion de titulos</i>)</strong></p>
<p>Le « Titulo » est le document officiel ayant validité sur l’ensemble du territoire espagnol. Il produit, en application de la réglementation en vigueur, des effets académiques « pleins » et habilite, le cas échéant, à l’exercice d’une profession réglementée.</p>
<p>L’homologation de diplôme correspond à une reconnaissance universitaire et revient à attribuer un diplôme ou un niveau de diplôme espagnol au titulaire du diplôme étranger. L’organisme compétent est le :</p>
<p><strong>NARIC España</strong><br class="manualbr">Ministerio de Educación<br class="manualbr">SG de Títulos y Reconocimiento de Cualificaciones<br class="manualbr">Paseo del Prado, 28 - 28014 Madrid<br class="manualbr">Tel. : 00 34 91 506 55 93 <br class="manualbr">Fax : 00 34 91 506 57 06</p>
<p><strong>Reconnaissance professionnelle (<i>reconocimiento profesional</i>)</strong></p>
<p>De nombreuses professions étant réglementées en Espagne, l’exercice d’un métier requiert généralement l’inscription à un collège professionnel en Espagne (l’équivalent des conseils de l’ordre en France). La liste des professions réglementées en Espagne est disponible sur le site Internet du <a href="http://www.mecd.gob.es/portada-mecd/" class="spip_out" rel="external">ministère espagnol de l’Education, de la Culture et des Sports</a>.</p>
<p>Si vous n’exercez pas de profession réglementée en Espagne, il n’est pas nécessaire de faire reconnaître officiellement vos diplômes. Vous devez seulement valoriser votre niveau d’études et négocier un poste en rapport avec votre curriculum vitae et vos qualifications.</p>
<p><strong>Equivalences entre diplômes français et espagnols</strong></p>
<ul class="spip">
<li>Brevet d’études du premier cycle (BEPC) : <i>Graduado en Educacion Secundaria (GES)</i> ;</li>
<li>Brevet des collèges : <i>Educación General Básica</i> ;</li>
<li>Brevet d’études professionnelles (BEP) / Certificat d’aptitude professionnelle (CAP) : <i>FP1 ou Formación Profesional 1° grado</i> ;</li>
<li>Baccalauréat Professionnel Technique : <i>FP2 ou Formación Profesional 2d grado</i> ;</li>
<li>Baccalauréat : <i>Bachiller</i> ;</li>
<li>Classes préparatoires : <i>Pas d’équivalence</i> ;</li>
<li>Brevet de technicien supérieur (BTS) / Diplôme universitaire de technologie (DUT) : <i>Título de Técnico superior</i> ;</li>
<li>Diplôme d’études universitaires générales (DEUG) : <i>Diploma Universitario</i> ;</li>
<li>Licence (Bac+3) : <i>Diplomatura</i> ;</li>
<li>Ecole de commerce / Maîtrise : <i>Licenciatura</i> ;</li>
<li>Ecole d’ingénieur : <i>Ingenieria superior</i> ;</li>
<li>Diplôme d’études supérieures spécialisées (DESS) : <i>Master / Diploma Universitario de postgrado</i> ;</li>
<li>Diplôme d’études approfondies (DEA) : 1<i>° año de doctorado / Postgrado</i> ;</li>
<li>Doctorat : <i>Doctorado</i>.</li></ul>
<p><strong>Accord bilatéral</strong></p>
<p>La France et l’Espagne ont signé le <strong>16 novembre 2006 </strong>à Gérone un accord sur la reconnaissance des diplômes et des grades de l’enseignement supérieur. Cet accord est <strong>entré en vigueur le 11 décembre 2007 </strong>et est paru au Journal officiel du 12 janvier 2008 (décret n°2008-34). Vous pouvez le consulter sur <a href="http://www.legifrance.gouv.fr/" class="spip_out" rel="external">Legifrance.gouv.fr</a>. Cet accord est conclu pour <strong>une durée de cinq ans</strong>, au-delà de laquelle il est tacitement reconduit par périodes d’un an.</p>
<p>Cet accord vise à encourager la mobilité des étudiants de chacun des deux pays en leur facilitant la possibilité de poursuivre leurs études dans l’autre pays et à favoriser l’intégration des étudiants sur le marché du travail grâce à une reconnaissance bilatérale des grades et des diplômes de l’autre pays.</p>
<p>Le présent accord s’applique aux diplômes délivrés et aux périodes d’études validées par les établissements dispensant en France des formations post-baccalauréat qui conduisent à un diplôme délivré sous l’autorité de l’Etat (universités, écoles supérieures et classes post-baccalauréat des lycées).</p>
<p>Dans le domaine professionnel, cet accord a pour objet de faciliter :</p>
<ul class="spip">
<li>la reconnaissance mutuelle des diplômes et des grades en vue d’accéder aux emplois publics de chacun des Etats ;</li>
<li>la reconnaissance, telle qu’elle est prévue par cet accord, d’un diplôme, d’une attestation de période d’études ou d’un grade académique déterminé, obtenu dans l’un des Etats pour qu’elle produise, dans l’autre Etat, les effets professionnels attachés par les législations nationales respectives à ces diplômes et grades académiques.</li></ul>
<p>En Espagne, ces effets professionnels ne dispensent pas de l’<i>homologación </i>du diplôme français au diplôme espagnol officiel spécifique exigée pour l’exercice des professions réglementées. Le « Ministerio de Educación y Ciencia » est compétent pour la reconnaissance du grade académique correspondant aux études officielles de <i>Grado</i> et, pendant la période de transition, pour la reconnaissance des grades académiques de <i>diplomado</i> et <i>licenciado</i>.</p>
<p><strong>Règles communautaires en matière de reconnaissance mutuelle des diplômes</strong></p>
<p>Vous trouverez des informations sur ce sujet sur les sites Internet suivant :</p>
<ul class="spip">
<li><a href="http://ec.europa.eu/eures/" class="spip_out" rel="external">Portail européen sur la mobilité de l’emploi (EURES)</a> ;</li>
<li><a href="http://ec.europa.eu/index_fr.htm" class="spip_out" rel="external">Commission européenne</a></li>
<li><a href="http://www.touteleurope.eu/fr/actions/social/education-formation/presentation/l-enseignement-superieur-dans-l-ue/la-reconnaissance-des-diplomes-dans-l-ue.html" class="spip_out" rel="external">Toute l’Europe</a></li>
<li><a href="http://enseignementsup-recherche.gouv.fr/cid20949/la-reconnaissance-des-diplomes-dans-union-europeenne.html" class="spip_out" rel="external">Ministère de l’enseignement supérieur et de la recherche</a>.</li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Modèles de CV</h3>
<p>Des modèles de CV figurent sur différents sites d’emploi, comme <a href="http://www.laboris.net/static/ca_curriculum_modelos.aspx" class="spip_out" rel="external">Laboris.net</a>.</p>
<p>Site à consulter : <a href="http://europass.cedefop.europa.eu/europass/home/vernav/Europass%2BDocuments/Europass%2BCV.csp?loc=fr_FR" class="spip_out" rel="external">Europass</a>.</p>
<p><i>Mise à jour : avril 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/emploi-stage/article/curriculum-vitae). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
