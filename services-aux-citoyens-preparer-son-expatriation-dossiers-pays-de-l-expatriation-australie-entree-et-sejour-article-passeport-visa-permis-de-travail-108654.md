# Passeport, visa, permis de travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/entree-et-sejour/article/passeport-visa-permis-de-travail-108654#sommaire_1">eVisitor</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/entree-et-sejour/article/passeport-visa-permis-de-travail-108654#sommaire_2">Visas</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/entree-et-sejour/article/passeport-visa-permis-de-travail-108654#sommaire_3">Sites à consulter</a></li></ul>
<p><strong>Important</strong> : l’ambassade d’Australie à Paris <strong>ne traite plus les demandes de visa</strong>. Pour les personnes résidant en France, et suivant la catégorie de visa, les demandes doivent être formulées soit par voie électronique soit auprès de <a href="http://www.spain.embassy.gov.au/madrcastellano/103.html" class="spip_out" rel="external">l’ambassade d’Australie à Madrid</a>.</p>
<p>Les autorités australiennes ont généralisé le visa électronique (e-visa). Aucune vignette n’est apposée dans le passeport, la délivrance du visa demandé étant notifiée par messagerie électronique (courriel précisant la date de délivrance ainsi que les caractéristiques du visa). Une fois sur place c’est le ministère australien de l’Immigration et de la citoyenneté qui prend le relais. <strong>En aucun cas, vous n’êtes autorisé à travailler en Australie sans permis adéquat.</strong></p>
<p>Les conditions d’entrée et de séjour sur le territoire australien relevant de la compétence exclusive des autorités du pays, le consulat de France en Australie <strong>n’est pas l’organisme compétent</strong> pour répondre à vos demandes d’information concernant votre séjour en Australie. Il est donc fortement recommandé aux intéressés d’anticiper leur demande de visa en fonction de leurs dates de séjour.</p>
<h3 class="spip"><a id="sommaire_1"></a>eVisitor</h3>
<p>Pour un séjour d’une <strong>durée maximale de 3 mois</strong> (tourisme, voyage d’affaires, visite privée ou familiale), le détenteur d’un passeport français doit être en possession d’une autorisation électronique de voyage (eVisitor - subclass 651). La <a href="http://www.immi.gov.au/e_visa/evisitor.htm" class="spip_out" rel="external">demande s’effectue en ligne</a>.</p>
<p>Aucune vignette n’est apposée dans le passeport mais un courriel de confirmation, précisant la date de délivrance et les conditions liées au visa, est transmis au demandeur.</p>
<p>Une demande d’eVisitor doit être formulée pour chaque membre d’une famille, y compris les enfants, qu’ils disposent de leur propre passeport ou qu’ils figurent sur le passeport de l’un des parents.</p>
<p>L’intéressé doit voyager avec le passeport déclaré lors de la demande d’eVisitor ; en cas de délivrance d’un nouveau titre de voyage, le détenteur doit en informer sans délai et en tout état de cause avant la date de son voyage, les autorités australiennes.</p>
<p><strong>Éligibilité :</strong></p>
<ul class="spip">
<li>être en possession d’un passeport français valable 6 mois après la date de retour ;</li>
<li>être hors d’Australie lors de la demande et de la délivrance de l’eVisitor.</li></ul>
<p><strong>Conditions :</strong></p>
<ul class="spip">
<li>ne peut être renouvelé ;</li>
<li>ne permet pas de travailler ;</li>
<li>permet d’étudier ou d’effectuer un stage dans la limite de 3 mois.</li></ul>
<p><strong>Coût :</strong> gratuit.</p>
<p><strong>Délais</strong></p>
<p>La demande doit être formulée au moins deux semaines avant la date de voyage. Néanmoins, les demandes d’eVisitor sont généralement, et suivant les cas de figure, satisfaites relativement rapidement (de 3 à 10 jours).</p>
<p>NB/ Pour un séjour touristique d’une durée supérieure à 3 mois, la demande d’un visa touristique (subclass 676) doit être privilégiée.</p>
<h3 class="spip"><a id="sommaire_2"></a>Visas</h3>
<p><strong>Working Holiday Visa / Visa vacances travail</strong> (subclass 417)</p>
<p>La France et l’Australie ont signé un accord créant un programme « vacances-travail » destiné à permettre à de jeunes ressortissants (âgés de 18 à 30 ans) de chacun des deux pays de séjourner dans l’autre, à titre individuel, dans le but d’y passer des vacances, en ayant la possibilité d’y occuper une activité professionnelle salariée et de compléter ainsi les moyens financiers dont ils disposent. Le visa « vacances-travail » a une validité de 12 mois maximum.</p>
<p><strong>Important</strong> : il est fortement déconseillé de solliciter une demande de 2ème WHV (lequel répond à des conditions strictes d’éligibilité) sur la base de fausses déclarations. La coopération entre les différentes administrations australiennes étant particulièrement efficace, les personnes se risquant à frauder sont systématiquement appréhendées à leur arrivée en Australie et dans les 72h placées dans un vol retour sur la France.</p>
<p><strong>Avertissement</strong> : travailler en Australie ne peut se faire qu’en possession d’un visa autorisant une activité rémunérée et dans les limites (notamment d’heures de travail) spécifiées par le visa. Le travail illégal est passible de poursuites et peut entrainer de sévères sanctions telles que l’annulation du visa, l’expulsion du territoire australien et l’interdiction de séjour en Australie pour une période allant jusqu’à 3 ans.</p>
<p><strong>Visas pour les travailleurs qualifiés </strong></p>
<p>Les visas d’immigration qualifiée sont le meilleur moyen, pour le travailleur étranger qualifié et expérimenté, de résider et de travailler en Australie. Dans le cadre du programme général australien d’immigration qualifiée (<i>The General Skilled Migration program - GSM</i>), tout immigrant qualifié ou candidat-travailleur qualifié peut faire valoir ses qualifications universitaires et professionnelles, son expérience professionnelle et ses capacités linguistiques pour satisfaire aux conditions pour immigrer en Australie.</p>
<p>De nombreuses options de visas existent pour les travailleurs qualifiés.</p>
<p>Les autres catégories de visas sont répertoriées sur le site du <a href="http://www.immi.gov.au/visawizard/" class="spip_out" rel="external">ministère australien de l’Immigration et de la citoyenneté</a>. Cette ressource renseigne sur les spécificités propres à chaque visa, notamment leur durée et leurs conditions d’obtention : un système de points s’applique à la plupart de ces visas, le candidat devant remplir des conditions spécifiées (de langue notamment) pour atteindre le nombre de points requis.</p>
<h3 class="spip"><a id="sommaire_3"></a>Sites à consulter</h3>
<ul class="spip">
<li><a href="http://www.spain.embassy.gov.au/madr/home.html" class="spip_out" rel="external">le site de l’ambassade d’Australie à Madrid</a></li>
<li>ministère australien de l’Immigration et de la citoyenneté : <a href="http://www.immi.gov.au/" class="spip_out" rel="external">www.immi.gov.au/</a>et <a href="http://www.citizenship.gov.au/" class="spip_out" rel="external">www.citizenship.gov.au/</a>(pour connaître les conditions d’obtention de la nationalité australienne)</li>
<li><a href="http://www.eta.immi.gov.au/" class="spip_out" rel="external">Electronic Travel Authority (ETAT)</a></li></ul>
<p><i>Mise à jour : octobre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/entree-et-sejour/article/passeport-visa-permis-de-travail-108654). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
