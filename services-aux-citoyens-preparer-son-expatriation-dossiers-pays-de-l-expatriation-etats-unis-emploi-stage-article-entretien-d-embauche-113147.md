# Entretien d’embauche

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/emploi-stage/article/entretien-d-embauche-113147#sommaire_1">Conduite de l’entretien</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/emploi-stage/article/entretien-d-embauche-113147#sommaire_2">Apparence et attitude</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/emploi-stage/article/entretien-d-embauche-113147#sommaire_3">Négociation du salaire</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/emploi-stage/article/entretien-d-embauche-113147#sommaire_4">Questions préférées des recruteurs</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/emploi-stage/article/entretien-d-embauche-113147#sommaire_5">Erreurs à éviter</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/emploi-stage/article/entretien-d-embauche-113147#sommaire_6">Après l’entretien</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Conduite de l’entretien</h3>
<p>C’est au cours de l’entretien que le niveau de langue sera apprécié, pour déterminer la capacité à faire face aux situations rencontrées dans le cadre professionnel. Le degré de connaissances linguistiques exigées peut bien entendu varier en fonction de la nature de l’emploi et des responsabilités.</p>
<p>L’entretien, qui est le plus souvent individuel, dure en règle générale une heure. Pour de nombreux postes, les entreprises soumettent les candidats à des tests d’évaluation.</p>
<p>Si vous n’êtes pas résident aux Etats-Unis, l’entretien sera l’occasion d’évoquer les conditions de séjour, la nature du visa et les modalités pratiques de sa demande.</p>
<p>Les diplômes permettent de connaître le niveau théorique des candidats mais <strong>les expériences professionnelles sont des atouts essentiels</strong> aux Etats-Unis.</p>
<p><strong> <i>Elevator speech</i> </strong></p>
<p>L’<i>elevator speech </i>est très courant aux Etats-Unis : il s’agit de se présenter et mettre en valeur son projet efficacement en trente secondes. C’est un exercice délicat surtout lorsqu’on manque de préparation.</p>
<p>La présentation doit capter l’attention de l’interlocuteur, susciter son intérêt et dire beaucoup en peu de mots.</p>
<h3 class="spip"><a id="sommaire_2"></a>Apparence et attitude</h3>
<p>La façon de s’habiller pour un entretien doit être en accord avec la culture de l’entreprise. Dans les secteurs de la finance par exemple, privilégiez un code vestimentaire classique. Dans les secteurs de la publicité, des relations publiques, du design et de l’informatique, le code vestimentaire est plus laxiste.</p>
<p>Faites attention aux détails : chaussures propres et cirées ; maquillage, parfum et bijoux discrets.</p>
<p>Démarquez-vous par votre tenue ou votre attitude (un vêtement, un signe extérieur ou un détail insolite de votre carrière par exemple) : cela permettra au recruteur de vous distinguer par rapport à un autre candidat.</p>
<p>La ponctualité à l’entretien et la façon dont le candidat est préparé pour cet entretien permettent de savoir l’importance que ce dernier porte à son image.</p>
<p>Ces quelques détails permettent à l’employeur de déterminer si les candidats sont sérieux ou pas.</p>
<h3 class="spip"><a id="sommaire_3"></a>Négociation du salaire</h3>
<p>La négociation du salaire doit venir à la fin de l’entretien, lorsque les recruteurs ont eu un aperçu de votre valeur.</p>
<p>Ne soyez pas le premier à donner un prix, cela vous discréditerait.</p>
<p>Si un recruteur vous demande vos attentes, répondez que vous souhaiteriez connaitre leur grille des salaires.</p>
<h3 class="spip"><a id="sommaire_4"></a>Questions préférées des recruteurs</h3>
<ul class="spip">
<li>Présentez votre parcours professionnel</li>
<li>Pourquoi pensez-vous que vous êtes le bon candidat pour ce poste ?</li>
<li>Que connaissez-vous de notre entreprise ?</li>
<li>Parlez-moi de vos points forts</li>
<li>Parlez-moi de telle ou telle expérience</li>
<li>Quelles sont vos prétentions salariales ?</li>
<li>Avez-vous des questions ?</li></ul>
<h3 class="spip"><a id="sommaire_5"></a>Erreurs à éviter</h3>
<ul class="spip">
<li>Oublier de mentionner vos anciens responsables dans vos références ;</li>
<li>Mentir sur vos expériences ;</li>
<li>Etre trop présomptueux sur ses compétences ;</li>
<li>Ne pas poser de questions sur le poste ;</li>
<li>Etre désagréable ou acerbe ;</li>
<li>Arriver à l’entretien sans préparation ;</li>
<li>Etre en retard ;</li>
<li>Refuser de parler du salaire.</li></ul>
<h3 class="spip"><a id="sommaire_6"></a>Après l’entretien</h3>
<p>Après l’entretien, envoyer une lettre de remerciement peut être un bon moyen pour distinguer votre candidature. Mais attention : une lettre mal écrite peut aussi se retourner contre vous.</p>
<p>Ne pas rédiger de note trop courte et trop générale. Personnalisez votre lettre en relation avec l’entretien que vous venez de passer : cela permettra à votre interlocuteur de vous reconnaitre.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/emploi-stage/article/entretien-d-embauche-113147). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
