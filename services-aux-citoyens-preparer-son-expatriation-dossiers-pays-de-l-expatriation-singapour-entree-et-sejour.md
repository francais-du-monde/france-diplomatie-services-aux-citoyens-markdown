# Entrée et séjour

<h2 class="rub22902">Passeport, visa, permis de travail</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/entree-et-sejour/#sommaire_1">Visas</a></li></ul>
<p>Quel que soit le motif de votre séjour, renseignez-vous auprès de la section consulaire de l’ambassade de Singapour à Paris, qui vous informera sur la réglementation en matière d’entrée et de séjour à Singapour, réglementation que vous devrez impérativement respecter.</p>
<p>N’hésitez pas à consulter le site de l’<a href="http://www.mom.gov.sg/" class="spip_out" rel="external">Immigration and Checkpoints Authority</a> afin de connaître les modalités d’entrée de séjour dans le pays.</p>
<p>Une fois sur place c’est le <a href="http://www.mom.gov.sg/" class="spip_out" rel="external">Ministry of Manpower</a> qui prend le relais. <strong>En aucun cas, vous n’êtes autorisé à travailler à Singapour sans permis adéquat. </strong></p>
<p>Le Consulat de France à Singapour n’est pas l’organisme compétent pour répondre à vos demandes d’information concernant votre séjour à Singapour.</p>
<h3 class="spip"><a id="sommaire_1"></a>Visas</h3>
<p><strong>Les titulaires de passeports français</strong> n’ont <strong>pas besoin de visa</strong> pour se rendre à Singapour pour un <strong>séjour de moins de trois mois</strong> s’ils remplissent les conditions suivantes :</p>
<ul class="spip">
<li>être en possession d’un passeport valide plus de six mois après la date d’entrée à Singapour ;</li>
<li>avoir un vol de retour confirmé ;</li>
<li>disposer de fonds suffisants.</li></ul>
<p>Depuis novembre 2008, les ressortissants de l’UE venant à Singapour en visite (tourisme) de courte durée se voient <strong>attribuer une autorisation de séjour de 90 jours</strong> et non plus de 30 jours renouvelables comme auparavant.</p>
<p><strong>Attention</strong></p>
<p>Après plusieurs sorties consécutives du territoire singapourien, vous risquez d’être refoulé et d’avoir à rentrer en France quelques mois avant de pouvoir revenir. Vous pourrez aussi être amené(e) à justifier votre présence (prospection par exemple) ou à apporter la preuve que vous disposez d’un sponsor (entreprise, local ou résident permanent).</p>
<p><strong>Cartes de séjour</strong></p>
<p>Il existe plusieurs catégories de titres de travail et de séjour à Singapour.</p>
<p>L’<i>Employment Pass</i> se décline en plusieurs catégories selon les critères applicables :</p>
<ul class="spip">
<li><strong>P Pass</strong> : P1 pour un salaire mensuel de plus de 8000 dollars de Singapour (SGD),</li>
<li><strong>P2 </strong>pour les salaires compris entre 4500 et 8000 SGD. Ils sont délivrés pour un ou deux ans, puis renouvelables pour des périodes d’un à trois ans. Ils concernent les étrangers titulaires d’un diplôme d’études supérieures.</li></ul>
<p>Les détenteurs du <i>P Pass</i> peuvent faire venir conjoint (couple marié) et enfants de moins de 21 ans sous le statut du <i>Dependant Pass (DP)</i>. Le conjoint non marié peut venir à Singapour grâce au <i>Long Term Social Visit Pass (LTSVP)</i>. La durée de validité du DP et LTSVP est calquée sur celle de l’Employment Pass du conjoint.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <i>Q Pass</i> : pour les personnes ayant un diplôme technique ou un salaire compris entre 3000 et 4500 SGD. Le conjoint marié et les enfants de moins de 21 ans ont le droit d`accompagner le titulaire du <i>Q Pass</i>, qui est délivré pour un an ou deux et est ensuite renouvelable.</p>
<p>Les conjoints des titulaires d’un <i>Employment Pass</i> qui disposent eux-mêmes d`un <i>Dependant Pass</i> peuvent obtenir une autorisation de travailler appelée <i>Letter of Consent</i>. Elle est plus facile a obtenir qu`un <i>Employment Pass</i> (pas de salaire minimum), sa durée de validité dépend de celle du <i>Dependant Pass</i>.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Le <i>S Pass</i> : pour les salaires d`au moins 2200 SGD par mois. Cette catégorie de titre de travail vise à fournir une main d`oeuvre étrangère adaptée aux besoins de l’industrie en personnel qualifié de niveau moyen. Les détenteurs de <i>S Pass</i> dont le salaire dépasse 4000 SGD peuvent faire venir conjoints et enfants de moins de 21 ans sous le statut du <i>Dependant Pass</i>.</p>
<p>Les personnes qui demandent l’un de ces trois <i>Pass</i> doivent bénéficier du support d’une société enregistrée à Singapour (considérée comme un sponsor). En général, il s`agit de l’entreprise qui emploie la personne.</p>
<p>Le <i>Work Permit</i> concerne les travailleurs semi ou non qualifiés. Pas de critère de salaire L’entreprise qui embauche des travailleurs étrangers relevant du régime du Work Permit doit respecter certains quotas de travailleurs singapouriens en fonction de son secteur d`activité. Les titulaires d’un Work Permit ne peuvent faire venir leur famille.</p>
<p>Le <i>Training Employment Pass</i> est destiné aux étudiants fréquentant une liste limitative d’établissements d’enseignement ou aux stagiaires de sociétés étrangères, et percevant un salaire d’au moins 3000 SGD. Valable trois mois, il n’est pas renouvelable.</p>
<p>Le <i>Student Pass</i> s`adresse aux étudiants étrangers faisant une partie ou la totalité de leur scolarité dans une école ou université de Singapour.</p>
<p>Ce <i>Pass</i> permet :</p>
<ul class="spip">
<li>aux étudiants de plus de 14 ans inscrits dans l’un des 45 établissements d’enseignement listés sur le site du <a href="http://www.mom.gov.sg/foreign-manpower/working-in-singapore/Pages/employment-of-foreign-students.aspx" class="spip_out" rel="external">Ministry of Manpower</a> de travailler pendant les vacances scolaires sans avoir à faire une demande de Work Permit.</li>
<li>aux étudiants appartenant à l’une des 22 universités listées sur cette même page web de travailler tout au long de l’année, dans la limite de 16h par semaine, sans avoir à demander un Work Permit.</li></ul>
<p>The National University Of Singapore, the Singapore Management of Singapore ainsi que l’INSEAD font partie de ces deux listes. Le Lycée français appartient à la première liste.</p>
<p>L’<i>Entrepass</i> est un titre spécifique accordé à l’entrepreneur étranger prêt à créer une entreprise à Singapour. Cette entreprise, dont l’entrepreneur étranger doit détenir au moins 30% du capital, doit être enregistrée en tant que <i>Private Limited Company</i> auprès de l’<a href="http://www.acra.gov.sg/" class="spip_out" rel="external">Accounting and Corporate Regulatory Authority</a> (ACRA). Une caution d’une banque singapourienne d’un montant de 50 000 SGD est nécessaire. L’<i>Entrepass</i>, valide deux ans, est renouvelable tous les trois ans aussi longtemps que l’activité professionnelle mise en place reste viable.</p>
<p>Il donne également à son titulaire la possibilité de faire venir son conjoint et ses enfants de moins de 21 ans.</p>
<p><strong>Attention :</strong> la réglementation singapourienne en matière de droit au séjour des étrangers étant très évolutive, il est prudent, avant de concrétiser tout projet de séjour à Singapour, de consulter les sites suivants :</p>
<ul class="spip">
<li><a href="http://www.ica.gov.sg/" class="spip_out" rel="external">Immigration and Checkpoints Authority</a></li>
<li><a href="http://www.mom.gov.sg/" class="spip_out" rel="external">Ministry of Manpower</a></li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-singapour-entree-et-sejour-article-animaux-domestiques.md" title="Animaux domestiques">Animaux domestiques</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-singapour-entree-et-sejour-article-vaccination-110481.md" title="Vaccination">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-singapour-entree-et-sejour-article-demenagement-110480.md" title="Déménagement">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-singapour-entree-et-sejour-article-passeport-visa-permis-de-travail-110479.md" title="Passeport, visa, permis de travail">Passeport, visa, permis de travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/entree-et-sejour/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
