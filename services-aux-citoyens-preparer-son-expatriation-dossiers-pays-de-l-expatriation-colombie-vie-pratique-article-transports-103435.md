# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/vie-pratique/article/transports-103435#sommaire_1">Importation de véhicules </a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/vie-pratique/article/transports-103435#sommaire_2">Permis de conduire</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/vie-pratique/article/transports-103435#sommaire_3">Code de la route</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/vie-pratique/article/transports-103435#sommaire_4">Assurances et taxes</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/vie-pratique/article/transports-103435#sommaire_5">Achat et location</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/vie-pratique/article/transports-103435#sommaire_6">Entretien</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/vie-pratique/article/transports-103435#sommaire_7">Réseau routier</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/vie-pratique/article/transports-103435#sommaire_8">Transports en commun</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Importation de véhicules </h3>
<p>L’achat et l’importation d’un véhicule, neuf ou d’occasion, doit être réalisé sur place, les formalités relevant du concessionnaire. Pour éviter de compliquer la procédure il est déconseillé d’importer un véhicule lors du déménagement.</p>
<h3 class="spip"><a id="sommaire_2"></a>Permis de conduire</h3>
<p>Les Français de passage en Colombie peuvent circuler pendant les trois premiers mois avec un permis de conduire international.</p>
<p>L’accord de réciprocité en matière de permis de conduire est actuellement suspendu entre la France et la Colombie. De ce fait, les Français qui souhaitent s’établir en Colombie doivent <strong>circuler avec un permis de conduire colombien</strong>. Ce permis leur sera délivré par le Ministère colombien des Transports après <strong>évaluation théorique et pratique</strong>.</p>
<h3 class="spip"><a id="sommaire_3"></a>Code de la route</h3>
<p>La conduite est très agressive et le code de la route est souvent peu respecté. De nombreux accidents sont à déplorer, il est donc conseiller d’être très prudent.</p>
<h3 class="spip"><a id="sommaire_4"></a>Assurances et taxes</h3>
<p>L’assurance tout risque est obligatoire pour les véhicules neufs bien qu’elle ne couvre en réalité que 90% des risques, un grand nombre de véhicules (notamment des taxis) n’étant pas assuré. Pour les véhicules d’occasion seule l’assurance au tiers est obligatoire mais il est conseillé de la compléter par une assurance tout risque.</p>
<h3 class="spip"><a id="sommaire_5"></a>Achat et location</h3>
<p>Pratiquement toutes les marques européennes, américaines et asiatiques sont représentées en Colombie mais tous les modèles n’existent pas. L’achat peut se faire chez un concessionnaire ou par petites annonces, le véhicule reste dans tous les cas soumis à la TVA.</p>
<p>Les enseignes internationales de location sont présentes dans les aéroports et les villes mais les tarifs sont élevés (mieux vaut alors réserver depuis la France.) Il faut compter entre 50 et 150 € TTC selon le modèle choisi.</p>
<p>En général il est déconseillé aux français de passage de louer une voiture pour des raisons de sécurité. Si vous décidez quand même de louer, veillez à ne pas partir sans roue de secours, réserve de carburant, téléphone portable et provisions et choisissez de préférence un 4x4. Evitez tant que possible les petites routes de campagnes où peuvent sévir guérillas et paramilitaires.</p>
<h3 class="spip"><a id="sommaire_6"></a>Entretien</h3>
<p>Les véhicules peuvent être entretenus sur place. Des pièces détachées de véhicules de marque européennes sont disponibles sur place. Attention, bien veiller à ce que les pièces usagées soient remplacées par des pièces réellement neuves.</p>
<h3 class="spip"><a id="sommaire_7"></a>Réseau routier</h3>
<p>Le réseau routier est plutôt en mauvais état, surtout dans les axes qui relient les grandes villes.</p>
<h3 class="spip"><a id="sommaire_8"></a>Transports en commun</h3>
<p>La majorité des déplacements dans le pays se font en <strong>avion</strong>. Le réseau des vols intérieur est d’ailleurs le plus dense d’Amérique Latine. Les vols sont assez chers, mais permettent de gagner un temps considérable et sont le choix le plus sûr pour les déplacements de ville à ville. <i>Avianca</i> et <i>Aero República</i> proposent des <i>pass</i> à ceux qui effectuent plusieurs vols intérieurs. Le mieux est de se renseigner au guichet de chaque compagnie à l’aéroport de Bogotá.</p>
<p><strong>Les bus</strong> présentent l’avantage d’être économiques, efficaces et d’offrir un réseau très étendu. Il faut toutefois être prudent pour des raisons de sécurité et éviter de prendre les transports en commun la nuit. Cependant l’état des routes de campagne et les très nombreux arrêts rendent les trajets très longs (plus de 20h pour relier Bogota à Carthagène.) Les compagnies sont nombreuses et les destinations sont généralement desservies plusieurs fois par jour. À l’exception des périodes de Noël et de Pâques, on peut acheter son billet le jour même au terminal central. Dans les villes, les bus sont lents et bondés, mieux vaut opter pour les taxis qui restent bon marché et acceptent les longs trajets. Par sécurité mieux vaut ne jamais héler de taxi dans la rue, et n’emprunter que des radio-taxis préalablement contactés par téléphone.</p>
<p>À côté des bus, on trouve aussi des chivas (vieux bus en bois, circulant surtout sur les petites routes) et les colectivos (taxis collectifs utilisés pour des trajets fixes).</p>
<h4 class="spip">Autres moyens de transport </h4>
<ul class="spip">
<li>Le trafic maritime, en particulier le long de la côte Pacifique, est irrégulier et plus que rudimentaire.</li>
<li>Le réseau ferroviaire est quasi inexistant.</li>
<li>Medellín est la seule ville colombienne dotée du <a href="http://www.metrodemedellin.gov.co/" class="spip_out" rel="external">métro</a> depuis 1995. Il est ouvert du lundi au samedi de 4 h 30 à 23 h, et le dimanche de 5 h à 22 h. Le ticket coûte l’équivalent de 1,50 US$.</li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/vie-pratique/article/transports-103435). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
