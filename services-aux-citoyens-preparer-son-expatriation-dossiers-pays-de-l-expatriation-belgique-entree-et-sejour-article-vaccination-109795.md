# Vaccination

<p>Aucune vaccination n’est obligatoire pour se rendre en Belgique. Cependant, il convient d’être à jour pour les vaccins suivants : diphtérie, poliomyélite, tétanos.</p>
<p>Pour en savoir plus, vous pouvez consulter notre thématique sur les <a href="services-aux-citoyens-preparer-son-expatriation-sante.md" class="spip_in">vaccinations</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/entree-et-sejour/article/vaccination-109795). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
