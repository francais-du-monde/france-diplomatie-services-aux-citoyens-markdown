# Légalisation par correspondance

<p>La légalisation des actes peut également être effectuée au Bureau des légalisations par correspondance. <br class="manualbr">Le délai de traitement est de 48 heures minimum.</p>
<p>La procédure s’effectue selon les modalités suivantes :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  les documents à légaliser doivent <strong>être clairement identifiés</strong> dans une lettre de présentation et leur <strong>pays de destination doit être clairement mentionné</strong> ;</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Veuillez joindre une enveloppe dûment timbrée (si en recommandé, joindre le formulaire rempli) et portant l’adresse de retour pour le renvoi des documents après légalisation  (par voie postale uniquement). </p>
<p><strong>Attention</strong> : votre courrier <strong>ne pourra pas</strong> être transmis à une ambassade étrangère par notre service.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  pour régler les frais afférents à cette procédure, il convient de joindre un chèque doit être libellé à l’ordre de « Régie des légalisations (DFAE) » <strong>(le règlement en espèces est refusé)</strong>.</p>
<p>Afin de déterminer le tarif exact, la nationalité du demandeur doit être également précisée dans votre courrier (joindre un justificatif si les documents ne l’indiquent pas).</p>
<p>Préalablement à votre envoi et pour éviter tout retour de vos courriers, il convient de respecter les conditions de recevabilité des documents destinés à cette procédure qui sont décrites dans le paragraphe précédent.</p>
<p>En cas de doute, vous pouvez joindre le Bureau des Légalisations :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  par téléphone au 01 53 69 38 28 ou 01 53 69 38 29 (permanence de 14h00 à 16h00) ; </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/legalisation-et-notariat/legalisation-et-certification-de-signatures/article/legalisation-par-correspondance#bureau.legalisation#mc#diplomatie.gouv.fr#" title="bureau.legalisation..åt..diplomatie.gouv.fr" onclick="location.href=mc_lancerlien('bureau.legalisation','diplomatie.gouv.fr'); return false;" class="spip_mail">par courriel</a>.</p>
<p><i>Mise à jour : juillet 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/legalisation-et-notariat/legalisation-et-certification-de-signatures/article/legalisation-par-correspondance). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
