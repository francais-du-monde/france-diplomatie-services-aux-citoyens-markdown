# Présence française

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/presence-francaise/#sommaire_1">Ambassade et consulats de France</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/presence-francaise/#sommaire_2">Autorités françaises dans le pays</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/presence-francaise/#sommaire_3">Associations dans le pays</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/presence-francaise/#sommaire_4">Presse française</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Ambassade et consulats de France</h3>
<p>Consulter notre <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/article/annuaire-des-representations-francaises-a-l-etranger" class="spip_in">annuaire des ambassades et consulats de France à l’étranger</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Autorités françaises dans le pays</h3>
<p><strong>Réseau français de coopération et d’action culturelle</strong></p>
<p>Les coordonnées des services du réseau de coopération et d’action culturelle français aux Etats-Unis ainsi que la description de l’activité de ces services se trouvent dans la rubrique culture du <a href="http://fr.ambafrance-us.org/spip.php?article342" class="spip_out" rel="external">site internet de l’Ambassade</a> ou sur le site internet du réseau culturel : <a href="http://latitudefrance.diplomatie.gouv.fr/AnnuaireCarto.html" class="spip_out" rel="external">Latitude France</a>.</p>
<p><strong>Economie : réseau mondial</strong></p>
<p>Business France, l’Agence française pour le développement international des entreprises, est présente aux Etats-Unis. Sa mission est d’accompagner les entreprises françaises dans leur développement à l’international.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://export.businessfrance.fr/etats-unis/bureaux-etats-unis-a-votre-service-a-l-export.html" class="spip_out" rel="external">Site de la Mission économique Business France aux Etats-Unis</a></p>
<p>Les services économiques sont également présents. Ils sont une émanation de la Direction générale du Trésor et ont pour missions principales l’analyse macroéconomique et l’animation des relations économiques bilatérales entre la France et les pays où ils sont implantés.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="https://www.tresor.economie.gouv.fr/Pays/etats-unis" class="spip_out" rel="external">Site internet du service économique français aux Etats-Unis</a></p>
<p><strong>Vos élus à l’Assemblée des Français de l’étranger</strong></p>
<p>Pour toute information sur l’Assemblée des Français de l’étranger et pour connaître les conseillers et les Sénateurs qui représentent les Français établis hors de France dans votre circonscription, vous pouvez consulter les sites Internet suivants :</p>
<ul class="spip">
<li><a href="http://www.assemblee-afe.fr/" class="spip_out" rel="external">Assemblée des Français de l’étranger (AFE)</a></li>
<li>Sénat au service des Français de l’étranger : <a href="http://www.senat.fr/expatries/dossiers_pays/etats_unis.html" class="spip_out" rel="external">dossier spécifique aux Etats-Unis sur le site du Sénat</a></li></ul>
<p><strong>Députés des Français de l’étranger</strong></p>
<p>La révision constitutionnelle du 23 juillet 2008 a introduit la représentation à l’Assemblée nationale des Français établis hors de France. La loi organique relative à l’élection des députés et sénateurs a été publiée au journal officiel du 19 avril 2011. La création de onze circonscriptions législatives à l’étranger a permis aux Français expatriés d’élire leur député à l’Assemblée nationale aux scrutins de mai et juin 2012.</p>
<p>Pour plus d’information : <a href="http://www.votezaletranger.gouv.fr/" class="spip_out" rel="external">Votez à l’étranger</a></p>
<h3 class="spip"><a id="sommaire_3"></a>Associations dans le pays</h3>
<p>Compte tenu du nombre particulièrement important des associations françaises présentes aux Etats-Unis, il est impossible d’en donner ici une liste exhaustive.</p>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li>Liste des associations françaises maintenue à jour par les Consulats généraux de France aux Etats-Unis :
<br>— <a href="http://www.consulfrance-atlanta.org/spip.php?article2087" class="spip_out" rel="external">Consulat général de France à Atlanta</a>
<br>— <a href="http://www.consulfrance-boston.org/spip.php?article787" class="spip_out" rel="external">Consulat Général de France à Boston</a>
<br>— <a href="http://www.consulfrance-chicago.org/spip.php?article397" class="spip_out" rel="external">Consulat Général de France à Chicago</a>
<br>— <a href="http://www.consulfrance-houston.org/spip.php?rubrique62" class="spip_out" rel="external">Consulat général de France à Houston</a>
<br>— <a href="http://www.consulfrance-nouvelleorleans.org/Associations-francaises.html" class="spip_out" rel="external">Consulat général de France à La Nouvelle-Orléans</a>
<br>— <a href="http://www.consulfrance-losangeles.org/spip.php?article1082" class="spip_out" rel="external">Consulat général de France à Los Angeles</a>
<br>— <a href="http://www.consulfrance-miami.org/spip.php?article525" class="spip_out" rel="external">Consulat général de France à Miami</a>
<br>— <a href="http://www.consulfrance-newyork.org/-Francais-" class="spip_out" rel="external">Consulat Général de France à New York</a>
<br>— <a href="http://www.consulfrance-sanfrancisco.org/spip.php?article347" class="spip_out" rel="external">Consulat général de France à San Francisco</a>
<br>— <a href="http://www.consulfrance-washington.org/spip.php?article18" class="spip_out" rel="external">Consulat Général de France à Washington</a></li>
<li>Notre article dédié aux <a href="services-aux-citoyens-preparer-son-expatriation-en-savoir-plus-article-associations-des-francais-de-l-etranger.md" class="spip_in">Associations des Français de l’étranger</a></li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Presse française</h3>
<p>Les quotidiens et hebdomadaires français sont disponibles plus ou moins facilement selon l’endroit où l’on se trouve. Des librairies assurent, notamment dans les grandes villes, la diffusion des livres français.</p>
<p><i>Mise à jour : juin 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-etats-unis-presence-francaise-article-presence-francaise.md" title="Présence française">Présence française</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/presence-francaise/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
