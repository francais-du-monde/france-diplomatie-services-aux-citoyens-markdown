# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/vie-pratique/article/transports#sommaire_1">Importation de véhicule</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/vie-pratique/article/transports#sommaire_2">Permis de conduire</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/vie-pratique/article/transports#sommaire_3">Code de la route</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/vie-pratique/article/transports#sommaire_4">Assurances et taxes</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/vie-pratique/article/transports#sommaire_5">Achat et location</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/vie-pratique/article/transports#sommaire_6">Immatriculation</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/vie-pratique/article/transports#sommaire_7">Entretien</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/vie-pratique/article/transports#sommaire_8">Réseau routier</a></li>
<li><a id="so_9" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/vie-pratique/article/transports#sommaire_9">Transports en commun</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Importation de véhicule</h3>
<p>Pour importer son véhicule au Luxembourg, il n’y a pas de formalités particulières à accomplir pour les véhicules provenant d’un autre Etat membre de l’Union européenne.</p>
<p>Cependant, le changement de résidence dans un autre Etat membre oblige l’expatrié à faire immatriculer son véhicule dès que possible dans son nouveau pays d’accueil, après son arrivée et surtout dans les six mois suivant son établissement.</p>
<p>Lors de l’achat d’un véhicule neuf ou d’occasion dans un pays membre de l’Union européenne (UE), l’acquéreur doit effectuer des démarches administratives au Luxembourg. Le régime de TVA est différent selon que le <strong>véhicule est neuf ou d’occasion</strong> et selon que l’acquéreur est une personne non assujettie à la TVA (<strong>un particulier</strong>) ou une personne assujettie à la TVA (<strong>une entreprise</strong>).</p>
<p>Le particulier est tenu de déposer une déclaration de TVA spéciale (formulaire 446-L) pour l’achat d’un véhicule neuf et les entreprises sont tenues de payer la TVA luxembourgeoise que le véhicule soit neuf ou d’occasion. Le particulier peut toutefois être exonéré du paiement de la TVA luxembourgeoise notamment dans le cas d’un transfert de résidence, ou si le particulier a un statut de diplomate ou de fonctionnaire européen.</p>
<p>Lorsqu’une personne achète un véhicule (neuf ou d’occasion) dans un pays hors de l’Union européenne (UE), elle doit effectuer certaines formalités de dédouanement. Ces formalités peuvent être accomplies soit au Luxembourg soit dans l’Etat membre d’entrée. Les démarches auxquelles sont soumis les acquéreurs de <strong>véhicules neufs ou d’occasion</strong> englobent les formalités douanières liées au paiement des droits de douane (à hauteur de 10 % de la valeur d’achat du véhicule), de certains frais ainsi que les formalités liées au paiement de la TVA (15 % au Luxembourg).</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.ambafrance-lu.org/-Section-consulaire-" class="spip_out" rel="external">Consulat de France au Luxembourg</a> rubrique " Informations pratiques / Formalités auprès des autorités locales / Importation et immatriculation d’un véhicule ;</li>
<li><a href="http://www.guichet.public.lu/citoyens/fr/transports-mobilite/transports-individuels/index.html" class="spip_out" rel="external">Guichet public - transports-individuels</a>.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Permis de conduire</h3>
<p>Au Luxembourg, le service permis de conduire de la Société nationale de contrôle technique (SNCT) est compétent pour toutes les opérations administratives afférentes aux permis de conduire.</p>
<p>Le permis de conduire des pays membres de l’Union européenne est reconnu par le Grand-Duché de Luxembourg. Il n’est pas nécessaire de l’échanger contre un permis local. Cependant, il est conseillé de le faire enregistrer auprès des services luxembourgeois qui établiront un double en cas de perte ou de vol.</p>
<p>L’obtention d’un permis de conduire, quelle que soit sa <a href="http://www.mt.public.lu/formulaires/circulation_routiere/perrmis_conduire/index.html" class="spip_out" rel="external">catégorie</a>, se fait par l’intermédiaire d’une <a href="http://www.snct.lu/" class="spip_out" rel="external">auto-école agréée</a> au Luxembourg qui se charge de l’introduction d’une demande afférente auprès de l’organisme compétent.</p>
<p>Les procédures et conditions d’obtention du permis, notamment l’âge minimum requis (entre 16 et 21 ans), peuvent varier d’une catégorie à l’autre.</p>
<p>Pour l’obtention d’un permis de conduire, le candidat devra suivre un <strong>apprentissage théorique</strong> d’au moins 12 heures traitant des règles générales de circulation et des particularités inhérentes à la catégorie de permis de conduire sollicitée (six heures, si l’intéressé est titulaire d’une autre catégorie de permis de conduire).</p>
<p>L’enseignement théorique est complété par un <strong>apprentissage pratique</strong> dont la durée varie selon la catégorie de permis de conduire sollicitée et l’aptitude du candidat. Elle dépend aussi du fait que l’intéressé est éventuellement déjà titulaire d’un permis d’une autre catégorie.</p>
<h3 class="spip"><a id="sommaire_3"></a>Code de la route</h3>
<p>Au Grand-Duché de Luxembourg, la vitesse maximale autorisée est de 50 km/h à l’intérieur des agglomérations, 90 km/h sur route, 130 km/h sur autoroute réduite à 110km/h par temps de pluie ou autres précipitations.</p>
<p>En cas de non-respect du code de la route, les sanctions sont immédiates.</p>
<p>Le stationnement des voitures est réglementé dans toutes les agglomérations et les infractions sont sévèrement sanctionnées par une mise en fourrière très rapide du véhicule.</p>
<p>Depuis le 1er novembre 2008, de nouvelles dispositions du code de la route sont applicables en ce qui concerne entre autres, l’utilisation des dispositifs spéciaux de retenue pour les enfants, le port de la ceinture de sécurité, etc.</p>
<p>Le taux d’alcoolémie maximal a été abaissé à 0,5 g/l.</p>
<h3 class="spip"><a id="sommaire_4"></a>Assurances et taxes</h3>
<p>Seule la responsabilité civile est obligatoire.</p>
<p>Vous devez vous assurer auprès d’une compagnie locale pour être autorisé à circuler. Les primes sont très élevées, calculées en fonction de la cylindrée et de la valeur du véhicule.</p>
<h3 class="spip"><a id="sommaire_5"></a>Achat et location</h3>
<p>Les prix des voitures neuves comme des voitures d’occasion sont dans l’ensemble moins élevés qu’en France. On retrouve toutes les marques françaises.</p>
<p>Les véhicules d’occasion sont en vente dans les garages ou dans les agences automobile.</p>
<h3 class="spip"><a id="sommaire_6"></a>Immatriculation</h3>
<p>En cas d’installation au Luxembourg, vous devez faire immatriculer votre véhicule dès que possible après votre arrivée et dans tous les cas dans un délai de six mois suivant votre établissement.</p>
<p>Il n’y a aucune difficulté pour obtenir cette nouvelle immatriculation à condition de fournir tous les documents réclamés par les autorités locales :</p>
<ul class="spip">
<li>demande en obtention d’une carte d’immatriculation (formulaire pré-imprimé, à compléter) ;</li>
<li>timbre de chancellerie d’une valeur de 50 euros ;</li>
<li>document permettant d’identifier le propriétaire légitime du véhicule (facture, contrat de vente, etc.) ;</li>
<li>document relatif au règlement en bonne et due forme de la TVA (quittance, numéro TVA, etc.) ;</li>
<li>attestation d’assurance confirmant la couverture du véhicule par une assurance RC (responsabilité civile) en cours de validité ;</li>
<li>document relatif au règlement en bonne et due forme des droits de douane (vignette "705" délivré par la douane) ;</li>
<li>certificat de conformité attestant la conformité du véhicule à un type homologué/réceptionné ;</li>
<li>document relatif à l’immatriculation du véhicule dans son pays de provenance (carte ou certificat d’immatriculation, attestation de mise hors circulation, etc.). Tous les véhicules automoteurs, remorques et semi-remorques soumis à l’immatriculation au Luxembourg doivent passer un contrôle technique. Ce contrôle est assuré dans l’un des trois centres de la Société nationale de contrôle technique (SNCT). Après la première mise en circulation, ce contrôle technique doit être répété par la suite selon une périodicité précise.</li></ul>
<h3 class="spip"><a id="sommaire_7"></a>Entretien</h3>
<p>Le prix des pièces détachées est dans l’ensemble moins élevé qu’en France.</p>
<p>On peut facilement effectuer des réparations sur son véhicule dans un garage mais celles-ci sont plus onéreuses qu’en France en raison du coût élevé de la main d’œuvre locale.</p>
<h3 class="spip"><a id="sommaire_8"></a>Réseau routier</h3>
<p>Le réseau routier est en très bon état.</p>
<h3 class="spip"><a id="sommaire_9"></a>Transports en commun</h3>
<p>Les transports publics sont excellents. Les services d’autocars sont nombreux et desservent tout le pays. Les parkings publics sont eux aussi nombreux et bon marché.</p>
<p>En centre-ville, les autobus ou les taxis collectifs facilitent les déplacements. Il est recommandé de se garer dans les parkings extérieurs à la ville.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/luxembourg/" class="spip_in">Fiche Luxembourg sur le site du ministère des Affaires étrangères</a></li>
<li><a href="http://www.mt.public.lu/" class="spip_out" rel="external">Le site du ministère des Transports du Grand-Duché de Luxembourg</a></li>
<li><a href="http://www.cfl.lu/" class="spip_out" rel="external">Le site de la Société nationale des chemins de fer luxembourgeois</a> (SNCFL)</li>
<li><a href="http://www.mobiliteit.lu/" class="spip_out" rel="external">Le site du réseau des transports publics au Grand-Duché de Luxembourg</a></li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/vie-pratique/article/transports). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
