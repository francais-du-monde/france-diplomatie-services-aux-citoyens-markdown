# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/vie-pratique/article/cout-de-la-vie-109124#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/vie-pratique/article/cout-de-la-vie-109124#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/vie-pratique/article/cout-de-la-vie-109124#sommaire_3">Alimentation</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>L’Allemagne fait partie de la zone euro.</p>
<p>Sans être aussi fréquent qu’en France - retraits dans les distributeurs automatiques exceptés -, l’usage de la carte de crédit se développe de plus en plus. Le paiement en liquide reste très répandu. Il est cependant conseillé de se munir d’une carte bancaire allemande – EC Karte – ce moyen de paiement étant accepté dans tous les magasins (ce qui n’est pas souvent le cas avec une carte de crédit).</p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<p>Les transferts de fonds ne posent pas de problème particulier. Il n’est pas nécessaire d’effectuer une déclaration auprès de la Banque centrale.</p>
<p>A noter que les banques sont fermées le samedi.</p>
<h3 class="spip"><a id="sommaire_3"></a>Alimentation</h3>
<p>Prix moyen d’un repas dans un restaurant Selon les villes, le prix d’un repas dans un restaurant de qualité moyenne à supérieure peut varier de 25 € à 80 €.</p>
<p>A noter qu’on ne laisse pas un pourboire sur une table : on indique le montant au moment de l’addition.</p>
<p><i>Mise à jour : juillet 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/vie-pratique/article/cout-de-la-vie-109124). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
