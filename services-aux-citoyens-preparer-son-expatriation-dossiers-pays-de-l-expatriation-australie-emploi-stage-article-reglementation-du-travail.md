# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/emploi-stage/article/reglementation-du-travail#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/emploi-stage/article/reglementation-du-travail#sommaire_2">Contrat de travail – spécificités</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/emploi-stage/article/reglementation-du-travail#sommaire_3">Les catégories d’emplois (<i>employment categories</i>)</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/emploi-stage/article/reglementation-du-travail#sommaire_4">La rupture du contrat (<i>termination of employment</i>)</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/emploi-stage/article/reglementation-du-travail#sommaire_5">Congés</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/emploi-stage/article/reglementation-du-travail#sommaire_6">Fêtes légales</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/emploi-stage/article/reglementation-du-travail#sommaire_7">Emploi du conjoint</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<p>Bien que l’Australie soit naturellement assimilée aux pays de culture anglo-saxonne comme le Royaume-Uni, le Canada, les Etats-Unis ou la Nouvelle Zélande, comme exemple de régime libéral de protection sociale et de droit du travail, ce pays s’est toujours vanté de posséder un système original de réglementation du travail très différent de celui qui existe en Amérique du Nord et au Royaume-Uni.</p>
<p>Dès le début, l’Australie a été marquée par un engagement très fort de l’Etat dans l’économie, par un mouvement syndical influent et une réglementation sociale et professionnelle innovante. Cela a très tôt abouti à des avancées en matière de salaires et de limitation de la durée du travail. Au début du 20ème siècle, l’Australie étaient considérée comme le « paradis des travailleurs », avec des durées hebdomadaires ou annuelles de travail bien en dessous de celles de la plupart des pays européens et d’Amérique du Nord.</p>
<p><strong>La loi sur les relations du travail</strong></p>
<p>Le cadre juridique régissant les relations du travail en Australie est le <i>Workplace Relations Act 1996</i>, loi fédérale entrée en vigueur le 1er janvier 1997 et amendée le 27 mars 2006 par le <i>Workplace Relations Amendment Act 2005</i>, aussi dénommé <i>Work Choices</i>.</p>
<p>Ce code favorise les conventions collectives du travail (<i>awards </i>ou <i>certified agreements</i>) du système fédéral qui fournit le cadre minimum régissant les conditions pour l’emploi (<i>The Australian Fair Pay and Conditions Standard</i>). La Commission australienne des relations industrielles (<i>The Australian Industrial Relations Commission</i> - AIRC), quant à elle, détermine une vingtaine de thèmes clés des conventions collectives fédérales du travail, comme par exemple :</p>
<ul class="spip">
<li>la classification des employés ;</li>
<li>les horaires de travail ;</li>
<li>les rémunérations et leur versement ;</li>
<li>les bonus et les primes ;</li>
<li>les différentes catégories d’absences et de congés ;</li>
<li>les congés annuels ;</li>
<li>les jours fériés ;</li>
<li>les procédures en cas de litige ou de licenciement abusif.</li></ul>
<p>Aujourd’hui, le droit du travail australien privilégie la souplesse, la flexibilité, la facilité d’embauche et de licenciement. La protection légale de l’emploi est parfois minimale et ce, en dépit d’une forte activité syndicale.</p>
<p><strong>Le temps de travail (ordinary hours)</strong></p>
<p>La durée légale hebdomadaire (<i>ordinary hours</i>) pour un emploi à temps plein est fixée à 38 heures (soit 7,6 heures par jour pour une semaine de cinq jours). Habituellement, la journée commence à 8 heures 30 et se termine à 17 heures 30, pause déjeuner comprise. Dans la pratique, la durée peut varier en fonction du contrat et du secteur d’activité. Certains contrats prévoient une durée hebdomadaire de 42 heures 30. Dans tous les cas, une personne ne peut être obligée à travailler plus de 38 heures par semaine auxquelles s’ajoute un nombre raisonnable d’heures supplémentaires.</p>
<p>Les heures supplémentaires sont payées 50% de plus pour les deux premières heures et 100% de plus pour les heures suivantes. Le nombre de jours de repos hebdomadaire est fixé à deux.</p>
<h3 class="spip"><a id="sommaire_2"></a>Contrat de travail – spécificités</h3>
<p>Le contrat (ou entente) de travail écrit (<i>Australian Workplace Agreement</i> - AWA) est un accord individuel passé entre l’employeur et son employé fixant les conditions de travail et la rémunération. A ce titre, il est toujours fait référence au <i>Australian Fair Pay and Conditions Standard</i>, lequel détermine le cadre légal de la relation de travail. Il comprend cinq conditions essentielles :</p>
<ul class="spip">
<li>le versement d’un salaire minimum de base, y compris pour les emplois temporaires et les vacations ;</li>
<li>les horaires de travail ;</li>
<li>le congé annuel ;</li>
<li>l’absence pour motif personnel (<i>personal leave</i>) ;</li>
<li>le congé parental non rémunéré (<i>unpaid parental leave</i>).</li></ul>
<p>Il existe deux principaux types de contrats de travail :</p>
<ul class="spip">
<li>le plus courant est le contrat permanent et individuel à durée indéterminée dénommé <i>ongoing</i> ou <i>permanent contract</i>. Il concerne aussi bien un emploi à temps plein qu’à temps partiel ;</li>
<li>le contrat à durée déterminée (<i>fixed term employment contract</i>).</li></ul>
<p>Un contrat de travail passé par écrit a une valeur légale. Il doit ensuite être soumis au bureau juridique de l’emploi (<i>Office of the Employment Advocate</i>) lequel est chargé de le compléter et de l’approuver. Cet organisme fournit également des conseils et des informations aux employeurs et aux salariés qui souhaitent passer un contrat entre eux, ainsi que des explications quant à l’interprétation de l’<i>Australian Fair Pay and Conditions Standard. </i>Il aide également toute personne, quelle que soit sa nationalité, ayant des besoins professionnels spécifiques. Ce bureau n’est toutefois pas habilité à vous trouver un emploi.</p>
<p>La période d’essai est de trois mois.</p>
<p>En Australie, on fait aussi référence au contrat de prestations (<i>contract for services</i>), notamment pour les professions libérales ou indépendantes (avocat, médecin, chauffeur de taxi, etc.). Il repose sur un engagement professionnel et une entente morale.</p>
<p><strong>Le temps partiel (<i>part-time</i>)</strong></p>
<p>Bien que les emplois à temps plein (<i>full time</i>) soient nombreux, les contrats à temps partiel se sont développés dans de nombreux domaines d’activités tels que la maintenance dans la construction, la restauration, la brasserie, le bâtiment, le nettoyage, l’informatique, l’industrie, les bureaux et la vente au détail. Toutefois, ils peuvent concerner tous les secteurs d’activité et tous les métiers. Le temps partiel a connu une forte progression chez les femmes et représente 30% de la main d’œuvre totale. Plus de 75% des emplois à temps partiel dans les entreprises de plus de vingt salariés concernent les métiers de la restauration, l’hôtellerie, l’éducation, la santé, les services à la personne et la vente.</p>
<p>Les emplois à temps partiels intéressent tous les échelons professionnels y compris les cadres. Le choix du temps partiel est motivé par des raisons familiales, de santé et de style de vie. Les employés à temps partiel sont généralement plus satisfaits de leur travail que les employés à temps plein. Ils sont généralement payés sur une base horaire, mais ne bénéficient pas des mêmes avantages que les employés à temps plein, notamment en matière de congés annuels et maternité, d’absence pour raison de santé, etc… Enfin, les salaires sont moins élevés que ceux des employés à temps plein.</p>
<p>De nombreuses entreprises font appel à des agences privées de consultants, appelées <i>bodyshops</i>, qui mettent à disposition du personnel contractuel. Le contrat à temps partiel prévoie vingt heures de travail par semaine.</p>
<h3 class="spip"><a id="sommaire_3"></a>Les catégories d’emplois (<i>employment categories</i>)</h3>
<p><strong>Employés permanents (<i>continuing </i>or <i>regular employees</i>)</strong></p>
<p>Il s’agit d’employés permanents auxquels font appel des employeurs souhaitant un personnel stable, disposant de compétences et de connaissances spécifiques et indispensables à la fonction. Il s’agit généralement d’un contrat de travail à durée indéterminée. Les horaires varient selon qu’il s’agit d’un travail à temps plein ou à temps partiel. Les modalités de résiliation du contrat sont indiquées dans la convention (<i>award</i>).</p>
<p><strong>Employés temporaires ou vacataires <i>(casual employees)</i> </strong></p>
<p>Il s’agit de personnel généralement recruté pour une période déterminée, occasionnelle ou pour les besoins d’une activité saisonnière, telle que la collecte de fruits. Le contrat est alors à durée déterminée et ne concerne que des emplois spécifiques. Le salaire est fixé à l’heure ou à la journée, voire en fonction du rendement. Ce type de contrat ne donne pas droit aux congés maladie ou annuel, mais la rémunération peut compenser l’absence de ces avantages sociaux. La loi de 1996 concernant les relations du travail protège les travailleurs victimes d’un licenciement abusif.</p>
<p><strong>Autres catégories d’emplois</strong></p>
<p>Il existe également :</p>
<ul class="spip">
<li>les employés à l’essai (<i>on a trial basis</i> ou <i>probationary employees</i>),</li>
<li>les apprentis ou stagiaires (<i>apprentices and trainees</i>)</li>
<li>les stagiaires bénévoles (<i>unpaid training and work experience</i>). Cette dernière catégorie concerne les étudiants autorisés à faire un stage, à acquérir une expérience professionnelle ou à rester en qualité d’observateur sur un lieu de travail donné.</li></ul>
<h3 class="spip"><a id="sommaire_4"></a>La rupture du contrat (<i>termination of employment</i>)</h3>
<p>Légalement, il est prévu un préavis de six mois, à condition qu’il soit spécifié dans le contrat de travail. Ce préavis peut, selon les cas, être plus court ou atteindre jusqu’à douze mois. Il est vivement conseillé de veiller à ce que les conditions de licenciement soient clairement mentionnées dans le contrat.</p>
<p><strong>Licenciement (<i>dismissal</i>)</strong></p>
<p>Il survient en cas de comportement incorrect de l’employé ou de rupture définitive de la relation de travail.</p>
<p><strong>Licenciement avec préavis (<i>dismissal by notice</i>)</strong></p>
<p>L’employeur informe par écrit, conformément aux termes du contrat, l’employé de la fin de leur relation de travail. Dans ce cas, l’employé peut organiser son départ et se mettre à la recherche d’un nouvel emploi, continuer à travailler jusqu’à son départ ou encore quitter les lieux sur le champ. Il sera payé jusqu’à son départ définitif de l’entreprise. Cette procédure est reprise sous le terme de <i>« pay in lieu of notice »</i>.</p>
<p><strong>Licenciement sommaire <i>(summary dismissal)</i> </strong></p>
<p>Il y a licenciement sommaire lorsque l’employeur licencie sans préavis un ’employé pour faute grave ou pour conduite répréhensible (désobéissance, négligence professionnelle, comportement à risque à l’égard des autres membres du personnel, alcoolisme, consommation de stupéfiants). Toutefois, l’employeur doit justifier les raisons de sa décision.</p>
<p><strong>La retraite (<i>age pension or retirement age</i>)</strong></p>
<p>Il n’y a pas à proprement parlé d’âge légal de départ à la retraite. Le départ à la retraite est négocié avec l’employeur. Cependant, les salariés peuvent prétendre à la retraite comme suit :</p>
<p>Pour les hommes, si les revenus et le capital sont inférieurs à un certain montant et à partir de 65 ans et plus ;</p>
<p>Pour les femmes, si les revenus et le capital sont inférieurs à un certain montant et entre 63 ans et demi et 65 ans suivant la date de naissance :</p>
<ul class="spip">
<li>personnes nées entre le 1er juillet 1944 et le 31 décembre 1945 = 63 ans et demi</li>
<li>personnes nées entre le 1er janvier 1946 et le 30 juin 1947 = 64 ans</li>
<li>personnes nées entre le 1er juillet 1947 et le 31 décembre 1948 = 64 ans et demi</li>
<li>personnes nées après le 1er janvier 1949 = 65 ans</li></ul>
<p><strong>La représentation syndicale (<i>unions</i>)</strong></p>
<p>Malgré une baisse des adhésions, les syndicats demeurent nombreux, organisés et puissants. Ils sont représentés par le <a href="http://www.actu.asn.au/" class="spip_out" rel="external">Conseil australien des syndicats</a> (<i>Australian Council of Trade Unions</i> - ACTU).</p>
<p>L’organisme d’entente et de négociations entrepreneuriales (<i>Enterprise Bargaining Agreement</i> - EBA) consiste en un accord industriel collectif entre :</p>
<ul class="spip">
<li>un employeur et un syndicat représentant des employés ou</li>
<li>un employeur et des employés agissant pour leur propre compte.</li></ul>
<p>L’adhésion à un syndicat est volontaire.</p>
<p>Au moment de l’embauche, l’employeur doit impérativement vous préciser à quelle convention collective (<i>award</i>) ou accord d’entreprise (<i>enterprise agreement </i>ou <i>certified agreement</i>) l’emploi que vous occuperez est rattaché. Cette information doit apparaître dans la lettre d’embauche (<i>letter of offer</i>).</p>
<p><strong>La négociation d’entreprise (<i>enterprise bargaining </i>ou <i>workplace bargaining</i>)</strong></p>
<p>La négociation d’entreprise est le processus au cours duquel employeurs et employés négocient un certain nombre de règles et de conditions relatives à l’emploi. Cette négociation aboutit à un accord d’entreprise (<i>entreprise agreement</i>).</p>
<p>Les accords d’entreprise et les conventions collectives sont des documents juridiques établis par la Commission des relations du travail (<i>Industrial Relations Commission</i> - IRC). Ils stipulent les avantages sociaux et les rémunérations de base applicables à un emploi dans un secteur d’activité déterminé. Ils précisent également les responsabilités de l’employeur et de l’employé.</p>
<p>Vous ne devez pas accepter une rémunération inférieure à celle stipulée dans la convention collective ou l’accord d’entreprise. Il est conseillé, lors de la recherche d’un emploi, de prendre connaissance de la convention collective ou de l’accord d’entreprise applicable à la branche professionnelle qui vous intéresse. Enfin, il est préférable d’occuper un emploi soumis à une convention collective ou à un accord d’entreprise qui garantira le respect des conditions de travail.</p>
<h3 class="spip"><a id="sommaire_5"></a>Congés</h3>
<p>Les congés annuels rémunérés s’ajoutent aux jours fériés. A l’exception des emplois temporaires ou vacataires, le congé autorisé est de quatre semaines. Certaines catégories d’emplois aux horaires particuliers bénéficieront d’une absence allant jusqu’à cinq semaines.</p>
<p>L’absence pour motif personnel est prévue et autorise dix jours payés par an, sauf pour les emplois temporaires ou les vacations. Deux jours d’absence supplémentaires peuvent être accordés, mais ils ne seront pas rémunérés. Les vacataires dans ce cas uniquement peuvent en bénéficier. En cas d’absence pour raison médicale, l’employeur est en droit d’exiger un certificat médical.</p>
<p>Le congé maternité n’est pas rémunéré. Il prévoit jusqu’à 52 semaines d’absence par an, auxquelles certains types d’employés temporaires peuvent prétendre. Ce congé peut débuter six semaines avant la date prévue de l’accouchement.</p>
<h3 class="spip"><a id="sommaire_6"></a>Fêtes légales</h3>
<p>Les gouvernements des Etats et des Territoires fixent, en général pour plusieurs années, les dates des fêtes légales au niveau national <i>(statutory public holidays</i>) et régional. Certaines fêtes nationales (notamment la fête du travail), bien que désignées sous le même nom, peuvent être chômées à des dates différentes dans les différents Etats et Territoires.</p>
<p>Certaines fêtes régionales ne sont pas chômées par tout le monde et peuvent être circonscrites à certaines régions ou à certaines villes. Les fêtes locales, selon l’Etat ou le Territoire, peuvent être désignées sous le nom de <i>show days, show holidays, regional holidays, non-statutory holidays, local public holidays</i> ou <i>special holidays</i>.</p>
<p><strong>Niveau national</strong></p>
<p>— <strong>Nouvel An</strong> <i>(New Year’s Day) </i> : le 1er janvier.<br>— <strong>Fête nationale </strong>(<i>Australia Day</i>) : le 26 janvier. Ce jour marque l’anniversaire de l’arrivée de la première flotte (<i>First Fleet</i>) et de la création de la première colonie européenne en Australie le 26 janvier 1788.<br>— <strong>Vendredi Saint </strong>(<i>Good Friday</i>), <strong>samedi de Pâques </strong>(<i>Easter Saturday</i>) et <strong>lundi de Pâques </strong>(<i>Easter Monday</i>) : fin mars et début avril.<br>— <strong>Commémoration de la bataille de Gallipoli</strong> (<i>Australian and New Zealand Army Corps Day </i>ou <i>ANZAC day</i>) : le 25 avril.<br>— <strong>Anniversaire de la Reine </strong>(<i>Queen’s Birthday</i>) : le 2ème lundi de juin, à l’exception de l’Australie occidentale (dernier lundi de septembre).<br>— <strong>Fête du travail </strong>(<i>Labour Day - Eight hours day </i>en Tasmanie) : le 1er lundi d’octobre, à l’exception du Queensland (1er lundi de mai), de l’Australie occidentale (1er lundi de mars), de la Tasmanie et de l’Etat de Victoria (2ème lundi de mars).<br>— <strong>Noël et lendemain de Noël </strong>(<i>Christmas and Boxing Day</i>) : les 25 et 26 décembre. Le lendemain de Noël est consacré à l’échange des cadeaux de Noël.</p>
<p>A noter que les gouvernements des Etats et Territoires peuvent décider que certains jours fériés tombant un samedi ou un dimanche peuvent être chômés le premier jour ouvrable qui suit.</p>
<p><strong>Niveau régional</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span> <strong>Territoire de la capitale australienne </strong> :
<br>— <i>Canberra Day</i> le 2ème lundi de mars
<br>— <i>Family and Community Day</i> le 1er mardi de novembre.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span> <strong>Territoire du Nord </strong> :
<br>— <i>May Day</i> le 1er lundi de mai
<br>— <i>Picnic Day</i> le 1er lundi d’août,
<br>— En juillet et août il y a plusieurs jours fériés appelés <i>show days</i> : <i>Alice</i> <i>Springs Show Day, Tennant Creek Show Day, Katherine Show Day, Darwin Show Day</i> et <i>Borroloola Show Day</i>.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span> <strong>Nouvelle Galles du Sud </strong> :
<br>— <i>Bank Holiday</i> le 1er lundi d’août, auquel viennent s’ajouter des jours et des demi-journées fériés (<i>local public holidays</i>), limités à certaines régions.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span> <strong>Queensland</strong> :
<br>— <i>Royal Queensland Show</i> (uniquement dans la région de Brisbane) le 2ème mercredi d’août,
<br>— jours fériés (<i>show holidays</i>) à l’occasion de foires agricoles, horticoles ou industrielles
<br>— jours (<i>special holidays</i>) consacrés à la commémoration d’évènements particuliers. Les <i>show holidays</i> et les <i>special holidays</i> sont limités à certaines régions.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span> <strong>Australie du Sud</strong> :
<br>— <i>Adelaide Cup </i>le 2ème lundi de mars.
<br>— Le lendemain de Noël (<i>Boxing Day</i>) se nomme au Queensland <i>Proclamation Day</i>.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span> <strong>Australie occidentale </strong> :
<br>— <i>Foundation Day </i>le 1er lundi de juin.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span> <strong>Tasmanie </strong> :
<br>— <i>Devonport Cup </i>la 1ère quinzaine de janvier,
<br>— <i>Royal Hobart Regatta </i>le 2ème lundi de février,
<br>— <i>Launceston Cup </i>le dernier mercredi de février,
<br>— <i>King Island Show </i>le 1er mardi de mars,
<br>— <i>Burnie show</i>, <i>Royal Launceston Show, Flinders Island Show, Royal Hobart Show, Recreation Day </i>et <i>Devonport Show</i>. Toutes ces fêtes régionales sont limitées à certaines villes ou à certains districts.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span> <strong>Victoria </strong> :
<br>— <i>Melbourne Cup Day </i>le 1er mardi de novembre, auquel viennent s’ajouter de nombreux jours fériés, géographiquement limités à certaines villes ou régions, à l’occasion de foires et de manifestations.</p>
<h3 class="spip"><a id="sommaire_7"></a>Emploi du conjoint</h3>
<p>Le conjoint en possession d’un visa de la catégorie « immigration qualifiée » peut travailler en Australie dans le secteur privé. Une bonne connaissance et une bonne pratique de l’anglais sont indispensables. La demande de visa doit se faire avant le départ. L’occupation illégale d’un emploi est formellement interdite.</p>
<p>La France et l’Australie ont signé à Adélaïde le 2 novembre 2001 un accord sur l’emploi des personnes à charge des membres des missions officielles. Cet accord est en vigueur entre les deux pays depuis le 1er mai 2004 (décret n°2004-369 du 22 avril 2004 paru au Journal Officiel du 29 avril 2004). Cet accord permet aux conjoints des agents des missions diplomatiques ou consulaires françaises d’exercer une activité professionnelle salariée en Australie.</p>
<p>En ce qui concerne les conditions d’obtention d’un visa dans le cadre d’un regroupement familial avec un conjoint australien, vous pouvez consulter le site du <a href="http://www.immi.gov.au/" class="spip_out" rel="external">ministère australien de l’Immigration et de la citoyenneté</a> (<i>Department of Immigration and Citizenship</i>).</p>
<p><i>Mise à jour : octobre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/emploi-stage/article/reglementation-du-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
