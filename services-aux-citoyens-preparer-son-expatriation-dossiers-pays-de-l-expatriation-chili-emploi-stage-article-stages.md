# Stages

<p>Des possibilités de stages dans les entreprises françaises, franco-chiliennes ou chiliennes existent. La <a href="http://www.camarafrancochilena.cl/" class="spip_out" rel="external">Chambre de commerce et d’industrie franco-chilienne</a> a entrepris de constituer un fichier des demandeurs de stages en entreprise. Dans la mesure de ses possibilités, elle essaie de les transmettre à son réseau d’hommes d’affaires et d’entreprises.</p>
<p><strong>Avez-vous pensé au Volontariat International (V.I.E ou V.I.A.) ?</strong></p>
<p>Le V.I.E. permet aux entreprises françaises de confier à un jeune ressortissant de l’espace économique européen, âgé de 18 à 28 ans, une mission professionnelle (rémunérée) à l’étranger durant une période modulable de 6 à 24 mois.</p>
<p><i>Pour en savoir plus :</i></p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/emplois-stages-concours/volontaires-internationaux/" class="spip_in">Page thématique sur le volontariat international sur notre site</a></li>
<li><a href="http://www.civiweb.com/FR/index.aspx" class="spip_out" rel="external">http://www.civiweb.com/FR/index.aspx</a></li>
<li><a href="http://export.businessfrance.fr/default.html" class="spip_out" rel="external">Business France</a> - <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/emploi-stage/article/stages#infoVIE#mc#ubifrance.fr#" title="infoVIE..åt..ubifrance.fr" onclick="location.href=mc_lancerlien('infoVIE','ubifrance.fr'); return false;" class="spip_mail">Courriel</a></li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/emploi-stage/article/stages). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
