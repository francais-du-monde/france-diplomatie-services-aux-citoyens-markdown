# Convention fiscale

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/fiscalite/article/convention-fiscale-108400#sommaire_1">Champ d’application de la convention</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/fiscalite/article/convention-fiscale-108400#sommaire_2">Règles d’imposition</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Champ d’application de la convention</h3>
<p>Cet accord a pour objet de protéger les résidents de chacun des Etats contractants en matière d’impôts prélevés directement sur le revenu ou sur les sociétés. La Convention trouve donc à s’appliquer aux résidents de ces Etats.</p>
<p><strong>1. Notion de résidence</strong><br class="manualbr">L’article 4, paragraphe 2 de la Convention s’applique aux personnes qui sont considérées comme "résidents d’un Etat contractant" ou de chacun de ces deux Etats.<br class="manualbr">D’après ce même article de la Convention, une personne est considérée comme "résident d’un Etat contractant" lorsque, en vertu de la législation dudit Etat, elle se trouve assujettie à l’impôt à raison de son domicile, de sa résidence ou de critères analogues.<br class="manualbr">Au paragraphe 2, l’article 4 fournit des critères subsidiaires permettant de résoudre le cas de double résidence si l’assujettissement à l’impôt ne pouvait suffire. Ces critères sont :</p>
<ul class="spip">
<li>un foyer d’habitation permanent (il s’agit, par exemple, du lieu de situation du conjoint ou des enfants) ;</li>
<li>l’Etat où la personne possède le centre de ses intérêts vitaux (tant professionnels que privés) ;</li>
<li>l’Etat dans lequel elle séjourne de façon habituelle (notion de 183 jours de présence physique sur le territoire au cours d’une année fiscale) ;</li>
<li>à défaut, l’Etat dont elle possède la nationalité.</li></ul>
<p><strong>Dispositions conventionnelles sur certaines catégories de revenus</strong></p>
<p>Cette analyse ne tient pas compte des dispositions conventionnelles relatives aux revenus de capitaux mobiliers.</p>
<p><strong>2. Traitements, salaires, rémunérations privées</strong></p>
<p><strong>Principe :</strong> l’article 15, paragraphe 1 précise que les traitements et salaires d’origine privée ne sont, en règle générale, imposables que dans l’Etat où <strong>s’exerce l’activité personnelle</strong>.</p>
<p><strong>Exceptions à cette règle générale</strong></p>
<p>Le maintien de l’imposition dans l’Etat de résidence du bénéficiaire est prévu par le paragraphe 2 du même article sous réserve de trois conditions simultanément remplies :</p>
<ul class="spip">
<li>le séjour temporaire du bénéficiaire dans l’autre Etat ne dépasse pas une durée totale de 183 jours ;</li>
<li>la rémunération est payée par un employeur qui n’est pas résident de l’Etat d’exercice ;</li>
<li>la rémunération ne doit pas être à la charge d’un établissement stable ou d’une base fixe de l’employeur dans l’Etat.</li></ul>
<p><strong>Exemple</strong> <br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> Monsieur X est envoyé à Mexico trois mois, soit 90 jours (mai, juin, juillet de l’année n) par une PME fabriquant des articles de maroquinerie en vue de prospecter le marché. Cette entreprise ne dispose d’aucune succursale ni bureau au Mexique. Dans un tel cas, Monsieur X devra déclarer ses revenus en France.<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> Au contraire, si Monsieur X est envoyé du mois de février inclus au mois de novembre inclus, son séjour de plus de 183 jours au Mexique entraîne son imposition dans ce pays.</p>
<p>Il résulte des dispositions du paragraphe 4 de l’article 15 de la Convention que les revenus professionnels des salariés autres qu’intérimaires employés à bord d’un navire, d’un aéronef en trafic international ou à bord d’un bateau servant à la navigation intérieure ne sont imposables que dans l’Etat où se trouve la direction effective de l’entreprise.</p>
<p><strong>3. Rémunérations publiques</strong></p>
<p><strong>Principe :</strong> L’article 19, paragraphe 1 indique que les traitements, salaires et rémunérations analogues ainsi que les <strong>pensions de retraite</strong> payés par un Etat ou une personne morale de droit public de cet Etat restent imposables dans cet Etat.</p>
<p><strong>Exemple</strong><br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> Monsieur X est fonctionnaire de l’Etat Français, résident en activité en France et décide d’aller prendre sa retraite chez l’un de ses enfants à Mexico. Les montants de ses pensions resteront imposés en France, son dossier étant pris en charge par le Centre des impôts des non-résidents.</p>
<p><strong>Exception</strong></p>
<p><strong>Toutefois</strong>, en vertu des dispositions du paragraphe 2 du même article, les règles fixées au paragraphe 1 dudit article ne sont pas applicables aux rémunérations ou aux pensions versées au titre <strong>de services rendus dans le cadre d’une activité industrielle ou commerciale exercée par un Etat ou une personne morale de droit public</strong>.</p>
<p>Les sommes versées à ce titre sont imposées soit dans l’Etat d’exercice de l’activité (article 15 de la Convention), soit dans l’Etat de résidence du bénéficiaire (article 18 de la Convention).</p>
<p><strong>Exemples </strong><br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> Monsieur X, agent E.D.F. est envoyé au Mexique afin d’effectuer des travaux de conception avec les services locaux. Monsieur X est rémunéré par E.D.F. France.<br class="autobr">E.D.F. étant un établissement de droit public à caractère industriel et commercial, les rémunérations allouées à Monsieur X seront imposées au Mexique.<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> Monsieur X, retraité de la S.N.C.F., perçoit une pension de la Caisse vieillesse de la S.N.C.F. Or, Monsieur X, résident en France a décidé de vivre à Mexico. Cette retraite se verra ainsi imposable au Mexique puisque la S.N.C.F. a le caractère d’un établissement public, industriel et commercial.</p>
<p><strong>3. Pensions et rentes</strong></p>
<p>L’article 18 prévoit que les pensions de retraite de source privée ainsi que les rentes viagères restent imposables dans l’Etat dont le bénéficiaire est un résident. Cette disposition couvre également les pensions publiques auxquelles les règles de l’article 19 ne sont pas applicables.</p>
<p><strong>Exemple </strong><br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> Monsieur X, citoyen français qui a exercé une activité salariée au Mexique, décide de venir prendre sa retraite en France. Les pensions versées par le Mexique au titre de cette activité sont imposables en France.</p>
<p><strong>4. Étudiants, stagiaires, enseignants, chercheurs</strong></p>
<p><strong>4.1. Étudiants, stagiaires</strong><br class="manualbr">L’article 20 de la Convention prévoit que les étudiants, les stagiaires d’un Etat qui séjournent dans l’autre Etat à seule fin d’y poursuivre leurs études ou leur formation et qui perçoivent des subsides d’origine étrangère à cet Etat sont exonérés d’impôt par ce dernier Etat.</p>
<p><strong>4.2. Enseignants, chercheurs</strong><br class="manualbr">L’article 15, paragraphe 3 précise que les rémunérations versées aux enseignants et aux chercheurs résidents d’un Etat se rendant temporairement dans l’autre Etat en vue d’y exercer une activité pédagogique ou de recherche pendant une période <strong>ne dépassant pas deux ans </strong>dans une université, un collège, une école ou un autre établissement d’enseignement ou de recherche restent imposables dans l’Etat de résidence.</p>
<p><strong>5. Autres catégories de revenus</strong></p>
<p><strong>5.1. Bénéfices industriels et commerciaux</strong><br class="manualbr">L’article 7, paragraphe 1 dispose que les entreprises industrielles et commerciales sont imposables sur le territoire où se trouve un établissement stable.</p>
<p><strong>5.2. Bénéfices des professions non commerciales et des revenus non commerciaux</strong><br class="manualbr">L’article 14, paragraphe 1 dispose que les revenus provenant de l’exercice d’une profession libérale ou d’autres activités indépendantes sont imposables dans l’Etat sur le territoire duquel se trouve l’installation permanente où s’exerce de façon régulière l’activité personnelle.</p>
<p>Les revenus que les professionnels du spectacle ainsi que les sportifs réalisent en cette qualité dans l’un des deux Etats, à titre indépendant, restent imposables dans l’Etat d’exercice de l’activité selon les dispositions de l’article 17 de la Convention, paragraphe 1.</p>
<p>L’article 12, paragraphe 2 pose en principe que les revenus non commerciaux (redevances et droits d’auteur) sont imposables dans l’Etat d’origine des revenus du bénéficiaire, sous couvert de la production de formulaires spécifiques remis auprès des autorités fiscales de l’Etat dont relève le créancier des revenus (15 %).</p>
<p><strong>5.3. Revenus immobiliers</strong><br class="manualbr">L’article 6, paragraphe 1 dispose que les revenus des biens immobiliers y compris les bénéfices des exploitations agricoles sont imposables dans l’Etat où ils sont situés.</p>
<p>Cette règle s’applique également aux gains provenant de la cession ou de l’échange desdits biens ou droits selon les dispositions de l’article 13, paragraphe 1. En ce qui concerne les gains provenant de l’aliénation de tous biens autres que mobiliers ou immobiliers, le paragraphe 5 de l’article 13 précise qu’ils restent imposables dans l’Etat de résidence du cédant.</p>
<h3 class="spip"><a id="sommaire_2"></a>Règles d’imposition</h3>
<p><strong>Elimination de la double imposition</strong><br class="manualbr">L’élimination de la double imposition pour les résidents de France qui perçoivent des revenus de source mexicaine s’opère aux termes du paragraphe 1 de l’article 21 selon le régime de l’imputation.</p>
<p>Les revenus de source française ou mexicaine pour lesquels le droit d’imposer est dévolu à titre exclusif au Mexique doivent être maintenus en dehors de la base de l’impôt français, réserve faite toutefois de leur prise en compte pour la détermination du taux effectif.</p>
<p>Le calcul du taux effectif se décompose tout d’abord par la détermination d’une cotisation de base correspondant à l’ensemble des revenus de source française et (ou) étrangère passible de l’impôt français suivant les règles de la législation interne.</p>
<p>Ensuite, l’impôt exigible sera égal au produit de la cotisation de base par le rapport entre le montant net total des revenus conventionnellement imposables en France et le montant total du revenu net d’après lequel le calcul de ladite cotisation de base a été effectué.</p>
<p>Au résultat ainsi obtenu peuvent être appliquées les réfactions prévues par la loi interne (crédit ou réduction d’impôt).</p>
<p><strong>Tableau récapitulatif</strong></p>
<table class="spip">
<thead><tr class="row_first"><th id="id67ec_c0"> </th><th id="id67ec_c1">Établissement stable au Mexique : régime mexicain</th><th id="id67ec_c2">Pas d’établissement stable au Mexique : convention franco-mexicaine</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="id67ec_c0">Bénéfices</td>
<td headers="id67ec_c1">30 % des profits attribuables à l’établissement<br class="autobr">
30 % sur les plus-values inflationnistes (imposées comme des revenus)</td>
<td headers="id67ec_c2">Impôt français, déductible de toutes dépenses faites au Mexique ou à l’étranger<br class="autobr">
30 % maximum des revenus de source mexicaine</td></tr>
<tr class="row_even even">
<td headers="id67ec_c0">Revenus immobiliers</td>
<td headers="id67ec_c1">Ces revenus seront pris en compte pour déterminer le montant de l’assiette sur laquelle s’appliquera un taux de 30 %</td>
<td headers="id67ec_c2">25 % pour l’exploitation et la vente<br class="autobr">
Dans le cas d’une vente, si le vendeur remplit certaines conditions, le profit obtenu peut être assujetti au taux de 30 %</td></tr>
<tr class="row_odd odd">
<td headers="id67ec_c0">Redevances</td>
<td headers="id67ec_c1"></td>
<td headers="id67ec_c2">10 % de la redevance</td></tr>
<tr class="row_even even">
<td headers="id67ec_c0">Intérêts</td>
<td headers="id67ec_c1"></td>
<td headers="id67ec_c2">10 % des intérêts</td></tr>
</tbody>
</table>
<p><strong>Source</strong> : Guide des affaires Mexique 2013, Ubifrance</p>
<p><i>Mise à jour : septembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/fiscalite/article/convention-fiscale-108400). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
