# Convention fiscale

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/fiscalite-22708/article/convention-fiscale-108669#sommaire_1">Convention fiscale du 20 juin 2006</a></li></ul>
<p>La France et l’Australie ont signé à Canberra le 13 avril 1976 une convention tendant à éviter les doubles impositions et à prévenir l’évasion fiscale en matière d’impôts sur le revenu. Cette convention a été publiée au Journal officiel du 4 décembre 1977 et est <strong>entrée en vigueur le 21 septembre 1977</strong>.</p>
<p>La convention a été modifiée par un <strong>avenant signé à Paris le 19 juin 1989 </strong>et publié au Journal Officiel du 28 septembre 1990 (décret n°90-862 du 21 septembre 1990). Cet avenant est <strong>entré en vigueur le 1er janvier 1990</strong>.</p>
<p><strong>Une nouvelle convention fiscale </strong>tendant à éviter les doubles impositions en matière d’impôts sur le revenu et à prévenir l’évasion fiscale a été signée à Paris le 20 juin 2006 et est <strong>entrée en vigueur le 1er juin 2009</strong>. Elle a été publiée au Journal officiel du 21 juin 2009 (décret n°2009-732).</p>
<p>Vous pouvez consulter le texte de cette dernière convention sur le site Internet : <a href="http://www.impots.gouv.fr/" class="spip_out" rel="external">www.impots.gouv.fr</a> - rubrique « documentation Accédez à la rubrique International ».</p>
<h3 class="spip"><a id="sommaire_1"></a>Convention fiscale du 20 juin 2006</h3>
<p><strong>Champ d’application de la convention</strong></p>
<p>La convention a pour objet de protéger les résidents de chacun des Etats contractants en matière d’impôts prélevés directement sur le revenu ou sur le bénéfice des sociétés. Elle tend à éviter les doubles impositions en matière d’impôts sur le revenu et à prévenir l’évasion fiscale.</p>
<p><strong>Personnes visées (article 1)</strong></p>
<p>La Convention s’applique aux personnes qui sont des résidents d’un Etat contractant ou des deux Etats contractants.</p>
<p><strong>Impôts visés (article 2)</strong></p>
<p>1) Dans le cas de l’Australie :</p>
<ul class="spip">
<li>la convention s’applique à l’impôt sur le revenu,</li>
<li>et à celui sur la location de ressources « ressource rent tax » relatif aux projets offshore d’exploration et d’exploitation des ressources pétrolières, perçus selon la loi fédérale de l’Australie ;</li></ul>
<p>2) Dans le cas de la France :</p>
<ul class="spip">
<li>à l’impôt sur le revenu,</li>
<li>celui sur les sociétés,</li>
<li>les contributions sur l’impôt sur les sociétés ;</li>
<li>les contributions sociales généralisées et les contributions pour le remboursement de la dette sociale, y compris toutes retenues à la source afférentes aux impôts visés ci-dessus.</li></ul>
<p><strong>Notion de résidence (article 4)</strong></p>
<p>La convention s’applique aux personnes considérées commerésidentes d’un Etat contractantou de chacun de ces deux Etats. Une personne est considérée comme « résidente d’un Etat contractant » lorsque, en vertu de la législation de cet Etat, elle se trouve assujettie à l’impôt en raison de son domicile, de sa résidence ou de critères analogues.</p>
<p>La situation des personnes résidentes dans chacun des deux Etats contractants est résolue en recourant à des critères subsidiaires :</p>
<ul class="spip">
<li>le foyer d’habitation permanent (il s’agit, par exemple, du lieu où se trouvent le conjoint et/ou les enfants) ;</li>
<li>à défaut, l’Etat avec lequel la personne entretient les liens personnels et économiques les plus étroits.<strong></strong></li></ul>
<p><strong>Dispositions conventionnelles concernant certaines catégories de revenus</strong></p>
<p><strong>Revenus immobiliers (article 6)</strong></p>
<p>Les revenus provenant de biens immobiliers, y compris les revenus provenant de biens agricoles, de pâturages ou de biens forestiers, sont imposables dans l’Etat contractant où ces biens sont situés.<strong> </strong></p>
<p><strong>Bénéfices des entreprises (article 7)</strong></p>
<p>1) Les bénéfices d’une entreprise d’un Etat contractant ne sont imposables que dans cet Etat, à moins que l’entreprise n’exerce son activité dans l’autre Etat contractant par l’intermédiaire d’un établissement stable qui y est situé. Si l’entreprise exerce son activité d’une telle façon, les bénéfices de l’entreprise sont imposables dans l’autre Etat.</p>
<p>2) Lorsqu’une entreprise d’un Etat contractant exerce son activité dans l’autre Etat contractant par l’intermédiaire d’un établissement stable qui y est situé, il est imputé, dans chaque Etat contractant, à cet établissement stable les bénéfices qu’il aurait pu réaliser s’il avait constitué une entreprise distincte et séparée exerçant des activités identiques ou analogues dans des conditions identiques ou analogues et traitant en toute indépendance avec l’entreprise dont il constitue un établissement stable.</p>
<p><strong>Les dividendes (article 10)</strong></p>
<p>Le terme « dividendes » désigne les revenus provenant d’actions ou autres parts bénéficiaires à l’exception des créances, ainsi que les autres montants soumis au même régime fiscal que les distributions ou les revenus d’actions par la législation de l’Etat dont la société distributrice est un résident aux fins d’imposition.</p>
<p>De manière générale, l’article 10 maintient le droit au profit de l’Etat dont la société qui paie les dividendes est un résident, d’imposer ces dividendes à la source à un taux ne pouvant dépasser 15%.</p>
<p>D’autre part, des règles particulières sont prévues pour les dividendes de source française distribués à des résidents d’Australie et pour les dividendes de source australienne reçus par des sociétés françaises et redistribués à leurs actionnaires.</p>
<p><strong>Les intérêts (article 11)</strong></p>
<p>Ce terme désigne les revenus des fonds publics ou des obligations d’emprunt, assortis ou non des garanties hypothécaires ou d’une clause de participation aux bénéfices et les intérêts d’autres formes de créances ainsi que tous les autres revenus soumis au même traitement fiscal que les revenus de prêts par la législation fiscale de l’Etat d’où proviennent ces revenus. Les intérêts provenant d’un Etat et payés à un résident de l’autre Etat sont imposables dans le pays de la source à un taux ne pouvant dépasser 10%.</p>
<p><strong>Redevances (article 12)</strong></p>
<p>Le terme « redevances » désigne les paiements ou les sommes crédités, périodiques ou non, quelle que soit leur qualification ou la manière dont ils sont calculés, dans la mesure où ils sont versés au titre de :</p>
<p>1) l’usage ou la concession de l’usage d’un droit d’auteur, d’un brevet, d’un dessin ou d’un modèle, d’un plan, d’une formule ou d’un procédé secret, d’une marque de fabrique ou de commerce ou d’un autre bien ou droit analogue ;</p>
<p>2) la fourniture de connaissances ou d’informations scientifiques, techniques, industrielles ou commerciales ;</p>
<p>3) la fourniture d’une assistance qui est seulement auxiliaire et accessoire et qui est destinée à permettre l’utilisation ou la jouissance d’un bien ou droit mentionné à l’alinéa a) ou des connaissances ou informations mentionnées à l’alinéa b) ;</p>
<p>4) l’usage ou la concession de l’usage de :</p>
<ul class="spip">
<li>films cinématographiques ;</li>
<li>films ou disques ou bandes audio ou vidéo ou d’autres moyens de reproductions de sons ou d’images destinés à la télévision, à la radio ou autre radiodiffusion ;</li></ul>
<p>5) la renonciation totale ou partielle à l’usage ou à la fourniture d’un bien ou d’un droit cité dans le présent paragraphe<strong>.</strong></p>
<p><strong>Revenus d’emploi (article 14)</strong></p>
<p>1) Sous réserve des dispositions des articles 15, 17 et 18, les rémunérations qu’une personne physique, qui est un résident d’un Etat contractant, reçoit au titre d’un emploi salarié ne sont imposables que dans cet Etat, à moins que l’emploi ne soit exercé dans l’autre Etat contractant. Si l’emploi y est exercé, les rémunérations reçues à ce titre sont imposables dans cet autre Etat.</p>
<p>2) Nonobstant les dispositions du paragraphe 1, les rémunérations qu’une personne physique, qui est un résident d’un Etat contractant, reçoit au titre d’un emploi salarié exercé dans l’autre Etat contractant ne sont imposables que dans le premier Etat si :</p>
<ul class="spip">
<li>le bénéficiaire séjourne dans cet autre Etat pendant une période ou des périodes n’excédant pas au total 183 jours durant toute période de douze mois commençant ou se terminant durant l’année fiscale de cet autre Etat ; et</li>
<li>les rémunérations sont payées par un employeur ou au nom d’un employeur qui n’est pas un résident de cet autre Etat ; et</li>
<li>la charge des rémunérations n’est pas supportée par un établissement stable que l’employeur a dans l’autre Etat.</li></ul>
<p>3) Nonobstant les dispositions précédentes du présent article, les rémunérations reçues au titre d’un emploi salarié exercé à bord d’un navire ou d’un aéronef exploité en trafic international par un résident d’un Etat contractant sont imposables dans cet Etat.</p>
<p><strong>Exemple</strong></p>
<p>Monsieur X est envoyé trois mois, soit 90 jours (mai, juin, juillet de l’année n) par une PME située en France, fabricant d’articles de maroquinerie, en vue de prospecter le marché australien. Cette entreprise ne dispose d’aucune succursale, ni de bureau en Australie et rémunère l’intéressé. Dans un tel cas, Monsieur X devra déclarer ses revenus en France.</p>
<p>Au contraire, si Monsieur X est envoyé de février à novembre inclus, son séjour de plus de 183 jours en Australie entraîne une imposition dans ce pays.</p>
<p><strong>Jetons de présence (article 15)</strong></p>
<p>Les jetons de présence et autres rétributions similaires qu’un résident d’un Etat contractant reçoit en sa qualité de membre du conseil d’administration ou de surveillance d’une société qui est un résident de l’autre Etat contractant sont imposables dans cet autre Etat.<strong></strong></p>
<p><strong>Artistes et sportifs (article 16)</strong></p>
<p>Les revenus que les professionnels du spectacle, ainsi que les sportifs, réalisent en cette qualité dans l’un des deux Etats restent imposables dans l’Etat d’exercice de l’activité.</p>
<p><strong>Pensions et rentes (article 17)</strong></p>
<p>Le terme « rente » désigne toute somme déterminée, payable périodiquement à échéances fixes, à titre viager ou pendant une période déterminée ou qui peut l’être en vertu d’un engagement d’effectuer les paiements en contrepartie d’une prestation équivalente en argent ou évaluable en argent.</p>
<p>Sous réserve des dispositions du paragraphe 2 de l’article 18, les pensions et rentes versées à un résident d’un Etat contractant ne sont imposables que dans cet Etat.</p>
<p>Nonobstant toute autre disposition de la présente Convention, les pensions ou allocations au titre de blessures, invalidité ou décès pour cause de guerre et les pensions d’anciens combattants payées par un Etat contractant à un résident de l’autre Etat contractant et exonérées conformément à la législation du premier Etat, sont aussi exonérées dans l’autre Etat.<strong> </strong></p>
<p><strong>Exemple</strong></p>
<p>Monsieur X, citoyen français ayant exercé une activité salariée en Australie, décide de venir prendre sa retraite en France. Les pensions versées par l’Australie au titre de cette activité sont imposables en France.</p>
<p>Les cotisations sociales payées par une personne physique résidente d’un Etat à une institution ou à une caisse de retraite de l’autre Etat sont traitées fiscalement, sous réserve de certaines conditions, de la même manière que si elles avaient été payées à une institution ou à une caisse de retraite de l’Etat de résidence.</p>
<p><strong>Fonction publique (article 18)</strong></p>
<p>1) Salaires :</p>
<ul class="spip">
<li>Les salaires, traitements et autres rémunérations (autres que les pensions ou rentes), payés par un Etat contractant ou l’une de ses subdivisions politiques ou collectivités locales ou personnes morales de droit public à une personne physique au titre des services rendus à cet Etat ou à cette subdivision ou collectivité ou personne morale de droit public ne sont imposables que dans cet Etat.</li>
<li>Toutefois ces salaires, traitements et autres rémunérations similaires ne sont imposables que dans l’autre Etat contractant si les services sont rendus dans cet Etat et si la personne physique est un résident de cet Etat et possède la nationalité ou la citoyenneté de cet Etat sans posséder aussi la nationalité ou la citoyenneté du premier Etat.</li></ul>
<p>2) Pensions</p>
<ul class="spip">
<li>Les pensions payées par un Etat contractant ou l’une de ses subdivisions politiques ou collectivités territoriales ou par une de leurs personnes morales de droit public, soit directement, soit par prélèvement sur des fonds qu’ils ont constitués, à une personne physique au titre de services rendus à cet Etat, subdivision, collectivité ou personne morale ne sont imposables que dans cet Etat.</li>
<li>Toutefois, ces pensions ne sont imposables que dans l’autre Etat contractant si la personne physique est un résident de cet Etat et en possède la nationalité ou la citoyenneté sans posséder en même temps la nationalité ou la citoyenneté du premier Etat.</li></ul>
<p>3) Les dispositions des articles 14, 15, 16 et 17 s’appliquent aux salaires, traitements et autres rémunérations similaires ainsi qu’aux pensions payés au titre de services rendus dans le cadre d’une activité industrielle ou commerciale exercée par un Etat contractant ou l’une de ses subdivisions politiques ou collectivités territoriales ou par une personne morale de droit public.</p>
<p><strong>Etudiants (article 19)</strong></p>
<p>Les sommes qu’un étudiant qui est ou qui était, immédiatement avant de venir séjourner dans l’un des Etats contractants, un résident de l’autre Etat contractant et qui séjourne temporairement dans le premier Etat contractant à seule fin d’y poursuivre ses études, reçoit de sources situées en dehors du premier Etat pour couvrir ses frais d’entretien ou d’études ne sont pas imposables dans ce premier Etat.</p>
<p><strong>Autres revenus (article 20)</strong></p>
<p>Les éléments de revenu d’un résident d’un Etat contractant, d’où qu’ils proviennent, qui ne sont pas traités dans les articles précédents de la présente Convention ne sont imposables que dans cet Etat.</p>
<p><strong>Elimination des doubles impositions (article 23)</strong></p>
<p>Sous réserve des dispositions de la législation australienne en vigueur à un moment donné relative à l’attribution d’un crédit sur l’impôt australien de l’impôt payé dans un pays autre que l’Australie (qui n’affecte en rien les dispositions générales du présent article), l’impôt français payé en vertu de la législation française et conformément aux dispositions de la présente Convention, soit directement, soit par déduction, au titre des revenus qu’une personne qui est un résident d’Australie tire de sources situées en France ouvre droit à un crédit sur l’impôt australien dû au titre de ces revenus.</p>
<p>En ce qui concerne la France, les doubles impositions sont éliminées de la manière suivante :</p>
<p>1) Nonobstant toute autre disposition de la présente Convention, les revenus qui sont imposables ou ne sont imposables qu’en Australie conformément aux dispositions de la présente Convention sont pris en compte pour le calcul de l’impôt français lorsque le bénéficiaire de ces revenus est un résident de France et lorsqu’ils ne sont pas exemptés de l’impôt sur les sociétés en application de la législation interne française. Dans ce cas, l’impôt australien n’est pas déductible de ces revenus, mais le résident de France a droit, sous réserve des conditions et limites prévues aux i) et ii), à un crédit d’impôt imputable sur l’impôt français. Ce crédit d’impôt est égal :</p>
<ul class="spip">
<li>pour les revenus non mentionnés au ii), au montant de l’impôt français correspondant à ces revenus à condition que le bénéficiaire résident de France soit soumis à l’impôt australien à raison de ces revenus ;</li>
<li>pour les revenus visés à l’article 7 et au paragraphe 2 de l’article 13 qui sont soumis à l’impôt sur les sociétés, et pour les revenus visés à l’article 10, à l’article 11, à l’article 12, au paragraphe 1 de l’article 13, et au paragraphe 3 de l’article 14, à l’article 15, à l’article 16 et à l’article 20, au montant de l’impôt payé en Australie conformément aux dispositions de ces articles ; toutefois, ce crédit d’impôt ne peut excéder le montant de l’impôt français correspondant à ces revenus.</li></ul>
<p>2) Il est entendu que l’expression "montant de l’impôt français correspondant à ces revenus" employée au a) désigne :</p>
<ul class="spip">
<li>lorsque l’impôt dû à raison de ces revenus est calculé par application d’un taux proportionnel, le produit du montant des revenus nets considérés par le taux qui leur est effectivement appliqué ;</li>
<li>lorsque l’impôt dû à raison de ces revenus est calculé par application d’un barème progressif, le produit du montant des revenus nets considérés par le taux résultant du rapport entre l’impôt effectivement dû à raison du revenu net global imposable selon la législation française et le montant de ce revenu net global.</li></ul>
<p><strong>« Partnerships » (article 29)</strong></p>
<p>1) Dans le cas d’un <i>partnership</i> ou d’une entité similaire qui a son siège de direction effective en Australie et qui est fiscalement traité comme transparent en Australie :</p>
<ul class="spip">
<li>un associé qui est un résident d’Australie et dont la part des revenus, des bénéfices ou des gains du <i>partnership</i> est imposée en Australie de la même manière que s’il avait réalisé directement ces produits, a droit aux avantages prévus par la présente Convention à raison de sa part de ces produits qui proviennent de France de la même manière que si l’associé avait réalisé directement ces produits ;</li>
<li>un associé qui est un résident de France :
<br>— a droit aux avantages prévus par la présente Convention à raison de sa part des revenus, bénéfices ou gains du <i>partnership</i> provenant d’Australie de la même manière que si l’associé avait directement réalisé ces produits ; et
<br>— est imposable sur sa part des revenus, bénéfices ou gains du <i>partnership</i> provenant de France de la même manière que si l’associé avait directement réalisé ces produits mais ceux de ces produits qui sont imposés en Australie sont considérés pour l’application du paragraphe 2 de l’article 23 de la présente Convention comme provenant d’Australie.</li></ul>
<p>2) Dans le cas d’un <i>partnership</i> qui a son siège de direction effective dans un Etat autre qu’un des Etats contractants et qui est fiscalement traité comme transparent dans cet Etat tiers, un associé qui est un résident d’un Etat contractant et dont la part des revenus, des bénéfices ou des gains du <i>partnership</i> est imposée dans cet Etat contractant de la même manière que s’il avait réalisé directement ces produits, a droit aux avantages prévus par la présente Convention à raison de sa part de produits provenant de l’autre Etat contractant, sous réserve du respect des conditions suivantes :</p>
<ul class="spip">
<li>l’absence de dispositions contraires dans une convention fiscale entre un Etat Contractant et l’Etat tiers ; et</li>
<li>le fait que la part des revenus, des bénéfices ou des gains revenant à l’associé soit imposée de la même manière -y compris en ce qui concerne la nature, la source et la période d’imposition de ces produits- que si les produits avaient été réalisés directement par l’associé ; et</li>
<li>la possibilité d’échanger des renseignements sur le <i>partnership</i> et sur les associés en application des dispositions d’une convention fiscale entre l’Etat Contractant d’où proviennent les revenus, les bénéfices ou les gains et l’Etat tiers.</li></ul>
<p>3) Pour l’application des dispositions des paragraphes (1) et (2) du présent article, les revenus, bénéfices ou gains sont réputés provenir d’un Etat contractant notamment lorsqu’ils sont imputables à un établissement stable que le <i>partnership</i> ou que l’entité a dans cet Etat.</p>
<p>4) Lorsque, conformément à une disposition de la présente Convention, une société de personnes ou un groupement de personnes qui est un résident de France au sens du paragraphe 5 de l’Article 4, a droit à une exonération ou à un allègement de l’impôt sur le revenu, sur les bénéfices ou sur les gains en Australie, cette disposition ne peut être interprétée comme limitant le droit de l’Australie d’imposer les membres de la société de personnes ou du groupement qui sont des résidents de l’Australie sur leur part des produits ainsi exonérés ; mais ces produits sont considérés, pour l’application des dispositions du paragraphe 1 de l’article 23 de la présente Convention, comme ayant leur source en France.</p>
<p><i>Mise à jour : octobre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/fiscalite-22708/article/convention-fiscale-108669). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
