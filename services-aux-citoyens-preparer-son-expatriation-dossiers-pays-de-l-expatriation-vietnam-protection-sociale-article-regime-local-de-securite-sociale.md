# Régime local de sécurité sociale

<h4 class="spip">Généralités </h4>
<p>Il est recommandé au travailleur expatrié de contracter une assurance maladie en France ; en effet le régime vietnamien de sécurité sociale ne peut être retenu du fait de ses carences.</p>
<p>L’assurance-maladie a été introduite au Vietnam par le décret de 1998 relatif au régime d’assurance maladie de l’Etat. Un nouveau décret a été approuvé par le gouvernement et est entré en vigueur en juillet 2005 en remplacement du décret de 1998. L’objectif de ce nouveau décret était d’élargir la couverture, de fournir de meilleures prestations aux assurés et de simplifier les procédures.</p>
<p>Le décret fait référence à deux types d’assurance maladie gérés par un seul Fonds (La Sécurité sociale du Vietnam, VSS) :</p>
<ul class="spip">
<li>l’assurance maladie obligatoire : elle s’applique à tous les travailleurs vietnamiens et couvre 14 catégories d’assurés, contre 10 pour le décret de 1998. Les quatre nouvelles catégories sont les officiers de l’armée en activité, les parents des officiers de l’armée et de la police, les personnes percevant des pensions sociales et celles âgées de 90 ans ou plus ;</li>
<li>l’assurance maladie volontaire : elle s’applique à toutes les personnes désireuses d’y souscrire volontairement, par exemple les étrangers qui viennent au Vietnam pour affaires, pour études ou dans un but touristique, mais aussi les assurés du régime d’assurance maladie obligatoire qui souhaitent bénéficier de prestations d’un montant plus élevé.</li></ul>
<p>La caisse d’assurance maladie est financée par des cotisations versées par les employeurs et les assurés, par des contributions de l’Etat pour les membres subventionnés et par des fonds provenant de mesures visant à préserver et à renforcer la caisse.</p>
<p>Le nombre de personnes bénéficiant de ce système représente 39% de la population.</p>
<p>L’Etat a également développé une assurance maladie d’état visant plus spécialement les enfants de moins de six ans (gratuité des soins préventifs et curatifs) et les populations pauvres à travers la décision 139 instaurant le <i>Health care fund for the poor</i> en 2002.</p>
<p>Ce fonds est destiné aux 14,3 millions de personnes qui vivent sous le seuil de pauvreté et n’ont pas accès aux soins.</p>
<p>La gestion du Fonds est sous la responsabilité des gouvernements provinciaux. Le Fonds est financé par une contribution individuelle annuelle dont une partie est subventionnée par l’Etat et les autorités locales. A la fin de 2004, 11 millions (84% de la population cible) est couverte par le Fonds.</p>
<p><i>Source : <a href="http://www.gipspsi.org/" class="spip_out" rel="external">Gipspsi.org</a></i></p>
<h4 class="spip">Les assurances obligatoires</h4>
<p>L’adhésion au régime de sécurité sociale est obligatoire pour les contrats à durée déterminée de plus de trois mois et pour les contrats à durée indéterminée. Pour les contrats de moins de trois mois, la somme équivalente aux contributions sociales est ajoutée au salaire de base.</p>
<p>Les cotisations des salariés vietnamiens des entreprises étrangères, des bureaux de représentation, des succursales d’entreprises étrangères et de leurs employeurs concernent deux types d’assurances obligatoires : l’assurance sociale et l’assurance santé. La part patronale s’élève à 17% pour les deux assurances et la part salariale à 6% au total selon la répartition indiquée ci-dessous.</p>
<p>Une nouvelle prestation, l’indemnité chômage a été créée en 2006.</p>
<h4 class="spip">L’assurance sociale</h4>
<p>Elle ouvre le droit au versement d’une allocation pour les cinq cas suivants : maladie, maternité, accident du travail ou maladie professionnelle, retraite et décès. La cotisation patronale est de 11% et la cotisation salariale 5%.</p>
<h4 class="spip">L’assurance santé</h4>
<p>Elle permet la prise en charge des soins médicaux des salariés. La cotisation patronale est de 2% et la cotisation salariale 1%.</p>
<p>Depuis le 1er janvier 1999, les bénéficiaires de l’assurance santé doivent s’acquitter de 20% de leurs frais médicaux (ticket modérateur).</p>
<p>Il n’existe pas de prestations familiales au Vietnam.</p>
<p><i>Mise à jour : février 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/protection-sociale/article/regime-local-de-securite-sociale). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
