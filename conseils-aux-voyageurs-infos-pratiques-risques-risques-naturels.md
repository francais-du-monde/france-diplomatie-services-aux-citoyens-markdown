# Risques naturels

<h2 class="rub20995">Séismes</h2>
<h4 class="spip"> <strong>Vous résidez dans une région à risque sismique</strong> </h4>
<p><strong>A votre domicile, prévoyez des stocks de sécurité :</strong></p>
<p>Constituez des stocks vous permettant de vivre en autonomie pendant 7 jours (attention aux dates de péremption). Par exemple :</p>
<ul class="spip">
<li>de l’eau (en bonbonne ou en bouteilles, à raison de 2 litres par personne et par jour, plus quelques litres pour cuisiner) ;</li>
<li>des conserves avec languette permettant l’ouverture sans ouvre-boîtes (thon, sardines à l’huile, salades, etc…) ;</li>
<li>des soupes en sachet, des pâtes, du riz ;</li>
<li>des biscuits, compotes, barres de céréales, pâtes de fruits, pâte à tartiner, crème de marrons ;</li>
<li>une radio avec des piles de rechange ;</li>
<li>un vélo (et un casque).</li>
<li>le cas échéant, quelques produits pour bébés (petits pots salés et sucrés, lait en poudre).</li></ul>
<p>Veillez à ce que le réservoir d’essence de votre véhicule soit toujours suffisamment rempli (mais ne conservez pas de stocks de carburant à domicile).<br></p>
<h4 class="spip"> <strong>Vous résidez où vous êtes de passage dans une région à risque sismique</strong> </h4>
<p><strong>Préparez des sacs de secours :</strong></p>
<p>Préparez et conservez à portée de main (à votre domicile, sur votre lieu de travail et/ou à bord de votre voiture) un sac avec des éléments suivants (suggestion) :</p>
<ul class="spip">
<li>une lampe torche et des piles ;</li>
<li>une radio portable et des piles ;</li>
<li>un jeu de clés du domicile et du véhicule ;</li>
<li>une trousse de premiers secours (pansements, lingettes désinfectantes, gel hydro-alcoolique, anti-diarrhéiques…) et les traitements habituels quotidiens et d’urgence (asthme, diabète, pathologies cardiaques, allergies…), avec la photocopie du carnet de vaccination et les ordonnances du traitement habituel ;</li>
<li>des produits d’hygiène (brosse a dent, dentifrice, savon, gel douche, papier toilette, hygiène féminine) ;</li>
<li>des bouteilles d’eau ;</li>
<li>des barres aux céréales, pâtes de fruit, briques de jus de fruit… ;</li>
<li>une couverture ou un duvet ;</li>
<li>des lunettes de vue et de soleil ;</li>
<li>des pastilles désinfectantes pour l’eau ;</li>
<li>une liste des numéros de téléphone utiles (ambassade, chef d’ilot, médecin traitant, Croix Rouge…) ;</li>
<li>vos documents d’identité (ou copies), carte de séjour, carnet de santé, livret de famille… ;</li>
<li>vos cartes de crédit, chéquier, un peu d’argent liquide ;</li>
<li>un couteau suisse ou un canif multilame ;</li>
<li>un sifflet en métal ;</li>
<li>des produits anti-moustiques et de la crème solaire ;</li>
<li>une casquette ;</li>
<li>une grande serviette ou un grand drap propre ;</li>
<li>une couverture de survie ou un poncho ;</li>
<li>une carte du pays, un plan de la ville.<br class="manualbr"></li></ul>
<p>Pour les familles avec des enfants en bas âge :</p>
<ul class="spip">
<li>des couches ;</li>
<li>un biberon neuf ;</li>
<li>du lait en poudre ;</li>
<li>du petit matériel médico-hygiénique (pommade, éosine, talc…).</li></ul>
<p>Afin que vos enfants soient facilement identifiables s’ils devaient se retrouver seuls, faites leur porter une fiche d’identité en français et si possible en langue locale, indiquant leur nom, leur date de naissance, leur adresse et numéro de téléphone, leur nationalité et leurs éventuels problèmes de santé.</p>
<p>Identifiez les lieux les plus sûrs de votre domicile (contre les murs de soutien, dans les couloirs, dans les angles des pièces, sous les tables ou bureaux solides, dans les passages voûtés ou dans l’encadrement des portes) ainsi que les endroits les plus critiques (fenêtres, miroirs, objets suspendus, cheminée, meubles en hauteur ou non scellés aux murs).</p>
<p>Placez les objets fragiles, lourds ou coupants sur les étagères du bas, fixez les meubles aux murs, renforcez les fixations des objets suspendus, placez les lits loin des fenêtres ou cadres lourds. Repérez les lieux de rassemblement provisoires et les centres de refuge.</p>
<p>Enfin, organisez un petit exercice (sous forme de jeu) en famille qui permettra d’identifier les lieux de protection à l’intérieur de l’habitation ainsi que le circuit domestique d’évacuation.<br></p>
<p><br class="manualbr"><strong></strong></p>
<h4 class="spip">En cas de séisme :</h4>
<p><strong>A l’intérieur</strong></p>
<ul class="spip">
<li>Ne vous précipitez pas dehors ;</li>
<li>abritez-vous dans les lieux que vous avez identifiés comme sûrs et protégez-vous des chutes d’objets ;</li>
<li>éloignez-vous des endroits les plus critiques.</li></ul>
<p><strong>A l’extérieur </strong></p>
<ul class="spip">
<li>si vous êtes dans un véhicule, arrêtez-vous sur le bord de la route, loin des ponts et édifices ;</li>
<li>si vous êtes à pied, éloignez vous des constructions, arbres et fils électriques, à défaut, abritez-vous sous un porche.
<br></li></ul>
<p><br class="manualbr"><strong></strong></p>
<h4 class="spip">Après le séisme :</h4>
<ul class="spip">
<li>Gardez votre calme, vérifiez que vous n’êtes pas blessé ;</li>
<li>fermez les arrivées d’eau, d’électricité et de gaz, n’allumez pas de bougies, ne fumez pas ;</li>
<li>écoutez la radio et suivez les consignes des autorités ;</li>
<li>mettez des chaussures pour vous protéger des bris de verre ;</li>
<li>évacuez l’immeuble le plus rapidement possible en emportant le kit de survie ;</li>
<li>n’utilisez pas l’ascenseur ;</li>
<li>n’utilisez pas votre véhicule sauf en cas d’extrême urgence ;</li>
<li>ne touchez pas aux fils électriques tombés à terre ;</li>
<li>dirigez-vous vers un espace libre (parc, stade…), éloignez-vous de tout ce qui peut s’effondrer ;</li>
<li>ne pénétrez pas dans une zone sinistrée sans autorisation ;</li>
<li>n’allez pas chercher vos enfants, les établissements scolaires les prennent en charge ;</li>
<li>éloignez-vous des zones côtières (risques de raz de marée).</li></ul>
<p>Le réseau téléphonique peut être très perturbé. Ne l’encombrez pas. L’ambassade pourrait tenter de vous joindre. Si vous avez de la famille en France, pensez à contacter  un de vos proches et demandez-lui de faire passer le message aux autres.</p>
<p>Dans tous les cas, pensez à vous informer, auprès de l’ambassade ou du consulat le plus proche de votre résidence, des mesures prévues pour la sécurité de la communauté française.</p>
<p>Pour de plus amples renseignements sur les risques naturels, vous pouvez également consulter le site <a href="http://www.risquesetsavoirs.fr/" class="spip_out" rel="external">Risques et savoir</a>.<br></p>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/infos-pratiques/risques/risques-naturels/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
