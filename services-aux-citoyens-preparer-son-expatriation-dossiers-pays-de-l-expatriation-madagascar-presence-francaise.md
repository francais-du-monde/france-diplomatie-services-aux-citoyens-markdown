# Présence française

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/presence-francaise/#sommaire_1">Ambassade et consulat de France</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/presence-francaise/#sommaire_2">Autorités françaises dans le pays</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/presence-francaise/#sommaire_3">Associations dans le pays</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Ambassade et consulat de France</h3>
<p>Consulter notre <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/article/annuaire-des-representations-francaises-a-l-etranger" class="spip_in">annuaire des ambassades et consulats de France à l’étranger</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Autorités françaises dans le pays</h3>
<p><strong>Réseau français de coopération et d’action culturelle</strong></p>
<p>Les coordonnées des services du réseau de coopération et d’action culturelle français à Madagascar ainsi que la description de l’activité de ces services se trouvent dans la rubrique culture du site internet de l’Ambassade ou sur le site internet du réseau culturel : <a href="http://latitudefrance.diplomatie.gouv.fr/AnnuaireCarto.html" class="spip_out" rel="external">Latitude France</a>.</p>
<p><strong>Economie : réseau mondial</strong></p>
<p>Les services économiques sont présents dans le pays. Ils sont une émanation de la Direction générale du Trésor et ont pour missions principales l’analyse macroéconomique et l’animation des relations économiques bilatérales entre la France et les pays où ils sont implantés.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="https://www.tresor.economie.gouv.fr/pays/madagascar" class="spip_out" rel="external">Site internet du service économique français à Madagascar</a></p>
<p><strong>Vos élus à l’Assemblée des Français de l’étranger</strong></p>
<p>Pour toute information sur l’Assemblée des Français de l’étranger et pour connaître les conseillers et les Sénateurs qui représentent les Français établis hors de France dans votre circonscription, vous pouvez consulter les sites Internet suivants :</p>
<ul class="spip">
<li><a href="http://www.assemblee-afe.fr/" class="spip_out" rel="external">Assemblée des Français de l’étranger (AFE)</a></li>
<li>Sénat au service des Français de l’étranger : <a href="http://www.senat.fr/expatries/dossiers_pays/madagascar.html" class="spip_out" rel="external">dossier spécifique à Madagascar sur le site du Sénat</a></li></ul>
<p><strong>Députés des Français de l’étranger</strong></p>
<p>La révision constitutionnelle du 23 juillet 2008, publiée au Journal officiel du 19 avril 2011, introduit la représentation à l’Assemblée nationale des Français établis hors de France. La création de onze circonscriptions législatives à l’étranger permet aux Français expatriés d’élire leur député à l’Assemblée nationale à partir du scrutin de 2012.</p>
<p>Pour plus d’information : <a href="http://www.votezaletranger.gouv.fr/" class="spip_out" rel="external">Votez à l’étranger</a></p>
<h3 class="spip"><a id="sommaire_3"></a>Associations dans le pays</h3>
<p><a href="http://www.francais-du-monde.org/pays/madagascar/" class="spip_out" rel="external">Français du monde - ADFE Madagascar (FDM-madagascar)</a><br class="manualbr">Propriété Saint Denis <br class="manualbr">Près Malagasy Minerals Mines Ambatofotsy AMPANDRIANOMBY BP 203 <br class="manualbr">Poste Antaninarenina 101 Antananarivo <br class="manualbr">Tél. : 22 597 96 <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/presence-francaise/#francaisdumonde.madagascar#mc#gmail.com#" title="francaisdumonde.madagascar..åt..gmail.com" onclick="location.href=mc_lancerlien('francaisdumonde.madagascar','gmail.com'); return false;" class="spip_mail">Courriel</a></p>
<p><a href="http://www.tana-accueil.com" class="spip_out" rel="external">Tana Accueil</a><br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/presence-francaise/#tana.accueil#mc#gmail.com#" title="tana.accueil..åt..gmail.com" onclick="location.href=mc_lancerlien('tana.accueil','gmail.com'); return false;" class="spip_mail">Courriel</a></p>
<p><a href="http://www.fiafe.org/" class="spip_out" rel="external">Tamatave contact FIAFE</a><br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/presence-francaise/#tamataveaccueil#mc#yahoo.com#" title="tamataveaccueil..åt..yahoo.com" onclick="location.href=mc_lancerlien('tamataveaccueil','yahoo.com'); return false;" class="spip_mail">Courriel</a></p>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li><a href="http://www.ambafrance-mada.org/Annuaire-sectoriel-des-ONG-et" class="spip_out" rel="external">L’annuaire sectoriel des ONG et associations</a> sur le site de l’Ambassade de France à Tananarive.</li>
<li>Article thématique <a href="services-aux-citoyens-preparer-son-expatriation-en-savoir-plus-article-associations-des-francais-de-l-etranger.md" class="spip_in">Associations des Français de l’étranger</a>.</li></ul>
<p><i>Mise à jour : septembre 2015</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-madagascar-presence-francaise-article-presence-francaise.md" title="Présence française">Présence française</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/presence-francaise/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
