# Arrestation ou détention d’un proche à l’étranger

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/conseils-aux-familles/arrestation-ou-detention-d-un-proche-a-l-etranger/#sommaire_1">Détention d’un proche à l’étranger</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/conseils-aux-familles/arrestation-ou-detention-d-un-proche-a-l-etranger/#sommaire_2">Le transfert de fonds par voie de chancellerie</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Détention d’un proche à l’étranger</h3>
<p>En cas d’incarcération d’un proche à l’étranger, vous pouvez :</p>
<p><strong>Avertir :</strong></p>
<ul class="spip">
<li>le poste ou le service consulaire français du lieu d’incarcération le plus proche ;</li>
<li>le bureau de la protection des détenus du Ministère des Affaires Étrangères (01 43 17 80 32) ou les nuits et week-end au 01.53.59.11.00.</li></ul>
<p><strong>Obtenir des informations relatives à cette incarcération, et notamment :</strong></p>
<ul class="spip">
<li>ce que recouvre la protection consulaire telle que définie dans les conventions internationales ;</li>
<li>une liste d’avocats, si possibles francophones, à même d’exercer la défense  de votre proche. Cette liste n’est qu’indicative. Le paiement des honoraires incombe au détenu ou à ses proches, et pas au ministère des Affaires étrangères ;</li>
<li>des renseignements sur la situation de votre proche (conditions de détention, évolution de la procédure judiciaire, etc.), sans que cela constitue un droit et sous réserve que la personne détenue y consente.</li></ul>
<p><strong>Demander, selon les contingences</strong> (autorisations des autorités locales, possibilités matérielles des services du ministère des Affaires étrangères), <strong>à ce que votre proche bénéficie de l’acheminement</strong> :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  des fonds que vous souhaitez lui adresser. Lire notre article <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/conseils-aux-familles/arrestation-ou-detention-d-un-proche-a-l-etranger/article/le-transfert-de-fonds-par-voie-de-chancellerie" class="spip_in">Le transfert de fonds par voie de chancellerie</a>.</p>
<p><strong>Pour plus d’informations :</strong></p>
<iframe src="http://www.diplomatie.gouv.fr/fr/spip.php?page=pdfjs&amp;id_document=94251" width="100%" height="450" class="lecteurpdf lecteufpdf-94251spip_documents"></iframe>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Consulter la <a href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/brochure_detenus_juillet_2015_version_finale_cle8db889.pdf" class="spip_in" type="application/pdf">publication sur l’aide aux détenus à l’étranger</a> en version PDF</p>
<h3 class="spip"><a id="sommaire_2"></a>Le transfert de fonds par voie de chancellerie</h3>
<p>Afin d’améliorer le confort du quotidien de nos compatriotes détenus à l’étranger grâce à l’acquisition de certains biens ou services en prison, le ministère des Affaires étrangères et du Développement international organise un transfert de fonds par voie de chancellerie qui consiste à permettre aux proches et aux familles de leur faire parvenir de l’argent.</p>
<p><strong>A noter</strong> : ce mécanisme <strong>n’est pas une obligation incombant aux autorités françaises</strong> mais une facilité donnée aux proches d’améliorer le confort des personnes incarcérées à l’étranger.</p>
<p>A cette fin, vous pouvez vous adresser au Bureau de la Protection des détenus, qui vous indiquera les modalités de ce type de transfert.</p>
<blockquote class="texteencadre-spip spip"><br class="autobr">
<p class="spip" style="text-align:center;"><br class="autobr">
<strong>Ministère des Affaires étrangères et du Développement international</strong><br class="manualbr">Direction des Français à l’étranger et de l’Administration consulaire<br class="manualbr">Service des Conventions, des Affaires civiles et de l’Entraide judiciaire<br class="manualbr">Mission pour la Protection des droits des personnes<br class="manualbr">Bureau de la Protection des détenus<br class="manualbr">27, rue de la convention – CS 91 533<br class="manualbr">75 732 PARIS Cedex 15<br class="manualbr">Tél. : 01 43 17 80 32<br class="autobr">
</p>
<p><br class="autobr"></p>
</blockquote>
<p>A titre d’information, en 2015, 261 opérations de transfert ont été effectuées pour un montant de 64 640 euros.</p>
<p><i>Mise à jour : juillet 2016</i></p>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/conseils-aux-familles/arrestation-ou-detention-d-un-proche-a-l-etranger/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
