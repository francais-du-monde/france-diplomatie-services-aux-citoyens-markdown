# Questions / réponses

<p class="chapo">
    Ce document liste toutes les réponses aux questions fréquemment posées au Service Central d’État Civil.
</p>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/questions-reponses/#sommaire_1"><i>Comment demander un acte d’état civil ?</i></a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/questions-reponses/#sommaire_2"><i>Qu’est-ce qu’une transcription et comment faire transcrire un acte de l’état civil étranger ?</i></a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/questions-reponses/#sommaire_3"><i>Comment demander un certificat de capacité à mariage ?</i></a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/questions-reponses/#sommaire_4"><i>Comment faire établir un acte de naissance ?</i></a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/questions-reponses/#sommaire_5"><i>A qui s’adresser pour demander l’apposition d’une mention de divorce sur un acte de mariage ?</i></a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/questions-reponses/#sommaire_6"><i>A qui s’adresser pour la mise à jour du livret de famille ?</i></a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/questions-reponses/#sommaire_7"><i>Comment faire rectifier un acte d’état civil suite à une erreur ou omission ?</i></a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/questions-reponses/#sommaire_8"><i>Comment faire légaliser un acte d’état civil délivré par une autorité étrangère ?</i></a></li>
<li><a id="so_9" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/questions-reponses/#sommaire_9"><i>Comment faire légaliser un acte d’état civil français ?</i></a></li>
<li><a id="so_10" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/questions-reponses/#sommaire_10"><i>Comment faire apostiller un acte de l’état civil français ?</i></a></li>
<li><a id="so_11" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/questions-reponses/#sommaire_11"><i>Comment demander une attestation de non-inscription au Répertoire civil ? </i></a></li>
<li><a id="so_12" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/questions-reponses/#sommaire_12"><i>Comment demander un certificat de nationalité française ?</i></a></li>
<li><a id="so_13" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/questions-reponses/#sommaire_13"><i>Comment se procurer un acte de naissance spécial « S12 » ?</i></a></li>
<li><a id="so_14" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/questions-reponses/#sommaire_14"><i>Comment demander un extrait de casier judiciaire ?</i></a></li>
<li><a id="so_15" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/questions-reponses/#sommaire_15"><i>Quelle est la validité d’une carte nationale d’identité ?</i></a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a><i>Comment demander un acte d’état civil ?</i></h3>
<p>Il est recommandé d’effectuer les demandes d’actes d’état civil par le biais du formulaire en ligne accessible sur le lien suivant :</p>
<p><a href="https://pastel.diplomatie.gouv.fr/dali" class="spip_out" rel="external">Demande d’actes d’Etat-Civil</a></p>
<p>Vous pouvez aussi formuler votre demande par courrier postal à l’adresse suivante(délais de traitement cependant plus longs) :</p>
<p><strong>Service central d’état civil</strong><br class="autobr"><strong>11, rue de la Maison Blanche</strong><br class="autobr"><strong>44941 NANTES CEDEX 9</strong></p>
<p><strong>Pour votre information, les demandes d’actes d’état civil et leur délivrance sont GRATUITES.</strong></p>
<h3 class="spip"><a id="sommaire_2"></a><i>Qu’est-ce qu’une transcription et comment faire transcrire un acte de l’état civil étranger ?</i></h3>
<p>La transcription d’un acte de l’état civil étranger consiste à transposer dans les registres consulaires français les indications contenues dans un acte établi à l’étranger par une autorité étrangère.</p>
<p>Les ressortissants français peuvent demander la transcription de leurs actes de l’état civil étrangers en s’adressant aux services consulaires de nos ambassades ou à nos consulats dans les pays concernés.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span> Transcription d’un acte (hors Algérie, Maroc et Tunisie)</p>
<p>La demande de transcription est déposée au service consulaire de l’Ambassade ou au consulat de France compétent. Le formulaire de demande de transcription et les pièces à présenter sont disponibles sur le site internet de cette ambassade/ce consulat.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Transcription d’un événement survenu en Algérie, au Maroc ou en Tunisie</p>
<p>Le formulaire de demande de transcription et la liste des pièces à présenter sont disponibles sur le site internet du consulat de France territorialement compétent.</p>
<p>Pour l’Algérie : Alger, Oran ou Annaba (événement survenu après le 1er janvier 1963)</p>
<p>Pour le Maroc : Agadir, Casablanca, Fès, Marrakech, Rabat et Tanger</p>
<p>Pour la Tunisie : Tunis</p>
<p>Le dossier de demande de transcription est à envoyer à l’adresse suivante :</p>
<p><strong>Service central d’état civil</strong><br class="autobr"><strong>Bureau des Transcriptions pour le Maghreb</strong><br class="autobr"><strong>11 rue de la Maison blanche</strong></p>
<h3 class="spip"><a id="sommaire_3"></a><i>Comment demander un certificat de capacité à mariage ?</i></h3>
<p><strong>Cette formalité est à accomplir avant le mariage</strong>. Pour connaître les démarches à effectuer en vue de l’obtention de ce document, veuillez contacter le consulat général de France territorialement compétent ou consulter son site internet.</p>
<p>Le mariage à l’étranger étant soumis aux dispositions de l’article 63 du code civil, le ressortissant français doit procéder aux démarches préalables au mariage pour obtenir la délivrance d’un certificat de capacité à mariage de la part des autorités consulaires compétentes, le délai d’instruction des dossiers peut varier selon que les formalités préalables au mariage ont été accomplies ou non.</p>
<h3 class="spip"><a id="sommaire_4"></a><i>Comment faire établir un acte de naissance ?</i></h3>
<p>Les personnes ayant acquis la nationalité française par décret ou par déclaration (exemple : acquisition par mariage) et qui ne disposent pas d’un acte de naissance français peuvent en faire la demande à l’adresse suivante :</p>
<p><strong>Service central d’état civil</strong></p>
<p><strong>Département Établissement</strong><br class="autobr"><strong>11 rue de la Maison blanche</strong><br class="autobr"><strong>44941 Nantes cedex 9 </strong></p>
<p>Les documents à joindre sont les suivants :</p>
<p>I – Demande écrite de la personne</p>
<p>II – Justificatifs de la nationalité française au moyen :</p>
<ul class="spip">
<li>soit de la photocopie complète de la déclaration d’acquisition de la nationalité française</li>
<li>soit de la photocopie du décret de naturalisation</li>
<li>soit d’un certificat de nationalité française.</li></ul>
<p>III – L’acte de naissance original étranger (même de date ancienne), le cas échéant <strong>légalisé</strong> ou <strong>apostillé</strong>, et sa traduction si nécessaire.</p>
<p>IV – Autre(s) document(s)</p>
<ul class="spip">
<li>une copie intégrale de l’acte de mariage</li>
<li>tout document précisant les éléments d’état civil des parents (actes de naissance, livret de famille, acte de mariage…)</li>
<li>éventuellement tout document judiciaire ou administratif concernant l’état civil de l’intéressé (adoption, divorce…).</li></ul>
<p>Des documents complémentaires pourront le cas échéant être demandés.</p>
<h3 class="spip"><a id="sommaire_5"></a><i>A qui s’adresser pour demander l’apposition d’une mention de divorce sur un acte de mariage ?</i></h3>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Lorsque le divorce a été prononcé par une juridiction française (tribunal ou cour d’appel) </p>
<p><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> un mariage célébré en France : il convient de contacter la mairie où a été célébré le mariage.<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> un mariage célébré à l’étranger et transcrit sur les registres consulaires : veuillez adresser au Service central d’état civil la copie du jugement (tribunal de grande instance) ou de l’arrêt (cour d’appel), la preuve du caractère définitif de la décision, le livret de famille et une lettre de demande datée et signée.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Lorsque le divorce a été prononcé dans un autre Etat de l’Union européenne (sauf Danemark)</p>
<p>Les informations sont disponibles sur le lien suivant : <a href="http://www.diplomatie.gouv.fr/fr/vivre-a-l-etranger/vivre-a-l-etranger-vos-droits-et/le-consulat-a-votre-service/etat-civil-a-l-etranger/transcription-des-actes-d-etat/article/les-divorces-a-l-etranger" class="spip_out">divorce à l’étranger</a></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Lorsque le divorce a été prononcé dans un Etat hors de l’Union européenne ou au Danemark</p>
<p>Afin de faire inscrire la mention de divorce sur votre acte de naissance et sur votre acte de mariage, il faut impérativement constituer un dossier de vérification d’opposabilité de votre divorce en France. Vérifier l’opposabilité en France d’un divorce étranger, signifie vérifier que cette décision peut être reconnue en France. Cette procédure est effectuée par le procureur de la République territorialement compétent par rapport au lieu de célébration du mariage. Par conséquent :<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> Pour un mariage célébré en France : il convient de transmettre un dossier de vérification d’opposabilité au procureur de la République dont dépend la mairie qui a célébré le mariage.<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> Pour un mariage célébré à l’étranger et transcrit sur les registres consulaires : il convient de transmettre un dossier de vérification d’opposabilité au procureur de la République de Nantes à l’adresse suivante :</p>
<p><strong>Tribunal de Grande Instance<br class="autobr">Service civil du Parquet<br class="autobr">19, quai François Mitterrand<br class="autobr">44921 Nantes cedex 9</strong></p>
<h3 class="spip"><a id="sommaire_6"></a><i>A qui s’adresser pour la mise à jour du livret de famille ?</i></h3>
<p>Veuillez vous adresser à la mairie de votre résidence, qui après étude de votre demande, le transmettra, le cas échéant, aux services concernés.</p>
<h3 class="spip"><a id="sommaire_7"></a><i>Comment faire rectifier un acte d’état civil suite à une erreur ou omission ?</i></h3>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Pour un acte établi par le Service central d’état civil (actes référencés : ACQ… ou AR…ou OP…) :</p>
<p>Veuillez transmettre la demande de rectification de cet acte à l’adresse suivante :</p>
<p><strong>Service central d’état civil</strong><br class="autobr"><strong>11, rue de la Maison Blanche</strong><br class="autobr"><strong>44941 NANTES CEDEX 9</strong></p>
<p>Il vous indiquera la suite qui peut être réservée à votre demande.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Pour un acte détenu au Service central d’état civil qui a été établi par les autorités consulaires françaises à l’étranger (actes référencés CSL) :</p>
<p>La demande est à envoyer à l’adresse suivante :</p>
<p><strong>Tribunal de Grande Instance<br class="autobr">Service civil du Parquet<br class="autobr">19 quai François Mitterrand<br class="autobr"> 44921 Nantes cedex 9</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Pour un acte établi par une mairie française :</p>
<p>La demande est à effectuer au Tribunal de grande instance du lieu de l’événement.</p>
<p><i>Dans tous les cas</i>, votre dossier doit être composé des pièces suivantes :</p>
<ul class="spip">
<li>la demande datée et signée accompagnée de toute pièce justifiant de l’omission ou de l’erreur prétendue</li>
<li>la copie intégrale de votre acte de naissance étranger dûment traduit, légalisé ou apostillé sauf convention contraire</li>
<li>la copie de l’acte ou des actes à rectifier à rectifier</li></ul>
<h3 class="spip"><a id="sommaire_8"></a><i>Comment faire légaliser un acte d’état civil délivré par une autorité étrangère ?</i></h3>
<ul class="spip">
<li>Si vous résidez en France : par le consul du pays où l’acte a été établi</li>
<li>Si vous résidez à l’étranger : Il convient de vous adresser aux autorités locales.</li></ul>
<h3 class="spip"><a id="sommaire_9"></a><i>Comment faire légaliser un acte d’état civil français ?</i></h3>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Si vous résidez en France, vous pouvez faire légaliser votre acte d’état civil auprès du ministère des Affaires étrangères et du Développement international, à l’adresse suivante :</p>
<p><strong>Bureau des Légalisations <br class="manualbr">57 boulevard des Invalides <br class="manualbr">75007 PARIS <br class="manualbr">Tél : 01.53.69.38.28. - 01.53.69.38.29. (de 14h à 16h)</strong></p>
<p>Ce bureau est ouvert au public du lundi au vendredi (sauf jours fériés) de 8h30 à 13h15.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Si vous résidez à l’étranger, il convient de vous adresser au consulat de France.</p>
<h3 class="spip"><a id="sommaire_10"></a><i>Comment faire apostiller un acte de l’état civil français ?</i></h3>
<p><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> Si l’acte a été délivré par le Service Central d’Etat Civil : veuillez envoyer votre demande à l’adresse suivante :<br class="manualbr"><strong>Service de l’apostille<br class="manualbr">Cour d’appel de Rennes<br class="manualbr">Place du Parlement de Bretagne<br class="manualbr">CS 66423<br class="manualbr">35064 Rennes cedex<br class="manualbr">Tél. : 02 23 20 43 00</strong></p>
<p><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> Dans les autres cas : chaque Cour d’appel est compétente pour délivrer l’apostille sur les actes publics dont le signataire de l’acte ou l’autorité ayant apposé une mention d’enregistrement ou de certification se situe sur son ressort. Il convient de s’adresser à la Cour d’appel dont dépend l’officier de l’état civil signataire de l’acte de l’état civil qui doit être apostillé.</p>
<h3 class="spip"><a id="sommaire_11"></a><i>Comment demander une attestation de non-inscription au Répertoire civil ? </i></h3>
<p>Le certificat de non inscription au répertoire civil est délivré exclusivement aux ressortissants étrangers nés à l’étranger. La demande doit comporter le nom/nom de jeune fille, le prénom, ainsi que les date et lieu (pays et ville) de naissance. Cette demande est à envoyer au Service central d’état civil par voie postale, ou par courriel à l’adresse suivante : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/questions-reponses/#rc.scec#mc#diplomatie.gouv.fr#" title="rc.scec..åt..diplomatie.gouv.fr" onclick="location.href=mc_lancerlien('rc.scec','diplomatie.gouv.fr'); return false;" class="spip_mail">rc.scec<span class="spancrypt"> [at] </span>diplomatie.gouv.fr</a></p>
<h3 class="spip"><a id="sommaire_12"></a><i>Comment demander un certificat de nationalité française ?</i></h3>
<p>Vous trouverez toutes les informations sur le lien suivant : <a href="http://vosdroits.service-public.fr/particuliers/F1051.xhtml" class="spip_out" rel="external">service-public.fr</a></p>
<h3 class="spip"><a id="sommaire_13"></a><i>Comment se procurer un acte de naissance spécial « S12 » ?</i></h3>
<p>Il s’agit d’un document délivré par les autorités algériennes. Pour l’obtenir, veuillez contacter le ministère de l’Intérieur algérien, ou compléter le formulaire en ligne à l’adresse suivante : demande12s.interieur.gov.dz</p>
<h3 class="spip"><a id="sommaire_14"></a><i>Comment demander un extrait de casier judiciaire ?</i></h3>
<p>Ce document peut être demandé par écrit à l’adresse suivante :</p>
<p><strong>Casier judiciaire national<br class="autobr">107 rue du Landreau<br class="autobr">44317 Nantes Cedex 3</strong></p>
<p>Un formulaire de demande est également accessible sur le site internet du <a href="https://www.cjn.justice.gouv.fr/cjn/b3/eje20" class="spip_out" rel="external">Ministère de la Justice</a></p>
<h3 class="spip"><a id="sommaire_15"></a><i>Quelle est la validité d’une carte nationale d’identité ?</i></h3>
<p><a href="http://vosdroits.service-public.fr/particuliers/F32303.xhtml" class="spip_out" rel="external">service-public.fr</a></p>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/questions-reponses/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
