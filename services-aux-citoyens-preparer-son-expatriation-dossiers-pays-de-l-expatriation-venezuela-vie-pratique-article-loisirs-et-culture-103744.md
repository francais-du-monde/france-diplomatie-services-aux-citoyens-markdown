# Loisirs et culture

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/vie-pratique/article/loisirs-et-culture-103744#sommaire_1">Tourisme </a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/vie-pratique/article/loisirs-et-culture-103744#sommaire_2">Activités culturelles</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/vie-pratique/article/loisirs-et-culture-103744#sommaire_3">Sports </a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Tourisme </h3>
<p>Le Venezuela est un très beau pays qui offre nombre de paysages variés. La saison touristique s’échelonne sur toute l’année mais la saison sèche se révèle souvent plus agréable. Avant de partir, tenez compte des vacances locales pour réserver à l’avance car les Vénézuéliens prennent d’assaut les hôtels à Noël, pendant le carnaval et la Semaine sainte ; ces périodes sont particulièrement festives et animées.</p>
<p>Pour en savoir plus, vous pouvez consulter le <a href="http://www.mintur.gob.ve/" class="spip_out" rel="external">site en français du ministère vénézuélien du Tourisme</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Activités culturelles</h3>
<p>Le carnaval, qui a lieu les lundis et mardis gras, est l’un des évènements culturels majeurs du pays. Les festivités débutent une semaine à l’avance et s’étendent à l’ensemble du Venezuela. A Mérida, dans les Andes, la plus grande fête, la fiesta <i>del sol</i>, a lieu dans les cinq jours qui précèdent le mercredi des Cendres et s’anime de concerts, de folklore et de combats de taureaux. Parmi les évènements à ne pas manquer lors d’un séjour au Venezuela le Corpus Christi, qui a lieu au mois de juin le long de la côte caraïbe et au sud de Caracas, constitue une des fêtes les plus colorées du Venezuela : parades costumées rouge et or, masques cornus et danse des diables.</p>
<p>Si l’art en général occupe une place importante, la musique en particulier a un rôle prépondérant dans a culture vénézuélienne. L’offre musicale est assez variée, elle est le fruit de la synthèse entre les rythmes africains, européens et amérindiens.</p>
<p>Les chaines de télévision locales sont de qualité moyenne mais il est possible d’avoir accès à une offre plus ample moyennant un abonnement à des bouquets par câble ou satellite. Une seule chaîne française est disponible sur le câble (bouquet Intercable) : TV5.</p>
<p>Les principaux quotidiens nationaux sont Ultimas noticias, El Universal, El Nacional, El Mundo, 2001, Tal Cual et la Cadena global. Dans certains kiosques de Caracas vous pouvez trouver des journaux en langue anglaise. Il est également possible d’acheter des revues françaises deux fois par semaine devant le Colegio Francia à 13h ou de les consulter à l’Alliance Française. Une librairie française propose aussi des livres et ouvrages divers en français.</p>
<h3 class="spip"><a id="sommaire_3"></a>Sports </h3>
<p>Tous les sports, à l’exception des sports d’hiver, peuvent être pratiqués au Venezuela. Les sports les plus populaires sont le baseball, le football et le basket. Traditionnellement le pays reste aussi très attaché à la corrida, l’arène de la ville de Valencia étant la plus célèbre.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/vie-pratique/article/loisirs-et-culture-103744). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
