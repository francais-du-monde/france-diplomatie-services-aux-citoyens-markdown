# Emploi, stage

<h2 class="rub23460">Marché du travail</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/emploi-stage/#sommaire_1">Secteurs à fort potentiel</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/emploi-stage/#sommaire_2">Professions réglementées</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/emploi-stage/#sommaire_3">Rémunération</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Secteurs à fort potentiel</h3>
<p>La plupart des Français expatriés travaillent dans le secteur tertiaire. Il existe des emplois dans le secteur du tourisme pour les magyarophones et au sein des ressources humaines pour les francophones.</p>
<p><strong>Métiers les plus fréquemment recherchés :</strong></p>
<ul class="spip">
<li>Vendeurs et démonstrateurs en magasin</li>
<li>Charpentiers métalliers et monteurs de charpentes métalliques</li>
<li>Serveurs et barmans</li>
<li>Cuisiniers</li>
<li>Conducteurs de camions et poids lourds</li>
<li>Boulangers, pâtissiers et confiseurs</li>
<li>Aides ménagers</li>
<li>Mécaniciens et ajusteurs</li>
<li>Cireurs de chaussures et autres petits métiers des rues</li>
<li>Secrétaires</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Professions réglementées</h3>
<p>Les emplois de fonctionnaires (<i>köztisztviselo</i>) sont strictement réservés aux ressortissants hongrois, par contre les employés de l’Etat (<i>közalkalmazott</i>) peuvent être des ressortissants étrangers.</p>
<h3 class="spip"><a id="sommaire_3"></a>Rémunération</h3>
<p>En Hongrie, il existe un salaire minimum garanti, dont le montant est fixé chaque année par le gouvernement. Le montant du salaire minimum est national et s’applique obligatoirement à l’ensemble des employeurs et des salariés. Depuis le 1er janvier 2014, le montant du salaire minimum brut est de :</p>
<ul class="spip">
<li>101 500 HUF (soit approximativement 338 euros) pour le salaire mensuel ;</li>
<li>23 360 HUF (soit approximativement 78 euros) pour le salaire hebdomadaire ;</li>
<li>4673 HUF (soit approximativement 16 euros) pour le salaire journalier</li>
<li>584 HUF (soit approximativement 1,9 euros) pour le salaire horaire. Le salaire minimum brut garanti des travailleurs ayant une formation de niveau secondaire ou occupant un poste nécessitant au moins une formation professionnelle de niveau secondaire s’élève à 118 000 HUF (soit approximativement 393 euros).</li></ul>
<p>L’employeur est tenu de verser un salaire au travailleur et il n’y a pas de dérogation à cette règle. Un salaire peut être établi sur la base soit du temps de travail, soit du rendement, soit à la fois du temps de travail et du rendement.</p>
<p>Le salaire du travailleur doit être calculé et payé en une fois pour chaque périodicité de travail. Cependant, les parties peuvent convenir de dispositions différentes. Le salaire peut être versé en espèce ou par virement bancaire. L’employé reçoit un bulletin de salaire détaillé. Les déductions du salaire ne peuvent être effectuées que sur la base d’une loi, d’une décision exécutoire ou d’un accord avec le travailleur. Le bulletin de salaire contient le montant brut du salaire du travailleur, le montant des prestations d’assurance sociale (par ex. les allocations familiales), le montant des impôts prélevés à la source, des cotisations salariales, d’assurance maladie et de retraite et le montant net du salaire payé.</p>
<p>L’employeur déduit du salaire brut de la personne physique les cotisations suivantes, qu’il verse au NAV (à l’Office national du Fisc et des Douanes) :</p>
<ul class="spip">
<li>27% correspondant aux cotisations sociales payables par l’employeur. L’employeur peut bénéficier de taux différents, précisés ci-après, s’il emploi des personnes ayant des enfants ou s’il crée de nouveaux postes ou s’il emploie des chercheurs ;
<br>— 12,5% du salaire brut dans le cas des employés qui occupent des postes ne nécessitant pas de qualification ;
<br>— 12,5% du salaire brut dans le cas des employés de moins de 25 ans ou de plus de 55 ans ;
<br>— 0% du salaire brut durant les deux premières années d’emploi, puis 12,5% du salaire brut pour la troisième année d’emploi dans le cas des demandeurs d’emploi de longue durée ;
<br>— 0% du salaire brut durant les deux premières années d’emploi, puis 12,5% du salaire brut pour la troisième année d’emploi dans le cas des employés qui reprennent une activité après une période de droits à l’allocation de congés maternités, pendant ou après la période de droits à l’allocation d’éducation de l’enfant ;
<br>— 0% du salaire brut dans le cas des employés qui travaillent en tant que chercheurs ;
<br>— 0% du salaire brut durant les deux premières années d’emploi, puis 12,5% du salaire brut pour la troisième année d’emploi dans le cas de nouveaux salariés, d’entreprises opérant dans des zones spécifiques exemptées de taxes sur les entreprises.</li>
<li>1,5% correspondant à la contribution à la formation professionnelle ;</li>
<li>10 % correspondant à la cotisation retraite ;</li>
<li>7% correspondant à la cotisation d’assurance maladie ;</li>
<li>1,5% correspondant à la cotisation d’assurance chômage ;</li>
<li>16% correspondant au prélèvement à la source de l’impôt sur le revenu des personnes physiques. Il est à noter qu’une cotisation de prépension a été introduite. Elle est payée par l’employeur ou l’entrepreneur indépendant qui emploie des travailleurs à des postes donnant droit à une prépension (pression psychique élevée, exposition à certains risques). Le taux de cette cotisation est de 13 %. Le versement peut être effectué selon une fréquence mensuelle au compte en banque du NAV spécialement destiné à cet effet, soit par l’employeur étranger, soit par son mandataire, à défaut de quoi il appartient au salarié de s’acquitter du versement de la cotisation en question.</li></ul>
<p>Le dispositif START favorise l’entrée sur le marché du travail des jeunes en début de carrière, par la diminution des cotisations sociales de l’employeur. Pour les employés travaillant avec une carte START, qui ont reçu une formation du premier ou du second degré, les cotisations sociales salariales s’élèvent à 10% la première année et à 20% la seconde année. Pour les salariés diplômés de l’enseignement supérieur, les cotisations sociales salariales s’élèvent à 10% au cours des neuf premiers mois et à 20% lors des trois mois suivants. Les employés reprenant une activité à la suite d’un congé parental ou d’une période d’accompagnement de soins d’un membre de leur famille (avantage dont ils peuvent bénéficier pendant deux ans), les cotisations s’élèvent à 10 % la première année et à 20 % la seconde année. Pour les employeurs qui recrutent des demandeurs d’emploi de longue durée ou âgés de plus de 50 ans, la cotisation sociale salariale est de 0 % la première année et de 10 % la seconde. Les avantages ne peuvent dépasser le double du salaire minimum. Au-dessus de ce montant, ce sont les règles générales de cotisation qui s’appliquent. Lorsqu’un salarié disposant d’une carte START ou choisissant le système EKHO occupe un poste donnant droit à la prépension, l’employeur doit verser les cotisations réduites, ainsi que la cotisation de prépension. Si le système EKHO est choisi, il y a lieu de verser une cotisation de 15%.</p>
<p>Exemples de fourchettes de salaires pour des Français (chiffres de 2014). Il convient de noter qu’il existe d’importantes différences en fonction des secteurs, de la taille et de la complexité des entreprises, du statut de l’employé, de son expérience (au sein de multinationales, d’un environnement international), de ses connaissances en langue étrangère.</p>
<ul class="spip">
<li>Assistant : de 250 000 à 600 000 HUF/mois, soit approximativement de 833 à 2000 euros/mois ;</li>
<li>Directeur : de 1500 000 à 5000 000 HUF/mois, soit approximativement de 5000 à 16 667 euros/mois ;</li>
<li>Ingénieur : de 500 000 à 2000 000 HUF/mois, soit approximativement de 1667 à 6667 euros/mois. Des primes peuvent également s’ajouter (écolage, assurance vie, chèques vacances, habillement, etc.).</li></ul>
<p>Les avantages extra-salariaux (p. ex. les chèques vacances, l’abonnement pour les moyens de transport locaux, les chèques restaurant jusqu’à 12 500 HUF par mois, approximativement 42 euros) ne dépassant pas la somme de 500 000 HUF (approximativement 1667 euros), sont imposés à hauteur de 16% des 1,19% du montant. Ces avantages extra-salariaux sont également soumis à l’obligation de paiement d’une contribution à la sécurité sociale à hauteur de 14%. Pour les avantages extra-salariaux dépassant la somme de 500 000 HUF, la contribution à la sécurité sociale est de 27%.</p>
<p><i>Mise à jour : juin 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-hongrie-emploi-stage-article-stages.md" title="Stages">Stages</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-hongrie-emploi-stage-article-entretien-d-embauche-113472.md" title="Entretien d’embauche">Entretien d’embauche</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-hongrie-emploi-stage-article-lettre-de-motivation-113471.md" title="Lettre de motivation">Lettre de motivation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-hongrie-emploi-stage-article-curriculum-vitae-113470.md" title="Curriculum vitae">Curriculum vitae</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-hongrie-emploi-stage-article-recherche-d-emploi.md" title="Recherche d’emploi">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-hongrie-emploi-stage-article-reglementation-du-travail.md" title="Réglementation du travail">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-hongrie-emploi-stage-article-marche-du-travail.md" title="Marché du travail">Marché du travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/emploi-stage/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
