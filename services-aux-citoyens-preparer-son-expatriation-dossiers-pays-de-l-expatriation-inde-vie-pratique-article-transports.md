# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/vie-pratique/article/transports#sommaire_1">Importation de véhicule</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/vie-pratique/article/transports#sommaire_2">Permis de conduire</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/vie-pratique/article/transports#sommaire_3">Code de la route</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/vie-pratique/article/transports#sommaire_4">Assurances et taxes</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/vie-pratique/article/transports#sommaire_5">Achat et location</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/vie-pratique/article/transports#sommaire_6">Réseau routier</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/vie-pratique/article/transports#sommaire_7">Transports en commun</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Importation de véhicule</h3>
<p>Compte tenu de l’importance des taxes sur les véhicules importés (de 130 à 300%), il est recommandé d’acheter un véhicule sur place. La production locale offre désormais un assez grand choix de véhicules à des prix raisonnables.</p>
<h3 class="spip"><a id="sommaire_2"></a>Permis de conduire</h3>
<p><strong>Le permis de conduire international est reconnu en Inde.</strong> Ce dernier peut être utilisé par les Français de passage mais non par les résidents qui doivent obtenir un permis indien auprès de la "Motor Licensing Authority" (le "RTO" - Road Transport Office).</p>
<h3 class="spip"><a id="sommaire_3"></a>Code de la route</h3>
<p>La conduite s’effectue à gauche et la priorité est à droite mais il n’existe pas de règle en la matière. Les indications routières sont souvent écrites en langues locales. La vitesse autorisée en ville est de 50 km/h, hors agglomération 90 km/h. En pratique, elle est limitée par l’encombrement des routes.</p>
<h3 class="spip"><a id="sommaire_4"></a>Assurances et taxes</h3>
<p>Une taxe variable en fonction de la puissance et de l’âge du véhicule est à acquitter.</p>
<p>L’assurance au tiers est obligatoire, il est préférable de souscrire une police "tous risques". L’affiliation à une compagnie locale est indispensable pour conduire son véhicule.</p>
<p>Les assurances indiennes tiennent compte dans le montant des primes, des bonus antérieurs (réduction de prime jusqu’à 60 %). Il est donc recommandé de se munir d’une attestation du précédent assureur comme justificatif. En général, les assurances sont moins onéreuses qu’en occident.</p>
<p>Exemples de prime annuelle :</p>
<p>Tous risques sans franchise à New Delhi : 300 € pour un véhicule d’une valeur de 7000 €.</p>
<p>Pour les personnes ayant importé un véhicule, l’assurance sur les droits de douane (121 %) est obligatoire. En effet, en Inde, si à la suite d’un accident, la voiture est réduite à l’état d’épave, la législation locale prévoit que celle-ci doit être réexportée ou les droits de douane acquittés. C’est pourquoi, il est recommandé de souscrire cette assurance sur les "droits de douane".</p>
<h3 class="spip"><a id="sommaire_5"></a>Achat et location</h3>
<h4 class="spip">Achat</h4>
<p>Les marques suivantes ont constitué une entreprise conjointe en Inde et commercialisent certains modèles : Mercedes, BMW, Fiat, Ford, Mitsubishi, Honda, Toyota, Huyndai, Chevrolet, etc. Suzuki est présent à travers l’entreprise conjointe Maruti. La seule marque française représentée en Inde est Renault.</p>
<h4 class="spip">Location</h4>
<p>On peut louer un véhicule auprès de compagnies internationales, ou de petits loueurs locaux. Les tarifs sont variables selon la durée de location et le modèle. A New-Delhi, de 30 € la journée à 500 € le mois.</p>
<p>L’assurance et les dépenses d’entretien du véhicule sont comprises dans le prix de la location qui ne prévoit cependant pas la mise à disposition d’un chauffeur (150€ de plus).</p>
<h3 class="spip"><a id="sommaire_6"></a>Réseau routier</h3>
<p>Le réseau routier indien est en général médiocre. Les nouvelles autoroutes sont bonnes mais peu nombreuses, les routes nationales sont souvent étroites et mal entretenues.</p>
<p>On peut n’utiliser la route que pour de courts trajets en louant éventuellement un véhicule avec chauffeur (prix intéressants). Il est par ailleurs vivement déconseillé de rouler de nuit.</p>
<h3 class="spip"><a id="sommaire_7"></a>Transports en commun</h3>
<p>Les liaisons intérieures se font par avion (moyen le plus utilisé par les expatriés), par chemin de fer (possible pour des trajets de 200 à 300 km. La 1ère classe est climatisée et confortable) ou par autocar.</p>
<p>Les transports en commun en ville sont assurés surtout par autobus (souvent bondés), par taxis collectifs, bon marché mais peu confortables, et par taxis individuels et "rickshaw".</p>
<p>A noter que Calcutta et New Delhi sont équipés de lignes de métro. Le métro est en cours d’ouvrage à Bangalore et devrait être terminé d’ici 2013.</p>
<p>A Pondichéry les expatriés se déplacent souvent en deux roues ou en auto-rickshaw. Bombay a le meilleur système de transport du pays. On s’y déplace rapidement par les trains de banlieue, au confort sommaire et bondés aux heures de pointe (il est recommandé d’acheter des billets de 1ère classe).</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/vie-pratique/article/transports). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
