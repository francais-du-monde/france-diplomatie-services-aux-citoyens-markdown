# Vaccination

<p>La loi indienne ne prévoit pas de vaccination particulière pour un voyageur en provenance de France lorsqu’il entre sur le territoire indien, Toutefois, il convient de vous assurer, d’une part, que votre calendrier vaccinal est à jour et il est conseillé, d’autre part, d’effectuer certains vaccins préalablement à votre séjour :</p>
<p><strong>Adultes </strong> : mise à jour des vaccinations contre la diphtérie, le tétanos et la poliomyélite ; vaccination contre la typhoïde, l’hépatite A, l’hépatite B.</p>
<p><strong>Enfants </strong> : vaccinations recommandées en France par le ministère de la Santé - et en particulier : B.C.G. et hépatite B dès la naissance, rougeole dès l’âge de neuf mois.</p>
<p>Rage pour un séjour de longue durée. Encéphalite japonaise à partir de l’âge d’un an à faire sur place. Méningocoque A + C.</p>
<p>On peut trouver sur place des vaccins sous forme injectable : hépatite A, hépatite B, typhoïde, tétanos, poliomyélite, méningite, rage.</p>
<p>Pour en savoir plus, lisez notre rubrique <a href="services-aux-citoyens-preparer-son-expatriation-sante.md" class="spip_in">Santé</a> ou rendez vous sur la page dédiée à l’Inde sur le site internet de l’Institut Pasteur (recommandations et actualité infectieuse).</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/entree-et-sejour/article/vaccination-110401). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
