# Protection sociale

<h2 class="rub23574">Régime local de sécurité sociale</h2>
<p>Vous trouverez une présentation détaillée du système de sécurité sociale de la Slovénie sur le site de notre partenaire, le <a href="http://www.cleiss.fr/" class="spip_out" rel="external">CLEISS</a> (Centre des liaisons européennes et internationales de sécurité sociale). En voici la table des matières :</p>
<ul class="spip">
<li><a href="http://www.cleiss.fr/docs/regimes/regime_slovenie.html#introduction" class="spip_out" rel="external">Introduction</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_slovenie.html#generalites" class="spip_out" rel="external">Généralités</a> (Organisation, financement, taux de cotisation sur les salaires…)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_slovenie.html#maladie-deces" class="spip_out" rel="external">Maladie, décès</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_slovenie.html#maternite" class="spip_out" rel="external">Maternité</a> (Prestations en nature et en espèces)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_slovenie.html#pension" class="spip_out" rel="external">Assurance pension (vieillesse, invalidité, survivants)</a> (Pensions de vieillesse, d’invalidité, de survivant, aide sociale)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_slovenie.html#adtmp" class="spip_out" rel="external">Accidents du travail et maladies professionnelles</a> (Régime indemnitaire, y compris pour les survivants)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_slovenie.html#chomage" class="spip_out" rel="external">Chômage</a> (Indemnités et assistance chômage)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_slovenie.html#pf" class="spip_out" rel="external">Prestations familiales</a> (Allocations familiales, de naissance, d’adoption…)</li></ul>
<p><i>Mise à jour : septembre 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-slovenie-protection-sociale-23574-article-convention-de-securite-sociale-114775.md" title="Convention de sécurité sociale">Convention de sécurité sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-slovenie-protection-sociale-23574-article-regime-local-de-securite-sociale-114774.md" title="Régime local de sécurité sociale">Régime local de sécurité sociale</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/protection-sociale-23574/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
