# Volontariat

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/emploi/article/volontariat#sommaire_1">Bénévolat - Volontariat à l’international (VIE, VIA) </a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/emploi/article/volontariat#sommaire_2">Service volontaire européen (SVE)</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/emploi/article/volontariat#sommaire_3">Service civique </a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Bénévolat - Volontariat à l’international (VIE, VIA) </h3>
<p><strong>1 - Le volontariat civil international</strong></p>
<p>Dans le cadre de la réforme du service national, l’Assemblée nationale et le Sénat ont adopté une loi relative au volontariat civil international (VI). Au titre de la coopération internationale, la loi prévoit que « les volontaires internationaux participent à l’action de la France dans le monde en matière culturelle et d’environnement, de développement technique, scientifique et économique et d’action humanitaire ».</p>
<p>Ce volontariat international peut être effectué dans une entreprise (VIE) ou auprès d’une administration (VIA).</p>
<p><strong>Conditions</strong></p>
<p>Pour partir à l’étranger comme volontaire international, vous devez :</p>
<ul class="spip">
<li>être âgé de plus de 18 et de moins de 28 ans, la condition d’âge s’appréciant à la date de dépôt de la candidature ;</li>
<li>être ressortissant français ou d’un pays de l’<a href="http://www.diplomatie.gouv.fr/fr/politique-etrangere-de-la-france/europe/" class="spip_in">Espace économique européen</a> ;</li>
<li>être en règle avec les obligations de service national de votre pays ;</li>
<li>jouir de vos droits civiques ;</li>
<li>justifier d’un casier judiciaire vierge ;</li>
<li>remplir les conditions d’aptitude physique exigées pour les personnes exerçant des activités de même nature dans l’organisme d’accueil.</li></ul>
<p><strong>Un cadre de mission souple</strong></p>
<p>L’engagement au volontariat international s’inscrit dans des missions de 6 à 24 mois qui s’effectuent :</p>
<ul class="spip">
<li>en entreprise (VIE) ;</li>
<li>au sein d’une structure française, publique ou para-publique, relevant du ministère des Affaires étrangères ou du ministère de l’Economie (VIA) ;</li>
<li>dans des centres de recherche et universités publiques ;</li>
<li>auprès d’organisations internationales ou d’associations agréées. Dans tous les cas, le volontaire international est placé sous la tutelle de l’Ambassade de France du pays où s’effectue la mission.</li></ul>
<p>Le volontariat international couvre un large éventail de secteurs d’activité en entreprise (commerce, industrie, artisanat, culture) et tous les niveaux de qualification. Il s’adresse aux jeunes (hommes ou femmes) qu’ils soient étudiants, à la recherche d’un emploi ou jeunes diplômés exerçant déjà une activité. Le volontariat international ne peut être fractionné et doit être accompli auprès d’un seul organisme.</p>
<p><strong>Rémunération</strong></p>
<p>Le volontaire international est rémunéré entre 1 300 et 3 500 euros nets par mois, couverture sociale incluse, selon le pays et sans distinction de niveau d’études.</p>
<p>Cette rémunération forfaitaire comprend une partie fixe (682,97 euros depuis le 1er janvier 2011) et une partie variable en fonction du pays d’affectation.</p>
<p>Le Centre d’Information sur le Volontariat International (CIVI) est l’organisme d’information et de promotion du volontariat international. <br class="autobr">Il est placé sous la tutelle du ministère des Affaires étrangères, de la DGTDG Trésor (Direction générale du Trésor dépendant du ministère de l’Économie, des Finances - MINEFI) et d’UBIFRANCE (Agence française pour le développement international des entreprises).</p>
<p>Le CIVI centralise les candidatures qui doivent être déposées sur son site Internet. Pour postuler sur un poste de VIA relevant de la compétence du MAEE, du MINEFI ou d’UBIFRANCE, vous devez répondre aux annonces qui sont publiées sur le site en envoyant à l’adresse de courriel indiquée sur l’annonce, votre CV et une lettre de motivation en français.</p>
<p><strong>Contact :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.civiweb.com/" class="spip_out" rel="external">Business France Centre d’information sur le volontariat international (CIVI)</a><br class="manualbr">Téléphone : 0810 10 1828 (numéro Azur)  </p>
<p><strong>2- Le volontariat de solidarité internationale</strong></p>
<p><strong>Généralités</strong></p>
<p>Le contrat de volontariat de solidarité internationale (VSI) est défini par les textes suivants, consultables sur le site Internet de <a href="http://www.legifrance.gouv.fr/" class="spip_out" rel="external">Legifrance</a>, rubrique <strong>Rechercher un JO</strong> :</p>
<ul class="spip">
<li>loi n°2005-159 du 23 février 2005 relative au contrat de volontariat de solidarité internationale, publiée au Journal officiel du 24 février 2005 ;</li>
<li>décret d’application n°2005-600 du 27 mai 2005 publié au Journal Officiel du 29 mai 2005 ;</li>
<li>arrêté du 21 décembre 2005 fixant, d’une part, le montant des aides de l’Etat au volontariat de solidarité internationale et, d’autre part, les montants minimum et maximum des indemnités versées par les associations aux volontaires, publié au Journal officiel du 24 décembre 2005 ;</li>
<li>décret n°2000-1159 du 30 novembre 2000, article 8.</li></ul>
<p>Le contrat doit :</p>
<ul class="spip">
<li>être établi par écrit entre une association de droit français agréée par le ministère des Affaires étrangères et une personne majeure ;</li>
<li>organiser une collaboration désintéressée entre l’association et le volontaire ;</li>
<li>avoir une durée limitée dans le temps, qui ne peut excéder deux ans (à noter que la durée cumulée des missions, accomplies de façon continue ou pas, par le volontaire ne peut excéder six ans) ;</li>
<li>avoir pour objet l’accomplissement d’une mission d’intérêt général à l’étranger dans les domaines de la coopération au développement et de l’action humanitaire ;</li>
<li>contenir obligatoirement les informations prévues à l’article 9 du décret 2005-600 ;</li>
<li>fixer le montant et les conditions de versement de l’indemnité.</li></ul>
<p><strong>Conditions à remplir pour le candidat</strong></p>
<p>Le candidat doit :</p>
<ul class="spip">
<li>être majeur ;</li>
<li>accomplir sa mission dans un Etat situé hors de <a href="http://www.mfe.org/Default.aspx?SID=13082" class="spip_out" rel="external">l’Union européenne</a> ou partie à l’accord sur l’Espace économique européen. En outre, il ne doit pas être ressortissant, ni résident de l’Etat sur le territoire duquel il accomplit sa mission. Aucune activité professionnelle ne peut être exercée en même temps que la mission.</li></ul>
<p><strong> Les modalités</strong></p>
<p>Les associations garantissent aux volontaires :</p>
<ul class="spip">
<li>Une formation avant leur départ qui comprend une préparation technique, une information sur les conditions d’accomplissement de la mission et une sensibilisation aux relations interculturelles ;</li>
<li>Une indemnité ;</li>
<li>La prise en charge des frais de voyage aller et retour du volontaire et de sa famille ;</li>
<li>Une couverture sociale pour le volontaire et ses ayants droit, ainsi qu’une assurance maladie complémentaire, une assurance rapatriement sanitaire et une assurance en responsabilité civile ;</li>
<li>La délivrance, à l’issue de la mission, d’une attestation d’accomplissement de mission de volontariat de solidarité internationale ;</li>
<li>Un soutien technique pour leur réinsertion en fin de mission. Le volontaire bénéficie au minimum d’un congé de deux jours non chômés par mois de mission (au sens de la législation de l’État d’accueil).</li></ul>
<p><strong> Indemnité</strong></p>
<p>Le montant de l’indemnité versée au volontaire est fixé dans le contrat. Le montant minimum de cette indemnité est fixé à 100 euros, hors prise en charge du logement et de la nourriture. Le montant maximum correspond au montant de l’indemnité mensuelle auquel s’ajoute l’indemnité supplémentaire liée au pays d’affectation.</p>
<p>Cette indemnité n’est soumise en France ni à l’impôt sur le revenu, ni aux cotisations et contributions sociales.</p>
<p><strong> Droits au chômage au retour en France </strong></p>
<p>Le VSI d’une durée continue d’au moins un an est considéré comme un motif légitime de démission. Le volontaire qui a quitté son activité professionnelle pour partir en mission de VSI pourra percevoir des indemnités de chômage à son retour seulement s’il a suffisamment cotisé et si son contrat de VSI était d’au moins un an.</p>
<p>Partir en VSI suspend le versement des indemnités de chômage qui reprendra lors du retour en France.</p>
<p>A leur retour en France, les volontaires qui ne remplissent pas les conditions d’attribution du RSA et sont inscrits auprès de Pôle Emploi peuvent demander, dans un délai maximum d’un an à compter de la fin de la mission, à recevoir une prime forfaitaire d’insertion professionnelle. Celle-ci est versée trimestriellement et son montant total s’élève à 2001 euros. Cette prime n’est pas cumulable avec une autre aide financière liée à la recherche d’emploi et n’est versée que pour une durée maximale de neuf mois.</p>
<p>Le volontaire qui a effectué au moins 24 mois de mission peut prétendre, à son retour en France, à une indemnité de réinstallation dont le montant s’élève à 3700 euros. Si la durée de la mission est comprise entre 12 et 24 mois, l’indemnité de réinstallation n’est versée que si le retour est déterminé par un cas de force majeure. Le montant de l’indemnité est alors fonction de la durée de la mission.</p>
<p><strong>Candidatures</strong></p>
<p>Il est vivement recommandé de s’adresser aux associations (23 à ce jour) agréées par le ministère des Affaires étrangères pour l’envoi de volontaires de solidarité internationale ou inscrites à des collectifs d’ONG.</p>
<p>Certaines organisations non gouvernementales (ONG) ne recrutent que par le biais de candidatures spontanées. D’autres mettent régulièrement en ligne leurs offres de volontariat sur différents sites Internet et, notamment, celui de <a href="http://www.coordinationsud.org/" class="spip_out" rel="external">Coordination SUD</a>.</p>
<p>De plus en plus, les postes offerts nécessitent des compétences et une expérience dans des domaines techniques et de gestion.</p>
<p><strong>Obtenir de l’information sur le volontariat de solidarité internationale</strong></p>
<p><strong>France volontaires</strong><br class="manualbr">11 Rue Maurice Grandcoing - BP 203 <br class="manualbr">94203 Ivry sur Seine Cedex<br class="manualbr">Téléphone : 01 53 14 20 30 - Télécopie : 01 53 14 20 50<br class="manualbr">Courriel : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/emploi/article/volontariat#contact#mc#france-volontaires.org#" title="contact..åt..france-volontaires.org" onclick="location.href=mc_lancerlien('contact','france-volontaires.org'); return false;" class="spip_mail">contact<span class="spancrypt"> [at] </span>france-volontaires.org</a></p>
<p>La plate-forme France Volontaires, opérateur de référence en matière de volontariat, a été lancée en janvier 2010, se substituant à l’association française des volontaires du progrès (AFVP).</p>
<p>Cette plate-forme est destinée à promouvoir les différentes formes de "volontariats internationaux d’échanges et de solidarité" (VIES).</p>
<p>Elle constitue un lieu de communication, d’information et de capitalisation des expériences entre acteurs publics et non-gouvernementaux. Elle participe à la promotion de la vision française de la solidarité au développement que porte le volontariat.</p>
<p>Actuellement, on compte 300 volontaires répartis dans plus de 40 pays, principalement en Afrique. Elle est aussi présente en Asie, en Amérique latine et aux Caraïbes. Ses domaines d’intervention sont le développement local et territorial, la décentralisation et la maîtrise d’ouvrage locale, l’économie, la formation professionnelle, la santé, l’action sanitaire et sociale, l’éducation, l’enseignement, la francophonie, l’action culturelle et la communication, l’environnement, la biodiversité et le tourisme durable, le droit, la citoyenneté et la société civile, l’aménagement, les infrastructures et les équipements.</p>
<p>France volontaires recrute essentiellement sur candidatures spontanées. Les candidats doivent être diplômés de l’enseignement supérieur ou posséder une bonne expérience professionnelle dans un domaine de compétence répondant aux besoins de France volontaires, être exempts de toute contre-indication médicale et posséder le permis de conduire. La durée d’une mission est de deux ans.</p>
<p><a href="http://www.clong-volontariat.org/" class="spip_out" rel="external">Comité de liaison des organisations non gouvernementales de volontariat (CLONG-Volontariat)</a> <br class="manualbr">14 Passage Dubail - 75010 Paris<br class="manualbr">Téléphone : 01 42 05 63 00 - Télécopie : 01 44 72 93 73<br class="manualbr">Courriel : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/emploi/article/volontariat#clong#mc#clong-volontariat.org#" title="clong..åt..clong-volontariat.org" onclick="location.href=mc_lancerlien('clong','clong-volontariat.org'); return false;" class="spip_mail">clong<span class="spancrypt"> [at] </span>clong-volontariat.org</a></p>
<p>Créé en 1979, le CLONG-Volontariat regroupe 14 associations. Il a trois objectifs principaux :</p>
<ul class="spip">
<li>la promotion de l’engagement volontaire et la défense des intérêts des volontaires et de leurs associations ;</li>
<li>la valorisation du volontariat comme modalité fondamentale d’expression de la citoyenneté au sein de la solidarité internationale ;</li>
<li>parvenir à des avancées juridiques et statutaires pour le volontariat de solidarité internationale et renforcer la qualité des pratiques de volontariat. Il a également une mission d’information générale sur le VSI à travers son site Internet et des réunions d’information à Paris le premier mardi de chaque mois (inscriptions sur son site).</li></ul>
<p><a href="http://decouvrirlemonde.jeunes.gouv.fr/" class="spip_out" rel="external">Découvrir le Monde site</a> :<br class="autobr">portail de la mobilité internationale pour les jeunes.</p>
<p><a href="http://www.ritimo.org/" class="spip_out" rel="external">RITIMO</a> <br class="manualbr">21 ter rue Voltaire - 75011 Paris<br class="manualbr">Téléphone : 01 44 64 74 14 <br class="manualbr">Courriel : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/emploi/article/volontariat#contact#mc#ritimo.org#" title="contact..åt..ritimo.org" onclick="location.href=mc_lancerlien('contact','ritimo.org'); return false;" class="spip_mail">contact<span class="spancrypt"> [at] </span>ritimo.org</a></p>
<p>Le <strong>Réseau d’information et de documentation pour le développement durable et la solidarité internationale</strong> regroupe 70 centres de documentation et lieux d’information répartis dans toute la France qui informent, entre autres, sur la solidarité internationale, les relations Nord-Sud et le développement durable.</p>
<p>Les centres de documentation proposent au public une documentation sur la solidarité internationale et les pays du Sud, des conseils pour le départ, pour monter un projet ou s’informer sur les associations existantes, des guides pratiques, etc.</p>
<p>RITIMO publie tous les deux ans le <strong>Répertoire national des acteurs de la solidarité internationale</strong> consultable sur le site de <a href="http://www.ritimo.org/" class="spip_out" rel="external">RITIMO</a>.</p>
<p><a href="http://www.unesco.org/ccivs/" class="spip_out" rel="external">Comité de coordination du service volontaire international</a> <br class="manualbr">(CCSVI)Maison de l’Unesco - 1 rue Miollis - 75015 Paris<br class="manualbr">Téléphone : 01 45 68 49 36 - Télécopie : 01 42 73 05 21<br class="manualbr">Courriel : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/emploi/article/volontariat#ccivs#mc#unesco.org#" title="ccivs..åt..unesco.org" onclick="location.href=mc_lancerlien('ccivs','unesco.org'); return false;" class="spip_mail">ccivs<span class="spancrypt"> [at] </span>unesco.org</a></p>
<p>Créé en 1948 sous l’égide de l’UNESCO, le CCSVI est organisation non gouvernementale internationale qui favorise la paix, le développement et les droits de l’homme par la promotion, le développement et la coordination du service volontaire aux niveaux nationaux, régionaux et internationaux. Pour ce faire, il assure la coordination entre les associations de volontariat nationales et internationales qui forment le réseau de ses membres. On compte actuellement plus de 300 organisations membres présentes dans plus de 110 pays.</p>
<p>Ces organisations travaillent dans le domaine de l’environnement, de l’alphabétisation, de la préservation du patrimoine culturel, de l’aide aux réfugiés, de la santé, des urgences et du développement. Les programmes sont généralement exécutés sous la forme de chantiers internationaux où se retrouvent des volontaires nationaux et étrangers qui œuvrent pour une même cause. Certaines organisations proposent également des activités de volontariat à moyen et long terme.</p>
<p>Pour ceux qui souhaitent partir, le CCSVI met en ligne gratuitement sur son site le guide <i>Être volontaire en</i> <i>Europe, Afrique, Asie et Amériques. Guide et adresses indispensables. </i>Ce guide donne une vue d’ensemble sur les actions du CCSVI, ainsi que des adresses et conseils utiles avant de partir. Le CCSVI publie d’autres brochures plus spécialisées sur le volontariat et les différents domaines qu’il peut toucher.</p>
<p><strong>Sites institutionnels français</strong></p>
<ul class="spip">
<li><a href="http://www.service-public.fr/" class="spip_out" rel="external">Portail de l’administration française</a>.</li>
<li>Site Internet du <a href="http://www.service-civique.gouv.fr/" class="spip_out" rel="external">Service civique</a></li>
<li><a href="http://www.france-volontaires.org/" class="spip_out" rel="external">Plate-forme</a> regroupant toutes les informations concernant le volontariat international d’échange et de solidarité. Sa mission est d’orienter les candidats vers 39 ONG et associations partenaires du projet. Un numéro vert est à la disposition du public : 0811.06.10.10.</li></ul>
<p><strong>Quelques organismes proposant un volontariat à l’étranger</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Délégation catholique pour la coopération (DCC)-&gt;<a href="http://www.ladcc.org/" class="spip_url spip_out auto" rel="nofollow external">http://www.ladcc.org/</a>] <br class="manualbr">106 rue du Bac - 75007 Paris<br class="manualbr">Téléphone : 01 45 65 96 65 - Télécopie : 01 45 81 30 81<br class="manualbr">Courriel : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/emploi/article/volontariat#dcc#mc#ladcc.org#" title="dcc..åt..ladcc.org" onclick="location.href=mc_lancerlien('dcc','ladcc.org'); return false;" class="spip_mail">dcc<span class="spancrypt"> [at] </span>ladcc.org</a>
</p>
<p>Association agréée pour l’envoi de volontaires de solidarité internationale (VSI) et reconnue d’utilité publique, la DCC a pour mission l’envoi de volontaires sur des projets de développement menés par les communautés catholiques du monde entier. Présente dans 70 pays, les missions de volontariat concernent tous les domaines de développement et tous les types de métier. La durée souhaitée du volontariat est comprise entre un et deux ans.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.defap.fr/" class="spip_out" rel="external">DEFAP - Service protestant de mission</a> <br class="manualbr">102 boulevard Arago - 75014 Paris <br class="manualbr">Téléphone : 01 42 34 55 55 - Télécopie : 01 56 24 15 30<br class="manualbr">Courriel : <a href="http://defap&lt;span%20class='spancrypt'&gt;%20%5Bat%5D%20&lt;/span&gt;protestants.org/" class="spip_out" rel="external">defap<span class="spancrypt"> [at] </span>protestants.org</a>
</p>
<p>Service protestant de mission, le DEFAP coordonne et gère l’information et l’animation dans les communautés paroissiales de France. Il assure l’accueil et le suivi des boursiers étrangers en France et procède à l’envoi de volontaires pour la solidarité internationale (VSI) pour une durée de un à six ans, essentiellement en Afrique centrale, Afrique de l’Ouest, Afrique australe, à Madagascar et dans le Pacifique. Les postes proposés concernent l’enseignement, la gestion de projets, la santé et l’action pastorale.</p>
<p><strong>Autres associations</strong></p>
<p>23 associations françaises sont actuellement agréées pour le volontariat de solidarité internationale, qui interviennent le plus souvent en étroite liaison avec leurs partenaires de la société civiles du pays d’accueil. Les volontaires de France volontaires (ex AFVP), peuvent être affectés, en outre, sur des projets soutenus par les postes et les collectivités territoriales, dans le cadre de la coopération décentralisée.</p>
<h3 class="spip"><a id="sommaire_2"></a>Service volontaire européen (SVE)</h3>
<p>Le service volontaire européen (SVE) fait partie du programme "Jeunesse en action" élaboré par la Commission européenne, le Parlement européen et les Etats membres de l’Union européenne.</p>
<p><a href="http://www.jeunesseenaction.fr/" class="spip_out" rel="external">L’Agence française</a>, située à Paris à l’INJEP, gère et développe en France, au nom de la Commission européenne, les différents volets du programme européen « Jeunesse en action ».</p>
<p>Le SVE peut être effectué dans un des pays suivants :</p>
<ul class="spip">
<li>Etats membres de l’Union européenne</li>
<li>Islande, Liechtenstein, Norvège et Suisse</li>
<li>Turquie (pays candidats à l’adhésion à l’UE)</li>
<li><strong>Pays « partenaires voisins »</strong></li>
<li>Europe du Sud-est : Albanie, Bosnie-Herzégovine, Macédoine (ARYM), Kosovo, Monténégro, Serbie</li>
<li>Europe de l’Est et du Caucase : Arménie, Azerbaïdjan, Belarus, Fédération de Russie, Géorgie, Moldavie, Ukraine</li>
<li>Pays partenaires méditerranéens : Algérie, Égypte, Israël / Territoires palestiniens, Jordanie, Liban, Libye, Maroc, Syrie, Tunisie</li>
<li><strong>Autres pays participants</strong> : <strong>« partenaires dans le reste du monde »</strong></li>
<li>Pays ayant signé avec la Communauté européenne des accords dans le domaine de la jeunesse. Pays ACP d’Afrique, Caraïbes, Pacifique, pays des Amériques, Asie.</li></ul>
<p><strong>Définition</strong></p>
<p>Le SVE a pour objectif de favoriser la participation des jeunes à diverses formes d’activités de volontariat, tant au sein qu’à l’extérieur de l’Union européenne, dans les domaines suivants : l’art et la culture, le social, l’environnement et protection du patrimoine, les médias et l’information des jeunes contre les exclusions, le racisme et la xénophobie, la santé, l’économie solidaire, le sport, la diversité culturelle, l’avenir de l’Europe… Le volontaire participe individuellement ou collectivement à une activité d’intérêt général, non rémunérée.</p>
<p>Le SVE n’est ni un stage, ni une formation, ni un emploi-jeune, ni un séjour linguistique ou touristique. Il ne permet pas de suivre en même temps des études universitaires ou d’occuper un emploi rémunéré.</p>
<p><strong>Conditions </strong></p>
<ul class="spip">
<li>Etre âgé de 18 à 30 ans (extension possible aux 16-17 ans pour les jeunes avec moins d’opportunités)</li>
<li>Etre légalement résident dans un des pays participant au programme ou dans l’un des pays partenaires voisins</li>
<li>Le SVE doit être effectué hors du pays de résidence</li>
<li>Aucune formation, expérience, diplôme ou connaissance linguistique ne sont requis</li>
<li>Le volontaire est tenu de participer à un cycle de formation SVE. Ce cycle comprend notamment une formation à l’arrivée dans le pays d’accueil, une réunion à mi-parcours et une évaluation finale.</li>
<li>Le volontaire souhaitant effectuer son SVE dans un pays tiers doit, le cas échéant, demander un visa d’entrée dans le pays d’accueil concerné. Les délais d’obtention d’un visa pouvant être longs, il convient d’en solliciter la délivrance suffisamment à l’avance.</li></ul>
<p><strong> Modalités</strong></p>
<p>La participation au SVE est gratuite pour le volontaire. Le SVE n’est pas rémunéré, mais le volontaire perçoit une indemnité mensuelle d’argent de poche dont le montant varie selon le pays où est effectuée l’activité SVE.</p>
<p>Il s’agit d’une activité à temps plein effectuée en une seule fois et dont la durée est comprise entre 2 et 12 mois. Le volontaire bénéficie pour sa protection sociale d’une assurance de groupe contractée par la Commission européenne.</p>
<p><strong>Déroulement du SVE</strong></p>
<p>Pour postuler à un SVE, vous devez prendre contact avec une organisation d’envoi dans votre pays de résidence qui vous aidera à choisir et organiser votre projet d’accueil à l’étranger. Cette organisation est chargée de la préparation du volontaire et de son assistance avant, pendant et après l’activité SVE. Entre cinq et six mois sont nécessaires entre le premier contact avec une organisation d’envoi et le départ effectif.</p>
<p>Dans le pays d’accueil, l’organisation d’accueil fournit au volontaire une assistance personnelle et linguistique appropriée et lui désigne un tuteur. C’est à ce dernier que le volontaire devra s’adresser pour tout problème</p>
<p>A l’issue du SVE, une réunion d’évaluation est organisée dans le pays d’envoi. Le volontaire reçoit un certificat "Youthpass" qui décrit et valide l’expérience d’apprentissage non formel.</p>
<p>Contacts</p>
<ul class="spip">
<li><a href="http://www.jeunesseenaction.fr/" class="spip_out" rel="external"><strong>Agence française du programme européen « Jeunesse en action » (AFPEJA)</strong></a><br class="manualbr">INJEP - 95 avenue de France 75013 Paris<br class="manualbr">Téléphone : 01 70 98 93 69 Courriel : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/emploi/article/volontariat#pej#mc#injep.fr#" title="pej..åt..injep.fr" onclick="location.href=mc_lancerlien('pej','injep.fr'); return false;" class="spip_mail">peja<span class="spancrypt"> [at] </span>injep.fr</a></li>
<li>La liste des organisations d’envoi SVE en France est disponible sur le <a href="http://www.jeunesseenaction.fr/" class="spip_out" rel="external">site</a> <br class="manualbr">(Sélectionnez votre région dans le champ « Près de chez moi » en haut à droite)</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Service civique </h3>
<p>Le <a href="http://www.service-civique.gouv.fr/" class="spip_out" rel="external">service civique</a> a été créé par la loi du 10 mars 2010. Il se caractérise par un engagement volontaire d’une durée de six à douze mois. Il offre à tous les jeunes l’opportunité de servir les valeurs de la République tout en s’engageant en faveur d’un projet d’intérêt général collectif auprès d’un organisme agréé, d’un organisme à but non lucratif, d’une ONG ou d’une personne morale de droit public <strong>en France comme à l’étranger. </strong></p>
<p><i>Mise à jour : 15 janvier 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/emploi/article/volontariat). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
