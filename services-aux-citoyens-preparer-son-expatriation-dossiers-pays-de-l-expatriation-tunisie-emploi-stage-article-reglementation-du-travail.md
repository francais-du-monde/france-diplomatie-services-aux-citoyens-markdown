# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/emploi-stage/article/reglementation-du-travail#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/emploi-stage/article/reglementation-du-travail#sommaire_2">Fêtes légales</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/emploi-stage/article/reglementation-du-travail#sommaire_3">Emploi du conjoint</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<h4 class="spip">Législation du travail</h4>
<p>Selon l’article 258-2 du code du travail : « Le recrutement d’étrangers ne peut être effectué lorsqu’il existe des compétences [tunisiennes] dans les spécialités concernées par le recrutement ».</p>
<p>Le ministère de la Formation professionnelle et de l’emploi exerce un contrôle strict à l’encontre du salarié comme de l’employeur qui sont tous deux soumis à des procédures administratives très précises avant l’embauche et pendant la durée du contrat sous peine de sanctions légales (amendes, emprisonnement ou mesure de refoulement du travailleur étranger).</p>
<p>L’employeur qui souhaite procéder au recrutement d’un étranger a l’obligation d’obtenir l’agrément du ministère de l’Emploi et de faire valider le contrat de travail limité à une année, renouvelable trois ans, et qui doit être visé à chaque renouvellement. Il est exceptionnel que l’employeur parvienne à établir que la spécificité professionnelle du travailleur étranger fait effectivement défaut sur le marché local de l’emploi. Il existe indéniablement de plus en plus de compétences au niveau local et elles se diversifient.</p>
<p>Ce principe de préférence nationale à l’emploi vaut également au sein des structures étrangères non résidentes (dites off-shore) qui ne disposent que d’un faible quota de recrutement d’étrangers au niveau local.</p>
<p>Le seul frein à l’application du principe est la possession d’une carte de séjour obtenue en raison d’un lien familial avec un ressortissant tunisien.</p>
<h4 class="spip">Le contrat de travail</h4>
<p>Le travailleur étranger est tenu d’être muni d’un contrat de travail préalablement visé par le ministère de la Formation professionnelle et de l’Emploi et d’une carte de séjour.</p>
<p><strong>Le contrat de travail est obligatoirement d’une durée d’un an renouvelable une seule fois</strong>. Cette règle connaît cependant une exception quand le contrat de travail a pour cadre un projet de développement d’intérêt national mené par l’employeur. Dans ce cas, le contrat peut être renouvelé plus d’une fois. L’application de cette disposition requiert cependant que le projet ait reçu l’agrément exprès des autorités tunisiennes.</p>
<p>La période d’essai est de six mois pour un agent d’exécution, de neuf mois pour un agent de maîtrise et de 12 mois pour un cadre. L’âge légal de départ à la retraite est fixé à 60 ans.</p>
<h4 class="spip">Durée légale du travail</h4>
<p>La durée légale du travail hebdomadaire en Tunisie peut être de 40 ou de 48 heures. En règle générale, elle débute le lundi pour s’achever le vendredi pour le régime des 40 heures ou le samedi pour le régime de 48 heures, le dimanche étant le jour de repos hebdomadaire. Certaines entreprises privées ne travaillent pas le vendredi après-midi mais le samedi matin.</p>
<p>Les administrations sont ouvertes du lundi au samedi midi, mais sont fermées le vendredi après-midi.</p>
<p>Pendant les mois de juillet et août, les administrations et beaucoup d’entreprises appliquent les horaires de la journée continue (« séance unique »), de 7h 30 à 13h 30.</p>
<p>Ce système est aussi appliqué durant le mois du Ramadan.</p>
<p>Le congé annuel varie de 12 à 30 jours par an.</p>
<h4 class="spip">Rémunération</h4>
<p>Le salaire minimum interprofessionnel garanti (SMIG) s’élève à : 320 TND (régime de 48 heures hebdomadaires).</p>
<p>Les heures supplémentaires sont majorées :</p>
<ul class="spip">
<li>25% pour le régime de travail inférieur à 48H/semaine pour les 8 premières heures et 50% au-delà de cette durée ;</li>
<li>25% pour le régime de travail des entreprises agricoles ;</li>
<li>50% pour le régime de travail à temps partiel ;</li>
<li>75% pour le régime de travail de 48H/semaine.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Fêtes légales</h3>
<p>(15 jours chômés dont 10 payés)</p>
<h4 class="spip">Dates fixes</h4>
<ul class="spip">
<li>1er janvier - Jour de l’an (non payé)</li>
<li>14 janvier - Fête de la Révolution et de la Jeunesse (1 jour payé)</li>
<li>20 mars - Fête de l’Indépendance (payé)</li>
<li>9 avril - Commémoration des Martyrs (non payé)</li>
<li>1er mai - Fête du Travail (payé)</li>
<li>25 juillet - Fête de la République (payé)</li>
<li>13 août - Fête de la Femme (non payé)</li>
<li>15 octobre Fête de l’Evacuation (payé)</li></ul>
<h4 class="spip">Selon calendrier lunaire</h4>
<ul class="spip">
<li>Jour de l’An Hegire (calendrier musulman) (non payé)</li>
<li>Le mouled (non payé)</li>
<li>Aid el Fitr (3 jours payés)</li>
<li>Aid el Idha (2 jours payés)</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Emploi du conjoint</h3>
<p>Le conjoint reçoit automatiquement un titre de séjour calqué sur celui de son conjoint qui ouvre la possibilité d’occuper un emploi salarié.</p>
<p>Les dispositions concernant le contrat de travail restent les mêmes que celles évoquées au paragraphe précédent.</p>
<p><strong>A noter toutefois que le concubinage n’est pas reconnu en Tunisie.</strong></p>
<p><i>Mise à jour : novembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/emploi-stage/article/reglementation-du-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
