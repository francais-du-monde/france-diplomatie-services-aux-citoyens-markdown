# Emploi, stage

<h2 class="rub22833">Marché du travail</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/emploi-stage/#sommaire_1">Secteurs à fort potentiel</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/emploi-stage/#sommaire_2">Secteurs non accessibles à un ressortissant étranger</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/emploi-stage/#sommaire_3">Rémunération</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Secteurs à fort potentiel</h3>
<p>Pour un ressortissant français, l’enseignement dans les établissements turcs francophones - sous condition et après agrément des autorités turques (Français langue étrangère - mathématiques notamment) - offre encore des possibilités (adresser sa candidature au bureau emploi du consulat à partir du mois de mars / avril de chaque année). Les secteurs du commerce international et du tourisme (sous réserve de parler couramment le turc) présentent également des opportunités d’emploi.</p>
<h3 class="spip"><a id="sommaire_2"></a>Secteurs non accessibles à un ressortissant étranger</h3>
<ul class="spip">
<li>Administration publique, armée et police ;</li>
<li>Secteur de la santé (médecin, infirmier, etc.) ;</li>
<li>Secteur juridique : les avocats ne peuvent pas se présenter au tribunal et doivent exercer leur métier comme conseiller juridique ;</li>
<li>Secteur comptable.</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Rémunération</h3>
<h4 class="spip"> Barèmes de rémunération </h4>
<p>Le salaire est défini par le code du travail comme le montant versé en liquide à une personne, par l’employeur ou une tierce personne, en rémunération du travail effectué. Il doit être versé une fois par mois en livre turque.</p>
<p>Le salaire minimum légal s’applique à tous les secteurs. La loi impose sa révision tous les deux ans, mais dans les faits, il est revu deux fois par an.</p>
<p><strong>Le salaire minimum brut mensuel</strong> est de 1021,50 TRL, soit environ 380 EUR jusqu’au 31 décembre 2013.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-turquie-emploi-stage-article-stages.md" title="Stages">Stages</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-turquie-emploi-stage-article-recherche-d-emploi.md" title="Recherche d’emploi">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-turquie-emploi-stage-article-reglementation-du-travail-110019.md" title="Réglementation du travail">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-turquie-emploi-stage-article-marche-du-travail-110018.md" title="Marché du travail">Marché du travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/emploi-stage/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
