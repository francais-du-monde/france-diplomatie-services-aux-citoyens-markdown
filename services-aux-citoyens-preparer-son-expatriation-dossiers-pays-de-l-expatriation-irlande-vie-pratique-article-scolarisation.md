# Scolarisation

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/vie-pratique/article/scolarisation#sommaire_1">Les établissements scolaires français en Irlande</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/vie-pratique/article/scolarisation#sommaire_2">Enseignement supérieur</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Les établissements scolaires français en Irlande</h3>
<p>Fondé en 1970, le <a href="http://www.lfi.ie/" class="spip_out" rel="external">Lycée français d’Irlande</a> offre une éducation complète aux enfants de la maternelle au primaire et au secondaire. L’enseignement en langue française est entièrement basé sur le programme officiel français.</p>
<p>La mission initiale de l’établissement est de fournir une continuité dans l’éducation aux expatriés à l’étranger. Cependant, afin de mieux répondre aux défis de notre temps, la politique du gouvernement français a évolué ouvrant les écoles au pays d’accueil, offrant ainsi l’enseignement en plusieurs langues.</p>
<p>Pour toute information sur la scolarisation dans le système français à l’étranger, vous pouvez consulter nos rubriques thématiques sur les <a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures.md" class="spip_in">Etudes supérieures</a> et la <a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais.md" class="spip_in">Scolarité en français</a>.</p>
<p>Vous y trouverez notamment des renseignements sur :</p>
<ul class="spip">
<li>Les établissements français du primaire et du secondaire à l’étranger ;</li>
<li>Les bourses scolaires et la prise en charge des frais de scolarité à l’étranger ;</li>
<li>Les possibilités qui s’offrent à vous si votre enfant ne peut être scolarisé à l’étranger dans le système français (enseignement à distance par le CNED, programme français langue maternelle (FLAM), internats en France) ;</li>
<li>Les épreuves du baccalauréat à l’étranger ;</li>
<li>Les bourses d’études supérieures en France et à l’étranger ;</li>
<li>L’équivalence des diplômes.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Enseignement supérieur</h3>
<h4 class="spip">Au niveau européen</h4>
<p>Le <strong>processus de Bologne</strong> est un processus de réformes visant à créer un Espace européen de l’enseignement supérieur. Même si une mise en commun des systèmes d’enseignement supérieur de tous les Etats membres n’est pas à l’ordre du jour, la mobilité des personnes souhaitant passer d’un système éducatif à un autre, d’un pays à un autre, ainsi que l’employabilité seront renforcées.</p>
<p>L’objectif de ce processus vise à respecter un équilibre entre la diversité et l’homogénéité, caractéristiques très appréciées de l’Europe.</p>
<h4 class="spip">Au niveau national</h4>
<p>La loi sur les universités et la loi sur les qualifications (enseignement et formation), respectivement adoptées en 1997 et 1999, régissent la plupart des aménagements liés au processus de Bologne dans les établissements d’enseignement supérieur irlandais.</p>
<p>La politique éducative irlandaise est conduite par le <i>Department of Education and Science</i>. La HEA (<i>Higher Education Authority</i>), qui lui est rattachée assure le suivi des établissements d’enseignement supérieur.</p>
<p>L’enseignement supérieur irlandais compte sept universités, des <i>College</i> spécialisés (médecine, éducation, art) qui offrent une formation jusqu’au doctorat, 15 instituts de technologie et plusieurs dizaines d’autres établissements.</p>
<h4 class="spip">Procédures d’admission</h4>
<p>L’accès à l’enseignement supérieur et le choix des cursus sont limités par le nombre de places disponibles et par les résultats des candidats au <i>Leaving Certificate</i> (diplôme de fin d’études secondaires irlandais). Pour les titulaires du baccalauréat, certains établissements ont établi une équivalence entre les notes françaises et les notes du <i>Leaving Cert</i> qui permettent de comparer les niveaux et de déterminer si l’on est au-dessus du seuil d’admission ou non.</p>
<h4 class="spip">Droits d’inscription et aides</h4>
<p>Pour les études de premier cycle (<i>undergraduate</i>), les frais de scolarité ont été supprimés pour les étudiants des pays membres de l’Union européenne. Seul demeure à la charge des étudiants le paiement des frais administratifs (environ 800 euros par an). Les autres étudiants paient le coût réel des études qui varient selon le cursus et l’institution (jusqu’à 25 000 euros par an pour des études de médecine).</p>
<p>Au-delà du premier cycle (post-graduate), tous les étudiants (irlandais, membres de l’Union européenne et autres) paient le coût réel des études, qui peut être très élevé. Le ministère irlandais de l’Education et de la Science participe à divers programmes d’aide pour les étudiants du supérieur, qui sont attribuées sous certaines conditions.</p>
<h4 class="spip">Structuration des études</h4>
<p>Tous les étudiants de l’enseignement supérieur sont inscrits dans des programmes conformes à une structuration des études en trois cycles :</p>
<ul class="spip">
<li>en règle générale, la durée des programmes de <i>Bachelor</i> est de trois ou quatre mais elle peut être plus longue dans certaines disciplines spécifiques, telles que la médecine ou l’architecture ;</li>
<li>les programmes conduisant au <i>Master</i> durent de 1 à 3 ans ;</li>
<li>enfin, la durée minimale des programmes doctoraux est de trois ans à taux plein. Pour y accéder, les candidats doivent être titulaires d’un diplôme de Master mais certaines dérogations sont possibles (<i>Bachelor with Honours</i> de première classe). Le diplôme du doctorat est normalement délivré à l’issue d’une recherche supervisée débouchant sur la rédaction d’une thèse.</li></ul>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://europa.eu/youreurope/citizens/education/university/index_fr.htm" class="spip_out" rel="external">Education et jeunesse</a> sur le site de la Commission européenne ;</li>
<li><a href="http://www.ambafrance-ie.org/-Etudier-en-Irlande-" class="spip_out" rel="external">Etudier en Irlande</a> sur le site de l’Ambassade de France en Irlande ;</li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/vie-pratique/article/scolarisation). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
