# Scolarisation

<p>Pour toute information sur la scolarisation dans le système français à l’étranger, vous pouvez consulter nos thématiques sur les <a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures.md" class="spip_in">Etudes supérieures</a> et la <a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais.md" class="spip_in">Scolarité en français</a>.</p>
<p>Vous y trouverez des renseignements sur :</p>
<ul class="spip">
<li>les établissements français du primaire et du secondaire à l’étranger ;</li>
<li>les bourses scolaires des frais de scolarité à l’étranger ;</li>
<li>les possibilités qui s’offrent à vous si votre enfant ne peut être scolarisé à l’étranger dans le système français (enseignement à distance par le CNED, programme français langue maternelle (FLAM), internats en France) ;</li>
<li>les épreuves du baccalauréat à l’étranger ;</li>
<li>les bourses d’études supérieures en France et à l’étranger ;</li>
<li>l’équivalence des diplômes.</li></ul>
<p>Pour en savoir plus :</p>
<p><a href="http://www.aefe.fr/" class="spip_out" rel="external">Agence pour l’enseignement français à l’étranger</a>.</p>
<h4 class="spip">Les établissements scolaires français en Lituanie</h4>
<p><a href="http://www.efv.lt/" class="spip_out" rel="external">Ecole Française de Vilnius</a><br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/vie-pratique/article/scolarisation#info#mc#efv.lt#" title="info..åt..efv.lt" onclick="location.href=mc_lancerlien('info','efv.lt'); return false;" class="spip_mail">Courriel</a></p>
<h4 class="spip">Enseignement supérieur</h4>
<p>En Lituanie, on peut distinguer deux types d’établissements d’enseignement supérieur : publics ou privés.</p>
<p><strong>Les universités</strong>. On dénombre 15 universités publiques, dont 10 dispensant un enseignement général et technique, et cinq académies dans lesquelles une spécialité est enseignée (médecine, musique par exemple). Les universités privées sont au nombre de sept.</p>
<p>Les <i>kolegija</i> ou écoles supérieures non-universitaires sont techniques et professionnalisant et équivalent aux IUT français. Sur les 28 <i>kolegija</i>, 12 sont des établissements privés.</p>
<p>Pour entrer à l’université et préparer la licence <i>bakalauras</i>, il faut posséder le certificat de fin d’études secondaires <i>brandos atestatas</i>, équivalent du baccalauréat français. Ce sont les établissements d’enseignement supérieur eux-mêmes qui, individuellement, organisent les admissions et en déterminent le calendrier. L’année académique est divisée en deux semestres (automne et printemps).</p>
<p>Le diplôme est conditionné par l’obtention de crédits (European Credits Transfer System - ECTS). Les notes aux examens sont attribuées sur une échelle de 5 à 10. Certains cours ne donnent pas lieu à un examen final et l’attribution de crédits est alors liée à la signature du professeur dans le livret de l’étudiant.</p>
<p>Les cours sont généralement dispensés en lituanien, mais la plupart des établissements d’enseignement supérieur proposent quelques programmes d’études en langue étrangère, avant tout en anglais, mais aussi en allemand, en français ou en russe.</p>
<p>La structure des études, basée sur <strong>trois cycles principaux</strong>, est en vigueur depuis 1993 :</p>
<ul class="spip">
<li>le premier cycle dure de trois à quatre ans et conduit au niveau Licence <i>bakalauras</i> ou à une qualification professionnelle <i>profesine kvalifikacija</i> ;</li>
<li>le deuxième cycle dure d’un à deux ans et conduit à un niveau de Master <i>magistras</i> ou à une qualification professionnelle <i>profesine kvalifikacija</i> ;</li>
<li>le troisième cycle, les études doctorales, ne dure pas plus de trois à quatre ans. Il comprend des cours théoriques, des activités spécifiques de recherche et la rédaction d’une thèse. Après avoir suivi les cours de formation doctorale, la thèse doit être préparée et soutenue publiquement pour que le candidat puisse prétendre au titre de docteur. Les études doctorales doivent être organisées conjointement par des établissements d’enseignement supérieur et de recherches.</li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/vie-pratique/article/scolarisation). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
