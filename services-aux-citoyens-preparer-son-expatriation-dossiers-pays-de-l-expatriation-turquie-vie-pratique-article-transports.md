# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/vie-pratique/article/transports#sommaire_1">Importation de véhicule</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/vie-pratique/article/transports#sommaire_2">Permis de conduire</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/vie-pratique/article/transports#sommaire_3">Code de la route</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/vie-pratique/article/transports#sommaire_4">Assurances et taxes</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/vie-pratique/article/transports#sommaire_5">Achat et location</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/vie-pratique/article/transports#sommaire_6">Immatriculation</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/vie-pratique/article/transports#sommaire_7">Entretien</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/vie-pratique/article/transports#sommaire_8">Carburant</a></li>
<li><a id="so_9" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/vie-pratique/article/transports#sommaire_9">Réseau routier</a></li>
<li><a id="so_10" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/vie-pratique/article/transports#sommaire_10">Transports en commun</a></li>
<li><a id="so_11" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/vie-pratique/article/transports#sommaire_11">En cas d’accident </a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Importation de véhicule</h3>
<p>L’importation d’un véhicule personnel en franchise douanière est soumise à des règles très strictes. On ne peut importer qu’un seul véhicule. Seules les personnes autorisées à travailler en Turquie peuvent importer un véhicule. Les documents originaux du véhicule doivent être au nom du propriétaire importateur. Les formalités doivent être effectuées dans un délai de six mois à compter de l’importation. Le véhicule devra être réexporté. Il ne peut être revendu sur place sauf à un ressortissant étranger. Les démarches liées à l’immatriculation sont annuelles.</p>
<p>De façon générale, en raison de formalités longues et complexes, et de l’obligation d’acquitter des droits et des taxes élevés, il n’est pas conseillé d’importer un véhicule.</p>
<h3 class="spip"><a id="sommaire_2"></a>Permis de conduire</h3>
<p>Le permis de conduire français est reconnu. Vous pouvez, toutefois, obtenir un permis de conduire turc, par échange de votre document français. Cette formalité devient obligatoire si vous fixez définitivement votre résidence en Turquie.</p>
<p>En cas de conduite en état d’ébriété, le permis de conduire peut être immédiatement retiré.</p>
<h3 class="spip"><a id="sommaire_3"></a>Code de la route</h3>
<p>La conduite est à droite, la priorité également, mais le code de la route est peu respecté, et la conduite souvent très fantaisiste.</p>
<p>Il est recommandé de faire preuve de prudence et ne pas circuler la nuit.</p>
<p>La vitesse est limitée à 50 km/h en agglomération, 90 km/h sur route et 120 km/h sur autoroute.</p>
<h3 class="spip"><a id="sommaire_4"></a>Assurances et taxes</h3>
<p>L’assurance locale de circulation (au tiers), dite "Trafic", est obligatoire. Il est recommandé de la coupler avec une assurance tous risques, ainsi qu’avec une assurance complémentaire au tiers en vue d’augmenter le plafond de la prime d’assurance relative aux dégâts matériels causés à autrui.</p>
<p>Le prix d’une assurance tous risques est calculé en fonction de la valeur estimée du véhicule. Les sociétés d’assurance acceptent généralement de prendre en compte les bonus des assurances contractées dans d’autres pays. Il faut négocier avec les sociétés d’assurance pour obtenir des tarifs "comparables" avec ceux pratiqués en France (environ 25% plus cher). Il n’existe pas, ou très peu, de franchise.</p>
<h3 class="spip"><a id="sommaire_5"></a>Achat et location</h3>
<p>Citroën, Renault et Peugeot sont représentées, ainsi que différentes marques étrangères (Fiat, BMW, Mercedes, Toyota, Ford, Nissan…). Il n’y a pas de normes locales. Il est conseillé d’opter pour une voiture robuste et simple d’entretien, bonne routière en raison des distances entre les villes, avec la climatisation.</p>
<p>Il est possible d’acquérir sur place une voiture d’occasion, en particulier auprès d’expatriés quittant la Turquie, à un prix voisin de l’Argus ou plus élevé.</p>
<p>Les sociétés de location internationales (Avis, Europcar, Hertz, etc.) ou locales sont nombreuses.</p>
<h3 class="spip"><a id="sommaire_6"></a>Immatriculation</h3>
<p>Un contrôle technique est obligatoire tous les deux ans.</p>
<h3 class="spip"><a id="sommaire_7"></a>Entretien</h3>
<p>L’entretien des véhicules français ne pose pas de problème. Le service est d’environ 20% plus cher pour les modèles importés. La qualité du service est néanmoins inégale. Il est possible de se procurer des pièces détachées de marques françaises, avec des délais assez longs.</p>
<h3 class="spip"><a id="sommaire_8"></a>Carburant</h3>
<p>L’approvisionnement en carburant ne pose pas de problème particulier. Le super sans plomb peut toutefois être difficile à trouver dans certaines régions. Le prix des carburants est soumis à des révisions très fréquentes.</p>
<h3 class="spip"><a id="sommaire_9"></a>Réseau routier</h3>
<p>Le réseau routier est de bonne qualité. Il existe une autoroute Ankara/Istanbul (avec de nombreux tronçons de routes à quatre voies). De nombreuses voies expresses existent ou sont en construction. Les routes sont généralement bonnes sur les grands axes, mais avec parfois quelques surprises, surtout dans l’Est du pays.</p>
<h3 class="spip"><a id="sommaire_10"></a>Transports en commun</h3>
<p>A Ankara, il existe un petit réseau de métro. Les autobus sont très nombreux. Les taxis collectifs sont peu utilisés par les expatriés. Les taxis individuels sont nombreux et peu chers (Prise en charge : 2,20 TRY + 1,90 TRY par km).</p>
<p>A Istanbul les taxis, collectifs et individuels, sont les moyens de transport les plus utilisés. La circulation y est particulièrement dense. Le prix moyen pour des trajets courts en taxi est de 2,5 TRY (prise en charge) + 1,6 TRY/km. Toutefois, les chauffeurs de taxis ne connaissent pas toujours bien la ville, il faut donc donner des instructions en permanence sur la direction à prendre si on ne veut pas faire le tour de la ville.</p>
<p>Les "Dolmuchs", sorte de taxis collectifs fonctionnant sur des grands axes, sont très abordables, de même que les autobus, mais là aussi il est préférable de bien connaître sa destination.</p>
<p>Pour se déplacer à l’intérieur du pays, différentes solutions sont envisageables :</p>
<ul class="spip">
<li>les liaisons en autocar, qui sont très bon marché ;</li>
<li>le réseau aérien intérieur est dense, assuré par la compagnie aérienne Turkish Airlines (THY) avec des horaires quelquefois aléatoires ;</li>
<li>le réseau ferré relie les villes principales. Les trains sont très lents mais confortables et bon marché.</li></ul>
<p>Toutes les liaisons intérieures sont payables en monnaie locale.</p>
<h3 class="spip"><a id="sommaire_11"></a>En cas d’accident </h3>
<p>En Turquie, tout accident entraînant mort d’homme est automatiquement suivi par l’incarcération immédiate des chauffeurs des véhicules impliqués dans l’accident en attendant que la justice statue.</p>
<p>En cas d’accident, se méfier des intermédiaires qui peuvent proposer leurs services.</p>
<p>Les postes de secours routiers sont joignables au numéro de téléphone : 212 282 81 40. En cas d’accident, avec ou sans blessé, ne jamais déplacer le véhicule avant que la police n’ait procédé au constat. En exiger la copie.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/vie-pratique/article/transports). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
