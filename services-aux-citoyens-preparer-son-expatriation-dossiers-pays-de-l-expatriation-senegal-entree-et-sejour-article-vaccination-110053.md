# Vaccination

<p>Le vaccin contre la fièvre jaune n’est plus obligatoire (sauf pour les personnes en provenance d’une zone infectée), mais il est néanmoins conseillé car des cas sont encore décrits dans certains villages de brousse.</p>
<p>Vaccinations conseillées pour des raisons médicales :</p>
<ul class="spip">
<li>Adultes : mise à jour des vaccinations contre la diphtérie, le tétanos et la poliomyélite ; vaccinations contre la typhoïde, l’hépatite A, l’hépatite B, la méningite à méningocoque A et C.</li>
<li>Enfants : vaccinations recommandées en France par le ministère de la Santé et en particulier BCG dès le premier mois, méningite à méningocoque A et C dès l’âge de trois mois, rougeole dès l’âge de neuf mois.</li></ul>
<p>Le vaccin contre la rage est recommandé pour un séjour de longue durée.</p>
<p>Afin d’éviter d’éventuelles difficultés d’approvisionnement une fois sur place, il est recommandé de mettre à jour ses vaccinations avant le départ.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="services-aux-citoyens-preparer-son-expatriation-sante.md" class="spip_in">notre article thématique sur la vaccination</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/entree-et-sejour/article/vaccination-110053). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
