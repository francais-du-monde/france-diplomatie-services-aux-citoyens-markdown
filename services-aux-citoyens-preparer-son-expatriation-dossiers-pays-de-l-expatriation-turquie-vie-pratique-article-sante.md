# Santé

<p>A Istanbul, les conditions générales hygiène et l’état sanitaire sont convenables. Toutefois l’air pollué peut incommoder certaines personnes.</p>
<p>La qualité des soins dispensés en Turquie est bonne, la plupart des spécialités médicales sont disponibles. Toutefois il existe une "médecine à deux vitesses" : les hôpitaux publics pris en charge par la sécurité sociale locale sont réputés de qualité médiocre. Aussi est-il recommandé de bénéficier d’une couverture sociale française ou d’une assurance sociale privée complémentaire afin d’avoir accès à la médecine privée. Le tarif d’une consultation varie fortement d’un médecin à l’autre. Les prix pour les étrangers sont en général plus élevés que ceux pratiqués pour les Turcs. Une consultation chez un spécialiste dépasse les 100 €.</p>
<p>A Ankara, les conditions d’hygiène sont bonnes malgré la pollution. La médecine publique est de qualité inégale. La médecine privée est chère mais de très bonne qualité. Le coût des soins est élevé dans les établissements privés. Les honoraires d’un médecin généraliste sont d’environ 80 € mais peuvent dépasser les 100 € pour les spécialistes.</p>
<p><strong>Avant le départ :</strong><br class="manualbr">Consultez votre médecin (éventuellement votre dentiste) et souscrivez à une compagnie d’assistance couvrant les frais médicaux le rapatriement sanitaire.</p>
<p><strong>Prévention des maladies transmises par les piqûres d’insectes :</strong><br class="manualbr">Certains virus ou parasites peuvent être transmis par les piqûres de moustiques ou de tiques. Le port de vêtements longs et l’utilisation de répulsifs (sprays ou crème) diminuent significativement ce risque.</p>
<p><strong>Paludisme :</strong><br class="manualbr">Le pays connaît, dans le sud-est, des cas sporadiques de transmission de paludisme : il est possible de ne pas prendre de traitement préventif. Durant votre séjour, et durant le mois qui suit votre retour, un avis médical doit être pris rapidement, en cas de fièvre.</p>
<p><strong>Fièvre Crimée-Congo :</strong><br class="manualbr">Maladie virale transmise par les piqûres de tiques pouvant présenter quelques formes sévères. Cette affection touche principalement les départements d’Anatolie centrale (en particulier, ceux de Sivas, Tokat et Corum) et la côte de la mer Noire. Les forêts des alentours d’Istanbul et d’Ankara sont également des zones à risque. Il convient de prendre des mesures de protection individuelle :</p>
<ul class="spip">
<li>utiliser des produits répulsifs (à pulvériser sur la peau et/ou les vêtements) ;</li>
<li>éviter les zones où les tiques sont abondantes et les périodes où elles sont actives (du printemps à l’automne) ;</li>
<li>les rechercher régulièrement dans les vêtements ou sur la peau puis les enlever.</li></ul>
<p><strong>Vaccinations</strong> : <br class="manualbr">La mise à jour de la vaccination diphtérie-tétanos-poliomyélite est nécessaire. Autres vaccinations pouvant être conseillées (selon conditions d’hygiène et durée du séjour) : fièvre typhoïde, hépatites virales A et B. Demandez conseil à votre médecin.</p>
<p><strong>Hygiène alimentaire</strong> : <br class="manualbr">Il est préférable de consommer de l’eau en bouteilles capsulées. <br class="manualbr">Evitez l’ingestion de légumes crus et de fruits non pelés. <br class="manualbr">Evitez la consommation d’aliments (poisson, viande, volaille, lait) insuffisamment cuits. <br class="manualbr">Veillez à un lavage régulier et soigneux des mains avant chaque repas.</p>
<p><strong>Grippe aviaire</strong> <br class="manualbr">La grippe aviaire qui avait coûté la vie à quatre personnes en 2006 en Turquie, est réapparue début 2007 dans les départements de Batman et Diyarbakir, dans le sud-est du pays. Plus récemment, le ministère de l’agriculture a annoncé officiellement le 21 janvier 2008 l’existence d’un nouveau cas de grippe aviaire détecté dans le village de Caycuma (Zonguldak), province riveraine de la mer noire située au Nord-Ouest du pays. Des mesures de sécurité sanitaire ont été prises par les autorités locales (quarantaine, abattage des volailles, etc.). A ce stade, dans les endroits habituellement fréquentés par les ressortissants français en Turquie (Istanbul, côte égéenne et côte méditerranéenne, Cappadoce), la grippe aviaire n’apparaît pas comme une menace.</p>
<p>La direction générale de la Santé recommande aux voyageurs, lors de leur séjour, d’éviter tout contact avec les volailles et les oiseaux, c’est-à-dire de ne pas se rendre dans des élevages ni sur les marchés aux volatiles. Les recommandations générales d’hygiène lors des voyages dans les pays en développement, qui visent à se protéger des infections microbiennes, sont préconisées.</p>
<p><strong>Quelques règles simples :</strong></p>
<ul class="spip">
<li>ne caressez pas les animaux que vous rencontrez ;</li>
<li>veillez à votre sécurité routière (port de la ceinture de sécurité en automobile ou du casque en moto) ;</li>
<li>emportez dans vos bagages les médicaments dont vous pourriez avoir besoin.</li></ul>
<p><strong>Numéros utiles</strong> :</p>
<ul class="spip">
<li>Hôpital international. Tél. : 212 663 30 00 ;</li>
<li>Hôpital allemand. Tél. : 212 293 21 50 ;<br class="manualbr">Hôpital américain. Tél. : 212 231 40 50.</li></ul>
<p>Pour de plus amples renseignements, vous pouvez consulter :</p>
<ul class="spip">
<li>le site de l’<a href="http://www.invs.sante.fr/" class="spip_out" rel="external">INVS</a></li>
<li>le site de l’<a href="http://www.who.int/fr/index.html" class="spip_out" rel="external">OMS</a></li>
<li>le site de <a href="http://www.pasteur.fr/" class="spip_out" rel="external">l’institut Pasteur de Paris</a>.</li>
<li>le <a href="http://www.grippeaviaire.gouv.fr/" class="spip_out" rel="external">site interministériel relatif à la grippe aviaire</a></li></ul>
<p>Le <a href="http://www.cimed.org" class="spip_out" rel="external">site du CIMED</a> dispose également d’une fiche détaillée sur la santé en Turquie.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/vie-pratique/article/sante). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
