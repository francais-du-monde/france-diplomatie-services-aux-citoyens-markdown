# Lettre de motivation

<h4 class="spip">Rédaction</h4>
<p>La lettre de motivation doit :</p>
<ul class="spip">
<li>Etre personnelle, directe et concise ;</li>
<li>Etre manuscrite, sans rature, ni tache</li>
<li>Montrer en quelques lignes que le candidat connait l’entreprise</li>
<li>Mettre en avant les compétences pour le poste visé</li>
<li>Indiquer clairement les motivations personnelles</li>
<li>Se terminer par une proposition de rencontre</li></ul>
<h4 class="spip">Modèles de lettre de motivation</h4>
<p>Exemples de lettres de motivation sur les sites :</p>
<ul class="spip">
<li><a href="http://www.lettre-de-motivation.modele-cv-lettre.com/" class="spip_out" rel="external">www.lettre-de-motivation.modele-cv-lettre.com</a></li>
<li><a href="http://www.cadremploi.fr/" class="spip_out" rel="external">www.cadremploi.fr</a></li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/emploi-stage/article/lettre-de-motivation). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
