# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/vie-pratique/article/transports#sommaire_1">Importation de véhicule</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/vie-pratique/article/transports#sommaire_2">Permis de conduire</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/vie-pratique/article/transports#sommaire_3">Code de la route</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/vie-pratique/article/transports#sommaire_4">Assurances et taxes</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/vie-pratique/article/transports#sommaire_5">Achat et location</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/vie-pratique/article/transports#sommaire_6">Entretien</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/vie-pratique/article/transports#sommaire_7">Réseau routier</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/vie-pratique/article/transports#sommaire_8">Transports en commun</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Importation de véhicule</h3>
<p>S’il n’existe pas de normes locales relatives à l’entrée et à la revente d’un véhicule, l’Etat opère un contrôle régulier.</p>
<p><strong>Importation de véhicules d’occasion</strong></p>
<p>L’importation de voitures particulières d’occasion des catégories B, C, D et E (sauf par les diplomates) est soumise au Contrôle d’identification de véhicules.</p>
<p>Importés d’occasion ou CIVIO (Note 306-MEFB/Sg/DGD du 21/06/2006).</p>
<p><strong>Procédure à suivre :</strong></p>
<p>Le dépôt d’une demande de vérification doit être effectué auprès du bureau de la société GasyNet munie des pièces suivantes :</p>
<ul class="spip">
<li>Fiche de renseignement à l’importation (2 exemplaires) ;</li>
<li>Carte grise originale ;</li>
<li>Connaissement ou LTA ;</li>
<li>Copie de l’attestation de déménagement ou de l’autorisation de franchise ;</li>
<li>Carte d’identité nationale, passeport, carte de résident ou NIF et carte statistique.</li></ul>
<p>Le retrait du rapport d’inspection de recevabilité se fait dans les trois jours après identification physique du véhicule.</p>
<p><strong>Précisions complémentaires concernant les motos et engins</strong></p>
<p>Seules les voitures particulières des types B, C, D et E sont soumises au contrôle CIVIO. Par conséquent, les motocyclettes et engins divers en sont exclus. Après validation du BSC (bordereau de suivi des cargaisons), le dédouanement de ces marchandises est soumis au préalable à une demande de valeur à déposer auprès de la division valeur et sélectivité (immeuble de la société GasyNet, enceinte Galaxy Andraharo, Antananarivo) en ce qui concerne les « engins d’occasion » et les « motocyclettes » importées <strong>en quantité commerciale</strong>.</p>
<p>Les pièces à produire auprès de la division valeur et sélectivité aux fins de détermination de la valeur des engins d’occasion ou motos sont les suivantes :</p>
<ul class="spip">
<li>Carte grise originale ;</li>
<li>Facture originale ;</li>
<li>Notice technique ou attestation du constructeur mettant en évidence l’année de première mise en service de l’engin ;</li>
<li>Connaissement ;</li>
<li>BSC (bordereau de suivi des cargaisons).</li></ul>
<p>Après notification de la valeur par la <strong>division valeur et sélectivité</strong> à l’importateur et au bureau de dédouanement, la déclaration en douane doit être établie par un transitaire agréé en douane.</p>
<p>Source : <a href="http://www.douanes.gov.mg/" class="spip_out" rel="external">site des douanes malgaches</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Permis de conduire</h3>
<p>Le permis de conduire français ou international est reconnu. Il existe une pratique d’échange réciproque des permis entre la France et Madagascar : si votre séjour à Madagascar est supérieur à un an, vous devez solliciter l’échange de votre permis français contre un permis malgache auprès du ministère de l’Intérieur.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  site du <a href="http://www.ambafrance-mada.org/Permis-de-conduire" class="spip_out" rel="external">consulat de France</a>.</p>
<p>Pour des raisons pratiques, notamment pour la garde du véhicule, il peut être utile de recourir aux services d’un chauffeur.</p>
<h3 class="spip"><a id="sommaire_3"></a>Code de la route</h3>
<p>La conduite s’effectue à droite. La priorité est en principe à droite, mais <i>de facto</i> elle est coutumière. Les règles du Code de la route sont identiques à celles de la France.</p>
<p>En ville, la vitesse est limitée à 60 km/h, quand l’état des rues le permet.</p>
<p>Le port de la ceinture de sécurité dans les véhicules et celui du casque pour les motocyclistes sont obligatoires. La traumatologie routière à Madagascar est à l’origine du taux de mortalité le plus élevé.</p>
<h3 class="spip"><a id="sommaire_4"></a>Assurances et taxes</h3>
<p>L’assurance responsabilité civile est obligatoire. Il est cependant possible de souscrire une assurance tous risques, à des tarifs très élevés. En revanche, il n’est pas possible de contracter auprès d’une compagnie étrangère.</p>
<p>A titre d’exemple, montant de la prime annuelle pour un véhicule 4x4 de type Toyota :</p>
<ul class="spip">
<li>assurance au tiers : 750 € (incluant responsabilité civile, incendie, vol et bris de glace)</li>
<li>(l’assurance au tiers non illimitée n’existe pas à Madagascar).</li>
<li>assurance tous risques : 2550 € (réduction de 50% avec une attestation bonus de l’assureur précédent).</li></ul>
<h3 class="spip"><a id="sommaire_5"></a>Achat et location</h3>
<p>De nombreuses marques sont représentées sur place : Peugeot, Renault, Toyota, Mitsubishi, Hyundai, Nissan, Audi, Mercedes, Land Rover, Mazda, Dacia.</p>
<p>Une berline ordinaire est suffisante pour la capitale et les principales villes ; sur le reste du réseau routier, un véhicule 4x4 à essieux élevés est conseillé en raison du mauvais état des routes. Il est recommandé d’équiper les véhicules d’un système de climatisation.</p>
<p>A titre d’exemple, le coût de location d’une Renault Clio avec chauffeur (assurance incluse et kilométrage illimité, prix du carburant non compris) est estimé à 1300€ par mois ; le coût de location pour un véhicule 4x4 Nissan avec chauffeur (hors carburant) est compris entre 80 et 100 € par jour.</p>
<h3 class="spip"><a id="sommaire_6"></a>Entretien</h3>
<p>Le coût d’entretien d’un véhicule est inférieur aux prix français en ce qui concerne la main d’œuvre avec une qualité de service moyenne ; le prix des pièces de rechange est en revanche très élevé. Celles-ci ne sont pas disponibles immédiatement (délai de trois semaines pour les voitures françaises, 45 jours pour les véhicules étrangers).</p>
<h3 class="spip"><a id="sommaire_7"></a>Réseau routier</h3>
<p>Hormis les grands axes (Tananarive / Tamatave ; Tananarive / Majunga / Diego Suarez ; Tananarive / Fianarantsoa / Tuléar), l’état des routes est très aléatoire. Sur les pistes, un véhicule peut être gravement endommagé par les ornières. La plus grande vigilance est recommandée en temps de pluie et la nuit. Le comportement des taxis doit faire l’objet d’une attention particulière. Respecter une vitesse raisonnable permet de ne pas se laisser surprendre par des obstacles non signalés (camion en panne, troupeau, véhicule lent…) et par l’éclatement d’un pneu (risque élevé).</p>
<p>Dans la province Nord Est, le réseau routier est dégradé et parfois impraticable pendant la saison des pluies (décembre/avril), ce qui peut entraîner des interruptions ponctuelles d’approvisionnement en carburant.</p>
<p><strong>En cas d’accident grave, se rendre au plus vite au poste de police ou de gendarmerie le plus proche.</strong> En cas d’arrestation, ne protestez pas et demandez à joindre votre consulat le plus rapidement possible (arguer de votre droit si cela vous était interdit).</p>
<p>Il est fortement déconseillé de se déplacer de nuit.</p>
<h3 class="spip"><a id="sommaire_8"></a>Transports en commun</h3>
<p>Les services de transport public (y compris les taxis brousse) ne sont pas fiables, les véhicules étant souvent mal entretenus.</p>
<p>Le réseau ferroviaire ne circule plus. Seule une liaison Fianarantsoa – Manakara fonctionne mais de façon irrégulière.</p>
<p>Les bacs et les embarcations fluviales et maritimes utilisés à Madagascar sont le plus souvent vétustes et dénués de tout équipement de sécurité. La navigation de plaisance est fortement déconseillée la nuit, en l’absence de communications radio.</p>
<p>Le transport aérien s’effectue dans des conditions assez satisfaisantes. Les vols intérieurs sont soumis à des changements d’horaires de plus en plus fréquents ou à des annulations sans préavis. Outre la re-confirmation des billets, il est impératif de se renseigner, la veille et le jour-même, auprès de la compagnie ou de l’aéroport sur les horaires prévus.</p>
<p>Des liaisons hebdomadaires avec Paris sont assurées par Air France (trois), Corsair (une) et Air Madagascar (trois). La compagnie nationale malgache dessert aussi la plupart des villes de province, ainsi que Maurice et La Réunion, ces deux destinations étant également desservies par Air Mauritius et Air Austral.</p>
<p>Des vols dans les bagages en soute sont régulièrement signalés à l’aéroport international d’Ivato (Tananarive), y compris dans les valises fermées à clé ou cadenassées, à l’arrivée comme au départ. Il convient en conséquence de conserver avec soi tout objet de valeur.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  dossier <a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/madagascar/" class="spip_in">Madagascar</a> dans la rubrique Conseils aux voyageurs.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/vie-pratique/article/transports). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
