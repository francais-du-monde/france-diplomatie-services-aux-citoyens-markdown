# Convention fiscale

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/fiscalite/article/convention-fiscale#sommaire_1">Champ d’application de la convention</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/fiscalite/article/convention-fiscale#sommaire_2">Dispositions conventionnelles sur certaines catégories de revenus</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/fiscalite/article/convention-fiscale#sommaire_3">Règles d’imposition</a></li></ul>
<p>La France et le Maroc ont signé à Paris <strong>le 29 mai 1970</strong> une convention en vue d’éviter les doubles impositions et de prévenir l’évasion fiscale en matière d’impôts sur le revenu et sur la fortune. Cette convention est <strong>entrée en vigueur le 1er décembre 1971. </strong></p>
<p>Cette convention a été modifiée par l’avenant du 18 août 1989 publié au Journal Officiel du 22 décembre 1990, décret n°90-1135 du 18 décembre 1990.</p>
<p>Son texte intégral est consultable sur le site Internet de l’administration fiscale. Voici un résumé de ses points clés pour les particuliers :</p>
<h3 class="spip"><a id="sommaire_1"></a>Champ d’application de la convention</h3>
<p>Cet accord a pour objet de protéger les résidents de chacun des Etats contractants en matière d’impôts prélevés directement sur le revenu ou sur le bénéfice des sociétés.</p>
<p>La Convention trouve donc à s’appliquer aux résidents de ces Etats.</p>
<h4 class="spip">Notion de domicile (au lieu de résidence)</h4>
<p>L’article 2, paragraphe 1, de la convention s’applique aux personnes physiques dont le domicile est réputé dans un Etat contractant.</p>
<p>D’après l’article 8, paragraphe 2 de la convention, une personne est considérée comme "étant domiciliée d’un Etat contractant" lorsque, en vertu de la législation dudit Etat, elle se trouve assujettie à l’impôt à raison de son domicile, de sa résidence, de son siège de direction ou de critères analogues.</p>
<p>Le paragraphe 1, de l’article 2, fournit les critères subsidiaires permettant de résoudre les cas de double domicile si l’assujettissement à l’impôt ne pouvait suffire. Ces critères sont :</p>
<ul class="spip">
<li>un foyer permanent d’habitation (il s’agit, par exemple, du lieu d’habitation du conjoint ou des enfants) ;</li>
<li>le centre de ses activités professionnelles ;</li>
<li>le lieu de séjour habituel (notion de 183 jours de présence physique sur le territoire au cours d’une année fiscale).</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Dispositions conventionnelles sur certaines catégories de revenus</h3>
<h4 class="spip">Traitements, salaires, pensions et rentes</h4>
<h5 class="spip">Rémunérations privées</h5>
<p><strong>Principe</strong></p>
<p>L’article 18, paragraphe 1, de la convention dispose que les salaires, traitements d’origine privée et autres rémunérations similaires reçues au titre d’un emploi salarié ne sont, en règle générale, imposables que dans l’Etat <strong>où s’exerce l’activité personnelle</strong>.</p>
<p><strong>Exceptions à cette règle générale</strong></p>
<p>Le maintien de l’imposition dans l’Etat de résidence du bénéficiaire est prévu par le paragraphe 2 du même article, sous réserve de trois conditions simultanées :</p>
<ul class="spip">
<li>le séjour temporaire dans l’autre Etat ne dépasse pas une durée totale de 183 jours,</li>
<li>les rémunérations sont payées par un employeur qui n’est pas domicilié dans l’Etat de séjour du bénéficiaire,</li>
<li>les rémunérations ne doivent pas être déduites des bénéfices d’un établissement stable ou d’une base fixe de l’employeur dans l’Etat du séjour temporaire du bénéficiaire.</li></ul>
<p>Il résulte des dispositions du paragraphe 3 de l’article 18 de la convention que les revenus professionnels des salariés employés à bord d’un navire ou d’un aéronef en trafic international ne sont imposables que dans l’Etat où se trouve la direction effective de l’entreprise.</p>
<h5 class="spip">Rémunérations publiques</h5>
<p><strong>Principe</strong></p>
<p>Le nouvel article 18 bis, paragraphe 1, de la convention indique que les traitements, salaires et rémunérations analogues versés par un Etat contractant ou une personne morale de droit public de cet Etat restent imposables dans cet Etat.</p>
<p><strong>Exceptions</strong></p>
<p>Toutefois, en vertu du paragraphe 2 du même article, cette règle ne s’applique pas lorsque le bénéficiaire ne possède pas la nationalité de cet Etat. L’imposition est dès lors réservée à l’autre Etat, celui du domicile de l’intéressé.</p>
<p>D’autre part, en vertu des dispositions du paragraphe 4 du même article, les règles fixées au paragraphe 1 dudit article sont également applicables aux rémunérations versées au titre de services rendus dans le cadre d’une activité industrielle ou commerciale exercée par un Etat ou une personne morale de droit public.</p>
<p>Les sommes versées à ce titre sont imposables dans l’Etat d’exercice de l’activité (article 18 de la convention) en ce qui concerne les salaires et autres rémunérations similaires, soit dans l’Etat de résidence du bénéficiaire (article 17 de la convention) en ce qui concerne les pensions.</p>
<h5 class="spip">Pensions et rentes viagères</h5>
<p>L’article 17 de la convention prévoit que les pensions de retraite ainsi que les rentes viagères restent imposables dans l’Etat du domicile fiscal du bénéficiaire.</p>
<h5 class="spip">Etudiants et stagiaires</h5>
<p>L’article 22 de la convention prévoit que les étudiants ou les stagiaires d’un Etat qui séjournent dans l’autre Etat à seule fin d’y poursuivre leurs études ou leur formation et qui perçoivent des subsides d’origine étrangère à cet Etat sont exonérés d’impôt par ce dernier Etat.</p>
<h4 class="spip">Autres catégories de revenus</h4>
<h5 class="spip">Bénéfices industriels et commerciaux</h5>
<p>L’article 10, paragraphe 1, dispose que les entreprises industrielles et commerciales sont imposables dans l’Etat sur le territoire duquel se trouve un établissement stable.</p>
<h5 class="spip">Revenus fonciers</h5>
<p>L’article 9 dispose que les revenus des biens immobiliers y compris les bénéfices des exploitations agricoles ou forestières sont imposables uniquement dans l’Etat où ils sont situés.</p>
<p>Cette règle s’applique également aux gains provenant de la cession desdits biens selon les dispositions de l’article 24, paragraphe 1.</p>
<h5 class="spip">Bénéfices des professions non commerciales et des revenus non commerciaux</h5>
<p>L’article 20, paragraphe 1, stipule que les revenus provenant de l’exercice d’une profession libérale ou d’autres activités indépendantes sont imposables sur le territoire duquel se trouve l’installation permanente ou s’exerce de façon régulière l’activité personnelle.</p>
<p>Les revenus que les professionnels du spectacle réalisent en cette qualité dans l’un des deux Etats restent imposables dans l’Etat d’exercice de l’activité selon les dispositions de l’article 21 de la convention.</p>
<p>L’article 16, paragraphe 2, pose en principe que les revenus commerciaux (redevances et droits d’auteur) restent imposables dans l’Etat du domicile du bénéficiaire. Toutefois, si la législation de l’Etat de la source le permet, il lui revient de les imposer à un taux ne pouvant dépasser 5% pour les droits d’auteur et 10% pour les redevances.</p>
<p>Par ailleurs, le paragraphe 1 du même article prévoit que les redevances versées pour la jouissance de biens immobiliers ou de ressources naturelles restent imposables dans l’Etat de situation des biens.</p>
<h5 class="spip">Revenus de capitaux mobiliers</h5>
<p><strong>Les dividendes</strong></p>
<p>Ce terme désigne les revenus provenant d’actions ou de revenus assimilés aux revenus d’actions par la législation fiscale de l’Etat sur le territoire duquel la société a son domicile.</p>
<p>De manière générale, et en vertu de l’article 13, les dividendes payés par une société domiciliée sur le territoire d’un Etat contractant à une personne domiciliée sur le territoire de l’autre Etat contractant sont imposables dans cet autre Etat.</p>
<p>Par ailleurs, ces dividendes sont aussi imposables dans l’Etat contractant où la société qui paie les dividendes est domiciliée mais, si la personne qui reçoit les dividendes en est le bénéficiaire effectif, l’impôt ainsi établi ne peut excéder 15 pour cent du montant brut des dividendes.</p>
<p>Toutefois, en vertu de ce même paragraphe, les dividendes distribués par des sociétés françaises au profit de personnes domiciliées au Maroc sont exemptés de ladite retenue s’ils sont imposables au Maroc au nom du bénéficiaire. Pour bénéficier de l’exonération de l’impôt français, un certificat de l’administration fiscale marocaine doit être fourni.</p>
<p>D’autre part, le paragraphe 4 prévoit que lorsque les dividendes payés par une société française ont donné lieu en France à la perception du précompte mobilier, les bénéficiaires de ces revenus domiciliés au Maroc peuvent en obtenir le remboursement sous déduction de la retenue à la source.</p>
<p>Des règles particulières sont prévues lorsque le bénéficiaire des dividendes possède dans l’Etat de la source de ces revenus, un établissement stable. Selon les dispositions du paragraphe 5, les dividendes se rattachant à cet établissement sont imposables au lieu de situation de l’établissement</p>
<p><strong>Les intérêts</strong></p>
<p>Ce terme désigne les revenus des fonds publics ou des obligations d’emprunts, des créances de toute nature ainsi que de tous autres produits assimilés aux revenus de sommes prêtées par la législation fiscale de l’Etat du débiteur.</p>
<p>Les intérêts provenant d’un Etat contractant et payés à une personne domiciliée sur le territoire de l’autre Etat sont imposables dans cet autre Etat.</p>
<p>Le paragraphe 2 de l’article 14 de la convention précise que les intérêts provenant d’un Etat et payés à un bénéficiaire domicilié dans l’autre Etat sont imposables dans le pays de la source aux taux de 15% du montant brut des intérêts des dépôts à terme et des bons de caisse, et de 10% du montant des autres intérêts.</p>
<h3 class="spip"><a id="sommaire_3"></a>Règles d’imposition</h3>
<p>L’élimination de la double imposition pour les personnes domiciliées en France qui perçoivent des revenus de source marocaine s’opère aux termes de l’article 25, paragraphe 1.</p>
<p>Les revenus de source française ou marocaine pour lesquels le droit d’imposer est dévolu à titre exclusif au Maroc doivent être maintenus en dehors de la base de l’impôt français, réserve faite toutefois de leur prise en compte pour la détermination du taux effectif.</p>
<p>Le calcul du taux effectif se décompose tout d’abord par la détermination d’une cotisation de base correspondant à l’ensemble des revenus de source française et(ou) étrangère passible de l’impôt français suivant les règles de la législation interne.</p>
<p>Ensuite, l’impôt exigible sera égal au produit de cette cotisation de base par le rapport entre le montant net total des revenus conventionnellement imposables en France et le montant total du revenu net d’après lequel le calcul de ladite cotisation de base a été effectué.</p>
<p>Au résultat ainsi obtenu peuvent être appliquées les réfactions prévues par la loi interne (crédit ou réduction d’impôt).</p>
<p>En ce qui concerne les dividendes et les intérêts, l’article 25, paragraphes 2 et 3, prévoit des règles particulières. Il s’agit en fait de la méthode de l’imputation (ou crédit d’impôt).</p>
<p>Les dividendes ou les intérêts de source marocaine distribués à des personnes domiciliées en France, sont en principe imposables au taux conventionnel prévu.</p>
<p>Mais en vertu des dispositions de ces paragraphes, il est accordé au bénéficiaire de France une déduction d’impôt correspondant au montant de l’impôt prélevé au Maroc selon les taux prévus au paragraphe 3.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/fiscalite/article/convention-fiscale). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
