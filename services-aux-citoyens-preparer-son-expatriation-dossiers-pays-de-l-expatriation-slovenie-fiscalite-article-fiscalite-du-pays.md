# Fiscalité du pays

<p>En 2012, le montant des prélèvements a représenté 22% du PIB et 60% des revenus de l’Etat. L’Administration fiscale de la République de Slovénie collecte l’essentiel des taxes, mis à part les droits de douane, les accises et les plus-values sur importations, collectés par l’Administration des douanes.</p>
<h4 class="spip">Fiscalité des particuliers</h4>
<p><strong>Imposition sur le revenu</strong></p>
<p>L’imposition des particuliers est calculée sur une base individuelle. Il n’y a pas de foyers fiscaux. La base fiscale est mesurée à partir des revenus, divisés en six catégories : revenus du travail, revenus d’entreprise, revenus agricoles ou forestiers, rentes et redevances, revenus du capital et « autres revenus ».</p>
<p>Les résidents sont redevables pour l’ensemble de leurs revenus, les non-résidents uniquement pour leurs revenus provenant de leurs activités en Slovénie.</p>
<p>Concernant les revenus du capital, les intérêts et les dividendes sont imposés à hauteur de 25 %. Le taux est dégressif concernant les gains du capital : 25 % pour des actifs détenus depuis moins de 5 ans, 15% de 5 à 10 ans, 10% de 10 à 15 ans, 5 % de 15 à 20 ans, et exonérés d’impôts au-delà.</p>
<p>Pour les catégories de revenus restantes, les impôts sont payés pendant l’année fiscale sous la forme de paiements anticipés. Ces paiements anticipés sont définitifs pour les non-résidents, ils ne constituent qu’un prépaiement pour les résidents.</p>
<p>La base imposable est calculée après déduction des contributions de sécurité sociale et de certaines allocations. Le montant net est ensuite imposé à taux variable. Le barème d’imposition pour l’année 2014 est le suivant :</p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>De 0 € à 8021,34 €</td>
<td>16%</td></tr>
<tr class="row_even even">
<td>De 8021,34 € à 18 960,28 €</td>
<td>27%</td></tr>
<tr class="row_odd odd">
<td>De 18 960,28 € à 70 907,20 €</td>
<td>41%</td></tr>
<tr class="row_even even">
<td>70 907,20 € et plus</td>
<td>50%</td></tr>
</tbody>
</table>
<p>Quand le montant des paiements anticipés excède le montant final de l’impôt, un remboursement est prévu pour le contribuable.</p>
<p><strong>TVA</strong></p>
<p><strong>Le taux standard est de 22%</strong>. <strong>Le taux réduit, à 9,5%</strong>, s’applique à certains biens et services (abonnement eau de ville, médicaments, livres et journaux entre autres).</p>
<p>Il existe par ailleurs un droit d’accise à payer sur l’alcool, le tabac et l’énergie, que ces biens soient produits en Slovénie ou importés.</p>
<p><strong>Cotisations sociales</strong></p>
<p>L’employé cotise pour quatre types de contributions reversées à deux caisses différentes (Caisse de Santé, Caisse des retraites) ou à l’Etat : l’assurance retraite et handicap, l’assurance maladie, l’assurance chômage et les congés maternité. Les contributions sont directement retenues par l’employeur, qui les reverse ensuite aux caisses afférentes.</p>
<h4 class="spip">Fiscalité des professionnels</h4>
<p><strong>Impôt sur les sociétés</strong></p>
<p><strong>Le taux général d’imposition sur les sociétés s’élève à 17%</strong> pour l’année 2013. Le taux spécial de 0% s’applique aux fonds d’investissements, aux fonds de pension et aux compagnies d’assurance pour les retraites.</p>
<p>Les compagnies de transport maritime peuvent choisir également de substituer l’impôt sur le tonnage à l’impôt sur les sociétés calculé sur la base d’une taxe journalière.</p>
<p><strong>Les investissements en RD sont déductibles à 100%, ceux en équipements et en biens immatériels à hauteur de 40%</strong>. Il existe d’autres allégements fiscaux : pour l’emploi de personnes handicapées, de stagiaires, pour les donations ou pour des contributions extraordinaires aux compagnies d’assurance notamment.</p>
<p><strong>Cotisations sociales</strong></p>
<p>L’employeur paye également une part des cotisations sociales.</p>
<p><i>Mise à jour le septembre 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/fiscalite/article/fiscalite-du-pays). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
