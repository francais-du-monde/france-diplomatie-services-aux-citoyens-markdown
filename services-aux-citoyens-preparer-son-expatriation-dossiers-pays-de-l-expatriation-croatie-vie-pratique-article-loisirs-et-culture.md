# Loisirs et culture

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/vie-pratique/article/loisirs-et-culture#sommaire_1">Tourisme</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/vie-pratique/article/loisirs-et-culture#sommaire_2">Activités culturelles</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/vie-pratique/article/loisirs-et-culture#sommaire_3">Sports</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/vie-pratique/article/loisirs-et-culture#sommaire_4">Télévision – Radio</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/vie-pratique/article/loisirs-et-culture#sommaire_5">Presse française</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Tourisme</h3>
<ul class="spip">
<li><a href="http://www.croatie.eu/index.php?lang=4" class="spip_out" rel="external">Site officiel de la Croatie</a></li>
<li><a href="http://croatia.hr/fr-FR/Homepage" class="spip_out" rel="external">Office national du tourisme croate</a></li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Activités culturelles</h3>
<h4 class="spip">Activités culturelles locales</h4>
<ul class="spip">
<li><a href="http://www.hnk.hr/" class="spip_out" rel="external">Théâtre national de Zagreb</a></li>
<li><a href="http://www.culturenet.hr/" class="spip_out" rel="external">CultureNet</a></li>
<li><a href="http://www.teatar.hr/" class="spip_out" rel="external">Teatar.hr</a></li>
<li><a href="http://www.idemvan.hr/" class="spip_out" rel="external">Idem van</a></li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Sports</h3>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.croatie.eu/index.php?lang=4" class="spip_out" rel="external">Site officiel de la Croatie</a></p>
<h3 class="spip"><a id="sommaire_4"></a>Télévision – Radio</h3>
<p>France 24, TV5 Monde et RFI diffusent en Croatie.</p>
<p>Les principales chaînes de télévision croate sont :</p>
<ul class="spip">
<li>de service public : HTV1, HTV2, HTV3 – chaîne culturelle, HTV - chaîne d’information continue ;</li>
<li>privées : Nova, RTL, Doma.</li></ul>
<p>Parmi les nombreuses stations radio, on peut citer :</p>
<ul class="spip">
<li>de service public : HR1, HR2, HR3 - radio classique</li>
<li>privées : Otvoreni, Radio 101, Narodni radio,…</li></ul>
<h3 class="spip"><a id="sommaire_5"></a>Presse française</h3>
<p>La presse française est disponible dans les aéroports, dans certains points de presse (Tisak) et à la librairie internationale Algoritam de Zagreb (Gajeva 1), entre autres.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/vie-pratique/article/loisirs-et-culture). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
