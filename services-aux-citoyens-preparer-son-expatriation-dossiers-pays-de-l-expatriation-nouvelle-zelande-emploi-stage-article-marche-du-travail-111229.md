# Marché du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/emploi-stage/article/marche-du-travail-111229#sommaire_1">Secteurs à fort potentiel</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/emploi-stage/article/marche-du-travail-111229#sommaire_2">Professions règlementées</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/emploi-stage/article/marche-du-travail-111229#sommaire_3">Rémunération</a></li></ul>
<p>La Nouvelle-Zélande, pays moderne, politiquement stable et au cadre de vie privilégié offre des opportunités économiques intéressantes.</p>
<p>Pour en savoir plus :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.frogs-in-nz.com/tribu/Infos-Expats/S-installer-en-Nouvelle-Zelande/Quel-est-le-cout-de-la-vie-en-Nouvelle-Zelande" class="spip_out" rel="external">Le site de la communauté PVT et expatriés</a></p>
<p>Le marché du travail est dynamique, notamment dans le secteur du tertiaire. En effet, alors que l’économie néo-zélandaise reposait largement sur l’agriculture il y a encore quelques décennies, aujourd’hui, elle se tourne de plus en plus vers les services.</p>
<p>Le taux de chômage ne cesse de diminuer depuis une quinzaine d’années et a atteint environ 6,4 % en juin 2013.</p>
<h3 class="spip"><a id="sommaire_1"></a>Secteurs à fort potentiel</h3>
<p>Les industries créatives (telles que les technologies de l’information et de la communication, la publicité, le design), les biotechnologies, le tourisme (hôtellerie-restauration), l’agriculture-agroalimentaire, la banque, la finance, les assurances, l’immobilier, les industries et les énergies, le bâtiment, la santé, l’éducation et l’environnement sont autant de secteurs porteurs d’emplois en Nouvelle-Zélande.</p>
<p>Les profils recherchés actuellement sont les ingénieurs informaticiens, les spécialistes IT (technologie d’information), les infirmières (les équivalences de diplôme n’existent pas cependant), les professionnels du marketing, de la finance, de la comptabilité. Une pratique courante de l’anglais est exigée.</p>
<p>Une liste publiée chaque année par le gouvernement identifie les besoins professionnels urgents dans tout le pays :</p>
<ul class="spip">
<li><a href="http://www.visabureau.com/newzealand/skill-shortage.aspx" class="spip_out" rel="external">Visabureau.com</a></li>
<li><a href="http://www.immigration.govt.nz/" class="spip_out" rel="external">Immigration.govt.nz</a></li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Professions règlementées</h3>
<p>Certains secteurs professionnels ne reconnaissent pas (ou très difficilement) les équivalences de diplôme (exemple : professions médicales, architectes, ingénieurs en construction navale etc.).</p>
<p>Renseignements sur l’homologation de diplômes étrangers auprès de la <a href="http://www.nzqa.govt.nz/" class="spip_out" rel="external">New Zealand Qualifications Authority (NZQA)</a></p>
<h3 class="spip"><a id="sommaire_3"></a>Rémunération</h3>
<p>Exemples de salaires généralement pratiqués en 2008 :Employé dans une société de production : 2000 euros avant impôts par mois (temps plein). Employé dans une administration néo-zélandaise : 2500 euros avant impôts par mois (temps plein).</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/emploi-stage/article/marche-du-travail-111229). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
