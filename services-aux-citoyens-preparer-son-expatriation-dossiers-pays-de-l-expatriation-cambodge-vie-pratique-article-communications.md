# Communications

<h4 class="spip">Téléphone – Internet</h4>
<p>L’indicatif téléphonique du pays et de Phnom-Penh à partir de la France est le 00 855. Les liaisons téléphoniques fonctionnent assez bien, mais le coût est élevé au départ du Cambodge (environ 2 USD/minute vers la France).</p>
<h4 class="spip">Cybercafés à Phnom-Penh</h4>
<p><strong>A Reliable Khmer Web InterNet  Email Services </strong><br class="manualbr"># 150 Eo, Sihanouk Blvd (proche du supermarché Lucky) <br class="manualbr">Phnom-Penh <br class="manualbr">Tél. : (855) 12 912 812 <br class="manualbr">Heures d’ouverture : 7 heures à minuit</p>
<p><strong>Cambodia Internet Technology </strong><br class="manualbr">1 C, St. 278 <br class="manualbr">Sangkat Beoung Keng Kang 1 Khan Chorm Kar Morn <br class="manualbr">Phnom-Penh <br class="manualbr">Tél. : (855) 16 804 588 <br class="manualbr">Heures d’ouverture : de 8 heures à 23 heures</p>
<p><strong>Khmer Web 1 </strong><br class="manualbr">Office no. 29, rue 182 <br class="manualbr">Plov Okhna Tep Porn, Orusey Market <br class="manualbr">Phnom-Penh <br class="manualbr">Tél. : (855) 12 912 812 <br class="manualbr">Heures d’ouverture : 7 heures à 23 heures</p>
<p>Pour en savoir plus :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.tv5.org/TV5Site/voyageurs/accueil.php" class="spip_out" rel="external">TV5.org</a> </p>
<p>Téléphoner gratuitement par Internet Il est généralement très avantageux d’utiliser <strong>des logiciels de téléphonie sur IP</strong> (Skype, Google talk, Live messenger, Yahoo ! messenger, etc.) car les tarifs proposés à l’international sont souvent moindre que ceux des opérateurs locaux exploitant des lignes classiques, voire gratuits dans certaines conditions.</p>
<h4 class="spip">Poste</h4>
<p>Les liaisons postales sont quotidiennes et le courrier est acheminé en une semaine (Paris-Phnom-Penh) avec d’assez bonnes garanties de réception.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/cambodge/vie-pratique/article/communications). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
