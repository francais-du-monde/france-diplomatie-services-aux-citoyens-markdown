# Comment faire une demande de passeport ?

<p>Le passeport est un document essentiel car il atteste de votre identité et de votre nationalité à l’étranger. Pensez à vérifier régulièrement la validité de votre passeport et commencez les démarches de renouvellement plusieurs semaines avant sa date d’expiration.</p>
<p>Conseil : Scannez votre passeport et conservez en une copie sur votre messagerie électronique ou dans <a href="https://mon.service-public.fr/portail/app/public/actualites/details/14023" class="spip_out" rel="external">le compte service-public.fr</a></p>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/documents-officiels-a-l-etranger/article/comment-faire-une-demande-de-passeport#sommaire_1">A quelle autorité demander un passeport ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/documents-officiels-a-l-etranger/article/comment-faire-une-demande-de-passeport#sommaire_2">Quelles sont les pièces à fournir ?</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/documents-officiels-a-l-etranger/article/comment-faire-une-demande-de-passeport#sommaire_3">Quelle est la durée de validité du passeport ?</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/documents-officiels-a-l-etranger/article/comment-faire-une-demande-de-passeport#sommaire_4">Quel est le montant des droits à acquitter ?</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/documents-officiels-a-l-etranger/article/comment-faire-une-demande-de-passeport#sommaire_5">Quel est le délai de délivrance ?</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/documents-officiels-a-l-etranger/article/comment-faire-une-demande-de-passeport#sommaire_6">Comment demander un passeport pour un mineur ?</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/documents-officiels-a-l-etranger/article/comment-faire-une-demande-de-passeport#sommaire_7">Que faire en cas de perte ou de vol de passeport à l’étranger ?</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>A quelle autorité demander un passeport ?</h3>
<p>Quel que soit votre lieu de résidence, vous pouvez demander un passeport auprès de n’importe quelle ville de France ou de n’importe quel <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/article/annuaire-des-representations-francaises-a-l-etranger" class="spip_in">consulat ou ambassade de France à l’étranger</a>.</p>
<p><strong>A l’étranger</strong></p>
<p>Les ambassadeurs et consuls habilités sont compétents pour délivrer des passeports :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>un passeport ordinaire (valable 10 ans, 5 ans pour les mineurs) aux Français établis hors de France ou de passage à l’étranger.</strong> </p><dl class="spip_document_34621 spip_documents spip_documents_center">
<dt>
<img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L158xH218/passeport_biometrique-f411f.jpg" width="158" height="218" alt="JPEG - 38.9 ko"></dt>
<dt class="spip_doc_titre">passeport "ordinaire"</dt>
</dl>
<p><strong>NB :</strong> <strong>Il est obligatoire de se présenter personnellement lors de la demande et du retrait du passeport.</strong> Toutefois, depuis le 7 août 2010, les Français effectuant une demande de passeport à l’étranger ont la possibilité de se voir remettre leur titre par un consul honoraire spécialement habilité à cette fin et dépendant du poste diplomatique ou consulaire auprès duquel ils ont fait la demande, ou lors d’une tournée consulaire prévue et organisée par le personnel de ce même poste. La comparution personnelle du titulaire du titre reste obligatoire.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>un passeport d’urgence (valable 1 an) aux Français :</strong> </p>
<p>— de passage dépourvus de titre de voyage (en cas de perte ou de vol de passeport notamment) et qui doivent poursuivre leur voyage à l’étranger (pour un retour en France, un laissez-passer est délivré aux Français démunis de leur passeport) ;<br>— résidant dans la circonscription consulaire, dépourvus de titre de voyage ou munis d’un passeport dont la durée de validité n’est pas suffisante, devant voyager de toute urgence et qui, pour des raisons particulières, ne peuvent différer leur voyage.</p>
<dl class="spip_document_34623 spip_documents spip_documents_center">
<dt>
<img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L158xH222/passeport_urgence-f5478.jpg" width="158" height="222" alt="JPEG - 40.8 ko"></dt>
<dt class="spip_doc_titre">passeport d’urgence</dt>
</dl>
<p><strong>Attention</strong> : le passeport d’urgence est individuel. Il est lisible en machine, la photographie de son titulaire est numérisée : il ne permet pas d’entrer aux <a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/etats-unis/#enc4" class="spip_in">Etats-Unis d’Amérique</a> sans visa (depuis le 1er juillet 2009).</p>
<h3 class="spip"><a id="sommaire_2"></a>Quelles sont les pièces à fournir ?</h3>
<p><strong>Dans tous les cas</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  une photographie d’identité
<br>— récente et parfaitement ressemblante ;
<br>— de face ;
<br>— tête nue ;
<br>— de format 35 x 45 mm.
<br>— <a href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/depliant_norme_photo-2.pdf" class="spip_in" type="application/pdf">Pour en savoir plus dépliant sur les normes des photographies d’identité (PDF, 1,17 Mo)</a></p>
<p><strong>NB </strong> : Depuis le 1er septembre 2015, l’usager doit fournir la photographie d’identité.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  un justificatif de domicile parmi les documents suivants :
<br>— titre de propriété ou bail de location ;
<br>— quittance de loyer, de gaz ou d’électricité ;
<br>— attestation d’assurance du logement ;
<br>— titre de séjour délivré par les autorités locales.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  En cas de première demande de passeport : si le demandeur peut présenter une carte nationale d’identité plastifiée valide ou périmée depuis moins de cinq ans, une carte nationale d’identité non sécurisée en cours de validité ou périmée depuis moins de deux ans ou un passeport de service ou de mission valide ou périmé depuis moins de cinq ans : aucun document supplémentaire n’est requis. </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  En cas de renouvellement : Si le demandeur peut présenter un passeport biométrique, de service ou de mission valide ou périmé depuis moins de cinq ans, un passeport non biométrique valide ou périmé depuis moins de deux ans, une carte nationale d’identité sécurisée valide ou périmée depuis moins de cinq ans ou une carte nationale d’identité non sécurisée valide ou périmée depuis moins de deux ans : aucun document supplémentaire n’est requis.</p>
<p><strong>Pièces complémentaires</strong></p>
<p><strong>1. En cas de première demande de passeport :</strong> le demandeur ne peut pas présenter de carte nationale d’identité sécurisée ou il peut fournir une carte nationale d’identité non sécurisée périmée depuis plus de deux ans. Il faut fournir :</p>
<ul class="spip">
<li>un justificatif d’état civil : un extrait de l’acte de naissance du demandeur comportant l’indication de la filiation, ou à défaut, la copie intégrale de son acte de mariage ;</li>
<li>pour les personnes nées en France, l’acte doit être demandé à la mairie de naissance ;</li>
<li>pour les personnes nées à l’étranger dont l’acte de naissance est conservé par le service central de l’état civil du ministère des Affaires étrangères, l’acte peut être demandé :</li>
<li><a href="https://pastel.diplomatie.gouv.fr/dali/index2.html" class="spip_out" rel="external">Par Internet, sur le site du ministère des Affaires étrangères</a></li>
<li>Par courrier : au Service central de l’état civil, 11 rue de la Maison blanche 44941 Nantes Cedex 9 ;</li></ul>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  si le justificatif d’état civil ne suffit pas pour démontrer la nationalité : un justificatif de nationalité française (exemplaire enregistré de la déclaration de nationalité française ; décret de naturalisation ou de réintégration dans la nationalité française (ampliation, exemplaire du Journal Officiel ou attestation du ministère chargé des naturalisations) ; certificat de nationalité française).</p>
<p><strong>2. En cas de renouvellement de passeport : </strong> le passeport à renouveler est un passeport non sécurisé périmé depuis plus de deux ans : <strong>les documents à fournir sont les mêmes que pour une première demande</strong>.</p>
<p>Pour connaître la procédure à suivre en cas de perte ou de vol de passeport, consulter la rubrique <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/documents-officiels-a-l-etranger/article/comment-faire-une-demande-de-passeport#sommaire_7" class="spip_in">que faire en cas de perte ou de vol du passeport à l’étranger</a> ?</p>
<h3 class="spip"><a id="sommaire_3"></a>Quelle est la durée de validité du passeport ?</h3>
<table class="spip" summary="Titre de passeport et validité">
<caption>Durée de validité du passeport</caption>
<thead><tr class="row_first"><th id="id86ed_c0">Type de passeport</th><th id="id86ed_c1"> </th><th id="id86ed_c2">Validité</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="id86ed_c0">Passeport d’urgence</td>
<td headers="id86ed_c1"></td>
<td headers="id86ed_c2">1 an</td></tr>
<tr class="row_even even">
<td headers="id86ed_c0">Passeport "ordinaire"</td>
<td headers="id86ed_c1">- délivré à un majeur</td>
<td headers="id86ed_c2">10 ans</td></tr>
<tr class="row_odd odd">
<td headers="id86ed_c0"></td>
<td headers="id86ed_c1">- délivré à un mineur</td>
<td headers="id86ed_c2">5 ans</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_4"></a>Quel est le montant des droits à acquitter ?</h3>
<table class="spip" summary="Type de passeport et tarif">
<caption>Montant des droits à acquitter</caption>
<thead><tr class="row_first"><th id="id7ecb_c0">Type de passeport </th><th id="id7ecb_c1"> </th><th id="id7ecb_c2">Tarif *</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="id7ecb_c0">Passeport d’urgence</td>
<td headers="id7ecb_c1"></td>
<td headers="id7ecb_c2">45€</td></tr>
<tr class="row_even even">
<td headers="id7ecb_c0">Passeport « ordinaire »</td>
<td headers="id7ecb_c1">- délivré à un majeur</td>
<td headers="id7ecb_c2">96 €</td></tr>
<tr class="row_odd odd">
<td headers="id7ecb_c0"></td>
<td headers="id7ecb_c1">- délivré à un mineur âgé de plus de 15 ans</td>
<td headers="id7ecb_c2">52 €</td></tr>
<tr class="row_even even">
<td headers="id7ecb_c0"></td>
<td headers="id7ecb_c1">- délivré à un mineur de moins de 15 ans</td>
<td headers="id7ecb_c2">27 €</td></tr>
</tbody>
</table>
<p>* Le tarif est augmenté de 3 € si l’usager ne peut fournir la photographie aux normes OACI et que celle-ci doit être prise sur place (si les ambassades et postes consulaires qui délivrent des passeports biométriques sont équipés de dispositifs permettant la prise de photographie).</p>
<p>A l’étranger, il n’est pas demandé de fournir un timbre fiscal, il est possible de s’acquitter des droits de chancellerie directement au consulat. Vous pouvez régler par :</p>
<ul class="spip">
<li>espèces ;</li>
<li>mandat postal ;</li>
<li>chèque ou carte bancaire, auprès de certains consulats seulement.</li></ul>
<h3 class="spip"><a id="sommaire_5"></a>Quel est le délai de délivrance ?</h3>
<ul class="spip">
<li>le passeport d’urgence est délivré dans un délai très court ;</li>
<li>le délai de délivrance d’un passeport est de l’ordre de deux à trois semaines. C’est pourquoi il est recommandé d’anticiper le renouvellement du passeport.</li></ul>
<h3 class="spip"><a id="sommaire_6"></a>Comment demander un passeport pour un mineur ?</h3>
<p>Toute personne titulaire de l’autorité parentale peut demander un passeport individuel pour son enfant.</p>
<p><strong>Quelles sont les pièces à fournir ?</strong></p>
<p>Toutes les pièces précédemment énoncées (voir liste)</p>
<p>Une pièce prouvant l’exercice de l’autorité parentale :</p>
<ul class="spip">
<li>livret de famille ;</li>
<li>en cas de divorce ou de séparation : copie du jugement correspondant.</li></ul>
<p>Les enfants ne peuvent plus être inscrits sur le passeport de leurs parents.<br class="autobr">Toutefois, les passeports délivrés antérieurement au 12 juin 2006, qui font apparaître des enfants mineurs de moins de 15 ans sur le passeport de leurs parents, demeurent valables jusqu’au terme de leur validité normale.</p>
<h3 class="spip"><a id="sommaire_7"></a>Que faire en cas de perte ou de vol de passeport à l’étranger ?</h3>
<p>La perte ou le vol d’un passeport doit immédiatement être déclarée selon les modalités suivantes :</p>
<ul class="spip">
<li>en cas de vol : déclaration auprès des autorités de police locales puis à l’ambassade ou au poste consulaire le plus proche ;</li>
<li>en cas de perte : déclaration de perte à l’ambassade ou au poste consulaire le plus proche.</li></ul>
<p>Lors d’un voyage à l’étranger, il convient d’être particulièrement vigilant afin d’éviter de perdre son passeport ou d’être victime d’un vol.</p>
<p>Il est par ailleurs vivement conseillé de conserver, séparément, une photocopie du passeport.</p>
<p><i>Mise à jour : juillet 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/documents-officiels-a-l-etranger/article/comment-faire-une-demande-de-passeport). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
