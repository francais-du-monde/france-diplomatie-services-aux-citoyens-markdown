# Animaux domestiques

<h4 class="spip">Chiens et chats et autres animaux de compagnie</h4>
<p>L’animal doit être obligatoirement tatoué ou, le cas échéant, avoir un numéro d’identification par puce électronique (obligatoire pour le retour en métropole de même que le contrôle sérologique de la vaccination anti-rabique auprès du laboratoire départemental de la Sarthe).</p>
<p>Un certificat sanitaire délivré par les autorités vétérinaires compétentes du pays de provenance sera exigé à l’entrée dans le pays.</p>
<p>Il convient d’avoir également le passeport européen ou carnet de vaccination de votre animal.</p>
<p>Il est interdit d’importer les espèces de chiens dangereux et féroces appartenant aux races suivantes :</p>
<ul class="spip">
<li>pitt-bull</li>
<li>rott Weiler</li>
<li>tosa</li>
<li>mastiff ou boer bull ;</li></ul>
<p><strong>Pour plus d’informations :</strong></p>
<p>Contacter les services du ministère de l’Agriculture, des Ressources hydrauliques et de la Pêche - direction générale des services vétérinaires.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/entree-et-sejour/article/animaux-domestiques). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
