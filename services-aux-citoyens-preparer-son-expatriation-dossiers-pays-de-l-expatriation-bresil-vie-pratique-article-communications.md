# Communications

<h4 class="spip">Téléphone – Internet</h4>
<p>Les liaisons téléphoniques sont bonnes. Les communications sont moins coûteuses dans le sens Brésil-France.</p>
<p><strong>France  Brésil</strong></p>
<ul class="spip">
<li>00 55 + 61 pour Brasilia + numéro d’abonné ;</li>
<li>00 55 + 81 pour Recife + numéro d’abonné ;</li>
<li>00 55 + 21 pour Rio de Janeiro + 9 + numéro d’abonné ;</li>
<li>00 55 + 11 pour Sao Paulo + numéro d’abonné.</li></ul>
<p><strong>Brésil  France</strong></p>
<p>00 33 + le numéro d’abonné sans le "0" local (soit 9 chiffres).</p>
<p>L’internet est largement utilisé au Brésil avec une forte promotion de l’accès au haut débit (<i>banda larga</i>). Nombre d’expatriés ont recours à l’abonnement par le câble pour téléphoner à l’étranger.</p>
<h4 class="spip">Téléphoner gratuitement par Internet </h4>
<p>Il est généralement très avantageux d’utiliser <strong>des logiciels de téléphonie sur IP</strong> (Skype, Google talk, Live messenger, Yahoo ! messenger, etc.) car les tarifs proposés à l’international sont souvent moindre que ceux des opérateurs locaux exploitant des lignes classiques, voire gratuits dans certaines conditions.</p>
<h4 class="spip">Poste</h4>
<p>Les liaisons postales s’effectuent correctement.</p>
<p>Pour l’acheminement du courrier, il faut compter environ une huitaine de jours pour l’arrivée en France d’une lettre postée au Brésil, environ trois semaines pour un colis.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/vie-pratique/article/communications). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
