# Lettre de motivation

<h4 class="spip">Rédaction</h4>
<p>Dans la lettre de candidature, il convient de donner un aperçu de votre parcours professionnel, de son orientation, du domaine dans lequel vous souhaitez développer vos compétences.</p>
<p>L’<strong>entretien d’embauche</strong> est composé de :</p>
<ul class="spip">
<li>d’un à trois entretiens ;</li>
<li>pas de question sur les points personnels sensibles (la religion, la politique, la vie matrimoniale ou assimilée…) ;</li>
<li>le recruteur vous demandera de revoir la description des tâches ;</li>
<li>vous êtes censé être interrogé sur vos points forts, vos faiblesses, vos projets à court et à long terme ;</li>
<li>posez des questions à l´employeur ;</li>
<li>en fonction de la taille et de la culture de l´entreprise, d´autres exigences peuvent se présenter (tests psychologiques, tests de simulation de situations…).</li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/emploi-stage/article/lettre-de-motivation). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
