# Scolarisation

<p>Pour toute information sur la scolarisation dans le système français à l’étranger, vous pouvez consulter notre thématique sur les <a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures.md" class="spip_in">Etudes supérieures</a> et la <a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais.md" class="spip_in">Scolarité en français</a>.</p>
<p>Vous y trouverez des renseignements sur :</p>
<ul class="spip">
<li>les établissements français du primaire et du secondaire à l’étranger ;</li>
<li>les bourses scolaires et la prise en charge des frais de scolarité à l’étranger ;</li>
<li>les possibilités qui s’offrent à vous si votre enfant ne peut être scolarisé à l’étranger dans le système français (enseignement à distance par le CNED, programme français langue maternelle (FLAM), internats en France) ;</li>
<li>les épreuves du baccalauréat à l’étranger ;</li>
<li>les bourses d’études supérieures en France et à l’étranger ;</li>
<li>l’équivalence des diplômes.</li></ul>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.aefe.fr/" class="spip_out" rel="external">Agence pour l’enseignement français à l’étranger</a></p>
<h4 class="spip">Les établissements scolaires français en Slovénie</h4>
<p>L’<a href="http://www.efl.si/" class="spip_out" rel="external">école française de Ljubljana</a> est un établissement conventionné avec l’AEFE depuis 2001. Elle est installée dans les murs d’une école slovène, l’école « Livada » et accueille pour l’année scolaire 2013-2014 89 élèves (25 français, 30 slovènes, 11 franco-slovènes, 23 étrangers tiers), répartis de la maternelle (toute petite section) à la classe de seconde.</p>
<p><strong>Coordonnées de l’école française de Ljubljana :</strong></p>
<p>Ulica Dušana Kraigherja 2<br class="manualbr">1000 Ljubljana<br class="manualbr">Téléphone : +386 1 429 14 99<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/vie-pratique/article/scolarisation#direction#mc#efljubljana.com#" title="direction..åt..efljubljana.com" onclick="location.href=mc_lancerlien('direction','efljubljana.com'); return false;" class="spip_mail">Courriel</a></p>
<h4 class="spip">Enseignement supérieur</h4>
<p>Pas d’enseignement supérieur français en Slovénie.</p>
<p><i>Mise à jour : septembre 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/vie-pratique/article/scolarisation). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
