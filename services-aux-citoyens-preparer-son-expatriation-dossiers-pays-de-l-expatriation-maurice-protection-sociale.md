# Protection sociale

<h2 class="rub23541">Régime local de sécurité sociale</h2>
<p>Vous trouverez une présentation détaillée du système de sécurité sociale mauricien sur le site de l’<a href="http://www.issa.int/fr/country-details?countryId=NZregionId=ASIfiltered=false" class="spip_out" rel="external">Association internationale de la sécurité sociale (AISS)</a>.</p>
<p><i>Mise à jour : juin 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-maurice-protection-sociale-article-convention-de-securite-sociale-114409.md" title="Convention de sécurité sociale">Convention de sécurité sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-maurice-protection-sociale-article-regime-local-de-securite-sociale.md" title="Régime local de sécurité sociale">Régime local de sécurité sociale</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/protection-sociale/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
