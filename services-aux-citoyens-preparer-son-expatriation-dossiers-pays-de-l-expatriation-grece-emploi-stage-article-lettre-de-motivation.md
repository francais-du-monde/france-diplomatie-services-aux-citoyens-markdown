# Lettre de motivation

<h4 class="spip">Rédaction</h4>
<p>Pour la Grèce, il est conseillé de suivre les règles de rédaction de la lettre de motivation française.</p>
<p>Le candidat peut joindre des lettres de références. Il est préférable qu’elles se rapportent à sa dernière activité professionnelle.</p>
<p>Les entreprises multinationales exigent des lettres de recommandation (une ou deux en fonction de la durée des postes précédemment occupés par le candidat), éléments "clés" pour la confirmation des curriculum vitae. Les entreprises grecques de petite taille n’exigent pas ce type de lettre.</p>
<p><i>Mise à jour : novembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/emploi-stage/article/lettre-de-motivation). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
