# Convention fiscale

<p>Une convention internationale ayant primauté sur la loi interne, les dispositions de la loi du 29 décembre 1976 modifiant les règles de territorialité et les conditions d’imposition des Français de l’étranger (Journal Officiel du 30 décembre 1976) ne sont applicables que si elles ne contreviennent pas aux dispositions de la convention.</p>
<p>La convention fiscale conclue entre la France et la Finlande le 11 septembre 1970 et publiée, après ratification, au Journal Officiel du 22 avril 1972, établit un partage d’imposition des revenus entre les parties contractantes.</p>
<p>Le texte de la convention est disponible sur le site <a href="http://www.impots.gouv.fr/portal/static/" class="spip_out" rel="external">des impôts</a>.</p>
<p>Ses dispositions principales concernant les Français expatriés en Finlande sont les suivantes :</p>
<p>Sauf accords particuliers prévoyant des régimes spéciaux en cette matière, les salaires, traitements et autres rémunérations similaires qu’une personne domiciliée dans l’un des deux Etats contractants reçoit au titre d’un emploi salarié ne sont imposables que dans cet Etat, à moins que l’emploi ne soit exercé dans l’autre Etat contractant. Dans ce cas les rémunérations reçues à ce titre sont imposables dans cet autre Etat.</p>
<p>Nonobstant les dispositions du paragraphe ci-dessus, les rémunérations qu’une personne domiciliée dans un Etat contractant reçoit au titre d’un emploi salarié exercé dans l’autre Etat contractant ne sont imposables que dans le premier Etat si sont satisfaites les trois conditions ci-après :</p>
<ul class="spip">
<li>Le bénéficiaire séjourne dans l’autre Etat pendant une période ou des périodes n’excédant pas au total 183 jours au cours de l’année fiscale considérée,</li>
<li>Les rémunérations sont payées par un employeur ou au nom d’un employeur qui n’est pas domicilié dans l’autre Etat et,</li>
<li>Les rémunérations ne sont pas déduites des bénéfices d’un établissement stable ou d’une base fixe que l’employeur a dans l’autre Etat.</li></ul>
<p>Si le contribuable français effectue une mission dans l’Etat finlandais et que celle-ci est rémunérée par les Finlandais, le contribuable fera l’objet en France d’un taux effectif. En effet, cette mission imposable en Finlande sera rajoutée aux revenus français afin de déterminer le taux de l’impôt applicable aux revenus uniquement français.</p>
<p><i>Mise à jour : janvier 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/fiscalite-22347/article/convention-fiscale). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
