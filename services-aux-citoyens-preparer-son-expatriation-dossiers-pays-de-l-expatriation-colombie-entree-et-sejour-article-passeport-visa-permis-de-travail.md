# Passeport, visa, permis de travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/entree-et-sejour/article/passeport-visa-permis-de-travail#sommaire_1">Touristes</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/entree-et-sejour/article/passeport-visa-permis-de-travail#sommaire_2">Étudiants</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/entree-et-sejour/article/passeport-visa-permis-de-travail#sommaire_3">Visa d’affaires</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/entree-et-sejour/article/passeport-visa-permis-de-travail#sommaire_4">Visa de travail</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/entree-et-sejour/article/passeport-visa-permis-de-travail#sommaire_5">Les conjoints</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/entree-et-sejour/article/passeport-visa-permis-de-travail#sommaire_6">Visas temporaires spéciaux</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Touristes</h3>
<p>Les ressortissants français peuvent entrer sur le territoire colombien sans visa pour une visite touristique <strong>d’une durée maximum de trois mois</strong>, qui peut cependant être réduite à l’entrée du territoire. A la fin de cette période, une prolongation peut être demandée au département administratif de sécurité. Cette prolongation ne peut cependant pas excéder six mois.</p>
<h3 class="spip"><a id="sommaire_2"></a>Étudiants</h3>
<p>Les ressortissants français venant étudier dans une université colombienne ou effectuer un stage sur le territoire colombien peuvent bénéficier d’un visa pour une durée maximale <strong>d’un an</strong>. Pour les stages rémunérés l’étudiant doit faire une demande de visa de travail.</p>
<p><strong>Attention</strong> : les titulaires d’un visa ordinaire d’étudiant ou d’étudiant stagiaire ne peuvent en aucun cas travailler.</p>
<h3 class="spip"><a id="sommaire_3"></a>Visa d’affaires</h3>
<p>Il est délivré aux professionnels envoyés en mission en Colombie ou aux ressortissants français travaillant pour une société basée en Colombie. La durée de validité est de <strong>deux ans</strong> maximum. Le dossier doit être déposé au consulat général de Colombie en France dix jours minimum avant le départ.</p>
<h3 class="spip"><a id="sommaire_4"></a>Visa de travail</h3>
<p>Le visa de travail est délivré aux étrangers engagés par des sociétés privées ou publiques, aux directeurs, techniciens et personnel administratif expatriés par des sociétés à l’étranger. La durée de ce visa ne peut toutefois pas dépasser <strong>deux ans</strong>. Le dossier de demande de visa doit être déposé, 10 jours minimum avant le départ, au consulat général de Colombie à Paris.</p>
<p>Si vous avez obtenu un visa, vous devez également solliciter auprès du département administratif de sécurité une carte de résidence (« cédula de extranjería »). Sa durée de validité est égale à celle du visa.</p>
<h3 class="spip"><a id="sommaire_5"></a>Les conjoints</h3>
<p>Pour les conjoints ou compagnons de ressortissants colombiens un visa temporaire d’une durée maximale de deux ans doit être demandé au consulat général de Colombie à Paris.</p>
<h3 class="spip"><a id="sommaire_6"></a>Visas temporaires spéciaux</h3>
<ul class="spip">
<li>Visa spécial pour les volontaires des ONG. Sa validité est d’un an maximum ;</li>
<li>Visa d’adoption ;</li>
<li>Visa temporaire technique.</li></ul>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.cancilleria.gov.co/services/colombia/visas" class="spip_out" rel="external">Rubrique visas sur le site du ministère colombien des Affaires étrangères</a></li>
<li><a href="http://www.ambafrance-co.org/-Conditions-d-entree-et-de-sejour-" class="spip_out" rel="external">Rubrique décrivant les formalités de séjour sur le site de l’Ambassade de France en Colombie</a></li>
<li><a href="http://www.consulatcolombie.com/" class="spip_out" rel="external">Rubrique Visas sur le site de l’ambassade de Colombie en France</a></li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/entree-et-sejour/article/passeport-visa-permis-de-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
