# Entrée et séjour

<h2 class="rub22933">Passeport, visa, permis de travail</h2>
<p>De manière générale, et quel que soit le motif de votre séjour, il convient de contacter la section consulaire de l’ambassade du Maroc à Paris qui vous informera sur la règlementation en matière d’entrée et de séjour au Maroc, règlementation que vous devrez impérativement respecter. N’hésitez pas à consulter le site du ministère de l’Intérieur marocain afin de vous familiariser avec les modalités d’entrée et de séjour dans le pays.</p>
<p>Le Consulat de France au Maroc n’est pas l’organisme compétent pour répondre à vos demandes d’information concernant votre séjour au Maroc.</p>
<p>Pour un <strong>séjour égal ou inférieur à trois mois</strong>, le passeport en cours de validité (trois mois minimum) est obligatoire. Si aucun visa n’est exigé pour les ressortissants français, la carte nationale d’identité n’est pas reconnue pour pénétrer sur le territoire marocain (sauf dans le cas d’un voyage organisé en groupe).</p>
<p><strong>Tout Français désirant séjourner au Maroc au delà de trois mois ou d’y occuper un emploi, </strong>salarié ou non, <strong>doit se faire immatriculer dans les 15 jours qui suivent la date de son entrée et obtenir une autorisation de séjour auprès des autorités marocaines. </strong>Cette carte est nécessaire pour l’accomplissement de certains actes de la vie civile : ouverture d’un compte bancaire, établissement de carte grise automobile, etc.</p>
<p>La carte de séjour à une durée de validité d’une année renouvelable. La demande de renouvellement doit être formulée deux mois avant l’expiration de la carte. Après trois ans de séjour, il est possible d’obtenir un permis de séjour valable 10 ans.</p>
<p>Les pièces nécessaires pour l’obtention du permis de séjour sont à présenter <strong>à la préfecture de police ou à la brigade de gendarmerie de votre circonscription.</strong></p>
<p>Que vous effectuiez une première demande d’immatriculation ou bien un renouvellement, vous devez fournir les documents suivants :</p>
<ul class="spip">
<li>photocopie légalisée du contrat de travail pour les salariés ou du registre du commerce et patente pour les chefs d’entreprise ;</li>
<li>tout justificatif de ressources (pension de retraite)</li>
<li>2 imprimés de demande d’immatriculation à compléter et signer ;</li>
<li>7 photographies d’identités ;</li>
<li>1 timbre fiscal de 100 MAD ;</li>
<li>photocopie légalisée d’un justificatif de domicile ;</li>
<li>extrait de casier judiciaire de moins de six mois ;</li>
<li>photocopie légalisée du carnet international de vaccination ;</li>
<li>présentation du passeport en cours de validité.</li></ul>
<p>Attention, le concubinage n’est pas reconnu au Maroc, le conjoint non-salarié peut donc rencontrer des difficultés pour l’obtention d’une carte de séjour.</p>
<p>Les enfants de moins de 16 ans ne sont pas soumis à l’immatriculation. Ceux résidant avec les parents et voyageant seuls doivent fournir une copie légalisée de la carte de séjour des parents lors du passage à la frontière.</p>
<p>Pour exercer une activité professionnelle au Maroc, un étranger doit y être autorisé par les administrations compétentes. <strong>En aucun cas, vous n’êtes autorisé à travailler au Maroc sans permis adéquat.</strong></p>
<p>Les salariés devront avoir un contrat de travail visé par le ministère de l’Emploi ; les professions libérales devront justifier une autorisation d’exercer délivrée par le secrétariat général du gouvernement ; les créateurs de sociétés devront se faire enregistrer au registre du commerce et de la patente.</p>
<p>Le contrat de travail d’un étranger est visé par le Ministère de l’Emploi pour une période déterminée (un à trois ans), le renouvellement sera demandé par l’employeur avant l’expiration du contrat en cours. Le contrat est reconduit par reconduction expresse.</p>
<p>En cas de situation illégale au regard de l’autorisation de séjour, une amende, ainsi qu’une mesure d’expulsion assortie d’une interdiction de séjour sont appliquées. Vérifier que le passeport a été visé par les autorités de police des frontières (mention d’un numéro lors d’une première visite et cachet d’entrée) sous peine de se trouver, à son insu, en séjour illégal.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Section consulaire de l’<a href="http://www.amb-maroc.fr/" class="spip_out" rel="external">ambassade du Maroc à Paris</a>.</p>
<p><i>Mise à jour :  janvier 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-maroc-entree-et-sejour-article-animaux-domestiques.md" title="Animaux domestiques">Animaux domestiques</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-maroc-entree-et-sejour-article-vaccination-110837.md" title="Vaccination">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-maroc-entree-et-sejour-article-demenagement-110836.md" title="Déménagement">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-maroc-entree-et-sejour-article-passeport-visa-permis-de-travail-110835.md" title="Passeport, visa, permis de travail">Passeport, visa, permis de travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/entree-et-sejour/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
