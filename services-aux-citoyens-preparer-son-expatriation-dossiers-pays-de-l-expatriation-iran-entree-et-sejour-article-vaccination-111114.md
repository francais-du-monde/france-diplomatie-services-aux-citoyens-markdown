# Vaccination

<p>Aucune vaccination n’est obligatoire pour se rendre en Iran. Cf. site du <a href="http://www.cimed.org/" class="spip_out" rel="external">CIMED</a>.</p>
<p>Pour des raisons médicales il est conseillé aux adultes de mettre à jour des vaccinations contre la diphtérie, le tétanos, la poliomyélite, la typhoïde (longs séjours), l’hépatite A (à partir de 50 ans, une recherche préalable d’anticorps sérique totaux est justifiée), l’hépatite B (longs séjours et/ou séjours à risques). Les vaccinations recommandées aux enfants par le ministère de la Santé en France sont : B.C.G. et hépatite B dès le 1er mois (longs séjours), rougeole dès l’âge de neuf mois, l’hépatite A (possible à partir d’un an) et la typhoïde (à partir de cinq ans pour les longs séjours).</p>
<p><strong>Vaccinations exigées à l’entrée du pays</strong></p>
<p><strong>D’un point de vue administratif</strong> : un certificat de vaccination antiamarile est exigé à l’entrée du pays pour les voyageurs en provenance d’une zone infectée.</p>
<p><strong>Vaccinations recommandées d’un point de vue médical et basé sur des critères épidémiologiques</strong></p>
<p><strong>Systématiquement</strong> :</p>
<ul class="spip">
<li>Hépatite A* (pour les enfants : à partir de l’âge d’un an).<br class="manualbr">Pour les personnes nées avant 1945, ayant passé leur enfance dans un pays en développement ou ayant des antécédents d’ictère, une recherche préalable d’anticorps sériques (Ig G) peut permettre d’éviter une vaccination inutile.</li>
<li>Mise à jour des vaccinations incluses dans le <strong>Calendrier vaccinal français</strong>.</li></ul>
<p><strong>En fonction de la durée et des modalités du séjour </strong> :</p>
<ul class="spip">
<li>Typhoïde : si le séjour doit se dérouler dans des conditions d’hygiène précaires (pour les enfants : à partir de l’âge de deux ans).</li>
<li>Rage à titre préventif : pour des séjours prolongés en situation d’isolement (pour les enfants : dès qu’ils sont en âge de marcher).</li>
<li>Hépatite B : pour des séjours fréquents ou prolongés.</li></ul>
<p><strong>NB</strong> : Pour les enfants, toutes les vaccinations incluses dans le calendrier vaccinal français devront également être à jour. Dans le cas d’un long séjour les vaccinations contre l’hépatite B et contre la tuberculose sont recommandées dès la naissance et la vaccination anti rougeoleuse, dès l’âge de neuf mois.</p>
<p><i>Mise à jour : février 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/entree-et-sejour/article/vaccination-111114). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
