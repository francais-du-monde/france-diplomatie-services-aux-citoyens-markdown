# Entrée et séjour

<h2 class="rub23526">Passeport, visa, permis de travail</h2>
<p>Quel que soit le motif de votre séjour, renseignez-vous auprès du <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-etrangers-en-france/article/annuaire-des-representations-etrangeres-en-france" class="spip_in">Consulat général d’Haïti à Paris</a> qui vous informera sur la règlementation en matière d’entrée et de séjour en Haïti, règlementation que vous devrez impérativement respecter.</p>
<p>N’hésitez pas à consulter le site du ministère haïtien de l’Intérieur afin de vous familiariser avec les modalités d’entrée et de séjour dans le pays.</p>
<p>Vous pouvez également consulter les informations données à titre indicatif sur le site Internet de l’<a href="http://www.ambafrance-ht.org/conditions-d-entree-et-de-sejour" class="spip_out" rel="external">Ambassade de France en Haïti</a>.</p>
<p>La section consulaire de l’Ambassade de France en Haïti n’est pas l’organisme compétent pour répondre à vos demandes d’information concernant votre séjour en Haïti.</p>
<p><strong>En aucun cas, vous n’êtes autorisé à travailler en Haïti sans permis adéquat.</strong></p>
<p><i>Mise à jour : août 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-haiti-entree-et-sejour-article-animaux-domestiques.md" title="Animaux domestiques">Animaux domestiques</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-haiti-entree-et-sejour-article-vaccination.md" title="Vaccination">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-haiti-entree-et-sejour-article-demenagement-114245.md" title="Déménagement">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-haiti-entree-et-sejour-article-passeport-visa-permis-de-travail.md" title="Passeport, visa, permis de travail">Passeport, visa, permis de travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/entree-et-sejour/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
