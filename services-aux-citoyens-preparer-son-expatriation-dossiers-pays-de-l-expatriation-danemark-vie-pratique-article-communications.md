# Communications

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/vie-pratique/article/communications#sommaire_1">Téléphone – Internet</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/vie-pratique/article/communications#sommaire_2">Poste</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Téléphone – Internet</h3>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Téléphoner depuis la France vers le Danemark <br class="manualbr">00 45 + numéro à 8 chiffres </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Téléphoner depuis le Danemark vers la France <br class="manualbr">00 33 + le numéro d’abonné sans le "0" (soit les 9 derniers chiffres) </p>
<h4 class="spip">Téléphoner gratuitement par Internet </h4>
<p>Il est généralement très avantageux d’utiliser <strong>des logiciels de téléphonie sur IP</strong> (Skype, Google talk, Live messenger, Yahoo ! messenger, etc.) car les tarifs proposés à l’international sont souvent moindre que ceux des opérateurs locaux exploitant des lignes classiques, voire gratuits dans certaines conditions.</p>
<h3 class="spip"><a id="sommaire_2"></a>Poste</h3>
<p><strong>Les tarifs</strong> appliqués au 1er janvier 2014 pour les correspondances entre le Danemark et la France sont ceux en vigueur au sein de l’Union Européenne (ex. 14 DKK jusqu’à 50 g et 26 DKK jusqu’à 100 g, en économique respectivement 11,5 et 22 DKK).</p>
<p>En règle générale, les bureaux de poste sont ouverts de 10h00 à 18h00 en semaine et de 10h00 à 12h00 le samedi.</p>
<p><strong>Les timbres</strong> sont en vente dans les bureaux de poste ainsi que dans la plupart des supermarchés, kiosques à journaux et boutiques de souvenirs.</p>
<p>Sur le site internet des services postaux danois (<a href="http://www.postdanmark.dk/da/privat/Sider/privat.aspx" class="spip_out" rel="external">Postdanmark</a>) vous pouvez accéder au point de vente le plus proche de chez vous en entrant les données suivantes : code postal, adresse ou ville.</p>
<p><strong>Les boîtes à lettres</strong> sont rouges.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/vie-pratique/article/communications). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
