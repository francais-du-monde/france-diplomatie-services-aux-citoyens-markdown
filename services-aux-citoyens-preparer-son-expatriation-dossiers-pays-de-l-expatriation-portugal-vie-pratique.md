# Vie pratique

<h2 class="rub23032">Logement</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/vie-pratique/#sommaire_1">Conditions de location</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/vie-pratique/#sommaire_2">Généralités</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/vie-pratique/#sommaire_3">Où se loger ?</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/vie-pratique/#sommaire_4">Logement temporaire</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Conditions de location</h3>
<p>Le marché locatif est limité et tendu, notamment pour les étrangers. Il faut compter un à deux mois de délai pour la recherche d’un logement. En cas de location par une agence, la commission est payée par le propriétaire.</p>
<p>A Lisbonne, les quartiers résidentiels se trouvent au centre et à l’ouest. A Porto, ils sont situés dans la partie récente de la ville, à l’ouest et sur le front de mer.</p>
<p>Il est plus aisé de trouver un appartement qu’une villa. A Lisbonne, il est possible de trouver une villa dans la banlieue ouest, mais la circulation rend difficile les accès au centre de la capitale. Les studios n’existent quasiment pas. A Porto, les villas sont rares et très chères.</p>
<p>Les charges sont, en principe, payées par le propriétaire. A Porto, cependant, les modalités régissant les relations entre propriétaires et locataires sont parfois incertaines. Les consommations d’eau, d’électricité, de gaz et de chauffage sont imputées au locataire.</p>
<p>Il est hasardeux de se lancer dans un achat immobilier, excepté dans le cas d’un séjour de longue durée.</p>
<p>Dans le domaine de l’accession à la multipropriété dans les résidences balnéaires, plusieurs cas d’escroquerie ont été signalés, notamment en Algarve (d’où la nécessité d’être assisté avant et pendant la transaction par un homme de loi).</p>
<h3 class="spip"><a id="sommaire_2"></a>Généralités</h3>
<p>Le parc immobilier portugais est en majorité composé de constructions vétustes. Les espaces urbains centraux sont particulièrement occupés par ce type d’immeubles âgés, mais qui demeurent en bon état de conservation et bien entretenus.</p>
<p>Toutefois, ces habitations se trouvent loin de certains standards français en termes de chauffage ou d’isolation, les constructions plus récentes offrant plus de satisfaction à ces niveaux.</p>
<h4 class="spip">Electricité</h4>
<p>Le courant est de 220 V. Les prises de courant sont de type européen et ne nécessitent aucun adaptateur.</p>
<h4 class="spip">Electroménager</h4>
<p>Les cuisines sont le plus souvent équipées ; dans le cas contraire, les propriétaires acceptent généralement d’installer les appareils électro-ménagers de base (réfrigérateur, cuisinière et lave-linge) moyennant une légère hausse du loyer. Machine à laver la vaisselle et congélateur sont assez fréquents dans les logements pour expatriés.</p>
<h4 class="spip">Chauffage / climatisation</h4>
<p>Les moyens de chauffage le plus souvent employés sont des appareils individuels électriques, quelquefois des appareils individuels à gaz. Le chauffage central n’existe pas. La climatisation n’est pas nécessaire, mais certains immeubles modernes en sont pourvus.</p>
<h3 class="spip"><a id="sommaire_3"></a>Où se loger ?</h3>
<p>Trouver les informations La plupart des journaux publient des petites annonces de logements à louer (<i>alugam-se</i>). Recherchez toujours les annonces qui portent la référence <i>Mediador Autorizado</i> (intermédiaire autorisé). Le journal <a href="http://aeiou.expresso.pt/" class="spip_out" rel="external">Expresso</a> dispose d’une rubrique <i>imobiliario</i>.</p>
<p>Le site <a href="http://www.livinginlisbon.com/dossiers/content.php" class="spip_out" rel="external">LivingInLisbon</a> vous apportera de nombreuses informations et même consacre un dossier utile "Louer un appartement ou une maison à Lisbonne".</p>
<p>Le site de l’<a href="http://www.visitportugal.com/" class="spip_out" rel="external">Office du tourisme du Portugal</a> fournit aussi des renseignements précieux, et peut être consulté à l’adresse suivante.</p>
<p>Enfin, le site de l’<a href="http://www.embaixada-portugal-fr.org/voyager_loger.htm" class="spip_out" rel="external">ambassade du Portugal en France</a> indique les coordonnées des différents types d’hébergement disponibles.</p>
<p><strong>Louer un appartement ou une maison</strong></p>
<p>Comme en France, le prix de la location est fortement corrélé à l’emplacement du bien, ainsi qu’à son état général et à sa surface habitable. De même, une relation positive existe entre éloignement du centre-ville et baisse des prix. N’hésitez donc pas, si votre priorité est de trouver un logement à loyer peu onéreux et si vous disposez d’un moyen de locomotion, à cibler des zones géographiques détachées des cœurs urbains pour vous installer.</p>
<h4 class="spip">Les baux de location</h4>
<p>Après avoir trouvé le lieu à louer, il reste à conclure le contrat locatif. Il peut vous être demandé de trouver une personne (ou plusieurs) pouvant se porter caution pour vous. Comme en France, cette personne couvrira personnellement tout défaut de paiement du locataire. Il peut aussi vous être réclamé plusieurs mois (2 ou 3) de loyer d’avance, ce qui servira de caution.</p>
<p>Avant la signature du bail, il est conseillé d’établir un état des lieux inventoriant précisément les détails de l’état du logement. Cette vérification n’est pas toujours proposée ou effectuée. Le logement est alors présumé en bon état.</p>
<p>Il convient ensuite de porter une attention particulière aux obligations et devoirs du propriétaire. Ceux-ci sont les suivants :</p>
<ul class="spip">
<li>durant la durée du bail, le propriétaire garantit au locataire la jouissance paisible du bien immeuble faisant l’objet du contrat et ne pourra lui apporter des modifications sans son accord préalable ;</li>
<li>le propriétaire est responsable des vices ou défauts précédant la location ;</li>
<li>il est tenu au paiement des divers impôts immobiliers et taxes administratives, de même que les assurances, sauf si l’inverse est explicitement mentionné dans le bail ;</li>
<li>le propriétaire doit fournir chaque mois une quittance de loyer ;</li>
<li>il lui revient d’entamer l’action d’expulsion en cas de défaut de paiement ;</li>
<li>le propriétaire a le droit, depuis 2012, de procéder à une hausse du loyer décider de manière discrétionnaire. Il est tenu de soumettre son projet d’augmentation à son locataire, avec lequel il devra négocier.</li></ul>
<p>En ce qui concerne le locataire :</p>
<ul class="spip">
<li>payer son loyer aux échéances et sous la forme fixées par le bail ;</li>
<li>il ne pourra modifier le bien qui lui est loué sans le consentement éclairé de son propriétaire ;</li>
<li>il doit prendre soin du bien qui est mis à sa disposition « comme s’il lui appartenait » ;</li>
<li>le locataire est prioritaire pour le rachat de l’appartement qu’il occupe si ce dernier est mis en vente ;</li>
<li>si le bien est vendu à un tiers tout en ayant respecté le droit de préférence du locataire, le nouveau propriétaire pourra dénoncer le contrat de location et obtenir le départ sous 90 jours du locataire ;</li>
<li>si le propriétaire ne remplit pas ses devoirs vis-à-vis de l’entretien du bien ou s’il le modifie sans l’accord du locataire, ce dernier pourra saisir la copropriété ou l’agence de location, et intenter une action en justice si cela s’avère nécessaire pour faire reconnaître ses droits ;</li>
<li>depuis la loi de 2012 modifiant les contrats locatifs au Portugal, le locataire ne peut plus résilier son bail avant le tiers de sa durée prévue initialement. Si toutefois il le fait, il sera tenu de s’acquitter de la différence entre les paiements effectués et ceux manquants jusqu’au tiers de la durée ;</li>
<li>toujours en vertu de la loi de 2012, dans le cas où le propriétaire souhaite augmenter le loyer au cours du bail, le locataire a le droit de négocier cette hausse à la baisse, ou de la refuser.</li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Logement temporaire</h3>
<p>Pour les séjours de courte durée, plusieurs solutions existent. N’hésitez pas à vous renseigner convenablement avant votre départ ou à solliciter l’office du tourisme local une fois arrivé.</p>
<h4 class="spip">Les hôtels</h4>
<p>Très nombreux et de qualité variable selon la ville, l’emplacement et le nombre d’étoiles affiché, il est nécessaire de se renseigner avant l’arrivée sur place de manière à effectuer une sélection préalable et de se prémunir contre les mauvaises surprises. Toutes les chaînes d’hôtellerie françaises et internationales sont présentes dans la majeure partie du pays, ce qui standardise le cadre de comparaison de la qualité des prestations offertes. Les prix, sont généralement moins élevés qu’en France. Comptez à service égal une variation de 5 à 25%.</p>
<h4 class="spip">Les pousadas</h4>
<p>Ce sont de petits établissements qui diffèrent des hôtels par le cadre plus typique qu’ils proposent. En effet, les pousadas se rapprochent des maisons d’hôte françaises. De petite taille, elles proposent des services de qualité pour des prix souvent moins élevés que ceux des hôtels traditionnels. De tout standing, vous pourrez en trouver sur tout le territoire portugais.</p>
<h4 class="spip">Les auberges de jeunesse (pousadas de Juventude)</h4>
<p>Plus économiques, ces établissements proposent un hébergement à petit prix, souvent en chambres communes avec salles d’eaux partagées. Regroupées en une association, les informations les concernant sont disponibles à l’adresse suivante :</p>
<p><a href="http://www.pousadasjuventude.pt/" class="spip_out" rel="external">Movijovem</a><br class="manualbr">Rua Lucio de Azevedo 27<br class="manualbr">Empreendimento Patio Central<br class="manualbr">1600-000 Lisboa<br class="manualbr">Tél : (351 21) 72 32 100<br class="manualbr">Fax : (351 21) 72 32 101</p>
<p><i>Mise à jour : janvier 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-vie-pratique-article-pour-en-savoir-plus-111303.md" title="Pour en savoir plus">Pour en savoir plus</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-vie-pratique-article-environnement-culturel.md" title="Environnement culturel">Environnement culturel</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-vie-pratique-article-loisirs-et-culture-111302.md" title="Loisirs et culture">Loisirs et culture</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-vie-pratique-article-communications.md" title="Communications">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-vie-pratique-article-transports.md" title="Transports">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-vie-pratique-article-cout-de-la-vie-111299.md" title="Coût de la vie">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-vie-pratique-article-scolarisation.md" title="Scolarisation">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-vie-pratique-article-sante-111297.md" title="Santé">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-vie-pratique-article-logement-111296.md" title="Logement">Logement</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/vie-pratique/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
