# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/vie-pratique/article/cout-de-la-vie-114264#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/vie-pratique/article/cout-de-la-vie-114264#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/vie-pratique/article/cout-de-la-vie-114264#sommaire_3">Alimentation</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/vie-pratique/article/cout-de-la-vie-114264#sommaire_4">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>La monnaie nationale d’Haïti est la <strong>gourde</strong> (HTG).</p>
<p>Préoccupée par la volatilité du taux de change observée au cours du deuxième trimestre 2014, la Banque de la République d’Haïti (BRH) a décidé de resserrer sa politique monétaire en relevant les taux des réserves obligatoires et ses taux directeurs.</p>
<p>La BRH est également largement intervenue sur le marché des changes afin de limiter la dépréciation de la gourde. Ainsi, depuis le début de l’exercice, les autorités monétaires ont vendu 18MUSD sur le marché des changes contre 100 MUSD lors de l’exercice fiscal 2012/2013. Cependant, le FMI estime qu’elle n’a plus les moyens de soutenir la gourde.</p>
<p>La Banque centrale va poursuivre sa politique de resserrement monétaire pour stabiliser le taux de change de la monnaie nationale et contenir l’inflation.</p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<p>Les retraits en numéraire auprès des distributeurs automatiques de billets situés dans des lieux publics et les agences bancaires sont à proscrire car certains gangs attendent parfois leur proie à la sortie de ces établissements.</p>
<h3 class="spip"><a id="sommaire_3"></a>Alimentation</h3>
<p><strong>Conditions d’approvisionnement</strong></p>
<p>En dehors de Port-au-Prince, prévoir des réserves (conserves, eau en bouteille, lait stérilisé et produits d’entretien etc.) surtout en période cyclonique.</p>
<p><strong>Prix moyen d’un repas dans un restaurant</strong></p>
<p>Les prix des repas dans les restaurants de standard occidentaux sont comparables, voire plus élevés, que dans les restaurants français de moyen standing (20€ le plat en moyenne). Dans les restaurants haïtiens plus typiques, les plats varient entre 4 et 10€.</p>
<p>Le pourboire représente 10 à 15% du montant de l’addition.</p>
<h3 class="spip"><a id="sommaire_4"></a>Evolution des prix</h3>
<p>Le maintien de la stabilité des prix demeure la préoccupation principale des autorités monétaires haïtiennes. Le taux d’inflation, qui avait atteint un pic de 7,7% (en glissement annuel) à la fin mars 2013, a connu une baisse jusqu’au mois de février 2014 pour atteindre 3,2%. Il est légèrement remonté au mois d’avril 2014, à 3,5% (en glissement annuel). Cette baisse de l’inflation résulte de la relative stabilité des prix sur les marchés international et local grâce à la bonne campagne agricole haïtienne.</p>
<p><strong>Les projections du FMI pour 2014 font état d’une inflation contenue à 4% </strong>notamment grâce au maintien de la stabilité des prix des matières premières sur les marchés internationaux. La période de sècheresse actuelle pourrait entrainer une diminution de l’offre alimentaire locale et une augmentation des prix des produits locaux.</p>
<p><strong>Taux d’inflation mesuré par la variation de l’Indice des Prix à la Consommation.</strong></p>
<table class="spip">
<thead><tr class="row_first"><th id="id175b_c0"> </th><th id="id175b_c1">2010</th><th id="id175b_c2">2011</th><th id="id175b_c3">2012</th><th id="id175b_c4">2013</th><th id="id175b_c5">2014*</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<th headers="id175b_c0" id="id175b_l0">Evolution de l’IPC (en %) en fin d’exercice</th>
<td class="numeric virgule" headers="id175b_c1 id175b_l0">4,7</td>
<td class="numeric virgule" headers="id175b_c2 id175b_l0">10,4</td>
<td class="numeric virgule" headers="id175b_c3 id175b_l0">6,5</td>
<td class="numeric virgule" headers="id175b_c4 id175b_l0">4,5</td>
<td class="numeric " headers="id175b_c5 id175b_l0">5</td></tr>
</tbody>
</table>
<p><i>*Estimation FMI</i>, <i>source : BRH</i></p>
<p><strong>Variation de l’IPC par groupe de biens et services (en %), avril 2014</strong></p>
<table class="spip" summary="{{0,6}} |{{3,5}}">
<thead><tr class="row_first"><th id="id6c10_c0"> </th><th id="id6c10_c1">Pondération </th><th id="id6c10_c2">Variation mensuelle </th><th id="id6c10_c3">Variation glissement annuel </th></tr></thead>
<caption><strong>100,00%</strong></caption>
<tbody>
<tr class="row_odd odd">
<td headers="id6c10_c0">Alimentation, boisson, tabac</td>
<td headers="id6c10_c1">50,35%</td>
<td class="numeric virgule" headers="id6c10_c2">0,8</td>
<td class="numeric virgule" headers="id6c10_c3">2,7</td></tr>
<tr class="row_even even">
<td headers="id6c10_c0">Habillement, tissus et chaussures</td>
<td headers="id6c10_c1">6,86%</td>
<td class="numeric virgule" headers="id6c10_c2">0,6</td>
<td class="numeric virgule" headers="id6c10_c3">6,2</td></tr>
<tr class="row_odd odd">
<td headers="id6c10_c0">Loyer, énergie et eau</td>
<td headers="id6c10_c1">11,05%</td>
<td class="numeric virgule" headers="id6c10_c2">0,1</td>
<td class="numeric virgule" headers="id6c10_c3">4,8</td></tr>
<tr class="row_even even">
<td headers="id6c10_c0">Aménagement et entretien du logement</td>
<td headers="id6c10_c1">4,70%</td>
<td class="numeric virgule" headers="id6c10_c2">0,5</td>
<td class="numeric virgule" headers="id6c10_c3">3,8</td></tr>
<tr class="row_odd odd">
<td headers="id6c10_c0">Santé</td>
<td headers="id6c10_c1">2,90%</td>
<td class="numeric virgule" headers="id6c10_c2">1,5</td>
<td class="numeric virgule" headers="id6c10_c3">6,4</td></tr>
<tr class="row_even even">
<td headers="id6c10_c0">Transport</td>
<td headers="id6c10_c1">13,74%</td>
<td class="numeric virgule" headers="id6c10_c2">0,2</td>
<td class="numeric virgule" headers="id6c10_c3">1,9</td></tr>
<tr class="row_odd odd">
<td headers="id6c10_c0">Loisirs, spectacles, enseignement et culture</td>
<td headers="id6c10_c1">5,84%</td>
<td class="numeric virgule" headers="id6c10_c2">0,2</td>
<td class="numeric virgule" headers="id6c10_c3">4,3</td></tr>
<tr class="row_even even">
<td headers="id6c10_c0">Autres biens et services</td>
<td headers="id6c10_c1">4,56%</td>
<td class="numeric virgule" headers="id6c10_c2">0,7</td>
<td class="numeric virgule" headers="id6c10_c3">5,9</td></tr>
</tbody>
</table>
<p><i>Source : IHSI</i></p>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : août 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/vie-pratique/article/cout-de-la-vie-114264). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
