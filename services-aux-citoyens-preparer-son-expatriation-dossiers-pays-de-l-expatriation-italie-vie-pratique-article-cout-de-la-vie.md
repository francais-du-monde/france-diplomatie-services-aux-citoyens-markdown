# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/vie-pratique/article/cout-de-la-vie#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/vie-pratique/article/cout-de-la-vie#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/vie-pratique/article/cout-de-la-vie#sommaire_3">Budget</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/vie-pratique/article/cout-de-la-vie#sommaire_4">Alimentation</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/vie-pratique/article/cout-de-la-vie#sommaire_5">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>L’unité monétaire est l’euro.</p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<p>Il existe une liberté totale en matière de transfert de fonds. De nombreuses banques françaises sont présentes en Italie, mais la plupart n’ont pas de guichets ou de gestion de comptes de particuliers.</p>
<p>Une déclaration à la Banque d’Italie est obligatoire pour le transfert de plus de 10 329€.</p>
<p>Le règlement en espèces est le plus utilisé en Italie. Les chéquiers sont beaucoup moins utilisés qu’en France. La carte Bancomat, délivrée par la banque dans laquelle le compte a été ouvert, est très utilisée. Il s’agit d’une carte nationale de retrait (à débit immédiat), qui peut être utilisée dans les guichets automatiques de l’ensemble du pays. Les principales cartes de crédit utilisées en Italie sont Visa et Mastercard.</p>
<p>Les banques sont ouvertes du lundi au vendredi, de 8h30 à 13h30 et de 14h45 à 15h45. Elles sont fermées le samedi, dimanche et les jours fériés. Les services bancaires italiens sont tous payants (ouverture et clôture de compte, chéquiers, relevés de compte mensuels, retraits par carte bleue effectués dans une autre banque).</p>
<p>L’ouverture d’un compte bancaire s’effectue sur présentation d’une pièce d’identité, d’une preuve de domicile et du code d’identification fiscale. Vous signerez avec votre banque le contratto servizi di banca diretta (convention de compte) détaillant les conditions de fonctionnement et les frais bancaires.</p>
<h3 class="spip"><a id="sommaire_3"></a>Budget</h3>
<p>Le coût de la vie est en Italie est plus ou moins le même qu’en France. Il faut s’attendre à avoir des prix plus élevés dans les grandes villes. Il est à noter que les tarifs d’électricité, de gaz et de téléphone sont plus élevés qu’en France.</p>
<h3 class="spip"><a id="sommaire_4"></a>Alimentation</h3>
<p>Prix moyen d’un repas dans un restaurant</p>
<p><strong>Milan</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td></td>
<td>Euros</td></tr>
<tr class="row_even even">
<td>Restaurant de qualité supérieure</td>
<td>60-80</td></tr>
<tr class="row_odd odd">
<td>Restaurant de qualité moyenne</td>
<td>30</td></tr>
</tbody>
</table>
<p><strong>Rome</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td></td>
<td>Euros</td></tr>
<tr class="row_even even">
<td>Restaurant de qualité supérieure</td>
<td>50-70</td></tr>
<tr class="row_odd odd">
<td>Restaurant de qualité moyenne</td>
<td>20-30</td></tr>
</tbody>
</table>
<p>Le pourboire est conseillé, son montant est de 5% à 10% de l’addition.</p>
<h3 class="spip"><a id="sommaire_5"></a>Evolution des prix</h3>
<p>L’indice d’inflation estimé par l’ISTAT est de 3 % en 2012.</p>
<p><a href="http://www.istat.it/en/" class="spip_out" rel="external">Etude de l’Institut national de statistiques</a> sur l’inflation en 2012.</p>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/vie-pratique/article/cout-de-la-vie). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
