# Animaux domestiques

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/entree-et-sejour/article/animaux-domestiques-110402#sommaire_1">Départ de France</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/entree-et-sejour/article/animaux-domestiques-110402#sommaire_2">Retour en France</a></li></ul>
<p>La nouvelle réglementation indienne relative à l’importation d’animaux domestiques a connu récemment des modifications fondamentales. En effet, depuis le 4 Juillet 2013 :</p>
<ul class="spip">
<li>Ne sont autorisés que deux animaux domestiques par personne ;</li>
<li>L’importation d’animaux domestiques est soumise à un séjour minimum de deux ans dans le pays par le propriétaire. De ce fait sont exclus du dispositif les personnes avec un visa touriste, affaires, et toute autre catégorie de visa qui permet un court séjour ;</li>
<li>L’obtention d’un certificat de non-objection (<i>NOC</i> : <i>No objection certificate</i>) local est requis avant de quitter la France avec les animaux. Ceci implique l’intervention d’un intermédiaire local pour compléter les formalités administratives à la place du propriétaire.</li></ul>
<p>Afin de pouvoir satisfaire aux nouvelles conditions dans les délais, il convient d’entamer la procédure quelques semaines avant son départ.</p>
<p>La procédure d’importation d’un animal domestique comprend :</p>
<h3 class="spip"><a id="sommaire_1"></a>Départ de France</h3>
<h4 class="spip">Documents à fournir pour l’obtention du certificat de non-objection indien</h4>
<ul class="spip">
<li>Une attestation de l’employeur/société indien/ne que l’intéressé vient en Inde pour deux ans ;</li>
<li>Une lettre de l’intéressé demandant autorisation d’amener son animal domestique comme bagage accompagné ;</li>
<li>Un certificat du service vétérinaire dans le pays d’origine, dûment complété et signé. Le nom du propriétaire de l’animal et le nom de l’importateur doit être le même ;</li>
<li>Des informations relatives à la micro puce électronique de l’animal et le carnet de vaccination à jour ;</li>
<li>Le certificat de vaccination contre la rage établi au moins un mois avant l’arrivée en Inde ;</li>
<li>Des photographies de l’animal avec nom, date de naissance,sexe, race et couleur ;</li>
<li>Copie du passeport et du visa du propriétaire de l’animal ;</li>
<li>Un billet d’avion, avec le plan de vol, l’heure et le port d’arrivée en Inde, et le nom de la compagnie aérienne.</li></ul>
<h4 class="spip">Rôle d’un agent local</h4>
<ul class="spip">
<li>Il complète toutes les formalités administratives du côté indien afin d’obtenir le certificat de non-objection du ministère de l’agriculture et les autorités douanières indiens ;</li>
<li>Il transmet une copie du certificat de non-objection par mail/fax au propriétaire avant son départ du pays d’origine ;</li>
<li>Il remet l’original du certificat de non-objection au propriétaire à son arrivée en Inde.</li></ul>
<h4 class="spip">Tarifs des services</h4>
<p> (susceptible d’être modifié par le prestataire)</p>
<ul class="spip">
<li>INR 5000 + taxes par animal domestique importé en Inde <strong>comme bagage accompagné </strong> ;</li>
<li>INR 18000 + taxes + d’autres coûts constatés (bon de livraison de la compagnie aérienne, droits de douane, frais de transport et de manutention) pour chaque animal domestique importé en Inde <strong>en fret cargo</strong>.</li></ul>
<p>Ces frais peuvent varier sensiblement en fonction de la compagnie utilisée pour ces services et en fonction de l’évolution du taux de change INR/€.</p>
<p>NB : Détenteur d’un passeport diplomatique souhaitant importer un animal domestique en frêt cargo devra fournir, en outre, le certificat d’exemption des droits de douane (« <i>Duty Free Exemption Certificate</i> »)</p>
<p><strong>Liste de contacts en Inde : </strong></p>
<p><a href="http://www.clintus.com/" class="spip_out" rel="external">CLINTUS NETWORK LTD</a><br class="manualbr">Khasra n°332, Chattarpur, <br class="manualbr">New Delhi-110074<br class="manualbr">Inde<br class="manualbr">Tel. : + 91-11-26 80 38 76 ou 26 80 38 79 ou 26 80 62 40<br class="manualbr">Fax : + 91-11- 26 80 26 16 ou 26 80 26 31<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/entree-et-sejour/article/animaux-domestiques-110402#clintus#mc#del2.vsnl.net.in#" title="clintus..åt..del2.vsnl.net.in" onclick="location.href=mc_lancerlien('clintus','del2.vsnl.net.in'); return false;" class="spip_mail">Courriel</a></p>
<p><a href="http://www.mithalsindia.com/" class="spip_out" rel="external">MITHALS INTERNATIONAL MOVERS PVT. LTD</a><br class="manualbr">F-3/7 Okhla Industrial Area, Phase I<br class="manualbr">New Delhi 110020<br class="manualbr">Tel : + 11-26-37-16-18/16-19/16-20<br class="manualbr">Cel : + 91-98-11-01-41-10/98-10-02-55-59<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/entree-et-sejour/article/animaux-domestiques-110402#sales#mc#mithalsindia.com#" title="sales..åt..mithalsindia.com" onclick="location.href=mc_lancerlien('sales','mithalsindia.com'); return false;" class="spip_mail">Courriel</a></p>
<h3 class="spip"><a id="sommaire_2"></a>Retour en France</h3>
<h4 class="spip">Documents à fournir pour l’exportation d’un animal depuis l’Inde</h4>
<ul class="spip">
<li>Une lettre d’autorisation en faveur de l’exportateur (la signature doit être la même que celle du passeport) ;</li>
<li>3 copies de facture avec la valeur approximative de l’animal domestique pour la douane (la signature doit être la même que celle du passeport) ;</li>
<li>Une lettre signée de l’intéressé sollicitant l’exportation de l’animal domestique ;</li>
<li>3 connaissements de transport (qui seront remis en mains propres).</li></ul>
<h4 class="spip">Documents complémentaires à fournir</h4>
<ul class="spip">
<li>Le certificat sanitaire de l’animal établi par un vétérinaire local (en original) ;</li>
<li>Le carnet de vaccination à jour (en original) ;</li>
<li>Copie du passeport et du billet d’avion du propriétaire ;</li>
<li>Le certificat de vaccination contre la rage établi au moins un mois avant le départ ;</li>
<li>Des informations relatives à la micro puce électronique de l’animal ;</li>
<li>Des photographies de l’animal avec nom, date de naissance,sexe, race et couleur ;</li>
<li>L’adresse de destination à l’étranger avec un numéro de téléphone joignable 24h/24.</li></ul>
<p>NB : Il revient au propriétaire de se renseigner s’il y a besoin d’un certificat d’importation dans le pays destinataire avant de quitter l’Inde.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/entree-et-sejour/article/animaux-domestiques-110402). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
