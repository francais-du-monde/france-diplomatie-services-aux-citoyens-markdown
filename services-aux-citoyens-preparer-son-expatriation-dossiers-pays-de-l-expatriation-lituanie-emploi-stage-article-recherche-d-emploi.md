# Recherche d’emploi

<h4 class="spip">Outils pour la recherche d’emploi</h4>
<h4 class="spip">Journaux </h4>
<ul class="spip">
<li><a href="http://www.lrytas.lt/" class="spip_out" rel="external">Lietuvos Rytas</a></li>
<li><a href="http://www.respublika.lt/" class="spip_out" rel="external">Respublika</a></li>
<li><a href="http://lzinios.lt/lzinios/index.php" class="spip_out" rel="external">Lietuvos Zinios</a></li>
<li><a href="http://vz.lt/" class="spip_out" rel="external">Verslo Zinios</a></li>
<li><a href="http://www.delfi.lt/" class="spip_out" rel="external">Delfi</a></li></ul>
<h4 class="spip">Sites internet</h4>
<ul class="spip">
<li><a href="http://www.cv.lt/index.do" class="spip_out" rel="external">CV.lt</a></li>
<li><a href="http://www.cvonline.lt/" class="spip_out" rel="external">CVonline.lt</a></li>
<li><a href="http://www.cvinfo.lt/" class="spip_out" rel="external">CVinfo.lt</a></li>
<li><a href="http://www.cvmarket.lt/" class="spip_out" rel="external">CVmarket.lt</a></li>
<li><a href="http://www.darbaslietuvoje.lt/" class="spip_out" rel="external">Darbaslietuvoje.lt</a></li>
<li><a href="http://www.darbo.lt/darbas/" class="spip_out" rel="external">Darbo.lt</a></li></ul>
<h4 class="spip">Organismes pour la recherche d’emploi</h4>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.cci-fr.lt/" class="spip_out" rel="external">Chambre de commerce franco-lituanienne</a> <br class="manualbr">Svarco gatve 1, <br class="manualbr">LT-01130 Vilnius </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.tresor.economie.gouv.fr/SE/LITUANIE/" class="spip_out" rel="external">Service économique de l’Ambassade de France en Lituanie</a><br class="manualbr">Svarco gatve 1, LT-01130 Vilnius<br class="manualbr">Tél. : +370 5 219 96 40<br class="manualbr">Télécopie : +370 5 219 96 47<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/emploi-stage/article/recherche-d-emploi#vilnius#mc#dgtresor.gouv.fr#" title="vilnius..åt..dgtresor.gouv.fr" onclick="location.href=mc_lancerlien('vilnius','dgtresor.gouv.fr'); return false;" class="spip_mail">Courriel</a></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.ldb.lt/en/Information/Pages/default.aspx" class="spip_out" rel="external">Lietuvos darbo birža (agence pour l’emploi)</a> </p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/emploi-stage/article/recherche-d-emploi). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
