# Entrée et séjour

<h2 class="rub22908">Passeport, visa, permis de travail</h2>
<p>Quel que soit le motif de votre séjour, renseignez-vous auprès de la section consulaire de l’ambassade de Tunisie à Paris qui vous informera sur la règlementation en matière d’entrée et de séjour en Tunisie, règlementation que vous devrez impérativement respecter. N’hésitez pas à consulter le site du ministère de l’Intérieur tunisien afin de vous familiariser avec les modalités d’entrée et de séjour dans le pays.</p>
<p><strong>En aucun cas, vous n’êtes autorisé à travailler en Tunisie sans permis adéquat. </strong></p>
<p>Le Consulat de France en Tunisie n’est pas l’organisme compétent pour répondre à vos demandes d’information concernant votre séjour en Tunisie.</p>
<p>Au-delà de trois mois consécutifs de résidence, vous devez solliciter une autorisation de séjour et /ou de travail auprès du ministère tunisien de l’Intérieur (se rendre au bureau de police de son arrondissement) conformément à l’accord bilatéral en matière de séjour et de travail signé à Paris le 17 mars 1988 (modifié en 1991 et 2000). La carte de séjour, renouvelable, a une durée de validité de un an.</p>
<p>Pour l’obtention de la carte de séjour, différentes pièces administratives doivent être fournies : bail de location, contrat de travail visé par le ministère de la Formation professionnelle et de l’Emploi, passeport, deux timbres fiscaux, livret de famille, trois photos d’identité.</p>
<p>Les ressortissants français résidant en Tunisie et justifiant d’un séjour régulier de trois ans ou plus bénéficient de plein droit d’un titre de séjour d’une durée de dix ans valant autorisation de séjourner sur le territoire de la République tunisienne et d’un titre de travail de même durée permettant d’exercer toute profession salariée ou non, y compris commerciale.</p>
<p>Le concubinage n’est pas reconnu par les autorités tunisiennes. L’obtention de la carte de séjour peut donc être difficile pour un concubin sans emploi.</p>
<p><strong>Demande d’autorisation de travail</strong> doit être engagée préalablement à votre arrivée en Tunisie (saisir le Consulat général de Tunisie en France le plus proche de votre lieu de résidence afin de recueillir de plus amples renseignements sur la procédure administrative à suivre).</p>
<p>Les ressortissants français désireux d’exercer une activité professionnelle salariée en Tunisie pour une durée d’un an au minimum reçoivent, après contrôle médical et sur présentation d’un contrat de travail visé par les autorités compétentes, un titre de séjour valable un an renouvelable et portant la mention "salarié".</p>
<p>Les ressortissants français possédant également la nationalité tunisienne doivent impérativement présenter, à l’entrée comme à la sortie de Tunisie, un passeport tunisien en cours de validité. Leur passeport français n’est valable que pour les formalités de police en France.</p>
<p><strong>Pour en savoir plus</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Consulat Général de Tunisie à Paris<br class="manualbr">17, rue Lubeck – 75016 PARIS<br class="manualbr">Tél. : 01.53.70.69.10</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Ministère de l’Intérieur et du développement local</strong><br class="manualbr">Av. Habib Bourguiba -1000 Tunis<br class="manualbr">Tél. : (216) 71 333-000</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Ministère de la Formation professionnelle et de l’Emploi</strong>
10 boulevard Ouled Haffouz
1005 Tunis - Tunisie
Tél. : (216) 71 792-727 / 71.791.331 </p>
<p><i>Mise à jour : décembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-tunisie-entree-et-sejour-article-animaux-domestiques.md" title="Animaux domestiques">Animaux domestiques</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-tunisie-entree-et-sejour-article-vaccination-110514.md" title="Vaccination">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-tunisie-entree-et-sejour-article-demenagement-110513.md" title="Déménagement">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-tunisie-entree-et-sejour-article-passeport-visa-permis-de-travail.md" title="Passeport, visa, permis de travail">Passeport, visa, permis de travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/entree-et-sejour/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
