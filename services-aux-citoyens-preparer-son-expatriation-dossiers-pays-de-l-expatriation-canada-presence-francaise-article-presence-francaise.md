# Présence française

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/presence-francaise/article/presence-francaise#sommaire_1">Ambassade et consulat de France</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/presence-francaise/article/presence-francaise#sommaire_2">Autorités françaises dans le pays</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/presence-francaise/article/presence-francaise#sommaire_3">Associations dans le pays</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/presence-francaise/article/presence-francaise#sommaire_4">Presse française</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Ambassade et consulat de France</h3>
<p>Consulter notre <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/article/annuaire-des-representations-francaises-a-l-etranger" class="spip_in">annuaire des ambassades et consulats de France à l’étranger</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Autorités françaises dans le pays</h3>
<p><strong>Réseau français de coopération et d’action culturelle</strong></p>
<p>Les coordonnées des services du réseau de coopération et d’action culturelle français au Canada ainsi que la description de l’activité de ces services se trouvent dans la rubrique culture du site internet de l’Ambassade ou sur le site internet du réseau culturel : <a href="http://latitudefrance.diplomatie.gouv.fr/AnnuaireCarto.html" class="spip_out" rel="external">Latitude France</a>.</p>
<p><strong>Economie : réseau mondial</strong></p>
<p>Business France, l’Agence française pour le développement international des entreprises, <a href="http://export.businessfrance.fr/canada/export-canada-avec-nos-bureaux.html" class="spip_out" rel="external">est présente au Canada</a>. Sa mission est d’accompagner les entreprises françaises dans leur développement à l’international.</p>
<p>Les services économiques sont également présents. Ils sont une émanation de la Direction générale du Trésor et ont pour missions principales l’analyse macroéconomique et l’animation des relations économiques bilatérales entre la France et les pays où ils sont implantés.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.tresor.economie.gouv.fr/pays/canada" class="spip_out" rel="external">Site internet du service économique français au Canada</a></p>
<p><strong>Vos élus à l’Assemblée des Français de l’étranger</strong></p>
<p>Pour toute information sur l’Assemblée des Français de l’étranger et pour connaître les conseillers et les Sénateurs qui représentent les Français établis hors de France dans votre circonscription, vous pouvez consulter les sites Internet suivants :</p>
<ul class="spip">
<li><a href="http://www.assemblee-afe.fr/" class="spip_out" rel="external">Assemblée des Français de l’étranger (AFE)</a></li>
<li>Sénat au service des Français de l’étranger : <a href="http://www.senat.fr/expatries/dossiers_pays/canada.html" class="spip_out" rel="external">dossier spécifique au Canada sur le site du Sénat</a>.</li></ul>
<p><strong>Députés des Français de l’étranger</strong></p>
<p>La révision constitutionnelle du 23 juillet 2008, publiée au Journal officiel du 19 avril 2011, introduit la représentation à l’Assemblée nationale des Français établis hors de France. La création de onze circonscriptions législatives à l’étranger permet aux Français expatriés d’élire leur député à l’Assemblée nationale à partir du scrutin de 2012.</p>
<p>Pour plus d’information :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.votezaletranger.gouv.fr/" class="spip_out" rel="external">Votez à l’étranger</a> </p>
<h3 class="spip"><a id="sommaire_3"></a>Associations dans le pays</h3>
<ul class="spip">
<li><a href="http://www.ambafrance-ca.org/Associations,18" class="spip_out" rel="external">Liste des associations françaises</a> publiée sur le site de l’Ambassade de France au Canada ;</li>
<li>Notre article dédié aux <a href="services-aux-citoyens-preparer-son-expatriation-en-savoir-plus-article-associations-des-francais-de-l-etranger.md" class="spip_in">Associations des Français de l’étranger</a>.</li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Presse française</h3>
<p>A Montréal et Québec, il est facile d’avoir accès à la presse française. Le réseau des « Maisons de la presse internationale » (LMPI) dispose des principaux titres de la presse hexagonale. A noter que LMPI distribue les titres français dans les villes du Québec dont la population dépasse 2 000 habitants. Il en va de même pour les grands réseaux de librairies que sont Archambault et Renaud-Bray. Les principaux quotidiens nationaux sont généralement disponibles le jour même et ce, dès 18 heures. Vous trouverez également les principaux titres de la presse magazine. Bien entendu, les frais de transport rendent les publications plus onéreuses. Il faudra ainsi compter près de 4 $ pour un quotidien dont le prix est d’un euro en France. Pour avoir accès à des revues plus spécialisées, la Bibliothèque et archives nationales du Québec à Montréal est une bonne alternative. La salle de lecture comprend l’essentiel des titres de la presse française.</p>
<p><strong>Réseau des Alliances françaises au Canada</strong></p>
<p>Le réseau canadien comprend neuf <a href="http://www.af.ca/" class="spip_out" rel="external">Alliances françaises</a> (Ottawa-Gatineau, Calgary, Edmonton, Halifax, Moncton, Toronto, Vancouver, Victoria et Winnipeg) et cinq centres associés.</p>
<p>Certaines de ces alliances disposent d’une bibliothèque où il est possible de consulter, voire d’emprunter, des livres et des magazines en langue française.</p>
<p><i>Mise à jour : avril 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/presence-francaise/article/presence-francaise). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
