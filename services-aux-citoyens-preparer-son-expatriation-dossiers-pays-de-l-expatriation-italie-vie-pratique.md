# Vie pratique

<h2 class="rub22820">Logement</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/vie-pratique/#sommaire_1">Où se loger ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/vie-pratique/#sommaire_2">Conditions de location</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/vie-pratique/#sommaire_3">Gaz et électricité</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/vie-pratique/#sommaire_4">Electroménager</a></li></ul>
<p><strong>Vocabulaire :</strong></p>
<ul class="spip">
<li><strong>louer : </strong>affittare</li>
<li><strong>location : </strong>affitto</li>
<li><strong>à louer : </strong>affittasi</li>
<li><strong>immeuble : </strong>palazzo - stabile</li>
<li><strong>meublé : </strong>arredato</li>
<li><strong>partiellement meublé : </strong>parzialmente arredato</li>
<li><strong>état : </strong>stato</li>
<li><strong>très bon état : </strong>ottimo stato</li>
<li><strong>rénové : </strong>ristrutturato</li>
<li><strong>étage : </strong>piano</li>
<li><strong>rez de chaussée : </strong>piano terra</li>
<li><strong>dernier étage (terrasse)</strong> : attico</li>
<li><strong>chauffage individuel : </strong>riscaldamento autonomo</li>
<li><strong>collectif</strong> : centralizzato</li>
<li><strong>pièces : </strong>locali</li>
<li><strong>deux pièces : </strong>bilocale</li>
<li><strong>studio : </strong>monolocale</li>
<li><strong>cuisine : </strong>cucina</li>
<li><strong>avec coin repas : </strong>abitabile</li>
<li><strong> coin cuisine : </strong>angolo cottura</li>
<li><strong>salle de bain : </strong>bagno</li>
<li><strong>deux salles de bains : </strong>doppi servizi</li>
<li><strong>chambre à coucher : </strong>camera (da letto)</li>
<li><strong>cagibi/débarras : </strong>ripostiglio</li>
<li><strong>charges de copropriété : </strong>spese condominiali</li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Où se loger ?</h3>
<p>Bien que le rythme ait ralenti au cours de ces deux dernières années, le prix des immeubles en Italie continue à augmenter à un taux moyen de 5%.</p>
<p>Il est en général plus facile de trouver des logements meublés que vides, la loi protégeant mieux les propriétaires de meublés en Italie. Les logements à louer (en italien <i>Affittasi</i>) ou à vendre (<i>Vendesi</i>) sont généralement signalés par une affichette placardée sur l’immeuble.</p>
<p><strong>Où chercher ?</strong></p>
<p>Les frais d’agence sont très élevés : dans le cas d’une location 10% du loyer annuel sera à verser au moment de la signature du contrat.</p>
<p><a href="http://www.agenzieimmobiliari.com/" class="spip_out" rel="external">Site de recherche d’agences immobilières dans tout le pays</a></p>
<p>Le quotidien <a href="http://www.ilmessaggero.it/inscasa/casa.htm" class="spip_out" rel="external">Il Messaggero</a> publie chaque samedi son supplément immobilier "Casa". La Stampa, Il Corriere della Sera, il Giornale proposent une rubrique "appartements à louer".</p>
<p>L’hebdomadaire "Solo Case", qui paraît le samedi, est uniquement consacré au marché de l’immobilier, avec un chapitre spécial pour les locations. L’édition "Affiti di Solo Case" du mardi sort avec des annonces supplémentaires.</p>
<p>Les revues comme "Wanted in Rome" (bimensuel en anglais distribué dans les kiosques du centre ville) possèdent une rubrique immobilière. Les annonces des revues et quotidiens sont souvent les mêmes que celles des agences immobilières et sont classées par quartier.</p>
<p>A Florence, le journal "La Pulce" paraît trois fois par semaine (lundi, mercredi , vendredi). Les quotidiens "La Nazione" + supplément "Casa" (jeudi et dimanche) et "La Repubblica" proposent des annonces. "Panorama casa" et "Casa Dove" sont distribués gratuitement chez les marchands de journaux.</p>
<p>Plusieurs sites proposent des annonces :</p>
<ul class="spip">
<li><a href="http://www.secondamano.it/" class="spip_out" rel="external">Secondamano.it</a> (Milan)</li>
<li><a href="http://www.lapulce.it/" class="spip_out" rel="external">Lapulce.it</a> (Florence)</li>
<li><a href="http://www.portaportese.it/" class="spip_out" rel="external">Portaportese.it</a> (Rome)</li>
<li><a href="http://www.solocase.it/" class="spip_out" rel="external">Solocase.it</a></li>
<li><a href="http://www.caseonline.it/" class="spip_out" rel="external">Caseonline.it</a></li>
<li><a href="http://www.casa.it/" class="spip_out" rel="external">Casa.it</a></li>
<li><a href="http://www.ecasa.it/" class="spip_out" rel="external">Ecasa.it</a></li>
<li><a href="http://www.coinquilini.it/" class="spip_out" rel="external">Coinquilini.it</a> (site de colocation)</li></ul>
<p>Les étudiants peuvent consulter le site <a href="http://www.affitti-studenti.it/" class="spip_out" rel="external">Affitti-studenti.it</a> ou se renseigner auprès des cités universitaires, même si les places ne sont pas très nombreuses. L’Institut culturel italien de Paris dispose de la liste de ces établissements.</p>
<p><strong>Types d’hébergement</strong></p>
<p>Il est possible de loger en auberge de jeunesse, généralement bien entretenues et d’un bon rapport qualité-prix, à condition de posséder une carte de membre s’achetant auprès de n’importe quelle auberge et de réserver à l’avance en haute saison :</p>
<ul class="spip">
<li><a href="http://www.hostelsclub.com/youth-hostels-it-190-Rome.html" class="spip_out" rel="external">Hostelsclub.com</a></li>
<li><a href="http://www.hostelscentral.com/citta-133.html" class="spip_out" rel="external">Hostelscentral.com</a></li>
<li><a href="http://it.hostelbookers.com/ostelli/italia/roma/" class="spip_out" rel="external">Hostelbookers.com</a></li></ul>
<p>Il est possible de loger chez l’habitant grâce à l’organisme <a href="http://www.bbitalia.it/" class="spip_out" rel="external">Bed &amp; Breakfast Italia</a>.</p>
<p>L’hôtellerie italienne est relativement chère, surtout en été et dans les grandes villes. Le nombre d’étoiles n’a pas la même valeur qu’en France : un hôtel trois étoiles italien en vaudrait deux en France. La "lista degli alberghi" est disponible auprès des offices de tourisme.</p>
<p>Cette formule, qui peut paraître insolite au premier abord, vous garantira calme et propreté, à condition de respecter les occupants des lieux. Cependant, le rapport qualité-prix n’est plus aussi intéressant qu’auparavant. Le logement est proposé dans des dortoirs ou des chambres individuelles ou doubles.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p>Pour avoir des informations concernant le logement sur une ville précise, se connecter au site Internet de la ville en suivant le modèle suivant :</p>
<ul class="spip">
<li><a href="http://www.comune.nomdelaville.it" class="spip_url spip_out auto" rel="nofollow external">www.comune.nomdelaville.it</a> (ex. : <a href="http://www.comune.torino.it/" class="spip_url spip_out auto" rel="nofollow external">www.comune.torino.it/</a>  indice A-Z  case)</li>
<li><a href="http://www.enit.it/" class="spip_out" rel="external">Offices de tourisme en Italie</a></li>
<li><a href="http://www.ambafrance-it.org/" class="spip_out" rel="external">Site de l’Ambassade de France en Italie</a></li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Conditions de location</h3>
<p>L’usage en Italie est de verser au moment de la signature du bail une garantie locative équivalente à trois mois de loyer d’avance. La caution est placée sur un compte bancaire et doit être restituée, entièrement ou partiellement, selon les cas, au moment du départ.</p>
<p>Le contrat de location doit mentionner la durée, le loyer mensuel de location, l’obligation de préavis en cas de révocation du contrat, les obligations relatives aux charges d’entretien ordinaire et extraordinaire de la maison. Les charges sont en supplément du loyer. La durée du bail est généralement de quatre ans, reconductible une fois. La loi n°431 du 9 décembre 1998 régit les contrats de location immobilière et prévoit les règles encadrant les relations entre le propriétaire et le locataire. Il existe trois principaux contrats de location d’immeubles à usage résidentiel en Italie :</p>
<ul class="spip">
<li>Les contrats à usage résidentiel dont la durée est de quatre ans, renouvelable une fois ;</li>
<li>Les contrats à usage transitoire dont la durée est comprise entre un et dix-huit mois, non renouvelable, et qui ne peuvent être conclus que dans certains cas (nécessité du propriétaire ou CDD de la part du locataire) ;</li>
<li>Les contrats destinés aux étudiants universitaires dont la durée est comprise entre 6 et 36 mois ;</li></ul>
<p>Les aides au logement n’existent pas.</p>
<p>Il est conseillé de procéder à l’enregistrement du bail auprès de autorités fiscales (l’Ufficio del registro /bureau du registre foncier) dans les 20 jours qui suivent la conclusion du contrat : les droits d’enregistrement s’élèvent à 2% du loyer par an et sont payables pour moitié par le locataire, pour moitié par le propriétaire. Ce dernier doit obligatoirement faire parvenir au locataire un contrat portant mention des droits payés. Chaque année, l’enregistrement doit être renouvelé. Si le contrat n’est pas enregistré, il sera impossible, tant pour le propriétaire que pour le locataire, de bénéficier des aides et déductions fiscales prévues par la loi.</p>
<p>En cas de difficulté avec les propriétaires, il est possible de faire appel au syndicat des locataires S.U.N.I.A (Sindacato Unitario Nazionale Inquilini e Assegnatari - Syndicat unitaire national des locataires et des bénéficiaires) dont le siège se trouve via Galilei, 55 à Rome. Moyennant une cotisation annuelle, cet organisme peut vous aider dans vos démarches.</p>
<h4 class="spip">Signer un bail</h4>
<p>Une caution équivalente à deux ou trois mois de loyer sera demandée.</p>
<p>Le bail de location est soumis à des <strong>droits d’enregistrement</strong> normalement payables pour moitié par le locataire et pour moitié par le propriétaire. Si votre propriétaire a choisi l’option fiscale « <strong>CEDOLA SECCA</strong> », le bail est exempté d’office des droits d’enregistrement et vous n’avez aucune démarche à faire.</p>
<p><strong>Pour en savoir plus sur les conditions de location :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  le <a href="http://no-lavoronero.cgil.it/index-fr.htm" class="spip_out" rel="external">syndicat italien CGIL</a> met en ligne sur son site un guide (en français) pour les locations pour les citoyens italiens et les étrangers.</p>
<h5 class="spip">Rome</h5>
<p><strong>Le marché immobilier a connu un relâchement en 2012 en raison de la crise économique. Néanmoins, les quartiers de centre-ville et les plus recherchés en général ne connaissent pas de réelles baisses des prix du foncier.</strong></p>
<p>On constate un maintien des prix des loyers résidentiels dans la capitale, voire une poursuite des hausses dans les quartiers centraux de la ville.</p>
<p>Les quartiers résidentiels se situent à Prati, Parioli, Porta Pia, Aventino, Camilluccia et dans certains quartiers du centre historique (Via Veneto, quartier de Pincio). Les charges sont généralement exclues du prix du loyer, à celles-ci s’ajoutent les charges de copropriété (qui comprennent parfois le chauffage et l’eau). Certains appartements possèdent un système de climatisation. Les charges, notamment d’électricité sont plus élevées qu’en France.</p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Quartiers</td>
<td>Prix du m2/mois</td>
<td>Studio (50m2)</td>
<td>3 pièces (90m2)</td>
<td>5 pièces (110 m2)</td></tr>
<tr class="row_even even">
<td>Centre (Place Navone)</td>
<td>42,30 €</td>
<td>2112 €</td>
<td>3802 €</td>
<td>4647 €</td></tr>
<tr class="row_odd odd">
<td>Quartier résidentiel (Parioli)</td>
<td>27 €</td>
<td>1350 €</td>
<td>2430 €</td>
<td>2970 €</td></tr>
<tr class="row_even even">
<td>Banlieue (Trionfale1)</td>
<td>19,70 €</td>
<td>982 €</td>
<td>1768 €</td>
<td>2161 €</td></tr>
</tbody>
</table>
<h5 class="spip">Naples</h5>
<p>Le plus souvent les propriétaires proposent des locations de gré à gré sans contrat ou avec des contrats ne reflétant pas les conditions réelles. Deux mois de caution sont exigés. Généralement les baux sont signés pour des périodes allant de un à quatre ans.</p>
<p>Il est difficile de louer par le biais des agences immobilières. Le plus souvent c’est par le bouche à oreille que les ressortissants français trouvent à se loger dans cette ville.</p>
<p>En cas de location par une agence, la commission est de 10% du loyer annuel ou deux mois de loyer.</p>
<p>D’une manière générale, l’eau, l’électricité et le gaz font l’objet de factures individuelles qui s’ajoutent aux charges "condominiales". Le chauffage et la climatisation sont rarement collectifs. Il faut compter entre 150€ et 350€ par mois de charges, selon la qualité et les dimensions de l’appartement loué.</p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Quartiers</td>
<td>studio</td>
<td>3 pièces</td>
<td>5 pièces</td></tr>
<tr class="row_even even">
<td>Quartier résidentiel</td>
<td>1200</td>
<td>2500</td>
<td>3500</td></tr>
<tr class="row_odd odd">
<td>Banlieue</td>
<td>-</td>
<td>650</td>
<td>800</td></tr>
</tbody>
</table>
<h5 class="spip">Milan</h5>
<p>Le marché locatif est tendu à Milan et dans sa proche banlieue, ne permettant pas de véritable discussion sur le montant des loyers dont la variation est assez faible entre les différents quartiers de la ville. Veillez à ne pas entreprendre vos recherches de logement au mois d’août (tout est fermé).</p>
<p>Les quartiers résidentiels se situent dans le centre ville (Duomo, Corso Sempione), l’ouest et le sud-ouest.</p>
<p>Il est possible de trouver des villas en banlieue. Les appartements sont généralement dépourvus de cuisine équipée, souvent même l’évier peut manquer. Milan étant une ville universitaire, certains meublés sont le plus souvent aménagés pour des étudiants.</p>
<p>Le loyer est payable pat trimestre d’avance, avec un dépôt de garantie de trois mois de loyer hors charges, placé sur un compte bloqué rémunéré au profit du locataire. Les agences immobilières sont très chères (jusqu’à trois mois de loyer). Pour une place de parking, prévoir un supplément mensuel de 150 à 250 €.</p>
<p>Les charges sont très variables en fonction du standing du logement et du niveau des prestations assurées. Elles comprennent généralement l’eau froide, l’entretien des parties communes, le gardiennage (coût élevé), le chauffage collectif (coût élevé). Les charges en gaz et en électricité sont plus élevées qu’en France (prévoir entre 150 et 500€/mois). En été un système de climatisation peut s’avérer nécessaire.</p>
<h5 class="spip">Turin</h5>
<p>Les baux sont en général de trois à quatre ans avec versement d’une caution de deux à trois mois. La commission des agences immobilières oscille entre un et deux mois de loyer.</p>
<p>Les appartements peuvent être livrés sans évier, et en général ne disposent pas de meubles de cuisine et de placards. Il arrive que leur état nécessite d’importants travaux et les frais de remise en état (peinture, sols, sanitaires, électricité) sont, le plus souvent, à la charge des nouveaux locataires.</p>
<p>Les loyers ont quasiment doublé en quatre ans.</p>
<p>Les charges de copropriété, incluant l’eau, sont généralement élevées, sans compter d’importantes charges de chauffage, généralement collectif, six à huit mois par an.</p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Quartier</td>
<td>Studio</td>
<td>3 pièces 60m2</td>
<td>5 pièces 80-110m</td>
<td>Villa</td></tr>
<tr class="row_even even">
<td>Quartier résidentiel</td>
<td>650€</td>
<td>800 - 900€</td>
<td>2000€</td>
<td>3000-3500€</td></tr>
<tr class="row_odd odd">
<td>Banlieue</td>
<td>400€</td>
<td>700€</td>
<td>1100€</td>
<td>2000-2500€</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_3"></a>Gaz et électricité</h3>
<p>Le voltage est 220 V, la fréquence 50 Hz, les prises doubles sont similaires à celles utilisées en France ; les prises triples nécessitent l’usage d’un adaptateur. Le tarif de l’électricité en Italie est l’un des plus élevés d’Europe.</p>
<p><a href="http://www.aceaelectrabel.it" class="spip_out" rel="external">AceaElectrabel</a> assure la fourniture d’électricité dans Rome. Dans les autres villes, la fourniture d’électricité est assurée par <a href="http://www.enel.it/" class="spip_out" rel="external">Enel</a>.</p>
<p>Ci-après une liste, non exhaustive, des principaux fournisseurs :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>ENI</strong> (ITALGAS) - gaz et/ou électricité<br class="manualbr">n° vert 800 900 700<br class="manualbr">Via Ostiense, 72</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.aceaspa.it/Home.aspx" class="spip_out" rel="external">ACEA</a> - gaz et/ou électricité<br class="manualbr">Piazzale Ostiense 2<br class="manualbr">Tél. : 800 13 03 30 du lundi au vendredi de 8h30 à 19h00, ou 0657991</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="https://www.enel.it/it-it" class="spip_out" rel="external">ENEL</a> - gaz et/ou électricité<br class="manualbr">n° vert 800 90 08 60</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.edison.it/" class="spip_out" rel="external">Edison</a> (filiale d’EDF) - gaz et/ou électricité<br class="manualbr">n° vert 800 14 14 14</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.sorgenia.it/?bcsi_scan_96404f7f6439614d=KLA5P/+GuRy9g3yO/Ts+7AxXnXwFAAAAJESkCQ==:1" class="spip_out" rel="external">Sorgenia</a> - gaz et/ou électricité<br class="manualbr">n° vert 800 92 09 20</p>
<p>Attention : les numéros verts ne peuvent pas être contactés depuis l’étranger.</p>
<p><strong>Pour en savoir plus</strong> :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.developpement-durable.gouv.fr/IMG/pdf/LPS36.pdf" class="spip_out" rel="external">prix de l’électricité en Europe</a></p>
<h3 class="spip"><a id="sommaire_4"></a>Electroménager</h3>
<p>A Rome comme à Turin, les cuisines sont très rarement équipées, l’évier peut même parfois manquer dans certains appartements en location. Les appartements anciens, même réhabilités récemment ne comportent que rarement des placards ou des rangements intérieurs.</p>
<p>En outre, même si la plupart des immeubles locatifs sont dotés de caves et parfois de garages, les propriétaires hésitent à les inclure dans les locations. A Milan et à Naples l’équipement électroménager commence à faire son apparition dans les appartements proposés à la location.</p>
<h4 class="spip">Chauffage et climatisation</h4>
<p>Le chauffage, collectif en général, est au gaz, au fioul, ou au méthane. La climatisation est recommandée dans les villes du sud.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-italie-vie-pratique-article-loisirs-et-culture-109952.md" title="Loisirs et culture">Loisirs et culture</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-italie-vie-pratique-article-communications-109951.md" title="Communications">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-italie-vie-pratique-article-transports-109950.md" title="Transports">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-italie-vie-pratique-article-cout-de-la-vie.md" title="Coût de la vie">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-italie-vie-pratique-article-scolarisation.md" title="Scolarisation">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-italie-vie-pratique-article-sante.md" title="Santé">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-italie-vie-pratique-article-logement.md" title="Logement">Logement</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/vie-pratique/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
