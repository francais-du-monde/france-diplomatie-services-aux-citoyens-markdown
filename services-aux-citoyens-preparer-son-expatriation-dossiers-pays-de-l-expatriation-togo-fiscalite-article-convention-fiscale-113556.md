# Convention fiscale

<p>La France et le Togo ont signé, le 24 novembre 1971 une <a href="http://www.impots.gouv.fr/portal/deploiement/p1/fichedescriptive_1969/fichedescriptive_1969.pdf" class="spip_out" rel="external">Convention en matière de fiscalité</a> publiée au Journal Officiel le 6 août 1975 (n°75-698).</p>
<p>Cette Convention tend à éviter les doubles impositions en matière d’impôts sur le revenu, sur les successions, les droits d’enregistrement et droits de timbre qui pourraient résulter de l’application des législations de ces deux Etats.</p>
<p>Pour se procurer le texte intégral de la convention passée entre le Togo et la France reportez-vous aux adresses suivantes :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Journaux officiels <br class="manualbr">26, rue Desaix <br class="manualbr">75727 Paris Cedex 15.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/fiscalite/article/convention-fiscale-113556). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
