# Comment faire une demande de bourse scolaire à l’étranger ?

<p>Gérer la politique d’aide à la scolarité au bénéfice des enfants français résidant avec leur famille à l’étranger fait partie des missions de <a href="http://www.aefe.fr/" class="spip_out" rel="external">l’Agence pour l’enseignement français à l’étranger (AEFE)</a>. Cette aide repose un dispositif de bourses scolaires sous conditions de ressources.</p>
<p>Les conditions d’attribution de ces bourses sont les suivantes :</p>
<ul class="spip">
<li>l’enfant doit posséder la nationalité française ;</li>
<li>il doit résider avec sa famille dans le pays où est situé l’établissement fréquenté ;</li>
<li>il doit être inscrit, ainsi que le parent demandeur de la bourse, au registre des Français établis hors de France tenu par le consulat de son lieu de résidence ;</li>
<li>l’enfant doit être âgé d’au moins trois ans au cours de l’année civile de la rentrée scolaire ;</li>
<li>l’enfant doit fréquenter un établissement homologué par le ministère de l’Éducation nationale ou, à titre dérogatoire, un établissement dispensant au moins 50 % d’enseignement français en cas d’absence, d’éloignement ou de capacité d’accueil insuffisante d’un établissement homologué ;</li>
<li>l’enfant ne doit pas avoir accumulé un retard scolaire de plus de deux ans au-delà de l’âge de scolarisation obligatoire fixé à 16 ans</li></ul>
<p>La demande de bourse doit être déposée au <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/article/annuaire-des-representations-francaises-a-l-etranger" class="spip_in">consulat</a> du lieu de résidence dans les délais fixés par le consulat. Le dossier est tout d’abord examiné par une commission locale des bourses, présidée par le chef de poste diplomatique ou consulaire, qui transmet ses propositions à l’Agence pour l’enseignement français à l’étranger. La décision finale est prise par l’AEFE après avis de la commission nationale des bourses scolaires présidée par le directeur de l’Agence.</p>
<p><strong>À noter que les bourses attribuées dans les établissements scolaires en France ne sont pas transférables à l’étranger.</strong></p>
<p><strong>La demande de bourses ne vaut que pour l’année scolaire en cours. Elle doit donc être renouvelée chaque année. En outre, elle est indépendante de la procédure d’inscription de l’élève dans l’établissement scolaire lui-même.</strong></p>
<p>Sur le site de l’AEFE, <a href="http://www.aefe.fr/tous-publics/bourses/dispositifs-pour-les-eleves-francais/bourses-scolaires" class="spip_out" rel="external">voir l’article Bourses scolaires et ses documents à télécharger</a>.</p>
<p><i>Mise à jour :  juillet 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/scolarite-en-francais/article/aide-a-la-scolarite). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
