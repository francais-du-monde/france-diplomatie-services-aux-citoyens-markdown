# Recherche d’emploi

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/emploi-stage/article/recherche-d-emploi-111385#sommaire_1">Outils pour la recherche d’emploi</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/emploi-stage/article/recherche-d-emploi-111385#sommaire_2">Organismes pour la recherche d’emploi</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Outils pour la recherche d’emploi</h3>
<h4 class="spip">Journaux</h4>
<p>Kurier, Standard, Die Presse, Salzburger Nachrichten, Oberösterreichische Nachrichten etc. Les journaux locaux dans les villes de province, journaux gratuits des agences pour l’emploi régionales « Stellenangebote Arbeitsmarktservice ».</p>
<p>Les suppléments emploi sont publiés le samedi sauf pour le Wiener Zeitung (mercredi).</p>
<h4 class="spip">Sites internet</h4>
<ul class="spip">
<li><a href="http://derstandard.at/karriere" class="spip_out" rel="external">Der Karrieren-Standard</a></li>
<li><a href="http://www.kurier.at/" class="spip_out" rel="external">Kurier</a> (Anzeigen)</li>
<li><a href="http://www.wienerzeitung.at/" class="spip_out" rel="external">Wiener Zeitung</a> (offres d’emploi des services publics, ministères, universités etc.)</li>
<li><a href="http://www.vn.vol.at/" class="spip_out" rel="external">Vorarlberger Nachrichten</a> (Anzeigen)</li>
<li><a href="http://www.krone.at/" class="spip_out" rel="external">Kronen Zeitung</a></li>
<li><a href="http://www.salzburg.com/" class="spip_out" rel="external">Salzburger Nachrichten</a></li>
<li><a href="http://www.oscars.li/" class="spip_out" rel="external">Oscar´s Job-Magazin</a> - Tourismusjobs weltweit</li>
<li><a href="http://www.careerjet.at/" class="spip_out" rel="external">Carrerjet</a></li></ul>
<h5 class="spip">Sites emploi en général </h5>
<p>(penser à rechercher par mot clé « französisch » par exemple)</p>
<ul class="spip">
<li><a href="http://www.ams.or.at/" class="spip_out" rel="external">Agence autrichienne pour l’emploi (AMS)</a> (consulter <a href="http://jobroom.ams.or.at/jobroom/" class="spip_out" rel="external">e-jobroom</a>)</li>
<li><a href="http://www.jobnews.at/" class="spip_out" rel="external">Personalberater-Übersicht</a></li>
<li><a href="http://www.stepstone.at/" class="spip_out" rel="external">Stepstone.at</a></li>
<li><a href="http://www.jobpilot.at/" class="spip_out" rel="external">Jobs  Adverts</a></li>
<li><a href="http://www.adecco.at/" class="spip_out" rel="external">Adecco Personaldienstleistungen</a></li>
<li><a href="http://www.manpower.at/" class="spip_out" rel="external">Manpower - Zeitarbeit</a></li></ul>
<h5 class="spip">Sites d’emploi pour les étudiants / emplois divers</h5>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.jobwohnen.at/" class="spip_out" rel="external">Hochschülerschaft der Uni Wien</a></p>
<h5 class="spip">Sites d’emploi pour les juristes</h5>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.zbp.at/" class="spip_out" rel="external">Centre emploi de la Wirtschaftsuni</a> / Zentrum für Berufsplanung WU-Wien </p>
<h5 class="spip">Sites d’emploi pour les femmes </h5>
<ul class="spip">
<li><a href="http://www.powerfrauen.at/" class="spip_out" rel="external">Powerfrauen.at</a></li>
<li><a href="http://www.hofmann-personal.at/" class="spip_out" rel="external">Hofmann Personalleasing</a></li>
<li><a href="http://www.catro.com/" class="spip_out" rel="external">Catro - Personalsuche und - auswahl GmbH</a></li>
<li><a href="http://www.jobbox.at/" class="spip_out" rel="external">EBK Stellenvermittlung</a></li>
<li><a href="http://www.wbg.at/" class="spip_out" rel="external">WBG Wirtschaftsberatungsgesellschaft</a></li>
<li><a href="http://www.trenkwalder.com/" class="spip_out" rel="external">Trenkwalder Personaldienste</a></li>
<li><a href="http://www.dis-ag.at/" class="spip_out" rel="external">DIS AG - Personaldienstleistungen</a></li>
<li><a href="http://www.step.at/" class="spip_out" rel="external">Step - Personalbereitstellung</a></li>
<li><a href="http://www.tti.at/" class="spip_out" rel="external">T.T.I. Personaldienstleistungen</a></li>
<li><a href="http://www.fabian.at/" class="spip_out" rel="external">Fabian Personalberatung - Jobs in NÖ</a></li>
<li><a href="http://www.bueroring.at/" class="spip_out" rel="external">Büroring</a></li>
<li><a href="http://www.ago.at/" class="spip_out" rel="external">Akad. Gästedienst - Personalbereitstellung</a></li>
<li><a href="http://www.workshop.at/" class="spip_out" rel="external">Workshop Arbeitsvermittlung</a></li>
<li><a href="http://www.programmierfabrik.at/" class="spip_out" rel="external">Jobs in der IT-Branche</a></li></ul>
<h5 class="spip">Sites d’emploi pour l’hôtellerie-restauration </h5>
<p>(<i>Hotel- und Gastronomiejobs, GastroHelp - Stellenanzeiger für das Gastgewerbe, Gastro JobsTeamwork one - Stellen in Hotellerie, Gastronomie,Touris mus, Jobbörse f. Hotellerie, Gastronomie und Touristik</i>)</p>
<ul class="spip">
<li><a href="http://www.gastrojobs.com/" class="spip_out" rel="external">Gastrojobs.com</a></li>
<li><a href="http://www.hogastjob.com/" class="spip_out" rel="external">Hogastjob.com</a></li>
<li><a href="http://www.teamwork-one.at/" class="spip_out" rel="external">Teamwork-one.at</a></li>
<li><a href="http://www.hotel-career.at/" class="spip_out" rel="external">Hotel-career.at</a></li>
<li><a href="http://www.rollingpin.at/" class="spip_out" rel="external">Internationale Gastronomiejobs über ROLLING PIN</a></li></ul>
<h5 class="spip">Bourses d’emplois d’entreprises</h5>
<ul class="spip">
<li><a href="http://www.beko.at/" class="spip_out" rel="external">BEKO</a></li>
<li><a href="http://www.spar.at/" class="spip_out" rel="external">Spar Jobbörse</a></li>
<li><a href="http://www.rewe-group.at/" class="spip_out" rel="external">Jobbörse BML-Konzern (Billa, Bipa, etc.)</a></li>
<li><a href="http://www.siemens.at/" class="spip_out" rel="external">Siemens</a></li>
<li><a href="http://www.philips.at/" class="spip_out" rel="external">Philips</a></li>
<li><a href="http://www.unilever.at/" class="spip_out" rel="external">Unilever</a> (Karriere)</li>
<li><a href="http://www.accor.at/" class="spip_out" rel="external">Gastronomiejobs der ACCOR Hotelkette</a></li></ul>
<h5 class="spip">Autres bourses d’emplois</h5>
<ul class="spip">
<li>Pour les médecins : <a href="http://www.arztjobs.at/" class="spip_out" rel="external">Arztjobs.at</a> et <a href="http://www.jobboersemedizin.at/" class="spip_out" rel="external">Jobboersemedizin.at</a></li>
<li><a href="http://www.jobsearch.at/" class="spip_out" rel="external">Jobsearch</a></li>
<li><a href="http://www.jobcenter.at/" class="spip_out" rel="external">Jobcenter</a> (liens vers divers sites d’offres d’emploi entre autres)</li></ul>
<h5 class="spip">Pour rechercher des adresses d’entreprises autrichiennes</h5>
<ul class="spip">
<li><a href="http://www.gelbeseiten.at/" class="spip_out" rel="external">Gelbeseiten.at</a></li>
<li><a href="http://www.compnet.at/" class="spip_out" rel="external">Compnet.at</a></li>
<li>avec filiale en France <a href="http://www.wk.or.at/" class="spip_out" rel="external">Wk.or.at</a> (présence de l’Autriche en France, carnet d’adresses)</li></ul>
<h5 class="spip">La formation</h5>
<ul class="spip">
<li><a href="http://www.weiterbildung.at/" class="spip_out" rel="external">Weiterbildung.at</a></li>
<li><a href="http://www.waff.at/" class="spip_out" rel="external">Waff.at</a></li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Organismes pour la recherche d’emploi</h3>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.ams.at/" class="spip_out" rel="external">Agence pour l’emploi autrichienne</a> </p>
<h4 class="spip">Dispositif EFA - emploi francophone en Autriche</h4>
<p>Ce dispositif a été crée en 2009 par la Chambre de commerce franco-autrichienne (CCFA) et <a href="http://www.servithink-hr.at/index.php?lang=fr" class="spip_out" rel="external">Servithink Personalmanagement</a>, cabinet de conseil en recrutement, pour répondre aux besoins des candidats et des entreprises françaises ou francophones.</p>
<p>Pour accéder aux prestations gratuites d’EFA, il convient d’envoyer tout d’abord votre CV par <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/emploi-stage/article/recherche-d-emploi-111385#office#mc#servithink-pm.at#" title="office..åt..servithink-pm.at" onclick="location.href=mc_lancerlien('office','servithink-pm.at'); return false;" class="spip_mail">courriel</a>.</p>
<p>Un consultant de <a href="http://www.servithink-hr.at/index.php?lang=fr" class="spip_out" rel="external">Servithink Personalmanagement</a> vous contactera et vous proposera un entretien. Suite à cet entretien, vous aurez la possibilité de déposer votre CV anonyme sur le site d’efa.</p>
<p>Les entreprises à la recherche de collaborateurs français ont un accès sécurisé à ce site. <a href="http://www.servithink-hr.at/index.php?lang=fr" class="spip_out" rel="external">Servithink Personalmanagement</a> vous contactera, dès qu’une entreprise s’intéresse à vos prestations.</p>
<h4 class="spip">Agences intérimaires et bureaux de placement à Vienne</h4>
<p><a href="http://www.adecco.at/" class="spip_out" rel="external">ADECCO</a><br class="manualbr">Mariahilferstraße 123/6. Stock, 1060 Wien<br class="manualbr">Plusieurs filiales dans toute l’Autriche<br class="manualbr">Tél. : 059911.200.00</p>
<p><strong>ADECCO hôtellerie-catering</strong><br class="manualbr">Schubertring 6, 1010 Wien<br class="manualbr">Tél. : 059911 202.00</p>
<p><a href="http://www.ago.at/" class="spip_out" rel="external">Akademischer Gästedienst in Österreich Gesellschaft m.b.H.</a> <br class="manualbr">Wiedner Hauptstraße 51, 1040 Wien<br class="manualbr">Tél. : 01 503 66 00<br class="manualbr">Fax : 01 503 66 10</p>
<p><a href="http://www.avm-personal.at/" class="spip_out" rel="external">AVM - Arbeitskräfteüberlassungs- u. Handelsgesellschaft m.b.H.</a><br class="manualbr">Donaufelder Straße 63,1210 Wien <br class="manualbr">Hosnedlgasse 17, 1220 Wien <br class="manualbr">Tél. : 01 278 20 30<br class="manualbr">Fax : 01 278 20 30 20</p>
<p><a href="http://www.bueroring.at" class="spip_out" rel="external">BÜRORING Personalbereitstellung GmbH</a><br class="manualbr">Alserbachstraße 5/5, 1090 Wien<br class="manualbr">Tél. : 01 317 03 71 <br class="manualbr">Fax : 01 317 03 71 13</p>
<p><a href="http://www.startpeople.at/" class="spip_out" rel="external">STARTPEOPLE</a> <br class="manualbr">Plusieurs filiales dans toute l’Autriche<br class="manualbr">Tél. : 01 / 524 55 01</p>
<p><a href="http://www.eurojobs.at/" class="spip_out" rel="external">EUROJOBS Gmbh</a> <br class="manualbr">Siebensterngasse 21, 1070 Wien<br class="manualbr">Tél. : 01 522 6 522 0<br class="manualbr">Fax : 01 522 6 522 6</p>
<p><a href="http://www.at.issworld.com/" class="spip_out" rel="external">ISS G.m.b.H</a> <br class="manualbr">Brünner Straße 85, 1210 Wien<br class="manualbr">Tél. : 01 291 11 0<br class="manualbr">Fax : 01 290 14 73</p>
<p><a href="http://www.powerserv.at/" class="spip_out" rel="external">POWERSERV Austria</a><br class="manualbr">Plusieurs filiales dans toute l’Autriche<br class="manualbr">Tél. : 059 007</p>
<p><a href="http://www.manpower.at/" class="spip_out" rel="external">Manpower GmbH</a> <br class="manualbr">Plusieurs filiales dans toute l’Autriche<br class="manualbr">Tél. : 01 516 76-100<br class="manualbr">Fax : 01 516 76 199</p>
<p><a href="http://www.trenkwalder.com/at" class="spip_out" rel="external">TRENKWALDER</a> <br class="manualbr">Plusieurs filiales dans toute l’Autriche<br class="manualbr">Tél. : 01 291 120<br class="manualbr">Fax : 01 292 5800</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.ambafrance-at.org/spip.php?rubrique1203" class="spip_out" rel="external">site de l’ambassade de France en Autriche</a> </p>
<p><i>Mise à jour : février 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/emploi-stage/article/recherche-d-emploi-111385). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
