# Recherche d’emploi

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/emploi-stage/article/recherche-d-emploi#sommaire_1">Outils pour la recherche d’emploi</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/emploi-stage/article/recherche-d-emploi#sommaire_2">Organismes pour la recherche d’emploi</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/emploi-stage/article/recherche-d-emploi#sommaire_3">Opportunités d’emploi et de stages</a></li></ul>
<p>Pour trouver un emploi au Luxembourg, il existe des moyens similaires à ceux des autres Etats membres de l’Union européenne. Vous pouvez naturellement adresser des candidatures spontanées aux entreprises ou organismes qui vous intéressent mais aussi répondre aux offres d’emploi publiées dans la presse. Cependant, et à côté des agences intérimaires, des cabinets de recrutement et des centres d’orientation professionnelle, les services publics de l’emploi constituent des intermédiaires spécialisés susceptibles d’aider dans la recherche.</p>
<h3 class="spip"><a id="sommaire_1"></a>Outils pour la recherche d’emploi</h3>
<p>Voir le site <a href="http://www.guichet.public.lu/citoyens/fr/travail-emploi/index.html" class="spip_out" rel="external">Guichet public- travail-emploi</a>.</p>
<h4 class="spip">Journaux</h4>
<p>Principaux quotidiens luxembourgeois publiant des petites annonces : <a href="http://www.lequotidien.lu/" class="spip_out" rel="external">Le Quotidien</a> (francophone), <a href="http://www.tageblatt.lu/" class="spip_out" rel="external">Tageblatt</a> et Luxemburger Wort (germanophones).</p>
<h4 class="spip">Sites internet</h4>
<ul class="spip">
<li>La liste des principaux employeurs privés et publics sur le <a href="http://www.statistiques.public.lu/" class="spip_out" rel="external">portail des statistiques du Luxembourg</a></li>
<li><a href="http://www.abbl.lu/" class="spip_out" rel="external">Le site de l’Association des banques et banquiers du Luxembourg</a> (offres d’emploi dans le secteur financier) ;</li>
<li><a href="http://www.horesca.lu/" class="spip_out" rel="external">Le site de la Fédération nationale des hôteliers, restaurateurs et cafetiers du Grand-Duché de Luxembourg</a>.</li></ul>
<h4 class="spip">Réseaux</h4>
<ul class="spip">
<li><a href="http://www.cfci.lu/" class="spip_out" rel="external">Chambre française de commerce et d’industrie du Grand Duché de Luxembourg (CFCI)</a></li>
<li><a href="http://www.cc.lu/" class="spip_out" rel="external">Chambre de commerce du Luxembourg</a></li>
<li><a href="http://www.ubifrance.fr/belgique/export-belgique-avec-notre-bureau.html" class="spip_out" rel="external">Service économique régional de l’Ambassade de France en Belgique compétent pour le Luxembourg</a></li></ul>
<h4 class="spip">Annuaires</h4>
<h5 class="spip">Les agences d’intérim</h5>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://edituspro.luxweb.com/" class="spip_out" rel="external">Le portail internet du Luxembourg</a> (indiquer " intérim " comme mot-clé).</p>
<h5 class="spip">Les syndicats et chambres professionnelles</h5>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.luxembourg.public.lu/fr/politique/institutions-politiques/chambres-professionnelles/index.html" class="spip_out" rel="external">Le site du gouvernement du Grand-Duché de Luxembourg</a></p>
<h3 class="spip"><a id="sommaire_2"></a>Organismes pour la recherche d’emploi</h3>
<p>Pour les organismes en France susceptibles de vous aider dans votre recherche d’emploi, vous pouvez consulter la thématique <a href="services-aux-citoyens-preparer-son-expatriation-emploi.md" class="spip_in">Emploi</a>.</p>
<p>Si vous désirez trouver un emploi au Luxembourg ou si vous voulez tout simplement des informations sur les possibilités de travail, veuillez vous adresser à :</p>
<p><a href="http://www.adem.public.lu/" class="spip_out" rel="external">Administration de l’emploi</a><br class="manualbr">10, rue Bender<br class="manualbr">L-1299 Luxembourg<br class="manualbr">Tél. : (352) 247-88888<br class="manualbr">Fax : (352) 40 61 40<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/emploi-stage/article/recherche-d-emploi#info#mc#adem.public.lu#" title="info..åt..adem.public.lu" onclick="location.href=mc_lancerlien('info','adem.public.lu'); return false;" class="spip_mail">Courriel</a></p>
<p>Les services publics de l’emploi sont administrés par l’administration de l’emploi (ADEM) placé sous l’autorité du ministre du Travail et de l’Emploi et sous les ordres d’un directeur, chef d’administration. Son siège est à Luxembourg, sa compétence s’étend sur tout le territoire du Grand-Duché.</p>
<p>Le service de placement de cet organisme collecte les offres et les demandes d’emploi et œuvre à leur rapprochement. Il encadre et assiste les demandeurs d’emploi.</p>
<p>Des bureaux sont installés à Esch-sur-Alzette, Diekirch, Dudelange, Differdange, Wasserbillig et Wiltz (même numéro de téléphone).</p>
<p>Vous pouvez également accéder à des offres dans le domaine de la vente, de l’hôtellerie/restauration et des emplois de bureau via un serveur accessible à partir d’un poste téléphonique. Ce service résulte de la collaboration entre les services publics de l’emploi luxembourgeois (ADEM), belge (FOREM) et français (Pôle emploi).</p>
<h3 class="spip"><a id="sommaire_3"></a>Opportunités d’emploi et de stages</h3>
<ul class="spip">
<li><a href="http://www.guichet.public.lu/entreprises/fr/ressources-humaines/formation-professionnelle/index.html" class="spip_out" rel="external">Guichet public - formation professionnelle</a></li>
<li><a href="http://www.lifelong-learning.lu" class="spip_out" rel="external">www.lifelong-learning.lu</a></li>
<li><a href="http://www.men.public.lu/fr/index.html" class="spip_out" rel="external">Ministère de l’Education nationale et de la Formation professionnelle</a></li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/emploi-stage/article/recherche-d-emploi). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
