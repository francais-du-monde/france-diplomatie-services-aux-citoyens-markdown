# Convention fiscale

<p>La France et la Slovénie ont conclu le 7 avril 2004 une convention en vue d’éviter les doubles impositions en matière d’impôts sur le revenu et sur la fortune et de prévenir l’évasion et la fraude fiscales. Cette convention est entrée en vigueur entre les deux pays le <strong>1er mars 2007</strong>.</p>
<p>Le texte de la convention peut être obtenu auprès de la Direction des Journaux Officiels par courrier (26 rue Desaix - 75727 PARIS Cedex 15 - Téléphone : 01 40 58 75 00) ou consulté sur le site des <a href="http://www.journal-officiel.gouv.fr/" class="spip_out" rel="external">Journaux officiels</a> ou sur celui du <a href="http://www.impots.gouv.fr/" class="spip_out" rel="external">ministère des Finances</a>.</p>
<p><i>Mise à jour : septembre 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/fiscalite/article/convention-fiscale-114777). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
