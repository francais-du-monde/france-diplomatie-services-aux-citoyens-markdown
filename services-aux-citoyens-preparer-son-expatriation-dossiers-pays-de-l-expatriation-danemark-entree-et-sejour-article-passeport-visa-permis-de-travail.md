# Passeport, visa, permis de travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/entree-et-sejour/article/passeport-visa-permis-de-travail#sommaire_1">Formalités d’installation</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/entree-et-sejour/article/passeport-visa-permis-de-travail#sommaire_2">Permis de séjour </a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/entree-et-sejour/article/passeport-visa-permis-de-travail#sommaire_3">Enregistrement auprès des autorités locales</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Formalités d’installation</h3>
<p>Quel que soit le motif de votre séjour, renseignez-vous auprès de la section consulaire de l’ambassade de Danemark à Paris qui vous informera sur la règlementation en matière d’entrée et de séjour au Danemark, règlementation que vous devez impérativement respecter. Une fois sur place ce sont les autorités danoises qui prennent le relais.</p>
<p>Le consulat de France au Danemark n’est pas l’organisme compétent pour répondre à vos demandes d’information concernant votre séjour au Danemark.</p>
<p>Pour les séjours de <strong>moins de trois mois</strong>, aucune formalité administrative n’est requise, à l’exception de la présentation à la frontière d’une carte nationale d’identité ou d’un passeport valides.</p>
<h3 class="spip"><a id="sommaire_2"></a>Permis de séjour </h3>
<p>Si vous souhaitez séjourner au Danemark moins de trois mois, aucune formalité auprès des autorités locales n’est nécessaire.</p>
<p>Passé ce délai de trois mois, vous devez déposer une demande d’un certificat d’enregistrement (<i>registreringsbevis</i>) auprès des autorités danoises.</p>
<p>Les certificats sont administrés par le <strong>Statsforvaltning</strong> des cinq régions du Danemark.</p>
<p>Pour les <strong>ressortissants de l’UE/ EEE</strong> :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>résidant dans la commune de Copenhague ou dans la région avoisinante</strong>, s’adresser à : <br class="manualbr"><i>Statsforvaltningen Hovedstaden</i> <br class="manualbr">Borups Allé 177, blok D-E<br class="manualbr">2400 København NV<br class="manualbr">Tél. : 72 56 70 00</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>résidant en dehors de Copenhague</strong>, s’adresser au <i>Statsforvaltningen</i> de la région. Pour les <strong>ressortissants non communautaires</strong>, famille ou non d’un ressortissant de l’UE, ainsi que pour les placements au pair, la demande doit être déposée auprès de :<br class="manualbr">UDLAENDINGESERVICE <br class="manualbr">Ryesgade 53 <br class="manualbr">2100 København Ø<br class="manualbr">Tél. : 35 36 66 00</p>
<p><strong>Conditions d’obtention du certificat d’enregistrement danois</strong></p>
<p><i>Pièces à fournir</i> :</p>
<ul class="spip">
<li>le passeport ou la carte d’identité en cours de validité</li>
<li>une copie (de préférence en couleur) du passeport ou de la carte d’identité</li>
<li>une photo d’identité</li>
<li>des justificatifs du statut économique</li></ul>
<p>Le certificat d’enregistrement n’est accordé que si l’intéressé dispose de ressources suffisantes pendant la durée de son séjour au Danemark : un minimum de 5527 DKK/ mois (salaire brut) correspondant au niveau de l’aide sociale danoise (environ 741 EUR/ mois). Le certificat d’enregistrement est la preuve des droits dont disposent les ressortissants de l’UE déjà en raison de la règlementation de l’UE relative aux personnes et aux prestations de service.</p>
<p>Pour les <strong>jeunes salarié</strong> âgés de moins de 25 ans, le minimum est ramené à 4583 DKK/ mois (environ 619 EUR).</p>
<p><strong>Pour les Au-pair</strong> un contrat réglementaire est exigé. Le salaire est fixé à un minimum de 3050 DKK (412 EUR/ mois), la nourriture et l’hébergement sont gratuits (la valeur de ces derniers est estimée à 33948 DKK (4583 EUR) pour l’année 2009 par la direction des Impôts et reste imposable, tout comme le salaire). La limite d’âge est de 30 ans.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.nyidanmark.dk/en-us/coming_to_dk/au_pairs/au_pairs.htm" class="spip_out" rel="external">Nyidanmark.dk</a></p>
<p><strong> <i>Les travailleurs</i> </strong> titulaires d’un contrat de travail à durée indéterminée (CDI), obtiendront un permis valable cinq ans. Conditions de travail : min. 10-15 heures/ semaine ; salaire minimum correspondant au SMIC danois : env. 85 DKK/ heure. En cas de contrat à durée déterminée (CDD), le permis sera établi en fonction de sa durée.</p>
<p><strong>Les étudiants</strong> inscrits auprès d’un établissement d’enseignement supérieur reconnu par les autorités danoises (université, grande école), ainsi que leur conjoint et enfants, peuvent obtenir un permis de séjour valable pour une année la première fois, et renouvelable tous les ans jusqu’à l’obtention du diplôme.Les intéressés doivent remplir une déclaration auprès des autorités préfectorales danoises pour prouver qu’ils sont en mesure de subvenir à leurs besoins financiers.Des justificatifs de ressources ne sont pas demandés, mais au verso du permis de séjour, il est noté que le permis peut être retiré, si l’intéressé ou sa famille rencontre des difficultés financières.</p>
<p>Les étudiants non-titulaires d’une bourse <strong>Erasmus</strong> doivent également présenter une assurance maladie valable pour cinq semaines.</p>
<p>Pour les <strong>autres catégories</strong> (retraités, etc.), les demandes de long séjour sont examinées individuellement et l’octroi du permis de séjour est subordonné aux ressources propres. Les conjoints (famille) de ressortissants danois sont également tenus de produire des justificatifs de ressources (ex. : montant du revenu du conjoint).</p>
<p>Si vous cherchez un emploi au Danemark, vous serez autorisé à rester 6 mois sans certificat d’enregistrement.</p>
<h3 class="spip"><a id="sommaire_3"></a>Enregistrement auprès des autorités locales</h3>
<p>Après obtention de la carte de séjour, le résident doit <strong>s’inscrire au registre communal de la population</strong> (<i>Folkeregistret</i>) de la commune du lieu de résidence en vue d’<strong>obtenir le numéro CPR</strong> (numéro d’identification personnel).</p>
<p>Tous les citoyens danois sont enregistrés sur le registre de la population et possèdent ce qu’on nomme communément une " carte jaune " sur laquelle figure leur numéro CPR à dix chiffres dont les six premiers correspondent à la date de naissance (jour, mois, année) et les quatre autres chiffres forment une combinaison unique identifiant chaque personne.</p>
<p>Ce numéro est indispensable pour toute démarche officielle, et pour la couverture sociale, puisqu’il permet d’identifier chaque personne. Il est nécessaire pour les démarches de tous les jours, pour louer ou acheter des biens immobiliers ou ouvrir un compte en banque. Cette carte fait aussi office de carte nationale d’assurance maladie et permet donc d’avoir accès aux services médicaux couverts par cette assurance.</p>
<p>Pour les résidents en dehors de Copenhague, s’adresser à la commune de résidence.</p>
<p>A Copenhague, l’adresse est :</p>
<p><a href="http://www.kk.dk/" class="spip_out" rel="external">Folkeregisteret Københavns Kommune</a><br class="manualbr">Nyropsgade 1 <br class="manualbr">1602 København V<br class="manualbr">Tél. : 33 66 33 66<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/entree-et-sejour/article/passeport-visa-permis-de-travail#folkeregisteret#mc#okf.kk.dk#" title="folkeregisteret..åt..okf.kk.dk" onclick="location.href=mc_lancerlien('folkeregisteret','okf.kk.dk'); return false;" class="spip_mail">Courriel</a><br class="manualbr">Heures d’ouvertures : lundi à mercredi : 10h à 17h, jeudi : 9h à 18h et vendredi : 10h à 15h.</p>
<p>Pour en savoir plus :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.nyidanmark.dk/" class="spip_out" rel="external">Portail officiel (site en anglais)</a> pour les étrangers sur l’arrivée et le séjour au Danemark.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/entree-et-sejour/article/passeport-visa-permis-de-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
