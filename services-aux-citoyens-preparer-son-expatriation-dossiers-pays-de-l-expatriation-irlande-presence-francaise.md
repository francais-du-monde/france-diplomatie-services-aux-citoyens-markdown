# Présence française

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/presence-francaise/#sommaire_1">Ambassade et consulat de France</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/presence-francaise/#sommaire_2">Autorités françaises dans le pays</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/presence-francaise/#sommaire_3">Associations dans le pays</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/presence-francaise/#sommaire_4">Presse française</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Ambassade et consulat de France</h3>
<p>Consultez <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/article/annuaire-des-representations-francaises-a-l-etranger" class="spip_in">l’annuaire des ambassades et consulats français à l’étranger</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Autorités françaises dans le pays</h3>
<h4 class="spip">Réseau français de coopération et d’action culturelle</h4>
<p>Les coordonnées des services du réseau de coopération et d’action culturelle français en Irlande ainsi que la description de l’activité de ces services se trouvent dans la <a href="http://www.ambafrance-ie.org/-Service-culturel-et-scientifique-" class="spip_out" rel="external">rubrique culture du site internet de l’Ambassade</a> ou sur le <a href="http://www.latitudefrance.org/AnnuaireCarto.html" class="spip_out" rel="external">site internet du réseau culturel : Latitude France</a>.</p>
<h4 class="spip">Economie : réseau mondial</h4>
<p>Business France, l’Agence française pour le développement international des entreprises, est présente en Irlande. Sa mission est d’accompagner les entreprises françaises dans leur développement à l’international.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://export.businessfrance.fr/irlande/export-irlande-avec-nos-bureaux.html" class="spip_out" rel="external">Site de la Mission économique Business France en Irlande</a></p>
<p>Les services économiques sont également présents. Ils sont une émanation de la Direction générale du Trésor et ont pour missions principales l’analyse macroéconomique et l’animation des relations économiques bilatérales entre la France et les pays où ils sont implantés.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.tresor.economie.gouv.fr/pays/irlande" class="spip_out" rel="external">Site du service économique français en Irlande</a></p>
<h4 class="spip">Vos élus à l’Assemblée des Français de l’étranger</h4>
<p>Pour toute information sur l’Assemblée des Français de l’étranger et pour connaître les conseillers et les Sénateurs qui représentent les Français établis hors de France, vous pouvez consulter les sites Internet suivants :</p>
<ul class="spip">
<li>l’Assemblée des Français de l’étranger (AFE) : <a href="http://www.assemblee-afe.fr/" class="spip_out" rel="external">www.assemblee-afe.fr/</a></li>
<li>le Sénat au service des Français de l’étranger : <a href="http://www.senat.fr/expatries/dossiers_pays/irlande.html" class="spip_out" rel="external">consultez le dossier spécifique à l’Irlande</a></li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Associations dans le pays</h3>
<p><strong>Association Démocratique des Français à l’étranger - Français du monde (ADFE-FdM)</strong><br class="manualbr">17 William Bligh, The Gasworks<br class="manualbr">Dublin 4</p>
<p><a href="http://www.ufe.org/fr/" class="spip_out" rel="external">Union des Français de l’étranger (UFE)</a><br class="manualbr">Ruby House<br class="manualbr">Derryglen<br class="manualbr">Recess <br class="manualbr">Tél. : 00 353 (0) 85 77 88 122</p>
<p><a href="http://dublinaccueil.blogspot.com" class="spip_out" rel="external">Dublin Accueil</a><br class="manualbr">Courriel : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/presence-francaise/#dublin.accueil#mc#yahoo.fr#" title="dublin.accueil..åt..yahoo.fr" onclick="location.href=mc_lancerlien('dublin.accueil','yahoo.fr'); return false;" class="spip_mail">dublin.accueil<span class="spancrypt"> [at] </span>yahoo.fr</a></p>
<p><a href="http://www.breizheire.com/" class="spip_out" rel="external">Association bretonne en Irlande (BreizhEire)</a><br class="manualbr">BreizhEire rassemble les bretons et les amoureux de la Bretagne en Irlande autour d’évènements et d’activités culturelles. <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/presence-francaise/#breizheire#mc#gmail.com#" title="breizheire..åt..gmail.com" onclick="location.href=mc_lancerlien('breizheire','gmail.com'); return false;" class="spip_mail">Courriel</a></p>
<p>Pour en savoir plus, vous pouvez notre article <a href="services-aux-citoyens-preparer-son-expatriation-en-savoir-plus-article-associations-des-francais-de-l-etranger.md" class="spip_in">Associations des Français de l’étranger</a>.</p>
<h3 class="spip"><a id="sommaire_4"></a>Presse française</h3>
<p>Il est possible de se procurer en Irlande les journaux, magazines et livres français, sur abonnement ou par correspondance, mais également auprès de quelques kiosques ou librairies.</p>
<p>La librairie <a href="http://www.eason.ie/" class="spip_out" rel="external">Eason &amp; Son</a>  permet notamment de passer commande d’ouvrages en langue française (sur leur site à la rubrique <i>Deliveries</i>) :</p>
<p><strong>Eason  Son Bookstores</strong><br class="manualbr">40/42 Lower O’Connell Street - Dublin 1<br class="manualbr">Tél. : [353] (0)1 858 3800 - Télécopie : [353] (0)1 858 3806<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/presence-francaise/#mailtoinfo#mc#eason.ie#" title="mailtoinfo..åt..eason.ie" onclick="location.href=mc_lancerlien('mailtoinfo','eason.ie'); return false;" class="spip_mail">Courriel</a></p>
<p><i><a href="http://www.lepetitjournal.com/" class="spip_out" rel="external">Le Petit Journal</a></i>, journal des Français et francophones à l’étranger.</p>
<p><i>Mise à jour : décembre 2013</i></p>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/presence-francaise/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
