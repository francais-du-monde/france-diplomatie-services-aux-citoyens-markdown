# Loisirs et culture

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/vie-pratique/article/loisirs-et-culture#sommaire_1">Activités culturelles</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/vie-pratique/article/loisirs-et-culture#sommaire_2">Télévision – Radio</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/vie-pratique/article/loisirs-et-culture#sommaire_3">Presse française</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Activités culturelles</h3>
<p><strong>Activités culturelles en français</strong></p>
<p>Quelques films français grand public sont diffusés dans les salles de cinéma slovènes. La Cinémathèque slovène propose régulièrement des films français.</p>
<p>L’Institut français de Ljubljana propose chaque année un programme varié de films, de conférences, d’expositions (photographie, arts visuels, design, etc.) et de spectacles (musique classique, danse, théâtre, etc.). Il dispose en outre d’une médiathèque offrant la consultation et l’emprunt de près de 13 000 revues, livres et documents audiovisuels en français.</p>
<p><a href="http://www.institutfrance.si/?lang=sl" class="spip_out" rel="external">Institut français</a> <br class="manualbr">Charles Nodier Breg 12 <br class="manualbr">1000 LJUBLJANA <br class="manualbr">Tél. : (01) 200 05 00 <br class="manualbr">Télécopie : (01) 200 05 12</p>
<h3 class="spip"><a id="sommaire_2"></a>Télévision – Radio</h3>
<p>Le câble permet de recevoir TV5, France 2, Arte, Euronews. Les autres chaînes sont accessibles par le satellite. Toutes les principales chaînes européennes sont accessibles par le câble.</p>
<h3 class="spip"><a id="sommaire_3"></a>Presse française</h3>
<p>Des quotidiens nationaux et de magazines français d’information générale ou spécialisés (presse féminine, décoration, revues scientifiques) sont disponibles dans quelques kiosques librairies et dans certains supermarchés, parmi lesquels le « Maximarket » et le centre commercial Leclerc à Ljubljana. Une formule d’abonnement local est également possible pour les principaux quotidiens nationaux français.</p>
<p>Les librairies suivantes assurent la diffusion de journaux et de livres en français :</p>
<ul class="spip">
<li>Mladinska Knjiga Trgovina Slovenska cesta 29 <br class="manualbr">1000 LJUBLJANA <br class="manualbr">Tél. : (01) 159 76 27</li>
<li>Drzavna Zalozba Slovenije Mestni trg 26 <br class="manualbr">61000 LJUBLJANA</li></ul>
<p><i>Mise à jour : septembre 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/vie-pratique/article/loisirs-et-culture). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
