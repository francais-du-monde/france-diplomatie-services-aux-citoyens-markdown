# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/vie-pratique/article/cout-de-la-vie-111156#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/vie-pratique/article/cout-de-la-vie-111156#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/vie-pratique/article/cout-de-la-vie-111156#sommaire_3">Alimentation</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/vie-pratique/article/cout-de-la-vie-111156#sommaire_4">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>L’unité monétaire est le <strong>Litas</strong> (LTL ou Lt).</p>
<p>En juin 2004, la Lituanie a intégré avec l’Estonie et la Slovénie le mécanisme de change européen (MCE II). Il s’agit d’un dispositif communautaire visant à réguler l’entrée de nouveaux membres au sein de la zone euro, en fixant pour la devise de chaque pays candidat un cours pivot par rapport à l’euro. Jusqu’à leur entrée dans la zone euro, ces pays doivent maintenir le cours de leur devise dans une limite de fluctuation de +/-15% autour du taux pivot.</p>
<p>S’agissant de la Lituanie, le rapport a été fixé au 26/06/2004 tel que suit :</p>
<ul class="spip">
<li>1 LTL = 0,2896 €</li>
<li>1 € = 3,4528 LTL</li></ul>
<p>La Lituanie devrait adopter l’Euro au 1er janvier 2015.</p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<p>Il n’existe pas de limitation en matière de transferts de fonds</p>
<h3 class="spip"><a id="sommaire_3"></a>Alimentation</h3>
<p>Une large gamme de produits alimentaires est disponible sur place. La grande distribution est bien établie. L’offre de produits d’origine française s’étend chaque année.</p>
<p>Prix moyen d’un repas dans un restaurant : 35 à 40 litas Menu du jour (midi) : 15 à 18 litas.</p>
<h3 class="spip"><a id="sommaire_4"></a>Evolution des prix</h3>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/vie-pratique/article/cout-de-la-vie-111156). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
