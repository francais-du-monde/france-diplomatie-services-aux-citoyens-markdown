# Emploi, stage

<h2 class="rub22999">Marché du travail</h2>
<p class="chapo">
    Le chômage, du fait de la sévérité de la crise économique (le PIB s’est contracté de 14,8% en 2009), a été multiplié par trois depuis 2007 mais est en régression depuis trois ans (15,2 % en 2011, 13,2 % en 2012 et 11,5 % (estimé) en 2013). Dans ce contexte, l’émigration d’une partie de la main d’œuvre lituanienne, qui s’est intensifiée en 2009 (+30%), s’est poursuivie mais à un rythme plus réduit. En dépit de cette mauvaise conjoncture, la Lituanie reste, de par son potentiel de croissance (un des plus élevés d’Europe), un marché d’avenir.
</p>
<h4 class="spip">Secteurs à fort potentiel</h4>
<p>Compte tenu de la nature des projets d’investissement ainsi que des politiques stratégiques des autorités lituaniennes qui seront mis en œuvre au cours des prochaines années, les besoins de recrutement devraient essentiellement se concentrer dans les secteurs de l’industrie, l’énergie l’environnement, la RD (recherche et développement) et les services aux entreprises.</p>
<p>Le savoir-faire français étant particulièrement apprécié dans certains de ces secteurs, des opportunités seront donc à saisir pour les ressortissants français, notamment au travers de la formule du VIE.</p>
<p>A noter que du fait d’une quasi-absence d’expatriés français, la maitrise de l’anglais est indispensable dans les contacts avec le monde des affaires. La connaissance du lituanien est également vivement conseillée.</p>
<p>La concurrence avec la main d’œuvre lituanienne pourrait toutefois être rude, celle-ci étant qualifiée et l’une des moins chères d’Europe. 40% de la population active justifie d’un diplôme d’enseignement supérieur, 90% de la population lituanienne parle au moins une langue étrangère, 50% de la population est trilingue. 8 personnes sur 10 parlent russe, 30% de la population parle anglais.</p>
<h4 class="spip">Rémunération</h4>
<p>Sur recommandation du Conseil tripartite, le gouvernement fixe la rémunération horaire minimum et le salaire mensuel minimum et peut décider, pour certains secteurs économiques, certaines régions ou certains groupes de travailleurs, de montants différents de rémunération horaire minimum et de salaire mensuel minimum.</p>
<p>Depuis le 1er janvier 2013, le salaire minimum est fixé, pour les salariés des entreprises, institutions et organisations, quelle que soit leur forme juridique, à 6,06 Litas par heure et à 1000 Litas par mois (soit 289,6 euros).</p>
<p>La rémunération du travail comprend la rémunération principale, toutes les rétributions complémentaires, ainsi que les paiements de toute nature directement versés par l’employeur pour le travail réalisé. Les hommes et les femmes reçoivent une rémunération identique pour un même travail.</p>
<p>Le salaire de l’employé est calculé tous les mois en tenant compte du temps réellement travaillé. Les prélèvements, prévus par les lois lituaniennes, sont décomptés du salaire de l’employé : impôt sur le revenu (15%), cotisations sociales (9% part salariale), prélèvements d’après des ordres d’exécution et autres prélèvements à la demande de l’employé.</p>
<p>Si vous ne travaillez pas dans une institution d’État, le montant de votre salaire dépendra du nombre d’heures travaillées, des heures supplémentaires et des accords passés avec l’employeur en tenant compte de votre expérience et de la spécificité de votre travail.</p>
<p>Si vous travaillez dans une institution d’État ou une entreprise, votre salaire sera déterminé par le coefficient appliqué, l’ancienneté, la formation, la catégorie, les conditions de travail particulières ou les travaux supplémentaires réalisés.</p>
<p><i>Source : <a href="http://ec.europa.eu/eures/" class="spip_out" rel="external">EURES</a>, ministère lituanien des Affaires sociales et du Travail</i></p>
<p><i>Mise à jour : janvier 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-lituanie-emploi-stage-article-stages.md" title="Stages">Stages</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-lituanie-emploi-stage-article-recherche-d-emploi.md" title="Recherche d’emploi">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-lituanie-emploi-stage-article-reglementation-du-travail-111143.md" title="Réglementation du travail">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-lituanie-emploi-stage-article-marche-du-travail.md" title="Marché du travail">Marché du travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/emploi-stage/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
