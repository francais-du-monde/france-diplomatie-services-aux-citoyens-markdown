# Convention de sécurité sociale

<p><strong>Depuis le 1er juillet 2013, l’ancienne convention bilatérale franco-croate de 1995 n’est plus applicable dans les relations entre la France et la Croatie.</strong></p>
<p>Dans sa circulaire DSS/DACI/2013/373 du 22 octobre 2013, la division des affaires communautaires et internationales - direction de la sécurité sociale -, précise les conséquences en matière de sécurité sociale de l’élargissement de l’Union européenne (UE) à la Croatie au 1er juillet 2013 et les modalités de la <strong>mise en œuvre des règlements n° 883/2004 et n° 978/2009 entre la Croatie et les autres États membres de l’UE</strong>.</p>
<p>Elle précise également les dispositions transitoires au regard de la convention bilatérale de sécurité sociale franco-croate de 1995.</p>
<p>La libre circulation totale des ressortissants croates pour l’accès au marché de l’emploi en qualité de travailleurs salariés s’effectuera après une période transitoire de sept ans.</p>
<p><strong>En matière de sécurité sociale, les règlements n° 883/2004 et 987/2009 sont intégralement et immédiatement applicables dans les relations entre la France et la Croatie</strong>.</p>
<p>La circulaire présente également les dispositions applicables aux détachements en cours au 1er juillet 2013.</p>
<p>Enfin, elle attire l’attention sur le fait que les règlements 883/04 et 987/2009 ne s’appliquent pas à la Croatie dans ses relations avec l’Islande, le Liechtenstein et la Norvège, et avec la Suisse tant qu’elle n’est pas partie aux accords respectifs sur l’EEE et la Suisse.</p>
<p>Circulaire disponible sur le site du <a href="http://www.cleiss.fr/reglements/circulaires/13_373t0.pdf" class="spip_out" rel="external">CLEISS</a></p>
<p><a href="http://www.cleiss.fr" class="spip_out" rel="external">Centre des liaisons européennes et internationales de sécurité sociale</a> <br class="manualbr">11, rue de la Tour des Dames<br class="manualbr">75436 Paris Cedex 09<br class="manualbr">Tél. : 01.45.26.33.41</p>
<p><strong>Pour plus d’information :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  consultez notre rubrique thématique <a href="services-aux-citoyens-preparer-son-expatriation-protection-sociale.md" class="spip_in">Protection sociale</a>.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/protection-sociale/article/convention-de-securite-sociale-110892). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
