# Loisirs et culture

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/vie-pratique/article/loisirs-et-culture#sommaire_1">Tourisme</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/vie-pratique/article/loisirs-et-culture#sommaire_2">Activités culturelles</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/vie-pratique/article/loisirs-et-culture#sommaire_3">Sports</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/vie-pratique/article/loisirs-et-culture#sommaire_4">Télévision – Radio</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/vie-pratique/article/loisirs-et-culture#sommaire_5">Presse française</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Tourisme</h3>
<p>L’Espagne compte de très nombreux sites touristiques. On peut citer, pour leur intérêt historique et leur richesse artistique et culturelle : Saint-Jacques de Compostelle, Salamanque, Ségovie, Numance, Tolède, Grenade, Valladolid, Séville, Cordoue, El Escorial, etc.</p>
<p>Toutes les villes de la Costa Brava, de la Costa Blanca et de la Costa del Sol, les îles Baléares sont des sites balnéaires recherchés, ainsi que les parcs naturels de la Sierra de Grazalema, de Cazorla Segura et de Donana pour des randonnées.</p>
<p>Pour plus d’informations, s’adresser à :</p>
<p><a href="http://www.spain.info/" class="spip_out" rel="external">Office espagnol du tourisme</a><br class="manualbr">22 rue Saint Augustin<br class="manualbr">75002 Paris<br class="manualbr">Tél. : 01 45 03 82 57<br class="manualbr">Fax : 01 45 03 82 51</p>
<h3 class="spip"><a id="sommaire_2"></a>Activités culturelles</h3>
<h4 class="spip">Activités culturelles en français</h4>
<p><strong>Cinéma français</strong></p>
<p>Quelques cinémas à Madrid et Barcelone présentent des films français en version originale sous-titrée. Un festival du film français "Unifrancia" a lieu à Saragosse.</p>
<p><strong>Artistes français en Espagne</strong></p>
<p>Des manifestations culturelles françaises (conférences, expositions, théâtre, concerts …) sont organisées à travers le réseau d’action artistique français, parfois en collaboration avec les institutions culturelles espagnoles.</p>
<p><strong>Instituts français et Alliances françaises en Espagne</strong></p>
<p>Vous trouverez les listes des instituts français en Espagne sur le site Internet du ministère des Affaires étrangères : <a href="http://latitudefrance.org/" class="spip_out" rel="external">http://latitudefrance.org/</a>.</p>
<h4 class="spip">Activités culturelles locales</h4>
<p><strong>Cinéma</strong></p>
<p>Les salles de spectacle sont de bon confort, en général climatisées. Un festival international du cinéma a lieu à Saint Sébastien en septembre, un festival du film fantastique à Sitges.</p>
<p><strong>Spectacle vivant, musées et expositions</strong></p>
<p>Parmi les formes de spectacles typiquement espagnols, on peut citer le Flamenco, qui connaît un regain de popularité, et la Zarzuela (sorte d’opérette espagnole), qui ont lieu dans des salles spécialisées.</p>
<p>Madrid (Musée du Prado, Musée d’art contemporain, etc.) et Barcelone (Musée Picasso, Fondation Joan Miro, etc.), mais également Bilbao (Musée Guggenheim entre autres) ou Séville, possèdent de nombreux musées et galeries d’expositions.</p>
<p><a href="http://www.servicaixa.es/" class="spip_out" rel="external">Service de réservations en ligne</a>.</p>
<h3 class="spip"><a id="sommaire_3"></a>Sports</h3>
<p>Tous les sports peuvent être pratiqués en Espagne. Tout l’équipement peut être trouvé sur place.</p>
<p><strong>Pour chasser ou pêcher en Espagne</strong>, il est nécessaire d’obtenir un permis. L’intéressé doit se rendre au <a href="http://www.licenciascazaypesca.es/" class="spip_out" rel="external">Despacho de Licencia de Caza y Pesca</a> de la province.</p>
<p>Le tarif des permis varie selon leur durée de validité (en 2011, 18 € pour un an). Des renseignements sur l’obtention des permis sont disponibles sur le site de la <a href="http://www.munimadrid.es/" class="spip_out" rel="external">Municipalité de Madrid</a>.</p>
<p>Période de chasse : publiée chaque année dans le bulletin de chaque province (autour du mois de juin), mais en règle générale : à partir du 2ème ou 3ème dimanche d’octobre jusqu’au 1er dimanche de février.</p>
<p>Période de pêche : également publiée dans le Bulletin (vers le mois de février), en général ouverture le 3ème dimanche de mars et fermeture à la mi-août.</p>
<p><strong>Pour naviguer en Espagne</strong>, le certificat de conduite est obligatoire pour diriger une embarcation de plaisance lorsqu’il s’agit de voiliers de plus de cinq mètres de long ou de bateaux à moteur de plus de quatre mètres avec une puissance supérieure à 10 kilowatts.</p>
<p><strong>Si vous naviguez sur votre propre bateau</strong>, vous devrez présenter à l’entrée de chaque port la <strong>documentation</strong> qui, selon le pays d’enregistrement et le pavillon, atteste de la propriété de l’embarcation, ainsi que <strong>l’assurance obligatoire</strong>, devant couvrir un minimum de 150 000 euros de dommages aux tiers. Si les autorités constatent, sur le bateau, l’existence d’éléments portant atteinte à la <strong>sécurité maritime </strong>ou nuisibles pour l’environnement, elles peuvent s’opposer au mouillage, à la sortie ou à l’entrée au port.</p>
<p><strong>Pour louer un bateau</strong>, si le certificat de conduite n’a pas été obtenu en Espagne, il est nécessaire de présenter une <strong>autorisation de la capitainerie</strong> compétente, indiquant que les attributions octroyées sont les mêmes que celles du certificat étranger. Cette autorisation peut être demandée en personne ou à travers l’entreprise de location. Normalement, il s’agit d’une formalité rapide dans la mesure où les capitaineries possèdent la liste des équivalences entre certificats. La documentation du bateau sera fournie par l’entreprise de location.</p>
<p>Les embarcations à portance dynamique ou à grande vitesse font l’objet d’un contrôle particulier : elles doivent communiquer leurs plans de navigation, porter une marque EAV visible depuis les airs, et accoster dans des endroits réservés.</p>
<p><strong>Dans la mesure où ces conditions peuvent varier, nous vous recommandons de contacter le consulat ou l’ambassade d’Espagne afin de vérifier ces exigences.</strong></p>
<h3 class="spip"><a id="sommaire_4"></a>Télévision – Radio</h3>
<p><strong>Télévision</strong></p>
<p>Les Espagnols détiennent le record européen du nombre moyen d’heures passées devant la télévision, avec, en 2006, 3h40 par jour. Ils ont accès à six chaînes hertziennes nationales (TVE-1 et La 2, chaînes publiques, Antena 3, Cuatro, Telecinco, La Sexta) ainsi qu’à 18 chaînes autonomiques et locales (pour 12 régions). D’autre part, il existe plus d’un millier de chaînes de télévision locales en Espagne qui, en l’absence d’une loi propre, sont considérées "a-légales" par les juristes.</p>
<p>Il est possible de capter des chaînes étrangères avec une antenne parabolique (chaînes françaises, allemandes, anglaises et CNN). Toutes les chaînes de télévision françaises sont accessibles par satellite ou par câble.</p>
<p>Le type de système adopté est PAL. Il n’existe pas de redevance audiovisuelle en Espagne. La vidéo est répandue, il existe de très nombreux clubs.</p>
<p>Sites des principales chaînes espagnoles :</p>
<ul class="spip">
<li><a href="http://www.rtve.es/" class="spip_out" rel="external">RTVE.es</a></li>
<li><a href="http://www.antena3.com/PortalA3com/home.do" class="spip_out" rel="external">Antena3.com</a></li>
<li><a href="http://www.cuatro.com/" class="spip_out" rel="external">Cuatro.com</a></li>
<li><a href="http://www.telecinco.es/" class="spip_out" rel="external">Telecinco.es</a></li>
<li><a href="http://www.lasexta.com/" class="spip_out" rel="external">Lasexta.com</a></li></ul>
<p><strong>Radio</strong></p>
<p>On reçoit les émissions de Radio France Internationale partout en Espagne, celles de RMC et Sud Radio dans la région de Barcelone, dans de bonnes conditions. La réception de France Inter est en général médiocre.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Site de l’Ambassade de France en Espagne-&gt;<a href="http://ambafrance-es.org/" class="spip_url spip_out auto" rel="nofollow external">http://ambafrance-es.org/</a>].</p>
<h3 class="spip"><a id="sommaire_5"></a>Presse française</h3>
<p>De nombreuses librairies à Madrid, à Barcelone, à Bilbao et à Séville assurent la diffusion de la presse et des ouvrages en français. Tous les grands périodiques français sont disponibles dans les kiosques des grandes villes.</p>
<p>Il existe par ailleurs plusieurs sites Internet français destinés aux Français et francophones établis en Espagne. Vous trouverez leurs coordonnées sur le site de l’<a href="http://ambafrance-es.org/" class="spip_out" rel="external">Ambassade de France en Espagne</a>.</p>
<p><i>Mise à jour : avril 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/vie-pratique/article/loisirs-et-culture). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
