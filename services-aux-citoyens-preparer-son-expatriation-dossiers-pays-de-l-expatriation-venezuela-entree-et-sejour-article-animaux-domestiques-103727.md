# Animaux domestiques

<p>Pour prendre connaissance des principes généraux concernant l’exportation des animaux domestiques, référez-vous à notre article : <a href="services-aux-citoyens-preparer-son-expatriation-douanes-article-animaux-domestiques.md" class="spip_in">Animaux domestiques</a>.</p>
<p>Pour pouvoir faire entrer un animal domestique sur le sol vénézuélien vous devez être en possession d’un certificat vétérinaire de bonne santé, d’un carnet de vaccinations à jour et d’un certificat de vaccination antirabique. Il est également indispensable d’avoir fait vacciner les chiens contre la maladie de Carré. Pour tous les animaux il est recommandé d’obtenir, en plus des documents mentionnés ci-dessus, un permis de sortie du territoire délivré par le ministère de l’Agriculture à l’aéroport du pays de départ.</p>
<p>Pour plus d’informations, visitez le <a href="http://www.iatatravelcentre.com/VE-Venezuela-customs-currency-airport-tax-regulations-details.htm#Pets" class="spip_out" rel="external">site de l’association internationale du transport aérien</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/entree-et-sejour/article/animaux-domestiques-103727). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
