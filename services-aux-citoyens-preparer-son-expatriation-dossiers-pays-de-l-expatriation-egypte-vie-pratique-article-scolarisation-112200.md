# Scolarisation

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/vie-pratique/article/scolarisation-112200#sommaire_1">Enseignement supérieur</a></li></ul>
<p>Pour toute information sur la scolarisation dans le système français à l’étranger, vous pouvez consulter notre thématique sur les <a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures.md" class="spip_in">Etudes supérieures</a> et la <a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais.md" class="spip_in">Scolarité en français</a>.</p>
<p>Vous y trouverez des renseignements sur :</p>
<ul class="spip">
<li>les établissements français du primaire et du secondaire à l’étranger ;</li>
<li>les bourses scolaires et la prise en charge des frais de scolarité à l’étranger ;</li>
<li>les possibilités qui s’offrent à vous si votre enfant ne peut être scolarisé à l’étranger dans le système français (enseignement à distance par le CNED, programme français langue maternelle (FLAM), internats en France) ;</li>
<li>les épreuves du baccalauréat à l’étranger ;</li>
<li>les bourses d’études supérieures en France et à l’étranger ;</li>
<li>l’équivalence des diplômes.</li></ul>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.aefe.fr/" class="spip_out" rel="external">Agence pour l’enseignement français à l’étranger</a></p>
<h3 class="spip"><a id="sommaire_1"></a>Enseignement supérieur</h3>
<p>En Egypte, le système de l’enseignement supérieur se structure comme suit :</p>
<ul class="spip">
<li>18 universités publiques (dont l’Université Al Azhar),</li>
<li>une vingtaine d’universités privées</li>
<li>une cinquantaine d’instituts publics de formation technique</li>
<li>plus d’une centaine d’instituts privés techniques.</li></ul>
<p>Les universités publiques les plus renommées sont les Universités du Caire (250 000 étudiants), d’Ain Shams (200 000 étudiants), d’Alexandrie (140 000 étudiants) et d’Al Azhar.</p>
<p>On compte aujourd’hui une vingtaine d’universités privées dont l’université américaine du Caire (4800 inscrits), l’université allemande d’Égypte (3000 inscrits), l’université britannique (1200 inscrits) et l’université française (450 inscrits) et plusieurs universités privées arabes. S’il est certain que les droits de scolarité élevés (allant de 3000 à 6000 € par an) permettent des infrastructures plus récentes et modernes qu’à l’université publique, le mérite n’est plus la seule condition pour intégrer l’université.</p>
<p>L’accès à la formation supérieure publique est très sélectif et hiérarchisé puisque le critère d’admission principal est le pourcentage obtenu au baccalauréat égyptien (« Thanaweyya Amma »).</p>
<p>Cette sélection à l’entrée de l’université permet d’identifier quelles sont les facultés considérées comme filières « d’excellence » en Égypte. Dans l’ordre décroissant pour les sciences exactes, ce sont la médecine (résultat supérieur à 95%), la pharmacie et l’ingénierie (résultat supérieur à 85%) qui sont considérées comme prestigieuses. Pour les sciences humaines ce sont les sciences politiques, l’économie et le droit (résultat supérieur à 95 %).</p>
<p>Contrairement au système des universités publiques égyptiennes gratuites mais exigeant un niveau requis au Thanawiya Amaa, certaines universités privées proposent des enseignements dans des facultés scientifiques où il suffit de payer pour s’inscrire. Si des bourses d’études sur mérite sont parfois offertes, la sélection se fait donc ici plutôt sur critères financiers.</p>
<h4 class="spip">Cursus et diplômes</h4>
<p><strong>Filières générales</strong></p>
<p>Le <i>baccalauréos</i> ou licence est le diplôme qui sanctionne quatre années d’études après le baccalauréat égyptien. Titulaire de ce diplôme, l’étudiant égyptien a le choix entre une voie courte qui aboutit à un diplôme de type <i>master professionnel</i> et une voie longue qui aboutit à l’obtention d’un magistère (de 4 à 6 ans, en comptant une année de cours magistraux), puis d’un doctorat (PhD de quatre ans au minimum, incluant une année préparatoire sanctionnée par un examen et qui permet au directeur de thèse de choisir le sujet).</p>
<p>Seuls les cursus d’ingénierie et de médecine font exception à cette règle, puisque le diplôme d’ingénieur s’obtient en cinq ans (obtention du <i>Bachelor of Engineering</i>) et celui de médecine en six ans.</p>
<p>Une année d’études se divise en deux semestres. Le premier débute en septembre/octobre pour s’achever en janvier/février avec les examens de mi-année. Le second semestre débute en février/mars pour s’achever en juin/juillet avec les examens de fin d’année qui conditionne le passage en année supérieure.</p>
<p>En règle générale, les enseignements se font en arabe moderne qui tend à se rapprocher du dialecte égyptien à l’oral. Mais les cours peuvent être également dispensés en anglais à la faculté d’ingénierie, de sciences ou de pharmacie. La langue française occupe une place non négligeable à l’université publique égyptienne, notamment au sein des filières françaises qui y sont implantées.</p>
<p><strong>Filières techniques</strong></p>
<p>Les instituts supérieurs de technologie prolongent l’enseignement secondaire technique.</p>
<p>Ce sont des instituts de formation technique en grande majorité (cycle court en deux ans aboutissant à des diplômes de type BTS) et minoritairement des instituts supérieurs de formation technique (cycle de 4 à 5 ans). Les orientations dans ces instituts sont à peu près de 50% vers des filières commerciales et 50% vers des filières industrielles.</p>
<p>En comparaison avec les pays de la région, l’Égypte est dotée d’un fort potentiel universitaire et les formations d’excellence notamment en médecine et en ingénierie sont d’un bon niveau.</p>
<h4 class="spip">Coopération avec la France</h4>
<p>L’offre universitaire française en Égypte se singularise par ses quatre filières francophones spécialisées dans les universités publiques égyptiennes :</p>
<ul class="spip">
<li>L’Institut de droit des affaires internationales, créé en 1988 à l’Université du Caire, délivre un diplôme français de l’Université Paris I Panthéon Sorbonne.</li>
<li>Le département de gestion et de commerce international, créé en 1993 à l’Université d’Aïn Shams, en partenariat avec les Universités de Poitiers et de Panthéon Sorbonne, et l’Université de Paris Dauphine pour le MBA.</li>
<li>La filière francophone d’économie et de sciences politiques, créée en 1994 à l’Université du Caire, en partenariat avec l’IEP de Paris en sciences politiques et avec l’Université de Paris I dont elle délivre désormais une licence en économie ou en science politique selon l’option choisie par l’étudiant.</li>
<li>Le département francophone d’études juridiques, situé au sein de la faculté de droit de l’Université Ain Chams, en partenariat avec l’Université de Lyon III.</li></ul>
<p><strong>Les sections francophones et l’Université Senghor offrent par ailleurs des formations diplômantes en langue française, en partenariat avec des universités françaises. </strong></p>
<p><strong>La filière de journalisme n’existe plus.</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  L’Université Senghor, créée en 1990, a pour vocation d’être une université francophone internationale d’enseignement et de recherche à finalité professionnelle, au service du développement africain. Elle propose un master en développement sur deux ans de type master professionnel, décliné en sept spécialités professionnelles structurées au sein de quatre départements : administration -gestion / patrimoine culturel / santé / environnement
</p>
<p>L’Université Senghor a signé plusieurs accords de coopération avec des universités égyptiennes et étrangères, dont les universités de Paris et de Bordeaux III.</p>
<p><i>Mise à jour : avril 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/vie-pratique/article/scolarisation-112200). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
