# Logement

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/vie-pratique/article/logement#sommaire_1">Où se loger ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/vie-pratique/article/logement#sommaire_2">Conditions de location</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/vie-pratique/article/logement#sommaire_3">Hôtels</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/vie-pratique/article/logement#sommaire_4">Electricité</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/vie-pratique/article/logement#sommaire_5">Eau</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/vie-pratique/article/logement#sommaire_6">Electroménager</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Où se loger ?</h3>
<p><strong>Moncton et Halifax</strong></p>
<p>Les quartiers résidentiels sont situés au centre et à la périphérie de la ville (Shediac Road et Kingswood Park à Moncton, Fox Creek à Dieppe). Les logements sont généralement équipés en gros électroménager. Le marché locatif se limite à des appartements de petite taille (<i>condos</i> de type F2 ou F3) et il est difficile de trouver une maison à louer.</p>
<p>Pour un logement de trois pièces, les charges mensuelles varient de 250 à 300 CAD par mois pour l’électricité et le chauffage.</p>
<table class="spip" summary="">
<caption><strong>Exemples de loyers mensuels</strong></caption>
<thead><tr class="row_first"><th id="ida051_c0"> </th><th id="ida051_c1">Studio</th><th id="ida051_c2">3 pièces</th><th id="ida051_c3">Villa</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<th headers="ida051_c0" id="ida051_l0">Quartier résidentiel</th>
<td headers="ida051_c1 ida051_l0">500 à 600 CAD</td>
<td headers="ida051_c2 ida051_l0">1000 à 1200 CAD</td>
<td headers="ida051_c3 ida051_l0">1800 à 2000 CAD</td></tr>
<tr class="row_even even">
<th headers="ida051_c0" id="ida051_l1">Banlieue</th>
<td headers="ida051_c1 ida051_l1">---</td>
<td headers="ida051_c2 ida051_l1">650 à 750 CAD</td>
<td headers="ida051_c3 ida051_l1">1400 à 1700 CAD</td></tr>
</tbody>
</table>
<p><strong>Ottawa</strong></p>
<p>Les quartiers résidentiels sont ceux de Rockliffe, New Edimburgh, Alta Vista, Old Ottawa South et du Glebe.</p>
<p>Il existe une certaine variété de choix pour tous les types de logement. Il est cependant difficile de trouver de grands logements. Les appartements sont rarement meublés, mais disposent toujours d’une cuisine équipée. Les charges ne sont généralement pas comprises dans le prix du loyer (elles peuvent s’élever à 600$ par mois pour une maison).</p>
<table class="spip" summary="">
<caption><strong>Exemples de loyers mensuels</strong></caption>
<thead><tr class="row_first"><th id="idc049_c0"> </th><th id="idc049_c1">3 pièces</th><th id="idc049_c2">Villa</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<th headers="idc049_c0" id="idc049_l0">Rockliffe et New Edimburgh</th>
<td headers="idc049_c1 idc049_l0">2500 CAD</td>
<td headers="idc049_c2 idc049_l0">3500 CAD</td></tr>
<tr class="row_even even">
<th headers="idc049_c0" id="idc049_l1">Quartier résidentiel</th>
<td headers="idc049_c1 idc049_l1">1500 CAD</td>
<td headers="idc049_c2 idc049_l1">3000 CAD</td></tr>
<tr class="row_odd odd">
<th headers="idc049_c0" id="idc049_l2">Banlieue  </th>
<td headers="idc049_c1 idc049_l2">1000 CAD</td>
<td headers="idc049_c2 idc049_l2">2000 CAD</td></tr>
</tbody>
</table>
<p><strong>Toronto</strong></p>
<p>Toronto est une ville très étendue. Les principaux quartiers résidentiels sont situés près du centre ville (Forest Hill, Rosedale et l’Annex) et dans le quartier des Beaches.</p>
<p>Le nombre de logements offerts à la location est assez faible, ce qui rend le prix des loyers assez élevé. Les biens à la vente sont plus nombreux, mais d’un coût comparable à celui de Paris. A titre indicatif, le prix moyen d’un bien immobilier à l’achat s’élevait en Ontario, d’après les chiffres de l’<a href="http://www.crea.ca/" class="spip_out" rel="external">Association canadienne de l’immobilier</a>, en avril 2008 à 314 041 CAD (avril 2007 : 299 796 CAD).</p>
<p>Les charges à ajouter au loyer comprennent l’eau, le chauffage (généralement au gaz), l’électricité, le téléphone, le réseau câblé et l’enlèvement des ordures ménagères. Le loyer de nombreux logements inclut tout ou partie des charges. Les charges représentent en moyenne 7,5 à 8 % du montant du loyer.</p>
<table class="spip" summary="">
<caption><strong>Exemples de loyers mensuels</strong></caption>
<thead><tr class="row_first"><th id="id41c0_c0"> </th><th id="id41c0_c1">Prix du m2</th><th id="id41c0_c2">Studio</th><th id="id41c0_c3">3 pièces</th><th id="id41c0_c4">5 pièces</th><th id="id41c0_c5">Villa</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<th headers="id41c0_c0" id="id41c0_l0">Forest Hill</th>
<td headers="id41c0_c1 id41c0_l0">20 à 30 CAD</td>
<td headers="id41c0_c2 id41c0_l0">1200 CAD</td>
<td headers="id41c0_c3 id41c0_l0">2500 CAD</td>
<td headers="id41c0_c4 id41c0_l0">3700 CAD</td>
<td headers="id41c0_c5 id41c0_l0">---</td></tr>
<tr class="row_even even">
<th headers="id41c0_c0" id="id41c0_l1">Quartier résidentiel</th>
<td headers="id41c0_c1 id41c0_l1">supérieur à 30 CAD</td>
<td headers="id41c0_c2 id41c0_l1">1500 CAD</td>
<td headers="id41c0_c3 id41c0_l1">2800 CAD</td>
<td headers="id41c0_c4 id41c0_l1">4500 CAD</td>
<td headers="id41c0_c5 id41c0_l1">4500 CAD</td></tr>
<tr class="row_odd odd">
<th headers="id41c0_c0" id="id41c0_l2">Banlieue</th>
<td headers="id41c0_c1 id41c0_l2">10 à 20 CAD</td>
<td headers="id41c0_c2 id41c0_l2">800 CAD</td>
<td headers="id41c0_c3 id41c0_l2">1500 CAD</td>
<td headers="id41c0_c4 id41c0_l2">2500 CAD</td>
<td headers="id41c0_c5 id41c0_l2">2500 CAD</td></tr>
</tbody>
</table>
<p><strong>Vancouver</strong></p>
<p>Les quartiers résidentiels se situent à l’ouest de la ville (West Vancouver).</p>
<p>Le marché de la location connaît une forte demande et les loyers sont, par conséquent, élevés. La recherche d’un logement peut prendre plusieurs semaines. Les animaux de compagnie sont rarement acceptés. Les logements sont généralement dotés d’une cuisine équipée et souvent d’un lave-linge et d’un sèche-linge. Les immeubles sont pourvus de parkings loués entre 75 et 100 CAD. La plupart des immeubles du centre ville sont équipés de salles de sport et de piscines.</p>
<table class="spip" summary="">
<caption><strong>Exemples de loyers mensuels</strong></caption>
<tbody>
<tr class="row_odd odd">
<th id="id78a5_l0"></th>
<td headers="id78a5_l0"><strong>Prix du m2</strong></td>
<td headers="id78a5_l0"><strong>Studio</strong> (45 à 50 m2)</td>
<td headers="id78a5_l0"><strong>3 pièces</strong> (80 m2)</td>
<td headers="id78a5_l0"><strong>Villa</strong></td></tr>
<tr class="row_even even">
<th id="id78a5_l1">Quartier résidentiel</th>
<td headers="id78a5_l1">27 CAD</td>
<td headers="id78a5_l1">1215 à 1350 CAD</td>
<td headers="id78a5_l1">2160 CAD</td>
<td headers="id78a5_l1">3500 à 3800 CAD</td></tr>
<tr class="row_odd odd">
<th id="id78a5_l2">Banlieue</th>
<td headers="id78a5_l2">20 CAD</td>
<td headers="id78a5_l2">900 à 1000 CAD</td>
<td headers="id78a5_l2">1600 CAD</td>
<td headers="id78a5_l2">2800 à 3500 CAD</td></tr>
</tbody>
</table>
<p><strong>Québec</strong></p>
<p>Le marché de la location est en ce moment plutôt favorable. Prenez votre temps pour découvrir les différents quartiers de la ville et vous décider. En attendant de trouver un logement, vous pouvez louer, à la semaine ou au mois, un appartement ou une chambre meublé. La période la plus favorable pour chercher un logement est d’avril à juin, les contrats de location expirant généralement au 30 juin de l’année.</p>
<p>Si vous avez besoin de conseils pour trouver un logement (type d’appartement recherché, droits et devoirs du locataire, etc.), vous pouvez vous adresser à l’association ROMEL :</p>
<p><a href="http://www.romel-montreal.ca/" class="spip_out" rel="external">Regroupement des organismes du Montréal ethnique pour le logement (ROMEL)</a><br class="manualbr">6555 Chemin Côtes-des-Neiges - bureau 400 <br class="manualbr">Montréal (Québec) H3S 2A6<br class="manualbr">Téléphone : [1] (514) 641 10 57 <br class="manualbr">Télécopie : [1] (514) 341 84 04<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/vie-pratique/article/logement#information#mc#romel-montreal.ca#" title="information..åt..romel-montreal.ca" onclick="location.href=mc_lancerlien('information','romel-montreal.ca'); return false;" class="spip_mail">Courriel</a></p>
<p>Cet organisme offre gratuitement ses services à tous les immigrants. Il possède une banque de données de logements par quartier. Il peut vous mettre en contact avec le propriétaire ou le concierge ou vous remettre une liste d’appartements susceptibles de vous intéresser avec les numéros de téléphone des personnes à contacter.</p>
<p><strong>Union française</strong><br class="manualbr">429 rue Viger Est <br class="manualbr">Montréal (Québec) H2L 2N9<br class="manualbr">Téléphone : [1] (514) 845 51 95 <br class="manualbr">Télécopie : [1] (514) 845 57 28<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/vie-pratique/article/logement#info#mc#unionfrancaise.ca#" title="info..åt..unionfrancaise.ca" onclick="location.href=mc_lancerlien('info','unionfrancaise.ca'); return false;" class="spip_mail">Courriel</a></p>
<p>L’Union française tient à jour depuis plusieurs années une banque de logements pour les nouveaux arrivants.</p>
<p>Vous pouvez également éplucher les petites annonces des journaux, notamment le mercredi et le samedi :</p>
<ul class="spip">
<li><a href="http://www.cyberpresse.ca/" class="spip_out" rel="external">Cyberpresse</a> (le site des quotidiens La Presse, Le Soleil, La Tribune, La Voix de L’Est, Le Droit, Le Quotidien, Le Nouvelliste).</li>
<li><a href="http://www.ledevoir.com/" class="spip_out" rel="external">Le Devoir</a></li>
<li><a href="http://www.canada.com/montrealgazette/index.html" class="spip_out" rel="external">The Gazette</a></li>
<li><a href="http://www.canoe.com/journaldemontreal/" class="spip_out" rel="external">Le Journal de Montréal</a></li>
<li><a href="http://fr.canoe.ca/journaldequebec/" class="spip_out" rel="external">Le Journal de Québec</a></li>
<li><a href="http://www.voir.ca/" class="spip_out" rel="external">Voir</a>, hebdomadaire gratuit publié tous les jeudis</li></ul>
<p>Vous pouvez également consulter les sites Internet spécialisés.</p>
<p><strong>Lexique du logement au Québec</strong></p>
<ul class="spip">
<li>1 1/2 ; 2 1/2 ; 3 1/2 etc. : le nombre de pièces de l’appartement, dont la cuisine, et la salle de bain (demi-pièce).</li>
<li>Equipé ou semi-meublé : signifie que l’équipement électroménager de base (cuisinière et réfrigérateur) est fourni et inclus dans le loyer.</li>
<li>Chauffé ou non chauffé : signifie que les frais de chauffage sont compris ou pas dans le loyer.</li></ul>
<p><strong>Coût d’un logement</strong></p>
<p>Il dépendra du quartier choisi. A titre d’exemple, à Montréal, le Plateau Mont-Royal, Rosemont, Mile End sont plus chers que les quartiers d’Hochelaga</p>
<p>Maisonneuve, de Verdun ou de Saint-Michel. À Québec, il y a des différences de prix entre les quartiers de la Haute Ville et ceux de la Basse Ville.</p>
<p>Les quartiers résidentiels les plus proches du centre ville sont Outremont (francophone), Westmount (anglophone), Notre Dame de Grâce, Hampstead, Ville Mont-royal, Plateau Mont-Royal.</p>
<p>Il convient de compter 1100$ pour un studio en quartier résidentiel, 1700$ pour un trois pièces, 2100$ pour un cinq pièces, et environ 5000$ pour une villa).</p>
<p><strong>Annonces immobilières</strong></p>
<p>Vous en trouverez :</p>
<ul class="spip">
<li>en consultant la presse et les sites Internet ;</li>
<li>en vous rendant dans les quartiers de votre choix afin d’y repérer les annonces placées par certains propriétaires ;</li>
<li>en contactant les associations d’accueil et les organismes d’aide aux immigrants. En cas de doute ou d’incompréhension sur les termes du bail, n’hésitez pas à les consulter (consultation gratuite) ;</li>
<li>sur le campus universitaire si vous venez étudier au Canada.</li></ul>
<p>Si vous venez au Canada pour y travailler, votre futur employeur pourra vous aider à trouver un logement.</p>
<p>Les Canadiens déménageant le plus souvent au printemps ou en été, ce sont les meilleures saisons pour rechercher un logement puisque le choix est plus vaste.</p>
<h3 class="spip"><a id="sommaire_2"></a>Conditions de location</h3>
<p>Qu’il s’agisse d’une location ou d’un achat, les prix de l’immobilier varient fortement selon la province, la ville, le quartier, le type et la surface du logement et la proximité des services (commerces et transports en commun). Le logement est moins cher à l’extérieur des grandes villes. Au Canada, 35% à 50% des revenus sont consacrés à l’habitat (loyer, charges et services publics). Généralement, les locations sont gérées directement par les propriétaires par le biais de petites annonces, mais certaines peuvent être proposées par des agences immobilières (<i>real estate agencies</i>). La commission d’agence s’élève à 10% du loyer annuel et est généralement à la charge du propriétaire. Le marché locatif est réduit.</p>
<p>Les baux sont en général conclus pour un an renouvelable et le loyer est souvent payable d’avance (un ou deux mois). Le propriétaire peut exiger la remise de chèques postdatés pour la durée du bail. Le propriétaire peut demander des renseignements personnels et est autorisé à vérifier la solvabilité du locataire.</p>
<p>En guise de garantie, le règlement du dernier mois de loyer vous sera également demandé, sauf dans la province de Québec.</p>
<p>Enfin, au Canada, le contrôle de la hausse des loyers est assuré par l’administration municipale et provinciale. En Ontario, par exemple, il s’applique aux contrats de location en cours. Toutefois, en cas de changement de locataire, le propriétaire est autorisé à augmenter autant qu’il le souhaite le loyer. Dès qu’un contrat est conclu entre un locataire et un propriétaire, il est assujetti à nouveau au contrôle de la hausse des loyers.</p>
<p><strong>Au Québec : </strong></p>
<p><strong>Contrat de location (bail)</strong></p>
<p>Le bail doit être écrit. Ne cherchez pas à le rédiger vous-même. Il existe des contrats de location types que vous pourrez vous procurer chez les <i>dépanneurs</i> (épiceries). Le bail doit préciser la durée de location et le montant du loyer à verser le 1er jour de chaque mois. Le bail expire généralement au 30 juin de l’année. Vous pouvez cependant choisir, en accord avec votre propriétaire, une autre date pour la fin du bail.</p>
<p>Un propriétaire n’a pas le droit d’exiger le paiement d’une caution, votre numéro d’assuré social ou le nom de votre employeur. De plus, il ne peut refuser un logement au motif que vous n’avez pas d’emploi ou que vous avez des enfants. Votre logeur ne peut exiger des chèques postdatés. La pratique est courante au Québec, mais parfaitement illégale. Votre propriétaire n’est pas obligé d’accepter un chèque personnel et peut vous demander de payer en espèces. Exigez alors un reçu. En cas d’abus ou de litige avec votre propriétaire, vous pouvez porter plainte auprès de la <strong>Régie du logement</strong>. Celle-ci assure la protection des locataires. Elle informe également propriétaires et locataires sur leurs droits et obligations respectifs et, à défaut de règlement à l’amiable, tranche les litiges.</p>
<p>S’il est difficile de négocier les prix des loyers dans les quartiers de Montréal les plus recherchés (Plateau Mont-Royal, Rosemont ou Mile-End), vous pouvez encore le faire dans quelques quartiers éloignés du centre-ville.</p>
<p>De nombreux appartements sont loués avec réfrigérateur et cuisinière. Il se peut que l’immeuble dispose au sous-sol d’une buanderie équipée d’une machine à laver et d’un sèche linge payants accessibles à tous les locataires.</p>
<p>Le mode de chauffage est l’un des premiers éléments que devrez prendre en compte avant de signer un bail. Le chauffage est parfois compris dans le loyer. Cette formule présente cependant quelques inconvénients : d’une part, certains propriétaires chauffent très peu les appartements afin de faire des économies. D’autre part, l’augmentation annuelle de votre loyer pourra être plus élevée si le logement est loué chauffé. Si l’appartement où vous vivez est isolé, la note de chauffage peut être élevée. Renseignez-vous auprès des voisins pour vous faire une idée du coût annuel du chauffage.</p>
<p>Vérifiez si le logement est proche de toutes les commodités : commerces, garderie, laverie, poste, supermarchés, écoles, etc. La proximité d’un arrêt d’autobus ou d’une station de métro est primordiale, notamment l’hiver.</p>
<p><strong>La colocation</strong></p>
<p>La colocation est très courante au Québec. Un colocataire est un locataire qui loue un logement, par bail écrit ou verbal, avec un ou plusieurs autres locataires. Conclure un bail en colocation, comporte des avantages, notamment au niveau financier : le partage du loyer à payer et des autres dépenses (chauffage, électricité, téléphone, etc.). La colocation offre, de plus, la possibilité d’occuper un plus grand logement, de partager les tâches, etc.</p>
<p>Par contre, il peut arriver qu’un colocataire décide de quitter le logement à la fin du bail ; celui qui reste devra obligatoirement en assumer seul la responsabilité, notamment pour le paiement du loyer.</p>
<p>Pour éviter toute confusion et faciliter la preuve en cas de litige, il est recommandé que le bail et toute autre convention soient faits par écrit. Pour mieux se protéger, il est conseillé de conclure par écrit une convention de colocation où sont réglés les aspects pratiques de la cohabitation, tels : propriété et usage des meubles, assurance-responsabilité, droit à la cession ou sous-location et leurs modalités, jouissance du logement, jouissance exclusive de pièces comme une chambre à coucher, partage de tâches, répartition du loyer (exemple : 60% - 40%) et des autres coûts, tels chauffage, électricité, téléphone, câblodistribution.</p>
<p>Les colocataires s’engagent dans une relation juridique à la fois entre eux et avec un propriétaire commun. Ils doivent être prêts à envisager certains désagréments comme le défaut par l’un d’eux d’exécuter ses obligations.</p>
<p><strong>Quitter un logement</strong></p>
<p>Le locataire dispose généralement du droit au maintien dans les lieux. Ce qui signifie que votre propriétaire ne peut vous expulser comme bon lui semble. Lorsque vous quittez votre logement, vous devez aviser par écrit votre propriétaire trois mois avant la fin du bail.</p>
<p>Si vous dénoncer un bail avant son échéance sans motif valable, le propriétaire est en droit de vous demander de payer le loyer jusqu’à ce qu’il ait trouvé un autre locataire. Dans ce cas, la sous-location de votre appartement est une solution possible. Mais assurez-vous du sérieux de votre sous-locataire, car vous serez tenu responsable des dégâts éventuels que celui-ci causera. Vous pourriez même être dans l’obligation de payer le loyer s’il ne le fait pas.</p>
<p>Une autre solution est la cession de bail. Vous êtes alors libéré de toutes les obligations du bail et de toutes vos responsabilités envers le propriétaire.</p>
<p><strong>Type d’habitation </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Chambre<br class="manualbr">Il s’agit de maisons divisées en plusieurs chambres privées louées à des personnes qui partagent cuisine et salle de bains.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Appartement ou condo (condominium)<br class="manualbr">Il s’agit d’appartements d’une à trois chambres à coucher. Les studios sont constitués d’un espace de vie qui sert également de chambre à coucher.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Copropriété<br class="manualbr">Ce terme désigne un appartement faisant partie d’un complexe où chacun est propriétaire de son appartement. Certains propriétaires choisissent d’habiter leur logement, d’autres choisissent de la louer.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Duplex et triplex<br class="manualbr">Ce terme désigne des maisons divisées en deux (duplex), voire trois unités (triplex). Le propriétaire de la maison occupe souvent une des unités et offre l’autre, ou les autres, en location.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Maison mitoyenne<br class="manualbr">Ce terme désigne une rangée de trois maisons ou plus possédant un mur mitoyen. Ces maisons peuvent aussi posséder plusieurs étages, ce qui signifie que les deux étages supérieurs constituent une unité distincte des deux étages inférieurs.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Maison individuelle neuve ou traditionnelle de ville (de type géorgien ou victorien)<br class="manualbr">Ce sont des maisons spacieuses, bien équipées, avec jardin et terrasse en bois (deck) et situées dans des quartiers résidentiels récents ou anciens.</p>
<p><strong>Louer un appartement</strong></p>
<p>L’appartement est le type de logement le plus courant. Les grands appartements sont rares. Ils sont loués vides avec cuisine équipée. Il est cependant possible de trouver des appartements meublés. Les prix varient fortement d’une ville à l’autre. A Vancouver, les prix de l’immobilier, traditionnellement élevés, ont connu une envolée en raison des Jeux Olympiques d’hiver. Toronto est également réputée pour la cherté de ses logements.</p>
<p><strong>Acheter un bien immobilier</strong></p>
<p>Au Canada, les maisons, mais surtout les terrains, coûtent cher. Au troisième trimestre 2010, le prix moyen d’une maison individuelle de plain-pied s’établit à 324 531 $. Durant la même période, le prix des maisons standard à deux étages a atteint 360 329 $, et celui des appartements en copropriété, 226 481 $.</p>
<p>Bon nombre de nouveaux arrivants commencent par louer un logement jusqu’à ce que leurs revenus soient assurés. Il est toutefois possible d’acheter un bien immobilier dès son arrivée au Canada. Bien que le coût mensuel (<i>mortgage</i>) lié à l’achat d’une maison soit plus élevé que celui d’une location, la plupart des Canadiens considèrent l’achat d’une maison comme un but important dans la vie.</p>
<p><strong>Recherche</strong></p>
<p>Vous devez avoir recours aux services d’un agent immobilier. Celui-ci est rémunéré par une commission sur le prix de vente de la maison et ses services sont gratuits pour l’acheteur.</p>
<h3 class="spip"><a id="sommaire_3"></a>Hôtels</h3>
<p><strong>Hôtel</strong></p>
<p>Certains établissements proposent des locations hebdomadaires et mensuelles.</p>
<p><strong>Auberge de Jeunesse</strong></p>
<p>Il existe de nombreux "Bed and Breakfast" (B&amp;B), ainsi que les foyers de jeunes (YMCA, YWCA, etc.).</p>
<p><strong>Autres formes de logement</strong></p>
<p>Les nouveaux arrivants peuvent être hébergés chez un “ parrain ”. Vous pouvez vous renseigner sur place auprès des associations et des organismes d’accueil des immigrants.</p>
<p>La colocation (<i>flatshare</i>) est assez répandue et idéale pour se familiariser rapidement avec la vie locale et le pays, tout en sachant qu’il faudra peut-être jusqu’à deux mois de recherche avant de pouvoir emménager dans le logement que vous aurez choisi de louer.</p>
<p><strong>Logements temporaires à Montréal</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Résidence Maria-Goretti</strong> (réservée aux femmes)<br class="manualbr">3333, côte Sainte-Catherine <br class="manualbr">Montréal (Québec) H3T 1C8<br class="manualbr">Téléphone : [1] (514) 731 11 61<br class="manualbr">Chambres étudiantes et repas à prix modique.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.collegefrancais.ca/" class="spip_out" rel="external">Collège Français</a> (de mai à août)<br class="manualbr">185, rue Fairmount Ouest <br class="manualbr">Montréal (Québec) H2T 2M6<br class="manualbr">Téléphone : [1] (514) 495 25 81 <br class="manualbr">Télécopie : [1] (514) 271 28 23<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/vie-pratique/article/logement#info#mc#collegefrancais.ca#" title="info..åt..collegefrancais.ca" onclick="location.href=mc_lancerlien('info','collegefrancais.ca'); return false;" class="spip_mail">Courriel</a> </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Résidence des étudiants de l’Université Mc Gill (de la mi-mai à la mi-août)<br class="manualbr">3935, rue de l’Université <br class="manualbr">Montréal (Québec) H3A 2B4<br class="manualbr">Téléphone : 514 398 52 00 <br class="manualbr">Télécopie : [1] (514) 398 67 70</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.residences-etu.ca/" class="spip_out" rel="external">Résidence de l’Université de Montréal</a> (de mai à août)<br class="manualbr">2350, rue Édouard-Montpetit <br class="manualbr">Montréal (Québec) H3T 1J4<br class="manualbr">Téléphone : [1] (514) 343 65 31 <br class="manualbr">Télécopie : [1] (514) 343 23 53<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/vie-pratique/article/logement#residences#mc#umontreal.ca#" title="residences..åt..umontreal.ca" onclick="location.href=mc_lancerlien('residences','umontreal.ca'); return false;" class="spip_mail">Courriel</a></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Résidence de l’Université Concordia (de mai à août)<br class="manualbr">Hingston Hall HA 150 - 7141, rue Sherbrooke Ouest <br class="manualbr">Montréal (Québec) H4B 1R6<br class="manualbr">Téléphone : [1] (514) 848 24 24 poste 4758<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/vie-pratique/article/logement#sumres#mc#alcor.concordia.ca#" title="sumres..åt..alcor.concordia.ca" onclick="location.href=mc_lancerlien('sumres','alcor.concordia.ca'); return false;" class="spip_mail">Courriel</a> </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.sres.ulaval.ca/" class="spip_out" rel="external">Résidence de l’Université Laval</a> (de mai à août) <br class="manualbr">Service des résidence de l’Université Laval<br class="manualbr">Local 1604 - Pavillon Alphonse-Marie Parent - Cité universitaire - Sainte-Foy <br class="manualbr">Québec (Québec) G1K 7P4<br class="manualbr">Téléphone : [1] (418) 656 29 21 <br class="manualbr">Télécopie : [1] (418) 656 28 01<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/vie-pratique/article/logement#sres#mc#sres.ulaval.ca#" title="sres..åt..sres.ulaval.ca" onclick="location.href=mc_lancerlien('sres','sres.ulaval.ca'); return false;" class="spip_mail">Courriel</a></p>
<h3 class="spip"><a id="sommaire_4"></a>Electricité</h3>
<p>Le courant électrique est de 110 volts pour une fréquence de 60 hertz.</p>
<p>Les prises de courant sont de type américain avec deux broches planes parallèles et une broche ronde pour la mise à la terre.</p>
<p>Il faut donc prévoir des adaptateurs en vente sur place, mais aussi en France. Des transformateurs (1500 watts) permettent d’utiliser les appareils européens en 220 volts ou tout autre appareil d’un voltage différant de celui utilisé au Canada. Les ordinateurs intègrent les deux voltages.</p>
<p>Les ampoules sont à vis.</p>
<p><strong>Chauffage - Climatisation</strong></p>
<p>Les systèmes de chauffage et d’isolation sont de bonne qualité. Les moyens de chauffage utilisés sont, pour les maisons, le fuel, l’électricité ou le gaz et, pour les appartements, l’électricité. La climatisation est souhaitable, mais pas indispensable, à Ottawa et Toronto durant les deux mois d’été très chauds et humides.</p>
<p>La sécheresse de l’air dans les maisons et les appartements, très chauffés l’hiver, peut poser problème pour les occupants, mais aussi pour un mobilier fragile ou ancien. Il est vivement conseillé d’installer un humidificateur. Enfin, en hiver, il est recommandé dans les régions froides de ne jamais éteindre le chauffage en cas d’absence prolongée du domicile et d’entretenir la chaudière (révision et changement des filtres) avant l’hiver.</p>
<h3 class="spip"><a id="sommaire_5"></a>Eau</h3>
<p>Le site <a href="http://www.hc-sc.gc.ca/" class="spip_out" rel="external">Santé Canada</a> du gouvernement canadien vous informe sur la qualité de l’eau au Canada.</p>
<h3 class="spip"><a id="sommaire_6"></a>Electroménager</h3>
<p>L’équipement en électroménager fait partie des priorités des ménages canadiens.</p>
<p>Les cuisines sont toujours bien équipées. Tout l’équipement électroménager nécessaire est disponible sur place avec un large choix dans les marques américaines et, pour certains équipements, à des prix plus attractifs qu’en France.</p>
<p><strong>Equipement vidéo</strong></p>
<p>Le système de télévision adopté au Canada est le NTSC, alors qu’en France nous utilisons le système SECAM.</p>
<p>En ce qui concerne les DVD, le Canada se trouve en zone 1 et la France en zone 2. Il est donc conseillé d’acquérir un lecteur permettant de lire les disques de toutes les zones.</p>
<p><strong>Sécurisation</strong></p>
<p>Dans les grandes villes, il est préférable d’équiper sa maison d’un système d’alarme et de protection contre le vol. Dans certains quartiers résidentiels, il n’est pas rare de voir des panneaux de signalisation portant la mention <i>watch your neighbour</i>. Il s’agit d’un programme de surveillance de quartier et plus globalement de lutte contre la criminalité mis en place par la Police communautaire. Il s’agit de sensibiliser le voisinage à l’entraide mutuelle, c’est-à-dire à se connaître et à garder discrètement un œil ouvert sur les allers et venues suspects. Au moment de l’emménagement dans un quartier, le bon réflexe est de se présenter auprès de ses voisins.</p>
<p>Si le Canada autorise le port d’armes, la fréquence des agressions avec une arme à feu est six fois inférieure à celle des Etats-Unis.</p>
<p>Pour plus d’information, vous pouvez prendre contact avec le poste de police communautaire de votre quartier et consulter le site Internet de la <a href="http://www.rcmp.ca/" class="spip_out" rel="external">Gendarmerie royale du Canada</a>.</p>
<p><i>Mise à jour : avril 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/vie-pratique/article/logement). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
