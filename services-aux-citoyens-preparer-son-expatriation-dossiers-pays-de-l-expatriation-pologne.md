# Pologne

<p>Au <strong>31 décembre 2014</strong>,<strong> 6247 Français</strong> étaient enregistrés auprès de la section consulaire de l’ambassade de France à Varsovie. Cependant la part des binationaux dans cette communauté étant de 50%, on peut estimer que le pays compte quelque 10 000 personnes détentrices de la nationalité française.</p>
<p>Les échanges commerciaux franco-polonais ont doublé en dix ans, passant d’un volume total de 7,5 Mds € en 2004 à 14,7 Mds € en 2013 selon les données des douanes françaises.</p>
<p>On citera, parmi les nombreuses entreprises françaises implantées sur le marché polonais, Alstom, EDF, Dakia, France Telecom, Canal +, Accor, Société Générale, BNP Paribas, Crédit agricole, Michelin, Valeo, Carrefour, Auchan, Leclerc, Danone, L’Oreal, Saint Gobain et Lafarge.</p>
<p>L’économie polonaise, engagée depuis la chute du régime communiste dans un vaste processus de transformation et de modernisation, converge progressivement vers ses voisins d’Europe occidentale. Selon les données de la Commission européenne, la Pologne a connu entre 2004 et 2013 une croissance moyenne de 4,1% qui n’a été surpassée, au sein des 28, par la Slovaquie (4,2%).</p>
<p>Depuis le début du XXème siècle, les échanges humains entre la France et la Pologne n’ont jamais cessé. Par ailleurs, la libéralisation du pays depuis 1989 et plus encore son adhésion à l’Union européenne en 2004, ont abouti à une société ouverte et moderne, favorable à l’expatriation d’un Français. Celui qui désire venir s’installer dans ce pays ne rencontre, une fois acquise la connaissance de la langue nationale et/ou de la culture du pays, aucune difficulté d’intégration. La région de Varsovie, particulièrement développée aux plans économique et culturel, attire à elle seule plus de la moitié des familles expatriées recensées.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/dossiers-pays/pologne/" class="spip_in">Une description de la Pologne, de sa situation politique et économique</a></li>
<li><a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/pologne/" class="spip_in">Des informations actualisées sur les conditions locales de sécurité en Pologne</a></li>
<li><a href="http://www.ambafrance-pl.org/" class="spip_out" rel="external">Ambassade de France en Pologne</a></li></ul>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-pologne-entree-et-sejour.md">Entrée et séjour</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-pologne-entree-et-sejour-article-passeport-visa-permis-de-travail.md">Passeport, visa, permis de travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-pologne-entree-et-sejour-article-demenagement-114276.md">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-pologne-entree-et-sejour-article-vaccination-114277.md">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-pologne-entree-et-sejour-article-animaux-domestiques-114279.md">Animaux domestiques</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-pologne-emploi-stage.md">Emploi, stage</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-pologne-emploi-stage-article-marche-du-travail-114280.md">Marché du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-pologne-emploi-stage-article-reglementation-du-travail-114281.md">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-pologne-emploi-stage-article-recherche-d-emploi-114282.md">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-pologne-emploi-stage-article-stages.md">Stages</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-pologne-protection-sociale.md">Protection sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-pologne-protection-sociale-article-regime-local-de-securite-sociale-114287.md">Régime local de sécurité sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-pologne-protection-sociale-article-convention-de-securite-sociale.md">Convention de sécurité sociale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-pologne-fiscalite-23535.md">Fiscalité</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-pologne-fiscalite-23535-article-fiscalite-du-pays-114289.md">Fiscalité du pays</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-pologne-fiscalite-23535-article-convention-fiscale-114290.md">Convention fiscale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-pologne-vie-pratique.md">Vie pratique</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-pologne-vie-pratique-article-logement-114291.md">Logement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-pologne-vie-pratique-article-sante.md">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-pologne-vie-pratique-article-scolarisation.md">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-pologne-vie-pratique-article-cout-de-la-vie-114294.md">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-pologne-vie-pratique-article-transports.md">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-pologne-vie-pratique-article-communications.md">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-pologne-vie-pratique-article-loisirs-et-culture.md">Loisirs et culture</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-pologne-presence-francaise.md">Présence française</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-pologne-presence-francaise-article-presence-francaise.md">Présence française</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
