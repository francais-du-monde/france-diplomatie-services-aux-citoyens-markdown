# Convention de sécurité sociale

<p>Membre de l’Union européenne, la Slovénie est liée à la France par <a href="http://www.cleiss.fr/docs/index.html" class="spip_out" rel="external">un certain nombre d’accords de sécurité sociale</a> dont les principaux sont les règlements C.E.E. n°1408/71 et 574/72, relatifs à l’application des régimes de sécurité sociale aux travailleurs salariés et non salariés et aux membres de leur famille qui se déplacent à l’intérieur de la Communauté.</p>
<p>Dans le cadre de l’accord de l’Espace économique européen (E.E.E.) et de l’accord entre l’Union européenne et la Suisse, les règlements communautaires n°1408/71 et 574/72 sont applicables aux territoires et aux ressortissants de l’Islande, de la Norvège, du Liechtenstein et de la Suisse.</p>
<p>Les Français occupés en Slovénie relèvent, en principe, obligatoirement du régime slovène de protection sociale. Ils bénéficient, par ailleurs, des règlements communautaires de sécurité sociale leur permettant, en quelque sorte, le passage du régime français au régime slovène de sécurité sociale et réciproquement.</p>
<p>Les Français occupés en Slovénie peuvent, s’ils le désirent, adhérer à l’assurance volontaire « expatriés » auprès de la Caisse des Français de l’étranger. Il convient de préciser qu’une telle adhésion ne dispense pas les intéressés des obligations d’assurance existant dans le pays de travail.</p>
<p>Les Français travaillant en Slovénie peuvent aussi être maintenus au régime français de protection sociale, c’est-à-dire détachés dans le cadre des seuls règlements communautaires de sécurité sociale.</p>
<p>Bien entendu, les Français se trouvant en Slovénie en tant que touristes, étudiants, retraités ou chômeurs cherchant un emploi peuvent bénéficier également des règlements communautaires.</p>
<p>Tout renseignement complémentaire en matière de sécurité sociale peut être obtenu auprès du :</p>
<p><a href="http://www.cleiss.fr/docs/textes/rgt_presentation1.html" class="spip_out" rel="external">Centre des liaisons européennes et internationales de sécurité sociale (CLEISS)</a> <br class="manualbr">11, rue de la tour des Dames <br class="manualbr">75436 PARIS Cedex 09 <br class="manualbr">Tél : 01 45 26 33 41 <br class="manualbr">Télécopie : 01 49 95 06 50</p>
<p><i>Mise à jour : septembre 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/protection-sociale-23574/article/convention-de-securite-sociale-114775). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
