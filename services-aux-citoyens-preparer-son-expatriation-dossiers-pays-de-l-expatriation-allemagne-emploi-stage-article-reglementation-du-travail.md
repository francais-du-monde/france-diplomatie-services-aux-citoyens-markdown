# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/emploi-stage/article/reglementation-du-travail#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/emploi-stage/article/reglementation-du-travail#sommaire_2">Fêtes légales</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/emploi-stage/article/reglementation-du-travail#sommaire_3">Emploi du conjoint</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/emploi-stage/article/reglementation-du-travail#sommaire_4">Création d’entreprises – spécificités</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<p><strong>Règles de droit local</strong></p>
<p>Il n’existe pas de Code du travail en Allemagne, mais une série de lois en la matière. Vous pouvez obtenir des informations concernant le droit du travail et les lois en vigueur sur les sites suivants :</p>
<ul class="spip">
<li><a href="http://www.bmas.de/" class="spip_out" rel="external">ministère fédéral du Travail et des Affaires sociales</a> ;</li>
<li><a href="http://www.gesetze-im-internet.de" class="spip_out" rel="external">ministère fédéral de la Justice</a> (<i>Gesetze im Internet</i>) pour retrouver un texte de loi ;</li>
<li><a href="http://www.bundesarbeitsgericht.de/" class="spip_out" rel="external">tribunal fédéral du Travail</a> (<i>Bundesarbeitsgericht</i>)</li>
<li><a href="http://ec.europa.eu/eures/" class="spip_out" rel="external">EURES</a>  (site de la Commission européenne) rubrique "Vivre et travailler Allemagne" ;</li>
<li><a href="http://www.connexion-emploi.com/" class="spip_out" rel="external">Connexion-emploi.com</a> (site privé) / rubrique "Infos" ;</li>
<li><a href="http://www.arbeitsrecht.de/" class="spip_out" rel="external">Arbeitsrecht.de</a> (site privé) rubrique "Rechtkompakt"</li>
<li><a href="http://www.arbeitsrecht-beratung.de/" class="spip_out" rel="external">Arbeitsrecht-beratung.de</a> (site privé)</li>
<li>syndicat <a href="http://www.igmetall.de/" class="spip_out" rel="external">IG Metall</a> rubrique "Ratgeber  Bildung". Il est toujours important de demander s’il existe une convention collective ou une convention d’établissement régissant les salaires et conditions de travail dans l’entreprise.</li></ul>
<p>Les droits du comité d’entreprise, qui peut être créé dans toute entreprise de plus de 5 salariés, sont importants en Allemagne car celui-ci peut intervenir dans les décisions de recrutement ou de licenciement. S’il en existe un dans votre société et que des questions se posent après votre recrutement sur votre contrat de travail, il ne faut pas négliger de le consulter car il peut vous aider à faire valoir vos droits auprès de votre employeur.</p>
<p>Les documents suivants sont à fournir à l’employeur au moment de l’embauche. Si vous arrivez directement de France, ceux-ci seront à établir dans les premiers jours par l’intermédiaire du service du personnel de l’entreprise ou individuellement auprès des organismes allemands compétents :</p>
<ul class="spip">
<li>carte de retenue de l’impôt sur le revenu (auprès de la mairie du domicile) ;</li>
<li>carte d’assuré social (envoyée par la caisse auprès de laquelle vous cotiserez) ;</li>
<li>décompte des congés payés ;</li>
<li>certificats et attestations de travail pour les précédents emplois occupés ;</li>
<li>éventuellement extrait du casier judiciaire, selon l’activité exercée.</li></ul>
<p><strong>La période d’essai </strong></p>
<p>La période d’essai n’est pas obligatoire, mais <strong>contractuelle</strong>.</p>
<p>Dans le cadre d’un contrat de travail à durée indéterminée, la période d’essai est <strong>de 6 mois au plus</strong>. Dans la pratique, la période complète des 6 mois est souvent demandée pour des employés. La période d’essai peut également se présenter sous la forme d’un contrat à durée déterminée.</p>
<p>Pendant la période d’essai, l’une ou l’autre des deux parties contractantes peut résilier le contrat avec un préavis de deux semaines.</p>
<p><strong>Le salaire</strong></p>
<p>En Allemagne, il n’existe <strong>pas de salaire minimum</strong> légal, équivalent au SMIC en France. Néanmoins si une convention collective s’applique, l’employé peut prétendre au salaire conventionnel.</p>
<p>S’il s’agit d’un contrat hors convention collective, les deux parties conviennent librement du montant du salaire, des primes supplémentaires et des éventuelles réévaluations.</p>
<p>Toute discrimination est interdite entre les salariés hommes et femmes, ainsi qu’entre les salariés à temps plein et ceux à temps partiel.</p>
<p><strong>L’impôt sur le revenu est retenu à la source</strong>, c’est à dire prélevé chaque mois par l’employeur sur le salaire. Celui-ci doit donc verser son salaire à l’employé à la date prévue dans le contrat et lui remettre un bulletin de paie mentionnant le salaire brut et les retenues effectuées au titre de l’impôt sur le revenu et des cotisations sociales.</p>
<p><strong>Les horaires de travail</strong></p>
<p>La loi sur les horaires de travail fixe la durée de travail maximale à <strong>8 heures par jour</strong> <strong>ouvrable</strong> (du lundi au samedi inclus). Après négociation entre l’employeur et l’employé, cette durée peut être prolongée jusqu’à 10 heures par jour, mais avec des périodes de compensation.</p>
<p>Dans la pratique, la durée moyenne hebdomadaire de travail est <strong>de 40 heures</strong>. La convention collective du service public en Allemagne fixe le temps de travail hebdomadaire à 42 heures maximum et nombre d’entreprises s’alignent sur ce temps de travail. Le recours aux horaires aménagés est également répandu.</p>
<p>En comparaison avec la France et Paris, la journée de travail commence et se termine plus tôt en Allemagne.</p>
<p>Une <strong>pause de 30 minutes minimum</strong> doit être accordée à l’employé travaillant plus de 6 heures (45 minutes au delà de 9 heures de travail).</p>
<p><strong>Les heures supplémentaires</strong></p>
<p>Généralement, l’autorisation et la rémunération des heures supplémentaires sont réglementées par les conventions collectives ou d’entreprise.</p>
<p>L’employeur n’a, en principe, pas le droit d’exiger de ses salariés qu’ils effectuent des heures supplémentaires, sauf en cas d’urgence non prévisible mettant en danger les intérêts vitaux de l’entreprise.</p>
<p>En revanche, s’il est stipulé dans le contrat de travail que le salarié s’engage à effectuer des heures supplémentaires à la demande de l’employeur (dans l’intérêt de l’entreprise), il ne peut refuser d’en effectuer sous peine d’avertissement, voire de licenciement.</p>
<p>Dans ce dernier cas, veillez à ce que soit mentionné dans votre contrat la rémunération de vos heures supplémentaires : soit par une majoration de salaire, soit par l’octroi d’un repos compensateur.</p>
<p><strong>Les congés payés</strong></p>
<p>Après six mois de travail, vous avez droit à prendre en principe l’intégralité des congés payés annuels.</p>
<p>La loi fixe à <strong>24 jours ouvrables</strong> le nombre de jours de congé par année civile. La plupart des entreprises accordent cependant quelques jours de congés supplémentaires sur la base de conventions collectives ou d’une convention d’entreprise, ce qui porte généralement ce nombre de 25 à 30 jours (soit 5 à 6 semaines par an).</p>
<p>En cas de report de jours de congés sur l’année suivante, ceux-ci doivent être pris avant le 31 mars.</p>
<p><strong>Congé pour maladie, congé de maternité et congé parental d’éducation</strong></p>
<p>Tout empêchement doit être signalé sans délai à l’employeur.</p>
<p><strong>En cas de maladie</strong>, un certificat médical d’arrêt de travail doit lui être remis <strong>dans les trois jours.</strong> Passé ce délai, les jours de maladie seront décomptés des congés payés.</p>
<p><strong>Le congé de maternité</strong> est accordé sur la période suivante :</p>
<ul class="spip">
<li>6 semaines avant l’accouchement (sauf si l’employée y renonce) ;</li>
<li>8 semaines après l’accouchement. Il peut néanmoins être prolongé avant ou après l’accouchement sur avis médical.</li></ul>
<p>Certains travaux sont interdits pendant la grossesse, ainsi que le travail de nuit et pendant les dimanches et jours fériés. La durée de travail ne peut excéder <strong>8 heures 30 par jour</strong> (ou 80 heures sur deux semaines).</p>
<p>Enfin, l’employée ne peut être licenciée pendant la durée de sa grossesse et dans les 4 mois suivant l’accouchement.</p>
<p>La naissance d’un enfant donne droit à un <strong>congé parental d’éducation</strong>, à la condition que les deux parents travaillent.</p>
<p>Une prime d’éducation (<i>Elterngeld</i>) peut être obtenue auprès du service des affaires sociales (<i>Amt fürVersorgung und Soziales</i>) du lieu de résidence. Cette prime s’élève à 67 % du dernier salaire net. Elle ne peut être inférieure à 300 euros, ni supérieure à 1 800 euros. Elle peut être perçue pendant 14 mois au total.</p>
<p>La durée du congé parental d’éducation est, selon le souhait du salarié, <strong>de trois ans au plus</strong>. Le congé peut débuter à n’importe quel moment avant le troisième anniversaire de l’enfant, avec un <strong>préavis de quatre semaines</strong>.</p>
<p>La mère et le père peuvent bénéficier du congé car trois changements de bénéficiaire sont possibles. Le prolongement du congé n’est possible qu’avec l’accord de l’employeur ou si le changement de bénéficiaire est impossible.</p>
<p>Enfin, il y a interdiction de licenciement pendant le congé sans autorisation administrative.</p>
<p><strong>Le préavis</strong></p>
<p>Dans le cas de rupture du contrat de travail, les délais légaux de préavis sont les suivants :</p>
<ul class="spip">
<li>pendant la période d’essai et pour un contrat d’une durée inférieure ou égale à six mois : 14 jours minimum ;</li>
<li>à partir de sept mois de travail : quatre semaines minimum avant le 15 ou la fin du mois.</li></ul>
<p>Attention : l’employeur peut prévoir d’autres délais dans le contrat de travail, comme par exemple la fin d’un trimestre. Le délai en cas de démission ne peut cependant être plus long qu’en cas de licenciement.</p>
<p>Dans le cas d’un licenciement, ce délai de base augmente <strong>en fonction de l’ancienneté</strong> du salarié dans l’entreprise :</p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<th id="id568c_l0">2 ans</th>
<td headers="id568c_l0">1 mois à la fin d’un mois calendaire</td></tr>
<tr class="row_even even">
<th id="id568c_l1">5 ans</th>
<td headers="id568c_l1">2 mois à la fin d’un mois calendaire</td></tr>
<tr class="row_odd odd">
<th id="id568c_l2">8 ans</th>
<td headers="id568c_l2">3 mois à la fin d’un mois calendaire</td></tr>
<tr class="row_even even">
<th id="id568c_l3">10 ans</th>
<td headers="id568c_l3">4 mois à la fin d’un mois calendaire</td></tr>
<tr class="row_odd odd">
<th id="id568c_l4">12 ans</th>
<td headers="id568c_l4">5 mois à la fin d’un mois calendaire</td></tr>
<tr class="row_even even">
<th id="id568c_l5">15 ans</th>
<td headers="id568c_l5">6 mois à la fin d’un mois calendaire</td></tr>
<tr class="row_odd odd">
<th id="id568c_l6">20 ans</th>
<td headers="id568c_l6">7 mois à la fin d’un mois calendaire</td></tr>
</tbody>
</table>
<p>La résiliation du contrat doit s’effectuer par écrit, par courrier recommandé avec accusé de réception ou idéalement par remise en main propre contre décharge. En respectant le délai de préavis, l’employé peut résilier son contrat de travail à tout moment.</p>
<p>En revanche, l’employeur, dès lors qu’il emploie plus de 5 salariés dans l’établissement concerné, ne peut licencier un salarié sans en donner la raison. Les trois principaux motifs sont :</p>
<ul class="spip">
<li>le licenciement économique : respect du délai de résiliation du contrat ;</li>
<li>le licenciement pour motif personnel : respect du délai de résiliation du contrat ;</li>
<li>le licenciement pour faute grave : immédiat, pas de préavis à respecter. L’employeur peut aussi proposer une convention de départ négocié (<i>Aufhebungsvertrag</i>). Il s’agit d’un accord entre les deux parties au niveau du délai et des indemnités de départ, qui sont non imposables jusqu’à un certain plafond.</li></ul>
<p>En Allemagne, il n’y a pas d’indemnité de licenciement si le licenciement individuel est justifié et, seulement en cas de licenciement collectif, si un plan social est négocié par le comité d’entreprise.</p>
<p>En revanche, en cas de licenciement abusif, le salarié doit être en principe réintégré dans l’entreprise (sauf pour les cadres dirigeants).</p>
<p><strong>Litige </strong></p>
<p>Si le litige porte sur les conditions de votre licenciement, vous ne disposez que de <strong>trois semaines</strong> à compter de la date du licenciement pour assigner votre employeur.</p>
<p>En cas de litige avec votre employeur, il est conseillé de faire appel à un avocat. En Allemagne, le recours à un avocat pour régler des différents avec son employeur est pratique courante. Il peut donc être judicieux de souscrire une assurance « protection juridique ».</p>
<p>Vous pouvez également obtenir des conseils juridiques auprès du greffe (<i>Rechtsantragsstelle</i>) du tribunal du travail de votre circonscription, ainsi qu’auprès de <i>l’Arbeitsamt.</i> De plus, une aide peut vous être offerte par le syndicat professionnel de votre branche si vous y étiez adhérent.</p>
<p>Enfin, vous pouvez obtenir une liste d’avocats parlant français dans votre région auprès du <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/article/annuaire-des-representations-francaises-a-l-etranger" class="spip_in">consulat de France</a> de votre résidence.</p>
<p><strong>Contrats spécifiques</strong></p>
<p>Outre les règles de droit local applicables aux contrats à durée indéterminée vues précédemment, si vous êtes embauché sous contrat à durée déterminée, à temps partiel ou en tant qu’intérimaire, il existe quelques règles spécifiques à connaître.</p>
<p><strong>Contrat à durée déterminée</strong></p>
<p>Le contrat à durée déterminée doit être passé par écrit. Dans le cas contraire, il a la valeur d’un contrat à durée indéterminée.</p>
<p>Depuis le 1er janvier 2001, la loi différencie deux types de contrats à durée déterminée :</p>
<p><strong>le CDD motivé par une cause objective</strong>, comme par exemple le remplacement d’un salarié malade, un besoin temporaire de renfort de personnel dans l’entreprise (emploi saisonnier), après une formation ou selon la nature même du travail (pour les artistes, dans la presse, etc.). Il est possible d’obtenir <strong>plusieurs CDD successifs</strong> s’ils sont motivés par une cause objective.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  la durée du <strong>CDD sans cause objective (CDD simplifié)</strong> ne peut dépasser <strong>2 ans</strong> (un seul contrat ou au plus trois prolongations pendant ces deux ans). L’employeur ne peut proposer un CDD simplifié à un salarié qui a déjà été employé dans l’entreprise en CDI ou en CDD, que le contrat soit récent ou pas. Si votre employeur souhaite vous garder dans l’entreprise, il sera donc obligé de vous proposer un CDI. </p>
<p><strong>Fin du contrat</strong></p>
<p>Dans le cas d’un CDD conclu pour remplacer une personne malade ou pour la réalisation d’un projet particulier, la date exacte de fin du contrat n’est pas obligatoirement précisée. Lorsque le travail est achevé, l’employeur doit notifier au moins 2 semaines à l’avance la fin du contrat.</p>
<p>Sauf en cas de motif grave, un CDD avec une date de fin ne peut être résilié prématurément, même avec préavis, sauf si cela est expressément prévu dans le contrat ou dans les conventions collectives applicables.</p>
<p>Il existe une exception en ce qui concerne le remplacement d’une personne en congé parental. Dans ce cas, votre employeur pourra éventuellement mettre fin à votre CDD avec un préavis de 3 semaines si cette personne interrompt son congé parental avant la date initialement prévue.</p>
<p>Enfin, si vous travaillez un jour de plus que la durée prévue par votre CDD (ou le jour suivant la date de fin de votre contrat ou de votre tâche), le contrat est automatiquement considéré comme prolongé en CDI.</p>
<p><strong>Contrat à temps partiel</strong></p>
<p>Les employés à temps partiel ont les même droits que les employés à temps plein, notamment pour la participation à des formations, l’octroi d’une prime de Noël ou le nombre de jours de congés, le tout calculé au prorata des heures de travail.</p>
<p>Depuis le 1er janvier 2001, tout salarié, y compris les cadres, a le droit d’obtenir une réduction et une répartition de son temps de travail aux conditions suivantes :</p>
<ul class="spip">
<li>l’entreprise doit compter plus de 15 salariés,</li>
<li>le salarié doit avoir au moins 6 mois d’ancienneté dans l’entreprise,</li>
<li>la demande de réduction de temps de travail doit être déposée au moins 3 mois avant le début des modifications souhaitées. L’employeur peut toutefois refuser en invoquant des raisons économiques. Cependant, si l’employeur ne réagit pas ou refuse la demande moins d’un mois avant la date souhaitée par le salarié, la réduction du temps de travail demandée devient effective.</li></ul>
<p>En revanche, après un refus justifié, le salarié ne pourra déposer une nouvelle demande qu’après un délai de 2 ans.</p>
<p><strong>Les « minijobs »</strong></p>
<p>Les lois du 20 décembre 2002 relatives à la réforme du marché du travail instituent un dispositif d’aide aux petits emplois de proximité. Les emplois qualifiés de « minijobs » sont désormais définis comme ceux dont la rémunération mensuelle ne dépasse pas 800 euros.</p>
<p>Ils ouvrent droit à un allégement des charges sociales et fiscales variable selon le niveau de rémunération.</p>
<p>Particulièrement attractif pour les employés, cette nouvelle disposition leur permet de cumuler leur emploi principal avec un « minijob » ne dépassant pas 400 euros mensuels, sans pour autant payer plus de cotisations sociales et d’impôt sur le revenu.</p>
<p>Pour en savoir plus, vous pouvez consulter le site suivant :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.minijob-zentrale.de/" class="spip_out" rel="external">www.minijob-zentrale.de</a></p>
<p><strong>Contrat de travail temporaire / intérim</strong></p>
<p>Le contrat de travail temporaire est un <strong>contrat à durée indéterminée</strong>. Le travailleur temporaire est en effet lié par son contrat de travail à l’entreprise de travail temporaire et non à l’entreprise utilisatrice. C’est donc cette première qui lui verse son salaire, même durant les périodes au cours desquelles l’intérimaire n’est pas placé.</p>
<p>En revanche, une mission, c’est à dire la mise à disposition du personnel auprès d’une entreprise utilisatrice, ne peut dépasser <strong>12 mois</strong>. L’entreprise de travail temporaire est tenue de fournir à l’intérimaire les éléments principaux du contrat (nature de la tâche, durée de la mission, etc.).</p>
<p><strong>Contrat de travail – spécificités</strong></p>
<p>Les ressortissants français n’ont pas besoin d’autorisation de travail pour obtenir un contrat de travail en Allemagne. Ils jouissent des mêmes droits que les salariés allemands.</p>
<p>Le contrat de travail précise les droits et obligations de l’employeur et de l’employé. Le contrat de travail peut être oral, mais la forme écrite est à privilégier. S’il s’agit d’un contrat oral, l’employeur est tenu de remettre à l’employé un résumé écrit des éléments essentiels du contrat au plus tard un mois après la date de prise d’effet du contrat.</p>
<p>Pour être valable, un contrat à durée déterminée (CDD) doit toujours faire l’objet d’un écrit.</p>
<p><strong>Détachement</strong></p>
<p>Est considéré comme détaché le salarié d’une entreprise ayant son siège social en France qui est envoyé par celle-ci pour une mission plus ou moins longue à l’étranger. La durée du détachement est d’ un an, avec possibilité de prolongation pour une année supplémentaire.</p>
<p>Pour en savoir plus sur le détachement, vous pouvez consulter le site du <a href="http://www.cleiss.fr/" class="spip_out" rel="external">Centre des liaisons européennes et internationales de sécurité sociale</a> (CLEISS) / rubrique "Informations pratiques  Détachement ".</p>
<p>Le contrat de travail initial n’est pas modifié, mais un avenant précise les nouvelles conditions de travail. Le salarié reste affilié au régime général de la Sécurité Sociale française et conserve les mêmes caisses d’assurance maladie, chômage et de retraite.</p>
<p>L’avenant doit préciser :</p>
<ul class="spip">
<li>le montant des indemnités de rapatriement ;</li>
<li>les conditions de réintégration dans la société d’origine ;</li>
<li>la répartition fiscale (clause dite d’égalisation fiscale). En général, l’entreprise s’engage à payer le surplus d’imposition lié au séjour à l’étranger.</li></ul>
<p><strong>Expatriation</strong></p>
<p>On parle d’expatriation lorsque la mission à l’étranger se poursuit au-delà des délais autorisés pour le détachement ou lorsque le salarié ou son employeur a opté pour ce statut.</p>
<p>Principales clauses d’un contrat :</p>
<ul class="spip">
<li><strong>Définition des deux parties</strong> : l’employeur d’une part et l’employé d’autre part. L’expatrié doit savoir s’il garde un lien de subordination avec la maison mère.</li>
<li><strong>Début et fin du contrat</strong> : date de début et de fin, dans le cas d’un CDI, du contrat.</li>
<li><strong>Durée du contrat et période d’essai</strong> : contrat à durée indéterminée (CDI), contrat à durée déterminée (CDD) ou contrat de travail temporaire, date, durée et forme de la période d’essai, qui peut se présenter sous la forme d’un CDD plus ou moins déguisé. Il est possible de prévoir que le contrat ne prenne effet qu’à la condition que les autorisations de séjour soient obtenues.</li>
<li><strong>Objet du contrat</strong> : l’activité, la fonction ou la mission, les attributions et la situation hiérarchique du salarié dans l’entreprise.</li>
<li><strong>Lieu de travail</strong> : ce point est important pour les entreprises dont le siège social et les filiales sont situés à des endroits différents, lorsque le lieu de formation dans l’entreprise est différent de celui de la mission définitive, dans le cas d’une mutation interne vers un autre site ou encore pour déterminer, le cas échéant, le droit applicable et le tribunal compétent en cas de litige.</li>
<li><strong>Temps de travail</strong> : durée journalière ou hebdomadaire et horaires de travail.</li>
<li><strong>Montant de la rémunération</strong> : salaire de base, primes, indemnités diverses, lieu et périodicité des versements (bien définir les éléments et le calcul d’une rémunération variable).</li>
<li><strong>Absences</strong> : délai d’information de l’employeur, document à fournir en cas de maladie ou pour tout autre motif d’absence.</li>
<li><strong>Congés</strong> : nombre de jours de congés payés par an ou par mois.</li>
<li><strong>Obligation de confidentialité et clause de non-concurrence</strong> : engagement au secret professionnel et conditions de dédommagement de l’employeur en cas de violation de cette clause (attention aux pièges d’une clause de non-concurrence post-contractuelle).</li>
<li><strong>Préavis de fin de contrat</strong> : conditions, durée du préavis, éventuellement dispense de préavis avec maintien du salaire, juridiction compétente en cas de litige.</li>
<li><strong>Clauses supplémentaires</strong> : par exemple, réglementation concernant les heures supplémentaires ou les activités professionnelles secondaires.</li>
<li><strong>Remboursement de frais</strong> : conditions de prise en charge des frais de voyage professionnel, d’hébergement ou de restauration, par exemple.</li>
<li><strong>Déménagement</strong> : conditions de prise en charge, répartition des frais entre l’employeur et le salarié.</li>
<li><strong>Autres clauses</strong> : selon le cas, scolarité des enfants, mise à disposition d’un véhicule ou d’un logement de fonction, etc.</li></ul>
<p><strong>Recrutement local</strong></p>
<p>Si le travailleur français est engagé sous contrat de droit local par une entreprise française ou allemande, il est soumis aux dispositions de la législation du travail allemande.</p>
<p>Pour éviter tout litige, il faut que le contrat comporte le plus grand nombre de clauses relatives aux conditions d’emploi et de séjour du salarié en accord avec la réglementation locale.</p>
<h3 class="spip"><a id="sommaire_2"></a>Fêtes légales</h3>
<p>Aux congés viennent s’ajouter les jours fériés. Ceux-ci sont déterminés par la législation des länder (sauf le jour de l’unité allemande du 3 octobre) et leur nombre peut donc varier en fonction de votre lieu de travail en Allemagne.</p>
<ul class="spip">
<li>1er janvier : Jour de l’An</li>
<li>6 janvier : Fête des Rois (Epiphanie) (a)</li>
<li>Vendredi Saint</li>
<li>Lundi de Pâques</li>
<li>1er mai : Fête du travail</li>
<li>Ascension</li>
<li>Pentecôte et lundi de Pentecôte</li>
<li>Fête-Dieu (b)</li>
<li>15 août : Assomption ©</li>
<li>3 octobre : Fête Nationale</li>
<li>31 octobre : Fête de la Réforme (d)</li>
<li>1er novembre : Toussaint (e)</li>
<li>21 novembre : Jour de Pénitence et de Prière (f)</li>
<li>25 et 26 décembre : Noël <br class="manualbr">(a) seulement pour le Bade-Wurtemberg, la Bavière et la Saxe-Anhalt ;<br class="manualbr">(b) seulement pour le Bade-Wurtemberg, la Bavière, la Hesse, la Rhénanie du Nord-Westphalie, la Rhénanie-Palatinat et la Sarre ;<br class="manualbr">© seulement pour la Sarre et les régions catholiques en Bavière ;<br class="manualbr">(d) seulement pour la Saxe, le Brandebourg, le Mecklembourg-Poméranie, la Saxe-Anhalt et la Thuringe ;<br class="manualbr">(e) seulement pour le Bade-Wurtemberg, la Rhénanie du Nord-Westphalie, la Bavière, la Rhénanie-Palatinat et la Sarre ;<br class="manualbr">(f) seulement en Saxe.</li></ul>
<p><a href="http://www.diplomatie.gouv.fr/fr/" class="spip_url spip_out"></a>Pour en savoir plus</p>
<ul class="spip">
<li><a href="http://www.tv5.org/TV5Site/voyageurs/accueil.php" class="spip_out" rel="external">TV5</a> rubrique "Jours fériés dans le monde"</li>
<li><a href="http://www.bahn.com/i/view/FRA/fr/index.shtml" class="spip_out" rel="external">Office du tourisme allemand</a> rubrique "Infos pratiques / préparation du voyage / Jours fériés et vacances scolaires "</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Emploi du conjoint</h3>
<p>Le conjoint d’un ressortissant français obtient, en principe, une autorisation de séjour d’une durée équivalente à celle de son conjoint. Cependant, il faut distinguer deux cas :</p>
<ul class="spip">
<li><strong>si celui-ci est ressortissant de l’Union européenne</strong>, il obtiendra un permis de séjour qui l’autorisera à travailler ;</li>
<li><strong>si le conjoint n’est pas ressortissant de l’Union européenne</strong>, il obtiendra un permis de séjour d’une durée identique à son conjoint. Le conjoint et les enfants à charge d’un ressortissant d’un Etat membre bénéficiant du droit de séjour sur le territoire d’un État membre ont le droit d’accéder à toute activité salariée ou non salariée sur l’ensemble du territoire de ce même État membre, même s’ils n’ont pas la nationalité d’un État membre (Directive 2004/38/CE).</li></ul>
<p>Sites à consulter :</p>
<ul class="spip">
<li><a href="http://www.paris.diplo.de" class="spip_out" rel="external">Ambassade d’Allemagne en France</a></li>
<li><a href="http://europa.eu/youreurope/citizens/work/index_fr.htm" class="spip_out" rel="external">L’Europe est à vous</a></li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Création d’entreprises – spécificités</h3>
<p>Pour créer une entreprise, il faut posséder un permis de séjour et s’inscrire au registre du commerce. Il existe en Allemagne de nombreux séminaires d’information sur la création d’entreprise. Ils sont proposés aussi bien par les universités que par les chambres de commerce ou encore par des organismes privés de formation. Le ministère fédéral de l’économie, ainsi que les chambres de commerce publient une brochure sur les nombreuses aides accordées aux créateurs d’entreprise.</p>
<p>En raison de la législation européenne concernant la liberté d’établissement, il est prévu que les ressortissants de l’Union européenne puissent créer librement une entreprise en Allemagne dans les mêmes conditions que les citoyens allemands. Aucune caution n’est nécessaire.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://europa.eu/youreurope/business/index_fr.htm" class="spip_out" rel="external">L’Europe est à vous</a> </p>
<p>La <a href="http://www.ihk.de/" class="spip_out" rel="external">Chambre de commerce et d’industrie allemande</a> (IHK - <i>Industrie- und Handelskammer</i>) vend des brochures présentant l’intégralité des formalités à accomplir en Allemagne pour créer une entreprise :  rubrique "Starthilfe und Unternehmensförderung".</p>
<p>Vous pouvez également vous adresser à la <i>Deutscher Industrie - und Handelskammertag</i> (DIHK) de Berlin :</p>
<p><a href="http://www.ihk.de/" class="spip_out" rel="external">Deutscher Industrie- und Handelskammertag</a> (DIHK)<br class="manualbr">Breite Strasse 29 - 10178 Berlin<br class="manualbr">Téléphone : (0049) 30 / 203 08-0 <br class="manualbr">Télécopie : (0049) 30/20308-1000<br class="manualbr">Courriel : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/emploi-stage/article/reglementation-du-travail#infocenter#mc#berlin.dihk.de#" title="infocenter..åt..berlin.dihk.de" onclick="location.href=mc_lancerlien('infocenter','berlin.dihk.de'); return false;" class="spip_mail">infocenter<span class="spancrypt"> [at] </span>berlin.dihk.de</a></p>
<p>Il peut être intéressant de prendre contact avec la Chambre franco-allemande de commerce et d’industrie, si les activités s’exerceront entre la France et l’Allemagne :</p>
<ul class="spip">
<li><a href="http://www.francoallemand.com/" class="spip_out" rel="external">Chambre franco-allemande de commerce et d’industrie (CFACI)</a><br class="manualbr">18 rue Balard</li>
<li>75015 Paris<br class="manualbr">Téléphone : 01 40 58 35 35</li>
<li>Télécopie : 01 45 75 47 39<br class="manualbr">Courriel : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/emploi-stage/article/reglementation-du-travail#info#mc#francoallemand.com#" title="info..åt..francoallemand.com" onclick="location.href=mc_lancerlien('info','francoallemand.com'); return false;" class="spip_mail">info<span class="spancrypt"> [at] </span>francoallemand.com</a></li></ul>
<p>Tous les länder, ainsi que la plupart des grandes villes allemandes, disposent d’un service d’expansion économique (<i>Büro für Wirtschaftsförderung</i>), chargé d’attirer les investisseurs internationaux.</p>
<p>La <a href="http://www.ccfa.de/" class="spip_out" rel="external">Chambre de commerce française en Allemagne</a> (CCFA) de Sarrebruck aide à l’implantation des filiales françaises en Allemagne :</p>
<ul class="spip">
<li>Lebacher Strasse 4 - 66113 Saarbrücken <br class="manualbr">ou : Postfach 100543 - 66005 Saarbrücken<br class="manualbr">Téléphone : (49) 681 99630</li>
<li>Télécopie : (49) 681 99 63 111 <br class="manualbr">Courriel : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/emploi-stage/article/reglementation-du-travail#info#mc#ccfa-saa.de#" title="info..åt..ccfa-saa.de" onclick="location.href=mc_lancerlien('info','ccfa-saa.de'); return false;" class="spip_mail">info<span class="spancrypt"> [at] </span>ccfa-saa.de</a>
Vous trouverez ci-dessous une liste de sites Internet allemands pouvant être une source d’information et une aide intéressante pour les créateurs d’entreprise :</li></ul>
<ul class="spip">
<li><a href="http://www.bmwi.de/" class="spip_out" rel="external">Ministère fédéral de l’Economie et de la Technologie</a> ;</li>
<li><a href="http://www.existenzgruender.de/" class="spip_out" rel="external">Ministère fédéral de l’Economie et de la Technologie - Portail des créateurs d’entreprise</a> ;</li>
<li><a href="http://www.deutschland.de/" class="spip_out" rel="external">Portail de l’Allemagne</a>. La rubrique "économie" recense de nombreux sites.</li>
<li><a href="http://www.akademie.de/" class="spip_out" rel="external">www.akademie.de</a> rubrique "Existenzgründung"</li>
<li><a href="http://www.ifex.de/" class="spip_out" rel="external">Ministère de l’Economie du Bade-Wurtemberg</a></li></ul>
<p>Côté français, le site Internet de la <a href="http://www.tresor.economie.gouv.fr/" class="spip_out" rel="external">direction générale du trésor</a> du ministère de l’Economie, des Finances et de l’Emploi ("espace entreprises") permet d’accéder à des informations utiles pour tout créateur d’entreprise ou pour toute entreprise souhaitant s’installer ou exporter en Allemagne.</p>
<p>Pour obtenir des informations précises sur le marché allemand, il est conseillé de s’adresser au <a href="http://www.tresor.economie.gouv.fr/se/allemagne/" class="spip_out" rel="external">service économique à Berlin</a>.</p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/emploi-stage/article/reglementation-du-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
