# Curriculum vitae

<p>Les normes de présentation d’un CV en Colombie sont très proches de ce qu’on peut trouver en France :</p>
<ul class="spip">
<li><strong>Une page est la norme</strong>, ne jamais dépasser deux pages même en cas de longue carrière, éviter les mises en page trop denses.</li>
<li><strong>Informations personnelles</strong> : nom, prénom, date de naissance, date d’arrivée en Colombie, numéros de téléphone, adresse mail, adresse postale.</li>
<li><strong>Objectifs</strong> : à mentionner si vous avez un objectif précis, deux lignes maximum.</li>
<li><strong>Parcours professionnel</strong> : expériences passées classées par ordre chronologique inverse. Nom de la société, secteur, poste occupé, pensez à bien décrire précisément les fonctions réelles exercées et les compétences gagnées, surtout pour les expériences les plus récentes.</li>
<li><strong>Formation</strong> : années d’études, établissements, diplômes et certificats obtenus. Il est possible de mentionner le baccalauréat, mais ce n’est pas indispensable si vous avez un diplôme supérieur.</li>
<li><strong>Compétences supplémentaires</strong> : par exemple les outils ou les logiciels ou environnements informatiques maîtrisés, etc.</li>
<li><strong>Langues</strong> : précisez toujours votre niveau d’espagnol même s’il est basique.</li>
<li><strong>Loisirs</strong> : rubrique facultative à maintenir très brève car elle n’est que très rarement significative pour les recruteurs.</li>
<li><strong>Références</strong> : explicites, ou encore « disponibles sur demande », privilégier le cas échéant les références locales de nature à rassurer les employeurs sur l’adaptation du candidat au marché du travail colombien.</li>
<li>Adapter le CV au poste visé, un CV trop générique a moins de chances d’attirer l’attention qu’un CV ciblé.</li>
<li>Eviter à tout prix les fautes d’orthographe, faire relire le CV par un tiers.</li>
<li>Il est recommandé de traduire (ou faire traduire) votre CV en espagnol.</li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/emploi-stage/article/curriculum-vitae). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
