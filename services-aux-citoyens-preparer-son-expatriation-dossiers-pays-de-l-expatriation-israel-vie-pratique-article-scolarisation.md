# Scolarisation

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/vie-pratique/article/scolarisation#sommaire_1">Les établissements scolaires français en Israël</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/vie-pratique/article/scolarisation#sommaire_2">Enseignement supérieur</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/vie-pratique/article/scolarisation#sommaire_3">Pour en savoir plus</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Les établissements scolaires français en Israël</h3>
<p>Plusieurs établissements dispensent un enseignement en français :</p>
<ul class="spip">
<li>le <a href="http://www.lfj-aefe.co.il/" class="spip_out" rel="external">Lycée français de Jérusalem</a> couvre toutes les sections, depuis l’école maternelle jusqu’au baccalauréat ;</li>
<li>à Tel Aviv, le <a href="http://www.ambafrance-il.org/-College-francais-Marc-Chagall-de-.html" class="spip_out" rel="external">collège français Marc Chagall</a> accueille les enfants de 3 à 11 ans (de la petite section de maternelle a la 6ème incluse), puis le Lycée franco-israélien Raymond Leven prend le relais jusqu’à la terminale ;</li>
<li>la <a href="http://www.ambafrance-il.org/-La-scolarite-en-francais-.html" class="spip_out" rel="external">liste complète des établissements homologués par l’Education nationale française en Israël</a> est disponible sur le site Internet de l’Ambassade de France en Israël.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Enseignement supérieur</h3>
<p>Le paysage universitaire israélien compte près de 70 institutions d’enseignement (universités, collèges privés, etc.). Sept universités, qui possèdent l’habilitation à former des étudiants jusqu’au niveau du doctorat, tiennent le haut du pavé : l’Université Hébraïque de Jérusalem, l’Université de Tel-Aviv, l’Université de Haïfa, l’Université de Bar-Ilan, l’Université Ben Gourion du Néguev, le Technion (<i>Israel Institute of Technology</i>) et l’Institut Weizmann.</p>
<p>De très bonne qualité, l’enseignement supérieur israélien accueille aujourd’hui plus de 300000 étudiants. Les domaines d’études les plus répandus sont :</p>
<ul class="spip">
<li>pour les étudiants de licence (<i>bachelor</i>) : les sciences humaines et sociales ;</li>
<li>pour les étudiants de master : les sciences humaines, le commerce et la gestion ;</li>
<li>pour les étudiants de doctorat : les sciences naturelles et les mathématiques.</li></ul>
<h4 class="spip">Accès à l’université</h4>
<p>Il existe de nombreux accords bilatéraux entre les universités israéliennes et les universités et grandes écoles françaises. Ces accords permettent aux étudiants et chercheurs français d’effectuer un ou plusieurs semestres d’échange ou de recherche en Israël. Dans ce cas, les frais de scolarité sont généralement pris en charge par l’université ou la grande école d’origine en France.</p>
<p>Les Français ayant acquis la nationalité israélienne et qui souhaitent commencer ou reprendre des études supérieures en Israël peuvent le faire sur présentation à l’université d’un diplôme du secondaire (Bac pour la France). Cependant, s’ils n’ont pas obtenu de notes suffisamment élevées, s’ils n’ont pas le niveau suffisant en hébreu ou si leur diplôme universitaire français n’est pas reconnu par l’université d’accueil, leur inscription peut être refusée et ils seront alors aiguillés vers :</p>
<ul class="spip">
<li>une <i><a href="http://www.gov.il/FirstGov/TopNavEng/EngSituations/ESStudentsGuide/ESSPre-Academic+Programs/" class="spip_out" rel="external">mechina</a></i> (cycle d’étude préparatoire aux études supérieures en Israël), s’ils sortent du lycée ou ont suivi moins de 18 mois d’enseignement supérieur ;</li>
<li>un programme d’intégration universitaire « TAKAH », s’ils ont déjà un diplôme supérieur en France.</li></ul>
<p><strong>Plus d’information sur les cycles préparatoires :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.gov.il/FirstGov/TopNavEng/EngSituations/ESStudentsGuide/ESSPre-Academic+Programs/" class="spip_out" rel="external">Programmes pré-académiques et apprentissage de l’hébreu sur le portail du gouvernement israélien</a></p>
<h4 class="spip">Enseignement en anglais</h4>
<p>De nombreuses universités proposent des cycles complets d’enseignement dispensés en anglais à destination des étudiants étrangers, aussi bien aux niveaux licence (<i>bachelor</i>) que master ou doctorat, et ce, dans toutes la plupart des disciplines.</p>
<h4 class="spip">Coût des études, bourses et logement</h4>
<p><strong>Pour les étudiants français n’ayant pas acquis la nationalité israélienne :</strong></p>
<p>Le coût des études en Israël est très variable. Pour les universités, il s’échelonne entre 10000 à 25000 ILS (2000 à 5000 €) par an, en fonction de l’université et du niveau d’études. Pour les institutions privées, il peut monter jusqu’à plus de 40000 ILS (8000 €) par an.</p>
<p>Les étudiants désireux de suivre un cursus en Israël peuvent approcher différents acteurs en vue de l’obtention d’une bourse d’étude : le gouvernement israélien, les universités elles-mêmes, ou encore des sources privées. L’attribution des bourses est essentiellement basée sur les besoins spécifiques de ces différents acteurs et sur les résultats obtenus par l’étudiant.</p>
<p><strong>Pour les étudiants français ayant acquis la nationalité israélienne :</strong></p>
<p>L’Autorité des étudiants (<i>Student Authority</i>) du ministère de l’Immigration couvre intégralement les frais universitaires des étudiants français ayant acquis la nationalité israélienne. De plus, ces étudiants bénéficient d’une aide pour leur logement (assistance financière ou placement privilégié dans les dortoirs des universités).</p>
<p>De plus amples informations sont disponibles dans l’article <a href="http://www.gov.il/FirstGov/TopNavEng/EngSituations/ESStudentsGuide/ESSHousing/" class="spip_out" rel="external">Bourses et logement</a> sur le site du gouvernement israélien.</p>
<h3 class="spip"><a id="sommaire_3"></a>Pour en savoir plus</h3>
<ul class="spip">
<li><a href="http://www.mfa.gov.il/MFA/Facts+About+Israel/Education/EDUCATION-+Higher+Education.htm" class="spip_out" rel="external">Description de l’enseignement supérieur en Israël</a> sur le site du ministère des Affaires étrangères israélien</li>
<li><a href="http://www.mfa.gov.il/MFA/Facts+About+Israel/Education/Higher_education_Israel-Selected_data_2010-11.htm" class="spip_out" rel="external">Etude statistique portant sur l’enseignement supérieur israélien</a> sur le même site</li></ul>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  A propos de la scolarisation dans le système français à l’étranger, consultez nos rubriques thématiques sur les <a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures.md" class="spip_in">Etudes supérieures</a> et la <a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais.md" class="spip_in">Scolarité en français</a> :
<br>— les établissements français du primaire et du secondaire à l’étranger ;
<br>— les bourses scolaires et la prise en charge des frais de scolarité à l’étranger ;
<br>— les possibilités qui s’offrent à vous si votre enfant ne peut être scolarisé à l’étranger dans le système français (enseignement à distance par le CNED, programme français langue maternelle (FLAM), internats en France) ;
<br>— les épreuves du baccalauréat à l’étranger ;
<br>— les bourses d’études supérieures en France et à l’étranger ;
<br>— l’équivalence des diplômes.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/vie-pratique/article/scolarisation). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
