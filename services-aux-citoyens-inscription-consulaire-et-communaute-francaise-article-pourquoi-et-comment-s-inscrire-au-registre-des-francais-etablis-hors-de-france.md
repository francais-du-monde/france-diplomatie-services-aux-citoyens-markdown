# Pourquoi et comment s’inscrire au Registre des français établis hors de France ?

<p class="chapo">
      Si vous vivez à l’étranger, vous pouvez vous inscrire au registre des Français établis hors de France auprès de votre consulat. Cette inscription facilite vos démarches à l’étranger.
</p>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/inscription-consulaire-et-communaute-francaise/article/pourquoi-et-comment-s-inscrire-au-registre-des-francais-etablis-hors-de-france#sommaire_1">De quoi s’agit-il ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/inscription-consulaire-et-communaute-francaise/article/pourquoi-et-comment-s-inscrire-au-registre-des-francais-etablis-hors-de-france#sommaire_2">S’inscrire pour la première fois</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/inscription-consulaire-et-communaute-francaise/article/pourquoi-et-comment-s-inscrire-au-registre-des-francais-etablis-hors-de-france#sommaire_3">Actualiser son dossier en cours de séjour</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>De quoi s’agit-il ?</h3>
<p>L’inscription au registre des français établis hors de France s’adresse à tout Français qui s’établit pour plus de 6 mois dans un pays étranger.</p>
<p>Grâce à cette inscription, les services consulaires peuvent vous communiquer des informations (échéances électorales, sécurité, événements) et contacter vos proches en cas d’urgence.</p>
<p>Cette inscription facilite l’accomplissement de certaines formalités :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Demande de documents d’identité (passeport, carte nationale d’identité)</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Demande de bourse pour vos enfants scolarisés dans un établissement français en Europe ou hors Europe</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Inscription sur la liste électorale consulaire</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Recensement</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Réduction des tarifs des légalisations et copies conformes</p>
<h3 class="spip"><a id="sommaire_2"></a>S’inscrire pour la première fois</h3>
<p><strong>En ligne</strong></p>
<p>Il suffit de vous connecter au <a href="https://www.service-public.fr/particuliers/vosdroits/F33307" class="spip_out" rel="external">Registre des français à l’étranger</a> et de scanner, pour vous même et vos enfants mineurs, les documents suivants :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Carte d’identité ou passeport français en cours de validité ou périmé depuis moins de deux ans </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Justificatif de résidence dans la circonscription consulaire</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Photo d’identité</p>
<p>Dès que l’inscription sera validée, l’attestation d’inscription ainsi que la carte d’inscription consulaire seront enregistrées dans le porte-documents service-public.fr et pourront être imprimées chaque fois que vous en aurez besoin.</p>
<p><strong>Sur Place</strong></p>
<p>L’inscription peut également se faire sur place, en se présentant <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/article/annuaire-des-representations-francaises-a-l-etranger" class="spip_out">au consulat ou à l’ambassade</a> avec les documents suivants :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Carte d’identité ou passeport français en cours de validité ou périmé depuis moins de deux ans </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Justificatif de résidence dans la circonscription consulaire</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Photo d’identité</p>
<h3 class="spip"><a id="sommaire_3"></a>Actualiser son dossier en cours de séjour</h3>
<p>Si vous êtes déjà inscrit(e) ou avez été inscrit(e) sur le registre des Français établis hors de France, vous pouvez, à tout moment, vérifier et actualiser les données vous concernant.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  vos données personnelles</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  votre situation familiale</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  votre situation électorale </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  vos données de sécurité</p>
<p><strong>En ligne</strong></p>
<p><a href="https://www.service-public.fr/particuliers/vosdroits/F33307" class="spip_out" rel="external">Registre des français à l’étranger</a></p>
<p>Après chaque modification, l’attestation d’inscription et la carte d’inscription consulaire sont actualisées et enregistrées dans votre porte-documents sur le site service-public.fr</p>
<p>Vous pouvez imprimer ces documents quand vous en avez besoin.</p>
<p><strong>Sur Place</strong></p>
<p><a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/article/annuaire-des-representations-francaises-a-l-etranger" class="spip_out">Annuaire des ambassades ou consulats français à l’étranger</a></p>
<p><strong>Renouveler son inscription</strong></p>
<p>Un courriel vous invite à renouveler votre inscription 3 mois avant la date d’échéance.</p>
<p>Par ailleurs, vous pouvez également, choisir à tout moment de prolonger la durée de votre inscription (dans la limite de 5 ans).</p>
<p><sup>Mis à jour : 14.06.16</sup></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/inscription-consulaire-et-communaute-francaise/article/pourquoi-et-comment-s-inscrire-au-registre-des-francais-etablis-hors-de-france). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
