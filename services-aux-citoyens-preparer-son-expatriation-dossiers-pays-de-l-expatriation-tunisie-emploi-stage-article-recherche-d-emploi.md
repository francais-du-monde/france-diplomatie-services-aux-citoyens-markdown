# Recherche d’emploi

<h4 class="spip">Outils pour la recherche d’emploi</h4>
<h4 class="spip">Journaux</h4>
<p><strong>Différents quotidiens et hebdomadaires proposent régulièrement des offres d’emploi </strong>collectées par l’Agence tunisienne de l’emploi ou émanant directement d’entreprises : La Presse, Le Temps, Le Renouveau, Réalités (hebdomadaire), Essahafa (quotidien en arabe). Ces publications disposent d’un site Internet (cf.ci-après).</p>
<h4 class="spip">Sites internet</h4>
<ul class="spip">
<li><a href="http://www.emploi.nat.tn/" class="spip_out" rel="external">Agence tunisienne de l’emploi - ministère de l’Emploi</a> ;</li>
<li><a href="http://www.letemps.com.tn/" class="spip_out" rel="external">Le Temps : quotidien en français</a></li>
<li><a href="http://www.realites.com.tn/" class="spip_out" rel="external">Réalités online : hebdomadaire en français et en arabe</a></li></ul>
<p>Différents sites internet proposent également des offres :</p>
<ul class="spip">
<li><a href="http://www.emploi-tunisie.com/" class="spip_out" rel="external">Emploi-tunisie.com</a></li>
<li><a href="http://www.concours-emploi.com/" class="spip_out" rel="external">Concours-emploi.com</a></li>
<li><a href="http://www.tanitjobs.com/" class="spip_out" rel="external">Tanitjobs.com</a></li></ul>
<h4 class="spip">Organismes pour la recherche d’emploi</h4>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.ctfci.org/" class="spip_out" rel="external">Chambre tuniso-française de commerce et d’industrie</a></p>
<p>Les activités concernant l’emploi (mise en relation des demandeurs et des employeurs potentiels) ont été confiées par convention à la Chambre tuniso-française de commerce et de l’industrie.</p>
<p>Créée en 1974 et composée d’ entreprises du monde économique tuniso-français, la CTFCI a pour mission le développement des relations d’affaires entre partenaires tunisiens et français et la promotion des relations commerciales, industrielles, financières et techniques entre la Tunisie et la France.</p>
<p>39 Avenue du Japon<br class="manualbr">BP 25 – 1073 Tunis Montplaisir<br class="manualbr">Tél. : (216) 71 844 310 - Fax : (216) 71 845 962</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.businessfrance.fr/" class="spip_out" rel="external">Business France</a></p>
<p>Dans le cadre de ses différentes prestations, l’Agence internationale d’aide à l’export pour les entreprises françaises, propose la formule V.I.E. (volontariat internationale à l’entreprise) qui permet aux filiales ou entreprises françaises en Tunisie, de bénéficier d’une aide en terme de ressources humaines ;</p>
<p>1 place de l’indépendance<br class="manualbr">1000 – Tunis<br class="manualbr">Tél. : (216) 71105 080 – Fax : (216) 71105092</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.investintunisia.com/" class="spip_out" rel="external">Foreign Investment Promotion Agency</a> (FIPA)</p>
<p>La FIPA est une agence à contacter en cas de souhait de création d’entreprise.</p>
<p>Rue Slaheddine El Ammami<br class="manualbr">Centre Urbain Nord - 1004 Tunis<br class="manualbr">Tél. : (216) 71 703 140 - Fax : (216) 71 702 600</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Agence de promotion de l’industrie (API)</strong></p>
<p>L’API est une agence à contacter en cas de souhait de création d’entreprise.</p>
<p>63, rue de Syrie<br class="manualbr">1002 TUNIS Belvédère<br class="manualbr">Tél. : (216) 71 792 144 - Fax : (216) 71 782 482</p>
<p><i>Mise à jour : novembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/emploi-stage/article/recherche-d-emploi). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
