# Convention fiscale

<p>La France et Madagascar ont signé, le 22 juillet 1983, une convention en matière de fiscalité publiée au Journal officiel du 11 décembre 1984.</p>
<p>Un rectificatif a été publié au Journal officiel du 9 mars 1985.</p>
<p>Cette convention tend à éviter les doubles impositions qui pourraient résulter de l’application des législations de ces deux Etats, de prévenir l’évasion fiscale et d’établir des règles d’assistance administrative en matière fiscale.</p>
<p>Le texte de la convention et de ses avenants peut être obtenu à la Direction des Journaux Officiels,</p>
<ul class="spip">
<li><strong>par courrier :</strong> <br class="manualbr">26 rue Desaix <br class="manualbr">75727 Paris cedex 15,</li>
<li><strong>par télécopie : </strong> <br class="manualbr">01 40 58 77 80,</li>
<li>ou sur le site internet du <a href="http://www.impots.gouv.fr/portal/deploiement/p1/fichedescriptive_1918/fichedescriptive_1918.pdf" class="spip_out" rel="external">ministère des Finances</a></li></ul>
<p>Les dispositions conventionnelles, qui ont primauté sur les dispositions du droit interne, selon l’article 55 de la Constitution française répartissent entre les deux Etats le droit d’imposer les revenus perçus par leurs résidents respectifs.</p>
<h4 class="spip">Champ d’application de la convention</h4>
<p>Cet accord a pour objet de protéger <strong>les résidents de chacun des Etats contractants</strong> en matière d’impôts prélevés directement sur le revenu ou sur le bénéfice des sociétés.</p>
<p>La convention trouve donc à s’appliquer aux résidents de ces Etats (article 1 de la Convention)</p>
<p><strong>Notion de résidence</strong> : l’article 4 paragraphe 1 de la convention précise que l’expression « résident d’un Etat » désigne la personne qui, en vertu de la législation de cet Etat, y est assujettie à l’impôt, en raison de son domicile, de sa résidence, de son siège social statutaire ou de tout autre critère de nature analogue.</p>
<p>Au paragraphe 2, l’article 4 fournit des critères subsidiaires permettant de résoudre le cas de double résidence si l’assujettissement à l’impôt ne pouvait suffire.</p>
<p><strong>Ces critères sont :</strong></p>
<ul class="spip">
<li>son foyer d’habitation permanent ;</li>
<li>le centre de ses intérêts vitaux ;</li>
<li>son lieu de séjour habituel ;</li>
<li>à défaut, l’Etat dont elle possède la nationalité.</li></ul>
<p>Si une personne possède la double nationalité ou si elle ne possède la nationalité d’aucun des deux Etats contractants, la question est tranchée d’un commun accord par les autorités de ces deux Etats (article 4, paragraphe 2, d).</p>
<h4 class="spip">Elimination de la double imposition</h4>
<p>La double imposition est évitée selon un dispositif prévu par l’article 22 de la Convention.</p>
<ul class="spip">
<li>en ce qui concerne Madagascar : exonération dès lors que les revenus sont imposables en France. Toutefois, l’impôt malgache est calculé au taux correspondant au revenu mondial.</li>
<li>en ce qui concerne la France : exonération dès lors que les revenus sont imposables à Madagascar. Toutefois, l’impôt français est calculé au taux correspondant au revenu mondial.</li></ul>
<p>Les dividendes, intérêts, redevances, tantièmes, ainsi que les revenus des artistes et sportifs provenant de Madagascar restent imposables en France.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/fiscalite/article/convention-fiscale). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
