# Les missions du Service central d’état civil

<p class="chapo">
      Le <strong>Service central d’état civil</strong> est dépositaire des registres d’état civil relatifs aux évènements d’état civil (naissance, reconnaissance, mariage, divorce, adoption, etc) survenus à l’étranger ou dans les territoires anciennement sous administration française et qui concernent des ressortissants français. Ce service a été créé par <a href="https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000675912" class="spip_out" rel="external">décret du 1er juin 1965</a> afin de répondre au besoin de centralisation en un lieu unique les registres d’état civil conservés jusque-là par plusieurs administrations.
</p>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/en-savoir-plus/article/missions-du-service-central-d-etat-civil#sommaire_1">Le Service est dépositaire de l’état civil des Français de l’étranger</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/en-savoir-plus/article/missions-du-service-central-d-etat-civil#sommaire_2">Il le met à jour et l’exploite</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/en-savoir-plus/article/missions-du-service-central-d-etat-civil#sommaire_3">Dans certaines situations, il établit ou reconstruit cet état civil</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/en-savoir-plus/article/missions-du-service-central-d-etat-civil#sommaire_4">Il transcrit dans ses registres certaines décisions judiciaires</a></li></ul>
<p class="spip_document_88590 spip_documents spip_documents_right">
<img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L250xH182/visite_fekl7_cle03b933-2e3f5.jpg" width="250" height="182" alt=""></p>
<h3 class="spip"><a id="sommaire_1"></a>Le Service est dépositaire de l’état civil des Français de l’étranger</h3>
<p>Il conserve quelques 15 millions d’actes provenant de trois sources principales :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Les registres duplicata de l’état civil consulaire. Au début de chaque année, les ambassades et les consulats français transmettent au Service les duplicata des registres des actes qu’ils ont établis l’année précédente.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Les registres d’état civil établis dans les pays anciennement sous souveraineté française, avant leur indépendance.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Les actes d’état civil établis pour les personnes qui acquièrent la nationalité française.</p>
<p>Le Service central d’état civil n’est dépositaire que des actes d’état civil de moins de 100 ans. Les actes de plus de cent ans sont versés aux services d’archives chargés de la conservation et de la communication des registres plus anciens :</p>
<p>Service en charge des actes de l’état civil consulaire, des actes établis au Maroc et en Tunisie pendant la période du protectorat, et des actes établis pour les personnes devenues françaises :<br class="manualbr">Ministère des Affaires étrangères<br class="manualbr">Direction des Archives <br class="manualbr">Division politique archivistique <br class="manualbr">3 Rue Suzanne Masson <br class="manualbr">93126 LA COURNEUVE CEDEX - France</p>
<p>Service en charge des actes établis dans les anciennes colonies avant l’indépendance : <br class="manualbr">Archives Nationales d’Outre Mer<br class="manualbr">29 chemin du Moulin de Testas<br class="manualbr">13090 AIX-EN-PROVENCE - France</p>
<h3 class="spip"><a id="sommaire_2"></a>Il le met à jour et l’exploite</h3>
<p>Cette mise à jour se fait par apposition de mentions lorsque l’état civil de la personne évolue au cours de sa vie (mariage, divorce, etc). L’exploitation concerne la délivrance de copies ou d’extraits d’acte de même que l’établissement et la mise à jour de livrets de famille.</p>
<h3 class="spip"><a id="sommaire_3"></a>Dans certaines situations, il établit ou reconstruit cet état civil</h3>
<ul class="spip">
<li>Il établit les actes des personnes qui acquièrent la nationalité française, de plein droit (exemple : naissance et résidence en France), par déclaration (exemple : mariage avec un conjoint français) ou par décret de naturalisation.</li>
<li>En application des dispositions de la <a href="https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000692958" class="spip_out" rel="external">loi 68-671 du 25 juillet 1968</a>, il reconstruit, à la demande des personnes concernées, les actes qui ont été ou auraient dû être établis en Algérie ou dans les autres pays anciennement sous souveraineté française, et qui sont manquants dans ses archives.</li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Il transcrit dans ses registres certaines décisions judiciaires</h3>
<p>Sont principalement concernées :</p>
<ul class="spip">
<li>Les décisions judiciaires rendues en France, mais concernant un événement d’état civil survenu à l’étranger (adoptions, jugements déclaratifs de naissance ou de décès, etc).</li>
<li>Les décisions judiciaires concernant les personnes nées à l’étranger relatives aux</li>
<li>La gestion du Répertoire civil des personnes nées à l’étranger. Les inscriptions au Répertoire civil concernent les décisions judiciaires relatives aux tutelles, curatelles ou au régime matrimonial. Elles sont, selon la situation, inscrites au <a href="https://www.service-public.fr/particuliers/vosdroits/R18788" class="spip_out" rel="external">Répertoire civil</a> ou au Répertoire civil annexe. Elles s’effectuent toujours par l’intermédiaire d’un greffier, d’un avocat ou d’un notaire.</li></ul>
<p><i>Mise à jour : mars 2016</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/en-savoir-plus/article/missions-du-service-central-d-etat-civil). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
