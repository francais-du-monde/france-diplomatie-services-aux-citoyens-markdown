# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/vie-pratique/article/cout-de-la-vie-108673#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/vie-pratique/article/cout-de-la-vie-108673#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/vie-pratique/article/cout-de-la-vie-108673#sommaire_3">Budget</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/vie-pratique/article/cout-de-la-vie-108673#sommaire_4">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>L’unité monétaire est le dollar australien (<i>Australian dollar</i>). Ses symboles sont A$, $A, AU$ ou $AU, mais son code officiel est AUD. Cette monnaie est convertible.</p>
<p>Le dollar australien est divisé en 100 cents. Il comprend des pièces de 1 et 2 dollars et de 5, 10, 20 et 50 cents. Les pièces de 1 et 2 cents ont été retirées de la circulation en 1991. Il existe des billets de 5, 10, 20, 50 et 100 dollars. Depuis 1988, les billets sont en polypropylène et possèdent une fenêtre transparente comportant un filigrane du découvreur de l’Australie, le capitaine James Cook.</p>
<p>Pour en savoir plus sur la monnaie australienne, vous pouvez consulter le site Internet de la <a href="http://www.rba.gov.au/" class="spip_out" rel="external">Banque nationale d’Australie</a> (<i>Reserve Bank of Australia</i>) ainsi que celui de la <a href="http://www.ramint.gov.au/" class="spip_out" rel="external">Monnaie australienne</a> (Royal Australian Mint).</p>
<p>Au 1er juin 2013, un dollar australien valait 0,7359 € (un euro = 1,35267 AUD). La moyenne annuelle du taux de change pour 2012 a été de 0,806041 € pour 1 AUD, la moyenne au 15 juin 2013 est de 0,779068 € pour 1 AUD.</p>
<p>A noter qu’il n’existe pas de limite à l’importation de devises en Australie, vous devez cependant déclarer à votre arrivée toute somme en espèces, quelle que soit la devise, égale ou supérieure à 10.000 dollars australiens. Concernant les autres titres ou valeurs (par exemple, traveller’s cheques), ceux-ci doivent être obligatoirement déclarés, quelle que soit leur valeur.</p>
<p>Pour en savoir plus sur l’importation de devises et de valeurs, vous pouvez consulter le site Internet de <a href="http://www.austrac.gov.au/reporting_physical_currency.html" class="spip_out" rel="external"><i>l’Australian Transaction Reports and Analysis Centre</i></a><i>.</i></p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<p><strong>Réseau bancaire</strong></p>
<p>Le secteur bancaire australien est dominé par quatre grandes banques :</p>
<ul class="spip">
<li><a href="http://www.anz.com.au/" class="spip_out" rel="external">Australia  New-Zealand Banking Group</a></li>
<li><a href="http://www.commbank.com.au/" class="spip_out" rel="external">Commonwealth Bank of Australia</a></li>
<li><a href="http://www.nab.com.au/" class="spip_out" rel="external">National Australia Bank</a></li>
<li><a href="http://www.westpac.com.au/" class="spip_out" rel="external">Westpac Banking Corporation</a></li></ul>
<p>Les banques françaises présentes en Australie sont des banques d’affaires et n’ouvrent pas de comptes aux particuliers. Parmi les banques étrangères présentes en Australie, on peut citer :</p>
<ul class="spip">
<li><a href="http://www.hsbc.com.au/" class="spip_out" rel="external">HSBC</a></li>
<li><a href="http://www.citibank.com.au/" class="spip_out" rel="external">Citibank</a></li>
<li><a href="http://www.ingdirect.com.au/" class="spip_out" rel="external">ING Direct</a></li></ul>
<p>Les banques sont ouvertes du lundi au jeudi de 9h30 à 16h00 et de 9h30 à 17h00 le vendredi ; les agences présentes dans les centres commerciaux sont ouvertes le samedi de 9h30 à 13h00.</p>
<p><strong>Compte bancaire</strong></p>
<p>Sur place, l’ouverture d’un compte bancaire est simple et rapide à condition de fournir son passeport, une adresse postale « stable » en Australie dans un délai de six semaines suivant votre arrivée et de bénéficier d’au moins 100 points en matière de justification d’identité. Chaque preuve d’identité fournie correspond en effet à un nombre de points ; ainsi votre passeport vous rapportera 70 points, votre permis de conduire 40, une carte de crédit 25, une facture utilitaire 25, etc.).</p>
<p>A défaut de logement, certaines banques acceptent de vous domicilier momentanément à leur adresse, ou à celle de votre employeur. Les personnes titulaires d’un visa de travail temporaire doivent obtenir un numéro fiscal (<i>Tax File Number </i>- TFN) qu’il est possible de demander sur le site Internet du <a href="http://www.ato.gov.au/Individuals/" class="spip_out" rel="external">centre australien des impôts</a>.</p>
<p>Dès que le compte bancaire est ouvert, il est conseillé de l’approvisionner rapidement afin d’éviter des frais importants. La plupart des banques offrent à leurs clients un service bancaire en ligne et par téléphone.</p>
<p>Il est également possible d’ouvrir un compte bancaire avant le départ de France. Plusieurs banques, dont les quatre grandes banques australiennes, offrent un service en ligne d’aide bancaire aux immigrants afin de leur faciliter la tâche. A votre arrivée et dans un délai de six semaines, vous n’aurez plus qu’à vous identifier auprès de l’agence bancaire choisie.</p>
<p>Il existe deux types de comptes :</p>
<ul class="spip">
<li><i>Transaction Account </i> : c’est le plus courant car il permet d’effectuer toutes les transactions bancaires courantes et quotidiennes (retraits, versements et achats) effectuées sur votre compte bancaire. Il n’est généralement pas ou très faiblement rémunéré.</li>
<li><i>Savings Account </i> : compte d’épargne à rémunération fixe.</li></ul>
<p>Certaines banques prélèvent des frais mensuels pour la tenue du compte. Il est vivement conseillé de bien se renseigner auprès des banques sur les frais relatifs à l’ouverture et à la clôture d’un compte bancaire. N’hésitez pas à comparer les prestations bancaires d’un établissement à l’autre.</p>
<p><strong>Retrait d’argent</strong></p>
<p>Sur place, les cartes de crédit internationales telles que Visa, Mastercard et American Express, permettent le retrait d’espèces dans les distributeurs de billets appelés ATM, ainsi que le règlement des achats. Si vous utilisez une carte de crédit française, renseignez-vous auprès de votre banque sur les frais et commissions de change qui vous seront facturés.</p>
<p>Il est toutefois préférable de demander à votre banque australienne une carte de crédit soit à débit différé (30 jours), soit à débit simple. Celle-ci ne permet pas le débit différé à 30 jours, ni les paiements sur Internet et à l’international. Le retrait d’espèces avec votre carte de débit dans un distributeur ATM distinct du réseau de votre banque australienne entraînera à chaque opération des frais. Il s’effectue grâce à un code appelé PIN que vous choisirez.</p>
<p>Le <i>cash out</i> est un moyen de retirer des espèces, sans frais, aux caisses des supermarchés et/ou dans les bureaux de poste. Au moment de régler vos achats, on vous demandera si vous souhaitez retirer de l’argent (<i>Any cash out ?</i>). Si oui, votre compte sera débité automatiquement du montant de vos achats additionné du montant que vous aurez choisi et qui vous sera remis en espèces. Le principe est semblable à celui du distributeur de billets.</p>
<p>Certains commerces (stations services, magasins, supermarchés) sont équipés de distributeurs de billets ATM. Les chèques de voyage sont également acceptés.</p>
<p><strong>Virement bancaire</strong></p>
<p>Le virement d’un compte français vers un compte australien peut prendre de deux à cinq jours suivant les banques et est sans limitation de montant dans un sens comme dans l’autre. Le dépôt d’un chèque émanant d’une banque étrangère peut prendre de trois à quatre semaines. Le taux de change varie en fonction de la nature de l’opération, change d’espèces ou de chèques, cette dernière solution étant plus avantageuse. Un expatrié français peut solder son compte en fin de séjour. Un quitus fiscal est exigé avant de quitter le pays.</p>
<p><strong>Transfert d’argent</strong></p>
<p>Si vous avez un besoin urgent d’argent, vous pouvez avoir recours aux agences <a href="http://www.westernunion.com/" class="spip_out" rel="external">Western Union</a> ; un de vos proches en France doit se rendre dans un bureau de poste <a href="https://www.labanquepostale.fr/" class="spip_out" rel="external">proposant ce service</a> et remplir un formulaire. Une commission, au prorata du montant viré, sera prélevée. Pour pouvoir récupérer l’argent, vous devez vous rendre dans une agence de Western Union, muni du numéro de transfert (<i>Money Transfert Control Number</i>) et d’une pièce d’identité. Rapide, cette solution permet le transfert d’un montant maximum de 8 000€, l’argent étant disponible dans les minutes qui suivent la confirmation de l’ordre ; en fonction du décalage horaire, la procédure peut prendre quelques heures.</p>
<h3 class="spip"><a id="sommaire_3"></a>Budget</h3>
<p>L’Australie offre une excellente qualité de vie. Selon l’étude « Top Ten des villes les plus agréables à vivre au monde », menée en 2012 par <i>The</i> <i>Economist</i>, la ville australienne de Melbourne, située dans l’Etat du Victoria, arrive en première position pour la deuxième année consécutive ; Adelaïde, cinquième ville de l’Australie, située dans l’Etat du South Australia, se situe en sixième position, suivie de Sydney, Etat du New South Wales et de Perth, Western Australia, qui arrive neuvième.</p>
<p><strong>Le coût de la vie est élevé</strong> en Australie et varie selon les Etats et les capitales. Dans le Queensland, mais aussi en Australie méridionale et occidentale, les dépenses quotidiennes demeurent inférieures à celles du reste du pays. Le mode de vie et le lieu d’habitation influent inévitablement sur le budget.</p>
<p><strong>Coût de la vie en Australie - Classement des villes par ordre décroissant (indice 100)</strong></p>
<p>Résultats de l’étude ECA International sur le coût de la vie : il y a seulement trois ans, pas une ville australienne ne se trouvait dans le top 50 de ce classement ; dorénavant les villes australiennes sont en tête de classement.Sydney, classée 16e, a le coût de la vie le plus élevé, suivie par Canberra, Adélaïde, Melbourne, Brisbane, Darwin et Perth :</p>
<ul class="spip">
<li>17 - Sydney (16ème en 2012)</li>
<li>23 - Canberra (17ème en 2012)</li>
<li>27 - Perth (23ème en 2012)</li>
<li>28 - Melbourne (21ème en 2012)</li>
<li>30 - Adelaïde (18ème en 2012)</li>
<li>34 - Darwin (24ème en 2012)</li>
<li>35 - Brisbane (24ème en 2012)</li></ul>
<p><strong>A noter</strong> : l’obtention d’un visa étudiant est assujettie à certaines exigences financières imposées par le ministère australien de l’Immigration et de la citoyenneté. Ainsi il vous faut disposer d’un budget annuel de18610 AUD pour vos besoins personnels.</p>
<table class="spip" summary="">
<caption>Prix moyen d’un repas dans un restaurant (en AUD)</caption>
<tbody>
<tr class="row_odd odd">
<td>Restaurant « à prix moyens »</td>
<td class="numeric virgule">40,00</td></tr>
<tr class="row_even even">
<td>Restaurant « bon-marché »</td>
<td class="numeric virgule">17,00</td></tr>
<tr class="row_odd odd">
<td>Restauration rapide</td>
<td class="numeric virgule">8,00</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_4"></a>Evolution des prix</h3>
<table class="spip" summary="">
<caption>Evolution de l’inflation</caption>
<tbody>
<tr class="row_odd odd">
<td>2001 = 3,10%</td>
<td>2004 = 2,6%</td>
<td>2007 = 2,9%</td>
<td>2010 = 2,7%</td></tr>
<tr class="row_even even">
<td>2002 = 3,0%</td>
<td>2005 = 2,7%</td>
<td>2008 = 3,7%</td>
<td>2011 = 3,1%</td></tr>
<tr class="row_odd odd">
<td>2003 = 2,4%</td>
<td>2006 = 3,3%</td>
<td>2009 = 2,1%</td>
<td>2012 = 2,2%</td></tr>
</tbody>
</table>
<table class="spip" summary="">
<caption>Evolution de l’indice des prix à la consommation (tous groupes confondus) dans les grandes villes</caption>
<thead><tr class="row_first"><th id="id9f05_c0"> </th><th id="id9f05_c1"> Annuelle 2010 </th><th id="id9f05_c2">Annuelle 2011 </th><th id="id9f05_c3">Annuelle 2012 </th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="id9f05_c0">Sydney</td>
<td headers="id9f05_c1">2,4 %</td>
<td headers="id9f05_c2">3,2 %</td>
<td headers="id9f05_c3">2,5 %</td></tr>
<tr class="row_even even">
<td headers="id9f05_c0">Melbourne</td>
<td headers="id9f05_c1">3,1 %</td>
<td headers="id9f05_c2">3,1 %</td>
<td headers="id9f05_c3">2,1 %</td></tr>
<tr class="row_odd odd">
<td headers="id9f05_c0">Brisbane</td>
<td headers="id9f05_c1">3 %</td>
<td headers="id9f05_c2">2,4 %</td>
<td headers="id9f05_c3">2,2 %</td></tr>
<tr class="row_even even">
<td headers="id9f05_c0">Adélaïde</td>
<td headers="id9f05_c1">2,5 %</td>
<td headers="id9f05_c2">3,6 %</td>
<td headers="id9f05_c3">2,1 %</td></tr>
<tr class="row_odd odd">
<td headers="id9f05_c0">Perth</td>
<td headers="id9f05_c1">2,6 %</td>
<td headers="id9f05_c2">2,9 %</td>
<td headers="id9f05_c3">2,1 %</td></tr>
<tr class="row_even even">
<td headers="id9f05_c0">Hobart</td>
<td headers="id9f05_c1">2,3 %</td>
<td headers="id9f05_c2">3,2 %</td>
<td headers="id9f05_c3">1,0 %</td></tr>
<tr class="row_odd odd">
<td headers="id9f05_c0">Darwin</td>
<td headers="id9f05_c1">2,4 %</td>
<td headers="id9f05_c2">2,4 %</td>
<td headers="id9f05_c3">2,5 %</td></tr>
<tr class="row_even even">
<td headers="id9f05_c0">Canberra</td>
<td headers="id9f05_c1">2,1 %</td>
<td headers="id9f05_c2">3,6 %</td>
<td headers="id9f05_c3">1,7 %</td></tr>
<tr class="row_odd odd">
<td headers="id9f05_c0">National</td>
<td headers="id9f05_c1">2,7 %</td>
<td headers="id9f05_c2">3,1%</td>
<td headers="id9f05_c3">2,2%</td></tr>
</tbody>
</table>
<table class="spip" summary="">
<caption>Evolution des prix par type de dépenses</caption>
<thead><tr class="row_first"><th id="ida5a4_c0">Type de dépenses</th><th id="ida5a4_c1">Annuelle 2011
</th><th id="ida5a4_c2">Annuelle 2012</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="ida5a4_c0">Alimentation</td>
<td headers="ida5a4_c1">2,5%</td>
<td headers="ida5a4_c2">0,3%</td></tr>
<tr class="row_even even">
<td headers="ida5a4_c0">Boissons alcoolisées et tabac</td>
<td headers="ida5a4_c1">3,1%</td>
<td headers="ida5a4_c2">3,5%</td></tr>
<tr class="row_odd odd">
<td headers="ida5a4_c0">Vêtements et chaussures</td>
<td headers="ida5a4_c1">2,6%</td>
<td headers="ida5a4_c2">0,6%</td></tr>
<tr class="row_even even">
<td headers="ida5a4_c0">Logement</td>
<td headers="ida5a4_c1">4,0%</td>
<td headers="ida5a4_c2">4,4%</td></tr>
<tr class="row_odd odd">
<td headers="ida5a4_c0">Dépenses familiales (dont services)</td>
<td headers="ida5a4_c1">0,2%</td>
<td headers="ida5a4_c2">0,8%</td></tr>
<tr class="row_even even">
<td headers="ida5a4_c0">Santé</td>
<td headers="ida5a4_c1">3,6%</td>
<td headers="ida5a4_c2">7,7%</td></tr>
<tr class="row_odd odd">
<td headers="ida5a4_c0">Transports</td>
<td headers="ida5a4_c1">4,0%</td>
<td headers="ida5a4_c2">2,0 %</td></tr>
<tr class="row_even even">
<td headers="ida5a4_c0">Communication</td>
<td headers="ida5a4_c1">1,6%</td>
<td headers="ida5a4_c2">1,6%</td></tr>
<tr class="row_odd odd">
<td headers="ida5a4_c0">Loisirs</td>
<td headers="ida5a4_c1">0,5%</td>
<td headers="ida5a4_c2">-1,7%</td></tr>
<tr class="row_even even">
<td headers="ida5a4_c0">Education</td>
<td headers="ida5a4_c1">5,8%</td>
<td headers="ida5a4_c2">6,1%</td></tr>
<tr class="row_odd odd">
<td headers="ida5a4_c0">Services financiers et assurances</td>
<td headers="ida5a4_c1">5,6%</td>
<td headers="ida5a4_c2">3,1%</td></tr>
</tbody>
</table>
<p><i>Mise à jour : octobre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/vie-pratique/article/cout-de-la-vie-108673). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
