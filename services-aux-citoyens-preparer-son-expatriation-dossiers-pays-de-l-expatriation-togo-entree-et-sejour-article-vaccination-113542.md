# Vaccination

<p>Seule la vaccination contre la fièvre jaune est exigée pour tous les voyageurs âgés de plus d’un an (contrôle du carnet de vaccination à l’aéroport). Les vaccinations suivantes sont conseillées pour des raisons médicales :</p>
<p><strong>Adultes</strong> : mise à jour des vaccinations contre la fièvre jaune, la diphtérie, le tétanos et la poliomyélite ; vaccination contre la typhoïde (longs séjours), l’hépatite A (à partir de 50 ans, une recherche préalable d’anticorps sériques totaux est justifiée), l’hépatite B (longs séjours et/ou séjours à risques). IDR à la tuberculine récent.</p>
<p><strong>Enfants</strong> : vaccinations recommandées en France par le ministère de la Santé - et en particulier : B.C.G. et hépatite B dès le premier mois (longs séjours), rougeole dès l’âge de neuf mois, hépatite A possible dès l’âge d’un an et la typhoïde à partir de cinq ans (longs séjours).</p>
<p>Pour des séjours au Nord du Togo : méningite à méningocoque A et C dès l’âge de trois mois.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Notre rubrique thématique <a href="services-aux-citoyens-preparer-son-expatriation-sante.md" class="spip_in">Santé</a></p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/entree-et-sejour/article/vaccination-113542). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
