# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/vie-pratique/article/cout-de-la-vie-113560#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/vie-pratique/article/cout-de-la-vie-113560#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/vie-pratique/article/cout-de-la-vie-113560#sommaire_3">Alimentation</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/vie-pratique/article/cout-de-la-vie-113560#sommaire_4">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>L’unité monétaire est le franc CFA.</p>
<p>Le franc CFA vaut 0,00152449 euros, c’est-à-dire qu’un euro équivaut à 655,95 francs CFA.</p>
<p>Pour obtenir des Francs CFA, il est possible de changer ses devises sur place ou d’effectuer des transferts depuis une banque française moyennant des frais.</p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<p>L’utilisation des cartes bancaires n’est pas très répandue, la carte visa peut être acceptée dans certains magasins et grands hôtels. La carte Mastercard n’est pas reconnue.</p>
<h3 class="spip"><a id="sommaire_3"></a>Alimentation</h3>
<p>Des pénuries temporaires sur quelques produits laitiers importés peuvent entraîner un report de la consommation vers d’autres produits courants. Il est préférable de se procurer les produits de toilette spécifiques, les articles relevant de l’épicerie fine et le linge de maison avant le départ.</p>
<p>Prix moyen d’un repas dans un restaurant</p>
<p>Dans un restaurant de qualité moyenne le prix d’un repas est de 23€.</p>
<h3 class="spip"><a id="sommaire_4"></a>Evolution des prix</h3>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/vie-pratique/article/cout-de-la-vie-113560). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
