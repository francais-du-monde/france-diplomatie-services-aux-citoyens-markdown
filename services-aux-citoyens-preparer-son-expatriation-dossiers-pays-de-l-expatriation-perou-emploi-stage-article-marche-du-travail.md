# Marché du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/emploi-stage/article/marche-du-travail#sommaire_1">Contexte</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/emploi-stage/article/marche-du-travail#sommaire_2">Secteurs à fort potentiel</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/emploi-stage/article/marche-du-travail#sommaire_3">Rémunération</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Contexte</h3>
<p>Le décret législatif 689 du 5 novembre 1991 et son règlement (décret suprême 014-92 TR du 23 septembre 1992) énoncent que les entreprises nationales et étrangères pourront employer du personnel étranger à hauteur de 20% du total des employés et que leurs rémunérations ne pourront pas être supérieures de plus de 30% à la liste de référence des salaires.</p>
<p>Cependant, les employeurs pourront solliciter une exonération de ce pourcentage limitatif s’il s’agit de personnel professionnel, de techniciens spécialisés, du personnel de direction et/ou gérants, et selon ce qui est prévu dans la réglementation.</p>
<p>Pour plus de détails :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.mintra.gob.pe/contenidos/archivos/prodlab/Reglamento%20de%20Ley%20de%20contratacion%20de%20personal%20extranjero.pdf" class="spip_out" rel="external">Règlement relatif à l’embauche des travailleurs étrangers</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Secteurs à fort potentiel</h3>
<p>Avec une croissance de de 5,7% en 2013, le Pérou fait aujourd’hui partie des économies les plus dynamiques de la région avec des perspectives très optimistes pour les années suivantes.<br class="autobr"><strong>Le Pérou bénéficie de richesses naturelles en abondance.</strong> De ce fait, les secteurs de la pêche, l’agriculture maraîchère tournée vers l’exportation et la culture à contre-saison, l’extraction de minerais (zinc, or, cuivre, plomb) ainsi que d’hydrocarbures, sont des secteurs développés nécessitant une main d’œuvre qualifiée.</p>
<p><strong>L’industrie reste relativement peu développée</strong>. Le secteur du tourisme propose de nombreuses possibilités (sites naturels et archéologiques précolombiens, tourisme d’aventure en forêt amazonienne ou dans les Andes, tourisme balnéaire sur la côte).</p>
<p><strong>Des possibilités existent aussi dans le secteur des infrastructures</strong> (assainissement des eaux, transport, énergie).</p>
<p>Il est recommandé de faire appel à un avocat pour la constitution d’une société afin de mettre en place un projet d’investissement. Le pays cherche notamment à diversifier son économie. Les entreprises françaises qui possèdent des technologies et un savoir-faire spécifiques pourront facilement s’implanter au Pérou.</p>
<h3 class="spip"><a id="sommaire_3"></a>Rémunération</h3>
<p>La durée maximale de travail est de 8 heures/jour et de 48 heures/semaine.</p>
<p>Depuis le 1er juin 2012, le salaire minimum est de 750 soles par mois (environ 267 USD). La rémunération du travail de nuit est majorée de 35% (22h00-6h00). <br class="autobr">La périodicité du paiement varie en fonction de la catégorie de personnel :</p>
<ul class="spip">
<li>pour les <strong>salariés</strong> : les ouvriers reçoivent un salaire journalier avec un paiement hebdomadaire et les employés, un salaire mensuel payé à la quinzaine ou au mois ;</li>
<li>pour les <strong>travailleurs indépendants</strong> : elle est fixée dans le contrat.</li></ul>
<p>Le salaire n’augmente pas avec l’ancienneté. Le versement du salaire se fait par virement ou en liquide.</p>
<p>Il existe un système obligatoire de pensions (assurance vieillesse, invalidité et survivants) soit public (SNP), soit privé (SPP). Les cotisants du SNP paient 13% de leurs revenus bruts et ceux du SPP en moyenne 13,2%.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.trabajo.gob.pe/archivos/file/faqs/PREGUNTAS_FRECUENTES_2012.pdf" class="spip_out" rel="external">Le site du ministère du travail péruvien</a>.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/emploi-stage/article/marche-du-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
