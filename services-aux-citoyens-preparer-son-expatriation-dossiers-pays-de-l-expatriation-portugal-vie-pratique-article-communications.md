# Communications

<h4 class="spip">Téléphone – Internet</h4>
<p>Les liaisons téléphoniques sont bonnes.</p>
<p>Pour appeler le Portugal de l’étranger : ajouter le 2 juste après le code international : 351 21 pour Lisbonne ou 351 22 pour Porto.</p>
<p>Le Portugal possède trois réseaux de téléphonie mobile (TMN, Vodafone et Optimus) couvrant l’ensemble du territoire.</p>
<p>Il existe des <a href="http://www.cybercafes.com/" class="spip_out" rel="external">cybercafés</a> dans les plus grandes villes.</p>
<h4 class="spip">Utilisation de votre portable français</h4>
<p>Vous pouvez utiliser votre téléphone portable partout en Europe et dans de nombreuses autres régions du monde grâce à la norme technique GSM de l’Union européenne. Les personnes qui utilisent leur téléphone portable à l’étranger réalisent d’importantes économies – de l’ordre de 60 % – grâce au règlement européen sur l’itinérance (<i>« roaming »</i>), qui plafonne le coût des appels téléphoniques effectués ou reçus dans un autre pays de l’Union européenne (« eurotarif »). De plus, une facturation à la seconde a été introduite après les premières 30 secondes pour les appels effectués et dès la première seconde pour les appels reçus. Le coût d’envoi d’un texto depuis l’étranger a lui aussi nettement diminué, il est désormais de 11 cents (hors TVA) maximum.</p>
<p>Les clients reçoivent un message automatique les informant du coût des appels, des textos et des services de transmission de données à l’étranger (en cas de navigation sur Internet ou de téléchargement d’un film, par exemple). Les prix de gros que les opérateurs se facturent mutuellement sont également plafonnés, ce qui devrait réduire les coûts supportés par les consommateurs. Un mécanisme a également été mis en place afin d’éviter aux consommateurs de devoir payer des factures astronomiques pour des services d’itinérance : après un premier avertissement, la connexion Internet mobile est suspendue dès que le montant atteint un certain niveau. Une limite de 50 euros par mois s’applique par défaut si le client n’a pas défini un montant spécifique.</p>
<p>Vous trouverez sur un <a href="http://ec.europa.eu/information_society/activities/roaming/index_en.htm" class="spip_out" rel="external">site Internet de l’Union européenne</a> les eurotarifs appliqués par les opérateurs des 28 Etats membres, ainsi que les liens vers leurs sites. Vous pourrez également y comparer les tarifs pratiqués pour l’envoi de textos ou l’utilisation de services de données.</p>
<p><i>Source : <a href="http://europa.eu/travel/comm/index_fr.htm#phone" class="spip_out" rel="external">Europa.eu</a></i></p>
<h4 class="spip">Téléphoner gratuitement par Internet </h4>
<p>Il est avantageux d’utiliser <strong>des logiciels de téléphonie sur IP</strong>, car les tarifs proposés à l’international sont souvent moindres que ceux des opérateurs locaux exploitant des lignes classiques, voire gratuits dans certaines conditions.</p>
<h4 class="spip">Poste</h4>
<p> (la poste au Portugal = CTT – Correios)</p>
<p>Le délai moyen d’acheminement du courrier postal est de quatre jours. La distribution n’est faite que pour les adresses exactes et complètes ; il est donc nécessaire d’apporter un maximum de précision au libellé des adresses (notamment l’étage, …).</p>
<p>Tarif pour une lettre de 20 grammes : 0,36 € au Portugal et 0,70 € pour la France.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/vie-pratique/article/communications). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
