# Croatie

<p>Au <strong>31 décembre 2014</strong>, le nombre des Français enregistrés s’établit à <strong>1076</strong>.</p>
<p>Les expatriés représentent environ 60% des enregistrés (Français établis depuis moins de cinq ans dans la circonscription). Il s’agit d’une population jeune, environ 50% des inscrits ayant moins de 35 ans, tandis que 15% sont des retraités.</p>
<p>La présence française augmente régulièrement avec près de 60 filiales (Dukat, Société générale, PSA, l’Oréal, Axéréal, CGA-CGM…) et bureaux de représentation (Air-France, AFP…) Les sociétés à participation française emploient aujourd’hui près de 8000 personnes. Elles sont représentées depuis avril 2012 par un <a href="http://www.fhpk.hr/" class="spip_out" rel="external">Club d’affaires franco-croate</a>.</p>
<p>La communauté française bénéficie d’un campus franco-allemand (Eurocampus). Les deux écoles, française et allemande, partagent les mêmes locaux et mutualisent un certain nombre d’ateliers pédagogiques.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/croatie/" class="spip_in">Une description de la Croatie, de sa situation politique et économique et des informations actualisées sur la situation sécuritaire du pays</a> ;</li>
<li><a href="http://www.ambafrance-hr.org/" class="spip_out" rel="external">Ambassade de France en Croatie</a>.</li></ul>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-croatie-entree-et-sejour-22940.md">Entrée et séjour</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-croatie-entree-et-sejour-22940-article-passeport-visa-permis-de-travail-110880.md">Passeport, visa, permis de travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-croatie-entree-et-sejour-22940-article-demenagement-110881.md">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-sante.md">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-croatie-entree-et-sejour-22940-article-animaux-domestiques-110883.md">Animaux domestiques</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-croatie-emploi-stage.md">Emploi, stage</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-croatie-emploi-stage-article-marche-du-travail-110884.md">Marché du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-croatie-emploi-stage-article-reglementation-du-travail-110885.md">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-croatie-emploi-stage-article-recherche-d-emploi.md">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-croatie-emploi-stage-article-stages.md">Stages</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-croatie-protection-sociale.md">Protection sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-croatie-protection-sociale-article-regime-local-de-securite-sociale.md">Régime local de sécurité sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-croatie-protection-sociale-article-convention-de-securite-sociale-110892.md">Convention de sécurité sociale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-croatie-fiscalite.md">Fiscalité</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-croatie-fiscalite-article-fiscalite-du-pays-110893.md">Fiscalité du pays</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-croatie-fiscalite-article-convention-fiscale.md">Convention fiscale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-croatie-vie-pratique.md">Vie pratique</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-croatie-vie-pratique-article-logement-110897.md">Logement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-croatie-vie-pratique-article-sante.md">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-croatie-vie-pratique-article-scolarisation-110895.md">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-croatie-vie-pratique-article-cout-de-la-vie-110898.md">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-croatie-vie-pratique-article-transports-110899.md">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-croatie-vie-pratique-article-communications-110900.md">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-croatie-vie-pratique-article-loisirs-et-culture.md">Loisirs et culture</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-croatie-vie-pratique-article-pour-en-savoir-plus-110902.md">Pour en savoir plus</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-croatie-presence-francaise.md">Présence française</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-croatie-presence-francaise-article-presence-francaise.md">Présence française</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
