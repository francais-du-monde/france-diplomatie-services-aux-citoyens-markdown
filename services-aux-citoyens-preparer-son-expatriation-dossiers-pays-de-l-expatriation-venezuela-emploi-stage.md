# Emploi, stage

<h2 class="rub22137">Marché du travail</h2>
<p class="chapo">
    Après deux années de récession (en 2009 et 2010), l’économie vénézuélienne a rebondi en 2011 (+4,2%), renouant avec une croissance soutenue qui lui permet de figurer au 5ème rang des puissances latino-américaines, avec un PIB estimé à 315 Mds USD.
</p>
<p>Le pays possède un <strong>taux de chômage de 7,4%</strong>, représentant 1,1 million de chômeurs, auxquels s’ajoutent les 260000 jeunes entrant chaque année sur le marché du travail. <strong>L’emploi informel représente quant à lui près de 43% du total.</strong></p>
<p><strong>Environ 60 filiales françaises sont présentes sur le sol vénézuélien</strong>. Il s’agit en partie de grandes firmes et de quelques PME qui se sont implantées avec succès. Le secteur privilégié des investissements français reste avant tout celui des hydrocarbures (Total, Technip, Schlumberger, IFP…). Les entreprises françaises sont également impliquées dans les domaines électriques et ferroviaires, en particulier dans le développement du métro de Caracas. Parmi les autres entreprises françaises dans le pays, on peut citer Thales, Alstom, Legrand, GDF-Suez, Saint Gobain, Rhodia, Alcatel, Air France, Renault, PSA, Pernod Ricard, L’Oréal, Yves Rocher, Veritas, Michelin, Sanofi, Servier, Casino et Veolia.</p>
<h4 class="spip">Secteurs porteurs</h4>
<h4 class="spip">Le secteur des hydrocarbures</h4>
<p>Le pays dispose d’un énorme potentiel dans le domaine des hydrocarbures : il possède les premières réserves prouvées de brut au monde (297 Mds de barils, soit 20% des réserves de la planète) devant l’Arabie Saoudite ainsi que les huitièmes réserves de gaz. Il est le sixième producteur mondial de pétrole (2,85 Mb/j) et le neuvième exportateur (1,56 Mb/j), Dès l’accession au pouvoir d’Hugo Chavez en 1999, la nouvelle administration a exprimé sa volonté de poursuivre le développement du secteur, en particulier dans la ceinture pétrolifère de l’Orénoque, région stratégique concentrant près de 86% des réserves d’hydrocarbures du pays (256 Mds de barils), qui offre de nombreuses opportunités d’investissements.</p>
<h4 class="spip">Le marché du matériel et des équipements médicaux</h4>
<p>La santé constitue l’une des priorités des autorités comme en témoigne la hausse significative des investissements publics et privés dans ce secteur depuis quelques années. Dans ce domaine, représentant 40% de nos ventes, la France bénéficie d’une excellente image en termes de qualité et de fiabilité.</p>
<h4 class="spip">Le marché des parfums et des cosmétiques</h4>
<p>Ce marché (10% de nos exportations) connaît actuellement une nette récupération et se positionne au premier rang des pays de la communauté andine en termes de consommation.</p>
<h4 class="spip">Les biens d’équipements</h4>
<p>La croissance de l’économie vénézuélienne s’est notamment manifestée par une augmentation de l’importation des biens d’équipements. Les secteurs les plus dynamiques sont le secteur électrique et les transports.</p>
<h4 class="spip">L’agriculture et l’industrie agroalimentaire</h4>
<p>Le développement du secteur agricole (3% du PIB) fait également partie des priorités du gouvernement. De par ses conditions climatiques, son immense territoire (2 fois la France) et ses longues côtes, le Venezuela présente un fort potentiel, en particulier dans les domaines de la pêche et de l’aquaculture. Ces dernières années, le gouvernement met en œuvre une politique de relance afin de donner un nouvel élan à la production agricole : café, fruits tropicaux, riz, tabac, et cacao.</p>
<h4 class="spip">Autres secteurs dynamiques</h4>
<p>Parmi les autres secteurs dynamiques on peut également citer le secteur bancaire et celui des télécommunications.</p>
<h4 class="spip">Rémunération</h4>
<p>Le <strong>salaire minimum</strong> légal est de <strong>2047,48 bolivars par mois</strong>, ce qui correspond à environ 240 euros.</p>
<p><i>Mise à jour : décembre 2013</i></p>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/emploi-stage/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
