# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/emploi-stage/article/reglementation-du-travail-110378#sommaire_1">Les congés</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/emploi-stage/article/reglementation-du-travail-110378#sommaire_2">Salaires, primes et charges sociales</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/emploi-stage/article/reglementation-du-travail-110378#sommaire_3">Emploi du conjoint</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/emploi-stage/article/reglementation-du-travail-110378#sommaire_4">Contrats de travail - spécificités</a></li></ul>
<p>Droit du travail</p>
<h4 class="spip">Législation du travail</h4>
<p>Le droit du travail est assez contraignant en Grèce, malgré les réformes entreprises ces dernières années visant à accroître la flexibilité du marché. En l’absence de codification, une jurisprudence abondante complète une pluralité de textes réglementaires.</p>
<p>Cette situation explique en grande partie l’existence de l’économie informelle dans plusieurs secteurs de l’économie, notamment le bâtiment et le tourisme. Le gouvernement s’efforce de réduire l’emploi non déclaré en renforçant l’inspection du travail.</p>
<h5 class="spip">Les relations sociales</h5>
<p><strong>Libertés syndicales</strong></p>
<p>La Grèce a ratifié les conventions 87 et 98 de l’OIT sur la liberté d’association et la protection du droit de s’organiser, le droit d’organiser et de négocier collectivement. La liberté syndicale et l’exercice des droits syndicaux pour les travailleurs sont également garantis par la Constitution.</p>
<p>La loi 1876/1990 (articles 22 et 23) a instauré pour les partenaires sociaux une obligation périodique de négocier et elle a mis en place le cadre juridique des conventions collectives (générale nationale, de branche, de profession et d’entreprise.</p>
<p>En vertu de la législation votée en 2012, les conventions collectives du travail seront conclues désormais pour une durée minimale d’un an et maximale de trois ans.</p>
<p>Celles qui étaient en vigueur au 14 février 2012, depuis moins de deux ans, arriveront à expiration à la fin des trois ans suivant la date de leur mise en vigueur, sauf si elles sont dénoncées plus tôt par les parties.</p>
<p>Les clauses réglementaires d’une convention de branche restent en vigueur pendant une période de trois mois avant son échéance ou sa dénonciation, au lieu de six mois précédemment. A la fin des trois mois, et jusqu’à la conclusion d’une nouvelle convention, les clauses de la convention précédente concernant le salaire de base, les primes d’ancienneté, d’enfants, d’études et de travail dangereux restent en vigueur. Toute autre prime est supprimée.</p>
<p><strong>Droit de grève</strong></p>
<p>Le droit de grève est garanti par la Constitution. Il est exercé par les organisations syndicales légalement constituées. La loi prévoit que la grève doit faire l’objet d’un préavis de 24 heures minimum.</p>
<p><strong>Contrôle du droit du travail</strong></p>
<p>L’application de la réglementation du travail est contrôlée par l’administration et par les tribunaux.</p>
<p>Le contrôle de l’Inspection du travail aboutit dans la majorité des cas à la médiation. L’Inspection du travail bénéficie cependant d’un large pouvoir et a la possibilité d’actionner le juge pénal, seule ou conjointement avec le salarié lésé.</p>
<p>Les tribunaux jouent également un rôle important dans l’évolution du droit du travail.</p>
<p>Depuis février 2012, le recours unilatéral à l’arbitrage prévu par l’article 14 de la loi 3899/10 n’est plus permis : le recours n’est possible qu’avec l’accord des parties et ne peut concerner que la fixation du salaire de base, journalier ou mensuel.</p>
<p>Parmi les éléments à prendre en considération, l’arbitre peut retenir les données économiques et financières, la conjoncture, l’évolution de la compétitivité, la diminution du coût du travail par unité de production pendant la durée du programme d’adaptation budgétaire du pays et l’activité de production dans la branche professionnelle concernée.</p>
<p>Les recours qui ont été déposés unilatéralement auprès de l’Organisme de médiation et d’arbitrage (OMED) et n’ont pas encore donné lieu à une décision d’arbitrage, ne seront pas discutés et seront classés.</p>
<p><strong>Les réformes en cours visent à rendre plus</strong> flexibles les rémunérations dans le secteur privé afin de maîtriser le coût du travail sur le long terme. Le gouvernement vient d’institutionnaliser l’octroi de salaires inférieurs au salaire de base pour les nouveaux arrivants sur le marché du travail, d’assouplir le régime des licenciements collectifs et de faciliter <strong>l’emploi à temps partiel.</strong></p>
<p><strong>Loi 3846/2010 (JO A/66/11.5.10) portant garantie de la sécurité du travail</strong>.</p>
<h4 class="spip">Formes spécifiques d’emploi</h4>
<p>L’article 1er de la loi prévoit que le contrat entre l’employeur et l’employé sur la prestation de service ou d’ouvrage pour une durée déterminée ou indéterminée, notamment dans les cas de rémunération à façon, télétravail, emploi à domicile, est considéré comme une relation de travail dépendante déguisée si le travail est exécuté par la personne elle-même, exclusivement ou en majeure partie auprès du même employeur pendant neuf mois consécutifs.</p>
<h5 class="spip">Travail à temps partiel</h5>
<p>L’employeur et l’employé peuvent, par contrat individuel écrit, convenir d’un travail journalier, hebdomadaire, bimensuel ou mensuel pour une durée déterminée ou indéterminée mais à temps partiel. Ce contrat doit être communiqué dans les huit jours à l’Inspection du travail, faute de quoi, il est considéré comme contrat à temps plein déguisé.</p>
<p>Le contrat de travail peut prévoir toute forme d’emploi par roulement (emploi sur un plus petit nombre de jours par semaine ou moins de semaines par mois ou moins de mois par an), panaché avec un horaire de travail normal.</p>
<p>Lorsque ses activités sont en baisse, l’employeur peut, au lieu de dénoncer le contrat de travail, appliquer, après information et concertation avec les représentants du personnel, un système d’emploi par roulement dans son entreprise, dont la durée ne peut dépasser six mois par an. Les accords sont notifiés dans les huit jours à l’Inspection du travail. En cas d’absence d’organisation syndicale ou de conseil des salariés, la concertation a lieu avec l’ensemble du personnel.</p>
<p>Les contrats individuels doivent mentionner l’identité des contractants, le lieu du travail, le siège de l’entreprise ou l’adresse de l’employeur, la durée de l’emploi, le mode de répartition et les périodes de travail, le mode de rémunération et tout avenant au contrat.</p>
<p>Si le travail à temps partiel prévoit un horaire journalier réduit, le travail doit être continu et quotidien.</p>
<p>Dans tous les cas, l’emploi les dimanches et jours fériés ainsi que le travail de nuit doivent être rémunérés avec les augmentations légales.</p>
<p>Les rémunérations des salariés à temps partiel sont calculées sur la base des rémunérations à temps plein, au prorata des heures travaillées. Si le temps de travail est inférieur à quatre heures par jour, la rémunération est augmentée de 7,5%.</p>
<p>La dénonciation d’un contrat de travail pour cause de refus par le salarié de la proposition par l’employeur de travailler à temps partiel est nulle.</p>
<p>Les salariés à temps partiel ont droit à un congé annuel rémunéré et une gratification de congés d’un montant proportionnel à leur rémunération.</p>
<p>Au cas où il s’avère nécessaire de fournir un travail supplémentaire au-delà du temps contractuel, le salarié a l’obligation de la fournir s’il en mesure de le faire et son refus serait considéré comme contraire à la bonne foi. Le travail supplémentaire est alors rémunéré avec une augmentation de 10%.</p>
<p>Le salarié travaillant à temps plein dans une entreprise de plus de 20 employés peut, après un an de travail, demander la transformation de son contrat de travail en contrat à temps partiel, tout en conservant le droit de revenir au temps plein, sauf si le refus de l’employeur est justifié par les besoins de l’entreprise. Il doit préciser la durée pendant laquelle il va travailler à temps partiel et la nature de son activité.</p>
<p>En cas d’offre de travail dans l’entreprise, l’employé à temps partiel a un droit de priorité pour un recrutement à un poste à temps plein. La durée du temps partiel est prise en compte comme ancienneté.</p>
<p>Les salariés à temps partiel ont accès aux actions de formation professionnelle appliquées par l’entreprise, dans les mêmes conditions que les salariés à temps plein. Ils bénéficient des mêmes services sociaux que ces derniers.</p>
<p>Ces dispositions légales peuvent être complétées ou modifiées par conventions collectives d’entreprises.</p>
<h5 class="spip">Travail temporaire</h5>
<p>L’article 3 de la loi 3846/2010 prévoit que la cession de salarié à un employeur indirect n’est permise que pour des raisons spécifiques, justifiées par des besoins exceptionnels, temporaires ou saisonniers.</p>
<p>L’entreprise de travail temporaire (ETT) ne peut exercer d’autre activité que :</p>
<ul class="spip">
<li>l’intermédiation en recherche d’emploi, pour laquelle elle doit obtenir une licence spéciale, octroyée dans les conditions du décret présidentiel 160/1999 (JO 157/99) ;</li>
<li>l’évaluation de la main d’œuvre.</li></ul>
<p>Droits des salariés temporaires : l’offre de travail sous la forme d’emploi temporaire nécessite un contrat préalable, écrit, à durée déterminée ou indéterminée, établi entre l’entreprise de travail temporaire (employeur direct) et le salarié. Le contrat doit mentionner les conditions de l’emploi et sa durée ainsi que les conditions de travail auprès des employeurs indirects (qui utilise le salarié), la rémunération et les conditions de couverture sociale.</p>
<p>Les conditions de travail (y compris les rémunérations) des salariés sous contrat ou relation de travail temporaire auprès de l’employeur indirect sont au moins égales à celles qui s’appliqueraient s’ils étaient employés directement par le même employeur pour le même poste.</p>
<p>Les employés temporaires sont informés par l’employeur indirect des offres d’emploi dans l’entreprise, afin d’avoir les même possibilités que les autres employés de remplir ces postes. L’information peut être affichée à un emplacement de l’entreprise, visible et accessible.</p>
<p>L’employeur indirect doit informer les représentants du personnel sur le nombre de salariés temporaires, leur utilisation et sur les perspectives d’emploi direct.</p>
<p>Les salariés temporaires bénéficient des mêmes services sociaux que les salariés directs : cantine, crèche, transport, sauf si les conditions d’emploi justifient un traitement différent, comme l’horaire différent ou la durée du contrat. Ils jouissent, en matière d’hygiène et de sécurité, du même niveau de protection que celui offert aux salariés permanents de l’employeur indirect. Ce dernier est responsable des conditions d’exécution du travail et en cas d’accident du travail, sous réserve d’une prévision contractuelle de coresponsabilité avec l’ETT. Les salariés temporaires sont couverts par la branche maladie de l’IKA-ETAM.</p>
<p>L’emploi temporaire ne peut être supérieur à 12 mois. Exceptionnellement, la durée de l’emploi peut aller jusqu’à 18 mois, si le salarié temporaire remplace un salarié permanent dont le contrat est suspendu (pour toute raison). En cas de dépassement de ces délais, le contrat temporaire est transformé en CDI auprès de l’employeur indirect.</p>
<p>Après l’expiration du contrat initial, si un nouveau contrat est passé avant qu’un délai de 45 jours calendaires soit écoulé, le nouveau contrat est considéré comme CDI. Ces dispositions ne s’appliquent pas aux entreprises hôtelières et de restauration.</p>
<p>Toute clause au contrat qui interdirait ou empêcherait l’emploi permanent du salarié temporaire ou qui viserait à lui soustraire ses droits syndicaux ou ses droits à couverture sociale, serait nulle.</p>
<p>Le contrat passé entre l’ETT et l’employeur indirect prévoit les conditions de rémunération et d’assurance du salarié et les raisons de sa mise à disposition. L’employeur indirect doit préciser, avant le contrat, les qualifications professionnelles demandées et les caractéristiques de l’emploi.</p>
<p>L’ETT et l’employeur indirect sont solidairement responsables vis-à-vis du salarié temporaire pour ce qui concerne sa rémunération et le paiement des cotisations sociales. La responsabilité de l’employeur indirect est suspendue lorsque le contrat prévoit que la rémunération et le versement des charges sociales relèvent de l’employeur direct.</p>
<p>Le travail temporaire est interdit dans les cas suivants :</p>
<ul class="spip">
<li>lorsqu’il est utilisé pour remplacer des employés permanents qui exercent leur droit de grève</li>
<li>lorsque l’employeur indirect a procédé au cours des six mois précédents à des licenciements de salariés de la même spécialité pour des raisons économiques ou à des licenciements collectifs au cours des 12 mois précédents.</li>
<li>lorsque l’emploi, en raison de sa nature, comporte des risques particuliers pour la santé et la sécurité des travailleurs.</li>
<li>lorsque l’employé est régi par les dispositions spéciales sur l’assurance des ouvriers du bâtiment.</li></ul>
<p>En cas de non-conformité à ces dispositions légales, les sanctions varient entre 3000 et 30 000 euros, selon la gravité de la faute.</p>
<p>Par ailleurs, la Grèce a adapté en 2012, sa législation à la directive 2008/104 du Parlement européen et du Conseil, relative au travail intérimaire.</p>
<h5 class="spip">Mise en disponibilité des salariés</h5>
<p>Les entreprises dont l’activité économique est en baisse peuvent, au lieu de dénoncer les contrats de travail, mettre (par écrit) leurs salariés en disponibilité, pendant une durée de trois mois par an maximum. Cette intervention doit être précédée d’une négociation avec les représentants du personnel.</p>
<p>En l’absence de délégués du personnel dans l’entreprise, l’information et la négociation s’effectuent avec l’ensemble du personnel. Pendant la durée de sa disponibilité, le salarié perçoit la moitié de son salaire moyen des deux années précédentes, sous régime d’emploi à temps plein. A l’expiration des trois mois, le même employé ne peut être mis à nouveau en disponibilité qu’après un délai de trois mois. L’employeur doit communiquer à l’Inspection du travail, à l’IKA et à l’OAED, la déclaration de mise en disponibilité de tout ou partie de son personnel.</p>
<p>Les employeurs qui mettent leurs salariés en disponibilité sans respecter les dispositions légales, doivent verser les rémunérations dans leur totalité, même si les employés avaient accepté leur mise en disponibilité et n’avaient pas accompli leur travail.</p>
<h5 class="spip">Télétravail</h5>
<p>Lorsque l’employeur établit un contrat de télétravail, il doit fournir dans les huit jours au salarié l’ensemble des informations nécessaires à l’accomplissement de son travail, notamment le lien hiérarchique avec ses supérieurs dans l’entreprise, le détail de ses tâches, le mode de calcul de sa rémunération, le mode d’évaluation du temps de travail, l’indemnisation du coût impliqué par la prestation (installations informatiques, équipement, pannes d’appareils, etc.). Si le contrat comprend un accord sur la capacité d’être opérationnel à tout moment, il doit fixer des limites temporelles et les délais de réaction du salarié.</p>
<p>Dans le cas où le travail est transformé en télétravail, l’accord doit prévoir un temps d’adaptation de trois mois pendant lequel chacune des parties, après l’expiration d’un délai de 15 jours, peut mettre un terme au télétravail. Le salarié peut revenir dans l’entreprise à un poste correspondant à celui qu’il occupait.</p>
<p>Dans tous les cas, l’employeur prend en charge le coût subi par le salarié qui travaille sous cette forme d’emploi, en particulier, celui des télécommunications. Il fournit au salarié une aide technique et lui rembourse ses dépenses de réparation des appareils utilisés et les fait remplacer en cas de panne. Cette obligation concerne également les appareils appartenant au salarié, sauf si le contrat en prévoit différemment. Le contrat prévoit également le mode d’indemnisation du fait de l’utilisation professionnelle de la résidence du salarié. Des conventions collectives peuvent fixer des cadres spécifiques pour régler ces questions.</p>
<p>Dans les deux mois suivant l’établissement du contrat, l’employeur envoie par écrit au salarié les coordonnées du représentant du personnel.</p>
<h4 class="spip">Détermination de la durée du travail</h4>
<p>Les entreprises appliquant l’horaire conventionnel jusqu’à 40 heures par semaine peuvent, pendant une période d’activité supplémentaire, employer les salariés deux heures de plus par jour, au-delà des huit heures légales, sous réserve d’une réduction d’horaire correspondante pendant une autre période de l’année (activité réduite). Au lieu d’une réduction d’heures de travail, l’employeur peut octroyer au salarié un jour de repos ou une combinaison des deux possibilités.</p>
<p>Ces périodes d’horaire supplémentaire et d’horaire réduit ne doivent pas dépasser au total 4 mois par an. La durée moyenne de travail pendant ces 4 mois est limitée à 40 heures par semaine (ou, si l’entreprise applique un horaire inférieur, à l’horaire conventionnel), heures supplémentaires non comprises, et à 48 heures avec les heures supplémentaires.</p>
<p>La loi prévoit une autre possibilité : dans les entreprises appliquant l’horaire conventionnel jusqu’à 40 heures par semaine, si un surcroît de travail apparaît, tenant soit à la nature, soit à l’objet des activités, soit à des causes inhabituelles ou imprévues, l’employeur peut convenir avec les salariés d’une répartition annuelle de 256 heures de travail avec des périodes de travail accru ne pouvant dépasser 32 semaines par an et des périodes de travail réduit le restant de l’année. Là aussi, la durée moyenne de travail sur l’année est limitée à 40 heures par semaine (ou à l’horaire conventionnel réduit). La compensation peut s’effectuer par l’octroi d’un congé supplémentaire (repos) ou par la combinaison d’un congé et d’une réduction d’heures de travail ou par un allongement des congés payés annuels.</p>
<p>La compensation peut s’effectuer par l’octroi d’un congé supplémentaire (repos) ou par la combinaison d’un congé et d’une réduction d’heures de travail ou par un allongement des congés payés annuels.</p>
<p>Depuis 2012, (loi 4093/2012) plusieurs mesures d’assouplissement du droit du travail sont appliquées :</p>
<ul class="spip">
<li>l’organisation du temps de travail est régie par les conventions sectorielles ou d’entreprise ;</li>
<li>la période minimale de repos quotidien est réduite à 11 heures (12 auparavant) ; les dispositions légales concernant le repos obligatoire des salariés doivent s’appliquer pendant les périodes d’activité supplémentaire.</li>
<li>les horaires d’ouverture des magasins sont déconnectés des horaires des salariés, qui commencent une heure plus tôt ou s’achèvent une heure plus tard que l’horaire d’ouverture des magasins, sans préjudice des huit heures de travail conventionnelles ;</li>
<li>mise en œuvre d’un travail hebdomadaire de six jours dans certaines branches (commerce de détail, notamment), sans rémunération ou temps de repos supplémentaires, dans la mesure où les 40h hebdomadaires sont respectées.</li></ul>
<p>La rémunération du travail pendant les périodes d’activité accrue est égale à celle de la semaine normale de 40 heures (ou à celle de l’horaire conventionnel s’il est inférieur à 40 heures) et les dispositions légales sur la rémunération des heures supplémentaires ne sont pas applicables.</p>
<p>Le travail journalier est limité à 10 heures pendant les périodes d’activité intensive.</p>
<p>La loi ne modifie pas le régime précédent : pendant la période à horaire réduit, le dépassement de l’horaire de travail est permis exceptionnellement et rémunéré avec un salaire augmenté de 30% pour les cinq (ou huit) premières heures, de 75% pour les suivantes. S’il n’est pas satisfait aux conditions de la loi sur les heures supplémentaires (approbation par l’Inspection du travail), chaque heure de travail dépassant les limites ci-dessus est caractérisée comme « heure supplémentaire exceptionnelle » et rémunérée avec une augmentation de 100%.</p>
<p>Les modalités d’application de ces dispositions sont déterminées par conventions collectives d’entreprise ou par accords passés entre l’employeur et le syndicat du personnel ou le conseil des salariés ou l’union de personnes (une union est constituée de 5 salariés au moins, si le nombre total du personnel s’élève à 20 personnes). Dans les entreprises où il n’existe pas de syndicat ou de conseil de salariés ou dans celles qui emploient moins de 20 personnes, l’accord est passé entre l’employeur et le syndicat de branche ou la fédération correspondante. En cas de désaccord, la question peut être renvoyée à l’Organisme de Médiation et d’Arbitrage (OMED).</p>
<p>La loi prévoit qu’un autre système de détermination du temps de travail peut être fixé par convention collective d’entreprise ou de branche, selon les particularités de l’entreprise ou de la branche.</p>
<p>Les dispositions de la loi s’appliquent également :</p>
<ul class="spip">
<li>aux entreprises saisonnières,</li>
<li>aux salariés ayant un contrat de travail de moins d’un an.</li></ul>
<h5 class="spip">Travail du samedi</h5>
<p>La loi prévoit que le travail exécuté le 6ème jour de la semaine, par dérogation au régime des cinq jours hebdomadaires, est rémunéré avec une augmentation de 30%. Cette disposition ne s’applique pas aux secteurs de l’hôtellerie et de la restauration.</p>
<h4 class="spip">Compétences de l’Inspection du travail</h4>
<p>L’inspection du Travail doit avoir accès aux archives et registres de l’entreprise ainsi qu’à la structure du processus de production. Elle peut intervenir en conciliateur sur les conflits à l’intérieur de l’entreprise, qu’ils soient individuels ou collectifs et le directeur est tenu d’être présent ou représenté pendant la procédure.</p>
<h5 class="spip">Tableaux du personnel</h5>
<p>Lorsque l’entreprise change de représentant légal ou recrute de nouveaux salariés, l’employeur doit soumettre à l’Inspection du Travail un tableau complémentaire dans les 15 jours suivant le changement. En cas de modification de l’horaire ou de l’organisation du temps de travail, l’employeur doit fournir des tableaux complémentaires, au plus tard, le jour de changement de l’horaire ou de l’organisation et, dans tous les cas, avant la prise de fonction des employés.</p>
<h5 class="spip">Tableau des heures supplémentaires</h5>
<p>L’employeur doit tenir un registre spécial des heures supplémentaires pendant cinq ans.</p>
<h5 class="spip">Sanctions administratives</h5>
<p>L’employeur qui ne se conforme pas aux dispositions du droit du travail s’expose à :</p>
<ul class="spip">
<li>des sanctions allant de 500 à 50 000 euros,</li>
<li>une suspension du fonctionnement d’un secteur ou de l’ensemble de l‘entreprise pendant une durée pouvant aller jusqu’à trois jours, ou une fermeture définitive d’un secteur ou de toute l’entreprise.</li></ul>
<p>La loi interdit le travail des enfants de moins de 15 ans</p>
<p>Il est interdit d’employer des enfants de moins de 15 ans, sauf pour des activités culturelles (théâtrales, musicales, cinématographiques ou publicitaires) dans des conditions bien spécifiques. Entre 15 et 18 ans, le travail des enfants est très réglementé. L’employeur doit tenir un registre des employés mineurs et déposer un dossier auprès de l’inspection du travail.</p>
<h3 class="spip"><a id="sommaire_1"></a>Les congés</h3>
<p>Les congés annuels sont de 20 jours par an pour les salariés qui travaillent cinq jours par semaine (24 jours pour ceux qui travaillent six jours par semaine). Ils sont de 21 jours la deuxième année et de 22 jours à partir de la 3ème année. Le salarié ayant plus de 10 ans d’ancienneté ou bien celui ayant 12 ans de carrière au total bénéficie de 25 jours de congé. Un jour supplémentaire est octroyé après 25 ans de service.</p>
<p>La prise du congé annuel en deux périodes pendant l’année calendaire est permise exceptionnellement en cas de besoin sérieux et urgent de l’entreprise et après accord de l’Inspection du travail. La première période ne peut comprendre moins de six jours ouvrables (pour les salariés qui travaillent six jours par semaine) ou de cinq jours (pour ceux qui travaillent cinq jours par semaine) ou 12 jours pour les mineurs. Le morcellement du congé peut se faire en plus de deux périodes mais, dans ce cas, l’une des périodes doit comprendre au moins 12 jours ouvrables (pour six jours travaillés par semaine) ou 10 jours ouvrables (pour cinq jours travaillés par semaine).</p>
<p>Les congés de maternité sont de 17 semaines au total (partagés avant et après l’accouchement). Pendant les 30 mois suivant la fin du congé de maternité, la salariée peut bénéficier d’un aménagement d’horaire, avec réduction du temps de travail. six mois supplémentaires peuvent être octroyés comme congé spécial et financés par l’OAED.</p>
<h3 class="spip"><a id="sommaire_2"></a>Salaires, primes et charges sociales</h3>
<h4 class="spip">Salaires</h4>
<p>Consultez l’article <a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-grece-emploi-stage-article-marche-du-travail-110377.md" class="spip_in">Marché du travail</a>.</p>
<h4 class="spip">Les charges sociales</h4>
<p>Les contributions à l’IKA représentent 44,06% du salaire (en catégorie normale) dont 16% de part salariale et 28,06% de part patronale.</p>
<p>Comme les salaires, les contributions sociales sont versées sur 14 mois.</p>
<p>Le travail nocturne des femmes est en principe permis en Grèce mais, conformément à la Directive européenne 98/85, peut être interdit sur prescription médicale en cas de grossesse et après l’accouchement.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p><a href="http://www.ika.gr/" class="spip_out" rel="external">IKA</a> <br class="manualbr">8 rue Aghiou Constantinou <br class="manualbr">10241 Athènes <br class="manualbr">Tél. : (30) 210 522 48 61</p>
<p><strong>Bulletin du droit du travail</strong> <br class="manualbr">18 rue Emmanuel Benaki <br class="manualbr">10678 Athènes <br class="manualbr">Tél. : (30) 210 38 26 933</p>
<p><a href="http://www.ypakp.gr/" class="spip_out" rel="external">Ministère du Travail et des Affaires Sociales</a><br class="manualbr">40 rue Piréos <br class="manualbr">10182 Athènes <br class="manualbr">Tél. : (30) 210 529 50 01 <br class="manualbr">Fax : (30) 210 529 53 68</p>
<p><a href="http://www.oaed.gr/" class="spip_out" rel="external"><strong>OAED</strong> - Direction générale de l’emploi</a><br class="manualbr">8 rue Ethnikis Antistasseos Alimos - Athènes <br class="manualbr">Tél. : (30) 210 99 24 409 - 99 89 292.</p>
<h3 class="spip"><a id="sommaire_3"></a>Emploi du conjoint</h3>
<p>Les ressortissants de l’Union européenne bénéficient des mêmes droits que les ressortissants locaux en matière d’emploi. Il faut souligner qu’une bonne connaissance du grec moderne reste cependant une condition essentielle pour trouver un emploi. Le secteur du tourisme peut parfois offrir quelques opportunités saisonnières. A noter que le concubinage n’est pas reconnu en Grèce.</p>
<h4 class="spip">Cotisations sociales </h4>
<p>Les salariés du régime général doivent prendre contact avec le bureau compétent de l’<a href="http://www.ika.gr/" class="spip_out" rel="external">Idrima Koinonikon Asphalisseon</a> (IKA - ETAM) (Institut d’assurance sociale - Caisse générale d’assurance des travailleurs salariés) ou avec l’Office de l’emploi et de la main-d’œuvre (OAED). L’OAED a également des bureaux dans les quartiers des grandes villes. En province, l’IKA - ETAM représente l’OAED en matière de chômage et de prestations familiales. L’IKA est compétent en matière d’assurances maladie, maternité, décès, pension de vieillesse, d’invalidité et de survivants. En cas d’accident du travail, l’IKA - ETAM verse les prestations de l’assurance maladie pendant la période d’incapacité temporaire, de même que celles de l’assurance invalidité en cas d’incapacité permanente.</p>
<h3 class="spip"><a id="sommaire_4"></a>Contrats de travail - spécificités</h3>
<p>Toutefois, pour toute démarche ou pour l’établissement d’un contrat de travail, il est recommandé de s’entourer des conseils d’un avocat spécialisé dont une liste est disponible auprès de la mission économique UbiFrance-Grèce.</p>
<h4 class="spip">Le contrat de travail</h4>
<h5 class="spip">Contenu du contrat</h5>
<p>Le contrat de travail à temps plein peut être écrit ou oral. Le contrat à temps partiel doit être écrit.</p>
<h5 class="spip">Durée du contrat</h5>
<ul class="spip">
<li>Le contrat à durée indéterminée et à temps plein est la règle en Grèce.</li>
<li>Les contrats à durée déterminée (CDD) représentent 12,1% des contrats de travail (14,3% dans l’UE). Ce type de contrat n’est autorisé que s’il est strictement limité à une activité étant, de par sa nature, temporaire.</li>
<li>Le travail à temps partiel est encore peu développé en Grèce et ne concerne que 6% des salariés du secteur privé. Toutefois, cette forme d’emploi se développe depuis le début de la crise économique.</li></ul>
<h5 class="spip">Rupture du contrat</h5>
<ul class="spip">
<li>Le contrat de travail à durée déterminée ne peut être dénoncé avant sa date d’expiration que pour « motif sérieux ».</li>
<li>Le contrat individuel à durée indéterminée peut être rompu à tout moment, sans motif réel et sérieux, à condition de dénoncer le contrat par écrit et verser des indemnités proportionnelles à l’ancienneté de l’employé dans l’entreprise. Toutefois, le juge reste le seul habilité, en dernier ressort, à juger si le licenciement est justifié ou abusif.</li></ul>
<p>Loi 3863/10 du 15 juillet 2010 (JO A/115/10) sur le nouveau système des retraites et sur le droit du travail prévoit une réduction du coût des licenciements, des heures supplémentaires et de l’emploi des jeunes.</p>
<h5 class="spip">Régime des licenciements</h5>
<p>La loi modifie le régime des licenciements collectifs régi par la loi 1387/83 (JO A/110/83). Un licenciement est considéré comme collectif (et soumis à certaines restrictions légales) au-delà d’un minimum de :</p>
<ul class="spip">
<li>six salariés (contre quatre auparavant) dans les entreprises qui emploient entre 20 et 150 personnes,</li>
<li>5% du personnel (contre 2% auparavant) et jusqu’à 30 personnes dans les entreprises de plus de 150 salariés.Un assouplissement est également apporté au régime des licenciements individuels : un contrat à durée indéterminée, supérieur à deux mois, peut être dénoncé par écrit par l’employeur avec un préavis correspondant à son ancienneté :</li></ul>
<h5 class="spip">Préavis avant le licenciement</h5>
<table class="spip">
<thead><tr class="row_first"><th id="id3f90_c0">Ancienneté </th><th id="id3f90_c1">Préavis </th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="id3f90_c0">De 2 mois à 2 ans</td>
<td headers="id3f90_c1">1 mois</td></tr>
<tr class="row_even even">
<td headers="id3f90_c0">De 2 ans à 5 ans</td>
<td headers="id3f90_c1">2 mois</td></tr>
<tr class="row_odd odd">
<td headers="id3f90_c0">De 5 ans à 10 ans</td>
<td headers="id3f90_c1">3 mois</td></tr>
<tr class="row_even even">
<td headers="id3f90_c0">De 10 ans à 15 ans</td>
<td headers="id3f90_c1">4 mois</td></tr>
<tr class="row_odd odd">
<td headers="id3f90_c0">De 15 ans à 20 ans</td>
<td headers="id3f90_c1">5 mois</td></tr>
<tr class="row_even even">
<td headers="id3f90_c0">Au-delà de 20 ans</td>
<td headers="id3f90_c1">6 mois</td></tr>
</tbody>
</table>
<p>L’employeur qui ne fait pas usage du préavis peut dénoncer le contrat de travail en versant l’indemnité prévue par les lois 2112/20 (JO A/67/20) et 3198/55 (JO A/98/55). Lorsque l’indemnité de licenciement est supérieure à deux mois de salaires, l’employeur doit verser au moment du licenciement une indemnisation correspondant à deux mois de salaire, le solde pouvant être versé par traite tous les deux mois. Le montant de chaque versement ne peut être inférieur à deux mois de salaires.</p>
<p>Les salariés entre 55 et 64 ans qui perdent leur emploi, par licenciement collectif ou individuel et restent chômeurs, ont le droit de recourir à l’auto-assurance à compter des deux mois suivant la rupture du contrat de travail. L’employeur qui les a licenciés est alors dans l’obligation de participer au coût de l’auto-assurance à hauteur de :</p>
<ul class="spip">
<li>50% pour les assurés âgés de 55 à 60 ans pendant trois années,</li>
<li>80% pour les assurés âgés de 60 à 64 ans pendant trois années.Afin de couvrir le restant du coût de l’auto assurance, l’OAED peut mettre en œuvre des programmes d’aides, financés par le LAEK (fonds d’aide au chômage) prévoyant :
<br>— la prise en charge du coût restant (50% ou 20%) pour la durée légale pendant laquelle l’employeur est obligé de participer à l’auto-assurance
<br>— l’inscription de cette catégorie de licenciés dans les programmes d’emploi dans le secteur public des chômeurs de longue durée âgés de 55 à 64 ans, dans les conditions de la loi 1892/90 (JO A/101/90). La loi indique que le nombre de salariés de plus de 55 ans ne peut dépasser 10% du nombre total des personnes licenciées.</li></ul>
<p><strong>Réduction du coût des heures supplémentaires</strong></p>
<p>Dans les entreprises qui appliquent l’horaire conventionnel de 40 heures maximum par semaine, le salarié peut se voir imposer par l’employeur cinq heures de travail supplémentaire par semaine (de la 41ème à la 45ème heure), lesquelles sont rémunérées avec un salaire augmenté de 20% (contre 25% auparavant). Dans les entreprises qui fonctionnent six jours par semaine, les heures supplémentaires correspondant à huit heures par semaine (de la 41ème à la 48ème heure).</p>
<p>Au-delà de 45 heures par semaine (ou 48 heures pour ceux qui travaillent six jours par semaine) et dans une limite de 120 heures par an, la rémunération est augmentée de 40% (contre 50% auparavant). Au-delà de 120 heures par an, l’augmentation est de 60% (contre 75% auparavant).</p>
<p>Le ministre du Travail, après avis du Conseil supérieur du travail, peut accorder des dérogations d’heures supplémentaires (au-delà des limites permises) dans toutes les entreprises et tous les emplois en général, ainsi que dans l’administration et les personnes morales de droit public, dans les cas d’urgence pour un travail dont l’exécution ne peut être reportée, ou dans des cas exceptionnels, pour le service des Forces armées, de l’Etat et des personnes morales de droit public. Dans ce cas, la rémunération est augmentée de 60%.</p>
<p><i>Mise à jour : novembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/emploi-stage/article/reglementation-du-travail-110378). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
