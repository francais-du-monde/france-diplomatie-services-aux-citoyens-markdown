# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/vie-pratique/article/transports#sommaire_1">Importation de véhicule</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/vie-pratique/article/transports#sommaire_2">Immatriculation</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/vie-pratique/article/transports#sommaire_3">Permis de conduire</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/vie-pratique/article/transports#sommaire_4">Code de la route</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/vie-pratique/article/transports#sommaire_5">Assurances et taxes</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/vie-pratique/article/transports#sommaire_6">Achat et location</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/vie-pratique/article/transports#sommaire_7">Entretien</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/vie-pratique/article/transports#sommaire_8">Réseau routier</a></li>
<li><a id="so_9" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/vie-pratique/article/transports#sommaire_9">Transports en commun</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Importation de véhicule</h3>
<p><strong>Les informations fournies dans cette page le sont à titre indicatif</strong>. Elles sont susceptibles de modifications en fonction de l’évolution de la législation grecque. Pour plus de précisions il convient de contacter le service des douanes de la ville où vous résidez en Grèce.</p>
<p>L’importation d’une voiture à pot catalytique n’est pas obligatoire, mais elle est préférable.</p>
<p>L’importation d’un véhicule diesel est autorisée pour les propriétaires qui résident en dehors de l’Attique et de Thessalonique.</p>
<p>Il faut distinguer TVA, Taxe différentielle ou spéciale de consommation, achat des plaques minéralogiques et taxe annuelle (vignette).</p>
<p>Les Français qui possèdent un véhicule, et qui résident en Grèce, sont tenus d’immatriculer leur véhicule en Grèce. En vertu de la législation européenne l’assurance des véhicules immatriculés en Grèce doit être souscrite auprès d’un assureur établi en Grèce.</p>
<p>Importation d’un véhicule neuf depuis la France : les familles nombreuses résidentes en Grèce (3 enfants et plus) qu’elles soient françaises ou grecques peuvent bénéficier d’une diminution sensible du montant de la taxe d’importation.</p>
<h3 class="spip"><a id="sommaire_2"></a>Immatriculation</h3>
<p>L’importation de votre véhicule en Grèce se fait depuis la France via l’Italie, puis par ferries de Bari et Brindisi jusque Patras (liaisons plusieurs fois par jour). Les plaques d’immatriculation doivent être changées avant les six mois d’anniversaire du jour de la traversée. Penser à conserver les billets de votre traversée qui serviront de justificatifs auprès des autorités grecques.</p>
<h4 class="spip">Conditions de circulation et d’immatriculation d’un véhicule "communautaire" en Grèce </h4>
<p><i>( Source : 18ème Direction des Douanes - 40 av. Amalias - Athènes)</i></p>
<h5 class="spip">A - Les touristes </h5>
<p>Ressortissants des pays communautaires peuvent circuler en Grèce avec leur véhicule immatriculé dans leur pays d’origine <strong>pour une période totale de six mois dans l’année</strong>. Si ces personnes ont l’intention de rentrer dans leur pays d’origine pour revenir ensuite en Grèce, elles peuvent demander au Bureau des douanes le plus proche de leur résidence en Grèce d’immobiliser leur véhicule en Grèce pendant six mois. De cette manière, dès leur retour en Grèce, elles pourront à nouveau circuler pour une période totale de six mois par an.</p>
<p>NB : un touriste qui choisit avant le délai de six mois de transférer son véhicule en Grèce peut faire une déclaration spécifique auprès des douanes grecques (<i>idiki dilossi</i>), puis suivre les démarches décrites au paragraphe C.</p>
<h5 class="spip">B - Les personnes travaillant dans le secteur privé</h5>
<p>Une personne <strong>souhaitant transférer son domicile en Grèce</strong>, et y amener sa voiture, est tenue d’obtenir le <i>certificat de changement de résidence</i> délivré par un consulat grec, et suivre la même procédure que celle mentionnée au paragraphe C.</p>
<p>Si ces personnes <strong>ne souhaitent pas</strong> transférer leur domicile en Grèce, il ne sont autorisés qu’à circuler six mois ; au-delà de ce délai, ils devront rapatrier leur véhicule, n’ayant pas droit à la procédure d’immobilisation temporaire (non-transfert de résidence, notamment au plan fiscal = non reconnaissance du statut de salarié), à moins que la personne ne régularise sa situation conformément au paragraphe C.</p>
<p>NB : la loi grecque précise que les personnes travaillant en Grèce pour la réalisation d’un ouvrage public d’intérêt national (cas des grands chantiers) peuvent circuler avec les plaques d’immatriculation de leur pays d’origine. Pour bénéficier de ce statut dérogatoire, elles doivent néanmoins justifier de cette situation auprès du bureau des douanes grec de leur lieu de résidence (contrat de travail notamment).</p>
<h5 class="spip">C - Les Français souhaitant s’installer en Grèce</h5>
<p>Pour les personnes souhaitant s’installer en Grèce et y venir avec un véhicule régulièrement immatriculé dans l’espace communautaire :</p>
<p><strong>Conditions préalables relatives au véhicule</strong></p>
<p>Le propriétaire doit prouver que le véhicule est en sa possession depuis six mois au moins, et qu’il l’utilise depuis la même période.</p>
<p><strong>Modalités du transfert</strong></p>
<p>Il appartient à l’intéressé de :</p>
<ul class="spip">
<li>demander un "certificat de changement de résidence (<i>pistopoihtiko metoikesias</i>), avant le départ du véhicule, à un des consulats de Grèce en France (Paris ou Marseille).</li>
<li>conserver le billet de bateau afin de pouvoir prouver la date d’entrée du véhicule sur le territoire hellénique.</li>
<li>procéder à la "déclaration d’arrivée du véhicule -Dilossi afixis ochmatos" , auprès des douanes grecques à l’ entrée en Grèce (port de Patras ou autre port). Ce document est nécessaire lors des démarches pour l’obtention des plaques minéralogiques.</li>
<li>d’effectuer les démarches administratives (immatriculation, changement de plaques minéralogiques) en Grèce <strong>dans un délai d’un mois</strong> à partir de la date d’entrée du véhicule en Grèce auprès de la direction de la circulation locale (la plus proche du lieu de domicile).</li></ul>
<p><strong>Charges et conditions</strong></p>
<p>Devra être jointe au dossier la facture d’achat, l’assurance du véhicule, une attestation du ministère des Transports (2 rue Anastasseos et Ioannis Tsigante, 160 00 Papagou , Tél. : 210 650 84 38), certifiant que le véhicule a été construit suivant les prescriptions techniques d’antipollution telles que mentionnées dans les circulaires de la Communauté européenne.</p>
<p>La taxe différentielle à acquitter (TD) est fixée au prorata de la valeur initiale (VA) et de l’âge (AG) du véhicule : plus le véhicule est ancien plus élevée est la taxe car s’applique en outre un coefficient "K".</p>
<p>Les plaques sont délivrées pour cinq ans. Si les intéressés souhaitent vendre le véhicule avant l’expiration de ce délai, ils devront en avertir les douanes en vue d’acquitter une taxe libératoire.</p>
<p>NB : importation suite au mariage ou héritage : dérogation tendant vers la gratuité.</p>
<p>Pour plus d’informations, vous pouvez consulter le site interministériel grec suivant : <a href="http://www.kep.gov.gr/" class="spip_out" rel="external">www.kep.gov.gr</a> (également disponible en français).</p>
<h4 class="spip">Véhicule immatriculé en plaques grecques </h4>
<h5 class="spip">Vignette </h5>
<p>Suite à l’immatriculation en plaques minéralogiques helléniques, le véhicule importé doit être déclaré au fisc grec du quartier de résidence. La vignette est de l’ordre de 60 à 200 Euros selon les cylindrées. La vétusté du véhicule n’est pas prise en considération.</p>
<h5 class="spip">Taxe annuelle</h5>
<p>En revanche, le propriétaire d’un véhicule doit prouver par sa déclaration d’impôts qu’il a les moyens d’acquérir et d’entretenir son véhicule en fonction de ses revenus.</p>
<p>Le ressortissant d’un État membre de l’Union européenne, propriétaire d’un premier véhicule importé et acheté depuis au moins six mois, bénéficie d’un abattement de 50% pendant cinq ans.</p>
<h5 class="spip">Vente d’un véhicule importé</h5>
<p>Le propriétaire d’un véhicule importé peut vendre sa voiture à n’importe quel moment conformément à la législation hellénique.</p>
<p>S’il est bénéficiaire de l’exemption de la TVA et de la taxe spéciale de consommation, il doit garder la voiture pendant cinq ans et un jour en sa possession après la date de dédouanement de celle-ci en Grèce. En cas de revente de la voiture dans le courant de la première année de son séjour en Grèce, il devra reverser 80% de cette exemption, dans le courant de la deuxième année 60%, dans le courant de la troisième année 40%, et dans le courant de la quatrième année 20%.</p>
<p>Dans tous les autres cas, c’est à dire importation d’une première voiture achetée moins de six mois avant le transfert de résidence, importation d’une deuxième voiture, et achat sur place d’un véhicule, le propriétaire a le droit de vendre à tout moment conformément à la législation hellénique. Cependant, si la voiture est classée vétuste, elle devra être donnée à l’État grec ou revendue à un citoyen non grec de l’Union européenne.</p>
<p><i>Source : <a href="http://www.ambafrance-gr.org/Importation-d-un-vehicule" class="spip_out" rel="external">consulat de France en Grèce</a></i></p>
<h3 class="spip"><a id="sommaire_3"></a>Permis de conduire</h3>
<p>La directive européenne 91/439 CEE du Conseil du 29/07/1991 a été transposée dans la législation française par décret 98/1103 du 8/12/1998, et dans la législation hellénique le 3/05/1999 (58930/480). Le permis de conduire français est donc valable en Grèce.</p>
<h3 class="spip"><a id="sommaire_4"></a>Code de la route</h3>
<p>En raison de la pollution atmosphérique, il existe une restriction de circulation au centre d’Athènes pour les véhicules immatriculés en Grèce (un jour sur deux durant la semaine). Ces restrictions ne s’appliquent cependant pas aux véhicules de location ainsi qu’aux véhicules étrangers présents pour une courte durée. La circulation est libre le week-end, ainsi qu’au mois d’août.</p>
<p>La conduite s’effectue à droite et la priorité est à droite. Le respect des règles du code de la route est aléatoire. Le taux d’accidents mortels est le plus élevé des pays de l’Union européenne.</p>
<p>Taux d’alcool dans le sang à partir duquel la conduite est interdite : 0,5 g/l. Taux d’alcool dans l’air expiré à partir duquel la conduite est interdite : 0,25 g/l.</p>
<p>Usage au volant d’un téléphone tenu en main est interdit ; en revanche, il est possible d’utiliser l’appareil bluetooth.</p>
<p>Port de la ceinture de sécurité : obligatoire à l’avant comme à l’arrière. Les enfants mesurant moins de 1m50 doivent être installés à l’arrière du véhicule sur siège spécial pour enfants.</p>
<p>Port du casque sur les cyclos et les motos obligatoire. Il est obligatoire de disposer d’un triangle de signalisation, d’une trousse de secours et d’un extincteur.</p>
<p>Informations pratiques : il est interdit de stationner à moins de cinq mètres d’une bouche d’incendie, de cinq mètres d’un passage piétons, de 10 mètres d’une intersection, de 12 mètres d’un arrêt de bus et de 20 mètres d’un feu de signalisation.</p>
<p>Les autoroutes sont payantes - Ethniki Odos (National Road), Attiki Odos (périphériques d’Athènes, liaison autoroutière de et vers l’aéroport) Attention : en pratique, la bande d’arrêt d’urgence de l’autoroute et de la route nationale est souvent utilisée comme une voie de circulation à part entière, ce qui est formellement interdit par le code de la route.</p>
<p>En cas d’infraction commise avec un véhicule de location, les forces de l’ordre prennent généralement contact avec le loueur afin de connaître l’identité du conducteur. Il est conseillé d’éviter de signer des contrats de location rédigés dans une langue que vous ne comprenez pas.</p>
<p>Les numéros à appeler en cas d’urgence ou d’accident sont :</p>
<ul class="spip">
<li>Le 100 : Police</li>
<li>Le 166 : Samu</li>
<li>Le 199 : Pompiers</li>
<li>Le 112 : Numéro de détresse (également francophone)</li></ul>
<h3 class="spip"><a id="sommaire_5"></a>Assurances et taxes</h3>
<p>L’assurance au tiers est obligatoire. Le montant des primes varie considérablement en fonction de la cylindrée. <a href="http://www.diplomatie.gouv.fr/fr/" class="spip_url spip_out"></a></p>
<h3 class="spip"><a id="sommaire_6"></a>Achat et location</h3>
<p>Toutes les marques de véhicules françaises et étrangères sont représentées.</p>
<p>Les taxes sur les véhicules deviennent particulièrement importantes dès que la cylindrée dépasse 1800 cc. Dans tous les cas il s’avère moins onéreux d’acheter directement un véhicule sur place que d’en importer un en vue de son immatriculation en Grèce.</p>
<p>On peut se procurer sur place un véhicule d’occasion dans un garage ou par annonces émanant de particuliers.</p>
<p>Le marché de la location est actif. De nombreux loueurs internationaux sont représentés en Grèce. Compter entre 70€ et 100€ par jour (1400cc).</p>
<p>Il convient d’être vigilant à l’égard des petites agences de location de voitures, de motos et de mobylettes non agréées, en particulier dans les îles. Ces agences ne fournissent pas toujours aux clients des véhicules révisés et offrant toutes les garanties de sécurité. On ne saurait trop recommander aux touristes de bien lire toutes les clauses du contrat d’assurance.</p>
<h3 class="spip"><a id="sommaire_7"></a>Entretien</h3>
<p>Il est possible de faire réparer et entretenir sa voiture dans de bonnes conditions à un coût inférieur à celui de la France. Les pièces détachées de modèle courant se trouvent aisément.</p>
<h3 class="spip"><a id="sommaire_8"></a>Réseau routier</h3>
<p>Le réseau autoroutier relie Athènes à Patras (Péloponèse) vers le sud, et Thessalonique vers le nord. Il est en général de bonne qualité. Le réseau autoroutier, peu dense, est en voie de modernisation.</p>
<p>En cas d’accident de la circulation mettant en cause un tiers, un constat de police est obligatoire. Attendre la police pour faire le constat, même pour un petit accrochage et même s’il faut attendre longtemps, sinon les assurances ne remboursent pas. Alors, le seul recours est d’introduire une action en justice, procédure longue, coûteuse et dont l’issue n’est pas prévisible. S’il y a des victimes, le conducteur étranger peut être gardé à vue puis présenté au tribunal qui se chargera de déterminer sa responsabilité. Dans la majorité des cas, il y a lieu de faire appel à un avocat local pour assurer sa défense. Il est donc nécessaire de vérifier les risques couverts par son contrat d’assurance (responsabilité civile, versement d’une caution de garantie et assistance juridique).</p>
<h3 class="spip"><a id="sommaire_9"></a>Transports en commun</h3>
<h4 class="spip">Réseau ferroviaire </h4>
<p>Le <a href="http://www.ose.gr/el/%CE%91%CF%81%CF%87%CE%B9%CE%BA%CE%AE%CE%A3%CE%B5%CE%BB%CE%AF%CE%B4%CE%B1.aspx" class="spip_out" rel="external">réseau ferroviaire</a> dessert les principales villes. Dans l’ensemble il est très médiocre (sauf entre Athènes et Thessalonique) et très peu utilisé.</p>
<h4 class="spip">Réseau maritime </h4>
<p><a href="http://www.paleologos.gr/" class="spip_out" rel="external">Réseau maritime</a> très important. Les conditions de sécurité des liaisons par bacs et par ferries sont conformes aux règlements internationaux. Leur fréquence est nombreuse, surtout l’été.</p>
<p>Les liaisons maritimes intérieures sont assurées par des Car-ferries, des hydroglisseurs ou des bateaux.</p>
<h4 class="spip">Transport aérien</h4>
<p>L’aéroport international d’Athènes, Eleftherios Venizelos, situé à SPATA à environ 27 km au Nord- Est de la capitale, se caractérise par de fréquents retards, l’espace aérien étant saturé en période estivale.</p>
<p>L’annulation d’un vol charter, par exemple, peut conduire à différer le retour de 24 heures, sans pour autant que les bagages soient restitués aux passagers.</p>
<p>Les liaisons aériennes sont effectuées par la compagnie nationale <a href="http://www.olympicair.com/Home.aspx?a_id=710" class="spip_out" rel="external">Olympic Air</a> et quelques compagnies privées.</p>
<h4 class="spip">Transports urbains</h4>
<p>A Athènes, les liaisons sont assurées par métro, tram, electriko, autobus, trolleybus et taxis. Le ticket pour tous les moyens de transports ( metro, tram, bus et trolleybus) est de 1,40 € et de 1,20€ pour emprunter uniquement les bus et trolleybus. Kle ticket est valable 1h30. Un trajet dans le centre d’Athènes est facturé de 5 à 10 €. Les taxis individuels prennent souvent plusieurs passagers indépendants, chacun payant sa part.</p>
<p>Dans le souci de réduire la pollution atmosphérique, les véhicules particuliers diesel sont interdits (réservés aux taxis, autocars, camionneurs, …). Les résidents en possession d’un véhicule privé aux plaques minéralogiques helléniques subissent une restriction à la circulation :</p>
<ul class="spip">
<li>dans le centre ville (rues Hamosternas, Kalirois, Frantzi, Iliou, Sp. Mercouri, Michalakopoulou ; Fidipidou, Patission, Marni, Karolou, Achilléos, Iera odos et avenues Imittou, Alexandras, Konstantinoupoléos, Piréos) : <strong>circulation alternée</strong> <strong>selon le dernier numéro de la plaque minéralogique</strong> (pair les jours pairs, ou impair les jours impairs) du lundi au jeudi de 8h00 à 20h00 et du vendredi de 8h00 à 13h00. <strong>La circulation est libre les vendredis après 13h00, samedis, dimanches et jours fériés.</strong></li>
<li>le triangle historique (rue Athinas, rue Stadiou, rue Hermou) est seul accessible en semaine pour les riverains et les livraisons.</li></ul>
<p>Pendant les jours de très grand indice de pollution, la restriction est appliquée dans toute la ville d’Athènes à tous les véhicules, exception faite des bus, trolleys et taxis.</p>
<p>Un nouvel réseau routier ATTIKI ODOS est le nouveau périphérique d’Athènes : péage 2,8 €</p>
<p>Aujourd’hui, le système de transport public d’Athènes et agrandi et amélioré. Les travaux effectués ont beaucoup amélioré les conditions de transport.</p>
<p><i>Mise à jour : novembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/vie-pratique/article/transports). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
