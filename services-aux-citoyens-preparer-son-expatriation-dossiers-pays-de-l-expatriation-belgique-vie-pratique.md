# Vie pratique

<h2 class="rub22795">Logement</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/vie-pratique/#sommaire_1">Où se loger ? </a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/vie-pratique/#sommaire_2">Conditions de location </a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Où se loger ? </h3>
<p>Il existe un marché locatif très important en Belgique et trouver un logement ne pose pas de difficulté particulière. A Bruxelles, les quartiers résidentiels se situent à Uccle, Woluwe Saint-Pierre, Ixelles et Krainem ; à la périphérie et au centre-ville à Liège et Anvers. Il faut compter entre une et deux semaines de recherche à Bruxelles et Anvers, un mois à Liège. Les appartements et villas non meublés se trouvent sans difficulté. En revanche, les logements meublés sont rares.</p>
<p>La prospection s’effectue par les petites annonces, le "bouche à oreilles", via les affiches "à louer" apposées sur les logements disponibles, sur les nombreux sites web de « couch-surfing » ou de colocation, via les réseaux sociaux mais il est également possible de recourir à une agence immobilière. En cas de location par l’intermédiaire d’une agence, la commission est toujours payée par le propriétaire.</p>
<h4 class="spip">Où chercher ?</h4>
<p>Vous pouvez consulter les <strong>quotidiens</strong> locaux, comme <a href="http://www.lesoir.be/" class="spip_out" rel="external">Le Soir</a> et <a href="http://www.lalibre.be/" class="spip_out" rel="external">La Libre Belgique</a>, qui publient des offres immobilières. Le <a href="http://www.vlan.be/fr/index.php" class="spip_out" rel="external">principal journal de petites annonces</a> publie chaque lundi un supplément payant consacré à l’immobilier.</p>
<p>Il existe par ailleurs de nombreux <strong>sites d’annonces immobilières</strong> :</p>
<ul class="spip">
<li><a href="http://www.belgimmo.be/" class="spip_out" rel="external">Belgimmo.be</a></li>
<li><a href="http://www.immoweb.be/fr/" class="spip_out" rel="external">Immoweb.be</a></li></ul>
<p>Sites d’immobilier <strong>entre particuliers</strong> :</p>
<ul class="spip">
<li><a href="http://www.immobelgique.com/" class="spip_out" rel="external">Immobelgique</a></li>
<li><a href="http://www.pap.be/" class="spip_out" rel="external">PAP.be</a></li></ul>
<p><strong>Sites de colocation :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.appartager.be/" class="spip_out" rel="external">Appartager.be</a></p>
<p>Pour une <strong>location temporaire</strong>, le <a href="http://www.bruxelles.irisnet.be/fr/citoyens/home/logement.shtml" class="spip_out" rel="external">portail de la région de Bruxelles-Capitale</a> indique une liste d’appartements meublés, d’aparthotels ou de flathotels.</p>
<p>Pour toute information sur les <strong>auberges de jeunesse</strong>, contacter :</p>
<p><a href="http://www.laj.be/" class="spip_out" rel="external">Les Auberges de jeunesse</a> <br class="manualbr">Rue de la Sablonnière 28 <br class="manualbr">1000 Bruxelles <br class="manualbr">Tél : (32 2) 219 56 76 <br class="manualbr">Fax : (32 2) 219 14 51</p>
<p><a href="http://www.vjh.be/" class="spip_out" rel="external">Vlaamse Jeugdherbergcentrale</a> <br class="manualbr">Van Stralenstraat 40 <br class="manualbr">Tél :(32 3) 232 72 18 <br class="manualbr">Fax : (32 3) 231 81 26</p>
<p>Les <strong>logements sociaux</strong> - destinés aux personnes isolées ou aux ménages ayant des revenus modestes - relèvent de la compétence des régions, les conditions diffèrent donc d’une région à l’autre. Pour toute information, vous pouvez consulter les sites suivants : <a href="http://www.cil-wic.be/" class="spip_out" rel="external">Centre d’information du logement de la région de Bruxelles-Capitale</a> et <a href="http://www.belgium.be/fr/logement/logement_social/" class="spip_out" rel="external">Portail Belgium.be</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Conditions de location </h3>
<p><strong>Contrat de bail </strong></p>
<p>La durée du bail est de trois ans minimum (loi de février 1991). Il est conseillé de souscrire un bail de neuf ans, contrat plus protecteur pour le locataire.</p>
<p>Le versement d’une caution (trois mois de loyer) déposée sur un compte bancaire bloqué, rémunérateur d’intérêts, est demandé. En outre, ces contrats prévoient un dédit proportionnel en cas de départ avant le terme du contrat (<strong>trois mois de loyer</strong> en cas de départ dans la première année, <strong>deux mois de loyer</strong> en cas de départ au cours de la deuxième année et <strong>un mois de loyer</strong> pour un départ au cours de la troisième année de location).</p>
<p>Le contrat doit comporter au moins les mentions suivantes :</p>
<ul class="spip">
<li>les nom et adresse du bailleur ;</li>
<li>le nom du locataire ;</li>
<li>la date à laquelle la location prend cours ;</li>
<li>le montant du loyer (le loyer et les charges) ;</li>
<li>la situation et la description de l’immeuble</li></ul>
<p>Depuis le 15 juin 2007, tout contrat de bail doit obligatoirement être conclu par écrit, chaque partie devant recevoir un exemplaire de cet accord.</p>
<p>Les baux comportent une clause d’indexation annuelle.</p>
<p>Pour en savoir plus : <a href="http://www.belgium.be/fr/logement/location/bail/" class="spip_out" rel="external">Belgium.be</a>.</p>
<h4 class="spip">Enregistrement du contrat de bail</h4>
<p>Depuis le 1er janvier 2007, le bailleur doit faire enregistrer le contrat de bail écrit. Il doit déposer le contrat au bureau d’enregistrement du ministère des Finances du lieu où est situé le logement. Par cet enregistrement, le contrat de bail acquiert une date certaine et devient contraignant pour les tiers. A partir de cette date certaine, le locataire est protégé par la loi contre l’expulsion par le nouveau propriétaire, en cas de vente du bien loué.</p>
<p>Le bailleur doit faire enregistrer le contrat de bail affecté exclusivement à l’habitation dans les deux mois à compter de la signature. L’enregistrement est gratuit pour ce type de bail s’il est effectué dans les délais.</p>
<p>Un <strong>état des lieux</strong> doit impérativement être dressé à l’entrée comme au départ, des contestations survenant généralement au départ du locataire. La solution la plus simple et la moins onéreuse consiste en un état des lieux établi à l’amiable, c’est-à-dire conjointement par le locataire et le propriétaire. Les deux parties (locataire et propriétaire) peuvent aussi désigner un expert dont elles se partageront les honoraires. Enfin, chacun peut aussi désigner son propre expert dont il réglera seul les honoraires. Sans état des lieux du propriétaire, les dégâts locatifs lui incombent.</p>
<p>Le loyer est payable d’avance (un mois). Les charges ne sont généralement pas incluses dans le montant du loyer. Une assurance incendie est obligatoire.</p>
<h4 class="spip">Exemples de prix de location</h4>
<p><strong>Bruxelles</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Type de logement</td>
<td>Loyer mensuel en quartier résidentiel (<i>au sud et à l’est du centre ville : Uccle, Ixelles, Woluwé)</i></td></tr>
<tr class="row_even even">
<td>studio</td>
<td>750 à 1150 €</td></tr>
<tr class="row_odd odd">
<td>3 pièces</td>
<td>1000 à 1300 €</td></tr>
<tr class="row_even even">
<td>5 pièces</td>
<td>1250 à 2000 €</td></tr>
<tr class="row_odd odd">
<td>villa</td>
<td>1800 à 3500 €</td></tr>
</tbody>
</table>
<p><strong>Anvers</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Type de logement</td>
<td>loyer mensuel en quartier résidentiel  (<i>au sud d’Anvers : Berchem, Wilrijk, et au Nord ouest : Brasschaat, Kapellen)</i></td>
<td>loyer en banlieue</td></tr>
<tr class="row_even even">
<td>studio</td>
<td>400 à 500 €</td>
<td>350 €</td></tr>
<tr class="row_odd odd">
<td>3 pièces</td>
<td>1250 €</td>
<td>500 €</td></tr>
<tr class="row_even even">
<td>5 pièces</td>
<td>1250 €</td>
<td>600 €</td></tr>
<tr class="row_odd odd">
<td>villa</td>
<td>1950 €</td>
<td>1000 €</td></tr>
</tbody>
</table>
<p><strong>Liège</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Type de logement</td>
<td>loyer mensuel en quartier résidentiel  <i>(centre ville / Cointe, Embourg)</i></td>
<td>loyer en banlieue</td></tr>
<tr class="row_even even">
<td>studio</td>
<td>600 €</td>
<td>300 €</td></tr>
<tr class="row_odd odd">
<td>3 pièces</td>
<td>1000 €</td>
<td>800 €</td></tr>
<tr class="row_even even">
<td>5 pièces</td>
<td>1500 €</td>
<td>1000 €</td></tr>
<tr class="row_odd odd">
<td>villa</td>
<td>2000 €</td>
<td>1500 €</td></tr>
</tbody>
</table>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li>De nombreuses <a href="http://www.belgium.be/fr/logement/index.jsp" class="spip_out" rel="external">informations sur le logement</a> (location, vente, etc.)</li>
<li><a href="http://www.bruxelles.irisnet.be/vivre-a-bruxelles/logement" class="spip_out" rel="external">Portail de la région Bruxelles Capitale</a></li>
<li><a href="http://www.cil-wic.be/" class="spip_out" rel="external">Centre d’information sur le logement</a></li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-belgique-vie-pratique-article-pour-en-savoir-plus-109839.md" title="Pour en savoir plus">Pour en savoir plus</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-belgique-vie-pratique-article-loisirs-et-culture.md" title="Loisirs et culture">Loisirs et culture</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-belgique-vie-pratique-article-communications-109811.md" title="Communications">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-belgique-vie-pratique-article-transports.md" title="Transports">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-belgique-vie-pratique-article-cout-de-la-vie-109812.md" title="Coût de la vie">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-belgique-vie-pratique-article-scolarisation.md" title="Scolarisation">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-belgique-vie-pratique-article-sante.md" title="Santé">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-belgique-vie-pratique-article-logement.md" title="Logement">Logement</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/vie-pratique/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
