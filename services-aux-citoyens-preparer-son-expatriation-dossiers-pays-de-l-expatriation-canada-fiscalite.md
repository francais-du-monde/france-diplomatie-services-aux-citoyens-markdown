# Fiscalité

<h2 class="rub23182">Fiscalité du pays</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/fiscalite/#sommaire_1">Inscription auprès du fisc</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/fiscalite/#sommaire_2">Année fiscale</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/fiscalite/#sommaire_3">Impôts directs et indirects</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/fiscalite/#sommaire_4">Quitus fiscal</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/fiscalite/#sommaire_5">Solde du compte en fin de séjour</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/fiscalite/#sommaire_6">Coordonnées des centres d’information fiscale</a></li></ul>
<p>L’<a href="http://www.cra-arc.gc.ca/" class="spip_out" rel="external">Agence du Revenu du Canada</a> met en ligne sur son site Internet une brochure destinée aux nouveaux arrivants dans ce pays qui vous aidera à comprendre le système fiscal canadien et à remplir votre déclaration d’impôt.</p>
<h3 class="spip"><a id="sommaire_1"></a>Inscription auprès du fisc</h3>
<p>Vous devez tout d’abord déterminer si vous êtes fiscalement domicilié au Canada. C’est le cas si vous avez établi dans ce pays des liens de résidence importants, généralement appréciés à la date de votre arrivée au Canada. On considère comme liens de résidence le fait d’avoir un domicile au Canada, d’y vivre avec un époux, un conjoint de fait ou des personnes à charge ou encore d’y posséder des biens personnels ou des liens sociaux.</p>
<p>Si vous avez un doute sur votre statut de résident fiscal, vous pouvez consulter le site Internet de l’<a href="http://www.cra-arc.gc.ca/" class="spip_out" rel="external">Agence du Revenu du Canada</a>.</p>
<p><strong>Numéro d’assurance sociale (NAS)</strong></p>
<p>Pour déclarer vos revenus, vous devez tout d’abord avoir un numéro d’assurance sociale (NAS). Si ce n’est pas encore le cas, vous devez en faire la demande auprès du bureau de <a href="http://www.servicecanada.gc.ca/" class="spip_out" rel="external">Service Canada</a> de votre région.</p>
<p><strong>Où déclarer ses impôts ?</strong></p>
<p>Les taux et les crédits d’impôt variant d’une province ou d’un territoire à l’autre, vous devez tout d’abord vous procurer la <i>trousse d’impôt</i> de la province ou du territoire où vous <strong>résidiez au 31 décembre de l’année précédant la déclaration.</strong> La trousse comprend le guide, la déclaration, les annexes et les renseignements et formulaires provinciaux ou territoriaux (sauf pour le Québec). Elle est généralement disponible de début février à début mai.</p>
<p>Vous pouvez vous procurer cette <i>trousse</i> :</p>
<ul class="spip">
<li>sur le site Internet de l’<a href="http://www.cra-arc.gc.ca/" class="spip_out" rel="external">ARC</a> ; choisissez la province ou le territoire.</li>
<li>par téléphone en appelant le 1 800 959 3376.</li>
<li>en vous présentant près de chez vous à un <a href="http://www.postescanada.ca/cpo/mc/default.jsf?LOCALE=fr" class="spip_out" rel="external">bureau de poste</a> ou de <a href="http://www.servicecanada.gc.ca/" class="spip_out" rel="external">Service Canada</a>.</li></ul>
<p>Vous pouvez ensuite transmettre votre déclaration de revenus :</p>
<ul class="spip">
<li>par Internet en utilisant l’application <a href="http://www.impotnet.gc.ca/" class="spip_out" rel="external">Impôtnet</a>. Ce service ne s’adresse qu’aux personnes ayant déjà effectué une déclaration de revenus auprès de l’ARC.</li>
<li>par téléphone en appelant le service Impôtel au 1 800 959 1110. Ce service est réservé aux contribuables qui produisent une déclaration avec les renseignements fiscaux les plus communs.</li>
<li>par la poste ou en personne en envoyant ou en déposant votre déclaration de revenus au centre fiscal de votre région. Son adresse est indiquée dans la <i>trousse d’impôt</i>.</li></ul>
<p>Le traitement d’une déclaration de revenus transmise par Internet peut prendre d’une à quatre semaines et celui d’une déclaration transmise par la poste de quatre à six semaines.</p>
<p><strong>Délai d’envoi de la déclaration</strong></p>
<p>La déclaration de revenus doit être envoyée <strong>au plus tard le 30 avril </strong>qui suit l’année visée par la déclaration. En cas de retard, des pénalités seront imposées si vous devez des impôts.</p>
<p>Pour les travailleurs indépendants, ce délai est fixé <strong>au plus tard au 15 juin</strong> qui suit l’année visée par la déclaration.</p>
<p><strong>Pourquoi remplir une déclaration de revenus ?</strong></p>
<p>Vous devez effectuer chaque année une déclaration de revenus si vous avez des impôts à payer ou si vous souhaitez obtenir le remboursement d’impôts. La déclaration est obligatoire même si vous n’avez vécu au Canada qu’une partie de l’année visée. Elle permet de calculer vos impôts fédéraux et provinciaux ou territoriaux. La déclaration reprend les revenus de l’année visée, ainsi que les déductions et les crédits d’impôt auxquels vous pouvez prétendre. Selon le cas, vous aurez droit à un remboursement total ou partiel de l’impôt sur les revenus de l’année prise en compte ou bien vous devrez régler un solde au Trésor canadien.</p>
<p>Même si vous n’avez aucun revenu à déclarer ou aucun impôt à payer, il est fortement recommandé d’effectuer une déclaration.</p>
<p>En effet, celle-ci permet aux services canadiens de vérifier si vous avez droit, au niveau fédéral, à un crédit d’impôt pour la taxe sur les produits et services / taxe de vente harmonisée (TPS/TVH) et à la prestation fiscale canadienne pour enfants (PFCE) et, au niveau provincial ou territorial, de vérifier si vous avez droit aux prestations et aux crédits d’impôt accordés dans le cadre de certains programmes.</p>
<p><strong>Quels revenus déclarer ?</strong></p>
<p>Si, au cours de l’année visée, vous n’avez pas toujours été fiscalement domicilié au Canada, vous devez déclarer les revenus suivants :</p>
<p><strong>pour la partie de l’année où vous n’étiez pas résident au Canada</strong>, vous ne devez déclarer que les revenus tirés d’un emploi au Canada ou d’une entreprise exploitée au Canada, les gains en capital imposables réalisés à la suite de la cession de biens canadiens imposables et la partie imposable des bourses d’études, de perfectionnement, d’entretien et des subventions de recherche que vous avez reçues de sources canadiennes. Mais vous ne devez pas inclure les gains ou les pertes provenant de la cession de biens canadiens imposables, ni une perte provenant d’une entreprise exploitée au Canada si, selon une convention fiscale, ces gains ou le revenu de cette entreprise sont exempts d’impôt au Canada.</p>
<p><strong>Pour la partie de l’année où vous étiez résident du Canada</strong>, vous devez déclarer tous les revenus perçus qu’ils soient de provenance canadienne ou étrangère. Certains revenus de source française ne seront peut-être pas imposables en application de la convention fiscale passée entre la France et le</p>
<p>Canada. Pour en savoir plus, reportez-vous à l’article sur la convention fiscale dans ce chapitre.</p>
<h3 class="spip"><a id="sommaire_2"></a>Année fiscale</h3>
<p>L’année fiscale débute au 1er avril et se termine le 31 mars.</p>
<p><strong>Au Québec</strong> : Pour un particulier, l’année d’imposition correspond généralement à l’année civile, soit du 1er janvier au 31 décembre. Pour une entreprise, l’année d’imposition correspond généralement à l’exercice financier. L’année d’imposition couvre la période du 1er janvier au 31 décembre.</p>
<p>Un particulier doit produire sa déclaration de revenus au plus tard le 30 avril suivant la fin de l’année d’imposition (ou avant le 15 juin si ce même particulier exploite une entreprise individuelle) dans la mesure où il a un revenu imposable. Les sociétés qui résident au Canada sont tenues de produire une déclaration de revenus au plus tard six mois après la fin de leur année d’imposition.</p>
<p>Des pénalités peuvent être infligées en cas de production tardive de la déclaration fiscale ou d’omission d’un ou de plusieurs revenus. Au Canada, la fraude fiscale, y compris le fait de n’avoir pas produit de déclaration fiscale ou de faire de fausses déclarations, est considérée comme un crime. Elle peut être sanctionnée par une amende élevée et une peine d’emprisonnement. Pour prévenir la fraude fiscale, le Canada a mis en place un programme de recoupement des fichiers en collaboration avec les services d’immigration.</p>
<h3 class="spip"><a id="sommaire_3"></a>Impôts directs et indirects</h3>
<p><strong>Déductions</strong></p>
<p>Peuvent être déduits :</p>
<ul class="spip">
<li>les cotisations versées à des régimes enregistrés d’épargne-retraite (REER), sauf si c’est la première année que vous effectuez une déclaration de revenus ;</li>
<li>dans certains cas, notamment pour les étudiants boursiers, et sous certaines conditions, les frais de déménagement pour venir au Canada ;</li>
<li>la pension alimentaire versée pour vos enfants ou au profit de l’ex-époux ou de l’ancien conjoint de fait, même si ce dernier ne réside pas au Canada ;</li>
<li>le montant, non imposable en application d’une convention fiscale, des revenus perçus après votre arrivée au Canada.</li></ul>
<p><strong>Crédits d’impôt non remboursables fédéraux et provinciaux ou territoriaux</strong></p>
<p>Les crédits d’impôt non remboursables permettent de réduire l’impôt à payer. Ces crédits sont déduits de l’impôt dû. Toutefois, si le total de ces crédits dépasse votre impôt à payer, la différence ne vous sera pas remboursée.</p>
<p>Les conditions à remplir pour avoir droit aux crédits d’impôt non remboursables provinciaux ou territoriaux sont les mêmes que pour les crédits d’impôt non remboursables fédéraux correspondants.</p>
<p>Toutefois, dans la plupart des cas, les montants et les calculs à effectuer diffèrent.</p>
<p>Si vous êtes arrivé au Canada au cours de l’année visée, le montant total des crédits d’impôts que vous pouvez demander sera peut-être limité.</p>
<p>Vous devez indiquer dans votre déclaration de revenus, les crédits d’impôts non remboursables pour la partie de l’année où vous n’étiez pas résident au Canada et pour la partie de l’année où vous étiez résident dans ce pays. Pour la 1ère période de l’année, vous ne pouvez demander des crédits d’impôt que sur certains revenus de source canadienne.</p>
<p><strong>Exemptions</strong></p>
<p>Certains revenus, tels que les gains de loterie, les allocations d’anciens combattants et la prestation fiscale canadienne pour enfants (PFCE), ne sont pas imposables.</p>
<p><strong>Paiement et remboursement de l’impôt</strong></p>
<p>L’impôt sur le revenu est perçu annuellement auprès des particuliers et des entreprises par l’Agence du Revenu du Canada (ARC) pour le compte du gouvernement fédéral, des provinces ou des territoires. Le montant de l’impôt à payer est fonction du revenu imposable (c’est à dire les revenus perçus moins les déductions auxquelles vous avez droit) reçu au cours de l’année d’imposition.</p>
<p>L’impôt sur le revenu est perçu de différentes façons :</p>
<ul class="spip">
<li><strong>par retenue à la source</strong> pour les salariés. L’employeur déduit généralement du salaire l’impôt sur le revenu, ainsi que les cotisations au Régime de pensions du Canada et à l’assurance-emploi (AE). Ces sommes sont transmises par l’employeur directement à l’ARC.</li>
<li><strong>par acomptes provisionnels</strong> pour les personnes qui perçoivent un revenu sur lequel l’impôt n’est pas retenu ou est retenu de façon insuffisante. Il peut s’agir de personnes travaillant à leur compte ou qui touchent un revenu de location. Les acomptes provisionnels sont versés tous les trimestres au cours de l’année civile pour laquelle vous gagnez un revenu imposable.</li></ul>
<p>Vous devez également acquitter un impôt dans la province ou le territoire de votre résidence.</p>
<p><strong>Attention</strong> : tout solde dû doit être payé <strong>au plus tard le 30 avril de l’année</strong> qui suit l’année d’imposition et ce, quelle que soit la date limite d’envoi de la déclaration de revenus.</p>
<p>Après le traitement de votre déclaration de revenu, votre centre fiscal vous adressera un avis de cotisation. Ce document indique les changements ou les corrections apportés à votre déclaration et, selon le cas, le montant du remboursement en votre faveur effectué par l’ARC ou du solde dû à l’ARC.</p>
<p>Vous pouvez avoir droit à un remboursement dans l’un des cas suivants :</p>
<ul class="spip">
<li>vous avez payé trop d’impôt retenu à la source ;</li>
<li>vous avez payé plus d’acomptes provisionnels que nécessaire ;</li>
<li>vous avez réclamé plus de crédits d’impôt que le total d’impôt à payer.</li></ul>
<p><strong>Taxe sur les produits et services (TPS) ou <i>goods and services tax</i> (GST)</strong></p>
<p>La TPS est une taxe fédérale perçue par l’Agence du Revenu du Canada (ARC) sur la plupart des produits et services vendus ou fournis au Canada. Elle s’élevait au 1er janvier 2013 à 5 %.</p>
<p>Les familles disposant de faibles revenus peuvent compenser, en totalité ou en partie, le paiement de la TPS en demandant un crédit d’impôt pour cette taxe. Ce crédit d’impôt est payé tous les trimestres et n’est pas imposable.</p>
<p><strong>Taxe de vente provinciale (TVP) ou <i>provincial sales tax </i>(PST)</strong></p>
<p>Dans certaines provinces, cette taxe est désignée sous le nom de taxe de vente au détail (<i>retail sales tax</i>).</p>
<p>Les biens assujettis à cette taxe et son taux varient d’une province ou d’un territoire à l’autre. Son montant est calculé sur la somme du prix hors taxe et de la TPS. L’Alberta, le Nunavut, les Territoires du Nord-Ouest et le Yukon n’ont pas de taxe de vente provinciale.</p>
<table class="spip">
<thead><tr class="row_first"><th id="idaea2_c0">Province / Territoire</th><th id="idaea2_c1">Taux de la TVP</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="idaea2_c0">Colombie britannique</td>
<td headers="idaea2_c1">7 % (biens et services)
<p>10 % (boissons alcoolisées)</p>
<p>7 à 10 % (véhicules)</p>
</td></tr>
<tr class="row_even even">
<td headers="idaea2_c0">Manitoba</td>
<td headers="idaea2_c1">7 %</td></tr>
<tr class="row_odd odd">
<td headers="idaea2_c0">Ontario</td>
<td headers="idaea2_c1">8 %</td></tr>
<tr class="row_even even">
<td headers="idaea2_c0">Ile du Prince Edouard</td>
<td headers="idaea2_c1">10 %</td></tr>
<tr class="row_odd odd">
<td headers="idaea2_c0">Québec</td>
<td headers="idaea2_c1">7,5 %</td></tr>
<tr class="row_even even">
<td headers="idaea2_c0">Saskatchewan</td>
<td headers="idaea2_c1">5 %</td></tr>
</tbody>
</table>
<p><strong>Taxe de vente harmonisée (TVH) ou harmonized sales tax (HST)</strong></p>
<p>Les provinces du Nouveau Brunswick, de Terre-Neuve et Labrador et de la Nouvelle Écosse ont fusionné la taxe de vente provinciale (8 %) avec la TPS pour créer la taxe de vente harmonisée (TVH).</p>
<p>Au 1er janvier 2008, le taux de la TVH était de 13 %.</p>
<p>La TVH s’applique sur les mêmes produits et services que ceux qui sont assujettis à la TPS.</p>
<p><strong>Taxe d’accise</strong></p>
<p>Des taxes d’accise sur un nombre limité de produits ont été maintenues en dépit de l’entrée en vigueur de la TPS. A titre d’exemple, une taxe d’accise est perçue sur les produits suivants :</p>
<ul class="spip">
<li>produits pétroliers (essence au plomb : 11 cents le litre ; essence sans plomb : 10 cents le litre ; diesel : 4 cents le litre) ;</li>
<li>véhicules énergivores dont la consommation en carburant est d’au moins 13 litres aux 100 kilomètres : le montant de la taxe d’accise varie entre 1 000 CAD et 4 000 CAD ;</li>
<li>les climatiseurs d’automobiles : 100 CAD ;</li>
<li>les primes d’assurance : 10 % des primes nettes de l’année civile.</li></ul>
<p><strong>Impôt fédéral</strong></p>
<p>Les résidents canadiens sont assujettis à l’impôt fédéral pour les revenus de source mondiale. Cet impôt est calculé à des taux progressifs. Pour connaître les taux fixés pour 2014, vous pouvez consulter le site de l’<a href="http://www.cra-arc.gc.ca/tx/ndvdls/fq/txrts-fra.html" class="spip_out" rel="external">Agence du revenu du Canada</a>.</p>
<p><strong>Impôt provincial ou territorial</strong></p>
<p>A l’impôt fédéral vient s’ajouter l’impôt provincial ou territorial. A l’exception de la province du Québec qui a son propre régime fiscal, l’impôt provincial se calcule, de façon similaire à l’impôt fédéral, par tranches de revenu imposable. Des surtaxes et réductions (crédits d’impôt) sont éventuellement appliquées. Le taux d’imposition cumulé fédéral et provincial ou territorial varie fortement d’une région à l’autre.</p>
<h3 class="spip"><a id="sommaire_4"></a>Quitus fiscal</h3>
<p>Il n’est pas exigé de quitus fiscal en quittant le pays.</p>
<p>Au Québec : Le contribuable peut obtenir une attestation fiscale (quitus). Ce document est indispensable lors de la présentation de justificatifs auprès des autorités fiscales du pays dont il devient le résident</p>
<h3 class="spip"><a id="sommaire_5"></a>Solde du compte en fin de séjour</h3>
<p>Lors d’un retour définitif en France, il est essentiel de conserver précieusement tous vos documents fiscaux (déclarations, courriers, etc.) en relation avec votre séjour professionnel au Canada ou au Québec, y compris votre numéro d’assurance sociale. Celui-ci est en effet indispensable pour tout contact ultérieur avec l’administration fiscale canadienne.</p>
<p><i>Source : <a href="http://www.cra-arc.gc.ca/" class="spip_out" rel="external">Agence du revenu du Canada</a></i></p>
<p>Vous trouverez toute information utile sur les formalités à effectuer à l’occasion de votre départ du Canada sur le site Internet de l’<a href="http://www.cra-arc.gc.ca/" class="spip_out" rel="external">Agence du revenu du Canada (ARC)</a>.</p>
<p>Vous n’êtes plus considéré comme fiscalement domicilié au Canada lorsque vous rompez les liens de résidence avec ce pays. Vous êtes généralement considéré comme non-résident à la <strong>dernière</strong> des dates suivantes :</p>
<ul class="spip">
<li>la date à laquelle vous quittez le Canada ;</li>
<li>la date à laquelle votre époux ou votre conjoint de fait ou les personnes à votre charge quittent le Canada ;</li>
<li>la date à laquelle vous devenez résident du pays où vous immigrez.</li></ul>
<p><strong>Vous devez informer l’ARC de la date de votre départ du Canada</strong>. En effet, certains crédits d’impôt et certaines prestations sont liées à votre statut de résidence.</p>
<p>Un expatrié du secteur privé peut solder son compte en fin de séjour.</p>
<p>Si cette formalité n’a pas été effectuée, vous devrez l’année suivant celle de votre départ du Canada déclarer vos revenus de sources canadienne et étrangère.</p>
<p>Si vous avez vécu au Canada pour une partie seulement de l’année d’imposition, vous devrez remplir une déclaration de revenus canadienne si vous êtes dans l’une des situations suivantes :</p>
<ul class="spip">
<li>vous devez de l’impôt ;</li>
<li>vous voulez recevoir un remboursement parce que vous avez payé trop d’impôt pendant l’année d’imposition.</li></ul>
<p><strong>Le cas du Québec : </strong></p>
<p>L’impôt fédéral sur le revenu applicable aux personnes physiques résidentes au Canada et au Québec</p>
<p>Le système fiscal canadien comprend essentiellement l’impôt sur le revenu des particuliers et des entreprises, ainsi que les taxes à la consommation servant au financement des services publics. En matière d’impôt sur le revenu, la principale loi fédérale est la Loi de l’impôt sur le revenu. En matière de taxes à la consommation, la principale loi est la Loi sur la taxe d’accise. La perception des impôts et l’administration des lois fiscales fédérales est assurée par l’<a href="http://www.cra-arc.gc.ca/" class="spip_out" rel="external">Agence du revenu du Canada</a>.</p>
<p>Le "British North America Act" de 1867 a mis en place le régime d’imposition séparée entre l’Etat fédéral et les provinces. Les provinces et territoires possèdent par ailleurs leur propre système fiscal. Les provinces peuvent prélever un impôt supplémentaire sur les personnes physiques et les sociétés. En matière d’impôt sur le revenu, les lois fiscales provinciales renvoient pour l’essentiel à la loi fédérale et la perception des impôts est confiée à l’Agence du revenu du Canada. Le Québec est la seule province à s’être dotée d’une loi de l’impôt sur le revenu (la Loi sur les impôts) et perçoit également lui-même ses impôts et taxes. Les provinces de l’Ontario et de l’Alberta perçoivent leur propre impôt sur le revenu des sociétés.</p>
<p><strong>Les personnes résidentes</strong></p>
<p>L’assujettissement à l’impôt sur le revenu repose sur la notion de résidence. Toute personne, quelle que soit sa nationalité et sous réserve de l’application d’une convention fiscale, est considérée comme ayant sa résidence fiscale au Canada lorsque sa résidence principale ou son domicile permanent est situé au Canada. La durée du séjour sur le territoire canadien est déterminante pour définir la notion de résidence. Ainsi toute personne qui réside au Canada plus de 183 jours au cours d’une même année est considérée comme fiscalement résidente au Canada. Cette règle s’applique également à la province de Québec. Le résident canadien est imposé sur son revenu global, sous réserve de l’application d’une convention fiscale, tandis que la personne non résidente ne sera imposée que sur les revenus perçus au Canada. Cette obligation fiscale est illimitée. La loi distingue les revenus selon leur nature ou leur source et chaque revenu répond à ses propres règles de calcul.</p>
<p>Principales sources du revenu global (en anglais <i>income</i>) :</p>
<ul class="spip">
<li>les revenus d’affaires ;</li>
<li>les revenus immobiliers ;</li>
<li>les revenus de services ;</li>
<li>les revenus salariaux y compris les avantages liés à l’emploi.</li></ul>
<p>Tout contribuable peut bénéficier d’un certain nombre de crédits d’impôt. Les principaux crédits sont l’exemption personnelle de base accordée à tous les particuliers, pour les personnes à leur charge, pour les personnes handicapées et pour les impôts payés à l’étranger.</p>
<p><strong>Les personnes non-résidentes</strong></p>
<p>D’une façon générale, les personnes non résidentes qui perçoivent des revenus issus de biens situés au Canada doivent s’acquitter d’un impôt de 25 % applicable sur le montant net des paiements qu’ils perçoivent. Le payeur canadien doit, en principe, effectuer une retenue à la source sur les paiements faits à des non-résidents.</p>
<p>Ce taux peut être réduit en application d’une convention fiscale internationale. Dans le cas de paiement de dividendes, il est maintenant prévu dans certains cas qu’aucun impôt ne sera retenu, ni ne sera payable.</p>
<p>Par ailleurs, une personne non-résidente doit s’acquitter de l’impôt au Canada dans les trois cas suivants :</p>
<ul class="spip">
<li>salaire perçu au Canada ;</li>
<li>disposition de certains bien dits « biens canadiens imposables » ;</li>
<li>exploitation d’une entreprise au moyen d’un établissement stable.</li></ul>
<h3 class="spip"><a id="sommaire_6"></a>Coordonnées des centres d’information fiscale</h3>
<ul class="spip">
<li><strong>L’Agence du revenu du Canada</strong> (ARC) est chargée des questions relatives à l’impôt :<br class="manualbr"><strong>Renseignements téléphoniques pour les particuliers </strong> : 1 800 959 7383 (déclaration de revenus, acomptes provisionnels et REER)</li>
<li><strong>Bureau international des services fiscaux (questions internationales et non-résidents)</strong><br class="manualbr">2204 chemin Walkley <br class="manualbr">Ottawa (Ontario) K1A 1A8<br class="manualbr">Téléphone : 1 800 267 5177 (appels du Canada et des États-Unis)<br class="manualbr">Téléphone : 1 613 954 1368 (appels provenant de l’extérieur du Canada et des États-Unis)<br class="manualbr">Télécopie : 1 613 941 2505</li></ul>
<p>Vous trouverez <strong>les coordonnées des centres et services fiscaux de chaque province ou territoire </strong>à la rubrique " contactez-nous  bureaux des services et centres fiscaux " du site Internet de l’<a href="http://www.cra-arc.gc.ca/" class="spip_out" rel="external">ARC</a>.</p>
<p>La rubrique " international et non-résidents  particuliers " renseigne les particuliers en fonction de leur situation respective (nouveaux arrivants, étudiants, etc.).</p>
<p><strong>Sites Internet des ministères des Finances des provinces et des territoires :</strong></p>
<ul class="spip">
<li><a href="http://www.finance.gov.ab.ca/" class="spip_out" rel="external">Alberta</a> (<i>ministry of Finance and Enterprise</i>).</li>
<li><a href="http://www.gov.pe.ca/pt/" class="spip_out" rel="external">Ile du Prince Edouard</a> (<i>department of the Provincial Treasury</i>).</li>
<li><a href="http://www.gov.mb.ca/finance/" class="spip_out" rel="external">Manitoba</a> (<i>ministry of Finance</i>).</li>
<li><a href="http://www.gnb.ca/0024/index-f.asp" class="spip_out" rel="external">Nouveau Brunswick</a> (ministère des Finances).</li>
<li><a href="http://www.gov.ns.ca/finance/" class="spip_out" rel="external">Nouvelle Ecosse</a> (<i>ministry of Finance</i>).</li>
<li><a href="http://www.gov.nu.ca/finance/" class="spip_out" rel="external">Nunavut</a> (ministère des Finances).</li>
<li><a href="http://www.fin.gov.on.ca/" class="spip_out" rel="external">Ontario</a>
 (ministère des Finances).</li>
<li><a href="http://www.revenu.gouv.qc.ca/" class="spip_out" rel="external">Québec</a> (ministère du Revenu).</li>
<li><a href="http://www.finance.gov.sk.ca/" class="spip_out" rel="external">Saskatchewan</a> (<i>ministry of Finance</i>).</li>
<li><a href="http://www.fin.gov.nl.ca/fin/" class="spip_out" rel="external">Terre-Neuve et Labrador</a> (<i>ministry of Finance</i>).</li>
<li><a href="http://www.fin.gov.nt.ca/" class="spip_out" rel="external">Territoires du Nord-Ouest</a> (<i>ministry of Finance</i>).</li>
<li><a href="http://www.finance.gov.yk.ca/" class="spip_out" rel="external">Yukon</a> (<i>ministry of Finance</i>).</li></ul>
<p><i>Mise à jour : avril 2014</i></p>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/fiscalite/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
