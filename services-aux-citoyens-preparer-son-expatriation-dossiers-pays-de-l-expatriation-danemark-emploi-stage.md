# Emploi, stage

<h2 class="rub22987">Marché du travail</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/emploi-stage/#sommaire_1">Les possibilités d’emplois dans les différents secteurs</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/emploi-stage/#sommaire_2">Les secteurs porteurs</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/emploi-stage/#sommaire_3">Rémunération</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/emploi-stage/#sommaire_4">Quelques fourchettes de salaires </a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Les possibilités d’emplois dans les différents secteurs</h3>
<p>Le modèle danois du marché du travail est appelé <strong>la flexicurité</strong>. Ce néologisme a été composé par la contraction des mots <strong>flexibilité</strong> et <strong>sécurité</strong>. La flexicurité est le fait d’allier une flexibilité à l’embauche et au licenciement pour les employeurs à un filet de protection sociale généreux et étendu pour les employés. D’une part l’employeur est libre d’adapter son besoin de main d’œuvre aux exigences du marché grâce à une procédure de licenciement simplifiée et courte et d’autre part, l’employé est assuré d’un revenu minimum en cas de chômage.</p>
<p>Le marché du travail danois est l’un des plus sains et équilibrés de toute l’Europe, cet état de fait est notamment dû à une économie des plus stables et à une nouvelle politique de l’emploi du gouvernement. Ce plan d’action " plus d’actifs " (présenté en mai 2002) comporte deux objectifs principaux : favoriser au maximum la réinsertion des chômeurs et faciliter l’accès à l’emploi pour le maximum de personnes.</p>
<p>Après avoir régulièrement baissé, notamment durant les années 2000, le chômage a fortement augmenté depuis le début de la crise économique et est remonté à 6% de la population active en 2009 pour atteindre 7,2% en 2013, ce qui reste de 3,7 points de pourcentage inférieur à la moyenne européenne.</p>
<p>Le Danemark continuera à manquer de main-d’œuvre dans certains domaines. Il manquera entre autres de spécialistes bien formés dans le secteur privé, en particulier dans les secteurs informatique, industriel et biomédical. À court terme, le secteur public ne sera confronté, de manière générale, qu’à une pénurie de certains travailleurs hautement qualifiés, notamment des médecins. Mais dans quelques années, la génération du baby-boom devrait quitter le marché du travail, ce qui entraînera une pénurie de main-d’œuvre générale dans un nombre plus important de groupes de travailleurs, notamment dans le secteur des soins.</p>
<p>Le Danemark compte 5,5 millions d’habitants et la population active est d’environ 2,65 millions de personnes. Le Danemark est une société tournée vers les services et la connaissance. Environ 75% de la main-d’œuvre est employée dans les services publics et privés, le commerce, les transports et la communication. L’agriculture représente seulement 3% de la main-d’œuvre. Le Danemark possède un système de protection sociale très développé, financé par l’impôt. Ainsi, le secteur public représente environ un tiers des emplois.</p>
<p>Par ailleurs, le Danemark se caractérise par un nombre élevé de petites et moyennes entreprises et, au cours de ces dernières années, il s’est placé parmi les 10pays les plus compétitifs au monde.</p>
<p>Parmi les entreprises les plus connues, citons Maersk (transport maritime et extraction de pétrole), ARLA (produits laitiers), Novo Nordisk (médecine), Brandtex (habillement), Bang  Olufsen (radio-télévision), Danfoss (installations de climatisation et de chauffage), ainsi que Grundfos (pompes), Velux (fenêtres), Lego (jouets) et Vestas (l’un des principaux fabricants mondiaux d’éoliennes).</p>
<h3 class="spip"><a id="sommaire_2"></a>Les secteurs porteurs</h3>
<h4 class="spip">Biens de consommations, santé et habitat</h4>
<p>L’habitat occupe une grande place dans le budget du consommateur danois comme en témoignent non seulement les statistiques mais aussi les multiples émissions télévisées et expositions sur l’habitat. En effet, pour les Danois, la maison, le « chez-soi », sont très importants. La tendance internationale au « cocooning » est bien présente dans ce pays où le soleil se fait rare en hiver.</p>
<p>L’univers de l’enfant est un autre secteur porteur au Danemark. L’attention particulière portée à la génération future, associée à une population à fort pouvoir d’achat, garantit un environnement économique solide pour le marché de la puériculture et de l’enfance, et une véritable aubaine pour les professionnels du secteur. Les Danois consacrent également de plus en plus de moyens à leurs activités de loisirs, et à leurs animaux domestiques. Enfants, loisirs, et animaux ; trois domaines dans lesquels les Danois refusent de faire des économies, même en temps de crise.</p>
<p>Le consommateur danois privilégie les valeurs d’environnement, de bien-être, de sécurité et de commerce éthique, tout en restant néanmoins attentif au prix. De manière générale, il existe donc des opportunités au Danemark pour les biens de consommation certifiés bio et/ou issu du commerce éthique, notamment dans le secteur des cosmétiques, de l’hygiène, des textiles.</p>
<p>Enfin, des récentes réformes dans le domaine de la santé méritent également d’être soulignées : le gouvernement danois a décidé d’investir dans la construction de cinq nouveaux hôpitaux au Danemark dans les 10 à 12 années à venir, et dans la modernisation de huit hôpitaux existants, le tout créant un besoin d’équipements et de matériel médicaux ultra-modernes.</p>
<h4 class="spip">Produits, équipements et technologies agroalimentaires</h4>
<p>L’agro-alimentaire reste le secteur de référence en ce qui concerne l’image de qualité française. Le « made in France » y est très porteur. Les produits français pour épicerie fine (moyenne gamme supérieure) ont notamment de bonnes perspectives sur le marché danois. Avec la tendance « nouvelle cuisine nordique » il y a un intérêt nouveau au Danemark pour des productions alimentaires de qualité de niche.</p>
<p>A noter également : les produits faciles, prêts à consommer ont de plus en plus de succès sur le marché danois. Le développement du bio y est aussi très fort : 13 % de la production danoises totale en 2009 (fruits et légumes).</p>
<p>Du côté des vins et apéritifs, une nouvelle tendance se développe pour le cidre. Le « cidre suédois » est pour l’instant le plus connu mais le vrai cidre de qualité, en particulier français, pourrait trouver rapidement sa place sur le marché danois.</p>
<p>Dans le secteur des vins, et notamment dans la grande distribution, le développement récent des <i>bag-in-box</i> est notable.</p>
<p>Au niveau de la grande distribution au Danemark, les évolutions récentes démontrent le développement significatif des enseignes « hard discount ».</p>
<p>Dans un tout autre secteur, celui du machinisme et des équipements agricoles, une opportunité d’affaires se crée pour les produits destinés à une production à la ferme ou pour « micro-IAA », en particulier pour : vins et bières, fromages, panification, salaisons, charcuteries, conserves de fruits et légumes.</p>
<h4 class="spip">Infrastructures, transports, industrie</h4>
<p>Le Danemark a de grands projets d’infrastructure à venir : la construction du lien fixe de <i>Femern</i> reliant le Danemark et l’Allemagne (tunnel à quatre voies routières et deux voies ferrées électrifiées d’une longueur de 19 km) s’inscrivant parmi les grands chantiers futurs de l’Europe ; la remise en état du réseau ferré, notamment le changement de la totalité du système de signalisation ; l’extension du métro de Copenhague (+17 stations sur 15,5 km de voies) ; et la construction du tramway d’Aarhus, deuxième plus grande ville du Danemark.</p>
<p>La flotte de commerce est un secteur très porteur au Danemark : le Danemark contrôle 7 % du tonnage mondial, et les armateurs danois transportent environ 10 % du commerce mondial de marchandises.</p>
<p>Dans le secteur automobile, le pays mise sur les véhicules électriques. Particulièrement actif dans le domaine des énergies renouvelables, le Danemark aspire à devenir un pays moteur pour le déploiement de parcs de véhicules non-polluants alimentés par l’électricité produite par les éoliennes.</p>
<p>Un bouquet énergétique composé encore essentiellement d’énergies fossiles : L’énergie consommée au Danemark provient de quatre sources primaires (2009) : le pétrole (38,8%), le gaz naturel (20,5%), le charbon (21,1%) et les énergies renouvelables (17,6%).</p>
<p>Cependant, depuis l’adoption du plan énergétique ambitieux de février 2008, les énergies renouvelables sont de plus en plus développées : leur part dans la consommation a ainsi grimpé de plus de 2% en un an.</p>
<p>Ainsi, tout produit contribuant à la réduction de la facture énergétique et des émissions pourrait profiter de ce marché fortement tourné vers les énergies renouvelables.</p>
<h4 class="spip">Nouvelles technologies, innovation, services</h4>
<p>Le Danemark est une excellente arène pour les technologies de l’information et la communication. Régulièrement classé parmi les pays les plus dynamiques pour l’adoption des nouvelles technologies, le Danemark est pionnier dans plusieurs secteurs comme l’e-commerce ou encore l’e-administration (Forum Economique Mondial).</p>
<p>Le tout numérique est une réalité au Danemark avec des taux de pénétration des services et d’équipements parmi les plus élevés de la zone.</p>
<p>Le Danemark est également un marché ouvert et soutenu par une politique ambitieuse des autorités en matière d’innovations, notamment dans les technologies de l’information et de la communication (informatique, internet, téléphonie mobile, etc.).</p>
<p>Les dépenses totales en RD ont d’ailleurs atteint l’objectif européen de Barcelone (3 % du PIB pour 2010), avec près de 70 % des efforts de recherche financés par des fonds privés, contre 30 % par des fonds publics.</p>
<p>Les équipements, services et applications IT (convergence services, terminaux, réseaux) font donc partie des créneaux porteurs au Danemark.</p>
<p>Par ailleurs, le marché danois s’ouvre de plus en plus aux réseaux de franchise étrangers sous l’impulsion des enseignes américaines. Dans bien des cas, le marché danois sert de laboratoire expérimental pour l’ensemble des pays de l’Union européenne.</p>
<h3 class="spip"><a id="sommaire_3"></a>Rémunération</h3>
<h4 class="spip">Modalités de rétribution</h4>
<p>L’employeur verse le salaire net, c’est-à-dire que l’impôt sur le revenu est prélevé à la source. L’impôt sur le revenu au Danemark est progressif et varie selon les communes. Le précompte mensuel est calculé selon le pourcentage figurant sur la carte fiscale personnelle, délivrée par le bureau de la commune de résidence (il est à noter que ce taux varie en fonction du montant du salaire). Ce taux d’imposition varie d’environ 38% à 58% du salaire brut. Le régime fiscal danois finance en grande partie le système de protection sociale.</p>
<p>Le salaire mensuel minimum n’existe pas dans la législation danoise. Dans la " grande convention " signée entre LO (<i>Landsorganisationen</i>) et DA (<i>Dansk Arbejdsgiverforening</i>), il est stipulé depuis 1993, à l’article premier, que toutes les questions relatives aux rémunérations doivent être déterminées par accords collectifs. Les règles qui régissent le marché du travail sont négociées tous les trois ans en général et consignées dans les conventions collectives. Généralement, ces accords sont négociés entre les syndicats des salariés et celui du patronat, au niveau national, selon les branches d’activité et couvrent donc un nombre important d’entreprises. Pour les employeurs non adhérent à un syndicat patronal, une convention dite " d’adhésion ", calquée sur la convention collective de la branche correspondante, est signée avec le personnel de l’entreprise. Ainsi, le minimum salarial dépend de la branche concernée. Cependant, il est de plus en plus fréquent de négocier son salaire avec son employeur directement.</p>
<p>Il peut donc exister des différences selon les entreprises, les professions et les domaines et les bassins d’emploi. En effet, selon les statistiques, la région où les salaires sont les plus élevés est celle du " Grand Copenhague " (en moyenne la différence entre les salaires de Copenhague et ceux d’autres régions est d’environ 4000 DKK). Mais ce constat est à relativiser car les salaires coïncident généralement avec le coût de la vie (le montant des loyers est en moyenne trois fois plus élevé à Copenhague et ses environs), plus cher à Copenhague par rapport au reste du pays.</p>
<p>On peut, pour information, approximativement évaluer le salaire horaire brut minimum moyen de 108 DKK (2013). En effet, le salaire minimum est purement indicatif, l’immense partie des employés percevant un salaire plus élevé.</p>
<p>A titre indicatif, un jeune diplômé de l’université sans expérience peut percevoir environ 23 000 DKK pour un poste de secrétaire, 26 000 DKK pour un commercial, 29 000 DKK pour un ingénieur.</p>
<p>Les jeunes diplômés sont souvent obligés d’accepter des conditions salariales moins favorables puisque la fourchette salariale dans le cadre d’un premier emploi peut varier de 18 000 DKK à 28 000 DKK bruts mensuels, en fonction de la formation initiale. Le système de formation généralement peu élitiste et le nombre de jeunes diplômés arrivant chaque année sur le marché du travail sont autant de facteurs qui peuvent expliquer qu’il existe une concurrence à la baisse sur les salaires et que les employeurs valorisent plus l’expérience professionnelle que les diplômes. Ils exigent que les jeunes diplômés " fassent leurs preuves " avant de leur offrir un statut salarial plus favorable. A cet égard, il est à noter que le système des grandes écoles est un système typiquement français qui ne sera pas forcément connu ni reconnu par les employeurs danois.</p>
<h3 class="spip"><a id="sommaire_4"></a>Quelques fourchettes de salaires </h3>
<p>(salaires bruts en couronnes danoises par mois)</p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><i>profession </i></td>
<td><i>Jeunes professionnels </i></td>
<td><i>5 ans d’expérience </i></td></tr>
<tr class="row_even even">
<td>Chef des ventes</td>
<td>35 853</td>
<td>56 296</td></tr>
<tr class="row_odd odd">
<td>Assistant marketing</td>
<td>22 814</td>
<td>30 456</td></tr>
<tr class="row_even even">
<td>Expert en informatique</td>
<td>26 823</td>
<td>38 886</td></tr>
<tr class="row_odd odd">
<td>Chef de projet</td>
<td>35 652</td>
<td>49 258</td></tr>
<tr class="row_even even">
<td>Professeur</td>
<td>25 814</td>
<td>34 203</td></tr>
<tr class="row_odd odd">
<td>Secrétaire</td>
<td>20 863</td>
<td>27 324</td></tr>
<tr class="row_even even">
<td>Infirmière</td>
<td>26 962</td>
<td>35 252</td></tr>
<tr class="row_odd odd">
<td>Employé dans la publicité</td>
<td>27 815</td>
<td>31 169</td></tr>
<tr class="row_even even">
<td>Ingénieur</td>
<td>28 165</td>
<td>37 665</td></tr>
<tr class="row_odd odd">
<td>Technicien</td>
<td>24 712</td>
<td>27 895</td></tr>
</tbody>
</table>
<p><i>source : <a href="http://www.erlimited.com/" class="spip_out" rel="external">Erlimited.com</a></i></p>
<p>Les divers syndicats publient chaque année les statistiques sur les salaires déclarés par leurs membres.Ainsi pour les salaires des ingénieurs, vous pouvez consulter le site Internet de l’<a href="http://www.ida.dk/" class="spip_out" rel="external">IDA (Ingeniorer i Danmark)</a>.</p>
<p>Ces statistiques sont également disponibles du rle site internet de <a href="http://www.di.dk/" class="spip_out" rel="external">DI (Dansk Industri), la Confédération des Industries Danoises</a> et de <a href="http://www.dst.dk/da/Statistik/Publikationer/VisPub.aspx?cid=19581" class="spip_out" rel="external">Danmarks Statistik</a>.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-danemark-emploi-stage-article-stages.md" title="Stages">Stages</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-danemark-emploi-stage-article-entretien-d-embauche-111097.md" title="Entretien d’embauche">Entretien d’embauche</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-danemark-emploi-stage-article-lettre-de-motivation.md" title="Lettre de motivation">Lettre de motivation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-danemark-emploi-stage-article-curriculum-vitae.md" title="Curriculum vitae">Curriculum vitae</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-danemark-emploi-stage-article-recherche-d-emploi.md" title="Recherche d’emploi">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-danemark-emploi-stage-article-reglementation-du-travail.md" title="Réglementation du travail">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-danemark-emploi-stage-article-marche-du-travail-111092.md" title="Marché du travail">Marché du travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/emploi-stage/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
