# Recherche d’emploi

<h4 class="spip">Outils pour la recherche d’emploi</h4>
<p><strong>Journaux</strong></p>
<p>La presse recense peu d’offres d’emplois destinées à des étrangers espérant un salaire correct.</p>
<p><strong>Sites internet</strong></p>
<p>Les sites Internet sont embryonnaires :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.aldaba.com/" class="spip_out" rel="external">Aldaba.com</a> </p>
<p><strong>Réseaux</strong></p>
<p>Les recherches personnelles auprès d’employeurs potentiels sont préconisées ainsi que le dépôt de petites annonces.</p>
<p>Les réseaux et relations sont primordiaux pour trouver un emploi en RD.</p>
<p><strong>Annuaires</strong></p>
<p>Liste des entreprises françaises présentes en République Dominicaine.</p>
<ul class="spip">
<li><a href="http://www.ccifranco-dominicana.org/" class="spip_out" rel="external">Chambre de commerce dominico-française</a></li>
<li>Liste des entreprises membres de la <a href="http://www.camarasantodomingo.do/red-empresarial/sectores/" class="spip_out" rel="external">Chambre de commerce de Saint-Domingue</a> (97 000 entreprises inscrites).</li>
<li><a href="http://camarasantiago.org/Slistadoempresas.html" class="spip_out" rel="external">Liste des entreprises membres de la Chambre de commerce de Santiago</a></li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/emploi-stage/article/recherche-d-emploi). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
