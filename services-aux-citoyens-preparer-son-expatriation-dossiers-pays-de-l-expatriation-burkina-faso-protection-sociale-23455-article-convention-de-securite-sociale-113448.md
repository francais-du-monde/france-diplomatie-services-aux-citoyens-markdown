# Convention de sécurité sociale

<p>Il n’existe pas de convention de sécurité sociale entre la France et le Burkina Faso. <br class="manualbr">Vous pouvez donc vous trouver dans l’une des deux situations suivantes :</p>
<ul class="spip">
<li>Travailleurs salariés détachés dans le cadre de la législation française ;</li>
<li>Travailleurs français expatriés (salariés, non-salariés, retraités, autres catégories) relevant de la législation locale.Pour plus d’information, consultez notre rubrique thématique sur la <a href="services-aux-citoyens-preparer-son-expatriation-protection-sociale.md" class="spip_in">Protection sociale</a>.</li></ul>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/protection-sociale-23455/article/convention-de-securite-sociale-113448). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
