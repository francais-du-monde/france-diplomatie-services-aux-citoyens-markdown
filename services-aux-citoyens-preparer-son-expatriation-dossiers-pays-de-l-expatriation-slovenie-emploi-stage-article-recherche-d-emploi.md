# Recherche d’emploi

<h4 class="spip">Outils pour la recherche d’emploi</h4>
<p>Journaux :</p>
<ul class="spip">
<li><a href="http://www.delo.si/" class="spip_out" rel="external">Delo</a></li>
<li><a href="http://www.dnevnik.si/" class="spip_out" rel="external">Dnevnik</a></li>
<li><a href="http://nov.vecer.com/" class="spip_out" rel="external">Vecer</a></li></ul>
<p>Sites internet :</p>
<ul class="spip">
<li><a href="http://www.ess.gov.si/iskalci_zaposlitve/prosta_delovna_mesta" class="spip_out" rel="external">Ess.gov.si</a></li>
<li><a href="http://www.mojedelo.com/" class="spip_out" rel="external">Mojedelo.com</a></li>
<li><a href="http://www.zaposlitev.net/delo.php" class="spip_out" rel="external">Zaposlitev.net</a></li>
<li><a href="http://www.mojazaposlitev.si/" class="spip_out" rel="external">mojaZaposlitev.si</a></li>
<li><a href="https://ec.europa.eu/eures/main.jsp?lang=fracro=jobcatId=52parentId=0langChanged=true" class="spip_out" rel="external">Ec.europa.eu</a></li></ul>
<h4 class="spip">Organismes pour la recherche d’emploi</h4>
<ul class="spip">
<li><a href="http://english.ess.gov.si/" class="spip_out" rel="external">Agence slovène pour l’emploi</a> (zavod rs za zaposlovanje)</li>
<li><a href="http://www.info-tujci.si/" class="spip_out" rel="external">Service pour l’emploi des étrangers</a> - <strong>Služba za zaposlovanje tujcev</strong> <br class="manualbr"><strong>Dunajska 20</strong><br class="manualbr"><strong>1000 Ljubljana</strong> <br class="manualbr">Tél : + 386 (0)1/ 300 49 40<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/emploi-stage/article/recherche-d-emploi#tujci#mc#ess.gov.si#" title="tujci..åt..ess.gov.si" onclick="location.href=mc_lancerlien('tujci','ess.gov.si'); return false;" class="spip_mail">Courriel</a></li>
<li><strong>Adecco</strong><br class="manualbr">Contact : <br class="manualbr">Slovenska cesta 41 <br class="manualbr">1000 LJUBLJANA <br class="manualbr">Tél : 00386 1 234 9270 et 00386 1234 9 277 <br class="manualbr">Portable : 00386 1661 591<br class="manualbr">Télécopie : 00386 1 234 9274 <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/emploi-stage/article/recherche-d-emploi#ljubljana#mc#adecco.si#" title="ljubljana..åt..adecco.si" onclick="location.href=mc_lancerlien('ljubljana','adecco.si'); return false;" class="spip_mail">Courriel</a></li></ul>
<p>D’<a href="http://www.adecco.si/en/index.asp" class="spip_out" rel="external">autres agences Adecco</a> se trouvent également à Maribor, Celje, Kranj, Koper et Novo mesto.</p>
<p><i>Mise à jour : septembre 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/emploi-stage/article/recherche-d-emploi). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
