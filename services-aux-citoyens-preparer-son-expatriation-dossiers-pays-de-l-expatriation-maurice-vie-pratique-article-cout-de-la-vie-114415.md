# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/vie-pratique/article/cout-de-la-vie-114415#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/vie-pratique/article/cout-de-la-vie-114415#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/vie-pratique/article/cout-de-la-vie-114415#sommaire_3">Alimentation</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/vie-pratique/article/cout-de-la-vie-114415#sommaire_4">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>L’unité monétaire est la roupie mauricienne (MUR.)</p>
<p>Il n’y a pas de contrôle des changes. Le taux de change moyen est 1 euro équivaut à environ 40 roupies.</p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<p>Les transferts de fonds sont libres pour les expatriés. La roupie mauricienne n’est pas exportable.</p>
<p>Il est facile d’ouvrir un compte bancaire à l’île Maurice, sur présentation d’un document d’identité, de vos derniers relevés bancaires, d’une copie du permis de travail et d’une preuve de résidence.</p>
<p>La Banque des Mascareignes (Groupe Caisse d’épargne) est la seule banque française. Il existe plusieurs banques étrangères, comme HSBC, Barclays Bank, Habib Bank, Indian Ocean International Bank (IOIB), etc…</p>
<p>Il n’est pas difficile de changer des euros sur place auprès des banques et bureaux de change qui se trouvent dans toutes les villes importantes. Les guichets automatiques acceptent la plupart des cartes de paiement.</p>
<p>Les cartes de paiement sont acceptées dans la plupart des hôtels, restaurants, stations-service et boutiques.</p>
<h3 class="spip"><a id="sommaire_3"></a>Alimentation</h3>
<p>Prix moyen d’un repas dans un restaurant</p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td></td>
<td>MUR</td>
<td>Euros</td></tr>
<tr class="row_even even">
<td>Restaurant de qualité supérieure</td>
<td>2 000</td>
<td>50</td></tr>
<tr class="row_odd odd">
<td>Restaurant de qualité moyenne</td>
<td>800</td>
<td>20</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_4"></a>Evolution des prix</h3>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/vie-pratique/article/cout-de-la-vie-114415). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
