# L’incidence du retour en France sur l’assurance chômage

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/emploi/article/le-chomage#sommaire_1">Vous revenez d’un pays hors de l’Espace économique européen ou autre que la Suisse </a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/emploi/article/le-chomage#sommaire_2">Vous revenez d’un pays de l’Espace économique européen ou de la Suisse</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/emploi/article/le-chomage#sommaire_3">Vous êtes demandeur d’emploi indemnisé par un pays de l’Union européenne, la Norvège, l’Islande, la Suisse ou le Liechtenstein et vous venez chercher un travail en France </a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/emploi/article/le-chomage#sommaire_4">Vous avez démissionné pour suivre votre conjoint expatrié</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Vous revenez d’un pays hors de l’Espace économique européen ou autre que la Suisse </h3>
<p><strong>L’expatrié affilié au Pôle Emploi Services </strong>(Institution gestionnaire de l’Assurance chômage des expatriés) <strong>à titre obligatoire ou facultatif par votre employeur ou sur démarche individuelle </strong></p>
<p>En cas de perte d’emploi, l’expatrié à son retour en France doit s’inscrire comme demandeur d’emploi au Pôle Emploi de son domicile, dans les 12 mois suivant la perte de son activité. Il peut obtenir une allocation chômage sous certaines conditions. Celle-ci est calculée selon la réglementation de la convention relative à l’Assurance chômage française et prend en compte la base des salaires de l’expatriation.</p>
<p>Cette adhésion préalable au Pôle Emploi Services est importante car le demandeur d’emploi, s’il est indemnisé, bénéficie automatiquement de la couverture assurance maladie, assurance vieillesse et retraite complémentaire.</p>
<p>L’expatrié qui avant son départ à l’étranger bénéficiait d’une allocation chômage conserve ses droits à l’assurance chômage.</p>
<p>Le titulaire d’une allocation du Pôle emploi doit obligatoirement déclarer son expatriation pour en interrompre le versement. Les droits sont préservés pendant un délai de trois ans ajouté à la durée des droits restants. Au retour, l’expatrié peut bénéficier d’une reprise du reliquat de ses anciens droits.</p>
<p><strong>L’expatrié non affilié au Pôle Emploi Services </strong></p>
<p>En cas de perte d’emploi, l’expatrié qui n’est pas affilié au Pôle Emploi Services <strong>n’est pas indemnisé au titre de l’assurance chômage à son retour en France</strong>.</p>
<p>Des mécanismes d’insertion peuvent intervenir pour lui assurer un revenu minimum (<a href="http://www.pole-emploi.fr/candidat/allocation-temporaire-d-attente-ata--@/article.jspz?id=60952" class="spip_out" rel="external">Allocation temporaire d’attente</a> pour les travailleurs salariés expatriés non couverts par le régime d’assurance chômage qui, lors de leur retour en France, justifient d’une durée de travail de 182 jours au cours des 12 mois précédant la fin de leur contrat de travail).<br class="manualbr">Cette allocation lui permet de bénéficier de la couverture maladie universelle (CMU).</p>
<h3 class="spip"><a id="sommaire_2"></a>Vous revenez d’un pays de l’Espace économique européen ou de la Suisse</h3>
<p><strong>Les travailleurs français ayant perdu leur emploi en Europe qui reprennent une activité en France </strong></p>
<p>Les règlements communautaires prévoient l’indemnisation du chômage par le pays de résidence lors du retour, des personnes ayant perdu leur emploi au sein d’un autre Etat de l’Espace économique européen ou en Suisse. Le Pôle Emploi peut, si le travailleur se trouve sans emploi en France après y avoir repris une activité, faire appel aux périodes de travail accomplies en Europe pour servir des allocations chômage.</p>
<p>Pour que le régime français d’assurance chômage reconnaisse les périodes de cotisations acquises au sein d’un autre Etat de l’Espace économique européen ou en Suisse, le chômeur doit :</p>
<ul class="spip">
<li>présenter au Pôle Emploi l’imprimé <a href="http://www.cleiss.fr/reglements/u1.html" class="spip_out" rel="external">U1</a>  (<i>Attestation concernant les périodes à prendre en compte pour l’octroi des prestations de chômage</i>) validé par la caisse d’assurance chômage locale du lieu de travail qu’il quitte. Cette attestation indique au Pôle Emploi la durée de son activité et la rémunération perçue ;</li>
<li>exercer impérativement une activité professionnelle pendant une journée au moins en France avant l’inscription auprès de Pôle Emploi.</li></ul>
<p>Le montant de l’allocation chômage sera calculé sur la base de ce salaire perçu en France.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.cleiss.fr/particuliers/je_viens_chercher_un_emploi.html" class="spip_out" rel="external">CLEISS</a></p>
<p><strong>Les travailleurs français ayant perdu leur emploi en Europe qui demandent le transfert de leurs droits vers la France</strong></p>
<h3 class="spip"><a id="sommaire_3"></a>Vous êtes demandeur d’emploi indemnisé par un pays de l’Union européenne, la Norvège, l’Islande, la Suisse ou le Liechtenstein et vous venez chercher un travail en France </h3>
<p>Le demandeur d’emploi indemnisé par l’un des 28 pays membres de l’Union européenne où il a exercé son activité salariale peut bénéficier, sous certaines conditions, du transfert de ses prestations chômage.</p>
<p>Avant tout départ pour la France, le demandeur d’emploi fait remplir le <strong><a href="http://www.cleiss.fr/reglements/u2.html" class="spip_out" rel="external">formulaire U2</a> </strong>par le service public de l’emploi de son pays. A partir de ce moment-là, il a 7 jours pour s’inscrire comme demandeur d’emploi auprès d’une agence Pôle emploi, en France.</p>
<p>Arrivé en France, le demandeur d’emploi s’inscrit obligatoirement à l’agence Pôle emploi de son lieu de domicile dans les 7 jours suivant son arrivée sur le territoire français et remet le <strong><a href="http://www.cleiss.fr/reglements/u2.html" class="spip_out" rel="external">formulaire U2</a></strong>. Pôle emploi services transmet la notification d’inscription comme demandeur d’emploi à l’organisme compétent de l’état membre. Ce dernier versera les allocations chômage, pendant la durée d’exportation prévue par le pays membre.</p>
<p>Le paiement de ses allocations est effectué par Pôle emploi, pour le compte du service public étranger pendant trois mois maximum.</p>
<p>Si votre situation personnelle ne vous permet pas de bénéficier de ces dispositions, reportez-vous au paragraphe sur « L’expatrié non affilié au Pôle Emploi Services » au chapitre précédent consacré aux salariés expatriés en dehors de l’Espace économique européen et de la Suisse. Les possibilités d’indemnisation sont identiques.</p>
<h3 class="spip"><a id="sommaire_4"></a>Vous avez démissionné pour suivre votre conjoint expatrié</h3>
<p>On entend par conjoint : l’époux, le concubin ou le partenaire lié par un pacte civil de solidarité (PACS).</p>
<p><strong>Vous revenez de n’importe quel pays </strong></p>
<p><i>Avant votre départ à l’étranger, vous n’êtes pas inscrit en tant que demandeur d’emploi </i></p>
<p>Lors de votre retour en France, vous pouvez bénéficier des prestations d’assurance chômage sous certaines conditions :</p>
<ul class="spip">
<li>ne pas avoir séjourné plus de quatre années à l’étranger à compter de la date de cessation de l’activité française.</li>
<li>vous inscrire au retour comme demandeur d’emploi à l’agence Pôle emploi dont dépend votre domicile français.</li>
<li>fournir à Pôle emploi l’attestation de l’employeur remise lors de la rupture de contrat de travail.</li>
<li>fournir votre lettre de démission précisant le motif : « pour suivi de conjoint » et un justificatif de votre résidence à l’étranger.</li>
<li>remettre un justificatif de l’activité à l’étranger de votre conjoint (contrat de travail, certificat de travail).</li></ul>
<p><strong>Vous revenez d’un pays de l’Espace économique européen ou de la Suisse </strong></p>
<p><i>Vous étiez parti après vous être ouvert en France des droits aux allocations chômage. </i></p>
<p>Lors de votre retour en France, vous pourrez percevoir le reliquat des droits ouverts avant votre départ sous réserve que votre inscription en France comme demandeur d’emploi intervienne dans la limite du délai de déchéance (trois ans augmenté de la durée de vos droits).</p>
<p><i>Vous étiez parti sans vous être ouvert en France des droits aux allocations chômage.</i></p>
<p>Lors de votre retour en France :</p>
<ul class="spip">
<li>si vous avez retrouvé un travail dans le pays d’accueil, vos droits sont ceux d’un expatrié ;</li>
<li>si vous n’en avez pas retrouvé, les droits aux allocations chômage au titre de l’emploi exercé en France sont préservés pendant quatre ans.</li></ul>
<p>Dans ce dernier cas, votre retour et votre inscription comme demandeur d’emploi en France doivent impérativement intervenir dans les quatre ans suivant la fin de vos fonctions exercées en France.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p><a href="http://www.pole-emploi.fr/candidat/salarie-expatrie-hors-europe-@/article.jspz?id=60557" class="spip_out" rel="external">Pôle Emploi Services - Service aux Expatriés</a></p>
<ul class="spip">
<li>Courrier : TSA 10107 - 92891 NANTERRE CEDEX 9</li>
<li>Téléphone : 01 46 52 97 00</li>
<li>Courriel : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/emploi/article/le-chomage#pesmobiliteinternationale#mc#pole-emploi.fr#" title="pesmobiliteinternationale..åt..pole-emploi.fr" onclick="location.href=mc_lancerlien('pesmobiliteinternationale','pole-emploi.fr'); return false;" class="spip_mail">pesmobiliteinternationale<span class="spancrypt"> [at] </span>pole-emploi.fr</a></li>
<li>Accueil : 14 rue de Mantes — 92700 COLOMBES</li>
<li>Lundi au jeudi : 9h – 17h et vendredi : 9h – 16h</li></ul>
<p><i>Mise à jour : mars 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/emploi/article/le-chomage). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
