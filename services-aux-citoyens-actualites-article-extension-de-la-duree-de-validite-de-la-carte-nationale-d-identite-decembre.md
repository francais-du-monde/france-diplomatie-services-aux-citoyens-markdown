# Extension de la durée de validité de la carte nationale d’identité (décembre 2013)

<p class="spip_document_77821 spip_documents spip_documents_center">
<img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L720xH300/affiche_cni_bd_une_0_cle05fe78-0d3bb.jpg" width="720" height="300" alt=""></p>
<p><strong>A compter du 1er janvier 2014, la durée de validité de la carte nationale d’identité passe de 10 à 15 ans pour les personnes majeures (plus de 18 ans).</strong></p>
<p>L’allongement de cinq ans pour les cartes d’identité concerne :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  les nouvelles cartes d’identité sécurisées (cartes plastifiées) délivrées à partir du 1er janvier 2014 à des personnes majeures ;</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  les cartes d’identité sécurisées délivrées (cartes plastifiées) entre le 2 janvier 2004 et le 31 décembre 2013 à des personnes majeures ;</p>
<p><strong>ATTENTION :</strong> cette prolongation ne s’applique pas aux cartes nationales d’identité sécurisées pour les personnes mineures. Elles seront valables 10 ans lors de la délivrance. Inutile de vous déplacer dans votre mairie ou votre consulat.</p>
<p><strong>Si votre carte d’identité a été délivrée entre le 2 janvier 2004 et le 31 décembre 2013, la prolongation de 5 ans de la validité de votre carte est automatique.</strong> Elle ne nécessite aucune démarche particulière. La date de validité inscrite sur le titre ne sera pas modifiée.</p>
<p>Les États membres de l’Union européenne et un certain nombre d’autres pays d’Europe ou du pourtour méditerranéen acceptent la carte nationale d’identité comme document de voyage.</p>
<p><strong>Pour les cartes en apparence périmées mais dont la validité est prolongée de 5 ans, les autorités des pays suivants ont officiellement confirmé qu’elles l’acceptaient comme document de voyage</strong> :</p>
<ul class="spip">
<li>Andorre ;</li>
<li>Bulgarie ;</li>
<li>Croatie ;</li>
<li>Grèce ;</li>
<li>Hongrie ;</li>
<li>Lettonie ;</li>
<li>Malte ;</li>
<li>Monaco ;</li>
<li>Monténégro ;</li>
<li>République tchèque ;</li>
<li>Saint-Martin ;</li>
<li>Serbie ;</li>
<li>Slovénie ;</li>
<li>Suisse ;</li>
<li>Tunisie (uniquement pour les binationaux ou personnes participant à des voyages de groupe organisés par un professionnel du tourisme).</li>
<li>Turquie</li></ul>
<p><strong>Les autorités des pays suivants n’ont pas officiellement transmis leur position</strong> quant à leur acceptation de la carte nationale d’identité en apparence périmée mais dont la validité est prolongée de 5 ans comme document de voyage :</p>
<ul class="spip">
<li>pays de l’Union européenne : Allemagne, Autriche, Belgique, Chypre, Danemark, Espagne, Estonie, Finlande, Irlande, Italie, Lituanie, Luxembourg, Pays-Bas, Pologne, Portugal, Roumanie, Royaume-Uni, Slovaquie, Suède ;</li>
<li>de l’Espace Schengen : Islande, Liechtenstein, Norvège ;</li>
<li>Vatican ;</li>
<li>Albanie, Ancienne République Yougoslave de Macédoine, Bosnie-Herzégovine ;</li>
<li>Égypte ;</li>
<li>Maroc (uniquement pour les binationaux ou personnes participant à des voyages de groupe organisés par un professionnel du tourisme).</li></ul>
<p>De façon à éviter tout désagrément pendant votre voyage, <strong>il vous est fortement recommandé de privilégier l’utilisation d’un passeport valide à une CNI portant une date de fin de validité dépassée</strong>, même si elle est considérée par les autorités françaises comme étant toujours en cours de validité.</p>
<p>Vous pouvez télécharger les fiches d’informations sur l’allongement de la durée de validité de la CNI  traduites pour chaque pays acceptant la carte nationale d’identité comme document de voyage  sur <a href="http://www.interieur.gouv.fr/Actualites/L-actu-du-Ministere/Duree-de-validite-de-la-CNI" class="spip_out" rel="external">le site du ministère de l’Intérieur</a>.</p>
<h4 class="spip">Rappel sur les documents d’identité et visas</h4>
<p><strong>Vérifiez les documents de voyage requis (carte nationale d’identité, passeport, visa) pour l’entrée et le séjour dans votre pays de destination </strong> auprès de <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-etrangers-en-france/" class="spip_in">l’ambassade et du consulat de ce pays en France.</a> S’agissant du passeport, certains pays exigent une validité minimum.</p>
<p>Au sein de l’Union européenne (UE), la carte nationale d’identité en cours de validité est suffisante pour voyager. <br class="autobr">Hors UE, la plupart des Etats exigent un passeport valide plusieurs mois après la date prévue de retour en France. Adressez-vous en temps utile à votre préfecture pour son renouvellement éventuel.</p>
<p><strong>Pensez à conserver, à votre domicile, la photocopie des documents que vous emportez</strong> (en cas de perte ou de vol à l’étranger) et à vous munir d’au moins 2 photos d’identité.</p>
<p>Pour plus de sécurité,<strong> le ministère des Affaires étrangères vous recommande de stocker ces documents sur le site <a href="https://connexion.mon.service-public.fr/auth/0?spid=http://portail.msp.gouv.fr&amp;minlvl=1&amp;mode=0&amp;failure_id=0" class="spip_out" rel="external">mon.service-public.fr</a>.</strong> Ce portail permet de créer en quelques clics un espace de stockage personnel, gratuit et confidentiel, accessible sur internet 24h/24. En cas de vol ou de perte de vos papiers d’identité, vous pourrez télécharger ces pièces et faciliter la preuve de votre identité.</p>
<p>En cas de perte ou vol du passeport ou de la carte nationale d’identité lors d’un séjour à l’étranger, vous devez, en tout premier lieu, en faire la déclaration aux autorités locales de police. A partir de cette déclaration, l’ambassade ou le consulat de France pourra établir, selon les cas, un laissez-passer ou un passeport d’urgence. Attention cette formalité est payante et nécessite un délai.</p>
<p><i>Mise à jour : 24.06.2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/actualites/article/extension-de-la-duree-de-validite-de-la-carte-nationale-d-identite-decembre). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
