# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/vie-pratique/article/transports-114416#sommaire_1">Importation de véhicule</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/vie-pratique/article/transports-114416#sommaire_2">Permis de conduire</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/vie-pratique/article/transports-114416#sommaire_3">Code de la route</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/vie-pratique/article/transports-114416#sommaire_4">Assurances et taxes</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/vie-pratique/article/transports-114416#sommaire_5">Achat et location</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/vie-pratique/article/transports-114416#sommaire_6">Immatriculation</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/vie-pratique/article/transports-114416#sommaire_7">Entretien</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/vie-pratique/article/transports-114416#sommaire_8">Réseau routier</a></li>
<li><a id="so_9" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/vie-pratique/article/transports-114416#sommaire_9">Transports en commun</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Importation de véhicule</h3>
<p>L’importation d’une voiture personnelle est possible, il est cependant important de considérer les points ci-dessous afin de calculer la rentabilité de l’opération bien que les voitures soient plus chères à l’île Maurice.</p>
<ul class="spip">
<li>La voiture doit avoir été enregistrée au nom du propriétaire, dans le pays de sa résidence, six mois au moins avant son arrivée à Maurice.</li>
<li>Le coût de transport depuis la France d’un véhicule est élevé (environ 3000 à 4000 euros).</li>
<li>La conduite étant à gauche à Maurice, la conduite d’une voiture équipée d’un volant à gauche est dangereuse. Vous devrez solliciter une homologation de la N.T.A. (<i>National Transport Authority</i>) pouvant exiger la modification du véhicule.</li>
<li>Une taxe de 15 % sur le C. I. F. (<i>Cost Insurance Freight</i>) + 15 % de V.A.T. sont applicables sur la voiture.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Permis de conduire</h3>
<p>Les permis de conduire français et international sont reconnus. Le Bureau du trafic, à Port-Louis, délivre sur présentation du permis français, une licence mauricienne, à renouveler tous les ans. La procédure est gratuite et faite dans la journée.</p>
<p>Le permis à point (<i>counterpart</i>) ne s’applique pas aux ressortissants étrangers résidents à Maurice, mais ils sont toutefois redevables des amendes prévus par le code de la route et susceptibles de passer en Cour en cas d’infraction grave.</p>
<h3 class="spip"><a id="sommaire_3"></a>Code de la route</h3>
<p>La conduite s’effectue à gauche ; la priorité est à droite. La vitesse est réglementée à 40 km/h en ville, 80 km/h sur routes et 110 km/h sur autoroutes. Maurice commence à se doter de nombreux radars fixes et mobiles et des patrouilles de police procèdent à des contrôles ciblés.</p>
<p>Il faut faire preuve de la plus grande prudence du fait de la conduite à gauche, des routes étroites, du manque de signalisation et d’éclairage public etc. Le taux d’accidents mortels est particulièrement élevé.</p>
<h3 class="spip"><a id="sommaire_4"></a>Assurances et taxes</h3>
<p>Tout véhicule doit être obligatoirement assuré. Les compagnies d’assurance, en cas d’accident, exigent le plus souvent une copie du rapport de police sur les responsabilités des protagonistes. Dès le retour en France il convient d’informer immédiatement sa compagnie d’assurance et de consulter, le cas échéant un avocat.</p>
<p>L’affiliation à une compagnie d’assurance locale est obligatoire pour le risque tiers. Ceci conditionne l’obtention de la vignette, elle-même obligatoire. Exemples de coûts d’une prime d’assurance pour une Renault Clio 1100 c.c. :</p>
<ul class="spip">
<li>tiers illimitée : 5000 MUR (soit 121 euros)</li>
<li>tous risques avec franchise : 20 000 MUR (soit 487 euros)</li></ul>
<p>Le montant de la franchise : 5000 MUR (soit 121 euros)</p>
<h3 class="spip"><a id="sommaire_5"></a>Achat et location</h3>
<p><strong>Achat</strong></p>
<p>De nombreuses marques françaises (Peugeot, Renault et Citroën) et étrangères (Mercedes, Rover, BMW, Nissan, Toyota) sont représentées. Il est donc possible d’acheter un véhicule français ou étranger sur place dans plusieurs garages et auprès de concessionnaires de marques automobiles. Les véhicules sont très chers car très taxés (comptez entre 800 000 et 1 million MUR pour une Citroen C3 ou Scenic Renault).</p>
<p>Il est préférable d’acquérir un véhicule robuste et climatisé. Acheter un véhicule d’occasion est possible.</p>
<p><strong>Location</strong></p>
<p>Différentes agences de location sont présentes sur l’île : Hertz, Avis, Europcar, et les agences locales.</p>
<p>La location d’une voiture est une pratique courante mais onéreuse (1 500 à 2 000 MUR par jour minimum - soit de 37 à 50 euros - pour un véhicule de cylindrée moyenne). L’âge minimum requis est de 21 ans.</p>
<h3 class="spip"><a id="sommaire_6"></a>Immatriculation</h3>
<p>Les deux derniers chiffres de plaque d’immatriculation représentent l’année de mise en circulation du véhicule. Les couleurs des plaques d’immatriculation sont généralement jaunes ou noires.</p>
<h3 class="spip"><a id="sommaire_7"></a>Entretien</h3>
<p>Entretenir ou faire réparer son véhicule est possible à un coût comparable à celui de France pour un service satisfaisant :</p>
<p>Vidange / graissage : 2000 MUR (soit 50 euros).</p>
<p>Il est également possible de se procurer des pièces détachées de voitures de marques françaises mais avec un délai de plus d’un mois et à un coût élevé.</p>
<h3 class="spip"><a id="sommaire_8"></a>Réseau routier</h3>
<p>La circulation est libre sur tout le territoire. Un réseau d’autoroute de qualité a récemment été construit sur l’île. Le réseau de routes principales est relativement correct (goudronnées). Les autres routes sont souvent très étroites.</p>
<p><strong>En cas d’accident</strong></p>
<p>En cas d’accident, se rendre immédiatement auprès du poste de police le plus proche. Les véhicules sont en principe immobilisés jusqu’à l’arrivée de la police. Le constat amiable existe mais n’est pas applicable dans un certain nombre de cas : accidents avec blessés ou morts, cas d’alcoolisme avéré, véhicule non assuré.</p>
<p>En cas d’accident grave, un contrôle de l’alcoolémie est effectué. Le gouvernement mauricien a revu à la baisse le taux d’alcoolémie autorisé dans le sang passant de 0,8g/l à 0,5g/l.</p>
<h3 class="spip"><a id="sommaire_9"></a>Transports en commun</h3>
<p>Les liaisons hors agglomération sont assurées par autocars. La seule liaison aérienne intérieure régulière est à destination de l’Ile Rodrigues (ATR42/500 et ATR42/300 - durée de vol 1h30).</p>
<p>En ville, les taxis et les bus sont les modes de transport en commun.</p>
<p><strong>Bus</strong></p>
<p>Il existe un bon réseau de bus Maurice. Toutefois, les bus sont peu confortables et la plupart ne sont pas climatisés. Ils circulent entre 5h00 et 21h00. Port-Louis est bien desservi.</p>
<p>Les plus grandes compagnies de bus sont :</p>
<ul class="spip">
<li>National Transport Corporation (NTC)</li>
<li>United Bus Service (UBS)</li>
<li>Mauritius Bus Transport (MTB)</li>
<li>Triolet Bus Service (TBS)</li>
<li>Rose Hill Transport (RHT)</li></ul>
<p><strong>Taxis</strong></p>
<p>Les taxis sont dépourvus de compteur, il est donc important de négocier le prix de la course avant le départ.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/vie-pratique/article/transports-114416). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
