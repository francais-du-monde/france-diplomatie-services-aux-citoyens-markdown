# Animaux domestiques

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/entree-et-sejour-22762/article/animaux-domestiques-109693#sommaire_1">Chiens, chats et furets</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/entree-et-sejour-22762/article/animaux-domestiques-109693#sommaire_2">Autres animaux</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/entree-et-sejour-22762/article/animaux-domestiques-109693#sommaire_3">Pour plus d’informations</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Chiens, chats et furets</h3>
<p>Depuis le 1er janvier 2012, les chiens, chats et furets provenant d’autres pays membres de l’Union européenne doivent, pour entrer au Royaume-Uni, répondre aux conditions suivantes :</p>
<ul class="spip">
<li>être identifiés à l’aide d’une puce électronique ;</li>
<li>être vaccinés contre la rage (dernier rappel à jour datant de plus de 21 jours avant l’entrée sur le territoire du Royaume-Uni) ;</li>
<li>être titulaires d’un passeport européen délivré par un vétérinaire ;</li>
<li>emprunter un transporteur et un point d’entrée autorisés pour les animaux de compagnie (<a href="http://www.defra.gov.uk/wildlife-pets/pets/travel/pets/routes/" class="spip_out" rel="external">liste des transporteurs et routes  autorisées</a>).</li></ul>
<p>Conditions complémentaires pour les <strong>chiens uniquement </strong> :</p>
<ul class="spip">
<li>avoir été vermifugé contre le ténia (échinococcose) entre 120 et 24h avant l’arrivée sur le territoire (avec indication par le vétérinaire de cette étape sur le passeport, <a href="http://www.defra.gov.uk/wildlife-pets/pets/travel/pets/pet-owners/#from1jan" class="spip_out" rel="external">plus d’informations</a>)</li>
<li>être âgés de trois mois ou plus</li></ul>
<p><strong>Les pitbull, tosa japonais, dogue argentin ou fila brazilero sont des races interdites au Royaume-Uni.</strong></p>
<h3 class="spip"><a id="sommaire_2"></a>Autres animaux</h3>
<p>Les règles vétérinaires varient selon l’espèce concernée. Une autorisation spéciale est requise pour certaines espèces menacées - serpents, lézards, tortues, par exemple. Votre vétérinaire saura vous renseigner sur les certificats exigés.</p>
<h3 class="spip"><a id="sommaire_3"></a>Pour plus d’informations</h3>
<ul class="spip">
<li>Notre <a href="services-aux-citoyens-preparer-son-expatriation-douanes-article-animaux-domestiques.md" class="spip_in">article thématique sur l’exportation d’animaux domestiques</a></li>
<li><a href="http://agriculture.gouv.fr/transport" class="spip_out" rel="external">Information sur le site du ministère de l’Agriculture français</a></li>
<li>Information sur le site du <a href="https://www.gov.uk/take-pet-abroad" class="spip_out" rel="external">Department for Environment, Food and Rural Affairs</a> ou appelez la Defra Pets Helpline (00 44) (0)870 241 1710</li>
<li>Information sur le site du <a href="https://www.gov.uk/pet-travel-legislation" class="spip_out" rel="external">Department for Environment, Food and Rural Affairs</a></li></ul>
<p>En cas de doute, veuillez contacter le service d’information de l’<a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-etrangers-en-france/article/annuaire-des-representations-etrangeres-en-france" class="spip_in">ambassade de Grande-Bretagne</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/entree-et-sejour-22762/article/animaux-domestiques-109693). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
