# Santé

<p>Pour une présentation des risques épidémiologiques ainsi que des conseils sur la médecine au quotidien, consultez les pages suivantes :</p>
<ul class="spip">
<li>Conseils, liste de médecins et hôpitaux sur le site de l’ambassade de France à Prague</li>
<li>Page dédiée à la santé en République tchèque sur le site Conseils aux voyageurs</li>
<li>Fiche Prague sur le site du <a href="http://www.cimed.org" class="spip_out" rel="external">CIMED</a>.</li></ul>
<h4 class="spip">Médecine de soins</h4>
<p>L’état sanitaire du pays et les conditions d’hygiène sont bons. Tous les soins, de bonne qualité, sont disponibles sur place.</p>
<p><strong>Pour un court séjour, se munir de la carte européenne d’assurance maladie à demander à votre caisse d’assurance maladie au moins 15 jours avant le départ.</strong> Pour plus d’information, consultez le site de l’<a href="http://www.ameli.fr/assures/droits-et-demarches/a-l-etranger/index.php" class="spip_out" rel="external">assurance maladie</a>.</p>
<p><strong>Il est recommandé de contracter une assurance rapatriement.</strong></p>
<p>Aucune vaccination n’est exigée des voyageurs internationaux, quelle que soit leur provenance.</p>
<p>Les vaccinations recommandées d’un point de vue médical sont : diphtérie, tétanos, poliomyélite ; elle doivent être à jour).</p>
<p>En fonction de la durée et des modalités du séjour :</p>
<ul class="spip">
<li>Hépatite A : en dehors des grandes villes ;</li>
<li>Encéphalite à tiques pour des activités de plein air au printemps et en été (pour les enfants : à partir de l’âge de trois ans).</li></ul>
<p>Pour les enfants, toutes les vaccinations incluses dans le calendrier vaccinal français devront également être à jour.</p>
<p><strong>Numéros utiles</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Urgence - police - secours : 155. </p>
<p><strong>Médecins agréés auprès de l’ambassade</strong></p>
<p>Consultez la <a href="http://www.france.cz/spip.php?article370" class="spip_out" rel="external">liste des médecins accrédités par le consulat</a>.</p>
<p><strong>Urgences (hôpitaux disposant d’un accueil multilingue pour les étrangers)</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Hôpital Na Homolce<br class="manualbr">Roentgenova 2 <br class="manualbr">150 00 Prague. 5 <br class="manualbr">Tél : 257 271 111. </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Hôpital v Motole<br class="manualbr">V Uvalu 84<br class="manualbr">150 18 Prague 5 <br class="manualbr">Tél. : 224 431 111</p>
<p>Pour de plus amples renseignements, vous pouvez consulter le site de l’<a href="http://www.invs.sante.fr/" class="spip_out" rel="external">INVS</a>, ainsi que celui de l’<a href="http://www.who.int/fr/index.html" class="spip_out" rel="external">OMS</a>, qui vous renseigneront sur l’état sanitaire de ce pays, ou les sites de <a href="http://www.pasteur-lille.fr/" class="spip_out" rel="external">l’institut Pasteur de Lille</a>, de <a href="http://www.pasteur.fr/" class="spip_out" rel="external">l’institut Pasteur de Paris</a> et du <a href="http://www.cimed.org/" class="spip_out" rel="external">CIMED</a>.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/vie-pratique/article/sante). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
