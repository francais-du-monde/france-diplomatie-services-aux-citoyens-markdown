# Santé

<p>L’état sanitaire du pays est satisfaisant et comparable à celui de la France. De même, l’infrastructure médicale est de bonne qualité et les conditions d’hygiène correctes.</p>
<p>Les consultations à domicile sont parfois possibles moyennant un supplément d’honoraires. Le prix des consultations est élevé (consultation simple : 300 ILS, visite : 400 ILS, spécialiste : 600 ILS).</p>
<p>Les hôpitaux publics israéliens n’acceptent que difficilement les étrangers qui n’ont pas de numéro d’identification qui rentrent dans leurs critères informatiques et les documents qui conviennent pour assurer la prise en charge.</p>
<p>De nombreux médecins français, généralistes et spécialistes exercent dans les villes principales. Bon nombre de médecins israéliens ont suivi leurs études aux Etats-Unis, en Europe ou en France.</p>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li><a href="http://www.ambafrance-il.org/Sante.html" class="spip_out" rel="external">Conseils, liste de médecins et hôpitaux</a> sur le site de l’ambassade de France en Israël ;</li>
<li><a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/israel-territoires-palestiniens/" class="spip_in">Page dédiée à la santé en Israël</a> dans les Conseils aux voyageurs.</li>
<li>Rubriques thématiques sur le <a href="http://www.cimed.org" class="spip_out" rel="external">site du CIMED</a>.</li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/vie-pratique/article/sante). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
