# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/vie-pratique/article/cout-de-la-vie-111130#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/vie-pratique/article/cout-de-la-vie-111130#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/vie-pratique/article/cout-de-la-vie-111130#sommaire_3">Alimentation</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/vie-pratique/article/cout-de-la-vie-111130#sommaire_4">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>L’unité monétaire est le rial iranien.</p>
<p>Le système des changes a fluctué entre un et plusieurs taux différents. Le taux de change de la banque centrale est début 2014 : 1 euro = 33 000 rials iraniens. Il existe également un marché libre (bureaux de change agréés par la banque centrale) sur lequel 1 euro s’échange début 2014 autour de 40 000 rials iraniens.</p>
<p>Bien que la monnaie iranienne soit le rial, les prix sont assez souvent exprimés en tomans (un toman vaut 10 rials) ou en billet (1 billet = 1000 tomans ou 10 000 rials).</p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<h4 class="spip">Transfert de fonds</h4>
<p>Il y a un contrôle des changes, très strict. Le rial iranien n’est pas convertible. Il est donc impossible de convertir des rials iraniens en euros pour les rapatrier en quittant le pays.</p>
<p>Le moyen de paiement le plus utilisé est le paiement en liquide. Pour les milieux d’affaires le paiement par chèque est davantage employé. Les cartes bancaires internationales ne peuvent être utilisées. Il est impossible de payer ou d’effectuer des retraits avec une carte bancaire étrangère. Le dollar américain est accepté assez largement. Il est conseillé de voyager avec du numéraire, en dollars américains (coupures récentes) ou en euros qui sont aisément changés dans les banques et les bureaux de change en ville.</p>
<h3 class="spip"><a id="sommaire_3"></a>Alimentation</h3>
<p>Il n’y a pas de difficultés particulières d’approvisionnement, mais les produits importés sont chers, et leur prix varie du simple au triple en raison de leur arrivée irrégulière sur le marché iranien. On peut toutefois rencontrer des périodes de pénuries pour certains produits.</p>
<p><strong>Prix moyen d’un repas dans un restaurant</strong></p>
<p>Le prix moyen d’un repas dans un restaurant de qualité supérieure est de 20 à 30 euros, et de 12 euros dans un restaurant de qualité moyenne. Il convient de laisser un pourboire, représentant environ 10 % de la note.</p>
<h3 class="spip"><a id="sommaire_4"></a>Evolution des prix</h3>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : février 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/vie-pratique/article/cout-de-la-vie-111130). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
