# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/emploi-stage/article/reglementation-du-travail-110490#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/emploi-stage/article/reglementation-du-travail-110490#sommaire_2">Contrat de travail – spécificités</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/emploi-stage/article/reglementation-du-travail-110490#sommaire_3">Fêtes légales</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/emploi-stage/article/reglementation-du-travail-110490#sommaire_4">Emploi du conjoint</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<p>En 1968, le législateur singapourien a adopté l’<i>Employment Act</i> inspiré de la <i>Common Law</i> anglo-saxonne. Ce texte de référence en matière de législation du travail à Singapour s’applique aux salariés singapouriens ou étrangers, à l`exclusion des cadres, des employés soumis à des obligations de confidentialité, des travailleurs indépendants, des représentants légaux et des fonctionnaires qui sont, eux, soumis à la <i>Common Law</i>.</p>
<p>Il est possible de consulter l’<i>Employment Act</i> à l’adresse Internet suivante : <a href="http://statutes.agc.gov.sg/" class="spip_out" rel="external">Statutes.agc.gov.sg</a></p>
<p>Les inspecteurs du travail (<i>Inspecting Officer</i>) veillent au respect de la législation du travail. Pour exercer leur mission, ils ont un droit d’accès au lieu de travail et aux divers documents de l’entreprise.</p>
<p>Outre un taux de chômage très bas, le marché de l’emploi est caractérisé par l’absence de droit du travail. Cela permet aux entreprises une plus grande liberté d’embauche mais les salariés ne bénéficient pas, par exemple, d’un salaire minimum garanti.</p>
<h4 class="spip">La période d’essai</h4>
<p>La période d’essai n’est pas prévue par la loi mais les parties y ont souvent recours. Sa durée varie d’un à trois mois pour un salarié et de trois à six mois pour les cadres. Durant cette période, les parties peuvent mettre fin unilatéralement au contrat moyennant le respect du délai du préavis (entre un jour et une semaine).</p>
<h4 class="spip">La fin du contrat de travail</h4>
<p>L’employeur comme l’employé peut mettre fin au contrat de travail à tout moment, sans motif, en moyennant le respect d`un délai de préavis pouvant varier d’une journée à quatre semaines, selon l’ancienneté de l’employé. Le préavis doit être notifié par écrit. L`employeur peut dispenser l’employé de son préavis mais doit lui verser son salaire pendant cette période.</p>
<h4 class="spip">Les heures de travail</h4>
<p>L’<i>Employment Act </i>prévoit qu’un salarié ne peut pas travailler plus de huit heures par jour et quarante quatre heures par semaine (les salariés locaux travaillent en général le samedi matin). Les entreprises françaises implantées à Singapour ne travaillent généralement pas le week-end.</p>
<p>Toute heure de travail supplémentaire doit être payée au minimum une fois et demi le taux horaire ordinaire. Outre les onze jours fériés singapouriens, les employés ont droit à une journée de repos par semaine. Lorsqu’un jour férié tombe un dimanche, il est en principe reporté au lendemain.</p>
<h4 class="spip">Les congés payés</h4>
<p>Le nombre de jours de congés payés accordés à l’employé dépend de son ancienneté dans l’entreprise. Le minimum légal de congés payés en vigueur est de sept jours pour un contrat de six mois, ou 14 jours sur un an + les 10 jours fériés en vigueur. Souvent, les sociétés internationales proposent entre 16 et 21 jours de congés par an.</p>
<h4 class="spip">Le congé maladie</h4>
<p>L’employé dispose de quatorze jours de congé maladie par an (soixante jours en cas d’hospitalisation) s’il a travaillé dans l’entreprise plus de six mois. Il doit présenter un certificat médical et informer l’employeur dans les 48 heures.</p>
<h4 class="spip">Le congé maternité</h4>
<p>Il est de huit semaines si l’employée a travaillé au moins six mois dans la société avant l`accouchement. Il est maintenant prolongé de quatre semaines supplémentaires à prendre dans les 6 mois suivant l’accouchement.</p>
<h3 class="spip"><a id="sommaire_2"></a>Contrat de travail – spécificités</h3>
<p>La législation du travail à Singapour est très souple et peu contraignante pour l’employeur. Les parties au contrat de travail sont libres de régir leurs relations contractuelles comme elles le souhaitent. En principe, les dispositions relatives au salarié ne peuvent pas être moins favorables que celles prévues dans la loi (<i>Employment Act</i>).</p>
<p>D’autres textes touchant au droit du travail trouvent s’appliquent :</p>
<ul class="spip">
<li>le <i>Central Provident Fund Act</i> (cotisations sociales),</li>
<li>le <i>Workmen’s Compensation Act</i> (ouvriers dont le salaire mensuel est inférieur à 1600 SGD),</li>
<li>le <i>Trade Union Act</i> (droit syndical),</li>
<li>le <i>Children Development Co-Savings Act</i> (congés maternité)</li>
<li>le <i>Workplace Safety and Health Act</i> (hygiène et sécurité au travail).</li></ul>
<p>La plupart des contrats sont à durée indéterminée avec renouvellement tacite toutes les années. Très peu d’emplois se présentent sous la forme d’un contrat à durée temporaire.</p>
<h4 class="spip">Cotisations sociales</h4>
<p>La protection sociale à Singapour repose sur le <strong>Central Provident Fund (CPF). </strong>Il s’agit d’un système de capitalisation individuelle obligatoire pour les singapouriens et non ouvert aux étrangers non résidents permanents. Employeurs et employés cotisent.</p>
<p>Initialement destiné à financer la retraite, il est maintenant partiellement utilisable pour financer l’achat d’un logement, les frais d’hospitalisation, etc.</p>
<p>Le <i>Supplementary Retirement Scheme</i> (SRS) est un autre régime non-obligatoire est particulièrement destiné aux non-Singapouriens et aux « <i>non PR »</i>. Les employeurs et les employés peuvent cotiser.</p>
<h4 class="spip">Recrutement de personnel de maison</h4>
<h5 class="spip">Pour une employée locale singapourienne</h5>
<p>Vous avez plusieurs possibilités :</p>
<ul class="spip">
<li>En « part time » de 4 a 20h par semaine</li>
<li>En « full time » plus de 20 h par semaine</li></ul>
<p>Une singapourienne qui travaille plus de 12h par semaine doit cotiser au CPF ainsi que son employeur. Le salaire varie autour de 12 SGD de l’heure. Il faut éventuellement ajouter le transport, la nourriture et un bonus de fin d’année.</p>
<h5 class="spip">Pour une employée étrangère à demeure</h5>
<p>Le recrutement peut se faire de plusieurs façons :</p>
<ul class="spip">
<li>Par transfert d’une autre famille ;</li>
<li>Par consultation des petites annonces du « Straits Times » et des panneaux d’affichage des supermarchés ;</li>
<li>Par l’intermédiaire d’une agence qui propose des employés de maison travaillant déjà à Singapour ou une employée de maison qu’elle fera venir de son pays d’origine.</li></ul>
<p>Le salaire est de 300 SGD à 600 SGD et plus selon l’expérience de la personne.</p>
<p>L’agence se charge de toutes les démarches administratives, du recrutement, d’un remplacement éventuel en cas de problème. Il est indispensable de choisir une « Licensed Agency ».</p>
<h4 class="spip">Démarches administratives</h4>
<p>Il est possible, à condition qu’il s’agisse d’un transfert, d’effectuer ces démarches soi-même. Il faut alors retirer un dossier « Application for a work permit for a foreign domestic worker » auprès du <a href="http://www.mom.gov.sg/" class="spip_out" rel="external">Ministry of Manpower</a></p>
<h4 class="spip">Responsabilité</h4>
<p>Dans la cité-Etat de Singapour, un foyer sur sept emploie une ou plusieurs employées de maison ; on en compte ainsi 170 000, des femmes venues pour la plupart de pays tels que les Philippines, l’Indonésie ou l’Inde. Ces immigrées sont assujetties à des règlements gouvernementaux draconiens. L’employeur est civilement responsable de son employée. Au regard de la loi singapourienne, les employées étrangères n’ont pas le droit de se marier avec un singapourien et sont interdites de maternité.</p>
<h3 class="spip"><a id="sommaire_3"></a>Fêtes légales</h3>
<p><strong>Lorsqu’un jour férié tombe un dimanche, le lundi qui suit est considéré comme chômé.</strong></p>
<ul class="spip">
<li>New Year’s day (1er janvier) : jour de l’an</li>
<li>Chinese New Year (deux jours) (janv-fev) : Nouvel An Chinois. C’est l’une des principales fêtes chinoises. Elle se situe le premier jour de la première lune et a toujours lieu entre le 21 janvier et le 20 février. Le quartier chinois et les maisons chinoises sont décorés de rouge et or. Les chinois échangent des « Hong Bao », enveloppes rouges porte-bonheur, contenant quelques billets de banque neufs, rouges de préférence. C’est le seule période de l’année où les magasins ferment pour deux jours ou plus.</li>
<li>Hari Raya Haji (date variable) : cette fête a lieu le 10ème jour du 12ème mois du calendrier musulman et marque le pélerinage à la Mecque. Dans les mosquées, on sacrifie des animaux selon le rituel musulman avant de les distribuer aux pauvres.</li>
<li>Good Friday (avril) : Vendredi Saint</li>
<li>Labour Day (1er mai) : Fête du travail</li>
<li><strong>Vesak Day</strong> (mai) : Anniversaire du 3ème Prince. La communauté bouddhiste fête la naissance, l’illumination et l’entrée en « Nirvana » de Bouddha. Les moines dans leur robe jaune safran chantent des « sutras » (cantiques), des oiseaux sont libérés de leurs cages et une procession aux flambeaux marque la fin de la cérémonie.</li>
<li><strong>National Day</strong> (9 août) : Fête Nationale de Singapour. Cette fête célèbre l’indépendance de Singapour en 1965 et donne lieu à des parades.</li>
<li><strong>Deepavali</strong> (oct-nov) : Fête des lumières. Elle est célébrée à la fois par les Hindous et les Sikhs. Elle marque le triomphe de la lumière sur l’obscurité, de la sagesse sur l’ignorance, du bien sur le mal.</li>
<li><strong>Hari Raya Puasa</strong> (fin d’année) : ce jour marque la fin du Ramadan pour les musulmans. Vêtus de leurs plus beaux habits, ils vont rendre visite à leur famille et leurs amis.</li>
<li><strong>Christmas Day</strong> (25 décembre) : Noël</li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Emploi du conjoint</h3>
<p>Les détenteurs du <i>P Pass</i> peuvent faire venir leur conjoint (couple marié) et enfants de moins de 21 ans sous le statut du <i>Dependant Pass</i>.</p>
<p>Le conjoint non marié peut venir à Singapour grâce au <i>Long Term Social Visit Pass</i>. La durée de validité de ces derniers est calquée sur celle de l’<i>Employment</i><i> Pass</i> du conjoint.</p>
<p>Les détenteurs de <i>S Pass</i> dont le salaire dépasse 4000 SGD peuvent faire venir conjoints et enfants de moins de 21 ans sous le statut du <i>Dependant Pass</i>.</p>
<p>L’<i>Entrepass</i> permet à l’entrepreneur de sortir et rentrer librement sur le territoire singapourien pendant toute la validité du <i>Pass</i>. L’entrepreneur a également la possibilité de faire bénéficier du droit de séjour sur le territoire à sa famille directe, à partir du moment où il débute son activité à Singapour.</p>
<p>Les conjoints des titulaires d’un <i>Employment Pass</i> qui disposent eux-mêmes d’un <i>Dependant Pass</i> peuvent obtenir une autorisation de travailler appelée <i>Letter Of Consent</i>. Elle se fait auprès du <i>Singapore Immigration and Registration</i> et est plus facile à obtenir qu’un <i>Employment Pass</i> (elle ne requiert pas de salaire minimum de 2500 SGD). Sa durée de validité dépend de celle du <i>Dependant Pass</i>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/emploi-stage/article/reglementation-du-travail-110490). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
