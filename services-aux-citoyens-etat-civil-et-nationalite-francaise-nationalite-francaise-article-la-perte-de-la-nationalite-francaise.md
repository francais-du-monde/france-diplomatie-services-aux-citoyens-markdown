# La perte de la nationalité française

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/nationalite-francaise/article/la-perte-de-la-nationalite-francaise#sommaire_1">Par déclaration</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/nationalite-francaise/article/la-perte-de-la-nationalite-francaise#sommaire_2">Par décret</a></li></ul>
<p>Elle est le plus souvent volontaire et s’effectue :</p>
<ul class="spip">
<li>par déclaration</li>
<li>par décret</li></ul>
<p>Elle peut néanmoins, dans des cas très particuliers, être involontaire (désuétude, déchéance).</p>
<h3 class="spip"><a id="sommaire_1"></a>Par déclaration</h3>
<p>Des cas de répudiation de la nationalité française sont prévus par le code civil, sous certaines conditions, en faveur, notamment, des enfants nés à l’étranger d’un seul parent français ou nés en France d’un seul parent né en France. Par ailleurs, toute personne majeure, résidant habituellement à l’étranger, qui acquiert volontairement une nationalité étrangère peut, sous certaines conditions, perdre la nationalité française par déclaration expresse.</p>
<p>En cas de mariage avec un étranger, le conjoint français peut également répudier la nationalité française, à condition d’avoir acquis la nationalité de son conjoint et que la résidence habituelle du ménage ait été fixée à l’étranger.</p>
<p>Les Français de moins de 35 ans ne peuvent souscrire une déclaration de perte de la nationalité française que s’ils sont en règle avec les obligations du service national.</p>
<h3 class="spip"><a id="sommaire_2"></a>Par décret</h3>
<p>Les personnes qui ne remplissent pas les conditions relatives à la perte de la nationalité française par déclaration peuvent être autorisées par décret souvent dit de « libération des liens d’allégeance » à perdre la qualité de Français, à condition de posséder une nationalité étrangère.</p>
<p><i>Mise à jour : mars 2016</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/nationalite-francaise/article/la-perte-de-la-nationalite-francaise). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
