# Curriculum vitae

<h4 class="spip">Rédaction</h4>
<p>Dans la rédaction du CV, il convient de :</p>
<ul class="spip">
<li>respecter l´ordre chronologique</li>
<li>pour chaque poste, détailler les expériences professionnelles et les responsabilités exercées</li>
<li>préciser les compétences linguistiques.</li></ul>
<p><a href="http://www.diplomatie.gouv.fr/fr/IMG/docx/Modeles_de_CV_cle85d558.docx" class="spip_in" type="application/vnd.openxmlformats-officedocument.wordprocessingml.document">Modèle de CV</a></p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/emploi-stage/article/curriculum-vitae). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
