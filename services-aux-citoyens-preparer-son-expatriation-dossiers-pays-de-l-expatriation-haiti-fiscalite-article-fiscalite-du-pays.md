# Fiscalité du pays

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/fiscalite/article/fiscalite-du-pays#sommaire_1">Impôt sur le revenu</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/fiscalite/article/fiscalite-du-pays#sommaire_2">Inscription auprès du fisc</a></li></ul>
<p>Le système d’imposition haïtien est proche du système français. On y retrouve les principaux impôts, dont l’impôt sur le revenu.</p>
<h3 class="spip"><a id="sommaire_1"></a>Impôt sur le revenu</h3>
<p>Il existe en Haïti un impôt sur le revenu, désigné sous le nom d’<strong>impôt sur le revenu</strong> pour les personnes physiques et d’<strong>impôt sur les sociétés</strong> pour les sociétés et autres personnes morales.</p>
<p><strong>Personnes imposables</strong> :</p>
<ul class="spip">
<li>toutes personnes physiques ayant leur domicile fiscal en Haïti, en raison de l’ensemble de leurs revenus ;</li>
<li>toutes personnes physiques dont le domicile fiscal est situé hors d’Haïti, en raison de leurs revenus de source haïtienne ;</li>
<li>toutes personnes physiques de nationalité haïtienne ou étrangère qui recueillent les bénéfices ou revenus personnels dont l’imposition est attribuée à Haïti par une convention internationale relative aux doubles impositions.</li></ul>
<p>Sont considérés comme ayant leur <strong>domicile fiscal</strong> en Haïti :</p>
<ul class="spip">
<li>les personnes qui ont en Haïti leur foyer ou qui y séjournent pendant plus 183 jours au cours d’une année d’imposition ;</li>
<li>celles qui exercent en Haïti une activité professionnelle salariée ou non ;</li>
<li>celles qui ont en Haïti le centre de leurs intérêts économiques ;</li>
<li>les agents de l’Etat qui exercent leur fonction ou sont chargés de mission dans un pays étranger et qui ne sont pas soumis dans ce pays a un impôt personnel sur l’ensemble de leurs revenus.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Inscription auprès du fisc</h3>
<p>Il est conseillé de prendre contact avec l’administration fiscale haïtienne pour déterminer si vous êtes imposable en Haïti ou non :</p>
<p>Directeur des Opérations – 37016025 <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/fiscalite/article/fiscalite-du-pays#chescof68#mc#yahoo.fr#" title="chescof68..åt..yahoo.fr" onclick="location.href=mc_lancerlien('chescof68','yahoo.fr'); return false;" class="spip_mail">courriel</a></p>
<p><i>Mise à jour : août 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/fiscalite/article/fiscalite-du-pays). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
