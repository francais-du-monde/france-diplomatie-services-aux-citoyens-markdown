# Egypte

<p>Au 31 décembre 2014, <strong>5905 personnes</strong> étaient inscrites au registre des Français établis hors de France.</p>
<p>La population active, essentiellement composée de cadres et de professions intellectuelles, travaille surtout dans le secteur tertiaire. La communauté française réside principalement dans les deux gouvernorats du Caire et d’Alexandrie.</p>
<p>95 entreprises françaises, employant environ 40 000 salariés, sont recensées dans le pays. Cette présence est surtout concentrée dans les services (télécommunications, banque, assurances, tourisme, distribution, nettoyage et traitement de déchets). Les investissements français concernent également les secteurs industriels (ciment, gaz industriels, pharmacie, chimie, équipement électrique, matériaux de construction) et les filières agroalimentaires.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/egypte/" class="spip_in">Une description de l’Egypte, de sa situation politique et économique</a></li>
<li><a href="http://www.diplomatie.gouv.fr/fr/dossiers-pays/egypte/" class="spip_in">Des informations actualisées sur les conditions locales de sécurité en Egypte</a></li>
<li><a href="http://www.ambafrance-eg.org/" class="spip_out" rel="external">Ambassade de France en Egypte</a></li></ul>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-egypte-entree-et-sejour-23154.md">Entrée et séjour</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-egypte-entree-et-sejour-23154-article-passeport-visa-permis-de-travail-112182.md">Passeport, visa, permis de travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-egypte-entree-et-sejour-23154-article-demenagement-112183.md">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-egypte-entree-et-sejour-23154-article-vaccination-112184.md">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-egypte-entree-et-sejour-23154-article-animaux-domestiques-112185.md">Animaux domestiques</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-egypte-emploi-stage.md">Emploi, stage</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-egypte-emploi-stage-article-marche-du-travail-112186.md">Marché du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-egypte-emploi-stage-article-reglementation-du-travail-112187.md">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-egypte-emploi-stage-article-recherche-d-emploi.md">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-egypte-emploi-stage-article-curriculum-vitae-112189.md">Curriculum vitae</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-egypte-emploi-stage-article-stages.md">Stages</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-egypte-protection-sociale-23156.md">Protection sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-egypte-protection-sociale-23156-article-regime-local-de-securite-sociale-112194.md">Régime local de sécurité sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-egypte-protection-sociale-23156-article-convention-de-securite-sociale-112195.md">Convention de sécurité sociale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-egypte-fiscalite.md">Fiscalité</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-egypte-fiscalite-article-fiscalite-du-pays.md">Fiscalité du pays</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-egypte-fiscalite-article-convention-fiscale-112197.md">Convention fiscale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-egypte-vie-pratique.md">Vie pratique</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-egypte-vie-pratique-article-logement.md">Logement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-egypte-vie-pratique-article-sante.md">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-egypte-vie-pratique-article-scolarisation-112200.md">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-egypte-vie-pratique-article-cout-de-la-vie-112201.md">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-egypte-vie-pratique-article-transports.md">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-egypte-vie-pratique-article-communications-112203.md">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-egypte-vie-pratique-article-loisirs-et-culture.md">Loisirs et culture</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-egypte-presence-francaise.md">Présence française</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-egypte-presence-francaise-article-presence-francaise.md">Présence française</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
