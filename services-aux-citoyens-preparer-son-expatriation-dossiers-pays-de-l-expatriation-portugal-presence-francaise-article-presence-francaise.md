# Présence française

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/presence-francaise/article/presence-francaise#sommaire_1">Ambassade et consulat de France</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/presence-francaise/article/presence-francaise#sommaire_2">Autorités françaises dans le pays</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/presence-francaise/article/presence-francaise#sommaire_3">Associations dans le pays</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Ambassade et consulat de France</h3>
<p>Consulter notre <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/article/annuaire-des-representations-francaises-a-l-etranger" class="spip_in">annuaire des ambassades et consulats de France à l’étranger</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Autorités françaises dans le pays</h3>
<h4 class="spip">Réseau français de coopération et d’action culturelle</h4>
<p>Les coordonnées des services du réseau de coopération et d’action culturelle français au Portugal, ainsi que la description de l’activité de ces services se trouvent dans la rubrique Présence française/Acteurs culturels du site Internet de l’Ambassade ou sur le site Internet du réseau culturel : <a href="http://latitudefrance.diplomatie.gouv.fr/AnnuaireCarto.html" class="spip_out" rel="external">Latitude France</a>.</p>
<h4 class="spip">Economie : réseau mondial</h4>
<p>Business France, l’agence française pour le développement international des entreprises, est présente au Portugal. Sa mission est d’accompagner les entreprises françaises dans leur développement à l’international.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Site du <a href="http://export.businessfrance.fr/portugal/export-portugal-avec-notre-bureau.html" class="spip_out" rel="external">bureau Business France au Portugal</a> </p>
<p>Les services économiques sont également présents. Ils sont une émanation de la Direction générale du Trésor et ont pour missions principales l’analyse macroéconomique et l’animation des relations économiques bilatérales entre la France et les pays où ils sont implantés.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Site internet du <a href="https://www.tresor.economie.gouv.fr/pays/portugal" class="spip_out" rel="external">service économique de l’ambassade de France au Portugal</a> </p>
<h4 class="spip">Vos élus à l’Assemblée des Français de l’étranger</h4>
<p>Les Français établis à l’étranger sont à l’heure actuelle représentés par 155 conseillers à l’Assemblée des Français à l’étranger, 12 sénateurs établis hors de France et, depuis les élections législatives de 2012, 11 députés.</p>
<p>Une réforme de ce système de représentation est prévue par la loi n°2013-659 du 22 juillet 2013 qui vise à créer des conseils consulaires destinés à remplacer l’ensemble des comités consulaires actuels. Les membres de ces comités seront tous élus (élections prévues en mai 2014), en contrepartie de quoi, le nombre de membres de l’Assemblée des Français à l’étranger sera réduit.</p>
<p>Pour toute information sur l’Assemblée des Français de l’étranger et pour connaître les représentants élus dans votre circonscription, vous pouvez consulter les sites Internet suivants :</p>
<ul class="spip">
<li><a href="http://www.assemblee-afe.fr/" class="spip_out" rel="external">Assemblée des Français de l’étranger (AFE)</a></li>
<li>Sénat au service des Français de l’étranger : <a href="http://www2.senat.fr/expatries/dossiers_pays/portugal.html" class="spip_out" rel="external">dossier spécifique au Portugal sur le site du Sénat</a></li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Associations dans le pays</h3>
<p><a href="http://www.francais-du-monde.org/section/portugal/" class="spip_out" rel="external">Association démocratique des Français à l’étranger - Français du monde (ADFE-FdM)</a><br class="manualbr">Président : Mehdi Benlahcen<br class="manualbr">105-2 rua Vicente Borga, 1200-863 Lisbonne<br class="manualbr">Téléphone : 00351 96 28 26 810<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/presence-francaise/article/presence-francaise#contact#mc#fdm-portugal.org#" title="contact..åt..fdm-portugal.org" onclick="location.href=mc_lancerlien('contact','fdm-portugal.org'); return false;" class="spip_mail">Courriel</a></p>
<p><a href="http://www.ufe-portugal.com/" class="spip_out" rel="external">Union des Français de l’étranger (UFE)</a><br class="manualbr">Edificio Institut français du Portugal <br class="manualbr">Avenida Luis Bivar 91 <br class="manualbr">1050-143 Lisbonne<br class="manualbr">Téléphone : 00 351 213 111 455 / 00351 917 432 507<br class="manualbr">[Courriel:mailtoinfo<span class="spancrypt"> [at] </span>ufe-portugal.com]</p>
<p><a href="http://www.lisbonneaccueil.org" class="spip_out" rel="external">Lisbonne Accueil</a><br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/presence-francaise/article/presence-francaise#lisbonne.accueil#mc#gmail.com#" title="lisbonne.accueil..åt..gmail.com" onclick="location.href=mc_lancerlien('lisbonne.accueil','gmail.com'); return false;" class="spip_mail">Courriel</a></p>
<p><a href="http://vivre-a-porto.over-blog.com/" class="spip_out" rel="external">Porto contact FIAFE</a><br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/presence-francaise/article/presence-francaise#vivreaporto#mc#gmail.com#" title="vivreaporto..åt..gmail.com" onclick="location.href=mc_lancerlien('vivreaporto','gmail.com'); return false;" class="spip_mail">Courriel</a></p>
<p>Deux sites vous apporteront des conseils pratiques dans le cadre de votre installation à Lisbonne :</p>
<ul class="spip">
<li><a href="http://www.livinginlisbon.com/" class="spip_out" rel="external">LivingInLisbon</a></li>
<li><a href="http://lisbonneactive.free.fr/index.html" class="spip_out" rel="external">Lisbonneactive</a></li></ul>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.ambafrance-pt.org/Associations" class="spip_out" rel="external">La liste des associations publiée sur le site de l’Ambassade de France au Portugal</a></li>
<li>Notre article dédié aux <a href="services-aux-citoyens-preparer-son-expatriation-en-savoir-plus-article-associations-des-francais-de-l-etranger.md" class="spip_in">Associations des Français de l’étranger</a></li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/presence-francaise/article/presence-francaise). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
