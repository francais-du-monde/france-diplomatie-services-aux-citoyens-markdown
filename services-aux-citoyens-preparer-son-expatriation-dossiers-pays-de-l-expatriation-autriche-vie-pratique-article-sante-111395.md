# Santé

<p>L’état sanitaire de l’Autriche ainsi que son infrastructure médicale sont comparables à ceux des autres pays européens.</p>
<p>Une consultation chez un médecin généraliste coûte 40 à 60 € et davantage chez un spécialiste.</p>
<p>Chaque année, des cas de <strong>méningo-encéphalite virale</strong> (maladie neurologique grave) transmises par les <strong>morsures de tiques</strong> sont rapportés par les autorités sanitaires locales. Plusieurs cas, dont certains mortels, sont signalés chaque année malgré la campagne de sensibilisation de la population locale et touristique menée systématiquement au printemps.</p>
<p><strong>La vaccination contre cette maladie est fortement recommandée</strong>, en particulier pour tout déplacement en zone forestière. Prenez l’avis de votre médecin ou d’un centre de vaccinations internationales.</p>
<p><strong>En cas de court séjour, se munir de la carte européenne d’assurance maladie à demander à votre caisse d’assurance maladie au moins 15 jours avant le départ.</strong> Pour plus d’information, consultez le site de l’<a href="http://www.ameli.fr/assures/droits-et-demarches/a-l-etranger/index.php" class="spip_out" rel="external">assurance maladie</a> en ligne.</p>
<p><strong>Consulter le médecin traitant avant le départ et contracter une assurance de rapatriement sanitaire.</strong></p>
<p>Des coordonnées de médecins francophones sont disponibles sur le site du <a href="http://www.ambafrance-at.org/spip.php?rubrique278" class="spip_out" rel="external">consulat de France</a> à Vienne.</p>
<p><i>Mise à jour : février 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/vie-pratique/article/sante-111395). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
