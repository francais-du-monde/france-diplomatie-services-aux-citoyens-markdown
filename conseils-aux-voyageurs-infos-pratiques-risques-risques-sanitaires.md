# Risques sanitaires

<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="conseils-aux-voyageurs-infos-pratiques-risques-risques-sanitaires-article-rage-voyage-a-l-etranger-avec-un-animal-de-compagnie.md" title="Rage - voyage à l’étranger avec un animal de compagnie ">Rage - voyage à l’étranger avec un animal de compagnie </a></li>
<li><a href="conseils-aux-voyageurs-infos-pratiques-risques-risques-sanitaires-article-coronavirus-mers.md" title="Coronavirus MERS">Coronavirus MERS</a></li>
<li><a href="conseils-aux-voyageurs-infos-pratiques-risques-risques-sanitaires-article-chikungunya-dengue-zika.md" title="CHIKUNGUNYA, DENGUE, ZIKA">CHIKUNGUNYA, DENGUE, ZIKA</a></li>
<li><a href="conseils-aux-voyageurs-infos-pratiques-risques-risques-sanitaires-article-sante-vaccinations.md" title="Santé / Vaccinations">Santé / Vaccinations</a></li>
<li><a href="conseils-aux-voyageurs-infos-pratiques-risques-risques-sanitaires-article-grippe-a-h1n1-93144.md" title="Grippe A (H1N1)">Grippe A (H1N1)</a></li>
<li><a href="conseils-aux-voyageurs-infos-pratiques-risques-risques-sanitaires-article-grippe-aviaire-93142.md" title="Grippe aviaire">Grippe aviaire</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/infos-pratiques/risques/risques-sanitaires/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
