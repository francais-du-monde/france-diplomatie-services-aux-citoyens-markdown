# Espagne

<p>Au <strong>31 décembre 2014</strong>, <strong>90 610 Français </strong>étaient enregistrés auprès des consulats français présents en Espagne.</p>
<p>Si l’on ajoute environ 60 000 personnes non enregistrées, on peut estimer la communauté française en Espagne à près de 150 000 personnes, soit la quatrième après celle des Etats-Unis, du Royaume-Uni et de l’Allemagne. Cette population est répartie de la manière suivante :</p>
<ul class="spip">
<li>circonscription consulaire de Madrid : 39 479 enregistrés ;</li>
<li>circonscription consulaire de Barcelone : 35 445 enregistrés ;</li>
<li>circonscription consulaire de Séville : 10 015 enregistrés ;</li>
<li>circonscription consulaire de Bilbao : 5 671 enregistrés.</li></ul>
<p>La communauté française, dont la proportion de double-nationaux est en légère diminution, a évolué au cours des dernières années et connaît un net rajeunissement (la moyenne d’âge est de 34 ans). La structure socio-professionnelle s’est modifiée au profit des cadres et des professions libérales, notamment dans la région de Madrid.</p>
<p>De nombreuses sociétés françaises sont implantées en Espagne, dont la majeure partie en Catalogne.</p>
<p>Les entreprises françaises sont présentes dans tous les grands secteurs de l’économie : banques et assurances, constructeurs automobiles et leurs fournisseurs, grande distribution, agro-alimentaire, chimie, télécommunications.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/dossiers-pays/espagne/" class="spip_in">Une description de l’Espagne, de sa situation politique et économique</a></li>
<li><a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/espagne/" class="spip_in">Des informations actualisées sur les conditions locales de sécurité en Espagne</a></li>
<li><a href="http://www.ambafrance-es.org/" class="spip_out" rel="external">Ambassade de France en Espagne</a></li></ul>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-espagne-entree-et-sejour-23173.md">Entrée et séjour</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-espagne-entree-et-sejour-23173-article-passeport-visa-permis-de-travail-112447.md">Passeport, visa, permis de travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-espagne-entree-et-sejour-23173-article-demenagement-112448.md">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-espagne-entree-et-sejour-23173-article-vaccination-112449.md">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-espagne-entree-et-sejour-23173-article-animaux-domestiques-112450.md">Animaux domestiques</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-espagne-emploi-stage.md">Emploi, stage</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-espagne-emploi-stage-article-marche-du-travail.md">Marché du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-espagne-emploi-stage-article-reglementation-du-travail-112452.md">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-espagne-emploi-stage-article-recherche-d-emploi.md">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-espagne-emploi-stage-article-curriculum-vitae.md">Curriculum vitae</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-espagne-emploi-stage-article-lettre-de-motivation.md">Lettre de motivation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-espagne-emploi-stage-article-entretien-d-embauche-112456.md">Entretien d’embauche</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-espagne-emploi-stage-article-stages.md">Stages</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-espagne-protection-sociale-23175.md">Protection sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-espagne-protection-sociale-23175-article-regime-local-de-securite-sociale-112458.md">Régime local de sécurité sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-espagne-protection-sociale-23175-article-convention-de-securite-sociale-112459.md">Convention de sécurité sociale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-espagne-fiscalite.md">Fiscalité</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-espagne-fiscalite-article-fiscalite-du-pays.md">Fiscalité du pays</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-espagne-fiscalite-article-convention-fiscale.md">Convention fiscale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-espagne-vie-pratique.md">Vie pratique</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-espagne-vie-pratique-article-logement-112463.md">Logement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-espagne-vie-pratique-article-sante.md">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-espagne-vie-pratique-article-scolarisation-112465.md">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-espagne-vie-pratique-article-cout-de-la-vie-112466.md">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-espagne-vie-pratique-article-transports.md">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-espagne-vie-pratique-article-communications-112468.md">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-espagne-vie-pratique-article-loisirs-et-culture.md">Loisirs et culture</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-espagne-presence-francaise.md">Présence française</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-espagne-presence-francaise-article-presence-francaise.md">Présence française</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
