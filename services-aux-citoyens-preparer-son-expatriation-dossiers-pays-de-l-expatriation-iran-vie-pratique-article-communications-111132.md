# Communications

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/vie-pratique/article/communications-111132#sommaire_1">Téléphone – Internet</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/vie-pratique/article/communications-111132#sommaire_2">Télévision</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/vie-pratique/article/communications-111132#sommaire_3">Poste</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Téléphone – Internet</h3>
<p>Les liaisons téléphoniques sont moyennes. Les opérateurs iraniens ne sont pas toujours à jour de leurs paiements aux opérateurs internationaux, de ce fait les volumes disponible sont réduits, et il est parfois difficile de passer un appel vers l’étranger ou de se faire appeler.</p>
<p>Depuis la France le code du pays est le 98, pour Téhéran, le 21 (précédé du ‘0’ depuis une autre province iranienne ou un portable). Le coût d’une minute de téléphone pour Paris est de 0,51 euro environ.</p>
<p>Avec environ 8 millions d’abonnés, le réseau des télécommunications est relativement satisfaisant à Téhéran et dans les grandes villes. C’est l’un des secteurs où le développement a été le plus marquant. Introduit à Téhéran en 1993 le réseau de téléphone mobile s’étend petit à petit au pays. Tous les hôtels internationaux offrent la possibilité d’envoyer des fax.</p>
<p>L’accès à internet est très restreint. De nombreux sites sont interdits d’accès. L’utilisation de protocoles VPN n’est pas toujours possible. L’internet mobile est quasiment inexistant.</p>
<h3 class="spip"><a id="sommaire_2"></a>Télévision</h3>
<p>L’accès à la télévision par satellite est très variable. Une forte proportion de la population possède une antenne satellite, ce qui en principe est interdit, et regarde les chaînes iraniennes basées à l’étranger ou les chaînes étrangères. Dans le même temps les autorités organisent un brouillage souvent puissant (mais pas constant) des liaisons satellites, qui vise certaines chaînes mais déborde largement sur les fréquences voisines.</p>
<h3 class="spip"><a id="sommaire_3"></a>Poste</h3>
<p>Les délais postaux sont variables : de une semaine à un mois, avec des garanties de réception incertaines pour les colis.</p>
<p>Les timbres s’achètent uniquement dans les bureaux de poste.</p>
<p><i>Mise à jour : février 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/vie-pratique/article/communications-111132). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
