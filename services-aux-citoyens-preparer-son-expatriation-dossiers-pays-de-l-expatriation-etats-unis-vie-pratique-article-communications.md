# Communications

<p><strong>Téléphone – Internet</strong></p>
<p>Les liaisons téléphoniques sont extrêmement aisées et peu coûteuses. Elles sont automatiques avec la France. Il est possible de téléphoner un télégramme par l’intermédiaire d’une compagnie privée telle que la Western Union ou d’en remettre le texte, y compris en français, à l’un de ses bureaux.</p>
<p><strong>Indicatifs</strong></p>
<ul class="spip">
<li><strong>Depuis la France vers les Etats-Unis</strong> : 00 1 + indicatif régional de l’Etat (3 chiffres) + le numéro d’abonné (7 chiffres) ;</li>
<li><strong>Depuis les Etats-Unis vers la France</strong> : 011 33 + le numéro d’abonné sans le "0" local (soit 9 chiffres). Pour établir un appel en PCV (<i>collect call</i>) vers la France, il est possible de joindre un opérateur français par le biais du service France Direct en composant :</li></ul>
<ul class="spip">
<li>sur le réseau ATT : 1-800-225-5288</li>
<li>sur le réseau Sprint : 1 800 4 737 262 ;</li>
<li>sur le réseau MCI : 1-800-888-8000Vous pouvez également obtenir un PCV en tapant "1-800-COLLECT", ou par l’intermédiaire d’un opérateur en composant le 0, puis l’indicatif de la région et enfin le numéro de votre correspondant. Un opérateur vous répondra et vous n’aurez qu’à préciser que vous désirez un <i>collect call</i> .</li></ul>
<p>Les liaisons internet avec la France sont excellentes. Les services du Web sont proposés par tous les câbles opérateurs, les compagnies de télévision et de téléphone.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.tv5.org/cms/chaine-francophone/voyageurs/Outils/p-6716-Indicatifs-telephoniques.htm" class="spip_out" rel="external">TV5 Monde</a> </p>
<p><strong>Téléphoner gratuitement par Internet </strong></p>
<p>Il est généralement très avantageux d’utiliser <strong>des logiciels de téléphonie sur IP</strong> (Skype, Google talk, Live messenger, Yahoo ! messenger, etc.) car les tarifs proposés à l’international sont souvent moindre que ceux des opérateurs locaux exploitant des lignes classiques, voire gratuits dans certaines conditions.</p>
<p><strong>Poste</strong></p>
<p>Les lettres, journaux et colis sont acheminés vers la France dans un délai moyen de quatre à sept jours (par avion).</p>
<p>Il est possible de domicilier son courrier en poste restante dans le bureau de poste de son choix, en faisant libeller l’adresse d’expédition selon le format suivant :</p>
<p>Nom du destinataire<br class="manualbr">c/o General Delivery<br class="manualbr">Main Post Office<br class="manualbr">Nom de la ville, Etat, code postal</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Le site du <a href="http://www.usps.com/" class="spip_out" rel="external">service postal américain</a> (<i>The United States Postal Service</i>). </p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/vie-pratique/article/communications). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
