# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/cambodge/vie-pratique/article/transports#sommaire_1">Permis de conduire</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/cambodge/vie-pratique/article/transports#sommaire_2">Code de la route</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/cambodge/vie-pratique/article/transports#sommaire_3">Assurances et taxes</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/cambodge/vie-pratique/article/transports#sommaire_4">Achat et location</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/cambodge/vie-pratique/article/transports#sommaire_5">Entretien</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/cambodge/vie-pratique/article/transports#sommaire_6">Réseau routier</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/cambodge/vie-pratique/article/transports#sommaire_7">Transports en commun</a></li></ul>
<p>En règle générale, éviter de circuler de nuit.</p>
<p>En cas d’accident, il est indispensable d’être assuré et les amendes immédiates peuvent être très élevées. Numéro d’appel d’urgence du Samu local : 119.</p>
<h3 class="spip"><a id="sommaire_1"></a>Permis de conduire</h3>
<p>Le permis international et le permis français sont reconnus. La demande de permis local s’effectue auprès du Ministère des Transports ; un délai de 15 jours est à prévoir.</p>
<p>Le permis est payant environ 30 USD.</p>
<h3 class="spip"><a id="sommaire_2"></a>Code de la route</h3>
<p>La conduite et la priorité sont à droite. La vitesse est limitée à 50 km/h en ville ; hors agglomération, il n’y a pas de limitation autre que l’état des routes. Dans la réalité, la conduite au Cambodge demeure très dangereuse.</p>
<h3 class="spip"><a id="sommaire_3"></a>Assurances et taxes</h3>
<p>Les assurances sont désormais obligatoires depuis 2001. Une assurance aux tiers simple coûte environ 200 USD pour un véhicule de catégorie moyenne.</p>
<h3 class="spip"><a id="sommaire_4"></a>Achat et location</h3>
<p>Les marques de véhicules représentées sur place sont relativement nombreuses : Peugeot, Mercedes, Suzuki, Toyota, Honda, Land Rover… Le 4x4 climatisé est conseillé. On peut acheter sur place un véhicule neuf avec un délai de trois à quatre mois si le modèle n’est pas disponible. Les véhicules avec volant à droite sont interdits. Les prix des véhicules sont de 25 000 à 40 000 EUROS (environ 30 à 40 % plus cher que les prix français).</p>
<p>Grand choix de voitures d’occasion à des prix très abordables à partir de 5000 euros.</p>
<p>Les véhicules de location (de l’ordre de 25 USD par jour avec chauffeur) sont vétustes et l’importation d’un véhicule personnel est très onéreuse en regard de son prix local.</p>
<h3 class="spip"><a id="sommaire_5"></a>Entretien</h3>
<p>L’entretien et les réparations peuvent être effectuées à bon marché mais avec une qualité médiocre. Se procurer des pièces détachées de voitures de marque française (par l’intermédiaire du concessionnaire) demande quelques semaines.</p>
<h3 class="spip"><a id="sommaire_6"></a>Réseau routier</h3>
<p>Dans la capitale, la circulation est désordonnée. Hors des villes, les routes et ponts sont souvent en mauvais état (et parfois coupés durant la saison des pluies). Les dépanneurs et les postes à essence sont rares (il est préférable de circuler à deux véhicules). Les taxis collectifs et les bus locaux sont souvent en mauvais état (hormis les cars climatisés reliant Phnom-Penh à Sihanoukville et Kompong Cham).</p>
<p>L’accroissement du nombre de véhicules a entraîné une augmentation importante de celui d’accidents de la circulation, notamment des accidents graves de moto. Le port du casque par le conducteur de moto comme par le passager, est, dans ces conditions, indispensable.</p>
<h3 class="spip"><a id="sommaire_7"></a>Transports en commun</h3>
<p><strong>Réseau ferroviaire </strong></p>
<p>Il n’y a pas de service ferroviaire au Cambodge.</p>
<p><strong>Réseau fluvial </strong></p>
<p>Les bateaux de transport de passagers entre Phnom-Penh, Siem Reap, Stung Treng et le Viet-Nam ne répondent pas aux normes de sécurité. Le trajet entre Kratie et Stung Treng est dangereux en raison du banditisme. A signaler également la création, sur la ligne Phnom-Penh-Siem Reap, d’une compagnie (Mékong Express) utilisant des bateaux mieux équipés.</p>
<p><strong>Transport maritime</strong></p>
<p>En 1999 et en 2000, des navires de pêche opérant dans les eaux cambodgiennes ont été victimes d’actes de piraterie. En revanche, aucune attaque visant des navires de tourisme ou de plaisance n’a, à ce jour, été signalée.</p>
<p>Les navires locaux qui, à Sihanoukville, Kampot et Kep, proposent aux touristes des excursions vers les îles proches répondent rarement aux normes de sécurité nautique.</p>
<p><strong>Transport aérien</strong></p>
<p>Les deux aéroports internationaux (Phnom-Penh Pochentong et Siem Reap) sont gérés par la S.C.A. (société concessionnaire de l’aéroport), dont l’actionnaire majoritaire est un groupe français. Les autres aéroports sont gérés par l’aviation civile cambodgienne.</p>
<p>Les compagnies aériennes étrangères suivantes desservent actuellement le Cambodge : Bangkok Airways, Thai Airways, Vietnam Airlines, Silk Air, Dragonair, China Southern Airlines, Lao Aviation, Malaysia Airlines, Shanghai Airlines. L’entretien de leurs appareils est assuré hors du Cambodge. Depuis fin mars 2001, Siem Reap Airlines, filiale au Cambodge de Bangkok Airways, assure des vols quotidiens entre Siem Reap et Phnom-Penh (avions ATR 72 entretenus en Thaïlande).</p>
<p>En ce qui concerne les compagnies aériennes locales, "President Airlines" dispose de deux Antonov 24, régulièrement entretenus par le loueur russe suivant les normes de l’aviation civile russe et d’un "Fokker 28" entretenu en Malaisie ou en Indonésie, suivant les normes des aviations civiles de ces deux pays. "Royal Phnom-Penh Airways" (créée en octobre 1999) dispose de deux Antonov 24 qui font l’objet d’un contrat d’entretien avec le loueur russe. Il convient toutefois de signaler que, en septembre 2001, un Antonov 24 de "Royal Phnom-Penh Airways" a subi des dégâts légers en heurtant une antenne lors de son atterrissage à Siem Reap, et qu’à la même période, le Fokker 28 de "President Airlines" a éprouvé des difficultés de sortie de son train d’atterrissage.</p>
<p>Aucun accident ne s’est produit depuis celui du 3 septembre 1997 qui avait causé la mort de 65 personnes (le Tupolev 134 du vol Vietnam Airlines s’était écrasé à Phnom-Penh Pochentong lors de son atterrissage dans des conditions météo difficiles).</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/cambodge/vie-pratique/article/transports). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
