# Animaux domestiques

<p>L’animal voyageant en Thaïlande doit être accompagné d’un certificat sanitaire. Les chiens et les chats ont aussi besoin d’un certificat antirabique. Toute personne désireuse d’importer ou de faire transiter un animal en/ à travers la Thaïlande doit confirmer avant le transport l’itinéraire de vol et la date exacte d’arrivée, ainsi que les détails de l’animal (à savoir la race, le sexe et la quantité).</p>
<p>Pour tout renseignement complémentaire sur les documents à fournir à l’arrivée en Thaïlande, se renseigner directement auprès de l’aéroport Suvarnabhumi :</p>
<p><a href="http://airportthai.co.th/main/en/687-passengers-traveling-with-pets" class="spip_out" rel="external">Suvarnabhumi Airport Animal Quarantine Station</a><br class="manualbr">Tel / Fax : (66) 2134 0731-2 ou (66) 2134 3640</p>
<p>Normalement, si tous vos documents sont en règle et les procédures respectées, il n’y a pas de quarantaine.</p>
<p>Le ministère de Développement de l’élevage de Thaïlande (<i>Bureau of Disease Control and Veterinary Services. </i><i>Department of Livestock Development Phayathai road, Ratchtavee Bangkok 10400</i>) a interdit l’importation des animaux suivants : pitbull terrier et american staffordshire terrier.</p>
<p>L’exportation de tout animal est autorisée uniquement pour les détenteurs d’une licence d’exportation et d’un certificat sanitaire officiel, qui peut être obtenue auprès de l’office vétérinaire de l’aéroport de Bangkok. Les animaux peuvent voyager en cabine ou en soute.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Notre article thématique <a href="services-aux-citoyens-preparer-son-expatriation-douanes-article-animaux-domestiques.md" class="spip_in">Animaux domestiques</a></p>
<p><i>Mise à jour : juillet 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/entree-et-sejour/article/animaux-domestiques-114427). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
