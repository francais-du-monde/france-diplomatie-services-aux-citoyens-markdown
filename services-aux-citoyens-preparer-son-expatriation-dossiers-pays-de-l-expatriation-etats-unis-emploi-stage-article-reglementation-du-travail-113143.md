# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/emploi-stage/article/reglementation-du-travail-113143#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/emploi-stage/article/reglementation-du-travail-113143#sommaire_2">Contrat de travail – spécificités</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/emploi-stage/article/reglementation-du-travail-113143#sommaire_3">Fêtes légales</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/emploi-stage/article/reglementation-du-travail-113143#sommaire_4">Emploi du conjoint</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/emploi-stage/article/reglementation-du-travail-113143#sommaire_5">Création d’entreprises – spécificités</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<p><strong>Durée légale du travail</strong></p>
<p>Dans la législation américaine il n’existe aucune limite du nombre d’heure de travail quotidien ou hebdomadaire</p>
<p>Une semaine de travail est en moyenne de 40 heures. Toute heure supplémentaire est rémunérée à un taux 1,5 fois supérieur au salaire standard. Il n’est pas interdit aux entreprises d’adopter des dispositions plus favorables.</p>
<p><strong>Salaire minimum légal</strong></p>
<p>Depuis 2010, le salaire minimum fédéral est de 7,25 dollars/heure. Il est garanti à tous les employés auxquels ne peut être appliqué un salaire inférieur, appelé <i>subminimum</i>.</p>
<p>Chaque Etat est libre de fixer sa propre norme en matière de salaire minimum : il peut être inférieur, supérieur ou égal au salaire minimum fédéral. Le salaire minimum proposé par chaque Etat américain <a href="http://www.dol.gov/whd/minwage/america.htm" class="spip_out" rel="external">peut être consulté en ligne</a>.</p>
<p>Au moment de l’embauche, un employeur annonce un salaire brut (annuel, mensuel ou horaire), c’est-à-dire avant cotisations sociales et impôts.</p>
<p><strong>Congés</strong></p>
<p>Congés annuels :</p>
<p>Même si la législation américaine n’impose pas aux employeurs d’accorder des congés annuels ou des jours fériés chômés, 90% des américains bénéficient de congés payés ou non-payés.</p>
<p>En moyenne, les travailleurs américains ont neuf jours de congés annuels (11 pour les fonctionnaires et huit pour les travailleurs du secteur privé).</p>
<p>Congés pour raisons médicales ou familiales :</p>
<p>Un salarié peut prendre un congé de 12 semaines par an pour des raisons familiales et/ou médicales spécifiques. Ce congé n’est pas payé mais la place du salarié dans l’entreprise est assurée. Il est toujours assuré par son entreprise.</p>
<p>Le salarié peut prendre ce congé dans les cas suivants :</p>
<ul class="spip">
<li>Maternité à partir de la naissance de l’enfant jusqu’à sa première année.</li>
<li>Maladie grave du conjoint, d’un enfant ou d’un parent</li>
<li>Maladie grave du salarié qui l’empêche de remplir ses fonctions</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Contrat de travail – spécificités</h3>
<p>Le contrat de travail peut être fait soit dans le cadre d’un détachement, soit dans le cadre d’une expatriation.</p>
<ul class="spip">
<li>Dans le cadre du détachement, l’entreprise emploie un salarié en France puis le détache à l’étranger, pour une période variable mais limitée, au terme de laquelle l’employé retrouve son poste en France. Ce contrat sera soumis au droit français.</li>
<li>Dans le cadre de l’expatriation, le salarié est recruté soit en France, soit à l’étranger pour le compte d’une entreprise implantée à l’étranger ou d’une société locale. Les conditions de négociation ne seront pas les mêmes selon que l’employeur fait venir le salarié de France ou qu’il l’engage selon les modalités d’un contrat de travail local. Dans le premier cas, si les parties en décident ainsi, <strong>le contrat pourra être soumis au droit français.</strong> S’il s’agit d’un contrat local, les relations de travail seront régies par le droit local. Pour en savoir plus, consultez le <a href="http://www.cleiss.fr/" class="spip_out" rel="external">site du CLEISS</a>.</li></ul>
<p>Il est recommandé de faire examiner le contrat de travail, avant sa signature, par un expert de Pôle emploi à l’international, où un service "Expa Conseil" est spécialement proposé :</p>
<p><a href="http://www.emploi-international.org/" class="spip_out" rel="external">Pôle emploi - Agence Emploi International</a><br class="manualbr">48, boulevard de la Bastille<br class="manualbr">75012 Paris<br class="manualbr">Tél. : 01 53 02 25 50<br class="manualbr">Fax : 01 53 02 25 65 /66 (expa conseil)<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/emploi-stage/article/reglementation-du-travail-113143#pei-expaconseil.75830#mc#pole-emploi.fr#" title="pei-expaconseil.75830..åt..pole-emploi.fr" onclick="location.href=mc_lancerlien('pei-expaconseil.75830','pole-emploi.fr'); return false;" class="spip_mail">Courriel</a></p>
<h4 class="spip">Contrat soumis au droit français</h4>
<p><strong>Clauses obligatoires</strong></p>
<p>Dans tous les cas, il est indispensable que figurent certaines clauses essentielles au contrat de travail :</p>
<ul class="spip">
<li>l’identification et la qualité des parties ;</li>
<li>la législation applicable au contrat et la juridiction compétente en cas de litige ;</li>
<li>la durée du contrat (durée déterminée ou indéterminée, conditions de la reconduction éventuelle) ;</li>
<li>l’existence d’une période d’essai et sa durée ;</li>
<li>le lieu de travail ;</li>
<li>la date du début de l’activité.</li></ul>
<p><strong>Clauses soumises à l’autonomie de la volonté</strong></p>
<p>Certaines clauses sont soumises à l’autonomie de la volonté des parties : c’est à dire que les parties sont libres d’aménager leur contrat à leur guise et de décider du contenu de certaines clauses. Cette volonté des parties a force de loi pourvu qu’elle ne décide pas de conditions d’emploi plus défavorables que celles prévues par la loi en vigueur.</p>
<p>Les parties au contrat sont tenues de déterminer certaines clauses obligatoires, telles que :</p>
<ul class="spip">
<li>la fonction à exercer, l’intitulé du poste, la qualification et la classification du poste ainsi que les liens de subordination ;</li>
<li>le montant de la rémunération, des primes et indemnités, et les modalités de révision de la rémunération ;</li>
<li>le régime de prévoyance, de chômage et de retraite, sans oublier les régimes complémentaires de prévoyance, de retraite ou d’assurance chômage ;</li>
<li>les modalités d’ancienneté, d’avancement, de promotion ou de mutation ;</li>
<li>les modalités de rupture du contrat (préavis, indemnité de rupture), clauses de non-concurrence ;</li>
<li>la durée des congés.</li></ul>
<p>Les parties sont libres d’ajouter d’autres clauses, selon le cas considéré et selon la législation applicable : des clauses déterminant les conditions de séjour, les modalités de départ et de rapatriement.</p>
<p>Selon le poids dont bénéficiera l’employé dans la négociation, il sera éventuellement possible de négocier :</p>
<ul class="spip">
<li>la durée du séjour ;</li>
<li>les frais de voyage (mode de voyage et classe), les frais de rapatriement en fin de séjour, la prise en charge du voyage aller et du voyage retour pour l’intéressé et sa famille, ainsi que des voyages en France à l’occasion des congés et éventuellement en cas de maladie grave de l’intéressé, d’un membre de sa famille ou en cas de décès d’un ascendant direct de l’intéressé ou de son conjoint ;</li>
<li>la prise en charge des dépenses de déménagement à l’aller et au retour (frais de douane, assurances, transport) ;</li>
<li>les indemnités d’installation et de réinstallation ;</li>
<li>le recyclage éventuel au retour en France ;</li>
<li>la répartition de la rémunération entre versements locaux et versements en France ;</li>
<li>les avantages éventuellement accordés (logement, employés de maison, voiture de fonction ou indemnités d’utilisation, etc.) ;</li>
<li>la prise en charge des frais de scolarité des enfants ;</li>
<li>la visite médicale d’aptitude de l’intéressé et de sa famille avant le départ et à chaque congé.</li></ul>
<h4 class="spip">Contrat soumis au droit américain</h4>
<p>Le marché du travail aux Etats-Unis est libéral. Les contrats ne sont pas toujours écrits, mais passent par un accord systématique entre l’employeur et le salarié. Ainsi, même dans le cas d’un accord verbal, le contrat est réputé exister entre les deux parties. On parle alors d’un contrat <i>"At Will"</i>, qui implique notamment que toute rupture de contrat peut intervenir sans formalités, sans motivation, ni préavis, qu’il s’agisse d’une décision à l’initiative de l’employeur ou du travailleur. Toutefois la pratique courante veut que le salarié respecte une période de préavis de deux semaines avant son départ.</p>
<p>Le contrat oral étant soumis à l’interprétation du droit américain, la marge de négociation de l’employé s’en trouve considérablement réduite. Il convient par conséquent, lorsque cela est possible, de solliciter un contrat écrit sur lequel figureront les clauses arrêtées par les parties. Cette relation de travail, encadrée par un contrat écrit, est appelée <i>"Contract Employee"</i>. Elle prévoit notamment l’embauche pour une durée déterminée, garantie au travailleur.</p>
<p>Il est d’usage également de déterminer dans le contrat la base de rémunération ; on distingue ainsi :</p>
<ul class="spip">
<li>une rémunération dite <i>"Hourly Position"</i>, calculée sur une base horaire de quarante heures hebdomadaires et soumise à la réglementation sur les heures supplémentaires ;</li>
<li>une rémunération dite <i>"Salary Position"</i>, fixée de façon forfaitaire, quel que soit le volume horaire effectif ; ce système s’apparente à la notion française du statut de cadre.</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Fêtes légales</h3>
<ul class="spip">
<li>1er janvier : <i>New Year’s Day</i> (Nouvel An) ;</li>
<li>3ème lundi de janvier : <i>Martin Luther King Jr’s Birthday</i> (Commémoration de Martin Luther King) ;</li>
<li>3ème lundi de février : <i>Washington’s Birthday</i> (Jour du Président) ;</li>
<li>dernier lundi de mai : <i>Memorial Day</i> (Journée de la mémoire) ;</li>
<li>4 juillet : <i>Independence Day</i> (Fête nationale) ;</li>
<li>1er lundi de septembre : <i>Labor Day</i> (Fête du travail) ;</li>
<li>2ème lundi d’octobre : <i>Colombus Day</i> (Fête de Christophe Colomb) ;</li>
<li>11 novembre : <i>Veterans Day</i> (Armistice - Jour des vétérans) ;</li>
<li>4ème jeudi de novembre <i>Thanksgiving Day</i> ;</li>
<li>25 décembre : <i>Christmas Day</i> (Noël).</li></ul>
<p>Le jour chômé est reporté à un vendredi ou à un lundi lorsqu’il tombe en milieu de semaine.</p>
<h3 class="spip"><a id="sommaire_4"></a>Emploi du conjoint</h3>
<p>La législation américaine en matière d’immigration encadre très précisément les conditions dans lesquelles un ressortissant étranger est autorisé à travailler.</p>
<p>Par exemple, le conjoint d’une personne recrutée sous visa H1B et détenteur en conséquence d’un visa H2 ne pourra pas travailler.</p>
<p>S’il existe des cas de dérogations délivrées en vue de permettre l’activité professionnelle du conjoint, celles-ci sont limitées et soumises à des critères particuliers.</p>
<p>En tout état de cause, la législation des Etats-Unis <strong>ne reconnaît pas le statut de concubin ou l’union par un PACS</strong> ; il n’est donc pas possible de s’en prévaloir auprès des autorités américaines.</p>
<h3 class="spip"><a id="sommaire_5"></a>Création d’entreprises – spécificités</h3>
<p>La création d’entreprise aux Etats-Unis est plus simple et plus rapide qu’en France. Cela ne veut pourtant pas dire que l’implantation sera réussie.</p>
<p>Il est donc nécessaire de bien se préparer : le consommateur européen étant différent du consommateur américain, définissez votre projet et fixez vos objectifs en faisant au préalable une étude de marché pour éviter les mauvaises surprises.</p>
<p><strong>Les différentes formes juridiques aux Etats-Unis : </strong></p>
<p><i>Sole Proprietor :</i></p>
<ul class="spip">
<li>Vous êtes le seul propriétaire et responsable de votre affaire : votre responsabilité est illimitée et s’étend à la communauté des biens des époux.</li>
<li>Si votre entreprise se développe, vous pourrez changer la forme juridique de celle-ci.</li></ul>
<p><i>General Partnership :</i></p>
<ul class="spip">
<li>Contrat oral ou écrit entre deux associés : chacun encourt une responsabilité personnelle et illimitée en cas de perte.</li>
<li>Le <i>General Partnership </i>ne demande pas de formalité juridique.</li></ul>
<p><i>Limited Partnership :</i></p>
<ul class="spip">
<li>Contrat entre des <i>General Partners</i>et les <i>LimitedParners</i>.</li>
<li>Les <i>Limited partners</i> sont des investisseurs responsables uniquement de leur apport dans l’entreprise</li></ul>
<p>Société (<i>Corporation</i>) :</p>
<p>Il y a deux types de <i>corporation</i> : la <i>C corporation</i> et la <i>S corporation</i>.</p>
<p>La <i>C corporation </i>est celle qui ressemble le plus à la SA française.</p>
<ul class="spip">
<li>La société est alors une personne morale à part entière, responsable de ses dettes.</li>
<li>Il peut y avoir autant d’actionnaires qu’on le souhaite : ils peuvent être des personnes physiques ou morales et de nationalité différentes.</li>
<li>La responsabilité des actionnaires est limitée au montant de leurs investissements.</li></ul>
<p>La <i>S corporation</i> est un dérivé de la <i>C corporation</i></p>
<ul class="spip">
<li>Les actionnaires doivent obligatoirement résider aux Etats-Unis.</li>
<li>Leur nombre est limité à 35</li>
<li>La société ne peut être actionnaire</li>
<li>L’année fiscale correspond à l’année calendaire</li></ul>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li>Le site Internet du <a href="http://www.dol.gov/" class="spip_out" rel="external">ministère américain du Travail</a>  <i>(Department of Labor)</i></li>
<li>Le <i>Bureau of Labor Statistics</i> publie sur internet l’Occupational Outlook <a href="http://www.bls.gov/ooh/" class="spip_out" rel="external">handbook</a> qui fournit un descriptif de tous les aspects de l’emploi et du travail dans 600 métiers</li>
<li><a href="http://www.pole-emploi-international.fr/" class="spip_out" rel="external">Pôle Emploi international</a> <br class="manualbr">Tél. : 01 53 02 25 50<br class="manualbr">Fax : 01 53 02 25 65 /66 (expa conseil)<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/emploi-stage/article/reglementation-du-travail-113143#expaconseil.anaem#mc#pole-emploi.fr#" title="expaconseil.anaem..åt..pole-emploi.fr" onclick="location.href=mc_lancerlien('expaconseil.anaem','pole-emploi.fr'); return false;" class="spip_mail">Courriel Expaconseil</a> et <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/emploi-stage/article/reglementation-du-travail-113143#pei-paris.75830#mc#pole-emploi.fr#" title="pei-paris.75830..åt..pole-emploi.fr" onclick="location.href=mc_lancerlien('pei-paris.75830','pole-emploi.fr'); return false;" class="spip_mail">courriel PEI-Paris</a></li>
<li><a href="http://www.europusa.com/" class="spip_out" rel="external">EuropUsa</a></li></ul>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/emploi-stage/article/reglementation-du-travail-113143). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
