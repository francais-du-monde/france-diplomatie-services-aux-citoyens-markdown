# Stage

<p>Le stage qu’effectue un étranger en Colombie n’est pas considéré comme un « emploi ». Le stagiaire n’est donc pas soumis aux mêmes règles de visa et de travail que les Colombiens, les nouveaux immigrants ou les travailleurs immigrés. Pour effectuer un stage en Colombie un ressortissant étranger doit obtenir un visa d’étudiant stagiaire. Pour obtenir ce visa l’étudiant doit obligatoirement être en mesure de fournir une convention de stage signé entre l’établissement d’origine et l’entreprise qui l’accueillera en Colombie.</p>
<p><strong>Avez-vous pensé au volontariat international ?</strong></p>
<p>Le V.I.E. permet aux entreprises françaises de confier à un jeune ressortissant de l’espace économique européen, âgé de 18 à 28 ans, une mission professionnelle (rémunérée) à l’étranger durant une période modulable de 6 à 24 mois.</p>
<p>C’est UbiFrance, l’agence française pour le développement international des entreprises, qui gère pour l’entreprise les aspects contractuels, la rémunération et la protection sociale du volontaire.</p>
<p>Toutes les entreprises françaises souhaitant développer leurs activités à l’étranger peuvent employer des V.I.E.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/emplois-stages-concours/volontaires-internationaux/" class="spip_in">Volontaires internationaux</a></li>
<li><a href="http://www.civiweb.com" class="spip_out" rel="external">Civiweb</a></li>
<li><a href="http://export.businessfrance.fr/default.html" class="spip_out" rel="external">Business France</a></li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/emploi-stage/article/stage). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
