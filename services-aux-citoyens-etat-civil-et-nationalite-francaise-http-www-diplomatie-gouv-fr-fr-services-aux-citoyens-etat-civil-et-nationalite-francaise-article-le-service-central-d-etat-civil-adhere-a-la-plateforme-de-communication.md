# Le SCEC élargit à nouveau le champ de COMEDEC

<p>Depuis le 1er juin 2016, la vérification de l’état civil des personnes nées à l’étranger pour les demandes de passeport effectuées dans les consulats se fait via la plateforme de COMmunication Electronique des Données de l’Etat Civil. Cette procédure était déjà en vigueur depuis le 1er février pour les personnes nées dans l’Union européenne.</p>
<p>Pratiquement, les usagers de nationalité française nés à l’étranger n’ont plus à produire de copie d’acte de naissance : le poste ou la mairie se chargent de transmettre la demande de vérification de leur état civil directement via la plateforme COMEDEC à laquelle le Service central de l’état civil s’est raccordé voici six mois.</p>
<p>Cette avancée, qui entre dans le cadre de la dématérialisation des services rendus par l’administration, contribuera ainsi à réduire les délais de traitement de certains dossiers pour nos compatriotes résidant à l’étranger.</p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/http-www-diplomatie-gouv-fr-fr-services-aux-citoyens-etat-civil-et-nationalite-francaise-article-le-service-central-d-etat-civil-adhere-a-la-plateforme-de-communication). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
