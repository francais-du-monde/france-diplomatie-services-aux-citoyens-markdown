# Marché du travail

<h4 class="spip">Secteurs à fort potentiel</h4>
<p>Les secteurs traditionnellement générateurs d’emploi sont :</p>
<ul class="spip">
<li>Les services et notamment le secteur touristique ;</li>
<li>Le secteur agricole ;</li>
<li>Le secteur textile.</li></ul>
<p>La nécessité pour les entreprises tunisiennes de se rapprocher des standards internationaux de qualité et de compétitivité devrait profiter aux secteurs suivants :</p>
<ul class="spip">
<li>l’agriculture et la pêche ;</li>
<li>l’industrie chimique dont la plasturgie ;</li>
<li>les cosmétiques ;</li>
<li>l’agroalimentaire ;</li>
<li>l’environnement ;</li>
<li>la sous-traitance électronique et mécanique (essentiellement tournée vers le secteur automobile) ;</li>
<li>l’informatique ;</li>
<li>le secteur pharmaceutique ;</li>
<li>les sociétés de services externalisés (notamment centres d’appels) ;</li>
<li>le conseil en entreprises.</li></ul>
<p>Toutefois, l’expansion des différents secteurs qui composent l’économie tunisienne, si importante soit-elle, n’annule en rien les restrictions à l’emploi de la main d’œuvre étrangère.</p>
<p>La réalité du principe de préférence nationale à l’emploi restreint fortement l’accès à l’emploi pour les étrangers n’étant pas titulaires d’une carte de séjour obtenue en raison d’un lien familial avec un ressortissant tunisien.</p>
<h4 class="spip">Professions règlementées</h4>
<p>Le secteur public tunisien n’est pas accessible pour les étrangers.</p>
<p><i>Mise à jour : novembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/emploi-stage/article/marche-du-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
