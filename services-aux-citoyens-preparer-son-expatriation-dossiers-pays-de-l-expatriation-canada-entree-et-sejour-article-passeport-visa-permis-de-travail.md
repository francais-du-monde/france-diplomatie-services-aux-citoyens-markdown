# Passeport, visa, permis de travail

<p>Quel que soit le motif de votre séjour, renseignez-vous auprès de la section consulaire de l’ambassade du Canada à Paris qui vous informera sur la règlementation en matière d’entrée et de séjour au Canada, règlementation que vous devrez impérativement respecter. N’hésitez pas à consulter le site du ministère de l’Intérieur canadien afin de vous familiariser avec les modalités d’entrée et de séjour dans le pays.</p>
<p><strong>En aucun cas, vous n’êtes autorisé à travailler au Canada sans permis adéquat.</strong></p>
<p>Le Consulat de France au Canada n’est pas l’organisme compétent pour répondre à vos demandes d’information concernant votre séjour au Canada.</p>
<h4 class="spip">Les différents types de visa</h4>
<p>Les ressortissants français n’ont pas besoin de visa de résident temporaire (ou visa de visiteur) pour se rendre au Canada pour un séjour ne dépassant pas six mois.</p>
<p>Vous devez cependant apporter la preuve à l’entrée sur le territoire canadien que vous remplissez les conditions suivantes :</p>
<ul class="spip">
<li>être en possession d’un passeport en règle dont la validité dépasse d’au moins un jour le séjour projeté au Canada ;</li>
<li>avoir un motif légitime pour séjourner temporairement au Canada (tourisme, étude, voyage d’affaires, etc.) ;</li>
<li>être en bonne santé ;</li>
<li>disposer des fonds suffisants pour subvenir à ses besoins pendant le séjour au Canada ou présenter une attestation bancaire indiquant que les fonds sont disponibles et pourront être transférés. Le montant nécessaire varie en fonction des circonstances de votre visite, de la durée de votre séjour et de l’endroit où vous habiterez (à l’hôtel ou chez des parents ou des amis) ;</li>
<li>posséder une réservation pour un billet de retour soit vers son pays de résidence, soit vers un tiers pays ou être en mesure d’acheter ce billet ;</li>
<li>ne pas avoir l’intention de travailler ou d’étudier au Canada à moins d’avoir obtenu l’autorisation avant son arrivée.</li></ul>
<p>La durée du séjour est déterminée par l’agent au point d’entrée. A moins d’indication contraire, le séjour autorisé est de six mois à partir de la date du tampon d’admission. Dans certains cas, la durée autorisée du séjour peut être inférieure ou supérieure à six mois. Dans ce cas, la durée du séjour autorisée sera indiquée à côté du tampon d’admission ou sur un document distinct.</p>
<p>La personne admise comme résident temporaire n’est pas autorisée à changer sur place son statut pour devenir étudiant, travailleur ou résident permanent. Toute personne dont l’intention est d’aller résider au Canada pour une période indéterminée (c’est-à-dire sans date d’expiration) doit au préalable obtenir un visa d’immigrant.</p>
<p><strong>Séjour pour études d’une durée maximale de six mois</strong></p>
<p>Vous n’avez pas besoin de permis d’études si vous envisagez de suivre au Canada un cours ou un programme d’échange universitaire de six mois ou moins.</p>
<p>Pour étudier au Canada, vous devez :</p>
<ul class="spip">
<li>avoir été accepté par une école, un collège, une université ou un autre établissement d’enseignement au Canada ;</li>
<li>prouver que vous avez assez d’argent pour payer :</li>
<li>les frais de scolarité,</li>
<li>les frais de subsistance, pour vous et les membres de votre famille (époux ou conjoint de fait et/ou enfants à charge) qui vous accompagnent au Canada (selon les lignes directrices de Citoyenneté et Immigration Canada, en plus des frais de scolarité, vous devez disposer d’environ 10 000 $ par année pour couvrir vos frais de subsistance, de 4000 $ pour le premier accompagnant et de 3000 $ pour chaque accompagnant additionnel), et</li>
<li>les frais de transport de retour pour vous et les membres de votre famille qui vous accompagnent au Canada ;</li>
<li>être un citoyen respectueux de la loi, sans casier judiciaire, et ne pas présenter de risques pour la sécurité du Canada ;</li>
<li>être en bonne santé et disposé à vous soumettre à un examen médical, au besoin ;</li>
<li>convaincre l’agent d’immigration que vous quitterez le Canada une fois vos études terminées.</li></ul>
<p><strong>Etudes d’une durée supérieure à six mois</strong></p>
<p>Si vous désirez étudier pour une période de plus de six mois dans un établissement d’enseignement au Canada, de l’école primaire à l’université, vous devez obtenir un permis d’études avant de partir. La demande doit être présentée au bureau des visas canadien de votre pays de résidence. Pour le Québec, le permis d’étude est soumis à l’obtention d’un <strong>certificat de sélection du Québec</strong>, délivré par la Délégation générale du Québec à Paris.</p>
<p>Vous devez au préalable avoir été accepté dans un établissement d’enseignement au Canada.</p>
<p>Si votre demande est acceptée, une lettre d’introduction confirmant l’approbation vous sera délivrée (<strong>ce n’est pas votre permis d’études</strong>). Vous devrez présenter ce document à votre arrivée au Canada. Le permis d’études est prorogeable sur place au Canada.</p>
<p>Il est à noter que les étudiants peuvent désormais travailler 20 heures par semaine en dehors du campus. Un visa d’étudiant coûtait 125 $ fin 2006.</p>
<p><strong>Travailler au Canada</strong></p>
<p>Si vous souhaitez travailler au Canada, vous devez <strong>déposer une demande de visa de résident temporaire</strong>. Il convient de faire la distinction entre le séjour temporaire et la résidence permanente au Canada. La résidence permanente est ouverte aux travailleurs qualifiés et professionnels, aux gens d’affaires et au regroupement familial.</p>
<p>Toute personne souhaitant travailler au Canada devra obligatoirement être munie d’un numéro d’assurance sociale NAS pour bénéficier des prestations et des services accordés par les programmes gouvernementaux. Le NAS sert également d’identification fiscale par l’Agence du revenu Canada. Le numéro NAS s’obtient auprès d’un centre Service Canada provincial qui figure sur le site Internet <a href="http://www.servicecanada.gc.ca/" class="spip_out" rel="external">Service Canada</a>.</p>
<p>A noter que le travail au noir est interdit et sévèrement réprimé au Canada. Les sanctions peuvent entraîner une expulsion et une interdiction de territoire. Les provinces déploient d’importants moyens financiers pour lutter contre ce problème. Les programmes mis à disposition des personnes désireuses de s’installer et de travailler temporairement au Canada sont développés. La politique d’immigration demeure elle aussi souple.</p>
<p><strong>Travail temporaire</strong></p>
<p>Vous devez être en possession d’un permis de travail en cours de validité pour travailler au Canada, à l’exception de certains emplois temporaires pour lesquels un permis n’est pas requis. Vous pouvez consulter la liste de ces emplois temporaires sur le site <a href="http://www.cic.gc.ca/" class="spip_out" rel="external">Citoyenneté et Immigration Canada</a>.</p>
<p><strong>Au Canada, un stage est considéré comme un emploi même s’il est non-rémunéré ou de courte durée. Un permis de travail temporaire peut donc être requis. </strong></p>
<p>La demande de permis de travail temporaire doit être effectuée auprès du service des visas de l’Ambassade du Canada à Paris. Dans certains cas et sous certaines conditions, il est possible de faire la demande de permis de travail temporaire depuis le Canada. Pour plus d’informations, se reporter au site Internet <a href="http://www.cic.gc.ca/" class="spip_out" rel="external">Citoyenneté et Immigration Canada</a>.</p>
<p>Une des principales conditions est de trouver au préalable un employeur prêt à vous embaucher.</p>
<p>Avant de déposer votre demande, vous devez vous procurer les documents suivants :</p>
<ul class="spip">
<li>une offre d’emploi écrite de la part de votre employeur ;</li>
<li>des justificatifs indiquant que vous êtes en mesure de satisfaire aux exigences du poste (confirmation de vos études ou de votre expérience de travail, par exemple) ;</li>
<li>le cas échéant, l’avis positif sur le marché du travail en rapport avec votre offre d’emploi établi par Ressources humaines et Développement social Canada (RHDSC). C’est à votre employeur de l’obtenir et de vous le faire parvenir. L’avis sur le marché du travail confirme qu’un travailleur étranger peut occuper l’emploi. Certains emplois sont dispensés de cet avis. Dans certains cas, vous pouvez présenter votre demande pendant que vous attendez l’avis favorable sur le marché du travail. A noter qu’un avis positif sur le marché du travail ne garantit pas automatiquement l’obtention d’un permis de travail.</li></ul>
<p>Vous devez en outre satisfaire aux exigences générales d’entrée et de séjour au Canada, en plus de celles applicables au permis de travail.</p>
<p><strong>Le permis de travail n’est pas un document d’immigration.</strong> Il ne vous permet pas de vivre au Canada de façon permanente.</p>
<p>Si vous désirez que les membres de votre famille vous accompagnent au Canada, ceux-ci doivent présenter une demande en ce sens.</p>
<p>Si vous êtes autorisé à travailler au Canada, vous recevrez une lettre d’autorisation (<strong>cette lettre n’est pas un permis de travail</strong>). Vous devrez présenter ce document à votre arrivée au Canada. Un permis de travail énonçant les conditions de votre séjour et de votre travail au Canada vous sera remis.</p>
<p>Le permis de travail est prorogeable et modifiable sur place sous certaines conditions.</p>
<p><strong>Résidence permanente</strong></p>
<p>Elle ne concerne que les travailleurs qualifiés et professionnels, les gens d’affaires (investisseurs, entrepreneurs et travailleurs indépendants), les immigrants au titre du regroupement familial, les candidats des provinces et les réfugiés. Le visa de résident permanent donne les mêmes droits qu’un citoyen canadien, excepté le droit de vote.</p>
<p>La plupart du temps, la demande de visa de résident permanent devra être déposée auprès d’un bureau des visas situé à l’extérieur du Canada. Pour le Québec, L’obtention d’un visa</p>
<p>de résident permanent nécessite au préalable l’obtention d’un Certificat de sélection du Québec. Ce document doit être demandé auprès de la Délégation générale du Québec à Paris.</p>
<p>Dans certains cas, la demande pourra être faite après l’arrivée au Canada.</p>
<p>Vous trouverez sur le site Internet <a href="http://www.cic.gc.ca/" class="spip_out" rel="external">Citoyenneté et Immigration Canada</a> les conditions à remplir et les formalités à accomplir pour chaque catégorie.</p>
<p><strong>Maintien du statut de résident permanent</strong></p>
<p>Pour conserver le statut de résident permanent, il faut remplir certaines conditions de résidence. Le résident permanent est réputé se conformer aux dispositions régissant l’obligation de résidence si pendant <strong>au moins 730 jours sur une période de cinq ans</strong> :</p>
<ul class="spip">
<li>il est physiquement présent au Canada ;</li>
<li>il accompagne, hors du Canada, un citoyen canadien qui est son époux ou conjoint de fait ou, dans le cas d’un enfant, l’un de ses parents ;</li>
<li>il travaille hors du Canada à temps plein pour une entreprise canadienne ou pour la Fonction publique fédérale ou provinciale ;</li>
<li>il accompagne, hors du Canada, un résident permanent qui est son époux ou conjoint de fait ou, dans le cas d’un enfant, l’un de ses parents, et qui travaille à temps plein pour une entreprise canadienne ou pour la Fonction publique fédérale ou provinciale ;</li>
<li>il est en possession d’un permis de retour pour résident permanent émis avant le 28 juin 2002.</li></ul>
<p><strong>Professions réservées</strong></p>
<p>Le Canada offre de nombreuses possibilités d’emploi et connaît une pénurie de main d’œuvre qualifiée et de techniciens, mais aussi de personnel médical et de médecins généralistes. Paradoxalement, certaines professions (médecins, infirmiers, pharmaciens, enseignants, ingénieurs, juristes…) sont réglementées par des Ordres professionnels et ne sont donc pas accessibles aux étrangers et aux nouveaux immigrants.</p>
<p>L’accord fédéral sur le commerce intérieur (ACI) qui a pour but de faciliter la libre circulation des personnes, des produits et des services à l’intérieur du Canada, stipule que tout travailleur compétent pour exercer un métier ou une profession dans une province ou un territoire aura accès au même emploi dans le reste du Canada dès lors que la compétence professionnelle est mutuellement reconnue. Ceci revêt une importance particulière pour les professions libérales (médecin, ingénieur, etc.) et les métiers réglementés (machiniste, mécanicien industriel, etc.).</p>
<p>Il est donc essentiel de bien s’informer et de préparer son séjour professionnel. Il pourra être demandé au candidat de suivre une formation, réussir un examen professionnel ou encore obtenir un diplôme. Pour gagner du temps, il est vivement recommandé de procéder à la reconnaissance de ses diplômes avant de se rendre au Canada, en s’informant auprès de l’Ambassade du Canada à Paris et de faire traduire en anglais ses diplômes et lettres de recommandation professionnelle.</p>
<p>Selon un rapport de l’Organisation de coopération et de développement économiques (OCDE), le Canada est l’un des pays où les régimes de réglementation des professions sont les plus lourds.</p>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li><a href="http://www.serendreaucanada.gc.ca/" class="spip_out" rel="external">Se rendre au Canada</a></li>
<li><a href="http://www5.hrsdc.gc.ca/NOC-CNP/" class="spip_out" rel="external">Classification nationale des professions et guide sur les carrières</a></li>
<li><a href="http://www.cicdi.ca/" class="spip_out" rel="external">Centre d’information canadien sur les diplômes internationaux</a>.</li></ul>
<p><strong>Programmes de mobilité des jeunes </strong></p>
<p>Le Canada et la France ont signé le 3 octobre 2003 un accord relatif aux échanges de jeunes. Cet accord est entré en vigueur le 1er mars 2004 (décret n°2004-200 paru au Journal officiel du 4 mars 2004). Son texte est consultable sur le site Internet du <a href="http://www.legifrance.gouv.fr/" class="spip_out" rel="external">service public à la diffusion du droit</a>.</p>
<p>Cet accord permet aux Français et Canadiens âgés de 18 à 35 ans de partir dans l’autre pays afin de faire une expérience professionnelle, d’effectuer un stage pratique dans le cadre de leurs études, d’avoir un travail d’été et de travailler au cours d’un voyage de découverte.</p>
<p><strong>Emploi de perfectionnement</strong></p>
<p>Ce programme s’adresse aux jeunes professionnels souhaitant acquérir une expérience professionnelle sous couvert d’un contrat de travail à durée déterminée (maximum 18 mois).</p>
<p><strong>Stage d’études</strong></p>
<p>Ce programme s’adresse aux étudiants souhaitant effectuer un stage pratique en entreprise prévu dans le cadre de leurs études ou de leur formation en France (maximum 12 mois).</p>
<p><strong>Job d’été pour étudiants</strong></p>
<p>Ce programme s’adresse aux étudiants souhaitant exercer une activité professionnelle rémunérée pendant leurs vacances d’été (maximum trois mois).</p>
<p><strong>Programme Vacances-Travail </strong></p>
<p><a href="http://www.pvtcanada.com/" class="spip_out" rel="external">Ce programme</a> s’adresse aux jeunes souhaitant effectuer un séjour de découverte touristique et culturelle tout en travaillant (minimum six mois, maximum 24 mois à compter de l’été 2013). En 2012 pour les PVT de l’année 2013, 6750 places étaient allouées aux ressortissants français et 750 pour les ressortissants Belges. En 2013, la France a signé de nouveaux accords avec le Canada qui permettront d’augmenter le nombre de places pour les inscriptions en PVT pour 2014.</p>
<p>Votre interlocuteur est l’Ambassade du Canada à Paris et les formalités administratives sont payantes.</p>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li><a href="http://www.canadainternational.gc.ca/france/index.aspx?lang=fra" class="spip_out" rel="external">Ambassade du Canada à Paris</a></li>
<li><a href="http://www.dfait-maeci.gc.ca/" class="spip_out" rel="external">Ministère des Affaires étrangères et du Commerce international du Canada</a></li>
<li><a href="http://www.cic.gc.ca/" class="spip_out" rel="external">Citoyenneté et Immigration Canada</a></li></ul>
<p><i>Mise à jour : avril 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/entree-et-sejour/article/passeport-visa-permis-de-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
