# Animaux domestiques

<p>Les animaux domestiques (chiens, chats, etc.) accompagnés de leur propriétaire doivent répondre à certaines exigences avant de quitter le territoire français. L’animal doit tout d’abord se conformer aux règlementations françaises en matière de santé et de protection animales. Pour cela, le propriétaire doit avoir en sa possession les documents suivants :</p>
<ul class="spip">
<li>l’identification par micropuce (ou tatouage) - <i>obligatoire </i> ;</li>
<li>le certificat de vaccination antirabique en cours de validité - <i>obligatoire </i> ;</li>
<li>le certificat international de bonne santé (CIBS), délivré et signé par un vétérinaire français et tamponné par la Direction des services vétérinaires de la ville où se trouve l’animal domestique. La validité de ce document est de sept jours - <i>obligatoire </i>. Les titulaires du passeport canin ou félin sont dispensés de la présentation du CIBS ;</li>
<li>le titrage sérologique des anticorps anti-rabiques dans un laboratoire agréé - <i>conseillé </i> ;</li>
<li>le carnet de vaccination à jour - <i>conseillé</i>.</li></ul>
<p>Ces documents doivent être validés par la Direction départementale des services vétérinaires dont relève le vétérinaire traitant qui les a émis.</p>
<p>Vous pouvez consulter les adresses des Directions départementales des services vétérinaires (DDSV) sur le <a href="http://www.agriculture.gouv.fr/" class="spip_out" rel="external">site du ministère de l’Agriculture et de la pêche</a>. Pour en savoir plus, contacter votre vétérinaire ou la Direction départementale des services vétérinaires.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Notre <a href="services-aux-citoyens-preparer-son-expatriation-douanes-article-animaux-domestiques.md" class="spip_in">article thématique sur l’exportation d’animaux domestiques</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/entree-et-sejour/article/animaux-domestiques-109981). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
