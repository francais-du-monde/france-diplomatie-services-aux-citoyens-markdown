# Déménagement

<p>Il est possible de faire acheminer ses affaires par fret aérien ou par bateau puis par la route. Les délais d’acheminement sont de 20 à 30 jours par bateau, auxquels il faut ajouter quatre à cinq semaines pour les procédures de dédouanement. Les frais varient selon les transporteurs.</p>
<p>Pour plus d’information sur les procédures douanières, vous pouvez consulter la <a href="http://www.ambafrance-co.org/Service-des-Douanes-francaises-en" class="spip_out" rel="external">page du service des douanes de l’ambassade de France en Colombie</a> ou notre rubrique thématique <a href="services-aux-citoyens-preparer-son-expatriation-douanes.md" class="spip_in">Douanes</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/entree-et-sejour/article/demenagement-103418). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
