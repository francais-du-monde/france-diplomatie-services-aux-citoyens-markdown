# Scolarisation

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/vie-pratique/article/scolarisation-110995#sommaire_1">Les établissements scolaires français au Ghana</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/vie-pratique/article/scolarisation-110995#sommaire_2">Enseignement supérieur</a></li></ul>
<p>Pour toute information sur la scolarisation dans le système français à l’étranger, vous pouvez consulter nos thématiques sur les <a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures.md" class="spip_in">Etudes supérieures</a> et la <a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais.md" class="spip_in">Scolarité en français</a>.</p>
<p>Vous y trouverez des renseignements sur :</p>
<ul class="spip">
<li>les établissements français du primaire et du secondaire à l’étranger ;</li>
<li>les bourses scolaires et la prise en charge des frais de scolarité à l’étranger ;</li>
<li>les possibilités qui s’offrent à vous si votre enfant ne peut être scolarisé à l’étranger dans le système français (enseignement à distance par le CNED, programme français langue maternelle (FLAM), internats en France) ;</li>
<li>les épreuves du baccalauréat à l’étranger ;</li>
<li>les bourses d’études supérieures en France et à l’étranger ;</li>
<li>l’équivalence des diplômes.</li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Les établissements scolaires français au Ghana</h3>
<p>Le lycée français Jacques Prévert d’Accra est un établissement conventionné de l’AEFE qui dispense un enseignement de la maternelle à la terminale en présentiel. Il est centre d’examens pour le diplôme national du brevet et le Baccalauréat. Des préparations aux certifications Cambridge sont par ailleurs possibles. Il accueille plus de 560 élèves d’au moins 30 nationalités différentes.</p>
<p>Il permet une scolarisation dans une section bilingue qui couvre les enseignements de la petite section au CE2. Cette section progresse d’un niveau chaque année et atteindra le CM1 l’année prochaine.</p>
<p>Cet établissement connait actuellement une croissance importante et un projet de restructuration extension est en cours de définition.</p>
<p>Les enfants voulant suivre un enseignement non dispensé au sein du Lycée (langue vivante par exemple), peuvent le faire avec le CNED, les notes obtenues entrant dans le livret scolaire de l’élève.</p>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li><a href="http://www.ambafrance-gh.org/-Lycee-Francais-" class="spip_out" rel="external">Lycée francais</a></li>
<li><a href="http://www.aefe.fr/" class="spip_out" rel="external">Agence française pour l’enseignement français à l’étranger</a>.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Enseignement supérieur</h3>
<p>Il est possible de poursuivre des études supérieures dans de <a href="http://www.ghanauniversities.org/Default.aspx" class="spip_out" rel="external">nombreuses universités publiques (il en existe neuf) et privés au Ghana</a>. Les trois principales universités publiques sont University of Ghana (Accra), KNUST à Kumasi et UCC à Cape Coast.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/vie-pratique/article/scolarisation-110995). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
