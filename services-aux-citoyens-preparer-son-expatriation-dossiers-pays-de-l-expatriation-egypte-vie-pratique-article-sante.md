# Santé

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/vie-pratique/article/sante#sommaire_1">Soins</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Soins</h3>
<p><strong>Avant le départ :</strong></p>
<p>Consultez votre médecin et souscrivez à une compagnie d’assurance couvrant les frais médicaux et le rapatriement sanitaire. Veillez à ce que les prestations proposées par le voyagiste comprennent bien une telle assurance. Durant votre séjour, veillez à consulter un médecin en cas de fièvre, de syndrome grippal ou de troubles digestifs sévères.</p>
<p><strong>Épidémie de grippe aviaire :</strong></p>
<p>La Direction générale de la santé recommande aux voyageurs d’éviter tout contact avec les volailles et les oiseaux, c’est-à-dire de ne pas se rendre dans des élevages ni sur les marchés aux volatiles.</p>
<p>Les recommandations générales d’hygiène lors des voyages dans les pays en développement, qui visent à se protéger des infections microbiennes, sont préconisées :</p>
<ul class="spip">
<li>éviter de consommer des produits alimentaires crus ou peu cuits, en particulier les viandes et les œufs ;</li>
<li>se laver régulièrement les mains à l’eau et au savon ou un soluté hydro-alcoolique.</li></ul>
<p>Le virus se transmet par voie aérienne (voie respiratoire) soit par contact direct, notamment avec les sécrétions respiratoires et les matières fécales des animaux malades, soit de façon indirecte par l’exposition à des matières contaminées (par l’intermédiaire de la nourriture, de l’eau, du matériel et des mains ou des vêtements souillés). Les espaces confinés favorisent la transmission du virus.</p>
<p>Il est conseillé de consulter un médecin en cas de syndrome grippal, avec installation brutale de fièvre, de céphalées, de douleurs musculaires, de douleurs dorsales, de vomissements, fièvre ou diarrhées hémorragique.</p>
<p><strong>Autres recommandations</strong></p>
<p>L’attention du voyageur est appelée sur la <strong>pollution atmosphérique</strong> particulièrement élevée au Caire. Il est recommandé aux personnes présentant un état de santé déficient (allergies, asthme, difficultés respiratoires…) de ne pas y séjourner longtemps.</p>
<p>Certaines maladies, notamment virales, sont transmises par des <strong>piqûres d’insectes</strong>. Il est recommandé à cet effet d’utiliser des répulsifs (sprays anti-moustiques) et de porter des vêtements longs couvrants.</p>
<p>La mise à jour de la <strong>vaccination</strong> dipthérie-tétanos-polio est nécessaire. Il est par ailleurs recommandé d’effectuer les vaccins contre l’hépatite A et la fièvre typhoïde.</p>
<p><strong>Hygiène alimentaire</strong> : il est déconseillé de boire l’eau du robinet. Préférez les eaux en bouteilles capsulées. Evitez l’ingestion de glaçons, de jus de fruits frais, de légumes crus et de fruits non pelés. Evitez la consommation d’aliments insuffisamment cuits (poisson, viande, volaille, lait). Veillez à un lavage soigneux des mains avant chaque repas.</p>
<p>Pour de plus amples renseignements, vous pouvez consulter le site de l’<a href="http://www.invs.sante.fr/" class="spip_out" rel="external">INVS</a>, ainsi que celui de l’<a href="http://www.who.int/fr/index.html" class="spip_out" rel="external">OMS</a>, qui vous renseigneront sur l’état sanitaire de ce pays, ou les sites de <a href="http://www.pasteur-lille.fr/" class="spip_out" rel="external">l’institut Pasteur de Lille</a>, de <a href="http://www.pasteur.fr/" class="spip_out" rel="external">l’institut Pasteur de Paris</a> et du <a href="http://www.cimed.org/" class="spip_out" rel="external">CIMED</a>.</p>
<p>Pour une présentation des risques épidémiologiques ainsi que des conseils sur la médecine au quotidien, consultez les pages suivantes :</p>
<ul class="spip">
<li>Conseils, liste de médecins et hôpitaux sur le site des consulats généraux de France <a href="http://www.ambafrance-eg.org/Coordonnees-utiles-medecins" class="spip_out" rel="external">au Caire</a> et à <a href="http://www.ambafrance-eg.org/Liste-de-notoriete-medicale-a" class="spip_out" rel="external">Alexandrie</a></li>
<li><a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/egypte/" class="spip_in">Page dédiée à la santé en Egypte sur le site Conseils aux voyageurs</a></li>
<li>Fiches Le Caire et Alexandrie sur le site du <a href="http://www.cimed.org" class="spip_out" rel="external">CIMED</a>.</li></ul>
<p><i>Mise à jour : avril 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/vie-pratique/article/sante). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
