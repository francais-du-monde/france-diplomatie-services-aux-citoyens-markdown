# Communications

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/vie-pratique/article/communications-109811#sommaire_1">Téléphone - Internet</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Téléphone - Internet</h3>
<p>Les liaisons téléphoniques et postales ne posent aucun problème.</p>
<p>Code téléphonique pour joindre la Belgique à partir de la France : 00+32 suivi du numéro à huit chiffres (sans le 0 initial)<br class="manualbr">Code téléphonique pour joindre la France à partir de la Belgique : 00+33 suivi du numéro à neuf chiffres (sans le 0 initial)<br class="manualbr">Il convient de composer le :</p>
<ul class="spip">
<li>2 pour Bruxelles</li>
<li>9 pour Gand</li>
<li>3 pour Anvers.</li></ul>
<p>Il existe plusieurs compagnies pour l’ouverture d’une ligne téléphonique et internet, telles que Belgacom, Proximus, Mobistar, Scarlet, Coditel, 5 Telecom… Il faut noter que les fournisseurs d’accès en Belgique restent plus chers qu’en France.</p>
<p>Principal site de comparaison des tarifs téléphoniques en Belgique : <a href="http://www.speed.be/" class="spip_out" rel="external">www.speed.be</a></p>
<h4 class="spip">Utilisation de votre portable français</h4>
<p>Vous pouvez utiliser votre téléphone portable partout en Europe et dans de nombreuses autres régions du monde, grâce à la norme technique GSM de l’Union européenne. Les personnes qui utilisent leur téléphone portable à l’étranger réalisent d’importantes économies – de l’ordre de 60 % – grâce au règlement de l’UE sur l’itinérance (« roaming »), qui plafonne le coût des appels téléphoniques effectués ou reçus dans un autre pays de l’UE (« eurotarif »). De plus, une facturation à la seconde a été introduite après les premières 30 secondes pour les appels effectués, et dès la première seconde pour les appels reçus. Le coût d’envoi d’un SMS depuis l’étranger a lui aussi nettement diminué. Renseignez-vous auprès de votre opérateur français pour les tarifs.</p>
<p>Les clients reçoivent un message automatique les informant du coût des appels, des SMS et des services de transmission de données à l’étranger (en cas de navigation sur internet ou de téléchargement d’un film, par exemple). Les prix de gros que les opérateurs se facturent mutuellement sont également plafonnés, ce qui devrait réduire les coûts supportés par les consommateurs. Un mécanisme a également été mis en place afin d’éviter aux consommateurs de devoir payer des factures astronomiques pour des services d’itinérance : après un premier avertissement, la connexion internet mobile est suspendue dès que le montant atteint un certain niveau.</p>
<p><i>Source : <a href="http://europa.eu/youreurope/citizens/travel/money-charges/mobile-roaming-costs/index_fr.htm" class="spip_out" rel="external">L’Europe est à vous</a> </i></p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/vie-pratique/article/communications-109811). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
