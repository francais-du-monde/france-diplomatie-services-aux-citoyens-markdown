# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/vie-pratique/article/cout-de-la-vie-109858#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/vie-pratique/article/cout-de-la-vie-109858#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/vie-pratique/article/cout-de-la-vie-109858#sommaire_3">Alimentation</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/vie-pratique/article/cout-de-la-vie-109858#sommaire_4">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>L’unité monétaire est le yen (JPY ou <strong>¥</strong>).</p>
<p>Au 13 juin 2013, le yen vaut 0.0079 euros c’est-à-dire qu’un euro équivaut à 125.85yens.</p>
<p>Les prix indiqués en euros dans le présent chapitre ont été calculés sur la base du taux de change du yen ci-dessus. Toutefois, compte tenu de l’évolution très volatile du taux de change, il est conseillé d’actualiser ces données avant le départ pour le Japon afin de mieux connaître la réalité des coûts.</p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<p>Bien que les cartes bancaires soient utilisées de plus en plus dans les grands magasins et restaurants d’un certain niveau, il reste difficile de payer par carte bancaire dans la majorité des commerces. Tous les distributeurs n’acceptent pas les cartes étrangères.</p>
<p>Les chèques sont très rarement utilisés. Les paiements se font souvent en argent liquide ou par virements bancaires.</p>
<p>Les plus grandes banques sont Mizuho, Fuji bank, Mitsubishi Tokyo UFJ, Citibank, Sumitomo (SMBC), Sakura bank, Diichi Kangyo bank et la poste japonaise.</p>
<p>Pour ouvrir un compte bancaire, il convient de remplir un formulaire (en japonais), présenter votre Alien card ou votre passeport et disposer d’une adresse au Japon.</p>
<p>Les banques ferment assez tôt (vers 15h). Il est cependant possible de trouver des distributeurs de billets (ATM) dans les <i>kombini</i> (superettes), ouverts plus longtemps.</p>
<p>L’usage de la carte de crédit est maintenant répandu au Japon mais, dans certains petits commerces et surtout hors des grandes agglomérations, il est possible que le paiement ne soit accepté qu’en numéraires. En revanche, pour retirer des espèces, il est préférable de recourir aux distributeurs de billets (ATM) des banques internationales ou à ceux situés dans les points de passage des voyageurs (aéroports, gares..). En principe, sur les autres types de distributeur (banques locales, commerces), une indication précise si le retrait international ou non est possible.</p>
<p>Les personnes se rendant dans le Kansai trouveront sur le site du <a href="http://www.consulfrance-kyoto.org/" class="spip_out" rel="external">Consulat général de France à Kyoto</a> les adresses des distributeurs fonctionnant avec les cartes émises à l’étranger pour les localités d’Osaka et Kyoto.</p>
<p>D’une manière générale, il est également conseillé de se pourvoir en <i>travellers chèques</i> libellés en yens.</p>
<h3 class="spip"><a id="sommaire_3"></a>Alimentation</h3>
<h4 class="spip">Prix moyen d’un repas dans un restaurant</h4>
<ul class="spip">
<li>de 850 à 3 500 yens dans un restaurant de catégorie moyenne ;</li>
<li>à partir de 7 000 yens dans un restaurant de 1ère catégorie.</li></ul>
<p>Les prix indiqués ci-dessus concernent le repas de midi, sans les vins ; le soir, il faut en augmenter le prix de 30 à 100% en fonction du standing de l’établissement. Il est possible de déjeuner dans un établissement japonais de restauration rapide pour un coût de 600 Yens.</p>
<p>Le pourboire n’est pas une coutume japonaise. Il convient d’ajouter les taxes de consommation (5%) et une taxe spéciale de service pour les hôtels de luxe (10%) et Ryokan (15%).</p>
<h3 class="spip"><a id="sommaire_4"></a>Evolution des prix</h3>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : juillet 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/vie-pratique/article/cout-de-la-vie-109858). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
