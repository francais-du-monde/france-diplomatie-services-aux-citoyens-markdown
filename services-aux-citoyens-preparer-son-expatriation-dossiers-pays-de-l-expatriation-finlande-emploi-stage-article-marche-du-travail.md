# Marché du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/emploi-stage/article/marche-du-travail#sommaire_1">Contexte</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/emploi-stage/article/marche-du-travail#sommaire_2">Secteurs à fort potentiel</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/emploi-stage/article/marche-du-travail#sommaire_3">Rémunération</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Contexte</h3>
<p>La Finlande compte un peu plus de 5,4 millions d’habitants. Au cours des prochaines années, la population en âge de travailler va diminuer en raison de l’augmentation des départs à la retraite, mais dans le même temps l’immigration va augmenter et les personnes vont rester en activité de plus en plus longtemps. En 2013, le pays comptait un peu moins de 2,5 millions d’actifs.</p>
<p>En Finlande, les services emploient sensiblement plus de monde que l’industrie et la construction. Les secteurs qui emploient le plus sont le commerce, les transports, les services d’hôtellerie et de restauration, l’éducation, les services sociaux et de santé. L’emploi dans les services devrait continuer à croître à l’avenir.</p>
<p>Les entreprises qui emploient le plus sont : la société Itella dans le secteur des services postaux et de la messagerie, le fabricant de produits forestiers et de papier UPM et la société de grande distribution Kesko. En Finlande, le secteur public est lui aussi un employeur important, en particulier les communes (qui emploient notamment les enseignants et le personnel dans les domaines de la santé et des affaires sociales). La ville de Helsinki est par exemple le premier employeur du pays. Elle offre des emplois dans l’éducation, les services sociaux et de santé, les transports et l’entretien.</p>
<p>La poursuite de l’incertitude économique – <strong>la croissance est négative en Finlande en 2012, 2013 et 2014</strong> - s’est traduite en Finlande par un <strong>gel de l’emploi dans un certain nombre d’entreprises</strong>. Le secteur tertiaire est le seul à afficher des perspectives d’emploi légèrement plus réjouissantes. Selon les dernières informations recueillies, le principal obstacle à l’embauche est l’incertitude quant à la demande de produits et services. Parallèlement, on observe toutefois en Finlande également une pénurie de main-d’œuvre dans de nombreux secteurs, notamment celui des services.</p>
<h3 class="spip"><a id="sommaire_2"></a>Secteurs à fort potentiel</h3>
<p>Les secteurs à fort potentiel sont ceux de la santé et des services à la personne.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  guide <a href="http://www.mol.fi/mol/fi/99_pdf/fi/06_tyoministerio/06_julkaisut/05_esitteet/tme7601ra_finlande.pdf" class="spip_out" rel="external">Travailler en Finlande</a> </p>
<h3 class="spip"><a id="sommaire_3"></a>Rémunération</h3>
<p>A l’échelon national, il n’existe pas de législation instituant un salaire minimum. Néanmoins, les conventions collectives prévoient, par branche d’activité, un salaire minimum applicable à cette activité, souvent largement supérieur au salaire minimum français.</p>
<p><i>Mise à jour : janvier 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/emploi-stage/article/marche-du-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
