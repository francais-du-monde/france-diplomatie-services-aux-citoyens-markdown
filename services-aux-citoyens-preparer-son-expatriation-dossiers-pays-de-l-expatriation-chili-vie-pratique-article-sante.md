# Santé

<p>Pour une présentation des risques épidémiologiques ainsi que des conseils sur la médecine au quotidien, consultez les pages suivantes :</p>
<ul class="spip">
<li>Conseils, liste de médecins et hôpitaux sur le site du <a href="http://www.ambafrance-cl.org/" class="spip_out" rel="external">consulat de France à Santiago</a></li>
<li>Page dédiée à la santé au Chili sur le site <a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/chili/" class="spip_in">Conseils aux voyageurs</a></li>
<li>Fiche <strong>Santiago</strong> sur le site du <a href="http://www.cimed.org/" class="spip_out" rel="external">CIMED</a></li></ul>
<p>En raison de la pollution à Santiago et du pollen de certaines plantes, les personnes fragiles développent des allergies "asthmatiformes".</p>
<p>A Santiago existent d’excellents centres hospitaliers privés (Clinique allemande, clinique Las Condes, etc) avec des infrastructures de pointe et un personnel médical très qualifié (frais médicaux très élevés), mais des garanties de paiement sont exigées à l’entrée ; il est recommandé d’être muni d’une carte de crédit et d’une assurance médicale/voyage. Les services de santé en province sont moins bien équipés et n’offrent donc pas la même qualité de soins.</p>
<p>Consulter son médecin traitant avant le départ et contracter une assurance de rapatriement sanitaire.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/vie-pratique/article/sante). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
