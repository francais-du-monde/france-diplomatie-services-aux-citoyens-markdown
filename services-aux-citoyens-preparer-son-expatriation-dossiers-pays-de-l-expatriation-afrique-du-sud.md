# Afrique du Sud

<p>Au <strong>31 décembre 2014</strong>, 7629 Français étaient enregistrés au registre (4609 pour Johannesburg, 3020 pour Le Cap).</p>
<p>Bénéficiant d’un assez bon niveau socioprofessionnel, les Français interviennent dans une gamme de secteurs plutôt large : industrie, services, professions libérales, représentants de sociétés françaises, etc.</p>
<p>Près de 300 entreprises françaises possèdent une filiale ou un bureau de représentation en Afrique du Sud. Ces entreprises opèrent dans des secteurs très diversifiés : industrie agroalimentaire, électrique, chimique, mécanique, BTP, énergie, banque, tourisme, etc.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/dossiers-pays/afrique-du-sud/" class="spip_in">Une description de l’Afrique du Sud, de sa situation politique et économique</a> ;</li>
<li><a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/afrique-du-sud/" class="spip_in">Des informations actualisées sur les conditions locales de sécurité en Afrique du Sud</a>.</li></ul>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-entree-et-sejour-22968.md">Entrée et séjour</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-entree-et-sejour-22968-article-passeport-visa-permis-de-travail-111002.md">Passeport, visa, permis de travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-entree-et-sejour-22968-article-demenagement-111003.md">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-entree-et-sejour-22968-article-vaccination-111004.md">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-entree-et-sejour-22968-article-animaux-domestiques-111005.md">Animaux domestiques</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-emploi-stage.md">Emploi, stage</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-emploi-stage-article-marche-du-travail-111006.md">Marché du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-emploi-stage-article-reglementation-du-travail-111007.md">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-emploi-stage-article-recherche-d-emploi-111008.md">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-emploi-stage-article-curriculum-vitae-111009.md">Curriculum vitae</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-emploi-stage-article-lettre-de-motivation.md">Lettre de motivation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-emploi-stage-article-stages.md">Stages</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-protection-sociale-22970.md">Protection sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-protection-sociale-22970-article-regime-local-de-securite-sociale-111013.md">Régime local de sécurité sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-protection-sociale-22970-article-convention-de-securite-sociale-111014.md">Convention de sécurité sociale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-fiscalite-22971.md">Fiscalité</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-fiscalite-22971-article-fiscalite-du-pays-111016.md">Fiscalité du pays</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-fiscalite-22971-article-convention-fiscale-111017.md">Convention fiscale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-vie-pratique.md">Vie pratique</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-vie-pratique-article-logement-111018.md">Logement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-vie-pratique-article-sante.md">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-vie-pratique-article-scolarisation-111020.md">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-vie-pratique-article-cout-de-la-vie-111021.md">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-vie-pratique-article-transports.md">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-vie-pratique-article-communications-111023.md">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-vie-pratique-article-loisirs-et-culture.md">Loisirs et culture</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-vie-pratique-article-pour-en-savoir-plus-111025.md">Pour en savoir plus</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-presence-francaise.md">Présence française</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-presence-francaise-article-presence-francaise.md">Présence française</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
