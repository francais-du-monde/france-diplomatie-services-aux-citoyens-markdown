# Personne créancière

<p class="chapo">
      Vous êtes une personne créancière d’une obligation alimentaire ; vous pouvez poursuivre l’action en recouvrement de votre créance à l’étranger selon les modalités suivantes en fonction de votre situation :
</p>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/conseils-aux-familles/recouvrement-de-creances/article/personne-creanciere#sommaire_1">1) Vous résidez en France et le débiteur d’aliments est domicilié dans un pays de l’Union européenne</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/conseils-aux-familles/recouvrement-de-creances/article/personne-creanciere#sommaire_2">2) Vous résidez en France et le débiteur d’aliments est domicilié dans un pays en dehors de l’Union européenne</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/conseils-aux-familles/recouvrement-de-creances/article/personne-creanciere#sommaire_3">3) Vous résidez hors de France et le débiteur d’aliments est domicilié en France</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/conseils-aux-familles/recouvrement-de-creances/article/personne-creanciere#sommaire_4">4) Vous résidez hors de France et le débiteur d’aliments est domicilié hors de France</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>1) Vous résidez en France et le débiteur d’aliments est domicilié dans un pays de l’Union européenne</h3>
<p><i>Le Règlement (CE) n°4/2009 du Conseil du 18 décembre 2008 relatif à la compétence, la loi applicable, la reconnaissance et l’exécution des décisions et la coopération en matière d’obligations alimentaires s’applique. Toutefois, les pièces exigibles pour la constitution de votre dossier diffèrent selon la nature de votre demande.</i></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Vous sollicitez <a href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/1_-_pieces_a_fournir_cle03da6d.pdf" class="spip_in" type="application/pdf">la reconnaissance ou la reconnaissance et la déclaration constatant la force exécutoire d’une décision, ou/et l’exécution d’une décision</a> fixant une pension alimentaire. </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Vous sollicitez <a href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/2_-_pieces_a_fournir_cle09c6b9.pdf" class="spip_in" type="application/pdf">l’obtention d’une décision</a> </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Vous sollicitez <a href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/3_-_pieces_a_fournir_cle0e1bc3.pdf" class="spip_in" type="application/pdf">la modification d’une décision</a> </p>
<h3 class="spip"><a id="sommaire_2"></a>2) Vous résidez en France et le débiteur d’aliments est domicilié dans un pays en dehors de l’Union européenne</h3>
<p>Votre débiteur d’aliments réside soit dans <a href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/SKMBT_C35312112816120_cle474c79.pdf" class="spip_in" type="application/pdf">un pays signataire d’une convention internationale en matière de recouvrement de créances alimentaires</a>  (convention de La Haye du 23 novembre 2007 sur le recouvrement international des aliments destinées aux enfants et à d’autres membres de la famille ; <a href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/ny_conv_cle0add4b.pdf" class="spip_in" type="application/pdf">convention de New York du 20 juin 1956 sur le recouvrement des aliments à l’étranger</a>) soit dans <a href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/2_-_pays_non_signataires_cle832447.pdf" class="spip_in" type="application/pdf">un pays n’ayant signé aucune convention internationale en cette matière</a>.</p>
<h3 class="spip"><a id="sommaire_3"></a>3) Vous résidez hors de France et le débiteur d’aliments est domicilié en France</h3>
<p>Si vous résidez à l’étranger, vous devez saisir l’autorité centrale en matière de recouvrement d’aliments de votre pays de résidence afin qu’il constitue le dossier et le transmette au bureau RCA.</p>
<p>Le bureau RCA ne peut donc directement traiter votre demande de recouvrement et doit être saisi par son homologue étranger.</p>
<h3 class="spip"><a id="sommaire_4"></a>4) Vous résidez hors de France et le débiteur d’aliments est domicilié hors de France</h3>
<p>Le bureau RCA n’est pas compétent pour connaître de votre situation.</p>
<p>Votre dossier relève de la compétence des autorités centrales des pays où vous-même et votre débiteur résidez.</p>
<p><i>Mise à jour : juin 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/conseils-aux-familles/recouvrement-de-creances/article/personne-creanciere). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
