# Convention de sécurité sociale

<p>La France et le Maroc sont liés par une nouvelle convention de sécurité sociale qui est entrée en vigueur le 1er juin 2011. Elle a repris et amélioré les acquis de l’ancien dispositif (notamment la convention franco-marocaine du 9 juillet 1965). Cet accord vise les ressortissants français ou marocains qui exercent une activité salariée dans l’un des deux Etats.</p>
<p>Cette convention contient des dispositions en matière :</p>
<ul class="spip">
<li>d’assurance maladie-maternité,</li>
<li>d’assurance invalidité,</li>
<li>d’assurance vieillesse et survivants</li>
<li>d’assurance accidents du travail,</li>
<li>de prestations familiales.</li></ul>
<p><strong>Objectifs de cette nouvelle convention</strong></p>
<p>Elle réaffirme notamment les dispositions traditionnelles en matière de sécurité sociales telles que :</p>
<ul class="spip">
<li>le principe d’assujettissement à la législation du pays d’emploi sous certaines réserves. Ainsi, les travailleurs détachés au titre de la convention, les fonctionnaires des Postes, les recrutés locaux de nationalité française ayant exercé leur droit d’option restent affiliés au régime de l’Etat d’origine et sont donc exemptés de cotisations sociales locales ;</li>
<li>le principe d’égalité de traitement entre les travailleurs nationaux et expatriés ;</li>
<li>le principe de l’exportation des prestations habituellement soumises à condition de résidence (pensions, prestations familiales), garantissant ainsi le maintien des droits acquis ;</li>
<li>le principe de coordination des régimes de sécurité sociale, grâce à la prise en compte des périodes de cotisation accomplies dans les deux Etats pour l’ouverture et le calcul des droits à certaines prestations.</li></ul>
<p><strong>Principales innovations de cette nouvelle convention :</strong></p>
<ul class="spip">
<li>elle concerne désormais une gamme plus large d’assurés sociaux (travailleurs non salariés, chômeurs indemnisés, fonctionnaires, étudiants et stagiaires en formation professionnelle, pensionnés et préretraités.</li>
<li>la durée du détachement pour les travailleurs salariés est élargie à trois ans et renouvelable une fois (cette durée est de six mois pour les travailleurs non salariés).</li>
<li>un instrument de coordination permet de prendre en compte des périodes d’assurance accomplies dans des Etats tiers liés à la France et au Maroc.</li>
<li>l’extension des soins de santé et du droit aux allocations familiales pour les pensionnés, les personnes en formation professionnelle qui ne résident pas dans l’Etat compétent et leurs ayants-droit.</li>
<li>la prise en charge des soins d’immédiate nécessité pour les ayants-droit d’un travailleur marocain qui résident au Maroc, à l’occasion d’un séjour temporaire au Maroc.</li>
<li>le remboursement des prestations en nature dans le cadre de cette convention s’effectue sur la base de coûts réels (factures) et non plus sur la base de forfaits. Tout renseignement complémentaire au sujet de l’application de la convention franco-marocaine peut être obtenu auprès du :</li></ul>
<p><a href="http://www.cleiss.fr/" class="spip_out" rel="external">Centre des Liaisons Européennes et Internationales de Sécurité Sociale</a><br class="manualbr">11, rue de la Tour des Dames<br class="manualbr">75436 PARIS Cedex 09<br class="manualbr">Tél. : 01-45-26-33-41<br class="manualbr">Fax : 01-49-95-06-50</p>
<h4 class="spip">Travailleurs Français exerçant au Maroc une activité salariée ou assimilée, non détachés bénéficiant des dispositions prévues par la convention Franco-Marocaine</h4>
<p>Un travailleur français non détaché qui exerce une activité salariée ou assimilée au Maroc, relève du régime local en vertu du principe de l’égalité de traitement posé par l’article 1er de la Convention.</p>
<h4 class="spip">Droits du travailleur pour lui-même et les ayants droit qui l’accompagnent pendant la période de travail au Maroc</h4>
<p>Le travailleur et ses ayants droit bénéficient des dispositions prévues par la législation marocaine dans les mêmes conditions que les travailleurs marocains.</p>
<h5 class="spip">Maladie, maternité</h5>
<p>Pour l’ouverture du droit aux prestations, la caisse marocaine de sécurité sociale peut faire appel, en tant que de besoin, aux périodes d’assurance ou équivalentes antérieurement accomplies en France.</p>
<p>A cet effet, le travailleur français devra, avant le départ, demander à sa caisse l’établissement du formulaire SE 350-09 "Attestation des périodes d’assurance".</p>
<h5 class="spip">Allocations familiales</h5>
<p>Pour l’examen des droits éventuels aux prestations familiales locales, la caisse marocaine tient compte, si cela s’avère nécessaire, des périodes de travail ou assimilées accomplies en France. Le travailleur aura donc là encore intérêt, par exemple, à demander à sa caisse d’allocations familiales, l’établissement du formulaire SE 350-02 "Attestation des périodes d’inscription aux institutions d’allocations familiales".</p>
<h5 class="spip">Invalidité</h5>
<p>La pension d’invalidité est liquidée conformément aux dispositions de la législation applicable à l’intéressé au moment de l’interruption de travail suivie d’invalidité et supportée par l’organisme compétent aux termes de cette législation.</p>
<h5 class="spip">Vieillesse</h5>
<p>Le travailleur français occupé au Maroc cotise au régime local d’assurance vieillesse et acquiert de ce fait des droits à pension. Lors de la liquidation de la pension de vieillesse les institutions compétentes procèdent de la manière suivante :</p>
<ul class="spip">
<li>si l’intéressé satisfait aux conditions requises par la législation de chacun des deux pays sans qu’il soit nécessaire de recourir à la totalisation des périodes d’assurance, l’institution compétente de chacun des deux pays procède à la liquidation séparée des droits et attribue à l’assuré une pension nationale ; deux pensions nationales sont alors attribuées ;</li>
<li>si ce dernier ne satisfait aux conditions requises par la législation de chacun des deux États qu’en recourant à la totalisation des périodes d’assurance, l’institution compétente de chacun des deux Etats procède à la liquidation des droits par totalisation et proratisation et attribue, de ce fait, à l’intéressé une pension proportionnelle ; deux pensions proportionnelles sont octroyées ;</li>
<li>lorsque le droit à pension est acquis au titre de la législation de l’un des deux Etats compte tenu des seules périodes d’assurance accomplies sous cette législation, l’institution compétente de cet Etat procède à la liquidation d’une pension nationale comme dans la première hypothèse.</li></ul>
<p>L’institution compétente de l’autre Etat procède alors à la liquidation d’une pension proportionnelle comme dans la deuxième hypothèse. L’intéressé reçoit une pension nationale et une pension proportionnelle.</p>
<h4 class="spip">Droits du travailleur et de ses ayants droit au cours d’un séjour temporaire en France ou d’un transfert de résidence (retour temporaire au cours d’une période d’arrêt de travail)</h4>
<h5 class="spip">Congés payés</h5>
<p>Un travailleur français occupé au Maroc bénéficie des prestations en espèces lors d’un séjour temporaire effectué en France, lors d’un congé payé, lorsque son état vient à nécessiter des soins médicaux d’urgence, y compris l’hospitalisation sans que la durée du service des prestations puisse excéder six mois, et sous réserve que la caisse marocaine ait donné son accord.</p>
<p>Le travailleur et ses ayants droit qui l’accompagnent, peuvent obtenir des prestations en nature pour des soins urgents, lors d’un séjour temporaire effectué à l’occasion des congés payés en France, sous réserve que la caisse marocaine ait donné son accord. Cette autorisation n’est valable que pour une durée maximum de trois mois, ce délai pouvant être prorogé pour une nouvelle période de trois mois par décision de l’institution d’affiliation, après avis favorable de son contrôle médical.</p>
<p>Pour bénéficier des soins, le travailleur se met en rapport avec la caisse primaire d’assurance maladie qui adresse une demande de prise en charge à l’institution marocaine d’affiliation (formulaire SE 350-27 "Droit aux prestations en nature - séjour du travailleur à l’occasion du congé payé").</p>
<p>Pour obtenir les prestations en espèces, le travailleur doit, dans un délai de sept jours suivant la date de délivrance du certificat d’arrêt de travail, se mettre en rapport avec la caisse primaire compétente, laquelle fait procéder à l’examen médical de l’intéressé et transmet le dossier à la caisse marocaine. Cette dernière notifie sa décision au moyen du formulaire SE 350-12.</p>
<h4 class="spip">Transfert de résidence en cas de maladie ou de maternité</h4>
<p>En cas de maladie ou de maternité, sous réserve de l’accord de l’institution marocaine d’affiliation laquelle tient dûment compte des motifs du transfert (raisons médicales notamment), un travailleur en cours d’indemnisation conserve le bénéfice des indemnités journalières lorsqu’il transfère sa résidence sur le territoire de l’autre Etat (formulaire SE 350-10 ou SE 350-11 en cas de prorogation).</p>
<p><strong>Soins de santé en cas de maladie </strong> : le travailleur salarié occupé au Maroc continue à bénéficier des soins en cas de transfert de résidence à condition que préalablement au départ, il ait obtenu l’autorisation de l’institution marocaine d’affiliation. Cette autorisation n’est valable que pour une durée maximum de trois mois, ce délai pouvant être prorogé pour une nouvelle période de trois mois par la caisse marocaine après avis favorable de son contrôle médical, voire au-delà en cas de maladie d’exceptionnelle gravité. Il aura donc intérêt à demander avant de quitter le Maroc l’établissement du formulaire SE 350-25 "Attestation du droit au bénéfice des prestations en nature des assurances maladie et maternité".</p>
<p><strong>Soins de santé en cas de maternité </strong> : l’autorisation est valable jusqu’à la fin du service des prestations de l’assurance maternité prévue par la législation française. Toutefois, en cas de grossesse pathologique ou de suites de couches pathologiques, ce délai peut être prorogé sur justifications et après avis du contrôle médical de la caisse marocaine.</p>
<h4 class="spip">Transfert de résidence en cas d’accident du travail</h4>
<p>Un travailleur français salarié au Maroc, admis aux prestations en nature et en espèces, conserve le bénéfice de ces prestations en cas de transfert de résidence en France à condition que préalablement au départ, il ait obtenu l’autorisation de la caisse marocaine laquelle tient compte des motifs de ce transfert (formulaire SE 350-15, le formulaire SE 350-16 étant utilisé en cas de demande de prorogation).</p>
<p>Les prestations en nature sont servies par la caisse primaire, les indemnités journalières étant versées directement par la caisse marocaine.</p>
<h4 class="spip">Droit de la famille demeurée en France</h4>
<h5 class="spip">Maladie, maternité</h5>
<p>Pour obtenir les soins, les membres de la famille doivent se faire inscrire auprès de la caisse primaire d’assurance maladie en présentant le formulaire SE 350-28 "Attestation pour l’inscription des familles" délivré par la caisse marocaine à la demande du travailleur. Cette attestation est valable douze mois et le travailleur doit se préoccuper de son renouvellement.</p>
<h5 class="spip">Prestations familiales</h5>
<p>Les travailleurs salariés occupés au Maroc peuvent prétendre pour leurs enfants demeurés en France aux allocations familiales transférables suivant un taux inclus dans un barème. Le paiement de ces allocations est limité à quatre enfants. Toutefois, dans le cadre de la législation interne française, leur famille a droit en sus à des allocations différentielles (article L 512-3 du code de la sécurité sociale).</p>
<p>Pour obtenir le versement des allocations familiales dans le cadre de la convention franco-marocaine, le travailleur doit remplir les conditions d’activité requises par la législation marocaine. Il doit se munir avant le départ d’un formulaire SE 350-03 "état de famille" établi par les autorités françaises compétentes en matière d’état civil (mairies) ainsi que de toutes pièces supplémentaires justifiant que les enfants considérés remplissent les conditions requises pour ouvrir droit aux allocations familiales. Ces pièces devront avoir été établies dans les trois mois précédant la date de leur production.</p>
<p>Le travailleur présente sa demande d’allocations familiales auprès de la caisse marocaine au moyen du formulaire SE 350-04, il fournit à l’appui de sa demande l’état de famille et les pièces justificatives en sa possession. Les états de famille sont renouvelés au 1er avril de chaque année et les allocations familiales sont servies dans la limite de quatre enfants, suivant un barème.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p>Pour vous informer sur la protection sociale des Français résidant à l’étranger, vous pouvez consulter notre thématique sur la <a href="services-aux-citoyens-preparer-son-expatriation-protection-sociale.md" class="spip_in">Protection sociale</a>.</p>
<p><strong>Maison des Français de l’étranger</strong><br class="manualbr">Bureau de la protection sociale<br class="manualbr">48 rue de Javel - 75015 Paris<br class="manualbr">Téléphone : 01 43 17 62 52 <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/protection-sociale/article/convention-de-securite-sociale#social#mc#mfe.org#" title="social..åt..mfe.org" onclick="location.href=mc_lancerlien('social','mfe.org'); return false;" class="spip_mail">Courriel</a></p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/protection-sociale/article/convention-de-securite-sociale). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
