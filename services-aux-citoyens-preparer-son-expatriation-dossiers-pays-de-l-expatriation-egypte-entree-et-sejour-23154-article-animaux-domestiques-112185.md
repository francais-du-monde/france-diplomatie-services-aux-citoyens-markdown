# Animaux domestiques

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/entree-et-sejour-23154/article/animaux-domestiques-112185#sommaire_1">Informations générales quand le pays de destination se trouve hors de l’Union européenne</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/entree-et-sejour-23154/article/animaux-domestiques-112185#sommaire_2">Légalisation des documents</a></li></ul>
<p>Les autorités égyptiennes exigent à l’entrée un certificat de bonne santé de moins de trois jours et un carnet de santé à jour.</p>
<p>On ne peut conseiller que d’être à jour dans les exigences européennes : puce et titrage antirabique ; en effet cette dernière analyse ne peut se faire que dans un laboratoire européen conventionné.</p>
<h3 class="spip"><a id="sommaire_1"></a>Informations générales quand le pays de destination se trouve hors de l’Union européenne</h3>
<p>Certains pays réglementent l’entrée des animaux sur leur territoire (permis d’importation, quarantaine, interdiction). Prévoyez un délai d’au moins dix jours pour effectuer toutes les formalités, voire de plusieurs mois pour les pays exigeant une quarantaine.</p>
<p>Pour connaître les conditions exactes, vous devrez prendre contact :</p>
<ul class="spip">
<li>avec <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-etrangers-en-france/article/annuaire-des-representations-etrangeres-en-france" class="spip_in">l’ambassade en France</a> du pays de destination. Si, au cours de son transport de la France vers le pays de destination, l’animal doit transiter par un ou plusieurs pays, vous devrez également vous conformer à la réglementation du ou des pays de transit de l’animal. A noter que certains pays refusent le transit d’animaux.
<p>Des informations générales sur la réglementation de nombreux pays sont également disponibles sur le site Internet de l’<a href="http://www.iatatravelcentre.com/" class="spip_out" rel="external">Association internationale du transport aérien (AITA)</a>.</p></li>
<li>le cas échéant, avec la ou les compagnies aériennes pour connaître les conditions de transport de l’animal (en soute ou en cabine, normes des cages, nourriture, etc.).
<p>Pour connaître les normes internationales de transport des animaux et des cages, vous pouvez consulter le site Internet de l’<a href="http://www.iata.org/" class="spip_out" rel="external">Association internationale du transport aérien (AITA)</a>. Si l’ambassade dispose d’une information particulière, celle-ci doit être communiquée à votre vétérinaire traitant, titulaire d’un mandat sanitaire, qui vous guidera pour la suite.</p></li></ul>
<p>Dans le cas où l’ambassade ne disposerait pas d’informations sur la réglementation sanitaire de son pays, vous devrez respecter, par défaut, les conditions suivantes :</p>
<ul class="spip">
<li>l’animal doit être en règle vis-à-vis des exigences réglementaires françaises en matière de santé et de protection animales. Pour plus de renseignements sur la réglementation française, vous pouvez prendre contact avec votre vétérinaire traitant, titulaire d’un mandat sanitaire, ou la Direction départementale des services vétérinaires (DDSV) de votre département. Vous trouverez les coordonnées des DDSV sur le site Internet du <a href="http://agriculture.gouv.fr/" class="spip_out" rel="external">ministère de l’Agriculture</a>.</li>
<li>les documents suivants sont obligatoires pour l’animal et doivent être établis par le vétérinaire traitant, titulaire d’un mandat sanitaire :
<br>— <strong> identification par micropuce ou tatouage</strong> ;
<br>— <strong>certificat de vaccination contre la rage en cours de validité</strong> ;
<br>— <strong>certificat international de bonne santé, établi par le vétérinaire traitant, titulaire du mandat sanitaire, dans la semaine précédant le départ de France</strong>.</li>
<li>Il est également conseillé de faire <strong>procéder à un titrage des anticorps anti-rabiques </strong>dans un laboratoire agréé et de se munir du carnet de vaccination tenu à jour de l’animal. Vous devrez ensuite prendre rendez-vous avec le service " santé et protection animales " de la Direction départementale des services vétérinaires (DDSV) dont relève le vétérinaire traitant, pour la validation des documents établis par celui-ci.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Légalisation des documents</h3>
<p>Certains pays exigent que les documents validés par la DDSV soient ensuite légalisés ou munis de l’apostille. Il convient donc de se renseigner sur ce point auprès de l’<a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-etrangers-en-france/article/annuaire-des-representations-etrangeres-en-france" class="spip_in">ambassade du pays de destination</a>.</p>
<p>Pour connaître le régime de légalisation du pays de destination, vous pouvez également consulter le site Internet du <a href="services-aux-citoyens-legalisation-et-notariat-legalisation-et-certification-de-signatures.md" class="spip_in">ministère des Affaires étrangères</a>.</p>
<p>L’apostille s’obtient auprès des cours d’appels. Vous pouvez trouver leurs coordonnées sur le site Internet du <a href="http://www.justice.gouv.fr/" class="spip_out" rel="external">ministère de la Justice</a>.</p>
<p>La légalisation est effectuée par le bureau des légalisations du ministère des affaires étrangères. Pour toute information sur les légalisations, vous pouvez consulter le site Internet du <a href="services-aux-citoyens-legalisation-et-notariat-legalisation-et-certification-de-signatures.md" class="spip_in">ministère des Affaires étrangères</a> ou contacter :</p>
<p><strong>Le bureau des légalisations</strong><br class="manualbr">57 boulevard des Invalides <br class="manualbr">75007 Paris<br class="manualbr">Téléphone (de 14 à 16 heures) : 01 53 69 38 28 / 01 53 69 38 29 <br class="manualbr">Télécopie : 01 53 69 38 31</p>
<p>Pour toute information complémentaire, vous pouvez consulter le site Internet du <a href="http://agriculture.gouv.fr/" class="spip_out" rel="external">ministère de l’Agriculture</a>.</p>
<p><i>Mise à jour : avril 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/entree-et-sejour-23154/article/animaux-domestiques-112185). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
