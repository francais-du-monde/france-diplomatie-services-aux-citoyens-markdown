# Scolarisation

<p>Pour toute information sur la scolarisation dans le système français à l’étranger, vous pouvez consulter notre thématique sur les <a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures.md" class="spip_in">Etudes supérieures</a> et la <a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais.md" class="spip_in">Scolarité en français</a>.</p>
<p>Vous y trouverez des renseignements sur :</p>
<ul class="spip">
<li>les établissements français du primaire et du secondaire à l’étranger ;</li>
<li>les bourses scolaires et la prise en charge des frais de scolarité à l’étranger ;</li>
<li>les possibilités qui s’offrent à vous si votre enfant ne peut être scolarisé à l’étranger dans le système français (enseignement à distance par le CNED, programme français langue maternelle (FLAM), internats en France) ;</li>
<li>les épreuves du baccalauréat à l’étranger ;</li>
<li>les bourses d’études supérieures en France et à l’étranger ;</li>
<li>l’équivalence des diplômes.</li></ul>
<p>Pour en savoir plus :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Consultez le site de l’<a href="http://www.aefe.fr/" class="spip_out" rel="external">Agence pour l’enseignement français à l’étranger</a></p>
<h4 class="spip">Les établissements scolaires français au Vietnam</h4>
<ul class="spip">
<li><a href="http://www.lfay.com.vn/site_spip/" class="spip_out" rel="external">Lycée français Alexandre Yersin à Hanoi</a></li>
<li><a href="http://lfiduras.com/" class="spip_out" rel="external">Lycée français Marguerite Duras à Ho Chi Minh Ville</a></li></ul>
<h4 class="spip">Enseignement supérieur</h4>
<p>Il est possible de suivre localement des études supérieures à Hanoï cependant la maîtrise du vietnamien est indispensable et le concours d’entrée à l’université est très sélectif. Il est toutefois possible de suivre des cursus en français dans des formations françaises délocalisées, ou en anglais dans de nombreux établissements (par exemple, Centre franco-vietnamien de Gestion).</p>
<p>De nombreux accords de coopération inter-universitaires franco-vietnamiens existent et les échanges d’étudiants figurent parmi les priorités que se sont fixées les autorités françaises et vietnamiennes en matière de coopération internationale.</p>
<p>Pour en savoir plus :</p>
<ul class="spip">
<li>Site de <a href="http://www.ambafrance-vn.org/-Francais-" class="spip_out" rel="external">l’ambassade de France au Vietnam</a></li>
<li>Site du <a href="http://www.consulfrance-hcm.org/article.php3?id_article=82" class="spip_out" rel="external">consulat de France à Ho Chi Minh</a></li></ul>
<p><i>Mise à jour : février 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/vie-pratique/article/scolarisation). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
