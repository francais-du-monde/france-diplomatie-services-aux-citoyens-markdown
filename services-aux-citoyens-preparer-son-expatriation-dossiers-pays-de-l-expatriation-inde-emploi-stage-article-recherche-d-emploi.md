# Recherche d’emploi

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/emploi-stage/article/recherche-d-emploi#sommaire_1">Outils pour la recherche d’emploi</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/emploi-stage/article/recherche-d-emploi#sommaire_2">Organismes pour la recherche d’emploi</a></li></ul>
<p>Les avis de recrutement susceptibles d’intéresser les ressortissants étrangers sont publiés dans certains journaux ou revues économiques spécialisés. Les candidats à une expatriation en Inde peuvent déposer leur candidature auprès des grands cabinets de recrutement internationaux implantés dans le pays. Ces cabinets, dans la grande majorité américains ou anglais, sont souvent chargés de trouver les dirigeants ou techniciens de haut niveau pour leurs clients indiens.</p>
<h3 class="spip"><a id="sommaire_1"></a>Outils pour la recherche d’emploi</h3>
<h4 class="spip">Journaux</h4>
<ul class="spip">
<li>Times India Ascent</li>
<li><a href="http://economictimes.indiatimes.com/" class="spip_out" rel="external">The Economic Times</a>,</li>
<li><a href="http://www.businessworld.in/" class="spip_out" rel="external">Business World</a></li></ul>
<h4 class="spip">Sites internet</h4>
<ul class="spip">
<li><a href="http://www.naukri.com/" class="spip_out" rel="external">Naukri</a>,</li>
<li><a href="http://www.monsterindia.com/" class="spip_out" rel="external">Monster</a></li>
<li>Times Job</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Organismes pour la recherche d’emploi</h3>
<p>Cabinet de placements : Michael Page, KTA Associates, Global Hunt, Hewitt</p>
<p>Service de placement des Français en Inde de la <a href="http://www.ifcci.org.in/fr/" class="spip_out" rel="external">Chambre de commerce et d’industrie franco-indienne</a> (IFCCI)</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/emploi-stage/article/recherche-d-emploi). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
