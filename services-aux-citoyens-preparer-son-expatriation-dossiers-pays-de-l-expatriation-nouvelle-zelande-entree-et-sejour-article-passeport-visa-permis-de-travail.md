# Passeport, visa, permis de travail

<p>Quel que soit le motif de votre séjour, consultez le site du Ministère de l’Intérieur Néo-zélandais et renseignez-vous auprès de la section consulaire de l’ambassade de Nouvelle-Zélande à Paris qui vous informera sur les modalités d’entrée et de séjour en Nouvelle-Zélande.</p>
<p><strong>En aucun cas, vous n’êtes autorisé à travailler en Nouvelle-Zélande sans permis adéquat. </strong></p>
<p>Le consulat de France en Nouvelle-Zélande n’est pas l’organisme compétent pour répondre à vos demandes d’information concernant votre séjour en Nouvelle-Zélande.</p>
<p>Les ressortissants français peuvent séjourner en tant que touristes en Nouvelle-Zélande <strong>jusqu’à trois mois sans visa</strong>, à condition de disposer d’un billet d’avion aller et retour valide, d’un passeport en cours de validité et de la preuve de leur viabilité financière (minimum de $NZ 1000 par mois et par personne ou, si vous logez chez un particulier, minimum de $NZ 500).</p>
<p>Il y a, pour les <strong>séjours de plus de trois mois</strong>, plusieurs types de visa dont les pièces justificatives à fournir peuvent varier. Pour obtenir un permis de résidence, les services de l’immigration prennent en compte les conditions d’entrée sur le territoire, les motifs du séjour, les possibilités d’assurer son indépendance financière, les liens familiaux ou les garants présentés, etc…</p>
<p>Généralement, il faut compter entre un et trois mois pour obtenir un permis de travail. Les conditions sont identiques pour le conjoint ou le concubin.</p>
<p>L’<a href="http://www.immigration.govt.nz/" class="spip_out" rel="external">Immigration New Zealand (INZ)</a>, service néo-zélandais de l’immigration, est l’agence gouvernementale responsable de la politique d’immigration.</p>
<p>L’INZ a un bureau à l’Ambassade de Nouvelle-Zélande à Paris (Contact entre 9h00 et 12h30 du lundi au vendredi au (+33) (0)1 45 01 43 42).</p>
<h4 class="spip">Les différents types de visa</h4>
<p>Le futur employeur néo-zélandais doit fournir un contrat et un descriptif de l’emploi offert, précisant les qualifications requises, la durée du contrat, la rémunération offerte, la prise en charge d’une prévoyance sociale ou autre, les modalités de rapatriement en fin de contrat, etc. Il doit également contacter un bureau du service d’immigration (NZIS) en Nouvelle-Zélande afin de demander un accord de principe pour embaucher un travailleur étranger.</p>
<p>Il est nécessaire de faire la demande d’un <strong>numéro IRD</strong> auprès de l’<a href="http://www.ird.govt.nz/how-to/irdnumbers/" class="spip_out" rel="external">Inland Revenue (le fisc néo-zélandais)</a>. Ce dernier vous sera demandé par votre employeur et pour l’ouverture d’un compte bancaire.</p>
<h4 class="spip">Visa de travail ou "Work Visa"</h4>
<p>Vous pouvez demander un <strong>visa de travail temporaire</strong> si vous remplissez l’une des conditions suivantes :</p>
<ul class="spip">
<li>vous avez une offre d’emploi par un employeur néo-zélandais ;</li>
<li>vous êtes étudiant et allez faire un stage en NZ ;</li>
<li>vous voulez rejoindre votre partenaire en NZ et y travaillez ;</li>
<li>vous devez participer a un événement précis en NZ (ex. tournoi sportif/spectacle etc.) et pour lequel vous avez un CDD.</li></ul>
<p>Le visa de travail, apposé sur le passeport et attestant que le titulaire dispose de l’autorisation de travailler en Nouvelle-Zélande pour une durée déterminée, doit être obtenu avant de rentrer en Nouvelle-Zélande.</p>
<p>L’obtention du visa de travail temporaire n’est jamais garanti car c’est le NZIS qui prend la décision finale. Ainsi, ne faites aucune réservation de voyage pour la Nouvelle-Zélande avant d’avoir obtenu le visa de travail temporaire.</p>
<p>Le visa de travail comporte parfois des contraintes concernant par exemple le type de travail recherché ou le lieu demandé. Le titulaire d’un visa de travail ne pourra rester plus de trois ans en Nouvelle-Zélande. Au-delà de cette période, celui-ci devra obtenir un permis de résidence s’il désire rester dans le pays.</p>
<p><strong>Les demandeurs de stage</strong> qui sont actuellement inscrits dans un institut d’enseignement supérieur d’Etat au niveau tertiaire (IUT, université, grande école, polytechnique) en France peuvent faire une <strong>demande de visa de travail</strong> pour effectuer un stage en Nouvelle-Zélande.</p>
<p>La demande de visa de travail temporaire pour effectuer un stage doit être obligatoirement accompagnée d’une <strong>copie de la convention de stage</strong> (précisant les fonctions que vous exercerez, la durée du contrat et signée par vous, votre institut d’enseignement et l’employeur en Nouvelle-Zélande) et, éventuellement, du contrat offert par l’employeur.</p>
<p>Si la convention de stage ne précise pas que vous serez couvert(e) par une assurance étudiant pendant votre séjour, il faudra joindre à votre demande de visa une documentation attestant que vous bénéficiez d’une <strong>assurance santé</strong> couvrant l’ensemble des risques liés à la maladie-hospitalisation-invalidité en Nouvelle-Zélande pendant la durée de votre séjour (consultez votre assureur habituel, votre fournisseur de carte bancaire ou votre agence de voyages).</p>
<p>Si votre stage n’est pas rémunéré ou inférieur au salaire minimum, vous devez fournir la preuve que vous pouvez subvenir à vos frais de séjour à la hauteur de $NZ 1000/mois (environ 600 €/mois) avec une <strong>attestation bancaire </strong>(si celle-ci est de vos parents, une garantie parentale et une copie de leur carte d’identité ou passeport). A titre indicatif, au 1er avril 2008, le salaire minimum légal ("minimum wage") pour les personnes âgées de 16 ans ou plus est de $NZ384 par semaine de 40 heures.</p>
<p>La durée du stage ne pourra dépasser six mois.</p>
<h4 class="spip">Permis de travail ou "Work Permit"</h4>
<p>Un permis de travail est exigé pour tout visiteur désirant travailler en Nouvelle-Zélande, même de manière temporaire. Le permis de travail figure aussi dans le passeport et s’obtient une fois en possession du visa de travail et une fois débarqué sur le sol néo-zélandais. Il contient des informations sur l’emploi, le nom de l’employeur et le secteur géographique dans lequel son titulaire est autorisé à travailler. Il est rarement restrictif.</p>
<p>Le service d’immigration exige certaines preuves, telles que l’aptitude ou la qualification qu’aucun Néo-Zélandais résidant en Nouvelle-Zélande ne corresponde au profil recherché pour l’offre d’emploi souhaitée. Les demandes sont examinées à la lumière de la situation du marché et de l’emploi local.</p>
<h4 class="spip">Visa Vacances-Travail (dit VVT) ou "Working Holiday Scheme"</h4>
<p>Les <strong>jeunes de 18 à 30 ans </strong>peuvent bénéficier du <strong>Visa-Vacances-Travail</strong> (accord de réciprocité signé entre les deux pays le 2 juin 1999).</p>
<p>Les autorités néo-zélandaises autorisent les jeunes Français à séjourner en Nouvelle-Zélande à titre individuel dans le but d’y passer des vacances, en ayant la possibilité d’y exercer un emploi pour compléter les moyens financiers dont ils disposent. Elles leur délivrent à cet effet un visa vacances-travail à entrées multiples d’une <strong>durée de validité d’un an.</strong></p>
<p>A l’arrivée en Nouvelle-Zélande, les bénéficiaires du programme se voient délivrer par le service de l’immigration une autorisation de travail valable douze mois : celle-ci leur permet d’exercer un emploi auprès de plusieurs employeurs successifs, et aussi de s’inscrire à un cours de formation ou d’étude pour une durée maximale de trois mois non renouvelable. Ils sont tenus de se conformer à la législation locale du travail.</p>
<p>Pour ceux qui peuvent prouver qu’ils ont travaillé dans les domaines de l’agriculture, horticulture ou viticulture pendant au moins 3 mois, une extension de 3 mois supplémentaires de leur VVT peut être accordée.</p>
<p>Afin de faire une demande de VVT, vous devez :</p>
<ul class="spip">
<li>ne pas avoir bénéficié antérieurement de ce programme ;</li>
<li>être titulaire d’un passeport français ;</li>
<li>être âgé de 18 à 30 ans révolus à la date du dépôt de la demande de VVT ;</li>
<li>ne pas avoir ou être accompagné d’enfants ou de dépendants.</li></ul>
<p>Les demandes de VVT se font en ligne sur le site de l’<a href="https://www.immigration.govt.nz/secure/default.htm" class="spip_out" rel="external">Immigration</a>.</p>
<p>Pour toute information complémentaire, se reporter au site internet de <a href="http://www.nzembassy.com/fr/france" class="spip_out" rel="external">l’ambassade de Nouvelle-Zélande</a>.</p>
<h4 class="spip">Légalisation des documents</h4>
<p>Certains pays exigent que les documents validés par la DDSV soient ensuite légalisés ou munis de l’Apostille. Il convient donc de se renseigner sur ce point auprès de l’<a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-etrangers-en-france/article/annuaire-des-representations-etrangeres-en-france" class="spip_in">ambassade du pays de destination</a>.</p>
<p>L’apostille s’obtient auprès des cours d’appels. Vous pouvez trouver leurs coordonnées sur le <a href="http://www.justice.gouv.fr/" class="spip_out" rel="external">site Internet du ministère de la Justice</a>.</p>
<p>La légalisation est effectuée par le bureau des légalisations du ministère des affaires étrangères. Pour toute information sur les légalisations, vous pouvez consulter le <a href="services-aux-citoyens-legalisation-et-notariat-legalisation-et-certification-de-signatures.md" class="spip_in">site Internet du ministère des Affaires étrangères</a>.</p>
<p><strong>Bureau des légalisations</strong><br class="manualbr">57 boulevard des Invalides <br class="manualbr">75007 Paris<br class="manualbr">Tél. (de 14 à 16 heures) : 01 53 69 38 28 / 01 53 69 38 29 <br class="manualbr">Télécopie : 01 53 69 38 31</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Pour toute information complémentaire, vous pouvez consulter le <a href="http://agriculture.gouv.fr/" class="spip_out" rel="external">site Internet du ministère de l’Agriculture et de la Pêche</a>.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/entree-et-sejour/article/passeport-visa-permis-de-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
