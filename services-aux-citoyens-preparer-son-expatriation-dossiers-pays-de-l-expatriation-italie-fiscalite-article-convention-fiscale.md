# Convention fiscale

<p>Il n’existe pas en Italie d’équivalent du code général des impôts français. Toutefois, en matière de législation fiscale on peut se référer aux textes de base suivants :</p>
<ul class="spip">
<li>en matière d’impôts directs : au TUIR (texte unique des impôts sur les</li>
<li>en matière de TVA : au DPR n° 633 du 26.10.72 tel que modifié ultérieurement.</li></ul>
<p>Deux conventions fiscales ont été signées entre l’Italie et la France :</p>
<ul class="spip">
<li>la convention du 5 octobre1989 en matière d’impôts sur le revenu et sur la fortune,</li>
<li>la convention du 20 décembre 1990 en matière d’impôts sur les successions et les donations.</li></ul>
<p>La convention franco-italienne en matière d’impôts sur le revenu et sur la fortune s’applique aux personnes physiques, aux sociétés et associations de personnes résidant dans un des Etats ou dans les deux Etats contractants. Elle précise le régime fiscal des divers types de revenus.</p>
<p>Le texte de la convention et de ses avenants peut être obtenu à la Direction des Journaux Officiels, par courrier : <br class="manualbr">26 rue Desaix <br class="manualbr">75727 Paris cedex 15,<br class="manualbr">par télécopie : 01 40 58 77 80</p>
<p>ou sur le site <i>internet</i> du <a href="http://www.impots.gouv.fr/portal/deploiement/p1/fichedescriptive_1736/fichedescriptive_1736.pdf" class="spip_out" rel="external">ministère des Finances</a>.</p>
<p>Les dispositions conventionnelles qui ont primauté sur les dispositions du droit interne, selon l’article 55 de la Constitution française, répartissent entre les deux Etats le droit d’imposer les revenus perçus par leurs résidents respectifs.</p>
<h4 class="spip">Champ d’application de la convention</h4>
<h4 class="spip">Personnes concernées et impôts visés (art. 1 et 2 de la convention)</h4>
<p>Cet accord a pour objet de protéger <strong>les résidents de chacun des Etats contractants </strong>en vue d’éviter les doubles impositions en matière d’impôts sur le revenu et sur la fortune perçus pour le compte d’un Etat, de ses subdivisions politiques ou administratives ou collectivités locales (Italie) ou de ses collectivités territoriales (France) quel que soit le système de perception.</p>
<p>La convention trouve donc à s’appliquer aux résidents de ces Etats.</p>
<h4 class="spip">Notion de résidence</h4>
<p>L’article 4, paragraphe 1 précise que l’expression « résident d’un Etat » désigne la personne qui, en vertu de la législation de cet Etat, y est assujettie à l’impôt, en raison de son domicile, de sa résidence, de son siège de direction ou de tout autre critère de nature analogue.</p>
<p>Au paragraphe 2, l’article 4 énumère des critères subsidiaires permettant de résoudre le cas de double résidence si l’assujettissement à l’impôt ne pouvait suffire. Ces critères sont :</p>
<ul class="spip">
<li>le foyer d’habitation permanent</li>
<li>le centre de ses intérêts vitaux (Etat où les liens personnels et économiques sont les plus étroits)</li>
<li>le lieu de séjour habituel</li>
<li>à défaut, l’Etat dont elle possède la nationalité.</li></ul>
<p>Si une personne possède la double nationalité ou si elle ne possède la nationalité d’aucun d’eux, la question est tranchée d’un commun d’accord par les autorités des deux Etats contractants (art. 4, paragraphe 2d).</p>
<h4 class="spip">Elimination de la double imposition</h4>
<p>La double imposition est évitée selon un dispositif prévu par l’article 24 de la convention.</p>
<p><strong>Dans le cas de la France</strong> :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  le bénéficiaire de bénéfices et revenus provenant d’Italie, imposables dans cet Etat selon la convention et également imposables en France lorsqu’ils reviennent à un résident en France, a droit à un crédit d’impôt imputable sur l’impôt français déterminé dans les conditions prévues au paragraphe 1 de l’art. 24.</p>
<p>Il en est de même pour les résidents de France qui possèdent une fortune imposable en Italie.</p>
<p><strong>Dans le cas de l’Italie</strong> :</p>
<p>Les revenus perçus par un résident d’Italie imposables en France peuvent être compris dans la base imposable en Italie, sauf disposition contraire prévue par la convention.</p>
<p>Dans ce cas, l’impôt sur les revenus payé en France est déduit de l’impôt établi en Italie dans les conditions prévues au paragraphe 2 de la convention.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/fiscalite/article/convention-fiscale). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
