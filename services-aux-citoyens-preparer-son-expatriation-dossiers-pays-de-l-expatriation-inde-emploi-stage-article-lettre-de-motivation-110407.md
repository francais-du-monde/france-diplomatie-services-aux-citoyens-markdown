# Lettre de motivation

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/emploi-stage/article/lettre-de-motivation-110407#sommaire_1">Rédaction</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/emploi-stage/article/lettre-de-motivation-110407#sommaire_2">Modèles de lettre de motivation</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Rédaction</h3>
<p>Lorsque vous vous portez candidat à un emploi en Inde, vous pouvez utiliser tant l’hindi que l’anglais, selon la société ciblée et votre niveau linguistique. Cependant, il faut savoir que des notions d’hindi pourront être requises pour la plupart des postes dans les entreprises indiennes.</p>
<p>Prenez le temps de vous renseigner sur l’organisation de l’employeur auprès duquel vous souhaitez obtenir un emploi et personnalisez votre lettre de motivation en fonction de cette société, si possible en mentionnant le nom du responsable auquel vous écrivez.</p>
<p>Dans la plupart des cas, une lettre de motivation est utilisée en guise d’introduction et devrait accompagner votre CV, qu’elle soit envoyée par mail ou par courrier, déposée sur un site internet de recherche d’emploi ou remise en mains propres au responsable de l’employeur.</p>
<p>La lettre de motivation sera la première chose que l’employeur verra ; il est donc important de soigner sa présentation en la personnalisant et en la rédigeant proprement pour retenir l’attention de votre employeur et l’inciter à regarder de plus près votre CV.</p>
<p>Une lettre de motivation fait partie intégrante de votre candidature pour un emploi. Elle doit faire référence à vos projets présents et futurs, pour démontrer à vos potentiels employeurs ce que vous pouvez faire pour eux maintenant et à l’avenir. Cela les confortera dans l’idée que vous vous inscrivez dans les perspectives à long terme de leurs entreprises.</p>
<p>La lettre de motivation devrait être dactylographiée dans un style utilisant des mots puissants et des verbes d’action. Normalement, elle est composée de trois ou quatre paragraphes et n’est jamais plus longue qu’une page. Il convient d’être concis et aller droit au but. Il est préférable de couper tout paragraphe d’une longueur supérieure à sept lignes, pour rendre la lettre de motivation plus compréhensible.</p>
<p>Dans une lettre de motivation, indiquez votre nom, votre nationalité (si vous êtes étranger) et vos coordonnées (adresse, téléphone/fax et e-mail). Souvent, les lettres de motivation sont conservées longtemps dans les dossiers, les contacts mentionnés devront donc être le plus précis possible sur le long terme.</p>
<p>Prévoyez une ligne qui explique l’objet de votre lettre. Expliquez votre motivation et pourquoi vous pensez être la personne adéquate pour cet emploi. Terminez votre lettre de motivation en exprimant votre souhait de rencontrer le recruteur pour un entretien.</p>
<p>Incluez toujours des expressions courtes, polies et formelles dans vos lettres de motivation mais n’ajoutez jamais un document officiel (diplômes ou lettres de recommandation).</p>
<p>Vérifiez l’orthographe et la grammaire utilisées dans votre lettre de motivation, en utilisant le correcteur automatique de votre logiciel de traitement de texte ou, au besoin, en la faisant corriger par un professionnel.</p>
<p>Il est important que votre lettre de motivation soit un outil de promotion de vos meilleures qualifications en rapport avec les caractéristiques de l’emploi recherché. Si elle est bien rédigée, cela incitera l’employeur à lire votre CV.</p>
<p>Ne pensez pas que les recruteurs, qui reçoivent des centaines de lettres de motivation pour chaque emploi, retiendront celles qui sont mal rédigées et qui n’expliquent pas l’intérêt que votre candidature peut représenter pour leur entreprise.</p>
<h3 class="spip"><a id="sommaire_2"></a>Modèles de lettre de motivation</h3>
<p>Name (Nom)<br class="manualbr">Mailing Address (Adresse)<br class="manualbr">City, state/province, zip/post code and country(Code postal, ville, pays)<br class="manualbr">Phone/Fax number(s) (N° de téléphone/fax)<br class="manualbr">Email address (Adresse e-mail)</p>
<p>Date of writing (Date)</p>
<p>Recipient name (Nom du recruteur)<br class="manualbr">Professional title (Titre professionnel)<br class="manualbr">Organization name (Nom de la société)<br class="manualbr">Mailing address (Adresse mail)<br class="manualbr">City, state/province, zip/post code and country (Code postal, ville, pays)</p>
<p>Dear Mr./Ms. Last Name</p>
<p>I am writing to apply for the position of Receptionist as advertised in the March 1, 2010 Victorian Times. I believe that my experience as a receptionist and office assistant with farm supply companies would be an asset to your company.</p>
<p>When reviewing your company website, I learned that Maxwell is a company I would feel very capable working for. My past work experience has taught me how to work pleasantly and efficiently with people, even when working under pressure. I work well in team environments and can juggle many tasks at once. My word processing abilities are quick and accurate.</p>
<p>In addition to secretarial training, I have taken a variety of night school courses in switchboard operation, Windows 7, Word and Excel spreadsheets. I also have Level 2 First Aid, which has proven an asset in industrial settings.</p>
<p>I am very interested in becoming a receptionist for your company and would like to meet with you at your convenience to discuss this opportunity.</p>
<p>Sincerely,</p>
<p>(Handwritten signature) Signature<br class="manualbr">Your name, typed (Prénom, nom)<br class="manualbr">Enclosure : resume (CV)</p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/emploi-stage/article/lettre-de-motivation-110407). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
