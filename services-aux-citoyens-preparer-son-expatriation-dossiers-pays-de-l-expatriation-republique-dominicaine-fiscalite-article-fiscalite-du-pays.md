# Fiscalité du pays

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/fiscalite/article/fiscalite-du-pays#sommaire_1">Année fiscale</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/fiscalite/article/fiscalite-du-pays#sommaire_2">Liste des principaux impôts</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Année fiscale</h3>
<p>L’année fiscale commence le 1er janvier et s’achève le 31 décembre pour les particuliers.</p>
<p>Les entreprises peuvent arrêter la date de clôture de l’exercice fiscal au 31 mars, au 30 juin ou au 30 septembre.</p>
<p>Les déclarations d’impôt sur les bénéfices doivent être déposées auprès des antennes locales de la Direction générale des impôts internes (<i>Dirección General de Impuestos Internos</i>).</p>
<h3 class="spip"><a id="sommaire_2"></a>Liste des principaux impôts</h3>
<h4 class="spip">Impôts sur le revenu, les bénéfices et les plus-values</h4>
<p><strong>Impôt sur le revenu, prélevé à la source</strong></p>
<p>La loi 11-92 grève les revenus obtenus en République dominicaine par toute personne physique nationale ou étrangère. De même, le Code fiscal grève certains revenus de source étrangère. Les personnes étrangères doivent seulement payer des impôts sur les revenus de source dominicaine et, après la troisième année de résidence dans le pays, sur les revenus financiers de source étrangère.</p>
<p>Cette échelle salariale sera ajustée chaque année en fonction de l’inflation à partir 2016. Pour les exercices 2013, 2014 et 2015, cet ajustement ne s’applique pas.</p>
<p>Pour les entreprises, le taux en vigueur en 2013 est de 29%. Il existe des exemptions notamment pour les entreprises des zones franches et les investissements dans le tourisme.</p>
<p><strong>Impôts sur la plus-value</strong></p>
<p>La plus-value est la valeur résultant de la soustraction du prix de vente ou de la valeur de la propriété, moins le coût d’acquisition ajusté de l’inflation. Si le résultat est positif, ce montant pourrait être un gain en capital assujetti à l’impôt sur le revenu au taux de 29%.</p>
<h4 class="spip">Taxes sur la propriété</h4>
<p><strong>Taxe sur les actifs</strong></p>
<p>Il s’agit d’une taxe annuelle de 1 % prélevée sur le montant total des actifs des personnes juridiques.</p>
<p><strong>IPI / VSS - Taxe sur le patrimoine immobilier, loi n° 18-88 et ses modifications</strong></p>
<p>Il s’agit d’une taxe annuelle de 1% prélevée sur le montant total des actifs immobiliers des particuliers (incluant le foncier), dont la valeur est supérieure à 6,5 millions de pesos. Elle s’applique également aux établissements commerciaux professionnels et industriels appartenant à des particuliers.</p>
<p><strong>Impôt sur le transfert de biens immobiliers, loi n° 288-04 et ses amendements</strong></p>
<p>Le transfert d’une propriété en République dominicaine est soumis au paiement d’une taxe de transfert de propriété de 3% sur la valeur du bien. Cette taxe est payée par l’acheteur, sauf indication contraire, dans un délai ne dépassant pas six mois après la vente, à défaut s’applique une majoration et des intérêts.</p>
<p><strong>Impôt sur l’enregistrement des véhicules, loi n° 557-05 et ses amendements</strong></p>
<p><strong>Impôt sur les héritages et les donations, loi n° 25-69 et ses modifications</strong></p>
<p><strong>Impôt additionnel sur les opérations immobilières transactions immobilières, contenues dans la loi n° 3341 et ses amendements</strong></p>
<h4 class="spip">Taxe sur la valeur ajoutée</h4>
<p>Conformément à la loi n° 147-00 du 26 décembre 2000, l’<strong>ITBIS</strong> (<i>Impuesto sobre las Transferencias de Bienes Industrializados y Servicios</i>) est une taxe sur la valeur ajoutée.</p>
<p>Le taux principal est de 18% pour les années 2013 et 2014, 2015 puis sera réduit à 16%. Les produits de bases ne sont pas soumis à cette taxe ou sont soumis à un taux réduit (11% en janvier 2014).</p>
<h4 class="spip">Droits d’accise</h4>
<ul class="spip">
<li>Impôt sélectif à la consommation (ISC) : principalement sur l’alcool et les cigarettes</li>
<li>Impôt sur les hydrocarbures, loi n° 112-00 et ses amendements</li>
<li>Impôt Ad-Valorem sur la consommation domestique de combustibles fossiles, loi n° 557-05 et ses amendements</li></ul>
<h4 class="spip">Les droits de douane</h4>
<p>La République Dominicaine applique deux accords de libre-échange :</p>
<ul class="spip">
<li>Le DR CAFTA avec les pays centroaméricains et les Etats-Unis ;</li>
<li>L’Accord de partenariat économique entre l’UE et le CARIFORUM (CARICOM +RD).</li></ul>
<h4 class="spip">Exemption</h4>
<ul class="spip">
<li>Importations effectuées par des organismes ou institutions publiques, sous réserve de l’autorisation de la Présidence, et réalisées en cas d’urgence pour la sécurité nationale ou en cas de force majeure.</li>
<li>Donations faites par des institutions étrangères et les envois de secours.</li>
<li>Les effets personnels de dominicains rentrants au pays.</li>
<li>Les échantillons dont l’entrée est justifiée à des fins d’exposition.</li>
<li>Les présentoirs et catalogues dont l’importation est réalisée par des sociétés légalement établies.</li>
<li>Les ordinateurs personnels, leurs composants et logiciels.</li>
<li>Les matières premières, le matériel d’emballage, les machines et équipements et pièces de rechange destinés à l’industrie pharmaceutique ou vétérinaire, quand les achats émanent d’un laboratoire pharmaceutique.</li></ul>
<p>Des exemptions existent également pour les importations destinées aux zones franches.</p>
<h4 class="spip">Formalités de dédouanement</h4>
<p>Dès l’arrivée des marchandises, au port ou à l’aéroport, il faut présenter les documents suivants :</p>
<ul class="spip">
<li>Facture consulaire (sur imprimés spéciaux délivrés par le consulat dominicain),</li>
<li>Facture commerciale (3 exemplaires),</li>
<li>Connaissement d’embarquement (5 exemplaires),</li>
<li>Certificat d’origine.</li></ul>
<p>La facture commerciale (Pro Forma) doit être accompagnée d’un formulaire délivré par la Banque Centrale (C1, C2 ou C3). Ce formulaire, pour lequel il faut payer 4,75 % de la valeur en dollars du montant total, et sans lequel aucune marchandise ne peut être dédouanée, définit le statut de la marchandise : C1 - paiement par lettre de crédit ; C2 - paiement effectué après la réception de la marchandise ; C3 - paiement des marchandises sur plusieurs livraisons.</p>
<p>La facture commerciale, le connaissement d’embarquement et le certificat d’origine doivent être visés par le consulat de la République Dominicaine et la Chambre de commerce et d’industrie de la localité du fabricant ou autre organisme officiel compétent.</p>
<h4 class="spip">Durée du dédouanement</h4>
<p>La durée moyenne de dédouanement peut aller de quatre à six jours ouvrables à trois semaines. Cependant, pour les marchandises périssables, une procédure accélérée peut être mise en place. Pour cela, il faut déposer une caution à la DGA avant l’arrivée des marchandises.</p>
<p>Il est vivement conseillé de faire appel à un transitaire pour le dédouanement des marchandises.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.dgii.gov.do/Paginas/Index.aspx" class="spip_out" rel="external">Direction général des impôts</a></li>
<li><a href="https://www.aduanas.gob.do/" class="spip_out" rel="external">Douanes</a></li>
<li><a href="http://www.sice.oas.org/ctyindex/DOM/DOMagreements_s.asp" class="spip_out" rel="external">Traités de libre échange</a></li>
<li><a href="http://www.ccifranco-dominicana.org/" class="spip_out" rel="external">Chambre de commerce franco-dominicaine</a></li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/fiscalite/article/fiscalite-du-pays). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
