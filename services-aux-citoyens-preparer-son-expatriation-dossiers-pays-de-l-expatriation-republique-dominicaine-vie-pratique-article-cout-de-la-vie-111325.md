# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/vie-pratique/article/cout-de-la-vie-111325#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/vie-pratique/article/cout-de-la-vie-111325#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/vie-pratique/article/cout-de-la-vie-111325#sommaire_3">Alimentation</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/vie-pratique/article/cout-de-la-vie-111325#sommaire_4">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>L’unité monétaire est le peso dominicain (DOP).</p>
<p>Taux de change officiel de la Banque Centrale (taux acheteur moyen sur novembre 2013) :</p>
<ul class="spip">
<li>1 USD = 42,46 DOP</li>
<li>1 EUR = 57,29 DOP</li></ul>
<p>Par sa politique d’interventions ponctuelles sur le marché des changes, la Banque Centrale de République Dominicaine (BCRD) a réussi à diminuer significativement la volatilité du peso. L’ancrage vis-à-vis du dollar a fortement limité les fluctuations du cours, bien que le peso soit sur une dynamique de dépréciation tendancielle.</p>
<p><strong>Taux de change moyen DOP/EUR</strong></p>
<table class="spip">
<thead><tr class="row_first"><th id="id1822_c0"> </th><th id="id1822_c1">2009</th><th id="id1822_c2">2010</th><th id="id1822_c3">2011</th><th id="id1822_c4">2012</th><th id="id1822_c5">2013</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<th headers="id1822_c0" id="id1822_l0">Taux de change moyen DOP/EUR</th>
<td class="numeric virgule" headers="id1822_c1 id1822_l0">48,91</td>
<td class="numeric virgule" headers="id1822_c2 id1822_l0">47,58</td>
<td class="numeric virgule" headers="id1822_c3 id1822_l0">52,68</td>
<td class="numeric virgule" headers="id1822_c4 id1822_l0">50,42</td>
<td class="numeric virgule" headers="id1822_c5 id1822_l0">55,12</td></tr>
</tbody>
</table>
<p>La monnaie dominicaine s’est sensiblement dépréciée face au dollar, au rythme de 6,3% entre novembre 2012 et novembre 2013. Le cours du peso face à la monnaie européenne se caractérise par une plus forte fluctuation (-11% sur la même période).</p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<p>Les retraits sont limités à 10 000 pesos au distributeur.</p>
<p>Il convient d’être prudent pour l’usage des cartes bancaires, qui ne doivent jamais rester hors de vue de leur propriétaire (risque de prise d’empreinte suivie d’une copie).</p>
<p>Les deux seules banques étrangères installées en République dominicaine sont la Scotia Bank et la City Bank.</p>
<h3 class="spip"><a id="sommaire_3"></a>Alimentation</h3>
<p><strong>Prix moyen d’un repas dans un restaurant</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Type de restaurant</td>
<td>DOP</td>
<td>euros</td></tr>
<tr class="row_even even">
<td>Restaurant de qualité supérieure</td>
<td>1550</td>
<td>27,12</td></tr>
<tr class="row_odd odd">
<td>Restaurant de qualité moyenne</td>
<td>850</td>
<td>14,87</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_4"></a>Evolution des prix</h3>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/vie-pratique/article/cout-de-la-vie-111325). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
