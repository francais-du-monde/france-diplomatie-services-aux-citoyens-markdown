# Recherche d’emploi

<p><a href="https://ec.europa.eu/eures/main.jsp?catId=7938&amp;acro=living&amp;lang=fr&amp;parentId=7710&amp;countryId=FI&amp;living" class="spip_out" rel="external">Conditions de vie et de travail en Finlande</a> (site d’Eures).</p>
<p><strong>Outils pour la recherche d’emploi</strong></p>
<p><strong>Journaux</strong></p>
<p>Le quotidien le plus lu est le <a href="http://www.hs.fi/" class="spip_out" rel="external">Helsingin Sanomat</a>.</p>
<p><strong>Sites internet</strong></p>
<p>L’internet est un outil important dans la recherche d’un emploi. Les emplois vacants communiqués à l’Agence pour l’emploi et le développement économique (Agence TE-toimisto) sont répertoriés sur le site Internet <a href="http://www.mol.fi/" class="spip_out" rel="external">Mol.fi</a> (en finnois, suédois et anglais). La plupart des entreprises mettent sur leurs propres sites Internet leurs annonces d’offre d’emploi et postuler en ligne est recommandé voire obligatoire.</p>
<p><strong>Organismes pour la recherche d’emploi</strong></p>
<ul class="spip">
<li><a href="http://www.mol.fi/mol/en/02_working/index.jsp" class="spip_out" rel="external">Agence pour l’emploi</a></li>
<li><a href="http://www.cimo.fi/" class="spip_out" rel="external">Center for International Mobility</a> <br class="manualbr">PO Box 343, FI-00531 Helsinki, Finland</li>
<li><a href="http://www.tem.fi/" class="spip_out" rel="external">Ministry of Employment and the Economy</a><br class="manualbr">P.O. Box 32, FI-00023 GOVERNMENT, Finland<br class="manualbr">Tel :+358 10 606 000</li></ul>
<p><i>Mise à jour : janvier 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/emploi-stage/article/recherche-d-emploi). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
