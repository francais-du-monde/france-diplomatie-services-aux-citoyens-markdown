# Scolarisation

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/vie-pratique/article/scolarisation#sommaire_1">Les établissements scolaires français au Mexique</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/vie-pratique/article/scolarisation#sommaire_2">Enseignement supérieur</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/vie-pratique/article/scolarisation#sommaire_3">Pour en savoir plus</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Les établissements scolaires français au Mexique</h3>
<p>Pour toute information sur la scolarisation dans le système français à l’étranger, vous pouvez consulter <a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais.md" class="spip_in">notre thématique sur les études et la scolarisation</a>.</p>
<p>Vous y trouverez des renseignements sur :</p>
<ul class="spip">
<li>les établissements français du primaire et du secondaire à l’étranger ;</li>
<li>les bourses scolaires et la prise en charge des frais de scolarité à l’étranger ;</li>
<li>les possibilités qui s’offrent à vous si votre enfant ne peut être scolarisé à l’étranger dans le système français (enseignement à distance par le CNED, programme français langue maternelle (FLAM), internats en France) ;</li>
<li>les épreuves du baccalauréat à l’étranger ;</li>
<li>les bourses d’études supérieures en France et à l’étranger ;</li>
<li>l’équivalence des diplômes.</li></ul>
<p>Trois établissements scolaires sont conventionnés par l’AEFE au Mexique :</p>
<p>1. Le <a href="http://www.lfm.edu.mx/" class="spip_out" rel="external">lycée franco-mexicain de Mexico</a> scolarise de la maternelle à la terminale (plus une section BTS) 3100 élèves en section française dont 1354 Français et 316 en section mexicaine (section bilingue).</p>
<p>Le LFM est situé dans le quartier résidentiel et d’affaires de Polanco (Avenida Homero 1521 – Colonia Polanco – 11560 México DF). Une annexe (classes de primaire et collège) existe dans le sud de la ville, à Coyoacan (Cerro Xico 24 –Oxtopulco Universidad – Colonia Coyoacan – 04310 México DF).</p>
<p>2. L’école Molière – LFM AC de Cuernavaca : extension du LFM de Mexico, cet établissement qui scolarise 259 élèves (classes de primaire et collège) est situé : Francisco I. Madero 315 – Ocotepec – 62220 Cuernavaca (Etat de Morelos).</p>
<p>Tarifs pour 2013/2014 pour le LFM de Mexico et L’école Molière de Cuernavaca :</p>
<p><strong>Réinscription - Inscription et les droits d’admission</strong><br class="manualbr">Année 2013-2014 : Section française</p>
<table class="spip">
<thead><tr class="row_first"><th id="id23e4_c0">Maternelle</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="id23e4_c0">Admission</td>
<td>MS, PS et GS</td>
<td class="numeric ">13 500</td></tr>
<tr class="row_even even">
<td headers="id23e4_c0">Inscription</td>
<td>MS et PS</td>
<td class="numeric ">9 975</td></tr>
<tr class="row_odd odd">
<td headers="id23e4_c0">Inscription</td>
<td>GS</td>
<td class="numeric ">11 760</td></tr>
<tr class="row_even even">
<td headers="id23e4_c0"><strong>Primaire</strong></td></tr>
<tr class="row_odd odd">
<td headers="id23e4_c0">Admission</td>
<td>CP, CE1, CE2, CM1 et CM2</td>
<td class="numeric ">11 500</td></tr>
<tr class="row_even even">
<td headers="id23e4_c0">Inscription</td>
<td>CP, CE1 et CE2</td>
<td class="numeric ">12 355</td></tr>
<tr class="row_odd odd">
<td headers="id23e4_c0">Inscription</td>
<td>CM1 et CM2</td>
<td class="numeric ">13 950</td></tr>
<tr class="row_even even">
<td headers="id23e4_c0"><strong>Secondaire</strong></td></tr>
<tr class="row_odd odd">
<td headers="id23e4_c0">Admission</td>
<td>6ème., 5ème., 4ème. et 3ème.</td>
<td class="numeric ">7 800</td></tr>
<tr class="row_even even">
<td headers="id23e4_c0">Inscription</td>
<td>6ème., 5ème., 4ème. et 3ème.</td>
<td class="numeric ">14 905</td></tr>
<tr class="row_odd odd">
<td headers="id23e4_c0"><strong>Préparatoire</strong></td></tr>
<tr class="row_even even">
<td headers="id23e4_c0">Admission</td>
<td>2nd., 1er. et Terminal</td>
<td class="numeric ">4 900</td></tr>
<tr class="row_odd odd">
<td headers="id23e4_c0">Inscription</td>
<td>2nd., 1er. et Terminal</td>
<td class="numeric ">17 340</td></tr>
</tbody>
</table>
<p><strong>Droits mensuels de scolarité 2013-2014</strong></p>
<table class="spip">
<thead><tr class="row_first"><th id="idab70_c0"> </th><th id="idab70_c1">1er enfant</th><th id="idab70_c2">2e enfant</th><th id="idab70_c3">3e enfant</th><th id="idab70_c4">4 enfant</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="idab70_c0">Section maternelle mexicaine</td></tr>
<tr class="row_even even">
<td headers="idab70_c0">1ère et 2ème</td>
<td headers="idab70_c1">4 875</td>
<td headers="idab70_c2">4 875</td>
<td headers="idab70_c3">4 560</td>
<td headers="idab70_c4">4 245</td></tr>
<tr class="row_odd odd">
<td headers="idab70_c0">3ème</td>
<td headers="idab70_c1">5 990</td>
<td headers="idab70_c2">5 605</td>
<td headers="idab70_c3">5 245</td>
<td headers="idab70_c4">4 885</td></tr>
<tr class="row_even even">
<td headers="idab70_c0">Section maternelle française</td></tr>
<tr class="row_odd odd">
<td headers="idab70_c0">P.S. et M.S.</td>
<td headers="idab70_c1">5 780</td>
<td headers="idab70_c2">5 405</td>
<td headers="idab70_c3">5 060</td>
<td headers="idab70_c4">4 715</td></tr>
<tr class="row_even even">
<td headers="idab70_c0">GS</td>
<td headers="idab70_c1">6 700</td>
<td headers="idab70_c2">6 265</td>
<td headers="idab70_c3">5 865</td>
<td headers="idab70_c4">5 460</td></tr>
<tr class="row_odd odd">
<td headers="idab70_c0">Section primaire mexicaine</td></tr>
<tr class="row_even even">
<td headers="idab70_c0">1ère, 2ème  et 3ème</td>
<td headers="idab70_c1">6 365</td>
<td headers="idab70_c2">5 955</td>
<td headers="idab70_c3">5 570</td>
<td headers="idab70_c4">5 190</td></tr>
<tr class="row_odd odd">
<td headers="idab70_c0">4ème, 5ème et 6ème</td>
<td headers="idab70_c1">7 165</td>
<td headers="idab70_c2">6 700</td>
<td headers="idab70_c3">6 270</td>
<td headers="idab70_c4">5 840</td></tr>
<tr class="row_even even">
<td headers="idab70_c0">Section primaire française</td></tr>
<tr class="row_odd odd">
<td headers="idab70_c0">CP., CE1, et CE2</td>
<td headers="idab70_c1">7 095</td>
<td headers="idab70_c2">6 635</td>
<td headers="idab70_c3">6 210</td>
<td headers="idab70_c4">5 785</td></tr>
<tr class="row_even even">
<td headers="idab70_c0">CM1, CM2</td>
<td headers="idab70_c1">8 000</td>
<td headers="idab70_c2">7 480</td>
<td headers="idab70_c3">7 000</td>
<td headers="idab70_c4">6 520</td></tr>
<tr class="row_odd odd">
<td headers="idab70_c0">1er Cycle secondaire – section française</td></tr>
<tr class="row_even even">
<td headers="idab70_c0">6ème., 5ème., 4ème. et 3ème.</td>
<td headers="idab70_c1">8 555</td>
<td headers="idab70_c2">8 000</td>
<td headers="idab70_c3">7 490</td>
<td headers="idab70_c4">6 975</td></tr>
<tr class="row_odd odd">
<td headers="idab70_c0">Cycle secondaire mexicain</td></tr>
<tr class="row_even even">
<td headers="idab70_c0">6ème., 5ème., 4ème. et 3ème.</td>
<td headers="idab70_c1">7 520</td>
<td headers="idab70_c2">7 035</td>
<td headers="idab70_c3">6 580</td>
<td headers="idab70_c4">6 130</td></tr>
<tr class="row_odd odd">
<td headers="idab70_c0">Préparatoire 2nd cycle</td></tr>
<tr class="row_even even">
<td headers="idab70_c0">2nd., 1ère et Terminale</td>
<td headers="idab70_c1">10 010</td>
<td headers="idab70_c2">9 360</td>
<td headers="idab70_c3">8 760</td>
<td headers="idab70_c4">8 160</td></tr>
<tr class="row_odd odd">
<td headers="idab70_c0">Cursus intermédiaire (C.I.)</td>
<td headers="idab70_c1">8 220</td>
<td headers="idab70_c2">7 690</td>
<td headers="idab70_c3">7 195</td>
<td headers="idab70_c4">6 700</td></tr>
</tbody>
</table>
<p>3. <a href="http://francomexicanoguada.tripod.com/" class="spip_out" rel="external">Le lycée franco-mexicain de Guadalajara</a> scolarise de la maternelle à la terminale 895 élèves dont 283 français (2013/2014). Il est situé Francisco Villa 235 – Colonia El Bajío – 45019 Zapopan, commune résidentielle de la banlieue de Guadalajara.</p>
<table class="spip" summary="">
<caption>Tarifs pour 2013/2014</caption>
<tbody>
<tr class="row_odd odd">
<td>Maternelle (PS, MS, GS)</td>
<td>$ 5 100</td></tr>
<tr class="row_even even">
<td>Elémentaire</td>
<td>$ 5 600</td></tr>
<tr class="row_odd odd">
<td>Collège</td>
<td>$ 6 500</td></tr>
<tr class="row_even even">
<td>Lycée 2nde</td>
<td>$ 6 700</td></tr>
<tr class="row_odd odd">
<td>Lycée 1ere</td>
<td>$ 7 100</td></tr>
<tr class="row_even even">
<td>Lycée terminale</td>
<td>$ 7 300</td></tr>
</tbody>
</table>
<p><i>10 % de remise pour le 3ème enfant et 12 % à partir du 4ème</i>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Enseignement supérieur</h3>
<p>Les ressortissants français titulaires du baccalauréat peuvent s’inscrire en première année de licence dans une université mexicaine. Selon les établissements, ils doivent satisfaire à une épreuve de langue espagnole et à un concours d’entrée. Pour l’admission aux universités publiques, il existe un concours d’entrée et les étudiants mexicains et étrangers sont admis en fonction des résultats obtenus et du nombre de places disponibles. Pour les établissements privés, un test d’admission peut être demandé.</p>
<p>Chaque université mexicaine est libre de reconnaître ou non les diplômes français car il n’existe pas d’accord franco-mexicain en la matière (sauf pour le baccalauréat). La reconnaissance des diplômes peut donc différer d’un établissement à l’autre. Dans certains cas, la « revalidation » par le ministère d’Education publique peut être demandée.</p>
<p>La coopération scientifique franco-mexicaine s’est construite dans le cadre de plusieurs programmes qui jouent le rôle de piliers (ECOS-Nord, PCP etc.). Elle s’appuie sur le soutien de grands organismes de recherche comme le CNRS et l’IRD. Dans le domaine des sciences sociales, le CEMCA joue un rôle prépondérant en organisant des séminaires, des colloques et des publications consacrés tant à la recherche pure qu’à la recherche appliquée.</p>
<h3 class="spip"><a id="sommaire_3"></a>Pour en savoir plus</h3>
<p><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> Notre <a href="http://www.mfe.org/index.php/Thematiques/Etudes-Scolarisation" class="spip_out" rel="external">rubrique thématique sur les études et la scolarisation à l’étranger</a><br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> <a href="http://www.sre.gob.mx/francia/etudes.htm" class="spip_out" rel="external">Page enseignement supérieur au Mexique</a> de l’ambassade du Mexique en France<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> <a href="http://www.conacyt.mx/" class="spip_out" rel="external">Page du conseil national de sciences et technologie</a><br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> <a href="http://www.anuies.mx/" class="spip_out" rel="external">Page de l’association nationale des universités et institutions de l’éducation supérieure</a></p>
<p><i>Mise à jour : septembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/vie-pratique/article/scolarisation). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
