# Incarcération

<blockquote class="texteencadre-spip spip"><strong>En cas d’incarcération d’un proche à l’étranger, vous pouvez :</strong></blockquote>
<p><strong> - Avertir :</strong></p>
<p>•  le poste ou le service consulaire français du lieu d’incarcération le plus proche ;</p>
<p>•  le bureau de la protection des détenus du ministère des Affaires étrangères (01 43 17 80 32) ou les nuits et week-end au 01.53.59.11.00.</p>
<p><strong> - Obtenir des informations relatives à cette incarcération, et notamment :</strong></p>
<p>•  ce que recouvre la protection consulaire telle que définie dans les conventions internationales ;</p>
<p>•  une liste d’avocats, si possibles francophones, à même d’exercer la défense de votre proche. Cette liste n’est qu’indicative. Le paiement des honoraires incombe au détenu ou à ses proches, et non pas au ministère des Affaires étrangères ;</p>
<p>•  des renseignements sur la situation de votre proche (conditions de détention, évolution de la procédure judiciaire, etc.), sans que cela constitue un droit et sous réserve que la personne détenue y consente.</p>
<p><strong> - Demander, selon les contingences</strong> (autorisations des autorités locales, possibilités matérielles des services du ministère des Affaires étrangères), <strong>à ce que votre proche bénéficie de l’acheminement des fonds que vous souhaitez lui adresser</strong>. Lire notre article "<a href="http://diplomatie.gouv.fr/fr/services-aux-citoyens/conseils-aux-familles/arrestation-ou-detention-d-un/article/le-transfert-de-fonds-par-voie-de" class="spip_out" rel="external">Le transfert de fonds par voie de chancellerie</a>".</p>
<p><strong>Pour plus d’informations, </strong> consulter la publication sur l’aide aux détenus à l’étranger <a href="http://diplomatie.gouv.fr/fr/IMG/pdf/brochure_detenus_juillet_2015_cle88f19f-2.pdf" class="spip_out" rel="external">en version PDF</a>.</p>
<blockquote class="texteencadre-spip spip"><strong>Le transfèrement</strong></blockquote>
<p>Lorsqu’il existe une convention multilatérale ou bilatérale de transfèrement de détenus applicable, <strong>les Français détenus à l’étranger peuvent présenter une demande auprès de l’administration pénitentiaire locale, en vue de finir d’exécuter leur peine en France.</strong> Il leur revient d’en informer le consulat de France.</p>
<p>En l’absence de convention de transfèrement entre l’État de condamnation et la France, les Français détenus peuvent également présenter une demande. Dans ce cas, le transfèrement peut intervenir en application d’un accord simplifié négocié entre les deux États.</p>
<p>Dans la majorité des cas, le transfèrement ne peut avoir lieu qu’à certaines conditions, notamment : que le jugement soit définitif, que le détenu se soit acquitté de ses obligations pécuniaires (amendes,…) dans l’État de condamnation, que les actes qui ont donné lieu à la condamnation dans le pays étranger constituent une infraction au regard de la législation française et que les autorités étrangères et françaises se soient mises d’accord sur le transfèrement.</p>
<p><i>Mise à jour : juillet 2016</i></p>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/infos-pratiques/assistance-aux-francais/incarceration-20987/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
