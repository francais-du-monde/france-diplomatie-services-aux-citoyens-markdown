# Protection sociale

<h2 class="rub22793">Régime local de sécurité sociale</h2>
<p>Vous trouverez une présentation détaillée du système de sécurité sociale belge sur le site de notre partenaire, le <a href="http://www.cleiss.fr/" class="spip_out" rel="external">CLEISS</a> (Centre des liaisons européennes et internationales de sécurité sociale). En voici la table des matières :</p>
<p><strong>Le régime belge de sécurité sociale (salariés)</strong></p>
<ul class="spip">
<li><a href="http://www.cleiss.fr/docs/regimes/regime_belgique_s0.html" class="spip_out" rel="external">Historique</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_belgique_s1.html" class="spip_out" rel="external">Présentation</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_belgique_s2.html" class="spip_out" rel="external">Financement</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_belgique_s3.html" class="spip_out" rel="external">Soins de santé, indemnités, maternité, paternité, décès</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_belgique_s4.html" class="spip_out" rel="external">Prestations familiales</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_belgique_s5.html" class="spip_out" rel="external">Accidents du travail, maladies professionnelles</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_belgique_s6.html" class="spip_out" rel="external">Chômage</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_belgique_s7.html" class="spip_out" rel="external">Pensions de vieillesse et de survivants</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_belgique_sannexes.html" class="spip_out" rel="external">Annexes</a></li></ul>
<p><strong>Le régime belge de sécurité sociale (non-salariés)</strong></p>
<ul class="spip">
<li><a href="http://www.cleiss.fr/docs/regimes/regime_belgique_ns1.html" class="spip_out" rel="external">Présentation</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_belgique_ns2.html" class="spip_out" rel="external">Paiement des cotisations</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_belgique_ns3.html" class="spip_out" rel="external">Assurance soins de santé</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_belgique_ns4.html" class="spip_out" rel="external">Régime de l’incapacité de travail</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_belgique_ns5.html" class="spip_out" rel="external">Maternité</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_belgique_ns6.html" class="spip_out" rel="external">Pensions</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_belgique_ns7.html" class="spip_out" rel="external">Prestations familiales</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_belgique_ns8.html" class="spip_out" rel="external">Prestations non contributives</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_belgique_ns9.html" class="spip_out" rel="external">Assurance sociale en faveur des travailleurs indépendants en cas de faillite</a></li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-belgique-protection-sociale-article-convention-de-securite-sociale-109805.md" title="Convention de sécurité sociale">Convention de sécurité sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-belgique-protection-sociale-article-regime-local-de-securite-sociale.md" title="Régime local de sécurité sociale">Régime local de sécurité sociale</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/protection-sociale/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
