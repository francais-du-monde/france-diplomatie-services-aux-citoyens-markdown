# Fiscalité

<h2 class="rub22842">Fiscalité du pays</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/fiscalite/#sommaire_1">Présentation </a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/fiscalite/#sommaire_2">Année fiscale </a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/fiscalite/#sommaire_3">Barème de l’impôt </a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/fiscalite/#sommaire_4">Quitus fiscal </a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/fiscalite/#sommaire_5">Solde du compte en fin de séjour </a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/fiscalite/#sommaire_6">Coordonnées des centres d’information fiscale </a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/fiscalite/#sommaire_7">Actualités</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Présentation </h3>
<h4 class="spip">Date et lieu de dépôt des déclarations de revenus</h4>
<p>La déclaration d’impôt doit être souscrite avant le 1er mars de chaque année. L’administration fiscale sénégalaise met à la disposition des contribuables un formulaire réglementaire (de couleur blanche) à remplir et à déposer au centre de services fiscaux dont ils relèvent.</p>
<h4 class="spip">Modalités de paiement des impôts pour un salarié</h4>
<p>L’impôt est prélevé par <strong>retenue à la source</strong>.</p>
<p>Les entrepreneurs individuels, les titulaires de revenus non soumis à une retenue à la source libératoire (revenus fonciers, certains revenus de capitaux mobiliers, …) sont tenus de souscrire une déclaration annuelle de revenus.</p>
<p>Les titulaires de revenus non commerciaux sont tenus de souscrire leur déclaration de résultat au plus tard le 31 mars. Les entreprises imposables à l’impôt sur les sociétés, au plus tard le 30 avril.</p>
<p>Chaque membre du foyer est imposé séparément.</p>
<h3 class="spip"><a id="sommaire_2"></a>Année fiscale </h3>
<p>L’année fiscale est alignée sur l’année civile : 1er janvier – 31 décembre.</p>
<h3 class="spip"><a id="sommaire_3"></a>Barème de l’impôt </h3>
<p>L’IRPP (impôt sur le revenu des personnes physiques) est un impôt annuel unique établi au profit du budget de l’Etat.</p>
<h4 class="spip">Champ d’application</h4>
<p><strong>Les personnes assujetties sont celles qui ont un domicile fiscal au Sénégal</strong> :</p>
<ul class="spip">
<li>les personnes qui ont au Sénégal leur foyer d’habitation permanente ou leur lieu de séjour principal, sous réserve des conventions internationales signées par le Sénégal relatives aux doubles impositions.</li>
<li>les personnes qui exercent au Sénégal une activité professionnelle, à moins qu’elles ne justifient que cette activité y est exercée à titre accessoire.</li>
<li>les personnes se trouvant en congé hors du Sénégal au 1er janvier de l’année de l’imposition et pour lesquelles le Sénégal demeure la résidence qu’elles avaient en raison de leurs fonctions avant leur départ en congé.</li></ul>
<p>Sont considérés comme revenus de source sénégalaise :</p>
<ul class="spip">
<li>les revenus d’immeubles ou d’exploitations sis au Sénégal ;</li>
<li>les revenus de professions exercées au Sénégal ;</li>
<li>les revenus de capitaux mobiliers et valeurs mobilières placés au Sénégal ;</li>
<li>les profits tirés de certaines opérations relatives à des fonds ou à des immeubles situés au Sénégal ou à des actions ou parts de sociétés dont l’actif est constitué principalement par de tels biens ou droits ;</li>
<li>les pensions et rentes viagères lorsque le débiteur est domicilié ou est établi au Sénégal ;</li>
<li>les revenus non commerciaux perçus par les inventeurs ou au titre des droits d’auteur, ainsi que les produits tirés de la propriété industrielle ou commerciale lorsque le débiteur est domicilié ou établi au Sénégal ;</li>
<li>les sommes payées en rémunération des prestations de toute nature fournies ou utilisées au Sénégal.</li></ul>
<p><strong>Sont exemptées de l’impôt</strong> les personnes dont le revenu net annuel imposable, après division par le nombre de parts dont elles bénéficient n’excède pas la somme de<i> 600.000 FCFA</i>.</p>
<h4 class="spip">Détermination des revenus imposables</h4>
<h5 class="spip">Les revenus catégoriels</h5>
<p><strong>Traitements, salaires, pensions et rentes viagères</strong></p>
<p>Revenus imposables : il s’agit des rémunérations regroupant les traitements publics ou privés, les primes et indemnités de toute nature, les émoluments, les salaires ainsi que les avantages en nature accordés ou payés. Le salaire imposable au titre d’une année fiscale est constitué des divers versements en espèces faits au salarié, ainsi que certains avantages en nature évalués forfaitairement.</p>
<p>Du montant total du salaire ainsi obtenu <strong>peuvent être déduits </strong> :</p>
<ul class="spip">
<li>les retenues faites par l’employeur ou les cotisations versées à titre obligatoire pour la constitution d’une retraite dans la limite de 13,2% du total des traitements et salaires, indemnités, émoluments et avantages en nature.</li>
<li>les frais professionnels dès lors qu’ils ne sont pas couverts par une allocation ou indemnité spéciale. Cette déduction est forfaitairement fixée à 10% du revenu brut, après défalcation des retenues ou cotisations pour la constitution d’une retraite.</li>
<li>les indemnités kilométriques dans les limites fixées par arrêté du ministre chargé des Finances, à savoir 50 000 FCFA/mois (100 000 FCFA pour les représentants commerciaux effectuant plus de 500 km par semaine).</li></ul>
<p><strong>Les bénéfices industriels et commerciaux</strong></p>
<p>Cette catégorie concerne les revenus réalisés par les entreprises individuelles et les membres de sociétés de personnes. Elle comprend également les revenus d’opérations commerciales, industrielles ou artisanales réalisées par des non-résidents. Les modalités de détermination du bénéfice des entreprises individuelles sont les mêmes que celles utilisées pour la détermination du bénéfice fiscal des sociétés.</p>
<p><strong>Revenus fonciers </strong></p>
<p>Cela concerne les revenus des propriétés bâties telles que maison et usines, les revenus de l’outillage des établissements industriels, les revenus des propriétés non bâties, les revenus accessoires, provenant, notamment, de la location des droits d’affichage, de la concession du droit de propriété ou d’usufruit.</p>
<p><strong>Revenus non commerciaux</strong></p>
<p>La détermination des revenus des professions libérales suit les mêmes règles que celles applicables aux bénéfices industriels et commerciaux.</p>
<p><strong>Bénéfices des exploitations agricoles</strong></p>
<p>Le bénéfice agricole est déterminé dans les mêmes formes que celles prévues en matière de bénéfices industriels et commerciaux.</p>
<p><strong>Revenus de capitaux mobiliers</strong></p>
<p>Les revenus de capitaux mobiliers incluent les revenus de valeurs mobilières (dividendes) et les revenus de créances, dépôts et cautionnements (intérêts).</p>
<h5 class="spip">Calcul de l’impôt</h5>
<p>Pour le calcul de l’impôt, le revenu imposable est soumis à :</p>
<ul class="spip">
<li>un droit proportionnel</li>
<li>un droit progressif</li></ul>
<p><strong>Le droit proportionnel</strong> est calculé à des taux différents en fonction de chaque catégorie de revenu.</p>
<p><strong>Traitements et salaires</strong></p>
<ul class="spip">
<li>jusqu’à 700 000 FCFA : 0 %</li>
<li>pour la fraction du revenu annuel supérieur à 700 000 FCFA : 11 %<br class="manualbr">Ces taux s’appliquent également aux rémunérations allouées aux associés gérants majoritaires de SARL et aux associés gérants commandités de sociétés en commandite simple.</li></ul>
<p><strong>Revenus fonciers</strong></p>
<p>Le revenu net foncier est imposé au taux de 20 %.</p>
<p><strong>Revenus de capitaux mobiliers</strong></p>
<p>Les différents taux du droit proportionnel applicables aux revenus de capitaux mobiliers correspondent aux taux de la retenue à la source applicable à ces revenus à savoir :</p>
<ul class="spip">
<li>10 % pour les produits des actions, parts sociales et parts d’intérêt de sociétés civiles</li>
<li>13 % pour les obligations</li>
<li>15 % pour les lots</li>
<li>16 % pour les autres revenus et notamment les jetons de présence et autres rémunérations d’administrateurs, les revenus de créances, d’impôts et cautionnements.<br class="manualbr">Ce taux ramené à 8 % pour les intérêts, arrérages et autres produits des comptes de dépôts et des comptes-courants ouverts dans les comptes d’une banque, d’un établissement de banque, d’un agent de change, d’un courtier en valeurs mobilières et des comptables du Trésor. Par ailleurs, lorsque ces revenus sont inclus dans les bénéfices d’une entreprise industrielle, commerciale ou d’une exploitation agricole ou d’une profession non commerciale, le droit proportionnel n’est pas dû. Il n’est pas dû également lorsque les revenus de capitaux mobiliers sont exemptés de retenue à la source.</li></ul>
<p><strong>Bénéfices industriels et commerciaux, bénéfices agricoles et bénéfices des professions non commerciales</strong></p>
<ul class="spip">
<li>jusqu’à 330 000 FCFA de bénéfice net : 0 %</li>
<li>au-delà de 330 000 FCFA : 25 %<br class="manualbr">Ce taux est néanmoins réduit de moitié en ce qui concerne les ouvriers et artisans, sous certaines conditions.</li></ul>
<p><strong>Charges familiales du contribuable</strong></p>
<p>Pour le calcul du <strong>droit progressif</strong>, le revenu net global du contribuable est divisé par un certain nombre de parts fixé d’après les charges de famille du contribuable. Ensuite le tarif progressif ci-dessous est appliqué au revenu correspondant à une part. Le montant d’impôt dû par le contribuable est égal au produit de la somme ainsi obtenue par le nombre de parts.</p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Situation et charges de famille / Nombre de parts</strong> <br> <br>Marié sans enfant à charge : 1,5
<p>Même situation avec</p>
<p>un enfant à charge : 2</p>
<p>deux enfants à charge : 2,5</p>
<p>trois enfants à charge : 3 <br> <br>Célibataire, divorcé ou veuf</p>
<p>sans enfants à charge : 1</p>
<p>ayant un enfant à charge : 1,5</p>
<p>ayant deux enfants à charge : 2</p>
<p>ayant trois enfants à charge : 2,5</p>
<p>ayant quatre enfants à charge : 3 <br> <br>et ainsi de suite en augmentant d’une demi-part par enfant à la charge du contribuable.</p>
</td></tr>
</tbody>
</table>
<p>L’enfant majeur, infirme, donne toutefois droit à une part. Sont considérés comme des enfants à charge :</p>
<ul class="spip">
<li>les enfants mineurs ou infirmes ou âgés de moins de 25 ans poursuivant leurs études</li>
<li>les enfants orphelins ou abandonnés ou recueillis</li>
<li>ou ceux sur lesquels le contribuable exerce la puissance paternelle.</li></ul>
<p>Le contribuable peut opter pour l’imposition commune avec sa conjointe et ses enfants mineurs habitant avec lui. Dans ce cas, le nombre de parts à prendre en considération pour le calcul du droit progressif ne pourra, en aucun cas, dépasser 5.</p>
<p>Le nombre de parts à prendre en considération pour le calcul de l’impôt ne peut, en aucun cas, dépasser 1,5 pour les personnes qui, quelle que soit leur nationalité, ne résident pas au Sénégal mais y disposent de revenus imposables.</p>
<p><strong>L’impôt pour une part est calculé par application du barème suivant</strong> :</p>
<table class="spip">
<thead><tr class="row_first"><th id="idea2e_c0">Revenu imposable (en FCFA)  </th><th id="idea2e_c1">Taux  </th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="idea2e_c0">0 à 600 000</td>
<td headers="idea2e_c1">0%</td></tr>
<tr class="row_even even">
<td headers="idea2e_c0">600 001 à 890 000</td>
<td headers="idea2e_c1">18%</td></tr>
<tr class="row_odd odd">
<td headers="idea2e_c0">890 001 à 1 010 000</td>
<td headers="idea2e_c1">22%</td></tr>
<tr class="row_even even">
<td headers="idea2e_c0">1 010 001 à 1 410 000</td>
<td headers="idea2e_c1">25%</td></tr>
<tr class="row_odd odd">
<td headers="idea2e_c0">1 410 001 à 2 475 000</td>
<td headers="idea2e_c1">28%</td></tr>
<tr class="row_even even">
<td headers="idea2e_c0">2 475 001 à 3 540 000</td>
<td headers="idea2e_c1">30%</td></tr>
<tr class="row_odd odd">
<td headers="idea2e_c0">3 540 001 à 7 650 000</td>
<td headers="idea2e_c1">35%</td></tr>
<tr class="row_even even">
<td headers="idea2e_c0">7 650 001 à 9 650 000</td>
<td headers="idea2e_c1">40%</td></tr>
<tr class="row_odd odd">
<td headers="idea2e_c0">9 650 001 à 12 650 000</td>
<td headers="idea2e_c1">45%</td></tr>
<tr class="row_even even">
<td headers="idea2e_c0">au delà de 12 650 000</td>
<td headers="idea2e_c1">50%</td></tr>
</tbody>
</table>
<h5 class="spip">Autres impôts dus par les personnes physiques (MF et TRIMF)</h5>
<p>L’impôt du minimum fiscal (MF) est dû par toute personne résidant au Sénégal, âgée d’au moins 14 ans et relevant d’une des cinq catégories précisées par le Code Général des Impôts.</p>
<p>Le tarif de l’impôt est le suivant :</p>
<ul class="spip">
<li>catégorie exceptionnelle : 12 000 FCFA</li>
<li>1ère catégorie : 4 000 FCFA</li>
<li>2ème catégorie : 3 200 FCFA</li>
<li>3ème catégorie : 600 FCFA<br class="manualbr">Il est recouvré par voie de rôle nominatif</li></ul>
<p>La taxe représentative de l’impôt du minimum fiscal (TRIMF) est due par toute personne physique résidant au Sénégal qui perçoit des traitements, indemnités, émoluments, pensions et rentes viagères et qui n’en est pas expressément exonérée ;</p>
<p>Les contribuables sont classés en quatre catégories, en fonction du revenu brut annuel (salaire plus indemnités, plus avantages en nature) qu’ils perçoivent. A chaque catégorie correspond un montant de taxe :</p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Catégorie</td>
<td>revenu brut annuel (en FCFA)</td>
<td>TRIMF (en FCFA)</td></tr>
<tr class="row_even even">
<td>exceptionnelle</td>
<td>supérieur ou égal à 12 000 000</td>
<td>18 000</td></tr>
<tr class="row_odd odd">
<td>1ère</td>
<td>supérieur ou égal à 2 000 000 et inférieur à 12 000 000</td>
<td>6 000</td></tr>
<tr class="row_even even">
<td>2ème</td>
<td>supérieur ou égal à 1 000 000 et inférieur à 2 000 000</td>
<td>4 800</td></tr>
<tr class="row_odd odd">
<td>3ème</td>
<td>supérieur ou égal à 600 000 et inférieur à 1 000 000</td>
<td>3 600</td></tr>
<tr class="row_even even">
<td>4ème</td>
<td>inférieur à 600 000</td>
<td>900</td></tr>
</tbody>
</table>
<h5 class="spip">La contribution globale unique (CGU)</h5>
<p>Apparue avec la réforme fiscale de 2004, la contribution globale unique (GGU) a été mise en place afin d’inciter le secteur informel à entrer dans le cadre de la légalité et à payer des taxes. Elle est une synthèse de 6 impôts : l’impôt sur le revenu assis sur les bénéfices industriels et commerciaux, l’impôt du minimum fiscal, la contribution des patentes, la contribution des licences, la taxe sur la valeur ajoutée et la contribution forfaitaire à la charge des employeurs.</p>
<p>Sont soumises à la CGU les personnes physiques dont le chiffre d’affaires annuel, tous droits et taxes compris n’excède pas :</p>
<ul class="spip">
<li>25 millions de FCFA lorsqu’elles réalisent des opérations de prestations de service</li>
<li>50 millions de FCFA lorsqu’elles réalisent des opérations de livraisons de biens</li></ul>
<p>Sont exclues de la CGU :</p>
<ul class="spip">
<li>les personnes physiques dont l’activité relève de la catégorie des bénéfices commerciaux</li>
<li>les personnes physiques effectuant des opérations de vente, de locations d’immeubles ou des gestions immobilières.</li></ul>
<p>La CGU est établie chaque année en fonction du chiffre d’affaires réalisé du 1er janvier au 31 décembre de l’année précédente après déduction des opérations soumises au précompte.</p>
<p>Il existe deux tableaux tarifaires de CGU, le tarif des commerçants et le tarif des prestataires de service :</p>
<ul class="spip">
<li>le chiffre d’affaires annuel des commerçants se distribue en 20 tranches de 0 à 50 millions de FCFA correspondant à une CGU à acquitter variant de 5 000 FCFA et 4 200 000 FCFA</li>
<li>le chiffre d’affaires annuel des prestataires de services se distribue en 11 tranches de 0 à 25 millions de FCFA correspondant à une CGY à acquitter variant entre 10 000 FCFA et 3 000 000 FCFA</li></ul>
<p>La CGU à payer par les entreprises nouvelles est réduite au prorata du nombre de jours d’activité au cours de la première année.</p>
<p>Des acomptes doivent être versés spontanément dans les quinze premiers jours du mois de février, mai et août à la caisse comptable du Trésor du lieu principal d’établissement. Chaque acompte représente le tiers de l’impôt dû. Toutefois, les contribuables dont le montant de l’impôt n’excède pas 100 000 FCFA sont tenus de payer leurs impôts en une fois avant le 1er mai de l’année en cours.</p>
<h5 class="spip">L’impôt sur les sociétés (IS)</h5>
<p>Les assujettis de plein droit sont les sociétés de capitaux (société anonyme et société à responsabilité limitée) et les sociétés civiles quand elles se livrent à des opérations de nature industrielle, commerciale, agricole, artisanale, forestière et minière. Les sociétés civiles se livrant à des opérations de nature agricole ou artisanale, peuvent opter pour l’imposition selon le régime des sociétés de personnes.</p>
<p>Sont imposables par option (cette option est définitive et irrévocable) : les sociétés de fait, les groupements d’intérêt économique (GIE), les sociétés en nom collectif, les sociétés en participation, les sociétés en commandite simple, les sociétés à responsabilité limitée où l’associé unique est une personne qui opte pour l’assujettissement à l’impôt sur les sociétés. Sont passibles de l’IS, sous réserve des conventions internationales, les bénéfices réalisés au Sénégal. Le taux de l’IS est fixé depuis le 30 juin 2006 à 25% du bénéfice imposable.</p>
<p>L’impôt minimum forfaitaire s’applique aux sociétés et aux personnes morales passibles de l’impôt sur les sociétés. Il est dû par toute société ou personne morale déficitaire ou dont le résultat fiscal ne permet pas de générer un impôt sur les sociétés. Ses tarifs sont les suivants :</p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Chiffres d’affaires hors taxes (en FCFA)</td>
<td>Tarif</td></tr>
<tr class="row_even even">
<td>jusqu’à 250 000 000</td>
<td>500 000 FCFA</td></tr>
<tr class="row_odd odd">
<td>de 250 000 001 à 500 000 000</td>
<td>750 000 FCFA</td></tr>
<tr class="row_even even">
<td>au-delà de 500 000 000</td>
<td>1 000 000 FCFA</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_4"></a>Quitus fiscal </h3>
<p>Un quitus fiscal est exigé avant de quitter le pays.</p>
<h3 class="spip"><a id="sommaire_5"></a>Solde du compte en fin de séjour </h3>
<p>Un expatrié français du secteur privé peut solder son compte en fin de séjour.</p>
<h3 class="spip"><a id="sommaire_6"></a>Coordonnées des centres d’information fiscale </h3>
<p><a href="http://www.finances.gouv.sn/" class="spip_out" rel="external">Direction Générale des Impôts et des Domaines (DGID)</a> <br class="manualbr">Bloc Fiscal - Rue Thiong<br class="manualbr">BP 1561 - Dakar<br class="manualbr">Tel : 00 (221) 33 889 20 02<br class="manualbr">Fax : 00 (221) 33 823 21 29</p>
<p>A toutes fins utiles, le lecteur pourra consulter le <a href="http://www.impots.gouv.fr/" class="spip_out" rel="external">site du ministère français des Finances</a> rubrique "Particuliers" puis "Vos préoccupations" puis "Vivre hors de France".</p>
<h3 class="spip"><a id="sommaire_7"></a>Actualités</h3>
<p>Le code des impôts 2013 prévoit un nouveau dispositif qui pourrait bénéficier aux retraités français qui résident au Sénégal.</p>
<p>L’article 180 de ce code prévoit une décote de 80% de l’impôt dû pour les contribuables « exclusivement titulaires de pensions et rentes viagères de source étrangère. ».</p>
<p>Il précise que « lorsqu’un contribuable dispose de pensions et rentes viagères de source étrangère et d’autres sources de revenus imposables au Sénégal, le montant définitif de l’impôt afférent aux pensions et rentes est arrêté conformément aux dispositions du point 1 (paragraphe ci-dessus) tandis que le montant de l’impôt afférent aux autres revenus est exclusivement déterminé par application des règles prévues aux articles 174 et 179 du présent Code. La cotisation globale du pensionné est alors constituée par le total de l’impôt sur les pensions et rentes de source étrangère et le total de l’impôt exigible sur les autres revenus du contribuable. ».</p>
<p>Enfin, le bénéfice de cette décote est conditionné par le versement des pensions et rentes viagères de source étrangère sur un compte libellé en Francs CFA ouvert au Sénégal en qualité de résident au sens de la réglementation des changes. « Les contribuables devront également joindre à leur déclaration annuelle de revenus une attestation de versement des pensions ou des arrérages établie par le débirentier ou tout autre document en tenant lieu et une attestation indiquant le montant en devises reçu sur le compte du pensionné ou crédirentier et la contre-valeur en Francs CFA au jour du transfert, délivrée par l’établissement financier ou par tout autre organisme intervenant dans le paiement des pensions ou des arrérages. ».</p>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li><a href="http://www.impotsetdomaines.gouv.sn/fr/cgi-2013" class="spip_out" rel="external">Le nouveau Code général des impôts et des domaines sénégalais</a> ;</li>
<li><a href="http://www.ambafrance-sn.org/Le-Service-Economique-Regional-SER" class="spip_out" rel="external">Service économique régional français au Sénégal</a>.</li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-senegal-fiscalite-article-convention-fiscale.md" title="Convention fiscale">Convention fiscale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-senegal-fiscalite-article-fiscalite-du-pays-110064.md" title="Fiscalité du pays">Fiscalité du pays</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/fiscalite/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
