# Scolarisation

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/vie-pratique/article/scolarisation-110392#sommaire_1">Les établissements scolaires français en Grèce</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/vie-pratique/article/scolarisation-110392#sommaire_2">Enseignement supérieur</a></li></ul>
<p>Pour toute information sur la scolarisation dans le système français à l’étranger, vous pouvez consulter notre thématique sur les <a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures.md" class="spip_in">Etudes supérieures</a> et la <a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais.md" class="spip_in">Scolarité en français</a>.</p>
<p>Vous y trouverez des renseignements sur :</p>
<ul class="spip">
<li>les établissements français du primaire et du secondaire à l’étranger ;</li>
<li>les bourses scolaires et la prise en charge des frais de scolarité à l’étranger ;</li>
<li>les possibilités qui s’offrent à vous si votre enfant ne peut être scolarisé à l’étranger dans le système français (enseignement à distance par le CNED, programme français langue maternelle (FLAM), internats en France) ;</li>
<li>les épreuves du baccalauréat à l’étranger ;</li>
<li>les bourses d’études supérieures en France et à l’étranger ;</li>
<li>l’équivalence des diplômes.</li></ul>
<p>Pour en savoir plus :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Consultez le site de l’<a href="http://www.aefe.fr/" class="spip_out" rel="external">Agence pour l’enseignement français à l’étranger</a>.</p>
<h3 class="spip"><a id="sommaire_1"></a>Les établissements scolaires français en Grèce</h3>
<p>Deux établissements scolaires français permettent de suivre une scolarisation française, à Athènes et Thessalonique. L’un fait partie du réseau de l’AEFE, l’autre de celui de la Mission Laïque.</p>
<p>Le <a href="http://lfh.edu.gr/" class="spip_out" rel="external">lycée franco-hellénique Eugène Delacroix</a> à Athènes  présente la particularité de faire cohabiter deux sections différentes, chacune suit les objectifs et les programmes officiels de son système éducatif de référence :</p>
<ul class="spip">
<li>la section française de la maternelle (petite section) au lycée, ministère français de l’Education nationale ;</li>
<li>la section hellénique, du collège (gymnasium, 3 ans) au lycée, ministère hellénique de l’Education nationale et des cultes.</li></ul>
<p>L’<a href="http://www.efth.gr/" class="spip_out" rel="external">Ecole française de Thessalonique</a> propose une scolarisation complète de la maternelle jusqu’à fin primaire. Au-delà, et depuis juillet 2013, l’école a obtenu l’autorisation du ministère hellénique de l’Education et des cultes d’ouvrir un niveau collège et lycée. Ces niveaux seront développés progressivement.</p>
<p>Pour en savoir plus :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.ift.gr/" class="spip_out" rel="external">Institut français de Thessalonique</a>.</p>
<p>Actuellement, un suivi CNED encadré par des répétiteurs locaux est proposé aux collégiens et lycéens. Ces élèves sont regroupés en une classe à plusieurs niveaux et suivent les enseignements CNED au sein de l’établissement de Thessalonique. L’Ecole française de Thessalonique fait partie du réseau de la Mission Laïque française.</p>
<h3 class="spip"><a id="sommaire_2"></a>Enseignement supérieur</h3>
<p>Le baccalauréat français n’est reconnu par les universités grecques que comme un diplôme de fin d’études secondaires ; <strong>il ne donne pas accès à l’enseignement supérieur</strong>. Son équivalent est l’Apolytirio (diplôme de fin d’études).</p>
<p>L’accès à l’enseignement supérieur est soumis à une double condition : l’obtention de l’Apolytirio et la réussite au concours national panhellénique (la note exigée pour l’entrée dans un département est fonction de l’attractivité de ce département. Plus un département est demandé par les candidats, plus la note exigée est élevée), qui se prépare dans les lycées grecs sur les trois années de scolarité en lycée, L’inscription est subordonnée à un bon niveau en langue grecque. Depuis 2010, la note minimum de 10/20 aux examens nationaux de fin de lycée (<i>concours panhellénique classant</i>) n’est plus exigée pour l’entrée dans l’enseignement supérieur.</p>
<p>Toutes les disciplines sont enseignées dans les universités d’Athènes et de Thessalonique.</p>
<p>L’Université hellénique délivre un diplôme de fin d’études qui s’acquiert au bout de 8 à 12 semestres d’enseignement suivant les filières, le <i>Ptychio, </i>240 ECTS (4 années) ou <i>diploma</i>.</p>
<p>Le cursus universitaire peut être prolongé par une formation post-diplômante (<i>Metaptychiako diploma</i> ou diplôme de spécialisation), d’une durée d’un ou deux ans selon les disciplines équivalent du Master 2, qui permet après son obtention de postuler pour un doctorat. La formation d’ingénieur, quant à elle, dure cinq ans + une spécialisation de 1 ou 2 ans.</p>
<p>L’enseignement supérieur en Grèce est dispensé dans les Universités (établissements d’enseignement supérieur - AEI) et les établissements supérieurs d’enseignement technologiques (ATEI).</p>
<p>La reconnaissance des diplômes relève de la compétence du <a href="http://www.doatap.gr/" class="spip_out" rel="external">DOATAP</a> (NARIC grec) (loi du 1er avril 2005 le <strong>principe de comparabilité des cursus l’emporte</strong> sur celui de leur identité. La reconnaissance des périodes d’études est du ressort des établissements d’enseignement supérieur. La section de la reconnaissance des qualifications professionnelles, SAEI, assure la reconnaissance professionnelle des diplômes étrangers de l’enseignement supérieur et l’application en Grèce des directives européennes.</p>
<p><i>Mise à jour : novembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/vie-pratique/article/scolarisation-110392). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
