# Curriculum vitae

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/emploi-stage/article/curriculum-vitae-113145#sommaire_1">Rédaction</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/emploi-stage/article/curriculum-vitae-113145#sommaire_2">Diplômes</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/emploi-stage/article/curriculum-vitae-113145#sommaire_3">Modèles de CV</a></li></ul>
<p>Le CV à l’américaine (en anglais <i>resume</i>) est en général plus long qu’un CV français : l’expérience professionnelle et la formation sont détaillées par des informations concrètes.</p>
<p>Rédigez votre CV en anglais américain et non en anglais britannique. Attention à la présentation des dates (le mois est indiqué en premier pour les dates américaines).</p>
<p>Présentez votre CV de manière rétro-chronologique : affichez les évènements du plus récent au plus ancien.</p>
<h3 class="spip"><a id="sommaire_1"></a>Rédaction</h3>
<p><strong>L’état civil </strong></p>
<p>L’état civil est situé en haut de page. Il est composé :</p>
<ul class="spip">
<li>Du Nom de famille (last Name) et du prénom (first Name)</li>
<li>De l’adresse aux Etats-Unis et/ou en France</li>
<li>Du numéro de téléphone personnel et professionnel</li>
<li>De l’adresse e-mail.</li></ul>
<p><strong>Attention :</strong> pour éviter toute forme de discrimination, le CV exclut :</p>
<ul class="spip">
<li>La photographie</li>
<li>Le sexe</li>
<li>La situation de famille</li>
<li>La date de naissance</li>
<li>L’âge</li></ul>
<p>Toute candidature qui contiendrait l’un de ces éléments deviendrait automatiquement irrecevable.</p>
<p><strong>Le projet professionnel ou "Career Objective" </strong></p>
<p>Définissez en quelques lignes vos objectifs professionnels à court ou moyen terme.</p>
<p><strong>L’expérience professionnelle ou "Work Experience"</strong></p>
<p>Le détail des expériences professionnelles est organisé de la façon suivante :</p>
<ul class="spip">
<li>La date (mois et année)</li>
<li>Le nom de la société</li>
<li>Le secteur d’activité</li>
<li>La localisation (Etat et ville)</li>
<li>Fonctions occupées : développer la nature des missions et des tâches accomplies en valorisant les résultats obtenus (gains réalisés, produit des ventes etc.).</li></ul>
<p>Les stages font partie intégrante de l’expérience professionnelle.</p>
<p><strong>La formation ou "Education"</strong></p>
<p>La formation est détaillée de la façon suivante :</p>
<ul class="spip">
<li>Etablissements supérieurs fréquentés (universités, écoles…)</li>
<li>La localisation (Etat et ville)</li>
<li>Diplômes obtenus ou enseignements suivis : expliquer le contenu pour tenter de déterminer leur équivalent dans le système américain.</li></ul>
<p>Attention !</p>
<ul class="spip">
<li>A partir du niveau bac +4, ne plus mentionner l’obtention du baccalauréat</li>
<li>Eviter l’utilisation de sigles pour le nom d’une école : ils ne sont pas connus outre-Atlantique.</li></ul>
<p><strong>Les compétences linguistiques ou <i>languages</i></strong></p>
<p>Détaillez vos compétences en faisant apparaitre votre niveau de pratique pour chaque langue.</p>
<ul class="spip">
<li>La maitrise de l’anglais est impérative</li>
<li>La pratique de l’espagnol est aussi recherchée, particulièrement dans le sud des Etats-Unis.</li></ul>
<p>Termes utiles :</p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Langue maternelle</td>
<td>Mother tongue / native speaker</td></tr>
<tr class="row_even even">
<td>Bilingue</td>
<td>Bilingual</td></tr>
<tr class="row_odd odd">
<td>Courant</td>
<td>Fluent</td></tr>
<tr class="row_even even">
<td>Maîtrise</td>
<td>Proficient</td></tr>
<tr class="row_odd odd">
<td>Bonnes notions</td>
<td>Basic</td></tr>
<tr class="row_even even">
<td>Notions</td>
<td>Knowledge of</td></tr>
</tbody>
</table>
<p><strong>Les compétences informatiques ou <i>computer skills</i> </strong></p>
<p>Faites apparaitre votre niveau pour chaque environnement informatique.</p>
<p>Exemples :</p>
<ul class="spip">
<li>proficient with Microsoft Office</li>
<li>proficient in both P.C. &amp; Mac environments / computer literate</li>
<li>proficient in Internet research</li></ul>
<p><strong>Les activités extra-professionnelles (<i>Extraprofessional Activities</i>) </strong><br class="autobr">Précisez la pratique d’un sport ou d’une passion, la participation à une association, un club, etc. ;</p>
<p>Citez des activités originales représentant un avantage véritable pour l’emploi postulé ou traduisant des qualités appréciables dans le cadre professionnel.</p>
<p><strong>Références</strong></p>
<p>Dans cette rubrique, mentionnez toute personne (identité et qualité) qualifiée pour juger vos compétences et vos réalisations professionnelles.</p>
<p>Si vous ne souhaitez pas faire figurer ces noms directement sur le CV, écrire par exemple <i>references available on request</i>.</p>
<p>Les recruteurs américains n’hésitent pas à se rapprocher des contacts indiqués en recommandation pour approfondir l’examen d’un dossier de candidature.</p>
<h3 class="spip"><a id="sommaire_2"></a>Diplômes</h3>
<p><strong>Equivalence et reconnaissance des diplômes </strong></p>
<p>Il n’existe pas d’équivalence officielle entre les diplômes français et américains : chaque université ou école est libre d’apprécier l’équivalence qu’elle peut accorder aux titres universitaires.</p>
<p>Aux Etats-Unis, il existe des services privés d’évaluation auxquels les universités peuvent vous demander de recourir. Vous trouverez plus d’informations concernant la politique de reconnaissance des diplômes étrangers dans le système éducatif américain sur le site du <a href="http://www2.ed.gov/about/offices/list/ous/international/usnei/us/edlite-visitus-forrecog.html" class="spip_out" rel="external">US Department of Education</a>.</p>
<p><strong>Correspondance du niveau d’études</strong></p>
<table class="spip">
<thead><tr class="row_first"><th id="id5907_c0"> </th><th id="id5907_c1">Diplômes américains  </th><th id="id5907_c2">Diplômes français  </th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="id5907_c0">Undergraduate studies</td>
<td headers="id5907_c1">High School Diploma
<p>Bachelor’s Degree</p>
</td>
<td headers="id5907_c2">Baccalauréat
<p>Licence (2ème cycle)</p>
</td></tr>
<tr class="row_even even">
<td headers="id5907_c0">Graduate studies</td>
<td headers="id5907_c1">Master’s Degree</td>
<td headers="id5907_c2">Master</td></tr>
<tr class="row_odd odd">
<td headers="id5907_c0">Post-graduate studies</td>
<td headers="id5907_c1">Ph.D</td>
<td headers="id5907_c2">Doctorat</td></tr>
</tbody>
</table>
<p>En cas de doute, s’adresser aux services d’aide à la recherche d’emploi cités plus haut : ils devraient être en mesure de juger au cas par cas de la traduction la plus appropriée.</p>
<h3 class="spip"><a id="sommaire_3"></a>Modèles de CV</h3>
<p>Les sites suivants vous présentent des modèles de CV susceptibles de vous aider dans votre rédaction :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.iagora.com/iwork/resumes/cv_usa.html" class="spip_out" rel="external">Iagora.com</a></p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/emploi-stage/article/curriculum-vitae-113145). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
