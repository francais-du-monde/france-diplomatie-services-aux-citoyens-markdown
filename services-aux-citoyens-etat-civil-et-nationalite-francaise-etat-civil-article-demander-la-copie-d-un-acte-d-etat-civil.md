# Demander la copie d’un acte d’état civil

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/etat-civil/article/demander-la-copie-d-un-acte-d-etat-civil#sommaire_1">Lieu de l’événement</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/etat-civil/article/demander-la-copie-d-un-acte-d-etat-civil#sommaire_2">Les actes de naissance et de mariage</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/etat-civil/article/demander-la-copie-d-un-acte-d-etat-civil#sommaire_3">Les actes de reconnaissance</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/etat-civil/article/demander-la-copie-d-un-acte-d-etat-civil#sommaire_4">Les actes de décès</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/etat-civil/article/demander-la-copie-d-un-acte-d-etat-civil#sommaire_5">Apposition de mentions</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Lieu de l’événement</h3>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Si l’événement (naissance, mariage, décès) est survenu en France métropolitaine ou dans les départements et collectivités d’Outre-Mer</p>
<p>Les officiers d’état civil communaux sont les seuls compétents. Vous pouvez effectuer votre <a href="https://mdel.mon.service-public.fr/acte-etat-civil.html" class="spip_out" rel="external">demande de copie ou d’extrait d’acte d’état civil en ligne</a>.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Si l’événement (naissance, mariage, décès) est survenu à l’étranger, ou dans les territoires anciennement sous administration française, et concerne des ressortissants français</p>
<p>Vous pouvez :</p>
<ul class="spip">
<li>Soit faire une <a href="https://pastel.diplomatie.gouv.fr/dali/index2.html" class="spip_out" rel="external">demande en ligne au Service central d’état civil</a></li>
<li>Soit vous adresser à l’<a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/article/annuaire-des-representations-francaises-a-l-etranger" class="spip_in">ambassade ou au consulat</a> qui détient l’acte dans ses registres.<strong>3-2 Les actes concernés</strong></li></ul>
<p>La délivrance des copies et extraits d’actes d’état civil est régie par le <a href="https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000006061111" class="spip_out" rel="external">décret n° 62-921 du 3 août 1962</a> et par l’Instruction générale relative à l’état civil du Ministère de la Justice du 11 mai 1999 modifiée (<a href="https://www.legifrance.gouv.fr/affichJO.do?idJO=JORFCONT000000004571" class="spip_out" rel="external">Journal officiel du 28 juillet 1999</a>).</p>
<h3 class="spip"><a id="sommaire_2"></a>Les actes de naissance et de mariage</h3>
<p>Sous réserve que le requérant indique dans sa demande le nom et le prénom usuel des parents du titulaire de l’acte, des copies intégrales et des extraits avec filiation peuvent être délivrés :</p>
<ul class="spip">
<li>au titulaire de l’acte s’il est majeur ou émancipé</li>
<li>à ses parents</li>
<li>à ses grands-parents</li>
<li>à ses enfants</li>
<li>à son conjoint</li>
<li>à son représentant légal (parent(s), tuteur, curateur</li>
<li>à son mandataire (notaire, avocat).</li></ul>
<p>Les frères et sœurs du titulaire de l’acte ne peuvent obtenir de copies intégrales. Des extraits avec filiation peuvent leur être délivrés s’ils indiquent les nom et prénom usuels des parents du titulaire de l’acte, et s’ils justifient de leur qualité d’héritier.</p>
<p>Les héritiers autres que les descendants, ascendants, frères et sœurs ou conjoint peuvent aussi obtenir des extraits avec filiation sans avoir à indiquer les nom et prénom usuel des parents du titulaire de l’acte, mais ils doivent justifier de leur qualité d’héritier en produisant une attestation notariale.</p>
<p>Toute personne peut solliciter un extrait sans filiation en indiquant seulement la date et le lieu de l’événement, et les noms et prénoms du ou des titulaire(s) de l’acte.</p>
<h3 class="spip"><a id="sommaire_3"></a>Les actes de reconnaissance</h3>
<p>Des copies intégrales des actes de reconnaissance sont délivrées :</p>
<ul class="spip">
<li>au titulaire de l’acte s’il est majeur ou émancipé</li>
<li>à ses parents</li>
<li>à ses grands-parents</li>
<li>à ses enfants</li>
<li>à son conjoint</li>
<li>à son représentant légal (parent(s), tuteur, curateur)</li>
<li>à son mandataire (notaire, avocat)</li>
<li>à ses héritiers à condition qu’ils justifient de cette qualité.</li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Les actes de décès</h3>
<p>Des copies intégrales des actes de décès sont délivrées à tout requérant qui indique la date et le lieu du décès.</p>
<h3 class="spip"><a id="sommaire_5"></a>Apposition de mentions</h3>
<p>Les mentions les plus couramment apposées en marge des actes d’état civil sont les mentions de mariage, de PACS, de décès, de divorce, de séparation et les mentions de reconnaissance.</p>
<p>Peuvent également être apposées des mentions de rectification, voire une mention d’annulation, sur instruction du Procureur de la République ou en exécution d’une décision judiciaire.</p>
<p>Lorsque le mariage, la reconnaissance ou le décès survient à l’étranger, la mention correspondante ne pourra être apposée que si l’acte à mentionner a été dressé ou transcrit sur les registres consulaires.</p>
<p><i>Mise à jour : mars 2016</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/etat-civil/article/demander-la-copie-d-un-acte-d-etat-civil). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
