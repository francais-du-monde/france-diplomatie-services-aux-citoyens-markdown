# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/emploi-stage/article/reglementation-du-travail-110982#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/emploi-stage/article/reglementation-du-travail-110982#sommaire_2">Fêtes légales</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/emploi-stage/article/reglementation-du-travail-110982#sommaire_3">Emploi du conjoint</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<p>La législation du travail ghanéenne se conforme au <i>Labour Act</i> de 2003 (Act 651).</p>
<p>Les principales dispositions pour un salarié sont les suivantes :</p>
<ul class="spip">
<li>Le permis de travail conditionne toute embauche et est obtenu auprès du Ghana Immigration Service (voir notre article <a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-ghana-entree-et-sejour-article-passeport-visa-permis-de-travail-110977.md" class="spip_in">Passeport, visa, permis de travail</a>).</li>
<li>Le salaire, de même que les avantages divers se négocient librement lors du contrat (à titre indicatif, le revenu minimum journalier est de 5,24 cedis ghanéens (GHC) pour l’année 2013).</li>
<li>Le droit aux congés payés annuel est de 15 jours minimum par an.</li>
<li>La journée de travail est de huit heures ; la durée hebdomadaire du travail est de 40 heures. La rémunération des heures supplémentaires est fonction du taux fixé par l’employeur. Le salarié a droit à 48h consécutives de repos hebdomadaire et l’année compte 13 jours fériés payés.</li>
<li>Les congés payés pour maternité s’élèvent à trois mois (six semaines avant et après l’accouchement).</li>
<li>La période d’essai est fixée à six mois maximum ; en cas de rupture de contrat, la durée du préavis est d’un mois en cas de contrat d’une durée de plus de trois ans, et à deux semaines en cas de contrat de moins de trois ans ; les indemnités sont négociables.</li>
<li>L’âge légal de départ à la retraite est de 60 ans (droit à une retraite pleine), voire 55 ans, auquel cas la retraite versée par la SSNIT n’est que partielle.</li>
<li>Le contrat de travail doit être établi par écrit, quelle que soit sa durée.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Fêtes légales</h3>
<h4 class="spip">Fêtes à date fixe</h4>
<ul class="spip">
<li>1er janvier : Jour de l’An</li>
<li>6 mars : Jour de l’Indépendance</li>
<li>1er Mai : Fête du Travail</li>
<li>26 mai : Jour de l’unité africaine</li>
<li>1er juillet : Fête de la République</li>
<li>21 septembre : Founder’s day</li>
<li>7 décembre : Farmers’ day</li>
<li>25 décembre : Noël</li>
<li>26 décembre : Boxing Day</li></ul>
<h4 class="spip">Fêtes à date mobile</h4>
<ul class="spip">
<li>Aid El Fitr : Fête du Ramadan</li>
<li>Aid El Adha : Fête du Sacrifice</li>
<li>Vendredi Saint</li>
<li>Lundi de Pâques</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Emploi du conjoint</h3>
<p>Il existe très peu de possibilités d’emploi sur place pour le conjoint.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/emploi-stage/article/reglementation-du-travail-110982). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
