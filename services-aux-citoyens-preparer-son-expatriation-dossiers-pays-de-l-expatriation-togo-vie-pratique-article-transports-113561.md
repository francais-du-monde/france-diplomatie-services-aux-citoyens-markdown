# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/vie-pratique/article/transports-113561#sommaire_1">Importation de véhicule</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/vie-pratique/article/transports-113561#sommaire_2">Permis de conduire</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/vie-pratique/article/transports-113561#sommaire_3">Code de la route</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/vie-pratique/article/transports-113561#sommaire_4">Assurances et taxes</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/vie-pratique/article/transports-113561#sommaire_5">Achat et location</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/vie-pratique/article/transports-113561#sommaire_6">Immatriculation</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/vie-pratique/article/transports-113561#sommaire_7">Entretien</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/vie-pratique/article/transports-113561#sommaire_8">Réseau routier</a></li>
<li><a id="so_9" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/vie-pratique/article/transports-113561#sommaire_9">Transports en commun</a></li></ul>
<p>La circulation est libre sur tout le territoire, mais des contrôles par barrages de gendarmerie ou militaires peuvent se produire.</p>
<p>En cas d’accident, l’expatrié est le plus souvent considéré comme responsable.</p>
<h3 class="spip"><a id="sommaire_1"></a>Importation de véhicule</h3>
<p>Les normes exigées à l’entrée du véhicule et à la revente sont identiques aux normes françaises.</p>
<h3 class="spip"><a id="sommaire_2"></a>Permis de conduire</h3>
<p>Les permis de conduire français et international sont reconnus.</p>
<h3 class="spip"><a id="sommaire_3"></a>Code de la route</h3>
<p>La circulation s’effectue à droite et la priorité est à droite. La vitesse en ville est limitée à 40 km/h et à 90 km/h hors agglomération.</p>
<h3 class="spip"><a id="sommaire_4"></a>Assurances et taxes</h3>
<p>Au minimum, une assurance au tiers est obligatoire. Les véhicules sont taxés à 54 %. Un impôt est payé sur <strong>tous</strong> les véhicules importés au Togo, en fonction de la valeur définie par la COTEC.</p>
<h3 class="spip"><a id="sommaire_5"></a>Achat et location</h3>
<p>Les marques Peugeot, Citroën et Renault ainsi que des marques étrangères (Toyota, Nissan, Mitsubishi, Suzuki, Isuzu, Hyundai, Mercedes) sont représentées. Il est conseillé d’opter pour un véhicule tout terrain climatisé.</p>
<p>Il est possible d’acheter un véhicule neuf chez les concessionnaires et d’occasion au port à des prix intéressants. La qualité de ces véhicules reste cependant aléatoire.</p>
<h3 class="spip"><a id="sommaire_6"></a>Immatriculation</h3>
<ul class="spip">
<li>Véhicule neuf : la société s’occupe de toutes les formalités administratives.</li>
<li>Véhicule d’occasion acheté au port : le transitaire s’occupe du dédouanement et de toutes les formalités administratives.</li></ul>
<h3 class="spip"><a id="sommaire_7"></a>Entretien</h3>
<p>Un contrôle régulier de l’Etat sur les véhicules entraîne une visite technique semestrielle. Les réparations et l’entretien peuvent être assurés sur place, mais la qualité des services n’est pas toujours satisfaisante. De plus, le prix des pièces détachées est élevé.</p>
<h3 class="spip"><a id="sommaire_8"></a>Réseau routier</h3>
<p>L’état général des axes principaux en ville est acceptable, il existe des chemins de terre plus ou moins carrossables dès que l’on s’en écarte.</p>
<p>A l’intérieur du pays, réhabilitation en cours d’une partie du réseau. L’état des pistes est passable, un véhicule tout terrain est fortement recommandé.</p>
<h3 class="spip"><a id="sommaire_9"></a>Transports en commun</h3>
<p>Les liaisons internes sont assurées par véhicule personnel. Ce moyen est particulièrement utilisé par les expatriés. Il est également possible d’avoir recours aux "taxis brousses". Lors de déplacement en brousse, il est recommandé d’être particulièrement prudent. Pas de vitesse excessive (rencontre d’animaux, dépassement sans visibilité, insuffisance de signalisation). Les accidents de la route sont nombreux, tant en raison des risques pris par les conducteurs, que de l’état des pistes. En ville, les seuls transports en commun utilisés par les expatriés sont le véhicule personnel, les taxis, collectifs ou individuels (il faut compter 2500 francs CFA pour la traversée de Lomé, en taxi individuel).</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/vie-pratique/article/transports-113561). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
