# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/emploi-stage/article/reglementation-du-travail-112187#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/emploi-stage/article/reglementation-du-travail-112187#sommaire_2">Contrat de travail – spécificités</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/emploi-stage/article/reglementation-du-travail-112187#sommaire_3">Fêtes légales</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/emploi-stage/article/reglementation-du-travail-112187#sommaire_4">Emploi du conjoint</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<p>La législation égyptienne prévoit que 90% du personnel d’une entreprise doit être égyptien et représenter au minimum 80% de la masse salariale.</p>
<p>La durée du travail ne doit pas excéder 48 heures par semaine et huit heures par jour. Tout salarié a droit à au moins un jour de repos hebdomadaire.</p>
<p>Le nombre de congés payés est de 15 jours au bout de six mois de service, de 21 jours au bout d’un an et de un mois pour les employés ayant effectué 10 ans de service consécutif ou ayant plus de 50 ans. Chaque année comporte un minimum de 13 jours fériés définis par décret.</p>
<p>Les congés maladie sont payés 75% du salaire-plafond pendant les 90 premiers jours et 85% pour les 90 jours suivants (dans les entreprises industrielles, 100% du salaire-plafond pendant un mois et 75% pendant huit mois). Les congés maternité sont payés, à partir de six mois de service, pendant 90 jours.</p>
<p>La durée de la période d’essai est trois mois au maximum. En cas de démission, un préavis de trois mois peut être exigé de l’employé.</p>
<p>L’âge légal de la retraite est fixé à 60 ans.</p>
<h3 class="spip"><a id="sommaire_2"></a>Contrat de travail – spécificités</h3>
<p>Un contrat de travail en trois exemplaires, rédigé en arabe, doit être signé par l’employeur et l’employé puis être remis à chaque partie ainsi qu’au bureau de sécurité sociale compétent. Le non-respect de cette obligation est passible d’une amende.</p>
<p>Il est conseillé de ne pas omettre, dans un contrat de travail, des clauses essentielles qui en précisent les modalités :</p>
<ul class="spip">
<li>Identification des parties ;</li>
<li>Le lieu de travail et la durée du contrat (durée déterminée ou indéterminée, conditions de la reconduction éventuelle) ;</li>
<li>La fonction à exercer et les liens de subordination ;</li>
<li>Le montant de la rémunération globale annuelle brute (répartition éventuelle de ce montant entre versements locaux et versements en France, évolution de la rémunération au cours des années) ;</li>
<li>Les avantages éventuellement accordés (logement, employés de maison, voiture de fonction ou indemnités d’utilisation, etc.) ;</li>
<li>Le régime de prévoyance, de chômage et de retraite, sans oublier les régimes de retraites complémentaires, en fonction du pays d’affectation ;</li>
<li>Eventuellement, la prise en charge des frais de scolarité des enfants ;</li>
<li>La durée des congés en fonction du pays d’affectation ;</li>
<li>Le mode de voyage et la classe, la prise en charge du voyage aller et du voyage retour pour l’intéressé et sa famille, des voyages en France à l’occasion des congés et éventuellement des voyages en cas de maladie grave de l’intéressé, d’un membre de sa famille ou de décès d’un ascendant direct de l’intéressé ou de son conjoint ;</li>
<li>La prise en charge des dépenses de déménagement à l’aller et au retour à préciser (frais de douane, assurances, transport) ;</li>
<li>Les éventuelles indemnités d’installation et de réinstallation ;</li>
<li>Les conditions de rupture du contrat et ses conséquences et pour l’une ou l’autre des parties, la législation applicable ;</li>
<li>L’existence d’une période d’essai et sa durée ;</li>
<li>Le lieu de signature du contrat (et la législation du travail applicable), le tribunal compétent en cas de différend ;</li>
<li>L’éventuelle visite médicale d’aptitude de l’intéressé et de sa famille avant le départ et à chaque congé.</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Fêtes légales</h3>
<p>Les services administratifs sont généralement fermés le jeudi et le vendredi, le secteur privé plutôt le vendredi et le samedi et les magasins plutôt le dimanche.</p>
<ul class="spip">
<li>1er janvier : jour de l’An</li>
<li>25 avril : anniversaire de la Libération du Sinaï.</li>
<li>1er mai : fête du Travail</li>
<li>18 juin : anniversaire de la République</li>
<li>23 juillet : fête nationale - anniversaire de la Révolution</li>
<li>6 octobre : fête des Forces armées</li>
<li>24 octobre : jour de Suez</li>
<li>23 décembre : jour de la Victoire</li></ul>
<p>Les dates des fêtes religieuses musulmanes - fête du Sacrifice (Grand Baïram), nouvel An musulman (El am Hejir), naissance du Prophète (Mouled), fête de la fin du Ramadan (Petit Baïram) - varient chaque année en fonction du calendrier lunaire.</p>
<h3 class="spip"><a id="sommaire_4"></a>Emploi du conjoint</h3>
<p>Les possibilités sont très limitées. Le chômage est important, le niveau des rémunérations est très bas et les entreprises sont généralement contraintes d’employer 90% d’Egyptiens. Un permis de travail est nécessaire.</p>
<p><i>Mise à jour : avril 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/emploi-stage/article/reglementation-du-travail-112187). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
