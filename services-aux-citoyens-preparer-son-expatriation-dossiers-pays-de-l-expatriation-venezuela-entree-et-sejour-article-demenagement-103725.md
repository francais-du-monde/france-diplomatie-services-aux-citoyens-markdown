# Déménagement

<p>Le passage en douane peut être assez long et compliqué. Les délais moyens d’acheminement sont de 1 à 6 mois entre le départ de France et la livraison à domicile au Venezuela.</p>
<p>Le prix moyen est d’environ 8.000 euros pour 20m3 et les formalités douanières sont faites par le déménageur. Il est possible de faire appel à des transporteurs internationaux.</p>
<p>Pour plus d’information sur les procédures douanières, vous pouvez consulter la page du <a href="http://www.ambafrance-ve.org/?Service-des-Douanes" class="spip_out" rel="external">service des douanes de l’ambassade de France au Venezuela</a> ou notre rubrique <a href="services-aux-citoyens-preparer-son-expatriation-douanes.md" class="spip_in">Douanes</a>.</p>
<p>S’agissant d’un déménagement international, il est primordial de s’entourer d’un maximum de garanties en faisant appel à un professionnel disposant de certifications reconnues (marque NF Service, ISO 9002, etc.)</p>
<p><a href="http://www.csdemenagement.fr/" class="spip_out" rel="external">Chambre syndicale du déménagement</a><br class="manualbr">Téléphone : 01 49 88 61 40 <br class="manualbr">Télécopie : 01 49 88 61 46<br class="manualbr">Courriel : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/entree-et-sejour/article/demenagement-103725#contact#mc#csdemenagement.fr#" title="contact..åt..csdemenagement.fr" onclick="location.href=mc_lancerlien('contact','csdemenagement.fr'); return false;" class="spip_mail">contact<span class="spancrypt"> [at] </span>csdemenagement.fr</a></p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/entree-et-sejour/article/demenagement-103725). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
