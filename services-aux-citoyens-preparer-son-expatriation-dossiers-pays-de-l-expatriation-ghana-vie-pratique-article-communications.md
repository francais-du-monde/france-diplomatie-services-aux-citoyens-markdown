# Communications

<h4 class="spip">Téléphone</h4>
<p>Les communications au Ghana aujourd’hui fonctionnent principalement via la téléphonie mobile. Plusieurs grands opérateurs y sont représentés, Vodafone, MTN, Airtel ou Tigo. L’obtention d’une ligne est simple ; elle se fait sur présentation d’une pièce d’identité dans une des agences de l’opérateur.</p>
<p>Il existe le choix entre un abonnement avec paiement mensuel sur facture ou la possiblité d’achat de cartes pré-payées à gratter avec nombre d’unités variable. Ces cartes (« Scratch cards ») sont en ventes un peu partout en ville ou sont proposées par des vendeurs ambulants dans les rues.</p>
<p>Il existe aussi la possibilité d’installer une ligne de téléphone fixe à domicile par l’opérateur Vodafone.</p>
<h4 class="spip">Internet</h4>
<p>L’accès à internet peut se faire par le biais du téléphone portable ou par l’installation d’un modem à domicilepar l’opérateur Vodafone.</p>
<p>Il existe aussi sur le marché des clés USB – modem avec carte SIM fonctionnant sur le mode des cartes pré-payées.</p>
<h4 class="spip">Poste</h4>
<p>Le délai d’acheminement du courrier est variable (trois à huit jours) avec une garantie de réception aléatoire. Toutefois, il est recommandé d’avoir une boîte postale faute de quoi le courrier pourrait ne pas être délivré à domicile.</p>
<p>Par ailleurs, la réception de colis est possible au Ghana moyennant l’acquittement d’une taxe d’importation.</p>
<p>Il est possible de faire acheminer son courrier urgent par une expédition rapide de type DHL, FedEx ou EMS.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/vie-pratique/article/communications). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
