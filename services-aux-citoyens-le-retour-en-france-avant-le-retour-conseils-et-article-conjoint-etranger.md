# Conjoint étranger

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/avant-le-retour-conseils-et/article/conjoint-etranger#sommaire_1">Le visa du conjoint étranger d’un Français</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/avant-le-retour-conseils-et/article/conjoint-etranger#sommaire_2">La carte de séjour ou de résident du conjoint étranger</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Le visa du conjoint étranger d’un Français</h3>
<p>Si votre conjoint n’est pas ressortissant d’un État membre de l’Union européenne ou de l’Espace économique européen, d’Andorre, de Monaco, de Suisse, du Saint-Siège, de Saint-Martin ou du Liechtenstein, <strong>il doit être en possession d’un visa de long séjour. </strong></p>
<p>Les visas de long séjour sont délivrés par les autorités consulaires françaises du pays où réside le conjoint.</p>
<p>Vous pouvez également consulter la rubrique "<a href="http://www.diplomatie.gouv.fr/fr/venir-en-france/" class="spip_in">Venir en France</a>".</p>
<p>Vous y trouverez aussi des informations sur la procédure à suivre pour déposer une demande.</p>
<p>Enfin, vous pouvez consulter le portail de l’administration française : <a href="http://vosdroits.service-public.fr/particuliers/F16162.xhtml" class="spip_out" rel="external">www.service-public.fr</a> (Rubrique « Etrangers en France&gt;Entrée en France des étrangers&gt;Conditions pour entrer en France&gt;Visas de long séjour pour la France »).</p>
<p>Le visa de long séjour délivré au conjoint d’un citoyen français vaut dorénavant titre de séjour et autorisation de travail pour une durée d’un an. Son titulaire bénéficie des droits attachés à la carte de séjour temporaire « vie privée et familiale ».</p>
<p>De nouvelles dispositions législatives relatives à l’intégration républicaine dans la société française prévoient désormais que le conjoint étranger d’un citoyen français doit se soumettre, sauf exception, dans le pays dans lequel il sollicite son visa de long séjour, à une évaluation de son degré de connaissance de la langue française et des valeurs de la République, et au besoin, suivre une formation de deux mois maximum dans ce même pays. Le visa ne peut être délivré que sur production d’une attestation de suivi de cette formation, lorsqu’elle a été jugée nécessaire.</p>
<h3 class="spip"><a id="sommaire_2"></a>La carte de séjour ou de résident du conjoint étranger</h3>
<p>Si les conditions requises pour l’obtention de la carte de résident ne sont pas remplies en terme de durée du mariage, le conjoint étranger d’un citoyen français bénéficie de plein droit d’une carte de séjour temporaire, à condition que la communauté de vie n’ait pas cessé depuis le mariage, que le conjoint n’ait pas perdu la nationalité française et lorsque le mariage a été célébré à l’étranger, qu’il ait été préalablement transcrit sur les Registres de l’État-civil français.</p>
<p>Vous êtes français, vos enfants de moins de 21 ans à votre charge sont étrangers : ils peuvent bénéficier de plein droit d’une carte de résident à condition de détenir un visa de long séjour (pour un séjour de plus de trois mois).</p>
<p>La signature du contrat d’accueil et d’intégration (CAI) est obligatoire pour les personnes admises pour la première fois au séjour en France et qui reçoivent de plein droit une carte de séjour « vie privée et familiale ».</p>
<p>Pour effectuer les démarches, vous pouvez vous adresser :</p>
<ul class="spip">
<li>à la préfecture, à la sous-préfecture, à la mairie ou au commissariat de votre lieu de résidence ;</li>
<li>à Paris, à la préfecture de police.</li></ul>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Consultez le <a href="http://vosdroits.service-public.fr/particuliers/N20306.xhtml" class="spip_out" rel="external">portail de l’administration française</a>. </p>
<p><i>Mise à jour : janvier 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/avant-le-retour-conseils-et/article/conjoint-etranger). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
