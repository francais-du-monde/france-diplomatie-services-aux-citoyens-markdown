# Fiscalité

<h2 class="rub22881">Fiscalité du pays</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/fiscalite-22881/#sommaire_1">Année fiscale</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/fiscalite-22881/#sommaire_2">Barème de l’impôt</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/fiscalite-22881/#sommaire_3">Imposition des sociétés</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/fiscalite-22881/#sommaire_4">TVA et autres impôts indirects</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/fiscalite-22881/#sommaire_5">Quitus fiscal</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/fiscalite-22881/#sommaire_6">Solde du compte en fin de séjour</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/fiscalite-22881/#sommaire_7">Coordonnées des centres d’information fiscale</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Année fiscale</h3>
<p>L’année fiscale commence au 1er janvier de chaque année et se termine le 31 décembre. Les déclarations d’impôts sont effectuées chaque année à partir du mois de mars et les impôts sont payables en trois mensualités.</p>
<h3 class="spip"><a id="sommaire_2"></a>Barème de l’impôt</h3>
<h4 class="spip">Régime général de l’imposition sur le revenu</h4>
<p>L’imposition sur le revenu est régie par la loi 4172/2013 du 23 juillet 2013 portant réforme du Code général des impôts sur le revenu. Sont assujetties toutes les personnes qui acquièrent un revenu en Grèce, indépendamment de leur nationalité et de leur lieu de résidence.</p>
<p>Toute personne physique ayant atteint l’âge de 18 ans doit faire une déclaration, indépendamment du montant de ses revenus et de déclaration pour les revenus d’activités salariales imposées à l’étranger. L’obligation concerne toutes les personnes physiques âgées de plus de 18 ans et n’étant pas sous tutelle, dans la mesure où elles résident en Grèce (183 jours minimum). « La déclaration doit comporter obligatoirement tous les revenus indépendamment de leur mode d’imposition, même ceux qui sont exonérés d’impôts ».</p>
<p>Une personne exerçant une profession ou possédant une entreprise doit faire obligatoirement sa déclaration par Internet. La déclaration est faite entre le 1er février et le 30 juin de l’année.</p>
<p>La déclaration doit mentionner l’ensemble des revenus, indépendamment du mode d’imposition, ainsi que les revenus exonérés. Pour les revenus imposés forfaitairement, la déclaration fait apparaître la retenue déjà versée.</p>
<h4 class="spip">Impôt sur le revenu des personnes physiques</h4>
<p>La loi réduit à 3 les tranches d’imposition sur les salaires et les retraites. La franchise d’impôt a été supprimée et est remplacée par un abattement de 2100 € pour les revenus inférieurs à 21000€ (cf. infra).</p>
<p><strong>Barème fiscal en 2013</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Tranche de revenu</td>
<td>Taux d’imposition</td>
<td>Impôt par tranche</td>
<td>Impôt total</td></tr>
<tr class="row_even even">
<td>Jusqu’à 25 000 €</td>
<td>22%</td>
<td>5500</td>
<td>5500</td></tr>
<tr class="row_odd odd">
<td>25 000 - 42 000 €</td>
<td>32%</td>
<td>5440</td>
<td>10 940</td></tr>
<tr class="row_even even">
<td>&gt; 42000 €</td>
<td>42%</td>
<td></td>
<td></td></tr>
</tbody>
</table>
<p>Le revenu provenant d’entreprise en nom propre ou de profession libérale est imposé comme suit :</p>
<p><strong>Barème fiscal en 2013</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Tranche de revenu</td>
<td>Taux d’imposition</td>
<td>Impôt par tranche</td>
<td>Impôt total</td></tr>
<tr class="row_even even">
<td>Jusqu’à 50 000 €</td>
<td>26%</td>
<td>13 000</td>
<td>13 000</td></tr>
<tr class="row_odd odd">
<td>&gt; 50 000 €</td>
<td>33%</td>
<td></td>
<td></td></tr>
</tbody>
</table>
<p>Pour les nouvelles entreprises commerciales individuelles et les nouveaux travailleurs indépendants qui commencent une activité à compter du 1er janvier 2013 et pendant les trois premières années de leur activité, le taux de la première tranche (26%) est réduit de 50% jusqu’à 10000€ de revenu.</p>
<p>Est considéré comme revenu salarié, déduction faite des contributions sociales, le revenu des sociétés en nom propre de prestation de service ou de profession libérale, à condition que :</p>
<ul class="spip">
<li>un contrat écrit ait été signé avec les personnes qui reçoivent leurs services</li>
<li>les personnes qui reçoivent leurs services ne dépassent pas le nombre de trois ou si ce nombre est supérieur, 75% des revenus bruts de l’entreprise ou de la profession proviennent d’une seule personne.</li></ul>
<p>Le <strong>revenu d’exploitation agricole est imposé à 13%.</strong> Pour l’exercice 2013, la loi prévoit l’application aux entreprises agricoles du barème des salariés et des retraités. La dépense objective minimale du contribuable est fixée à 3000€ par an pour les célibataires et à 5000€ pour les couples qui font une déclaration commune.</p>
<p>Les revenus immobiliers et mobiliers sont soumis au barème d’imposition suivant :</p>
<p><strong>Barème fiscal en 2013</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Tranche de revenu</td>
<td>Taux d’imposition</td>
<td>Impôt par tranche</td>
<td>Impôt total</td></tr>
<tr class="row_even even">
<td>Jusqu’à 12 000 €</td>
<td>11%</td>
<td>1200</td>
<td>1200</td></tr>
<tr class="row_odd odd">
<td>&gt; 12 000 €</td>
<td>33%</td>
<td></td>
<td></td></tr>
</tbody>
</table>
<p>La franchise d’imposition est supprimée. Le montant brut des revenus immobiliers est soumis à un impôt complémentaire de 1,5% qui passe à 3% si la superficie de la maison dépasse 300m2, ou s’il s’agit d’un bail professionnel.</p>
<p>Taxation des signes extérieurs de richesse : voitures de plus de 1929 cm3 (de 5 à 10% de la dépense annuelle forfaitaire), avions et hélicoptères (10% de la dépense annuelle forfaitaire) ou piscines (10% de la dépense annuelle forfaitaire).</p>
<p><strong>Abattements :</strong></p>
<p>L’impôt sur les revenus des salariés et retraités est réduit de la façon suivante :</p>
<ul class="spip">
<li>pour un revenu jusqu’à 21000 € : de 2100 €. Si l’impôt qui en résulte est inférieur à 2100€ la baisse est limitée au montant de l’impôt.</li>
<li>pour un revenu supérieur à 21000 € : baisse de 100 € par tranche de 1000 € de revenu jusqu’à atteindre 2100 €.</li></ul>
<p>Afin de conserver le montant de l’abattement, <strong>le contribuable doit présenter des factures d’achat de biens ou de services, dépensés par lui-même ou sa famille, égales à 25% de son revenu déclaré.</strong> Il n’est pas exigé que le montant des factures dépasse 10 500€.</p>
<p>Les factures peuvent concerner les dépenses d’entretien de la maison, les transports de biens, l’entretien et la réparation de voitures ou motos, les pièces de rechange de voitures, les huiles et antigels, la location de taxi, carburants, droits d’inscription dans les instituts de langues étrangères. Ne sont pas comprises les dépenses médicales, les pensions, les donations qui viennent en déduction de l’impôt.</p>
<p>Dans le cas où les factures sont inférieures au montant demandé, l’impôt est augmenté de la différence entre la somme des factures présentées et le montant exigé, <strong>augmenté de 22%.</strong></p>
<p>Par ailleurs <strong>l’impôt est réduit de 10%</strong> pour chacune des dépenses suivantes :</p>
<ul class="spip">
<li>dépenses médicales et hospitalières non couvertes par les caisses de sécurité sociale et dépassant 5% du revenu imposable. La réduction ne peut dépasser 3000 €.</li>
<li>pension alimentaire versée par un conjoint à un autre. La réduction d’impôt ne peut dépasser 1500€.</li>
<li>les donations supérieures à 100 €, faites aux fondations, à l’Eglise, aux établissements culturels ou éducatifs à caractère non lucratif, etc. La réduction ne peut dépasser 5% du revenu imposable.Un abattement de 200 € bénéficie aux handicapés, victimes de guerre, etc.</li></ul>
<p>Dans le cas où le total des réductions d’impôt est supérieur à l’impôt lui-même, la différence n’est pas remboursée ni compensée.</p>
<p>Il faut déduire du montant du total de l’impôt résultant du barème fiscal :</p>
<ul class="spip">
<li>les retenues à la source. Si la retenue est supérieure à l’impôt, la différence est remboursée au contribuable.</li>
<li>l’impôt versé dans un Etat avec lequel la Grèce a passé une convention de non double imposition, pour le montant du revenu perçu dans cet Etat.</li></ul>
<p>L’impôt est payable en trois tranches égales dont la première est versée le dernier jour ouvrable du mois suivant la réception de l’avis d’imposition, la deuxième tranche le troisième mois et la dernière le cinquième mois. Lorsque l’avis d’imposition est reçu en août ou septembre, l’impôt est versé en deux tranches et, après octobre, en une seule tranche.</p>
<p><strong>Le versement comptant permet de bénéficier d’un bonus de 1,5%.</strong></p>
<p>Les ressortissants de l’UE qui perçoivent plus de 90% de leur revenu en Grèce peuvent bénéficier des abattements ci-dessus.</p>
<h4 class="spip">Revenus de valeurs mobilières et transferts d’actions</h4>
<p>Les intérêts des dépôts et des prêts obligataires perçus en Grèce ou à l’étranger <strong>sont imposés à 15%</strong> (au lieu de 10% précédemment).</p>
<p>Les résidents en Grèce qui perçoivent des intérêts sur leurs dépôts ou prêts obligataires à l’étranger doivent verser l’impôt en même temps que leur déclaration avant le 31 janvier de l’année suivant le dépôt, accompagné du certificat de la banque étrangère. L’impôt retenu à l’étranger est déduit du montant de l’impôt grec.</p>
<p>Les plus-values résultant des transferts d’actions de sociétés anonymes locales non cotées à la Bourse d’Athènes au profit de personnes physiques, morales grecques ou étrangères, <strong>sont imposées au taux de 20%.</strong></p>
<p>L’impôt sur les transactions boursières est majoré et passe de 0,2/1000 à 0,35/1000 à partir du 1er août 2013.</p>
<p>Les bénéfices acquis par des personnes physiques ou des entreprises qui tiennent des registres avec la méthode de comptabilité simple ou double, de la vente d’actions de sociétés cotées à la Bourse d’Athènes à un prix supérieur au prix d’achat, sont <strong>imposés à hauteur de 20%</strong> si les actions sont acquises après le 1er juillet 2013. L’impôt est à la charge du vendeur.</p>
<h3 class="spip"><a id="sommaire_3"></a>Imposition des sociétés</h3>
<h4 class="spip">Imposition des sociétés de personnes</h4>
<p>La franchise d’imposition de 5000€ est supprimée. Le revenu net est imposé comme suit, déduction faite des bénéfices exonérés d’impôts ou imposés forfaitairement et des bénéfices provenant de dividendes de SA ou de coopératives ou de parts de SARL.</p>
<p>Pour en savoir plus : voir <i>Le guide des affaires en Grèce - UbiFrance » (2013)</i></p>
<h3 class="spip"><a id="sommaire_4"></a>TVA et autres impôts indirects</h3>
<h4 class="spip">Modification du Code de la TVA</h4>
<p>Membre de l’Union européenne, la Grèce applique le régime communautaire de la TVA dans son commerce intérieur et extérieur.</p>
<p>Le taux normal est de 23%. Depuis 2006, la TVA s’applique également aux ventes d’immeubles neufs.</p>
<p>Un taux réduit, de 13%, s’applique aux biens de première nécessité (alimentation, produits agricoles, produits pharmaceutiques, courant électrique, etc.).</p>
<p>Un taux de 6,5% s’applique aux livres, journaux, périodiques et certaines activités culturelles (cinéma, théâtre).</p>
<p><strong>Possibilité de choisir le mode d’imposition :</strong><br class="manualbr">La location d’espace destiné à une activité professionnelle du locataire est soumise à TVA de façon autonome, ou peut être imposée dans le cadre de contrats mixtes, si le propriétaire le souhaite et soumet pour cet espace une demande de choix d’imposition.</p>
<p>Le choix d’imposition peut concerner tout ou partie de l’immobilier. La demande de choix peut être déposée avant l’utilisation de l’immobilier, ou dans les 30 jours suivant le démarrage de l’activité.</p>
<p>Ces dispositions s’appliquent aux entreprises d’exploitation de centres commerciaux qui avaient déposé une demande de choix d’imposition et avaient reçu une approbation avant le 31 décembre 2012.</p>
<h4 class="spip">Droits d’accise</h4>
<p>La Grèce applique des taxes sur les produits spiritueux, sur le tabac, sur les carburants.</p>
<p>Une taxe dite de « classification » s’applique aux automobiles en fonction de leur cylindrée, de leur ancienneté et de leur technologie. La taxe est calculée sur le prix de vente en gros de l’industrie automobile.</p>
<h4 class="spip">Taxe foncière</h4>
<p>Le propriétaire ou l’usufruitier du bien immobilier peut demander à son bureau fiscal la séparation de la taxe foncière (EETHDE) de sa note d’électricité et un certificat après versement des traites de 2012, ou en cas de difficulté de paiement, d’un montant de 50 € au moins.</p>
<h4 class="spip">Taxe sur les voitures de luxe</h4>
<p>La taxe sur les voitures de luxe s’applique aux véhicules d’occasion avec le même barème que celui appliqué aux véhicules neufs mais elle est calculée sur la base de leur valeur fiscale.</p>
<h4 class="spip">Imposition des navires sous pavillons étrangers</h4>
<p>Une taxe est appliquée aux navires sous pavillon étranger gérés par des sociétés grecques ou étrangères installées en Grèce, dans les mêmes conditions, critères et coefficients applicables aux navires sous pavillon grec. Cette imposition s’applique sous réserve de l’application de l’imposition des bénéfices provenant de l’exploitation de navires sous pavillon étranger et des dispositions prévues par les conventions bilatérales de non double imposition ainsi que des conventions maritimes bilatérales.</p>
<p>Les sociétés de gestion des navires doivent effectuer chaque année, en janvier, une déclaration au bureau des impôts des navires (« DOY Ploion ») mentionnant le nom, le pavillon et l’âge du bateau.</p>
<p>La loi prévoit l’exonération de toute taxe, contribution ou retenue sur le revenu des actionnaires des sociétés de portefeuille (holding companies) qui ont directement ou indirectement des actions dans des sociétés maritimes battant pavillon grec ou étranger si l’exploitation de leur navire est faite par une société grecque ou étrangère installée en Grèce.</p>
<h3 class="spip"><a id="sommaire_5"></a>Quitus fiscal</h3>
<p>Un quitus fiscal n’est pas exigé à la sortie du pays.</p>
<h3 class="spip"><a id="sommaire_6"></a>Solde du compte en fin de séjour</h3>
<p>Le compte peut être soldé en cours comme en fin de séjour.</p>
<h3 class="spip"><a id="sommaire_7"></a>Coordonnées des centres d’information fiscale</h3>
<p>Les personnes assujetties à l’IRPP doivent demander un formulaire au service fiscal de leur région d’installation. Les non-résidents qui perçoivent des revenus en Grèce doivent s’adresser à la <strong>Direction du service des impôts des résidents étrangers</strong> (18 rue Lykourgou, Athènes, Tél 210 52 45 308- 52 25 502, Fax 210 52 28 208- 52 29 226). Toutefois, si le revenu provient de l’exercice d’une entreprise individuelle ou d’une profession libérale en Grèce, le service compétent est celui du siège de l’entreprise.</p>
<p>Il existe un Bureau de communication et d’information du citoyen (G.E.P.O.). Ce service, indépendant du ministère des Finances, ne répond que verbalement aux demandes. Il s’agit d’un service qui a été mis en place afin de répondre aux questions fiscales simples.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Bureau de communication et d’Information du citoyen (G.E.P.O.) </strong><br class="manualbr">10 rue Karaghéorghi Servias<br class="manualbr">101 84 Athènes<br class="manualbr">Tél. : (30) 210 33 75 059/61 <br class="manualbr">Fax : (30) 210 33 75 064 </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Lieu des centres de plaintes fiscales pour l’Attique, Péloponnèse, Thessalie, Epire, Crète, îles de la mer Egée et de la mer ionienne : </strong><br class="manualbr">2, rue Kolonou et Piréos <br class="manualbr">104 37 Athènes <br class="manualbr">2ème étage – bureau 22 <br class="manualbr">du lundi au vendredi de 07h30 à 15h00 <br class="manualbr">Tél. : (30) 210 52 39 703 / 52 21 944 / 52 32 390 / 52 44 747 <br class="manualbr">Fax : (30) 210 52 39 701 </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.minfin.gr/" class="spip_out" rel="external">Ministère des Finances grec</a></p>
<p><i>Mise à jour : novembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-grece-fiscalite-22881-article-convention-fiscale-110389.md" title="Convention fiscale">Convention fiscale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-grece-fiscalite-22881-article-fiscalite-du-pays-110388.md" title="Fiscalité du pays">Fiscalité du pays</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/fiscalite-22881/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
