# Vivre sa retraite à l’étranger

<h4 class="spip">La prise de contact avec votre caisse de retraite</h4>
<p>Le fait de partir vivre à l’étranger pendant votre retraite ne vous empêchera pas de percevoir vos pensions.</p>
<p>Il est donc important de prendre contact avec votre caisse de retraite afin de connaître les différents aspects relatifs au versement de votre retraite à l’étranger<span class="spip_note_ref"> [<a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/retraite/article/vivre-sa-retraite-a-l-etranger#nb1" class="spip_note" rel="footnote" title="L’allocation de solidarité aux personnes âgées (ASPA) et l’allocation (...)" id="nh1">1</a>]</span>.</p>
<p>Votre caisse de retraite pourra ainsi vous renseigner sur les différentes modalités de paiement qui existent pour votre pays d’accueil (virement, lettre chèque, mise à disposition des sommes dues au guichet d’une banque),ainsi que sur l’ensemble des justificatifs à lui fournir.</p>
<p>Il est à noter que si vous êtes titulaire d’une pension militaire ou civile de fonctionnaire, d’une retraite du combattant ou du traitement de la Légion d’honneur à titre militaire et de la médaille militaire, vous pouvez demander à en percevoir le montant auprès de la trésorerie/comptabilité de l’ambassade ou du consulat de France à l’étranger.</p>
<h4 class="spip">Le certificat de vie</h4>
<p>Dès que vous avez connaissance de votre nouvelle adresse, vous devez la signaler à votre caisse de retraite primaire et/ou complémentaire qui vous adressera l’imprimé nécessaire au paiement ou à la poursuite du paiement de votre retraite.</p>
<p>Vous devrez faire remplir un <strong>certificat de vie</strong> par l’autorité compétente de votre pays d’accueil (mairie, notaire public) ou à défaut par le Consulat de France et l’adresser à chaque caisse de retraite primaire ou complémentaire à laquelle vous êtes affilié(e).</p>
<p><strong>La périodicité de ce document est généralement d’une année</strong>. Toutefois, elle peut être trimestrielle, bimensuelle ou mensuelle pour certains pays.</p>
<p><strong>Il est impératif d’effectuer cette démarche afin de pouvoir continuer à percevoir votre retraite dans le pays d’accueil, car la non-production de ce document interrompt le versement de votre pension.</strong></p>
<p>Observations : Suite aux dispositions du décret n° 2000-1277 du 26 décembre 2000, les consulats et ambassades français à l’étranger peuvent refuser de légaliser les attestations d’existence.</p>
<p>La circulaire CNAV n° 2002/47 du 25 juillet 2002 a rappelé le dispositif de contrôle d’existence, les règles en matière de paiement et les imprimés à utiliser par les caisses de l’Assurance retraite.</p>
<p><strong>Pour en savoir plus</strong> :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  site de la <a href="http://www.cnav.fr/" class="spip_out" rel="external">Caisse nationale d’assurance vieillesse</a></p>
<p><i>Mise à jour : mars 2013</i></p>
<p><span class="spip_note_ref">[<a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/retraite/article/vivre-sa-retraite-a-l-etranger#nh1" class="spip_note" title="Notes 1" rev="footnote">1</a>] </span>L’allocation de solidarité aux personnes âgées (ASPA) et l’allocation supplémentaire d’invalidité (ASI) ne sont versées qu’aux résidents en France.</p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/retraite/article/vivre-sa-retraite-a-l-etranger). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
