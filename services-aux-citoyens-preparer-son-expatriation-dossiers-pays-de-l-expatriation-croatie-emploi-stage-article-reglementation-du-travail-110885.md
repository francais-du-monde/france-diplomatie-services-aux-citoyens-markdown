# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/emploi-stage/article/reglementation-du-travail-110885#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/emploi-stage/article/reglementation-du-travail-110885#sommaire_2">Contrat de travail – spécificités</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/emploi-stage/article/reglementation-du-travail-110885#sommaire_3">Fêtes légales</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/emploi-stage/article/reglementation-du-travail-110885#sommaire_4">Création d’entreprises – spécificités</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<p>La législation du travail a été adoptée le 8 juin 1995. Elle est en cours de réforme.</p>
<p>L’organisation du travail est régie par le Code du travail et les conventions collectives signées entre les syndicats d’employeurs et de salariés.</p>
<p>La durée du travail hebdomadaire est de 40 heures (article 30 du Code du travail). Il peut être accompli des heures supplémentaires, mais pas plus de 108 heures par an.</p>
<p>Les congés annuels doivent être au minimum de 18 jours ouvrables pour les personnes majeures et de 24 jours ouvrables pour les personnes n’ayant pas atteint leur majorité.</p>
<p>A partir de 20 salariés, les salariés peuvent élire un ou plusieurs représentants du personnel, qui pourront former un conseil des salariés à la demande d’un syndicat ou de 10% au moins des salariés.</p>
<p>Le droit de grève est reconnu par la loi.</p>
<h3 class="spip"><a id="sommaire_2"></a>Contrat de travail – spécificités</h3>
<p>L’employeur doit établir un contrat de travail par écrit, avant la signature du contrat. L’employeur doit faire connaître à l’employé ses droits et ses obligations résultant du contrat, ainsi que les conditions de salaire.</p>
<p>Le contrat de travail doit impérativement contenir les informations suivantes (article 12 du Code du travail) : l’identification des parties, le type de travail pour lequel la personne est embauchée, le lieu de ce travail, la date et le début du travail, la durée du travail, le salaire brut, le temps de travail par jour et par semaine.</p>
<h4 class="spip">Contrat à durée déterminée</h4>
<p>Le code du travail en vigueur depuis 2010, en cours de réforme, a reçu ses premiers amendements en juin 2013. Ces amendements touchent plus particulièrement les contrats à durée déterminée qui jusqu’à présent ne pouvaient excéder une durée cumulée de trois ans. A présent, il n’y a plus de restriction sur la durée des premiers contrats CDD, à condition de justifications précises. Il ne peut être conclu que dans les cas prévus par la loi. Un tel contrat ne peut prendre fin avant sa date d’expiration, sauf si les parties en conviennent autrement.</p>
<h4 class="spip">Contrat à durée indéterminée</h4>
<p>Comme son nom l’indique, aucune durée n’est convenue explicitement.</p>
<h4 class="spip">Rupture du contrat de travail</h4>
<p>Comme le stipulent les articles 103 à 122 du Code du travail,le contrat de travail ne peut être rompu que pour une raison légale figurant dans le code du travail :</p>
<ul class="spip">
<li>décès du salarié,</li>
<li>expiration du contrat à durée déterminée,</li>
<li>départ à la retraite,</li>
<li>incapacité,</li>
<li>accord mutuel,</li>
<li>démission du salarié,</li>
<li>décision de justice.</li></ul>
<p>La loi a prévu trois cas de licenciement : le licenciement économique, le licenciement pour cause réelle et sérieuse et le licenciement pour faute.</p>
<p>Une période de préavis doit être respectée, elle est définie à l’article 113 du Code du travail.</p>
<p>Les amendements 2013 au Code du travail permettent désormais aux employeurs de résilier les contrats de travail durant les périodes d’essai sans motif spécifique.</p>
<h3 class="spip"><a id="sommaire_3"></a>Fêtes légales</h3>
<ul class="spip">
<li>1er janvier : Jour de l’an / Nova Godina</li>
<li>6 janvier : Epiphanie / Sveta Tri Kralja</li>
<li>21 avril (2014) : Lundi de Pâques / Uskrsnji ponedjeljak</li>
<li>1er mai : Fête du travail / Praznik rada</li>
<li>19 juin : Fête Dieu / Tijelovo</li>
<li>22 juin : Jour de l’antifascisme / Dan antifasisticke borbe</li>
<li>25 juin : Fête Nationale croate / Dan Drzavnosti</li>
<li>5 août : Reconnaissance civile / Dan domovinske zahvalnosti</li>
<li>15 août : Assomption / Velika Gospa</li>
<li>8 octobre : Fête de l’indépendance / Dan neovisnosti</li>
<li>1er novembre : Toussaint / Dan Svih Svetih</li>
<li>25 décembre : Noël / Bozic</li>
<li>26 décembre : Lendemain de Noël / Sveti Stjepan</li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Création d’entreprises – spécificités</h3>
<p><a href="http://www.hitro.hr/Default.aspx?sec=18" class="spip_out" rel="external">Comment créer une société en Croatie</a>.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/emploi-stage/article/reglementation-du-travail-110885). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
