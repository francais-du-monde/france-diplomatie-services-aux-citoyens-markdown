# Curriculum vitae

<p><strong>Rédaction</strong></p>
<p>Le CV pour postuler à un emploi à Madagascar ne comporte pas de particularité par rapport à la France : soyez précis, en évitant les formulations vagues et en personnalisant votre CV en fonction de chaque employeur. Soyez concis et efficace dans la rédaction des principales rubriques de votre CV (formation, expérience professionnelle, connaissances linguistiques et informatiques, activités extra-professionnelles). Limitez-vous à une page (deux maximum).</p>
<p><strong>Diplômes (équivalence, légalisation)</strong></p>
<p>Il n’y a pas d’équivalence de diplômes légalement attestée, mais les diplômes français peuvent être reconnus par les recruteurs.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/emploi-stage/article/curriculum-vitae-111183). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
