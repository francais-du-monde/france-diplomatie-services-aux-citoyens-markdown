# Régime local de sécurité sociale

<p>Vous trouverez une présentation détaillée du système de sécurité sociale grec sur le site de notre partenaire, le <a href="http://www.cleiss.fr/" class="spip_out" rel="external">CLEISS</a> (Centre des liaisons européennes et internationales de sécurité sociale). En voici la table des matières :</p>
<ul class="spip">
<li><a href="http://www.cleiss.fr/docs/regimes/regime_grece.html#a" class="spip_out" rel="external">Généralités</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_grece.html#b" class="spip_out" rel="external">Assurance maladie, maternité, décès</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_grece.html#c" class="spip_out" rel="external">Accidents du travail et maladies professionnelles</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_grece.html#d" class="spip_out" rel="external">Assurance pension</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_grece.html#e" class="spip_out" rel="external">Chômage</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_grece.html#f" class="spip_out" rel="external">Prestations familiales</a></li></ul>
<p><i>Mise à jour : novembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/protection-sociale/article/regime-local-de-securite-sociale-110386). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
