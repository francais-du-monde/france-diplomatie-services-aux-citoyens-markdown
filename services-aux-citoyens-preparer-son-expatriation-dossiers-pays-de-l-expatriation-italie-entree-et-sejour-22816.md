# Entrée et séjour

<h2 class="rub22816">Passeport, visa, permis de travail</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/entree-et-sejour-22816/#sommaire_1">Permis de séjour</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/entree-et-sejour-22816/#sommaire_2">Inscription à l’état civil (anagrafe) / Certificat de résidence</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/entree-et-sejour-22816/#sommaire_3">Permis de travail</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/entree-et-sejour-22816/#sommaire_4">Numéro d’identification fiscal</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/entree-et-sejour-22816/#sommaire_5">Demande de protection sociale italienne</a></li></ul>
<p>Quel que soit le motif de votre séjour, renseignez-vous auprès de la section consulaire de l’ambassade d’Italie à Paris qui vous informera sur la règlementation en matière d’entrée et de séjour en Italie, règlementation que vous devrez impérativement respecter. N’hésitez pas à consulter le site ministère de l’Intérieur italien afin de vous familiariser avec les modalités d’entrée et de séjour dans le pays.</p>
<p>Le Consulat de France en Italie n’est pas l’organisme compétent pour répondre à vos demandes d’information concernant votre séjour en Italie.</p>
<h3 class="spip"><a id="sommaire_1"></a>Permis de séjour</h3>
<p>La carte de séjour n’existe plus depuis le mois d’avril 2007. <strong>Les citoyens de l’UE n’ont pas besoin de permis pour séjourner en Italie</strong>.</p>
<p>Les ressortissants de pays n’appartenant pas à l’Union Européenne devront, en cas de résidence de plus de trois mois sur le territoire italien, dans les huit jours qui suivent leur arrivée, déclarer leur présence et se faire établir un permis de séjour auprès de la <i>Questura</i>  (préfecture de police) de la province de résidence (à Rome, - Divisione stranieri - Via Teofilo Patini, 23 -).</p>
<p>Informations sur le site :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.poliziadistato.it" class="spip_out" rel="external">Poliziadistato.it</a> / rubrique " Per il cittadino stranieriingresso "</p>
<h3 class="spip"><a id="sommaire_2"></a>Inscription à l’état civil (anagrafe) / Certificat de résidence</h3>
<p>Pour les citoyens français, <strong>pour un séjour excédant trois mois</strong>, l’attestation d’inscription à l’état civil de la commune de résidence est requise. La loi italienne oblige en effet toute personne résidant en Italie (salariée, étudiante ou possédant les ressources financières suffisantes et une police d’assurance sanitaire) à se faire inscrire dans les registres de recensement de son lieu de résidence principale.</p>
<p>Le formulaire d’inscription est disponible sur le site : <a href="http://poliziadistato.it/" class="spip_out" rel="external">Poliziadistato.it</a> / "Per il cittadino stranieri / Cittadini della comunità europea / Documenti / Richiesta iscrizione anagrafe".</p>
<p>Les citoyens européens exerçant une activité salariée devront fournir une preuve de leur emploi ; pour les étudiants, une copie de leur carte d’étudiant ; pour les personnes sans emploi, une attestation d’inscription à une assurance maladie.</p>
<h3 class="spip"><a id="sommaire_3"></a>Permis de travail</h3>
<p>Tout citoyen français a le droit de séjourner pour une durée supérieure à 3 mois sur le territoire d’un autre Etat membre de l’Union européenne ou d’un autre Etat partie à l’accord sur l’Espace économique européen pour y exercer une activité professionnelle salariée ou non salariée.</p>
<p>Il n’est plus nécessaire de posséder un permis de travail en Italie.</p>
<h3 class="spip"><a id="sommaire_4"></a>Numéro d’identification fiscal</h3>
<p>Un numéro d’identification fiscal gratuit (<i>codice fiscale</i>) est nécessaire pour pouvoir effectuer certaines transactions en Italie, comme l’acquisition d’un téléphone portable ou d’une voiture ; la signature d’un contrat de location ; pour l’ouverture d’un compte en banque ; ou simplement enregistrer son curriculum vitae sur une base de donnée. Unique et personnel, <strong>il est votre numéro d’identification auprès de l’administration italienne</strong>. Chaque membre de la famille résidant avec vous doit posséder le sien, y compris les enfants. <strong>L’attribution du code fiscal </strong><i>(codice fiscale),</i> numéro d’identité fiscale, peut être sollicitée, gratuitement, auprès d’un <strong>Consulat général d’Italie</strong> avant votre arrivée.</p>
<p>Ce numéro peut être obtenu <strong>auprès d’un des bureaux de l’Agenzia delle Entrate de votre ville de résidence</strong>, sur présentation de documents d’identité en cours de validité et leurs photocopies. Un formulaire de demande devra être rempli sur place.</p>
<p>Informations sur le site <a href="http://www.agenziaentrate.it/" class="spip_out" rel="external">Agenzia Entrate</a> / Servizi / Codice fiscale</p>
<h3 class="spip"><a id="sommaire_5"></a>Demande de protection sociale italienne</h3>
<p>Si vous vous installez en Italie, il vous faudra solliciter votre <strong>inscription auprès de l’Agence sanitaire locale </strong>(<i>Azienda Sanitaria Locale</i>) ou A.S.L., de votre quartier afin de pouvoir bénéficier du système de soins gratuits.</p>
<p>Pour obtenir votre carte d’assuré social, vous devez vous munir des documents suivants ainsi que de leurs photocopies :</p>
<ul class="spip">
<li>Une pièce d’identité en cours de validité ;</li>
<li>Le code fiscal de chaque membre adulte de la famille ;</li>
<li>Le certificat de résidence (le cas échéant, le document provisoire ("tagliando") ;</li>
<li>Une fiche d’état civil « Stato di famiglia » délivré par la mairie de votre lieu de résidence ;</li>
<li>Une demande de choix de médecin ;</li></ul>
<p>Une fois cette carte obtenue, le nom d’un médecin généraliste sera désigné. Toute consultation sera gratuite, sans paiement anticipé. Pour les médicaments en pharmacie il n’y aura pas non plus de règlement anticipé. Les consultations chez un spécialiste public seront gratuites mais dans le privé les honoraires seront élevés et les remboursements limités, sauf si le patient cotise à une mutuelle.</p>
<p><strong>Sites à consulter :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.ambparigi.esteri.it/Ambasciata_Parigi" class="spip_out" rel="external">Section consulaire de l’ambassade d’Italie à Paris</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-italie-entree-et-sejour-22816-article-animaux-domestiques-109934.md" title="Animaux domestiques">Animaux domestiques</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-sante.md" title="Vaccination">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-italie-entree-et-sejour-22816-article-demenagement-109932.md" title="Déménagement">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-italie-entree-et-sejour-22816-article-passeport-visa-permis-de-travail-109931.md" title="Passeport, visa, permis de travail">Passeport, visa, permis de travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/entree-et-sejour-22816/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
