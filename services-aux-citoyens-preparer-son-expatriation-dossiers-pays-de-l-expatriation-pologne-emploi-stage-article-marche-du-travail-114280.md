# Marché du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/emploi-stage/article/marche-du-travail-114280#sommaire_1">Secteurs à fort potentiel</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/emploi-stage/article/marche-du-travail-114280#sommaire_2">Secteurs à faible potentiel</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/emploi-stage/article/marche-du-travail-114280#sommaire_3">Rémunération</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Secteurs à fort potentiel</h3>
<p>Les secteurs à forts potentiels sont ceux des services (hôtellerie, tourisme), travaux publics, bâtiment, grande distribution et les emplois les plus souvent proposés à des Français sont : ingénieurs d’affaires, de projets, commerciaux et technico-commerciaux, chefs de produits, acheteurs, responsables administratif et financier, directeur.</p>
<h3 class="spip"><a id="sommaire_2"></a>Secteurs à faible potentiel</h3>
<p>Droit, médecine, architecture, comptabilité car dans ces secteurs d’activité, il faut en plus d’une connaissance parfaite des lois polonaises, avoir une habilitation (comptable, juriste, etc.) et les diplômes étrangers sont peu reconnus dans les faits (en dépit d’un système officiel d’équivalence).</p>
<h3 class="spip"><a id="sommaire_3"></a>Rémunération</h3>
<p>Le montant du salaire minimum légal est fixé chaque année, au plus tard le 15 septembre pour l’année suivante, par négociation lors de la réunion de la Commission tripartite des affaires socio-économiques.</p>
<p>Au 1er janvier 2014, le salaire minimum brut mensuel s’élevait pour les travailleurs à plein temps à 1680 zlotys, soit 405 euros après avoir été relevé de 5% (soit 80 zlotys ou 19,3€) par rapport à 2013 ; le Conseil des ministres a annoncé le 10 juin 2014 le relèvement du salaire minimum à 1750 PLN (422€) en 2015. Après déduction des cotisations d’assurance sociale, de l’impôt sur le revenu et des cotisations de sécurité sociale, le salaire minimum s’élève actuellement en termes nets à 1237 PLN (298,3€).</p>
<p>Au mois de mai, le salaire mensuel brut moyen s’établissait dans le secteur privé à 3878,3 PLN, soit 935,4€, en hausse de 4,8% en glissement annuel, selon les données du Bureau des statistiques (GUS).</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/emploi-stage/article/marche-du-travail-114280). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
