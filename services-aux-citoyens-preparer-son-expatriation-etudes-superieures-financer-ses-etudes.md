# Financer ses études

<h2 class="rub12800">Financer ses études ou un stage à l’étranger</h2>
<p><strong class="caractencadre-spip spip">Programmes du ministère des Affaires étrangères</strong></p>
<p>D’une façon générale, le ministère des Affaires étrangères ne finance pas la mobilité internationale des étudiants français, sauf dans le cadre du Collège d’Europe à Bruges et son antenne à Natolin en Pologne, institution européenne d’excellence que la France s’engage à soutenir dans une perspective de renforcement de la construction européenne. Le ministère des Affaires étrangères propose, dans le cadre du <a href="http://www.coleurope.eu/website/study/apply-masters-programme" class="spip_out" rel="external">Collège d’Europe</a> des bourses à des étudiants français admis dans ce Collège.</p>
<p><strong class="caractencadre-spip spip">Autres sources de financements</strong></p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/etudes-superieures/financer-ses-etudes/article/bourse-du-fonds-d-amitie-france-nouvelle-zelande-appel-a-candidatures-2016" class="spip_in">Bourse du Fonds d’amitié France / Nouvelle Zélande</a></li>
<li><a href="http://www.canadainternational.gc.ca/france/study-etudie/grants-bourses.aspx?lang=fra&amp;view=d" class="spip_out" rel="external">Bourses des organismes canadiens délivrant des bourses d’études</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures-financer-ses-etudes-article-programmes-des-autres-ministeres.md" class="spip_in">Programmes des autres ministères</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures-financer-ses-etudes-article-programmes-proposes-par-les-organismes-d-echanges.md" class="spip_in">Programmes des Organismes d’échanges</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures-financer-ses-etudes-article-programmes-proposes-par-les-organismes-publics-francais.md" class="spip_in">Programmes des Organismes publics français</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures-financer-ses-etudes-article-programmes-proposes-par-les-conseils-regionaux.md" class="spip_in">Programmes des Conseils régionaux</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures-financer-ses-etudes-article-programmes-proposes-par-les-13243.md" class="spip_in">Programmes des Conseils départementaux</a></li></ul>
<p><i>Mise à jour : octobre 2015</i></p>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/etudes-superieures/financer-ses-etudes/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
