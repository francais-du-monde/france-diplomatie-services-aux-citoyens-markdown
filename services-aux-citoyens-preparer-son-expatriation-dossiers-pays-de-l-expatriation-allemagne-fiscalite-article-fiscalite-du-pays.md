# Fiscalité du pays

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/fiscalite/article/fiscalite-du-pays#sommaire_1">Déclaration de revenus</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/fiscalite/article/fiscalite-du-pays#sommaire_2">Modalités de paiement de l’impôt sur le revenu</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/fiscalite/article/fiscalite-du-pays#sommaire_3">Année fiscale</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/fiscalite/article/fiscalite-du-pays#sommaire_4">Barème de l’impôt sur le revenu pour les personnes physiques</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/fiscalite/article/fiscalite-du-pays#sommaire_5">Pour en savoir plus</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Déclaration de revenus</h3>
<p>Le montant exact de l’impôt est établi après l’expiration de l’année civile, sur le revenu perçu par le contribuable au cours de cette période. En principe, tout contribuable doit faire <strong>une déclaration annuelle de ses revenus</strong> de l’année précédente (<i>Einkommenssteuererklärung</i>) avant le 31 mai auprès de l’hôtel des impôts (<i>Finanzamt</i>). Toute personne ayant des enfants à charge et bénéficiant de l’<strong>allocation familiale</strong> (<i>Kindergeld</i>) doit également remplir une déclaration.</p>
<p>Le site internet <a href="http://www.elster.de" class="spip_out" rel="external">ELSTER</a> permet de remplir ces obligations déclaratives en ligne.</p>
<h3 class="spip"><a id="sommaire_2"></a>Modalités de paiement de l’impôt sur le revenu</h3>
<p>En Allemagne, les salariés paient leur impôt sur le revenu sous forme de retenue à la source sur les salaires (<i>Lohnsteuer)</i>.</p>
<p>La carte de retenue à la source sur salaire (<i>Lohnsteuerkarte</i>) a été dématérialisée en 2012. Chaque salarié communique à son employeur son numéro d’identification fiscal et sa date de naissance. Ces éléments permettent à l’employeur d’avoir accès aux données relatives au salarié permettant de procéder au montant adéquat de retenue à la source (catégorie fiscale, situation de famille, nombre d’enfants et affiliation religieuse) auprès d’un serveur de l’administration fiscale, laquelle tient à jour ces différentes informations.</p>
<p>Les revenus de capitaux mobiliers font également l’objet d’une retenue à la source de 25% (plus taxe de solidarité).</p>
<p><strong>Les autres revenus sont à mentionner lors du dépôt de la déclaration annuelle des revenus.</strong></p>
<h3 class="spip"><a id="sommaire_3"></a>Année fiscale</h3>
<p>La période de l’année fiscale correspond à l’année civile (1er janvier – 31 décembre).</p>
<h3 class="spip"><a id="sommaire_4"></a>Barème de l’impôt sur le revenu pour les personnes physiques</h3>
<p>Les classes d’impositions s’agissant de la retenue à la source sur salaire (Lohnsteuer) sont les suivantes :</p>
<table class="spip">
<thead><tr class="row_first"><th id="id0b56_c0">Classes </th><th id="id0b56_c1">Conditions </th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="id0b56_c0">I</td>
<td headers="id0b56_c1">- salariés célibataires - salariés mariés, veufs ou divorcés qui n’appartiennent ni à la classe III, IV ou V - salariés non-résidents</td></tr>
<tr class="row_even even">
<td headers="id0b56_c0">II</td>
<td headers="id0b56_c1">Même cas qu’en classe I lorsque ces contribuables ont un enfant à charge élevé au foyer pour lesquels ils bénéficient de l’abattement parent seul (Haushaltsfreibetrag)</td></tr>
<tr class="row_odd odd">
<td headers="id0b56_c0">III</td>
<td headers="id0b56_c1">Salariés mariés non séparés de fait : un des conjoints n’a aucun revenu salarial  Salariés veufs lorsque le couple au jour du décès était soumis à une obligation fiscale illimitée et non séparé de fait, l’année du décès  Salariés divorcés, l’année du divorce</td></tr>
<tr class="row_even even">
<td headers="id0b56_c0">IV</td>
<td headers="id0b56_c1">Salariés mariés, non séparés de fait lorsque les deux conjoints sont salariés et n’ont pas opté pour les classes III et V</td></tr>
<tr class="row_odd odd">
<td headers="id0b56_c0">IV avec facteur</td>
<td headers="id0b56_c1">Nouvelle option depuis 2010 permettant une meilleure répartition de la retenue à la source entre conjoints avec une différence importante de salaire.</td></tr>
<tr class="row_even even">
<td headers="id0b56_c0">V</td>
<td headers="id0b56_c1">Salariés mariés, lorsque l’autre conjoint a demandé l’application de la classe III</td></tr>
<tr class="row_odd odd">
<td headers="id0b56_c0">VI</td>
<td headers="id0b56_c1">Salariés qui perçoivent en même temps plusieurs salaires de différents employeurs.</td></tr>
</tbody>
</table>
<p>Il existe une contribution de solidarité qui est retenue à la source avec l’impôt sur le revenu et représente 5,5% de cet impôt sur le revenu.</p>
<p>Les retenues opérées par l’employeur se composent de l’impôt sur le revenu, de la taxe de solidarité, le cas échéant de l’impôt d’église.</p>
<p>L’administration (BMF - ministère des Finances allemand) publie des tableaux permettant en fonction du salaire brut et de la classe d’imposition de connaître de montant de la retenue à la source sur salaire (Lohnsteuertabelle). De nombreux sites internet permettent également de connaître le salaire net à partir du salaire brut (calcul incluant dans ce cas également les prélèvements sociaux).</p>
<p>Lorsqu’un salarié peut justifier de dépenses déductibles excédant les forfaits dont les barèmes de la retenue tiennent déjà compte (frais professionnels d’un montant supérieur au forfait de 1000 € automatiquement déduit, frais liés à la garde des enfants etc.), il est possible de demander à l’administration la prise en compte de ces frais pour l’application de la retenue mensuelle sur salaire (Lohnsteuer). Le formulaire dénommé « Antrag auf Lohnsteuer-Ermässigung » (disponible dans les centre des finances et sur internet) doit être rempli et adressé au centre des finances (Finanzamt) territorialement compétent accompagné des pièces justificatives idoines. L’administration, si elle donne droit à la demande, modifiera alors les données figurant sur la carte électronique de retenue à la source permettant à l’employeur d’ajuster le montant de retenue à la source.</p>
<h3 class="spip"><a id="sommaire_5"></a>Pour en savoir plus</h3>
<p>Le ministère des Finances du land de résidence est l’interlocuteur pour ce qui concerne l’imposition sur le revenu. On peut y accéder (ou en obtenir les coordonnées) sur le <a href="http://www.bundesfinanzministerium.de/Web/DE/Home/home.html" class="spip_out" rel="external">portail internet du ministère des Finances fédéral</a>.</p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/fiscalite/article/fiscalite-du-pays). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
