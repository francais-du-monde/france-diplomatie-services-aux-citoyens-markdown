# Actes notariés à l’étranger

<h2 class="rub5304">‬Actes notariés‬ à l’étranger</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/legalisation-et-notariat/actes-notaries-a-l-etranger/#sommaire_1">Quels actes authentiques peut-on établir à l’étranger ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/legalisation-et-notariat/actes-notaries-a-l-etranger/#sommaire_2">Quels sont les points communs entre les actes authentiques établis en France et à l’étranger ?</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/legalisation-et-notariat/actes-notaries-a-l-etranger/#sommaire_3">Quelles sont les différences entre les actes authentiques établis en France et à l’étranger ?</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/legalisation-et-notariat/actes-notaries-a-l-etranger/#sommaire_4">Comment faire établir un acte authentique à l’étranger ?</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/legalisation-et-notariat/actes-notaries-a-l-etranger/#sommaire_5">L’établissement d’un acte authentique est-il payant ?</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/legalisation-et-notariat/actes-notaries-a-l-etranger/#sommaire_6">Où demander la copie d’un acte authentique d’un contrat de mariage ?</a></li></ul>
<p>A l’étranger, dans la plupart des ambassades et consulats, des  <strong>actes authentiques</strong> (ou actes notariés) peuvent être établis s’ils sont destinés à produire des effets sur le territoire national. En Europe toutefois, depuis le 1er janvier 2005, l’usager peut seulement s’adresser à un notaire local (dans les 27 Etats membres de l’Union européenne ainsi que dans les pays suivants : Andorre, Islande, Monaco, Norvège, Saint-Siège et Suisse).</p>
<h3 class="spip"><a id="sommaire_1"></a>Quels actes authentiques peut-on établir à l’étranger ?</h3>
<p>Les actes authentiques sont de nature très variée.</p>
<p>Il s’agit notamment de :</p>
<ul class="spip">
<li>procurations (pour consentir ou accepter une donation, acheter, vendre, emprunter, hypothéquer, recueillir ou renoncer à une succession, etc.) ;</li>
<li>contrats de mariage ;</li>
<li>désignation de la loi applicable au mariage ;</li>
<li>consentements à adoption (simple) de la part d’un majeur de nationalité française ;</li>
<li>testaments ;</li>
<li>donations entre époux ;</li>
<li>actes de notoriété ;</li>
<li>inventaires successoraux (sous réserve de l’existence d’une convention consulaire avec le pays de résidence) ;</li>
<li>prestations de serment en matière successorale ; certificats d’hérédité.</li></ul>
<p>Cette liste n’est pas limitative.</p>
<h3 class="spip"><a id="sommaire_2"></a>Quels sont les points communs entre les actes authentiques établis en France et à l’étranger ?</h3>
<p>Comme les actes reçus par un notaire en France, les actes établis dans une ambassade ou un consulat sont revêtus de <strong>la force exécutoire</strong>. Directement établis en droit français, ils permettent ainsi d’éviter des frais supplémentaires de traduction et de légalisation.</p>
<h3 class="spip"><a id="sommaire_3"></a>Quelles sont les différences entre les actes authentiques établis en France et à l’étranger ?</h3>
<p>L’agent diplomatique ou consulaire emprunte au notaire de profession certaines de ses attributions mais, sous deux aspects, son activité notariale est limitée :</p>
<ul class="spip">
<li>il peut simplement informer l’usager et non orienter ses choix (par exemple, pour la rédaction d’un contrat de mariage ou d’un testament). Il n’a pas, contrairement au notaire titulaire, de devoir de conseil.</li>
<li>il ne peut en aucun cas accepter un mandat particulier ou se charger des formalités consécutives à la réception d’un acte.</li>
<li>le notaire consulaire n’est pas rémunéré pour son activité notariale. Les droits acquittés sont exclusivement des droits de chancellerie.</li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Comment faire établir un acte authentique à l’étranger ?</h3>
<p>Vérifier que des actes authentiques peuvent être établis à l’ambassade ou au consulat (selon que l’on réside ou non en Europe) et se prémunir des éléments suivants :</p>
<ul class="spip">
<li>une preuve de nationalité ;</li>
<li>une preuve d’identité et d’état civil ;</li>
<li>l’adresse du domicile dans la circonscription.</li></ul>
<p>Il convient également de se procurer un <strong>projet d’acte</strong> auprès du notaire qui a été préalablement consulté en France.</p>
<p>Ainsi, par exemple :</p>
<ul class="spip">
<li>si de futurs époux projettent de se marier à l’étranger et d’établir au préalable un contrat de mariage de droit français, ils prendront préalablement l’attache de leur notaire en France. Celui-ci les conseillera sur le régime matrimonial le mieux adapté à leur situation et préparera un projet de contrat de mariage à l’intention de la représentation diplomatique ou consulaire compétente ;</li>
<li>quand des parents ou des grands-parents désirent consentir une donation à leurs enfants ou petits-enfants domiciliés à l’étranger, ces derniers doivent accepter la donation par procuration authentique. Ils donnent ainsi mandat à un tiers qui recevra la donation en leurs noms. Le notaire chargé du dossier de la donation adressera donc à l’ambassade ou au consulat une maquette de la procuration dans laquelle les parties à l’acte seront désignées, les biens identifiés, les charges et conditions de la donation précisées.</li></ul>
<h3 class="spip"><a id="sommaire_5"></a>L’établissement d’un acte authentique est-il payant ?</h3>
<p>L’établissement de tout acte authentique est soumis à la perception de droits de chancellerie.</p>
<h3 class="spip"><a id="sommaire_6"></a>Où demander la copie d’un acte authentique d’un contrat de mariage ?</h3>
<p>Une copie authentique (ou expédition) de l’acte authentique peut être délivrée par le poste diplomatique ou consulaire qui le conserve, à la demande du comparant à l’acte ou bien d’un ayant droit. Comme l’acte lui-même, cette délivrance donne lieu à la perception de droits de chancellerie.</p>
<p>S’agissant plus spécialement d’un contrat de mariage, l’époux accompagnera sa demande d’une copie de l’acte de mariage ou du livret de famille. Il s’adressera, dans le courant de l’année de réception du contrat, à l’ambassade ou consulat qui en est dépositaire, ou, à partir de l’année suivante :<br class="manualbr"><strong>Ministère des Affaires étrangères</strong><br class="manualbr">Service des Français à l’étranger<br class="manualbr">Sous-direction de l’Administration des Français<br class="manualbr">27, rue de la Convention<br class="manualbr">75732 Paris Cedex 15.</p>
<p>Pour plus d’informations sur les différents aspects du notariat français, <a href="http://www.notaires.fr/" class="spip_out" rel="external">consulter le site Internet du Conseil Supérieur du Notariat</a></p>
<p><i>Mise à jour : avril 2013</i></p>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/legalisation-et-notariat/actes-notaries-a-l-etranger/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
