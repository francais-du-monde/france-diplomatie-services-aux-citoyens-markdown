# Loisirs et culture

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/vie-pratique/article/loisirs-et-culture#sommaire_1">Tourisme</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/vie-pratique/article/loisirs-et-culture#sommaire_2">Activités culturelles</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/vie-pratique/article/loisirs-et-culture#sommaire_3">Sports</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/vie-pratique/article/loisirs-et-culture#sommaire_4">Télévision – Radio</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/vie-pratique/article/loisirs-et-culture#sommaire_5">Presse française</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Tourisme</h3>
<p>La Finlande offre de larges possibilités dans ce domaine et une excellente organisation. Le pays est très riche en sites touristiques naturels d’accès aisé (l’archipel du sud-ouest - îles Åland -, la région des lacs, la Carélie, la Laponie, la capitale). Quelques sites présentent un intérêt historique : château et cathédrale de Turku, château d’Hameenlinna et de Savonlinna, nombreuses petites églises médiévales (surtout dans l’ouest), manoirs du XVIIIème et XIXème siècles, etc. La Finlande a sept sites inscrits au patrimoine mondial de l’humanité de l’UNESCO.</p>
<p>Pour en savoir plus :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.visitfinland.com/" class="spip_out" rel="external">Visitfinland.com</a> </p>
<h3 class="spip"><a id="sommaire_2"></a>Activités culturelles</h3>
<p><strong>Activités culturelles en français</strong></p>
<p>Le service de coopération et d’action culturelle et l’Institut français de Finlande proposent une importante programmation artistique (théâtre, musique, danse, cirque). Il y a, en moyenne, une manifestation d’importance par mois.</p>
<p>De même, les expositions d’art plastique sont nombreuses et régulières, en moyenne six par an parfois dans des galeries, mais également dans les musées et en province, avec le soutien du SCAC et de l’institut français auxquelles il faut ajouter les expositions de photographies, les événements multimédia et art-vidéo.</p>
<p><strong>Institut français de Finlande</strong><br class="manualbr">Kaapelitehdas<br class="manualbr">Tallberginkatu 1 C 135 (2e étage)<br class="manualbr">00180 Helsinki<br class="manualbr">Tel : <strong>+358 (0) 9 2510 210</strong> <br class="manualbr">Site Internet : <a href="http://www.france.fi/" class="spip_out" rel="external">www.france.fi</a></p>
<p><strong>Activités culturelles locales</strong></p>
<p>Les programmes nationaux sont en langues finnoise et suédoise. Il y a de nombreuses radios locales.</p>
<p>Il y a quatre chaînes hertziennes nationales (YLE TV 1, YLE TV 2, MTV3, Nelonen). Le câble numérique permet de capter TV5 Monde, France 2 et Arte, ainsi que les canaux internationaux occidentaux ou CNN. Les paraboles satellites donnent la possibilité de recevoir un éventail plus large de chaînes françaises ou étrangères, à condition de bénéficier d’un abonnement domicilié en France pour TPS/Canalsat et d’avoir obtenu l’accord de l’opérateur.</p>
<p>Environ 200 films sortent chaque année à Helsinki dans 364 salles très confortables. Les principales sorties sont celles des films américains. Les productions finlandaises représentent 25 % des entrées. Quelques films français récents sont projetés (plus nombreux à la cinémathèque).</p>
<p>La vie musicale est très présente à Helsinki tout au long de l’année pendant les festivals de musique, de danse et de théâtre qui sont très nombreux dans tous le pays.</p>
<h3 class="spip"><a id="sommaire_3"></a>Sports</h3>
<p>Tous les sports sont possibles (notamment rugby, football, tennis, volley-ball, pétanque, golf…). Sans oublier les sports d’hiver (ski de fond et ski alpin, patinage, hockey sur glace…).</p>
<p>Il y a de nombreux clubs (tennis, football, hockey, musculation…). Les clubs de sport, dotés de piscine, saunas, équipements de musculation, restent, pour la plupart, ouverts jusqu’à une heure avancée de la journée (22 heures), en ouvrant leurs portes dès 7 heures du matin.</p>
<p>Pour la chasse il faut obtenir un permis général auprès de la police locale d’une part et d’autre part auprès de l’association propriétaire des terrains de chasse. Pour importer une arme, il faut faire une déclaration en douane et à la police du lieu d’arrivée. Le permis est obligatoire. Pour certaines chasses (élans et bientôt l’ours), il est nécessaire de passer un examen. Les périodes d’ouverture et de fermeture de la chasse dépendent des espèces chassées.</p>
<p>Pour la pêche autre que la pêche à la ligne il faut un permis à compter de 16 ans. Il est obtenu auprès des autorités communales après s’être acquitté d’une faible taxe payable dans les bureaux de poste. S’ajoute à cela, l’autorisation du propriétaire des eaux. La pêche est autorisée toute l’année selon les espèces de poisson, la meilleure période étant le printemps et l’automne.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.slu.fi/eng/finnish_sports_federation/sports_in_finland/" class="spip_out" rel="external">www.slu.fi</a> </p>
<h3 class="spip"><a id="sommaire_4"></a>Télévision – Radio</h3>
<p>Des films français sont diffusés sur les chaînes locales toujours en version originale sous-titrée. L’installation d’une antenne parabolique permet de capter toutes les chaînes françaises (en particulier Arte, la Cinquième, France 2).</p>
<p>Des programmes français sont diffusés sur capital FM (reprise des programmes de R.F.I. Paris), 2h30 par jour à des horaires différents selon les villes.</p>
<h3 class="spip"><a id="sommaire_5"></a>Presse française</h3>
<p>Les plus grands quotidiens français (le Monde, Libération, le Figaro…) et magazines d’actualité (le Nouvel Observateur, le Point, l’Express…) ou d’économie sont disponibles dans les grandes librairies du centre-ville avec un retard de deux jours pour les quotidiens et d’une semaine pour les grands magazines.</p>
<p>Les librairies académiques (magasins Stockmann) à Helsinki et dans quelques grandes villes de province, ainsi que les librairies <i>Suomalainen kirjakauppa</i>, assurent la diffusion de la presse et d’ouvrages français.</p>
<p><i>Mise à jour : janvier 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/vie-pratique/article/loisirs-et-culture). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
