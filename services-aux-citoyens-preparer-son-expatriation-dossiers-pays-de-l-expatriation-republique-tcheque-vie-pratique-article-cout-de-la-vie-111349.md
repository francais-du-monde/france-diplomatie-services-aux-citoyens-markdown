# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/vie-pratique/article/cout-de-la-vie-111349#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/vie-pratique/article/cout-de-la-vie-111349#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/vie-pratique/article/cout-de-la-vie-111349#sommaire_3">Alimentation</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/vie-pratique/article/cout-de-la-vie-111349#sommaire_4">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>L’unité monétaire est la couronne tchèque (CZK).</p>
<p>En janvier 2014, la couronne tchèque (CZK) vaut 0,03643 euros, c’est-à-dire qu’un euro équivaut à 27,39 couronnes tchèques.</p>
<p>Les cartes bancaires internationales sont acceptées dans les hôtels, les grands magasins et la plupart des restaurants. Il est possible de s’approvisionner dans les distributeurs automatiques. Toutefois, le moyen de paiement le plus commun reste très majoritairement l’argent liquide.</p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<p>Il n’existe plus de contrôle des changes. La couronne tchèque est convertible.</p>
<p>Les virements entre comptes bancaires français et tchèques sont sûrs et rapides. Ces virements dits « virement SEPA » prennent au maximum trois jours.</p>
<p>L’ouverture d’un compte bancaire se fait très facilement sur présentation d’une pièce d’identité, d’un justificatif de domicile et d’un justificatif de séjour délivré par les autorités tchèques.</p>
<p><strong>A noter</strong> : la notion de « compte joint » n’existe pas.</p>
<p>Pour en savoir plus :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Consultez le <a href="http://www.czechtourism.com/" class="spip_out" rel="external">portail touristique tchèque</a> </p>
<h3 class="spip"><a id="sommaire_3"></a>Alimentation</h3>
<h4 class="spip">Prix moyen d’un repas dans un restaurant</h4>
<p>Le prix d’un repas au restaurant peut varier considérablement selon la catégorie : de 12 à 86 euros selon qu’il s’agit d’un déjeuner dans une pizzeria ou d’un dîner gastronomique.</p>
<h3 class="spip"><a id="sommaire_4"></a>Evolution des prix</h3>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/vie-pratique/article/cout-de-la-vie-111349). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
