# Recherche d’emploi

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/emploi-stage/article/recherche-d-emploi#sommaire_1">Outils pour la recherche d’emploi</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/emploi-stage/article/recherche-d-emploi#sommaire_2">Organismes pour la recherche d’emploi</a></li></ul>
<p><strong>Il est essentiel d’être familiarisé avec Internet et de disposer d’une adresse électronique.</strong> En effet, la plupart des recruteurs communiquent désormais par ce biais et demandent aux candidats d’envoyer leur C.V. par courriel.</p>
<p><strong>La connaissance de l’anglais est indispensable</strong>, avec quelques nuances toutefois selon le secteur d’activité visé. Un niveau moyen ou scolaire peut être suffisant dans les domaines de la restauration ou de l’hôtellerie. Les postes dans les <i>call centres </i>nécessitent une bonne compréhension de la langue. En revanche pour des emplois plus qualifiés et à haute technicité une bonne maîtrise de l’anglais est nécessaire.</p>
<h3 class="spip"><a id="sommaire_1"></a>Outils pour la recherche d’emploi</h3>
<h4 class="spip">Journaux</h4>
<p>Sur place, de nombreuses offres d’emploi sont publiées dans les principaux titres de la presse quotidienne :</p>
<ul class="spip">
<li><a href="http://www.irishtimes.com/" class="spip_out" rel="external">The Irish Times</a></li>
<li><a href="http://www.independent.ie/" class="spip_out" rel="external">The Irish Independent</a></li>
<li><a href="http://www.examiner.ie/" class="spip_out" rel="external">The Irish Examiner</a></li>
<li><a href="http://www.sbpost.ie/" class="spip_out" rel="external">The Sunday Business Post</a></li></ul>
<h4 class="spip">Sites internet</h4>
<p><strong>Sites français :</strong></p>
<ul class="spip">
<li><a href="http://www.pole-emploi.fr/accueil/" class="spip_out" rel="external">Pôle emploi</a></li>
<li><a href="http://www.pole-emploi-international.fr/www.eei.com.fr/eei/accueil.jsp" class="spip_out" rel="external">Pôle emploi international</a></li>
<li><a href="http://www.afpa.fr/" class="spip_out" rel="external">Association pour la formation professionnelle des adultes</a></li></ul>
<p><strong>Sites irlandais :</strong></p>
<ul class="spip">
<li><a href="http://www.approachpeople.com/" class="spip_out" rel="external">Approachpeople.com</a> (ce site s’adresse plus particulièrement aux Français cherchant un emploi en Irlande)</li>
<li><a href="http://www.careerjet.ie/" class="spip_out" rel="external">Careerjet.ie</a></li>
<li><a href="http://www.collinsmcnicholas.ie/" class="spip_out" rel="external">Collinsmcnicholas.ie</a></li>
<li><a href="http://www.construction-jobs.ie/" class="spip_out" rel="external">Construction-jobs.ie</a> (emplois dans le secteur du bâtiment)</li>
<li><a href="http://www.dublinwork.com/" class="spip_out" rel="external">Dublinwork.com</a> (ce site recense en majorité des offres d’emploi dans la capitale)</li>
<li><a href="http://www.frsrecruitment.com/" class="spip_out" rel="external">Frsrecruitment.com</a></li>
<li><a href="http://www.graftonrecruitment.com/" class="spip_out" rel="external">Graftonrecruitment.com</a></li>
<li><a href="http://www.gumtree.ie/" class="spip_out" rel="external">Gumtree.ie</a> (offres d’emploi sur Cork et Dublin)</li>
<li><a href="http://www.headhunt.ie/" class="spip_out" rel="external">Headhunt.ie</a></li>
<li><a href="http://www.hoteljobs.ie/" class="spip_out" rel="external">Hoteljobs.ie</a> (hôtellerie)</li>
<li><a href="http://www.jobfinder.ie/" class="spip_out" rel="external">Jobfinder.ie</a> (comptabilité, éducation, finance, santé et secteur public)</li>
<li><a href="http://www.jobs.ie/" class="spip_out" rel="external">Jobs.ie</a></li>
<li><a href="http://www.manpower.ie/" class="spip_out" rel="external">Manpower.ie</a></li>
<li><a href="http://www.merc.ie/" class="spip_out" rel="external">Merc.ie</a> (ce site est surtout spécialisé dans le recrutement de cadres supérieurs)</li>
<li><a href="http://www.monster.ie/" class="spip_out" rel="external">Monster.ie</a></li>
<li><a href="http://www.osborne.ie/" class="spip_out" rel="external">Osborne.ie</a></li>
<li><a href="http://www.recruitireland.com/" class="spip_out" rel="external">Recruitireland.com</a></li>
<li><a href="http://www.rescon.ie/" class="spip_out" rel="external">Rescon.ie</a> (commerce, finance, vente et marketing)</li>
<li><a href="http://www.salesplacement.ie/" class="spip_out" rel="external">Salesplacement.ie</a> (vente et marketing)</li>
<li><a href="http://www.sigmarrecruitment.com/" class="spip_out" rel="external">Sigmarrecruitment.com</a></li>
<li><a href="http://www.thejob.ie/" class="spip_out" rel="external">Thejob.ie</a> (secteur des services, de la restauration de l’hôtellerie et de la vente)</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Organismes pour la recherche d’emploi</h3>
<p>Pour les organismes en France susceptibles de vous aider dans votre recherche d’emploi, vous pouvez consulter notre rubrique thématique <a href="services-aux-citoyens-preparer-son-expatriation-emploi.md" class="spip_in">Emploi</a>.</p>
<p>Les agences locales pour l’emploi</p>
<p>L’Autorité nationale pour la formation et l’emploi (FAS - <i>Irish National Training and Employment Authority</i>) est le service public irlandais de l’emploi et l’équivalent du <a href="http://www.pole-emploi.fr/accueil/" class="spip_out" rel="external">Pôle emploi</a> en France.</p>
<p>Il est possible de s’y inscrire comme demandeur d’emploi, d’accéder aux offres et de participer à des actions de formation. Il est indispensable de justifier de sa qualité de résident en Irlande pour bénéficier de ces services.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>FAS (Foras Aiseanna Saothair) - Head Office</strong><br class="manualbr">27-33 Upper Baggot Street - Dublin 4<br class="manualbr">Téléphone : [353] (0)1 607 05 00 - Télécopie : [353] (0)1 607 06 08<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/emploi-stage/article/recherche-d-emploi#info#mc#fas.ie#" title="info..åt..fas.ie" onclick="location.href=mc_lancerlien('info','fas.ie'); return false;" class="spip_mail">Courriel</a>  </p>
<h4 class="spip">Le réseau "EURES" </h4>
<p>Les services européens de l’emploi EURES sont un réseau de coopération qui a pour vocation de faciliter la libre circulation des travailleurs au sein de l’Espace économique européen, la Suisse y participe également. Parmi les partenaires du réseau figurent les services publics de l’emploi, les syndicats et les organisations d’employeurs.</p>
<p>La Commission européenne assure la coordination du réseau. Son site internet regroupe les offres d’emploi et permet aux candidats de déposer leur C.V. <a href="http://ec.europa.eu/eures/" class="spip_out" rel="external">en ligne</a>.</p>
<h4 class="spip">Les agences de recrutement</h4>
<p>Le recrutement par le biais des agences peut s’avérer une bonne solution pour les candidats possédant un profil ciblé.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/emploi-stage/article/recherche-d-emploi). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
