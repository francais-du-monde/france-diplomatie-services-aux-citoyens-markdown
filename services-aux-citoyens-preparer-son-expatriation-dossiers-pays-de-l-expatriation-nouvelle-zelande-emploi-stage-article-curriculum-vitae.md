# Curriculum vitae

<h4 class="spip">Rédaction</h4>
<p>Le CV est particulièrement détaillé en Nouvelle-Zélande (2 ou 3 pages). Il comporte les rubriques suivantes :</p>
<p><strong>Personal Details Objectives Education</strong><br class="manualbr"><strong>Work History</strong><br class="manualbr"><strong>Skills</strong><br class="manualbr"><strong>Interests</strong><br class="manualbr"><strong>References</strong></p>
<p>Les sites suivants vous donneront des conseils pour vous aider à rédiger votre CV :</p>
<ul class="spip">
<li><a href="http://www.workingin-newzealand.com/jobs/job-tools/cv#.UqZUzc-PdlE" class="spip_out" rel="external">Workingin-newzealand.com</a></li>
<li><a href="http://www.nzs.com/new-zealand-articles/business/cv-writing.html" class="spip_out" rel="external">Nzs.com</a></li>
<li><a href="http://www.careers.govt.nz/" class="spip_out" rel="external">Careers.govt.nz</a></li></ul>
<h4 class="spip">Diplômes</h4>
<p>Certains secteurs professionnels ne reconnaissent pas (ou très difficilement) les équivalences de diplôme (exemple : professions médicales, architectes, ingénieurs en construction navale etc.).</p>
<p>Il convient de contacter la <a href="http://www.nzqa.govt.nz/" class="spip_out" rel="external">NZQA (New Zealand Qualifications Authority)</a> pour toute information sur les équivalences de diplômes :</p>
<p>Level 13<br class="manualbr">125 The Terrace<br class="manualbr">Wellington 6011</p>
<p>Ou</p>
<p>The Millenium Centre<br class="manualbr">Building A<br class="manualbr">Level 2<br class="manualbr">600 Great South Rd<br class="manualbr">Auckland 1062<br class="manualbr">Tel : (+64) (0)4 463 3000 ; Fax : (+64) (0)4 463 3112<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/emploi-stage/article/curriculum-vitae#helpdesk#mc#nzqa.govt.nz#" title="helpdesk..åt..nzqa.govt.nz" onclick="location.href=mc_lancerlien('helpdesk','nzqa.govt.nz'); return false;" class="spip_mail">Courriel</a></p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/emploi-stage/article/curriculum-vitae). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
