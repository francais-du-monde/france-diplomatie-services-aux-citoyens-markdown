# Animaux domestiques

<p>Pour pouvoir faire entrer un animal domestique sur le sol israélien, il faut un certificat vétérinaire de bonne santé datant de moins de 15 jours et un carnet de vaccinations à jour. Le vaccin contre la rage est obligatoire et il n’y a pas de procédure de mise en quarantaine.</p>
<h4 class="spip">Pour plus d’informations</h4>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Notre article thématique sur l’exportation d’animaux domestiques</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/entree-et-sejour/article/animaux-domestiques). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
