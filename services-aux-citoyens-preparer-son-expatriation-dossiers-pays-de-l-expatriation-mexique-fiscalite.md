# Fiscalité

<h2 class="rub22689">Fiscalité du pays</h2>
<p>Depuis 2005, le Mexique a mené une révision de sa fiscalité sur les recommandations de l´OCDE. La réforme fiscale de 2008 est marquée par l´instauration de deux nouveaux impôts : l’IETU, qui remplace l´impôt sur l´actif, et l´IDE qui grève les dépôts de devises.</p>
<p><strong>1. L’impôt sur le revenu (ISR)</strong> : la loi sur l´impôt sur le revenu prévoit que toutes les personnes physiques et morales, qui ont leur résidence au Mexique, soient soumises à l´impôt sur la base de tous les revenus qu´elles reçoivent au Mexique et à l´étranger. A l´inverse, les personnes résidentes à l´étranger ne devront acquitter l´impôt sur le revenu au Mexique que dans la mesure où elles disposent d´un établissement permanent au Mexique, ou, à défaut, si les revenus perçus sont considérés comme émanant d´une source de production nationale.</p>
<p><strong>2. La résidence fiscale </strong> : les personnes physiques sont considérées comme ayant leur résidence fiscale au Mexique lorsqu´elles disposent de leur maison d´habitation dans le pays, ou lorsqu´elles y ont le centre de leurs intérêts vitaux. Quant aux personnes morales, elles sont considérées comme ayant leur résidence fiscale au Mexique lorsqu´elles sont juridiquement constituées, conformément aux lois mexicaines, lorsqu´elles gèrent depuis le Mexique la majorité de leurs affaires ou lorsque le centre du pouvoir effectif y est établi.</p>
<p><strong>3. L´établissement permanent</strong>, qui est le critère d´application de l´ISR aux personnes non résidentes au Mexique, est considéré comme existant lorsque la personne mène au Mexique une partie ou la totalité de ses affaires, lorsque la personne dispose au Mexique d´un salarié ayant le pouvoir de signer des contrats en son nom ou lorsqu’elle dispose d´un représentant légal pour ses affaires.</p>
<p><strong>4. Les personnes morales résidentes</strong> au Mexique sont imposables sur toute forme de revenu reçu au cours de l´exercice fiscal correspondant. Elles sont en outre assujetties à l´impôt sur le revenu provenant d’établissements à l’étranger. La base d’imposition est déterminée sur le revenu global obtenu lors de l´exercice fiscal, après déductions légales (Art. 29 à 36 de la loi ISR).<br class="manualbr">Si les déductions fiscales sont supérieures à l´impôt devant être acquitté, ce déficit fiscal est reportable sur l´impôt à payer sur une période de 10 ans (Art. 61 à 63 de la loi ISR). Les personnes morales non résidentes au Mexique et ayant un établissement stable au Mexique sont imposées sur le revenu généré par cet établissement.</p>
<p><strong>5. Les personnes physiques résidentes</strong> doivent payer l´impôt calculé sur les revenus mondiaux qu´elles ont obtenu au cours de l´année calendaire correspondante.</p>
<p><strong>6. Les personnes morales non résidentes</strong> et sans établissement stable au Mexique sont imposables sur tout revenu de source mexicaine, sous le régime de la Convention franco-mexicaine de non double imposition. <strong>Les personnes physiques non résidentes </strong>se verront imposer sur les revenus de source mexicaine uniquement. Le prélèvement de l´impôt se fera à la source, par l´entité mexicaine qui effectue un paiement à la personne physique.</p>
<p><strong>7. L’IETU et l’IDE</strong></p>
<ul class="spip">
<li><strong>7.1. L´impôt patronal à taux unique (l´IETU)</strong><br class="manualbr">La loi sur l´impôt patronal à taux unique (<i>Ley del Impuesto Empresarial a Tasa Unica</i>) est entrée en vigueur le 1er janvier 2008 et a abrogé la loi sur l´impôt sur l´actif.<br class="manualbr">L´IETU est un impôt complémentaire de l´ISR qui grève à un taux unique les bénéfices résultant des activités patronales. Toutes les personnes physiques et morales résidentes sur le territoire mexicain, ainsi que celles qui résident à l´étranger, mais qui disposent d´un établissement permanent dans le pays sont dans l´obligation de s´acquitter de l´IETU pour les revenus générés par la vente de biens, la prestation de services indépendants ou l´usage temporaire d´un bien.<br class="manualbr">La loi prévoit une série d´exemptions et de déductions fiscales importantes, mais distinctes de celles de l´ISR, ainsi que des mécanismes de crédits d´impôts pour le paiement de l´IETU.<br class="manualbr">Depuis 2010, les contribuables sont taxés sur leurs revenus nets imposables à hauteur de 17,5%.</li>
<li><strong>7.2 L´Impôt sur les devises (l´IDE)</strong><br class="manualbr">Le 1er janvier 2008 est entrée en vigueur la loi créant l´impôt sur les dépôts de devises (<i>Impuesto a los Depositos de Efectivo </i>(IDE)). L´objectif de cet impôt est de combattre l´économie informelle en identifiant, puis en imposant les personnes physiques et morales qui effectuent des dépôts de devises. Seuls sont soumis à imposition les dépôts de sommes en liquide auprès d´une banque. Un transfert de banque à banque ne sera pas grevé par cet impôt dont le taux d´imposition est de 2%. Ainsi, les sommes déposées auprès d´une institution financière au cours d´un mois calendaire, pour le compte d´une même personne physique ou morale, seront imposables si les sommes déposées excèdent un total de 25 000 pesos MEX. L´IDE est prélevé directement par les institutions bancaires à chaque fin de mois calendaire, et reversé au service du fisc. Il existe un mécanisme de compensation entre l´IDE et l´impôt sur le revenu.</li></ul>
<p><strong>8. La TVA (IVA)</strong> : une taxe sur la valeur ajoutée (<i>Impuesto al Valor Agregado</i>, loi du 29/12/78) s’applique au Mexique sur la vente de biens, la prestation de services indépendants, l’octroi de droits temporaires d’usage de biens et l’importation de biens et de services.</p>
<p>Le <strong>taux général</strong> de la TVA est de 16 %, à l’exception des zones frontalières où le taux est de 11 %.</p>
<p>Les exportations sont imposées à un taux de 0 %, tout comme les denrées alimentaires de base, les services agricoles rattachés à la production de ces denrées de base et les médicaments. Un grand nombre de services sont exemptés (art.15), tels que la commission des administrateurs de fonds de retraites, les services de transports terrestres de personnes à l´exception du train, certaines polices d´assurances, ou encore les prêts à titre gratuit. Le calcul de la TVA se fait <i>ad valorem</i>, pour les biens ou pour les services.</p>
<p>L’importation de biens tangibles est assujettie aux droits de douane et à la TVA, qui doivent être acquittés avant dédouanement à la direction des douanes. Cependant, les produits qui sont importés pour être transformés, puis réexportés, dans le cadre du régime d’importation IMMEX, sont exempts de TVA.</p>
<p><strong>Source </strong> : Guide des affaires Mexique 2013, Ubifrance Mexico.</p>
<p><strong>9. Autres impôts</strong> : seuls les impôts fédéraux, d’application générale sur tout le territoire mexicain, seront évoqués ici. Le régime des impôts locaux, de moindre importance, est à rechercher dans les codes ou lois fiscales des différents États.</p>
<ul class="spip">
<li><strong>9.1. Impôt spécial sur la production et les services (Impuesto Especial Sobre Producción Y Servicios - IEPS</strong>, loi du 30/12/80) est un impôt qui a pour vocation de taxer les importations du pays, tout comme la vente de certains biens spécifiques sur le territoire national. Dans cette perspective, sont également taxées les prestations de services telles que la médiation, la représentation ou la distribution, dès lors que ces services s´inscrivent dans la perspective de la vente du bien. Comme la TVA, cet impôt est indirect dans la mesure où il est payé par le consommateur final. Sont soumis à l´IEPS, la vente ou l´importation des produits tels que l´alcool (taux entre 25 et 50% selon la teneur en alcool), les cigarettes (160%) et cigares (160%) ou encore les carburants (taux variable mensuellement).</li>
<li><strong>9.2.Divers</strong> : <strong>impôt sur l’usage ou la possession de véhicules</strong> (<i>Impuesto sobre Tenencia o Uso de Vehiculos</i>, loi du 30/12/80) est un  impôt annuel à taux variable appliqué sur la valeur d’achat des véhicules à moteur ; <strong>impôt sur les automobiles neuves</strong> <i>(Impuesto Sobre Automoviles Nuevos</i>, loi du 30/12/96) à taux de 2 à 17 % appliqué sur la valeur d’achat des automobiles neuves (hors TVA) ; <strong>retenue</strong> à la source sur les marques et les royalties : 10 à 15 % (taux applicable avec la France).</li></ul>
<p><strong>10. Année fiscale</strong> : la période de l’année fiscale correspond à l’année civile. Les personnes physiques et morales doivent présenter leur déclaration dans les 3 mois qui suivent la clôture de l’année fiscale (de janvier à mars). La déclaration peut être effectuée <strong>via le programme électronique</strong> DeclaraSAT.</p>
<p><strong>11. Barème de l’impôt :</strong> l’impôt sur le revenu - le régime fiscal des personnes physiques et morales résidentes</p>
<p><strong>Personnes physiques salariées</strong><br class="manualbr">L’impôt sur les salaires est prélevé<strong> à la source </strong>chaque mois par l’employeur. Le salaire de base et tous les avantages en nature sont imposables. Certaines composantes du salaire sont exonérées, comme les prévoyances maladie et vieillesse.</p>
<table class="spip">
<thead><tr class="row_first"><th id="id5dc2_c0">Limite inférieure</th><th id="id5dc2_c1">Limite supérieure</th><th id="id5dc2_c2">Impôt fixe</th><th id="id5dc2_c3">Pourcentage sur l’excédent de la limite inférieure</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="id5dc2_c0">$ Pesos</td>
<td headers="id5dc2_c1">$ Pesos</td>
<td headers="id5dc2_c2">$ Pesos</td>
<td headers="id5dc2_c3">%</td></tr>
<tr class="row_even even">
<td headers="id5dc2_c0">0,01</td>
<td headers="id5dc2_c1">5 952,84</td>
<td headers="id5dc2_c2">0,00</td>
<td headers="id5dc2_c3">1,92</td></tr>
<tr class="row_odd odd">
<td headers="id5dc2_c0">5 952,85</td>
<td headers="id5dc2_c1">50 524,92</td>
<td headers="id5dc2_c2">114,24</td>
<td headers="id5dc2_c3">6,40</td></tr>
<tr class="row_even even">
<td headers="id5dc2_c0">50 524,93</td>
<td headers="id5dc2_c1">88 793,04</td>
<td headers="id5dc2_c2">2 966,76</td>
<td headers="id5dc2_c3">10,88</td></tr>
<tr class="row_odd odd">
<td headers="id5dc2_c0">88 793,05</td>
<td headers="id5dc2_c1">103 218,00</td>
<td headers="id5dc2_c2">7 130,88</td>
<td headers="id5dc2_c3">16,00</td></tr>
<tr class="row_even even">
<td headers="id5dc2_c0">103 218,01</td>
<td headers="id5dc2_c1">123 580,20</td>
<td headers="id5dc2_c2">9 438,60</td>
<td headers="id5dc2_c3">17,92</td></tr>
<tr class="row_odd odd">
<td headers="id5dc2_c0">123 580,21</td>
<td headers="id5dc2_c1">249,243.49</td>
<td headers="id5dc2_c2">13 087,44</td>
<td headers="id5dc2_c3">21,36</td></tr>
<tr class="row_even even">
<td headers="id5dc2_c0">249 243,49</td>
<td headers="id5dc2_c1">392 841,96</td>
<td headers="id5dc2_c2">39,929.04</td>
<td headers="id5dc2_c3">23,52</td></tr>
<tr class="row_odd odd">
<td headers="id5dc2_c0">392 841,97</td>
<td headers="id5dc2_c1">Et au-delà</td>
<td headers="id5dc2_c2">73,703.40</td>
<td headers="id5dc2_c3">30,00</td></tr>
</tbody>
</table>
<p><strong>Personnes morales</strong></p>
<p>Le taux d’imposition uniforme de l’ISR des personnes morales est de 30%.</p>
<p><strong>Taux d’imposition des plus-values de cessions immobilières</strong></p>
<p>Les taux sont établis par tranche, jusqu’à 32 %.</p>
<p><strong>Impôt sur les revenus financiers</strong></p>
<p>Le taux est de 32 % pour le salaire maximum (pour les personnes physiques et morales)</p>
<p><strong>12. Quitus fiscal</strong></p>
<p>L’impôt sur le revenu étant déduit directement des salaires, il n’est pas exigé de quitus fiscal à la sortie du pays.</p>
<p><strong>13. Solde du compte en fin de séjour</strong></p>
<p>Le site du Servicio de Administracion Tributaria (SAT), fournit de nombreuses informations pratiques concernant les déclarations de revenu, le paiement de l’impôt et les procédures fiscales au Mexique (<a href="http://www.sat.gob.mx/" class="spip_out" rel="external">www.sat.gob.mx</a>).</p>
<p>La « orientación al contribuyente » dispense également un service de consultation fiscale téléphonique au : 01 800 46 36 728.</p>
<p><strong>Pour en savoir plus</strong></p>
<ul class="spip">
<li><a href="http://export.businessfrance.fr/default.html" class="spip_out" rel="external">Mission Business France au Mexique</a> et son Guide des Affaires Mexique 2013 ;</li>
<li>Service économique français au Mexique.</li></ul>
<p><i>Mise à jour : septembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-mexique-fiscalite-article-convention-fiscale-108400.md" title="Convention fiscale">Convention fiscale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-mexique-fiscalite-article-fiscalite-du-pays.md" title="Fiscalité du pays">Fiscalité du pays</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/fiscalite/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
