# Entrée et séjour

<h2 class="rub22860">Passeport, visa, permis de travail</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/entree-et-sejour/#sommaire_1">Séjour n’excédant pas 90 jours</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/entree-et-sejour/#sommaire_2">Séjour de plus de 90 jours</a></li></ul>
<p>Quel que soit le motif de votre séjour, renseignez-vous auprès de la section consulaire de l’ambassade de Roumanie à Paris qui vous informera sur la règlementation en matière d’entrée et de séjour en Roumanie, règlementation que vous devrez impérativement respecter. N’hésitez pas à consulter le site du ministère de l’Intérieur roumain afin de vous familiariser avec les modalités d’entrée et de séjour dans le pays.</p>
<p><strong>En aucun cas, vous n’êtes autorisé à travailler en Roumanie sans permis adéquat.</strong></p>
<h3 class="spip"><a id="sommaire_1"></a>Séjour n’excédant pas 90 jours</h3>
<p>Les Français, comme tous les ressortissants des pays membres de l’Union européenne, peuvent entrer en Roumanie <strong>sans visa à condition que la durée de leur séjour n’excède pas 90 jours.</strong></p>
<p>Ils doivent cependant être en possession d’un <strong>passeport ou d’une carte nationale d’identité en cours de validité</strong>. Ce document d’identité doit être <strong>valable au moins six mois</strong> à compter de la date d’entrée sur le territoire roumain. Aucun cachet d’entrée ne sera apposé sur le document de voyage par la police des frontières.</p>
<p>Les membres de la famille d’un Français qui ne sont pas ressortissants d’un Etat membre de l’Union européenne doivent, sauf exemptions prévues par la loi, être en possession d’un passeport en cours de validité et demander un visa auprès de l’ambassade ou du consulat roumain. La liste des représentations diplomatiques et consulaires roumaines à l’étranger est disponible sur le site Internet du <a href="http://www.mae.ro/" class="spip_out" rel="external">ministère roumain des Affaires étrangères</a> / rubrique Missions diplomatiques / Missions roumaines.</p>
<p>Les membres de la famille qui ne sont pas ressortissants d’un Etat membre de l’Union européenne sont dispensés de visa s’ils sont en possession d’un document en cours de validité attestant de leur résidence sur le territoire d’un autre État membre en qualité de membre de la famille d’un citoyen de l’Union européenne.</p>
<h3 class="spip"><a id="sommaire_2"></a>Séjour de plus de 90 jours</h3>
<p><strong>Dans un délai de 15 jours au plus</strong> suivant la date d’entrée en Roumanie, vous devez, ainsi que les membres de votre famille, vous présenter auprès de l’entité territoriale la plus proche de la police des frontières roumaine ou de l’Office roumain de l’immigration afin de déclarer votre présence sur le sol roumain.</p>
<p>Si vous souhaitez rester en Roumanie plus de 90 jours, vous devez solliciter la prolongation de votre droit de séjour auprès de l’autorité compétente : l’office roumain pour l’immigration ou l’un de ses bureaux situés dans le chef-lieu de chaque département. Vous devrez justifier d’un emploi, de moyens de subsistance ou d’une inscription auprès d’un établissement d’enseignement supérieur roumain.</p>
<p>Les membres de la famille d’un Français, quelle que soit leur nationalité, accompagnant ces derniers ou les rejoignant ultérieurement, bénéficient également d’un droit de résidence en Roumanie.</p>
<p>Les documents généralement demandés sont le passeport ou la carte nationale d’identité en cours de validité et un justificatif de domicile. Les autorités roumaines vous délivreront :</p>
<ul class="spip">
<li>un certificat d’enregistrement si vous êtes ressortissant d’un Etat membre de l’Union européenne valable pour une période entre un an et cinq ans ;</li>
<li>une carte de résidence pour les membres de la famille qui ne sont pas ressortissants d’un pays de l’Union européenne.</li></ul>
<h4 class="spip">Résidence permanente</h4>
<p>Les citoyens de l’Union européenne résidant de façon légale et continue sur le territoire roumain sur <strong>une période d’au moins cinq ans</strong> bénéficient du droit de résidence permanente et peuvent demander une carte de résidence permanente, valable 10 ans, sauf pour les mineurs de moins de 14 ans dont la viabilité de la carte de résidence permanente est de cinq ans. Cette disposition s’applique également aux membres de la famille qui ne sont pas ressortissants de l’Union européenne à condition qu’ils résident sur le territoire roumain pendant au moins cinq ans en qualité de membres de la famille d’un citoyen de l’Union européenne.</p>
<p>Les citoyens de l’Union européenne doivent prouver qu’ils ont le statut de travailleur salarié ou indépendant ou qu’ils disposent d’une assurance médicale et des moyens nécessaires à leur subsistance ou qu’ils sont inscrits auprès d’un établissement d’enseignement.</p>
<p>Les documents requis pour la délivrance de ces documents varient en fonction de la situation de chacun.</p>
<p>Pour Bucarest et sa région, la délivrance des documents de résidence se fera à l’adresse suivante :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>ministère de l’Intérieur et de la Réforme administrative</strong><br class="manualbr">autorité nationale pour les étrangers<br class="manualbr">27 rue Nicolae Iorga - Bucarest secteur 1. </p>
<p>Les autorités roumaines vous délivreront :</p>
<ul class="spip">
<li>une carte de résidence permanente si vous êtes ressortissant d’un Etat membre de l’Union européenne ;</li>
<li>une carte de résidence permanente pour les membres de la famille qui ne sont pas ressortissants de l’Union européenne.</li></ul>
<h4 class="spip">Pour en savoir plus</h4>
<ul class="spip">
<li><a href="http://paris.mae.ro/" class="spip_out" rel="external">Site Internet de l’ambassade de Roumanie en France</a> / rubrique Visas ;</li>
<li><a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/roumanie/" class="spip_in">Conseils aux voyageurs</a> du ministère des Affaires étrangères</li>
<li><a href="http://www.mae.ro/" class="spip_out" rel="external">Ministère roumain des Affaires étrangères</a> / rubrique Visas et services consulaires ;</li>
<li><a href="http://www.oim.ro/index.php/ro/" class="spip_out" rel="external">L’Office roumain pour l’immigration</a>.</li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-roumanie-entree-et-sejour-article-animaux-domestiques-110336.md" title="Animaux domestiques">Animaux domestiques</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-roumanie-entree-et-sejour-article-vaccination-110335.md" title="Vaccination">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-roumanie-entree-et-sejour-article-demenagement-110334.md" title="Déménagement">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-roumanie-entree-et-sejour-article-passeport-visa-permis-de-travail.md" title="Passeport, visa, permis de travail">Passeport, visa, permis de travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/entree-et-sejour/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
