# Santé

<p>Pour une présentation des risques épidémiologiques ainsi que des conseils sur la médecine au quotidien, consultez les pages suivantes :</p>
<ul class="spip">
<li><a href="http://www.ambafrance-sn.org/Liste-des-medecins-francophones" class="spip_out" rel="external">Liste des médecins francophones sur le site de l’Ambassade de France au Sénégal</a> ;</li>
<li><a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/senegal/" class="spip_in">Dossier Sénégal sur le site Conseils aux voyageurs</a></li>
<li>Fiche Dakar sur le site du <a href="http://www.cimed.org/" class="spip_out" rel="external">CIMED</a></li>
<li>Fiche Saint-Louis sur le site du <a href="http://www.cimed.org/" class="spip_out" rel="external">CIMED</a>.</li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/vie-pratique/article/sante). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
