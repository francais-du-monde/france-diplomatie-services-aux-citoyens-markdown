# Logement

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/vie-pratique/article/logement#sommaire_1">Où se loger ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/vie-pratique/article/logement#sommaire_2">Conditions de location</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/vie-pratique/article/logement#sommaire_3">Electricité</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/vie-pratique/article/logement#sommaire_4">Electroménager</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Où se loger ?</h3>
<p>Il est possible de trouver à Dakar des logements de toute catégorie et à tous les prix, meublés ou non.</p>
<p>Les loyers, dans les quartiers proches du Plateau, seront les plus élevés. Les quartiers de Fann, Ouakam, le Point E, Mermoz, les Almadies présentent des prix relativement élevés également. Les quartiers Hann Maristes, Liberté, Comico, N’gor et Yoff proposent des tarifs plus abordables.</p>
<p>Un relevé de prix effectué fin septembre 2013 fait ressortir les loyers mensuels suivants (appartement T1 non meublé, 55-75 m2) :</p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Quartiers Sacré Coeur / Maristes</td>
<td>180 000 à 250 000 XOF</td></tr>
<tr class="row_even even">
<td>Quartiers Yoff Virage / Sotrac Mermoz / Mamelles</td>
<td>250 000 à 450 000 XOF</td></tr>
<tr class="row_odd odd">
<td>Quartiers Plateau / Fann Résidence / Almadies</td>
<td>350 000 à 650 000 XOF</td></tr>
</tbody>
</table>
<p>Il convient de trouver le meilleur compromis entre coût du loyer et temps de trajet car Dakar connaît d’importants problèmes d’embouteillages.</p>
<p>Il existe de nombreuses <strong>agences immobilières</strong> dans la capitale (Avenue Pompidou, place de l’indépendance, etc), toutefois les tarifs pratiqués sont élevés. Vous pouvez également consulter les <strong>journaux </strong>gratuits comme le Tam-Tam (bimensuel). Enfin le bouche à oreille peut être un bon moyen de trouver un logement, en vous adressant notamment aux personnes âgées connaissant bien le quartier et les logements disponibles, en échange d’une faible commission.</p>
<p>Des formules de <strong>logement chez l’habitant</strong> tendent à se développer dans les principales villes du Sénégal. Elles peuvent présenter une solution d’appoint plus économique pour l’hébergement.</p>
<p><strong>Pour en savoir plus</strong> :</p>
<ul class="spip">
<li><a href="http://www.au-senegal.com/-Hebergements-.html" class="spip_out" rel="external">Hébergement au Sénégal</a> ;</li>
<li><a href="http://www.au-senegal.com/-Immobilier-agences-immobilieres-.html" class="spip_out" rel="external">Agences immobilières au Sénégal</a></li>
<li><a href="http://www.expatdakar.org/guide/index.htm" class="spip_out" rel="external">Site pour les expatriés à Dakar</a></li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Conditions de location</h3>
<h4 class="spip">Dakar</h4>
<p>Le marché locatif est florissant et les constructions neuves importantes.</p>
<p>Cependant, le choix d’un logement demande de la vigilance : de nombreux bâtiments présentent de mauvaises finitions.</p>
<p>Les quartiers résidentiels, en ordre croissant d’éloignement par rapport au centre-ville, sont les suivants : Dakar Plateau, Fann Résidence, Point E, Mermoz, Hann Mariste, Mamelles, Almadies, Ngor et Yoff.</p>
<p>L’excédent de demande sur l’offre conduit à des loyers et des prix de vente élevés au regard des prestations offertes, souvent inférieures aux exigences des locataires ou acheteurs potentiels.</p>
<p>Les parties fixent librement la durée des baux. Le bail peut être conclu pour une durée déterminée ou indéterminée.</p>
<p>La pratique des avances a tendance à se développer, certains propriétaires exigent une avance de trois mois de loyer. Une caution d’un mois, voire de deux, est demandée lors de toute location. Il peut venir s’ajouter des frais de courtage correspondant à un mois de loyer.</p>
<p>Une fois l’état des lieux effectué, le contrat signé et enregistré auprès de la direction générale des Impôts et Domaines (bureau de l’enregistrement des baux), la disponibilité de l’appartement ou de la villa est souvent immédiate.</p>
<p>Pour des raisons de sécurité, les fenêtres des maisons sont généralement munies de grilles. Un gardiennage est recommandé (environ 300 à 400 € par mois pour une surveillance 24h/24).</p>
<p>La plupart des appartements sont loués sans climatisation, cette dernière étant indispensable, en particulier de mai à mi-novembre. A charge pour les locataires de la faire installer.</p>
<p>L’eau et l’électricité sont à la charge du locataire. A noter que les coupures d’eau et d’électricité sont fréquentes.</p>
<p>Les tarifs de l’électricité sont trois fois supérieurs aux tarifs pratiqués en France.</p>
<p>Il est recommandé de relier les appareils électriques à des régulateurs de tension, voire à des onduleurs, afin de les protéger des effets dévastateurs des sauts de tension.</p>
<p>Pour le coût de l’eau, lorsqu’il existe un seul compteur pour tout un immeuble, un taux forfaitaire est appliqué à chaque locataire. Dans une villa, le coût est entièrement à la charge du locataire. Selon la tranche de consommation, le prix (TTC) du mètre cube peut varier de 250 à 800 FCFA.</p>
<h4 class="spip">Saint-Louis</h4>
<p>Le marché locatif est très restreint. Il n’existe pas de quartiers résidentiels. On peut se loger soit à proximité du centre, soit dans les quartiers de Sor ou de Vauvert (maisons avec jardins).</p>
<p>Les logements, souvent vétustes, nécessitent le plus souvent des aménagements liés au confort et à l’habitabilité ; ils sont très rarement meublés.</p>
<p>Il faut compter environ un mois de délai de recherche. La durée des baux est d’un an. Le loyer est payable d’avance (trois mois). Il existe des agents immobiliers indépendants. En cas de recours à un intermédiaire "informel", la commission d’usage est de 10% du loyer annuel. Les charges mensuelles représentent approximativement 40% du loyer. La climatisation s’avère nécessaire.</p>
<p>Les immeubles sont gardés la nuit par des gardiens privés. Les villas sont encloses et équipées de barreaux mais il est indispensable d’embaucher un gardien de nuit (50.000 à 80.000 FCFA/mois).</p>
<p><strong>Pour en savoir plus</strong> :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.demarches.gouv.sn/" class="spip_out" rel="external">Demarches.gouv.sn</a>.</p>
<h3 class="spip"><a id="sommaire_3"></a>Electricité</h3>
<p>Le courant électrique est de 220 volts et 50 Hz.</p>
<p>Compte tenu d’importantes variations de tension, l’utilisation de régulateurs et de prises anti-foudre est vivement recommandée.</p>
<h3 class="spip"><a id="sommaire_4"></a>Electroménager</h3>
<p>Les cuisines ne sont en général pas équipées. Les appareils électroménagers sont disponibles sur place, mais ils sont proposés à un prix élevé avec une qualité de service après-vente assez variable.</p>
<p>La climatisation est indispensable pendant la saison des pluies (de mai à mi-novembre).</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/vie-pratique/article/logement). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
