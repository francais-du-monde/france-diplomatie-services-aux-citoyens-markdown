# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/vie-pratique/article/transports-111157#sommaire_1">Importation de véhicule</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/vie-pratique/article/transports-111157#sommaire_2">Permis de conduire</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/vie-pratique/article/transports-111157#sommaire_3">Code de la route</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/vie-pratique/article/transports-111157#sommaire_4">Assurances et taxes</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/vie-pratique/article/transports-111157#sommaire_5">Achat et location</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/vie-pratique/article/transports-111157#sommaire_6">Immatriculation</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/vie-pratique/article/transports-111157#sommaire_7">Entretien</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/vie-pratique/article/transports-111157#sommaire_8">Réseau routier</a></li>
<li><a id="so_9" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/vie-pratique/article/transports-111157#sommaire_9">Transports en commun</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Importation de véhicule</h3>
<p>Trois possibilités s’offrent à un Français qui désire se rendre en Lituanie en voiture :</p>
<p><strong>Pour un séjour de moins de trois mois :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Un Français peut résider moins de trois mois et utiliser sa propre voiture sans avoir besoin d’effectuer de démarches sur place. Seul, un certificat international pour automobile (équivalent de la carte grise), délivré par la préfecture de son lieu de résidence en France, peut lui être demandé.</p>
<p><strong>Pour un séjour de trois mois à trois ans :</strong></p>
<p>Un Français qui désire résider plus de trois mois en Lituanie doit s’adresser à la société d’enregistrement REGITRA. Il devra présenter :</p>
<ul class="spip">
<li>une attestation de résidence (délivrée par son employeur, la municipalité) ou son contrat d’achat ou de location d’appartement ;</li>
<li>le certificat délivré par le service d’immigration du ministère de l’intérieur lors de la déclaration de résidence ;</li>
<li>REGITRA lui délivrera ainsi une attestation temporaire d’enregistrement sans même à avoir à changer les plaques et la carte grise. Cette attestation est valable durant toute la période du visa ou du permis de résidence.</li></ul>
<p><strong>Pour un séjour supérieur à trois ans :</strong></p>
<p>Un Français désirant résider plus de trois ans en Lituanie doit d’abord retirer de l’enregistrement son véhicule en France, à la préfecture de son lieu de résidence. Il en obtient des plaques d’immatriculation provisoires lui permettant de se rendre en Lituanie. Sur place, il doit s’adresser à la société REGITRA et présenter :</p>
<ul class="spip">
<li>une attestation de résidence ;</li>
<li>le certificat délivré par le service d’immigration du ministère de l’intérieur lors de la déclaration de résidence ;</li>
<li>REGITRA lui fournira de nouvelles plaques d’immatriculation et une carte grise lituaniennes, valable pour toute la période de la validité de son visa. Les frais de dossier s’élèvent à environ 80 litas.</li></ul>
<p>REGITRA <br class="manualbr">Leipkalnio 97, Vilnius <br class="manualbr">Tel : 266 04 21 / 264 04 72 <br class="manualbr">Fax : 266 04 23</p>
<h3 class="spip"><a id="sommaire_2"></a>Permis de conduire</h3>
<p>La France et la Lituanie ont mis en place une procédure de reconnaissance et d’échange de permis de conduire sans avoir à repasser le permis. Cet accord est devenu effectif depuis le 1er octobre 2001.</p>
<p>En conséquence, tout ressortissant français s’établissant pour plus de 6 mois sur le territoire de la république de Lituanie est tenu de demander l’échange de son permis national français contre un permis lituanien. Au-delà de 6 mois de résidence, le permis de conduire national ou international est considéré comme invalide (décret n°35 du 24/01/2001 du Commissaire Général de Police, articles 44 et 45). Le permis de conduire français est cependant rendu à son détenteur après vérification. <strong>Seul l’échange du permis national français est possible ; il n’y a pas d’échange pour le permis international.</strong></p>
<p>A cette fin, le titulaire du permis doit se rendre au service de la police routière dont dépend son domicile et présenter les documents suivants :</p>
<ul class="spip">
<li>Un formulaire de demande à remplir (sur place) ;</li>
<li>Une carte à remplir (sur place) ;</li>
<li>Le passeport ou autre document confirmant l’identité de la personne ;</li>
<li>Un justificatif du lieu de résidence, de travail ou d’études ;</li>
<li>Un certificat médical autorisant la conduite du véhicule correspondant à la catégorie du permis de conduire (établi par le ministère de la Santé) ;</li>
<li>L’original du permis de conduire accompagné d’une copie certifiée conforme par le notaire et traduite en lituanien (par précaution conserver une copie) ;</li>
<li>2 photos (format spécial permis de conduire avec coin blanc en bas à droite) ;</li>
<li>Le reçu prouvant le paiement des tarifs établis.</li></ul>
<p>Toute personne qui n’aurait pas régularisé sa situation dans les délais impartis devra obtenir le permis lituanien selon les règles applicables aux ressortissants lituaniens.</p>
<h3 class="spip"><a id="sommaire_3"></a>Code de la route</h3>
<p>La circulation est libre sur la totalité du territoire. La conduite s’effectue à droite, la priorité également.</p>
<p><strong>Règles particulières</strong> : les voyageurs se déplaçant en véhicule de location doivent impérativement être en mesure de présenter les documents suivants sous peine de voir immobiliser leur véhicule en cas de contrôle :</p>
<ul class="spip">
<li>photocopie de la carte grise, certifiée conforme par l’agence de location,</li>
<li>attestation de l’agence de location précisant que l’original de la carte grise est conservé à l’agence, que le locataire est autorisé à voyager en Lituanie et comportant les références du contrat de location ainsi que le numéro du titre de voyage ou d’identité du conducteur.</li></ul>
<p><strong>Le taux d’alcoolémie</strong> toléré s’élève à 0,4 g/l et à 0,2 g /l pour les jeunes conducteurs comptant moins de deux ans de permis. Le permis de conduire est retiré sur le champ en cas d’infraction à cette disposition. Des peines d’amende et de prison particulièrement lourdes peuvent être prononcées.</p>
<p><strong>Vitesses autorisées</strong> :</p>
<p>Elles varient suivant la période de l’année et les axes routiers :</p>
<ul class="spip">
<li>autoroutes : 130 km/h du 1er avril au 31 octobre, 110 km/h du 1er novembre au 31 mars,</li>
<li>autoroute Vilnius-Kaunas : 100 km/h toute l’année,</li>
<li>routes nationales : 90 km/h</li>
<li>agglomérations : 50 km/h</li></ul>
<p><strong>Les pneus d’hiver</strong> sont obligatoires du 1er novembre au 10 avril. Les pneus cloutés, autorisés durant cette période sont interdits le reste de l’année.</p>
<p>Les véhicules doivent circuler toute l’année avec <strong>les feux de croisement</strong> allumés.</p>
<p>Il est rappelé que tout véhicule circulant en Lituanie doit obligatoirement comporter les éléments de sécurité suivants : triangle, gilet réfléchissant, extincteur, trousse de premiers secours.</p>
<p>A la suite d’une modification intervenue le 1er mai 2008 dans la réglementation lituanienne en matière de code de la route, la procédure de constat à l’amiable est désormais autorisée. Elle reste cependant peu connue des conducteurs. Lors d’accident matériel de la circulation, et en cas de mauvaise volonté ou de mauvaise foi manifeste du protagoniste, il peut néanmoins être nécessaire de prévenir immédiatement la police afin d’établir un constat et demander un interprète. Dans cette éventualité, les véhicules ne doivent en aucun cas être déplacés.</p>
<h3 class="spip"><a id="sommaire_4"></a>Assurances et taxes</h3>
<p>Les assurances sont obligatoires. La carte verte délivrée par les compagnies d’assurances françaises est reconnue par la Lituanie en sa qualité de membre de l’Union européenne. Il est cependant judicieux de vérifier ce point auprès de son assureur avant tout déplacement en Lituanie. Bien que les contrôles aux frontières internes des pays Schengen aient cessé depuis l’entrée de la Lituanie dans ce groupe le 21 décembre 2007, il reste nécessaire de produire les documents originaux en cas de contrôle routier.</p>
<h3 class="spip"><a id="sommaire_5"></a>Achat et location</h3>
<p>Les marques françaises et la plupart des marques étrangères sont représentées sur place.</p>
<h3 class="spip"><a id="sommaire_6"></a>Immatriculation</h3>
<p>Pour immatriculer un véhicule en Lituanie en provenance d’un autre Etat membre de l’UE, il convient de présenter à REGITRA les documents suivants :</p>
<ul class="spip">
<li>formulaire de demande d’immatriculation ;</li>
<li>pièces d’identité ;</li>
<li>document d’origine du véhicule (document attestant de l’ancienne immatriculation) ;</li>
<li>un certificat de conformité ;</li>
<li>document certifiant l’acquisition du véhicule (facture…) ;</li>
<li>certificat d’assurance (responsabilité civile).</li></ul>
<h3 class="spip"><a id="sommaire_7"></a>Entretien</h3>
<p>Il est possible de faire effectuer des réparations sur place, à un coût sensiblement égal à celui de la France.</p>
<h3 class="spip"><a id="sommaire_8"></a>Réseau routier</h3>
<p>L’état des grands axes routiers est satisfaisant, celui des routes secondaires est dans un état plus inégal.</p>
<p>Certains tronçons non asphaltés ne nécessitent pas pour autant l’usage d’un véhicule tout terrain.</p>
<h3 class="spip"><a id="sommaire_9"></a>Transports en commun</h3>
<p>L’aéroport de Vilnius est situé à une vingtaine de minutes du centre ville.</p>
<p>Le réseau de chemin de fer est fiable.</p>
<p>Un réseau de bus, tramways et trolleybus permet de circuler en ville. A Vilnius, le coût des transports en commun est peu élevé. Le prix du km en taxi coûte 1,50 Lt (0,45 €) avec une prise en charge de 1,70 Lt (0,50 €). Le prix d’une course vers l’aéroport est d’environ 20 Lt (6 €).</p>
<p>Pour en savoir plus :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.infotourlituanie.fr/index.php?id=588" class="spip_out" rel="external">Infotourlituanie.fr</a> </p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/vie-pratique/article/transports-111157). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
