# Entretien d’embauche

<h4 class="spip">Apparence et attitude</h4>
<p>Les questions lors des entretiens d’embauche portent d’avantage sur vos projets d’avenir et sur ce que vous pourriez apporter à l’entreprise que sur vos diplômes. Sur ce plan, les entretiens sont donc généralement plus ouverts (souvent moins structurés) qu’en France.</p>
<p>Montrez que vous avez fait des recherches sur l’entreprise et insistez sur votre capacité d’adaptation et sur vos atouts (sans en faire trop !). Soyez sûr de vous, sans être arrogant.</p>
<p>N’hésitez pas à poser des questions pertinentes sur le poste pour lequel vous postulez et sur l’organisation et, ainsi, à montrer votre intérêt pour le poste.</p>
<h4 class="spip">Questions préférées des recruteurs</h4>
<ul class="spip">
<li>What aspect of this job do you consider most crucial ?</li>
<li>What do you like best about your job/studies … what do you like least ?</li>
<li>What are your long term career objectives and how do you plan to achieve them ?</li>
<li>How do you work under pressure ?</li>
<li>Describe your ideal job …</li>
<li>How do you think a friend would describe you ?</li>
<li>What are your strengths ? Your weaknesses ?</li>
<li>Why did you choose this particular field of work ?</li>
<li>What have you done that shows initiatives ?</li>
<li>What makes you a good supervisor ?</li>
<li>What are your salary requirements / expectations ?</li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/emploi-stage/article/entretien-d-embauche). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
