# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/vie-pratique/article/transports#sommaire_1">Importation de véhicule</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/vie-pratique/article/transports#sommaire_2">Permis de conduire</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/vie-pratique/article/transports#sommaire_3">Code de la route</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/vie-pratique/article/transports#sommaire_4">Assurances et taxes</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/vie-pratique/article/transports#sommaire_5">Achat et location</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/vie-pratique/article/transports#sommaire_6">Immatriculation</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/vie-pratique/article/transports#sommaire_7">Carburant</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/vie-pratique/article/transports#sommaire_8">Réseau routier</a></li>
<li><a id="so_9" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/vie-pratique/article/transports#sommaire_9">Transports en commun</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Importation de véhicule</h3>
<p>Malgré l’avantage financier que peut présenter l’importation d’un véhicule sur le prix d’achat, il convient de considérer cette option sous tous ses angles. En effet, la conduite à gauche suppose, pour un meilleur confort, que le volant soit situé à droite. Dans le cas contraire, il peut s’avérer plus difficile de trouver une compagnie d’assurance qui accepte de garantir les risques du véhicule en Irlande, à moins de supporter une majoration non négligeable du montant de la prime.</p>
<p>En tout état de cause, si vous importez un véhicule en Irlande (à l’exception des véhicules introduits par des visiteurs pour un séjour temporaire), vous devez tout d’abord être en mesure de présenter un document attestant que vous en êtes le propriétaire (carte grise, attestation d’assurance…). Vous devez également être en possession d’un certificat d’exportation ou de la carte grise du véhicule.</p>
<p>Vous devez, sous un délai de sept jours à compter de la date d’arrivée de votre véhicule en Irlande, prendre rendez-vous avec le service national de contrôle technique pour que votre véhicule soit inspecté. Les démarches pour l’enregistrement du véhicule auprès des services de l’administration fiscale et des douanes (<i>Revenue Commissioners</i>), ainsi que le paiement de la taxe d’immatriculation des véhicules (VRT - <i>Vehicle Registration Tax</i>) doivent être effectués dans les 30 jours suivant l’arrivée du véhicule en Irlande, par l’intermédiaire du service d’immatriculation des véhicules (VRO - <i>Revenue Vehicle Registration Office</i>). Après inspection du VRO, vous procèderez au paiement de la VRT.</p>
<p>A l’issue de ces procédures de régularisation du véhicule sur le territoire irlandais, vous recevrez un reçu attestant du paiement de la VRT et indiquant le numéro d’immatriculation attribué au véhicule, ainsi qu’un formulaire RF 100 à utiliser pour la demande de taxe sur les véhicules motorisés. Depuis le 1er juillet 2008, cette taxe est proportionnelle à la puissance du moteur du véhicule et par conséquent il est possible d’en être exonéré. De plus, le prix à payer correspond au niveau des émissions de CO2 du véhicule.</p>
<p>Vous disposez d’un délai de trois jours pour apposer le nouveau numéro d’immatriculation sur le véhicule. Tout manquement à cette règle est passible d’une amende infligée par la police irlandaise (<i>Garda Siochana</i>).</p>
<p>Le certificat d’immatriculation du véhicule est délivré par le ministère des Transports. Il vous sera envoyé par courrier dès que la demande de taxe sur les véhicules motorisés aura été déposée auprès du bureau local chargé du recouvrement de cette taxe.</p>
<p>En ce qui concerne l’assurance automobile, la loi irlandaise exige que tout conducteur d’un véhicule motorisé souhaitant conduire son véhicule sur la voie publique souscrive à un contrat d’assurance automobile.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li>Article <a href="http://www.citizensinformation.ie/en/moving_country/moving_to_ireland/coming_to_live_in_ireland/importing_car_into_ireland.html" class="spip_out" rel="external">Importing a vehicle into Ireland</a> sur le portail officiel du service public irlandais ;</li>
<li><a href="http://www.revenue.ie/" class="spip_out" rel="external">L’administration fiscale et des douanes en Irlande</a></li>
<li>Le service d’immatriculation des véhicules : <br class="manualbr"><i>Vehicle Registration Office</i> - St John’s House<br class="manualbr">High Street Tallaght<br class="manualbr">Dublin 24<br class="manualbr">Tél : (LoCall) 1 890 777 121.</li>
<li><a href="http://www.transport.ie/" class="spip_out" rel="external">Ministère des Transports irlandais (<i>The Department of Transport</i>)</a></li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Permis de conduire</h3>
<p>En application des directives européennes, les ressortissants français venant s’établir en Irlande ne sont plus tenus d’échanger leur permis de conduire national contre un permis irlandais dès leur entrée sur le territoire.</p>
<p>Selon la directive 91/439/CEE entrée en vigueur le 1er juillet 1996 et remplacée par la directive 2000/275/CE, un principe de reconnaissance réciproque des permis de conduire a été établi et le permis national reste valable. Cependant, l’autorité irlandaise compétente, en l’occurrence le Département de la sécurité routière (RSA - <i>The Road Safety Authority)</i>, pourra y inscrire des mentions indispensables à sa gestion, ainsi que des dispositions nationales concernant plusieurs domaines (durée de validité, mesures fiscales…).</p>
<p><a href="http://www.rsa.ie/" class="spip_out" rel="external">The Road Safety Authority</a> <br class="manualbr">Téléphone : 1890 506080<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/vie-pratique/article/transports#info#mc#rsa.ie#" title="info..åt..rsa.ie" onclick="location.href=mc_lancerlien('info','rsa.ie'); return false;" class="spip_mail">Courriel</a></p>
<p>Si vous désirez faire échanger votre permis de conduire français, la demande se fait localement en retirant un formulaire auprès du <i>Motor Taxation Office</i> ou dans les bureaux de poste et de police.</p>
<p><strong>Motor Taxation Office </strong><br class="manualbr">Dublin City Council<br class="manualbr">Block B - Blackhall Walk - Queen Street - Dublin 7<br class="manualbr">Téléphone : (01) 222 22 22</p>
<p>La validité du permis de conduire irlandais est limitée dans le temps et les frais d’échange dépendent de la durée souhaitée :</p>
<ul class="spip">
<li>5 euros pour un an</li>
<li>15 euros pour 3 ans</li>
<li>25 euros pour 10 ans</li></ul>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.rsa.ie/en/RSA/Licensed-Drivers/Driving-licence/Holders-of-foreign-licenses/" class="spip_out" rel="external">Article spécifique sur le site du Département de la sécurité routière en Irlande</a> (antenne du ministère des Transports).</p>
<h3 class="spip"><a id="sommaire_3"></a>Code de la route</h3>
<p>En avril 2004, la Commission européenne a lancé une charte européenne pour la sécurité routière. Depuis le 20 janvier 2005, en République d’Irlande, les limitations de vitesse, ainsi que les distances sont indiquées en km/h et non plus en miles.</p>
<p>Les vitesses maximales autorisées sont :</p>
<ul class="spip">
<li>50 km/h en ville ;</li>
<li>80 km/h sur les routes secondaires (panneaux blancs) ;</li>
<li>100 km/h sur les toutes nationales (panneaux verts) ;</li>
<li>120 km/h sur les autoroutes.</li></ul>
<p>La conduite s’effectue à gauche.</p>
<p>Le taux d’alcoolémie à ne pas dépasser est de 0,5 gramme d’alcool par litre de sang et 0,2 gramme par litre de sang pour les apprentis et jeunes conducteurs (<i>« learner »)</i>.</p>
<p>L’âge minimal de conduite est de 17 ans.</p>
<p>Le système du permis à point existe en Irlande.</p>
<p>Le port de la ceinture est obligatoire tant à l’avant qu’à l’arrière du véhicule. Le port du casque est obligatoire tant pour le conducteur de la moto que pour le passager. Les enfants de moins de 12 ans ne peuvent s’assoir sur le siège passager avant.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.rsa.ie/" class="spip_out" rel="external">Département de la sécurité routière en Irlande (<i>Road Safety Authority</i>)</a></p>
<h3 class="spip"><a id="sommaire_4"></a>Assurances et taxes</h3>
<p>En vertu du principe de la liberté contractuelle, il n’existe pas de droit absolu à la conclusion d’un contrat avec une entreprise d’assurances donnée.</p>
<p>La réglementation irlandaise fixe les conditions à respecter par les entreprises qui proposent des assurances pour les véhicules immatriculés en Irlande. En droit irlandais, un assureur automobile ne peut offrir de police approuvée (c’est-à-dire une assurance responsabilité obligatoire fournissant la couverture requise par la loi) pour une voiture immatriculée en Irlande que s’il est membre du Bureau des assureurs d’automobiles d’Irlande (MIBI - <i>Motor Insurers Bureau of Ireland</i>). Le MIBI administre les fonds de garantie auxquels doivent cotiser toutes les entreprises.</p>
<p>Les assurances sont obligatoires pour le risque au " tiers ". Elles doivent être contractées auprès d’une compagnie d’assurances irlandaise ou d’une compagnie étrangère représentée localement. Il n’existe pas en Irlande de règle particulière accordant un délai de réflexion ou le droit de résilier un contrat d’assurances. Les droits des assurés en la matière dépendent des clauses du contrat. Le montant des primes est généralement très élevé et représente souvent le double du tarif pratiqué en France.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.mibi.ie/" class="spip_out" rel="external">Bureau des assureurs automobile d’Irlande (MIBI - <i>Motor Insurers Bureau of Ireland</i>)</a></li>
<li><a href="http://www.rsa.ie/" class="spip_out" rel="external">Département de la sécurité routière en Irlande (<i>The Road Safety Authority</i>)</a></li></ul>
<h3 class="spip"><a id="sommaire_5"></a>Achat et location</h3>
<p>Toutes les marques étrangères et notamment françaises sont représentées en Irlande à des prix généralement supérieurs à ceux pratiqués en France.</p>
<p>Les concessionnaires assurent le service après-vente, mais le coût de l’entretien et des réparations est élevé (20 à 40% plus cher qu’en France). Un contrôle technique est exigé tous les ans pour les véhicules de plus de quatre ans.</p>
<p>Il existe de nombreuses entreprises de location et il convient de se renseigner sur les conditions du marché. La plupart des compagnies proposent des devis en ligne sur Internet.</p>
<p>Pour en savoir plus :</p>
<ul class="spip">
<li><a href="http://www.europcar.ie/" class="spip_out" rel="external">Europcar en Irlande</a></li>
<li><a href="http://www.hertz.ie/" class="spip_out" rel="external">Hertz</a></li>
<li><a href="http://www.avis.ie/" class="spip_out" rel="external">Avis</a></li></ul>
<h3 class="spip"><a id="sommaire_6"></a>Immatriculation</h3>
<p>Les procédures d’immatriculation ne font pas l’objet d’une harmonisation communautaire. Par conséquent, chaque Etat membre détermine les documents à fournir pour effectuer les démarches.</p>
<p>En Irlande, avant d’immatriculer un véhicule, l’administration vérifiera sa conformité technique (procédure dite de " réception "). Si le véhicule n’est pas neuf, elle pourra aussi s’assurer de son bon fonctionnement en faisant un contrôle technique. Pour les véhicules de plus de quatre ans, un contrôle technique est obligatoire chaque année.</p>
<p>Le gouvernement irlandais impose à tout propriétaire d’un véhicule motorisé le paiement d’une taxe annuelle sur les véhicules motorisés, la <i>Motor Tax</i>, également appelée <i>Duty on Motor Vehicles</i>. Elle est recouvrée par le <i>Motor Taxation Office</i>. Le produit de cette taxe est destiné à l’entretien et à la rénovation du réseau routier.</p>
<p>Le montant de la taxe est fonction de la cylindrée du véhicule. En 2011, la valeur de la taxe pour les véhicules légers est comprise dans une fourchette de 185 euros (jusqu’à 1000 cc) à 1683 euros (au-dessus de 3000 cc). Ces tarifs sont ceux applicables en cas de paiement comptant de la taxe annuelle. En cas d’échelonnement de la taxe, les tarifs subissent une majoration de 1% pour un paiement semestriel, 13% pour un paiement trimestriel et 20% pour un paiement mensuel.</p>
<p>Le processus d’immatriculation doit être achevé dans les 30 jours qui suivent l’arrivée de votre véhicule en Irlande.</p>
<h3 class="spip"><a id="sommaire_7"></a>Carburant</h3>
<p>Pour connaître les prix du carburant en Irlande, consultez le site de l’Union Internationale des Transports Routiers (International Road Transport Union).</p>
<h3 class="spip"><a id="sommaire_8"></a>Réseau routier</h3>
<p>Face aux conséquences qu’entraîne l’augmentation de la population, l’Irlande n’a pas le système de transport qu’elle mérite. <br class="autobr">La saturation des infrastructures ainsi que les problèmes de congestion dans les principales villes font que le pays a dû se doter d’un plan d’investissements massif. Le plan " Transport 21 " couvre la période 2006-2015 et destine une grande partie de ses fonds au programme des routes nationales.</p>
<p>Les autoroutes sont en bon état, de même que les principaux axes routiers.</p>
<h3 class="spip"><a id="sommaire_9"></a>Transports en commun</h3>
<p>En Irlande, l’emprise de l’Etat sur le secteur des transports publics est quasi totale. La politique générale des transports émane du ministère des Transports, mais celui-ci n’a qu’un rôle stratégique et de surveillance. L’aspect opérationnel est confié aux sociétés d’Etat et agences gouvernementales dont l’Etat est l’unique propriétaire.</p>
<p>A l’intérieur du pays, les transports peuvent être effectués en voiture, en autocar, en train ou par voie aérienne. CIE (<i>Coras Iompair Eireann</i>) est le principal fournisseur de transports dans tout le pays et coordonne les actions opérationnelles des trois filiales : Irish Rail, Dublin Bus et Bus Eireann.</p>
<p>En ville, le mode de transport le plus utilisé est le "DART" <i>(Dublin Area Rapid Transit)</i>, train rapide proche du RER parisien. Il longe la côte du nord au sud de Dublin. Il est également possible d’utiliser le bus, dont le réseau est dense. Selon le parcours, le prix du ticket est compris entre 1,20 euros et 10 euros. Le taxi offre une autre solution pour un prix raisonnable.</p>
<p>Par ailleurs, Dublin a deux lignes de tramway le « LUAS » : la Red Line qui relie Tallaght à Connolly et la Green Line qui relie Sandyford à St-Stephen’s Green.</p>
<p>Il existe également la « leap card », une carte sur laquelle il est possible de créditer de l’argent ce qui évite de payer en monnaie dans le bus, le DART ou le LUAS.</p>
<p><a href="http://www.transport.ie/" class="spip_out" rel="external">The Department of Transport</a> <br class="manualbr">Transport House - 44 Kildare Street - Dublin 2<br class="manualbr">Tél. : [353] (0) 1 670 7444 ou Locall 1890 443 311 <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/vie-pratique/article/transports#info#mc#transport.ie#" title="info..åt..transport.ie" onclick="location.href=mc_lancerlien('info','transport.ie'); return false;" class="spip_mail">Courriel</a></p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="https://www.leapcard.ie/Home.aspx" class="spip_out" rel="external">Site d’information sur la « leap card »</a></li>
<li><a href="http://www.irishrail.ie/home" class="spip_out" rel="external">Site du réseau ferré en Irlande</a></li>
<li><a href="http://www.buseireann.ie/" class="spip_out" rel="external">Compagnie nationale de bus Irlandaise</a></li>
<li><a href="http://www.dublinbus.ie/Your-Journey1/Tourist-Information/" class="spip_out" rel="external">Guide d’accueil pour les touristes sur le site du réseau de bus de Dublin</a></li>
<li><a href="http://www.luas.ie/" class="spip_out" rel="external">Site du tramway à Dublin (<i>Dublin Light Rail System</i>)</a></li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/vie-pratique/article/transports). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
