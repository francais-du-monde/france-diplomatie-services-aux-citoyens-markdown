# Logement

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/vie-pratique/article/logement#sommaire_1">Où se loger ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/vie-pratique/article/logement#sommaire_2">Conditions de location</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/vie-pratique/article/logement#sommaire_3">Hôtels</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/vie-pratique/article/logement#sommaire_4">Electricité</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/vie-pratique/article/logement#sommaire_5">Electroménager</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Où se loger ?</h3>
<p>A Accra, les quartiers résidentiels sont les suivants : Airport Residential, Cantonments, East Legon (où se situe l’école française), Labone, North Ridge, Ringway estates, Tesano. De nombreaux expatriés vivent également dans d’autres quartiers, souvent construits plus récemment (Spintex, Roman Ridge, Dzorwulu, entre autres).</p>
<h3 class="spip"><a id="sommaire_2"></a>Conditions de location</h3>
<p>Alors que la demande de logements à louer a longtemps été largement supérieure à l’offre, le marché est actuellement en train de s’équilibrer, notamment en ce qui concerne les villas non meublées. L’offre reste cependant largement inadaptée, avec une prédominance de maisons et d’appartements de grande taille, souvent mal conçus.</p>
<p>Les prix sont élevés. Il faut compter de l’ordre de 2500 à 7500 USD pour un logement de trois chambres et plus. Le délai de recherche d’un logement atteint souvent un mois.</p>
<p>Même les quartiers résidentiels sont sujets aux coupures d’eau et d’électricité. Il n’existe pas de <strong>charges</strong> locatives à proprement parler : l’eau (réseau national mais aussi livraison par camions), l’électricité et le ramassage des poubelles se paient séparément, de même que l’indispensable gardiennage et les dépenses liées à l’utilisation d’un générateur.</p>
<p>Le montant mensuel estimé de ces dépenses liées au logement est de 600 €.</p>
<p>Les transactions s’effectuent directement entre le propriétaire et le locataire, et la négociation est possible clause par clause, aucune véritable législation protectrice du locataire n’étant applicable au Ghana. Attention à bien vérifier que le propriétaire dispose bien du titre de propriété (cas connu de fraude), lequel est habituellement conservé à son domicile. En cas de recours à une agence immobilière, il en coûtera au minimum un mois de loyer. Ne pas hésiter à négocier le prix du loyer. La durée du bail est généralement de un ou deux ans. Un état des lieux est nécessaire. Le loyer est payable d’avance (en général pour un an, parfois pour deux). Bien vérifier quels équipement sont inclus dans le prix et ceux pour lesquels il vous faudra payer une somme supplémentaire (appareils de climatisation, meubles de cuisine, réservoirs d’eau, générateur…). Pour les logements non terminés dont on vous promet qu’ils seront terminés à une date fixe, et en cas de travaux en cours ne payer d’avance qu’une partie de leur coût.</p>
<h3 class="spip"><a id="sommaire_3"></a>Hôtels</h3>
<p>Il existe une offre d’hôtels haut de gamme assez étendue à Accra (Novotel, Mövenpick, Golden Tulip, Holiday Inn) à des tarifs cependant élevés. Mais il existe aussi des hôtels au confort plus sommaires à des tarifs plus abordables.</p>
<p>Pour se loger sur une plus longue période, il existe un nombre d’appartements-hôtels avec cuisines équipées.</p>
<p>Des informations sur l’hébergement sont disponibles sur le site internet indiqué ci-dessous, sur les sites des hôtels.</p>
<h3 class="spip"><a id="sommaire_4"></a>Electricité</h3>
<p>Le courant est alternatif (220 volts) et très irrégulier, les prises de courant sont de type anglais (trois broches). Prévoir des régulateurs de tension, disponibles sur place. La distribution d’électricité est assez régulière et les coupures relativement peu fréquentes (deux ou trois par mois, d’une durée variable de quelques minutes à plusieurs heures). Durant le premier semestre de l’année 2013, des coupures de courant récurrentes ont eu lieu en raison de la rupture du gazoduc ouest-africain qui a désorganisé le fonctionnement des centrales fonctionnant au gaz naturel. Les choses sont rentrées dans l’ordre. Un générateur personnel reste cependant recommandé.</p>
<h3 class="spip"><a id="sommaire_5"></a>Electroménager</h3>
<p>Les disponibilités en appareils électroménagers sont bonnes.</p>
<p>La climatisation est recommandée sauf durant les mois les plus frais de l’année (juillet et août) et sauf dans les logements de conception plus ancienne mieux abrités et ventilés.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/vie-pratique/article/logement). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
