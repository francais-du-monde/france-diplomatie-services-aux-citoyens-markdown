# Recherche d’emploi

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/emploi-stage/article/recherche-d-emploi#sommaire_1">Outils pour la recherche d’emploi</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/emploi-stage/article/recherche-d-emploi#sommaire_2">Organismes sur place pour la recherche d’emploi</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Outils pour la recherche d’emploi</h3>
<h4 class="spip">Média </h4>
<h5 class="spip">Principaux titres de presse</h5>
<ul class="spip">
<li><a href="http://www.lesoir.be/" class="spip_out" rel="external">Le Soir</a>, surtout le samedi</li>
<li><a href="http://www.standaard.be/" class="spip_out" rel="external">De Standaard</a> (en flamand), le samedi</li>
<li><a href="http://www.lalibrebelgique.be" class="spip_out" rel="external">La Libre Belgique</a> (et son supplément Talent)</li>
<li><a href="http://www.vlan.be/" class="spip_out" rel="external">Le Vlan</a></li>
<li><a href="http://www.trends.be/" class="spip_out" rel="external">Trends-Tendances</a> (hebdomadaire)</li>
<li><a href="http://www.ackroyd.be/" class="spip_out" rel="external">The Bulletin</a>, hebdomadaire en anglais, pour les offres de sociétés anglophones et internationales</li></ul>
<h5 class="spip">Principaux sites d’emploi</h5>
<ul class="spip">
<li><a href="http://www.monster.be/" class="spip_out" rel="external">Monster.be</a></li>
<li><a href="http://www.stepstone.be/" class="spip_out" rel="external">Stepstone.be</a></li>
<li><a href="http://www.references.be/" class="spip_out" rel="external">References.be</a></li>
<li><a href="http://www.federgon.be" class="spip_out" rel="external">Federgon.be</a></li>
<li><a href="http://www.optioncarriere.be/" class="spip_out" rel="external">Optioncarriere.be</a></li>
<li><a href="http://www.arbajob.com/" class="spip_out" rel="external">Arbajob.com</a></li>
<li><a href="http://www.jobat.be/" class="spip_out" rel="external">Jobat.be</a></li>
<li><a href="http://www.optioncarriere.be/" class="spip_out" rel="external">Optioncarriere.be</a></li>
<li><a href="http://www.ingenuity.be" class="spip_out" rel="external">Ingenuity.be</a> (site d’emploi pour les ingénieurs)</li>
<li><a href="http://www.lexgo.be/" class="spip_out" rel="external">Lexgo.be</a> (annonces de recrutement pour les juristes)</li>
<li><a href="http://www.selor.be/" class="spip_out" rel="external">Selor.be</a> (emplois dans l’administration)</li>
<li><a href="http://www.alterjob.be/" class="spip_out" rel="external">Alterjob.be</a> (portail emplois du secteur non-marchand)</li>
<li><a href="http://www.vacature.com/home" class="spip_out" rel="external">Vacature.com/home</a> (site en flamand)</li>
<li><a href="http://www.adg.be/" class="spip_out" rel="external">Adg.be</a> (pour la communauté germanophone)</li>
<li><a href="http://www.belgique.enligne-fr.com/annonces_os.php" class="spip_out" rel="external">Belgique.enligne-fr.com/annonces_os.php</a> (offres de stages)</li>
<li><a href="http://www.e-emploi.be/" class="spip_out" rel="external">E-emploi.be</a> (référence les sites d’emploi autour de l’axe "Belgique francophone - Bruxelles")</li></ul>
<p><i>Le signalement d’organismes sur ce site ne saurait engager la responsabilité de la MFE quant à la qualité de leurs services.</i></p>
<h4 class="spip">Annuaires d’entreprises </h4>
<p>Le répertoire des principales implantations françaises en Belgique est édité et vendu par <a href="http://www.ubifrance.fr" class="spip_out" rel="external">Ubifrance</a> : implantation, expatriation, VIE et stages  investissements français et étrangers les principales implantations françaises en Belgique.</p>
<p>Il est par ailleurs possible de consulter des annuaires d’entreprises belges via les sites suivants :</p>
<ul class="spip">
<li><a href="http://www.kompass.com/kinl/index.php?_Lang=fr" class="spip_out" rel="external">Kompass</a></li>
<li><a href="http://www.abc-d.be/" class="spip_out" rel="external">ABC</a> pour le commerce et l’industrie</li>
<li><a href="http://www.trendstop.be/" class="spip_out" rel="external">Trendstop</a></li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Organismes sur place pour la recherche d’emploi</h3>
<p>Une fois arrivé en Belgique et inscrit auprès de la mairie, le candidat peut commencer ses démarches pour trouver un emploi. Pour multiplier ses chances de succès, il ne devra négliger aucune piste :</p>
<ul class="spip">
<li>les relations professionnelles</li>
<li>les candidatures spontanées (lettre de motivation + CV)</li>
<li>les annonces de journaux</li>
<li>les sites internet</li>
<li>les agences pour l’emploi</li>
<li>les centres d’orientation professionnelle</li>
<li>les agences d’intérim</li>
<li>les cabinets de recrutement</li></ul>
<h4 class="spip">Organismes à contacter</h4>
<h5 class="spip">Agences régionales pour l’emploi </h5>
<p>L’inscription à l’Agence régionale pour l’emploi est obligatoire pour bénéficier des allocations chômage et doit être effectuée dans les 7 jours suivant l’arrivée en Belgique. Cette inscription s’effectue auprès d’un des bureaux du <a href="http://www.leforem.be/" class="spip_out" rel="external">Forem</a> ou de la Maison de l’emploi la plus proche de votre domicile : particuliers  s’inscrire comme demandeur d’emploi.</p>
<p>Si le candidat était au chômage en France, il doit être muni des formulaires réglementaires (E301/E303) qui lui permettront de transférer ses droits aux allocations chômage pendant 3 mois.</p>
<p><strong>Trois administrations régionales et communautaires se chargent du placement en Belgique</strong> :</p>
<ul class="spip">
<li>Pour la région wallone : <a href="http://www.leforem.be/" class="spip_out" rel="external">Leforem.be</a></li>
<li>Pour la région flamande : <a href="http://www.vdab.be/" class="spip_out" rel="external">Vdab.be</a></li>
<li>Pour la région bruxelloise : <a href="http://www.actiris.be/" class="spip_out" rel="external">Actiris.be</a></li></ul>
<h5 class="spip">Contacts</h5>
<ul class="spip">
<li><a href="http://www.onem.fgov.be/" class="spip_out" rel="external">ONEM</a>  (Office national pour l’emploi)<br class="manualbr">(compétent pour l’administration et l’octroi des allocations de chômage)<br class="manualbr">Chaussée de Charleroi, 60 - 1000 Bruxelles - Tél. : 02.542.16.11</li>
<li><a href="http://www.adg.be/" class="spip_out" rel="external">ADG</a>  (Agence pour l’emploi de la communauté germanophone)</li>
<li><strong>IBFFP</strong> (Institut bruxellois francophone pour la formation professionnelle / Bruxelles Formation)<br class="manualbr">Rue Royale, 93 <br class="manualbr">1000 Bruxelles <br class="manualbr">Tél. : 0800 555 66 (appel gratuit pour toute information sur les formations)</li>
<li>Le réseau <a href="http://ec.europa.eu/eures/" class="spip_out" rel="external">EURES</a>  (European Employment Services), qui est une initiative de la Commission Européenne, a pour but de faciliter la mobilité des travailleurs au sein de l’Espace économique européen. Plus de 500 euroconseillers font fonctionner le réseau et circuler les informations entre les différentes agences nationales pour l’emploi. Ils peuvent être contactés à travers les organismes officiels pour l’emploi.</li>
<li><a href="http://europa.eu/epso/index_fr.htm" class="spip_out" rel="external">Office européen de sélection du personnel</a> (emploi au sein d’une institution de l’Union européenne)</li></ul>
<h5 class="spip">Agences d’intérim </h5>
<ul class="spip">
<li><a href="http://www.adecco.be/" class="spip_out" rel="external">Adecco</a>  (avec ses agences spécialisées)</li>
<li><a href="http://www.manpower.be/" class="spip_out" rel="external">Manpower Belgium</a></li>
<li><a href="https://www.randstad.be/fr/" class="spip_out" rel="external">Randstad</a></li>
<li><a href="http://www.vedior.be/" class="spip_out" rel="external">Vedior</a> (en relation avec la Commission Européenne)</li>
<li><a href="http://www.startpeople.be/" class="spip_out" rel="external">Start People Interim</a></li>
<li><a href="http://www.federgon.be/" class="spip_out" rel="external">Fédération des agences privées d’intérim</a></li></ul>
<h5 class="spip">Cabinets de recrutement</h5>
<p>Les listes des principaux cabinets de recrutement sont consultables dans les annuaires téléphoniques régionaux.</p>
<h5 class="spip">Centres d’informations</h5>
<p>Le réseau <a href="http://www.inforjeunes.be/" class="spip_out" rel="external">Infor Jeunes</a>, présent dans de nombreuses villes belges, vous apportera des renseignements sur le travail, les formations, etc.</p>
<p>Le Service public fédéral Emploi, Travail et Concertation sociale publie à l’attention des jeunes diplômés une brochure riches en informations <a href="http://www.emploi.belgique.be/publicationDefault.aspx?id=3604" class="spip_out" rel="external">Les clés pour un premier emploi</a></p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/emploi-stage/article/recherche-d-emploi). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
