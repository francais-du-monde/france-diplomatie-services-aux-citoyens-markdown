# Scolarisation

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/vie-pratique/article/scolarisation-109123#sommaire_1">Les établissements scolaires français en Allemagne</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/vie-pratique/article/scolarisation-109123#sommaire_2">Enseignement supérieur</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Les établissements scolaires français en Allemagne</h3>
<p>Pour toute information sur la scolarisation dans le système français à l’étranger, vous pouvez consulter notre thématique sur les <a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais.md" class="spip_in">études et la scolarisation</a> .</p>
<p>Vous y trouverez des renseignements sur :</p>
<ul class="spip">
<li>les établissements français du primaire et du secondaire à l’étranger ;</li>
<li>les bourses scolaires et la prise en charge des frais de scolarité à l’étranger ;</li>
<li>les possibilités qui s’offrent à vous si votre enfant ne peut être scolarisé à l’étranger dans le système français (enseignement à distance par le CNED, programme français langue maternelle (FLAM), internats en France) ;</li>
<li>les épreuves du baccalauréat à l’étranger ;</li>
<li>les bourses d’études supérieures en France et à l’étranger ;</li>
<li>l’équivalence des diplômes.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Enseignement supérieur</h3>
<p>Les étudiants originaires des pays membres de l’Union européenne sont soumis aux mêmes formalités qu’un étudiant allemand.</p>
<p>L’étudiant devra présenter l’attestation de réussite au baccalauréat, en français accompagnée de sa traduction en allemand, ainsi que des attestations permettant de prouver que l’intéressé(e) a suivi au minimum 400 heures d’allemand et possède, en conséquence, une maîtrise suffisante de la langue allemande.</p>
<p>Les notes françaises sont affectées d’un coefficient correcteur. Ainsi, selon la moyenne obtenue au baccalauréat, la période d’attente avant de commencer ses études pourra dans certains cas être longue (selon la matière choisie).</p>
<p>L’admission est limitée pour certaines spécialités : médecine, pharmacie, économie…</p>
<p>L’autorisation du ministère allemand de l’Education est <strong>souvent</strong> nécessaire.</p>
<p>395 établissements d’enseignement supérieur - <i>Hochschulen</i> - accueillent, en Allemagne, en 2009/2010, deux millions vingt cinq mille étudiants dont 12% d’étudiants étrangers.</p>
<p>Les établissements d’enseignement supérieur se déclinent en plusieurs catégories :</p>
<ul class="spip">
<li>104 universités générales et techniques (<i>Universitäten et Technische Universitäten)</i> accueillent un peu plus de 67,8% de tous les étudiants (2009/2010). Traditionnellement, les universités - <i>Universitäten</i> - proposent un enseignement académique classique et se consacrent à l’enseignement et à la recherche. Elles attribuent les titres universitaires et délivrent les qualifications au professorat. Un dixième d’entre elles qualifiées d’universités techniques - T<i>echnische Universitäten</i> - proposent, principalement mais non exclusivement, les disciplines des sciences de l’ingénieur.</li>
<li>Les 190 écoles supérieures de sciences appliquées - <i>Fachhochschulen</i> -, qui accueillent 28,1% des étudiants (2009/2010), proposent un enseignement académique plus proche de la pratique que celui des universités, dans des disciplines de spécialisation à but professionnel. Les études durent trois à quatre ans (soit 6 à 8 semestres) et incluent des stages. Elles peuvent désormais déboucher sur un doctorat. Les étudiants accèdent le plus souvent à des fonctions de responsabilités intermédiaires. S’ajoutent à cet effectif 30 écoles supérieures spécialisées dans l’administration - <i>Verwaltungsfachhochschulen</i>.</li>
<li>51 écoles supérieures des beaux-arts et de musique - <i>Kunst- und Musikhochschulen</i>- et 14 écoles supérieures de théologie.</li>
<li>6 écoles supérieures de pédagogie - <i>Pädagogische Hochschulen</i> - situées dans le Bade-Wurtemberg, forment les enseignants des classes des établissements équivalant aux dernières années du primaire, au collège et aux premières années de lycée, enseignement général ou technique.</li></ul>
<p><strong>Pour en savoir plus</strong> :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures.md" class="spip_in">Espace étudiant</a></p>
<p><i>Mise à jour : juillet 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/vie-pratique/article/scolarisation-109123). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
