# Convention fiscale

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/fiscalite/article/convention-fiscale-113153#sommaire_1">Champ d’application de la convention</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/fiscalite/article/convention-fiscale-113153#sommaire_2">Elimination de la double imposition</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/fiscalite/article/convention-fiscale-113153#sommaire_3">Modalités d’imposition des revenus catégoriels</a></li></ul>
<p><i>Ces informations n’ont qu’une valeur indicative et ne se substituent pas à la documentation officielle de l’administration.</i></p>
<p><strong>Impôts sur le revenu et sur la fortune</strong></p>
<p>La France et les Etats-Unis ont signé <strong>le 31 août 1994</strong> à Washington une convention en vue d’éviter les doubles impositions et de prévenir l’évasion et la fraude fiscales en matière d’impôts sur le revenu et sur la fortune. Cette convention, parue au Journal Officiel du 22 mars 1996 (décret n°96-222), est <strong>entrée en vigueur le 30 décembre 1995</strong>.</p>
<p>Elle a été modifiée par un <strong>avenant</strong> signé à Washington le <strong>8 décembre 2004</strong> et <strong>entré en vigueur le 21 décembre 2006</strong> (décret n°2007-78 paru au Journal officiel du 24 janvier 2007) et par un <strong>nouvel avenant signé le 13 janvier 2009, entré en vigueur le 23 décembre 2009</strong> :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www11.minefi.gouv.fr/boi/boi2010/14aipub/textes/14a610/14a610.pdf" class="spip_out" rel="external">Bulletin officiel des impôts N° 83 du 14 septembre 2010</a>.</p>
<p><strong>Impôts sur les successions et sur les donations</strong></p>
<p>La France et les Etats-Unis ont signé le <strong>24 novembre 1978</strong> une convention tendant à éviter les doubles impositions et à prévenir l’évasion fiscale en matière d’impôts sur les successions et sur les donations. Cette convention, parue au Journal officiel du 1er octobre 1980 (décret n°80-771), est <strong>entrée en vigueur le 1eroctobre 1980. </strong></p>
<p>Elle a été modifiée par un <strong>avenant</strong> signé à Washington le <strong>8 décembre 2004</strong> et <strong>entré en vigueur le 21décembre 2006</strong> (décret n°2007-78paru au Journal officiel du 24 janvier 2007).</p>
<p>Ces textes sont disponibles sur les sites Internet suivants :</p>
<ul class="spip">
<li><a href="http://www.impots.gouv.fr/" class="spip_out" rel="external">administration fiscale</a> ;</li>
<li>le <a href="http://www.legifrance.gouv.fr/" class="spip_out" rel="external">service public de la diffusion du droit</a>. Les dispositions conventionnelles qui, en application de l’article 55 de la Constitution française, priment sur les dispositions du droit interne, répartissent entre les deux Etats le droit d’imposer les revenus perçus par leurs résidents respectifs.</li></ul>
<p>L’analyse qui suit ne concerne que la convention fiscale en matière d’impôts sur le revenu et sur la fortune.</p>
<h3 class="spip"><a id="sommaire_1"></a>Champ d’application de la convention</h3>
<p>Cette convention tend à éviter les doubles impositions qui pourraient résulter de l’application des législations de ces deux Etats et fixe des règles d’assistance administrative et juridique réciproques en matière d’impôts sur le revenu et sur la fortune. Elle s’applique, sauf dispositions contraires, aux résidents de l’un ou des deux Etats contractants.</p>
<p><strong>Notion de résidence</strong></p>
<p>L’article 4, paragraphes 1 et 2, de la convention définit les personnes qui peuvent être considérées comme "résidentes d’un Etat contractant" ou de chacun de ces deux Etats.</p>
<p>Aux termes du paragraphe 1 de cet article de la convention, une personne est considérée comme "résidente d’un Etat contractant" lorsque, en vertu de la législation de cet Etat, elle se trouve assujettie à l’impôt en raison de son domicile, de sa résidence, de son siège de direction, de son siège social ou de tout autre critère analogue. Toutefois, cet article ne s’applique pas aux personnes qui ne sont assujetties à l’impôt dans cet Etat que pour les revenus de sources situées dans cet Etat ou pour la fortune qui y est située.</p>
<p>Concernant la justification du domicile, la France ne considère un citoyen des Etats-Unis ou un étranger admis à séjourner en permanence aux Etats-Unis et titulaire de la " carte verte " comme un résident des Etats-Unis que lorsque cette personne physique y séjourne à titre principal.</p>
<p>Le paragraphe 4 du même article règle la situation des personnes résidant dans les deux Etats contractants en appliquant les critères suivants :</p>
<ul class="spip">
<li>le foyer d’habitation permanent (il s’agit, par exemple, du lieu de situation du conjoint ou des enfants) ;</li>
<li>l’Etat où la personne possède le centre de ses intérêts vitaux (tant professionnels que privés) ;</li>
<li>l’Etat dans lequel elle séjourne de façon habituelle (notion de 183 jours de présence physique sur le territoire au cours d’une année fiscale) ;</li>
<li>à défaut, l’Etat dont elle possède la nationalité.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Elimination de la double imposition</h3>
<p>L’article 24 de la convention fixe les dispositions en vue d’éviter la double imposition entre les deux Etats contractants.</p>
<p>Le régime de l’imposition exclusive dans l’un des Etats est le régime normal s’appliquant aux contribuables qui n’ont pas la nationalité américaine.</p>
<p>Cet article prévoit la généralisation de la méthode de l’imputation d’un crédit d’impôt en matière de revenus et de fortune.</p>
<p>La France réimpose des revenus de source américaine attribués à une personne physique ou morale résidente en France, que l’imposition aux Etats-Unis de ces revenus soit exclusive ou non. Le crédit d’impôt imputable sur l’impôt français est égal soit à l’impôt américain (dividendes, intérêts, artistes…), soit à l’impôt français.</p>
<p>Si le bénéficiaire résident en France est de nationalité américaine, l’article 24, paragraphe 1-b), étend, sous certaines conditions, l’effacement total de l’imposition en France par l’attribution d’un crédit d’impôt égal à l’impôt français, aux revenus suivants de source américaine : dividendes, intérêts, redevances, plus-values mobilières, , pensions alimentaires, revenus des enseignants, des chercheurs et des étudiants qui seraient exonérés de l’impôt américain s’ils n’étaient pas de nationalité américaine.</p>
<p>Pour bénéficier de l’exonération ou de la restitution de l’impôt français, le ressortissant américain résident fiscal en France doit apporter la preuve de son imposition aux Etats-Unis.</p>
<p>Par ailleurs, l’article 24, paragraphe 1-d)ii), prévoit que le crédit d’impôt est imputable sur l’impôt français, soit en appliquant un taux proportionnel aux revenus américains si l’impôt français correspondant est un impôt proportionnel, soit en appliquant, s’il s’agit d’un impôt progressif, aux revenus américains le taux moyen résultant de l’imposition en droit interne français, du revenu mondial du contribuable comme s’il n’y avait pas de convention fiscale (règle du taux effectif).</p>
<h3 class="spip"><a id="sommaire_3"></a>Modalités d’imposition des revenus catégoriels</h3>
<h4 class="spip">Traitements, salaires, pensions et rentes</h4>
<h5 class="spip">Rémunérations privées</h5>
<p><strong>Principe</strong></p>
<p>L’article 15, paragraphe 1, précise que les salaires et les traitements d’origine privée perçus au titre d’un emploi salarié par un résident d’un Etat contractant sont, en règle générale, imposables dans l’Etat où s’exerce l’activité.</p>
<p><strong>Exceptions à cette règle générale</strong></p>
<p>1. Le paragraphe 2 de l’article 15 prévoit que les rémunérations qu’un résident d’un Etat contractant reçoit au titre d’un emploi salarié exercé dans l’autre Etat contractant ne sont imposables que dans l’Etat de résidence si les trois conditions suivantes sont remplies simultanément :</p>
<ul class="spip">
<li>le bénéficiaire séjourne dans l’Etat où il travaille pendant une période ou des périodes n’excédant pas au total 183 jours au cours de toute période de 12 mois commençant ou se terminant dans l’année fiscale considérée ;</li>
<li>les rémunérations sont payées par un employeur ou pour le compte d’un employeur qui n’est pas un résident de l’Etat où cette personne exerce son emploi ;</li>
<li>la charge des rémunérations n’est pas supportée par un établissement stable ou une base fixe que l’employeur a dans l’Etat où cette personne exerce son emploi.</li></ul>
<p><strong>Exemple</strong> : Monsieur X est envoyé trois mois, soit 90 jours (mai, juin, juillet de l’année n) par une PME française en vue de prospecter le marché américain. Cette entreprise ne dispose d’aucune succursale, ni bureau aux Etats-Unis. Dans un tel cas, Monsieur X devra déclarer ses revenus en France.</p>
<p>Au contraire, si Monsieur X est envoyé du mois de février inclus au mois de novembre inclus, son séjour de plus de 183 jours aux Etats-Unis entraîne son imposition dans ce pays, même si son salaire est versé par cette même PME et dans les mêmes conditions.</p>
<p>2. Il résulte des dispositions du paragraphe 3 de l’article 15 de la convention que les revenus professionnels des salariés, résidents d’un Etat, employés en tant qu’équipage régulier à bord d’un navire ou d’un aéronef en trafic international, ne sont imposables que dans l’Etat de résidence.</p>
<h5 class="spip">Rémunérations publiques</h5>
<p><strong>Principe</strong></p>
<p>L’article 19 de la convention prévoit que les rémunérations, autres que les pensions, payées par un Etat contractant à une personne physique au titre de services rendus à cet Etat ne sont imposables que dans cet Etat.</p>
<p><strong>Exceptions</strong></p>
<p>Les rémunérations et pensions publiques ne sont pas concernées par cette règle dès lors qu’elles résultent d’une activité industrielle ou commerciale.</p>
<p>D’autre part, ces rémunérations publiques ne sont imposables que dans l’autre Etat contractant si les services sont rendus dans cet Etat et si la personne physique est un résident de cet Etat et en possède la nationalité sans posséder en même temps la nationalité du premier Etat (article 19 - 1 b)).</p>
<p>Enfin, l’article 29, paragraphe 9, prévoit que les rémunérations publiques autres que les pensions ne sont imposables qu’aux Etats-Unis si les services sont rendus aux Etats-Unis et si la personne physique est un résident des Etats-Unis et en possède la nationalité ou est un étranger admis à séjourner en permanence aux Etats-Unis (titulaire de la « carte verte »).</p>
<h5 class="spip">[Pensions (article 18)</h5>
<p>Les sommes payées en application de la législation sur la sécurité sociale d’un Etat contractant, ainsi que les sommes versées par un Etat contractant dans le cadre d’un régime de retraite au titre d’un emploi antérieur, à un résident de l’autre Etat contractant ne sont imposables que dans le premier Etat. Il doit s’agir d’un régime de retraite constitué dans cet Etat.</p>
<p>Pour l’application de cet article, la France reconnaît :</p>
<ul class="spip">
<li>les plans qualifiés visés par la section 401 (a) de l’<i>Internal Revenue Code</i> ;</li>
<li>les plans individuels de retraite (<i>individual retirement plans</i>) [y compris les plans individuels de retraite qui font partie d’un plan de retraite simplifié des salariés qui remplit les conditions de la section 408 (k), les comptes individuels de retraite (<i>retirement individual accounts</i>), les rentes viagères individuelles de retraite et les comptes visés par la section 408 (p) ] ;</li>
<li>les plans qualifiés visés par la section 403 (a) et ceux visés par la section 403 (b), sont considérés de façon générale comme correspondant à un régime de retraite constitué, établi et reconnu aux fins d’imposition en France. Pour l’application de cet article, les Etats-Unis reconnaissent les régimes de retraite français ou tout autre régime de retraite organisés en application de la législation française sur la sécurité sociale.</li></ul>
<p>Les cotisations versées par les salariés résidents d’un Etat contractant, sans toutefois posséder la nationalité de cet Etat, à un régime de retraite de l’autre Etat contractant sont déductibles du revenu imposable de l’Etat de résidence dans les mêmes conditions que les cotisations versées à un régime de retraite de l’Etat de résidence du salarié.</p>
<h5 class="spip">Etudiants et stagiaires</h5>
<p>L’article 21, paragraphes 1-a) et 1-b), de la convention, prévoit que les personnes résidentes d’un Etat contractant immédiatement avant de se rendre dans l’autre Etat contractant sont exonérées d’impôt dans ce dernier Etat lorsqu’elles y séjournent temporairement dans le but principal :</p>
<ul class="spip">
<li>d’y poursuivre des études auprès d’une université ou d’un établissement d’enseignement agréé ;</li>
<li>d’y effectuer un stage destiné à assurer la formation nécessaire à l’exercice d’une profession ou d’une spécialité ;</li>
<li>d’y étudier ou d’y effectuer des recherches en tant que bénéficiaire d’une bourse, d’une allocation ou d’une récompense reçue d’une organisation gouvernementale, religieuse, charitable, scientifique, artistique, culturelle ou éducative. L’exonération d’impôts porte sur les sommes suivantes :</li></ul>
<ul class="spip">
<li>les dons reçus de l’étranger pour les frais d’entretien, d’études, de recherche ou de formation ;</li>
<li>les bourses, allocations ou récompenses ;</li>
<li>les revenus provenant de services personnels rendus dans l’Etat de séjour temporaire qui n’excèdent pas 5000 dollars ou leur équivalent en euros au cours de l’année d’imposition considérée. Ces avantages sont limités à une période raisonnablement ou habituellement requise pour réaliser l’objet du séjour et ne devant pas excéder cinq années d’imposition au total.</li></ul>
<p>Par ailleurs, l’article 21 dans son paragraphe 2 prévoit que la personne résidente d’un Etat contractant immédiatement avant de se rendre dans l’autre Etat contractant pour y effectuer un séjour temporaire et y travailler en qualité d’employé ou de contractuel d’un résident de son Etat d’origine dans le but soit d’y acquérir une expérience technique, soit d’y poursuivre ses études, est exonérée d’impôt dans l’Etat de séjour pendant une période de 12 mois consécutifs pour les revenus n’excédant pas 8000dollars ou l’équivalent en euros.</p>
<h5 class="spip">Enseignants et chercheurs</h5>
<p>L’article 20 paragraphe 1 de la convention dispose que les enseignants et chercheurs, résidents d’un Etat, qui se rendent dans l’autre Etat, à l’invitation du Gouvernement de cet Etat ou d’un établissement d’enseignement ou de recherche agréé de cet Etat, dans le but principal d’y enseigner ou de s’y livrer à des travaux de recherche, auprès d’une université ou d’un établissement d’enseignement ou de recherche agréé, ne sont pas imposables dans l’Etat de séjour, pendant une période n’excédant pas deux années à compter de la date d’arrivée dans l’Etat de séjour. Ils demeurent par conséquent imposables dans l’Etat d’origine. A noter que cet avantage n’est octroyé qu’une seule fois.</p>
<p>Le paragraphe 2 du même article prévoit que ce régime d’exonération n’est pas applicable aux revenus provenant de travaux de recherche entrepris en vue de la réalisation d’un avantage particulier bénéficiant à une personne déterminée.</p>
<p>Il est important d’indiquer par ailleurs à ces personnes que l’administration fiscale américaine met à leur disposition un formulaire <i>"FORM 8843 – Treaty-based Return Position Disclosure"</i> leur permettant d’obtenir cette exonération.</p>
<h5 class="spip">Autres catégories de revenus</h5>
<p><strong>Imposition de la fortune</strong></p>
<p>L’article 23, paragraphe 1 a), dispose que la fortune constituée par des biens immobiliers situés dans un Etat est imposable dans cet Etat.</p>
<p>Aux termes du paragraphe 2, la fortune d’une personne physique constituée sous forme d’actions, parts ou droits qui font partie d’une participation d’au moins 25% dans une société résidente d’un Etat, reste imposable dans cet Etat.</p>
<p>Le paragraphe 5 précise que les autres éléments de la fortune d’un résident d’un Etat restent imposables dans cet Etat.</p>
<p><strong>Imposition des gains en capital</strong></p>
<p>L’article 13, paragraphe 1, de la convention prévoit que les gains provenant de la vente de biens immobiliers sont imposables dans l’Etat où sont situés ces biens.</p>
<p>L’article 13, paragraphe 3, dispose que la cession de biens mobiliers qui font partie de l’actif d’un établissement stable qu’une entreprise ou un résident d’un Etat a dans l’autre Etat, est imposable dans ce dernier Etat. Lorsque le transfert assimilé à une aliénation de ces biens mobiliers a lieu hors de ce dernier Etat, les gains correspondant à la période écoulée jusqu’à la date de ce transfert sont imposables dans ce dernier Etat. Les gains correspondant à la période écoulée après cette date sont imposables dans le premier Etat.</p>
<p>Le paragraphe 6 du même article prévoit que les gains provenant de la cession de biens autres que ceux visés aux paragraphes 1, 2, 3 et 4 ne sont imposables que dans l’Etat de résidence du cédant.</p>
<p><strong>Exemple</strong></p>
<p>Monsieur X détient la majorité des actions d’une société dont il est le dirigeant aux Etats-Unis. A l’occasion de sa retraite, il décide de venir résider en France.</p>
<p>En vertu du paragraphe 6 de l’article 13, deux cas sont analysés :</p>
<p>1. Monsieur X cède une partie de ses actions avant son retour : la plus-value réalisée est imposable aux Etats-Unis, étant observé que Monsieur X était résident aux Etats-Unis et donc imposable pour l’ensemble de ses revenus dans ce pays.</p>
<p>2. Monsieur X cède ses actions trois ans après son retour en France alors qu’il est devenu imposable dans ce pays. En application de la Convention telle que décrite ci-dessus, la plus-value est imposable en France.</p>
<p><strong>Bénéfices des entreprises</strong></p>
<p>Aux termes de l’article 7, paragraphe 1, de la convention, les bénéfices des entreprises d’un Etat contractant ne sont imposables que dans cet Etat. Mais si l’entreprise exerce son activité dans l’autre Etat contractant par l’intermédiaire d’un établissement stable, ses bénéfices sont imposables dans l’autre Etat, mais uniquement dans la mesure où ces bénéfices sont imputables à cet établissement stable.</p>
<p>La notion d’établissement stable est définie à l’article 5 de la convention.</p>
<p><strong>Revenus non commerciaux et bénéfices des professions indépendantes</strong></p>
<p>En vertu des dispositions du paragraphe 1 de l’article 14 de la convention, les revenus qu’un résident d’un Etat contractant tire d’une profession libérale ou d’autres activités de caractère indépendant ne sont imposables que dans cet Etat, à moins que ce résident n’exerce de telles activités dans l’autre Etat et qu’il n’y dispose de façon habituelle d’une base fixe pour l’exercice de ses activités. Dans ce cas, les revenus sont imposables dans l’autre Etat, mais uniquement dans la mesure où ils sont imputables à cette base fixe.</p>
<p>On entend par " profession libérale " les activités indépendantes d’ordre scientifique, littéraire, artistique, éducatif ou pédagogique, ainsi que les activités indépendantes des médecins, avocats, ingénieurs, architectes, dentistes et comptables.</p>
<p><strong>Redevances</strong></p>
<p>L’article 12, paragraphe 1, prévoit que les redevances provenant d’un Etat contractant et payées à un résident de l’autre Etat contractant ne sont imposables que dans l’Etat de résidence du bénéficiaire.</p>
<p><strong>Artistes et sportifs</strong></p>
<p>L’article 17 de la convention prévoit l’imposition des artistes et sportifs dans l’Etat d’exercice de l’activité, sauf si le montant des recettes brutes (y compris les dépenses remboursées ou supportées) ne dépasse pas 10000 dollars ou l’équivalent en euros pour l’année d’imposition considérée.</p>
<p><strong>Revenus immobiliers</strong></p>
<p>L’article 6, paragraphe 1, prévoit que les revenus des biens immobiliers (y compris les bénéfices des exploitations agricoles ou forestières) sont imposables dans l’Etat où ces biens sont situés.</p>
<p>Cette règle s’applique également aux gains provenant de la cession ou de l’échange desdits biens ou droits selon les dispositions de l’article 13, paragraphe 1.</p>
<p><strong>Revenus de capitaux mobiliers - les dividendes</strong></p>
<p>L’article 10, paragraphe 6 a), de la convention définit la notion de dividende. Elle comprend tous les revenus soumis au régime de la distribution. Elle inclut également les intérêts de certains titres de créances.</p>
<p>L’article 10, paragraphe 1, prévoit que les dividendes sont imposables dans l’Etat de résidence du bénéficiaire.</p>
<p>Toutefois, le paragraphe 2 du même article dispose que l’Etat de la source peut également imposer les dividendes à un taux qui ne peut excéder :</p>
<ul class="spip">
<li>5% du montant brut lorsque le bénéficiaire effectif est une société sous réserve de respecter certaines conditions (directement ou indirectement 10% du capital si la société distributrice est résidente de France ou 10% des droits de vote si la société distributrice est résidente des Etats-Unis) ;</li>
<li>ou, 15% du montant brut dans tous les autres cas.</li></ul>
<p>Cependant, aucune imposition ne peut être opérée par l’Etat de la source lorsque le bénéficiaire effectif des dividendes est une société qui a détenu directement ou indirectement, pendant une période de douze mois précédant la date de détermination des droits à dividende, soit 80% du capital de la société distributrice lorsqu’elle est résidente française, soit 80% des droits de vote de la société distributrice lorsqu’elle est résidente des Etats-Unis et qui respecte certaines conditions (paragraphe 3 de l’article 10).</p>
<p><strong>Revenus de capitaux mobiliers - les intérêts</strong></p>
<p>L’article 11, paragraphe 1, de la convention prévoit l’imposition exclusive des intérêts dans l’Etat de résidence du bénéficiaire.</p>
<p>Toutefois, les intérêts relatifs à des titres assortis d’une clause de participation aux bénéfices du débiteur ou d’une entreprise associée au sens de l’article 9 de la convention, sont aussi imposables dans l’Etat de la source à un taux ne pouvant excéder 15%.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/fiscalite/article/convention-fiscale-113153). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
