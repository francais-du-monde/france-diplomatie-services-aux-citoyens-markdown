# Loisirs et culture

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/vie-pratique/article/loisirs-et-culture#sommaire_1">Tourisme</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/vie-pratique/article/loisirs-et-culture#sommaire_2">Activités culturelles</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/vie-pratique/article/loisirs-et-culture#sommaire_3">Sports</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/vie-pratique/article/loisirs-et-culture#sommaire_4">Télévision – Radio</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/vie-pratique/article/loisirs-et-culture#sommaire_5">Presse française</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Tourisme</h3>
<p>Le pays regorge de sites naturels exceptionnels (volcans en activité, forêts, geysers, glaciers, plages) et se prête particulièrement aux activités de plein air : randonnées, ski, rafting… et la faune est variée. L’aspect sauvage des paysages, la qualité de l’environnement et le vaste choix de pratiques sportives ainsi que l’impact du tournage des films <i>Le Seigneur des Anneaux</i> et <i>Le Hobbit</i> ont participé à l’essor du tourisme international.</p>
<p><strong>Les hauts-lieux du tourisme</strong> :</p>
<ul class="spip">
<li>dans l’île du Nord : Auckland, Rotorua, Taupo, Napier, la Baie des Îles, Northland, Taranaki/Mont Egmont et Wellington ;</li>
<li>dans l’île du Sud : la côte ouest, Christchurch, Akaora, Dunedin, Queenstown, Kaikoura, Abel Tasman Park et Marlborough Sounds.</li></ul>
<p>Il est conseillé de tenir compte des variations rapides de température en hiver, surtout dans les montagnes, et de prévoir une protection contre le soleil. Pour plus de renseignements :</p>
<ul class="spip">
<li><a href="http://www.newzealand.com/travel" class="spip_out" rel="external">Newzealand.com</a></li>
<li><a href="http://www.tourism.govt.nz/" class="spip_out" rel="external">Tourism.govt.nz</a></li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Activités culturelles</h3>
<p><strong>Activités culturelles en français</strong></p>
<p><strong>Les alliances françaises, centres culturels et médiathèques</strong></p>
<p>Les Alliances françaises présentent des expositions, organisent des soirées et des manifestations sociales et sont équipées de bibliothèques et de médiathèques. Elles s’associent au montage d’opérations hors les murs, tels que festivals de cinéma ou concerts. La bibliothèque la plus importante (Centre de ressources sur la France contemporaine) se trouve au sein de l’Alliance de Wellington.</p>
<p><a href="http://www.french.co.nz/" class="spip_out" rel="external">Alliance française de Wellington</a> <br class="manualbr">PO Box 3002 <br class="manualbr">78, Victoria Street <br class="manualbr">3rd floor <br class="manualbr">6015 Wellington <br class="manualbr">Tél. : 04 472 12 72 <br class="manualbr">Fax : 04 472 29 36 <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/vie-pratique/article/loisirs-et-culture#alliance#mc#french.co.nz#" title="alliance..åt..french.co.nz" onclick="location.href=mc_lancerlien('alliance','french.co.nz'); return false;" class="spip_mail">Courriel</a></p>
<p>Les coordonnées des onze Alliances françaises de Nouvelle-Zélande sont disponibles sur le site de la <a href="http://www.fondation-alliancefr.org/" class="spip_out" rel="external">Fondation Alliance française</a> en Nouvelle-Zélande.</p>
<p>Le centre de ressources sur la France contemporaine situé dans les locaux de l’Alliance française de Wellington propose une vaste collection de livres, vidéos, CD, DVD, magazines et livres de référence sur la France.</p>
<p><a href="http://www.french.co.nz/" class="spip_out" rel="external">French Resource Centre</a> <br class="manualbr">Level 3 <br class="manualbr">78, Victoria Street <br class="manualbr">PO Box 3002 <br class="manualbr">Wellington</p>
<p><a href="http://www.alliance-francaise.co.nz/" class="spip_out" rel="external">Alliance française d’Auckland</a> <br class="manualbr">9, Kirk Street Grey Lynn Auckland <br class="manualbr">Tél. : 09 376 00 09 <br class="manualbr">Fax : 09 376 00 98 <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/vie-pratique/article/loisirs-et-culture#information#mc#alliance-francaise.co.nz#" title="information..åt..alliance-francaise.co.nz" onclick="location.href=mc_lancerlien('information','alliance-francaise.co.nz'); return false;" class="spip_mail">Courriel</a></p>
<p><a href="http://www.af-christchurch.co.nz/" class="spip_out" rel="external">Alliance française de Christchurch</a> <br class="manualbr">PO Box 650 <br class="manualbr">Christchurch <br class="manualbr">Tél. : 03 365 83 70 <br class="manualbr">Fax : 03 365 83 72 <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/vie-pratique/article/loisirs-et-culture#enquiries#mc#afchristchurch.org.nz#" title="enquiries..åt..afchristchurch.org.nz" onclick="location.href=mc_lancerlien('enquiries','afchristchurch.org.nz'); return false;" class="spip_mail">Courriel</a></p>
<p><strong>Activités culturelles locales</strong></p>
<p>Un réseau de salles de cinéma assure la projection de films essentiellement américains, britanniques, australiens et néo-zélandais. La production théâtrale est importante, surtout dans les quatre principales villes du pays (Wellington, Auckland, Christchurch, Dunedin). Les concerts et récitals, très fréquents, sont présentés dans de bons centres.</p>
<p>Les orchestres et compagnies de danse ont acquis une reconnaissance internationale (orchestre symphonique à Wellington, l’orchestre philharmonique d’Auckland, le Royal New Zealand Ballet et le Black Grace Dance Company).</p>
<p>Une importante manifestation culturelle biennale a lieu dans la capitale, dans le cadre du Festival international des Arts. Plusieurs centres culturels accueillent des manifestations artistiques avec à l’affiche des artistes locaux ou internationaux.</p>
<p>Les grandes villes organisent des expositions (peinture, sculpture, céramique, livres…) avec un retour en force de la culture traditionnelle maorie et polynésienne.</p>
<h3 class="spip"><a id="sommaire_3"></a>Sports</h3>
<p>Tous les sports collectifs ou individuels peuvent être pratiqués, en particulier le rugby, le football, le basket, le volley, l’aviron, le cricket, le tennis, le squash, la voile, le ski, les sports équestres, l’alpinisme, la randonnée, le delta-plane, le golf… A noter que la navigation de plaisance peut être dangereuse (vents, récifs).</p>
<p>Les rencontres sportives sont très nombreuses, hebdomadaires en général. Des courses hippiques ont lieu tous les jours et sont retransmises à la télévision sur la chaîne Livetrack.</p>
<h3 class="spip"><a id="sommaire_4"></a>Télévision – Radio</h3>
<p>A Auckland, la chaîne communautaire ’Face TV’ propose pendant 30 minutes par jour la rediffusion du journal télévisé de TV5. Pour plus d’informations sur les programmes et le mode de réception, consulter <a href="http://facetv.co.nz/index.php/tune-in" class="spip_out" rel="external">le site de la chaîne</a>.</p>
<p>La Nouvelle-Zélande a adopté la télévision digitale en décembre 2013. L’ensemble des chaînes diffusées gratuitement porte le nom de Freeview. Parmi ces chaînes se trouvent deux chaînes publiques de divertissement (TV One et TV2), une chaîne subventionnée par le gouvernement (Maori Television), quatre chaînes privées de divertissement (TV3, FOUR, Prime et Choice), une chaîne musicale (C4), une chaîne d’informations (Al Jazeera), une chaîne d’émissions religieuses (FirstLight), une chaîne dédiée aux courses hippiques (Trackside), une chaîne de sports (Sommet Sports), une chaîne chinoise (CTV8) et trois chaînes de téléachat.</p>
<p>La radio nationale, <i>Radio New Zealand</i>, comprend trois stations : Radio New Zealand National, Radio New Zealand Concert et le réseau AM.<i>Radio New Zealand</i> <i>International </i>émet en ondes courtes sur la région Pacifique-sud.</p>
<p>L’Université Victoria de Wellington dispose d’une antenne satellitaire qui permet notamment la réception de programmes en français.</p>
<p>Le cinéma français détient une place de choix parmi les cinématographies étrangères. Dans les villes d’Auckland et de Wellington, les films français sont toujours présentés en version originale. Les festivals internationaux programment des films français. Pour plus de renseignements sur ces événements et sur les cycles français des cinémathèques, consulter les sites suivants :</p>
<ul class="spip">
<li><a href="http://www.nzfilmsociety.org.nz/" class="spip_out" rel="external">Nzfilmsociety.org.nz</a></li>
<li><a href="http://www.filmarchive.org.nz/" class="spip_out" rel="external">Filmarchive.org.nz</a></li></ul>
<p>Les émissions de Radio France Internationale sont accessibles par le satellite Asiasat 3 et Optus 2.</p>
<h3 class="spip"><a id="sommaire_5"></a>Presse française</h3>
<p>Les grands quotidiens français sont difficiles à se procurer.</p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/vie-pratique/article/loisirs-et-culture). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
