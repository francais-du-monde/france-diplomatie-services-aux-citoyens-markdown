# Quelles élections et selon quelles modalités ?

<p class="spip_document_77502 spip_documents spip_documents_center">
<img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L1000xH190/demarches_administratives_cle0d4f71-1-190ee.jpg" width="1000" height="190" alt=""></p>
<p>Les Français de l’étranger ont la possibilité de voter dans un bureau de vote ouvert auprès de leur ambassade ou poste consulaire pour tous les scrutins nationaux ainsi que pour l’élection des conseillers consulaires.</p>
<h4 class="spip">Définitions préalables :</h4>
<ul class="spip">
<li><strong>Scrutin national :</strong> élection du président de la République, élections législatives, européennes, référendum.</li>
<li><strong>Scrutin local :</strong> élections régionales, cantonales, municipales.</li>
<li><strong>Elections spécifiquement destinées aux Français de l’étranger : </strong> conseillers consulaires.</li></ul>
<h4 class="spip">A l’étranger, quelles modalités de vote ? </h4>
<ul class="spip">
<li><strong>Elections présidentielle, européennes, référendum : </strong> à l’urne et par procuration.</li>
<li><strong>Elections législatives :</strong> à l’urne, par procuration, par voie électronique et par correspondance sous pli fermé.</li>
<li><strong>Election des conseillers consulaires :</strong> vote à l’urne, par procuration et par voie électronique (internet).</li></ul>
<p><i>Mise à jour : janvier 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/droit-de-vote-et-elections-a-l-etranger/article/quelles-elections-et-selon-quelles). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
