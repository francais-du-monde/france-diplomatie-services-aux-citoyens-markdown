# Passeport, visa, permis de travail

<p>Quel que soit le motif de votre séjour, renseignez-vous auprès de la section consulaire de l’ambassade de Turquie à Paris qui vous informera sur la règlementation en matière d’entrée et de séjour en Turquie, règlementation que vous devrez impérativement respecter. N’hésitez pas à consulter le site ministère de l’Intérieur turc afin de vous familiariser avec les modalités d’entrée et de séjour dans le pays.</p>
<p><strong>En aucun cas, vous n’êtes autorisé à travailler en Turquie sans permis adéquat. </strong></p>
<p>L’ambassade de France à Ankara n’est pas l’organisme compétent pour répondre à vos demandes d’information concernant votre séjour en Turquie.</p>
<p>Pour toute information de séjour en Turquie, il est vivement conseillé de prendre l’attache de l’un des <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-etrangers-en-france/article/annuaire-des-representations-etrangeres-en-france" class="spip_in">consulats de Turquie en France</a>. <strong>Les informations suivantes sont données à titre indicatif.</strong></p>
<p>Pour les touristes français, la carte nationale d’identité ou le passeport suffisent. Aucun visa n’est demandé pour un <strong>séjour inférieur à trois mois</strong>.</p>
<p>Les enfants mineurs doivent être également munis d’un titre de voyage ou d’une carte d’identité. Faute d’être en mesure de pouvoir présenter l’un de ces documents, des familles françaises ont été invitées à reprendre le premier avion pour la France. Le livret de famille <strong>ne peut en aucun cas</strong> se substituer à l’un de ces deux documents.</p>
<p>Pour une mission d’une durée inférieure à trois mois aucune démarche particulière n’est à effectuer. L’entrée en Turquie pour les ressortissants français se fait avec un passeport ou une carte nationale d’identité en cours de validité.</p>
<p><strong>Séjour de plus de trois mois</strong> :</p>
<p>La durée du titre de séjour (kimlik ou ikamet) ne peut dépasser la date de validité du passeport ; un contrôle régulier de la date d’expiration du passeport pourra permettre son renouvellement anticipé (deux mois avant la date de fin) et n’aura pas de répercussion dans la procédure de demande de renouvellement d’un titre de séjour sur le sol turc.</p>
<p><strong>Autorisation de travail</strong> : l’employeur doit en faire la demande, après recrutement. L’autorisation de travail peut permettre d’obtenir un permis de séjour temporaire auprès de la Police des étrangers. Toutefois, ce permis temporaire ne sera pas prolongé par les autorités à sa date d’expiration sans visa de travail.</p>
<p>L’autorisation de travail obtenue, il convient de faire personnellement une demande de <strong>visa de travail</strong> (calisma vizesi) auprès du consulat de Turquie le plus proche de votre domicile en France.</p>
<p>A l’entrée en Turquie, le cachet d’entrée doit figurer sur le passeport qui comporte le visa.</p>
<p>Pour la délivrance du <strong>permis de séjour </strong>pour raison de travail, il convient de se présenter à la Police des étrangers (Istanbul Emniyet Müdürlügü)</p>
<p>Une taxe est perçue pour chaque renouvellement de permis de séjour. Les délais d’obtention d’un permis de travail sont relativement longs. Dès lors que la personne venant travailler est en règle, il n’y a aucun problème particulier pour le conjoint, il doit toutefois faire les démarches pour obtenir le permis de séjour (Ikamet). Le concubinage et la PACS ne sont pas officiellement reconnus par les autorités turques mais les partenaires pacsés de Français résidents bénéficient (depuis janvier 2008) de facilités d’obtention d’une carte de séjour.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://paris.cg.mfa.gov.tr/" class="spip_out" rel="external">Section consulaire de l’ambassade de Turquie à Paris</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/entree-et-sejour/article/passeport-visa-permis-de-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
