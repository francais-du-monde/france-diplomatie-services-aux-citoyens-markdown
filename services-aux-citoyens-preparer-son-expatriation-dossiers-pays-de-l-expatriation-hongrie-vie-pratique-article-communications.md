# Communications

<h4 class="spip">Téléphone – Internet</h4>
<p>Les liaisons téléphoniques avec la France sont bonnes, mais relativement chères et au niveau des abonnements à internet les prix sont proches de ceux pratiqués en France. En outre, comme en France, il existe des possibilités d’abonnement regroupant plusieurs types de services.</p>
<p>L’indicatif téléphonique du pays est le 36. Les numéros de téléphone fixe de Budapest comportent 7 chiffres, précédés du 1 pour tout appel en dehors de la capitale. Pour la province il existe des indicatifs à deux chiffres, qui correspondent à 48 territoires et les numéros de téléphones sont composés de six chiffres.</p>
<p>Pour effectuer un appel vers l’étranger : 00 (ou le signe + à partir d’un portable) + indicatif du pays + numéro Attention pour un appel vers la France depuis la Hongrie, il convient de supprimer le zéro de l’indicatif des grandes zones téléphoniques françaises, comme dans l’exemple suivant pour Paris : 00+33+1…).</p>
<p>Les numéros de portable en Hongrie comportent 11 chiffres : 06 + les deux chiffres de l’indicatif de l’opérateur + les 7 chiffres du numéro de la personne. Les trois principaux opérateurs sont T-Mobile (indicatif : 30), Telenor (indicatif : 20) et Vodafone (indicatif : 70).</p>
<p>La majorité des téléphones publics fonctionnent avec des télécartes ou des pièces. Les cartes téléphoniques "telefon kartya" peuvent être achetées dans les bureaux de poste, les bureaux de tabac, les agences de voyage et chez les marchands de journaux. Il est possible également d’acheter des cartes téléphoniques prépayées, comme la carte "NeoPhone", à des tarifs avantageux.</p>
<p>Les clients peuvent recevoir un message automatique les informant du coût des appels, des SMS et des services de transmission de données à l’étranger (en cas de navigation sur internet ou de téléchargement d’un film, par exemple). Les prix de gros que les opérateurs se facturent mutuellement sont également plafonnés, ce qui devrait réduire les coûts supportés par les consommateurs. Un mécanisme a également été mis en place afin d’éviter aux consommateurs de devoir payer des factures astronomiques pour des services d’itinérance : après un premier avertissement, la connexion internet mobile est suspendue dès que le montant atteint un certain niveau. Une limite de 50 euros par mois s’applique par défaut si le client n’a pas défini un montant spécifique.</p>
<p>Vous trouverez sur le <a href="http://ec.europa.eu/information_society/activities/roaming/index_en.htm" class="spip_out" rel="external">site de l’UE</a>, en anglais, les eurotarifs appliqués par les opérateurs des 28 pays membres, ainsi que les liens vers leurs sites. Vous pourrez également y comparer les tarifs pratiqués pour l’envoi de SMS ou l’utilisation de services de données.</p>
<p><i>Source : <a href="http://europa.eu/travel/comm/index_fr.htm#phone" class="spip_out" rel="external">Europa.eu</a></i></p>
<p><strong>Téléphoner gratuitement par Internet</strong></p>
<p>Il est généralement très avantageux d’utiliser <strong>des logiciels de téléphonie sur IP</strong> (Skype, Google talk, Live messenger, Yahoo ! messenger, etc.) car les tarifs proposés à l’international sont souvent moindre que ceux des opérateurs locaux exploitant des lignes classiques, voire gratuits dans certaines conditions.</p>
<h4 class="spip">Poste</h4>
<p>Les liaisons postales sont quotidiennes et le délai d’acheminement varie de deux à sept jours.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.posta.hu/" class="spip_out" rel="external">Site de la poste hongroise</a></p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/vie-pratique/article/communications). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
