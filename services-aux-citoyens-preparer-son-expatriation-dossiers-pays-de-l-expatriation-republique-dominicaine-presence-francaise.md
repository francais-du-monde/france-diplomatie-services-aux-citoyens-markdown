# Présence française

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/presence-francaise/#sommaire_1">Ambassade et consulat de France</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/presence-francaise/#sommaire_2">Autorités françaises dans le pays</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/presence-francaise/#sommaire_3">Associations dans le pays</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Ambassade et consulat de France</h3>
<p>Consulter l’<a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/article/annuaire-des-representations-francaises-a-l-etranger" class="spip_in">annuaire des ambassades et consulats de France à l’étranger</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Autorités françaises dans le pays</h3>
<h4 class="spip">Réseau français de coopération et d’action culturelle</h4>
<p>Le service de coopération et d’action culturelle (SCAC) développe une coopération culturelle, linguistique et universitaire en liaison avec le Lycée français de Saint-Domingue et des quatre Alliances françaises situées dans les villes de Saint-Domingue et Santiago de los Caballeros, Mao et Montecristi.</p>
<p>Les coordonnées des services du réseau de coopération et d’action culturelle français en Afrique du Sud ainsi que la description de l’activité de ces services se trouvent sur site internet de l’Ambassade ou sur le site internet du réseau culturel : <a href="http://latitudefrance.diplomatie.gouv.fr/AnnuaireCarto.html" class="spip_out" rel="external">Latitude France</a>.</p>
<h4 class="spip">Economie</h4>
<p>Business France, l’Agence française pour le développement international des entreprises, accompagne les entreprises françaises dans leur développement en <a href="http://export.businessfrance.fr/republique-dominicaine/export-republique-dominicaine-avec-nos-bureaux.html" class="spip_out" rel="external">République Dominicaine</a>.</p>
<p>Les services économiques sont également présents en République Dominicaine. Ils sont une émanation de la Direction générale du Trésor et ont pour missions principales l’analyse macroéconomique et l’animation des relations économiques bilatérales entre la France et les pays où ils sont implantés.</p>
<p>Le service économique de Saint-Domingue a une compétence régionale sur la République dominicaine et Haïti.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Site internet du <a href="http://www.tresor.economie.gouv.fr/Pays/republique-dominicaine" class="spip_out" rel="external">service économique français en République Dominicaine</a></p>
<h4 class="spip">Vos élus à l’Assemblée des Français de l’étranger</h4>
<p>Pour toute information sur l’Assemblée des Français de l’étranger et pour connaître les conseillers et les Sénateurs qui représentent les Français établis hors de France dans votre circonscription, vous pouvez consulter les sites Internet suivants :</p>
<ul class="spip">
<li><a href="http://www.assemblee-afe.fr/" class="spip_out" rel="external">Assemblée des Français de l’étranger (AFE)</a></li>
<li>Sénat au service des Français de l’étranger : <a href="http://www.senat.fr/expatries/dossiers_pays/republique_dominicaine.html" class="spip_out" rel="external">dossier spécifique à la République Dominicaine sur le site du Sénat</a></li></ul>
<h4 class="spip">Députés des Français de l’étranger</h4>
<p>La révision constitutionnelle du 23 juillet 2008, publiée au Journal officiel du 19 avril 2011, introduit la représentation à l’Assemblée nationale des Français établis hors de France. La création de onze circonscriptions législatives à l’étranger permet aux Français expatriés d’élire leur député à l’Assemblée nationale à partir du scrutin de 2012.</p>
<p>Pour plus d’information : <a href="http://www.assemblee-nationale.fr/14/tribun/fiches_id/610860.asp" class="spip_out" rel="external">Assemblee nationale</a></p>
<h3 class="spip"><a id="sommaire_3"></a>Associations dans le pays</h3>
<p><strong>Union des Français de l’étranger (UFE)</strong><br class="manualbr">Apartado postal #998<br class="manualbr">SANTO DOMINGO<br class="manualbr">Tel. : 1 809 909 4295 <br class="manualbr">Fax : 1 809 221 5933</p>
<p><a href="http://www.saintdomingueaccueil.org" class="spip_out" rel="external">Saint-Domingue Accueil (SDA) Francophone</a><br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/presence-francaise/#stdoac#mc#gmail.com#" title="stdoac..åt..gmail.com" onclick="location.href=mc_lancerlien('stdoac','gmail.com'); return false;" class="spip_mail">Courriel</a></p>
<p><a href="http://www.ccifranco-dominicana.org/" class="spip_out" rel="external">Chambre de commerce dominico-française</a> <br class="manualbr">Avenida Abraham Lincoln # 456 - Plaza Lincoln - 2do Piso -Local 44<br class="manualbr">La Julia Apartado Postal 1442 - Santo Domingo<br class="manualbr">Téléphone : 809 472 05 00 / 04 07 <br class="manualbr">Télécopie : 809 472 04 66<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/presence-francaise/#admin#mc#ccdfdo.org#" title="admin..åt..ccdfdo.org" onclick="location.href=mc_lancerlien('admin','ccdfdo.org'); return false;" class="spip_mail">Courriel</a></p>
<p>Pour en savoir plus :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Notre article dédié aux <a href="services-aux-citoyens-preparer-son-expatriation-en-savoir-plus-article-associations-des-francais-de-l-etranger.md" class="spip_in">Associations des Français de l’étranger</a>.</p>
<p><i>Mise à jour : septembre 2015</i></p>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/presence-francaise/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
