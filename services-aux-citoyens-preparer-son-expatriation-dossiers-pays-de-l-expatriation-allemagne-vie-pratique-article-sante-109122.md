# Santé

<p>L’état sanitaire de l’Allemagne, ainsi que son infrastructure médicale, sont comparables à ceux de la France.</p>
<p>Le coût des consultations est fixe et rigoureusement tarifé, il <strong>peut varier de 1 à 3,5 fois</strong> en fonction du type d’assurance sociale du patient. Il est de 25€ chez un médecin généraliste, 45€ chez un spécialiste.</p>
<p>Les consultations ont lieu en général de 8 à 12h et de 16 à 18 h, sauf le mercredi, samedi et dimanche.</p>
<p><strong>Numéros utiles </strong></p>
<ul class="spip">
<li>Urgences médicales : 112</li>
<li>Accidents de la circulation : 110</li></ul>
<p><strong>Praticiens francophones</strong></p>
<p>Des listes de médecins francophones sont disponibles sur le site de <a href="http://www.ambafrance-de.org/LISTE-DE-MEDECINS-AVOCATS-ET" class="spip_out" rel="external">l’ambassade de France à Berlin</a></p>
<p><strong>A savoir</strong></p>
<p>Avec l’arrivée du printemps débute chaque année l’activité des <strong>tiques</strong>. Extrêmement répandus <strong>en Bavière</strong>, ces petits acariens, localisés sur la végétation basse (sous-bois, fourrés, hautes herbes…) peuvent être porteurs d’agents pathogènes (borrelia, virus) et transmettre, en cas de morsure sur l’homme, la maladie de Lyme (borreliose) ou la méningo-encéphalite de printemps.</p>
<p>S’il n’existe à l’heure actuelle aucun vaccin contre la maladie de Lyme, présente partout en Bavière, les signes dermatologiques à court terme (érythème d’abord localisé autour de la morsure, puis migrant) et les complications nerveuses (névrites), articulaires (arthrites) et cardiaques à long terme disparaissent généralement sans séquelles sous antibiothérapie.</p>
<p>Il n’en est pas de même pour la méningo-encéphalite de printemps. Beaucoup plus rare, et relativement localisée (région du Bayerischer Wald près de la frontière tchèque, plaine danubienne, vallées de l’Isar, de la Salzach, de la Vils et de l’Inn ; à Munich même le risque est statistiquement quasi nul), cette pathologie peut cependant exposer les moins de 15 ans et les plus de 60 ans (système immunitaire plus faible) à un pronostic grave. La "Société bavaroise pour la médecine tropicale et immunitaire" recommande la vaccination. Un vaccin efficace (mais uniquement contre le risque de méningite) est disponible en République fédérale d’Allemagne, en Autriche et en France (centres anti-amariles). Il est bien toléré dans la majorité des cas, mais présente néanmoins des effets secondaires chez les personnes sensibles. La vaccination, qui doit donc se faire au cas par cas, s’adresse principalement aux gens vivant en permanence à la campagne ou à ceux qui y séjournent pour une longue durée (campeurs, randonneurs, pêcheurs, etc.).</p>
<p>Il faut savoir que toute tique n’est pas porteuse de germes, que toute morsure n’est pas contaminatrice et que toute contamination n’implique pas un pronostic grave. Des paramètres comme la température, le degré d’humidité, l’habitat de ces insectes, l’état immunitaire du "mordu" jouent un rôle pour le pronostic.</p>
<p>Pour en savoir plus :</p>
<p>Rubrique <a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/allemagne/" class="spip_in">Conseils aux voyageurs</a>.</p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/vie-pratique/article/sante-109122). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
