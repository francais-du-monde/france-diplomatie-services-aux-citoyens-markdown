# Communications

<h4 class="spip">Téléphone – Internet</h4>
<p>Les liaisons téléphoniques sont généralement bonnes entre la France et Madagascar.</p>
<p>On trouve des <a href="http://www.cybercafe.com/" class="spip_out" rel="external">cybercafés</a> dans la capitale. Les liaisons internet, bien qu’en amélioration, restent aléatoires en raison de la lenteur des transferts de données et de la saturation des serveurs locaux. Le coût des abonnements est très élevé.</p>
<p>Pour en savoir plus sur les téléphones fixes/portables et internet :</p>
<ul class="spip">
<li>site de <a href="http://www.tana-accueil.com/" class="spip_out" rel="external">Tana accueil</a></li>
<li><a href="http://www.tv5.org/TV5Site/voyageurs/accueil.php" class="spip_out" rel="external">TV5.org</a></li></ul>
<p><strong>Téléphoner gratuitement par Internet </strong></p>
<p>Il est généralement très avantageux d’utiliser <strong>des logiciels de téléphonie sur IP</strong> (Skype, Google talk, Live messenger, Yahoo ! messenger, etc.) car les tarifs proposés à l’international sont souvent moindre que ceux des opérateurs locaux exploitant des lignes classiques, voire gratuits dans certaines conditions.</p>
<h4 class="spip">Poste</h4>
<p>L’acheminement du courrier requiert au minimum une semaine, avec de bonnes garanties de réception en ce qui concerne le trafic intérieur. La poste se montre en revanche peu fiable pour l’international, notamment concernant les colis, en particulier lorsque la mention du contenu indique des cassettes vidéo ou DVD.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/vie-pratique/article/communications). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
