# Fiscalité du pays

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/fiscalite/article/fiscalite-du-pays#sommaire_1">Généralités</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/fiscalite/article/fiscalite-du-pays#sommaire_2">Impôt sur le revenu des personnes physiques</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/fiscalite/article/fiscalite-du-pays#sommaire_3">Modalités d’imposition des plus-values des personnes physiques</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/fiscalite/article/fiscalite-du-pays#sommaire_4">Barème de l’impôt sur le revenu des personnes physiques</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/fiscalite/article/fiscalite-du-pays#sommaire_5"> Quitus fiscal </a></li></ul>
<p><i>Ces informations n’ont qu’une valeur indicative et ne se substituent pas à la documentation officielle de l’administration fiscale américaine.</i></p>
<h3 class="spip"><a id="sommaire_1"></a>Généralités</h3>
<p>Les Etats-Unis étant un Etat fédéral, les compétences fiscales sont réparties entre la Fédération et les Etats fédérés. Il est donc important de se conformer aux dispositions juridiques en vigueur, à savoir celles régies par le système fédéral et celles relevant de l’Etat, auxquelles s’ajoutent parfois des réglementations particulières de la compétence des autorités locales (comtés et communes).</p>
<p>La législation fiscale au niveau fédéral est codifiée dans l’<i>Internal Revenue Code</i> (IRC).</p>
<p>Les données suivantes ne sont qu’une indication des principales dispositions prévues au niveau fédéral et ne sauraient se substituer à l’information fournie par les autorités américaines. Vous êtes donc invité à vous reporter aux sources officielles et à compléter ces informations par la consultation de la réglementation fiscale locale de votre Etat de résidence.</p>
<h3 class="spip"><a id="sommaire_2"></a>Impôt sur le revenu des personnes physiques</h3>
<p>Aux Etats-Unis, l’impôt sur le revenu est autoliquidé, c’est-à-dire qu’il est calculé directement par le contribuable.</p>
<p>L’impôt est payé soit directement à la source (<i>Withholding tax</i>), soit par l’intermédiaire d’acomptes provisionnels (<i>Estimated tax</i>).</p>
<p>Un contribuable peut être soumis à une pénalité si le montant cumulé de l’impôt payé par voie de retenue à la source et/ou d’acomptes provisionnels n’est pas égal au plus faible des deux montants suivants :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  90% de l’impôt sur le revenu de l’année ;</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  ou, 100% de l’impôt sur le revenu dû au titre de l’année précédente.</p>
<h4 class="spip">Le paiement par voie de retenue à la source</h4>
<p>Ces retenues concernent certains types de revenus :</p>
<p><strong>Les revenus soumis obligatoirement à une retenue à la source</strong></p>
<p>Les salaires et tous les paiements annexes versés, en espèces ou en nature, par un employeur : (salaires, pourboires, intéressement au chiffre d’affaires, indemnités de licenciement, y compris les indemnités provenant de l’assurance maladie ou accident sont en principe soumis à une retenue à la source. Toutefois, des exceptions existent. Les principales sont : les remboursements de frais, les salaires versés aux militaires en poste dans certaines zones de combat, les salaires servis par des gouvernements étrangers, les organisations internationales, les salaires résultant d’une activité exercée hors des Etats-Unis.</p>
<p>Les retenues à la source de l’impôt sur le revenu fédéral sont prélevées et reversées par l’employeur en même temps que les cotisations au régime général de sécurité sociale et au régime d’assurance chômage. Elles sont décomptées selon les rythmes de paiement retenus habituellement par l’employeur.</p>
<p>Le versement de pensions ou rentes (fonds de pensions versées par l’employeur, comptes épargne retraite, contrats d’assurance-vie,…) donne lieu également à une retenue à la source effectuée par le payeur à moins que le contribuable y renonce expressément.</p>
<p><strong>Les revenus soumis à une retenue à la source dans certains cas</strong></p>
<p>Les entreprises et notamment les banques qui versent certains types de revenus (intérêts, dividendes, paiements à un tiers dans le cadre d’une relation d’affaires,…) doivent en principe informer l’administration fiscale américaine de l’identité du bénéficiaire. Ces sommes ne sont généralement pas soumises à une retenue à la source sauf si le bénéficiaire refuse de communiquer au payeur certaines des informations requises par la réglementation fiscale (ex : numéro d’identification). Dans ce cas de figure, le payeur doit procéder à une retenue à la source de sauvegarde (<i>backup withholding</i>) lorsque les sommes versées au bénéficiaire récalcitrant sont supérieures ou égales à 600 dollars.</p>
<p><strong>Les revenus volontairement soumis à retenue à la source</strong></p>
<p>Les contribuables peuvent opter pour l’application de la retenue à la source sur certains paiements effectués par l’Etat fédéral ou ses agences.</p>
<p>Sont notamment visés les allocations versées par la sécurité sociale, les indemnités de chômage, les paiements compensateurs de pertes de récoltes et autres paiements spécifiques prévus par le Secrétaire du <a href="http://www.treasury.gov/Pages/default.aspx" class="spip_out" rel="external">Département du Trésor</a>. Dans ce cas, le contribuable choisit le taux de retenue qu’il veut se voir appliquer (7%, 10%, 15% ou 25%).</p>
<h4 class="spip">Le paiement d’acomptes provisionnels</h4>
<p>Pour les catégories de revenus qui n’entrent pas dans le champ d’application de la retenue à la source, la loi prévoit que les contribuables doivent régler la majeure partie de leur impôt fédéral sur les revenus de l’année en cours, par le versement, chaque trimestre, d’un montant d’impôt estimé (<i>Estimated Tax</i>).</p>
<p>Les textes comprennent de nombreuses spécificités. Cependant, sont globalement soumis aux paiements d’acomptes provisionnels tous les revenus des personnes physiques qui n’ont pas supporté la retenue à la source (revenus d’un entrepreneur individuel, intérêts, dividendes, plus-values, loyers,…).</p>
<p>Les paiements sont à effectuer pour les 15 avril, 15 juin, 15 septembre de l’année en cours et le 15 janvier de l’année suivante. Ce dernier paiement n’est pas exigé si le contribuable dépose sa déclaration des revenus de l’année écoulée, accompagnée du solde dû, avant le 31 janvier.</p>
<h4 class="spip">La déclaration</h4>
<p>Dans tous les cas, qu’il s’agisse de retenue à la source ou du paiement d’acomptes provisionnels, le contribuable est soumis à l’obligation de déclarer les revenus effectivement perçus au cours de l’exercice fiscal précédent. Cette formalité permet notamment à l’administration de corriger les écarts pouvant apparaître entre la provision effectuée et le solde définitif.</p>
<p>Le dépôt de la déclaration intervient en règle générale entre le 31 janvier et le 15 avril, avec des possibilités de report sous certaines conditions.</p>
<p>Selon le cas, le formulaire prend la forme d’une déclaration générale d’impôt sur le revenu à laquelle peuvent s’ajouter certaines annexes en fonction de la nature des revenus du contribuable :</p>
<ul class="spip">
<li>forme détaillée (déclaration 1040) : elle est souscrite en particulier par les titulaires de revenus provenant d’activités indépendantes et les associés ou partenaires de sociétés transparentes ;</li>
<li>forme semi-détaillée : déclaration 1040A ;</li>
<li>forme simplifiée (déclaration 1040 EZ) : elle est réservée aux personnes célibataires ou mariées déposant des déclarations conjointes, de moins de 65 ans, sans personne à charge et dont les revenus imposables n’excédant pas 100 000 dollars sont constitués uniquement de salaires et intérêts de placements n’excédant pas 1500 dollars. Cette déclaration ne permet pas d’opter pour la déduction des frais réels ;</li>
<li>les annexes à joindre à la déclaration représentent une quinzaine de <i>schedules</i> permettant de déterminer le montant imposable des revenus catégoriels : revenus fonciers, redevances, gains et pertes en capital, revenus de valeurs mobilières, résultat d’activités professionnelles, ainsi que les frais déductibles.</li></ul>
<h4 class="spip">Crédits d’impôt</h4>
<p>Certaines situations donnent droit au bénéfice de crédits d’impôt qui viennent en déduction du montant de l’impôt brut. Les contribuables qui exercent une activité professionnelle indépendante peuvent bénéficier par ailleurs des crédits d’impôt accordés aux sociétés.</p>
<p>Les principaux crédits d’impôt pour charge de famille sont les suivants :</p>
<p><strong>Crédit pour garde d’enfants ou pour personnes à charge (<i>Child and Dependent Care Credit</i>)</strong></p>
<p>Cette mesure s’applique à la garde d’enfants de moins de 13 ans ou à la prise en charge de personnes souffrant d’un handicap physique ou mental (y compris le conjoint handicapé).</p>
<p>Le taux de crédit est proportionnel au niveau de revenu du contribuable et s’applique sur le montant des dépenses supportées. Il varie de 20 à 35% sans que le crédit d’impôt puisse excéder 1050 dollars pour une personne à charge et 2100 dollars pour deux personnes à charge ou plus.</p>
<p><strong>Crédit pour enfants à charge (<i>Child Tax Credit</i>)</strong></p>
<p>Ce crédit concerne les enfants âgés de moins de 17 ans à la fin de l’année d’imposition.</p>
<p>Son montant est fixé de façon forfaitaire et s’élève à 1000 dollars. Il diminue à partir d’un certain seuil de revenu.</p>
<p><strong>Crédits pour études (<i>Credits for Higher Education Tuition</i>)</strong></p>
<p>Ces crédits couvrent des dépenses de scolarité ou de formation.</p>
<p>Depuis l’imposition des revenus de l’année 2009 et jusqu’à l’imposition des revenus de l’année 2017, le contribuable peut bénéficier d’un nouveau dispositif de crédit d’impôt baptisé le <i>American Opportunity Credit</i>. Ce crédit d’impôt est calculé sur les dépenses engagées pendant les quatre premières années d’études supérieures du contribuable, de son épouse ou de ses enfants à charge (100% des 2000 premiers dollars, puis 25% des 2000 dollars suivants). Son montant maximum est de 2500 dollars par étudiant éligible et par an.</p>
<p>A partir de l’imposition des revenus de l’année 2018, un autre dispositif de crédit d’impôt baptisé le <i>Hope Scholarship Credit</i> viendra remplacer le <i>Americain Opportunity Credit</i>. Ce crédit d’impôt est calculé sur les dépenses engagées pendant les deux premières années d’études supérieures du contribuable, de son épouse ou de ses enfants à charge (100% des 1000 premiers dollars, puis 50% des 1000 dollars suivants). Son montant maximum est de 1500 dollars par étudiant éligible et par an. Le <i>Lifetime Learning Credit</i> est égal à 20% des 10000 premiers dollars engagés pour une année donnée pour financer les études supérieures du contribuable, de son épouse ou de ses enfants à charge. Le montant maximum est de 2000 dollars. Ce dispositif de crédit d’impôt ne peut pas se cumuler avec le <i>American Opportunity Credit</i> (ou le <i>Hope Scholarship Credit</i>) et à la différence des autres dispositifs existant n’est pas limité à un nombre d’année.</p>
<h3 class="spip"><a id="sommaire_3"></a>Modalités d’imposition des plus-values des personnes physiques</h3>
<p>Le traitement fiscal des plus et moins-values est fondé sur la durée de détention des biens (plus ou moins 12 mois).</p>
<p>A court terme, une compensation est effectuée entre moins-values et plus-values pour déterminer le sort fiscal du solde dégagé :</p>
<ul class="spip">
<li>si la compensation fait apparaître une plus-value nette, celle-ci est intégrée au revenu imposable et soumise au barème progressif de l’impôt sur le revenu ;</li>
<li>si la compensation fait apparaître une moins-value nette, celle-ci vient s’imputer sur les plus-values à long terme au titre des revenus imposables de l’année de réalisation. Si un excédent demeure après cette opération, il est déductible des revenus de l’année dans la limite de 3000 dollars, le solde demeurant reportable sur les exercices fiscaux futurs. Les plus-values à long terme font quant à elles l’objet d’une imposition distincte au titre de l’année de leur réalisation et sont imposées à un taux proportionnel : 0% pour les contribuables dont les revenus restent dans les limites des tranches d’imposition de 10 et 15%, 15 % pour ceux dont les revenus restent dans les limites des tranches d’imposition de 25, 28, 33 et 25% et enfin 20% pour ceux dont les revenus sont imposés dans la tranche supérieure de 39,6 %.</li></ul>
<p>La plus-value réalisée lors de la cession d’une résidence principale (résidence occupée en tant que telle pendant deux ans dans les cinq ans précédant la vente) bénéficie d’un abattement à la base de 250 000 dollars pour une personne célibataire ou de 500 000 dollars pour un couple. Cet abattement ne peut être utilisé à raison de plus d’une vente tous les deux ans.</p>
<h3 class="spip"><a id="sommaire_4"></a>Barème de l’impôt sur le revenu des personnes physiques</h3>
<p>Avant le calcul de l’impôt brut, deux types d’abattement peuvent être pratiqués :</p>
<ul class="spip">
<li>un abattement personnel (<i>Personal Exemption</i>) dont le montant est actualisé chaque année en fonction de l’inflation (3 950 dollars pour 2014). Cet abattement s’applique pour chaque personne à charge, ainsi que pour le conjoint dans le cas d’une déclaration de revenus conjointe ;</li>
<li>une déduction de base (<i>Standard Deduction</i>) qui n’est obtenue qu’une fois par déclaration, quel que soit le nombre de personnes à charge. Cette déduction s’élève pour 2014 à 12 400 dollars pour un couple marié, à 8950 dollars pour un chef de famille et à 6200 dollars pour une personne célibataire ou mariée souscrivant une déclaration distincte. Le revenu imposable après abattement est soumis au barème progressif. Le barème de l’impôt varie selon la situation de famille et les choix opérés par les contribuables. Il comprend sept tranches d’imposition de 0% à 39,6% (taux applicables à l’imposition des revenus de 2014).</li></ul>
<table class="spip" summary="">
<caption><strong>Barème de l’impôt 2014 pour un couple déposant une déclaration conjointe</strong></caption>
<tbody>
<tr class="row_odd odd">
<td><strong>Montant du revenu</strong></td>
<td>%</td></tr>
<tr class="row_even even">
<td>Supérieur à 0 dollars et égal ou inférieur à 18 150 dollars</td>
<td>10 %</td></tr>
<tr class="row_odd odd">
<td>Supérieur à 18 150 dollars et égal ou inférieur à 73 800 dollars</td>
<td>15 %</td></tr>
<tr class="row_even even">
<td>Supérieur à 73 800 dollars et égal ou inférieur à 148 850 dollars</td>
<td>25 %</td></tr>
<tr class="row_odd odd">
<td>Supérieur à 148 850 dollars et égal ou inférieur à 226 850 dollars</td>
<td>28 %</td></tr>
<tr class="row_even even">
<td>Supérieur à 226 850 dollars et égal ou inférieur à 405 100 dollars</td>
<td>33 %</td></tr>
<tr class="row_odd odd">
<td>Supérieur à 405 100 dollars et égal ou inférieur à 457 600 dollar</td>
<td>35%</td></tr>
<tr class="row_even even">
<td>Supérieur à 457 600 dollars</td>
<td>39,6 %</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_5"></a> Quitus fiscal </h3>
<p>La réglementation fiscale prévoit, qu’avant de quitter les Etats-Unis, les étrangers (<i>resident aliens</i>) doivent demander un quitus fiscal (<i>sailing permit</i>) afin de justifier que leur situation est régulière au regard de l’impôt. Il existe des exceptions à cette obligation notamment en faveur des diplomates ou, si certaines conditions sont remplies, des étudiants.</p>
<p>Ce quitus doit être demandé entre 30 et 15 jours avant la date prévue du départ. Pour l’obtenir, il est nécessaire que le contribuable souscrive une déclaration provisoire (1040-C ou 2063 si le contribuable n’a eu aucun revenu pour l’année en cours et la précédente) et se rende au bureau local des impôts dont il dépend.</p>
<p>La souscription de la déclaration provisoire (1040-C) doit normalement être accompagnée du paiement de l’impôt correspondant.</p>
<p>Ceci ne dispense néanmoins pas le contribuable de devoir souscrire l’année suivant celle de son départ (année N+1) une déclaration définitive reprenant tous ses revenus de l’année N (année au cours de laquelle il a définitivement quitté les Etats-Unis). C’est alors l’imprimé concernant les étrangers non-résidents (1040-NR) qui doit être utilisé, accompagné du paiement de l’éventuel solde d’impôt correspondant.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Site internet de l’<a href="http://www.irs.gov/" class="spip_out" rel="external">administration fiscale américaine</a> (<i>Internal Revenue Service</i>) :</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/fiscalite/article/fiscalite-du-pays). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
