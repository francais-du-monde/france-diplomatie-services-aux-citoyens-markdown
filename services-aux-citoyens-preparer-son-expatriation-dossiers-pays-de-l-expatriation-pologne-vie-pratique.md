# Vie pratique

<h2 class="rub23536">Logement</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/vie-pratique/#sommaire_1">Où se loger ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/vie-pratique/#sommaire_2">Conditions de location</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/vie-pratique/#sommaire_3">Hôtels</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/vie-pratique/#sommaire_4">Electricité</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/vie-pratique/#sommaire_5">Electroménager</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Où se loger ?</h3>
<p><strong>Cracovie</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Loyer moyen mensuel</strong></td>
<td>PLN</td>
<td>euros</td></tr>
<tr class="row_even even">
<td>Studio vide</td>
<td>2000 à 2665</td>
<td>500 à 666</td></tr>
<tr class="row_odd odd">
<td>Appartement (3 pièces, vide)</td>
<td>2665 à 4670</td>
<td>666 à 1167</td></tr>
<tr class="row_even even">
<td>Appartement ou villa (5 pièces, vide)</td>
<td>5000 à 13 400</td>
<td>1250 à 3350</td></tr>
</tbody>
</table>
<p><strong>Varsovie</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Loyer moyen mensuel</strong></td>
<td>PLN</td>
<td>euros</td></tr>
<tr class="row_even even">
<td>Studio vide</td>
<td>2400 à 3200</td>
<td>600 à 800</td></tr>
<tr class="row_odd odd">
<td>Appartement (3 pièces, vide)</td>
<td>3200 à 5600</td>
<td>800 à 1400</td></tr>
<tr class="row_even even">
<td>Appartement ou Villa (5 pièces, vide)</td>
<td>6000 à 16 000</td>
<td>1500 à  4000</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_2"></a>Conditions de location</h3>
<p>La recherche d’un logement passe par une agence immobilière qui s’occupe également des formalités administratives (contrat de location). La commission de l’agence représente un mois de loyer. Elle peut également s’effectuer par "le bouche à oreille" ou les annonces dans les quotidiens.</p>
<p>Il n’y a pas de règle générale concernant la durée du contrat de location : il peut être signé pour une durée de 1 mois à cinq ans. Le loyer est payable un mois à l’avance, mais il arrive que l’on donne trois mois, voire un an de loyer ; les modalités sont définies dans le contrat. Un état des lieux est conseillé.</p>
<p>Les quartiers résidentiels de Varsovie sont ceux de Saska Kepa à l’Est (où se trouve le lycée français), Zoliborz au Nord et de Mokotow au Sud. Le délai de recherche va de deux à six semaines.</p>
<p>A Cracovie, ils se situent en dehors du centre : Wola Justowska, Kliny. Il est plus aisé de trouver un logement meublé que vide. Dans le cas d’un logement vide, le propriétaire se réserve bien souvent une pièce pour y entreposer ses meubles. Le délai moyen de recherche est d’un mois environ.</p>
<p>Les loyers sont fréquemment payables en dollars ou en euros.</p>
<p>Il convient de remarquer que les dépenses afférentes aux consommations d’eau sont comprises dans le montant du loyer, tandis que l’électricité et le gaz sont en sus et varient fréquemment à la hausse. Le chauffage s’effectue généralement au gaz et il n’est pas besoin de climatisation en été.</p>
<p>Pour trouver rapidement un logement en Pologne, contacter des agences immobilières.</p>
<p><strong>Agences immobilières</strong></p>
<p><i>Cette liste n’est pas exhaustive</i></p>
<p><strong>Agencja Gajewski</strong><br class="manualbr">ul. Pulawska 54/56/1 <br class="manualbr">02-529 VARSOVIE <br class="manualbr">Tél/fax : +48 22 646 03 56 ou 646 03 60 <br class="manualbr">Tél + 48 601 346 975 ou 602 171 070 (parle français)</p>
<p><strong>Azur Polska</strong><br class="manualbr">Al. Jana Pawla II nr 15 <br class="manualbr">00-818 VARSOVIE <br class="manualbr">Tél. : +48 22 890 82 30 <br class="manualbr">Portable : +48 602 46 82 82 (parle français) <br class="manualbr">Fax : +48 22 624 93 18</p>
<p><strong>Immopol Property</strong><br class="manualbr">ul. Bartycka 24/3 <br class="manualbr">00-716 VARSOVIE <br class="manualbr">Tél/fax : +48 22 651 04 75 <br class="manualbr">Tél : +48 22 651 05 73 (parle français)</p>
<p><strong>Move One Relocations</strong><br class="manualbr">ul. Koszykowa 54 <br class="manualbr">00-675 VARSOVIE <br class="manualbr">Tél : 22 630 81 69 <br class="manualbr">Fax : 22 630 81 66 <br class="manualbr">Portable : +48 509 711 217</p>
<p><strong>Perfect Properties</strong><br class="manualbr">ul. Senatorska 12 <br class="manualbr">00-082 VARSOVIE <br class="manualbr">Tél : +48 22 829 57 76 <br class="manualbr">Portable : +48 668 136 146 <br class="manualbr">Fax : +48 22 829 57 68</p>
<p><strong>WGN Real-Estate</strong><br class="manualbr">ul. Zurawia 20 <br class="manualbr">00-515 VARSOVIE <br class="manualbr">Tél. : +48 22 622 44 40 <br class="manualbr">Portable : +48 667 620 251 (parle français) <br class="manualbr">Fax : +48 22 629 90 99</p>
<h3 class="spip"><a id="sommaire_3"></a>Hôtels</h3>
<p><strong>Cracovie</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Prix moyen d’une chambre d’hôtel (chambre double)</strong></td>
<td>PLN</td>
<td>euros</td></tr>
<tr class="row_even even">
<td>Grand tourisme</td>
<td>716</td>
<td>179</td></tr>
<tr class="row_odd odd">
<td>Moyen tourisme</td>
<td>488</td>
<td>122</td></tr>
</tbody>
</table>
<p><strong>Varsovie</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Prix moyen d’une chambre d’hôtel (chambre double)</strong></td>
<td>PLN</td>
<td>euros</td></tr>
<tr class="row_even even">
<td>Grand tourisme</td>
<td>800</td>
<td>200</td></tr>
<tr class="row_odd odd">
<td>Moyen tourisme</td>
<td>540</td>
<td>135</td></tr>
</tbody>
</table>
<h4 class="spip">Auberges de jeunesse </h4>
<p>Il est possible de loger chez l’habitant ou en auberge de jeunesse. Les auberges de jeunesse polonaises (schroniska mlodziezowe) sont gérées par la Polskie Towarzystwo Schronisk Mlodziezowych (PTSM) membre de la <a href="http://www.fuaj.org/" class="spip_out" rel="external">Fédération Unie des Auberges de Jeunesse (FUAJ)</a>.</p>
<p>A peu près 125 auberges fonctionnent toute l’année et 450 ont une activité saisonnière en Pologne.</p>
<h3 class="spip"><a id="sommaire_4"></a>Electricité</h3>
<p>Le courant électrique est alternatif, 220 volts, 50 hz. Les prises de courant sont les mêmes qu’en France.</p>
<h3 class="spip"><a id="sommaire_5"></a>Electroménager</h3>
<p>Toutes les grandes marques sont représentées dans tous les domaines (petit et gros électroménager) : Philips, Whirlpool, Siemens, Moulinex, Electrolux, etc. Prix équivalents aux prix pratiqués en France.</p>
<p>Les cuisines sont souvent équipées. On y trouve le plus fréquemment un fourneau, un réfrigérateur et un lave-linge.</p>
<p>Le chauffage, central ou individuel, est au gaz ou au charbon.</p>
<p><i>Mise à jour : juin 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-pologne-vie-pratique-article-loisirs-et-culture.md" title="Loisirs et culture">Loisirs et culture</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-pologne-vie-pratique-article-communications.md" title="Communications">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-pologne-vie-pratique-article-transports.md" title="Transports">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-pologne-vie-pratique-article-cout-de-la-vie-114294.md" title="Coût de la vie">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-pologne-vie-pratique-article-scolarisation.md" title="Scolarisation">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-pologne-vie-pratique-article-sante.md" title="Santé">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-pologne-vie-pratique-article-logement-114291.md" title="Logement">Logement</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/vie-pratique/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
