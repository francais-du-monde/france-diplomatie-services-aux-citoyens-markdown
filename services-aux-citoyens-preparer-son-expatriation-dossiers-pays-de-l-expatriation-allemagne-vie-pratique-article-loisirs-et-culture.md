# Loisirs et culture

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/vie-pratique/article/loisirs-et-culture#sommaire_1">Tourisme</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/vie-pratique/article/loisirs-et-culture#sommaire_2">Activités culturelles</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/vie-pratique/article/loisirs-et-culture#sommaire_3">Sports</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/vie-pratique/article/loisirs-et-culture#sommaire_4">Télévision - Radio</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/vie-pratique/article/loisirs-et-culture#sommaire_5">Presse française</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Tourisme</h3>
<p>L’Allemagne est un pays touristique doté de nombreux sites et parcs naturels, musées, stations thermales, châteaux et cathédrales. Il existe, par ailleurs, une grande variété de « routes touristiques » (Route des châteaux, Route du vin, Route romantique, Route enchantée…)</p>
<p>Pour toute information, vous pouvez vous adresser à :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.germany.travel/fr/index.html" class="spip_out" rel="external">Office national allemand du tourisme</a> (ONAT)<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/vie-pratique/article/loisirs-et-culture#office-france#mc#germany.travel#" title="office-france..åt..germany.travel" onclick="location.href=mc_lancerlien('office-france','germany.travel'); return false;" class="spip_mail">Courriel</a> </p>
<h3 class="spip"><a id="sommaire_2"></a>Activités culturelles</h3>
<h4 class="spip">Activités culturelles en français</h4>
<p>Concernant le cinéma, d’une façon générale, les films français sont présentés dans les salles en version allemande, quelquefois en version originale, sous-titrée en allemand, comme au cinéma "Paris" et au ciné-club "l’Arsenal" à Berlin. La cinémathèque de Düsseldorf consacre des rétrospectives à des réalisateurs français. Les films sont alors projetés en version originale sous-titrée. Il en est de même pour le "Kommunales Kino" de Stuttgart.</p>
<p>Quelques représentations théâtrales en langue française sont organisées en liaison avec les instituts français. A Francfort existe un théâtre international depuis 1993. A Sarrebruck, chaque année au mois de mai, se déroule le festival de théâtre "Perspectives", désormais franco-allemand.</p>
<p>Des expositions d’œuvres d’artistes français ont lieu, en moyenne trois à cinq fois par an, dans chaque grande ville allemande, sans compter les manifestations d’origine privée.</p>
<p>Il n’y a pas d’Alliance française en Allemagne. En revanche, les instituts français, nombreux et actifs, organisent expositions, conférences, spectacles, concerts, projections… La plupart disposent de bibliothèques et de salles de lecture.</p>
<p>Les sites des instituts français et centres culturels en Allemagne sont accessibles à partir du site de l’<a href="http://www.institut-francais.fr/" class="spip_out" rel="external">Institut français</a>.</p>
<h4 class="spip">Activités culturelles locales</h4>
<p>Pour l’amateur de films en langue allemande, la vidéo (système PAL) est assez répandue.</p>
<p>Les salles de cinéma abondent. La sortie des films s’effectue de façon quasi-simultanée avec la France, le jeudi. La seule ville de Francfort dispose de 80 salles. D’innombrables représentations théâtrales se déroulent durant dix mois de l’année. La relâche a généralement lieu en juillet et en août.</p>
<p>Plus de 3000 musées d’une extrême variété, et renfermant pour nombre d’entre eux d’inestimables collections de peinture et de sculpture, se disputent les faveurs du public allemand et étranger.</p>
<p>Dans le domaine musical, opéras, salles de concerts, festivals, manifestations locales ou événements de portée internationale organisés dans les cadres les plus variés - sans être exclusivement réservés à la période estivale - sont à même de combler les attentes les plus diverses.</p>
<h3 class="spip"><a id="sommaire_3"></a>Sports</h3>
<p>Tous les sports peuvent être pratiqués en Allemagne. Pour pratiquer le sport de son choix, il faut de préférence être membre d’un des nombreux clubs sportifs, même si leurs cotisations sont souvent élevées.</p>
<p>En matière de chasse et de pêche, chaque land a établi sa propre législation qui vient compléter la législation fédérale.</p>
<p>A ce dernier titre, il est nécessaire de contracter une assurance et d’obtenir l’autorisation du propriétaire des terres sur lesquelles on se propose de chasser. Le permis de chasse est obligatoire. Pour une durée inférieure à quinze jours, il convient de s’adresser à "l’Ordnungsamt" territorialement compétent et de produire un permis de chasse français accompagné de sa traduction en allemand ainsi qu’une attestation d’assurance spécifique contractée auprès d’une compagnie allemande. Au-delà, le résident étranger, même titulaire du permis de chasse de son pays d’origine doit passer l’examen du permis de chasse allemand qui se prépare en sept mois. Une carte de détention d’arme (Waffenbesitzkarte) est exigée. Cette dernière est obtenue auprès des services municipaux sur présentation du permis de chasse. Si l’arme est importée, après accord préalable, les services douaniers accordent au propriétaire un délai d’un mois pour la présentation de sa carte de détention d’arme. Les périodes d’ouverture de la chasse sont variables en fonction du gibier et du land.</p>
<p>Pour la pêche, en raison des mesures visant la protection de l’environnement et de la présence d’un fort mouvement écologiste, la pratique n’est possible que sur quelques rares plans d’eau ou rivières. Un permis journalier peut alors être obtenu auprès de la mairie du lieu.</p>
<h3 class="spip"><a id="sommaire_4"></a>Télévision - Radio</h3>
<p>La télévision allemande compte deux chaînes hertziennes publiques, auxquelles s’ajoutent les chaînes régionales. Selon les villes, une quinzaine de chaînes supplémentaires est disponible via le câble ou le satellite. L’offre de programmes de télévision et de radio est variée et de bonne qualité. La qualité des programmes est comparable à celle des programmes diffusés en France.</p>
<p>Le système adopté est le système PAL. Pour la lecture des DVD, l’Allemagne se trouve, comme la France, en zone 2.</p>
<p>Certains câblo-opérateurs régionaux diffusent des programmes français ou francophones, notamment <a href="http://www.tv5monde.com/" class="spip_out" rel="external">TV5MONDE</a>. <a href="http://www.arte.tv/fr" class="spip_out" rel="external">Arte</a> n’est disponible que sur le câble ou le satellite.</p>
<p>Les possibilités de recevoir les émissions des stations de radios dépendent de la situation géographique et de l’appareil récepteur. <a href="http://www.rfi.fr/" class="spip_out" rel="external">Radio France Internationale</a> (RFI) est reçue, en modulation de fréquence, dans quelques villes d’Allemagne ou sur bouquets satellitaires.</p>
<p><a href="http://www.franceinter.fr/" class="spip_out" rel="external">France Inter</a> et les radios périphériques peuvent être captées sur ondes longues sur la plus grande partie du territoire.</p>
<h3 class="spip"><a id="sommaire_5"></a>Presse française</h3>
<p>La presse française est disponible, tout particulièrement dans les gares et les aéroports, mais aussi au hasard des kiosques de rue.</p>
<p><i>Mise à jour : juillet 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/vie-pratique/article/loisirs-et-culture). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
