# Santé

<h4 class="spip">Médecine de soins</h4>
<p><strong>Médecins</strong></p>
<p>La Suisse fait référence en matière de santé. Toutes les spécialités médicales sont représentées et tous les médicaments et traitements nécessaires sont disponibles.</p>
<p>Le pays ne connait pas de pénurie de médecins généralistes à l’exception des régions dépeuplées où le nombre de médecins est beaucoup plus limité. La tradition du médecin de famille est très ancrée au sein de la population.</p>
<p>Un service de consultations médicales sur place et à domicile est assuré par SOS médecins à Genève et dans tout le canton de Genève. Il opère toute l’année, 7jours/7 et 24h/24. Quant au reste du pays, les habitants sont toujours informés grâce à des circulaires administratives ou par leur médecin de famille, des médecins de garde qu’ils peuvent appeler en dehors des horaires de consultations. <strong>Le numéro 144 (ambulance)</strong> permet également d’entrer rapidement en contact avec un médecin. Les visites à domicile sont également développées. Leur but est d’éviter les séjours hospitaliers ambulatoires et/ou les transports.</p>
<p>La fourchette des coûts de consultation est assez large mais ceux-ci demeurent élevés. Il est également possible de bénéficier de consultations téléphoniques payantes. Pour les dentistes, le coût n’est pas limité. L’importance des frais engendrés par les soins de santé en Suisse conduit les Français à maintenir leur protection sociale en France, dans la mesure du possible. Néanmoins, on ne pourra se passer d’une bonne garantie en cas d’urgence ou d’éloignement de la frontière, qui contraindrait à utiliser les structures locales.</p>
<p>Bien que le système de santé suisse fonctionne bien, il est, au vu des coûts qu’il entraîne, au cœur de nombreux débats et polémiques.</p>
<p><strong>Hôpitaux</strong></p>
<p>Les soins de santé sont dispensés au sein d’établissements de soins publics (établissements publics tels que les hôpitaux de ville ou de région), semi-privés ou privés. Les structures hospitalières publiques sont de bon niveau et communément utilisées pour tout type de pathologie. Néanmoins, les cliniques privées sont nombreuses en raison du niveau de vie élevé. La différence entre établissements publics et privés n’est pas établie selon des critères de qualité de soins mais plutôt selon les conditions plus ou moins luxueuses d’accueil et d’hébergement.</p>
<p>La liste suivante est donnée à titre indicatif :</p>
<ul class="spip">
<li>Hôpital Inselspital - Berne : 031 632 21 11</li>
<li>Hôpital Cantonal - Fribourg : 026 426 71 11</li>
<li>Kantonsspital Basel - Bâle ville : 061 265 40 30</li>
<li>Hôpital des Cadolles - Neuchâtel : 032 722 92 29</li>
<li>Hôpital Pourtales - Neuchâtel : 032 727 11 11</li>
<li>Liestal Kantonsspital - Bâle campagne : 061 925 20 91</li>
<li>Olten Kantonsspital - Soleure : 062 206 41 11</li>
<li>Kantonsspital - Lucerne : 041 205 11 11</li>
<li>Delémont hôpital régional - Jura : 032 421 21 21</li>
<li>Universitätsspital - Zurich : 044 255 11 11</li>
<li>Hôpital Cantonal de Genève : 022 372 33 11</li>
<li>Kantonsspital - Saint Gall : 071 494 14 40</li>
<li>Hôpital civil - Lugano : 091 805 61 11</li>
<li>Hôpital La Carita - Locarno : 091 756 71 11</li>
<li>Spital Oberengadin - Samedan (St. Moritz) : 081 851 81 11</li>
<li>Centre hospitalier universitaire vaudois - Lausanne : 021 314 11 11</li></ul>
<p>Il est vivement conseillé de consulter son médecin traitant avant le départ et de souscrire une assurance rapatriement.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.vd.ch/" class="spip_out" rel="external">Réseau Santé vaudois</a></li>
<li><a href="http://www.rsv-gnw.ch/" class="spip_out" rel="external">Réseau Santé du Valais</a></li>
<li><a href="http://www.bag.admin.ch/" class="spip_out" rel="external">Office fédéral de la santé publique suisse</a></li>
<li><a href="http://www.les-medecins.ch/" class="spip_out" rel="external">Annuaire de médecins et établissements médicaux</a></li>
<li><a href="http://www.doctorfmh.ch/" class="spip_out" rel="external">Registre officiel de médecins suisses</a> (accessible aux patients et professionnel de santé)</li></ul>
<p><strong>Carte européenne d’assurance maladie</strong></p>
<p><strong>La carte européenne d’assurance maladie</strong> (CEAM) atteste de vos droits à l’assurance maladie en Europe. Lors d’un séjour temporaire dans un Etat membre de l’Espace économique européen (EEE) ou en Suisse, elle vous permet de bénéficier de la prise en charge des soins médicalement nécessaires.</p>
<p>La CEAM remplace définitivement les formulaires E 111 et E 111 B (utilisés pour les touristes), ainsi que les formulaires E 110, E 119, E 128 utilisés jusqu’à présent pour les séjours temporaires en Europe.</p>
<p>La CEAM est <strong>valable pour un séjour temporaire </strong>(à l’occasion de vacances, d’un détachement professionnel, d’un stage, d’un séjour linguistique, par exemple).</p>
<p>Délivrée gratuitement dans un délai minimum de deux semaines à la demande de l’intéressé par les caisses d’assurance maladie, la CEAM se présente sous la forme d’une carte plastique non électronique distincte de la carte Vitale. Il s’agit d’une carte nominative et individuelle.</p>
<p><strong>Elle a une durée de validité maximale d’un an</strong></p>
<p>La carte européenne d’assurance maladie peut être présentée dans les Etats suivants :</p>
<p>Allemagne, Autriche, Belgique, Bulgarie, Chypre, Danemark, Espagne, Estonie, Finlande, Grèce, Hongrie, Irlande, Islande, Italie, Lettonie, Lituanie, Liechtenstein, Luxembourg, Malte, Norvège, Pays-Bas, Pologne, Portugal, République tchèque, Roumanie, Royaume-Uni, Slovaquie, Slovénie, Suède et Suisse.</p>
<p><strong>Pour en savoir plus</strong></p>
<ul class="spip">
<li><a href="http://ec.europa.eu/social/" class="spip_out" rel="external">Site de la Commission européenne</a> : rubrique Vivre et travailler à l’étranger / Les soins de santé à l’étranger / La carte européenne d’assurance maladie.</li>
<li><a href="http://www.cleiss.fr/" class="spip_out" rel="external">Centre des liaisons européennes et internationales de sécurité sociale</a> (CLEISS) : rubrique " Infos pratiques &gt; séjours temporaires ".</li>
<li><a href="http://www.ameli.fr/" class="spip_out" rel="external">Site Internet de l’Assurance maladie</a> en France : rubrique Assurés &gt; Droits et démarches &gt; A l’étranger.</li></ul>
<p><i>Mise à jour : février 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/vie-pratique/article/sante). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
