# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/paraguay/vie-pratique/article/transports#sommaire_1">Importation de véhicule</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/paraguay/vie-pratique/article/transports#sommaire_2">Permis de conduire</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/paraguay/vie-pratique/article/transports#sommaire_3">Assurances et taxes</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/paraguay/vie-pratique/article/transports#sommaire_4">Achat et location</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/paraguay/vie-pratique/article/transports#sommaire_5">Entretien</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/paraguay/vie-pratique/article/transports#sommaire_6">Réseau routier</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/paraguay/vie-pratique/article/transports#sommaire_7">Transports en commun</a></li></ul>
<p>Les vols de voiture deviennent très répandus et n’épargnent aucun quartier de la ville. En cas de vol ou d’incident, il convient de s’adresser au commissariat de police le plus proche.</p>
<p><strong>Police (numéros d’appel local)</strong></p>
<ul class="spip">
<li>Assomption : 911 - 130</li>
<li>Ciudad del Este : 911</li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Importation de véhicule</h3>
<p>Aucune norme technique n’est exigée à l’entrée du territoire, mis à part le positionnement du volant à la gauche du tableau de bord.</p>
<p>Il est préférable d’utiliser un véhicule tout-terrain compte tenu de l’état général des routes et des conditions climatiques (inondations fréquentes en saison des pluies). La climatisation est de rigueur.</p>
<p>L’importation d’un véhicule impose de passer par un agent de douane et est soumise aux formalités d’entrée :</p>
<ul class="spip">
<li>légalisation de la facture d’achat par l’ambassade du Paraguay à Paris ;</li>
<li>à l’importation, paiement des droits de douane au service d’<i>Hacienda</i> (le ministère des Finances) contre remise d’un "certificat de nationalisation" : environ 33 % de la valeur du véhicule établie par les douanes.</li>
<li>avec ce certificat et les documents d’importation, rédaction d’un titre de propriété par un notaire : environ 5 % du prix de la voiture neuve ;</li>
<li>inscription au registre national de l’immatriculation et obtention des plaques : environ 95 €</li></ul>
<p>L’ensemble de ces formalités suppose un délai de trois à cinq mois.</p>
<h3 class="spip"><a id="sommaire_2"></a>Permis de conduire</h3>
<p>Le permis français n’est pas reconnu. Le permis international n’est valable quant à lui que pour les séjours inférieurs à trois mois. Pour un séjour supérieur à trois mois, il est donc nécessaire de passer le permis paraguayen.</p>
<h3 class="spip"><a id="sommaire_3"></a>Assurances et taxes</h3>
<p>L’assurance du véhicule est obligatoire. Compte tenu des habitudes de conduite locales, qui se traduisent notamment par des infractions régulières au code de la route, il convient de souscrire une assurance tous risques.</p>
<h3 class="spip"><a id="sommaire_4"></a>Achat et location</h3>
<p>Toutes les marques sont représentées (Renault, Peugeot, Citroën et marques étrangères).</p>
<p>Les véhicules achetés localement sont tropicalisés et garantis pièces détachées comprises. Les taxes d’importation pour les véhicules non construits dans un pays du Mercosur sont très élevées (environ 33% du prix d’achat et le transport). Les prix sont en conséquence plus élevés qu’en France (environ +10%).</p>
<p>Il est possible d’acquérir un véhicule d’occasion auprès d’un particulier ou d’un garage. Cependant la prudence est de mise compte tenu du fait que de nombreux véhicules en circulation au Paraguay sont volés ou en situation irrégulière.</p>
<h3 class="spip"><a id="sommaire_5"></a>Entretien</h3>
<p>L’entretien d’un véhicule peut se faire sur place.</p>
<p>Le service est moyen. La main-d’œuvre est bon marché, mais les pièces détachées sont chères.</p>
<p>Pour les véhicules de marque française, il faut parfois un délai important pour l’approvisionnement des pièces.</p>
<h3 class="spip"><a id="sommaire_6"></a>Réseau routier</h3>
<p>Le réseau routier est peu entretenu ; il existe moins de 4000 km de routes asphaltées pour une superficie proche de celle de la France.</p>
<p>La circulation est assez chaotique et dangereuse : insuffisance de formation des automobilistes, non-respect du code de la route, absence de système de priorité et de sanctions.</p>
<p>D’une manière générale, les paraguayens sont peu ou mal assurés auprès des compagnies locales souvent défaillantes.</p>
<h3 class="spip"><a id="sommaire_7"></a>Transports en commun</h3>
<p><strong>Avion</strong></p>
<p>L’aéroport (Silvio Pettirossi) est situé à 30 minutes du centre de la capitale : la course en taxi coûte environ 100 000 PYG (soit 16 €).</p>
<p>Il existe des liaisons quotidiennes avec la France, via São Paulo ou Buenos Aires (de 16 à 19 heures de vol, escale comprise).</p>
<p>Il existe quelques liaisons intérieures ; de nombreux services d’avions privés sont disponibles.</p>
<p><strong>Train </strong></p>
<p>Il n’existe pas de réseau ferré au Paraguay.</p>
<p><strong>Autocar</strong></p>
<p>Le réseau est assez important à partir de la capitale ; il s’agit d’un moyen de transport bon marché mais peu confortable. Les meilleures compagnies sont « Nuestra Señora de la Asuncion » et la « Encarnacena » pour les déplacements en province.</p>
<p><strong>Taxis</strong></p>
<ul class="spip">
<li>Prise en charge : 5000 PYG (soit environ 0,82 €)</li>
<li>Tarif kilométrique : 9000 PYG (soit environ 0,98 €)</li></ul>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/paraguay/vie-pratique/article/transports). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
