# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/norvege/vie-pratique/article/cout-de-la-vie-111219#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/norvege/vie-pratique/article/cout-de-la-vie-111219#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/norvege/vie-pratique/article/cout-de-la-vie-111219#sommaire_3">Alimentation</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/norvege/vie-pratique/article/cout-de-la-vie-111219#sommaire_4">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>L’unité monétaire est la couronne norvégienne (NOK).</p>
<p>En janvier 2014, la couronne norvégienne vaut 0,119 euros, c’est-à-dire qu’un euro équivaut à 8,40 couronnes norvégiennes.</p>
<p>La plupart des cartes bancaires sont acceptées en Norvège dans les hôtels, les restaurants, les grands magasins, la majorité des commerces et des stations-services ; en revanche, ce n’est pas toujours le cas dans les supermarchés, le Vinmonopolet et les pharmacies qui n’acceptent que les cartes à débit immédiat.</p>
<p>Les retraits d’argent peuvent s’effectuer dans les banques, les bureaux de poste (il en existe un dans chaque ville), les distributeurs automatiques (important réseau ouvert 24h/24), les kiosques et supermarchés (moins coûteux que dans les distributeurs automatiques).</p>
<p>Attention aux horaires d’ouverture des banques : 9h-15h (15h30 en hiver) du lundi au vendredi.</p>
<p>Si plusieurs grandes banques françaises sont présentes dans le pays (BNP, Crédit Lyonnais, Société Générale), elles ne gèrent cependant pas de comptes de particuliers.</p>
<p>L’ouverture d’un compte bancaire en Norvège est conditionnée par la possession d’un numéro statistique personnel (<i>personnummer</i>).</p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<p>Les transferts de fonds sont libres. Il n’existe pas de contrôle des changes.</p>
<h3 class="spip"><a id="sommaire_3"></a>Alimentation</h3>
<p>Les prix varient selon la situation des magasins à Oslo. Les produits agricoles sont largement subventionnés ; le niveau des prix de distribution devient donc artificiel tandis que les produits importés, s’ils sont de qualité, restent chers car taxés. Les norvégiens qui le peuvent font leur marché en Suède où les magasins proches de la frontière sont ouverts le dimanche. Beaucoup profitent des soldes saisonnières (Tilbud). L’importation de viande non conditionnée reste interdite pour des raisons sanitaires.</p>
<p><strong>Prix moyen d’un repas dans un restaurant</strong></p>
<p>Prix moyen d’un déjeuner pour deux personnes : 980 NOK (121€)</p>
<p>Compter environ 100 NOK (12€) pour un menu fast food ; 60NOK (7,20€) pour une bière ; 40 NOK (4,80€) pour un café expresso.</p>
<h3 class="spip"><a id="sommaire_4"></a>Evolution des prix</h3>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/norvege/vie-pratique/article/cout-de-la-vie-111219). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
