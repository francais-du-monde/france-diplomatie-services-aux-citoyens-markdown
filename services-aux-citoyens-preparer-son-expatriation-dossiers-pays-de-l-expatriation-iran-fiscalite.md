# Fiscalité

<h2 class="rub22995">Fiscalité du pays</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/fiscalite/#sommaire_1">Année fiscale </a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/fiscalite/#sommaire_2">Quitus fiscal </a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/fiscalite/#sommaire_3">Inscription auprès du fisc</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/fiscalite/#sommaire_4">Impôts directs et indirects</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/fiscalite/#sommaire_5">Impôts locaux</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/fiscalite/#sommaire_6">Droits et taxes sur le commerce international</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Année fiscale </h3>
<p>L’année fiscale en Iran commence le 21 mars et prend fin le 20 mars de l’année suivante.</p>
<h3 class="spip"><a id="sommaire_2"></a>Quitus fiscal </h3>
<p>Un quitus fiscal est exigé à chaque départ d’Iran, ainsi que pour le renouvellement du permis de travail des résidents étrangers.</p>
<h3 class="spip"><a id="sommaire_3"></a>Inscription auprès du fisc</h3>
<p>Les impôts sur le revenu sont prélevés à la source. Pour les expatriés il appartient à l’employeur de déclarer les salaires aux autorités fiscales et au ministère du travail, et de payer les impôts prélevés.</p>
<h3 class="spip"><a id="sommaire_4"></a>Impôts directs et indirects</h3>
<p>Le droit iranien (loi sur les impôts directs adoptée en 1988) prévoit des taxes sur les héritages (avec un certain nombre d’exemptions), les revenus immobiliers, les salaires et avantages en nature ; les salariés du secteur public et ceux du secteur privé sont traités différemment. Les bénéfices des professions libérales et des commerçants sont soumis à l’impôt selon le même barème que les salariés du secteur privé, qui va de 15 à 35% ; une partie du revenu, fixée par les autorités en fonction du revenu minimum, est d’office exemptée d’impôts. Les taxes religieuses sont versées sur une base volontaire seulement, et peuvent ouvrir droit à des déductions d’impôts.</p>
<p>Les salaires du secteur public sont en principe soumis à un impôt de 10%. Dans le secteur privé le taux de l’impôt sur le revenu, prélevé à la source par l’employeur, évolue entre 15% et un taux marginal de 35% (pour les revenus supérieurs à un milliard de rials par an soit 30 000 € au taux officiel fin 2013). L’administration fiscale iranienne définit par ailleurs des revenus minimaux pour les expatriés, selon leur fonction et leur origine, et établit l’impôt sur cette base, plutôt que sur le revenu réel.</p>
<p>Les retraites, pensions et indemnités de fin de fonctions ainsi que la prime annuelle du Nouvel an persan sont exemptées d’impôt sur le revenu.</p>
<p>Il existe par ailleurs des exemptions pour les revenus des personnes physiques issue de leur activité dans les zones franches, dans l’agriculture. D’autres exemptions concernent la taxation des entreprises dans certains secteurs économiques et dans certaines régions. La taxation est différente pour les industries qui s’établissent en centre-ville ou à l’extérieur.</p>
<h3 class="spip"><a id="sommaire_5"></a>Impôts locaux</h3>
<p>Une taxe foncière est payée par les propriétaires (personnes physiques ou personnes morales) aux municipalités.</p>
<h3 class="spip"><a id="sommaire_6"></a>Droits et taxes sur le commerce international</h3>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://en.intamedia.ir/pages/default.aspx" class="spip_out" rel="external">Administration fiscale iranienne</a></p>
<p><i>Mise à jour : février 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-iran-fiscalite-article-convention-fiscale.md" title="Convention fiscale">Convention fiscale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-iran-fiscalite-article-fiscalite-du-pays-111125.md" title="Fiscalité du pays">Fiscalité du pays</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/fiscalite/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
