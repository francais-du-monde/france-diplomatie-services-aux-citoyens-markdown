# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/emploi-stage/article/reglementation-du-travail#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/emploi-stage/article/reglementation-du-travail#sommaire_2">Contrat de travail – spécificités</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/emploi-stage/article/reglementation-du-travail#sommaire_3">Fêtes légales</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/emploi-stage/article/reglementation-du-travail#sommaire_4">Emploi du conjoint</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/emploi-stage/article/reglementation-du-travail#sommaire_5">Création d’entreprises – spécificités</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<p>Le temps de travail est légalement fixé à 40 heures par semaine et huit heures par jour avec un repos hebdomadaire le samedi et le dimanche. La durée maximum de travail est de 48 heures par semaine. Les heures supplémentaires sont majorées de 50% les jours normaux et de 100% les jours fériés.</p>
<p>Le nombre des congés payés est de 21 jours pour les personnes ayant moins de 15 ans d’activité et de 25 jours pour les autres. 11 jours fériés payés sont prévus dans l’année.</p>
<p>La durée de la période d’essai est de 15 jours (90 pour les postes de direction. La durée du préavis en cas de rupture de contrat est de 15 jours (30 jours pour les postes de direction).</p>
<p>Au 1er janvier 2013, le salaire minimum mensuel brut en Roumanie est de 157,50 euros. Les charges sociales correspondent à 35,5% du salaire pour la part patronale et à 17% du salaire pour la part salariale.</p>
<p>Les congés de maternité sont de 126 jours. Ils peuvent aller jusqu’à deux ans avec 85% du salaire.</p>
<p>L’âge légal du départ à la retraite et de 57 ans pour les femmes et de 62 ans pour les hommes.</p>
<h3 class="spip"><a id="sommaire_2"></a>Contrat de travail – spécificités</h3>
<p>Le contrat à durée déterminée ne peut pas être conclu pour une période supérieure à 36 mois.</p>
<p>Les droits salariaux sont proportionnels au temps de travail effectif.</p>
<p>Pour plus d’informations, voir le site du <a href="http://www.mmuncii.ro/" class="spip_out" rel="external">ministère du Travail, de la Famille, de la Protection sociale et des Personnes âgées</a>.</p>
<h3 class="spip"><a id="sommaire_3"></a>Fêtes légales</h3>
<ul class="spip">
<li>1er et 2 janvier (Jour de l’An)</li>
<li>1er et 2ème jour de la Pâques orthodoxe</li>
<li>1er mai (Fête du travail)</li>
<li>50ème et 51ème jours après la Pâques orthodoxe (fête religieuse, Rusalii)</li>
<li>15 août (fête religieuse)</li>
<li>30 novembre (fête religieuse)</li>
<li>1er décembre (Fête nationale)</li>
<li>25 et 26 décembre (Noël)</li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Emploi du conjoint</h3>
<p>Pour pouvoir travailler en Roumanie, le conjoint doit obtenir une carte de séjour spécifique et un permis de travail. Les possibilités d’emploi sont variables selon le type et le niveau de qualification. Elles existent cependant dans de nombreux domaines à condition de maîtriser le roumain et/ou l’anglais.</p>
<h3 class="spip"><a id="sommaire_5"></a>Création d’entreprises – spécificités</h3>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://export.businessfrance.fr/roumanie/export-roumanie-avec-notre-bureau.html" class="spip_out" rel="external">Mission Business France en Roumanie</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/emploi-stage/article/reglementation-du-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
