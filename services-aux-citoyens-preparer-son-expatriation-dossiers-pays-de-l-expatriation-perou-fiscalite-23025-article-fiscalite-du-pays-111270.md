# Fiscalité du pays

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/fiscalite-23025/article/fiscalite-du-pays-111270#sommaire_1">Généralité</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/fiscalite-23025/article/fiscalite-du-pays-111270#sommaire_2">Les divers impôts</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/fiscalite-23025/article/fiscalite-du-pays-111270#sommaire_3">Solde du compte en fin de séjour </a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/fiscalite-23025/article/fiscalite-du-pays-111270#sommaire_4">Coordonnées de centres d’information fiscale </a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Généralité</h3>
<p>Au Pérou, la Superintendance nationale des douanes et de l’administration fiscale (SUNAT) est l’autorité compétente dans le domaine fiscal ainsi qu’en matière douanière. Elle a entrepris de moderniser le système fiscal en réduisant le nombre d’impôts et en simplifiant les formalités.</p>
<p>Si la pression fiscale progresse régulièrement, elle demeure encore à un faible niveau, à 16% du PIB en 2012, malgré les mesures mises en œuvre pour lutter contre l’évasion fiscale, en particulier par l’intermédiaire de l’ITF (impôt sur les transactions financières). L’objectif du gouvernement et de la SUNAT est d’atteindre une pression fiscale de 18% d’ici 2016.</p>
<p>Les principaux impôts péruviens sont : l’impôt sur le revenu (IR), l’impôt général sur les ventes (IGV), l’impôt sélectif à la consommation (ISC), l’impôt sur les transactions financières (ITF), et l’impôt temporaire sur les actifs nets (ITAN).</p>
<p>L’unité d’imposition fiscale (UIT) est un montant de référence créé pour maintenir en valeur constante les seuils d’imposition, d’exonération, d’éligibilité ainsi que les amendes. La valeur de l’UIT est déterminée chaque année par une résolution du ministère de l’Economie et des Finances. Elle a été fixée à 3800 soles (environ 1380 USD) en décembre 2013 pour l’année 2014 (+100 soles par rapport à 2013).</p>
<p>La période de l’année fiscale est l’année calendaire. Le dépôt des déclarations de revenus s’effectue en mars de chaque année. Les formulaires sont fournis par la SUNAT.</p>
<h3 class="spip"><a id="sommaire_2"></a>Les divers impôts</h3>
<h4 class="spip">Impôt sur le revenu</h4>
<p>L’impôt sur le revenu est un impôt direct annuel grevant les revenus du capital, du travail et de l’exercice de toute activité économique des contribuables résidents au Pérou, qu’ils soient personnes physiques ou morales. Il concerne également les revenus, de source péruvienne, des contribuables non-résidents au Pérou.</p>
<p>Pour les entreprises (IR de <strong>troisième catégorie</strong>), cet impôt s’applique à n’importe quel gain ou bénéfice d’opérations réalisées avec des tiers, ainsi qu’au résultat obtenu à la clôture de l’exercice comptable.</p>
<p>Le taux s’appliquant aux personnes morales, aux associations en participation et aux co-entreprises sur leurs revenus de la <strong>troisième catégorie</strong> est de 30% du revenu net. Les dividendes distribués sont taxés au taux de 4,1%. Le taux s’appliquant aux personnes physiques est de 15% du montant total de leurs revenus jusqu’à 27 UIT, de 21% jusqu’à 54 UIT, et de 30% au-delà.</p>
<h4 class="spip">L’impôt général sur les ventes (IGV)</h4>
<p>L’impôt général sur les ventes (IGV) équivalent de la TVA, est un impôt indirect prélevé sur la vente de biens meubles, la prestation ou l’utilisation de services, les contrats de construction, la primo vente d’immeubles réalisés par les constructeurs eux-mêmes et l’importation de biens. Le taux de l’IGV a été ramené à 16% en 2011, mais il existe de nombreuses exonérations, notamment pour favoriser les exportations et le développement régional, de l’Amazonie en particulier. A ce taux s’ajoute 2% au titre de l’impôt pour le développement municipal. L’IGV total réellement payé s’élève donc en réalité à 18%.</p>
<h4 class="spip">L’impôt sélectif à la consommation (ISC)</h4>
<p>L’impôt sélectif à la consommation est une taxe sur des produits de consommation ciblés, nationaux ou importés, qui ne sont pas considérés comme de première nécessité (alcool, tabac, bijoux, parfums) ou mauvais pour l’environnement (carburant).Ces derniers sont généralement imposés ad-valorem : l’impôt est calculé sur la base de la valeur de vente du produit ou de la valeur en douane dans le cas des importations. C’est le cas pour les automobiles (10%), et les boissons non alcoolisées (17%). En 2013, le gouvernement a décidé d’augmenter l’ISC sur les boissons alcoolisées (vins, spiritueux et bières). Les boissons ayant un dosage d’alcool entre 0 et 6 degrés sont désormais assujetties à une taxe fixe de 1,35 sol/litre ou à un montant équivalent à 30% du prix de vente au public ; celles ayant un dosage supérieur à 6° sont taxées à 25% de leur prix de vente au public ou par un montant fixe (2,50 soles/litre pour les boissons entre 6 et 20° et 3,40 soles/litre pour celles dont le dosage est supérieur à 20°). L’impôt payé est le montant le plus important entre ces deux manières de calculer (montant fixe ou taxe ad valorem).Enfin, certains produits sont encore imposés sur les volumes produits ou importés, en particulier le tabac et les carburants.</p>
<h4 class="spip">L’Impôt temporaire sur les actifs nets (ITAN)</h4>
<p>L’ITAN s’applique sur la valeur nette des actifs de l’entreprise au 31 décembre de l’année précédente à un taux de 0,4% à partir d’un million de <i>nuevos soles</i> péruviens.</p>
<h4 class="spip">Les impôts municipaux</h4>
<p>Il existe de nombreux impôts municipaux comme l’impôt sur les véhicules, l’impôt foncier, ou l’impôt sur les transferts de biens meubles.</p>
<p><i>Source : l’essentiel d’un marché Pérou et Bolivie 2010-2011 - Ubifrance</i></p>
<h3 class="spip"><a id="sommaire_3"></a>Solde du compte en fin de séjour </h3>
<p>Un expatrié français relevant du secteur privé peut solder son compte en fin de séjour sous réserve d’être en règle avec l’administration fiscale.</p>
<h3 class="spip"><a id="sommaire_4"></a>Coordonnées de centres d’information fiscale </h3>
<p>Le site de la <a href="http://www.sunat.gob.pe/" class="spip_out" rel="external">Superintendance nationale des douanes et de l’administration fiscale</a> fournit une information détaillée sur la fiscalité péruvienne.</p>
<p>Tel : <strong>0-801-12-100 ou 315-0730</strong> (pour Lima)</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><strong>Service économique de Lime – Pérou</strong><br class="manualbr">Adresse : Calle Los Nogales 326 San Isidro - Lima<br class="manualbr">Tél. : + 511 203 6290 – Télécopie : + 511 203 6300</li>
<li><a href="http://www.ccipf.com/" class="spip_out" rel="external">Chambre de commerce franco-péruvienne</a><br class="manualbr">Adresse : Calle Los Nogales 326 San Isidro - Lima<br class="manualbr">Tél. : +511421 40 50 ; Fax : (+51 1) 421 90 93</li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/fiscalite-23025/article/fiscalite-du-pays-111270). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
