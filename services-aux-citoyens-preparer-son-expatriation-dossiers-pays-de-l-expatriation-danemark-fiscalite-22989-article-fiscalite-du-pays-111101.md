# Fiscalité du pays

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/fiscalite-22989/article/fiscalite-du-pays-111101#sommaire_1">Présentation</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/fiscalite-22989/article/fiscalite-du-pays-111101#sommaire_2">Année fiscale</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/fiscalite-22989/article/fiscalite-du-pays-111101#sommaire_3">Barème de l’impôt</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/fiscalite-22989/article/fiscalite-du-pays-111101#sommaire_4">Quitus fiscal </a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/fiscalite-22989/article/fiscalite-du-pays-111101#sommaire_5">Solde du compte en fin de séjour</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/fiscalite-22989/article/fiscalite-du-pays-111101#sommaire_6">Coordonnées des centres d’information fiscale</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Présentation</h3>
<p>Au Danemark, le montant total des impôts et taxes collectés représente environ 50% du PIB. L’Etat supporte en contrepartie les dépenses liées à l’éducation, les allocations de chômage, les pensions de retraite et de vieillesse, les prestations de santé, ainsi que d’autres services comme la garde des enfants.</p>
<h4 class="spip">Impôt sur le revenu des personnes physiques</h4>
<p>L’impôt sur le revenu des personnes physiques est la plus importante source de recettes fiscales de l’Etat (près de la moitié en volume). Les autres impôts perçus sont : la taxe sur la valeur ajoutée (TVA), les droits d’accise, l’impôt sur les sociétés et la contribution salariale.</p>
<p>L’année d’imposition correspond à l’année calendaire. L’impôt sur le revenu est <strong>retenu à la source</strong> par l’employeur qui le reverse à Told-ogSkat (la Direction des Douanes et des Impôts danoise). Cette dernière renvoie chaque année au contribuable un relevé mentionnant toutes les informations relatives à son imposition (aarsopgorelse), qui lui ont été directement transmises par l’employeur, les banques, les organismes sociaux, etc. En règle générale, cette procédure est donc automatique et ne suppose aucune démarche pour le contribuable. Toutefois, en cas de rectificatifs à apporter à l’imposition, il appartient à ce dernier de les signaler à l’administration fiscale.</p>
<p>Chaque citoyen de plus de 18 ans bénéficie d’un abattement personnel de DKK 42 900 par an (2014) avant calcul de son impôt sur le revenu. De plus, diverses déductions telles que les cotisations de retraite versées, les intérêts payés, les frais de transport jusqu’au lieu de travail et les cotisations syndicales font varier le montant de l’impôt exigible.</p>
<h4 class="spip">Impôts sur les sociétés</h4>
<p>Le Danemark compte plus de 70 000 sociétés. L’impôt sur les sociétés est géré conjointement par l’administration centrale des Douanes et des Impôts et par les centres régionaux des douanes et des impôts.</p>
<h4 class="spip">Taxation des capitaux et impôt foncier</h4>
<p>L’impôt foncier est perçu par les municipalités et les départements sur tous les types de biens immobiliers. Les valeurs foncières sont taxées à un taux fixé chaque année par les municipalités et les départements. De plus, des charges sont perçues sur les bâtiments affectés à des activités commerciales en zone urbaine et à l’administration.</p>
<p>C’est Told-og Skat qui assure l’évaluation des biens, les valeurs foncières sont aujourd’hui réévaluées sur une base annuelle.</p>
<h4 class="spip">Contributions indirectes (accises)</h4>
<p>Une taxe d’accise est perçue sur les véhicules à moteur, l’énergie, les alcools et le tabac. Dans les années 90, une nouvelle forme de taxe d’accise a été mise en place. Cette taxe d’accise "verte" est prélevée sur la consommation de biens polluants ou de ressources dites rares (eau, pétrole, essence, électricité, etc.)</p>
<h3 class="spip"><a id="sommaire_2"></a>Année fiscale</h3>
<p>L’année fiscale pour les particuliers s’étend du 1er janvier au 31 décembre. Pour les sociétés, il convient de consulter le site Internet du <a href="http://www.skat.dk/" class="spip_out" rel="external">Skat</a> : <i>Tax calendar for businesses</i>.</p>
<h3 class="spip"><a id="sommaire_3"></a>Barème de l’impôt</h3>
<h4 class="spip">Impôt sur le revenu des personnes physiques</h4>
<p>L’impôt sur le revenu perçu par l’Etat est calculé selon un barème progressif (chiffres pour 2014).</p>
<p>Le taux d’imposition de base (bundskat) est égal à 4,64%. Il est calculé sur le revenu imposable après déduction de l’abattement personnel. Si le revenu du contribuable est inférieur à DKK 100 000, celui-ci ne verse que l’impôt de base.</p>
<p>Le taux d’imposition supérieur (topskat) est égal à 15%. Il est calculé sur le revenu personnel majoré des revenus positifs nets des capitaux. L’impôt est calculé sur la portion des revenus qui dépasse l’abattement de base de 421000 DKK. La fraction de l’abattement ne pouvant être pleinement utilisée, peut être transmise au conjoint du contribuable.</p>
<p>Les impôts locaux et départementaux varient selon le département et la municipalité de résidence. En 2014, le taux d’imposition local et départemental global se situe à 25,15%.</p>
<p>Une contribution salariale est calculée sur tous les revenus des personnes physiques, sous forme de pourcentage du revenu avant déductions. Cette contribution s’élève à 8%.</p>
<p>Divers impôts et contributions mineurs viennent s’ajouter aux impôts ci-dessus, parmi lesquels la contribution au régime d’assurance vieillesse complémentaire des salariés danois (ATP) et l’impôt versé par les membres de l’église luthérienne danoise (l’Eglise nationale danoise) : 0,97 % en moyenne</p>
<p>En raison du plafonnement de l’imposition, l’impôt sur le revenu versé à l’Etat, au département et aux collectivités locales <strong>ne peut excéder 51,50% du revenu</strong>.</p>
<h5 class="spip">Le cas particulier des expatriés</h5>
<p>En 1992, le Danemark a introduit un régime dérogatoire de taxation du personnel expatrié. Le taux de l’impôt sur le revenu était alors de 30%. Mais en 1994, une modification sensible a été apportée à ce système. En effet, ce taux d’imposition de 30% a été ramené à 25%. En contre partie le personnel expatrié se doit d’acquitter la contribution de sécurité sociale, actuellement d’un montant de 9%. Cette contribution intervient en amont de l’impôt et est déductible. Il en ressort alors que les revenus perçus subissent une imposition de l’ordre de 33%, alors qu’un Danois d’un statut socio-professionnel égal et gagnant au moins 60 000 couronnes par mois doit payer 60% de taxes.</p>
<p><strong>Ce régime dérogatoire de taxation est valable pour une période maximale de 36 mois et ne peut être obtenu qu’une fois. </strong></p>
<h3 class="spip">Impôts sur les sociétés</h3>
<p>Le taux de l’impôt sur les sociétés est de 25% du bénéfice imposable. Il est versé sous forme prévisionnel deux fois par an. Le bénéfice imposable ordinaire réalisé par les sociétés pendant l’exercice fiscal est évalué selon des règles très similaires à celles appliquées au revenu des personnes physiques.</p>
<h4 class="spip">Taxe sur la valeur ajoutée</h4>
<p>La taxe est payée par les entreprises à tous les stades de la production et de la distribution des biens et services, y compris le secteur agricole. Contrairement aux autres pays, le Danemark applique un taux de TVA unique de 25%. L’éducation, les services financiers, les assurances, le transport de passagers et la presse ne sont pas soumis à la TVA.</p>
<h3 class="spip"><a id="sommaire_4"></a>Quitus fiscal </h3>
<p>Un quitus fiscal est exigé avant de quitter le Danemark</p>
<h3 class="spip"><a id="sommaire_5"></a>Solde du compte en fin de séjour</h3>
<p>Un expatrié français relevant du secteur privé peut solder son compte en fin de séjour.</p>
<h3 class="spip"><a id="sommaire_6"></a>Coordonnées des centres d’information fiscale</h3>
<p><a href="http://www.skm.dk/" class="spip_out" rel="external">Ministère des Impôts (Skatteministeriet)</a> <br class="manualbr">Nicolai Eigtveds Gade<br class="manualbr">1402 Copenhague K<br class="manualbr">Tél. : 33 92 33 92</p>
<p><a href="http://www.skat.dk/" class="spip_out" rel="external">Administration centrale des douanes et des impôts (SKAT)</a> <br class="manualbr">Sluseholmen 8B<br class="manualbr">2450 Copenhague SV<br class="manualbr">Tél. : 72 22 18 18<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/fiscalite-22989/article/fiscalite-du-pays-111101#skat#mc#skat.dk#" title="skat..åt..skat.dk" onclick="location.href=mc_lancerlien('skat','skat.dk'); return false;" class="spip_mail">Courriel</a></p>
<p><a href="http://www.landsskatteretten.dk/" class="spip_out" rel="external">Tribunal fiscal national (Landsskatteretten)</a><br class="manualbr">Ved Vesterport 6,6.<br class="manualbr">1612 Copenhague V<br class="manualbr">Tél. : 33 76 09 09<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/fiscalite-22989/article/fiscalite-du-pays-111101#inst#mc#lsr.dk#" title="inst..åt..lsr.dk" onclick="location.href=mc_lancerlien('inst','lsr.dk'); return false;" class="spip_mail">Courriel</a></p>
<p><strong>Pour en savoir plus :</strong></p>
<p><a href="https://www.tresor.economie.gouv.fr/Pays/danemark" class="spip_out" rel="external">Service économique de l’Ambassade de France au Danemark</a><br class="manualbr">Kongens Nytorv 4<br class="manualbr">1050 Copenhague K<br class="manualbr">Tél. : 33 67 01 00</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/fiscalite-22989/article/fiscalite-du-pays-111101). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
