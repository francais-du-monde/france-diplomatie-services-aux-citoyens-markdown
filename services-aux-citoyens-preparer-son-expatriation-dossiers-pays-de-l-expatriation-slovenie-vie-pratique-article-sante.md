# Santé

<p>Pour une présentation des risques épidémiologiques ainsi que des conseils sur la médecine au quotidien, consultez les pages suivantes :</p>
<ul class="spip">
<li><a href="http://www.ambafrance-si.org/Sante" class="spip_out" rel="external">Conseils, liste de médecins et hôpitaux</a> sur le site de l’ambassade de France en Slovénie</li>
<li>Page dédiée à la <a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/slovenie/" class="spip_in">santé en Slovénie</a> sur le site Conseils aux voyageurs</li>
<li>Fiche Ljubljana sur le site du <a href="http://www.cimed.org/" class="spip_out" rel="external">CIMED</a></li></ul>
<h4 class="spip">Médecine de soins</h4>
<p><strong>Santé</strong></p>
<p><strong>Pour les courts séjours, se munir de la carte européenne d’assurance maladie à demander à votre caisse d’assurance maladie au moins 15 jours avant le départ.</strong> Pour plus d’information, consultez le site de l’<a href="http://www.ameli.fr/assures/droits-et-demarches/a-l-etranger/index.php" class="spip_out" rel="external">Assurance maladie</a>.</p>
<p>Lors d’une urgence nécessitant une intervention chirurgicale même bénigne, s’adresser aux services d’urgence des hôpitaux publics qui sont d’un bon niveau.</p>
<p>Le coût des soins est élevé dans le privé. Comptez 80€ pour une visite chez un dentiste, à partir de 30€ pour un médecin généraliste.</p>
<p>Les risques de piqûres de tiques (surtout au printemps) susceptibles de transmettre des maladies neurologiques (encéphalites) existent pour les personnes désireuses de se promener en forêt ou pour les pêcheurs à la ligne. Un vaccin contre l’encéphalo-méningite est disponible localement. <strong>Consulter le médecin traitant avant le départ et contracter une assurance de rapatriement sanitaire.</strong></p>
<p><strong>Grippe A/H1N1</strong></p>
<p>Des conseils et des informations détaillés sont disponibles sur le site de l’<a href="http://www.ustavimo-gripo.si/" class="spip_out" rel="external">Institut national slovène pour la protection de la santé</a>, accessible en slovène et en anglais.</p>
<p><strong>Numéros utiles</strong></p>
<ul class="spip">
<li>Ambulances : 112</li>
<li>Centre chirurgical de Rozna Dolina : +386 1477 94 00</li>
<li>Cabinet médical BARSOS (cabinet privé) : 386.1.242.07.00.</li>
<li>Hôpital de Ljubljana (Klinicni center) : standard : +386 1522 50 50 - Urgences : +386 1522 84 08</li>
<li><strong>Centre médical Ljubljana-Centre (Metelkova)</strong> - Metelkova ulica 9 - 1000 Ljubljana <br class="manualbr">Tel : +386 (01) 4723-700</li>
<li><strong>Centre médical Ljubljana-Centre (Kotnikova)</strong> - Kotnikova 36 - 1000 Ljubljana <br class="manualbr">Tel : +386 (01) 3009-660</li></ul>
<p><strong>Appel d’urgences :</strong></p>
<ul class="spip">
<li>Adultes : +386 (01) 522 84 08</li>
<li>Enfants : +386 (01) 47 23 888</li>
<li>Urgences dentaires : +386 (01) 47 23 718</li></ul>
<p><strong>Premiers secours :</strong></p>
<ul class="spip">
<li>+386 (01) 522 23 61</li>
<li>+386 (01) 522 33 85</li></ul>
<p>Pour en savoir plus :</p>
<ul class="spip">
<li><a href="http://www.pasteur-lille.fr/vaccinations-voyages.php?pays=Slov%C3%A9nie" class="spip_out" rel="external">Institut Pasteur de Lille / Slovénie</a></li>
<li><a href="http://www.invs.sante.fr/" class="spip_out" rel="external">Institut de veille sanitaire</a></li>
<li><a href="http://www.who.int/fr/index.html" class="spip_out" rel="external">Organisation Mondiale de la Santé</a></li>
<li><a href="http://www.pasteur-lille.fr/" class="spip_out" rel="external">l’institut Pasteur de Lille</a></li></ul>
<p><i>Mise à jour : septembre 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/vie-pratique/article/sante). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
