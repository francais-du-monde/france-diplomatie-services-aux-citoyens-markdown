# Etat civil et nationalité française

<p class="spip_document_84782 spip_documents spip_documents_center">
<img src="http://www.diplomatie.gouv.fr/fr/IMG/jpg/rubrique_etat_civil_cle8bcbed.jpg" width="660" height="140" alt=""></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-etat-civil-et-nationalite-francaise-etat-civil.md">Etat civil</a></li>
<li><a href="services-aux-citoyens-etat-civil-et-nationalite-francaise-etat-civil-article-demander-la-copie-d-un-acte-d-etat-civil.md">Demander la copie d’un acte d’état civil</a></li>
<li><a href="services-aux-citoyens-etat-civil-et-nationalite-francaise-etat-civil-article-les-naissances-a-l-etranger.md">Déclarer la naissance d’un enfant à l’étranger</a></li>
<li><a href="services-aux-citoyens-etat-civil-et-nationalite-francaise-etat-civil-article-les-mariages-a-l-etranger.md">Se marier à l’étranger</a></li>
<li><a href="services-aux-citoyens-etat-civil-et-nationalite-francaise-etat-civil-article-comment-etablir-un-pacte-de.md">Enregistrer un PACS à l’étranger</a></li>
<li><a href="services-aux-citoyens-etat-civil-et-nationalite-francaise-etat-civil-article-les-divorces-a-l-etranger.md">Faire reconnaitre un divorce prononcé à l’étranger</a></li>
<li><a href="services-aux-citoyens-etat-civil-et-nationalite-francaise-etat-civil-article-les-deces-et-les-disparitions-a-l.md">Déclarer un décès à l’étranger</a></li>
<li><a href="services-aux-citoyens-etat-civil-et-nationalite-francaise-etat-civil-article-livrets-de-famille.md">Actualiser son livret de famille </a></li>
<li><a href="services-aux-citoyens-etat-civil-et-nationalite-francaise-etat-civil-article-rectification-d-un-acte-d-etat.md">Demander la rectification d’un acte d’état civil</a></li>
<li><a href="services-aux-citoyens-etat-civil-et-nationalite-francaise-etat-civil-article-reconstitution-d-actes-d-etat.md">La reconstitution d’actes d’état civil</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-etat-civil-et-nationalite-francaise-nationalite-francaise.md">Nationalité française</a></li>
<li><a href="services-aux-citoyens-etat-civil-et-nationalite-francaise-nationalite-francaise-article-la-nationalite-francaise.md">Qu’est-ce que la nationalité française ?</a></li>
<li><a href="services-aux-citoyens-etat-civil-et-nationalite-francaise-nationalite-francaise-article-la-double-nationalite.md">La double-nationalité</a></li>
<li><a href="services-aux-citoyens-etat-civil-et-nationalite-francaise-nationalite-francaise-article-les-principaux-textes-applicables.md">Les principaux textes applicables</a></li>
<li><a href="services-aux-citoyens-etat-civil-et-nationalite-francaise-nationalite-francaise-article-a-qui-s-adresser-pour-un-dossier-individuel.md">A qui s’adresser pour un dossier individuel ?</a></li>
<li><a href="services-aux-citoyens-etat-civil-et-nationalite-francaise-nationalite-francaise-article-la-delivrance-de-certificat-de-nationalite-francaise.md">La délivrance de certificat de nationalité française</a></li>
<li><a href="services-aux-citoyens-etat-civil-et-nationalite-francaise-nationalite-francaise-article-la-reintegration-dans-la-nationalite-francaise.md">La réintégration dans la nationalité française</a></li>
<li><a href="services-aux-citoyens-etat-civil-et-nationalite-francaise-nationalite-francaise-article-la-perte-de-la-nationalite-francaise.md">La perte de la nationalité française</a></li>
<li><a href="services-aux-citoyens-etat-civil-et-nationalite-francaise-nationalite-francaise-article-l-acquisition-de-la-nationalite-francaise.md">L’acquisition de la nationalité française</a></li>
<li><a href="services-aux-citoyens-etat-civil-et-nationalite-francaise-nationalite-francaise-article-l-attribution-de-la-nationalite-francaise.md">L’attribution de la nationalité française</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-etat-civil-et-nationalite-francaise-en-savoir-plus.md">En savoir plus</a></li>
<li><a href="services-aux-citoyens-etat-civil-et-nationalite-francaise-en-savoir-plus-article-les-missions-des-ambassades-et-des-consulats-francais-en-matiere-d-etat-civil.md">Les missions des ambassades et des consulats français en matière d’état civil</a></li>
<li><a href="services-aux-citoyens-etat-civil-et-nationalite-francaise-en-savoir-plus-article-missions-du-service-central-d-etat-civil.md">Les missions du Service central d’état civil (Nantes)</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-etat-civil-et-nationalite-francaise-questions-reponses.md">Questions / réponses</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
