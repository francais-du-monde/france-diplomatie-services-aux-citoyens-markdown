# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/paraguay/emploi-stage/article/reglementation-du-travail-113494#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/paraguay/emploi-stage/article/reglementation-du-travail-113494#sommaire_2">Fêtes légales</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/paraguay/emploi-stage/article/reglementation-du-travail-113494#sommaire_3">Emploi du conjoint</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<p><strong>Le cadre réglementaire est fixé par le Code du Travail paraguayen</strong> défini par la loi n° 213/93 du 29 octobre 1993 et modifié par la loi n° 496/95 du 25 août 1995, dont les principales dispositions sont résumées ici.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Le texte intégral de la loi de 1993 est disponible, en version espagnole, sur le site internet du <a href="http://www.ilo.org/dyn/natlex/" class="spip_out" rel="external">Bureau international du travail (B.I.T.)</a>. </p>
<p>La durée de la période d’essai ne peut excéder 30 jours pour un emploi non-qualifié et 60 jours pour un emploi qualifié.</p>
<p>La durée légale de travail est fixée à huit heures par jour ou 48 heures par semaine (diurne), et de sept heures par jour ou 42 heures par semaine (nocturne) Le travailleur a droit à une journée entière de repos par semaine. Le jour normal de repos hebdomadaire est le dimanche et les fériés établis par loi (art 217 du Code du Travail).</p>
<p>Les heures supplémentaires sont payables sur la base de 150% du taux horaire normal pour les heures effectuées pendant les jours ouvrés et sur la base de 200% pendant les dimanches et jours fériés.</p>
<p>Le droit à congé annuel est fonction de l’ancienneté dans l’entreprise : 12 jours pour les cinq premières années, 18 jours jusqu’à 10 ans et 30 jours au-delà de 10 ans d’ancienneté. Ces congés s’entendent sans préjudice des jours fériés payés, au nombre de 12 pendant l’année.</p>
<p>Les droits à congé pour cause de maladie sont versés pour une période de six mois au maximum, sur la base de 50% du salaire. Les congés maternité sont payés pendant 12 semaines, sur la base de 50% du salaire.</p>
<p>L’âge de départ à la retraite est fixé à 60 ans mais la sécurité sociale (ley orgánica del IPS : 98/92) admet également une cessation d’activité (pre-retraite) à 55 ans (85% du salaire correspondant aux trois dernières années).</p>
<h3 class="spip"><a id="sommaire_2"></a>Fêtes légales</h3>
<ul class="spip">
<li>1er janvier : Nouvel An</li>
<li>1er mars : Jour des Héros</li>
<li>Avril : Jeudi et Vendredi saints (fêtes mobiles)</li>
<li>1er mai : Fête du Travail</li>
<li>14 et 15 mai : Fête nationale (Jour de l’Indépendance)</li>
<li>12 juin : Jour de la Paix (armistice de la guerre du Chaco)</li>
<li>15 août : Fête de la Vierge d’Assomption</li>
<li>29 septembre : Anniversaire de la bataille de Boquerón</li>
<li>8 décembre : Fête de la Vierge de Caacupé</li>
<li>25 décembre : Nöel</li></ul>
<p><strong>Voir aussi : </strong> <a href="http://www.tv5.org/TV5Site/voyageurs/accueil.php" class="spip_out" rel="external">TV5.org</a></p>
<h3 class="spip"><a id="sommaire_3"></a>Emploi du conjoint</h3>
<p>L’emploi du conjoint est soumis aux conditions applicables aux travailleurs étrangers.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/paraguay/emploi-stage/article/reglementation-du-travail-113494). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
