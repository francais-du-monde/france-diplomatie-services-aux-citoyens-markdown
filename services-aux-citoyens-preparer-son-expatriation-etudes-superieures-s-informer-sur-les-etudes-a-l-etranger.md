# S’informer sur les études à l’étranger

<h2 class="rub12799">Sommaire</h2>
<p><strong>Trouver son université à l’étranger</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/Formationssuprecad-3.pdf" class="spip_in" type="application/pdf">Répertoire des formations supérieures francophones à l’étranger</a></p>
<p><strong>Les études dans l’Union européenne</strong></p>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures-s-informer-sur-les-etudes-a-l-etranger-article-centres-nationaux-de-ressources.md" class="spip_in">Les 4 centres nationaux de ressources en France</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures-s-informer-sur-les-etudes-a-l-etranger-article-la-reconnaissance-des-diplomes-au-sein-de-l-ue.md" class="spip_in">La reconnaissance des diplômes dans l’Union européenne</a></li></ul>
<p><strong>Les études hors Union européenne</strong></p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/politique-etrangere-de-la-france/cooperation-educative/les-actions-de-cooperation-dans-l-enseignement-superieur/assurer-une-veille-sur-les-systemes-d-enseignement-superieur-dans-le-monde-base/" class="spip_in">Fiches enseignement supérieur du Forum Curie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures-s-informer-sur-les-etudes-a-l-etranger-article-reconnaissance-des-diplomes-etrangers-en-france.md" class="spip_in">La reconnaissance des diplômes étrangers en France</a></li></ul>
<p><strong>Étudier à distance</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures-s-informer-sur-les-etudes-a-l-etranger-article-etudier-a-distance.md" class="spip_in">S’informer sur les études à distance</a></p>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/etudes-superieures/s-informer-sur-les-etudes-a-l-etranger/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
