# Déclarer un décès à l’étranger

<p class="chapo">
      Dans bon nombre de pays, le décès d’un ressortissant français doit être déclaré à l’état civil local dans les mêmes conditions que le décès d’un national de ce pays. Un acte de décès local est alors souvent établi.
</p>
<p><a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/article/annuaire-des-representations-francaises-a-l-etranger" class="spip_in">L’officier de l’état civil consulaire territorialement compétent</a> peut transcrire cet acte de décès local. Cette transcription n’est pas obligatoire, mais elle est fortement recommandée dans la mesure où elle permet notamment d’apposer la mention du décès sur l’acte de naissance français du défunt.</p>
<p>Dans tous les cas, qu’un acte de décès local ait ou non été établi, l’officier de l’état civil consulaire peut, à tout moment, recevoir une déclaration de décès et dresser l’acte, sous la seule réserve que la législation locale ne le lui interdise pas.</p>
<p>En cas de disparition individuelle ou collective de ressortissants français suite à un événement exceptionnel (accident maritime ou aérien, catastrophe naturelle, etc.), une procédure de déclaration judiciaire de décès peut être mise en place lorsque le ou les corps n’ont pu être retrouvés et identifiés.</p>
<p><i>Mise à jour : mars 2016</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/etat-civil/article/les-deces-et-les-disparitions-a-l). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
