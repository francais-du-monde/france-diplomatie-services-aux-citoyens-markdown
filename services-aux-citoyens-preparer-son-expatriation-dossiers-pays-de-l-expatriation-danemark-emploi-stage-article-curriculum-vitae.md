# Curriculum vitae

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/emploi-stage/article/curriculum-vitae#sommaire_1">10 étapes pour créer un bon CV</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/emploi-stage/article/curriculum-vitae#sommaire_2">Les règles d’or</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/emploi-stage/article/curriculum-vitae#sommaire_3">Pour être efficace et représentatif, un CV doit être :</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/emploi-stage/article/curriculum-vitae#sommaire_4">Le CV classique </a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/emploi-stage/article/curriculum-vitae#sommaire_5">Le CV thématique </a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/emploi-stage/article/curriculum-vitae#sommaire_6">Les rubriques du CV</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/emploi-stage/article/curriculum-vitae#sommaire_7">Diplômes</a></li></ul>
<p>Afin de proposer votre candidature à une entreprise danoise, les étapes sont similaires à celles que vous pourriez entreprendre pour une entreprise française. Il suffit de rédiger un CV ainsi qu’une lettre de motivation. La différence à souligner est, qu’au Danemark, le curriculum vitae prend une forme quelque peu distincte.</p>
<p>Tous deux sont des documents marketing qui fournissent des informations clés concernant vos connaissances, vos expériences, votre cursus et vos qualités personnelles.</p>
<p>Pour postuler, plusieurs possibilités s’offrent à vous :</p>
<ul class="spip">
<li>Vous répondez à une offre de stage ou d’emploi</li>
<li>Vous faites une candidature spontanée</li>
<li>Vous postulez directement sur le site Internet de l’entreprise</li>
<li>Vous déposez votre CV sur un site de recherche d’emploi</li></ul>
<p>Si vous envoyez votre CV par la poste, envoyez-le aussi par messagerie électronique. Privilégiez toujours l’envoi par la poste. Il est plus facile de supprimer un message électronique que de jeter une lettre déjà matérialisée sous format papier.</p>
<p>Avant de rédiger votre CV, faites votre propre évaluation. Il faut penser à votre projet professionnel et orienter votre CV en fonction de l’objectif défini. Si votre intérêt porte sur différents postes, créez un CV pour chacun d’entre eux.</p>
<p>Si vous répondez à une offre de stage ou d’emploi, vous pouvez suivre ces quelques étapes :</p>
<ul class="spip">
<li>repérez les mots clés de l’offre dans la présentation des fonctions à assumer, des produits ou services vendus,</li>
<li>renseignez-vous sur l’entreprise, réalisez quelques recherches sur Internet ou utilisez des annuaires d’entreprises tel que le <a href="http://www.kompass.fr/" class="spip_out" rel="external">Kompass</a> ou <a href="http://www.greens.dk/" class="spip_out" rel="external">Greens</a>. Les informations mises en ligne par Greens <i>on line</i> sont payantes, sauf dans les bibliothèques danoises qui ont acheté les droits.</li>
<li>rassemblez l’ensemble de ces informations afin de pouvoir en faire une synthèse,</li>
<li>notez votre parcours professionnel, vos activités extra professionnelles, vos formations et mettez en valeur vos compétences et qualifications pour mettre en lumière les similitudes entre ce que vous avez réalisé et les tâches que vous seriez amené à effectuer dans le poste.</li>
<li>prouvez à l’aide d’exemples concrets tirés de votre expérience professionnelle que vous êtes le ou la candidate idéal(e) pour le poste. sur cette base, revoyez votre C.V. (en anglais évidemment) afin de l’adapter si nécessaire et rédigez votre lettre de motivation.</li></ul>
<h3 class="spip"><a id="sommaire_1"></a>10 étapes pour créer un bon CV</h3>
<ul class="spip">
<li>Ciblez l’emploi que vous recherchez, donnez-lui un titre.</li>
<li>Déterminez les compétences, les savoirs et les expériences qui sont nécessaires pour ce poste.</li>
<li>Faites une liste de vos 2, 3 ou 4 points forts qui font de vous le candidat idéal.</li>
<li>Pour chaque compétence requise par le poste, pensez à plusieurs de vos expériences passées qui pourraient illustrer cette compétence.</li>
<li>Décrivez chaque expérience simplement mais de façon à ce qu’elle souligne et les résultats qui peuvent être bénéfiques à votre employeur (augmentation des ventes, résultats, travail spécifique, environnement spécial…)</li>
<li>Faites une liste de tous les emplois que vous avez occupés, même ceux qui n’ont pas été rémunérés mais qui démontrent que vous avez les compétences requises pour le poste proposé.</li>
<li>Faites une liste des formations qui sont adaptées à l’emploi pour lequel vous postulez.</li>
<li>Résumez vos points forts.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Les règles d’or</h3>
<ul class="spip">
<li>Évitez de mentir ou d’embellir la réalité, l’employeur découvrira tôt ou tard la vérité et vous perdrez ainsi vos chances d’être sélectionné pour le poste.</li>
<li>Ne perdez pas de vue l’objectif principal : une lecture facile et rapide. (pas de sigle sans précision de nom)</li>
<li>N’envoyez jamais votre CV seul mais avec une lettre d’accompagnement ou une lettre de candidature.</li>
<li>Essayez de personnaliser votre CV, le rendre attractif/différent afin que le recruteur puisse se souvenir de votre profil (présentation, format, papier de couleur, support autre que le papier, CD/pamphlet, cahier, etc.) mais sans exagération.</li>
<li>Adaptez votre CV à chaque candidature : mettre en valeur les compétences qui répondent exactement aux qualifications exigées dans le poste et supprimer les autres. (Ne citez pas le DEUG si vous êtes titulaire d’un diplôme de niveau Bac +4/5 et ne citez pas le BEPC si vous êtes titulaire du Baccalauréat).</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Pour être efficace et représentatif, un CV doit être :</h3>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Court</strong>
</p>
<p>Une (pour les nouveaux diplômés) à trois pages (maximum) retraçant les points significatifs de votre parcours.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Direct</strong>
</p>
<p>Faites apparaître clairement ce que vous pouvez offrir à l’entreprise et n’hésitez pas à présenter votre motivation professionnelle par une phrase en guise " d’accroche ".</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Lisible</strong>
</p>
<p>La présentation doit être claire. Indiquez les points les plus importants en les listant plutôt que de rédiger de longues phrases ou paragraphes.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Stylé
</p>
<p>Le CV est personnel mais il y a une mise en page à respecter, on peut le présenter de deux façons</p>
<h3 class="spip"><a id="sommaire_4"></a>Le CV classique </h3>
<p>C’est-à-dire de disposition anti-chronologique en respectant les dates/années et les différents postes occupés dans telle entreprise à telle période, en commençant toujours par la dernière expérience.</p>
<p>La plupart des recruteurs conseillent aux jeunes diplômés de présenter le CV sous cette forme car souvent leur expérience professionnelle est trop peu significative.</p>
<h3 class="spip"><a id="sommaire_5"></a>Le CV thématique </h3>
<p>Il doit mentionner les différentes expériences professionnelles et les compétences acquises.</p>
<p>Il n’y a pas de présentation type, elle se fait selon vos goûts, mais tenez compte des points suivants :</p>
<ul class="spip">
<li>Le CV sera toujours rédigé en anglais, même à l’attention d’une filiale française au Danemark et il n’est <strong>jamais manuscrit</strong>.</li>
<li>Utilisez le format A4 pour votre CV et votre lettre de motivation.</li>
<li>Imprimez seulement le recto, jamais de recto-verso.</li>
<li>Utilisez une taille de police entre 10 et 14 pour l’aisance de lecture.</li>
<li>Utilisez une seule et même police</li>
<li>Restez sobre, pas de fantaisies dans la présentation (sauf dans domaines spécifiques : design par exemple), pas de graphique…</li>
<li>Evitez l’italique.</li>
<li>N’agrafez pas le CV à votre lettre.</li></ul>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Présenté dans l’ordre anti-chronologique</strong>
</p>
<p>Commencez par les dates les plus récentes pour finir par les dates les plus anciennes et ceci vaut également pour les rubriques " expérience professionnelle " et " formation ".</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Explicite</strong>
</p>
<p>Faites figurer en caractères gras, soulignés et / ou encadrés, les fonctions exercées, les diplômes obtenus…etc. Les jeunes diplômés préciseront les cours suivis et les stages en entreprises et les emplois saisonniers (en guise d’expérience professionnelle), tandis que les autres mettront l’accent sur leur expérience professionnelle.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Un document professionnel qui doit vous mettre en valeur</strong>
<br>— Ne jamais noter d’abréviation ou d’initiale.
<br>— Inutile de mentionner des termes tels que CDD, vacation. En revanche, il est possible de mettre " trainee " ou " internship " lorsque vous avez fait un stage.
<br>— Evitez de noter explicitement " je ". Par exemple, au lieu de dire " j’étais responsable de la communication ", dire " responsable communication " </p>
<p>Notez l’expérience en première place : si vous avez peu de diplômes ou si vous avez déjà beaucoup d’expérience ou que vous n’êtes plus étudiant.</p>
<p>Notez la formation en première place : si vous avez peu d’expérience ou si vous êtes étudiant. Mettez en valeur vos diplômes, études, rapports et mémoires.</p>
<h3 class="spip"><a id="sommaire_6"></a>Les rubriques du CV</h3>
<h4 class="spip">Informations personnelles </h4>
<p><strong>Prénom</strong> (<i>first name</i>) et <strong>Nom</strong> (<i>last name</i>) et pas de surnom. Vous avez la possibilité d’apposer la première lettre de votre deuxième prénom, ex : Sylvia V. Sudrés.</p>
<p><strong>Adresse</strong> au Danemark et / ou en France (<i>address</i>). Utilisez une adresse permanente (celle de vos parents, d’un ami ou celle que vous comptez utiliser après l’obtention de votre diplôme).</p>
<p><strong>Téléphone</strong> personnel (<i>home phone number</i>), professionnel (office phone number), fixe et cellulaire (cell phone). Utilisez un numéro de téléphone permanent, incluant le code pays. Si vous avez un répondeur, enregistrez un message d’accueil neutre.</p>
<p><strong>Numéro de fax</strong> (<i>fax number</i>).</p>
<p><strong>E-mail</strong> (choisissez une adresse email à but professionnel, évitez les adresses fantaisistes).</p>
<p>Indiquez <strong>l’adresse de votre site web</strong> seulement si les pages web reflètent vos ambitions professionnelles.</p>
<p>Cette partie peut éventuellement comporter une photo d’identité en haut à droite du CV mais ce n’est en aucun cas obligatoire. Elle doit être récente et ressemblante, le fond uni. Pensez à sourire !</p>
<h4 class="spip">Le projet professionnel</h4>
<p>Vous devez accorder cette expression à votre objectif du moment : <i>internship objective</i> (demande de stage), <i>professional objective</i> (objectif professionnel), <i>career objective</i> (objectif de carrière)…</p>
<p>Cette rubrique est destinée à développer ce que l’on propose à l’entreprise en soulignant :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>les compétences</strong> que l’on peut apporter à cette société (synthétisées en 3/4 points forts) en soulignant ses qualités et en définissant son profil.
</p>
<p><strong>exemple</strong> : La fonction commerciale exige une <strong>bonne sociabilité</strong>, de la <strong>combativité</strong> et du <strong>dynamisme</strong> ainsi que le <strong>goût de la vente</strong>. Les fonctions administratives et comptables exigent une <strong>aptitude au calcul</strong>, de la <strong>rigueur</strong>, de la <strong>précision</strong> et de la <strong>méthode</strong>.</p>
<p>Plus larges seront vos compétences, plus votre valeur sur le marché de l’emploi sera appréciée. Les débutants devront puiser dans leurs expériences scolaires, leurs travaux saisonniers, leurs stages, leurs loisirs.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  l’objectif professionnel fixé
</p>
<p>Spécifiez le titre du poste ou faites-en une description. Si vous ne savez pas exactement quel type d’emploi vous aimeriez occuper, faites une description de poste plus large. Par exemple : <i>To obtain an entry-level position within a financial institution requiring strong analytical and organizational skills</i>. Ajustez votre objectif à chaque employeur, chaque emploi recherché.</p>
<ul class="spip">
<li>Préférez des catégories larges à des titres spécifiques (i.e : office management vs. secretary)</li>
<li>Utilisez des verbes d’action Utilisez des verbes d’action</li>
<li>Soyez clair, précis et direct.</li></ul>
<h4 class="spip">L’expérience professionnelle</h4>
<ul class="spip">
<li>L’objectif est de mettre en valeur vos responsabilités, vos succès et de détailler votre profil clairement.</li>
<li>Ne vous inventez pas des fonctions et n’embellissez-pas vos expériences : l’honnêteté est la meilleure des politiques.</li></ul>
<p><strong>Il est important de mentionner :</strong></p>
<ul class="spip">
<li><strong>les dates</strong> : mois et années, pas de jour</li>
<li>mettez votre <strong>dernière expérience en premier </strong></li>
<li>mettez au <strong>passé</strong> vos <strong>expériences terminées</strong> et au <strong>présent</strong> votre <strong>poste actuel</strong></li>
<li>Indiquez s’il s’agit d’un <strong>stage</strong> ou d’un <strong>emploi</strong></li>
<li>le nom de la société et son secteur d’activité le lieu : ne pas indiquer l’adresse entière mais seulement la ville et le pays</li>
<li>la fonction (position held) : essayez de donner un titre aux tâches que vous avez effectuées, même si elles sont variées. Ceci est une autre façon de présenter ce que vous avez pu réaliser ou ce que vous voulez réaliser.</li>
<li>les missions et tâches accomplies : évitez les jargons, les termes techniques, les abréviations, hormis si le recruteur est censé les connaître.</li></ul>
<p><strong>N’hésitez pas à :</strong></p>
<p><strong>Employer et diversifier les verbes d’action</strong> pour décrire ce qui a été réalisé, effectué, gagné ou économisé pour le compte de (des) entreprise(s) dans la (les) quelle(s) vous avez travaillé. Cela permet de dynamiser la présentation de votre CV.</p>
<p>Pour ceux qui ont une <strong>formation " technique "</strong> (commerciale, marketing, d’ingénierie…) utilisez des " puces " ou " tirets " afin que le lecteur puisse rapidement déterminer votre profil, vos expériences.</p>
<p>Pour ceux qui ont une <strong>formation plus littéraire</strong> (professorat, langues appliquées, journalisme…) et qui postulent à des postes où il est nécessaire d’avoir certaines qualités rédactionnelles, il serait préférable de présenter ses expériences de façon plus rédigée.</p>
<p><strong>Mentionner les stages qui font partie de votre expérience professionnelle.</strong></p>
<p><strong>Citer des chiffres</strong> pour quantifier, des exemples pour être concret et mettre en valeur les bénéfices de votre action : par exemple une réalisation professionnelle remarquable et / ou quantifiable :</p>
<ul class="spip">
<li>Une amélioration de performance</li>
<li>Une diminution de coûts de …DKK</li>
<li>Une augmentation de profits et / ou de ventes de …%</li>
<li>Un gain de temps</li>
<li>Un accroissement d’efficacité</li>
<li>Un développement de clientèle</li>
<li>Une certitude quant à un meilleur contrôle</li>
<li>Une aide à l’amélioration de la prise de décision</li>
<li>Une contribution à l’amélioration esthétique et fonctionnelle d’un produit</li>
<li>Une progression dans les relations avec le personnel</li>
<li>Une amélioration dans la fiabilité d’un produit, d’un service ou d’un outil de réflexion</li>
<li>La mise en place d’une nouvelle technologie, d’un nouveau procédé ou d’un nouveau design</li>
<li>Une rénovation des systèmes</li>
<li>Un perfectionnement des stratégies</li>
<li>Une rentabilisation des opérations jusque-là déficitaires</li>
<li>Une amélioration des conditions de travail</li>
<li>Une élimination de gaspillage de…%</li>
<li>Une identification et une résolution des problèmes</li>
<li>Un accès à un nouveau service</li>
<li>Une gestion d’un budget à hauteur de…..</li>
<li>L’obtention d’un prix</li>
<li>L’atteinte d’objectifs prédéfinis</li></ul>
<h4 class="spip">La formation</h4>
<p><strong>Indiquez dans cette rubrique :</strong></p>
<ul class="spip">
<li>Le <strong>nom de l’établissement</strong>, école ou université.</li>
<li>Le <strong>lieu</strong>, c’est-à-dire la ville ou le pays.</li>
<li>Les <strong>diplômes</strong> obtenus, les <strong>cours</strong> suivis du plus récent au plus ancien, en donnant leurs <strong>équivalences</strong> dans le système danois (par exemple : granted two years after gymnasium pour un BTS).</li>
<li>Votre <strong>niveau d’études</strong>, la matière la plus étudiée / votre spécialisation (major) ainsi que <strong>les notes, mentions ou récompenses obtenues</strong>. N’hésitez pas à <strong>indiquer vos scores</strong> à des examens tels que le TOEFL, TOIEC, GPA…</li></ul>
<h4 class="spip">Autres informations</h4>
<p>Langues (<i>languages</i>)</p>
<p>Afin d’évaluer votre niveau, vous pouvez utiliser les termes suivants :</p>
<ul class="spip">
<li><i>Mother tongue or native language</i> (langue maternelle)</li>
<li><i>2nd Native tongue</i> (2ème langue maternelle)</li>
<li><i>Bilingual</i> (bilingue) : vous maîtrisez parfaitement la langue à l’écrit et à l’oral, par exemple dans le cadre d’une double nationalité.</li>
<li><i>Fluent</i> (capable of flowing) : vous vous exprimez aisément.</li>
<li><i>Proficient</i> : votre niveau est bon mais vous manquez de pratique et ne pouvez converser à propos de tout de façon aisée.</li>
<li><i>Conversational</i> / niveau conversationnel (moyen) : vous comprenez et vous vous faites comprendre mais votre conversation est limitée.</li>
<li><i>Good Knowledge</i> : ce niveau fait référence à un niveau scolaire bon mais où la pratique manque.</li>
<li><i>Working Knowledge</i> : la conversation touchant à un secteur donné, celui du travail exercé, est moyenne à bonne, elle est limitée par ailleurs.</li>
<li><i>Beginner or Notions</i> : niveau débutant ou " faux " débutant dans le cas d’une langue non pratiquée depuis longtemps. La maîtrise du danois est vivement conseillée mais l’anglais est également très répandu dans le monde des affaires. Si vous parlez danois, mettez-le en valeur !</li></ul>
<p><strong>Informatique </strong> (<i>computer skills</i>)</p>
<p>Citez les logiciels maîtrisés (<i>software</i>) et précisez votre niveau, par exemple :</p>
<ul class="spip">
<li>Expert de Microsoft Office</li>
<li>Expert des systèmes d’exploitation Mac et PC.</li>
<li>Bon niveau informatique</li>
<li>Internet : expert de la recherche sur internet</li></ul>
<p>Remarque : faites en sorte de ne pas multiplier les termes à haute technicité, incompréhensibles pour la plupart de vos lecteurs, à part si vous postulez dans une société d’informatique (<i>Information Technology</i> (IT) = nouvelles technologies de l’information et de la communication).</p>
<h4 class="spip">Activités extra-professionnelles</h4>
<p>Toute activité extra-professionnelle est jugée très importante lorsque l’on postule à un emploi au Danemark.</p>
<p>Sport, association, club, activité de loisir, passion : citez-les si cela peut vous différencier des autres (expérience en tant que leader / leadership experience), si l’activité traduit des qualités appréciables requises pour le poste à pourvoir.</p>
<p>Les Danois précisent volontiers leur appartenance à une association ou à une organisation.</p>
<p><strong>Références</strong> (<i>references</i>)</p>
<p>Il n’est pas vraiment d’usage au Danemark de prouver ses qualifications et donc de fournir des photocopies de diplômes. De même, il n’est pas très fréquent d’attester de son passé professionnel par des références et des lettres rédigées par vos anciens employeurs, précisant vos qualités et les actions menées. Quoique l’habitude de citer des références ne soit pas encore très répandue, cette pratique se généralise pour les postes d’encadrement.</p>
<p>Les photocopies de diplômes et d’attestations d’employeurs sont normalement requises dans le secteur public.</p>
<p>Souvent, les références ainsi que les lettres de recommandation sont données suite au premier entretien.</p>
<p>Les références consistent à fournir le nom et les coordonnées de vos anciens employeurs et/ou professeurs après leur avoir demandé leur autorisation. Il est possible de demander aux précédents employeurs d’écrire une lettre de recommandation en anglais ou en danois.</p>
<p>La pratique la plus courante est d’indiquer en bas de votre CV : <i>references available upon request</i>. Vous pouvez aussi mettre vos références directement à la fin de votre CV. Dans le premier cas, préparez une liste de contacts (voir ci-dessous).</p>
<p><strong>Erreurs à ne pas faire</strong></p>
<ul class="spip">
<li>Ne jamais envoyer de CV-standard, toujours l’adapter en fonction du poste souhaité. Rédiger éventuellement deux types de CV : le CV-brut et un CV à adapter en fonction du poste.</li>
<li>Omettre la partie personnelle (divers/loisirs) : une des rubriques les plus importantes au Danemark car elle donne une idée sur la personnalité du candidat et l’image d’une personne " équilibrée ". Pour les employeurs danois, il importe d’employer une personne dans son " intégralité "</li>
<li>Eviter d’envoyer deux sortes de CV : thématique et anti-chronologique, cela risque de perturber l’employeur.</li>
<li>Eviter les trous dans le CV.</li>
<li>Eviter de faire une longue énumération des cours et formations suivis dans toutes sortes de domaines, seuls les cours adaptés au poste recherché doivent être notés. Votre CV est terminé, c’est l’heure des dernières vérifications !</li></ul>
<h4 class="spip">Récapitulatif</h4>
<p>Il ne doit y avoir aucune erreur grammaticale, faute d’orthographe ou incohérence.</p>
<p>Pensez aussi à soigner la présentation pour faire en sorte qu’il soit agréable à la lecture.</p>
<p>Faites-le relire et corriger par des professionnels ou des personnes compétentes (un professeur de langues, un professeur s’occupant de l’insertion professionnelle). Chacune d’entre elles vous donnera son point de vue et fera évoluer votre CV.</p>
<h3 class="spip"><a id="sommaire_7"></a>Diplômes</h3>
<p>A titre indicatif, voici une liste approximative des équivalences de diplômes :</p>
<table class="spip">
<thead><tr class="row_first"><th id="id4a4f_c0">Diplômes français  </th><th id="id4a4f_c1"> Diplômes danois </th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="id4a4f_c0">CAP/BEP</td>
<td headers="id4a4f_c1">Erhvervsgrunduddannelser</td></tr>
<tr class="row_even even">
<td headers="id4a4f_c0">Brevet des collèges</td>
<td headers="id4a4f_c1">Bevis for Folkeskolens Afgangsprov Ou  Folkeskolens 10.-klasseprove</td></tr>
<tr class="row_odd odd">
<td headers="id4a4f_c0">Baccalauréat</td>
<td headers="id4a4f_c1">Studentereksamen</td></tr>
<tr class="row_even even">
<td headers="id4a4f_c0">Bac Professionnel</td>
<td headers="id4a4f_c1">Hojere forberedelseseksamen (HF)  (général)  ou  Hojere handelseksamen (HHX)  (commerce)  ou  Hojere tekniskeksamen (HTX)  (sciences et technologie)</td></tr>
<tr class="row_odd odd">
<td headers="id4a4f_c0"><i>Undergraduate studies </i> Licence / Maîtrise (2eme cycle)</td>
<td headers="id4a4f_c1">Bachelor’s Degree (BA)</td></tr>
<tr class="row_even even">
<td headers="id4a4f_c0"><i>Graduate studies </i> DEA / DESS (3eme cycle)</td>
<td headers="id4a4f_c1">Master’s Degree / candidatgrad</td></tr>
<tr class="row_odd odd">
<td headers="id4a4f_c0">Post-graduate studies  Doctorat</td>
<td headers="id4a4f_c1">Ph.D Degree</td></tr>
</tbody>
</table>
<p>Pour les équivalences des diplômes étrangers, il faut contacter l’université compétente au Danemark. Eventuellement, il convient de se renseigner auprès <br class="autobr"><a href="http://www.ambafrance-dk.org/" class="spip_out" rel="external">du Service culturel de l’Ambassade de France au Danemark</a><br class="manualbr">Service de coopération scientifique et universitaire<br class="manualbr">Ostergade 18, 1.<br class="manualbr">1100 Copenhague K<br class="manualbr">Tél. : 33 38 47 03</p>
<p>ou de l’<a href="http://www.enic-naric.net/index.aspx?c=Denmark" class="spip_out" rel="external">Agence nationale pour la reconnaissance des diplômes étrangers</a>. Vous pouvez également contacter le <a href="http://www.iu.dk/" class="spip_out" rel="external">Danish Agency for International Education</a><br class="manualbr">Bredgade 43<br class="manualbr">1260 Copenhague K <br class="manualbr">Tél : 33 95 70 00</p>
<p>Sur le site internet vous obtiendrez des informations sur la reconnaissance des diplômes français au Danemark et pourrez télécharger les formulaires de demande d’équivalence. Une attestation de qualifications provenant de cette agence vous est utile dans le cadre de le recherche d’emploi car :</p>
<ul class="spip">
<li>elle sert de point de repère pour les employeurs danois. Ceux-ci sont ainsi plus en mesure d’apprécier vos qualifications.</li>
<li>elle fournit de plus amples informations sur votre niveau de qualification.</li>
<li>pour les employés du secteur public, le niveau de qualification est un des paramètres utilisés dans l’échelle des salaires dans les conventions collectives.</li>
<li>elle permet aux conseillers du marché du travail d’être plus en mesure d’évaluer vos opportunités d’emploi ou de formation professionnelle. Il faut savoir qu’il existe une liste de professions régulées au Danemark (notamment les professions du secteur de la santé). Pour être en mesure d’exercer une profession régulée, les diplômes français doivent faire l’objet d’une autorisation de l’autorité publique concernée.</li></ul>
<p><strong>Remarques</strong></p>
<p>Ne citez pas le DEUG et la Licence si vous êtes titulaire d’un diplôme de niveau Bac + 4/5 et ne citez pas le BEPC si vous êtes titulaire du Baccalauréat.</p>
<p>N’utilisez pas de sigle pour nommer votre école qui, aussi renommée soit-elle en France, n’est pas forcément connue à l’échelon international.</p>
<p class="document_doc">
<a class="spip_in" title="Doc:Modèle de CV , 17.4 ko, 0x0" href="http://www.diplomatie.gouv.fr/fr/IMG/docx/Modeles_de_CV_cle85d558-1.docx"><img width="52" height="52" alt="Doc:Modèle de CV , 17.4 ko, 0x0" src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L52xH52/docx-1c5f7.png" style="vertical-align: middle;"></a>
<a href="http://www.diplomatie.gouv.fr/fr/IMG/docx/Modeles_de_CV_cle85d558-1.docx">Modèle de CV - (Word, 17.4 ko)</a>
</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/emploi-stage/article/curriculum-vitae). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
