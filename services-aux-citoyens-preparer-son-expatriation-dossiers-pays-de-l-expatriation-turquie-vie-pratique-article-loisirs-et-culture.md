# Loisirs et culture

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/vie-pratique/article/loisirs-et-culture#sommaire_1">Tourisme</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/vie-pratique/article/loisirs-et-culture#sommaire_2">Activités culturelles</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/vie-pratique/article/loisirs-et-culture#sommaire_3">Sports</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/vie-pratique/article/loisirs-et-culture#sommaire_4">Télévision – Radio</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/vie-pratique/article/loisirs-et-culture#sommaire_5">Presse française</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Tourisme</h3>
<p>La Turquie est un pays très riche sur le plan touristique. Que ce soit pour les curiosités naturelles (Cappadoce, côte égéenne, côte méditerranéenne) ou les sites historiques (Ephèse, Aphrodisias, Troie…), les occasions de faire du tourisme sont innombrables.</p>
<p>Pour toute information concernant le tourisme, s’adresser à :</p>
<p><a href="http://www.goturkey.com/" class="spip_out" rel="external">Office turc du tourisme</a> <br class="manualbr">102 avenue des Champs Elysées <br class="manualbr">75008 Paris <br class="manualbr">Téléphone : 01 45 62 78 68 / 79 84 <br class="manualbr">Télécopie : 01 45 63 81 05</p>
<h3 class="spip"><a id="sommaire_2"></a>Activités culturelles</h3>
<h4 class="spip">Activités culturelles en français</h4>
<p>Des films français en VO, sous-titrés en langue turque, sont présentés dans les salles de cinéma à Ankara et Istanbul. En outre, les festivals annuels d’Ankara et d’Istanbul présentent traditionnellement un certain nombre de films français.</p>
<p>Les troupes de théâtre français et les spectacles musicaux se produisent cinq à six fois par saison. Des expositions d’artistes français sont organisées plusieurs fois par an, soit par les Centres culturels, soit par des galeries privées.</p>
<p>Instituts français en Turquie (Ankara, Istanbul, Izmir).</p>
<h4 class="spip">Activités culturelles locales</h4>
<p>La télévision turque comprend huit chaînes publiques et 13 chaînes privées. 10 chaînes étrangères (américaines, françaises, allemandes, anglaises, espagnole et italienne) sont reçues sur le câble.</p>
<p>Le cinéma propose surtout des films américains récents, en VO, mais également quelques films français, espagnols et anglais en VO.</p>
<p>Ankara compte de nombreuses salles de cinéma, plus ou moins confortables, cinq théâtres qui présentent, chacun, une pièce turque par saison. Des concerts, des spectacles de ballet et d’opéra sont présentés régulièrement. De nombreuses expositions ont lieu au Musée d’art moderne, à la Bibliothèque nationale, dans les galeries et souvent dans les banques (Fondations).</p>
<p>A Istanbul la vie culturelle est aussi intense que variée. Même en sortant chaque soir, un expatrié français ne pourrait suivre toutes les manifestations culturelles qui y ont lieu. Ces manifestations, généralement en langue locale, sont souvent de très bonne qualité.</p>
<p>Il existe plusieurs radios d’Etat et plus de mille stations privées.</p>
<h3 class="spip"><a id="sommaire_3"></a>Sports</h3>
<p>De nombreux sports (tennis, équitation, golf…) peuvent être pratiqués, au sein de clubs ou d’associations, nombreux mais assez chers. Les grands hôtels disposent de piscines accessibles moyennant un abonnement.</p>
<p>Il existe plusieurs petites stations de ski à trois heures de route environ d’Ankara. Le matériel peut être loué sur place, avec un bon rapport qualité-prix.</p>
<p>Les rencontres sportives sont fréquentes pour les sports les plus populaires : football, surtout, mais également basketball, volleyball, handball, hockey sur glace.</p>
<h3 class="spip"><a id="sommaire_4"></a>Télévision – Radio</h3>
<p>Les chaînes de télévision publiques et privées retransmettent des films, des séries, des magazines, des documentaires et des variétés français, achetés auprès de distributeurs français.</p>
<ul class="spip">
<li>TV5 Monde, Euronews et France 24 sont reçues sur les réseaux câblés ou par satellite. France 2 et Arte peuvent être captées par antenne parabolique à l’ouest de la Turquie.</li>
<li>Il est bien entendu possible d’écouter les radios françaises publiques ou privées sur le Net ; certaines sont disponibles en sous porteuse de TV5 Monde en réception par satellite.</li></ul>
<h3 class="spip"><a id="sommaire_5"></a>Presse française</h3>
<p>La presse française est disponible dans les kiosques, avec un décalage de 24 à 48h, à un prix majoré (environ le double).</p>
<p>A Ankara, une librairie implantée dans les locaux l’<a href="http://www.ccclank.com/" class="spip_out" rel="external">Institut français</a> diffuse certains journaux et des ouvrages.</p>
<p>A Istanbul, la Librairie EFY (Istiklal Caddesi N° 8, Taksim) et la Librairie Française (Siraselviler Caddesi, Beyoglu) assurent la diffusion d’ouvrages français.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/vie-pratique/article/loisirs-et-culture). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
