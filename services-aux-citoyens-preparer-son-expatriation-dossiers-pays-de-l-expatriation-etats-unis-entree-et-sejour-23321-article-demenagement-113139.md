# Déménagement

<p>Un déménagement vers les Etats-Unis peut se faire par avion ou par bateau (compter quelques jours d’acheminement par avion et trois à six semaines d’acheminement par bateau).</p>
<p>Les coûts sont moins onéreux si le déménagement est effectué par bateau.</p>
<p>Afin d’éviter des frais d’entreposage trop importants, il convient de prévoir l’arrivée du déménagement au moins une quinzaine de jours après son arrivée.</p>
<p>Les sommes supérieures à 10 000$ doivent être déclarées à la douane. La douane américaine est très vigilante sur les produits alimentaires : les produits non stérilisés (tels que fromages ou charcuterie), ainsi que les plantes vertes sont rigoureusement interdits.</p>
<p>Pour connaître les produits interdits à l’importation ainsi que toutes les informations concernant l’importation de biens personnels, nous vous conseillons de consulter le <a href="http://www.cbp.gov/travel/international-visitors" class="spip_out" rel="external">site des douanes américaines</a>. Vous pouvez contacter directement les douanes américaines du port d’arrivée :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.cbp.gov/trade" class="spip_out" rel="external">La liste des ports d’entrée aux USA</a></p>
<p>S’agissant d’un déménagement international, il est primordial de s’entourer d’un maximum de garanties en faisant appel à un professionnel disposant de certifications reconnues (marque NF Service, ISO 9002, etc.).</p>
<p><a href="http://www.csdemenagement.fr/" class="spip_out" rel="external">Chambre syndicale du déménagement</a> <br class="manualbr">Téléphone : 01 49 88 61 40 <br class="manualbr">Télécopie : 01 49 88 61 46<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/entree-et-sejour-23321/article/demenagement-113139#contact#mc#csdemenagement.fr#" title="contact..åt..csdemenagement.fr" onclick="location.href=mc_lancerlien('contact','csdemenagement.fr'); return false;" class="spip_mail">Courriel</a></p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li>Notre thématique <a href="services-aux-citoyens-preparer-son-expatriation-douanes.md" class="spip_in">Douanes</a> ;</li>
<li>Consulter le <a href="http://www.douane.gouv.fr" class="spip_out" rel="external">site de la douane française</a> ;</li>
<li><a href="http://www.fidi.org/" class="spip_out" rel="external">FIDI</a> (choisir "country : France" pour obtenir la liste des déménageurs internationaux).</li></ul>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/entree-et-sejour-23321/article/demenagement-113139). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
