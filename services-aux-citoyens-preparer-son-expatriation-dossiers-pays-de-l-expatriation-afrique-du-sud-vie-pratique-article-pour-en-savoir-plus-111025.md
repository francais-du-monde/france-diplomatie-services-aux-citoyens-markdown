# Pour en savoir plus

<p>Suggestions de lecture pour mieux comprendre l’histoire du pays et la culture sud-africaine :</p>
<ul class="spip">
<li>Nelson Mandela : <i>Un long chemin vers la liberté</i></li>
<li>André Brink : <i>Une saison blanche et sèche</i></li>
<li>J.M. Coetzee : <i>En attendant les barbares</i></li>
<li>Rian Malan : <i>Mon cœur de traître : le drame d’un Afrikaner</i></li>
<li>Solomon Tshikisho Plaatje : <i>Mhudi</i></li>
<li>Alan Paton : <i>Pleure, ô pays bien-aimé</i></li>
<li>Nadine Gordimer : <i>Ceux de July</i></li></ul>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/vie-pratique/article/pour-en-savoir-plus-111025). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
