# Transports

<p>En train ou en bus, il est possible de se déplacer partout c’est-à-dire dans toutes les régions de Suisse, dans de bonnes conditions, en toute sécurité et ponctualité. Les <a href="http://www.cff.ch/home.html" class="spip_out" rel="external">Chemins de fer fédéraux</a> (CFF) couvrent aussi le réseau bus et bateaux. Pour tout renseignement, consulter les CFF et les offices de tourisme.</p>
<p><i>Mise à jour : février 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/vie-pratique/article/transports-105257). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
