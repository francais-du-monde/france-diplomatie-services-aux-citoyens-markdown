# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/vie-pratique/article/cout-de-la-vie-110069#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/vie-pratique/article/cout-de-la-vie-110069#sommaire_2">Budget</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/vie-pratique/article/cout-de-la-vie-110069#sommaire_3">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>L’unité monétaire est le franc CFA.</p>
<p>Le franc CFA vaut 0,00152449 euros, c’est-à-dire qu’un euro équivaut à 655,96 francs CFA.</p>
<p>Le Sénégal appartient à la zone franc (dont la parité est établie par rapport à l’euro) et fait partie des Etats membres de l’Union économique et monétaire d’Afrique de l’Ouest (UEMOA).</p>
<p>Il est possible de se procurer des francs CFA par le change sur place, par la voie bancaire ou directement de France grâce à un compte en francs convertibles ouvert auprès d’un établissement financier agréé ou encore par l’intermédiaire de banques étrangères. Le rapatriement des revenus et les transferts de fonds entre pays de la zone franc s’effectuent librement sous réserve d’être confiés à des intermédiaires agréés, comme les banques.</p>
<p>Les banques sont ouvertes du lundi au vendredi de 7h45 à 18h00, avec parfois une interruption le vendredi. Les principales banques françaises implantées dans le pays sont le Crédit Lyonnais et les groupes français associés à des banques locales : BICIS (BNP), SGBS (Société Générale), CMS (Crédit Mutuel), CBAO. La principale banque étrangère implantée est la Citibank.</p>
<p>Les cartes bancaires sont d’un usage très limité (frais élevés), celui des chèques est plus répandu. Les paiements quotidiens se font essentiellement en liquide ou par chèques établis à partir d’un compte ouvert localement.</p>
<h3 class="spip"><a id="sommaire_2"></a>Budget</h3>
<p>A Dakar, les conditions d’approvisionnement sont bonnes et les pénuries rares et limitées. A Saint-Louis, l’approvisionnement est moins varié et les pénuries en produits frais (fruits, légumes, viandes) plus fréquentes, surtout en période d’hivernage (juillet à novembre). Les prix des denrées peuvent subir des augmentations assez sensibles en période d’hivernage.</p>
<p>Les mesures parafiscales en faveur de la stabilisation des denrées alimentaires de base au printemps, en lien avec la modération des prix des carburants, ont permis la stabilisation du budget mensuel dédié à l’alimentation, évalué entre 80.000 et 120.000 XOF.</p>
<p>En matière de santé, les dépenses fluctuent fortement en fonction des besoins individuels. En l’absence de traitement spécifique, ces dépenses mensuelles pour une personne âgée peuvent être évaluées entre 15.000 et 30.000 XOF.</p>
<p>Enfin, concernant le pôle habillement, le budget mensuel peut être évalué entre 10.000 et 25.000 XOF.</p>
<p><strong>Pour en savoir plus</strong> : <a href="http://dakar-ca-bouge.net/-Le-cout-de-la-vie-" class="spip_out" rel="external">Dakar-ca-bouge.net</a></p>
<h3 class="spip"><a id="sommaire_3"></a>Evolution des prix</h3>
<p>L’Indice harmonisé des prix à la consommation (IHPC) a connu au premier semestre une hausse de 0,5% en rythme annuel. Sur l’année 2013, ce chiffre devrait atteindre 1,2% estime le FMI. Cette stabilité des prix par rapport à 2012 en raison de la campagne agricole ayant permis de ravitailler suffisamment le marché en produits frais qui a été favorable à la maitrise des prix des produits alimentaires (+0,7%) et de la détente des prix de la communication (-4,7%). Cette donnée masque cependant l’évolution à la hausse des prix de certains postes notamment la santé (+2,7%) et le poste « restauration - hôtellerie » (+7,6%).</p>
<p>Pour en savoir plus, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/vie-pratique/article/cout-de-la-vie-110069). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
