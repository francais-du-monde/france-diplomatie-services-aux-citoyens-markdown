# Vaccination

<p>Selon <a href="http://www.who.int/countries/swe/en/" class="spip_out" rel="external">l’Organisation Mondiale de la Santé</a>, il n’est pas nécessaire de se vacciner contre une maladie particulière pour entrer en Suède.</p>
<p>Cependant, les vaccinations de base doivent être à jour. Cela inclut : rougeole-oreillons-rubéole (ROR), le vaccin diphtérie tétanos coqueluche, la varicelle, la polio et le vaccin annuel contre la grippe</p>
<p>En Suède, il existe un programme de vaccination systématique des enfants, et ce contre neuf maladies : diphtérie, coqueluche, poliomélyte, rougeole, oreillons, rubéole, les infections causées par l’haemophilus grippal de type B, les pneumocoques, et maintenant, pour les filles, la vaccination contre le HPV (Human Papillomavirus).</p>
<p>Voir également la <a href="http://wwwnc.cdc.gov/travel/destinations/traveler/none/sweden" class="spip_out" rel="external">fiche Suède</a> du Center for Disease Control  Prevention.</p>
<p>Sur les maladies et leur fréquence en Suède :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.vaccination.nu/vaccin/SE.cfm" class="spip_out" rel="external">Vaccination.nu</a></p>
<p><strong>Les maladies à risque faible/modéré en Suède :</strong></p>
<ul class="spip">
<li>Diphtérie (rare)</li>
<li>Hépatite A</li>
<li>Hépatite B</li>
<li>Méningocoque</li>
<li>Pneumocoque</li>
<li>Poliomyélite (rare)</li>
<li>Tétanos</li>
<li>Tuberculose</li>
<li>Turista</li></ul>
<p><strong>Maladies inexistantes en Suède :</strong></p>
<ul class="spip">
<li>Fièvre jaune</li>
<li>Encéphalite Japonaise</li>
<li>Choléra</li>
<li>Malaria</li>
<li>Rage</li>
<li>Fièvre Typhoïde</li></ul>
<p>Il existe un risque d’encéphalite à tiques (TBE) sur la côte Ouest de la Suède, dans l’archipel de Stockholm, sur la côte Ouest du Lac Mälaren, sur les bords des lacs Vänern et Vättern et dans les zones rurales en été, qui est transmise par les tiques.</p>
<p>Il est donc recommandé de se vacciner contre le TBE.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Notre rubrique thématique [-rub22117]</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/entree-et-sejour/article/vaccination-113518). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
