# République Tchèque

<p>Au 31 décembre 2014, 3667 Français étaient inscrits au registre des Français établis hors de France tenu par l’Ambassade de France à Prague. On estime à environ 3800 le nombre de Français établis en République tchèque. Après une forte progression enregistrée ces dernières années, l’importance numérique de la communauté française semble se stabiliser.</p>
<p>La communauté française se caractérise par sa jeunesse (75 % des inscrits ont moins de 40 ans) et son dynamisme. Il s’agit souvent de cadres ou de personnes exerçant une profession intellectuelle. Un grand nombre d’entre eux sont employés par des sociétés françaises implantées dans le pays.</p>
<p>Les ressortissants français résident majoritairement à Prague et dans ses environs (70 %), en Bohême (20 %) et en Moravie (10 %). Les principales villes de province où sont établis des Français sont Brno en Moravie méridionale, Plzen en Bohême occidentale, Olomouc et Ostrava, toutes deux situées en Moravie septentrionale.</p>
<p>Les entreprises françaises implantées dans le pays (près de 500 filiales) représentent des secteurs d’activité très variés : agro-alimentaire, matériaux de construction biens d’équipement, construction, banque, environnement, industrie automobile…</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/dossiers-pays/republique-tcheque/" class="spip_in">Une description de la République tchèque , de sa situation politique et économique</a> ;</li>
<li><a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/republique-tcheque/" class="spip_in">Des informations actualisées sur les <strong>conditions locales de sécurité</strong> en République tchèque</a> ;</li>
<li><a href="http://www.france.cz/" class="spip_out" rel="external">Ambassade de France en République tchèque</a>.</li></ul>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-tcheque-entree-et-sejour.md">Entrée et séjour</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-tcheque-entree-et-sejour-article-passeport-visa-permis-de-travail-111331.md">Passeport, visa, permis de travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-tcheque-entree-et-sejour-article-demenagement.md">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-tcheque-entree-et-sejour-article-vaccination-111333.md">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-tcheque-entree-et-sejour-article-animaux-domestiques-111334.md">Animaux domestiques</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-tcheque-emploi-stage.md">Emploi, stage</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-tcheque-emploi-stage-article-marche-du-travail-111335.md">Marché du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-tcheque-emploi-stage-article-reglementation-du-travail.md">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-tcheque-emploi-stage-article-recherche-d-emploi-111337.md">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-tcheque-emploi-stage-article-curriculum-vitae.md">Curriculum vitae</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-tcheque-emploi-stage-article-lettre-de-motivation.md">Lettre de motivation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-tcheque-emploi-stage-article-stages.md">Stages</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-tcheque-protection-sociale.md">Protection sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-tcheque-protection-sociale-article-regime-local-de-securite-sociale-111342.md">Régime local de sécurité sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-tcheque-protection-sociale-article-convention-de-securite-sociale.md">Convention de sécurité sociale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-tcheque-fiscalite-23043.md">Fiscalité</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-tcheque-fiscalite-23043-article-fiscalite-du-pays-111344.md">Fiscalité du pays</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-tcheque-fiscalite-23043-article-convention-fiscale-111345.md">Convention fiscale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-tcheque-vie-pratique.md">Vie pratique</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-tcheque-vie-pratique-article-logement-111346.md">Logement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-tcheque-vie-pratique-article-sante.md">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-tcheque-vie-pratique-article-scolarisation.md">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-tcheque-vie-pratique-article-cout-de-la-vie-111349.md">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-tcheque-vie-pratique-article-transports-111350.md">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-tcheque-vie-pratique-article-communications-111351.md">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-tcheque-vie-pratique-article-loisirs-et-culture-111352.md">Loisirs et culture</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-tcheque-presence-francaise.md">Présence française</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-tcheque-presence-francaise-article-presence-francaise.md">Présence française</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
