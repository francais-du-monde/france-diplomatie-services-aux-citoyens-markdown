# Etudes supérieures

<p>La majorité des étudiants partent à l’étranger dans le cadre d’un programme et peuvent obtenir des informations utiles auprès du bureau des relations internationales de leur établissement d’enseignement supérieur.</p>
<p>Si vous vous inscrivez directement auprès d’un établissement à l’étranger, vous devrez effectuer les démarches vous-même. <br class="autobr"><strong>Pensez à vous renseigner sur votre protection sociale sur place.</strong> En effet, vous ne serez plus couvert par la sécurité sociale française si vous n’êtes pas inscrit dans un établissement français.</p>
<p>Auprès de qui obtenir des informations sur les études à l’étranger ? Les diplômes étrangers sont-ils reconnus en France ? Existe-t-il des aides financières pour les projets d’études ?</p>
<p>Vous trouverez, dans ces rubriques, des contacts et des liens pour affiner votre projet d’études à l’étranger ainsi que des informations sur les opportunités de financement d’études supérieures.</p>
<p class="texte texte118143">
<dl class="spip_document_84241 spip_documents spip_documents_center">
<dt>
<img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L1000xH190/Etudes-Algerie-1-1000x190_cle0581bf-ef4ed.jpg" width="1000" height="190" alt="JPEG - 118.4 ko"></dt>
</dl>
</p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures-financer-ses-etudes.md">Financer ses études </a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures-financer-ses-etudes-article-programmes-proposes-par-les-organismes-d-echanges.md">Programmes proposés par les organismes d’échanges</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures-financer-ses-etudes-article-programmes-proposes-par-les-organismes-publics-francais.md">Programmes proposés par les Organismes publics français</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures-financer-ses-etudes-article-programmes-des-autres-ministeres.md">Programmes des autres ministères</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures-financer-ses-etudes-article-programmes-proposes-par-les-conseils-regionaux.md">Programmes proposés par les Conseils régionaux</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures-financer-ses-etudes-article-programmes-proposes-par-les-13243.md">Programmes proposés par les Conseils départementaux</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures-s-informer-sur-les-etudes-a-l-etranger.md">S’informer sur les études à l’étranger</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures-s-informer-sur-les-etudes-a-l-etranger-article-etudier-a-distance.md">Etudier à distance</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures-s-informer-sur-les-etudes-a-l-etranger-article-reconnaissance-des-diplomes-etrangers-en-france.md">Reconnaissance des diplômes étrangers en France</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures-s-informer-sur-les-etudes-a-l-etranger-article-la-reconnaissance-des-diplomes-au-sein-de-l-ue.md">La reconnaissance des diplômes au sein de l’UE</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures-s-informer-sur-les-etudes-a-l-etranger-article-centres-nationaux-de-ressources.md">Centres nationaux de ressources </a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures-carnet-d-adresses.md">Carnet d’adresses</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/etudes-superieures/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
