# Stage

<p>En vertu de l’accord bilatéral entre la Suisse et l’UE/AELE (libre circulation des personnes), les ressortissants des 15 anciens États de l’UE (la France en fait partie) ainsi que de la Norvège, n’ont plus besoin d’une autorisation de travail formelle. En raison de cette amélioration du statut juridique, les accords sur l’échange de stagiaires avec ces pays ont été suspendus.</p>
<p>En d’autres termes, effectuer un stage en Suisse répond aux mêmes exigences définies pour un stagiaire dans l’Hexagone. Concrètement, le futur stagiaire doit trouver lui-même l’employeur et parfois négocier une rémunération qui n’est pas automatique. Si désormais, tout ressortissant français ne nécessite plus de permis ou d’autorisation de travail pour effectuer un stage en Suisse, l’employeur sera tenu de l’annoncer auprès des autorités cantonales de la population ou l’Office du contrôle de l’habitant. Le stagiaire devra également se présenter à cette administration pour une simple déclaration et enregistrement.</p>
<p>Le choix de la rémunération appartient à l’employeur. Ce dernier n’est pas tenu de rémunérer le stagiaire. En cas de versement de salaire, les règles du contrat de travail s’appliqueront (cotisations sociales obligatoires, horaires, cadre de travail, etc.). Lorsque le stage est non rémunéré, il faut absolument être couvert en cas d’accident du travail, assurance que l’employeur est tenu de souscrire. Le stagiaire peut également négocier des avantages en nature tels que le paiement de ses déjeuners, du transport, etc.</p>
<p>Il n’existe pas en Suisse d’organisme national qui centralise les offres de stage pour les ressortissants de l’Union européenne. La démarche est donc individuelle.</p>
<p>Parmi les programmes de stages européens, on peut citer :</p>
<ul class="spip">
<li><a href="http://www.leonardo-da-vinci.ch" class="spip_out" rel="external">Leonardo da Vinci en Suisse</a></li>
<li><a href="http://www.eurodyssee.net" class="spip_out" rel="external">Eurodyssée</a></li></ul>
<p><strong>Ressortissants éligibles à l’assurance chômage</strong></p>
<p>En Suisse, le stage professionnel est une mesure professionnelle de l’assurance chômage, sous forme d’emploi temporaire. Le stage peut être effectué dans le secteur privé ou au sein d’une administration publique. Le stage se décline sous deux formes : réaliser ses premières expériences professionnelles et/ou approfondir ses connaissances. Il est également l’occasion de nouer des contacts et de se constituer un réseau.</p>
<p>Sa durée est de minimum six mois ou plus et il sera interrompu en cas de l’obtention d’un emploi correspondant au profil du stagiaire.</p>
<p>Pendant le stage professionnel, l’assurance-chômage continue de verser les indemnités journalières à hauteur de 75%. A son terme, elle facture à l’entreprise 25% du montant des indemnités journalières versées.</p>
<p><strong>Volontariat international en administration ou en entreprise (V.I.A. ou V.I.E.)</strong></p>
<p>Le volontariat international (V.I.) permet de partir à l’étranger pour une mission professionnelle tout en bénéficiant d’un statut public protecteur. Le V.I. n’est pas du bénévolat. Il perçoit mensuellement une indemnité forfaitaire, variable suivant le pays d’affectation, mais indépendante du niveau de qualification.</p>
<p>Le V.I. s’adresse aux jeunes de 18 à 28 ans, qu’ils soient étudiants, jeunes diplômés, ou demandeurs d’emploi. Il faut être de nationalité française ou d’un autre pays européen et en règle avec les obligations de service national de votre pays.</p>
<p>Le V.I. est une expérience professionnelle enrichissante et demeure un véritable tremplin pour une carrière internationale.</p>
<p>La durée du V.I. est de 6 à 24 mois et les missions s’effectuent :</p>
<ul class="spip">
<li>en entreprise ;</li>
<li>au sein d’une structure française, publique ou parapublique, relevant du ministère des Affaires étrangères ou du ministère de l’Economie et des Finances ;</li>
<li>dans une structure publique locale étrangère (gouvernement, ministères, municipalités, centres de recherche et universités publiques) ;</li>
<li>auprès d’organisations internationales ou d’associations agréées.</li></ul>
<p>Les offres de volontariat international en entreprise sont plus nombreuses que celles dans les administrations relevant du ministère des Affaires étrangères ou du ministère de l’Economie et des Finances.</p>
<p>Tous les métiers sont concernés :</p>
<ul class="spip">
<li>en entreprise : finances, marketing, commerce international, contrôle de gestion, comptabilité, mécanique, électronique, télécommunications, informatique, BTP, agronomie, tourisme, droit, ressources humaines…</li>
<li>en administration : animation culturelle, enseignement, veille économique, commerciale ou scientifique, informatique, sciences politiques, droit, économie, recherche, médecine, hôtellerie-restauration…</li></ul>
<p>Les grandes entreprises déposent également leurs offres de stages ou de V.I. directement sur leur site internet.</p>
<p><strong>Sélection</strong></p>
<p>Le dépôt des candidatures et la consultation des offres de mission à l’étranger se font <strong>uniquement</strong> sur le site Internet du <a href="http://www.civiweb.com/FR/index.aspx" class="spip_out" rel="external">CIVI</a>.</p>
<p>Toutefois, ce sont les entreprises qui sélectionnent le futur V.I. Le processus de sélection répond aux mêmes critères d’exigence que ceux d’une recherche d’emploi classique. Une attention toute particulière doit être portée à la rédaction du CV et de la lettre de motivation. Les entreprises font généralement appel aux V.I. plutôt qu’au recrutement d’un candidat sous contrat à durée indéterminée. Par le biais du V.I., les entreprises pourront ainsi tester la personne pour ensuite l’engager à l’issue du stage en cas d’entière satisfaction. Un bon esprit d’initiative et une forte motivation sont fortement souhaités. Les entreprises sélectionneront en priorité les diplômé(e)s titulaires d’un BAC + 5. 70% des stagiaires se sont vus offrir un emploi au sein de l’entreprise où ils ont effectué leur V.I.</p>
<p><a href="http://www.businessfrance.fr" class="spip_out" rel="external">Business France</a> est l’Agence française publique pour le développement international des entreprises, placée sous la tutelle du ministre de l’Économie, de l’Industrie et de l’Emploi, du secrétaire d’Etat chargé du Commerce extérieur, et de la direction générale du Trésor et de la Politique économique français. Les missions économiques-Business France, présentes dans 28 pays, facilitent la découverte des marchés étrangers et détectent les possibilités d’implantation offertes aux entreprises. Business France promeut également le V.I. en entreprise (V.I.E.).</p>
<p><i>Mise à jour : février 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/emploi-stage/article/stage). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
