# Emploi, stage

<h2 class="rub22879">Marché du travail</h2>
<p class="chapo">
    La récession de l’économie au cours des six dernières années se reflète sur le marché du travail : un nombre croissant d’entreprises a été amené à réduire ses effectifs, entrainant une envolée du taux de chômage ; les emplois à plein temps et à durée indéterminée auparavant prédominants ont été au fil de la crise remplacés par des contrats à durée déterminée ou à temps partiel ; tandis que le travail non déclaré s’est fortement développé.
</p>
<p>Le taux de travailleurs indépendants est le plus élevé d’Europe : 32% selon Eurostat (contre 15,1% dans la Zone euro et 15,2% dans l’Union européenne), dont 22,4% exercent une fonction d’employeur. Les diplômés de l’enseignement supérieur de troisième cycle exercent plus souvent des fonctions d’employeur, tandis que les personnes disposant d’un faible niveau d’éducation sont sous-représentées dans le groupe des employeurs.</p>
<p>Deux phénomènes viennent "griser" les études sur les indicateurs du marché grec : l’existence d’une économie informelle et l’immigration illégale. L’économie informelle est estimée à 23,6% du PIB en 2013. L’immigration illégale représenterait 15% de la main d’œuvre active. C’est une main d’œuvre peu qualifiée qui occupe principalement des emplois manuels. On la rencontre dans l’agriculture, l’industrie, le bâtiment, les travaux saisonniers et les services domestiques. Les immigrés résidant en Grèce constituent 9,5% des actifs.</p>
<p><strong>Evolution du chômage et prévisions</strong> (source : Commission européenne)</p>
<table class="spip">
<thead><tr class="row_first"><th id="id6fd0_c0">2010 </th><th id="id6fd0_c1">2011 </th><th id="id6fd0_c2">2012 </th><th id="id6fd0_c3">2013 </th><th id="id6fd0_c4">2014 </th><th id="id6fd0_c5">2015 </th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="id6fd0_c0">12,1 %</td>
<td headers="id6fd0_c1">17,7 %</td>
<td headers="id6fd0_c2">24,3 %</td>
<td headers="id6fd0_c3">27 %</td>
<td headers="id6fd0_c4">26 %</td>
<td headers="id6fd0_c5">24 %</td></tr>
</tbody>
</table>
<p>Pour pallier la rigidité du marché, les autorités ont pris une série de mesures en 2012 :</p>
<p><strong>Baisse du salaire minimum</strong> de la Convention nationale du travail de 22% (par rapport aux salaires en vigueur au 1er janvier 2012) pour tous les échelons (ancienneté et situation familiale), de 32% pour les jeunes de moins de 25 ans. Ces derniers passent d’un salaire brut de 751€ à 510€.</p>
<p><strong>Durée de vie de trois ans des conventions collectives</strong>.</p>
<p><strong>Durée des conventions collectives</strong> existantes réduite à un an.</p>
<p><strong>Maintien des clauses de la convention collective</strong> de branche arrivée à terme pour une durée de trois mois (au lieu des six mois prévus par la loi antérieure) ; en cas de non conclusion d’un nouvel accord collectif ou individuel dans ce délai, le salaire de base sera appliqué alors que les primes d’ancienneté, de famille, d’études et de travail insalubre seront maintenues jusqu’à la mise en vigueur d’une nouvelle convention collective ou d’un accord individuel.</p>
<p><strong>Gel des ajustements salariaux</strong> : les conventions collectives, actes réglementaires ou décisions d’arbitrage prévoyant une hausse des salaires, y compris du fait de l’ancienneté, sont suspendus jusqu’à ce que le taux de chômage recule à un taux inférieur à 10%.</p>
<p><strong>Suppression de la pérennité de l’emploi dans les entreprises publiques.</strong> En vue d’harmoniser les conditions de travail des entreprises publiques au marché du travail du secteur privé, les clauses relatives à la pérennité de l’emploi (contrats à durée déterminée arrivant à terme à un âge déterminé ou à la retraite) prévues par la loi ou des conventions collectives sont abrogées.</p>
<p><strong>Interdiction du recours unilatéral à l’arbitrage.</strong> Ce dernier ne peut traiter que les questions afférentes à la détermination du salaire minimum. Les recours unilatéraux déposés avant la date d’émission de l’acte du Conseil des ministres sont classés.</p>
<p><strong>Suppression de la valeur universelle de la Convention collective du travail</strong> ; le montant du revenu minimum relèvera d’une loi présentée par le gouvernement. Le salaire mensuel minimum reste inchangé (conforme à la loi 4046/2012 soit 510€ pour les moins de 25 ans et 586€ pour les plus de 25 ans).</p>
<p><strong>Le délai de préavis de licenciement des salariés nouvellement recrutés est limité à quatre mois</strong> (six mois auparavant) ; celui des salariés ayant 16 ans et plus de service ce délai est de 12 mois.</p>
<p><strong>Les indemnités des salariés de plus de 16 ans de service sont gelées</strong> au niveau acquis à la date d’entrée en vigueur des mesures, indépendamment des années qu’ils accompliront d’ici leur éventuel licenciement ; au-delà de l’indemnité de 12 mois de salaires, l’indemnité ne peut dépasser un mois de salaire plafonné à 2000€ par année supplémentaire.</p>
<p><strong>La prime de mariage est supprimée</strong> et les augmentations triennales sont gelées jusqu’au terme de l’application du PBMT.</p>
<p><strong>L’organisation du temps de travail</strong> est régie par les conventions sectorielles ou d’entreprise.</p>
<p><strong>La période minimale de repos quotidien est réduite</strong> à 11 heures (12 auparavant).</p>
<p><strong>Déconnexion</strong> entre les horaires d’ouverture des magasins et les horaires des salariés, qui commence une heure plus tôt ou s’achève une heure plus tard que l’horaire d’ouverture des magasins, sans préjudice des huit heures de travail conventionnelles.</p>
<p><strong>Mise en œuvre d’un travail hebdomadaire de six jours</strong> dans certaines branches (commerce de détail, notamment), sans rémunération ou temps de repos supplémentaires, dans la mesure où les 40h hebdomadaires sont respectées.</p>
<p><strong>Suppression de l’accord préalable</strong> de l’inspection du travail pour toute modification des conditions de travail des salariés, remplacé par une seule notification.</p>
<p>Les salariés représentent 62,9% de la population active (environ 3,36 millions de personnes) : 13,6% dans le secteur primaire, 15,8% dans le secondaire et 70,6% dans le tertiaire.</p>
<p><strong>Barèmes de rémunération </strong></p>
<p>Les employés sont généralement rémunérés pour leur travail sur une base mensuelle, hebdomadaire ou journalière. L’employeur verse le salaire après en avoir déduit les cotisations et contributions obligatoires de l’employé (sécurité sociale et impôt sur le revenu).</p>
<p>Par ailleurs, la loi prévoit que le salarié reçoit une prime de Noël égale à un mois de salaire, une prime de Pâques équivalente à un demi mois de salaire, et une allocation pour vacances d’un demi mois, soit, au total, 14 mois de salaire.</p>
<p>Le salaire minimum, auparavant objet d’un accord entre les partenaires sociaux, relève depuis la suppression de la valeur universelle de la Convention collective du travail (loi 4093/2012 portant application du Programme budgétaire à moyen terme 2013/2016), d’une loi présentée par le gouvernement.</p>
<p><strong>Une révision du système de salaire minimum est prévue au 1er trimestre 2014.</strong></p>
<p>Jusqu’à ce que le taux de chômage redescende à moins de 10%, l’effet des dispositions de lois, de règlements, de conventions collectives ou de décisions d’arbitrage prévoyant des augmentations salariales est suspendu. Cette mesure concerne les augmentations pour ancienneté dans l’entreprise, comme la prime de service, la prime des trois années et la prime des cinq années.</p>
<p>Les lois, les décisions réglementaires, les clauses de conventions collectives, les décisions d’arbitrage, les règlements intérieurs du personnel et les décisions des directions d’entreprises qui instituent des clauses d’emploi permanent, en dérogation aux règles générales du droit du travail ou qui prévoient l’application de dispositions du Code des agents de l’Etat sont supprimées. Ces dispositions s’appliquent également aux entreprises ou organismes relevant ou ayant relevé par le passé du secteur public.</p>
<p>Durée légale du temps de travail :  La durée du travail est en règle générale de 8 heures par jour / 40 heures par semaine. Elle est limitée à 48 heures par semaine (y compris les heures supplémentaires).</p>
<p>Période d’essai : l’existence d’une période d’essai et sa durée peuvent être indiquées au contrat. Elle peut durer jusqu’à un an.</p>
<p>Congés payés : ils sont de 20 jours par an pour les salariés qui travaillent cinq jours par semaine (24 jours pour ceux qui travaillent six jours par semaine). Ils sont de 21 jours la deuxième année et de 22 jours à partir de la 3ème année. Le salarié ayant plus de 10 ans d’ancienneté ou ayant 12 ans de carrière au total bénéficie de 25 jours de congé. Un jour supplémentaire est octroyé après 25 ans de service.</p>
<p>Congé maternité : les congés de maternité sont de 17 semaines au total (partagés avant et après l’accouchement). Pendant les 30 mois suivant la fin du congé de maternité, la salariée peut bénéficier d’un aménagement d’horaire, avec réduction du temps de travail.</p>
<p>Congé maladie : la rémunération du congé de maladie de courte durée est fonction du temps de service auprès du même employeur. Pendant la première année de travail, l’employé a droit à une rémunération correspondant à 15 jours travaillés. Jusqu’à quatre ans d’ancienneté, l’employé peut obtenir pour cause de maladie un congé d’un mois, entre 4 et 10 ans d’ancienneté : trois mois, de 10 à 15 ans : quatre mois et au-delà de 15 ans le congé est de six mois. Selon la jurisprudence, si l’employé dépasse les limites de maladie de courte durée, le contrat de travail n’est pas réputé résilié de plein droit du fait de l’employé (départ volontaire). La résiliation ou non du contrat sera appréciée par les tribunaux.</p>
<p>Retraite : Elle est fixée à 67 ans pour une retraite complète et à 62 ans pour une retraite minimale. Pour les professions pénibles ou malsaines, la retraite complète est accordée à 62 ans.</p>
<p><i>Mise à jour : novembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-grece-emploi-stage-article-stages.md" title="Stages">Stages</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-grece-emploi-stage-article-entretien-d-embauche.md" title="Entretien d’embauche">Entretien d’embauche</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-grece-emploi-stage-article-lettre-de-motivation.md" title="Lettre de motivation">Lettre de motivation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-grece-emploi-stage-article-curriculum-vitae-110380.md" title="Curriculum vitae">Curriculum vitae</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-grece-emploi-stage-article-recherche-d-emploi-110379.md" title="Recherche d’emploi">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-grece-emploi-stage-article-reglementation-du-travail-110378.md" title="Réglementation du travail">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-grece-emploi-stage-article-marche-du-travail-110377.md" title="Marché du travail">Marché du travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/emploi-stage/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
