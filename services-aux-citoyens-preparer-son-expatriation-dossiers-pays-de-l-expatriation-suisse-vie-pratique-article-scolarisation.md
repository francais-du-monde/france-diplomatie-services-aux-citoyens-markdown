# Scolarisation

<p><strong>Scolarisation dans le système français</strong></p>
<p><a href="http://www.efdb.ch/" class="spip_out" rel="external">Ecole française de Bâle</a> (homologuée)<br class="manualbr">Directeur : <strong>M.Alain WAGNER</strong><br class="manualbr">Engelgasse 103, 4052 Bâle<br class="manualbr">Tél : +41 (0)311 07 30<br class="manualbr">Fax : +41(0) 61 373 88 46<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> classes maternelles : petite, moyenne et grande sections<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> enseignement primaire : CP, CE1, CE2, CM1, CM2</p>
<p><a href="http://www.ecole-francaise-de-berne.ch/" class="spip_out" rel="external">Ecole française de Berne</a> (homologuée et conventionnée AEFE)<br class="manualbr">Directeur : <strong>M.Jean-André LAFONT</strong><br class="manualbr">Sulgenrain 11, 3007 Berne<br class="manualbr">Tél. : +41 (0)31 376 17 57<br class="manualbr">Fax : +41 (0)31 371 79 50<br class="manualbr">Courriel : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/vie-pratique/article/scolarisation#info#mc#ecole-francaise-de-berne.ch#" title="info..åt..ecole-francaise-de-berne.ch" onclick="location.href=mc_lancerlien('info','ecole-francaise-de-berne.ch'); return false;" class="spip_mail">info<span class="spancrypt"> [at] </span>ecole-francaise-de-berne.ch</a><br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> classes maternelles : petite, moyenne et grande sections<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> enseignement primaire : CP, CE1, CE2, CM1, CM2<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> enseignement secondaire : 6ème à 3ème</p>
<p><a href="http://www.ecole-francaise-geneve.ch/" class="spip_out" rel="external">Ecole française primaire de Genève</a> (homologuée)<br class="manualbr">Directeur : <strong>M.Roger DUPRAZ</strong><br class="manualbr">3, chemin des Vergers, 1208 Genève<br class="manualbr">Tél. : +41 (0)22 735 60 20<br class="manualbr">Fax : +41 (0)22 735 96 00<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/vie-pratique/article/scolarisation#epfg#mc#bluewin.ch#" title="epfg..åt..bluewin.ch" onclick="location.href=mc_lancerlien('epfg','bluewin.ch'); return false;" class="spip_mail">Courriel</a><br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> une classe maternelle : grande section<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> enseignement primaire : CP, CE1, CE2, CM1, CM2</p>
<p><a href="http://www.ecole-valmont.org/" class="spip_out" rel="external">Ecole française de Lausanne - Valmont</a> (homologuée)<br class="manualbr">Supérieure générale : <strong>sœur Hélène PEPE</strong><br class="manualbr">Chef d’établissement : <strong>M.Alain SULMON</strong><br class="manualbr">47 route d’Oron, 1010 Lausanne<br class="manualbr">Tél. : +41 (0)21 652 37 33<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> classes maternelles : petite, moyenne et grande sections<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> enseignement primaire : CP, CE1, CE2, CM1, CM2<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> enseignement secondaire : 6ème à terminale<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> baccalauréats préparés : L, ES et S</p>
<p><a href="http://www.lfz.ch/" class="spip_out" rel="external">Lycée français Marie Curie de Zurich</a> (homologué et conventionné AEFE)<br class="manualbr">Proviseur : <strong>MmeBrigitte RENN</strong><br class="manualbr"><strong>Ecole élémentaire</strong> : Im Tobelacker 22, 8044 Gockenhausen/ZH<br class="manualbr"><strong>Ecole maternelle</strong> : Auenstrasse 10, 8600 Dübendorf-Stettbach<br class="manualbr">Tél. : +41 (0)43 355 20 91<br class="manualbr">Fax : +41 (0)43 355 20 99<br class="manualbr"><strong>Collège et Lycée</strong> : Ursprungstrasse 10, 8044 Gockenhausen/ZH<br class="manualbr">Tél. : +41 (0)43 355 20 80<br class="manualbr">Fax : +41 (0)43 355 20 89</p>
<p>Pour toute information sur la scolarisation dans le système français à l’étranger, vous pouvez consulter notre <a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais.md" class="spip_in">thématique sur les études et la scolarisation</a>.</p>
<p>Vous y trouverez des renseignements sur :</p>
<ul class="spip">
<li>les établissements français du primaire et du secondaire à l’étranger ;</li>
<li>les bourses scolaires et la prise en charge des frais de scolarité à l’étranger ;</li>
<li>les possibilités qui s’offrent à vous si votre enfant ne peut être scolarisé à l’étranger dans le système français (enseignement à distance par le CNED, programme français langue maternelle (FLAM), internats en France) ;</li>
<li>les épreuves du baccalauréat à l’étranger ;</li>
<li>les bourses d’études supérieures en France et à l’étranger ;</li>
<li>l’équivalence des diplômes.</li></ul>
<p><strong>Enseignement supérieur</strong></p>
<p>Le système éducatif suisse comprend quatre niveaux : primaire, secondaire, tertiaire et supérieur. La scolarité obligatoire englobe le niveau primaire et la première partie du niveau secondaire (« degré secondaire I »). Au terme de la scolarité obligatoire, deux grandes voies de formation sont possibles :</p>
<ul class="spip">
<li>apprentissage professionnel pouvant être complété ultérieurement par le diplôme suisse de fin d’études secondaires appelé " la maturité " dans ce cas professionnelle ;</li>
<li>enseignement dans une école de maturité ou de culture générale.</li></ul>
<p>Ces deux options permettent ensuite l’accès soit aux hautes écoles spécialisées ou universitaires.</p>
<p>L’enseignement supérieur est quant à lui, caractérisé par une grande diversité institutionnelle, une offre de formation étendue alliant continuité et modernité, une qualité élevée de l’enseignement et de la recherche et une compétitivité internationale.</p>
<p>Cet enseignement trouve son prolongement avec d’un côté les examens professionnels fédéraux, les examens professionnels fédéraux supérieurs et les écoles supérieures (domaine tertiaire B) et, de l’autre, les hautes écoles universitaires et les hautes écoles spécialisées (domaine tertiaire A). Parallèlement, une solide offre de formation continue est proposée à tous les degrés.</p>
<p><strong>Établissements</strong></p>
<p>Des écoles privées existent également en Suisse. Dans le domaine de la formation professionnelle supérieure (tertiaire B), les écoles privées représentent un établissement sur deux. C’est dans celui de la formation continue que le secteur privé est le plus important.</p>
<p>Quant au système d’enseignement supérieur, il est constitué presque exclusivement de hautes écoles à caractère public financées principalement par la Confédération. Aucune université privée n’est reconnue sur le plan national. En revanche, le Conseil fédéral a octroyé pour la première fois en 2005 une autorisation à une haute école spécialisée privée.</p>
<p><strong>Les hautes écoles universitaires</strong></p>
<p>Les hautes écoles universitaires regroupent les universités et les écoles polytechniques fédérales. Leurs tâches essentielles sont l’enseignement, la recherche et les prestations de service. Chaque haute école universitaire jouit d’une grande autonomie académique, financière et organisationnelle, la coordination nationale étant assurée par la Conférence universitaire suisse.</p>
<p><strong>Les universités cantonales</strong></p>
<p>La Suisse compte dix universités cantonales. Cinq d’entre elles sont situées dans la partie germanophone du pays : Bâle, Berne, Lucerne, Saint-Gall et Zurich. Il existe trois universités romandes (Genève, Lausanne et Neuchâtel), une <a href="http://www.usi.ch/" class="spip_out" rel="external">université de la Suisse italienne</a> à Lugano et une université bilingue (français et allemand) à Fribourg. Université de Bâle est la plus ancienne du pays ; elle fut créée en 1460, suivie quelques décennies plus tard par celles de Genève et de Lausanne. La plus jeune, étant l’Université de la Suisse italienne, qui a ouvert ses portes en 1996.</p>
<p>La majorité des universités dispensent un enseignement en droit, en sciences économiques, en mathématiques et en sciences naturelles ainsi qu’en sciences humaines, sociales et linguistiques. La moitié d’entre elles disposent d’une filière médecine.</p>
<p><strong>Les écoles polytechniques fédérales (EPF)</strong></p>
<p>Les écoles polytechniques fédérales sont au nombre de deux : l’une à Zurich et l’autre à Lausanne et jouissent d’une excellente réputation.</p>
<p><strong>Les hautes écoles spécialisées (HES)</strong></p>
<p>On compte sept écoles de ce type réparties sur tout le territoire :</p>
<ul class="spip">
<li>Haute école spécialisée Bernoise</li>
<li>Haute école spécialisée de Suisse centrale</li>
<li>Haute école spécialisée de Suisse orientale</li>
<li>Haute école spécialisée du Nord-Ouest de la Suisse</li>
<li>Haute école spécialisée de la Suisse italienne</li>
<li>Haute école spécialisée de Suisse occidental</li>
<li>Haute école spécialisée zurichoise.</li></ul>
<p><strong>Les institutions</strong></p>
<p>Il existe un petit nombre d’institutions de renommée internationale qui dispensent de formation spécifiques débouchant sur un diplôme de niveau tertiaire. Elles sont subventionnées par la Confédération et parfois affiliées à une université. Nous pouvons citer :</p>
<ul class="spip">
<li><a href="http://graduateinstitute.ch/" class="spip_out" rel="external">Institut universitaire de hautes études internationales</a> à Genève (IUHEI)</li>
<li><a href="http://www.unil.ch/unisciences/IDHEAP" class="spip_out" rel="external">Institut de hautes études en administration publique</a> (IDHEAP) à Lausanne,</li>
<li><a href="http://www.iukb.ch/" class="spip_out" rel="external">Institut Universitaire Kurt Bösch</a> à Sion</li>
<li><a href="http://www.iued.unige.ch/" class="spip_out" rel="external">Institut universitaire d’études du développement</a> à Genève (IUED)</li></ul>
<p><strong>Écoles hôtelières</strong></p>
<p>Il existe à l’échelle nationale, un réseau d’écoles hôtelières réputées, membres de l’<a href="http://www.aseh.ch/" class="spip_out" rel="external">Association suisse des écoles hôtelières</a>.</p>
<p><strong>Ecoles d’horlogerie</strong></p>
<p>Nous pouvons citer : <br class="autobr">l’école d’horlogerie de Genève, l’école technique de la Vallée de Joux (<a href="http://www.etvj.vd.ch" class="spip_url spip_out auto" rel="nofollow external">www.etvj.vd.ch</a>),</p>
<ul class="spip">
<li><a href="http://www.zeitzentrum.ch" class="spip_out" rel="external">école d’horlogerie de Soleure</a>,</li>
<li><a href="http://www.bbz-biel.ch" class="spip_out" rel="external">Centre de Formation Professionnelle</a> Bienne,</li>
<li><a href="http://www.he-arc.ch" class="spip_out" rel="external">Haute Ecole Arc</a>,</li>
<li><a href="http://www.cpp.ch/" class="spip_out" rel="external">Centre professionnel de Porrentruy</a>.</li></ul>
<p><strong>Conditions d’admission</strong></p>
<p>Toutes les universités en Suisse ont adapté leurs filières de formation pour être en adéquation avec les accords de Bologne.</p>
<p>Toutes les filières ne sont pas accessibles aux travailleurs étrangers qui ne résident pas déjà sur le sol suisse (permis de séjour obtenu avant la demande d’admission) : c’est par exemple le cas des études de médecine.</p>
<p>Des tests de langues devront être passés dans certaines universités, et des examens d’admission sont demandés notamment lorsque le diplôme est partiellement reconnu. Ces examens concernent en premier lieu les étudiants étrangers. L’Université de St. Gall est la seule université suisse qui exige un examen d’admission pour les étudiants étrangers. La raison de cet examen est la limitation à 25% du nombre d’étudiants étrangers. Toutefois, les étudiants étrangers titulaires d’une maturité suisse reconnue sont exemptés de cet examen.</p>
<p>Le baccalauréat français donne accès aux universités suisses. Des mentions sont souvent exigées pour l’admission d’un bachelier. De même, la Maturité est admise en équivalence du baccalauréat français et permet de suivre des études supérieures en France. Dans tous les cas, se renseigner auprès des universités concernées sur les conditions d’admission, dont les critères sont fixés par pays. De nombreuses universités offrent la possibilité de s’inscrire en ligne.</p>
<p>La taxe universitaire annuelle pour les étudiants étrangers qui poursuivent leur étude à Genève est de l’ordre de 1000 CHF, de 1310 CHF à Berne, 1160 CHF à Lausanne et de 1578 CHF à Zurich. Quant au coût d’une année d’étude, il se situe entre 18 000 et 28 000 CHF et est en fonction du lieu de séjour et des prétentions individuelles.</p>
<p>Bourses :</p>
<p>La Suisse accorde des bourses aux étudiants étrangers titulaires d’un diplôme universitaire. Pour plus de renseignements, prendre contact avec la Commission fédérale des bourses pour étudiant(e)s et artistes étrangers (ESKAS) :<br class="manualbr">Hallwylstrasse 4, <br class="manualbr">CH-3003 Berne <br class="manualbr">Tél. +41 (0)31 323 26 76 <br class="manualbr">Fax +41 (0)31 232 30 20.</p>
<p><strong>Pour en savoir plus</strong></p>
<p>Au niveau fédéral :</p>
<ul class="spip">
<li>Département fédéral de l’intérieur (DFI) et du Département fédéral de l’économie (DFE) et plus particulièrement le Secrétariat d’Etat à l’éducation et à la recherche (SER) ;</li>
<li><a href="http://www.bbt.admin.ch/" class="spip_out" rel="external">Office fédéral de la formation professionnelle et de la technologie</a> (OFFT) ;</li>
<li><a href="http://www.bbt.admin.ch/" class="spip_out" rel="external">Commission fédérale des hautes écoles spécialisées</a> (CFHES) ;</li></ul>
<p>Au niveau cantonal :</p>
<p>• <a href="http://www.edk.ch/" class="spip_out" rel="external">Conférence des directeurs cantonaux de l’instruction publique</a> (CDIP)  <br class="autobr">• <a href="http://www.crus.ch/?L=1" class="spip_out" rel="external">Conférence des recteurs des universités suisses</a> (CRUS). Ce site dispense d’un panel d’informations telles que : les conditions et examens d’admission, la marche à suivre pour étudier en Suisse, le coût des études, les bourses, l’assurance-maladie, etc.</p>
<p><strong>Source </strong> : secrétariat d’Etat à l’éducation et à la recherche et Office fédéral de la formation professionnelle et de la technologie en collaboration avec Présence Suisse et la Conférence universitaire suisse (2006) et le CRUS.</p>
<p><i>Mise à jour : février 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/vie-pratique/article/scolarisation). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
