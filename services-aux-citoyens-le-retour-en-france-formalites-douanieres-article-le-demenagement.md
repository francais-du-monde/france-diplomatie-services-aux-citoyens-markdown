# Le déménagement

<p>Le déménageur ou le transitaire local que vous aurez chargé, après examen d’un devis estimatif, du transport de votre mobilier et de vos effets personnels, vous demandera d’en établir un <strong>inventaire détaillé</strong>. Une <strong>attestation de changement de résidence</strong> est souvent réclamée pour autoriser le transit en douane au départ. Si les autorités locales ne peuvent pas vous délivrer ce document, adressez-vous au consulat de France.</p>
<p>N’oubliez pas que la production d’un <strong>quitus fiscal</strong> ou bordereau de situation peut être exigée par les autorités administratives locales.</p>
<p><strong>Vous venez d’un État de l’Union européenne </strong></p>
<p>Vous n’avez pas de formalités douanières à accomplir.</p>
<p><strong>Vous venez d’un pays n’appartenant pas à l’Union européenne </strong></p>
<p>Vous pouvez bénéficier de la franchise (non paiement des droits et taxes) pour l’importation de vos biens personnels, si les conditions suivantes sont simultanément remplies :</p>
<ul class="spip">
<li>résidence à l’étranger depuis plus de 12 mois ;</li>
<li>utilisation et possession des biens depuis plus de six mois avant le transfert de résidence.</li></ul>
<p>Vous fournirez au service des douanes :</p>
<ul class="spip">
<li>un inventaire des biens (y compris les véhicules) détaillé, estimatif, daté et signé, en deux exemplaires ;</li>
<li>tout document prouvant que vous résidiez dans un pays tiers ;</li>
<li>tout document probant attestant que vous vous installez en France (certificat de changement de résidence, ordre de mutation, contrat de travail, carte de séjour, etc.).</li></ul>
<p>Si vous possédez des biens de valeur (antiquités, objets d’art, etc.) ou des biens exigeant l’accomplissement de formalités particulières, vous devrez remplir une déclaration spécifique que vous remettra le service des douanes.</p>
<p>Vous pouvez également remplir cette déclaration (Cerfa 10070*02) sur le portail <a href="http://www.douane.gouv.fr/" class="spip_out" rel="external">www.douanes.gouv.fr</a>.</p>
<p>En retour, la douane vous remettra :</p>
<ul class="spip">
<li>un exemplaire visé de l’inventaire ;</li>
<li>un certificat 846 A pour l’immatriculation du véhicule (série normale) ;</li>
<li>une carte de libre circulation établie sur votre demande.</li></ul>
<p>Les biens admis en franchise ne peuvent être cédés, loués ou prêtés pendant les 12 mois suivant leur importation en France. Dans le cas contraire, les impositions exigibles à l’importation seraient dues.</p>
<p><strong>Attention</strong> : Les animaux de selle, les motocycles, les véhicules automobiles et leurs remorques, les caravanes de camping, les bateaux de plaisance et les avions de tourisme doivent avoir été acquis toutes taxes comprises dans le pays d’origine ou de provenance pour bénéficier de la franchise. Dans tous les cas de transfert de résidence (intra-communautaire et de pays tiers) certains biens sont soumis à des formalités particulières : animaux familiers, armes et munitions, biens culturels, espèces de la faune et de la flore menacées d’extinction, médicaments et végétaux.</p>
<p>Pour en savoir plus, vous pouvez consulter le site Internet de la douane : <a href="http://www.douane.gouv.fr/" class="spip_url spip_out auto" rel="nofollow external">www.douane.gouv.fr/</a> Rubrique « Achats et tourisme  Vous déménagez  Vous vous installez en France métropolitaine  Vous souhaitez transférer en France votre résidence principale ».</p>
<p><a href="http://www.douane.gouv.fr/" class="spip_out" rel="external">Infos Douane Service</a><br class="manualbr">Depuis la France : <strong>0811 20 44 44</strong> de 8h30 à 18h<br class="manualbr">Hors Métropole ou depuis l’étranger : +33 1 72 40 78 50<br class="manualbr">Courriel : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/formalites-douanieres/article/le-demenagement#ids#mc#douane.finances.gouv.fr#" title="ids..åt..douane.finances.gouv.fr" onclick="location.href=mc_lancerlien('ids','douane.finances.gouv.fr'); return false;" class="spip_mail">ids<span class="spancrypt"> [at] </span>douane.finances.gouv.fr</a></p>
<p><i>Mise à jour : mars 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/formalites-douanieres/article/le-demenagement). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
