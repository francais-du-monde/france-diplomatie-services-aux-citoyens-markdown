# Régime local de sécurité sociale

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/protection-sociale-22904/article/regime-local-de-securite-sociale-110496#sommaire_1">Le détachement</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/protection-sociale-22904/article/regime-local-de-securite-sociale-110496#sommaire_2">L’expatriation</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/protection-sociale-22904/article/regime-local-de-securite-sociale-110496#sommaire_3">Le statut local</a></li></ul>
<p>Avant toute chose, sachez avec précision de quel statut vous dépendez. Détaché, expatrié ou local, vous trouverez ici les grandes lignes de chacun de ces statuts.</p>
<h3 class="spip"><a id="sommaire_1"></a>Le détachement</h3>
<p>Un salarié qui a un contrat de travail avec une entreprise, dont le siège social se situe en France, et qui se rend à sa demande à l’étranger, est considéré comme détaché par la sécurité sociale.</p>
<p><strong>Attention :</strong></p>
<p>La durée d’un contrat de travail en tant que détaché est de trois ans, renouvelable une fois. Le statut de détaché auprès de la Sécurité Sociale est de 12 mois renouvelable une fois.</p>
<ul class="spip">
<li>Le détachement a pour effet le maintien du salarié au régime de sécurité sociale français.</li>
<li>L’entreprise et le salarié continuent de cotiser en France pour tous les risques. Le régime de détachement ne s’applique pas de plein droit.</li></ul>
<p>Il n’est pas obligatoire, ce n’est qu’une option laissée à l’initiative de l’entreprise.</p>
<h3 class="spip"><a id="sommaire_2"></a>L’expatriation</h3>
<p>Est considéré comme expatrié par la sécurité sociale, le salarié qui :</p>
<ul class="spip">
<li>Part travailler à l’étranger de sa propre initiative</li>
<li>Est recruté directement par une entreprise étrangère, ou pour le compte d’une filiale étrangère.</li>
<li>Dépasse la durée du détachement.</li>
<li>Obtient le statut de salarié expatrié par son entreprise.</li></ul>
<p>L’expatrié doit s’affilier au régime de sécurité sociale singapourien s’il a le statut de <i>Permanent Resident (PR)</i>. Sinon, il devra souscrire auprès d’une assurance privée et/ou auprès d’une assurance volontaire en France. La Caisse des Français de l’étranger (CFE) est alors compétente. Couverture des risques : maladie, invalidité, maternité, accidents du travail, maladies professionnelles, vieillesse et retraite.</p>
<h3 class="spip"><a id="sommaire_3"></a>Le statut local</h3>
<p>Il dépend du contrat de travail que vous avez signé soit avec une société étrangère, soit avec la filiale locale d’une société étrangère.</p>
<p>La protection sociale à Singapour repose sur le <i>Central Provident Fund</i> (CPF). Il s’agit d’un système de capitalisation individuelle obligatoire pour les singapouriens et qui n’est pas ouvert aux étrangers non résidents permanents.</p>
<p>Initialement destiné à financer la retraite, il est maintenant partiellement utilisable pour financer l’achat d’un logement, les frais d’hospitalisation, etc.</p>
<p>Un autre régime, adapté aux non-Singapouriens n’ayant pas statut de PR, est le <i>Supplementary Retirement Scheme</i> (SRS). Employeur et employé peuvent cotiser.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/protection-sociale-22904/article/regime-local-de-securite-sociale-110496). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
