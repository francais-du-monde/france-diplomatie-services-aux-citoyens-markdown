# Communications

<h4 class="spip">Téléphone – Internet</h4>
<p>Les liaisons téléphoniques avec la France sont excellentes.</p>
<p><strong>Indicatifs</strong></p>
<p><strong>Depuis la France</strong> : 00 352 + le numéro d’abonné sans le " 0 " local.</p>
<p>Le Luxembourg ne dispose pas de préfixes locaux pour les différentes régions. Les numéros de téléphone se composent en général de six chiffres sauf pour les extensions des numéros standards des sociétés ou de l’Administration publique.</p>
<p><strong>Depuis le Luxembourg </strong> : 00 33 + le numéro d’abonné sans le " 0 " local (soit neuf chiffres).</p>
<p><strong>Utilisation d’un portable</strong></p>
<p>Vous pouvez utiliser votre téléphone portable français partout en Europe et dans de nombreuses autres régions du monde, grâce à la norme technique GSM de l’Union européenne. Les personnes qui utilisent leur téléphone portable à l’étranger réalisent d’importantes économies – de l’ordre de 60 % – grâce au règlement de l’UE sur l’itinérance (« roaming »), qui plafonne le coût des appels téléphoniques effectués ou reçus dans un autre pays de l’UE (« eurotarif »). De plus, une facturation à la seconde a été introduite après les premières 30 secondes pour les appels effectués, et dès la première seconde pour les appels reçus. Se renseigner auprès de son opérateur français sur les tarifs.</p>
<p>Les clients reçoivent un message automatique les informant du coût des appels, des SMS et des services de transmission de données à l’étranger (en cas de navigation sur internet ou de téléchargement d’un film, par exemple). Les prix de gros que les opérateurs se facturent mutuellement sont également plafonnés, ce qui devrait réduire les coûts supportés par les consommateurs. Un mécanisme a également été mis en place afin d’éviter aux consommateurs de devoir payer des factures astronomiques pour des services d’itinérance : après un premier avertissement, la connexion internet mobile est suspendue dès que le montant atteint un certain niveau. Une limite de 50 euros par mois s’applique par défaut si le client n’a pas défini un montant spécifique.</p>
<p>Toutefois, il est recommandé de souscrire à un abonnement auprès d’un opérateur luxembourgeois lorsque l’on s’installe dans le pays.</p>
<p>Vous trouverez sur <a href="http://ec.europa.eu/information_society/activities/roaming/tariffs/in_ms/index_en.htm" class="spip_out" rel="external">un site web de l’UE</a> les eurotarifs appliqués par les opérateurs des pays membres, ainsi que les liens vers leurs sites :</p>
<h4 class="spip">Téléphoner gratuitement par Internet </h4>
<p>Il est généralement très avantageux d’utiliser <strong>des logiciels de téléphonie sur IP</strong> (Skype, Google talk, Live messenger, Yahoo ! messenger, etc.) car les tarifs proposés à l’international sont souvent moindre que ceux des opérateurs locaux exploitant des lignes classiques, voire gratuits dans certaines conditions.</p>
<h4 class="spip">Poste</h4>
<p>Les liaisons postales sont excellentes.</p>
<p><strong>Pour plus d’informations : </strong></p>
<p><a href="http://www.post.lu/fr/particuliers/" class="spip_out" rel="external">Le site de la poste luxembourgeoise</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/vie-pratique/article/communications). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
