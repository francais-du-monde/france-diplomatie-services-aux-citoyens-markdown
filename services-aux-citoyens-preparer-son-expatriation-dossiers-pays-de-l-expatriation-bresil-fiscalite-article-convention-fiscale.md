# Convention fiscale

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/fiscalite/article/convention-fiscale#sommaire_1">Champ d’application de la convention</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/fiscalite/article/convention-fiscale#sommaire_2">Règles d’imposition</a></li></ul>
<p>La France et Brésil ont signé, le 10 septembre 1971, une Convention en matière de fiscalité publiée au Journal Officiel du 28 novembre 1972. Cette convention tend à éviter les doubles impositions en matière d’impôts sur les revenus.</p>
<p>Les dispositions conventionnelles qui ont primauté sur les dispositions du droit interne, selon l’article 55 de la Constitution française, répartissent entre les deux Etats le droit d’imposer les revenus perçus par leurs résidents respectifs. Voici un résumé de ses points clés pour les particuliers.</p>
<h3 class="spip"><a id="sommaire_1"></a>Champ d’application de la convention</h3>
<p>Cet accord a pour objet de protéger les résidents de chacun des Etats contractants en matière d’impôts prélevés directement sur le revenu ou sur les sociétés. La Convention trouve donc à s’appliquer aux résidents de ces Etats.</p>
<h3 class="spip"><a id="sommaire_2"></a>Règles d’imposition</h3>
<h4 class="spip">Notion de résidence</h4>
<p>L’article 4, paragraphe 1 de la Convention s’applique aux personnes qui sont considérées comme "résidents d’un Etat contractant" ou de chacun de ces deux Etats. D’après ce même article de la Convention, une personne est considérée comme "résidents d’un Etat contractant" lorsque, en vertu de la législation dudit Etat, elle se trouve assujettie à l’impôt à raison de son domicile, de sa résidence ou de critères analogues.</p>
<p>Au paragraphe 2, l’article 4 fournit des critères subsidiaires permettant de résoudre le cas de double résidence si l’assujettissement à l’impôt ne pouvait suffire. Ces critères sont :</p>
<ul class="spip">
<li>un foyer d’habitation permanent ;</li>
<li>l’Etat où la personne possède le centre de ses intérêts vitaux ;</li>
<li>l’Etat dans lequel elle séjourne de façon habituelle ;</li>
<li>à défaut, l’Etat dont elle possède la nationalité.</li></ul>
<h5 class="spip">Dispositions conventionnelles sur certaines catégories de revenus</h5>
<p>Cette analyse ne tient pas compte des dispositions conventionnelles relatives aux revenus de capitaux mobiliers.</p>
<h4 class="spip">Traitements, salaires, pensions et rentes</h4>
<h5 class="spip">Rémunérations privées</h5>
<p><strong>Principe</strong></p>
<p>L’article 15, paragraphe 1 précise que les traitements et salaires d’origine privée ne sont, en règle générale, imposables que dans l’Etat où s’exerce l’activité personnelle.</p>
<p>Exceptions à cette règle générale :</p>
<p><strong>1</strong>) Le maintien de l’imposition dans l’Etat de résidence du bénéficiaire est prévu par le paragraphe 2 du même article sous réserve de trois conditions simultanément remplies :</p>
<ul class="spip">
<li>le séjour temporaire du bénéficiaire dans l’autre Etat ne dépasse pas une durée totale de 183 jours ;</li>
<li>la rémunération est payée par un employeur qui n’est pas résident de l’Etat d’exercice ;</li>
<li>la rémunération ne doit pas être à la charge d’un établissement stable ou d’une base fixe de l’employeur dans l’Etat.</li></ul>
<p><strong>2</strong>) Il résulte des dispositions du paragraphe 3 de l’article 15 de la Convention que les revenus professionnels des salariés autres qu’intérimaires employés à bord d’un navire, d’un aéronef en trafic international ou à bord d’un bateau servant à la navigation intérieure ne sont imposables que dans l’Etat où se trouve la direction effective de l’entreprise.</p>
<h4 class="spip">Rémunérations publiques</h4>
<p><strong>Principe</strong></p>
<p>L’article 19, paragraphe 1 indique que les traitements, salaires et rémunérations analogues ainsi que les <strong>pensions de retraite </strong>payés par un Etat ou une personne morale de droit public de cet Etat restent imposables dans cet Etat.</p>
<p><strong>Exception</strong></p>
<p>Toutefois, en vertu de l’article 19, alinéa 2, cette règle ne s’applique pas lorsque le bénéficiaire possède la nationalité de l’autre Etat sans être en même temps ressortissant de l’Etat payeur ; l’imposition est réservée à l’Etat dont l’intéressé est le résident.</p>
<p>D’autre part, en vertu des dispositions du paragraphe 2 du même article, les règles fixées aux paragraphes dudit article ne sont pas applicables aux rémunérations ou aux pensions versées au titre de services rendus dans le cadre d’une activité industrielle ou commerciale exercée par un Etat ou une personne morale de droit public.</p>
<p>Les sommes versées à ce titre sont imposées soit dans l’Etat d’exercice de l’activité (article 15 de la Convention), soit dans l’Etat de résidence du bénéficiaire (article 18, paragraphe 1 de la Convention).</p>
<h5 class="spip">Pensions et rentes</h5>
<p>L’article 18, paragraphe 1 prévoit que les pensions de retraite de source privée ainsi que les rentes viagères restent imposables dans l’Etat dont le bénéficiaire est un résident.</p>
<p>Cette disposition couvre également les pensions publiques auxquelles les règles de l’article 19 ne sont pas applicables.</p>
<h5 class="spip">Etudiants, stagiaires, enseignants, chercheurs</h5>
<p><strong>Etudiants, stagiaires</strong></p>
<p>L’article 21, paragraphe 1 de la Convention prévoit que les étudiants, les stagiaires d’un Etat qui séjournent dans l’autre Etat à seule fin d’y poursuivre leurs études ou leur formation et qui perçoivent des subsides d’origine étrangère à cet Etat sont exonérés d’impôt par ce dernier Etat.</p>
<p>L’article 21, paragraphe 2 prévoit qu’un étudiant d’un Etat qui séjourne dans l’autre Etat uniquement en vue d’obtenir une formation pratique relative à ses études et qui perçoit une rémunération est exonéré d’impôts par ce dernier pendant une durée de deux ans.</p>
<p><strong>Enseignants, chercheurs</strong></p>
<p>L’article 20 précise que les rémunérations versées aux enseignants et chercheurs résidents d’un Etat se rendant temporairement dans l’autre Etat en vue d’y exercer une activité pédagogique ou de recherche pendant une période <strong>ne dépassant pas deux ans </strong>dans une université, un collège, une école, un établissement de recherche ou autre restent imposables dans l’Etat de résidence.</p>
<h4 class="spip">Autres catégories de revenus</h4>
<h5 class="spip">Bénéfices industriels et commerciaux</h5>
<p>L’article 7, paragraphe 1 dispose que les entreprises industrielles et commerciales sont imposables sur le territoire où se trouve un établissement stable.</p>
<h5 class="spip">Bénéfices des professions non commerciales et des revenus non commerciaux</h5>
<p>L’article 14, paragraphe 1 dispose que les revenus provenant de l’exercice d’une profession libérale ou d’autres activités indépendantes sont imposables dans l’Etat sur le territoire duquel se trouve l’installation permanente où s’exerce de façon régulière l’activité personnelle.</p>
<p>Les revenus que les professionnels du spectacle ainsi que les sportifs réalisent en cette qualité dans l’un des deux Etats, à titre indépendant, restent imposables dans l’Etat d’exercice de l’activité selon les dispositions de l’article 17 de la Convention.</p>
<p>L’article 12, paragraphe 1 pose en principe que les revenus non commerciaux (redevances et droits d’auteur) sont imposables dans l’Etat de résidence du bénéficiaire. Sous couvert de la production de formulaires spécifiques remis auprès des autorités fiscales de l’Etat dont relève le créancier des revenus, il peut y avoir imposition dans l’Etat d’où proviennent ces catégories de revenus à des taux différents (10 % droits d’auteur, 25 % redevances pour l’usage d’une marque, 15 % autres cas).</p>
<h5 class="spip">Revenus immobiliers</h5>
<p>L’article 6, paragraphe 1 dispose que les revenus des biens immobiliers y compris les bénéfices des exploitations agricoles sont imposables dans l’Etat où ils sont situés.</p>
<p>Cette règle s’applique également aux gains provenant de la cession ou de l’échange desdits biens ou droits selon les dispositions de l’article 13, paragraphe 1. En ce qui concerne les gains provenant de l’aliénation de tous biens autres que mobiliers ou immobiliers, le paragraphe 3 de l’article 13 précise qu’ils restent imposables dans l’Etat de résidence du cédant.</p>
<h4 class="spip">Elimination de la double imposition</h4>
<p>L’élimination de la double imposition pour les résidents de France qui perçoivent des revenus de source brésilienne s’opère aux termes du paragraphe 2 de l’article 22 selon le régime de l’imputation.</p>
<p>Les revenus de source française ou brésilienne pour lesquels le droit d’imposer est dévolu à titre exclusif à l’un des deux Etats doivent être maintenus en dehors de la base de l’impôt français (article 22, paragraphe 2-a), réserve faite toutefois de leur prise en compte pour la détermination du taux effectif.</p>
<p>Le calcul du taux effectif se décompose par la détermination d’une cotisation de base correspondant à l’ensemble des revenus de source française ou étrangère passible de l’impôt français suivant les règles de la législation interne en l’absence de Convention.</p>
<p>L’impôt exigible est donc égal au produit de la cotisation de base par le rapport entre le montant net total des revenus conventionnellement imposables en France et le montant total du revenu net d’après lequel le calcul de ladite cotisation de base a été effectué.</p>
<p>Au résultat ainsi obtenu peuvent être appliquées les réfactions prévues par la loi interne.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/fiscalite/article/convention-fiscale). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
