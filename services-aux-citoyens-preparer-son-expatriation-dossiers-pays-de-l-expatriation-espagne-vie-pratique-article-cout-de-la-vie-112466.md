# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/vie-pratique/article/cout-de-la-vie-112466#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/vie-pratique/article/cout-de-la-vie-112466#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/vie-pratique/article/cout-de-la-vie-112466#sommaire_3">Alimentation</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/vie-pratique/article/cout-de-la-vie-112466#sommaire_4">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>L’unité monétaire est l’euro.</p>
<p>Le chéquier est très peu utilisé. La carte bancaire est bien acceptée même si beaucoup de paiements s’effectuent encore en espèces. Le paiement à l’aide des principales cartes de crédit internationales est très répandu en Espagne ; les cartes acceptées sont généralement affichées à l’entrée du magasin. Au moment de payer, il vous faudra présenter votre passeport ou pièce d’identité.</p>
<p>Les chèques de voyage sont admis dans la plupart des hôtels, restaurants et commerces, à condition de présenter également son passeport.</p>
<p>Toutes les agences bancaires, ouvertes au public du lundi au vendredi, de 8h30 à 14 h (quelques-unes sont également ouvertes le samedi matin sauf les mois de juin à septembre) et les bureaux de change des grandes villes offrent au visiteur un service de change de devises.</p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<p>Au sein de l’Union européenne, tout particulier qui réside dans l’un des Etats membres est libre d’ouvrir un compte bancaire dans l’établissement de son choix, qu’il s’agisse d’un établissement étranger ou non, qu’il soit situé dans son Etat de résidence ou dans un autre Etat membre.</p>
<p>Plusieurs banques françaises (BNP, Société générale, Crédit agricole, Natixis…) sont présentes en Espagne, aucune dans la banque de détail. La seule banque disposant d’un réseau d’agences commerciales est Targo Bank, résultant d’une joint-venture entre le Banco Popular et le Crédit Mutuel.</p>
<p>L’ouverture d’un compte se fait facilement, sur présentation d’une pièce d’identité, d’une carte de résident ainsi que d’un justificatif de domicile en Espagne.</p>
<p>Les non-résidents peuvent ouvrir un compte en banque à condition de présenter un certificat de non-résidence (<i>certificado de no residencia) </i>à demander auprès de la « Dirección General de la Policía » de la commune de résidence, muni du passeport. Il convient de venir en personne chercher ce certificat une dizaine de jours plus tard. Une fois le certificat délivré, il est possible de procéder à l’ouverture du compte, qui sera opérationnel de suite et sans dépôt minimum exigé.</p>
<p>Les transferts de fonds (salaires, cotisations) pour les Français expatriés relevant du secteur privé sont libres.</p>
<h3 class="spip"><a id="sommaire_3"></a>Alimentation</h3>
<p>Prix moyen d’un repas dans un restaurant</p>
<p>A Madrid :</p>
<ul class="spip">
<li>Restaurant de qualité supérieure : de 36 à 90 euros</li>
<li>Restaurant de qualité moyenne : de 18 à 36 euros</li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Evolution des prix</h3>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de l’INE (Institut national de la Statistique).</p>
<p><i>Mise à jour : avril 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/vie-pratique/article/cout-de-la-vie-112466). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
