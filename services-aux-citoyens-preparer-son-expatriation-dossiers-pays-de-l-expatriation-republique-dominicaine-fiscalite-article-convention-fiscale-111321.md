# Convention fiscale

<p>La France et la République dominicaine n’ayant pas conclu de convention fiscale, le régime commun s’applique à l’expatrié français pour sa situation à l’égard du fisc français.</p>
<p>Pour en savoir plus, vous pouvez consulter notre rubrique thématique <a href="services-aux-citoyens-preparer-son-expatriation-fiscalite.md" class="spip_in">Fiscalité</a>.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/fiscalite/article/convention-fiscale-111321). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
