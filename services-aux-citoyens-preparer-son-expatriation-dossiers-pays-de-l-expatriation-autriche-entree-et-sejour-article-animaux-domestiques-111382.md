# Animaux domestiques

<p>En <strong>Autriche</strong>, adressez-vous au service vétérinaire de votre commune ou à votre vétérinaire traitant.</p>
<p><a href="http://www.wien.gv.at/kontakte/ma60/index.html" class="spip_out" rel="external">Coordonnées des services vétérinaires de Vienne</a></p>
<p><a href="http://www.tierschutzinwien.at/" class="spip_out" rel="external">Veterinäramt</a><br class="manualbr">Magistratsabteilung 60<br class="manualbr">Veterinärdienste  Tierschutz <br class="manualbr">Karl-Farkas-Gasse 16 <br class="manualbr">A-1030 Wien<br class="manualbr">Telefon : +43 / 1 / 4000 / 8060<br class="manualbr">Fax : +43 / 1 / 79 514 / 99 / 97 619 <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/entree-et-sejour/article/animaux-domestiques-111382#tierschutz#mc#ma60.wien.gv.at#" title="tierschutz..åt..ma60.wien.gv.at" onclick="location.href=mc_lancerlien('tierschutz','ma60.wien.gv.at'); return false;" class="spip_mail">Courriel</a></p>
<p>En France, la réalisation de la primo-vaccination antirabique n’est considérée comme valable qu’à partir de 21 jours après la fin du protocole de vaccination prescrit par le fabricant. La vaccination antirabique de rappel est considérée en cours de validité le jour de sa réalisation.</p>
<p>Pour l’introduction d’un animal en Autriche, il est exigé un certificat international de vaccination (<i>Internationaler Impfpaß</i>), délivré au moins 30 jours avant le passage de la frontière et accompagné d’une traduction allemande certifiée conforme (l’Office National autrichien à Paris s’en charge).</p>
<p>Quelque soit l’animal, s’il a plus de trois mois, il doit obligatoirement avoir été vacciné contre la rage (<i>Tollwut</i>) depuis plus d’un mois et moins d’un an, faute de quoi l’animal risque d’être gardé en quarantaine pour vaccination aux frais du propriétaire. Les propriétaires d’animaux âgés de moins de trois mois doivent fournir un certificat d’origine et de santé. Selon l’animal, d’autres vaccinations peuvent être exigées. Il est important de se renseigner auprès du vétérinaire traitant, lui seul étant en mesure de déterminer le choix du programme de vaccination le mieux adapté à l’animal et au contexte épidémiologique.</p>
<p>Pour le retour en France, la procédure est identique.</p>
<p>Pour tout renseignement, s’adresser à :</p>
<p><strong><a href="http://www.oeamtc.at/index.php" class="spip_out" rel="external">ÖAMTC-Touristikabteilung</a></strong><br class="manualbr">Schubertring 1 - 3<br class="manualbr">1010 Wien <br class="manualbr">Tél. 711 99 / 0</p>
<p>Pour plus de détails sur les conditions à remplir pour pouvoir voyager avec un animal, consultez le <a href="http://www.oeamtc.at/?id=2500%2C1135277%2C%2C" class="spip_out" rel="external">la page du Club ÖAMTC</a> ou rendez-vous sur la page du site de l’<a href="http://ec.europa.eu/food/animal/liveanimals/pets/index_fr.htm" class="spip_out" rel="external">Union européenne</a> consacrée à ce sujet.</p>
<p>En France : pour obtenir des informations concernant les réglementations pour les animaux, vous pouvez également contacter le service vétérinaire du ministère français de l’Agriculture à Paris au : Tél. : 01 49 55 84 29.</p>
<p>L’équivalent de la S.P.A. française est le :</p>
<p><a href="http://www.wr-tierschutzverein.org/" class="spip_out" rel="external">Wiener Tierschutzverein</a><br class="manualbr">Vösendorf Triesterstr. 8<br class="manualbr">Tél. : 699 24 50</p>
<p>On peut y acheter des animaux ; il faut alors avoir la majorité et présenter une pièce d’identité, le <i>Meldezettel</i>. Il faut également donner ses coordonnées (pour un éventuel contrôle) et signaler si le cas se présente le décès de l’animal. Ce dernier ne peut être envoyé à l’étranger.</p>
<p>Le Wiener Tierschutzverein à également un bureau qui s’occupe des <a href="http://www.wien.gv.at/gesellschaft/tiere/fundtiere/" class="spip_out" rel="external">animaux trouvés ou perdus</a> (<i>Fund- und Verluststelle</i>).</p>
<p>Plusieurs sociétés ont développé un service de garde d’animaux pendant votre absence, à domicile, dans des familles d’accueil ou dans des foyers. Vous en trouverez les adresses dans l’annuaire téléphonique (pages jaunes).</p>
<h4 class="spip">Information générale</h4>
<h5 class="spip">Le pays de destination se trouve dans l’Union européenne (sauf Irlande, Malte, Suède et Royaume-Uni)</h5>
<p>Une information très détaillée est disponible sur le site Internet du <a href="http://agriculture.gouv.fr/" class="spip_out" rel="external">ministère de l’Agriculture</a>.</p>
<p>Les chiens, les chats et les furets doivent satisfaire aux conditions suivantes :</p>
<ul class="spip">
<li>être identifiés par tatouage ou par puce électronique ;</li>
<li>être valablement vaccinés contre la rage ;</li>
<li>être titulaires d’un passeport délivré par un vétérinaire habilité attestant de l’identification et de la vaccination contre la rage de l’animal.</li></ul>
<p>Le système d’identification électronique, ainsi que la reconnaissance de la validité de la vaccination contre la rage, peuvent varier d’un Etat membre à l’autre. Il est donc vivement recommandé de prendre contact avec l’<a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-etrangers-en-france/article/annuaire-des-representations-etrangeres-en-france" class="spip_in">ambassade du pays de destination</a>.</p>
<p>En France, la réalisation de la primo-vaccination antirabique n’est considérée comme valable qu’à partir de 21 jours après la fin du protocole de vaccination prescrit par le fabricant. La vaccination antirabique de rappel est considérée en cours de validité le jour de sa réalisation.</p>
<p><strong>Pour plus d’informations :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Notre article thématique <a href="services-aux-citoyens-preparer-son-expatriation-douanes-article-animaux-domestiques.md" class="spip_in">Animaux domestiques</a>.</p>
<p><i>Mise à jour : février 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/entree-et-sejour/article/animaux-domestiques-111382). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
