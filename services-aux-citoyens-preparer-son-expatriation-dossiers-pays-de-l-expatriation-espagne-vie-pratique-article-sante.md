# Santé

<p>Pour une présentation des risques épidémiologiques ainsi que des conseils sur la médecine au quotidien, consultez les pages suivantes :</p>
<ul class="spip">
<li>Page <a href="http://www.ambafrance-es.org/Sante" class="spip_out" rel="external">Santé</a> sur le site de l’Ambassade de France en Espagne</li>
<li>Page dédiée à la <a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/espagne/" class="spip_in">santé en Espagne sur le site Conseils aux voyageurs</a></li></ul>
<p><strong>Carte européenne d’assurance maladie</strong></p>
<p><strong>La carte européenne d’assurance maladie </strong>(CEAM) atteste de vos droits à l’assurance maladie en Europe. Lors d’un séjour temporaire dans un Etat membre de l’ <a href="http://www.mfe.org/Default.aspx?SID=13082" class="spip_out" rel="external">Espace économique européen</a>(EEE) ou en Suisse, elle vous permet de bénéficier de la prise en charge des soins médicalement nécessaires.</p>
<p>La CEAM remplace définitivement les formulaires E 111 et E 111 B (utilisés pour les touristes), ainsi que les formulaires E 110, E 119, E 128 utilisés jusqu’à présent pour les séjours temporaires en Europe.</p>
<p>La CEAM est <strong>valable pour un séjour temporaire </strong>(à l’occasion de vacances, d’un détachement professionnel, d’un stage, d’un séjour linguistique, par exemple).</p>
<p>Délivrée gratuitement dans un délai minimum de deux semaines à la demande de l’intéressé par les caisses d’assurance maladie, la CEAM se présente sous la forme d’une carte plastique non électronique distincte de la carte Vitale. Il s’agit d’une carte nominative et individuelle.</p>
<p><strong>Elle a une durée de validité maximale d’un an.</strong></p>
<p>La <a href="http://www.cleiss.fr/particuliers/ceam.html" class="spip_out" rel="external">carte européenne d’assurance maladie</a> peut être présentée dans les Etats suivants :</p>
<p>Allemagne, Autriche, Belgique, Bulgarie, Chypre, Croatie, Danemark, Espagne, Estonie, Finlande, France, Grèce, Hongrie, Irlande, Islande, Italie, Lettonie, Liechtenstein, Lituanie, Luxembourg, Malte, Norvège, Pays-Bas, Pologne, Portugal, République slovaque, République tchèque, Roumanie, Royaume-Uni, Slovénie, Suède et Suisse.</p>
<p><i>Mise à jour : avril 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/vie-pratique/article/sante). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
