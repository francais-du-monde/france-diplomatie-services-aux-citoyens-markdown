# Convention fiscale

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/fiscalite/article/convention-fiscale#sommaire_1">Champ d’application de la convention</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/fiscalite/article/convention-fiscale#sommaire_2">Dispositions conventionnelles sur certaines catégories de revenus</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/fiscalite/article/convention-fiscale#sommaire_3">Elimination de la double imposition</a></li></ul>
<p>La France et le Sénégal ont signé le 29 mars 1974, une convention en matière de fiscalité publiée au Journal Officiel le 30 novembre 1976, modifiée par les avenants des 16 juillet 1984 et 10 janvier 1991 (respectivement publiés les 25 février 1986 et 27 février 1993).</p>
<p>Cette convention tend à éviter les doubles impositions en matière d’impôts sur le revenu et sur les successions qui pourraient résulter de l’application des législations de ces deux Etats.</p>
<p>Le texte de la convention peut être obtenu auprès de la Direction des Journaux Officiels par courrier (26, rue Desaix - 75727 Paris cedex 15), par télécopie (01.40.58.77.80), ou sur le <a href="http://www.impots.gouv.fr/" class="spip_out" rel="external">site internet du ministère des Finances</a>.</p>
<p>Les dispositions conventionnelles qui ont primauté sur les dispositions du droit interne, selon l’article 55 de la Constitution française répartissent entre les deux Etats, le droit d’imposer les revenus perçus par leurs résidents respectifs.</p>
<h3 class="spip"><a id="sommaire_1"></a>Champ d’application de la convention</h3>
<p>Cet accord a pour objet de protéger les résidents de chacun des Etats contractants en matière d’impôts prélevés directement sur le revenu ou sur les sociétés.</p>
<p>La convention trouve donc à s’appliquer aux résidents de ces Etats.</p>
<h4 class="spip">Notion de "domicile" (employée au lieu de "résidence")</h4>
<p>L’article 2 paragraphe 1 de la convention s’applique aux personnes qui sont considérées comme "domiciliées d’un Etat contractant" ou de chacun de ces deux Etats.</p>
<p>D’après ce même article de la convention, une personne est considérée comme "domiciliée d’un Etat contractant" lorsque, en vertu de la législation dudit Etat, elle se trouve assujettie à l’impôt en raison de son domicile ou de critère analogue.</p>
<p>Ce même paragraphe fournit des critères subsidiaires permettant de résoudre le cas de double domicile si l’assujettissement à l’impôt ne pouvait suffire.</p>
<p>Ces critères sont :</p>
<ul class="spip">
<li>un foyer d’habitation permanent,</li>
<li>l’Etat où la personne possède le centre de ses intérêts vitaux, c’est-à-dire le lieu avec lequel les relations personnelles sont les plus étroites,</li>
<li>l’Etat dans lequel elle séjourne de façon habituelle,</li>
<li>à défaut, l’Etat dont elle possède la nationalité.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Dispositions conventionnelles sur certaines catégories de revenus</h3>
<h4 class="spip">Traitements, salaires, pensions et rentes</h4>
<h5 class="spip">Rémunérations privées</h5>
<p><strong>Principe</strong></p>
<p>L’article 22, paragraphe 1, précise que les traitements et salaires d’origine privée ne sont, en règle générale, imposables que dans l’Etat où <strong>s’exerce l’activité personnelle</strong>.</p>
<p>Exceptions à cette règle générale :</p>
<ul class="spip">
<li>le maintien de l’imposition dans l’Etat de résidence du bénéficiaire est prévu par le paragraphe 2 du même article sous réserve de trois conditions simultanément remplies :</li>
<li>le séjour temporaire du bénéficiaire dans l’autre Etat ne dépasse pas une durée totale de 183 jours,</li>
<li>la rémunération est payée par un employeur qui n’est pas résident de l’Etat d’exercice,</li>
<li>la rémunération ne doit pas être à la charge d’un établissement stable ou d’une base fixe de l’employeur dans l’Etat.</li>
<li>Il résulte des dispositions du paragraphe 3 de l’article 22 de la convention que les revenus professionnels des salariés autres qu’intérimaires employés à bord d’un navire, d’un aéronef en trafic international ou à bord d’un bateau servant à la navigation intérieure ne sont imposables que dans l’Etat où se trouve la direction effective de l’entreprise.</li></ul>
<h5 class="spip">Rémunérations publiques</h5>
<p><strong>Principe</strong></p>
<p>Le nouvel article 17 indique que les traitements, salaires et rémunérations analogues hormis les <strong>pensions de retraite</strong> payés par un Etat ou une personne morale de droit public de cet Etat restent imposables dans cet Etat.</p>
<p>Toutefois, en vertu des dispositions du deuxième alinéa du même article, les règles fixées au premier alinéa dudit article ne sont pas applicables aux rémunérations au titre <strong>de services rendus dans le cadre d’une activité industrielle ou commerciale exercée par un Etat ou une personne morale de droit public.</strong></p>
<p>Les sommes versées à ce titre sont imposées dans l’Etat d’exercice de l’activité (article 22 de la Convention).</p>
<h5 class="spip">Pensions</h5>
<p>L’article 21 prévoit que les pensions y compris les pensions de sécurité sociale ainsi que les autres revenus similaires restent imposables dans l’Etat où le bénéficiaire est domicilié.</p>
<h5 class="spip">Etudiants, stagiaires</h5>
<p>L’article 24 de la convention prévoit que les étudiants et les stagiaires d’un Etat qui séjournent dans l’autre Etat à seule fin d’y poursuivre leurs études ou leur formation et qui perçoivent des subsides d’origine étrangère à cet Etat, sont exonérés d’impôt par ce dernier Etat.</p>
<h4 class="spip">Autres catégories de revenus</h4>
<h5 class="spip">Bénéfices industriels et commerciaux </h5>
<p>L’article 10 paragraphe 1 dispose que les entreprises industrielles et commerciales sont imposables sur le territoire où se trouve un établissement stable.</p>
<p>Bénéfices des professions non commerciales et des revenus non commerciaux</p>
<p>L’article 23 paragraphe 1 dispose que les revenus provenant de l’exercice d’une profession libérale ou d’autres activités indépendantes sont imposables dans l’Etat sur le territoire duquel se trouve l’installation permanente où s’exerce de façon régulière l’activité personnelle.</p>
<p>Le nouvel article 20 paragraphes 2 et 5 pose en principe que les revenus non commerciaux (droits d’auteur) sont imposables dans l’Etat de résidence du bénéficiaire, sauf pour les redevances, imposables au taux de 15%, dans l’Etat créancier des revenus.</p>
<h5 class="spip">Revenus immobiliers</h5>
<p>L’article 4 dispose que les revenus des biens immobiliers y compris les bénéfices des exploitations agricoles sont imposables dans l’Etat où ils sont situés.</p>
<p>Cette règle s’applique également aux gains provenant de la cession ou de l’échange desdits biens ou droits selon les dispositions du nouvel article 16, paragraphe 1.</p>
<p>En ce qui concerne les gains provenant de l’aliénation de tous biens autres que mobiliers ou immobiliers, le paragraphe 5 du nouvel article 16 précise qu’ils restent imposables dans l’Etat de domiciliation du cédant.</p>
<h5 class="spip">Revenus de capitaux mobiliers</h5>
<p><strong>Les dividendes</strong></p>
<p>Le nouvel article 13, paragraphe 5, de la convention définit la notion de dividendes.</p>
<p>Le paragraphe 2 du même article dispose que les dividendes restent imposables dans l’Etat d’origine des revenus à un taux ne pouvant dépasser 15% de leur montant brut.</p>
<p>Le paragraphe 3 prévoit le principe du transfert de l’avoir fiscal aux personnes domiciliées au Sénégal. Toutefois, pour les personnes morales, ce principe ne bénéficie qu’aux sociétés qui détiennent directement ou indirectement 10% du capital de la société française distributrice. Par ailleurs, les bénéficiaires de l’avoir fiscal doivent justifier de leur domiciliation fiscale au Sénégal.</p>
<p>Il est admis au paragraphe 4 du même article que les bénéficiaires ne pouvant prétendre au transfert de l’avoir fiscal, puissent obtenir le remboursement du précompte acquitté par la société distributrice française.</p>
<p><strong>Les intérêts</strong></p>
<p>Le nouvel article 15, paragraphe 4, détermine la notion d’intérêts.</p>
<p>Le paragraphe 2 de ce même article dispose que les intérêts sont imposables dans l’Etat d’origine de ces revenus à un taux ne pouvant excéder 15%.</p>
<p>Des exceptions sont toutefois prévues au paragraphe 3 dans le cadre de certaines catégories d’intérêts versés à raison de prêts publics ou de prêts liés au commerce international.</p>
<h3 class="spip"><a id="sommaire_3"></a>Elimination de la double imposition</h3>
<p>L’élimination de la double imposition pour les résidents de France qui perçoivent des revenus de source sénégalaise s’opère aux termes du nouvel article 26.</p>
<p>Les revenus de source française ou sénégalaise pour lesquels le droit d’imposer est dévolu à titre exclusif à l’un des deux Etats doivent être maintenus en dehors de la base de l’impôt français, réserve faite toutefois de leur prise en compte pour la détermination du taux effectif.</p>
<p>Le calcul du taux effectif se décompose par la détermination d’une cotisation de base correspondant à l’ensemble des revenus de source française ou étrangère passible de l’impôt français suivant les règles de la législation interne en l’absence de convention. Ces taxes sont établies chaque année et concernent le budget des collectivités locales.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/fiscalite/article/convention-fiscale). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
