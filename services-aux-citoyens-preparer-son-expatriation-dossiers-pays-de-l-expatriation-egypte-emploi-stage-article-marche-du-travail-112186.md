# Marché du travail

<p class="chapo">
      Contrairement à d’autres pays, notamment ceux qui sont membres de l’UE, la particularité de la présence économique/commerciale française en Egypte réside dans le fait qu’elle est très diversifiée et touche par conséquent des millions de consommateurs car elle couvre pratiquement tous les domaines.
</p>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/emploi-stage/article/marche-du-travail-112186#sommaire_1">Secteurs à fort potentiel</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/emploi-stage/article/marche-du-travail-112186#sommaire_2">Secteurs à faible potentiel</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/emploi-stage/article/marche-du-travail-112186#sommaire_3">Rémunération</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Secteurs à fort potentiel</h3>
<p>Les secteurs à fort potentiel sont entre autres la banque, la santé, l’agriculture, l’agroalimentaire, l’industrie, la construction, les énergies renouvelables, la grande distribution, les télécommunications, le pétrole, le tourisme, les BTP. Il est possible pour un étranger de travailler dans presque tous les secteurs, sauf ceux touchant à la sécurité, à l’exportation et au dédouanement. Il est également interdit à un étranger de posséder une société d’import ou une agence commerciale.</p>
<h3 class="spip"><a id="sommaire_2"></a>Secteurs à faible potentiel</h3>
<p>Les secteurs à faible potentiel : les produits de luxe (ex : dans l’alimentaire, le caviar, le foie gras, etc.), le second-œuvre (forte présence des marchés asiatiques), le marché du meuble, le plastique, la papeterie…</p>
<h3 class="spip"><a id="sommaire_3"></a>Rémunération</h3>
<p>Jusqu’à tout récemment, il n’existait pas de salaire minimum. Les nouvelles autorités, en l’occurrence le gouvernement formé le 16 juillet 2012 s’est penché sérieusement sur ce dossier et a décidé dans un premier temps de fixer un salaire minimum de 1200 LE (environ 120€), applicable au 1er janvier 2014. Cette date a été reportée au 1er février 2014.</p>
<p>Les heures supplémentaires de jour sont rémunérées avec une majoration du salaire normal de 25% ; les heures supplémentaires de nuit sont rémunérées avec une majoration du salaire normal de 50%.</p>
<p>A titre d’exemple, le salaire mensuel d’un cadre moyen commence à partir de 800 euros (en contrat local dans le secteur privé) mais peut varier selon le domaine d’activité et l’employeur, dépendamment si c’est le secteur privé ou public qui recrute.</p>
<p><i>Mise à jour : avril 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/emploi-stage/article/marche-du-travail-112186). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
