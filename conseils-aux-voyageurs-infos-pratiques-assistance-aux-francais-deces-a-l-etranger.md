# Décès à l’étranger

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/infos-pratiques/assistance-aux-francais/deces-a-l-etranger/#sommaire_1">Quand un parent ou un proche décède à l’étranger</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/infos-pratiques/assistance-aux-francais/deces-a-l-etranger/#sommaire_2">Que peuvent vous apporter les services consulaires français ?</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/infos-pratiques/assistance-aux-francais/deces-a-l-etranger/#sommaire_3">En cas d’homicide à l’étranger</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/infos-pratiques/assistance-aux-francais/deces-a-l-etranger/#sommaire_4">Accompagnement psychologique des familles</a></li></ul>
<p><strong>La mort d’un parent ou d’un proche est toujours un événement difficile à surmonter, d’autant lorsqu’elle survient à l’étranger et que s’y ajoutent des formalités supplémentaires - parfois complexes - qu’il convient inévitablement d’accomplir.</strong></p>
<p><strong>Quand le décès survient à l’étranger, vous vous retrouvez très rapidement confronté à de multiples interrogations :</strong></p>
<ul class="spip">
<li><strong>que dois-je faire maintenant ?</strong></li>
<li><strong>comment vais-je pouvoir communiquer avec des personnes qui ne parlent pas le français ?</strong></li>
<li><strong>comment rapatrier le corps ?</strong></li>
<li><strong>à qui puis-je demander de l’aide ?</strong></li></ul>
<p><strong>Les ambassades et consulats à l’étranger comme le Centre de crise peuvent vous assister de même que les organismes d’assistance et les compagnies d’assurance.</strong></p>
<p><strong>Cette page vous donne des indications sur l’aide que peuvent vous apporter les missions diplomatiques et les postes consulaires à l’étranger ainsi que le Centre de crise du ministère des Affaires étrangères et du Développement international lorsque ce décès a lieu à l’étranger. Elle vous indique également les démarches que vous pouvez effectuer vous-même et la procédure à suivre.</strong></p>
<h3 class="spip"><a id="sommaire_1"></a>Quand un parent ou un proche décède à l’étranger</h3>
<p>Si vous êtes en voyage à l’étranger et qu’un membre de votre famille ou un proche (ou toute personne que vous connaissez) décède, vous devez en informer les autorités consulaires françaises du pays où a eu lieu le décès.<br class="autobr">Si les proches n’ont pas été avertis directement du décès d’un ressortissant français, l’ambassade ou le consulat rassemble le maximum d’informations concernant cette personne et les circonstances de son décès. Ils prennent ensuite contact avec les services de police ou de gendarmerie en France afin que la famille et les proches résidant en France puissent être informés dans les meilleurs délais du décès et prendre rapidement les décisions qui s’imposent.</p>
<p>Si vous résidez à l’étranger, les services consulaires français du pays où vous vous trouvez seront sollicités afin de vous informer de ce décès.</p>
<p>Si vous apprenez le décès d’un proche par le biais d’une agence de voyages, des médias ou de tout autre moyen, nous vous invitons à prendre l’attache du Centre de crise du ministère des Affaires étrangères et du Développement international au +33 (0)1 53 59 11 10, 24h/24, 7j/7.</p>
<p>L’annonce officielle du décès ainsi que la remise du corps à la famille exigent que l’identité du défunt soit établie avec certitude. Selon les circonstances du décès (notamment à la suite d’un attentat terroriste ou d’un accident collectif), il arrive que les procédures d’identification prennent quelques jours, voire davantage. Dans certaines circonstances, afin de vous préserver, les autorités peuvent choisir de ne pas procéder à une présentation visuelle du corps aux familles. Par ailleurs, les services techniques scientifiques d’identification peuvent vous solliciter afin de disposer d’éléments personnels relatifs à la personne décédée. Il pourra ainsi vous être demandé de fournir des renseignements sur ses particularités physiques ou encore de prêter des objets lui ayant appartenu aux fins de prélèvement.</p>
<p>Les services consulaires et le Centre de crise ne manqueront pas de vous informer de l’état d’avancement des démarches de rapatriement du défunt en France. Elles procéderont, notamment, à la délivrance d’une autorisation de transport de corps ou de cendres permettant aux restes mortels de quitter légalement le pays étranger et de rentrer en France.</p>
<p>Dans la majorité des pays étrangers, le décès d’un ressortissant français doit être déclaré à l’état civil local dans les mêmes conditions que le décès d’un national de ce pays. Un acte de décès local est alors établi. Les services consulaires français pourront alors transcrire l’acte de décès étranger dans le registre d’état civil français. Il pourra vous être remis une dizaine de copies d’acte de décès certifiées conformes à l’original. Par la suite, vous pourrez vous procurer ce document au service central d’état civil à Nantes ou auprès du poste diplomatique et consulaire. L’établissement de l’acte de décès français n’est pas obligatoire, mais fortement recommandé, car il vous permettra d’effectuer un certain nombre de démarches en France (successions, pension de retraite ou salaires, emprunt…).</p>
<h3 class="spip"><a id="sommaire_2"></a>Que peuvent vous apporter les services consulaires français ?</h3>
<ul class="spip">
<li>Vous préciser, à titre indicatif, le coût d’une inhumation, ou d’une incinération locale ou encore d’un rapatriement en France de la personne défunte.</li>
<li>Vous communiquer les coordonnées de sociétés de pompes funèbres locales et françaises intervenant à l’étranger. Ils pourront vous assister auprès des pompes funèbres locales si celles-ci ne parlent pas français. Tous les frais liés au rapatriement de la dépouille ou des cendres, ainsi que le coût de l’inhumation sur place, sont à la charge de la famille.</li>
<li>Vous aider à effectuer des transferts d’argent dans le pays où a eu lieu le décès.</li>
<li>Remettre les documents d’identité du défunt aux autorités émettrices lorsque le défunt laisse des objets personnels et/ou des bagages. Ils se chargeront de vous renvoyer ses objets de valeur. Toutefois, le rapatriement de tout autre effet restera à votre charge ou à celle de la compagnie d’assurance concernée et sous son entière responsabilité.</li>
<li>Vous tenir régulièrement informé de l’évolution des procédures afin que vous ne vous sentiez pas démuni ou isolé durant cette épreuve. En effet, le délai nécessaire au rapatriement de votre parent ou de votre proche peut varier et dépendre d’un certain nombre de facteurs. Ainsi, lorsque la personne décède de mort naturelle, le corps peut être rapatrié plus rapidement que lorsque le décès est imputable à un crime, à un suicide ou à un accident.</li></ul>
<p><strong>Un cas particulier</strong> : l’existence d’une procédure pénale ouverte en France. Dans cette hypothèse particulière, le corps de votre proche ne vous sera pas immédiatement restitué.<br class="autobr">Au préalable, il faut que :</p>
<ul class="spip">
<li>les examens médico-légaux aient été effectués ;</li>
<li>le parquet ou le magistrat instructeur ait délivré un permis d’inhumer.</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>En cas d’homicide à l’étranger</h3>
<p>Vous pouvez avoir recours aux instances judiciaires françaises ou à celles du pays concerné. Le Centre de crise peut vous mettre en contact avec les instances juridiques compétentes pour effectuer vos démarches (liste d’avocats, associations d’aide aux victimes : Institut national d’aide aux victimes et de médiation, Fonds de garantie des actes de terrorisme et autres infractions…).</p>
<p>Dans le respect du droit local, l’ambassade ou le consulat français peuvent prêter leur aide pour les démarches suivantes :</p>
<ul class="spip">
<li>obtenir les documents nécessaires, y compris le cas échéant un certificat de décès, un rapport d’autopsie et des rapports de police ;</li>
<li>rechercher des renseignements sur les circonstances entourant le décès ;</li>
<li>réunir toutes informations complémentaires relatives aux enquêtes policières, aux arrestations et aux instances judiciaires, si la cause du décès est un homicide.
L’enquête sur le décès d’un Français à l’étranger reste toutefois exclusivement de la responsabilité des autorités locales (sauf si une procédure judiciaire est ouverte en France).</li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Accompagnement psychologique des familles</h3>
<p>L’annonce du décès ainsi que le retour du corps en France sont autant d’épreuves pour les familles. La famille et les proches peuvent bénéficier d’un soutien ou d’un accompagnement psychologique. Comme indiqué précédemment, le Centre de crise peut vous mettre en contact avec l’association d’aide aux victimes de votre domicile. Vous pouvez également contacter la plate-forme téléphonique 08VICTIMES (08 842 846 37 ou depuis l’étranger : +33 1 41 83 42 08).</p>
<p>En fonction des circonstances, les autorités publiques (ministère des Affaires étrangères et du Développement international, préfecture, juridiction compétente) veilleront, selon les cas, à mettre en place un dispositif de soutien par le biais notamment de la saisine de l’association d’aide aux victimes compétente ou, dans des circonstances particulières, l’intervention d’un ou plusieurs psychiatres ou psychologues.</p>
<p><strong>Vos contacts sont :</strong></p>
<p>À l’étranger, l’ambassade ou le consulat dans le pays ou la région concernés.<br>Les coordonnées téléphoniques pendant les heures de bureau ainsi que le n° de la permanence téléphonique d’urgence en dehors de ces heures sont disponibles sur les sites :</p>
<ul class="spip">
<li><a href="conseils-aux-voyageurs.md" class="spip_in">Conseils aux voyageurs</a></li>
<li><a href="http://www.mfe.org/index.php/Annuaires" class="spip_out" rel="external">Maisons des français de l’étranger</a></li></ul>
<p>En France, pour signaler la disparition d’un citoyen français à l’étranger :<br class="autobr">Ministère des Affaires étrangères et du Développement international<br class="autobr">Centre de crise<br class="autobr">37 quai d’Orsay | 75700 Paris SP<br class="autobr">Tél. : +33 (0)1 53 59 11 10 (24h/24 - 7j/7)<br class="autobr">alertes.cdc<span class="spancrypt"> [at] </span>diplomatie.gouv.fr</p>
<p><strong>En savoir plus</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="services-aux-citoyens-etat-civil-et-nationalite-francaise-etat-civil-article-les-deces-et-les-disparitions-a-l.md" class="spip_in">Transcription des actes d’état civil ou jugements étrangers : les décès et les disparitions à l’étranger</a></p>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/infos-pratiques/assistance-aux-francais/deces-a-l-etranger/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
