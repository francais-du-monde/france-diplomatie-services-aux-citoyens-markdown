# Convention de sécurité sociale

<p><strong>La France et le Togo sont liés par les accords bilatéraux suivants :</strong></p>
<ul class="spip">
<li>la Convention générale de sécurité sociale du 7 décembre 1971 entrée en vigueur le 1er juillet 1973 et modifiée par l’Avenant n° 1 du 29 août 1980 entré en vigueur le 1er avril 1982 ;</li>
<li>le Protocole n° 1, relatif au maintien de certains avantages de l’assurance maladie à des assurés sociaux togolais ou français qui se rendent au Togo ;</li>
<li>le Protocole n° 2, relatif au régime d’assurances sociales des étudiants ;</li>
<li>le Protocole n° 3, relatif à l’octroi aux ressortissants togolais de l’allocation aux vieux travailleurs salariés de la législation française ;</li>
<li>le Protocole n° 4, relatif à l’octroi des prestations de vieillesse non contributives de la législation française aux ressortissants togolais résidant en France ;</li>
<li>le Protocole n° 5 relatif à l’allocation supplémentaire du Fonds National de Solidarité ;</li></ul>
<p>En application conjointe de la législation interne et de la convention franco-togolaise, les Français occupés au Togo se trouvent généralement dans l’une des trois situations suivantes :</p>
<ul class="spip">
<li>travailleurs salariés détachés dans le cadre conventionnel ou dans le cadre de la législation interne ;</li>
<li>travailleurs qui ne sont plus soumis au régime français parce qu’ils ne sont pas détachés et auxquels les dispositions conventionnelles sont applicables ;</li>
<li>travailleurs expatriés.</li></ul>
<p>Tout renseignement complémentaire au sujet de l’application de la convention franco-togolaise peut être obtenu auprès du</p>
<p><a href="http://www.cleiss.fr/docs/textes/conv_togo.html" class="spip_out" rel="external">Centre des liaisons européennes et internationales de sécurité sociale</a><br class="manualbr">11, rue de la Tour des Dames <br class="manualbr">75436 PARIS CEDEX 09<br class="manualbr">Téléphone : 01-45-26-33-41<br class="manualbr">Télécopie : 01-49-95-06-50</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-protection-sociale.md" class="spip_in">notre rubrique thématique sur la protection sociale</a>.</li>
<li><a href="http://www.cleiss.fr/pdf/conv_togo.pdf" class="spip_out" rel="external">Convention de sécurité sociale entre la France et le Togo</a>.</li></ul>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/protection-sociale-23479/article/convention-de-securite-sociale-113554). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
