# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/vie-pratique/article/cout-de-la-vie-111021#sommaire_1">Monnaie et change </a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/vie-pratique/article/cout-de-la-vie-111021#sommaire_2">Opérations bancaires </a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/vie-pratique/article/cout-de-la-vie-111021#sommaire_3">Coût de la vie</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/vie-pratique/article/cout-de-la-vie-111021#sommaire_4">Evolution des prix </a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change </h3>
<p><strong>L’unité monétaire est le rand.</strong></p>
<p>Au 31 décembre 2013, le rand vaut 0,068 euro, c’est-à-dire qu’un euro équivaut à 14,58 rands.</p>
<p>Les moyens de paiement utilisés sont le numéraire, les chèques et les cartes de crédit.</p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires </h3>
<p>Les principales banques françaises installées dans le pays sont la Société Générale, BNP-Paribas, Crédit Agricole Indosuez, Natexis Banques Populaires et le Crédit Lyonnais. Ces établissements ne font pas de banque de détail.</p>
<p>Principales banques étrangères : Bank of Athen, Standard Bank, Nedbank, First National Bank (Barclays).</p>
<p>L’ouverture d’un compte dans une banque locale (FNB, Standard Bank, Nedbank etc.) est nécessaire pour le paiement de l’assurance de votre voiture, de l’abonnement téléphonique, etc mais généralement coûteux. En effet, chaque opération bancaire est payante (retrait/dépôt d’argent, paiement par carte, etc).</p>
<p><strong>La monnaie nationale n’est pas librement convertible.</strong></p>
<p>Aucun contrôle des changes ne s’impose en deçà de 750 000 rands.</p>
<p>Au-delà, une autorisation de la Banque Centrale est nécessaire. C’est la banque commerciale qui doit contacter la Banque Centrale quand le palier des 750 000 rands est atteint. Les délais d’obtention de l’autorisation de transfert varient en fonction du montant.</p>
<h3 class="spip"><a id="sommaire_3"></a>Coût de la vie</h3>
<p>D’une manière globale, les dépenses de la vie quotidienne sont moins élevées qu’en France. La viande, le pain, les fruits et légumes (de saison) coûtent moins chers. Le poisson est par contre plus cher à Johannesburg et Pretoria, principalement du fait de la distance par rapport aux côtes. L’essence est également moins chère (environ 13 rands le litre en décembre 2013).</p>
<p>Les sorties et loisirs (restaurant, spectacle, activités de loisirs, visites touristiques) sont abordables.</p>
<p>En revanche, le prix des biens de consommation courants (vêtements, électroménager, équipement de la maison) est équivalent voire plus élevé qu’en France.</p>
<h3 class="spip"><a id="sommaire_4"></a>Evolution des prix </h3>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/vie-pratique/article/cout-de-la-vie-111021). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
