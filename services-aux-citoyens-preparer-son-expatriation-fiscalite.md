# Fiscalité

<p class="chapo">
    Lorsque vous partez à l’étranger, votre situation fiscale peut changer. Il est indispensable de vous renseigner sur votre <strong>statut fiscal (résident ou non-résident)</strong> dans votre pays d’accueil pour déterminer si vous êtes assujetti à la fiscalité locale, à la fiscalité française ou aux deux. Vous éviterez ainsi des surprises désagréables au moment de votre départ définitif du pays d’accueil ou lors de votre retour en France.
</p>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/fiscalite/#sommaire_1">I-Comment déterminer votre résidence fiscale</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/fiscalite/#sommaire_2">II-Quelles sont les formalités si votre domicile fiscal reste en France ?</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/fiscalite/#sommaire_3">III-Quelles sont les formalités si votre domicile fiscal ne reste pas en France ?</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>I-Comment déterminer votre résidence fiscale</h3>
<p>Pour déterminer votre résidence fiscale, plusieurs critères entrent en ligne de compte : foyer, existence d’une convention fiscale, source des revenus…</p>
<p>Sous réserve des conventions fiscales internationales, vous êtes considéré comme domicilié fiscalement en France si vous répondez à un seul ou plusieurs de ces critères :</p>
<ul class="spip">
<li><strong>votre foyer (conjoint ou partenaire d’un PACS et enfants) reste en France</strong>, même si vous êtes amené, en raison de nécessités professionnelles, à séjourner dans un autre pays temporairement ou pendant la plus grande partie de l’année. A défaut de foyer, le domicile fiscal se définit par votre lieu de séjour principal ;</li>
<li><strong>vous exercez en France une activité professionnelle</strong> salariée ou non, sauf si elle est accessoire</li>
<li><strong>vous avez en France le centre de vos intérêts économiques</strong>. Il s’agit du lieu de vos principaux investissements, du siège de vos affaires, du centre de vos activités professionnelles, ou le lieu d’où vous tirez la majeure partie de vos revenus.</li></ul>
<p>Pour en savoir plus consultez la rubrique <a href="http://www.impots.gouv.fr/portal/dgi/public/popup;jsessionid=SNRTOCRKXZRODQFIEIPSFFA?espId=1&amp;typePage=cpr02&amp;docOid=documentstandard_6069&amp;temNvlPopUp=true" class="spip_out" rel="external">Êtes-vous bien non résident</a></p>
<h3 class="spip"><a id="sommaire_2"></a>II-Quelles sont les formalités si votre domicile fiscal reste en France ?</h3>
<p>Vous êtes passible de l’impôt en France sur l’ensemble de vos revenus, y compris la rémunération de votre activité à l’étranger.</p>
<h5 class="spip">Les formalités</h5>
<p> Vous devez alors déposer votre déclaration de revenus auprès du service des impôts dont dépend votre résidence principale.</p>
<p>Par ailleurs, vous avez obligation de faire connaître les références des comptes bancaires (utilisés ou clos) ouverts à l’étranger (imprimé N° 3916, disponible en ligne ou sur papier libre, à joindre à votre déclaration des revenus N° 2042).</p>
<h3 class="spip"><a id="sommaire_3"></a>III-Quelles sont les formalités si votre domicile fiscal ne reste pas en France ?</h3>
<p>Vous êtes en principe assujetti à l’impôt local de votre nouveau pays de résidence.</p>
<p>Vous n’êtes imposable en France en vertu d’une convention fiscale, que si vous avez des revenus de source française.</p>
<p>Dans certains cas vous pouvez être imposé en France si vous disposez directement ou indirectement d’une ou plusieurs habitations dans notre pays.</p>
<h5 class="spip">Les formalités</h5>
<p><strong>L’année de votre départ</strong></p>
<p><strong>Informer le plus tôt possible le Centre des Finances Publiques dont vous dépendez</strong>, de votre <strong>nouvelle adresse</strong> à l’étranger sans attendre le dépôt de votre déclaration.</p>
<p><strong>Créer un compte</strong> sur <a href="http://www.impots.gouv.fr/" class="spip_out" rel="external">impots.gouv.fr</a> si ce n’est pas encore fait, cela facilitera votre déclaration et vous permettra d’accéder facilement à vos justificatifs fiscaux, archivés électroniquement.</p>
<p>Si vous avez des revenus imposables en France avant et après départ, vous devrez remplir 2 déclarations :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  un imprimé n° 2042 comprenant tous vos revenus perçus du 1er janvier à la date de votre départ. N’oubliez pas de préciser votre nouvelle adresse dans le cadre prévu à cet effet sur votre déclaration, même si l’adresse d’envoi est correcte ainsi que votre date de départ, même si vous en avez déjà informé votre service des impôts. Si durant cette période vous avez perçu des revenus de source étrangère, ces derniers seront déclarés sur une déclaration 2047 et reportés sur votre déclaration de revenus n° 2042.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  un imprimé n° 2042-NR ne comprenant que vos seuls revenus de source française imposables en France, de votre départ au 31 décembre de l’année de votre départ.</p>
<p>Vous pouvez déclarer vos impôts en ligne sur <a href="http://www.impots.gouv.fr" class="spip_out" rel="external">impots.gouv.fr</a> ou envoyez ces deux déclarations au Centre de Finances Publiques de votre domicile en France (celui qui est indiqué sur la déclaration que vous avez reçue).</p>
<p>Si vous ne percevez aucun revenu de source française après votre départ, veuillez l’indiquer dans la case "Renseignements" si vous télé-déclarez ou sur papier libre agrafé à votre déclaration papier.</p>
<p>Important : Si vous déposez une déclaration papier, indiquez sur la première page de la déclaration, sous votre adresse, votre pays d’affectation.</p>
<p>Pour en savoir plus consultez la rubrique : <a href="http://www.impots.gouv.fr/portal/dgi/public/popup;jsessionid=SNRTOCRKXZRODQFIEIPSFFA?espId=1&amp;typePage=cpr02&amp;docOid=documentstandard_5952&amp;temNvlPopUp=true" class="spip_out" rel="external">Vous quittez la France</a></p>
<p><strong>Les années suivantes</strong></p>
<p>Déclarez en ligne sur <a href="http://www.impots.gouv.fr" class="spip_out" rel="external">www.impots.gouv.fr</a>, sinon adressez votre déclaration 2042 au Service des Impôts des Particuliers Non-Résidents si vous continuez à percevoir des revenus de source française imposables en France.</p>
<p>Pour en savoir plus, consultez la rubrique <a href="http://www.impots.gouv.fr/portal/dgi/public/popup;jsessionid=SNRTOCRKXZRODQFIEIPSFFA?espId=1&amp;typePage=cpr02&amp;docOid=documentstandard_5953&amp;temNvlPopUp=true" class="spip_out" rel="external">Vous résidez à l’étranger</a></p>
<p>Si vous détenez des biens en France, différents impôts peuvent s’appliquer. Vous trouverez toutes les informations sur les <a href="http://www.impots.gouv.fr/portal/dgi/public/popup;jsessionid=SNRTOCRKXZRODQFIEIPSFFA?espId=1&amp;typePage=cpr02&amp;docOid=documentstandard_5904&amp;temNvlPopUp=true" class="spip_out" rel="external">impôts locaux</a>, <a href="http://www.impots.gouv.fr/portal/dgi/public/popup;jsessionid=SNRTOCRKXZRODQFIEIPSFFA?espId=1&amp;typePage=cpr02&amp;docOid=documentstandard_6432&amp;temNvlPopUp=true" class="spip_out" rel="external">l’impôt sur la fortune</a>, l’imposition des <a href="http://www.impots.gouv.fr/portal/dgi/public/popup;jsessionid=SNRTOCRKXZRODQFIEIPSFFA?espId=1&amp;typePage=cpr02&amp;docOid=documentstandard_6482&amp;temNvlPopUp=true" class="spip_out" rel="external">plus-values immobilières</a>, etc.</p>
<p>Pour en savoir plus : <a href="http://www.impots.gouv.fr/" class="spip_out" rel="external">www.impots.gouv.fr/</a> particuliers/ vos préoccupations/ vivre hors de France / Vous résidez à l’étranger et vous détenez des biens en France</p>
<p><strong>A votre retour en France</strong></p>
<p>Les modalités de l’imposition à laquelle vous serez soumis seront fonction de votre précédent régime fiscal (imposable en France et/ou à l’étranger). Pensez à signaler votre nouvelle adresse aux autorités fiscales de votre ancien pays de résidence et aux autorités fiscales françaises, c’est-à-dire :</p>
<ul class="spip">
<li>le Service des impôts des particuliers non-résidents (SIPNR), si vous disposiez de revenus de source française durant votre séjour à l’étranger ;</li>
<li>le service des impôts dont relève votre nouveau domicile, si vous ne disposiez d’aucun revenu de source française durant votre séjour à l’étranger.</li></ul>
<p>Le formulaire de déclaration sera envoyé à votre domicile si vous avez effectué votre changement d’adresse en temps utile. Les formulaires (2042 et 2042 NR) sont également disponibles en ligne. La date limite pour envoyer la déclaration qui suit votre retour est celle fixée pour les résidents. Vous devrez adresser vos déclarations aux services mentionnés ci-dessus, en fonction de votre situation.</p>
<p>Pour en savoir plus consultez la rubrique <a href="http://www.impots.gouv.fr/portal/dgi/public/popup;jsessionid=SNRTOCRKXZRODQFIEIPSFFA?espId=1&amp;typePage=cpr02&amp;docOid=documentstandard_5954&amp;temNvlPopUp=true" class="spip_out" rel="external">Vous revenez en France</a></p>
<h5 class="spip">Les conventions fiscales</h5>
<p>Votre situation au regard de l’impôt sur le revenu peut varier selon le pays étranger où vous résidez.</p>
<p>L’objet des conventions fiscales est d’éviter la double imposition des revenus qui ont leur source dans un État et qui sont perçus par une personne fiscalement domiciliée dans un autre État (ou résidente de cet autre État).</p>
<p>Les règles d’imposition prévues par les conventions internationales varient selon les catégories de revenus.</p>
<p><a href="http://www.impots.gouv.fr/portal/dgi/public/documentation.impot?pageId=docu_international&amp;espId=-1&amp;sfid=440" class="spip_out" rel="external">Liste des pays et territoires avec lesquels la France a passé une convention fiscale en vigueur</a></p>
<p>Vous pouvez prendre connaissance du texte de la convention qui vous intéresse auprès de l’ambassade ou du consulat de France dans le pays concerné.</p>
<p><strong>Le cas des pensions de source française</strong></p>
<p>Vous percevez des pensions de la part d’organismes français et vous résidez hors de France.</p>
<p>Vous trouverez ci-dessous la liste des pays ayant signé une convention fiscale avec la France et le sort réservé aux pensions « publiques », « privées » et de « sécurité sociale ». A défaut de convention, les pensions de source française restent imposables en France :</p>
<p><a href="http://www.impots.gouv.fr/portal/deploiement/p1/fichedescriptive_5349/fichedescriptive_5349.pdf" class="spip_out" rel="external">Tableau d’imposition des pensions</a></p>
<p>Pour plus d’informations : consultez la rubrique : <a href="http://www.impots.gouv.fr/portal/dgi/public/popup;jsessionid=SNRTOCRKXZRODQFIEIPSFFA?espId=1&amp;typePage=cpr02&amp;docOid=documentstandard_5994&amp;temNvlPopUp=true" class="spip_out" rel="external">Vous percevez des pensions</a></p>
<h5 class="spip">Compte bancaire</h5>
<p>Quelques semaines avant votre départ de France, vous devrez informer votre banque de votre changement de résidence.<br class="manualbr">Si vous devenez non-résident fiscal français, vous devez déclarer votre changement de statut fiscal à votre banque. Votre compte actuel devient alors un compte de non-résident.<br class="manualbr">Certains placements et produits d’épargne ne peuvent être détenus par des personnes non fiscalement domiciliées en France.</p>
<p>A noter qu’en tant que non-résident vous pouvez être imposé en France sur vos revenus de source française, notamment sur les revenus de votre épargne. <a href="http://www.impots.gouv.fr/portal/dgi/public/popup;jsessionid=SNRTOCRKXZRODQFIEIPSFFA?espId=1&amp;typePage=cpr02&amp;docOid=documentstandard_5961&amp;temNvlPopUp=true" class="spip_out" rel="external">Les modalités d’imposition de ces revenus</a> sont fonction de l’existence ou pas d’une convention fiscale entre la France et votre pays de résidence.</p>
<p><strong>Pour plus d’informations</strong> : « Les clés de la banque » rubrique Particuliers/ changer de banque/ déménager/ <a href="http://www.lesclesdelabanque.com/Web/Cdb/Particuliers/Content.nsf/DocumentsByIDWeb/8TMGG7?OpenDocument" class="spip_out" rel="external">le déménagement à l’étranger</a></p>
<p><strong>Pour en savoir plus :</strong></p>
<p><strong>Service des Impôts des Particuliers des non-résidents (SIPNR)</strong><br class="manualbr">10, rue du Centre<br class="manualbr">TSA 10010<br class="manualbr">93465 NOISY-LE-GRAND Cedex<br class="manualbr">Téléphone standard : 01 57 33 83 00<br class="manualbr">Télécopie : 01 57 33 81 02 ou 01 57 33 81 03<br class="manualbr">Courriel : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/fiscalite/#sip.nonresidents#mc#dgfip.finances.gouv.fr#" title="sip.nonresidents..åt..dgfip.finances.gouv.fr" onclick="location.href=mc_lancerlien('sip.nonresidents','dgfip.finances.gouv.fr'); return false;" class="spip_mail">sip.nonresidents<span class="spancrypt"> [at] </span>dgfip.finances.gouv.fr</a></p>
<p><i>Mise à jour : mai 2015</i></p>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/fiscalite/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
