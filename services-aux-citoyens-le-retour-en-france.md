# Le retour en France

<p class="spip_document_84787 spip_documents spip_documents_center">
<img src="http://www.diplomatie.gouv.fr/fr/IMG/jpg/rubrique_preparer_retour_cle8152f8.jpg" width="660" height="140" alt=""></p>
<p>Simulateur en ligne « Retour en France ».</p>
<p><a href="http://retour.apps.simplicite.io/ext/REFFront" class="spip_out" rel="external">Le simulateur « retour en France »</a>, outil interactif conçu par différents services de l’Etat, est désormais en ligne. Ce service individualisé permet aux Français établis l’étranger de connaître les démarches à accomplir dans le cadre de leur éventuel retour. Il suffit de rentrer les paramètres relatifs à votre situation personnelle pour obtenir la liste des démarches, les délais dans lesquels vous pouvez ou devez les effectuer ainsi que la liste des justificatifs nécessaires.</p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-le-retour-en-france-avant-le-retour-conseils-et.md">Formalités avant le retour</a></li>
<li><a href="services-aux-citoyens-le-retour-en-france-avant-le-retour-conseils-et-article-demarches-utiles.md">Démarches utiles</a></li>
<li><a href="services-aux-citoyens-le-retour-en-france-avant-le-retour-conseils-et-article-conjoint-etranger.md">Conjoint étranger</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-le-retour-en-france-emploi.md">Emploi</a></li>
<li><a href="services-aux-citoyens-le-retour-en-france-emploi-article-la-reinsertion.md">La recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-le-retour-en-france-emploi-article-la-validation-des-acquis-de-l-experience-vae.md">La validation des acquis de l’expérience (VAE)</a></li>
<li><a href="services-aux-citoyens-le-retour-en-france-emploi-article-les-equivalences-de-diplomes.md">Les équivalences de diplômes</a></li>
<li><a href="services-aux-citoyens-le-retour-en-france-emploi-article-le-chomage.md">Le chômage</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-le-retour-en-france-assurance-maladie.md">Assurance maladie</a></li>
<li><a href="services-aux-citoyens-le-retour-en-france-assurance-maladie-article-la-securite-sociale.md">La sécurité sociale</a></li>
<li><a href="services-aux-citoyens-le-retour-en-france-assurance-maladie-article-vous-revenez-d-un-pays-hors-de-l.md">Vous revenez d’un pays hors de l’Union européenne</a></li>
<li><a href="services-aux-citoyens-le-retour-en-france-assurance-maladie-article-vous-revenez-d-un-pays-de-l-union-europeenne.md">Vous revenez d’un pays de l’Union européenne</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-le-retour-en-france-fiscalite.md">Fiscalité</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-le-retour-en-france-enseignement.md">Enseignement</a></li>
<li><a href="services-aux-citoyens-le-retour-en-france-enseignement-article-l-ecole-maternelle-et-l-ecole.md">L’école maternelle, l’école primaire/élémentaire</a></li>
<li><a href="services-aux-citoyens-le-retour-en-france-enseignement-article-le-college-le-lycee.md">Le collège, le lycée</a></li>
<li><a href="services-aux-citoyens-le-retour-en-france-enseignement-article-l-enseignement-international-en.md">L’enseignement international en France</a></li>
<li><a href="services-aux-citoyens-le-retour-en-france-enseignement-article-l-enseignement-superieur-103684.md">L’enseignement supérieur</a></li>
<li><a href="services-aux-citoyens-le-retour-en-france-enseignement-article-les-aides-financieres.md">Les aides financières</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-le-retour-en-france-formalites-douanieres.md">Formalités douanières</a></li>
<li><a href="services-aux-citoyens-le-retour-en-france-formalites-douanieres-article-le-demenagement.md">Le déménagement</a></li>
<li><a href="services-aux-citoyens-le-retour-en-france-formalites-douanieres-article-animaux-domestiques-retour-en-france.md">Animaux domestiques - Retour en France </a></li>
<li><a href="services-aux-citoyens-le-retour-en-france-formalites-douanieres-article-l-importation-en-france-d-un.md">L’importation en France d’un véhicule</a></li>
<li><a href="services-aux-citoyens-le-retour-en-france-formalites-douanieres-article-le-transfert-de-moyens-de-paiement.md">Le transfert de moyens de paiement</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-le-retour-en-france-la-retraite.md">Retraites</a></li>
<li><a href="services-aux-citoyens-le-retour-en-france-la-retraite-article-vous-revenez-d-un-pays-de-l-union-103674.md">Vous revenez d’un pays de l’Union européenne</a></li>
<li><a href="services-aux-citoyens-le-retour-en-france-la-retraite-article-vous-revenez-d-un-pays-hors-de-l-103673.md">Vous revenez d’un pays hors de l’Union européenne</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-le-retour-en-france-les-modes-de-garde-des-enfants.md">Les modes de garde des enfants</a></li>
<li><a href="services-aux-citoyens-le-retour-en-france-les-modes-de-garde-des-enfants-article-les-creches.md">Les crèches</a></li>
<li><a href="services-aux-citoyens-le-retour-en-france-les-modes-de-garde-des-enfants-article-les-autres-modes-de-garde.md">Les autres modes de garde</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
