# Convention fiscale

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/fiscalite/article/convention-fiscale-110343#sommaire_1">Champ d’application de la convention</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/fiscalite/article/convention-fiscale-110343#sommaire_2">Dispositions conventionnelles sur certaines catégories de revenus</a></li></ul>
<p>La France et la Roumanie ont signé, le 27 septembre 1974, une convention en matière de fiscalité publiée au Journal Officiel du 21 octobre 1975. Cette convention continue à s’appliquer.</p>
<p>Cette convention tend à éviter les doubles impositions en matière d’impôts sur les revenus et sur la fortune qui pourraient résulter de l’application des législations de ces deux Etats.</p>
<p>Le texte de la convention peut être obtenu auprès de la Direction des Journaux Officiels par courrier (26, rue Desaix 75727 PARIS Cedex 15), par télécopie (01 40 58 75 00) ou sur le <a href="http://www.impots.gouv.fr/portal/deploiement/p1/fichedescriptive_2107/fichedescriptive_2107.pdf" class="spip_out" rel="external">site Internet du ministère des Finances</a>.</p>
<p>Les dispositions conventionnelles qui ont primauté sur les dispositions du droit interne, selon l’article 55 de la Constitution française, répartissent entre les deux Etats, le droit d’imposer les revenus perçus par leurs résidents respectifs.</p>
<h3 class="spip"><a id="sommaire_1"></a>Champ d’application de la convention</h3>
<p>Cet accord a pour objet de protéger les résidents de chacun des Etats contractants en matière d’impôts prélevés directement sur le revenu ou sur le bénéfice des sociétés.</p>
<p>La convention trouve donc à s’appliquer aux résidents de ces Etats.</p>
<p>L’article 1 de la Convention s’applique aux personnes qui sont considérées comme « résidents d’un Etat contractant » ou de chacun de ces deux Etats.</p>
<p>D’après l’article 4, paragraphe 1,de la Convention, une personne est considérée comme « résident d’un Etat contractant » lorsque, en vertu de la législation dudit Etat, elle se trouve assujettie à l’impôt à raison de son domicile, de sa résidence ou de critères analogues.</p>
<p>Au paragraphe 2, l’article 4 fournit des critères subsidiaires permettant de résoudre le cas de double résidence si l’assujettissement à l’impôt ne pouvait suffire.</p>
<p>Ces critères sont :</p>
<ul class="spip">
<li>un foyer d’habitation permanent (il s’agit, par exemple, du lieu de situation du conjoint ou des enfants) ;</li>
<li>l’Etat où la personne possède le centre de ses intérêts vitaux (tant professionnels que privés) ;</li>
<li>l’Etat dans lequel elle séjourne de façon habituelle (notion de 183 jours de présence physique sur le territoire au cours d’une année fiscale) ;</li>
<li>à défaut, l’Etat dont elle possède la nationalité.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Dispositions conventionnelles sur certaines catégories de revenus</h3>
<h4 class="spip">Traitements, salaires, pensions et rentes</h4>
<h5 class="spip">Rémunérations privées</h5>
<p><strong>Principe</strong></p>
<p>L’article 15, paragraphe 1 précise que les traitements et salaires d’origine privée ne sont, en règle générale, imposables que dans l’Etat où s’exerce l’activité personnelle.</p>
<p><strong>Exceptions à cette règle générale</strong></p>
<p>Le maintien de l’imposition dans l’Etat de résidence du bénéficiaire est prévu par le paragraphe 2 du même article sous réserve de trois conditions simultanément remplies :</p>
<ul class="spip">
<li>le séjour temporaire du bénéficiaire dans l’autre Etat ne dépasse pas une durée totale de 183 jours ;</li>
<li>la rémunération est payée par un employeur qui n’est pas résident de l’Etat d’exercice ;</li>
<li>la rémunération ne doit pas être à la charge d’un établissement stable ou d’une base fixe de l’employeur dans l’Etat.</li></ul>
<p>Exemple : Monsieur X est envoyé trois mois, soit quatre-vingt-dix jours (mai, juin, juillet de l’année n) par une PME située en France, fabricant d’articles de maroquinerie, en vue de prospecter le marché roumain. Cette entreprise ne dispose d’aucune succursale ni bureau en Roumanie et rémunère l’intéressé. Dans un tel cas, Monsieur X devra déclarer ses revenus en France.</p>
<p>Au contraire, si Monsieur X est envoyé du mois de février inclus au mois de novembre inclus, son séjour de plus de 183 jours en Roumanie entraîne son imposition dans ce pays.</p>
<p>Il résulte des dispositions du paragraphe 3 de l’article 15 de la convention que les revenus professionnels des salariés autres qu’intérimaires employés à bord d’un navire, d’un aéronef en trafic international ou à bord d’un bateau servant à la navigation intérieure ne sont imposables que dans l’Etat où se trouve la direction effective de l’entreprise.</p>
<h5 class="spip">Rémunérations publiques</h5>
<p><strong>Principe</strong></p>
<p>Les articles cumulés 19, paragraphe 1 et 18, paragraphe 2, indiquent que les traitements, salaires et rémunérations analogues ainsi que les pensions de retraite payés par un Etat ou une personne morale de droit public de cet Etat restent imposables dans cet Etat.</p>
<p>Exemple : Monsieur X est fonctionnaire de l’Etat français. Il résidait pendant son activité en France et décide d’aller prendre sa retraite chez l’un de ses enfants en Roumanie. Les montants de ses pensions resteront imposés en France ; son dossier est alors pris en charge par le Centre des impôts des non-résidents.</p>
<p><strong>Exception</strong></p>
<p>Toutefois, en vertu de l’article 19, paragraphe 2, cette règle ne s’applique pas aux rémunérations versées au titre de services rendus dans le cadre d’une activité industrielle ou commerciale exercée par un Etat ou une personne morale de droit public.</p>
<p>Les sommes versées à ce titre sont imposées soit dans l’Etat d’exercice de l’activité (article 15 de la convention), soit dans l’Etat de résidence du bénéficiaire (article 18 de la convention).</p>
<p>Exemple : Monsieur X, agent E.D.F., est envoyé en Roumanie afin d’effectuer des travaux de conception avec les services locaux roumains. Monsieur X est rémunéré par E.D.F. France. E.D.F. étant un établissement de droit public à caractère industriel et commercial, les rémunérations allouées à Monsieur X seront imposées en Roumanie.</p>
<h5 class="spip">Pensions et rentes</h5>
<p>L’article 18, paragraphe 1, prévoit que les pensions de retraite de source privée y compris les pensions de Sécurité sociale ainsi que les rentes viagères restent imposables dans l’Etat dont le bénéficiaire est un résident.</p>
<p>En revanche, les pensions publiques versées par la France sont imposables en France.</p>
<p>Exemple : Monsieur X, citoyen français qui a exercé une activité salariée en Roumanie, décide de venir prendre sa retraite en France. Les pensions du régime général de la sécurité sociale roumaine versées par la Roumanie au titre de cette activité sont imposables en France.</p>
<h5 class="spip">Etudiants, chercheurs, enseignants</h5>
<p><strong>Etudiants, stagiaires</strong></p>
<p>L’article 20, paragraphe 1, de la convention prévoit que les étudiants, les stagiaires d’un Etat qui séjournent dans l’autre Etat à seule fin d’y poursuivre leurs études ou leur formation et qui perçoivent des subsides d’origine étrangère à cet Etat sont exonérés d’impôt par ce dernier Etat.</p>
<p><strong>Enseignants, chercheurs</strong></p>
<p>L’article 21, paragraphe 1, précise que les rémunérations versées aux enseignants ou aux chercheurs résidents d’un Etat se rendant temporairement dans l’autre Etat en vue d’y exercer une activité pédagogique ou de recherche pendant une période ne dépassant pas deux ans, restent imposables dans l’Etat de résidence.</p>
<h5 class="spip">Autres catégories de revenus</h5>
<p><strong>Bénéfices industriels et commerciaux</strong></p>
<p>L’article 7, paragraphe 1, dispose que les entreprises industrielles et commerciales sont imposables sur le territoire où se trouve un établissement stable.</p>
<p><strong>Bénéfices des professions non commerciales et des revenus non commerciaux</strong></p>
<p>L’article 14, paragraphe 1, stipule que les revenus provenant de l’exercice d’une profession libérale ou d’autres activités indépendantes sont imposables dans l’Etat sur le territoire duquel se trouve l’installation permanente ou base fixe où s’exerce de façon régulière l’activité personnelle.</p>
<p>Les revenus que les professionnels du spectacle ainsi que les sportifs réalisent en cette qualité dans l’un des deux Etats restent imposables dans l’Etat d’exercice de l’activité selon les dispositions de l’article 17, paragraphe 1, de la Convention.</p>
<p>L’article 12, paragraphe 2, pose en principe que les revenus non commerciaux (redevances et droits d’auteur) sont imposables dans l’Etat de la source à un taux ne pouvant dépasser 10%.</p>
<p><strong>Revenus immobiliers</strong></p>
<p>L’article 6, paragraphe 1, dispose que les revenus des biens immobiliers y compris les bénéfices des exploitations agricoles sont imposables dans l’Etat où ils sont situés.</p>
<p>Cette règle s’applique également aux gains provenant de la cession ou de l’échange desdits biens ou droits selon les dispositions de l’article 13, paragraphe 1.</p>
<p>En ce qui concerne les gains provenant de l’aliénation de tous biens autres que mobiliers ou immobiliers, le paragraphe 3 de l’article 13 précise qu’ils restent imposables dans l’Etat de résidence du cédant.</p>
<p><strong>Revenus de capitaux mobiliers</strong></p>
<p>Ce terme désigne les revenus provenant d’actions, actions ou bons de jouissance, parts de mines, parts de fondateur ou autres parts bénéficiaires, à l’exception des créances et les revenus d’autres parts sociales assimilés aux revenus d’actions.</p>
<p>De manière générale, l’article 10 reprend d’une part la règle suivant laquelle les dividendes payés par une société qui est un résident d’un Etat contractant à un résident de l’autre Etat sont imposables dans cet autre Etat.</p>
<p>D’autre part, il maintient le droit au profit de l’Etat dont la société qui paie les dividendes est un résident, d’imposer ces dividendes à la source. L’Etat de la source peut imposer les dividendes à un taux qui n’excède pas 10%.</p>
<p>Ce terme désigne les revenus des fonds publics, des obligations d’emprunts, des obligations d’emprunts et des créances de toute nature ainsi qu’aux intérêts produits par des titres négociables, les bons de caisse et les intérêts de créances ordinaires.</p>
<p>L’article 11 précise que les intérêts provenant d’un Etat et payés à un résident de l’autre Etat sont imposables dans l’Etat de la source à un taux ne pouvant dépasser 10%.</p>
<p>L’article 23, paragraphe 1, dispose que la fortune constituée de biens immobiliers reste imposable au lieu de leur situation.</p>
<p>Au titre des biens mobiliers faisant partie de l’actif d’un établissement stable, le paragraphe 2 du même article rattache leur imposition au lieu de situation de cet établissement ou d’une base fixe.</p>
<p>Quant aux autres éléments de la fortune, ils sont imposables dans l’Etat de résidence du bénéficiaire aux termes du paragraphe 4.</p>
<h4 class="spip">Elimination de la double imposition</h4>
<p>L’élimination de la double imposition pour les résidents de France qui perçoivent des revenus de source roumaine s’opère aux termes du paragraphe 2 de l’article 24 selon le régime de l’imputation ou de l’exonération.</p>
<p>Le paragraphe 2-b prévoit le système du crédit d’impôt ou de l’imputation en ce qui concerne en particulier les dividendes, les intérêts, les redevances et les revenus tirés des activités artistiques ou sportives qui ont supporté l’impôt roumain.</p>
<p>Les autres revenus de source française ou roumaine pour lesquels le droit d’imposer est dévolu à titre exclusif à la Roumanie doivent être maintenus en dehors de la base de l’impôt français, réserve faite toutefois de leur prise en compte pour la détermination du taux effectif.</p>
<p>Le calcul du taux effectif se décompose tout d’abord par la détermination d’une cotisation de base correspondant à l’ensemble des revenus de source française et(ou) étrangère passible de l’impôt français suivant les règles de la législation interne.</p>
<p>Ensuite, l’impôt exigible sera égal au produit de cette cotisation de base par le rapport entre le montant net total des revenus conventionnellement imposables en France et le montant total du revenu net d’après lequel le calcul de ladite cotisation de base a été effectué.</p>
<p>Au résultat ainsi obtenu peuvent être appliquées les réfactions prévues par la loi interne (crédit ou réduction d’impôt).</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/fiscalite/article/convention-fiscale-110343). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
