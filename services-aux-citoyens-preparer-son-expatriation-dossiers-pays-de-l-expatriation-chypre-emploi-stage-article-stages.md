# Stages

<p><strong>Avez-vous pensé au volontariat international (V.I.E. ou V.I.A.) ?</strong></p>
<p>Le V.I.E, Volontariat International en Entreprise, permet aux entreprises de droit français de <strong>confier à un jeune, homme ou femme, âgé de 18 à 28 ans et ressortissants de l’Espace économique européen, de tous niveaux de formation, une mission professionnelle à l’étranger</strong>. La durée de la mission est modulable de 6 à 24 mois et renouvelable une fois à l’intérieur de cette période.</p>
<p>Il <strong>offre aux entreprises françaises la possibilité de renforcer leur développement international </strong>avec de nombreux avantages : un environnement contractuel attractif, une gestion simplifiée des ressources humaines, une aide au <i>sourcing</i>, des avantages fiscaux incitatifs, des aides financières nationales et régionales etc.</p>
<p>Il <strong>permet à des jeunes de s’investir dans une expérience formatrice à l’étranger</strong>. Il propose ainsi un cadre de travail sécurisant pour le candidat puisque les garanties sont nombreuses : protection sociale, indemnité défiscalisée, expérience prise en compte au titre du régime de retraite. Le lancement du <strong>V.I.E. Pro</strong> en novembre 2013 permet également de faire valider une mission V.I.E. par une université (quatre Universités - pilotes - à ce stade) <strong>en remplacement du stage intégré dans le cursus de certaines licences professionnelles</strong>.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/emplois-stages-concours/volontaires-internationaux/" class="spip_in">Page thématique sur le volontariat international sur notre site</a></li>
<li>Espace candidats : <a href="http://www.civiweb.com/FR/index.aspx" class="spip_out" rel="external">Civiweb.com</a>, tél. : 0810101 828</li>
<li>Espace entreprises : <a href="http://export.businessfrance.fr/default.html" class="spip_out" rel="external">Business France</a>, tél. : 0810659 659</li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/emploi-stage/article/stages). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
