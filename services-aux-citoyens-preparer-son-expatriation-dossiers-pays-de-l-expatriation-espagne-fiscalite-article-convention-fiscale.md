# Convention fiscale

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/fiscalite/article/convention-fiscale#sommaire_1">Champ d’application de la convention</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/fiscalite/article/convention-fiscale#sommaire_2">Dispositions conventionnelles concernant le traitement fiscal des revenus</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/fiscalite/article/convention-fiscale#sommaire_3">Rémunérations publiques</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/fiscalite/article/convention-fiscale#sommaire_4">Pensions et rentes</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/fiscalite/article/convention-fiscale#sommaire_5">Règles d’imposition</a></li></ul>
<p>La France et l’Espagne ont signé le 10 octobre 1995 une convention en vue d’éviter les doubles impositions et de prévenir l’évasion fiscale en matière d’impôts sur le revenu et sur la fortune. Cette convention est entrée en vigueur le 11 juillet 1997.<br class="autobr">Son texte intégral est disponible sur le site Internet de l’administration fiscale. Voici un résumé de ses points clés pour les particuliers :</p>
<h3 class="spip"><a id="sommaire_1"></a>Champ d’application de la convention</h3>
<p>La convention trouve à s’appliquer aux résidents des deux Etats signataires.</p>
<p><strong>Notion de résidence</strong></p>
<p>L’article 1er de la convention s’applique aux personnes qui sont considérées comme résidentes d’un Etat contractant ou de chacun de ces deux Etats.<br class="autobr">D’après l’article 4, paragraphe 1, de la convention, une personne physique est considérée comme résidente d’un Etat contractant lorsqu’elle se trouve assujettie à l’impôt dans cet Etat, à raison de son domicile, de sa résidence, de son siège de direction ou de tout autre critère analogue.</p>
<p>Au paragraphe 2, l’article 4 prévoit des critères subsidiaires permettant de résoudre le cas de double résidence si l’assujettissement à l’impôt ne pouvait suffire. Ces critères sont :</p>
<ul class="spip">
<li>un foyer d’habitation permanent (il s’agit, par exemple, du lieu de situation du conjoint ou des enfants) ;</li>
<li>l’Etat où la personne possède le centre de ses intérêts vitaux (tant professionnels que privés) ;</li>
<li>l’Etat dans lequel elle séjourne de façon habituelle (notion de 183 jours de présence physique sur le territoire au cours d’une année fiscale) ;</li>
<li>à défaut, l’Etat dont elle possède la nationalité.</li>
<li></li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Dispositions conventionnelles concernant le traitement fiscal des revenus</h3>
<p><strong>Traitements, salaires, pensions et rentes</strong></p>
<p><strong>Rémunérations privées</strong></p>
<p><strong>Principe</strong></p>
<p>L’article 15, paragraphe 1 précise que les traitements et salaires d’origine privée sont, en règle générale, imposables dans l’Etat où s’exerce l’activité.<br class="autobr">Règles particulières<br class="autobr">L’article 15, paragraphe 2, déroge à ce principe et attribue le droit exclusif d’imposer les revenus correspondants à l’Etat de résidence du bénéficiaire, sous réserve du respect des trois conditions suivantes (cumulatives) :</p>
<ul class="spip">
<li>le séjour temporaire du bénéficiaire dans l’autre Etat ne dépasse pas une durée totale de 183 jours ;</li>
<li>la rémunération est payée par un employeur qui n’est pas résident de l’Etat d’exercice ;</li>
<li>la rémunération ne doit pas être à la charge d’un établissement stable ou d’une base fixe de l’employeur dans l’Etat.</li>
<li>Exemple : Monsieur X est envoyé trois mois à Séville, soit quatre-vingt-dix jours (mai, juin, juillet de l’année n) par une PME établie en France, fabricant d’articles de maroquinerie, en vue de prospecter le marché espagnol. L’entreprise qui rémunère l’intéressé ne dispose ni d’une succursale ni d’un bureau en Espagne. Dans un tel cas, Monsieur X devra déclarer ses revenus en France et ne sera pas imposé à raison des salaires dans l’Etat d’exercice de l’activité. Au contraire, si Monsieur X est envoyé du mois de février inclus au mois de novembre inclus, son séjour de plus de 183 jours en Espagne entraîne son imposition dans ce pays.</li></ul>
<p>Il résulte des dispositions de l’article 15, paragraphe 3 que les revenus professionnels des salariés employés à bord d’un navire ou d’un aéronef en trafic international ne sont imposables que dans l’Etat où se trouve la direction effective de l’entreprise.</p>
<h3 class="spip"><a id="sommaire_3"></a>Rémunérations publiques</h3>
<p><strong>Principe</strong></p>
<p>L’article 19 de la convention indique que les traitements, salaires et rémunérations analogues ainsi que les pensions de retraite payés par un Etat ou une personne morale de droit public de cet Etat ne sont imposables que dans cet Etat.</p>
<p>Exemple : Monsieur X, de nationalité française, est fonctionnaire de l’Etat Français. Il résidait en France pendant la période d’activité et décide d’aller prendre sa retraite en Espagne. Ses pensions publiques de source française resteront imposées en France ; son dossier est alors pris en charge par un centre des impôts spécial, le Centre des Impôts des Non-résidents.</p>
<p><strong>Exceptions</strong></p>
<p>Toutefois, en vertu de l’article 19, paragraphes 1-b et 2-b, cette règle ne s’applique pas lorsque le bénéficiaire possède la nationalité de l’autre Etat sans être en même temps ressortissant de l’Etat de résidence de l’intéressé.</p>
<p>Par ailleurs, les dispositions du paragraphe 3 du même article précisent que les règles fixées au paragraphe 1 dudit article ne sont pas applicables aux rémunérations ou aux pensions versées au titre de services rendus dans le cadre d’une activité industrielle ou commerciale exercée par un Etat ou une personne morale de droit public.</p>
<p>Les sommes versées à ce titre sont imposées soit dans l’Etat d’exercice de l’activité (article 15 de la convention applicable aux salaires), soit dans l’Etat de résidence du bénéficiaire (article 18 de la convention applicable aux pensions privées).</p>
<h3 class="spip"><a id="sommaire_4"></a>Pensions et rentes</h3>
<p>L’article 18 de la convention dispose que les pensions de retraite privées, ainsi que les rentes viagères, ne sont imposables que dans l’Etat dont le bénéficiaire est résident.</p>
<p>La convention ne comportant pas de disposition particulière concernant les prestations de sécurité sociale, il en résulte que les pensions privées versées au regard de la législation sociale d’un pays sont imposables dans l’Etat de résidence du bénéficiaire.</p>
<p><strong>Professeurs, chercheurs, étudiants, stagiaires</strong></p>
<p>L’article 20 prévoit que les professeurs ainsi que les chercheurs sont exonérés de toute imposition pendant deux ans dans l’Etat où ils séjournent temporairement, sur invitation d’un Etat ou d’un établissement reconnu. Ces rémunérations demeurent toutefois imposables dans l’Etat d’origine des intéressés.</p>
<p>L’article 21 indique que les étudiants et stagiaires sont exonérés dans l’Etat où ils font leurs études ou stages à raison des sommes provenant d’un autre Etat destinées à couvrir les frais d’entretien, d’études ou de formation.</p>
<p><strong>Bénéfices industriels et commerciaux</strong></p>
<p>L’article 7 dispose que les entreprises industrielles et commerciales sont imposables sur le territoire où se trouve l’établissement stable.</p>
<p>Revenus non commerciaux et bénéfices des professions non commerciales<br class="autobr">Conformément aux dispositions de l’article 14 paragraphe 1 de la convention, les revenus provenant de l’exercice d’une profession libérale sont imposables dans l’Etat sur le territoire duquel se trouve l’installation permanente où s’exerce de façon régulière l’activité personnelle.</p>
<p>L’article 12, paragraphe 1, indique que les revenus non commerciaux (redevances et droits d’auteur) sont en principe imposables dans l’Etat de résidence du bénéficiaire, sous réserve des dispositions spécifiques du paragraphe 2 du même article permettant à l’Etat de provenance des redevances de les imposer à un taux ne pouvant excéder 5%.</p>
<p><strong>Revenus immobiliers</strong></p>
<p>L’article 6, paragraphe 1, dispose que les revenus des biens immobiliers (y compris les bénéfices des exploitations agricoles ou forestières) sont imposables dans l’Etat où ils sont situés. Cette règle s’applique également aux gains provenant de la cession ou de l’échange desdits biens ou droits selon les dispositions de l’article 13, paragraphe 1.</p>
<p>Conformément aux modalités prévues par l’article 24 de la convention, la double imposition est corrigée, le cas échéant, dans l’Etat de résidence du bénéficiaire.<br class="autobr">Le paragraphe 5 de l’article 13 précise que les gains provenant de l’aliénation de tous biens autres que ceux expressément visés par les points précédents restent imposables dans l’Etat de résidence du cédant.</p>
<p><strong>Revenus de capitaux mobiliers</strong></p>
<p><strong>Les dividendes</strong></p>
<p>Ce terme désigne les revenus provenant d’actions, actions ou bons de jouissance, parts de mines, parts de fondateur ou autres parts bénéficiaires, à l’exception des créances et les revenus d’autres parts sociales assimilés aux revenus d’actions.<br class="autobr">De manière générale, l’article 10, paragraphe 2-a, précise que les intérêts provenant d’un Etat et payés à un résident de l’autre Etat font l’objet d’une imposition partagée entre les deux Etats. Le taux de la retenue appliquée dans l’Etat de la source ne peut dépasser 15%. La double imposition est corrigée dans l’Etat de résidence du bénéficiaire.</p>
<p>Par ailleurs, les dispositions cumulées du paragraphe 2 alinéa a de l’article 10, et du paragraphe 3 de ce même article, précisent que le transfert de l’avoir fiscal français (sous déduction de la retenue à la source de 15 %) s’effectue uniquement pour les distributions en faveur des personnes physiques résidentes d’Espagne et des personnes morales résidentes d’Espagne qui détiennent directement ou indirectement moins de 10% du capital social de la société française distributrice, à condition que le bénéficiaire soit effectivement imposé en Espagne.</p>
<p><strong>Les intérêts</strong></p>
<p>Ce terme désigne les revenus des fonds publics, des obligations d’emprunts, des obligations d’emprunts et des créances de toute nature ainsi qu’aux intérêts produits par des titres négociables, les bons de caisse et les intérêts de créances ordinaires.<br class="autobr">L’article 11, paragraphe 2, précise que les intérêts provenant d’un Etat et payés à un résident de l’autre Etat font l’objet d’une imposition partagée entre les deux Etats. Le taux de la retenue appliquée dans l’Etat de la source ne peut dépasser 10%. La double imposition est corrigée dans l’Etat de résidence du bénéficiaire.</p>
<p>Toutefois, le paragraphe 3 du même article prévoit les cas dans lesquels les intérêts restent exclusivement imposables dans l’Etat de résidence du bénéficiaire effectif des revenus.</p>
<p><strong>Les autres revenus</strong></p>
<p>Concernant les revenus qui ne sont pas expressément cités dans un article spécifique de la convention, celle-ci fixe le principe de l’imposition exclusive dans l’Etat de résidence de leur bénéficiaire, sauf, sous certaines conditions, s’ils sont imputables à un établissement stable ou à une installation fixe situés dans l’autre Etat.</p>
<p><strong>Imposition de la fortune</strong></p>
<p>L’article 23, paragraphe 1 alinéas a et b, prévoit que les biens immobiliers et assimilés (parts ou actions de sociétés immobilières transparentes) sont imposables dans l’Etat où ils sont situés.</p>
<p>Le paragraphe 3 du même article précise par ailleurs que les biens mobiliers faisant partie de l’actif d’un établissement stable ou constitutif d’une base fixe sont imposables dans l’Etat de situation de l’établissement stable ou la base fixe.<br class="autobr">Par ailleurs, le paragraphe 4 prévoit que l’imposition de la fortune constituée par des navires et aéronefs en trafic international est attribuée à l’Etat dans lequel se trouve le siège de direction effective de l’entreprise.</p>
<p>Pour les autres biens, l’imposition est dévolue à l’Etat de résidence du détenteur aux termes du paragraphe 5.</p>
<h3 class="spip"><a id="sommaire_5"></a>Règles d’imposition</h3>
<p><strong>Elimination de la double imposition</strong></p>
<p>L’élimination de la double imposition pour les résidents de France qui perçoivent des revenus de source espagnole s’opère aux termes des paragraphes 2-b et c, et 2-a de l’article 25 de la convention, selon deux méthodes :</p>
<ul class="spip">
<li>la méthode de l’imputation ou du crédit de l’impôt espagnol sur l’impôt dû en France.</li>
<li>la méthode de l’exemption avec taux effectif.</li></ul>
<p>La première méthode s’applique aux dividendes, intérêts, redevances. Elle consiste à inclure ces revenus (de source espagnole) dans la base imposable en France pour leur montant brut et à déduire le montant de l’impôt espagnol effectivement payé dans la limite du montant de l’impôt français correspondant à ces revenus.</p>
<p>La deuxième méthode, dite du taux effectif, concerne les autres revenus de source espagnole, qui peuvent être exonérés en France quand ils sont imposables en Espagne.</p>
<p>Cette mesure permet la prise en compte des revenus exonérés en France aux fins de la détermination du taux applicable aux autres revenus (non exonérés).</p>
<p><sub>Mise à jour : novembre 2015</sub></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/fiscalite/article/convention-fiscale). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
