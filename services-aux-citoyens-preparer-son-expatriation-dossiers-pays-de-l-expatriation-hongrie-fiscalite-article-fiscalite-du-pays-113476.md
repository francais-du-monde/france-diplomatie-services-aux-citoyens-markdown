# Fiscalité du pays

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/fiscalite/article/fiscalite-du-pays-113476#sommaire_1">Présentation</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/fiscalite/article/fiscalite-du-pays-113476#sommaire_2">Année fiscale</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/fiscalite/article/fiscalite-du-pays-113476#sommaire_3">Barème de l’impôt </a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/fiscalite/article/fiscalite-du-pays-113476#sommaire_4">Quitus fiscal</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/fiscalite/article/fiscalite-du-pays-113476#sommaire_5">Solde du compte en fin de séjour</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/fiscalite/article/fiscalite-du-pays-113476#sommaire_6">Coordonnées des centres d’information fiscale </a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/fiscalite/article/fiscalite-du-pays-113476#sommaire_7">Inscription auprès du fisc</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/fiscalite/article/fiscalite-du-pays-113476#sommaire_8">Impôts indirects</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Présentation</h3>
<p>Selon la législation fiscale hongroise relative à l’impôt sur le revenu des personnes physiques, et la convention fiscale de 1982 entre la France et la Hongrie, les particuliers français qui résident en Hongrie pendant plus de 183 jours par année civile, qui ont leur seule résidence permanente ou dont le centre des intérêts vitaux se trouve en Hongrie sont traités comme des résidents et sont assujettis à l’impôt sur le revenu des personnes physiques sur leurs revenus mondiaux. Les non-résidents sont assujettis à l’impôt sur leurs revenus de sources hongroises.</p>
<p>L’IR est applicable sur le revenu et sur la fortune, sur les gains provenant de l’aliénation de biens mobiliers ou immobiliers, sur le montant global des salaires payés par les entreprises ainsi que sur les plus-values. Il est calculé individuellement pour chaque personne physique ; il n’y a pas de regroupement par foyer fiscal. Pour une famille ayant un ou deux enfants, l’un des parents ou les deux parents (se répartissant proportionnellement le montant total de la déduction) bénéficient d’un crédit d’impôt de 62 500 HUF (soit approximativement 208 euros) par enfant et par mois sur la base imposable (soit avant les déductions de l’impôt sur le revenu (16%) + assurance maladie (7%) + cotisation de retraite (10%). Pour trois enfants et plus ce crédit d’impôt est de 206 250 HUF (soit approximativement 687,5 euros) par enfant et par mois. Un ressortissant étranger résidant en Hongrie ne peut avoir droit au crédit d’impôt que s’il ne bénéficie pas de ce type de droit dans un autre pays et si au moins 75% de son revenu est soumis à l’impôt en Hongrie.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://nav.gov.hu/nav/szolgaltatasok/kalkulatorok/csaladi_kedvezmeny_kalk" class="spip_out" rel="external">Calculateur en ligne du crédit d’impôt</a></p>
<p><strong>Régime d’impôt pour les expatriés ou les non-résidents</strong></p>
<p>Les expatriés sont soumis au même taux d’imposition que les autres résidents fiscaux en Hongrie. Toutefois sont exclus de leur base imposable certains compléments de revenus, tels que les remboursements de voyages professionnels prévus dans le contrat et dans le cas d’un détachement, la mise à disposition d’un logement. En revanche les cotisations versées par les expatriés sur les comptes de fonds de pensions étrangers ne sont pas déductibles du revenu imposable en Hongrie.</p>
<p>Les non-résidents sont assujettis à l’impôt sur leurs revenus provenant de sources hongroises, c’est-à-dire ceux versés par un employeur hongrois ou bien versés en contrepartie d’une activité effectuée sur le territoire hongrois. Ces revenus doivent faire l’objet d’une déclaration d’impôt annuelle.</p>
<h3 class="spip"><a id="sommaire_2"></a>Année fiscale</h3>
<p><strong>Impôt sur le revenu applicable aux personnes physiques résidentes en Hongrie</strong></p>
<p>L’année d’imposition correspond à l’année calendaire. Il n’existe pas de foyer fiscal, ainsi chaque membre du couple marié est imposé séparément.</p>
<p>En vertu des dispositions légales, les employeurs et les agents payeurs sont tenus de déposer une déclaration électronique mensuelle sur tous les impôts, cotisations et autres données, définis par la loi, liés aux paiements des rémunérations et prestations versés aux particuliers et générant des obligations fiscales et/ou des charges sociales. <strong>Les contribuables tenus de soumettre une déclaration électronique sur les cotisations sont obligés de s’acquitter de toutes leurs obligations de déclarations et de fourniture de données envers la Direction Nationale des Impôts et de la Douane (Nemzeti Adó- és Vámhivatal (NAV) par voie électronique.</strong></p>
<p>Les particuliers ne relevant pas de la catégorie mentionnée au paragraphe précédent sont libres de choisir la modalité de dépôt de leur déclaration annuelle de revenus, et peuvent notamment opter soit pour la voie traditionnelle, le formulaire sur papier, soit pour la voie électronique.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://en.nav.gov.hu/FAQs" class="spip_out" rel="external">Foire aux questions</a> et <a href="http://en.nav.gov.hu/e_services/E_returns_extra" class="spip_out" rel="external">E-returns</a> sur le site du Portail gouvernemental des impôts et des douanes.</p>
<h3 class="spip"><a id="sommaire_3"></a>Barème de l’impôt </h3>
<h4 class="spip">Impôt sur le revenu applicable aux personnes physiques résidentes en Hongrie</h4>
<p><strong>Impôt sur le revenu des personnes physiques</strong></p>
<p>L’assiette de l’impôt est l’ensemble des revenus imposables (revenu, fortune, gains provenant de l’aliénation des biens mobiliers ou immobiliers, montant global des salaires payés par les entreprises, plus-values), mais les contribuables peuvent bénéficier depuis le 1er janvier 2011 d’un avantage fiscal en fonction du nombre d’enfants. En 2013, les différents taux en fonction des tranches de revenu, ont été remplacés par un taux unique, fixé à 16%. L’impôt sur le revenu des personnes physiques fait l’objet d’une déclaration annuelle des revenus des contribuables à la <a href="http://www.nav.gov.hu/" class="spip_out" rel="external">Direction Nationale des Impôts et de la Douane (NAV)</a> par voie électronique ou postale.</p>
<h4 class="spip">Impôt sur les sociétés </h4>
<p><strong>Taux d’imposition des sociétés </strong></p>
<table class="spip">
<thead><tr class="row_first"><th id="id6e7e_c0">Principaux impôts </th><th id="id6e7e_c1">Assiette de l’impôt  </th><th id="id6e7e_c2">Taux d’imposition  </th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="id6e7e_c0">Impôt sur les sociétés</td>
<td headers="id6e7e_c1">Résultat fiscal (ou revenu minimum)</td>
<td headers="id6e7e_c2">10% / 19%</td></tr>
<tr class="row_even even">
<td headers="id6e7e_c0">Impôt de solidarité</td>
<td headers="id6e7e_c1">Résultat fiscal corrigé</td>
<td headers="id6e7e_c2">4%</td></tr>
<tr class="row_odd odd">
<td headers="id6e7e_c0">Taxe professionnelle locale</td>
<td headers="id6e7e_c1">Chiffre d’affaires net diminué des achats de matières premières et de fournitures et de certains coûts de RD</td>
<td headers="id6e7e_c2">2%</td></tr>
<tr class="row_even even">
<td headers="id6e7e_c0">Taxe de RD</td>
<td headers="id6e7e_c1">Assiette de la taxe professionnelle locale</td>
<td headers="id6e7e_c2">0,3%</td></tr>
</tbody>
</table>
<p>La fiscalité des sociétés est régie par la loi sur l’impôt sur les sociétés. Sont imposables en Hongrie les sociétés résidentes (les sociétés hongroises et les sociétés étrangères ayant leur centre de directions en Hongrie) et les sociétés non-résidentes exerçant une activité commerciale en Hongrie par le biais d’un établissement permanent.</p>
<p>Les sociétés résidentes sont imposées sur la totalité de leurs bénéfices mondiaux, tandis que les sociétés non-résidentes ne sont imposées que sur les bénéfices réalisés dans le cadre de leur établissement permanent en Hongrie. L’assiette de l’impôt (ou résultat fiscal) est déterminée sur la base du résultat comptable avant impôt corrigé de certains éléments, suivant un traitement particulier selon les règles fiscales hongroises.</p>
<p>L’impôt sur les sociétés concerne les sociétés commerciales, les entrepreneurs individuels, les associations, et les organisations sociales. Dans le cas où le chiffre d’affaires est inférieur à 500 M HUF (1,8 M EUR), l’IS est de 10%, mais si le chiffre d’affaires dépasse la limite de 500 M HUF le taux est de 19%. Le règlement de l’impôt s’effectue par acomptes, sur la base de l’impôt de l’année précédente. La déclaration de l’IS doit être effectuée annuellement avant le 31 mai. Toutes les sociétés enregistrées en Hongrie doivent déclarer leur impôt par voie électronique sur le site de la <a href="http://en.nav.gov.hu/" class="spip_out" rel="external">Direction Nationale des Impôts et de la Douane (NAV)</a>.</p>
<p><strong>Taxe de crise et nouvelle politique fiscale</strong></p>
<p>La taxe de crise est une taxe qui a été instaurée après l’arrivée au pouvoir du nouveau Gouvernement en avril 2010, afin de réduire une partie du déficit budgétaire. Elle concerne trois secteurs : les établissements de crédit (30% du bénéfice avant impôt), les fournisseurs d’énergie (1,05% du CA) et des sociétés de télécommunication (0% - 6,5% du CA).</p>
<p>Les changements intervenus en 2012 en matière fiscale sont les suivants :</p>
<ul class="spip">
<li>le régime d’imposition applicable au secteur des assurances est unifié par un nouvel impôt, qui se substitue aux trois prélèvements antérieurs : taxe financière, taxe d’accident et taxe de prévention des incendies. Depuis 2013, les assureurs ne paient plus la taxe exceptionnelle sur les entreprises du secteur financier. La taxe unifiée varie selon le type de produit (accident, assurance véhicule, le taux de l’assurance obligatoire des véhicules s’établissant à 30%), les produits d’assurance-vie sont exonérés d’impôt ;</li>
<li>la taxe bancaire « spéciale » a été réduite de moitié en 2013 et est maintenue en 2014. Une nouvelle taxe sur les transactions financières a été introduite en janvier 2013 : ses taux sont relativement faibles (0,3% pour les transferts d’argent et 0,6% pour les retraits et prélèvements) mais s’appliquent à une base très large des opérations de la clientèle, le prélèvement est opéré sur les banques ;</li>
<li>la taxe de crise sur le secteur de l’énergie dite « Robin Hood » (11 à 18%) reste en vigueur et s’additionne en pratique au taux d’impôt sur les sociétés.</li></ul>
<p><strong>Taxe locale</strong></p>
<p>Les sociétés doivent payer une taxe professionnelle locale, qui s’apparente à la taxe professionnelle française ; elle est à la fois un impôt direct et une taxe sur le chiffre d’affaires. Elle concerne les activités professionnelles exercées à titre permanent sur le territoire d’une Commune.</p>
<p>Son taux varie en fonction des communes, sans pouvoir excéder 2% du chiffre d’affaires net.</p>
<p><strong>Impôt sur les dividendes</strong></p>
<p>Les dividendes de source hongroise versés à une société résidant en Hongrie sont exonérés de l’impôt sur les dividendes. Depuis le 1er janvier 2006, les dividendes de source hongroise versés à une société étrangère sont également exonérés de l’impôt sur les dividendes.</p>
<p>Une retenue à la source de 25% est appliquée aux dividendes de source hongroise versés aux particuliers, quel que soit leur pays de résidence (sauf règles différentes prévues par une convention fiscale internationale).</p>
<p><strong>Impôt simplifié</strong></p>
<p>Depuis 2007, un impôt minimum sur les sociétés est appliqué. L’objectif est de permettre à l’administration fiscale de mieux contrôler les entreprises qui n’ont pas réalisé de profit (bénéfice supérieur à 2% du CA).</p>
<p>En 2012, deux nouveaux impôts ont été introduits : l’impôt sur les petits entrepreneurs contribuables (KATA) et l’impôt sur les petites entreprises (KIVA), en plus de l’impôt simplifié sur les entreprises individuelles (EVA).</p>
<p>L’impôt KATA, dont le montant est de 50 000 HUF (167 EUR) par mois, est applicable aux entrepreneurs individuels et aux petites entreprises dont le revenu annuel ne dépasse pas les 6 M HUF (21 482 EUR).</p>
<p>Les entreprises ayant un revenu annuel inférieur à 500 M HUF (1,7 M EUR), peuvent opter pour l’impôt KIVA fixé au taux unique de 16% (IS + cotisations sociales).</p>
<p>Enfin, l’impôt simplifié (EVA) concerne les entrepreneurs individuels et les petites entreprises qui ont une activité depuis au moins deux ans et des revenus annuels inférieurs à 25 M HUF (83 918 EUR). L’assiette correspond au chiffre d’affaires TTC et le taux applicable s’élève à 30%. L’EVA remplace l’impôt sur les sociétés et l’impôt sur les dividendes versés aux personnes physiques, ayant qualité d’associé. Toutefois, ceux qui ont choisi l’EVA, ne sont pas assujettis à la TVA de telle sorte qu’ils ne peuvent pas déduire la TVA acquittée et ne sont pas tenus de reverser la TVA collectée.</p>
<p><strong>EKHO</strong></p>
<p>Depuis le 1er janvier 2006, toute personne exerçant une activité individuelle dans le domaine des arts, de la culture ou des médias, en tant que musicien, compositeur, éditeur ou journaliste peut opter pour un paiement simplifié de l’impôt et des cotisations sociales, appelé "EKHO". Le commanditaire, lié par contrat, est alors tenu de choisir également ce type d’imposition pour le paiement des cotisations sociale.</p>
<p><strong>Taxe de RD</strong></p>
<p>Depuis 2004, les sociétés versent une taxe de contribution à l’innovation. Son assiette est identique à celle de la taxe professionnelle locale et son taux s’élève à 0,3% depuis 2006.</p>
<p><strong>Autres impôts</strong></p>
<p>Les sociétés et les particuliers sont assujettis à l’imposition sur :</p>
<ul class="spip">
<li>la cession de biens immobiliers hongrois ;</li>
<li>la cession des droits liés à ces biens ;</li>
<li>la cession de voitures</li></ul>
<p>Droits de mutation sur l’acquisition de biens immobiliers et de voitures</p>
<table class="spip">
<thead><tr class="row_first"><th id="idebec_c0"> Type de bien </th><th id="idebec_c1"> Taux d’imposition </th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="idebec_c0">Immobilier</td>
<td headers="idebec_c1">4% pour les immobiliers d’une valeur maximale de 1 Mds HUF (3,3 M EUR),
<p>2%, pour les immobiliers d’une valeur supérieure à 1 Mds HUF (3,3 M EUR), le montant de l’imposition est plafonné à 200 M HUF (600 mille EUR)</p>
</td></tr>
<tr class="row_even even">
<td headers="idebec_c0">Voitures</td>
<td headers="idebec_c1">300-850 HUF (1-2,8 EUR) par kW (en fonction de l’âge et de la puissance de la voiture)</td></tr>
</tbody>
</table>
<p><i>Source : Guide des affaires en Hongrie, 2013 (Ubifrance) et Site internet de la NAV</i></p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://en.nav.gov.hu/taxation" class="spip_out" rel="external">Portail gouvernemental des impôts et des douanes</a> (informations en anglais) </p>
<h3 class="spip"><a id="sommaire_4"></a>Quitus fiscal</h3>
<p>Un quitus fiscal n’est pas exigé à la sortie du pays.</p>
<h3 class="spip"><a id="sommaire_5"></a>Solde du compte en fin de séjour</h3>
<p>Un expatrié peut solder son compte en fin de séjour.</p>
<h3 class="spip"><a id="sommaire_6"></a>Coordonnées des centres d’information fiscale </h3>
<p>Pour plus d’informations sur la fiscalité hongroise s’adresser à :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://en.nav.gov.hu/contact/tax_contact" class="spip_out" rel="external">Service central clientèle</a></p>
<h3 class="spip"><a id="sommaire_7"></a>Inscription auprès du fisc</h3>
<p>Les contribuables doivent s’inscrire auprès de l’administration fiscale avant le début de leurs activités commerciales en Hongrie. Le numéro d’impôt généré lors de l’enregistrement est également un numéro d’identification à la TVA.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://en.nav.gov.hu/taxation/registration" class="spip_out" rel="external">Portail des impôts et des douanes</a></p>
<h3 class="spip"><a id="sommaire_8"></a>Impôts indirects</h3>
<p>Le taux standard de la TVA en Hongrie, pour les produits et services ordinaires est de 27%. Le taux réduit de 5% est applicable aux produits pharmaceutiques et à certains équipements médicaux, les services d’aides aux aveugles, les livres et journaux. Un taux de 17% est applicable pour les hôtels et les produits alimentaires de consommation courante, comme le lait.</p>
<p>Les droits d’accise sont des taxes indirectes sur la consommation ou l’utilisation de certains produits tels que les boissons alcoolisées, les tabacs manufacturés et les carburants.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.tresor.economie.gouv.fr/pays/hongrie" class="spip_out" rel="external">Service économique français en Hongrie</a></p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/fiscalite/article/fiscalite-du-pays-113476). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
