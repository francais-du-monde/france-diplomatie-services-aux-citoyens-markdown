# Emploi, stage

<h2 class="rub22403">Entretien d’embauche</h2>
<p><strong>Apparence et attitude</strong></p>
<p>En Suisse comme ailleurs, l’entretien d’embauche est une étape essentielle. Il doit être pris au sérieux et requiert une certaine préparation. Toutefois, l’entretien d’embauche ne garantit pas l’emploi. Il existe deux types d’entretiens d’embauche : l’entretien de présélection et l’entretien d’embauche final. Ce dernier est généralement plus long et peut parfois être très éprouvant.</p>
<p>Quelques règles essentielles pour réussir l’entretien :</p>
<ul class="spip">
<li>S’informer impérativement sur l’entreprise et ses activités en particulier le domaine qui vous concerne ;</li>
<li>Posséder les bases essentielles de la culture suisse et de la culture d’entreprise en Suisse ;</li>
<li>Être ponctuel. La ponctualité est un trait culturel suisse ;</li>
<li>Être discipliné et obéissant. En Suisse, on n’apprécie guère la contestation et le rapport de force. On privilégie le consensus ;</li>
<li>Être vêtu(e) d’une tenue propre et adaptée de type classique ;</li>
<li>Ne pas paraitre prétentieux sur ses compétences et son expérience professionnelles ;</li>
<li>Répondre aux questions en évitant les hors-sujets et les bavardages ;</li>
<li>Se concentrer sur ses compétences et ses capacités d’adaptation et à travailler en équipe ;</li>
<li>Exprimer son souhait de faire bénéficier l’entreprise de son expérience.</li></ul>
<p>Avoir sur soi son curriculum-vitae et tous documents susceptibles d’être demandés par le recruteur (lettres de référence, réalisation d’ouvrage, pièce d’identité, original du permis de travail, etc.).</p>
<p>Au préalable ; se constituer une sorte de pense-bête avec les trois points ci-dessous :</p>
<ul class="spip">
<li>Résumé des étapes essentielles du CV ;</li>
<li>Liste de ce que l’employeur doit absolument savoir sur vous ;</li>
<li>Motivations à l’égard de l’emploi en question.</li></ul>
<p>Il est également conseillé de simuler un entretien avec l’aide d’une personne de votre entourage afin d’améliorer votre façon de faire et d’être. Cet exercice est riche d’enseignement. Il vous donnera de la confiance et de l’assurance, ingrédients déterminants pour aborder sereinement l’entretien d’embauche.</p>
<p><strong>Questions préférées des recruteurs</strong></p>
<p>Les recruteurs aiment à orienter l’entretien vers les points suivants :</p>
<ul class="spip">
<li>Une brève présentation du candidat</li>
<li>Un résumé des parcours académique et professionnel afin de s’assurer de la cohérence du CV</li>
<li>La présentation d’un sujet que vous êtes censé connaître mieux que quiconque</li>
<li>L’opportunité de dire des choses sur vous qui n’apparaissent pas dans votre CV</li>
<li>Le souhait de vous exprimer à l’égard du poste pour lequel vous postulez et sur l’entreprise</li>
<li>Le souhait de vous connaître y compris votre personnalité</li>
<li>Tester votre niveau linguistique si le poste requiert la pratique d’une langue étrangère.</li></ul>
<p><strong>Erreurs à éviter</strong></p>
<p>Comme indiqué précédemment, il est essentiel de préparer son entretien d’embauche. Parmi les erreurs à éviter, nous pouvons citer les suivantes :</p>
<ul class="spip">
<li>Le retard</li>
<li>La négligence vestimentaire</li>
<li>L’incapacité de se présenter brièvement et d’aller à l’essentiel</li>
<li>L’incohérence du curriculum-vitae avec les propos que vous formulez</li>
<li>La confusion</li>
<li>Le mensonge</li>
<li>Le manque d’humilité</li>
<li>Le bavardage</li>
<li>La nervosité</li>
<li>La condescendance</li>
<li>L’absence de motivation.</li></ul>
<p><strong>Négociation du salaire</strong></p>
<p>Lors de l’entretien d’embauche, il n’est pas incongru d’aborder la question du salaire. Si la mise en relation avec l’entreprise s’est faite par le biais d’une agence de placement, vous serez informé du salaire prévu pour l’emploi proposé. Certains employeurs tenteront néanmoins de revoir le salaire à la baisse prétextant votre manque d’expérience en Suisse ou moyennant une rapide ascension des échelons. Il vous appartient de formuler gentiment votre souhait. L’idée est de rester dans le cadre fixé par l’agence de placement.</p>
<p>Dans tous les cas, une recherche sur les salaires en Suisse vous sera utile et permettra de vous situer.</p>
<p>N’hésitez pas à demander des précisions sur les échelons, sur l’attribution des primes et sur l’ensemble des avantages fournis par l’entreprise (assurance maladie, montant du 2ème pilier, avantages divers, etc.).</p>
<p>Enfin, il ne faut pas que la question du salaire soit perçue comme un problème ou un objet de conflit.</p>
<p><strong>Entretien téléphonique</strong></p>
<p>L’entretien d’embauche téléphonique n’est pas développé. En effet, pour des raisons évidentes de crédibilité, les entretiens de type face-à-face demeurent la pratique la plus courante. Les emplois temporaires pourront parfois être confiés à un candidat entendu lors d’un entretien téléphonique. Il n’empêche que ce dernier sera convoqué en personne pour finaliser l’entente. C’est également une occasion de vérifier les conditions de séjour (type et validité du permis de travail) du postulant, de jeter un œil sur ses références professionnelles et de se faire une idée bien réelle de la personne.</p>
<p><strong>Après l’entretien</strong></p>
<p>Avant de quitter l’employeur et en fonction de ce qu’il vous dira, il ne faut pas oublier de lui demander aimablement le délai nécessaire pour obtenir des nouvelles.</p>
<p>Si vous êtes retenu(e), l’entreprise prendra contact avec vous dans un futur proche. Dans le cadre d’une candidature spontanée qui aura débouché sur un entretien, il est également possible que votre profil intéresse vivement l’entreprise mais qu’aucun poste ne soit disponible au moment de l’entretien. Il vous appartiendra de maintenir régulièrement le contact avec celle-ci. Dans tous les cas, il est souhaitable de ne pas se faire oublier et, à défaut de nouvelles, il ne faut pas hésiter à appeler l’entreprise pour connaître la suite donnée à votre candidature. Il faut montrer votre motivation sans pour autant paraître insistant et impatient.</p>
<p>Si le recruteur ne vous donne pas sa carte de visite, n’hésitez pas à la lui réclamer et le jour même envoyez-lui un courriel de remerciement. Si malheureusement, l’entreprise ne vous recrute pas, conservez ses coordonnées et la carte de visite de votre interlocuteur. Vous pouvez essayer de demander gentiment les motifs du refus et un appui pour une orientation vers quelqu’un susceptible de vous aider. L’objectif étant de se constituer un réseau.</p>
<p><i>Mise à jour : février 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-suisse-emploi-stage-article-stage.md" title="Stage">Stage</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-suisse-emploi-stage-article-entretien-d-embauche-108897.md" title="Entretien d’embauche">Entretien d’embauche</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-suisse-emploi-stage-article-lettre-de-motivation-105247.md" title="Lettre de motivation">Lettre de motivation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-suisse-emploi-stage-article-curriculum-vitae-105246.md" title="Curriculum vitae">Curriculum vitae</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-suisse-emploi-stage-article-recherche-d-emploi.md" title="Recherche d’emploi">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-suisse-emploi-stage-article-reglementation-du-travail.md" title="Règlementation du travail">Règlementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-suisse-emploi-stage-article-marche-du-travail.md" title="Marché du travail">Marché du travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/emploi-stage/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
