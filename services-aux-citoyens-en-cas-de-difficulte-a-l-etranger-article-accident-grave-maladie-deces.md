# Accidents graves,  décès à l’étranger

<p>Le consulat est, en principe, prévenu par les autorités locales de tout accident grave concernant un Français.</p>
<p><strong>En cas d’accident grave,</strong> le consulat pourra prévenir votre famille et envisager avec elle les mesures à prendre. Dans la mesure du possible, le consulat se procurera les rapports de police et, si nécessaire, les rapports médicaux.</p>
<p><strong>En cas de maladie,</strong> le consulat pourra vous mettre en relation avec un médecin agréé par ses services et tiendra à votre disposition, dans la mesure du possible, une liste de médecins spécialisés. Les coordonnées de ces praticiens peuvent généralement être consultées sur le <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/article/annuaire-des-representations-francaises-a-l-etranger" class="spip_in">site internet du consulat</a>.</p>
<p>Qu’il s’agisse d’un accident grave ou d’une maladie, <strong>les honoraires et les frais médicaux restent à votre charge</strong>.</p>
<p><strong>En cas de décès</strong>, le consulat prendra contact avec la famille pour l’aviser et la conseiller dans les formalités légales de rapatriement ou d’inhumation de la dépouille mortelle ou de ses cendres. Les frais sont supportés soit par la famille, soit par l’organisme d’assurance du défunt.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li>la rubrique <a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/infos-pratiques/assistance-aux-francais/deces-a-l-etranger/article/deces-a-l-etranger" class="spip_in">Décès à l’étranger</a> de Conseils aux voyageurs.</li>
<li>la rubrique <a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/infos-pratiques/assistance-aux-francais/victimes-a-l-etranger-20988/article/victimes-a-l-etranger" class="spip_in">Victimes à l’étranger</a> de Conseils aux voyageurs.</li>
<li>la rubrique <a href="services-aux-citoyens-preparer-son-expatriation-protection-sociale.md" class="spip_in">Protection sociale</a> dans le guide <a href="services-aux-citoyens-preparer-son-expatriation.md" class="spip_in">Préparer son expatriation</a></li></ul>
<p><i>Mise à jour : août 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/en-cas-de-difficulte-a-l-etranger/article/accident-grave-maladie-deces). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
