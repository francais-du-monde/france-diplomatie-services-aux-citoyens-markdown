# Principaux cas particuliers

<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>dossiers d’adoption</strong> : présenter à l’appui des documents à légaliser l’agrément délivré par le Conseil général.
Pour les dossiers d’adoption en CHINE, il est également demandé la lettre d’accord de l’association en charge du dossier. </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>légalisation des actes à caractère religieux</strong> : il s’agit d’actes sous-seing privé qui ne pourront être légalisés par le ministère des Affaires étrangères sans avoir été au préalable légalisés par une mairie.</p>
<p><i>Mise à jour : juillet 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/legalisation-et-notariat/legalisation-et-certification-de-signatures/article/principaux-cas-particuliers). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
