# Vie pratique

<h2 class="rub23576">Logement</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/vie-pratique/#sommaire_1">Où se loger ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/vie-pratique/#sommaire_2">Conditions de location</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/vie-pratique/#sommaire_3">Hôtels</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/vie-pratique/#sommaire_4">Electricité</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/vie-pratique/#sommaire_5">Electroménager</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Où se loger ?</h3>
<p><strong>Ljubljana</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Loyer mensuel quartier résidentiel</strong></td>
<td>euros</td></tr>
<tr class="row_even even">
<td>Studio</td>
<td>600 - 800</td></tr>
<tr class="row_odd odd">
<td>3 pièces</td>
<td>1000 - 1400</td></tr>
<tr class="row_even even">
<td>5 pièces</td>
<td>1600 - 2500</td></tr>
<tr class="row_odd odd">
<td>Villa</td>
<td>2600 - 3000</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_2"></a>Conditions de location</h3>
<p>L’emplacement est le critère principal pour fixer le montant du loyer.</p>
<p>A Ljubljana, le centre historique, Rozna Dolina, Murgle et Trnovo sont des zones résidentielles. Certains quartiers sont à éviter comme celui de Fuzine.</p>
<p>Le prix annoncé du loyer ne comprend pas les charges (eau, électricité, téléphone, ordures ménagères, chauffage etc.). Les dépenses de chauffage peuvent être très élevées en raison de l’hiver long et rigoureux, et peuvent alors représenter 25 à 30% du loyer. Par exemple, les dépenses en chauffage pour un appartement de deux pièces s’élèvent en moyenne à 150 euros par mois. Les loyers sont payables en euros, en liquide ou bien par virement bancaire suivant les termes du contrat de location. Il est suggéré de disposer d’une version anglaise ou française du contrat de bail.</p>
<p>Un ou deux mois de caution sont exigés selon les propriétaires. Un mois de loyer payable à l’avance. Ces prix sont les prix des loyers bruts. Il convient d’ajouter les charges dont le montant est élevé.</p>
<p>Le coût de location d’une place de parking couverte est d’environ 100 euros.</p>
<p><strong>Recherche d’un logement</strong></p>
<p>Les principaux moyens pour rechercher un logement sont les petites annonces et les agences immobilières. Ces dernières limitent généralement leurs services à la recherche du logement. Il faut donc traiter directement avec le propriétaire une fois le contrat de location signé.</p>
<p>Le marché immobilier slovène se caractérise par une offre limitée de locations. Les meublés et les appartements sont les logements les moins difficiles à trouver. Il est possible de trouver des villas à proximité immédiate de Ljubljana. Il faut compter de 15 jours à deux mois pour aboutir dans la recherche d’un logement.</p>
<p>Si la location est trouvée par l’intermédiaire d’une agence immobilière, le montant de la commission est le plus souvent d’un mois de loyer. Le propriétaire exige le plus souvent un ou deux mois de loyer comme dépôt de garantie. Les baux sont généralement conclus pour un an renouvelables par tacite reconduction. Un état des lieux est nécessaire et le paiement du loyer s’effectue d’avance. Les prix sont indiqués en euros.</p>
<p>Vous trouverez la liste de quelques agences immobilières sur le site dédié aux locations immobilières :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.nepremicnine.net/" class="spip_out" rel="external">Nepremicnine.net</a></p>
<p><strong>Acheter un logement</strong></p>
<p>Il n’existe aucune restriction pour les citoyens de l’Union européenne qui souhaiteraient acheter un bien immobilier en Slovénie. Toutes les informations relatives à ce sujet sont disponibles en anglais sur <a href="http://www.mp.gov.si/si/" class="spip_out" rel="external">le site du ministère slovène de la Justice</a>.</p>
<p><strong>Quelques sites utiles</strong></p>
<ul class="spip">
<li><a href="http://www.stoja-trade.si/" class="spip_out" rel="external">Stoja-trade.si</a></li>
<li><a href="http://www.abc.si/" class="spip_out" rel="external">Abc.si</a></li>
<li><a href="http://www.property.si/" class="spip_out" rel="external">Property.si</a></li>
<li><a href="http://www.ljubljananepremicnine.si/" class="spip_out" rel="external">Ljubljananepremicnine.si</a></li>
<li><a href="http://www.occ-nepremicnine.com/" class="spip_out" rel="external">Occ-nepremicnine.com</a></li>
<li><a href="http://www.gradex.si/" class="spip_out" rel="external">Gradex.si</a></li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Hôtels</h3>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Prix moyen d’une chambre d’hôtel (chambre double)</strong></td>
<td>euros</td></tr>
<tr class="row_even even">
<td>Grand tourisme</td>
<td>150</td></tr>
<tr class="row_odd odd">
<td>Moyen tourisme</td>
<td>80</td></tr>
</tbody>
</table>
<p><strong>Auberges de jeunesse</strong></p>
<p>On compte une vingtaine d’auberges de jeunesse référencées en Slovénie. Pour plus d’informations :</p>
<p><strong>PZS-Hostelling International Slovenia </strong><br class="manualbr">Gosposvetska 84 <br class="manualbr">2000 MARIBOR <br class="manualbr">Tél : (02) 234 21 37 <br class="manualbr">Télécopie : (02) 234 21 36 <br class="manualbr">Internet : <a href="http://www.youth-hostel.si/" class="spip_out" rel="external">www.youth-hostel.si</a> <br class="manualbr">Courriel : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/vie-pratique/#info#mc#youth-hostel.si#" title="info..åt..youth-hostel.si" onclick="location.href=mc_lancerlien('info','youth-hostel.si'); return false;" class="spip_mail">info<span class="spancrypt"> [at] </span>youth-hostel.si</a></p>
<p>De nombreuses chambres chez l’habitant sont également disponibles, tant dans les villes que dans les zones rurales ou sur la côte adriatique.</p>
<h3 class="spip"><a id="sommaire_4"></a>Electricité</h3>
<p>Le courant est alternatif, 220 volts - 50 hertz. Les prises de courant sont de type triphasé. Les installations électriques sont de bon niveau.</p>
<h3 class="spip"><a id="sommaire_5"></a>Electroménager</h3>
<p>Les cuisines sont munies de la plupart des équipements courants : plaques de cuisson, four, lave-linge, lave-vaisselle, réfrigérateur et congélateur. Il est possible de trouver de l’électroménager de marque locale (Gorenje) ou étrangère.</p>
<p>La climatisation est utile en été mais elle n’est pas indispensable. De plus en plus de logements en sont équipés.</p>
<p><i>Mise à jour : septembre 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-slovenie-vie-pratique-article-loisirs-et-culture.md" title="Loisirs et culture">Loisirs et culture</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-slovenie-vie-pratique-article-communications.md" title="Communications">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-slovenie-vie-pratique-article-transports-114782.md" title="Transports">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-slovenie-vie-pratique-article-cout-de-la-vie-114781.md" title="Coût de la vie">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-slovenie-vie-pratique-article-scolarisation.md" title="Scolarisation">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-slovenie-vie-pratique-article-sante.md" title="Santé">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-slovenie-vie-pratique-article-logement.md" title="Logement">Logement</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/vie-pratique/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
