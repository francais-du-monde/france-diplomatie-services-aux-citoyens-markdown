# Protection sociale

<h2 class="rub23156">Régime local de sécurité sociale</h2>
<p>Le régime de sécurité sociale local s’adresse uniquement aux nationaux égyptiens. En dépit de cotisations particulièrement élevées, les remboursements des prestations sont insignifiants.</p>
<p>De ce fait, les nationaux qui en possèdent les moyens, ont recours à des assureurs privés pour l’hospitalisation, les accidents de travail, les admissions en maternité, les fonds sociaux, etc. Il en est de même pour les sociétés étrangères implantées en Egypte ainsi que pour les employés étrangers de sociétés égyptiennes.</p>
<p>Le régime local ne prend en charge en aucune circonstance les étrangers résidents en Egypte.</p>
<p><i>Mise à jour : avril 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-egypte-protection-sociale-23156-article-convention-de-securite-sociale-112195.md" title="Convention de sécurité sociale">Convention de sécurité sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-egypte-protection-sociale-23156-article-regime-local-de-securite-sociale-112194.md" title="Régime local de sécurité sociale">Régime local de sécurité sociale</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/protection-sociale-23156/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
