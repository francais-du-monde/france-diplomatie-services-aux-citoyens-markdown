# Venezuela

<p><strong>Au 31 décembre 2014, environ 4538 Français étaient inscrits à l’ambassade de France au Venezuela</strong> ; il s’agit donc de la plus importante communauté française de l’Amérique andine, même si le nombre de Français de passage est moins important qu’en Equateur ou au Pérou.</p>
<p>La communauté française se concentre principalement sur Caracas (80%) ou dans les autres grandes agglomérations du pays comme Maracaibo, Maracay, Barcelona ou l’île de Margarita.</p>
<p>Les ressortissants français ont en moyenne 55 ans et ont, pour la grande majorité, la double nationalité franco-vénézuélienne. <strong>45 % d’entre eux travaillent ou recherchent un emploi</strong>. Les catégories socioprofessionnelles les plus représentées sont les professeurs, les ingénieurs, les commerçants, les administrateurs de sociétés et les professions libérales.</p>
<p>La présence française au Venezuela est assez diversifiée puisqu’elle couvre des secteurs aussi variés que l’industrie (Saint Gobain), le BTP (Ciments Lafarge), les transports (Alstom), la santé (Sanofi-Aventis), les cosmétiques (L’Oréal), l’agroalimentaire (Pernod-Ricard), ou encore les services financiers (BNP Paribas). Elle représente aujourd’hui près de 90 filiales françaises implantées dans le pays, positionnant la France parmi les trois premiers investisseurs étrangers avec 5 milliards de dollars de stock.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/dossiers-pays/venezuela/" class="spip_in">Une description du Venezuela, de sa situation politique et économique</a> ;</li>
<li><a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/venezuela/" class="spip_in">Des informations actualisées sur les conditions locales de sécurité au Venezuela</a> (qui fait partie des dix pays au monde ayant le plus fort taux de criminalité) ;</li>
<li><a href="http://www.ambafrance-ve.org/" class="spip_out" rel="external">Ambassade de France au Venezuela</a>.</li></ul>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-venezuela-entree-et-sejour.md">Entrée et séjour</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-venezuela-entree-et-sejour-article-passeport-visa-permis-de-travail.md">Passeport, visa, permis de travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-venezuela-entree-et-sejour-article-demenagement-103725.md">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-venezuela-entree-et-sejour-article-vaccination.md">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-venezuela-entree-et-sejour-article-animaux-domestiques-103727.md">Animaux domestiques</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-venezuela-emploi-stage.md">Emploi, stage</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-venezuela-emploi-stage-article-marche-du-travail.md">Marché du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-venezuela-emploi-stage-article-reglementation-du-travail-103729.md">Règlementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-venezuela-emploi-stage-article-recherche-d-emploi.md">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-venezuela-emploi-stage-article-curriculum-vitae.md">Curriculum vitae</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-venezuela-emploi-stage-article-lettre-de-motivation.md">Lettre de motivation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-venezuela-emploi-stage-article-stage.md">Stage</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-venezuela-protection-sociale-22138.md">Protection sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-venezuela-protection-sociale-22138-article-regime-local-de-securite-sociale-103734.md">Régime local de sécurité sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-venezuela-protection-sociale-22138-article-convention-de-securite-sociale-103735.md">Convention de sécurité sociale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-venezuela-fiscalite.md">Fiscalité</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-venezuela-fiscalite-article-fiscalite-du-pays.md">Fiscalité du pays</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-venezuela-fiscalite-article-convention-fiscale-103737.md">Convention fiscale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-venezuela-vie-pratique.md">Vie pratique</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-venezuela-vie-pratique-article-logement-103738.md">Logement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-venezuela-vie-pratique-article-sante.md">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-venezuela-vie-pratique-article-scolarisation.md">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-venezuela-vie-pratique-article-cout-de-la-vie.md">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-venezuela-vie-pratique-article-transports.md">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-venezuela-vie-pratique-article-communications.md">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-venezuela-vie-pratique-article-loisirs-et-culture-103744.md">Loisirs et culture</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-venezuela-presence-francaise.md">Présence française</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-venezuela-presence-francaise-article-presence-francaise.md">Présence française</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
