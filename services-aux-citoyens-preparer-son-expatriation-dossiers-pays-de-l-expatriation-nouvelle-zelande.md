# Nouvelle-Zélande

<p>Au 31 décembre 2014, environ 4035 Français étaient inscrits au registre des Français établis hors de France. Ce nombre ne représente, malheureusement, qu’un tiers sans doute de la réelle présence française.</p>
<p>La majeure partie de la communauté française (50%) s’est établie dans l’agglomération d’Auckland, capitale économique du pays suivie de Wellington et sa région, capitale administrative, autre grand lieu d’implantation française. Il s’agit d’une population jeune. La part des binationaux représente presque la moitié de la communauté française en Nouvelle-Zélande. Les catégories socioprofessionnelles les plus représentatives sont celles des cadres et professions intellectuelles, des professions intermédiaires et des employés.</p>
<p>La présence française en Nouvelle-Zélande est assurée, au sein du tissu économique, par près de 70 implantations d’entreprises françaises et ce, dans la quasi totalité des secteurs d’activité : tourisme, environnement, assurance, énergie, agro-alimentaire, électronique.</p>
<p>Certaines de ces entreprises dépendent d’un siège social régional établi le plus souvent en Australie et n’emploient que très peu de Français.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/dossiers-pays/nouvelle-zelande/" class="spip_in">Une description de la Nouvelle-Zélande, de sa situation politique et économique</a> ;</li>
<li><a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/nouvelle-zelande/" class="spip_in">Des informations actualisées sur les conditions locales de sécurité en Nouvelle-Zélande</a> ;</li>
<li><a href="http://www.ambafrance-nz.org/" class="spip_out" rel="external">Ambassade de France en Nouvelle-Zélande</a>.</li></ul>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-nouvelle-zelande-entree-et-sejour.md">Entrée et séjour</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-nouvelle-zelande-entree-et-sejour-article-passeport-visa-permis-de-travail.md">Passeport, visa, permis de travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-nouvelle-zelande-entree-et-sejour-article-demenagement.md">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-nouvelle-zelande-entree-et-sejour-article-vaccination-111227.md">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-nouvelle-zelande-entree-et-sejour-article-animaux-domestiques-111228.md">Animaux domestiques</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-nouvelle-zelande-emploi-stage.md">Emploi, stage</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-nouvelle-zelande-emploi-stage-article-marche-du-travail-111229.md">Marché du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-nouvelle-zelande-emploi-stage-article-reglementation-du-travail-111230.md">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-nouvelle-zelande-emploi-stage-article-recherche-d-emploi.md">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-nouvelle-zelande-emploi-stage-article-curriculum-vitae.md">Curriculum vitae</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-nouvelle-zelande-emploi-stage-article-lettre-de-motivation.md">Lettre de motivation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-nouvelle-zelande-emploi-stage-article-entretien-d-embauche.md">Entretien d’embauche</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-nouvelle-zelande-emploi-stage-article-stages.md">Stages</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-nouvelle-zelande-protection-sociale-23018.md">Protection sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-nouvelle-zelande-protection-sociale-23018-article-regime-local-de-securite-sociale-111236.md">Régime local de sécurité sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-nouvelle-zelande-protection-sociale-23018-article-convention-de-securite-sociale-111237.md">Convention de sécurité sociale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-nouvelle-zelande-fiscalite.md">Fiscalité</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-nouvelle-zelande-fiscalite-article-fiscalite-du-pays.md">Fiscalité du pays</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-nouvelle-zelande-fiscalite-article-convention-fiscale-111239.md">Convention fiscale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-nouvelle-zelande-vie-pratique.md">Vie pratique</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-nouvelle-zelande-vie-pratique-article-logement-111240.md">Logement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-nouvelle-zelande-vie-pratique-article-sante.md">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-nouvelle-zelande-vie-pratique-article-scolarisation-111242.md">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-nouvelle-zelande-vie-pratique-article-cout-de-la-vie-111243.md">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-nouvelle-zelande-vie-pratique-article-transports-111244.md">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-nouvelle-zelande-vie-pratique-article-communications.md">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-nouvelle-zelande-vie-pratique-article-loisirs-et-culture.md">Loisirs et culture</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-nouvelle-zelande-presence-francaise.md">Présence française</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-nouvelle-zelande-presence-francaise-article-presence-francaise.md">Présence française</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
