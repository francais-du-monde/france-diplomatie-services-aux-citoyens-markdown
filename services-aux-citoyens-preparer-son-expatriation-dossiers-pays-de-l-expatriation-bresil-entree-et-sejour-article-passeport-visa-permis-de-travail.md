# Passeport, visa, permis de travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/entree-et-sejour/article/passeport-visa-permis-de-travail#sommaire_1">Les différents types de visa</a></li></ul>
<p>Quel que soit le motif de votre séjour, renseignez-vous auprès de la <a href="http://cgparis.itamaraty.gov.br/fr/visas.xml" class="spip_out" rel="external">section consulaire de l’ambassade du Brésil à Paris</a> qui vous informera sur la règlementation en matière d’entrée et de séjour au Brésil, règlementation que vous devrez impérativement respecter.</p>
<p>Une fois sur place <strong>le citoyen étranger qui est au Brésil</strong> et souhaite se renseigner sur les visas et le séjour en territoire brésilien doit <strong>s’adresser au bureau de la Police fédérale</strong> de la ville où il se trouve. Le consulat de France au Brésil n’est pas l’organisme compétent pour répondre à vos demandes d’information concernant votre séjour au Brésil.</p>
<p>Les ressortissants français sont <strong>exemptés de visa pour un séjour touristique ou un voyage d’affaires (non rémunéré) n’excédant pas 90 jours tous les 180 jours</strong> (le délai de séjour au Brésil se compte à partir du débarquement et ne peut pas être prolongé).</p>
<p><strong>Pièces exigées à l’arrivée au Brésil :</strong></p>
<ul class="spip">
<li>passeport ayant obligatoirement deux pages vierges au minimum et une validité d’au moins six mois à compter de l’entrée au Brésil. Le passeport biométrique ou à lecture optique n’est pas exigé ;</li>
<li>billet aller-retour avec la durée du séjour légal ;</li>
<li>justificatif de moyens de subsistance ;</li>
<li>les ressortissants français <a href="http://cgparis.itamaraty.gov.br/fr/vaccination.xml" class="spip_out" rel="external">peuvent être amenés à présenter le certificat international d’immunisation contre la fièvre jaune</a>.</li></ul>
<p>Pour un séjour <strong>supérieur à 90 jours, il convient de demander un visa de séjour, temporaire ou permanent </strong>selon le cas en s’adressant au consulat général du Brésil en France.</p>
<p><strong>Toutefois, un visa est nécessaire dans le cas d’un travail rémunéré. En aucun cas, vous n’êtes autorisé à travailler au Brésil sans permis adéquat.</strong> Il faut disposer d’un visa de travail, temporaire ou permanent.</p>
<p>Ce visa s’obtient auprès des services consulaires brésiliens sur la base d’un contrat en bonne et due forme. La demande est transmise au <a href="http://www.mte.gov.br/" class="spip_out" rel="external">ministère brésilien du Travail et de l’Emploi (MTE)</a>.</p>
<p>Depuis le 1er janvier 1999, les professionnels étrangers arrivés au Brésil avec un visa temporaire et un contrat de travail sont imposés dès leur arrivée comme s’ils étaient résidents au Brésil (il en est de même pour les personnes ayant un visa permanent), soit, selon le tableau du « barème », sur tous les revenus perçus en contrepartie de leur travail effectué au Brésil. Quant à ceux qui arrivent avec un visa temporaire mais sans contrat de travail, ils sont considérés comme non résidents pendant les six premiers mois de leur séjour (183 jours). A partir du 184ème jour, ils basculent dans le régime de droit commun.</p>
<p>L’entreprise contractante peut également s’adresser directement à ce ministère.</p>
<p>Il faut savoir que l’obtention d’un visa de travail au Brésil est difficile. Les délais varient de trois à six mois. Il convient de justifier pour l’entreprise que le poste ne peut être occupé par un Brésilien. Les procédures administratives étant complexes, les entreprises sont obligées de recourir à un cabinet d’avocats travaillant le plus souvent avec un "despachante" (intermédiaire accomplissant diverses procédures administratives en lieu et place des entreprises). Le salarié doit quant à lui démontrer qu’il occupait des fonctions équivalentes dans son pays d’origine depuis au moins deux ans.</p>
<p>Toute régularisation a posteriori est quasi impossible.</p>
<h3 class="spip"><a id="sommaire_1"></a>Les différents types de visa</h3>
<h4 class="spip">Le visa temporaire (VITEM)</h4>
<ul class="spip">
<li>VITEM I - <a href="http://cgparis.itamaraty.gov.br/fr/vitem1echanges.xml" class="spip_out" rel="external">échanges éducationnels</a> , <a href="http://cgparis.itamaraty.gov.br/fr/vitem1recherche.xml" class="spip_out" rel="external">recherche scientifique</a>, <a href="http://cgparis.itamaraty.gov.br/fr/vitem1benevolat.xml" class="spip_out" rel="external">bénévolat religieux ou social</a> , <a href="http://cgparis.itamaraty.gov.br/fr/vitem1sportif.xml" class="spip_out" rel="external">entraînement sportif-mineurs</a></li>
<li>VITEM II - <a href="http://cgparis.itamaraty.gov.br/fr/vitem2.xml" class="spip_out" rel="external">affaires</a></li>
<li>VITEM III - <a href="http://cgparis.itamaraty.gov.br/fr/vitem3.xml" class="spip_out" rel="external">séjour artistique et/ou sportif</a></li>
<li>VITEM IV - <a href="http://cgparis.itamaraty.gov.br/fr/vitem4.xml" class="spip_out" rel="external">étudiants</a></li>
<li>VITEM V - <a href="http://cgparis.itamaraty.gov.br/fr/vitem5.xml" class="spip_out" rel="external">assistance technique (avec ou sans contrat de travail), émigré avec contrat de travail brésilien, stagiaire auprès d’entreprises*, membre de l’équipage d’un navire</a><br class="manualbr"><strong>* </strong>D’après la RN ("<b>R</b>esolução <b>N</b>ormativa" du Conseil national d’immigration - CNI) 42/99, Le VITEM V ne s’applique qu’au stagiaire sous contrat avec une entreprise en France, pour un stage auprès d’une subsidiaire ou filiale brésilienne, payé par une source de l’étranger.</li>
<li>VITEM VI - <a href="http://cgparis.itamaraty.gov.br/fr/vitem6.xml" class="spip_out" rel="external">correspondant de presse étrangère</a></li>
<li>VITEM VII - <a href="http://cgparis.itamaraty.gov.br/fr/vitem7.xml" class="spip_out" rel="external">mission religieuse</a></li>
<li>Les personnes dépendantes (conjoints, enfants mineurs) d’un titulaire de VITEM I, IV, V, VI et VII doivent demander le visa VITEM - <a href="http://cgparis.itamaraty.gov.br/fr/vitemfamilial.xml" class="spip_out" rel="external">Regroupement familial</a>.</li></ul>
<h4 class="spip">Le visa permanent</h4>
<p>Le <a href="http://cgparis.itamaraty.gov.br/fr/visa_permanent.xml" class="spip_out" rel="external">visa permanent</a> s’accorde au citoyen étranger ayant l’intention de s’établir définitivement au Brésil, dans certains cas.</p>
<h5 class="spip">Cas des conjoints</h5>
<p>Si l’expatrié a un visa temporaire avec contrat de travail, son conjoint a alors droit à un visa temporaire sans contrat de travail, ce dernier étant subordonné au premier. Si l’expatrié a un visa permanent, son conjoint a droit au même visa.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li>Le site du <a href="http://cgparis.itamaraty.gov.br/fr/" class="spip_out" rel="external">consulat du Brésil en France</a>.</li>
<li>Le site du <a href="http://www.mte.gov.br/" class="spip_out" rel="external">ministère brésilien du Travail et de l’emploi (MTE)</a>.</li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/entree-et-sejour/article/passeport-visa-permis-de-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
