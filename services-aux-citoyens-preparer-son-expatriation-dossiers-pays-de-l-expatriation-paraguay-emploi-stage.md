# Emploi, stage

<h2 class="rub23466">Marché du travail</h2>
<p>L’emploi de personnel étranger est réservé aux postes pour lesquels une qualification particulière est nécessaire, non disponible auprès des travailleurs locaux.</p>
<p><strong>Rémunération</strong></p>
<p>S’agissant des contrats locaux, la fourchette de rémunération moyenne est peu élevée et se situe globalement entre 300 et 1000 €. Le salaire minimum  au Paraguay en 2014 est de 300 euros environ.</p>
<p><i>Mise à jour : juin 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-paraguay-emploi-stage-article-stages.md" title="Stages">Stages</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-paraguay-emploi-stage-article-recherche-d-emploi.md" title="Recherche d’emploi">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-paraguay-emploi-stage-article-reglementation-du-travail-113494.md" title="Réglementation du travail">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-paraguay-emploi-stage-article-marche-du-travail-113493.md" title="Marché du travail">Marché du travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/paraguay/emploi-stage/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
