# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/vie-pratique/article/transports-110532#sommaire_1">Permis de conduire</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/vie-pratique/article/transports-110532#sommaire_2">Code de la route</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/vie-pratique/article/transports-110532#sommaire_3">Assurances et taxes</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/vie-pratique/article/transports-110532#sommaire_4">Achat et location</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/vie-pratique/article/transports-110532#sommaire_5">Immatriculation</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/vie-pratique/article/transports-110532#sommaire_6">Entretien</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/vie-pratique/article/transports-110532#sommaire_7">Réseau routier</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/vie-pratique/article/transports-110532#sommaire_8">Transports en commun</a></li></ul>
<p>La liberté de circulation est totale excepté dans le Sud tunisien pour lequel une autorisation est nécessaire.</p>
<p>En cas d’accident, le délit de fuite est réprimé, il faut établir un constat d’accident à l’amiable. En cas d’accident mortel, le conducteur est placé en garde à vue. S’il est inculpé d’homicide involontaire, il est incarcéré dans l’attente du jugement. Dans certaines régions de Tunisie, notamment en milieu rural, il est conseillé de se placer dès que possible sous la protection de la police en cas d’accident grave de la circulation avec des tiers.</p>
<h3 class="spip"><a id="sommaire_1"></a>Permis de conduire</h3>
<p>Le permis de conduire français est reconnu en Tunisie pour une durée d’un an. Au-delà il doit être échangé contre un permis de conduire tunisien. Il convient de s’adresser à l’Agence technique pour le transport terrestre :</p>
<p><a href="http://www.attt.com.tn/" class="spip_out" rel="external">Agence technique pour le transport terrestre (ATTT)</a><br class="manualbr">Avenue du Japon - 6 impasse n°1<br class="manualbr">Montplaisir El Borgel<br class="manualbr">1073 TUNIS</p>
<p>Cette formalité administrative étant assez longue, il est conseillé d’effectuer votre demande dès vos six mois de résidence accomplis dans le pays. Les frais de dossier s’élèvent à 18 DT.</p>
<p>Pour passer le permis de conduire en Tunisie, l’intéressé doit avoir six mois de résidence dans le pays. Un permis obtenu lors d’un séjour touristique n’est pas reconnu valable à l’étranger, et ne peut donc être échangé.</p>
<p>Les personnes souhaitant s’installer en Tunisie devront obligatoirement contracter une assurance locale pour leur véhicule au delà de trois mois de séjour.</p>
<h3 class="spip"><a id="sommaire_2"></a>Code de la route</h3>
<p>La liberté de circulation est totale excepté dans le Sud tunisien pour lequel une autorisation est nécessaire. La conduite s’effectue à droite, la priorité est à droite ; la vitesse est limitée à 50 km/h en ville, à 90 km/h sur les routes et à 110 km/h sur les autoroutes. La ceinture de sécurité est obligatoire à l’avant des véhicules.</p>
<h3 class="spip"><a id="sommaire_3"></a>Assurances et taxes</h3>
<p>Le montant des taxes s’échelonne de 86 % à 471 % du prix du véhicule neuf en fonction de sa puissance, de sa cylindrée et du type de carburant utilisé, les moteurs diesels étant les plus taxés.</p>
<p>L’assurance au tiers est obligatoire. Les résidents doivent s’affilier à une compagnie d’assurances locale pour être autorisés à conduire leur véhicule.</p>
<p><i>Exemple de prime annuelle d’assurances « tous risques » </i> : Xantia : environ 800 € ; Renault Grand Scénic : entre 900 et 1000€.</p>
<h3 class="spip"><a id="sommaire_4"></a>Achat et location</h3>
<h4 class="spip">Location</h4>
<p>Il existe de nombreuses possibilités de location de véhicule sur place auprès de sociétés spécialisées (pour une petite cylindrée, il faut compter 30€ par jour sans chauffeur).</p>
<p>Pour louer un véhicule, il faut être âgé de 21 ans ou plus et posséder un permis daté d’au moins un an.</p>
<h3 class="spip"><a id="sommaire_5"></a>Immatriculation</h3>
<p>Une visite technique du véhicule est effectuée par les autorités locales de police, avant l’immatriculation. Un contrôle technique annuel est effectué sur les véhicules immatriculés localement.</p>
<p>Le montant de la vignette automobile est de 60 à 1000 dinars en fonction de la cylindrée de la voiture. La carte grise s’obtient auprès de l’ATTT.</p>
<p>Pour en savoir plus : <a href="http://www.attt.com.tn/" class="spip_out" rel="external">Agence technique des transports terrestres</a></p>
<h3 class="spip"><a id="sommaire_6"></a>Entretien</h3>
<p>La quasi-totalité des voitures de marques européennes et de nombreuses marques asiatiques sont représentées et peuvent être réparées sans difficulté. La main-d’œuvre est moins chère qu’en France (environ 10 dinars l’heure) mais la qualité des services rendus est variable.</p>
<h3 class="spip"><a id="sommaire_7"></a>Réseau routier</h3>
<p>La Tunisie compte 12 000 km de routes et 5000 km de pistes. Le réseau routier est en voie d’amélioration, mais les chaussées sont encore étroites sur de nombreux parcours. En général, les grands axes sont en bon état mais le réseau secondaire est de qualité variable.</p>
<h3 class="spip"><a id="sommaire_8"></a>Transports en commun</h3>
<p>Les compagnies TUNISAIR et SYPHAX assurent les liaisons aériennes fréquentes entre les principales villes de Tunisie (Tunis, Djerba, Sfax, Tozeur, Tabarka, etc.). La voie aérienne est d’un bon rapport qualité-prix.</p>
<p>Le réseau ferroviaire couvre la quasi-totalité du territoire et relie la plupart des grandes villes du nord (Tunis, Bizerte, Beja) et de l’est (Sousse, Sfax, Gabès). Le trajet Tunis-Bizerte s’effectue en 2h50 ; le trajet Tunis-Gabès dure 7 heures et couvre une distance de 406 km. Les trains sont assez lents, mais économiques et ponctuels.</p>
<p>Gare SNCFT, Tunis, tél : (216) 71.34.444 ou.345.511</p>
<p>Par autocar, les liaisons sont nombreuses mais le confort très aléatoire. Celles-ci sont relayées par le système de "louage". Il s’agit de taxis collectifs d’une capacité de cinq ou six personnes avec bagages. Le louage ne part que lorsqu’il est plein. Dans chaque ville, il existe une station de louage : les voitures à bandes bleues desservent les localités proches, celles à bandes rouges les longues distances.</p>
<p>Les horaires des bus reliant Tunis aux grandes villes figurent dans le Temps et la Presse, deux quotidiens de langue française.</p>
<p>Société nationale de transports SNTRI, Gare Routière Tunis-Sud / Bab El Falla : Tél : 71905.433 - Gare Routière Tunis-Nord / Bab Saâdoun : Tél : 71.8.577 ou 71.898.358</p>
<p>En ville, les transports en commun sont variés : métro (à Tunis), autobus (à éviter), taxis individuels (très nombreux et d’un coût abordable).</p>
<p><i>Mise à jour : novembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/vie-pratique/article/transports-110532). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
