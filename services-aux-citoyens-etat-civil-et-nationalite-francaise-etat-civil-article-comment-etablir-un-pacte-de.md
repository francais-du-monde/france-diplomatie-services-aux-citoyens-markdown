# Enregistrer un PACS à l’étranger

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/etat-civil/article/comment-etablir-un-pacte-de#sommaire_1">Qu’est-ce que le PACS ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/etat-civil/article/comment-etablir-un-pacte-de#sommaire_2">Les démarches à accomplir</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/etat-civil/article/comment-etablir-un-pacte-de#sommaire_3">Les pièces à fournir</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/etat-civil/article/comment-etablir-un-pacte-de#sommaire_4">L’enregistrement du PACS</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/etat-civil/article/comment-etablir-un-pacte-de#sommaire_5">La publicité du PACS</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/etat-civil/article/comment-etablir-un-pacte-de#sommaire_6">La dissolution du PACS</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Qu’est-ce que le PACS ?</h3>
<p>Instauré depuis le 15 novembre 1999, le Pacte civil de solidarité (PACS) est un contrat passé entre deux personnes majeures, de sexe différent ou de même sexe, pour organiser leur vie commune. Il crée des droits et obligations pour les partenaires, notamment « une aide mutuelle et matérielle ».</p>
<p>Vous ne pouvez pas conclure un PACS si :</p>
<ul class="spip">
<li>Vous êtes mineur, même émancipé.</li>
<li>Vous êtes marié.</li>
<li>Vous êtes déjà lié par un PACS.</li>
<li>Vous et votre partenaire êtes des parents proches.</li></ul>
<p>Depuis le 1er janvier 2009, les majeurs sous tutelle ou curatelle peuvent, sous certaines conditions, souscrire un PACS :</p>
<ul class="spip">
<li>Le majeur sous curatelle doit obtenir l’autorisation du curateur ou, à défaut, celle du juge des tutelles,</li>
<li>Le majeur sous tutelle doit obtenir l’autorisation du juge des tutelles ou, le cas échéant, du conseil de famille.</li></ul>
<p>Lorsque le PACS est conclu à l’étranger, l’un au moins des partenaires doit être de nationalité française.</p>
<h3 class="spip"><a id="sommaire_2"></a>Les démarches à accomplir</h3>
<p>Si vous êtes français(e), résidant à l’étranger, et souhaitez conclure un PACS avec un(e) autre Français(e) ou un(e) étranger(e), la déclaration conjointe doit être effectuée auprès de l’ambassade ou du consulat du lieu de la résidence commune.</p>
<h3 class="spip"><a id="sommaire_3"></a>Les pièces à fournir</h3>
<p>Le PACS suppose qu’une convention sur l’organisation de votre vie commune (patrimoine, gestion de vos biens, …) soit rédigée et signée par les deux partenaires. Vous pouvez l’établir vous-même (acte sous seing privé) ou solliciter les conseils d’un professionnel du droit (notaire, avocat, …) (acte authentique).</p>
<p>Outre un exemplaire de la convention, les documents suivants doivent être fournis par les candidats au PACS :</p>
<ul class="spip">
<li>une pièce d’identité pour chacun ;</li>
<li>la copie intégrale ou un extrait avec filiation de leur acte de naissance : ces pièces doivent être datées de moins de trois mois (six mois si la personne est de nationalité étrangère) ;</li>
<li>une attestation sur l’honneur - par personne - qu’il n’existe entre eux aucun lien de parenté ou d’alliance qui constituerait un empêchement pour conclure un PACS ;</li>
<li>une attestation sur l’honneur indiquant que le couple fixe sa résidence commune dans le ressort de l’ambassade ou du consulat où il fait sa demande ;</li>
<li>si l’un des partenaires est divorcé ou veuf : le livret de famille ou la copie intégrale de l’acte de mariage, portant la mention du divorce ou du décès ; Pour le candidat de nationalité étrangère, la copie intégrale de son acte de naissance doit être traduite et légalisée. Il doit également fournir :</li></ul>
<ul class="spip">
<li>un certificat de coutume délivré par les autorités compétentes de l’Etat dont il est ressortissant indiquant qu’il est majeur, célibataire et qu’il n’est pas placé sous tutelle.</li>
<li>un certificat de non PACS daté de moins de trois mois (délivré par le TGI de Paris 4, Boulevard du Palais, 75055 Paris Cedex 01 (France). Téléphone : +33 (0)1 44 32 51 51 ; Fax : +33 (0)1 44 32 78 56)</li></ul>
<h3 class="spip"><a id="sommaire_4"></a>L’enregistrement du PACS</h3>
<p>Après vérification des pièces présentées, l’agent diplomatique ou consulaire enregistre la déclaration si les conditions légales sont remplies.</p>
<p>Les partenaires sont informés, le cas échéant, des risques pouvant exister dans l’Etat de résidence du fait des lois, règlements ou usages sociaux concernant notamment la vie commune.</p>
<p>Le PACS produit ses effets entre les partenaires à la date de son enregistrement.</p>
<h3 class="spip"><a id="sommaire_5"></a>La publicité du PACS</h3>
<p>Après l’enregistrement de la déclaration, l’agent diplomatique ou consulaire fait procéder aux formalités de publicité. Il est fait mention en marge de l’acte de naissance de chaque partenaire de l’existence du PACS avec indication de l’identité de l’autre partenaire.</p>
<p>Cette mention est portée par l’officier de l’état civil du lieu de naissance de chaque partenaire, ou, pour les Français nés à l’étranger, par l’officier de l’état civil du service central de l’état civil de Nantes.</p>
<p>Pour les personnes de nationalité étrangère nées à l’étranger, cette information est portée sur un registre tenu au greffe du tribunal de grande instance de Paris.</p>
<p>Le PACS n’est opposable aux tiers qu’à compter du jour où les formalités de publicité sont accomplies.</p>
<p>La conclusion d’un PACS ne donne pas lieu à délivrance d’un livret de famille.</p>
<h3 class="spip"><a id="sommaire_6"></a>La dissolution du PACS</h3>
<p>Les cas de dissolution sont prévus par <a href="https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070721&amp;idArticle=LEGIARTI000023780843" class="spip_out" rel="external">l’article 515-7 du code civil</a> (décès ou mariage d’un ou des partenaires, déclaration conjointe des partenaires ou unilatérale de l’un d’eux).<br class="manualbr">Par ailleurs, l’article 506-1 alinéa 2 du code civil prévoit toujours qu’il peut être mis fin au pacte civil de solidarité en cas de placement d’un partenaire sous tutelle, cette faculté étant exercée par le tuteur autorisé par le conseil de famille ou, à défaut, par le juge des tutelles.</p>
<p>Le greffe du tribunal d’instance ou le poste consulaire ou diplomatique ayant reçu la déclaration de pacte civil de solidarité est seul compétent pour enregistrer la dissolution, quelle qu’en soit la cause et quel que soit le lieu de résidence des partenaires.</p>
<p>En outre, comme la déclaration et la modification, la dissolution du pacte civil de solidarité fait l’objet d’une publicité à l’état civil ou, lorsque l’un des partenaires est de nationalité étrangère et né à l’étranger, d’un enregistrement par le greffe du tribunal de grande instance de Paris.</p>
<p>La date à laquelle la dissolution du pacte civil de solidarité produit ses effets, entre les partenaires et à l’égard des tiers, diffère selon qu’elle intervient consécutivement au mariage ou au décès d’un ou des partenaires, ou bien qu’elle résulte d’une décision conjointe ou unilatérale de ces derniers.</p>
<p><i>Mise à jour : mars 2016</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/etat-civil/article/comment-etablir-un-pacte-de). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
