# Lettre de motivation

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/emploi-stage/article/lettre-de-motivation-111289#sommaire_1">Rédaction</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/emploi-stage/article/lettre-de-motivation-111289#sommaire_2">Modèles de lettre de motivation</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Rédaction</h3>
<p>La lettre de motivation (<i>carta de apresentação</i>) est le premier contact avec l’employeur potentiel. Son objectif est de susciter l’intérêt de la personne qui va recevoir votre candidature et de mettre en valeur un certain nombre d’éléments constitutifs de votre CV.</p>
<h4 class="spip">Quelques conseils</h4>
<p>La lettre de motivation doit être courte et ne doit pas dépasser une page.</p>
<p>Il faut éviter les longues phrases, difficilement compréhensibles pour le recruteur, d’autant plus que celui-ci est susceptible de recevoir de nombreuses candidatures chaque jour. La lettre peut être manuscrite ou dactylographiée.</p>
<p>Elle doit être adressée au responsable des ressources humaines. Si vous ne connaissez pas son nom, renseignez vous auprès de l’entreprise. S’il s’agit d’une petite entreprise, vous pouvez adresser votre candidature directement au directeur.</p>
<p>Si vous répondez à une annonce, reprenez en les termes dans votre lettre.</p>
<p>Attention aux fautes d’orthographe, rédhibitoires pour de nombreux employeurs. Relisez-vous et faites-vous relire par une autre personne.</p>
<h4 class="spip">La forme</h4>
<ul class="spip">
<li><strong>en haut à gauche </strong> : indiquez vos coordonnées (prénom, nom, adresse, code postal, ville, téléphone, adresse électronique).</li>
<li><strong>en haut à droite </strong> : indiquez les coordonnées de l’entreprise ou de votre interlocuteur privilégié au sein de celle-ci (civilité, prénom, nom, nom de la société, adresse, code postal, ville).</li>
<li><strong>1er paragraphe : </strong>mettez en avant l’intérêt que vous portez au poste et à l’entreprise, ainsi qu’à son secteur d’activité.</li>
<li><strong>2ème paragraphe </strong> : montrez que votre expérience correspond aux besoins de l’entreprise. Faites référence à votre expérience professionnelle (si vous avez déjà travaillé) ou à vos diplômes (si vous n’avez jamais travaillé).</li>
<li><strong>3ème paragraphe </strong> : demandez un entretien et concluez par une formule de politesse.</li></ul>
<p><i>Pour en savoir plus sur les différentes formules utilisées dans les lettres de motivation au Portugal, consultez la page web <a href="http://www.babla.fr/phrases/candidature/lettre-de-motivation/francais-portugais/" class="spip_out" rel="external">Babla.fr</a> (équivalents français/portugais).</i></p>
<h3 class="spip"><a id="sommaire_2"></a>Modèles de lettre de motivation</h3>
<p><strong>Exemple de lettre de motivation : candidature spontanée</strong></p>
<p>Exmo. SenhorDr. João Castro e Lima<br class="manualbr">Departamento de Recursos Humanos Jalconstroi, S.A.<br class="manualbr">Edifício Leste<br class="manualbr">1065-000 Lisboa<br class="manualbr">Lisboa, 25 de Julho de 2006</p>
<p>Exmo. Senhor</p>
<p>Tenho conhecimento de que a vossa empresa lidera o mercado no ramo das novas tecnologias, o que me dá garantias de ser o melhor local para poder desenvolver as minhas competências.</p>
<p>Licenciado em Engenharia Informática na Universidade Técnica de Lisboa com 5 anos de experiência, gostaria de, numa entrevista pessoal, poder prestar outras informações que penso serem de mútuo interesse. Subscrevo-me, com a mais elevada consideração</p>
<p>Assinatura</p>
<p>Anexo : Curriculum Vitae</p>
<p><strong>Exemple de lettre de motivation : réponse à une annonce</strong></p>
<p>Anabela Bela<br class="manualbr">Av. 1° de Maio, n.° 14<br class="manualbr">1250-007 Lisboa</p>
<p>Exmo. Senhor<br class="manualbr">Director de Recursos Humanos<br class="manualbr">Rua da Alegria, n.° 7<br class="manualbr">1250-006 LISBOA Exmo(a).</p>
<p>Senhor(a),</p>
<p>Gostaria de apresentar a minha candidatura para o cargo de Assessora de Produção que a vossa empresa anunciou na edição de 15 de Maio último no “Diário de Notícias”.</p>
<p>Junto envio em anexo o meu currículo que apresenta, em pormenor, a minha formação escolar, bem como a experiência prática adquirida durante os estágios que efectuei. Contudo, gostaria de aprofundar melhor as razões da minha candidatura numa possível entrevista.</p>
<p>Na expectativa de um próximo contacto, subscrevo-me com os melhores cumprimentos.</p>
<p>Anabela Bela</p>
<p><i>Source : <a href="http://emprego.sapo.pt/" class="spip_out" rel="external">Emprego.sapo.pt</a></i></p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/emploi-stage/article/lettre-de-motivation-111289). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
