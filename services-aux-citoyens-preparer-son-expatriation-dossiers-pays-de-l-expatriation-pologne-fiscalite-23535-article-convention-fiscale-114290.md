# Convention fiscale

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/fiscalite-23535/article/convention-fiscale-114290#sommaire_1">Champ d’application de la convention</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/fiscalite-23535/article/convention-fiscale-114290#sommaire_2">Dispositions conventionnelles sur certaines catégories de revenus</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/fiscalite-23535/article/convention-fiscale-114290#sommaire_3">Elimination de la double imposition</a></li></ul>
<p><strong>La France et la République populaire de Pologne ont signé, le 20 juin 1975, une convention en matière de fiscalité</strong> publiée au Journal Officiel du 1er décembre 1976.</p>
<p>Cette convention tend à éviter les doubles impositions en matière d’impôts sur les revenus et sur la fortune qui pourraient résulter de l’application des législations de ces deux Etats.</p>
<p>Le texte de la convention peut être obtenu auprès de la Direction des Journaux Officiels</p>
<ul class="spip">
<li>par courrier : 26 rue Desaix, 75727 Paris cedex 15</li>
<li>par télécopie : 01 40 58 77 80. Il est également consultable sur le site <a href="http://www.impots.gouv.fr/" class="spip_out" rel="external">de l’administration fiscale</a>.</li></ul>
<p>Les dispositions conventionnelles qui ont primauté sur les dispositions du droit interne, selon l’article 55 de la Constitution française répartissent entre les deux Etats, le droit d’imposer les revenus perçus par leurs résidents respectifs.</p>
<h3 class="spip"><a id="sommaire_1"></a>Champ d’application de la convention</h3>
<p>Cet accord a pour objet de protéger <strong>les résidents de chacun des Etats contractants</strong> en matière d’impôts prélevés directement sur le revenu ou sur le bénéfice des sociétés.</p>
<p>La convention trouve donc à s’appliquer aux résidents de ces Etats.</p>
<p>D’après l’article 4, paragraphe 1 de la convention, une personne est considérée comme "résident d’un Etat contractant" lorsque, en vertu de la législation dudit Etat, elle se trouve assujettie à l’impôt à raison de son domicile, de sa résidence ou de critères analogues.</p>
<p>Cet article fournit au paragraphe deux des critères subsidiaires permettant de résoudre le cas de double domicile si l’assujettissement à l’impôt ne pouvait suffire.</p>
<p>Ces critères sont :</p>
<ul class="spip">
<li>un foyer d’habitation permanent (il s’agit, par exemple, du lieu de situation du conjoint ou des enfants) ;</li>
<li>l’Etat où la personne possède le centre de ses intérêts vitaux (tant professionnels que privés) ;<br class="manualbr">- l’Etat dans lequel elle séjourne de façon habituelle (notion de 183 jours de présence physique sur le territoire au cours d’une année fiscale) ;</li>
<li>à défaut, l’Etat dont elle possède la nationalité. Pour les personnes morales, en cas de siège social situé dans les deux pays, le critère déterminant pris en compte sera celui du lieu effectif où se trouve le directoire.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Dispositions conventionnelles sur certaines catégories de revenus</h3>
<h4 class="spip">Traitements, salaires, pensions et rentes</h4>
<h5 class="spip">Rémunérations privées</h5>
<p><strong>Principe</strong></p>
<p>L’article 15, paragraphe 1 précise que les traitements et salaires d’origine privée ne sont, en règle générale, imposables que dans l’Etat où s’exerce l’activité.</p>
<p><strong>Exception à cette règle générale</strong></p>
<p>Le maintien de l’imposition dans l’Etat de résidence du bénéficiaire est prévu par le paragraphe 2 du même article sous réserve de trois conditions simultanément remplies :</p>
<ul class="spip">
<li>le séjour temporaire du bénéficiaire dans l’autre Etat ne dépasse pas une durée totale de 183 jours ;</li>
<li>la rémunération est payée par un employeur qui n’est pas résident de l’Etat d’exercice ;</li>
<li>la rémunération ne doit pas être à la charge d’un établissement stable ou d’une base fixe de l’employeur dans l’Etat.</li></ul>
<p>Exemple : Monsieur X est envoyé trois mois à Varsovie, soit quatre-vingt-dix jours (mai, juin, juillet de l’année n) par une PME située en France, fabricant d’articles de maroquinerie, en vue de prospecter le marché polonais. Cette entreprise ne dispose d’aucune succursale ni bureau en Pologne et rémunère l’intéressé. Dans un tel cas, Monsieur X devra déclarer ses revenus en France.</p>
<p>Au contraire, si Monsieur X est envoyé du mois de février inclus de l’année N au mois de mai inclus de l’année N+1, son séjour en Pologne entraîne son imposition dans ce pays.</p>
<h5 class="spip">Rémunérations publiques</h5>
<p>L’article 19 prévoit que les traitements, salaires et rémunérations analogues payés au titre de services à caractère public qui leur ont été rendus, ainsi que les pensions de retraite, par un Etat ou une personne morale de droit public de cet Etat, reste imposable dans cet Etat.</p>
<p>Exemple : Monsieur X est fonctionnaire de l’Etat français. Il résidait pendant son activité en France et décide d’aller prendre sa retraite chez l’un de ses enfants à Varsovie. Les montants de ses pensions resteront imposés en France ; son dossier est alors pris en charge par un centre des impôts spécial, le Centre des Impôts des Non-résidents.</p>
<p>Toutefois, en vertu des dispositions des paragraphes 1 a et 1 b de cet article, cette règle ne s’applique pas lorsque le bénéficiaire possède la nationalité de l’autre Etat, et est résident de cet autre Etat (et concernant les rémunérations, que les services y sont rendus). En pratique, cette situation est rare, aussi l’imposition de la rémunération s’effectue-t-elle dans l’Etat de la source.</p>
<p>Par ailleurs, le paragraphe4 de ce même article, prévoit que les règles de principe fixées ne sont pas applicables aux rémunérations ou aux pensions versées au titre de services rendus dans le cadre d’une activité industrielle ou commerciale exercée par un Etat ou une personne morale de droit public.</p>
<p>Les sommes versées à ce titre sont imposées soit dans l’Etat d’exercice de l’activité (article 15 de la convention), soit dans l’Etat de résidence du bénéficiaire (article 18 de la convention).</p>
<p>Exemple : Monsieur X, agent E.D.F., est envoyé en Pologne afin d’effectuer des travaux de conception avec les services locaux polonais.</p>
<p>Monsieur X est rémunéré par E.D.F. France.</p>
<p>E.D.F. étant un établissement de droit public à caractère industriel et commercial, les rémunérations allouées à Monsieur X seront imposées en Pologne.</p>
<h5 class="spip">Pensions et rentes</h5>
<p>L’article 18 prévoit que les pensions de retraite de source privée ainsi que les rentes viagères restent imposables dans l’Etat dont le bénéficiaire est un résident.</p>
<p>En outre, la convention ne comportant aucune disposition particulière concernant les prestations de sécurité sociale, il en résulte que les pensions privées versées par la Sécurité sociale française à des résidents de Pologne échappent à l’impôt français et sont par conséquent imposées en Pologne.</p>
<p>Exemple : Monsieur X, citoyen français qui a exercé une activité salariée en Pologne, décide de venir prendre sa retraite en France. Les pensions versées par la Pologne au titre de cette activité sont imposables en France.</p>
<h5 class="spip">Etudiants, stagiaires, enseignants, chercheurs</h5>
<p>L’article 20, paragraphe 3, de la convention prévoit que les étudiants, les stagiaires d’un Etat qui séjournent dans l’autre Etat à seule fin d’y poursuivre leurs études ou leur formation et qui perçoivent des subsides d’origine étrangère à cet Etat sont exonérés d’impôt par ce dernier Etat.</p>
<p>L’article 20, paragraphe 1, précise que les rémunérations versées aux enseignants ou aux chercheurs résidents d’un Etat se rendant temporairement dans l’autre Etat en vue d’y exercer une activité pédagogique ou de recherches pendant une période <strong>ne dépassant pas deux ans</strong>, dans une université, un collège, une école ou autre établissement restent imposables dans l’Etat de résidence.</p>
<h4 class="spip">Autres catégories de revenus</h4>
<h5 class="spip">Bénéfices industriels et commerciaux</h5>
<p>L’article 7, paragraphe 1, dispose que les entreprises industrielles et commerciales sont imposables sur le territoire où se trouve un établissement stable.</p>
<h5 class="spip">Bénéfices des professions non commerciales et des revenus non commerciaux</h5>
<p>L’article 14, paragraphe 1, stipule que les revenus provenant de l’exercice d’une profession libérale ou d’autres activités indépendantes sont imposables dans l’Etat sur le territoire duquel se trouve l’installation permanente ou base fixe où s’exerce de façon régulière l’activité personnelle.</p>
<p>L’article 12, paragraphe 1, pose en principe que les revenus non commerciaux (redevances et droits d’auteur) sont imposables dans l’Etat de résidence du bénéficiaire. Toutefois, le paragraphe 2 du même article permet à l’Etat, source des redevances de prélever un impôt ne pouvant excéder 10%, à l’exclusion des redevances provenant de droits d’auteur qui restent toujours imposables dans l’Etat de résidence du bénéficiaire aux termes du paragraphe 3.</p>
<p>L’article 17, paragraphe 1, prévoit l’imposition des artistes ou sportifs sur le lieu d’exercice de leurs activités personnelles.</p>
<h5 class="spip">Revenus immobiliers</h5>
<p>L’article 6, paragraphe 1, dispose que les revenus des biens immobiliers y compris les bénéfices des exploitations agricoles sont imposables dans l’Etat où ils sont situés.</p>
<h5 class="spip">Revenus de capitaux mobiliers</h5>
<p><strong>Les dividendes</strong></p>
<p>Ce terme désigne les revenus provenant d’actions, actions ou bons de jouissance, parts de mines, parts de fondateur ou autres parts bénéficiaires, à l’exception des créances et les revenus d’autres parts sociales assimilés aux revenus d’actions.</p>
<p>De manière générale, l’article 10, paragraphe 2, maintient le droit au profit de l’Etat, source des revenus de les imposer par une retenue à la source de 15% en général et de 5% lorsque le bénéficiaire détient une participation d’au moins 10% dans le capital de la société distributrice.</p>
<p><strong>Les intérêts</strong></p>
<p>Ce terme désigne les revenus des fonds publics, des obligations d’emprunts, des obligations d’emprunts et des créances de toute nature ainsi qu’aux intérêts produits par des titres négociables, les bons de caisse et les intérêts de créances ordinaires.</p>
<p>L’article 11, paragraphe 1, précise que les intérêts provenant d’un Etat et payés à un résident de l’autre Etat restent imposables dans l’Etat de résidence du bénéficiaire.</p>
<h3 class="spip"><a id="sommaire_3"></a>Elimination de la double imposition</h3>
<p>L’élimination de la double imposition pour les résidents de France qui perçoivent des revenus de source polonaise s’opère aux termes de l’article 23, paragraphe 2 selon la méthode de l’exemption ou du crédit d’impôt.</p>
<p>Ces méthodes n’excluent pas la prise en compte des revenus ainsi exonérés pour la détermination du taux effectif.</p>
<p>Le calcul du taux effectif se décompose tout d’abord par la détermination d’une cotisation de base correspondant à l’ensemble des revenus de source française et(ou) étrangère passible de l’impôt français suivant les règles de la législation interne.</p>
<p>Ensuite, l’impôt exigible sera égal au produit de cette cotisation de base par le rapport entre le montant net total des revenus conventionnellement imposables en France et le montant total du revenu net d’après lequel le calcul de ladite cotisation de base a été effectué.</p>
<p>Au résultat ainsi obtenu peuvent être appliquées les réfactions prévues par la loi interne (crédit ou réduction d’impôt).</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/fiscalite-23535/article/convention-fiscale-114290). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
