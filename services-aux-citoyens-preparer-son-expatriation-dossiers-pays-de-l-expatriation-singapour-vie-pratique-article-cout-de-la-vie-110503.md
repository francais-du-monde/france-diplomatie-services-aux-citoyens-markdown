# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/vie-pratique/article/cout-de-la-vie-110503#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/vie-pratique/article/cout-de-la-vie-110503#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/vie-pratique/article/cout-de-la-vie-110503#sommaire_3">Alimentation</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/vie-pratique/article/cout-de-la-vie-110503#sommaire_4">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>L’unité monétaire est le <i>Singapore Dollar</i> divisé en 100 cents. Les pièces de monnaie actuellement en circulation sont de 5, 10, 20, 50 cents et 1 dollar. Les billets existent en coupures de 2, 5, 10, 20, 50, 100, 500, 1000 et 10 000 $. Les billets de Brunei ont une valeur équivalente aux billets singapouriens et sont utilisables à Singapour.</p>
<p>En juin 2013, le dollar de Singapour vaut 0,5936 euros c’est-à-dire que 1 euro équivaut à 1,684 dollars de Singapour.</p>
<p>Il y a des comptoirs de change dans tous les quartiers touristiques et des distributeurs d’argent dans tous les quartiers commerciaux. Les cartes de crédit internationales (American Express, Visa, Mastercard…) sont acceptées presque partout sauf dans certains supermarchés.</p>
<p>Les moyens de paiement les plus utilisés sont la carte bancaire pour les achats courants, les virements bancaires ou <i>Internet banking</i> pour les factures domestiques, le paiement en cash si l’on veut négocier le prix (meubles par exemples) et les chèques.</p>
<p>Vous pourrez également retirer de l’argent en vous présentant à votre banque avec votre carnet de chèque local ou bien en vous présentant avec votre carte de crédit et votre passeport dans n’importe quelle banque mais c’est en général onéreux.</p>
<p>Si vous souhaitez convertir ou acheter des devises, les taux de change sont affichés dans toutes les banques mais il est beaucoup plus avantageux de s’adresser à un <i>Money Changer</i>. Situés dans la plupart des centres commerciaux, ils sont ouverts tous les jours y compris le dimanche. Vous pouvez essayer de négocier.</p>
<p>La plupart des banques sont ouvertes de 8h30 à 16h30 du lundi au vendredi, de 8h30 à 13h le samedi. Certaines agences sont ouvertes de 11h à 19h du lundi au samedi.</p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<p>Le dollar de Singapour est librement convertible.</p>
<p>Il n’existe aucune limitation en matière de transfert de fonds. Les transferts peuvent être effectués d’une banque française vers une banque singapourienne.</p>
<p><strong>Paiements par chèque :</strong></p>
<p>Les banques sont très pointilleuses sur la rédaction des chèques et il n’est pas rare de voir un chèque revenir pour une simple rature. Si vous faites une correction, signez toujours sous celle-ci.</p>
<p><strong>Quelques conseils :</strong></p>
<p>Barrer toujours votre chèque en haut à gauche, ainsi que les mots <i>Or Bearer</i> (au porteur). <i>Pay</i> : l’ordre – indiquer précisément la raison sociale.</p>
<p><i>Singapore Dollars</i> : le montant en toutes lettres en dollars et cents.</p>
<p>Si le montant ne comporte pas de cents, terminez par <i>only</i> (ex : <i>ten dollars only</i>).</p>
<p>Vous pouvez écrire les cents comme ceci : « 30/100 » (ex : ten dollars et 30/100) ou « 30 cents » ou « thirty cents ».</p>
<p><strong>Ouverture de comptes :</strong></p>
<p>De nombreuses banques proposent des comptes rémunérés. Le dépôt d’une somme minimum est obligatoire lors de l’ouverture d’un compte. Les principales banques singapouriennes sont : DBS, OCBC, OUB, UOB…</p>
<p>Singapour est une des plus importantes place financières en Asie, presque toutes les banques étrangères et françaises sont représentées. Les banques françaises n’ont pas d’activité de guichet. Quelques banques étrangères ont une offre de banque de détail (HSBC, Citibank, ABN Amro, Standard Chartered notamment).</p>
<p>Si vous avez une carte de crédit, vous pouvez effectuer des débits sur votre compte singapourien ou français, à partir des points de retrait ATM qui se trouvent partout dans Singapour.</p>
<p>Si vous avez un compte dans une banque locale, vous pouvez demander une carte de retrait d’espèces (N.E.T.S.) et /ou une carte de crédit Visa ou Mastercard. La carte locale N.E.T.S est très pratique. C’est une carte de débit (débit immédiat sur votre compte en banque). Elle est acceptée quasiment partout sauf dans la plupart des restaurants.</p>
<p><strong>Cashcard :</strong></p>
<p>C’est une carte que vous achetez à la banque et que vous renflouez à la banque, dans un ATM, dans certaines stations essences, dans les bureaux de poste ou dans certains parkings… Elle vous permet d’effectuer un certain nombre de paiements, tels le coût de l’examen du permis de conduire, le stationnement dans la plupart des parkings, le péage d’entrée en centre ville… Placée dans un boîtier situé à la base de votre pare-brise, elle est débitée électroniquement à chaque passage sous le portique…</p>
<h3 class="spip"><a id="sommaire_3"></a>Alimentation</h3>
<p>Les frais d’alimentation peuvent être peu élevés si vous choisissez d’adopter le mode de vie local et de consommer dans les « food courts » (lieux composés de petits « snacks » spécialisés en nourriture asiatique et bon marché) ou bien si vous achetez des produits locaux.</p>
<p>Si vous optez pour conserver une alimentation « européenne », les coûts seront plus élevés qu’en France car les denrées seront importées.<br class="autobr">Prix moyen d’un repas dans un restaurant - Food Court : 4 à 12 SGD</p>
<ul class="spip">
<li>Restaurant moyen : de 18 à 35 SGD (sans vin)</li>
<li>Restaurant de première catégorie : de 35 à 130 SGD (sans vin)</li>
<li>Restaurant de luxe : 130 à 280 SGD</li></ul>
<p>Il n’est pas d’usage de donner un pourboire.</p>
<p>Pour en savoir plus :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://paris-singapore.com/quel-est-le-salaire-minimum-pour-vivre-a-singapour" class="spip_out" rel="external">Quel est le salaire minimum pour vivre à Singapour ?</a> </p>
<h3 class="spip"><a id="sommaire_4"></a>Evolution des prix</h3>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/vie-pratique/article/cout-de-la-vie-110503). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
