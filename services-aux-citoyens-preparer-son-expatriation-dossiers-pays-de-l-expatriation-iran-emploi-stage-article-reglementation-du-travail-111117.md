# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/emploi-stage/article/reglementation-du-travail-111117#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/emploi-stage/article/reglementation-du-travail-111117#sommaire_2">Fêtes légales</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/emploi-stage/article/reglementation-du-travail-111117#sommaire_3">Emploi du conjoint</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<p>Le droit du travail iranien est généralement favorable à l’employé (conditions d’emploi, indemnités de fin de fonctions identiques même pour un licenciement disciplinaire). Dans la pratique cependant, les contrats courts et les contrats de prestation de service se multiplient afin de limiter les engagements de long terme des entreprises.</p>
<p>La durée légale du travail est de 8 heures/jour et 44 heures/semaine, avec des possibilités de flexibilité se compensant d’un jour à l’autre. Le travail de nuit (entre 22h et 6h) ouvre droit à une rémunération supérieure de 35%.</p>
<p>Les droits à congés sont d’un mois par an. Il s’y ajoute un certain nombre de fêtes légales (26, dont certaines tombent un jour chômé) et, fréquemment, des jours durant lesquels certaines activités sont suspendues (écoles, administration, certaines industries) en raison des pics de pollution.</p>
<h3 class="spip"><a id="sommaire_2"></a>Fêtes légales</h3>
<p>Le calendrier iranien est un calendrier solaire. L’année commence le jour de l’équinoxe du printemps : le 21 mars. Elle est divisée en douze mois, les six premiers (Farvardin, Ordibeshesht, Khordad, Tir, Mordad, Shahrivar) de 31 jours, les cinq suivants (Mehr, Aban, Azar, Dey, Bahman) de 30 jours, le dernier, Esfand, de 29 jours (sauf tous les quatre ans où il compte 30 jours). Le début de l’ère iranienne correspond au départ du Prophète de la Mecque (Hégire) en 622 après J.C. Pour passer de l’année grégorienne à l’année iranienne, il suffit de retrancher 621. Ainsi, 2014 - 621 : 1393.</p>
<p>Les fêtes légales correspondent pour partie à des jours déterminés selon le calendrier lunaire musulman (ces dates bougent donc de 10 jours/an par rapport aux calendriers grégorien et persan), et pour partie à des dates du calendrier persan.</p>
<p>On compte aussi de nombreuses fêtes religieuses musulmanes dont les dates varient en fonction du calendrier lunaire : début du ramadan, Aïd el Fitr. Il est conseillé de se renseigner avant d’entreprendre un voyage. Le vendredi est chômé. En général le jeudi est également un jour de repos.</p>
<h3 class="spip"><a id="sommaire_3"></a>Emploi du conjoint</h3>
<p>Il n’existe pas de possibilités légales d’emplois des conjoints. Les époux(ses) des détachées(ées) ne sont pas autorisés(ées) à occuper un emploi en Iran n’étant pas titulaires d’autorisation de travail.</p>
<p><i>Mise à jour : février 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/emploi-stage/article/reglementation-du-travail-111117). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
