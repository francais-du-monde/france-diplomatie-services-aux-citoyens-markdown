# Recherche d’emploi

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/emploi-stage/article/recherche-d-emploi#sommaire_1">Outils pour la recherche d’emploi</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/emploi-stage/article/recherche-d-emploi#sommaire_2">Organismes pour la recherche d’emploi</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Outils pour la recherche d’emploi</h3>
<h4 class="spip">Journaux</h4>
<p>Il existe un supplément « emploi » dans la presse locale le vendredi, par exemple dans les journaux Maariv et Yediot.</p>
<p>Les sites des journaux israéliens publient les annonces d’emploi sur leur site en les classant selon le métier ou la région. Certains sites proposent gratuitement l’envoi hebdomadaire de la liste des nouvelles offres d’emplois. Le journal Maariv propose, depuis peu, des alertes par mail qui seront envoyées dès que des annonces correspondant au profil du candidat sont publiées. Ce service est gratuit et permet d’être informé rapidement.</p>
<h4 class="spip">Sites internet</h4>
<p><strong>Sites en anglais :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.jobs-israel.com/" class="spip_out" rel="external">www.jobs-israel.com</a> (version anglaise de : <a href="http://www.jobnet.co.il/" class="spip_out" rel="external">www.jobnet.co.il</a>)<br class="manualbr">L’un des plus anciens sites israéliens d’offres d’emploi, il a été fondé en 1997 grâce à un financement du ministère des Sciences. Il permet de poster son CV et de consulter des offres d’emplois selon plusieurs critères (secteur, entreprise, etc.). Très orienté high-tech mais offre aussi des postes dans la finance, l’administration, le tourisme et le marketing. </p>
<p><strong>Sites en hébreu : </strong></p>
<ul class="spip">
<li><a href="http://www.yjobs.co.il/" class="spip_out" rel="external">www.Yjobs.co.il</a><br class="manualbr">Ce site appartient à Ynet, l’un des principaux portails israéliens. On y trouve toutes sortes d’annonces allant du secrétariat aux postes de directeur des ventes, dont de nombreuses offres orientées vers l’export pour lesquelles le français peut représenter un sérieux avantage.</li>
<li><a href="http://www.yad2.co.il/Job" class="spip_out" rel="external">www.yad2.co.il/Job</a><br class="manualbr">Section emploi du site de petites annonces Yad2.co.il, il propose plusieurs milliers d’annonces. Son utilisation est simple, il vous suffit de choisir votre domaine de prédilection, votre zone géographique et enfin le type d’emploi que vous recherchez.</li>
<li><a href="http://www.madas.co.il/c4-he/%D7%93%D7%A8%D7%95%D7%A9%D7%99%D7%9D.aspx" class="spip_out" rel="external">www.madas.co.il</a><br class="manualbr">Section emploi du site de l’université de Tel Aviv, récemment racheté par le groupe « Dapé Zahav », les pages jaunes israéliennes. Il est très orienté vers les jeunes diplômés et propose ainsi beaucoup d’emplois ne nécessitant pas d’expérience préalable.</li>
<li><a href="http://www.jobmaster.co.il/" class="spip_out" rel="external">www.jobmaster.co.il</a><br class="manualbr">Plus ancien, mais aussi plus généraliste. Vous y trouverez beaucoup d’offres d’intérim ou émanant de petites sociétés de placement professionnel. A ne pas négliger cependant car il possède un très large éventail d’offres dans des secteurs très variés.</li>
<li><a href="http://www.manpower.co.il/" class="spip_out" rel="external">www.manpower.co.il</a><br class="manualbr">Branche locale du géant des ressources humaines, très présent en Israël. Manpower travaille aussi en partenariat avec le ministère de l’Intégration et propose de nombreuses offres dans le domaine de la finance où le français est un atout.</li>
<li><a href="http://www.hever.co.il/" class="spip_out" rel="external">www.hever.co.il</a></li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Organismes pour la recherche d’emploi</h3>
<ul class="spip">
<li><a href="http://www.israelvalley.com/" class="spip_out" rel="external">Chambre de commerce et d’industrie Israël-France</a> ;</li>
<li>Comité consulaire pour l’emploi et la formation professionnelle porté, par convention passée avec le consulat de France à Tel-Aviv, par l’<a href="http://www.ami-emploi-israel.org/" class="spip_out" rel="external">association AMI</a> ;</li>
<li><a href="http://www.pole-emploi-international.fr/www.eei.com.fr/eei/accueil.jsp" class="spip_out" rel="external">Pôle emploi international</a> ;</li>
<li><a href="http://www.moia.gov.il/Moia_fr/" class="spip_out" rel="external">Ministère de l’Immigration et de l’Intégration</a> ;<br class="manualbr">Siège : Hakyria, 2 rue Kaplan Bat. 2<br class="manualbr">Jérusalem 91950 <br class="manualbr">Tél. : 02 675 26 11<br class="manualbr">Département Publication : 15 rue Hillel <br class="manualbr">Jérusalem 91130 <br class="manualbr">Tél. : 02 621 45 68 / 02 624 15 85</li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/emploi-stage/article/recherche-d-emploi). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
