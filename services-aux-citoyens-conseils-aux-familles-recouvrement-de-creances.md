# Recouvrement de créances alimentaires à l’étranger

<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-conseils-aux-familles-recouvrement-de-creances-article-presentation-generale-82911.md" title="Présentation générale">Présentation générale</a></li>
<li><a href="services-aux-citoyens-conseils-aux-familles-recouvrement-de-creances-article-la-procedure-de-recouvrement-de.md" title="La procédure de recouvrement de créances alimentaires à l’étranger">La procédure de recouvrement de créances alimentaires à l’étranger</a></li>
<li><a href="services-aux-citoyens-conseils-aux-familles-recouvrement-de-creances-article-personne-creanciere.md" title="Personne créancière">Personne créancière</a></li>
<li><a href="services-aux-citoyens-conseils-aux-familles-recouvrement-de-creances-article-personne-debitrice.md" title="Personne débitrice">Personne débitrice</a></li>
<li><a href="services-aux-citoyens-conseils-aux-familles-recouvrement-de-creances-article-l-obligation-alimentaire.md" title="L’obligation alimentaire">L’obligation alimentaire</a></li>
<li><a href="services-aux-citoyens-conseils-aux-familles-recouvrement-de-creances-article-l-aide-judiciaire.md" title="L’aide judiciaire">L’aide judiciaire</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/conseils-aux-familles/recouvrement-de-creances/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
