# Emploi, stage

<h2 class="rub22706">Marché du travail</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/emploi-stage/#sommaire_1">Rémunération</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Rémunération</h3>
<p><strong>Le salaire minimum</strong></p>
<p>Le salaire minimum (<a href="http://www.fairwork.gov.au/pay/national-minimum-wage/Pages/default.aspx" class="spip_out" rel="external">Federal Minimum Wage</a>), considéré comme étant le salaire permettant à un Australien de vivre dignement, s’élève à compter du 1er juillet 2013 à 16,37 AUD de l’heure, soit 622,20 AUD pour 38 heures hebdomadaires de travail.</p>
<p>Ce salaire, qui est revu annuellement, concerne la plupart des employés du système national. Toutefois, les travailleurs saisonniers et les employés non-réguliers perçoivent un supplément de 23%, soit un montant horaire de 20,14 AUD au 1er juillet 2013, les jeunes travailleurs (-16 ans à 20 ans inclus) un salaire de 40 à 10% inférieur au salaire adulte et les apprentis un salaire calculé en fonction de leur année d’apprentissage. Pour de plus amples informations, consulter :</p>
<p>La <a href="http://www.fairwork.gov.au/employment/hours-of-work/pages/default.aspx" class="spip_out" rel="external">durée hebdomadaire légale de travail</a> est de 38 heures ; un employeur peut demander à ses employés de travailler au-delà des heures normales, si les heures supplémentaires sont « raisonnables ». Mais un employé peut refuser d’effectuer des heures supplémentaires si la demande est manifestement déraisonnable. Afin de déterminer si les heures supplémentaires demandées sont « raisonnables », l’employeur doit bien prendre en considération différents éléments d’importance dont la situation personnelle et familiale de l’employé.</p>
<p><strong>L’échelle des salaires</strong></p>
<p>Il existe une échelle des salaires au sein d’une même fonction. Les rémunérations et les conditions de travail sont fixées par les conventions collectives de travail écrites et certifiées (<i>certified agreements</i> appelés aussi <i>awards</i>) pour un groupe particulier de travailleurs. Les salaires varient en fonction des qualifications, de l’âge, de l’expérience, de l’échelon, de l’ancienneté, du type de contrat (temporaire ou vacataire), du sexe et de la région. Les hommes gagnent en général plus que les femmes. Dans certaines régions l’écart est assez marqué.</p>
<p>Salaire annuel moyen à temps plein (2012) par Etat.</p>
<table class="spip" summary="">
<caption>Salaire annuel moyen à temps plein par Etat (2012)</caption>
<thead><tr class="row_first"><th id="id669b_c0">Etat</th><th id="id669b_c1">Salaire annuel moyen (AUD)</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="id669b_c0">Tasmania</td>
<td class="numeric virgule" headers="id669b_c1">61,532</td></tr>
<tr class="row_even even">
<td headers="id669b_c0">South Australia</td>
<td class="numeric virgule" headers="id669b_c1">63,471</td></tr>
<tr class="row_odd odd">
<td headers="id669b_c0">Victoria</td>
<td class="numeric virgule" headers="id669b_c1">68,219</td></tr>
<tr class="row_even even">
<td headers="id669b_c0">Queensland</td>
<td class="numeric virgule" headers="id669b_c1">69,186</td></tr>
<tr class="row_odd odd">
<td headers="id669b_c0">New South Wales</td>
<td class="numeric virgule" headers="id669b_c1">69,592</td></tr>
<tr class="row_even even">
<td headers="id669b_c0">Northern Territory</td>
<td class="numeric virgule" headers="id669b_c1">71,942</td></tr>
<tr class="row_odd odd">
<td headers="id669b_c0">Western Australia</td>
<td class="numeric virgule" headers="id669b_c1">78,743</td></tr>
<tr class="row_even even">
<td headers="id669b_c0">Capital Territory</td>
<td class="numeric virgule" headers="id669b_c1">80,766</td></tr>
</tbody>
</table>
<table class="spip" summary="">
<caption>Salaire annuel moyen à temps plein par secteurs (2012)</caption>
<thead><tr class="row_first"><th id="id3b3e_c0">Secteur</th><th id="id3b3e_c1">Salaire annuel moyen (AUD)</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="id3b3e_c0">Accommodation, Cafes and Restaurants</td>
<td class="numeric virgule" headers="id3b3e_c1">49,982</td></tr>
<tr class="row_even even">
<td headers="id3b3e_c0">Retail Trade</td>
<td class="numeric virgule" headers="id3b3e_c1">50,944</td></tr>
<tr class="row_odd odd">
<td headers="id3b3e_c0">Other Services</td>
<td class="numeric virgule" headers="id3b3e_c1">58,183</td></tr>
<tr class="row_even even">
<td headers="id3b3e_c0">Manufacturing</td>
<td class="numeric virgule" headers="id3b3e_c1">62,504</td></tr>
<tr class="row_odd odd">
<td headers="id3b3e_c0">Administrative and Support Services</td>
<td class="numeric virgule" headers="id3b3e_c1">62,800</td></tr>
<tr class="row_even even">
<td headers="id3b3e_c0">Rental, Hiring and Real Estate Services</td>
<td class="numeric virgule" headers="id3b3e_c1">63,352</td></tr>
<tr class="row_odd odd">
<td headers="id3b3e_c0">Arts and Recreation Services</td>
<td class="numeric virgule" headers="id3b3e_c1">64,308</td></tr>
<tr class="row_even even">
<td headers="id3b3e_c0">Construction</td>
<td class="numeric virgule" headers="id3b3e_c1">71,115</td></tr>
<tr class="row_odd odd">
<td headers="id3b3e_c0">Transport, Postal, Warehousing</td>
<td class="numeric virgule" headers="id3b3e_c1">71,151</td></tr>
<tr class="row_even even">
<td headers="id3b3e_c0">Wholesale Trade</td>
<td class="numeric virgule" headers="id3b3e_c1">72,608</td></tr>
<tr class="row_odd odd">
<td headers="id3b3e_c0">Health Care and Social Assistance</td>
<td class="numeric virgule" headers="id3b3e_c1">73,991</td></tr>
<tr class="row_even even">
<td headers="id3b3e_c0">Public Administration and Safety</td>
<td class="numeric virgule" headers="id3b3e_c1">76,970</td></tr>
<tr class="row_odd odd">
<td headers="id3b3e_c0">Education and Training</td>
<td class="numeric virgule" headers="id3b3e_c1">80,075</td></tr>
<tr class="row_even even">
<td headers="id3b3e_c0">Information Media and Telecommunications</td>
<td class="numeric virgule" headers="id3b3e_c1">84,656</td></tr>
<tr class="row_odd odd">
<td headers="id3b3e_c0">Electricity, Gas, Water and Waste Services</td>
<td class="numeric virgule" headers="id3b3e_c1">84,750</td></tr>
<tr class="row_even even">
<td headers="id3b3e_c0">Financial and Insurance Services</td>
<td class="numeric virgule" headers="id3b3e_c1">84,947</td></tr>
<tr class="row_odd odd">
<td headers="id3b3e_c0">Professional, Scientific and Technical Services</td>
<td class="numeric virgule" headers="id3b3e_c1">88,603</td></tr>
<tr class="row_even even">
<td headers="id3b3e_c0">Mining</td>
<td class="numeric virgule" headers="id3b3e_c1">121,586</td></tr>
</tbody>
</table>
<p><i>Mise à jour : octobre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-australie-emploi-stage-article-stages.md" title="Stages">Stages</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-australie-emploi-stage-article-entretien-d-embauche-108664.md" title="Entretien d’embauche">Entretien d’embauche</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-australie-emploi-stage-article-lettre-de-motivation-108663.md" title="Lettre de motivation">Lettre de motivation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-australie-emploi-stage-article-curriculum-vitae.md" title="Curriculum vitae">Curriculum vitae</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-australie-emploi-stage-article-recherche-d-emploi.md" title="Recherche d’emploi">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-australie-emploi-stage-article-reglementation-du-travail.md" title="Réglementation du travail">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-australie-emploi-stage-article-marche-du-travail-108659.md" title="Marché du travail">Marché du travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/emploi-stage/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
