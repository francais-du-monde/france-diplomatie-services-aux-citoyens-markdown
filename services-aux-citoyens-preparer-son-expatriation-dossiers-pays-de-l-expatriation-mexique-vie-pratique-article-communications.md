# Communications

<h4 class="spip">Téléphone – Internet</h4>
<p>Les liaisons téléphoniques et télégraphiques sont bonnes. Il est à noter que la situation de quasi monopole de l’opérateur Telmex/Telcel classe le prix du téléphone au Mexique parmi les plus élevés au monde.</p>
<p>L’indicatif du pays est le 52, suivi de celui de la ville : 55 pour Mexico, 33 pour Guadalajara, 81 pour Monterrey suivi des huit chiffres de votre correspondant. Exemple : 00 52 55 9171 9881 (consulat général de France à Mexico).</p>
<p>Pour les autres villes, l’indicatif est composé de trois chiffres suivi des sept chiffres de votre correspondant. Exemple : 00 52951515 21 84 (agence consulaire à Oaxaca).</p>
<p>Entre Etats du Mexique depuis un téléphone fixe : composer le 01 suivi de l’indicatif de la ville et des huit ou sept chiffres du numéro de votre correspondant.</p>
<p>Les numéros des téléphones portables comportent dix chiffres.</p>
<p>Pour appeler depuis la France, composez : 00 52 1 + les dix chiffres du numéro de votre correspondant.</p>
<p>Dans le pays pour les appels de portable à portable, composez les dix chiffres du numéro. Pour appeler un portable depuis un fixe dans le district fédéral, composez le 044 + les dix chiffres du numéro. Pour appeler un portable du district fédéral depuis un fixe d’un autre Etat du Mexique ou un portable d’un Etat depuis un fixe d’un autre Etat, composez le 045 + les dix chiffres du numéro.</p>
<p>Renseignements téléphoniques : 040.</p>
<p>On trouve partout des cabines téléphoniques qui fonctionnent avec des cartes à 30, 50 ou 100 pesos, vendues dans les kiosques à journaux</p>
<p>Pour téléphoner du Mexique vers la France il convient de composer : 00 33 suivi du numéro de votre correspondant, sans le 0 au début.</p>
<p>L’usage de l’Internet est de plus en plus répandu et dans tout le pays. Il est possible de s’abonner à différents serveurs, soit par le réseau téléphonique (Telmex), soit par le câble (Cablevision). De nombreux cybercafés fonctionnent, non seulement dans la capitale mais aussi en province (tarifs généralement modérés).</p>
<h4 class="spip">Téléphoner gratuitement par Internet</h4>
<p>Il est généralement très avantageux d’utiliser <strong>des logiciels de téléphonie sur IP</strong> (Skype, Google talk, Live messenger, Yahoo ! messenger, etc.) car les tarifs proposés à l’international sont souvent moindre que ceux des opérateurs locaux exploitant des lignes classiques, voire gratuits dans certaines conditions.</p>
<h4 class="spip">Poste</h4>
<p>Les délais sont assez longs et la fiabilité incertaine.</p>
<p>Les bureaux de poste (oficinas de correos) ouvrent en général du lundi au vendredi de 9h-17h, et le samedi matin.</p>
<p><i>Mise à jour : septembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/vie-pratique/article/communications). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
