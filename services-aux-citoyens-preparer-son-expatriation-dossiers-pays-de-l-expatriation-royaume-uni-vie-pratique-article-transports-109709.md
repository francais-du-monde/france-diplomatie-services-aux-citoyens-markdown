# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/vie-pratique/article/transports-109709#sommaire_1">Permis de conduire</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/vie-pratique/article/transports-109709#sommaire_2">Code de la route</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/vie-pratique/article/transports-109709#sommaire_3">Assurances et taxes</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/vie-pratique/article/transports-109709#sommaire_4">Achat et location</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/vie-pratique/article/transports-109709#sommaire_5">Immatriculation</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/vie-pratique/article/transports-109709#sommaire_6">Entretien</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/vie-pratique/article/transports-109709#sommaire_7">Réseau routier</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/vie-pratique/article/transports-109709#sommaire_8">Transports en commun</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Permis de conduire</h3>
<p>Il est légal de conduire avec un <strong>permis français en cours de validité</strong>. En effet, en application des directives européennes, les ressortissants français venant s’établir au Royaume-Uni ne sont plus tenus d’échanger leur permis de conduire français contre un permis britannique.</p>
<p>Cet échange reste toutefois possible et la demande se fait auprès de la <i>Driver and Vehicle Licensing Agency</i> au moyen d’un formulaire à retirer dans les bureaux de poste.</p>
<p>La délivrance du permis de conduire britannique entraîne automatiquement le retrait du permis français et son retour à l’autorité qui l’a émis en France. Le permis britannique est valable jusqu’à l’âge de 70 ans.</p>
<h4 class="spip">En cas de perte ou de vol </h4>
<p>En application de la Directive européenne 91/439, les personnes ayant été titulaires d’un permis de conduire français perdu ou volé et résidentes en Grande-Bretagne sont tenues de s’adresser aux autorités britanniques pour l’obtention d’un duplicata.</p>
<p>Pour cela, vous devez demander à la préfecture ou sous-préfecture émettrice du permis perdu ou volé, un "relevé d’information restreint" ou "Attestation de permis de conduire" et retirer le formulaire de demande dans votre bureau de poste britannique : "D1". Une fois remplis, il vous faudra retourner les formulaires aux autorités britanniques (<i>Driver and Vehicle Licensing Agency</i>) à l’adresse mentionnée sur ledit formulaire, accompagné de votre "relevé d’information restreint" au lieu de votre permis français, ainsi que les pièces requises par le <i>Driver and Vehicle Licensing Agency</i> (liste figurant sur le formulaire). Un permis britannique vous sera alors retourné sous trois semaines environ.</p>
<p><strong>Pour en savoir plus</strong> : <a href="http://www.ambafrance-uk.org/Le-permis-de-conduire-Toutes" class="spip_out" rel="external">ambassade de France au Royaume-Uni</a></p>
<h3 class="spip"><a id="sommaire_2"></a>Code de la route</h3>
<p>Veillez au respect le plus strict du code de la route, sous peine de pénalités importantes. Contrairement au système français, les interdictions de s’arrêter et de stationner ne sont pas toutes absolues : elles peuvent dépendre du jour de la semaine et de l’heure de la journée. Les indications sont données sur des panneaux et/ou un marquage au sol.</p>
<p>Les règles de circulation sont très strictes au Royaume-Uni et la plus grande prudence est de mise : <strong>la conduite s’effectue à gauche</strong>.</p>
<p>La vitesse est mesurée en <i>miles</i>/heure : 1 <i>mile</i> équivalant à 1,609 km</p>
<p>Les limites autorisées sont les suivantes :</p>
<ul class="spip">
<li>en ville : 30 m/h, soit 50 km/h (20m/h, soit 30km/h pour certaines zones résidentielles)</li>
<li>sur route : 50 m/h, soit 80 km/h ;</li>
<li>sur autoroute : 70 m/h, soit 110 km/h.</li></ul>
<p>Vous devez respecter le stationnement en ville et les zones réglementaires de parking. L’amende à régler en cas d’enlèvement du véhicule par la fourrière est très élevée : plus de 150 euros ; les amandes pour d’autres contraventions peuvent s’y ajouter (même si aucune signalisation ne met en garde). L’amende est divisée par deux si elle est réglée sous 15 jours.</p>
<p>A Londres, (comme dans d’autres grandes villes du Royaume Uni), il est fortement conseillé de garer sa voiture dans les parcs de stationnement plutôt que dans les rues où les emplacements sont réservés aux titulaires d’un permis de stationnement (« resident permit »).</p>
<h3 class="spip"><a id="sommaire_3"></a>Assurances et taxes</h3>
<h4 class="spip">Assurances</h4>
<p>Les assurances sont obligatoires pour le risque au "tiers". Les primes sont très variables et il est important de s’informer pour rechercher la formule la plus adaptée. Sur Internet, le site suivant propose une documentation et des liens utiles en matière d’assurance automobile : <a href="http://www.elephant.co.uk/" class="spip_out" rel="external">Elephant.co.uk</a></p>
<p>Un certain nombre de grandes sociétés d’assurance française (AXA, AGF, Groupama) sont implantées au Royaume-Uni.</p>
<h4 class="spip">Taxes</h4>
<p>La <i>Road Tax</i>, également appelée <i>Vehicle Excise Duty</i> (VED), est l’équivalent de l’ancienne vignette automobile en France. Elle est recouvrée par la <i>Driver and Vehicle Licensing Agency</i>. Le <a href="https://www.gov.uk/calculate-vehicle-tax-rates" class="spip_out" rel="external">montant de la taxe</a> varie en fonction de l’âge du véhicule (le tarif diffère au-delà de trois ans d’ancienneté), de la cylindrée et, pour les véhicules neufs, du degré de pollution déterminé par le taux de rejet de CO2.</p>
<p>Lors d’une première immatriculation, une taxe d’enregistrement (<a href="https://www.gov.uk/vehicle-registration/new-registrations-fee" class="spip_out" rel="external">Vehicle First Registration Fee</a>) est également prélevée. Son montant s’établit à 55 £ quel que soit le type de véhicule.</p>
<p>La <a href="http://www.dvla.gov.uk/" class="spip_out" rel="external">Drive and Vehicule Licensing Agency</a> est l’organisme compétent en matière de réglementation sur la conduite automobile :</p>
<p><strong>Driver and Vehicle Licensing Agency</strong><br class="manualbr">Longview Road<br class="manualbr">Swansea SA6 7JL<br class="manualbr">Tél. : [44] (0) 87 0240 0009</p>
<p>En ville, les frais en cas d’enlèvement du véhicule par la fourrière sont très élevés (plus de 100 £) et viennent s’ajouter au montant de la contravention. Londres a instauré un péage (<i>congestion charge</i>) de 10 £ par jour pour conduire un véhicule dans les rues du centre de Londres entre 7h00 et 18h30. Il est possible de payer ce péage par internet, téléphone (0845 900 1234), dans les magasins et stations services.</p>
<h3 class="spip"><a id="sommaire_4"></a>Achat et location</h3>
<p>Toutes les marques étrangères et notamment françaises sont distribuées au Royaume-Uni à des prix supérieurs de 25 à 35% à ceux pratiqués en France.</p>
<h3 class="spip"><a id="sommaire_5"></a>Immatriculation</h3>
<p>Dans les six mois qui suivent votre arrivée au Royaume-Uni, il faut procéder à l’immatriculation du véhicule en Grande-Bretagne auprès du <i>Vehicle Registration Office</i> proche de votre résidence (liste disponible dans les bureaux de poste). Le véhicule doit alors être assuré en Grande-Bretagne.</p>
<p>Pour tout renseignement, contactez :</p>
<p><a href="https://www.gov.uk/importing-vehicles-into-the-uk/registering-an-imported-vehicle" class="spip_out" rel="external">Driver  Vehicle Registration Licencing Agency</a><br class="manualbr">Foreign Licence Section <br class="manualbr">Longview Road, <br class="manualbr">Swansea SA6 7JL <br class="manualbr">Tel. : 00 44 1792 772 151/34<br class="manualbr">Fax : 00 44 1792 783071</p>
<h3 class="spip"><a id="sommaire_6"></a>Entretien</h3>
<p>Les concessionnaires assurent le service après-vente, mais le coût de l’entretien et des réparations est élevé. Un contrôle technique (MOT) est exigé tous les ans pour les véhicules de plus de trois ans.</p>
<h3 class="spip"><a id="sommaire_7"></a>Réseau routier</h3>
<p>L’état du réseau routier est bon avec de nombreuses voies à grande circulation. Les autoroutes ne sont pas équipées de péages mais certains ponts sont payants.</p>
<p><strong>Pour en savoir plus </strong> : <a href="http://www.touteleurope.fr/index.php?amp;id=57amp;cmd=FICHEamp;uid=17amp;cHash=2d2f47ecff" class="spip_out" rel="external">la sécurité routière dans l’UE</a></p>
<h3 class="spip"><a id="sommaire_8"></a>Transports en commun</h3>
<h4 class="spip">Chemins de fer</h4>
<p>Le réseau ferroviaire est relativement ancien.</p>
<p>Les horaires ne sont pas toujours respectés. Des perturbations affectent l’ensemble du réseau en raison des importants travaux de rénovation en cours de réalisation. Il est conseillé de se renseigner auprès des sociétés de chemin de fer ou des agences de voyage avant d’entreprendre tout déplacement en train, afin d’éviter des déconvenues.</p>
<p>En Ecosse, le réseau est également assez vétuste.</p>
<h4 class="spip">Transport aérien</h4>
<p>Pour les bagages et produits acceptés en cabine, se reporter à la rubrique <a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/infos-pratiques/preparer-son-depart/transport-aerien/" class="spip_in">Transport aérien</a>.</p>
<p>Des liaisons intérieures sont assurées régulièrement (toutes les heures pour LONDRES-EDIMBOURG) par les compagnies locales "British Midlands", "AIR UK/KLM", "Easyjet" et "British Airways".</p>
<p>"Air France", "British Airways" et "British Midlands" assurent quotidiennement des liaisons entre la France et l’Ecosse. Toutefois, aucun vol direct n’existe au départ de Glasgow.</p>
<h4 class="spip">Transports urbains</h4>
<p>Le métro, très étendu, couvre tout le "grand Londres". Les bus, plus économiques (75,30 £/mois) sont fréquents mais très ralentis par la circulation. Certains, contrairement au métro qui cesse de fonctionner à minuit, disposent d’un service de nuit.</p>
<p><strong>Le prix des transports londoniens est très élevé.</strong> L’Oystercard permet d’utiliser le réseau des trains, métros, bus et <i>Dockland Light Railways </i>(DLR) en fonction d’un découpage en six zones de tarification. Le centre de Londres est situé en zone 1 et l’aéroport d’Heathrow se trouve en zone 6.</p>
<p>La tarification en vigueur en 2013, pour un abonnement mensuel, est fixée selon le barème suivant :</p>
<ul class="spip">
<li>zones 1 et 2 : 116,80 £</li>
<li>zones 1 à 3 : 136,80 £</li>
<li>zones 1 à 4 : 167,50 £</li>
<li>zones 1 à 5 : 199 £</li>
<li>zones 1 à 6 : 213,60 £</li></ul>
<p><a href="http://www.transportforlondon.gov.uk/" class="spip_out" rel="external">Transportforlondon.gov.uk</a>ou <a href="http://www.tfl.gov.uk/" class="spip_out" rel="external">Tfl.gov.uk</a></p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/vie-pratique/article/transports-109709). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
