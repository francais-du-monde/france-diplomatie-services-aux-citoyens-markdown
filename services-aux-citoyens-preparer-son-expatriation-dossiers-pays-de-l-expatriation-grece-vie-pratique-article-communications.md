# Communications

<p><strong>Téléphone – Internet</strong></p>
<p>L’indicatif téléphonique de la Grèce est le 30. Voici quelques exemples des indicatifs des différentes villes de Grèce :</p>
<ul class="spip">
<li>Athènes : 210</li>
<li>Thessalonique : 231</li>
<li>Patra : 216</li></ul>
<p>Effectuer un appel d’un n° de téléphone fixe à Athènes depuis un portable grec :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Composer le 210 suivi du numéro à sept chiffres du correspondant à Athènes.</p>
<p>Effectuer un appel d’un n° de portable grec depuis un autre portable grec :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Composer le numéro complet à 10 chiffres du correspondant (commençant par 69…).</p>
<p>Effectuer un appel d’un n° de téléphone fixe à Athènes depuis un portable français (ou depuis la France) :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Composer le 00 30 suivi du numéro à complet à 10 chiffres du correspondant à Athènes (commençant par 210…).</p>
<p>Effectuer un appel d’un n° de téléphone portable grec depuis un portable français (ou depuis la France) :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Composer le 00 30 suivi du numéro complet à 10 chiffres du correspondant (commençant par 69…) </p>
<p>Il n’y a pas de difficulté pour se connecter à Internet. Plusieurs opérateurs proposent leur service : OTE, Forthnet, Wind, etc.</p>
<p>Vous pouvez utiliser votre téléphone portable partout en Europe et dans de nombreuses autres régions du monde, grâce à la norme technique GSM de l’Union européenne. Les personnes qui utilisent leur téléphone portable à l’étranger réalisent d’importantes économies – de l’ordre de 60 % – grâce au règlement de l’UE sur l’itinérance (« roaming »), qui plafonne le coût des appels téléphoniques effectués ou reçus dans un autre pays de l’UE (« eurotarif »). De plus, une facturation à la seconde a été introduite après les premières 30 secondes pour les appels effectués, et dès la première seconde pour les appels reçus. Le coût d’envoi d’un SMS depuis l’étranger a lui aussi nettement diminué, il est désormais de 8 cents (hors TVA) maximum.</p>
<p>Les clients reçoivent un message automatique les informant du coût des appels, des SMS et des services de transmission de données à l’étranger (en cas de navigation sur internet ou de téléchargement d’un film, p. ex.). Les prix de gros que les opérateurs se facturent mutuellement sont également plafonnés, ce qui devrait réduire les coûts supportés par les consommateurs. Un mécanisme a également été mis en place afin d’éviter aux consommateurs de devoir payer des factures astronomiques pour des services d’itinérance : après un premier avertissement, la connexion internet mobile est suspendue dès que le montant atteint un certain niveau. Une limite de 50 euros par mois s’applique par défaut si le client n’a pas défini un montant spécifique.</p>
<p>Vous trouverez sur un <a href="http://ec.europa.eu/information_society/activities/roaming/index_en.htm" class="spip_out" rel="external">site web de l’UE</a> (en anglais) les eurotarifs appliqués par les opérateurs des 28 pays membres, ainsi que les liens vers leurs sites. Vous pourrez également y comparer les tarifs pratiqués pour l’envoi de SMS ou l’utilisation de services de données.</p>
<p><strong>Téléphoner gratuitement par Internet </strong></p>
<p>Il est généralement très avantageux d’utiliser <strong>des logiciels de téléphonie sur IP</strong> (Skype, Google talk, Live messenger, Yahoo ! messenger, etc.) car les tarifs proposés à l’international sont souvent moindre que ceux des opérateurs locaux exploitant des lignes classiques, voire gratuits dans certaines conditions.</p>
<p><strong>Poste</strong></p>
<p>La Poste hellénique fonctionne bien. Il faut compter environ trois jours pour l’acheminement des lettres entre la Grèce et la France, davantage pour les colis. Les garanties de réception sont bonnes.</p>
<p><i>Mise à jour : novembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/vie-pratique/article/communications). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
