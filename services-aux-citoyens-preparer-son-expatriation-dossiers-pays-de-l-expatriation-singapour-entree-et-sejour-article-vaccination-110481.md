# Vaccination

<p>Aucune vaccination n’est exigée à l’entrée du pays pour un voyageur en provenance de France.</p>
<p>Les vaccinations suivantes sont conseillées pour des raisons médicales.</p>
<p><strong>Adultes </strong> :</p>
<ul class="spip">
<li>Mise à jour des vaccinations diphtérie, tétanos, poliomyélite.</li>
<li>Hépatite A et B pour un séjour prolongé.</li></ul>
<p>La vaccination contre la fièvre jaune est obligatoire si l’on a effectué un séjour préalable en zone d’endémie. Pour plus d’informations, il est conseillé de visiter le site Internet de l’<a href="http://www.pasteur-lille.fr/fr/accueil/index.htm" class="spip_out" rel="external">Institut Pasteur de Lille</a>. De même, il est préférable de consulter votre médecin traitant avant le départ et de contracter une assurance médicale couvrant le rapatriement sanitaire.</p>
<p><strong>Enfants </strong> :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Toutes les vaccinations incluses dans le calendrier vaccinal français doivent être à jour, en particulier pour les longs séjours : BCG dès le premier mois, rougeole dès l’âge de neuf mois.</p>
<p>Il est recommandé de mettre à jour ses vaccinations avant le départ. Il est impossible de prédire d’éventuelles difficultés d’approvisionnement une fois sur place. On trouve les mêmes vaccins qu’en France avec parfois des noms différents et des schémas de vaccination différents.</p>
<p>Pour en savoir plus, lisez notre rubrique thématique <a href="services-aux-citoyens-preparer-son-expatriation-sante.md" class="spip_in">Santé</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/entree-et-sejour/article/vaccination-110481). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
