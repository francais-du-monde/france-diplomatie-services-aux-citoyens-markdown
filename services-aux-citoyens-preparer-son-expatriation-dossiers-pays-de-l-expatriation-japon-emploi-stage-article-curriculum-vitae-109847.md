# Curriculum vitae

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/emploi-stage/article/curriculum-vitae-109847#sommaire_1">Rédaction</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/emploi-stage/article/curriculum-vitae-109847#sommaire_2">Modèles de CV</a></li></ul>
<p>Au Japon, une candidature se fait essentiellement par réponse à une offre d’emploi (trouvée dans des journaux ou magazines spécialisés, sur des sites de recherche d’emploi, ou directement sur le site de l’entreprise) ou encore par présentation via un cabinet de recrutement ou un chasseur de tête.</p>
<p>S’il est possible de faire des candidatures spontanées adressées aux responsables étrangers des filiales des sociétés françaises implantées au Japon, les entreprises japonaises n’ont pas l’habitude de recevoir des candidatures spontanées sans que le candidat leur ait été préalablement introduit / recommandé par une personne connue de l’entreprise.</p>
<p>Si vous postulez à un emploi ne requérant que l’anglais, une candidature en anglais peut être suffisante. Toutefois, la plupart des emplois s’adressant à des personnes bilingues anglais et japonais, il est recommandé de joindre également un CV japonais.</p>
<p>Même pour un poste ne requérant pas le japonais dans une société étrangère, un CV japonais peut être utile. En effet, si le manager peut être étranger, le responsable des ressources humaines sera très certainement Japonais, et un CV rédigé dans sa langue peut l’aider à mieux comprendre votre candidature.</p>
<p>Si vous ne parlez pas japonais, vous pouvez faire traduire votre CV en japonais pour le cas où on vous le demanderait. Mais par soucis de cohérence, ne présentez pas de candidature en japonais si vous ne le parlez pas.</p>
<h3 class="spip"><a id="sommaire_1"></a>Rédaction</h3>
<p><strong>Si votre candidature est en anglais</strong>, optez pour un CV de type américain. Les rubriques (dont l’ordre peut varier) sont généralement :</p>
<ul class="spip">
<li>Contact details</li>
<li>Objective</li>
<li>Experience and skill summary</li>
<li>Work experience</li>
<li>Education</li>
<li>Other information</li>
<li>References</li></ul>
<p><strong>La forme du curriculum vitae japonais est différente</strong> de celle du CV français ou anglo-saxon. Le CV japonais type se présente sous la forme d’un formulaire de deux pages que l’on se procure dans le commerce (papeteries, convenient stores, 100 yens shops) et que l’on remplit à la main. Les formulaires varient dans leur forme, ce qui permet de trouver le formulaire adapté à chaque profil.</p>
<p>A noter que le CV anonyme n’existe pas, et que dans certains cas, des informations très personnelles peuvent être mentionnées, comme la profession des parents, le nombre de frères et sœurs et l’état de santé. Dans tous les cas, il est nécessaire de coller sa photo.</p>
<p>Toutefois, il est de plus en plus fréquent de faire des candidatures électroniques (une photo scannée est acceptée).</p>
<p>Le CV, format Word ou Excel, est alors identique à celui manuscrit (de nombreux modèles sont téléchargeables gratuitement sur internet). Il est également possible de calquer la mise en page de son CV sur le modèle du CV anglo-saxon.</p>
<p>Le CV japonais s’appelle un « Rirekisho » (履歴書) Il met surtout l’accent sur le parcours scolaire et la chronologie des expériences professionnelles.</p>
<p>Le « Rirekisho » s’accompagne d’un « <strong>Shokumukeirekisho </strong> » (職務経歴書), littéralement « historique des expériences professionnelles », qui décrit les expériences professionnelles et sert également de lettre de motivation.</p>
<h4 class="spip">Les rubriques du Rirekisho</h4>
<p>Les rubriques à compléter sont les suivantes :</p>
<ul class="spip">
<li>état civil et coordonnées</li>
<li>études suivies et expériences professionnelles</li>
<li>certificats</li>
<li>loisirs</li>
<li>personnalité et motivation du candidat.</li></ul>
<p>Pour une candidature papier, le stylo à plume est conseillé. Il faut utiliser une encre noire. L’usage d’un correcteur est proscrit. Les dates : utilisation du calendrier japonais (nom d’ère et année), en écrivant les chiffres avec des chiffres romains.</p>
<p><a href="http://www.kumamotokokufu-h.ed.jp/kumamoto/bungaku/wa_seireki.html" class="spip_out" rel="external">Tableau de correspondance</a> entre ères japonaises et années grégoriennes (和暦西暦年号対応表).</p>
<p>履歴書</p>
<ul class="spip">
<li>date (suivant le calendrier japonais) à laquelle le CV a été rédigé</li>
<li>photo : format standard 4cm×3cm / tenue professionnelle (costume sombre)</li>
<li>candidature papier : photo collée, écrire son nom au dos (au cas où la photo se décolle)</li>
<li>candidature électronique : une photo scannée est acceptée</li></ul>
<h5 class="spip">Partie 1 : informations personnelles</h5>
<p>Partie “Furigana” : la règle veut qu’on utilise le syllabaire avec lequel le titre “furigana” est écrit (si -ふりがな utiliser les hiragana / si -フリガナ- utiliser les katakana) Toutefois, les noms d’étrangers s’écrivant en katakana, il est conseillé d’écrire les furigana en katakana.</p>
<ul class="spip">
<li>氏名 Prénom et nom : en romaji (caractères romains) – on peut mettre entre parenthèses le kanji de sa nationalité à côté (ex : 仏 pour Français) ;</li>
<li>Entourer son sexe (男・女) ;</li>
<li>印 : sceau : peut être laissé en blanc si on n’en a pas ;</li>
<li>生年月日 Date de naissance : selon le calendrier japonais, avec l’âge actuel entre parenthèses ;</li>
<li>現住所 Adresse actuelle : inscrire l’adresse complète, et la transcription en furigana dans la case du-dessus. Adresse complète au Japon en japonais (△丁目△番△号 □□マンション　△△号室 – transcription en furigana pour la partie ville – arrondissement suffit) ; si vous résidez à l’étranger, écrire votre adresse en romaji ;</li>
<li>自宅電話 Numéro de téléphone fixe</li>
<li>携帯電話 Numéro de téléphone mobile</li>
<li>Courriel</li>
<li>連絡先 et 連絡先電話 et communiquer l’adresse et le numéro de téléphone auxquels vous préférez être contacté (contact au Japon si vous êtes en France par exemple)</li></ul>
<h5 class="spip">Partie 2 : parcours scolaire et professionnel (学歴・職歴)</h5>
<p>Ecrire dans l’ordre chronologique, du plus ancien au plus récent, en séparant le parcours scolaire du parcours professionnel. Les dates sont inscrites suivant le calendrier japonais ( : ère et année, ex :14)<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> 学歴 (Parcours scolaire) : Ecrire「学歴」 au milieu de la première ligne</p>
<p>A mentionner :</p>
<p>Année – mois – (pays) ville – nom de l’école et type d’établissement (lycée, école, université) – département ou formation suivie – mention : - entrée, obtention du diplôme,</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  fin de la formation
Ex : 国立東京大学 XX 学部</p>
<p>Mentionner tous les établissements que vous avez fréquentés. Consacrer une ligne pour la date d’entrée, une ligne pour la date de sortie (obtention du diplôme). Les Japonais commencent généralement par la fin d’école primaire ou de collège, mais il est possible de commencer par l’année d’obtention du baccalauréat (fin de lycée) :</p>
<ul class="spip">
<li>職歴 (Parcours professionnel) : sauter une ligne et écrire「職歴」au milieu de la ligne suivante.<br class="manualbr">Cette partie concerne votre parcours professionnel à partir de la date de diplôme de fin d’études.<br class="manualbr">Cependant, les jeunes diplômés sans expérience professionnelle peuvent y indiquer les « arubaito » et autres stages qu’ils ont faits. A mentionner :</li>
<li>Année – mois – nom de la société – service d’affectation – poste – entrée en fonction.</li></ul>
<p>Il est possible de décrire brièvement votre poste sur les lignes suivantes :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Année – mois – raison du départ de la société ou mention « toujours en poste à ce jour ». On écrit « 以上 » (terminé) une ou deux ligne plus bas pour montrer que cette partie est complète.</p>
<h5 class="spip">Partie 3 : attestations, certificats, permis (免許・資格)</h5>
<p>Indiquer tous les certificats obtenus (permis de conduire, attestations en langues, autres certificats – comptabilité, informatique…), dans l’ordre chronologique.<br class="manualbr">普通自動車第一種運転免許取得 : (Obtention du permis de conduire catégorie 1)<br class="manualbr">日本語能力試験一級合格 : Réussite du JLPT 1<br class="manualbr">TOEIC 990 点取得 : Obtention de 990 points au TOEIC<br class="manualbr">取得に向けて現在勉強中」 : Etudie actuellement pour obtenir le…</p>
<h5 class="spip">Partie 4 : motivations</h5>
<p>Les parties suivantes varient selon le type d’expérience. Il s’agit de se présenter de manière générale.</p>
<p>特技・趣味・得意科目など : Décliner ses points forts, centres d’intérêt, spécialités…<br class="autobr">志望の動機 : Présenter brièvement ses motivations en se « vendant »<br class="autobr">Pour les étudiants, qui n’ont pas d’expérience professionnelle, et pour qui le rirekisho sera le seul document de candidature, les parties seront plus détaillées. Par exemple :</p>
<ul class="spip">
<li>志望動機 : Motivations</li>
<li>得意な科目および研究課題（学業で力を注いだこと） : Etudes : spécialités, sujet de recherche</li>
<li>学業以外で学生時代に力を入れたこと : Activités estudiantines : club, sport, etc.</li>
<li>趣味・特技 : Centres d’intérêt, compétences spéciales</li>
<li>私の長所・自己PR（セールスポイント) : Présentation de ses points forts (il s’agit de votre « argumentaire de vente »)</li></ul>
<h5 class="spip">Partie 5 : autres informations personnelles</h5>
<p>Il peut s’agir d’informations concernant la gare la plus proche, le temps de trajet, la famille à charge ou sa situation maritale.</p>
<h5 class="spip">Partie 6 : désirs particuliers concernant le travail</h5>
<p>On peut indiquer dans cette partie si on a des exigences particulières en matière de poste, salaire, lieu de travail, mobilité, date de début, etc.</p>
<p>Le Shokumukeirekisho ou « historique des expériences professionnelles ».</p>
<p>Le shokumukeirekisho accompagne toujours le rirekisho . Le shokumukeirekisho ressemble plus à un CV classique occidental, à la différence près qu’il est exclusivement consacré à la carrière professionnelle.<br class="manualbr">De format libre, il se fait de préférence sur ordinateur (Word, feuille A4). Il comporte deux parties : la carrière professionnelle (une à deux pages) et la présentation de ses motivations.</p>
<p>Attention à rester synthétique, et ne pas dépasser quatre pages (pour un candidat très expérimenté).</p>
<p>C’est un document important, qui vous permet de présenter vos expériences professionnelles et les domaines dans lesquels vous avez une expertise professionnelle. Il sert également à renseigner le recruteur sur votre personnalité et vos aptitudes pour le poste.</p>
<p>Les rubriques (à titre indicatif)</p>
<p>Pour une candidature dans une société japonaise, il est d’usage de suivre l’ordre chronologique (du plus ancien au plus récent). Pour une candidature dans une société étrangère, on suit l’ordre chronologique inversé (du plus récent au plus ancien).<br class="autobr">Il y a deux sortes de shokumukeirekisho :</p>
<p><strong>1) 編年体形式 (CV chronologique)</strong><br class="autobr">Les rubriques :</p>
<ul class="spip">
<li>職務概要 (Résumé de la carrière)</li>
<li>要約 (Détail des expériences professionnelles)</li>
<li>期間 (Langues, informatiques, autres compétences)</li>
<li>業務内容 (domaine, nom du projet,contenu de la mission)</li>
<li>開発環境 (environnement)</li>
<li>メンバー数と役割 (fonction, nombre de personnes dans l’équipe)</li>
<li>保有スキル (Autres compétences)</li>
<li>業務内容 PR (Points forts)</li></ul>
<p><strong>2) (CV thématique, par projets - adapté aux professions IT par exemple)</strong><br class="autobr">Les rubriques :</p>
<ul class="spip">
<li>職務概要 (Résumé de la carrière)</li>
<li>要約 (Détail) – sous forme de tableau présentant :
<br>— 期間 (périodes)
<br>— 業務内容 (domaine, nom du projet,contenu de la mission)
<br>— 開発環境 (environnement)
<br>— メンバー数と役割 (fonction, nombre de personnes dans l’équipe)</li>
<li>保有スキル (Autres compétences)</li>
<li>自己 PR (Points forts)</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Modèles de CV</h3>
<h4 class="spip">Exemple de shokumukeirekisho</h4>
<p>Date de rédaction (pour les dates, on peut utiliser le calendrier grégorien). Prénom et nom.</p>
<p><strong>Partie 1 : résumé de la carrière 職務要約</strong></p>
<p>Mentionner toutes ses expériences professionnelles<br class="autobr">Date de début – date de fin : société, poste</p>
<p><strong>Partie 2 : détail des expériences professionnelles (職務経歴)</strong></p>
<p>Indiquer pour chaque expérience :</p>
<ul class="spip">
<li>Informations sur la société (企業情報)</li>
<li>Nom de la société (企業名),</li>
<li>Dates de début et de fin (入社年月・退社年月)</li>
<li>Capital (資本金),</li>
<li>Ventes (売上げ),</li>
<li>Nombre d’employés (従業員数),</li>
<li>Si cotée en bourse ou pas (上場区別)</li>
<li>Secteur d’activité (事業内容)</li>
<li>Informations sur votre poste (業務情報)</li>
<li>Descriptif concret de votre travail (具体的に仕事の内容)</li>
<li>Vos résultats (成果)</li></ul>
<p><strong>Partie 3 : Langues et informatiques, autres compétences</strong> (経験・知識・能力の要約)</p>
<ul class="spip">
<li>取得資格等</li>
<li>得意分野/スキル</li></ul>
<p><strong>Partie 4 : Points forts</strong> (自己PR)</p>
<p>C’est la « lettre de motivation » du candidat, qui sert à mettre en avant ses points forts(en se basant sur ses expériences et résultats) et à expliquer ce qu’il veut faire.</p>
<p>Il faut utiliser ses propres mots (éviter les formules toutes faites), le but de l’exercice étant de séduire le recruteur et de le convaincre de ses compétences et motivations pour occuper le poste.</p>
<p>A titre indicatif, un plan de 自己PR pourra être :<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> 自己紹介 Introduction<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> アピールポイント Mettre en avant ses points forts, en illustrant avec des exemples tirés de votre expérience<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> スキル  Vos compétences, votre personnalité (toujours en relation avec le poste)</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Ce que vous voulez faire (désir d’évolution)</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/emploi-stage/article/curriculum-vitae-109847). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
