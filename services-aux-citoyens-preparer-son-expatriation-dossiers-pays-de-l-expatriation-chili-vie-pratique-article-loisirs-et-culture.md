# Loisirs et culture

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/vie-pratique/article/loisirs-et-culture#sommaire_1">Tourisme</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/vie-pratique/article/loisirs-et-culture#sommaire_2">Activités culturelles</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/vie-pratique/article/loisirs-et-culture#sommaire_3">Sports</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/vie-pratique/article/loisirs-et-culture#sommaire_4">Télévision – Radio</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/vie-pratique/article/loisirs-et-culture#sommaire_5">Presse française</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Tourisme</h3>
<p>Le Chili est un pays de contrastes qui offre une grande variété de paysages et d’activités.</p>
<p>Les principales régions touristiques sont les déserts du nord (Atacama), la région des lacs, la route australe, le parc national de Torres del Paine à l’extrême sud, les îles de Chiloé et de Pâques. Ces sites touristiques sont bien conservés.</p>
<p>Pour plus d’informations, vous pouvez consulter les sites internet suivants :</p>
<ul class="spip">
<li><a href="http://www.sernatur.cl/" class="spip_out" rel="external">www.sernatur.cl</a></li>
<li><a href="http://www.routard.com/guide/code_dest/chili.htm" class="spip_out" rel="external">www.routard.com</a></li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Activités culturelles</h3>
<h4 class="spip">Activités culturelles en français</h4>
<p>Une fois par an, généralement au mois d’octobre, la Chambre de commerce franco-chilienne et l’Ambassade de France au Chili organisent la <a href="http://www.semanafrancesa.cl/" class="spip_out" rel="external">Semaine Française au Chili</a>. De nombreuses activités culturelles et commerciales sont proposées avec pour objectif la diffusion de la culture française et la promotion de la marque France au Chili.</p>
<p>D’autre part, des activités culturelles françaises (expositions, concerts, conférences) sont organisées régulièrement à Santiago et dans les principales villes de province (Vina del Mar/Valparaiso, Concepcion, Osorno) par les instituts de l’Alliance française. Une centaine de manifestations se déroulent ainsi chaque année à Santiago et en province.</p>
<p>Le cinéma <a href="http://www.transeuropafilms.cl/" class="spip_out" rel="external">El Biógrafo</a> (Jose Victorino Lastaria 181, Santiago centro), diffuse régulièrement des films français sous-titrés.</p>
<p>Un programme des activités culturelles en français ou en relation avec la France est disponible via l’Institut franco-chilien.</p>
<p><strong>Institut Franco-Chilien</strong><br class="manualbr">Tél. : (562) 470 80 60 <br class="manualbr">Télécopie : (562) 470 80 90<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/vie-pratique/article/loisirs-et-culture#france#mc#netline.cl#" title="france..åt..netline.cl" onclick="location.href=mc_lancerlien('france','netline.cl'); return false;" class="spip_mail">Courriel</a></p>
<h4 class="spip">Activités culturelles locales</h4>
<p>Le théâtre au Chili présente des pièces d’origine chilienne. Il n’est accessible que si l’on maîtrise bien la langue.</p>
<p>Le <strong>festival de la Chanson de Viña Del Mar </strong>en février est renommé internationalement.</p>
<p>De nombreuses salles de cinéma offrent, pour les plus anciennes, un confort moyen et excellent dans les grands centres commerciaux plus récents. Les salles commerciales programment généralement des films nord-américains.</p>
<h3 class="spip"><a id="sommaire_3"></a>Sports</h3>
<p>Tous les sports peuvent être pratiqués au Chili.</p>
<p>On trouve à Santiago de nombreux clubs de sport. Les tarifs d’inscription sont cependant assez élevés. On peut se procurer les équipements nécessaires sur place. De nombreux clubs de remise en forme, aérobic et musculation existent dans les quartiers résidentiels.</p>
<p>Le sport national est bien évidemment le football. Il est souvent un facteur d’intégration chez les hommes chiliens ! Les trois principaux clubs sont :</p>
<ul class="spip">
<li><i>Colo Colo</i> (l’équipe la plus populaire et la plus ancienne),</li>
<li><i>Universidad de Chile </i>(la U)</li>
<li><i>Universidad Católica</i>.</li></ul>
<p>Il est à noter que la <a href="http://www.ligainternacional.cl/" class="spip_out" rel="external">Liga Internacional</a>) rassemble différentes équipes dans un championnat amateur, dont l’équipe « Francia » dans laquelle jouent plusieurs Français.</p>
<p>Le rodéo est également très pratiqué dans les campagnes. De nombreuses démonstrations sont effectuées à l’occasion de la fête nationale les 18 et 19 septembre.</p>
<p>Les eaux du Pacifique sont en général assez froides (le courant de Humboldt remonte depuis le pôle sud le long des côtes chiliennes). De grandes différences de températures de l’eau sont observées entre le nord et le sud. En raison de nombreux rouleaux, il est recommandé de se baigner aux abords des plages réputées non dangereuses ou soumises à une stricte surveillance.</p>
<p>Il existe de nombreuses plages pour la pratique du surf. La plage de Pichilemu (<a href="http://www.pichilemu.cl/" class="spip_out" rel="external">www.pichilemu.cl</a>) au Sud de Valparaiso est réputée pour sa compétition internationale de surf.</p>
<p>Il est possible de pratiquer le ski dans les stations proches de la capitale (Colorado, Valle Nevado). Les prix restent cependant assez élevés. Il faut compter 70 euros la journée par personne pour un forfait avec location de skis.</p>
<p>La chasse est autorisée du 1er avril au 1er août. Un permis est obligatoire. L’arme devra être déclarée à l’arrivée dans le pays et sera gardée à l’aéroport contre un reçu en attendant son enregistrement. Pour toute information complémentaire, vous devez vous adresser au <a href="http://historico.sag.gob.cl/OpenNet/asp/default.asp?boton=Hom" class="spip_out" rel="external">Servicio Agricola y Ganadero</a>.</p>
<p>La pêche sportive nécessite également l’obtention d’un permis qui diffère selon que son titulaire est résident ou pas. Pour l’obtenir, vous devez vous adresser au <a href="http://www.sernapesca.cl/" class="spip_out" rel="external">Service national de la pêche</a>. La période de pêche autorisée varie selon les espèces.</p>
<h3 class="spip"><a id="sommaire_4"></a>Télévision – Radio</h3>
<h4 class="spip">Télévision</h4>
<p>Le Chili dispose d’une offre télévisuelle gratuite de sept chaînes. Les deux principales chaînes sont TVN (Télévision Nationale) et Canal 13 (Universidad Cátolica).</p>
<p>Il est possible de regarder des programmes français et québécois sur le câble (<a href="http://www.tv5.org/TV5Site/programmes/accueil_continent.php" class="spip_out" rel="external">TV5</a>).</p>
<p>Il n’existe pas de taxe de type « redevance audiovisuelle » au Chili.</p>
<h4 class="spip">Radio</h4>
<p>Radio France Internationale offre des retransmissions (quelques émissions) via des radios FM locales : à Santiago : Radio Bio Bio (98.1 FM) et Radio Universidad de Chile (102.5 FM)</p>
<p>Toutes les fréquences de diffusion au Chili sont disponibles sur le site de <a href="http://www.rfi.fr/" class="spip_out" rel="external">Radio France International</a>, rubrique « capter RFI ».</p>
<h3 class="spip"><a id="sommaire_5"></a>Presse française</h3>
<p>Un grand nombre de journaux et revues français sont disponibles : Le Monde, Le Figaro, L’Express, Le Point, Le Nouvel Observateur, magasines d’histoire, d’art, etc. Dans les autres villes du Chili, il est recommandé de souscrire un abonnement.</p>
<p>La presse française est consultable gratuitement à l’Institut franco-chilien.</p>
<p>A noter : un français expatrié au Mexique a créé le site <a href="http://www.lepetitjournal.com/" class="spip_out" rel="external">Le Petit Journal</a> sur lequel vous trouverez des articles généraux sur l’actualité française et une section consacrée à certaines villes à l’étranger. Santiago en fait partie.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/vie-pratique/article/loisirs-et-culture). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
