# Entretien d’embauche

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/emploi-stage/article/entretien-d-embauche-114433#sommaire_1">Conduite de l’entretien</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/emploi-stage/article/entretien-d-embauche-114433#sommaire_2">Apparence et attitude</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/emploi-stage/article/entretien-d-embauche-114433#sommaire_3">A préparer</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/emploi-stage/article/entretien-d-embauche-114433#sommaire_4">Après l’entretien</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Conduite de l’entretien</h3>
<p>L’entretien peut être mené par des professionnels du recrutement comme par des responsables opérationnels. Il peut être très structuré comme assez informel. Il peut parfois être conduit par plusieurs personnes.</p>
<p>L’entretien est donc avant tout une mise en situation professionnelle où il s’agit de démontrer ses facultés d’adaptation et de connexion avec des interlocuteurs que l’on découvre généralement pour la première fois.</p>
<p>Il permet également, de part et d’autre, pour le recruteur comme pour le candidat, de vérifier l’adéquation profil de poste – profil de candidat.</p>
<h3 class="spip"><a id="sommaire_2"></a>Apparence et attitude</h3>
<p>La communication non verbale et para verbale est d’une importance capitale en Thaïlande et plus généralement en Asie. Appréhender et comprendre un minimum les codes de communication propres au pays permet de tirer le meilleur partie de cette mise en situation.</p>
<h3 class="spip"><a id="sommaire_3"></a>A préparer</h3>
<ul class="spip">
<li>Savoir exposer son projet professionnel en relation avec son projet de vie en Thaïlande</li>
<li>Savoir définir son « offre de services » apportant une plus-value aux problématiques locales tout en étant réaliste par rapport aux besoins et nécessités des entreprises</li>
<li>Savoir parler de ses compétences et réalisations professionnelles, en identifiant quelles sont celles qui font défaut sur le marché de l’emploi local</li>
<li>Recueillir en amont et pendant l’entretien le plus possible d’informations sur l’entreprise, le poste, le recruteur afin d’adapter au mieux son argumentaire</li>
<li>Une liste de questions à poser pendant l’entretien</li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Après l’entretien</h3>
<p>L’entretien permet d’établir un premier contact personnel avec un représentant de l’entreprise. Les processus de décision peuvent être longs et nécessiter la rencontre de plusieurs décisionnaires. Il s’agit donc de construire et nourrir la relation avec l’entreprise tout au long du processus de recrutement.</p>
<p>Remercier le recruteur 24 h après l’entretien, récapituler les points positifs de l’entretien et de sa motivation, le tenir informé de l’évolution de sa réflexion, procurer une information qui peut l’intéresser, mettre en relation l’entreprise avec un client ou un fournisseur potentiel, sont autant de moyens pour garder le contact et faire part de sa motivation.</p>
<p><i>Mise à jour : juillet 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/emploi-stage/article/entretien-d-embauche-114433). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
