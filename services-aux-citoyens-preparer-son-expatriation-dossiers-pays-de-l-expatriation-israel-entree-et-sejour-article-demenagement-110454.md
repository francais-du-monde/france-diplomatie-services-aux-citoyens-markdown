# Déménagement

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/entree-et-sejour/article/demenagement-110454#sommaire_1">Formalités douanières</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Formalités douanières</h3>
<p>C’est le service de l’import personnel des douanes israéliennes qui définit les différentes catégories d’importateurs et les droits qui leur sont éventuellement accordés, en fonction de leur statut administratif (nouvel immigrant, citoyen de retour, de longue date, touriste ou diplomate) et des effets importés. Dans tous les cas de figure, votre déménagement est considéré comme un import par les douanes israéliennes. Tous les effets personnels importés en Israël sont soumis à une procédure de dédouanement (pas uniquement les effets neufs et les appareils électroménagers)</p>
<p>Pour les délais moyens d’acheminement, il faut compter de 15 jours à 4 semaines pour un trajet France-Israël. Pour les déménagements maritimes soyez vigilants car un dédouanement ne pourra pas être effectué pendant les périodes de fêtes juives. Toute immobilisation d’un container resté à quai plus de quatre jours est à la charge du client.</p>
<p>Il existe des transporteurs spécialisés pour Israël, les plus connus sont :</p>
<ul class="spip">
<li>Ocean Company Ltd, Yakum Post 60972 Israel <br class="manualbr">Tél. +972 9 9553456 <br class="manualbr">Fax +972 9 9514321</li>
<li>Sonigo International Ltd, 53 <br class="manualbr">Hakidma St., Northern Industrial Area, PO Box 4068, <br class="manualbr">77521 Ashdod, Israel <br class="manualbr">Tél. : +972 8 6111-222 <br class="manualbr">Fax : +972 8 6111-223</li>
<li>Globus Israël, <br class="manualbr">Habosem 7 st. Industrial Zone <br class="manualbr">Ashdod Israël <br class="manualbr">Tél. : + 972 8 9324222</li></ul>
<p>Il est généralement conseillé de consulter, avant le départ, la dernière réglementation douanière auprès du bureau de douane du pays ou sur le <a href="http://ozar.mof.gov.il/customs/eng/mainpage.htm%20" class="spip_out" rel="external">site de l’autorité israélienne en matière de taxes et de douanes</a> (rubrique "personal import taxes").</p>
<p>Vous trouverez de plus amples informations sur les procédures douanières dans notre rubrique thématique <a href="services-aux-citoyens-preparer-son-expatriation-douanes.md" class="spip_in">Douanes</a>.</p>
<p>S’agissant d’un déménagement international, il est primordial de s’entourer d’un maximum de garanties en faisant appel à un professionnel disposant de certifications reconnues (marque NF Service, ISO 9002, etc.).</p>
<p><a href="http://www.csdemenagement.fr/" class="spip_out" rel="external">Chambre syndicale du déménagement</a> <br class="manualbr">Téléphone : 01 49 88 61 40 <br class="manualbr">Télécopie : 01 49 88 61 46<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/entree-et-sejour/article/demenagement-110454#contact#mc#csdemenagement.fr#" title="contact..åt..csdemenagement.fr" onclick="location.href=mc_lancerlien('contact','csdemenagement.fr'); return false;" class="spip_mail">Courriel</a></p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/entree-et-sejour/article/demenagement-110454). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
