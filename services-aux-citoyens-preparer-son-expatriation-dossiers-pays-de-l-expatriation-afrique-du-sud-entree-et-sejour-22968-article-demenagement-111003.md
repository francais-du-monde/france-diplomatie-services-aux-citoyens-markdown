# Déménagement

<p>Lors d’un déménagement par bateau ou par avion, le connaissement ou la LTA doivent bien préciser la destination finale. Pour le dédouanement, un expatrié doit fournir une copie de son passeport et de son permis de travail.</p>
<ul class="spip">
<li><strong>Fret aérien : environ 7 jours </strong></li>
<li><strong>Fret maritime : environ 30 jours</strong></li></ul>
<p>Il est impératif de disposer de sa carte d’accréditation avant d’entamer toute démarche relative au déménagement. Aussi, pour éviter des frais de stockage inutiles, il est important de bien prendre en considération le délai d’obtention de la carte d’accréditation pour programmer le départ du container (<strong>10 jours ouvrables</strong>).</p>
<p>Le dédouanement des effets personnels se fait par l’obtention d’un certificat de dédouanement (<strong>certificat A ou B</strong>) délivré par le DIRCO. Les documents à fournir pour obtenir le certificat de dédouanement sont : <i>Bill of Entry</i> (déclaration d’entrée) et l’inventaire. Le délai pour l’obtention du certificat <strong>A</strong> ou <strong>B</strong> est de trois jours ouvrables.</p>
<p>Le dédouanement d’un véhicule nécessitant des délais plus important, il est fortement déconseillé d’importer son véhicule dans le même container.</p>
<p><strong>NB</strong> : l’importation d’alcool donne lieu à la facturation de droits de douanes car elle n’est pas comptabilisée dans les quotas réservés pour l’ambassade ou les consulats.</p>
<p>Pour en savoir plus, vous pouvez consulter l’article thématique <a href="services-aux-citoyens-preparer-son-expatriation-douanes.md" class="spip_in">Douanes</a>.</p>
<p>S’agissant d’un déménagement international, il est primordial de s’entourer d’un maximum de garanties en faisant appel à un professionnel disposant de certifications reconnues (marque NF Service, ISO 9002, etc.)</p>
<p><a href="http://www.csdemenagement.fr/" class="spip_out" rel="external">Chambre syndicale du déménagement</a><br class="manualbr">Téléphone : 01 49 88 61 40 <br class="manualbr">Télécopie : 01 49 88 61 46<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/entree-et-sejour-22968/article/demenagement-111003#contact#mc#csdemenagement.fr#" title="contact..åt..csdemenagement.fr" onclick="location.href=mc_lancerlien('contact','csdemenagement.fr'); return false;" class="spip_mail">Courriel</a></p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/entree-et-sejour-22968/article/demenagement-111003). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
