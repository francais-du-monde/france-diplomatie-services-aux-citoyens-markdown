# Vaccination

<p class="texte texte109692">

    Pour en savoir plus, lisez notre article thématique <a href="services-aux-citoyens-preparer-son-expatriation-sante.md" class="spip_in">Santé</a>.
</p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/entree-et-sejour-22762/article/vaccination). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
