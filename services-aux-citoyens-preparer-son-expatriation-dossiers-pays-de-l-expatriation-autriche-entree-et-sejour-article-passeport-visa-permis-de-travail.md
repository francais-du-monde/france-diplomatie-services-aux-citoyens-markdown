# Passeport, visa, permis de travail

<p>Quel que soit le motif de votre séjour, renseignez-vous auprès de la section consulaire de l’<a href="http://www.bmeia.gv.at/fr/ambassade/paris.html" class="spip_out" rel="external">ambassade d’Autriche à Paris</a> qui vous informera sur la règlementation en matière d’entrée et de séjour en Autriche, règlementation que vous devrez impérativement respecter. N’hésitez pas à consulter le site <a href="https://www.help.gv.at/Portal.Node/hlpd/public/content/k507/Seite.5070000.html" class="spip_out" rel="external">ministère de l’Intérieur autrichien</a> afin de vous familiariser avec les modalités d’entrée et de séjour dans le pays.</p>
<p>Le consulat de France en Autriche n’est pas l’organisme compétent pour répondre à vos demandes d’information concernant votre séjour en Autriche.</p>
<p><i>Les informations suivantes sont données à titre indicatif</i></p>
<p>Depuis 1995, l’Autriche est membre de l’Union européenne et de l’espace Schengen : un ressortissant français peut donc entrer en Autriche sur présentation d’une carte d’identité française ou d’un passeport en cours de validité. Il n’a plus besoin de visa de séjour ni de permis de travail.</p>
<p>Si vous résidez <strong>plus de trois mois</strong> en Autriche, vous devez cependant effectuer les formalités suivantes.</p>
<h4 class="spip">Déclaration de résidence (<i>Meldezettel</i>)</h4>
<p>Vous devez, <strong>dans les trois jours</strong> suivant votre arrivée en Autriche, remplir une déclaration de résidence, le <i>Meldezettel</i>, auprès des services domiciliaires de la mairie, <i>Meldeamt</i> ou <i>Meldeservice</i>. Cette déclaration est obligatoire.</p>
<p>Ce formulaire d’immatriculation (<i>Meldezettel</i>) peut être obtenu gratuitement à la commune, acheté dans un bureau de tabac (<i>Trafik</i>) ou <a href="http://www.help.gv.at/Content.Node/documents/meldez.pdf" class="spip_out" rel="external">téléchargé gratuitement en ligne</a>.</p>
<p>Pour en savoir plus :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Consultez le portail <a href="https://www.help.gv.at/Portal.Node/hlpd/public/content/118/Seite.1180200.html" class="spip_out" rel="external">Help.gv.at</a> </p>
<p>Ce document sera demandé pour toutes les démarches en Autriche, y compris par les employeurs au moment de l’embauche.</p>
<h4 class="spip">Certificat d’inscription (<i>Anmeldebescheinigung</i>)</h4>
<p>Si vous vous établissez pour une longue durée en Autriche, il faut, de plus, demander un certificat d’inscription (<i>Meldebestätigung</i> ou <a href="https://www.help.gv.at/Portal.Node/hlpd/public/content/12/Seite.120810.html" class="spip_out" rel="external">Anmeldebescheinigung</a>) <strong>dans les quatre mois suivant la déclaration de domicile</strong>.</p>
<p>Ce document d’une validité illimitée et d’un coût d’environ 55 Euros est à demander auprès de la direction municipale compétente : <a href="https://www.wien.gv.at/amtshelfer/dokumente/aufenthalt/daueraufenthalt/bescheinigungen/anmeldebescheinigung.html" class="spip_out" rel="external">Magistratsabteilung 35</a>. Si vous ne résidez pas à Vienne, adressez-vous à la <i>Bezirkshauptmannschaft</i> ou à la mairie de votre domicile.</p>
<p>Vous devez également signaler <strong>dans un délai d’un mois</strong> auprès du <i>Meldeservice</i> tout changement survenu à l’étranger vous concernant (changement de nom, de situation familiale, etc.). De même, en cas de changement d’adresse ou de départ définitif du pays, vous devrez faire enregistrer ce changement <strong>dans les trois jours</strong> auprès des services domiciliaires de la mairie.</p>
<p><strong>Le non respect de ces formalités est passible d’une très forte amende. </strong></p>
<h4 class="spip">Carte de ressortissant de l’E.E.E (<i>Lichtbildausweis für EWR-Bürger</i>) et carte de séjour (<i>Daueraufenhaltskarte</i>) </h4>
<p>Les Français désirant s’installer en Autriche peuvent solliciter auprès du « magistrat » ou de la <i>Bezirkshauptmannschaft</i> la délivrance d’une <strong>carte de ressortissant de l’Espace Economique européen</strong> (<i>Lichtbildausweis für EWR-Bürger</i>). Cette carte n’est plus obligatoire, mais il est toutefois conseillé d’en faire une demande pour faciliter diverses formalités par la suite. Une <strong>carte de séjour</strong> (<a href="https://www.help.gv.at/Portal.Node/hlpd/public/content/12/Seite.120800.html" class="spip_out" rel="external">Daueraufenthaltskarte</a>) pour membres de famille de citoyen(ne)s de l’EEE et de citoyen(ne)s suisses peut également être sollicitée.</p>
<p>Pour en savoir plus :</p>
<ul class="spip">
<li><a href="https://www.help.gv.at/Portal.Node/hlpd/public/content/118/Seite.1180000.html" class="spip_out" rel="external">Portail de l’administration autrichienne</a> : inscription et désinscription de résidence ;</li>
<li><a href="http://www.bmi.gv.at/cms/BMI_ZMR/" class="spip_out" rel="external">Zentrales Melderegister</a> ;</li>
<li><a href="http://ec.europa.eu/youreurope/" class="spip_out" rel="external">Aide et conseils pour les citoyens de l’UE et leur famille</a> ;</li>
<li><a href="http://europa.eu/" class="spip_out" rel="external">Site de l’Union européenne</a>.</li></ul>
<p><i>Mise à jour : février 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/entree-et-sejour/article/passeport-visa-permis-de-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
