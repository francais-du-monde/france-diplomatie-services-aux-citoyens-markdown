# Logement

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/vie-pratique/article/logement-111322#sommaire_1">Où se loger ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/vie-pratique/article/logement-111322#sommaire_2">Conditions de location</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/vie-pratique/article/logement-111322#sommaire_3">Hôtels</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/vie-pratique/article/logement-111322#sommaire_4">Electricité</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/vie-pratique/article/logement-111322#sommaire_5">Electroménager</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Où se loger ?</h3>
<p><strong>Exemples de coût de logement (loyers mensuels)</strong></p>
<p>Le loyer d’une maison revient à peu près au même que celui d’un appartement de bon standing.</p>
<p>Cependant, en choisissant d’habiter une maison, il faudra compter en sus les frais de gardiennage, et des charges plus lourdes.</p>
<p>Un gardien privé employé à temps complet coûte environ 7000 pesos par mois et une société de gardiennage environ 9000 à 15 000 pesos par mois.</p>
<p>En outre, les quartiers où l’on trouve des maisons sont parfois un peu excentrés (Arroyo Hondo, Costa Verde).</p>
<p>Il existe des immeubles récents, de belle facture, dont les finitions ne sont pas toujours impeccables, dans tous les « bons » quartiers de Saint-Domingue : la Zone Coloniale, Gazcue, Piantini, Bella Vista, Los Cacicazgos …</p>
<p>Il y a souvent des places de parking, ainsi qu’une « area social », zone de convivialité où l’on peut trouver une salle de sport, une piscine, une aire de jeux pour les enfants, un barbecue …</p>
<p>Dans ces quartiers, au 1er janvier 2014, on observe les prix suivants (en euros) :</p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Type de logement</td>
<td>Surface</td>
<td>Loyer - bons quartiers</td>
<td>Loyer - très bons quartiers</td></tr>
<tr class="row_even even">
<td>Maison meublée 3 chambres</td>
<td>200 m<sup>2</sup></td>
<td>3700</td>
<td>5180</td></tr>
<tr class="row_odd odd">
<td>Maison non meublée 3 chambres</td>
<td>200 m<sup>2</sup></td>
<td>2220</td>
<td>2960</td></tr>
<tr class="row_even even">
<td>Maison non meublée 4 chambres</td>
<td>250 m<sup>2</sup></td>
<td>1850</td>
<td>3700</td></tr>
<tr class="row_odd odd">
<td>Appartement meublé 2 chambres</td>
<td>80-120 m<sup>2</sup></td>
<td>1480</td>
<td>2220</td></tr>
<tr class="row_even even">
<td>Appartement meublé 3 chambres</td>
<td>120-160 m<sup>2</sup></td>
<td>1850</td>
<td>2590</td></tr>
<tr class="row_odd odd">
<td>Appartement non meublé 1 chambres</td>
<td>50-70 m<sup>2</sup></td>
<td>-</td>
<td>-</td></tr>
<tr class="row_even even">
<td>Appartement non meublé 2 chambres</td>
<td>80-120 m<sup>2</sup></td>
<td>1184</td>
<td>1480</td></tr>
<tr class="row_odd odd">
<td>Appartement non meublé 3 chambres</td>
<td>120-160 m<sup>2</sup></td>
<td>1480</td>
<td>2220</td></tr>
<tr class="row_even even">
<td>Appartement non meublé 4 chambres</td>
<td>160-200 m<sup>2</sup></td>
<td>1850</td>
<td>2590</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_2"></a>Conditions de location</h3>
<p>Les quartiers résidentiels se situent à l’ouest et au nord-ouest de Saint-Domingue. Ils repoussent l’habitat pauvre à la périphérie de la ville. Ils s’étendent dans la plupart des zones de Saint-Domingue, qui est une ville en chantier perpétuel. Certains îlots populaires subsistent.</p>
<p><strong>Recherche d’un logement</strong></p>
<p>Si l’offre peut sembler importante (petites annonces à foison, agences immobilières), il reste relativement difficile de trouver à Saint-Domingue un logement réunissant tous les critères d’un expatrié. Souvent, il faudra faire des concessions (nombre de chambres, qualité du mobilier pour les meublés, présence ou non d’une piscine …), la sécurité devant être le point non négociable.</p>
<p>Le prix des loyers est fixé en USD mais également payable localement en pesos dominicains.</p>
<p>Deux mois de caution sont exigés et les baux sont généralement d’un an, renouvelable. Le loyer se paie d’avance, mensuellement. Certaines agences immobilières demandent parfois un mois de loyer au titre de leurs honoraires (même si en principe c’est au loueur que revient de payer les frais d’agence). Il est nécessaire de procéder à un état des lieux avec le propriétaire.</p>
<p>Les charges comprennent, le cas échéant, le gardiennage, l’ascenseur, le ramassage des ordures, le nettoyage des parties communes, l’entretien de la piscine, l’eau et le gaz. Le montant mensuel est généralement inclus dans le loyer.</p>
<p>Il convient d’ajouter que les prix ci-dessus ne garantissent pas des logements de qualité, relativement rares sur le marché immobilier de Saint-Domingue : problèmes de finitions, d’infiltration, matériels défectueux, fortes nuisances sonores, vis-à-vis entre immeubles voisins.</p>
<h3 class="spip"><a id="sommaire_3"></a>Hôtels</h3>
<p><strong>Exemples de prix moyen d’une chambre d’hôtel dans la Zone coloniale</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Prix moyen d’une chambre d’hôtel</strong> (chambre double)</td>
<td><strong>Euros</strong></td></tr>
<tr class="row_even even">
<td>Luxe (MGallery)</td>
<td>150 €</td></tr>
<tr class="row_odd odd">
<td>Moyen tourisme</td>
<td>80 €</td></tr>
<tr class="row_even even">
<td>Type « Pension »</td>
<td>50 €</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_4"></a>Electricité</h3>
<p>Le courant est alternatif en 110 volts (50 Hz), mais il est possible de faire installer par un électricien quelques prises en 220 volts (60 Hz) pour les appareils ménagers importés et les climatiseurs. Tous les appareils disponibles sur place sont équipés en 110 volts (prises américaines).</p>
<p>L’électricité coûte cher, surtout si l’on utilise beaucoup l’air conditionné, et les prix sont variables selon les quartiers.</p>
<p><strong>Exemples</strong> :</p>
<ul class="spip">
<li>Pour un appartement de 4 chambres dans le quartier de Los Cacicazgos, on doit compter environ 15 000 DOP par mois (262,50 € au taux de janvier 2014)</li>
<li>Pour une maison à Costa Verde, on peut atteindre 30 000 DOP (525 €) à certaines périodes de l’année.</li></ul>
<p>Les coupures d’électricité étant relativement nombreuses à Saint-Domingue, il faut veiller à ce que le logement soit équipé d’un groupe électrogène (<i>planta full</i>) ou d’inverseurs prenant le relais en cas de coupure.</p>
<h3 class="spip"><a id="sommaire_5"></a>Electroménager</h3>
<p>On peut trouver sur place tout l’électroménager (petit et grand), aux normes américaines le plus souvent.</p>
<p>Les appareils de type européen ne fonctionnent pas forcément, même branchés sur des prises en 220 volts, et même munis de transformateurs de courant.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/vie-pratique/article/logement-111322). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
