# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/vie-pratique/article/cout-de-la-vie#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/vie-pratique/article/cout-de-la-vie#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/vie-pratique/article/cout-de-la-vie#sommaire_3">Alimentation</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/vie-pratique/article/cout-de-la-vie#sommaire_4">Evolution des prix</a></li></ul>
<p>Le coût de la vie au Danemark est d’environ 30% supérieur à celui en France.</p>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>En septembre 2000, la population danoise a décidé par référendum de ne pas adopter l’euro dans le cadre de l’Union économique et monétaire (UEM) et de conserver la monnaie nationale : la couronne danoise (DKK).</p>
<p>Au 5 janvier 2014, une couronne équivaut à 0,134 euro et un euro équivaut à 7,45 DKK.</p>
<p>La couronne danoise est convertible et la réglementation des changes n’impose pas de mesures particulières. Le paiement est libéralisé depuis 1998. Les meilleures monnaies de facturation sont la couronne danoise, le dollar américain et l’euro.</p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<p>Les Français expatriés peuvent aisément procéder aux transferts de fonds. Aucune déclaration n’est nécessaire.</p>
<p>Il n’y a pas de banques françaises sur place, toutefois des accords de partenariat existent entre banques françaises et locales ; quelques banques étrangères (nordiques et anglo-saxonnes) sont présentes.</p>
<p>Danske bank et Nordea sont les deux plus importantes banques danoises.</p>
<p>Les banques sont ouvertes de 10h à 16h (jusqu’à 17h le jeudi), fermées les samedis et dimanches.</p>
<p>La Danske Bank possède une filiale au terminal 3 de l´aéroport de Copenhague, ouverte tous les jours de 6h à 20h30 (ouverture à 9h le premier et le dernier jour du mois).</p>
<p>Pour ouvrir un compte sur place, il sera nécessaire de présenter un document d’identité, un permis de résidence ainsi que votre numéro CPR (voir Formalités d’installation).</p>
<h3 class="spip"><a id="sommaire_3"></a>Alimentation</h3>
<p>Les conditions d’approvisionnement sont celles de l’ensemble des pays européens. Par ailleurs, il n’existe pas de restrictions particulières pour l’achat de vins et alcools.</p>
<h4 class="spip">Prix moyen d’un repas dans un restaurant</h4>
<p>Les tarifs des restaurants s’entendent, en règle générale, service compris.</p>
<p>Le prix moyen d’un repas est de 45 euros dans un restaurant de qualité moyenne et de 100 euros dans un restaurant de qualité supérieure.</p>
<h3 class="spip"><a id="sommaire_4"></a>Evolution des prix</h3>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/vie-pratique/article/cout-de-la-vie). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
