# Animaux domestiques

<p>Une information très détaillée est disponible sur le site Internet du <a href="http://agriculture.gouv.fr/" class="spip_out" rel="external">ministère de l’Agriculture</a>.</p>
<p>Les chiens, les chats et les furets doivent satisfaire aux conditions suivantes :</p>
<ul class="spip">
<li>être identifié par tatouage ou par puce électronique (cette puce doit être conforme à la norme ISO 11784 ou ISO 11785, sinon le propriétaire doit, lors de tout contrôle, fournir les moyens nécessaires à la lecture de la puce) ;</li>
<li>être valablement vacciné contre la rage ;</li>
<li>être titulaire d’un passeport délivré par un vétérinaire habilité attestant de l’identification et de la vaccination contre la rage de l’animal.En France comme en Pologne, la réalisation de la primo-vaccination antirabique n’est considérée comme valable qu’à partir de 21 jours après la fin du protocole de vaccination prescrit par le fabricant. La vaccination antirabique de rappel est considérée en cours de validité le jour de sa réalisation.</li></ul>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li><a href="http://www.wetgiw.gov.pl/" class="spip_out" rel="external">Glówny Inspektorat Weterynarii</a> (Inspection vétérinaire générale)</li>
<li>Notre article thématique <a href="services-aux-citoyens-preparer-son-expatriation-douanes-article-animaux-domestiques.md" class="spip_in">Animaux domestiques</a></li></ul>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/entree-et-sejour/article/animaux-domestiques-114279). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
