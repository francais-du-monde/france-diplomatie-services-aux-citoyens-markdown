# La validation des acquis de l’expérience (VAE)

<p>La validation des acquis de l’expérience (VAE) est un droit inscrit dans le Code du travail. Elle permet à toute personne, quel que soit son âge, sa nationalité, son niveau d’études et son statut, de faire reconnaître son expérience professionnelle ou liée à l’exercice de responsabilités syndicales, afin d’obtenir un diplôme, un titre ou un certificat de qualification professionnelle.</p>
<p>La seule condition requise est d’avoir exercé au moins trois ans une activité salariée, non salariée ou bénévole et en rapport avec le contenu de la certification envisagée.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Consulter le <a href="http://www.vae.gouv.fr" class="spip_out" rel="external">portail de la validation d’acquis de l’expérience</a></p>
<p>La VAE est un droit ouvert à tous (salariés, non salariés, demandeurs d’emploi, bénévoles, agents publics, personnes ayant exercé des responsabilités syndicales).</p>
<p>La VAE permet :</p>
<ul class="spip">
<li>d’obtenir, en totalité ou en partie, un des diplômes, titres ou certificats de qualification inscrits dans le répertoire national des certifications professionnelles.</li>
<li>d’accéder directement à un cursus de formation sans justifier du niveau d’études ou des diplômes et titres normalement requis.</li></ul>
<p><a href="http://www.vae.gouv.fr/?page=carte-prc" class="spip_out" rel="external">Les Points relais conseil</a> (PRC), placés sous la responsabilité des Conseils régionaux, vous accueillent et vous informent sur la démarche de VAE.</p>
<p>Ils vous aident à analyser la pertinence de votre projet VAE, à vous repérer parmi l’offre de certification (régionale et nationale), à vous orienter auprès des certificateurs concernés, à connaitre les possibilités de financement.</p>
<p>L’académie de Versailles a créé le site <a href="http://www.vaexpat.com" class="spip_out" rel="external">Vaexpat.com</a>. Il s’agit d’un dispositif d’aide à la réalisation de parcours de VAE à distance à l’attention des français expatriés amenés à travailler à l’étranger pour des organismes français ou autres, de leur conjoints, et des volontaires internationaux</p>
<p><strong>Pour plus d’informations : </strong> <br class="manualbr">Rectorat- DAFCO<br class="manualbr">19 avenue du Centre <br class="manualbr">78280 Guyancourt -BP 70101<br class="manualbr">78053 St-Quentin en Yvelines Cedex<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/emploi/article/la-validation-des-acquis-de-l-experience-vae#dafco-accueil#mc#ac-versailles.fr#" title="dafco-accueil..åt..ac-versailles.fr" onclick="location.href=mc_lancerlien('dafco-accueil','ac-versailles.fr'); return false;" class="spip_mail">dafco-accueil<span class="spancrypt"> [at] </span>ac-versailles.fr</a></p>
<p>Si vous connaissez déjà la certification que vous souhaitez obtenir par la VAE, vous pouvez vous adresser directement à l’<a href="http://www.vae.gouv.fr" class="spip_out" rel="external">organisme certificateur ou validateur concerné</a>.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Consulter le site du <a href="http://travail-emploi.gouv.fr/" class="spip_out" rel="external">ministère du Travail</a>.</p>
<p><i>Mise à jour : septembre 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/emploi/article/la-validation-des-acquis-de-l-experience-vae). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
