# Vaccination

<p>Aucune vaccination n’est exigée à l’entrée du pays pour les voyageurs venant de France métropolitaine.</p>
<p>Les vaccinations suivantes sont conseillées, pour des raisons médicales :</p>
<ul class="spip">
<li>pour les adultes, mise à jour des vaccinations contre la diphtérie, le tétanos et la poliomyélite ; vaccination contre la typhoïde, l’hépatite A et l’hépatite B.</li>
<li>pour les enfants, vaccinations recommandées en France par le ministère de la Santé, en particulier pour les longs séjours : BCG dès le premier mois, rougeole dès l’âge de neuf mois.</li></ul>
<p>Il est recommandé de mettre à jour ses vaccinations avant le départ. Il est impossible de prévoir d’éventuelles difficultés d’approvisionnement une fois sur place.</p>
<p>Pour en savoir plus, lisez notre rubrique <a href="services-aux-citoyens-preparer-son-expatriation-sante.md" class="spip_in">Santé</a>.</p>
<p><i>Mise à jour : avril 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/entree-et-sejour-23154/article/vaccination-112184). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
