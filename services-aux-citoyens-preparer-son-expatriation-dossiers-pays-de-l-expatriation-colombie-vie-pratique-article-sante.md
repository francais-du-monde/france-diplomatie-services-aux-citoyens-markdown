# Santé

<p>L’état sanitaire est satisfaisant en Colombie et il est possible de se faire soigner dans toutes les spécialités médicales. Cependant seules les plus grandes villes disposent d’infrastructures de santé capables de prendre en charge tout type d’urgence. Il s’agit souvent de cliniques privés qui n’acceptent que les personnes ayant payé d’avance. Les services de santé sont quasiment inexistants dans certaines zones du pays, les plus pauvres, hors des grands centres urbains.</p>
<p>Le système de médecine étant privé, le prix des consultations peut varier. Il faut compter entre 40 et 50€ pour une consultation chez un généraliste et de 45 à 100€ chez un spécialiste. Il est également possible de se faire soigner à domicile (entre 50 et 90€).</p>
<p>Des médecins français ou francophones exercent dans les villes principales. Il est possible de s’adresser au consulat général de la circonscription de résidence pour obtenir la liste de ces praticiens.</p>
<p><strong>Attention</strong> : avant le départ il est nécessaire de consulter son médecin et de souscrire à une compagnie d’assurance couvrant les frais médicaux et le rapatriement sanitaire.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li>fiche du <a href="http://www.cimed.org/" class="spip_out" rel="external">CIMED</a> (Comité d’informations médicales) spécifique à Bogota</li>
<li><a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/colombie/" class="spip_in">Fiche santé en Colombie dans les Conseils aux voyageurs</a></li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/vie-pratique/article/sante). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
