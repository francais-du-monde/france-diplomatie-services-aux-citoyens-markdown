# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/vie-pratique/article/cout-de-la-vie-110531#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/vie-pratique/article/cout-de-la-vie-110531#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/vie-pratique/article/cout-de-la-vie-110531#sommaire_3">Alimentation</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/vie-pratique/article/cout-de-la-vie-110531#sommaire_4">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p><strong>L’unité monétaire est le dinar tunisien (DT). </strong>Il se divise en millimes (1 DT = 1000 millimes).</p>
<p>Au 1er juillet 2013, le dinar tunisien vaut 0,482 euro c’est-à-dire qu’un euro équivaut à 2,07 dinars tunisiens.</p>
<p>Toutes les devises étrangères convertibles sont acceptées dans les nombreux bureaux de change et les distributeurs automatiques de billets sont disponibles dans la plupart des agglomérations. Les cartes de crédit internationales, acceptées dans les grands hôtels, ne sont en revanche pas utilisables dans les petits magasins et les petits restaurants.</p>
<p>On compte 20 banques de dépôts en Tunisie. Les principales banques françaises représentées dans le pays sont BNP-Paribas (UBCI), la Société Générale (UIB) et le Groupe Caisse d’épargne (BTK).</p>
<p>Pour en savoir plus : <a href="http://www.bct.gov.tn/bct/siteprod/francais/index1.jsp" class="spip_out" rel="external">site de la banque centrale de Tunisie</a></p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<p>Les transferts de fonds de la Tunisie vers la France sont possibles pour les étrangers non-résidents depuis 1987, après accord de la Banque centrale de Tunisie BCT) qui exige la production des documents suivants : quitus fiscal, preuve de l’origine des fonds, preuve de la nationalité française et attestation de résidence en France.</p>
<p>Les salariés contractuels peuvent transférer librement, sans autorisation de la BCT, 50% de leur salaire. Il convient de constituer un dossier auprès de la banque, comportant notamment une demande de transfert et une attestation de l’employeur établissant le montant des salaires versés et le montant des impôts. Les salariés souhaitant transférer des montants supérieurs à 50% doivent avoir l’autorisation de la BCT et constituer un dossier avec justificatifs.</p>
<h3 class="spip"><a id="sommaire_3"></a>Alimentation</h3>
<p>Prix moyen d’un repas dans un restaurant</p>
<ul class="spip">
<li>1ère catégorie : 40 dt</li>
<li>2ème catégorie : 20 dt</li>
<li>3ème catégorie : 10 dt</li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Evolution des prix</h3>
<p>Au terme de l’année 2012, le taux d’inflation a atteint 5,9 % en glissement annuel (contre 3,5 % en 2011). Cette hausse a concerné tous les groupes de produits, mais surtout les produits alimentaires et boissons (7,5 %), l’habillement et les chaussures (7,6 %) et le tabac (11,6 %).</p>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : novembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/vie-pratique/article/cout-de-la-vie-110531). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
