# Lettre de motivation

<p>Les candidatures sans lettre de motivation sont généralement écartées d’office. En Israël, les lettres de motivations sont généralement dactylographiées, et non manuscrites.</p>
<p>Qu’il s’agisse d’une réponse à une annonce ou d’une candidature spontanée, vous ne pourrez pas éviter l’exercice difficile et délicat de la lettre de motivation qui se doit d’accompagner systématiquement votre curriculum vitae.</p>
<p>Personnelle, soignée, percutante et pertinente, la lettre de motivation est votre première chance d’inciter le recruteur à vous rencontrer et/ou à prendre connaissance de votre CV. La lettre de motivation n’est pas une simple lettre d’accompagnement, elle doit inscrire votre candidature dans une démarche professionnelle cohérente et être unique et ciblée pour l’offre et l’entreprise que vous visez.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/emploi-stage/article/lettre-de-motivation). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
