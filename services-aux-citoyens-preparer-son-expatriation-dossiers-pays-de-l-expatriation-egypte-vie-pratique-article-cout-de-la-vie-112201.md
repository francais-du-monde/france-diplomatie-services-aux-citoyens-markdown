# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/vie-pratique/article/cout-de-la-vie-112201#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/vie-pratique/article/cout-de-la-vie-112201#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/vie-pratique/article/cout-de-la-vie-112201#sommaire_3">Alimentation</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/vie-pratique/article/cout-de-la-vie-112201#sommaire_4">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>L’unité monétaire est la livre égyptienne (EGP ou LE), <i>guineh</i> en arabe. Une livre se divise en 100 piastres.</p>
<p>Au 27 janvier 2014, une livre égyptienne vaut 0,106 euro (un euro vaut 9,44 livres egyptiennes).</p>
<p>Il n’y a pas de contrôle des changes. La livre égyptienne n’est plus liée au dollar américain depuis 2003.</p>
<p>Plusieurs banques françaises sont implantées en Egypte (BNP-Paribas, Crédit Agricole). Des banques étrangères sont également présentes, telles que American Express, Barclays bank, Saudi Bank, National bank of Abu Dhabi, City bank, etc…</p>
<p>Le paiement en espèces domine. L’usage de la carte de crédit se développe. Les chèques sont très peu utilisés.</p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<p>Il n’existe pas de dispositions locales en matière de transferts de fonds.</p>
<h3 class="spip"><a id="sommaire_3"></a>Alimentation</h3>
<p>On trouve pratiquement tout sur le marché égyptien, vins étrangers exceptés. Les produits importés sont très chers.</p>
<h3 class="spip"><a id="sommaire_4"></a>Evolution des prix</h3>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : avril 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/vie-pratique/article/cout-de-la-vie-112201). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
