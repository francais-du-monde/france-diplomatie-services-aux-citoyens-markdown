# Recherche d’emploi

<h4 class="spip">Outils pour la recherche d’emploi</h4>
<h4 class="spip">Journaux et sites internet</h4>
<p>Des petites annonces sont diffusées dans la presse :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://elcomercio.pe/" class="spip_out" rel="external">El Comercio</a>) </p>
<p>ou sur internet :</p>
<ul class="spip">
<li><a href="http://www.mintra.gob.pe/mostrarServicios.php?codServicios=33" class="spip_out" rel="external">Ministère du Travail</a></li>
<li><a href="http://www.computrabajo.com.pe/" class="spip_out" rel="external">Bourse de l’emploi</a></li></ul>
<p><strong>Cependant, plus de 80% des offres d’emploi ne sont pas publiées.</strong> Il est donc essentiel d’avoir une démarche très active. Le réseau relationnel est particulièrement important au Pérou pour trouver un emploi.</p>
<h4 class="spip">Organismes pour la recherche d’emploi</h4>
<p>L’équivalent de Pôle emploi n’existe pas au Pérou. Le ministère du Travail dispose d’une petite <a href="http://www.mintra.gob.pe/mostrarServicios.php?codServicios=33" class="spip_out" rel="external">bourse du travail</a>.</p>
<p>On peut également faire appel à des cabinets de recrutement, internationaux et locaux (Adecco, Manpower, KPMG, Deloitte, etc).</p>
<p>La Chambre de commerce et d’industrie franco-péruvienne (CCIFP) peut aussi vous informer :</p>
<p><a href="http://www.ccipf.com/" class="spip_out" rel="external">Chambre de commerce et d’industrie franco-péruvienne (CCIFP)</a><br class="manualbr">Los Nogales 326 - San Isidro - Lima 27<br class="manualbr">Téléphone : [51] 1 421 40 50 - Télécopie : [51] 1 421 90 93<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/emploi-stage/article/recherche-d-emploi#ccipf#mc#terra.com.pe#" title="ccipf..åt..terra.com.pe" onclick="location.href=mc_lancerlien('ccipf','terra.com.pe'); return false;" class="spip_mail">Courriel</a></p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/emploi-stage/article/recherche-d-emploi). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
