# Vie pratique

<h2 class="rub22972">Logement</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/vie-pratique/#sommaire_1">Où se loger ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/vie-pratique/#sommaire_2">Auberges de jeunesse et B&amp;B </a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/vie-pratique/#sommaire_3">Conditions de location</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/vie-pratique/#sommaire_4">Electricité</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/vie-pratique/#sommaire_5">Electroménager</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Où se loger ?</h3>
<p>Loyers donnés à titre indicatif, variables selon si le bien est loué meublé ou non.</p>
<h4 class="spip">Pretoria/Johannesburg</h4>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Loyer mensuel (quartier résidentiel)</strong></td>
<td>Rands</td></tr>
<tr class="row_even even">
<td>3 pièces</td>
<td>7000</td></tr>
<tr class="row_odd odd">
<td>5 pièces</td>
<td>12 000</td></tr>
<tr class="row_even even">
<td>Villa</td>
<td>25 000</td></tr>
</tbody>
</table>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Loyer mensuel (banlieue)</strong></td>
<td>Rands</td></tr>
<tr class="row_even even">
<td>2 pièces</td>
<td>5000</td></tr>
<tr class="row_odd odd">
<td>5 pièces</td>
<td>10 000</td></tr>
<tr class="row_even even">
<td>Villa</td>
<td>20 000</td></tr>
</tbody>
</table>
<p><strong>Hôtels</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Prix moyen d’une chambre d’hôtel (chambre double)</strong></td>
<td>Rands</td></tr>
<tr class="row_even even">
<td>Grand tourisme</td>
<td>2000</td></tr>
<tr class="row_odd odd">
<td>Moyen tourisme</td>
<td>1000</td></tr>
</tbody>
</table>
<h4 class="spip">Le Cap</h4>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Loyer mensuel (quartier résidentiel)</strong></td>
<td>Rands</td></tr>
<tr class="row_even even">
<td>3 pièces</td>
<td>11 000</td></tr>
<tr class="row_odd odd">
<td>Villa</td>
<td>35 000</td></tr>
</tbody>
</table>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Loyer mensuel (banlieue)</strong></td>
<td>Rands</td></tr>
<tr class="row_even even">
<td>Studio</td>
<td>4000</td></tr>
<tr class="row_odd odd">
<td>3 pièces</td>
<td>8000</td></tr>
<tr class="row_even even">
<td>Villa</td>
<td>20 000</td></tr>
</tbody>
</table>
<p><strong>Hôtels</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Prix moyen d’une chambre d’hôtel (chambre double)</strong></td>
<td>Rands</td></tr>
<tr class="row_even even">
<td>Grand tourisme</td>
<td>2500</td></tr>
<tr class="row_odd odd">
<td>Moyen tourisme</td>
<td>1200</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_2"></a>Auberges de jeunesse et B&amp;B </h3>
<p>A titre indicatif, le prix d’une chambre en <i>guesthouse</i> et <i>Bed and Breakfast</i> est de 400 à 700 rands (par personne).</p>
<p>Le site <a href="http://coasttocoast.co.za/" class="spip_out" rel="external">Coast to Coast</a> répertorie des auberges de jeunesse dans tout le pays.</p>
<p>Les sites <a href="http://www.savenues.com/" class="spip_out" rel="external">Savenues.com</a> et <a href="http://wheretostay.co.za/" class="spip_out" rel="external">Wheretostay.co.za</a> proposent également de nombreux hébergements.</p>
<h3 class="spip"><a id="sommaire_3"></a>Conditions de location</h3>
<p><strong>Il existe un important marché locatif, mais relativement étroit pour les logements de classe moyenne.</strong> Pour trouver un logement, quelques sites d’annonces existent : <a href="http://www.gumtree.co.za/" class="spip_out" rel="external">Gumtree.co.za</a> ou <a href="http://www.junkmail.co.za/" class="spip_out" rel="external">Junkmail.co.za</a>. Les agences immobilières sont très nombreuses.</p>
<p>Dans la recherche d’un logement, il est conseillé d’être attentif aux questions de sécurité (localisation du logement, protections anti-intrusions, safe haven, …).</p>
<p>Le délai moyen de recherche peut varier de deux à huit semaines. Il n’y a ni pas de porte ni reprise. Un état des lieux est établi. La durée des baux est d’un à trois ans et inclus un ou deux mois de caution ainsi que généralement une augmentation annuelle automatique de l’ordre de 10 % du loyer. En cas de location d’un logement par une agence, la commission est payée par le propriétaire.</p>
<p>Les loyers sont chers par rapport à la valeur des biens (le loyer mensuel dépasse souvent 1 % du prix d’achat). Il est parfois plus avantageux d’acheter un logement avec un prêt en France car le crédit local est très onéreux. La revente reste néanmoins délicate.</p>
<p>Pour une villa avec piscine et jardin, il convient de prévoir entre 1000 et 1200 rands mensuels pour l’électricité (en fonction des saisons) et de 800 à 1400 rands mensuels pour l’eau.</p>
<p>Les maisons sont rarement équipées de chauffage ou de climatisation (parfois chauffage au sol très onéreux) bien que les températures hivernales approchent 0°C la nuit. Il convient donc de s’équiper d’un chauffage d’appoint. La climatisation n’est pas indispensable malgré des températures estivales pouvant approcher 35° au plus chaud de la journée.</p>
<p>La sécurité du logement est primordiale. Le coût d’installation d’un système d’alarme est variable en fonction de l’équipement (clôtures électriques d’enceinte, alarmes volumétriques ou infra-rouges, pose de barreaux aux fenêtres, grilles intérieures isolant les chambres à coucher, <i>panic bouton</i> pour avertir la sécurité en cas d’urgence). En général, le coût d’installation s’élève à environ 8000 rands. Le coût mensuel moyen d’un gardien de nuit (armé) logé, nourri et blanchi est de minimum 10 000 rands et le coût d’une nuit de gardiennage est d’environ 600 rands.</p>
<h4 class="spip">Johannesburg</h4>
<p>Les quartiers résidentiels sont essentiellement situés dans la banlieue nord et nord-ouest (Sandton, Randburg, Morningside, Bryanston). Les quartiers de Fourways, Sunninghill et Lonehill sont en pleine expansion, bien que plus éloignés du "centre" de Sandton et du Lycée Jules Verne à Morningside.</p>
<p>Les valeurs sûres restent les quartiers de la périphérie nord du centre de Johannesburg : Saxonwold, Hyde Park, Westcliff, Houghton, Parkside, Parktown North, Parkhurst. Melville reste le quartier "artiste" très prisé.</p>
<p>Le centre-ville de Johannesburg et les quartiers adjacents (Hillbrow, Berea, Yeoville) sont dégradés et peu conseillés. Néanmoins une spéculation immobilière existe et contribue au développement de micro-quartiers où se réunit une population jeune et branchée.</p>
<p>Les loyers varient considérablement en fonction des quartiers et des systèmes de sécurité dont sont équipés les logements. Les quartiers sud de Johannesburg, très éloignés du Lycée français (Rosettenville, Turfontein), proposent des loyers très intéressants, mais pour des logements de qualité souvent médiocre.</p>
<p>Les quartiers d’extrême nord (Fourways) offrent des villas dans des lotissements protégés, dont les loyers rivalisent avec ceux des villas situées dans les environs du lycée français (Morningside) ou du Consulat de France (Saxonwold, Houghton).</p>
<h4 class="spip">Le Cap</h4>
<p>Les quartiers résidentiels au Cap sont nombreux à proximité du centre-ville (<i>City Bowl</i>) ou en banlieue : Camps Bay (5 km), Sea Point (3 km), Constantia (19 km), Bishop’s Court (14 km), Bellville/Durbanville (35 km), Minerlton (15 km). Il convient, pour des raisons de sécurité, de privilégier les quartiers de la périphérie du centre-ville (<i>City Bowl</i>) ou résidentiels.</p>
<h3 class="spip"><a id="sommaire_4"></a>Electricité</h3>
<p>Dans toute l’Afrique du Sud, le courant électrique est de 220/230 volts (périodes : 50 Hertz) sauf à Pretoria où il est de 220/250 volts (périodes : 50 Hertz). Les prises locales nécessitent des adaptateurs qu’il est facile de se procurer sur place.</p>
<p>La climatisation n’est pas indispensable, mais un chauffage électrique d’appoint est nécessaire durant les mois d’hiver (juin/septembre).</p>
<h3 class="spip"><a id="sommaire_5"></a>Electroménager</h3>
<p>Les cuisines sont généralement équipées d’un réfrigérateur, de plaques de cuisson et d’un four.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-vie-pratique-article-pour-en-savoir-plus-111025.md" title="Pour en savoir plus">Pour en savoir plus</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-vie-pratique-article-loisirs-et-culture.md" title="Loisirs et culture">Loisirs et culture</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-vie-pratique-article-communications-111023.md" title="Communications">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-vie-pratique-article-transports.md" title="Transports">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-vie-pratique-article-cout-de-la-vie-111021.md" title="Coût de la vie">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-vie-pratique-article-scolarisation-111020.md" title="Scolarisation">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-vie-pratique-article-sante.md" title="Santé">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-vie-pratique-article-logement-111018.md" title="Logement">Logement</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/vie-pratique/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
