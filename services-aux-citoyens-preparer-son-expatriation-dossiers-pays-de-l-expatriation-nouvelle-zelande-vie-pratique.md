# Vie pratique

<h2 class="rub23020">Logement</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/vie-pratique/#sommaire_1">Où se loger ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/vie-pratique/#sommaire_2">Conditions de location</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/vie-pratique/#sommaire_3">Hôtels</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/vie-pratique/#sommaire_4">Electricité</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/vie-pratique/#sommaire_5">Electroménager</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Où se loger ?</h3>
<p>Les quartiers résidentiels de Wellington se situent à Te Aro, Mount Victoria, Kelburn, Thorndon, Seatoun et Roseneath.</p>
<h4 class="spip">Colocation</h4>
<p>La colocation ("flatting"), répandue dans le pays, peut être un moyen économique de se loger durablement. Comptez environ entre 70€ et 130€ la semaine pour une chambre ou un studio (hors caution+charges). Des annonces sont publiées dans la presse locale (rubrique <i>headings share accomodation / flat to let</i>) et sur les sites internet d’annonces de particuliers.</p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Loyer hebdomadaire approximatif à Wellington</strong></td>
<td>Quartier résidentiel (NZD)</td>
<td>Quartier résidentiel (€)</td>
<td>Banlieue (NZD)</td>
<td>Banlieue (€)</td></tr>
<tr class="row_even even">
<td>Chambre seule</td>
<td>205</td>
<td>121</td>
<td>150</td>
<td>89</td></tr>
<tr class="row_odd odd">
<td>Une chambre</td>
<td>305</td>
<td>180</td>
<td>220</td>
<td>130</td></tr>
<tr class="row_even even">
<td>Deux chambres</td>
<td>450</td>
<td>266</td>
<td>330</td>
<td>195</td></tr>
<tr class="row_odd odd">
<td>Trois chambres</td>
<td>545</td>
<td>322</td>
<td>400</td>
<td>237</td></tr>
<tr class="row_even even">
<td>Quatre chambres</td>
<td>625</td>
<td>370</td>
<td>450</td>
<td>267</td></tr>
<tr class="row_odd odd">
<td>Cinq chambres</td>
<td>695</td>
<td>411</td>
<td>550</td>
<td>325</td></tr>
<tr class="row_even even">
<td>Six chambres</td>
<td>750 et plus</td>
<td>444 et plus</td>
<td>600 et plus</td>
<td>355 et plus</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_2"></a>Conditions de location</h3>
<h4 class="spip">Les offres</h4>
<p>Les offres de location sont largement diffusées dans la <strong>presse locale</strong> (édition du mercredi et du samedi) ou sur <strong>internet</strong>. Le recours aux <strong>agences immobilières </strong>est assorti d’une commission équivalente à une semaine de location additionnée de la GST (TVA néo-zélandaise) d’un taux de 15%.</p>
<p>Les petites annonces de logements utilisent la terminologie suivante :</p>
<ul class="spip">
<li><i>apartment</i> : appartement dans un immeuble ou dans une maison</li>
<li><i>flat</i> : maison jumelée ou mitoyenne.</li>
<li><i>house</i> : pavillon individuel avec jardin</li>
<li><i>townhouse</i> : pavillon individuel moderne avec petit jardin</li></ul>
<h4 class="spip">Le contrat de location</h4>
<p>La loi néo-zélandaise spécifie les droits et les obligations des propriétaires et locataires en matière de location. Selon le droit anglo-saxon, le propriétaire a tendance à être plus protégé qu’en France.</p>
<p>Un contrat de location doit être signé entre les deux parties. Deux types de location existent :</p>
<ul class="spip">
<li><strong>Periodic tenancy </strong> : la durée n’est pas spécifiée et la location se poursuit tant que le propriétaire ou le locataire n’a pas donné de préavis dans les délais réglementaires spécifiés par la loi pour mettre fin au contrat.</li>
<li><strong>Fixed term tenancy </strong> : la location se termine à la date spécifiée dans le contrat.</li></ul>
<p>Le locataire dispose d’un <strong>préavis</strong> de 21 jours pour avertir le propriétaire par écrit de sa décision de mettre un terme à la location. Le propriétaire doit respecter quant à lui un délai de 90 jours pour annoncer la fin de la location, réduit à 42 jours en cas de vente ou de reprise par la famille du propriétaire.</p>
<p>Une <strong>caution</strong> représentant deux à quatre semaines de location maximum est habituellement exigée par le propriétaire. Ce dernier a l’obligation de la transmettre au département des locations (Tenancy Services Centre) qui dépend du ministère du logement. La caution est restituée sur demande au locataire à son départ (si les loyers sont à jour et le logement loué rendu en bon état).</p>
<p>Le <strong>loyer</strong> se règle par quinzaine ou à la semaine. Une avance de deux semaines de location est souvent payable à l’entrée dans le logement. Le locataire doit s’acquitter de toutes les charges (électricité, eau, gaz, téléphone…). Il est recommandé de s’assurer du bon état de l’isolation et du chauffage, souvent très déficients.</p>
<p>Les murs sont assurés par le propriétaire. Toutefois, il est conseillé de souscrire une <strong>assurance</strong> pour les effets personnels.</p>
<p>La majorité des logements à louer sont non-meublés mais souvent équipés (réfrigérateur, machine à laver, four, rideaux et moquette). La signature d’un <strong>état des lieux</strong>, annexé au contrat de location est vivement recommandée surtout s’il s’agit d’un logement meublé.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.dbh.govt.nz/" class="spip_out" rel="external">Site dépendant du ministère du logement néo-zélandais</a> - une mine d’informations sur toutes les questions relatives à la location.
<br>— Un livret au format pdf intitulé <a href="http://www.dbh.govt.nz/renting-and-you" class="spip_out" rel="external">Renting and you - a guide to the law about renting</a> répertorie toutes les démarches à entreprendre, les droits et les obligations du locataire et du propriétaire.
<br>— Des <a href="http://www.dbh.govt.nz/market-rent" class="spip_out" rel="external">indications du prix du marché locatif dans toute la Nouvelle-Zélande</a> sont également disponibles.</li>
<li><a href="http://www.hnzc.co.nz/" class="spip_out" rel="external">Housing New Zealand Corporation</a> - concerne les locations attribuées par cet organisme qui fournit des logements à loyers modérés aux Néo-zélandais (accessible aux migrants résidant en Nouvelle-Zélande depuis plus de deux ans).</li>
<li>Informations pratiques pour les migrants sur <a href="http://www.newzealandnow.govt.nz/living-in-nz/family-friendly/choice-of-housing-options" class="spip_out" rel="external">Newzealandnow.govt.nz</a> et sur <a href="http://www.frogs-in-nz.com/tribu%20" class="spip_out" rel="external">Frogs-in-nz.com</a>.</li>
<li><a href="http://www.realestate.co.nz/" class="spip_out" rel="external">Agences immobilières en ligne</a></li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Hôtels</h3>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Prix moyen</strong></td>
<td>NZD</td>
<td>Euros</td></tr>
<tr class="row_even even">
<td>Grand tourisme (chambre double)</td>
<td>200 ou plus</td>
<td>120 ou plus</td></tr>
<tr class="row_odd odd">
<td>Gamme moyenne (chambre double)</td>
<td>Entre 50 et 180</td>
<td>Entre 30 et 110</td></tr>
<tr class="row_even even">
<td>Petit budget (chambre double ou 2 lits)</td>
<td>Entre 20 et 40</td>
<td>Entre 12,50 et 25</td></tr>
</tbody>
</table>
<p>Les possibilités d’hébergement en auberge de jeunesse ou chez l’habitant sont nombreuses.</p>
<p>Voir notamment :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Youth Hostels Association - New Zealand </strong><br class="manualbr">Level 1,Moorhouse City<br class="manualbr">166 Moorhouse avenue <br class="manualbr">PO box 436 <br class="manualbr">Christchurch New Zealand <br class="manualbr">Tél. : (64) (3) 379 9970 - Fax : (64) (3) 365 4476<br class="manualbr">Internet : <a href="http://www.yha.org.nz/" class="spip_out" rel="external">Yha.org.nz</a> et <a href="http://www.hostels.com/" class="spip_out" rel="external">Hostels.com</a>.</p>
<h3 class="spip"><a id="sommaire_4"></a>Electricité</h3>
<p>Le courant électrique est alternatif monophasé d’une fréquence de 50 hertz et d’une tension de 220 à 230 volts. Les prises de courant sont à trois ou deux fiches plates. Elles sont utilisées seulement en Nouvelle-Zélande et en Australie : il est conseillé de se munir d’adaptateurs.</p>
<h3 class="spip"><a id="sommaire_5"></a>Electroménager</h3>
<p>Le passage à la télévision digitale s’est achevé le 1er décembre 2013.</p>
<p>Il est nécessaire de chauffer les habitations huit à neuf mois de l’année. Le chauffage est assez onéreux en NZD et la piètre isolation des maisons engendre des factures conséquentes.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-nouvelle-zelande-vie-pratique-article-loisirs-et-culture.md" title="Loisirs et culture">Loisirs et culture</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-nouvelle-zelande-vie-pratique-article-communications.md" title="Communications">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-nouvelle-zelande-vie-pratique-article-transports-111244.md" title="Transports">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-nouvelle-zelande-vie-pratique-article-cout-de-la-vie-111243.md" title="Coût de la vie">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-nouvelle-zelande-vie-pratique-article-scolarisation-111242.md" title="Scolarisation">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-nouvelle-zelande-vie-pratique-article-sante.md" title="Santé">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-nouvelle-zelande-vie-pratique-article-logement-111240.md" title="Logement">Logement</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/vie-pratique/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
