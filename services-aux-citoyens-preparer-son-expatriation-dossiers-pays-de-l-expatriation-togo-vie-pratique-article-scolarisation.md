# Scolarisation

<p>Pour toute information sur la scolarisation dans le système français à l’étranger, vous pouvez consulter notre thématique sur les <a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures.md" class="spip_in">Etudes supérieures</a> et la <a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais.md" class="spip_in">Scolarité en français</a>.</p>
<p>Vous y trouverez des renseignements sur :</p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/vivre-a-l-etranger/vivre-a-l-etranger-vos-droits-et/le-reseau-scolaire-francais-a-l/article/agence-pour-l-enseignement" class="spip_out">Agence pour l’enseignement français à l’étranger (AEFE)</a></li>
<li><a href="http://www.diplomatie.gouv.fr/fr/vivre-a-l-etranger/vivre-a-l-etranger-vos-droits-et/le-reseau-scolaire-francais-a-l/article/aide-a-la-scolarite" class="spip_out">Faire une demande de bourses scolaires</a></li>
<li><a href="http://www.diplomatie.gouv.fr/fr/vivre-a-l-etranger/vivre-a-l-etranger-vos-droits-et/le-reseau-scolaire-francais-a-l/article/enseignement-a-distance" class="spip_out">Enseignement à distance</a></li></ul>
<h4 class="spip">Les établissements scolaires français au Togo</h4>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://lyc-francais-lome.ac-nantes.fr/" class="spip_out" rel="external">Lycée français de Lomé</a></p>
<h4 class="spip">Enseignement supérieur</h4>
<p>Il est possible de poursuivre localement des études supérieures dans les facultés de médecine, droit, sciences économiques, lettres et langues ; ainsi qu’à l’école internationale d’architecture, l’école supérieure de gestion, d’informatique et de secrétariat.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/vie-pratique/article/scolarisation). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
