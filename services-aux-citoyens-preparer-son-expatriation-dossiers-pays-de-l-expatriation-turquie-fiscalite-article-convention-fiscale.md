# Convention fiscale

<p>Une convention internationale ayant primauté sur la loi interne, les dispositions de la loi du 29 décembre 1976 modifiant les règles de territorialité et les conditions d’imposition des Français de l’étranger (Journal Officiel du 30 décembre 1976) ne sont applicables que si elles sont compatibles avec les dispositions de la convention.</p>
<p>La convention fiscale franco-turque du 18 février 1987 publiée au Journal Officiel du 6 juillet 1989 fixe les règles d’imposition relatives notamment à l’impôt sur le revenu applicables aux résidents des deux pays.</p>
<p>Le texte de la convention peut également être obtenu en s’adressant à l’imprimerie des Journaux officiels par courrier, 26, rue Desaix, 75727 PARIS CEDEX 15, par télécopie n° 40.58.77.80, ou sur Internet :</p>
<p>Ses dispositions principales concernant un Français expatrié sont les suivantes :</p>
<ul class="spip">
<li>Au sens de la présente Convention, l’expression "résident d’un Etat contractant" désigne toute personne qui, en vertu de la législation de cet Etat, est assujettie à l’impôt dans cet Etat, en raison de son domicile, de sa résidence, de son siège de direction officiel, du lieu de direction des affaires ou de tout autre critère de nature analogue.</li>
<li>Lorsque, selon les dispositions du paragraphe 1, une personne physique est un résident des deux Etats contractants, sa situation est réglée de la manière suivante :
<br>— cette personne est considérée comme un résident de l’Etat où elle dispose d’un foyer d’habitation permanent ;
<br>— si elle dispose d’un foyer d’habitation permanent dans les deux Etats, elle est considérée comme un résident de l’Etat avec lequel ses liens personnels et économiques sont les plus étroits (centre des intérêts vitaux) ;
<br>— si l’Etat où cette personne a le centre de ses intérêts vitaux ne peut pas être déterminé, ou si elle ne dispose d’un foyer d’habitation permanent dans aucun des Etats, elle est considérée comme un résident de l’Etat contractant où elle séjourne de façon habituelle ;
<br>— si cette personne séjourne de façon habituelle dans les deux Etats ou si elle ne séjourne de façon habituelle dans aucun d’eux, elle est considérée comme un résident de l’Etat dont elle possède la nationalité ;
<br>— si cette personne possède la nationalité des deux Etats ou si elle ne possède la nationalité d’aucun d’eux, les autorités compétentes des Etats contractants tranchent la question d’un commun accord.</li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/fiscalite/article/convention-fiscale). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
