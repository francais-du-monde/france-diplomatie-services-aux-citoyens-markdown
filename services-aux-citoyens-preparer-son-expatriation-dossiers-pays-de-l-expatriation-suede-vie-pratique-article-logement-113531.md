# Logement

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/vie-pratique/article/logement-113531#sommaire_1">Conditions de location</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/vie-pratique/article/logement-113531#sommaire_2">Hôtel</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/vie-pratique/article/logement-113531#sommaire_3">Electricité</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/vie-pratique/article/logement-113531#sommaire_4">Electroménager</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Conditions de location</h3>
<p>Le marché immobilier, dans la capitale comme dans les autres grandes villes de Suède, est très limité, surtout en ce qui concerne le logement locatif. Pour trouver une location, le bouche à oreille est souvent utilisé. On peut également avoir recours aux agences immobilières (Agence Newcomers, SAS Relocation service, etc.) et aux annonces parues dans les journaux ou sur internet (délai de recherche de un à deux mois).</p>
<p>A Stockholm, les quartiers résidentiels sont situés en centre-ville (Östermalm, Kungsholmen) et dans certaines banlieues (Danderyd, Lidingö, Värmdö, Bromma).</p>
<p>Les baux sont d’un à trois ans, avec une clause de dénonciation de trois mois. Les expatriés prennent souvent un bail de seconde main ou profitent d’une libération temporaire de l’habitation par les propriétaires (cela crée toutefois une insécurité du bail qui quoique légal, peut être rompu à tout moment avec un court préavis).</p>
<p>Il est nécessaire de payer le loyer un mois à l’avance. Certains propriétaires peuvent réclamer un dépôt de garantie allant jusqu’à trois mois de loyer. Un état des lieux n’est pas systématique, mais le logement doit être rendu dans le même état qu’avant la location.</p>
<p>L’eau et le chauffage sont compris dans le loyer. L’électricité fait l’objet d’une facturation séparée d’un montant moyen de 1500 SEK/trimestre.</p>
<p><strong>Exemples de coûts de logements (loyers mensuels en couronnes suédoises)</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Type de quartiers</td>
<td>studio</td>
<td>3 pièces</td>
<td>5 pièces</td>
<td>villa</td></tr>
<tr class="row_even even">
<td>Quartier résidentiel</td>
<td>7000</td>
<td>15 700</td>
<td>20 600</td>
<td>29 000</td></tr>
<tr class="row_odd odd">
<td>Banlieue</td>
<td>5000</td>
<td>14 700</td>
<td>17 700</td>
<td>26 000</td></tr>
</tbody>
</table>
<p>Les garages ou places de parkings ne sont généralement pas inclus dans les loyers. On doit compter environ 300 à 1000 SEK par mois pour pouvoir louer des emplacements, assez rares sur le marché.</p>
<h3 class="spip"><a id="sommaire_2"></a>Hôtel</h3>
<p><strong>Stockholm </strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Prix moyen d’une chambre d’hôtel (chambre double)</strong></td>
<td>couronnes suédoises</td>
<td>euros</td></tr>
<tr class="row_even even">
<td>Grand tourisme</td>
<td>2095</td>
<td>211</td></tr>
<tr class="row_odd odd">
<td>Moyen tourisme</td>
<td>1090</td>
<td>110</td></tr>
</tbody>
</table>
<p>Les plus grands hôtels de Stockholm (luxe, renommée et prix) : Grand Hôtel, Hôtel Diplomat, Hotel Esplanad, Sheraton.</p>
<p>Les grandes chaînes d’hôtels nordiques : Clarion, Scandic, Elite Hotels, Radisson SAS</p>
<p><strong>Auberges de jeunesse </strong></p>
<p>Il est conseillé de prendre la carte des AJ en France avant le départ. La Suède compte plus de 300 auberges de jeunesse et auberges familiales que l’on appelle les <i>vandrarhem</i>. Elles sont très bien aménagées et le niveau de confort est proche d’un hôtel deux étoiles. Certaines auberges sont ouvertes toute l’année mais la plupart ne le sont que pendant l’été et parfois ferment le 15 août. Il faut penser à réserver. Les prix les plus bas sont ceux en dortoirs. Souvent il existe des chambres pour deux et quatre personnes. L’office de tourisme suédois à Paris délivre gratuitement la liste de toutes les auberges de jeunesse de Suède.</p>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li><a href="http://www.svif.se/" class="spip_out" rel="external">http://www.svif.se/</a> (en anglais et allemand)</li>
<li><a href="http://www.svenskaturistforeningen.se/" class="spip_out" rel="external">Union Suédoise du Tourisme</a><a href="http://www.diplomatie.gouv.fr/fr/" class="spip_url spip_out"></a></li></ul>
<p>Il est possible de loger chez l’habitant. Les offices de tourisme des villes suédoises gèrent en général une liste de chambres privées chez l’habitant. Les prix sont plus élevés que pour les auberges de jeunesse, mais toujours plus avantageux que ceux pratiqués dans les hôtels.</p>
<h3 class="spip"><a id="sommaire_3"></a>Electricité</h3>
<p>Le courant est de 220 volts. Les prises de courant sont toutes avec fil de terre (norme SEMKO) compatibles avec les appareils français sauf parfois le gros électroménager.</p>
<h3 class="spip"><a id="sommaire_4"></a>Electroménager</h3>
<p>Les cuisines sont en général équipées (plaques de cuisson et four, réfrigérateur, congélateur, plus rarement lave-vaisselle). Les machines à laver sont parfois à usage collectif, dans les sous-sols d’immeubles. A défaut, tout l’équipement ménager est disponible sur place avec un choix très vaste.</p>
<p><i>Mises à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/vie-pratique/article/logement-113531). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
