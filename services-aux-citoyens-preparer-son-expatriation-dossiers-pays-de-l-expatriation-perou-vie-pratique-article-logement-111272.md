# Logement

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/vie-pratique/article/logement-111272#sommaire_1">Où se loger ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/vie-pratique/article/logement-111272#sommaire_2">Hôtels</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/vie-pratique/article/logement-111272#sommaire_3">Electricité</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/vie-pratique/article/logement-111272#sommaire_4">Electroménager</a></li></ul>
<p>Il est fortement conseillé de privilégier la sécurité du logement, les cambriolages étant fréquents.</p>
<h3 class="spip"><a id="sommaire_1"></a>Où se loger ?</h3>
<p>Les quartiers considérés résidentiels sont Miraflores (bord de mer) et San Isidro et plus excentrés, Surco, La Molina, Monterrico (plus proches du lycée franco-péruvien), Chacarilla, Barranco. Le centre de Lima et certains quartiers périphériques sont déconseillés. Le déplacement des enfants seuls est à proscrire.</p>
<h4 class="spip">Coût de l’immobilier</h4>
<p><strong>Lima (en USD)</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Quartiers</strong></td>
<td>Studio</td>
<td>3 pièces</td>
<td>5 pièces</td>
<td>Villa</td></tr>
<tr class="row_even even">
<td>quartier résidentiel</td>
<td>800</td>
<td>1800 à 2000</td>
<td>2300</td>
<td>2800</td></tr>
</tbody>
</table>
<h4 class="spip">Conditions de location</h4>
<p>Il est relativement aisé de trouver un logement (vide ou meublé) et son standing est généralement supérieur à celui des logements européens. Cependant le mobilier fourni est souvent médiocre. Les studios et les deux pièces commencent à apparaître sur le marché.</p>
<p>Il faut compter de quinze jours à un mois pour trouver une maison ou un appartement. Les appartements disponibles sont plus nombreux que les maisons.</p>
<p>La durée généralement admise des baux est comprise entre un et trois ans. Ils sont en général reconductibles avec l’accord des deux parties. Un état des lieux est indispensable. Il convient de payer en général un mois de loyer d’avance et deux mois de caution. Certains propriétaires demandent six mois à un an d’avance en contrepartie d’une diminution substantielle du prix du loyer.</p>
<p>En cas de location par une agence, la commission est payée par le propriétaire.</p>
<p>Les charges comprennent l’eau, le gardiennage, les taxes sur les ordures ménagères et les raccordements divers (câbles). Elles représentent un coût entre 180 et 300 USD par mois. Les factures d’électricité augmentent durant l’hiver austral (juin-octobre) car les chauffages d’appoint et l’usage de déshumidificateurs sont nécessaires compte tenu du taux élevé d’humidité à Lima en cette période.</p>
<h3 class="spip"><a id="sommaire_2"></a>Hôtels</h3>
<p>Les grandes chaînes hôtelières sont présentes à Lima et dans les villes touristiques du Pérou (Marriott, Hilton, Orient-Express, Melia, Libertadores, Casa Andina…). Il existe un Novotel et un hôtel Ibis à Lima. Les bons hôtels sont généralement situés à Miraflores, San Isidro et Barranco.</p>
<p>Les « hostales » sont des hôtels meilleur marché, plus nombreux dans le centre historique.</p>
<h4 class="spip">Appart-hôtels</h4>
<p>Très répandus en Amérique latine, les appart-hôtels proposent des appartements (cuisine, salon, salle de bain, chambre double) tout en offrant un véritable service hôtelier. Les locations se font à la journée, à la semaine ou au mois (compter environ 80 euros par nuit).</p>
<h4 class="spip">Auberges de jeunesse</h4>
<p>Les possibilités d’hébergement en auberge de jeunesse sont nombreuses :</p>
<ul class="spip">
<li><a href="http://www.hostels.com/fr/perou" class="spip_out" rel="external">Hostels.com</a></li>
<li><a href="http://www.french.hostelworld.com/country.php/Auberges-de-Jeunesse/Perou" class="spip_out" rel="external">Hostelworld.com</a></li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Electricité</h3>
<p>Le voltage utilisé est de 220 volts, les périodes (nombre de cycle par seconde) de 60 Hertz. Les appareils européens prévus pour 50 Hertz ne comprenant ni moteur, ni horloge interne fonctionnent normalement. Les prises de courant sont de type américain ou international.</p>
<h3 class="spip"><a id="sommaire_4"></a>Electroménager</h3>
<p>Les cuisines sont le plus souvent équipées (<i>linea blanca</i> qui comprend : plaques de cuisson et four, machine à laver le linge, réfrigérateur, voire sèche-linge). Le marché local offre un bon choix d’appareils électroménagers de marques principalement américaines et asiatiques.</p>
<p>Les logements ne sont équipés, ni de climatiseurs, ni d’appareils de chauffage. Des radiateurs électriques et des appareils de climatisation disponibles sur place, s’avèrent cependant nécessaires. Des déshumidificateurs sont recommandés surtout dans les quartiers proches de la mer.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/vie-pratique/article/logement-111272). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
