# Pourquoi s’inscrire auprès du consulat ?

<p><strong>Pourquoi s’inscrire au registre des Français établis hors de France ?</strong></p>
<p><strong>L’inscription au registre des Français établis hors de France est une formalité administrative simple et gratuite. </strong> Elle est en principe valable cinq ans.</p>
<p>L’inscription est facultative mais elle est vivement recommandée en raison des avantages qu’elle présente pour nos compatriotes. Elle permet en effet de :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>faciliter l’accomplissement de nombreuses formalités administratives</strong> et l’obtention de documents administratifs (passeport, carte nationale d’identité, etc.) ;</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>accéder à certaines procédures ou à certaines prestations liées à la résidence à l’étranger ;</strong> telles que, par exemple, l’octroi de bourses scolaires ou l’inscription sur la liste électorale d’une commune en France ;</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  être recensé pour les <a href="services-aux-citoyens-documents-officiels-a-l-etranger-article-attestation-de-recensement-et-de-la-participation-aux-journees-defense-et.md" class="spip_in">journées défense et citoyenneté</a> ;</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>recevoir régulièrement des informations du poste consulaire, </strong> notamment sur la situation sécuritaire du pays ou sur les principaux événements ou échéances concernant les Français ;</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>obtenir une carte consulaire et un certificat d’inscription au registre</strong> qui permettra de faciliter vos démarches auprès des services douaniers ou auprès des autorités locales le cas échéant.</p>
<p><i>Mise à jour : janvier 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/inscription-consulaire-et-communaute-francaise/article/pourquoi-s-inscrire-aupres-du). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
