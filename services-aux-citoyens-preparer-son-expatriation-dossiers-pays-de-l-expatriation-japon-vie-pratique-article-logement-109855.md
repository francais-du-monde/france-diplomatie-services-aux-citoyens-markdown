# Logement

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/vie-pratique/article/logement-109855#sommaire_1">Où se loger ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/vie-pratique/article/logement-109855#sommaire_2">Conditions de location</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/vie-pratique/article/logement-109855#sommaire_3">Hôtels</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/vie-pratique/article/logement-109855#sommaire_4">Electricité</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/vie-pratique/article/logement-109855#sommaire_5">Electroménager</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Où se loger ?</h3>
<p>A Tokyo, les quartiers résidentiels sont situés à Minato-ku, Shinjuku-ku, Shibuya-ku, Chiyoda-ku.</p>
<p>A Osaka, les quartiers Nord et Ashiya (près de Kobe) et certains quartiers de Kyoto sont les plus recherchés.</p>
<p>Il est plus aisé de trouver un appartement vide qu’une villa, de toutes façons très onéreuse. Le délai de recherche est au minimum de deux semaines, voire un mois. Il convient de s’adresser à une <strong>agence immobilière</strong>. Certains propriétaires ayant des réticences à louer à des étrangers, il faudra choisir de préférence une agence habituée à des transactions avec les étrangers. Une autre possibilité est la <strong>colocation </strong>proposée par des agences spécialisées dans la location pour les étrangers. Vous avez deux solutions pour trouver un logement :</p>
<ul class="spip">
<li>soit, si vous parlez japonais, rendre visite aux agences immobilières autour de la gare près de laquelle vous souhaitez habiter ;</li>
<li>soit vous vous adressez aux agences spécialisées dans la recherche de logements pour les étrangers. Sachez que de nombreux propriétaires japonais refusent de louer un logement à un étranger.</li></ul>
<p>Il existe très peu de possibilités pour un séjour au pair au Japon. Il n’y a pas d’organisme qui soit habilité à mettre en relation les familles d’accueil et les demandeurs. Les seules offres correspondant à ce moyen d’hébergement sont affichées dans les mairies des différents districts, et sont peu nombreuses.</p>
<p>Le moyen le moins onéreux de trouver un logement à Tokyo se situe autour de ce que l’on appelle les « Gaijin Houses ». Vous louez une chambre et partagez les pièces communes avec d’autres étrangers. Certains endroits accueillent aussi des japonais. A titre d’exemple, il faut compter un loyer entre 50 000 et 70 000 yens, selon la situation géographique de l’habitation.</p>
<p>Une autre possibilité peut être la colocation. Ce service est offert par des agences immobilières spécialisées pour les étrangers désireux de s’installer au Japon. Ils mettent à disposition de appartements meublés. C’est le cas de « Sakura House », par exemple. Il faut compter entre 60 000 et 90 000 yens, selon la localisation et la qualité de l’hébergement. Les logement sont très souvent connectés à Internet et meublés. Les frais d’emménagement se montent quant à eux à une simple caution ne dépassant jamais 50 000 yens.</p>
<h3 class="spip"><a id="sommaire_2"></a>Conditions de location</h3>
<p>Les frais pour le logement comprennent :</p>
<ul class="spip">
<li>le shikikin, caution remboursable partiellement (souvent difficiles à récupérer en intégralité car les Japonais changent l’intégralité des moquettes et tapisseries après chaque locataire), à la fin du bail qui permet de couvrir les loyers impayés ou de payer les éventuels frais de remise en état du logement. Cela correspond à environ deux mois de loyer ;</li>
<li>le reikin est un "cadeau" que l’on fait au propriétaire avant d’emménager. C’est une tradition japonaise d’après guerre particulièrement respectée dans les grandes agglomérations. Le reikin s’élève en général à deux mois de loyer mais diminue voir disparaît en fonction de la situation géographique du logement ainsi que de son âge ;</li>
<li>le chukai ryôkin ou tesuryô est un versement à l’agence immobilière correspondant à environ un mois de loyer pour la commission ;</li>
<li>vous devrez enfin payer le premier mois de loyer avant d’emménager. Les loyers se paient toujours en fin de mois pour le mois suivant. Une pénalité peut être réclamée en cas de retard.</li></ul>
<p>Lorsque la location est faite à titre individuel ou que votre entreprise n’est pas connue au Japon, ce qui est souvent le cas, les agences exigent un garant, une personne ou plus volontiers, une entreprise japonaise.</p>
<p><strong>Coût des loyers de type résidentiel à Tokyo :</strong></p>
<p>Compter environ 3 000 - 4 000 yens par m² et par mois. En fonction de l’arrondissement dans lequel vous résidez, les prix peuvent facilement varier du simple au double. Le montant des loyers est notablement inférieur en banlieue de grandes agglomérations ou en Province, mais il est parfois plus judicieux de sacrifier la superficie de son logement plutôt que la distance qui vous sépare de votre lieu de travail tant le trajet en train chaque jour peut s’avérer fastidieux. Tokyo compte 12 millions d’habitants.</p>
<p>Coûts des logements par quartiers tokyoïtes.</p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Quartier</td>
<td>Studio</td>
<td>1 LDK 2K-2DK</td>
<td>2LDK 3K – 3DK</td></tr>
<tr class="row_even even">
<td>Nerima</td>
<td>71 400 yens</td>
<td>93 500 yens</td>
<td>162 300 yens</td></tr>
<tr class="row_odd odd">
<td>Setagaya</td>
<td>86 000 yens</td>
<td>123 400 yens</td>
<td>323 900 yens</td></tr>
<tr class="row_even even">
<td>Shinjuku</td>
<td>95 300 yens</td>
<td>155 400 yens</td>
<td>227 200 yens</td></tr>
<tr class="row_odd odd">
<td>Meguro</td>
<td>101 700 yens</td>
<td>169 400 yens</td>
<td>248 200 yens</td></tr>
<tr class="row_even even">
<td>Chuo</td>
<td>107 200 yens</td>
<td>180 900 yens</td>
<td>229 800 yens</td></tr>
<tr class="row_odd odd">
<td>Chiyoda</td>
<td>107 000 yens</td>
<td>197 800 yens</td>
<td>305 300 yens</td></tr>
<tr class="row_even even">
<td>Shibuya</td>
<td>118 700 yens</td>
<td>206 700 yens</td>
<td>323 900 yens</td></tr>
<tr class="row_odd odd">
<td>Minato</td>
<td>127 000 yens</td>
<td>237 200 yens</td>
<td>370 870 yens</td></tr>
</tbody>
</table>
<p>Coût des logements par quartiers à Osaka :</p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Loyer mensuel quartier résidentiel</strong></td>
<td>yens</td>
<td>Euros</td></tr>
<tr class="row_even even">
<td>Studio</td>
<td>180 000</td>
<td>1450</td></tr>
<tr class="row_odd odd">
<td>3 pièces</td>
<td>300 000</td>
<td>2418</td></tr>
<tr class="row_even even">
<td>5 pièces</td>
<td>800 000</td>
<td>6448</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_3"></a>Hôtels</h3>
<p>Une chambre dans un hôtel de confort moyen coûtera entre 5500 et 10 000 Y (45-80 € environ) par personne et par nuit.</p>
<h4 class="spip">Auberges de jeunesse</h4>
<p>Il existe des possibilités de loger en auberge de jeunesse (<a href="http://www.jyh.or.jp/" class="spip_out" rel="external">Jyh.or.jp</a>) ou dans des pensions traditionnelles japonaises, les Ryokhan (<a href="http://www.japaneseguesthouses.com/" class="spip_out" rel="external">Japaneseguesthouses.com</a>).</p>
<h3 class="spip"><a id="sommaire_4"></a>Electricité</h3>
<p>Le courant est alternatif, 50 hz au nord du Japon, 60 hz au sud, le voltage 100 volts, (mais certains appartements sont équipés en 200 volts), les prises sont de type américain.</p>
<h3 class="spip"><a id="sommaire_5"></a>Electroménager</h3>
<p>La climatisation en été et le chauffage en hiver sont indispensables. Le chauffage est assuré par des appareils électriques individuels qui servent à la climatisation l’été, indispensable en juillet et août.</p>
<p>Les cuisines sont peu équipées : si l’on y trouve assez souvent plaques de cuisson et four, parfois un réfrigérateur, les lave-linge, lave-vaisselle et congélateur sont beaucoup plus rares. L’équipement électroménager disponible sur place est beaucoup plus cher et moins moderne qu’en France, notamment pour les machines à laver le linge (conçues pour laver à l’eau froide) et les lave-vaisselle.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/vie-pratique/article/logement-109855). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
