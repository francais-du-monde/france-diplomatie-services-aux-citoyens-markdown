# Animaux domestiques

<h4 class="spip">Chiens et chats</h4>
<p>Les animaux domestiques importés au Maroc doivent être en règle vis-à-vis des exigences réglementaires en matière de santé et de protection animale.</p>
<p>Les documents suivants sont obligatoires pour l’animal et doivent être établis par le vétérinaire traitant, titulaire d’un mandat sanitaire :</p>
<ul class="spip">
<li>Identification de l’animal par micro-puce ou tatouage ;</li>
<li>Certificat de vaccination contre la rage de moins de six mois ;</li>
<li>Certificat de bonne santé datant de moins de six jours.</li></ul>
<p>Il est conseillé de faire procéder à un titrage des anticorps antirabiques dans un laboratoire agréé et de se munir du carnet de vaccination tenu à jour de l’animal. Prendre ensuite un rendez-vous avec le service « santé et protection animales » de la direction départementale des services vétérinaires (DDSV) dont relève le vétérinaire traitant, pour la validation des documents établis par celui-ci.</p>
<p>On trouve des vétérinaires au Maroc.</p>
<p><strong>Pour plus d’informations :</strong></p>
<p>Notre article thématique <a href="services-aux-citoyens-preparer-son-expatriation-douanes-article-animaux-domestiques.md" class="spip_in">Animaux domestiques</a></p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/entree-et-sejour/article/animaux-domestiques). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
