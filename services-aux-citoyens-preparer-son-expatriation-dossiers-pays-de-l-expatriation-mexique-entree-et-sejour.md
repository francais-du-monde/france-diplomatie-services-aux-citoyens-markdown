# Entrée et séjour

<h2 class="rub22686">Passeport, visa, permis de travail</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/entree-et-sejour/#sommaire_1">Séjour de courte durée</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/entree-et-sejour/#sommaire_2">Les différents types de visa</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/entree-et-sejour/#sommaire_3">Les démarches</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Séjour de courte durée</h3>
<p><strong>Le passeport en cours de validité est obligatoire</strong>.<br class="manualbr">Aucun visa n’est exigé pour le séjour une durée de 180 jours par an maximum. Aucune activité, rémunérée ou bénévole, n’est alors autorisée.</p>
<p>A l’arrivée, sur présentation du passeport et du billet d’avion aller-retour, un formulaire appelé <strong>F.M.M. (formulaire migratoire multiple) </strong>sera délivré et devra être rempli et visé par les autorités migratoires. Ce document doit être conservé et remis à l’immigration ou à la compagnie aérienne lors de la sortie du territoire.</p>
<p>Les personnes souhaitant se rendre au Mexique, pour le compte d’O.N.G., observateur des droits de l’homme, doivent obtenir avant le départ un visa qui peut être sollicité auprès de la section consulaire de l’ambassade du Mexique à Paris ou directement par l’ONG auprès de l’INM .</p>
<h3 class="spip"><a id="sommaire_2"></a>Les différents types de visa</h3>
<p>Pour un séjour de plus de 6 mois, <strong>afin d’exercer une activité professionnelle</strong>, <strong>un visa est requis</strong>. Des visas de travail sont délivrés pour les séjours temporaires. <strong>Attention</strong> : depuis la réforme migratoire, il n’est plus possible de changer de statut migratoire (par exemple, de touriste à travailleur) sans sortir du Mexique. Il n’est pas nécessaire néanmoins de retourner en France. Le visa de travail peut être délivré dans tout consulat mexicain de votre choix. Votre employeur devra l’indiquer au moment où il effectuera ses démarches auprès de <strong>l’Instituto Nacional de Migracion :</strong></p>
<p>Bureau central :<br class="manualbr">Calle Homero n° 1832<br class="manualbr">Col. Los Morales Polanco Delegación Miguel Hidalgo Mexico D.F. 11510<br class="manualbr">Tel. : (55) 5387 2400</p>
<p>Délégation régionale pour le District Fédéral :<br class="manualbr">Avenida Ejército Nacional n° 862<br class="manualbr">Colonia Los Morales Polanco Sección Palmas<br class="manualbr">Del. Miguel Hidalgo – 11540 Mexico DF<br class="manualbr">Tél. : (55) 5387 2400<br class="manualbr">Ouverture au public : 09h00 à 13h00</p>
<p>La nouvelle loi migratoire a mis en place une nouvelle nomenclature en matière de statut migratoire.</p>
<p>Outre le statut de « visiteur sans permission d’exercer une activité rémunérée (touriste pour 180 jours maximum), il existe dorénavant également le statut de :</p>
<ul class="spip">
<li><strong>Visiteur avec permission d’exercer une activité rémunérée</strong>. Il concerne le ressortissant étranger bénéficiaire d’une offre d’emploi, invité par une autorité ou une institution académique, artistique, sportive ou culturelle pour laquelle il perçoit une rémunération dans le pays, ou qui vient pour occuper une activité rémunérée pour une saison en vertu d’accords interinstitutionnels avec des entités étrangères. La durée du séjour : 180 jours maximum.</li>
<li><strong>Résident temporaire</strong> : autorise l’étranger à séjourner dans le pays pour une durée maximum de quatre ans, avec la possibilité d’obtenir un permis d’exercer une activité rémunérée, sujet à une offre d’emploi avec le droit d’entrer et de sortir du territoire national tant de fois qu’il le souhaite. Il a le droit à la préservation de l’unité familiale ce qui lui permettra entrer avec ou solliciter postérieurement l’entrée de ses enfants, des enfants de son conjoint ou concubin, à condition qu’il s’agisse de mineurs, non mariés, ou qui sont placés sous sa tutelle ou dont il a la garde, de son conjoint, de son concubin, de ses père et mère. Ces personnes seront autorisées à résider régulièrement sur le territoire national, avec la possibilité d’obtenir un permis de travail, sujet à une offre d’emploi avec le droit d’entrer et de sortir du territoire national tant de fois qu’elles le souhaitent. Dans le cas où le résident temporaire obtient une offre d’emploi, il lui est délivré un permis de travail. Les étrangers qui obtiennent le statut de résident temporaire pourront introduire leurs biens mobiliers, conformément à la législation en vigueur.</li>
<li><strong>Résident permanent</strong> : autorise l’étranger à s’établir sur le territoire national pour une durée indéterminée, avec la permission d’exercer une activité rémunérée.</li></ul>
<p>Il existe également le statut de :</p>
<ul class="spip">
<li>Visiteur régional pour les étrangers provenant des pays voisins au Mexique.</li>
<li>Visiteur travailleur frontalier.</li>
<li>Visiteur pour raisons humanitaires pour les étrangers victimes ou témoins d’un délit commis sur le territoire national ou les mineurs migrants non accompagnés.</li>
<li>Visiteur en vue d’adoption.</li>
<li>Résident temporaire étudiant.</li></ul>
<p>Les anciennes cartes de résident FM2 et FM3 n’existent plus. Les étrangers se voient désormais délivrer soit un FMM (touristes), soit une carte de visiteur, soit une carte de résident (établie pour une durée de 1 à 4 ans pour les résidents temporaires). La délivrance des cartes de visiteur et des cartes de résident sont soumis au paiement de droits qui sont fixés pour 2013 à :</p>
<ul class="spip">
<li>Visiteur avec permission d’exercer une activité rémunérée : 2.350 pesos soit environ 133 €.</li>
<li>Résident temporairejusqu’à 1 an : 3.130 pesos soit environ 178 €.</li>
<li>Résident temporaire deux ans : 4.690 pesos soit environ 266 €.</li>
<li>Résident temporaire trois ans : 5.940 pesos soit environ 337 €.</li>
<li>Résident temporaire quatre ans : 7.040 pesos soit environ 400 €.</li>
<li>Résident permanent : 3.815 pesos soit environ 217 €.</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Les démarches</h3>
<p>Pour toute information sur les différents visas, vous pouvez consulter le site de l’<a href="http://embamex.sre.gob.mx/francia/index.php/fr/visas" class="spip_out" rel="external">ambassade du Mexique en France</a>, ainsi que le site de l’<a href="http://www.inami.gob.mx/" class="spip_out" rel="external">Institut national migratoire</a>.</p>
<p>Pour tout type de visas, vous devez vous rendre impérativement à la section consulaire de l’ambassade du Mexique à Paris pour déposer votre dossier :<br class="manualbr">4, rue Notre Dame des Victoires<br class="manualbr">75002 Paris<br class="manualbr">Lundi au vendredi de 09h00 à 12h00<br class="manualbr">téléphone : 01 42 86 56 20 <br class="manualbr">télécopie : 01 49 26 02 78<br class="manualbr">courriel : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/entree-et-sejour/#consularmex#mc#wanadoo.fr#" title="consularmex..åt..wanadoo.fr" onclick="location.href=mc_lancerlien('consularmex','wanadoo.fr'); return false;" class="spip_mail">consularmex<span class="spancrypt"> [at] </span>wanadoo.fr</a>).</p>
<p>Le retrait du visa se fera, pour les ressortissants français, dans un délai de deux jours ouvrables (à partir de la date où toutes les conditions sont remplies) à la section consulaire. Si le demandeur de visa réside en dehors de la région parisienne, il peut joindre au dossier une enveloppe Chronopost afin de pouvoir recevoir le passeport visé.</p>
<p><i>Mise à jour : septembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-mexique-entree-et-sejour-article-animaux-domestiques.md" title="Animaux domestiques">Animaux domestiques</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-mexique-entree-et-sejour-article-vaccination.md" title="Vaccination">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-mexique-entree-et-sejour-article-demenagement.md" title="Déménagement">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-mexique-entree-et-sejour-article-passeport-visa-permis-de-travail-108383.md" title="Passeport, visa, permis de travail">Passeport, visa, permis de travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/entree-et-sejour/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
