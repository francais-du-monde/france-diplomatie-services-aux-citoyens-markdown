# La réintégration dans la nationalité française

<p>Les personnes qui ont perdu la nationalité française à raison du mariage avec un étranger ou de l’acquisition par mesure individuelle d’une nationalité étrangère, peuvent, sous certaines conditions, être réintégrées par déclaration souscrite devant le juge d’instance ou, lorsqu’elles résident à l’étranger, devant le consul de France territorialement compétent. Elles doivent avoir conservé ou acquis avec la France des liens manifestes, notamment d’ordre culturel, professionnel, économique ou familial.</p>
<p>Sont formellement exclues de ce cas de réintégration par déclaration, les personnes ayant perdu la nationalité française par l’effet de l’accession à l’indépendance des anciens territoires français, ou par décret de libération des liens d’allégeance, ou du fait d’une déclaration de perte souscrite postérieurement au mariage avec un étranger. Ces personnes peuvent éventuellement réintégrer la nationalité française par décret, sans condition de stage, sous réserve qu’elles remplissent par ailleurs les conditions relatives à la naturalisation, et notamment la condition de résidence en France au moment de la signature du décret.</p>
<p><i>Mise à jour : mars 2016</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/nationalite-francaise/article/la-reintegration-dans-la-nationalite-francaise). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
