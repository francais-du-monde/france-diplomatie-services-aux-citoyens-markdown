# Passeport, visa, permis de travail

<p>Les citoyens Français désireux de s’établir au Venezuela doivent solliciter un visa de long séjour auprès de <a href="http://francia.embajada.gob.ve/" class="spip_out" rel="external">l’ambassade du Venezuela à Paris</a>. Une fois sur place il vous sera possible de demander la prolongation de votre visa, de retirer votre « cédula de extranjero » (équivalent à notre carte de séjour) ou de solliciter la nationalité vénézuélienne, à condition de remplir entre autres les conditions de résidence (cinq ans pour les conjoints de vénézuélien, dix ans pour les autres).</p>
<p>Pour plus d’informations vous pouvez consulter le <a href="http://www.saime.gob.ve/" class="spip_out" rel="external">service de l’immigration vénézuélien</a></p>
<h4 class="spip">Les différents types de visa</h4>
<h4 class="spip">Touristes</h4>
<p>Les plaisanciers doivent s’acquitter des démarches portuaires, à leur entrée dans le port. A plusieurs occasions, il a été fait état aux frontières par les services de l’immigration de la nécessité de détenir un visa pour les personnes rentrant par voie terrestres, or les ressortissants français peuvent séjourner au Venezuela jusqu’à 90 jours sans visa.</p>
<p><strong>Attention : le passeport doit être d’une validité minimum de 6 mois.</strong></p>
<h4 class="spip">Étudiants </h4>
<p>Les ressortissants français qui désirent étudier dans des établissements d’enseignement supérieur au Venezuela peuvent obtenir un visa étudiant. Attention, toutefois, il n’est en aucun cas possible d’exercer une activité rémunérée pendant un séjour en tant qu’étudiant dans le pays.</p>
<p>Les étudiants français souhaitant effectuer un stage sur le sol vénézuélien peuvent obtenir un visa de stagiaire à condition de disposer d’une convention de stage, d’un billet aller retour et d’une attestation de ressources financières.</p>
<h4 class="spip">Visa diplomatique de courtoisie ou de fonctionnaire</h4>
<p>Il est délivré suite à la demande d’une institution ou d’un organisme de l’état vénézuélien ou français.</p>
<h4 class="spip">Visa d’affaires </h4>
<p>Il est délivré aux commerçants, exécutifs, représentants d’entreprises ou industries, qui souhaitent entrer sur le territoire pour effectuer des activités ou des transactions commerciales, financières ou toute autre activité lucrative légale en rapport avec leurs affaires. Toutes les formalités doivent être effectuées le plus rapidement possible avant la date du départ, en raison des délais de délivrance du visa.</p>
<h4 class="spip">Visa de travail</h4>
<p>Le visa de travail est délivré aux étrangers engagés par des sociétés privées ou publiques, aux directeurs, techniciens et personnel administratif expatriés par des sociétés à l’étranger. L’autorisation du visa de travail est de la compétence du ministère du Pouvoir populaire pour les relations intérieures. Cette démarche est à effectuer dans les plus brefs délais ; la délivrance du visa pouvant prendre plusieurs mois.</p>
<h4 class="spip">Les conjoints </h4>
<p>Pour les personnes mariées à un citoyen vénézuélien il est possible d’obtenir un visa de séjour temporaire d’une durée d’un an. Ce visa est délivré au conjoint étranger ainsi qu’aux enfants mineurs.</p>
<h4 class="spip">Autres visas</h4>
<ul class="spip">
<li>Visa de producteur étranger en audiovisuel ;</li>
<li>Visa d’investisseur pour les personnes travaillant dans des entreprises ou industries qui ont une filiale au Venezuela. Ce visa est valable pour une durée de 3 ans avec entrées multiples sur le territoire ;</li>
<li>Visa pour les représentants religieux ;</li>
<li>Visa de réintégration pour les résidents du Venezuela qui ont séjourné dans un autre pays pendant une durée maximale de deux années consécutives ;</li>
<li>Visa de rentier ;</li>
<li>Visa de transit.</li></ul>
<h4 class="spip">Sites Internet à consulter</h4>
<ul class="spip">
<li><a href="http://francia.embajada.gob.ve/" class="spip_out" rel="external">Ambassade du Venezuela en France</a></li>
<li><a href="http://www.ambafrance-ve.org/?Formalites-d-entree-et-d" class="spip_out" rel="external">Rubrique dédiée sur le site de l’ambassade de France au Venezuela</a></li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/entree-et-sejour/article/passeport-visa-permis-de-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
