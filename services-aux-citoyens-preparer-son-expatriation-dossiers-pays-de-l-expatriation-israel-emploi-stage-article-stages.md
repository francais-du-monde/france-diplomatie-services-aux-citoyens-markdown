# Stages

<p>Le stage qu’effectue un étranger en Israël n’est pas considéré comme un « emploi ». Le stagiaire n’est donc pas soumis aux mêmes règles de visa et de travail que les Israéliens, les nouveaux immigrants ou les travailleurs immigrés. Un ressortissant étranger, qui n’a pas le statut d’immigrant selon la <i>loi du retour</i>, peut effectuer un stage professionnel de quelques mois en Israël avec son visa de touriste valable trois mois. Le visa peut être renouvelé pour trois mois de séjour supplémentaire en Israël. Lorsque le stage est organisé par l’intermédiaire d’une association officielle (comme l’Agence juive), un visa pour toute la durée du stage est fourni d’avance au stagiaire. Plus récemment, le gouvernement israélien accepte de délivrer des visas B/4 (volontaires étrangers) pour les Français en qualité de « volontaires du service en entreprises ».</p>
<p>Un stage effectué en qualité de touriste permet au stagiaire juif (futur immigrant ou Israélien expatrié à l’étranger) de ne pas entamer ses droits aux différents services fournis par les organes officiels de l’immigration (Agence juive et ministère de l’Intégration). Le candidat à un stage devra choisir son statut et son visa de séjour en prenant en compte l’éventualité d’une émigration future en Israël.</p>
<p><strong>Source : </strong> <a href="http://www.israelvalley.com/faq" class="spip_out" rel="external">FAQ de la chambre de commerce France-Israël</a></p>
<h4 class="spip">Avez-vous pensé au volontariat international (V.I.E ou V.I.A.) ?</h4>
<p>Le V.I.E. permet aux entreprises françaises de confier à un jeune ressortissant de l’espace économique européen, âgé de 18 à 28 ans, une mission professionnelle (rémunérée) à l’étranger durant une période modulable de 6 à 24 mois.</p>
<p>Un cadre juridique adéquat permet aujourd’hui d’effectuer un V.I.E. ou V.I.A. en Israël sans difficulté (ce n’était pas le cas avant). Le gouvernement israélien accepte désormais de délivrer des visas B4 pour les V.I.E.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/emplois-stages-concours/volontaires-internationaux/" class="spip_in">Volontaires internationaux</a></li>
<li><a href="http://www.civiweb.com/FR/index.aspx" class="spip_out" rel="external">http://www.civiweb.com/FR/index.aspx</a></li>
<li><a href="http://export.businessfrance.fr/default.html" class="spip_out" rel="external">http://www.businessfrance.fr</a> - <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/emploi-stage/article/stages#infoVIE#mc#ubifrance.fr#" title="infoVIE..åt..ubifrance.fr" onclick="location.href=mc_lancerlien('infoVIE','ubifrance.fr'); return false;" class="spip_mail">Courriel</a></li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/emploi-stage/article/stages). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
