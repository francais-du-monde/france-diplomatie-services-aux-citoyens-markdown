# Communications

<h4 class="spip">Téléphone – Internet</h4>
<p>Les liaisons téléphoniques sont bonnes. Les communications s’obtiennent facilement par l’automatique (composer le 00 972 pour l’indicatif téléphonique) ou par central international.</p>
<p>L’accès au réseau par un fournisseur israélien ne pose aucun problème. Les services proposés (qualité de la liaison, ADSL, etc.) sont comparables à ceux existant en France, à un coût cependant supérieur. L’internet est très répandu à Jérusalem Ouest et l’accès WIFI est en pleine expansion.</p>
<p>Téléphoner gratuitement par Internet Il est généralement très avantageux d’utiliser <strong>des logiciels de téléphonie sur IP</strong> (Skype, Google talk, Live messenger, Yahoo ! messenger, etc.) car les tarifs proposés à l’international sont souvent moindre que ceux des opérateurs locaux exploitant des lignes classiques, voire gratuits dans certaines conditions.</p>
<h4 class="spip">Poste</h4>
<p>Les liaisons postales sont quotidiennes. Il faut compter une dizaine de jours d’acheminement pour le courrier à destination ou en provenance de France. Les bureaux de poste principaux sont ouverts du dimanche au jeudi de 7 h à 18 h et le vendredi de 7 h à 12 h. Ils sont fermés les lundis, samedis et jours de fêtes.</p>
<p><a href="http://www.israelpost.co.il/" class="spip_out" rel="external">Site internet de la poste en Israël</a></p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/vie-pratique/article/communications). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
