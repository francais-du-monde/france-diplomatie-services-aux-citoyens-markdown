# Fiscalité du pays

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/fiscalite-22708/article/fiscalite-du-pays-108668#sommaire_1">Inscription auprès du fisc</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/fiscalite-22708/article/fiscalite-du-pays-108668#sommaire_2">Impôts directs et indirects</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/fiscalite-22708/article/fiscalite-du-pays-108668#sommaire_3">Impôts locaux </a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/fiscalite-22708/article/fiscalite-du-pays-108668#sommaire_4">Droits et taxes sur le commerce international</a></li></ul>
<p>Seul le gouvernement fédéral impose un impôt sur le revenu des particuliers. Cet impôt est généralement progressif et est fonction des revenus perçus. Trois sources de revenus sont soumises à l’impôt :</p>
<ul class="spip">
<li>l’impôt sur les salaires professionnels avec un taux progressif. Des réductions d’impôt sont prévues ;</li>
<li>l’impôt à taux fixe (30 %) sur les revenus commerciaux et sur les sociétés ;</li>
<li>la taxe sur les gains en capital redevable au moment de la réalisation des gains.</li></ul>
<p>L’année fiscale (<i>financial year</i>, <i>fiscal year</i>, ou encore <i>budget year</i>) commence le 1er juillet et se termine le 30 juin de l’année suivante. Toute personne physique résidente, dont le revenu imposable est supérieur au seuil de non-imposition, doit remplir une déclaration d’impôt avant le 31 octobre de l’année en cours. Un délai supplémentaire peut, dans certains cas, être accordé. De même, si la déclaration est déposée auprès d’un expert-comptable, le délai est alors fixé au 30 juin de l’année suivante, à condition d’être enregistré auprès de ce comptable avant le 31 octobre de l’année en cours.</p>
<p>Pour les salariés, la retenue à la source exige des employeurs d’établir, en fonction des salaires, le montant du revenu imposable et de payer l’impôt correspondant à l’administration fiscale.</p>
<p>Les règles d’imposition sont fixées par <i>The Income Tax Assessment Act </i>- ITAA, ainsi que par des lois complémentaires et annexes. L’une d’entre elles, la plus importante, détermine les taux de l’impôt sur le revenu (<i>The Income Tax Rates Act </i>1986 et <i>the Income Tax Act </i>1986) qui s’applique à toutes les catégories de contribuables : sociétés, entreprises, personnes physiques, fiduciaires, etc.</p>
<p>Pour en savoir plus, vous pouvez consulter le site Internet du <a href="http://www.ato.gov.au/" class="spip_out" rel="external">centre australien des impôts</a> (<i>Australian Taxation Office) </i>qui offre de nombreux services en ligne.</p>
<h3 class="spip"><a id="sommaire_1"></a>Inscription auprès du fisc</h3>
<p><strong>Définition de la résidence fiscale</strong></p>
<p>Une personne physique est considérée comme ayant sa résidence fiscale en Australie, indépendamment de sa nationalité et sous réserve de l’application des dispositions de la convention fiscale franco-australienne, lorsque son domicile habituel ou son lieu de résidence ou son habitation permanente est situé en Australie. Afin de déterminer la résidence fiscale, le droit fiscal australien utilise différents critères tels que les liens personnels et la durée de séjour sur son territoire :</p>
<ul class="spip">
<li>être établi en Australie de façon permanente ;</li>
<li>être domicilié en Australie et ne pas avoir d’autres domiciles à l’étranger ;</li>
<li>ne pas être domicilié en Australie, mais y avoir séjourné et occupé le même logement pendant au moins 183 jours (six mois) au cours de l’année fiscale écoulée, tout en justifiant d’un emploi fixe pour la majorité de la même période ;</li>
<li>séjourner à l’étranger dans le cadre d’un emploi d’une durée inférieure à deux ans et planifier un retour en Australie ;</li>
<li>avoir un domicile permanent ou une résidence habituelle ou des attaches personnelles et des engagements économiques en Australie.</li></ul>
<p>Une personne qui immigre en Australie avec pour objectif de s’y s’installer de façon permanente est considérée comme résidente à partir de la date de son arrivée sur le territoire australien. A contrario, est considérée comme non-résident fiscal la personne physique qui a l’intention de séjourner moins de 183 jours sur le territoire australien, celle qui n’a pas l’intention de s’installer en Australie de façon permanente et dont le domicile est situé à l’étranger.</p>
<h3 class="spip"><a id="sommaire_2"></a>Impôts directs et indirects</h3>
<p><strong>Barème de l’impôt</strong></p>
<p>Le barème de l’impôt est fonction de votre statut au regard du séjour. Les personnes non-résidentes, y compris les travailleurs temporaires saisonniers, qui perçoivent un salaire ou une rémunération en Australie doivent se reporter à la <a href="http://www.impots.gouv.fr/" class="spip_out" rel="external">convention fiscale</a> signée entre la France et l’Australie.</p>
<p>Cependant, toute personne exerçant une activité déclarée et rémunérée en Australie, quel que soit son statut au regard du séjour, doit être titulaire d’un numéro d’immatriculation fiscale (<i>Tax File Number</i> - TFN). Celui-ci s’obtient directement sur le site Internet du <a href="http://www.ato.gov.au/" class="spip_out" rel="external">centre australien des impôts</a> (<i>Australian Taxation Office</i>).</p>
<p>Les contribuables peuvent bénéficier d’un certain nombre de réductions d’impôts. L’impôt sur le revenu étant prélevé à la source, il en résultera un crédit/remboursement d’impôt ou une dette fiscale (<i>tax refund </i>ou<i> debt</i>).</p>
<p><strong>Barème de l’impôt</strong> (<i>income</i> <i>tax rates</i>) <strong>au 1er juillet 2012 pour les résidents</strong></p>
<p>Les montants n’incluent pas la cotisation assurance maladie (<i>Medicare Levy</i>) de 1,5%. Pour de plus amples informations sur cette cotisation (réduction, exemption totale, surcharge) : <a href="http://www.ato.gov.au/" class="spip_out" rel="external">www.ato.gov.au</a> rubrique « Individuals / Tax topics A-Z / Medicare Levy ».</p>
<p>Le système fiscal australien offre une gamme d’avantages fiscaux pour soutenir les familles, (compensations fiscales, remboursement d’impôt pour études…) repris dans le programme « <i>Tax for Families</i> ».</p>
<p><strong>Barème de l’impôt</strong> (<i>income</i> <i>tax rates</i>) <strong>pour les non-résidents</strong></p>
<p>Les résidents étrangers ne sont pas soumis au prélèvement assurance maladie (<i>Medicare Levy</i>).</p>
<p><strong>Départ d’Australie</strong></p>
<p>Il est essentiel de prévenir le <a href="http://www.ato.gov.au/" class="spip_out" rel="external">centre australien des impôts</a> de votre départ définitif et de votre situation professionnelle à l’égard de l’Australie suite à votre départ.</p>
<p><strong>Solde du compte en fin de séjour</strong></p>
<p>Il est possible d’effectuer sa déclaration de revenus avant la fin de l’année fiscale (<i>lodging an early tax return</i>) si les conditions suivantes sont remplies :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  pour les non-résidents fiscaux en Australie :
<br>— départ définitif d’Australie ;
<br>— ne plus percevoir de revenus provenant d’une activité salariale en Australie (en dehors des intérêts bancaires et de revenus tirés de redevances).</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  pour les résidents permanents fiscaux en Australie :
<br>— départ définitif de l’Australie ;
<br>— ne plus être considéré comme résident permanent fiscal en Australie ;
<br>— ne plus percevoir de revenus provenant d’une activité salariale en Australie (en dehors des intérêts bancaires et de revenus tirés de redevances).</p>
<p>En cas de départ temporaire d’Australie vous devez effectuer votre déclaration de revenus dans les délais prescrits du 1er juillet au 31 octobre.</p>
<p>Un service en ligne gratuit est également mis à la disposition des contribuables en vue d’effectuer leur déclaration de revenus. Se reporter au site Internet du <a href="http://www.ato.gov.au/" class="spip_out" rel="external">centre australien des impôts</a> : rubrique « Individuals / Lodge your tax return / E-tax ».</p>
<p><strong>Coordonnées des centres d’information fiscale</strong></p>
<p>Pour obtenir des informations sur le système fiscal australien, vous pouvez :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  vous rendre en personne à l’un des bureaux (<i>shopfronts</i>) dont l’adresse figure sur le site Internet du <a href="http://www.ato.gov.au/" class="spip_out" rel="external">centre australien des impôts</a> (<i>Australian Taxation Office</i>). Ces bureaux sont ouverts du lundi au vendredi de 8 heures 30 à 16 heures 45 et sont répartis sur tout le territoire australien.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  appeler le numéro 13 28 61 (uniquement pour les demandes des particuliers) du lundi au vendredi de 8 heures à 18 heures. Les autres numéros de téléphone sont disponibles en ligne.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  écrire au bureau des impôts de la capitale de votre Etat ou Territoire de résidence : Australian Tax Office - GPO Box 9990</p>
<h3 class="spip"><a id="sommaire_3"></a>Impôts locaux </h3>
<p>Les impôts locaux en Australie regroupent deux taxes distinctes : la taxe d’habitation (<i>Residential Land Tax</i>), que paient les propriétaires d‘un bien immobilier occupé par un locataire et la taxe foncière (<i>Rates</i>), que paient les propriétaires d’un terrain ou d’une parcelle de terrain. Leurs montants, ainsi que leurs cycles de paiement sont détermimés par les conseils locaux. Pour plus d’informations sur ces impôts, consulter les sites des bureaux de recettes (Revenue Offices) des Etats et Territoires australiens.</p>
<p>L’Australie applique également un droit de timbre lors de la vente d’une propriété, d’un terrain ou d’un espace commercial ; ce droit est à payer par l’acheteur au bureau de recettes (Revenue Office) de l’État.</p>
<h3 class="spip"><a id="sommaire_4"></a>Droits et taxes sur le commerce international</h3>
<p><strong>Taux des droits de douane</strong></p>
<p>Le taux des droits de douane en Australie est déterminé en fonction de la catégorie dans laquelle le produit importé est classé dans le tarif douanier. Dans certaines circonstances, des mesures antidumping ou compensatoires entraînant l’imposition de taux majorés de droits de douane peuvent également être appliquées. Pour consulter le tarif en cours des <a href="http://www.customs.gov.au/" class="spip_out" rel="external">douanes</a> (<i>Australian Customs  Border Protection Service</i>).</p>
<p><strong>Taxes indirectes</strong></p>
<p>Les marchandises importées peuvent être assujetties à une ou plusieurs taxes indirectes, notamment :</p>
<ul class="spip">
<li>la taxe sur les produits et services (<i>Goods and Services Tax - GST</i>) ;</li>
<li>la taxe de compensation sur le vin (<i>Wine Equalisation Tax - WET</i>) ;</li>
<li>la taxe sur les voitures de luxe(<i>Luxury Car Tax – LCT)</i>.</li></ul>
<p>Pour obtenir des renseignements concernant les conditions applicables aux trois taxes sus-mentionnées vous pouvez consulter le site internet de la <a href="http://www.customs.gov.au/" class="spip_out" rel="external">douane australienne</a> .</p>
<p><i>Mise à jour : octobre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/fiscalite-22708/article/fiscalite-du-pays-108668). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
