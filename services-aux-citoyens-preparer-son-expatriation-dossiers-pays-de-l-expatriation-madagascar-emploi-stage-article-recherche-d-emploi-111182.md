# Recherche d’emploi

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/emploi-stage/article/recherche-d-emploi-111182#sommaire_1">Média</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/emploi-stage/article/recherche-d-emploi-111182#sommaire_2">Annuaires d’entreprises</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/emploi-stage/article/recherche-d-emploi-111182#sommaire_3">Organismes sur place pour la recherche d’emploi</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Média</h3>
<p>La plupart des journaux ont une rubrique « emploi ». Les plus intéressantes seront trouvées dans « Les Nouvelles » ou « l’Express ». Les sites web sont de plus en plus consultés (Telma ou Orange sont probablement les plus efficaces).</p>
<h3 class="spip"><a id="sommaire_2"></a>Annuaires d’entreprises</h3>
<p>Presqu’inexistants, signalons toutefois l’arrivée de quelques guides utiles (mianjaika_com<span class="spancrypt"> [at] </span>yahoo.fr : un répertoire sur l’industrie et le commerce, un sur le tourisme et un nouveau sur Tananarive) et surtout les sites des associations professionnelles (GEM, SIM, Goticom, GEFP….).</p>
<p>Se renseigner auprès d’Ubifrance qui publie la liste des filiales/principales sociétés à capitaux privés français à Madagascar.</p>
<h3 class="spip"><a id="sommaire_3"></a>Organismes sur place pour la recherche d’emploi</h3>
<p><a href="http://www.ambafrance-mada.org/" class="spip_out" rel="external">Comité consulaire pour l’emploi et la formation professionnelle (CCPEFP)</a><br class="manualbr">c/o consulat général de France<br class="manualbr">3 rue Jean Jaurès - Ambatoména - Tananarive<br class="manualbr">Tél. : [261] 20 22 398 50 <br class="manualbr">Télécopie : [261] 20 22 398 84<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/emploi-stage/article/recherche-d-emploi-111182#mino.rabemanana#mc#diplomatie.gouv.fr#" title="mino.rabemanana..åt..diplomatie.gouv.fr" onclick="location.href=mc_lancerlien('mino.rabemanana','diplomatie.gouv.fr'); return false;" class="spip_mail">Courriel</a></p>
<p>Il existe un cabinet privé français pour le recrutement des cadres à Tananarive (et la gestion du personnel externalisé) :</p>
<p><strong>E = mc2 - International - Consulting</strong><br class="manualbr">Lot II J 170 Ivandry<br class="manualbr">BP 121 101 Tananarive<br class="manualbr">Tél. : (+261) 20 22 424 15 - 24 271 87 <br class="manualbr">Fax : (+261) 20 22 424 15<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/emploi-stage/article/recherche-d-emploi-111182#jvpoumett#mc#emc2-international.com#" title="jvpoumett..åt..emc2-international.com" onclick="location.href=mc_lancerlien('jvpoumett','emc2-international.com'); return false;" class="spip_mail">Courriel</a></p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/emploi-stage/article/recherche-d-emploi-111182). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
