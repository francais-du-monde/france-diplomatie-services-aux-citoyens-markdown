# Etat civil

<p>Le ministère des Affaires étrangères établit et conserve les actes d’état civil relatifs aux naissances, aux mariages, aux reconnaissances, ou aux décès <strong>qui surviennent à l’étranger et qui concernent des ressortissants français</strong>. Il procède également à la mise à jour de ces actes par apposition de mentions et en délivre des copies et des extraits aux requérants habilités.</p>
<p>Pour assurer cette mission, le ministère dispose d’officiers de l’état civil dans les ambassades et consulats français à l’étranger ainsi qu’au Service central d’état civil à Nantes.</p>
<p>La naissance, le mariage ou le décès d’un ressortissant français, qui a préalablement été enregistré à l’état civil local, peut ensuite être transcrit dans les registres de l’état civil français de l’ambassade ou le consulat territorialement compétent. Des copies et des extraits de l’acte peuvent ultérieurement être obtenus soit auprès de l’ambassade ou du consulat qui a procédé à la transcription, soit auprès du Service central d’état civil.</p>
<p>Une situation particulière prévaut pour les ressortissants français qui résident ou ont été confrontés à un évènement de vie (naissance, mariage, décès par exemple) en Algérie, au Maroc ou en Tunisie, puisque la transcription des actes d’état civil dressés par les autorités locales ou par les autorités consulaires françaises dans ces trois pays est exclusivement réalisée au Service central d’état civil à Nantes qui agit alors comme un poste consulaire.</p>
<p>Pour les ressortissants français résidant dans un pays sans représentation diplomatique française, la transcription des actes enregistrés à l’état civil local peut être effectuée directement par le Service central d’état civil. La demande et les justificatifs à produire sont les mêmes que pour une transcription par une ambassade ou un consulat français.</p>
<p>Enfin, le Service central d’état civil n’est pas compétent pour les actes d’état civil établis dans les collectivités territoriales, les départements et territoires d’Outre-Mer (Martinique, Guadeloupe, Guyane, Réunion, Saint-Pierre et Miquelon, Mayotte, Nouvelle-Calédonie, Polynésie française, Iles Wallis et Futuna).</p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-etat-civil-et-nationalite-francaise-etat-civil-article-demander-la-copie-d-un-acte-d-etat-civil.md" title="Demander la copie d’un acte d’état civil">Demander la copie d’un acte d’état civil</a></li>
<li><a href="services-aux-citoyens-etat-civil-et-nationalite-francaise-etat-civil-article-les-naissances-a-l-etranger.md" title="Déclarer la naissance d’un enfant à l’étranger">Déclarer la naissance d’un enfant à l’étranger</a></li>
<li><a href="services-aux-citoyens-etat-civil-et-nationalite-francaise-etat-civil-article-les-mariages-a-l-etranger.md" title="Se marier à l’étranger">Se marier à l’étranger</a></li>
<li><a href="services-aux-citoyens-etat-civil-et-nationalite-francaise-etat-civil-article-comment-etablir-un-pacte-de.md" title="Enregistrer un PACS à l’étranger">Enregistrer un PACS à l’étranger</a></li>
<li><a href="services-aux-citoyens-etat-civil-et-nationalite-francaise-etat-civil-article-les-divorces-a-l-etranger.md" title="Faire reconnaitre un divorce prononcé à l’étranger">Faire reconnaitre un divorce prononcé à l’étranger</a></li>
<li><a href="services-aux-citoyens-etat-civil-et-nationalite-francaise-etat-civil-article-les-deces-et-les-disparitions-a-l.md" title="Déclarer un décès à l’étranger">Déclarer un décès à l’étranger</a></li>
<li><a href="services-aux-citoyens-etat-civil-et-nationalite-francaise-etat-civil-article-livrets-de-famille.md" title="Actualiser son livret de famille ">Actualiser son livret de famille </a></li>
<li><a href="services-aux-citoyens-etat-civil-et-nationalite-francaise-etat-civil-article-rectification-d-un-acte-d-etat.md" title="Demander la rectification d’un acte d’état civil">Demander la rectification d’un acte d’état civil</a></li>
<li><a href="services-aux-citoyens-etat-civil-et-nationalite-francaise-etat-civil-article-reconstitution-d-actes-d-etat.md" title="La reconstitution d’actes d’état civil">La reconstitution d’actes d’état civil</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/etat-civil/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
