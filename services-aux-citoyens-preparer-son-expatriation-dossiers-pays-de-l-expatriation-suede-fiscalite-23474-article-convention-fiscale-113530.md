# Convention fiscale

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/fiscalite-23474/article/convention-fiscale-113530#sommaire_1">Champ d’application de la convention</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/fiscalite-23474/article/convention-fiscale-113530#sommaire_2">Règles d’imposition</a></li></ul>
<p>Une convention internationale ayant primauté sur la loi interne, les dispositions de la loi du 29 décembre 1976 modifiant les règles de territorialité et les conditions d’imposition des Français de l’étranger (Journal Officiel du 30 décembre 1976) ne sont applicables que si elles ne contreviennent pas aux dispositions de la convention.</p>
<p><strong>La France et la Suède ont signé deux conventions fiscales destinées à éviter les doubles impositions.</strong> La première, signée à Stockholm le 27 novembre 1990 et entrée en vigueur le 1er avril 1992, concerne les impôts sur le revenu et sur la fortune ; la seconde, signée le 8 juin 1994 et entrée en vigueur le 1er janvier 1996, concerne les impôts sur les successions et sur les donations.</p>
<p><strong>Voici un résumé de ses points clés pour les particuliers : </strong></p>
<p>Cette Convention tend à éviter les doubles impositions qui pourraient résulter de l’application des législations des deux Etats, de prévenir l’évasion fiscale et d’établir des règles d’assistance administrative réciproques en matière d’impôt sur le revenu et sur la fortune.</p>
<p>Le texte de la Convention et de son avenant peut être obtenu à la Direction des Journaux Officiels :</p>
<ul class="spip">
<li>par courrier : <br class="manualbr">26 rue Desaix, <br class="manualbr">75727 PARIS Cedex 15</li>
<li>par télécopie : 01.40.58.77.80.</li></ul>
<p>Il est également consultable sur le site Internet <a href="http://www.impots.gouv.fr/" class="spip_out" rel="external">de l’administration fiscale</a>.</p>
<p>Les dispositions conventionnelles qui ont primauté sur les dispositions du droit interne, selon l’article 55 de la Constitution française répartissent entre les deux Etats, le droit d’imposer les revenus pour leurs résidents respectifs.</p>
<h3 class="spip"><a id="sommaire_1"></a>Champ d’application de la convention</h3>
<p>Cet accord a pour objet de protéger les résidents de chacun des Etats contractants en matière d’impôts prélevés directement sur le revenu et sur la fortune, quel que soit le mode de perception.</p>
<p>La convention trouve donc à s’appliquer aux résidents de ces Etats.</p>
<p>L’article 4 de la Convention précise que l’expression « résident d’un Etat » désigne toute personne qui, en vertu de la législation de cet Etat y est assujettie à l’impôt à raison de son domicile, de sa résidence, de son siège de direction ou de tout autre critère analogue. Cet article énumère également des critères subsidiaires permettant de résoudre le cas de double résidence si l’assujettissement à l’impôt ne pouvait suffire.</p>
<p><strong>Ces critères sont :</strong></p>
<ul class="spip">
<li>le foyer d’habitation permanent ;</li>
<li>le centre de ses intérêts vitaux ;</li>
<li>le lieu de séjour habituel ;</li>
<li>à défaut, l’Etat dont elle possède la nationalité.</li></ul>
<p>Si une personne possède la double nationalité ou si elle ne possède la nationalité d’aucun d’eux, la question est tranchée d’un commun accord par les autorités des deux Etats contractants.</p>
<p><strong>Revenus concernés</strong></p>
<p>Les revenus concernés sont :</p>
<ul class="spip">
<li>les traitements et rémunérations : les traitements et salaires d’origine privée qu’un résident reçoit au titre d’un emploi salarié sont imposables dans l’Etat où est exercée l’activité ;</li>
<li>les revenus des biens immobiliers y compris les bénéfices des exploitations agricoles ou forestières sont imposables dans l’Etat où sont situés ces biens ;</li>
<li>les pensions de retraite de source privée ainsi que les rémunérations similaires sont imposables dans l’Etat dont le bénéficiaire est un résident ;</li>
<li>les bénéfices des entreprises : les entreprises industrielles et commerciales sont imposables sur le territoire où est situé l’établissement stable ;</li>
<li>les bénéfices provenant de l’exploitation de navires ou d’aéronefs ne sont imposables que dans l’Etat où se trouve la direction effective de l’entreprise.</li></ul>
<p><strong>Etudiants</strong></p>
<p>Les étudiants, les stagiaires d’un Etat qui séjournent dans l’autre Etat à seule fin d’y poursuivre leurs études ou leur formation n’y sont pas imposables si les sommes perçues pour couvrir leurs frais d’entretien, d’études ou de formation sont d’origine étrangère.</p>
<h3 class="spip"><a id="sommaire_2"></a>Règles d’imposition</h3>
<p>L’article 23 de la Convention prévoit les dispositions pour éviter la double imposition.</p>
<p>L’élimination de la double imposition pour les résidents de France qui perçoivent des revenus provenant de Suède, imposables dans cet Etat en application de la Convention, s’opère aux termes du paragraphe 1 de l’article 23 selon le régime de l’imputation, sur l’impôt français, d’un crédit d’impôt.</p>
<p>L’élimination de la double imposition pour les résidents en Suède qui perçoivent des revenus imposables en France s’opère par une déduction, sur l’impôt perçu par la Suède, d’un montant égal à l’impôt sur le revenu payé en France.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/fiscalite-23474/article/convention-fiscale-113530). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
