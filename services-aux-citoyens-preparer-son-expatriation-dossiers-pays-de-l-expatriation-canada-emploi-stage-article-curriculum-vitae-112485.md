# Curriculum vitae

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/emploi-stage/article/curriculum-vitae-112485#sommaire_1">Rédaction</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/emploi-stage/article/curriculum-vitae-112485#sommaire_2">Diplômes</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/emploi-stage/article/curriculum-vitae-112485#sommaire_3">Modèles de CV</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Rédaction</h3>
<ul class="spip">
<li>Le curriculum vitae canadien (en anglais <i>résumé</i>), qu’il soit rédigé en anglais ou en français, est semblable au curriculum vitae américain. Il privilégie la chronologie inversée et doit mentionner l’objectif de carrière (<i>career objective</i>). La rubrique " expérience professionnelle " (<i>work experience</i>) est rédigée dans un style télégraphique, en évitant l’emploi du "je". Les renseignements contenus dans cette dernière rubrique sont ordonnés de la façon suivante : tout d’abord la fonction occupée, suivie du nom de l’entreprise et de la période d’activité. Il n’est cependant pas nécessaire d’indiquer les coordonnées détaillées de vos employeurs.</li>
<li>L’Amérique du Nord est très friande de chiffres qui sont synonymes de résultats tangibles. N’hésitez pas à les intégrer dans votre curriculum vitae. Il peut s’agir des résultats scolaires et universitaires lorsqu’ils sont bons, du classement dans la promotion, etc..</li>
<li>L’équivalence en anglais de vos diplômes français doit être clairement indiquée.</li>
<li>Le bilinguisme français-anglais doit être indiqué dès le début du CV et ne doit pas être mentionné dans une rubrique indépendante. Lorsqu’il est envoyé par la poste, le curriculum vitae ne doit être ni plié, ni agrafé et être placé dans une grande enveloppe.</li>
<li>Mentionnez les périodes de stage et de bénévolat surtout si elles ont été effectuées au Canada.</li>
<li>Contrairement au curriculum vitae français qui doit tenir sur une seule page, les Canadiens acceptent les curriculums vitae de deux ou trois pages. Il est donc inutile d’essayer de gagner de l’espace en écrivant en minuscules ou en résumant les emplois occupés.</li>
<li>Le curriculum vitae ne doit pas être manuscrit, mais dactylographié sur un ordinateur. La police de caractères utilisée doit être claire, lisible et sans fantaisie. La couleur est acceptée. L’objectif est de se distinguer, tout en restant discret.</li>
<li>Utilisez un vocabulaire simple et privilégiez les verbes d’action.</li>
<li>Evitez les ratures, ne pliez pas votre curriculum vitae et utilisez un papier de bonne qualité.</li>
<li>Les mentions proscrites sont l’âge, l’année de naissance, la nationalité ou l’origine, ainsi que le nombre d’enfants à charge. Le curriculum vitae doit être accompagné d’une lettre de motivation dactylographiée.</li>
<li>La photographie n’est pas obligatoire.</li></ul>
<p><strong>Points à ne pas mentionner dans un CV</strong></p>
<ul class="spip">
<li>L’âge ;</li>
<li>La situation familiale ;</li>
<li>Le statut au Canada (résident permanent, permis de travail, etc.).</li>
<li>Le service militaire (à moins qu’il n’ait un lien direct avec l’emploi auquel vous postulez) ;</li>
<li>Pas de photographie ;</li>
<li>Le numéro de sécurité sociale.</li></ul>
<p><strong>Points à impérativement respecter</strong></p>
<p>Rédigez votre CV dans un format lettre (8,5 pouces par 11 pouces) et non sur du papier au format A4 ;</p>
<p>Indiquez, le cas échéant, que des références d’anciens employeurs sont disponibles sur demande de façon à permettre à l’entreprise de procéder à des vérifications.</p>
<p>Mentionnez votre adhésion à des associations professionnelles et ses certifications techniques (par exemple : certification Microsoft) ;</p>
<h3 class="spip"><a id="sommaire_2"></a>Diplômes</h3>
<p>Aux yeux d’un employeur canadien ou québécois, un diplôme français signifie peu de chose. En effet, les noms des diplômes diffèrent d’un pays à l’autre et les diplômes correspondent souvent à des réalités très différentes. C’est le cas des diplômes d’ingénieur.</p>
<p>La reconnaissance des diplômes est la principale pierre d’achoppement pour les nouveaux arrivants français.</p>
<p>Pour le cas spécifique du Québec, juridiquement, il n’existe pas d’équivalence de diplômes entre la France et la Belle Province. Cependant, des accords (le dernier date du 6 juin 2005) prévoient que, par exemple, le baccalauréat français et le diplôme d’études collégiales québécois permettent tous deux d’accéder au premier cycle universitaire. Le diplôme a souvent plus d’importance en France. La notion d’école prestigieuse est inconnue au Québec. Certains diplômes (DUT, BTS, etc.) n’ont pas d’équivalent au Québec.</p>
<p>Dans un premier temps, vous devrez faire reconnaître vos diplômes par le système éducatif québécois en demandant une évaluation. Ne vous précipitez pas pour obtenir une équivalence de vos diplômes si vous sentez que vos compétences suffisent pour convaincre un employeur. Pour plus d’informations, adressez-vous à :</p>
<p><strong>Direction des équivalences et de l’administration des ententes de sécurité sociale</strong><br class="manualbr">360, rue McGill <br class="manualbr">Montréal, Québec H2Y 2E9<br class="manualbr">Téléphone : 1 [514] 873 56 47 <br class="manualbr">Télécopie : 1 [514] 873 87 01 <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/emploi-stage/article/curriculum-vitae-112485#equivalences#mc#immq.gouv.qc.ca#" title="equivalences..åt..immq.gouv.qc.ca" onclick="location.href=mc_lancerlien('equivalences','immq.gouv.qc.ca'); return false;" class="spip_mail">Courriel</a></p>
<p><strong>Entente franco-québécoise sur la reconnaissance mutuelle des qualifications professionnelles</strong></p>
<p>L’entente franco-québécoise sur la reconnaissance mutuelle des qualifications professionnelles a été signée entre la France et la province canadienne du Québec le 17 janvier 2008. Cette entente a permis l’élaboration de 69 Arrangements de Reconnaissance Mutuels (ARM) qui entrent progressivement en vigueur. Ils permettent, par exemple, aux Français titulaires de certains diplômes d’exercer au Québec des métiers ou des professions réglementés qui leur étaient auparavant fermés.</p>
<p>Pour en savoir plus sur la reconnaissance des diplômes, vous pouvez consulter le site Internet du <a href="http://www.cicdi.ca/" class="spip_out" rel="external">Centre d’information canadien sur les diplômes internationaux</a>.</p>
<h3 class="spip"><a id="sommaire_3"></a>Modèles de CV</h3>
<p><strong>Vous pouvez trouver des modèles de curriculum vitae sur les sites Internet suivants :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.immigration-quebec.gouv.qc.ca/" class="spip_out" rel="external">Immigration Québec</a>.</p>
<p><i>Mise à jour : avril 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/emploi-stage/article/curriculum-vitae-112485). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
