# Animaux domestiques

<h4 class="spip">Le pays de destination se trouve hors de l’Union européenne</h4>
<p>Certains pays réglementent l’entrée des animaux sur leur territoire (permis d’importation, quarantaine, interdiction). Prévoyez un délai d’au moins dix jours pour effectuer toutes les formalités, voire de plusieurs mois pour les pays exigeant une quarantaine.</p>
<p>Pour connaître les conditions exactes, vous devrez prendre contact :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Avec <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/article/annuaire-des-representations-francaises-a-l-etranger" class="spip_in">l’ambassade en France</a> du pays de destination. Si, au cours de son transport de la France vers le pays de destination, l’animal doit transiter par un ou plusieurs pays, vous devrez également vous conformer à la réglementation du ou des pays de transit de l’animal. A noter que certains pays refusent le transit d’animaux.
</p>
<p>Des informations générales sur la réglementation de nombreux pays sont également disponibles sur le site Internet de l’<a href="http://www.iatatravelcentre.com/" class="spip_out" rel="external">Association internationale du transport aérien (AITA)</a>, ainsi que sur celui de l’<a href="http://www.vet-alfort.fr/" class="spip_out" rel="external">Ecole nationale vétérinaire de Maisons-Alfort</a>.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Le cas échéant, avec la ou les compagnies aériennes pour connaître les conditions de transport de l’animal (en soute ou en cabine, normes des cages, nourriture, etc.).
</p>
<p>Pour connaître les normes internationales de transport des animaux et des cages, vous pouvez consulter le site Internet de l’<a href="http://www.iata.org/" class="spip_out" rel="external">Association internationale du transport aérien (AITA)</a>. Si l’ambassade dispose d’une information particulière, celle-ci doit être communiquée à votre vétérinaire traitant, titulaire d’un mandat sanitaire, qui vous guidera pour la suite.</p>
<p>Dans le cas où l’ambassade ne disposerait pas d’informations sur la réglementation sanitaire de son pays, vous devrez respecter, par défaut, les conditions suivantes :</p>
<ul class="spip">
<li>L’animal doit être en règle vis-à-vis des exigences réglementaires françaises en matière de santé et de protection animales. Pour plus de renseignements sur la réglementation française, vous pouvez prendre contact avec votre vétérinaire traitant, titulaire d’un mandat sanitaire, ou la Direction départementale des services vétérinaires (DDSV) de votre département. Vous trouverez les coordonnées des DDSV sur le site Internet du <a href="http://agriculture.gouv.fr/" class="spip_out" rel="external">ministère de l’Agriculture</a>.</li>
<li>Les documents suivants sont obligatoires pour l’animal et doivent être établis par le vétérinaire traitant, titulaire d’un mandat sanitaire :
<br>— identification par micropuce ou tatouage ;
<br>— certificat de vaccination contre la rage en cours de validité ;
<br>— certificat international de bonne santé, établi par le vétérinaire traitant, titulaire du mandat sanitaire, dans la semaine précédant le départ de France.</li>
<li>Il est également conseillé de faire procéder à un titrage des anticorps anti-rabiques dans un laboratoire agréé et de se munir du carnet de vaccination tenu à jour de l’animal. Vous devrez ensuite prendre rendez-vous avec le service " santé et protection animales " de la Direction départementale des services vétérinaires (DDSV) dont relève le vétérinaire traitant, pour la validation des documents établis par celui-ci.</li></ul>
<p>Pour en savoir plus :</p>
<ul class="spip">
<li><a href="http://agriculture.gouv.fr/" class="spip_out" rel="external">ministère de l’Agriculture</a></li>
<li>Notre article thématique <a href="services-aux-citoyens-preparer-son-expatriation-douanes-article-animaux-domestiques.md" class="spip_in">Animaux domestiques</a></li></ul>
<p><strong>Légalisation des documents</strong></p>
<p>Certains pays exigent que les documents validés par la DDSV soient ensuite légalisés ou munis de l’apostille. Il convient donc de se renseigner sur ce point auprès de l’<a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-etrangers-en-france/article/annuaire-des-representations-etrangeres-en-france" class="spip_in">ambassade du pays de destination</a>.</p>
<p>Pour connaître le régime de légalisation du pays de destination, vous pouvez également consulter le site Internet du <a href="services-aux-citoyens-legalisation-et-notariat-legalisation-et-certification-de-signatures.md" class="spip_in">ministère des Affaires étrangères</a>.</p>
<p>L’apostille s’obtient auprès des cours d’appels. Vous pouvez trouver leurs coordonnées sur le site Internet du <a href="http://www.justice.gouv.fr/" class="spip_out" rel="external">ministère de la Justice</a>.</p>
<p>La légalisation est effectuée par le bureau des légalisations du ministère des affaires étrangères. Pour toute information sur les légalisations, vous pouvez consulter le site Internet du <a href="services-aux-citoyens-legalisation-et-notariat-legalisation-et-certification-de-signatures.md" class="spip_in">ministère des Affaires étrangères</a> :</p>
<p><strong>Le bureau des légalisations</strong><br class="manualbr">57 boulevard des Invalides <br class="manualbr">75007 Paris<br class="manualbr">Téléphone (de 14 à 16 heures) : 01 53 69 38 28 / 01 53 69 38 29 <br class="manualbr">Télécopie : 01 53 69 38 31</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/entree-et-sejour/article/animaux-domestiques). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
