# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/emploi-stage/article/reglementation-du-travail-110019#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/emploi-stage/article/reglementation-du-travail-110019#sommaire_2">Contrat de travail – spécificités</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/emploi-stage/article/reglementation-du-travail-110019#sommaire_3">Fêtes légales</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/emploi-stage/article/reglementation-du-travail-110019#sommaire_4">Emploi du conjoint</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/emploi-stage/article/reglementation-du-travail-110019#sommaire_5">Création d’entreprises – spécificités</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<p>La Grande Assemblée Nationale de Turquie (GANT) a adopté le 22 mai 2003 une loi qui modifie en profondeur le code du travail qui était en vigueur depuis 1971.</p>
<p>Le nouveau code apporte de nouvelles définitions comme celle de l’employé, du travail sur « invitation », du transfert de l’employé, etc.</p>
<p>Conformément à l’<strong>article 4</strong> du code, les sportifs, les employés de maison (cependant, selon l’article 6 de la loi 5510 relative à la sécurité sociale, si l’employé de maison travaille régulièrement à la même adresse, il doit être déclaré à la sécurité sociale), les personnes réhabilitées, les apprentis, les transports maritimes et aériens, les exploitations agricoles et forestières qui emploient moins de 50 salariés, les emplois liés à l’agriculture familiale et à l’artisanat ne sont pas inclus dans le champ d’application du code.</p>
<p>Le code précise, dans son <strong>article 5</strong>, que nul ne peut faire de discriminations relatives à la langue, la race, le sexe, les idées politiques, la croyance philosophique et la religion sur le lieu de travail. Si une telle discrimination survient, une indemnité égale à quatre mois de salaires devra être versée au salarié. La charge de la preuve est toutefois à la charge du salarié en matière de discrimination.</p>
<p>En cas de transfert du lieu de travail, le code prévoit que tous les droits et dettes découlant des contrats de travail en cours seront transférés à « l’acheteur ». Selon l’<strong>article 6a</strong>, la responsabilité commune des deux employeurs sera limitée à deux ans après la transaction de vente. L’employeur, vendeur ou acheteur, ne pourra annuler les contrats de travail lors de cette transaction.</p>
<p>Selon l’article 7, l’employeur peut « transférer » son employé, à condition d’obtenir son accord écrit et de manière temporaire, sur un autre lieu de travail au sein du même groupe de sociétés ou bien « sous-traiter » son salarié à un autre employeur, à condition que le salarié effectue un travail identique à celui qu’il accomplissait chez son employeur initial. Le contrat de travail provisoire ne doit pas dépasser six mois et devra être établi par écrit. Il pourra être renouvelé deux fois (au maximum).</p>
<h4 class="spip">La durée légale du travail </h4>
<p>La durée légale du travail est de 45 heures hebdomadaires au maximum. Ce délai sera partagé de manière égale entre les jours de travail hebdomadaires, sous réserve de ne pas dépasser 11 heures par jour.</p>
<h4 class="spip">Les pauses </h4>
<p>Conformément à l’article 68 du code du travail, l’employé doit bénéficier d’une pause de 15 minutes pour 4 heures de travail ; de 30 minutes pour une période travaillée de 4 à 7 heures et demie de travail et d’une heure pour les travaux supérieurs à 7 heures et demie, par jour.</p>
<h4 class="spip">Les heures supplémentaires </h4>
<p>Selon l’article 41 du code du travail, le total des heures supplémentaires pratiquées ne devra pas dépasser 270 heures par an. Les heures supplémentaires sont majorées de 50% pour chaque heure de travail.</p>
<h4 class="spip">Les congés payés </h4>
<p>Conformément à l’article 53 du code du travail, les salariés turcs ont droit à des congés annuels payés, à condition d’être en poste depuis au moins un an (y compris la période d’essai).</p>
<table class="spip">
<thead><tr class="row_first"><th id="id2b81_c0">Ancienneté dans le poste</th><th id="id2b81_c1">Durée des congés payés</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="id2b81_c0">Entre 1 et 5 ans</td>
<td headers="id2b81_c1">14 jours par an</td></tr>
<tr class="row_even even">
<td headers="id2b81_c0">Entre 5 et 15 ans</td>
<td headers="id2b81_c1">20 jours par an</td></tr>
<tr class="row_odd odd">
<td headers="id2b81_c0">Plus de 15 ans</td>
<td headers="id2b81_c1">26 jours par an</td></tr>
</tbody>
</table>
<p>Les congés payés pour les personnes âgées de moins de 18 ans et de plus de 50 ans ne peuvent pas être inférieurs à 20 jours par an.</p>
<p>Les durées des congés payés citées ci-dessus peuvent toutefois être modifiées en fonction des conventions collectives et des dispositions du contrat de travail.</p>
<h4 class="spip">Congé de maternité </h4>
<p>Conformément à l’article 74 du code du travail, les employées ont droit à huit semaines avant et huit semaines après l’accouchement. Cette période de congé peut être prolongée sur présentation d’un rapport médical. Si l’employée le souhaite, un congé supplémentaire sans rémunération d’une durée maximale de six mois peut lui être accordé.</p>
<h3 class="spip"><a id="sommaire_2"></a>Contrat de travail – spécificités</h3>
<p>L’article 8 définit le contrat de travail comme « un contrat par lequel l’employé s’engage à travailler pour l’employeur et par lequel l’employeur s’engage à rémunérer l’employé ». Le contrat de travail pour une période égale ou supérieure à un an doit être rédigé par écrit. Ce document est exempté de droit de timbre et de taxes notariales. Dans les cas qui ne nécessitent pas l’élaboration d’un contrat de travail, l’employeur se trouve dans l’obligation de fournir à son employé un document détaillant les conditions de travail, la durée du travail (hebdomadaire ou journalière) et le salaire dans les deux mois à partir de la date de prise de fonction.</p>
<p>Pour les travaux qui ne nécessitent pas la présence de l’employé tout au long de la journée, un système d’emploi sur « invitation » est introduit par le code (article 14). Ce travail sera défini par un contrat spécial. La durée du travail (hebdomadaire, mensuelle ou annuelle) ne peut excéder vingt heures par semaine.</p>
<h4 class="spip">Contrat à durée déterminée</h4>
<p>Le contrat sera considéré à durée déterminée s’il s’agit d’un travail ayant un délai précis ou d’un travail précis qui doit être achevé ou d’un travail qui nécessite l’obtention d’un résultat précis.</p>
<h4 class="spip">Contrat à durée indéterminée</h4>
<p>Le contrat sera considéré à durée indéterminée si la relation de travail n’est pas établie pour un délai précis.</p>
<h4 class="spip">Contrat de groupe</h4>
<p>Conformément à l’article 16, l’employeur pourra signer un contrat de travail intitulé « contrat de groupe » avec un salarié qui représente un groupe de salariés. Le salaire de chaque employé doit être déterminé dans le contrat de travail. Aucune retenue ne sera effectuée sur les salaires des employés en faveur du représentant du groupe.</p>
<h4 class="spip">La période d’essai</h4>
<p>La période d’essai des nouveaux salariés ne peut pas dépasser deux mois. Ce délai peut être prolongé jusqu’à quatre mois par le biais des conventions collectives. Pendant cette période d’essai, les parties au contrat pourront librement rompre le contrat de travail sans avoir à respecter le délai de préavis et sans qu’aucune compensation ne soit due.</p>
<h4 class="spip">La rupture du contrat de travail</h4>
<p>Selon l’article 17 du code du travail, les périodes de préavis en fonction du nombre de mois travaillés sont les suivantes :</p>
<table class="spip">
<thead><tr class="row_first"><th id="idccb3_c0">Période travaillée  </th><th id="idccb3_c1">Délais de préavis</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="idccb3_c0">Période inférieure à 6 mois</td>
<td headers="idccb3_c1">2 semaines</td></tr>
<tr class="row_even even">
<td headers="idccb3_c0">Entre 6 et 18 mois</td>
<td headers="idccb3_c1">4 semaines</td></tr>
<tr class="row_odd odd">
<td headers="idccb3_c0">Entre 18 mois et 3 ans</td>
<td headers="idccb3_c1">6 semaines</td></tr>
<tr class="row_even even">
<td headers="idccb3_c0">Plus de 3 ans</td>
<td headers="idccb3_c1">8 semaines</td></tr>
</tbody>
</table>
<p>Ces périodes de préavis représentent un minimum. Elles peuvent être augmentées contractuellement. En cas de mauvais usage du droit de licenciement, l’employeur devra payer au salarié une indemnité égale à trois fois le délai de préavis.</p>
<h4 class="spip">La résiliation du contrat de travail</h4>
<p>Conformément à l’article 18 et pour les établissements qui comprennent trente employés au minimum, l’employeur est obligé de justifier le motif de résiliation du contrat de travail si l’employé possède une ancienneté de six mois. L’employé ne peut pas être licencié par son employeur pour les raisons suivantes :</p>
<ul class="spip">
<li>adhésion à un syndicat ou participation aux activités syndicales hors des heures de travail ou pendant les heures de travail avec l’accord de son employeur ;</li>
<li>représentation syndicale ;</li>
<li>demande auprès des administrations judiciaires pour le suivi de ses droits provenant de la réglementation ou du contrat de travail ;</li>
<li>idées et tendances politiques, origine ethnique et sociale, grossesse, race, sexe, état civil, religion ;</li>
<li>congé de maternité ;</li>
<li>maladies épidémiques pendant une durée temporaire.</li></ul>
<p>L’employeur est obligé d’annoncer la résiliation du contrat de travail à son employé par écrit et il devra préciser clairement le motif de licenciement.</p>
<p>Le contrat de travail d’un salarié ne pourra pas être annulé pour des raisons de comportement ou de baisse de rentabilité, sans recueillir au préalable la défense du salarié. Toutefois, l’employeur aura un droit de licenciement direct dans les cas « d’atteinte à la moralité ».</p>
<h4 class="spip">Opposition à la résiliation du contrat</h4>
<p>Le salarié, dont le contrat de travail a été résilié, pourra saisir le Tribunal des Prud’hommes dans un délai d’un mois suivant la réception de l’acte notifiant la résiliation de son contrat si cette dernière est jugée illicite par l’employé. L’employeur aura la charge d’apporter la preuve de la régularité de la résiliation du contrat de travail de son employé. Le procès est conclu selon une procédure d’urgence de deux mois.</p>
<p>Dans le cas où le Tribunal des Prud’hommes rejetterait la validité de la résiliation, ou si l’employeur ne peut pas justifier la raison du licenciement, l’employeur est obligé de réembaucher le requérant dans le mois suivant. Si l’employeur ne le recrute pas, il sera dans l’obligation de lui verser une indemnité couvrant son salaire de quatre mois au minimum et huit mois au maximum. De plus, une indemnité couvrant le salaire de quatre mois au maximum devra être versée au requérant, représentant la période d’attente de la décision définitive du Tribunal. L’employé sera obligé de s’adresser à son employeur dans les 10 jours suivant la réception de la décision du Tribunal. Dans le cas contraire, son contrat de travail est considéré comme dissout.</p>
<h4 class="spip">Indemnité d’ancienneté</h4>
<p>En cas de licenciement ou démission de l’employé dans l’année suivant son mariage ou de décès, une indemnité légale plafonnée à 30 jours de salaire par année d’ancienneté, devra être versée. Elle sera calculée sur la base du dernier salaire perçu. Cette indemnité est plafonnée au montant du traitement du fonctionnaire le plus élevé.</p>
<p>Il est fixé à 3828,37 TRY jusqu’au 31 décembre 2015 (source : ministère du Travail et de la Sécurité sociale).</p>
<h3 class="spip"><a id="sommaire_3"></a>Fêtes légales</h3>
<ul class="spip">
<li>1er Janvier</li>
<li>23 avril : fête des enfants</li>
<li>1er mai : fête du travail</li>
<li>19 mai : fête de la jeunesse</li>
<li>30 août : fête de la victoire</li>
<li>29 octobre : fête nationale turque</li></ul>
<p>Ainsi que les fêtes musulmanes de Ramazan Bayram et de Kurban Bayram, dont la date varie en fonction du calendrier lunaire.</p>
<h3 class="spip"><a id="sommaire_4"></a>Emploi du conjoint</h3>
<p>Le concubinage n’est pas reconnu en Turquie.</p>
<p>Les possibilités d’emploi sont limitées, en raison, notamment, de la barrière linguistique. Les contrats locaux sont assortis de rémunérations faibles.</p>
<h3 class="spip"><a id="sommaire_5"></a>Création d’entreprises – spécificités</h3>
<p>L’environnement réglementaire turc est favorable aux affaires. Il est possible de créer une entreprise en Turquie, quelle que soit la nationalité de l’entrepreneur ou son lieu de résidence. Il suffit de déposer une demande accompagnée de documents requis auprès de tout bureau du registre de commerce. L’entreprise devient ainsi « entité légale » après son inscription au registre de commerce.</p>
<p><strong>Types d’entreprises :</strong></p>
<ul class="spip">
<li>Société par actions (S.A.) ;</li>
<li>Société à responsabilité limitée (SARL.) ;</li>
<li>Société en commandite ;</li>
<li>Société en nom collectif ;</li>
<li>Société coopérative ;</li>
<li>Entreprise commune ;</li>
<li>Association d’entreprises ;</li>
<li>Consortium.</li></ul>
<p>Le nouveau code de commerce turc (n° 6102) est en vigueur depuis le 1er juillet 2012. Ce nouveau code vise essentiellement à développer une approche de la gouvernance d’entreprises conforme aux normes internationales, à favoriser la transparence dans la gestion des opérations, et à aligner l’environnement turc des affaires à la réglementation de l’UE.</p>
<p><i>Mise à jour : décembre 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/emploi-stage/article/reglementation-du-travail-110019). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
