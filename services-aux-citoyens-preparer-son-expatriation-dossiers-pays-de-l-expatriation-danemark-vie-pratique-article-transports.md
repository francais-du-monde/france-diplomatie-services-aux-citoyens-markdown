# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/vie-pratique/article/transports#sommaire_1">Importation de véhicule et immatriculation</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/vie-pratique/article/transports#sommaire_2">Permis de conduire</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/vie-pratique/article/transports#sommaire_3">Code de la route</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/vie-pratique/article/transports#sommaire_4">Assurances et taxes</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/vie-pratique/article/transports#sommaire_5">Achat et location</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/vie-pratique/article/transports#sommaire_6">Entretien</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/vie-pratique/article/transports#sommaire_7">Réseau routier</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/vie-pratique/article/transports#sommaire_8">Transports en commun</a></li>
<li><a id="so_9" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/vie-pratique/article/transports#sommaire_9">Modes de transport utilisés à Copenhague</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Importation de véhicule et immatriculation</h3>
<p>Il est tout à fait possible d’importer au Danemark une voiture achetée dans un pays membre de l’Union Européenne sans <strong>aucune formalité douanière</strong>, même pour les séjours de longue durée.</p>
<p>Si vous pouvez prouver que vous resterez moins d’un an, vous n’avez pas de <strong>taxes</strong> à payer, mais il est nécessaire de s’adresser au registre central des véhicules pour faire enregistrer votre véhicule dans un délai <strong>maximal</strong> de 15 jours après votre arrivée dans le pays. Les coordonnées du registre sont : Centralregistret for Motorkøretøjer, Helgeshøj Allé 9, 2630 Taastrup.</p>
<p>Par contre, pour un séjour supérieur à un an, <strong>le véhicule doit être enregistré et immatriculé sur plaques minéralogiques danoises aussi dans les quinze jours qui suivent l’arrivée au Danemark</strong>. La première démarche est donc de contacter le centre d’Impôts - SKATTECENTER, pour se renseigner sur les droits à acquitter. Avant d’immatriculer votre véhicule, l’administration vérifiera sa conformité technique en passant un <strong>contrôle technique</strong> auprès de la "Synsvirksomhed".</p>
<p>Cette formalité comporte l’acquittement de taxes élevées :</p>
<ul class="spip">
<li>Un <strong>droit spécial</strong> pouvant aller <strong>jusqu’à 180%</strong> de la valeur du véhicule</li>
<li><strong>TVA (moms) de 25%</strong> si le véhicule a été acheté hors taxes. La valeur imposable du véhicule correspond au prix normal du véhicule facturé en cas de vente à un consommateur au Danemark. De plus, les droits d’enregistrement dépendent de l’année de la première mise en circulation du véhicule et du kilométrage au compteur.</li></ul>
<p>Dans tous les cas, se renseigner auprès de :</p>
<p><a href="http://www.skat.dk/SKAT.aspx?oID=166038" class="spip_out" rel="external">Skattecenter Kobenhavn - Motorbeskatningen</a><br class="manualbr">Sluseholmen 8B<br class="manualbr">2450 Copenhague SV<br class="manualbr">Tél. : 72 22 18 18</p>
<p>ou auprès du bureau de SKAT de votre domicile au Danemark.</p>
<p><strong>Nota bene</strong> : tant que la carte de séjour est temporaire, le paiement des droits peut être effectué par trimestre au taux de 4,5% de la valeur du véhicule, après le versement d’un dépôt de garantie de 4,5%. Une vignette d’une durée maximale de deux ans, renouvelable une fois pour deux ans, vous sera délivrée.</p>
<p>Dès lors que le permis de séjour devient permanent, la totalité de la taxe est à verser.</p>
<p>Il est également <strong>obligatoire</strong> de souscrire une assurance danoise.</p>
<h3 class="spip"><a id="sommaire_2"></a>Permis de conduire</h3>
<p>Le permis français, de même que le permis international sont reconnus comme sur l’ensemble du territoire de l’Union européenne.</p>
<p>L’échange est cependant toujours possible (mais payant : 260 DKK, soit 34,75EUR). La délivrance d’un permis de conduire international est conditionnée par la possession du permis danois.</p>
<p>Pour certaines catégories spéciales, notamment le permis poids lourds, les permis français et danois ne sont pas toujours équivalents.</p>
<p>Dans ce cas, s’adresser à <i>Borgerservice</i> de votre commune de résidence.</p>
<h3 class="spip"><a id="sommaire_3"></a>Code de la route</h3>
<p>La vitesse maximale autorisée est de 50 km/h en agglomération, 80 km/h sur route et de 110 à 130 km/h sur autoroute (avec remorque respectivement 50, 70 et 80 km/h). Le port de la ceinture de sécurité (et du casque pour les deux roues) est obligatoire pour le conducteur et les passagers. Il est obligatoire de circuler avec les feux allumés en position de code.</p>
<p>La priorité absolue est à donner aux cyclistes dans les agglomérations. Il existe de très nombreuses pistes cyclables. Des feux rouges et panneaux à l’attention des cyclistes réglementent leur circulation.</p>
<h3 class="spip"><a id="sommaire_4"></a>Assurances et taxes</h3>
<p>L’assurance est obligatoire et fonctionne sur le même principe qu’en France. Les prix sont élevés et augmentent rapidement en cas de sinistres.</p>
<h3 class="spip"><a id="sommaire_5"></a>Achat et location</h3>
<p>Toutes les marques automobiles françaises sont représentées au Danemark. Compte tenu des taxes élevées, les véhicules sont toutefois deux fois plus chers qu’en France (ex. pour une Citroën C4 Picasso : 47 385 € au Danemark, contre 23 050 € en France).</p>
<p>La location de véhicules est très coûteuse mais certains loueurs peuvent offrir des tarifs plus concurrentiels.</p>
<h3 class="spip"><a id="sommaire_6"></a>Entretien</h3>
<p>L’entretien et la fourniture de pièces détachées sont très coûteux.</p>
<h3 class="spip"><a id="sommaire_7"></a>Réseau routier</h3>
<p>Le réseau routier du Danemark est dense et moderne. Les autoroutes danoises sont gratuites.</p>
<h3 class="spip"><a id="sommaire_8"></a>Transports en commun</h3>
<h4 class="spip">Transport aérien</h4>
<p>L’aéroport international de Copenhague (<i>Københavns Lufthavne</i>) est situé à Kastrup sur l’île Amager, à 8 km au sud-est de Copenhague. Il est le principal aéroport de la Scandinavie (premier aéroport de Scandinavie en terme de passagers et de destinations), région qui englobe le Danemark, la Norvège et la Suède, et assure des liaisons aériennes à destination des aéroports nationaux et régionaux de cette région. Il dessert 134 destinations internationales dont 98 situées en Europe.</p>
<p>Il est accessible par :</p>
<ul class="spip">
<li>le train qui assure des liaisons directes avec Copenhague, Elsinore et Bornholm. La durée du trajet entre la gare centrale de Copenhague et l’aéroport est de 12 minutes ;</li>
<li>plusieurs lignes de bus (durée du trajet : 30 minutes) ;</li>
<li>le métro (départ toutes les 4-6 minutes, durée du trajet depuis la principale place de Copenhague « Kongens Nytorv » : 14 minutes). L´aéroport de Billund situé dans la région Midtjylland à 111 km à l´est d´Aarhus propose 60 destinations internationales dont un vol direct pour Paris</li></ul>
<p>La ville d’Aarhus possède un aéroport international (<i>Aarhus Lufthavn</i>), situé à 35 kilomètres au nord de la ville, avec des vols internationaux directs vers Londres, Barcelone, Stockholm et Oslo.</p>
<p>Aalborg possède un aéroport (<i>Aalborg Lufthavn</i>) situé dans la banlieue nord de la ville qui propose 43 vols internationaux directs dont un pour Nice en été.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.cph.dk/" class="spip_out" rel="external">Aéroport de Copenhague</a> (site en anglais)</li>
<li><a href="http://www.bll.dk/" class="spip_out" rel="external">Aéroport de Billund</a> (site en anglais)</li>
<li><a href="http://www.aar.dk/" class="spip_out" rel="external">Aéroport d’Aarhus</a> (site en anglais)</li>
<li><a href="http://www.aal.dk/" class="spip_out" rel="external">Aéroport d’Aalborg</a> (site en anglais)</li></ul>
<h4 class="spip">Transport ferroviaire</h4>
<p>Le réseau des chemins de fer couvre la totalité du pays. Des trains express d’un bon confort (IC3) assurent la liaison entre les principales villes de province et Copenhague.</p>
<p>Des réductions tarifaires sont proposées à certaines catégories de voyageurs.</p>
<p>Pour planifier vos déplacements en utilisant plusieurs moyens de transport (train, bus), vous pouvez utiliser le site Internet de la DSB : <a href="http://www.rejseplanen.dk/bin/query.exe/" class="spip_out" rel="external">www.rejseplanen.dk/bin/query.exe/</a> (site en anglais).</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.dsb.dk/" class="spip_out" rel="external">Chemins de fer danois</a> (<i>Danske Statsbaner - DSB</i>). </p>
<h4 class="spip">Transport routier</h4>
<p>Le Danemark dispose d’un réseau étendu de lignes d’autocars qui desservent les villes danoises et des destinations internationales, notamment la Suède et l’Allemagne.</p>
<p>La ligne 888 de la compagnie Abildskou Rutebilder assure des liaisons régulières entre Copenhague et la région d’Aarhus. Il faut compter 6 heures pour effectuer le trajet entre Aalborg et l’aéroport de Copenhague et un peu plus de 4 heures entre Aarhus et l’aéroport de Copenhague ; Les prix varient entre 300 DKK (40 Euros)pour un aller simple entre Aarhus et Copenhague et 340 DKK (46 Euros) entre Aalborg et Copenhague.</p>
<p>Pour en savoir plus :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.linie888.dk/" class="spip_out" rel="external">Linie888.dk</a> </p>
<p>La plupart de ces compagnies offrent des cartes pour plusieurs voyages et des réductions pour les enfants, les personnes de plus de 65 ans et les étudiants. Il faut généralement payer pour le transport des bagages encombrants, des vélos et des chiens de grande taille.</p>
<p>Sites Internet à consulter :</p>
<ul class="spip">
<li><a href="http://www.moviatrafik.dk/" class="spip_out" rel="external">Movia pour la région métropolitaine de Copenhague</a> (site accessible en anglais)</li>
<li><a href="http://abildskou.dk/" class="spip_out" rel="external">Abildskou Rutebilder</a> (site en anglais)</li>
<li><a href="http://www.ekspresbus.dk/" class="spip_out" rel="external">Thinggaard express</a> (site en danois). La ligne 980 assure la liaison entre Frederikshaven et Ejsberg. La durée du trajet est d’un peu plus de 5 heures et le coût d’un aller simple adulte s’élève à 340 DKK.</li>
<li><a href="http://www.graahundbus.dk/" class="spip_out" rel="external">Graahundbus</a> (site en danois). Cette compagnie assure des liaisons avec la Suède.</li>
<li><a href="http://www.eurolines.dk/" class="spip_out" rel="external">Eurolines</a> (site en anglais). Eurolines assure des départs internationaux à partir de plusieurs villes danoises (Aalborg, Aarhus, Copenhague, Kolding, Nykobing Falster, Rodby et Vejle) et vers de nombreux pays européens.</li></ul>
<h3 class="spip"><a id="sommaire_9"></a>Modes de transport utilisés à Copenhague</h3>
<h4 class="spip">Autobus et train</h4>
<p>La ville est très bien desservie par les bus. Le service normal s’arrête à une heure du matin et est relayé par un service de nuit qui dessert toute la région métropolitaine de Copenhague.</p>
<p>Il existe également un train de banlieue qui traverse la ville. Les enfants de moins de 12 ans, dont le nombre ne dépasse pas deux et accompagnés d’un adulte, voyagent gratuitement.</p>
<p>Pour connaître l’ensemble des tarifs et réductions concernant les bus, consulter : <a href="http://www.moviatrafik.dk/dinrejse/pages/dinrejse.aspx" class="spip_out" rel="external">Movia.dk</a>.</p>
<h4 class="spip">Métro</h4>
<p>Le métro de Copenhague a été ouvert en 2002. Il dispose actuellement de deux lignes : la ligne M1 qui circule entre Vanlose et Vestamager et la ligne M2 entre Vanlose et l’aéroport de Copenhague. Une ligne circulaire devrait être mise en service d’ici 2018.</p>
<p>Le métro fonctionne jour et nuit 7/7. Les rames circulent avec une fréquence de 2 à 6 minutes dans la journée et toutes les 15 minutes la nuit.</p>
<p>Les tickets peuvent être achetés à partir des distributeurs situés dans chaque station de métro. Les enfants de moins de 12 ans accompagnés d’un adulte voyagent gratuitement, à condition que leur nombre ne dépasse pas deux. Vous devrez acheter un ticket à 12 DKK supplémentaire si vous voyagez avec votre vélo ou avec votre chien (à l’exception des chiens guides et des petits chiens transportés dans un sac).</p>
<p>Un billet simple 2 zones valide 1 heure coûte 24 DKK. Le city pass 24h coûte 75 DKK (40 DKK pour un enfant) et 190 DKK (95 DKK pour un enfant) pour 72h.</p>
<p>Site Internet du <a href="http://intl.m.dk/" class="spip_out" rel="external">métro de Copenhague</a>.</p>
<p>La carte <i>Copenhaguen card</i> vous permet de voyager en train bus ou métro accompagné de deux enfants de moins de 10 ans et d´entrer gratuitement dans75 musées de la ville de Copenhague et sa région.Elle est valable sur l’ensemble du réseau et pour toutes les zones pour 24 heures (299 DKK) 48 heures (449) ou 72 heures (529 DKK). Les cartes de réduction sont en vente aux guichets des stations surveillées et aux points de vente Movia.</p>
<h4 class="spip">Taxis</h4>
<p>Tous les taxis sont équipés d’un compteur. Le prix d’une course comprend la TVA et le pourboire. Il faut ajouter au prix de la course un supplément pour le transport de bagages encombrants (valises, bicyclettes, etc.) : 20 DKK par article. Le prix varie en fonction de l’heure et du jour.</p>
<p>Tarif au kilomètre pour quatre personnes :</p>
<ul class="spip">
<li>du lundi au vendredi entre 7 heures et 16 heures : 12,50 DKK ;</li>
<li>du lundi au jeudi entre 16 heures et 7 heures le jour suivant, le vendredi entre 16 et 23 heures et le samedi et le dimanche entre 7 et 23 heures : 13,50 DKK ;</li>
<li>le vendredi soir et le samedi soir entre 23 heures et 7 heures le jour suivant et certains jours fériés : 16,80 DKK. La prise en charge sur route est facturée 24 à 40 DKK alors que le tarif pour une prise en charge sur commande est compris entre 24 et 40 DKK. L´attente est facturée 6,25 DKK/minute.</li></ul>
<p>Tous les taxis acceptent les paiements par carte bancaire.</p>
<h4 class="spip">Vélo</h4>
<p>La pratique du vélo n´est pas réservée aux weekends et fait partie intégrante de la vie des Danois. Ainsi il est le moyen de transport privilégié des Copenhagois.</p>
<p><a href="http://www.visitdenmark.fr/" class="spip_out" rel="external">Coordonnées des loueurs de vélo</a> (site en français).</p>
<p>Pour l´achat d´un vélo d´occasion consulter les annonces du <a href="http://www.dba.dk/" class="spip_out" rel="external">Blå Avis</a> (« Le journal bleu », site en danois) ou rendez-vous aux <a href="https://www.politi.dk/Koebenhavn/da/Borgerservice/Hittegods/hittegodskontor/" class="spip_out" rel="external">enchères de Police de Copenhague</a> qui ont lieu tous les samedis matin.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/vie-pratique/article/transports). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
