# Entrée et séjour

<h2 class="rub22723">Passeport, visa, permis de travail</h2>
<p>De manière générale, pour toute information relative aux conditions de séjour en Allemagne, il est vivement conseillé de contacter la <a href="http://www.paris.diplo.de/Vertretung/paris/fr/Startseite.html" class="spip_out" rel="external">section consulaire de l’ambassade d’Allemagne à Paris</a>.</p>
<p>En tant que ressortissant d’un pays membre de l’Union Européenne, vous avez le droit de vous rendre dans tout autre pays de l’Union Européenne sans avoir à remplir de formalités particulières. Il vous suffit d’être en possession d’une carte nationale d’identité ou d’un passeport en cours de validité. Aussi longtemps que votre séjour dans un autre pays de l’Union européenne ne dépasse pas trois mois, vous n’avez pas à demander de titre de séjour.</p>
<p>De même, pour exercer une activité professionnelle ne dépassant pas trois mois, le droit au séjour est reconnu en Allemagne sans qu’aucun titre ne soit délivré, mais vous devrez <strong>déclarer votre présence, dans les huit jours suivant votre arrivée en Allemagne, </strong>au bureau spécialisé<strong> </strong>(<i>Einwohnermeldeamt</i>) <strong>à la mairie </strong>(<i>Rathaus</i>) <strong>de votre lieu de résidence</strong>. Vous y remplirez un formulaire de déclaration de domicile (<i>Meldebescheinigung</i>) qui vous sera demandé pour de nombreuses autres formalités, notamment pour l’ouverture d’un compte bancaire.</p>
<p>L’Allemagne a récemment modifié ses lois sur l’immigration. Il en résulte la suppression, depuis le 1er janvier 2005, du permis de séjour (<i>Aufenthaltserlaubnis</i>) pour les ressortissants de l’Union européenne, qui ne devront plus en faire la demande aux services des étrangers. Cette réglementation concerne également les prolongations.</p>
<p>Il n’est pas exigé d’obtenir un permis de travail pour exercer une activité professionnelle en Allemagne. Le traité de Rome du 25 mars 1957 instituant les Communautés Européennes permet en effet la libre circulation des marchandises et des personnes, y compris des travailleurs et des demandeurs d’emploi.</p>
<p>La <strong>carte d’imposition </strong>(<i>Lohnsteuerkarte</i>) est la dernière démarche que le nouvel arrivant, et futur salarié, doit effectuer auprès des autorités allemandes. Elle précise son statut fiscal en Allemagne. Pour l’obtenir, il suffit de se rendre auprès des services compétents de la mairie de son domicile (<i>Einwohnermeldeamt</i>) et de présenter, outre une pièce d’identité, son certificat de résidence et son autorisation de séjour. Cette carte devra être donnée à l’employeur lors de l’embauche.</p>
<p><strong>Récapitulatif des démarches officielles</strong></p>
<table class="spip">
<thead><tr class="row_first"><th id="ide6a3_c0"> </th><th id="ide6a3_c1">Documents officiels à obtenir</th><th id="ide6a3_c2">Lieu</th><th id="ide6a3_c3">Pièces à présenter</th><th id="ide6a3_c4">A quel moment</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="ide6a3_c0">1ère étape</td>
<td headers="ide6a3_c1"><strong>Meldebescheinigung </strong>(Certificat de résidence)</td>
<td headers="ide6a3_c2"><strong>Einwohnermeldeamt</strong><strong>Stadtbüro </strong>de la mairie (<strong>Rathaus</strong>) du lieu d’habitation</td>
<td headers="ide6a3_c3"><strong>Pièce d’identité</strong> en cours de validité et <strong>Anmeldeformular</strong> (à acheter dans une papeterie ou chez un buraliste) signé par le propriétaire du logement ou accompagné du contrat de location</td>
<td headers="ide6a3_c4">Moins d’une semaine après l’installation dans un nouveau logement</td></tr>
<tr class="row_even even">
<td headers="ide6a3_c0">2ème étape</td>
<td headers="ide6a3_c1"><strong>Lohnsteuerkarte</strong> (Carte d’imposition)<br class="autobr">
N’existe plus au format papier</td>
<td headers="ide6a3_c2"><strong>Einwohnermeldeamt</strong> de la Mairie (<strong>Rathaus</strong>) du domicile</td>
<td headers="ide6a3_c3"><strong>Pièce d’identité</strong> et <strong>Meldebescheinigung</strong></td>
<td headers="ide6a3_c4">Dès le début du premier contrat de travail ou de l’inscription à l’ <strong>Arbeitsamt</strong></td></tr>
</tbody>
</table>
<p>Depuis le 1er janvier 2013 « la carte d’imposition électronique » (elektronische Lohnsteuerkate) a été mise en place : dès l’inscription du nouvel arrivant et de sa famille en Allemagne auprès de la mairie de résidence, ses données sont transmises à la centrale fédérale des impôts (Bundeszentralamt für Steuern).</p>
<p>Ce service envoie alors à chacun un « numéro d’identification électronique personnel » (persönliche Identifikationsnummer). Ce numéro doit être communiqué par l’employé à son employeur, celui-ci devant être équipé du programme informatique ElStAM lui permettant de procéder au retrait à la source de l’impôt sur le revenu de ses employés</p>
<p><strong>Pour en savoir plus</strong> :</p>
<ul class="spip">
<li><a href="http://www.auswaertiges-amt.de/" class="spip_out" rel="external">Ministère fédéral des Affaires étrangères</a></li>
<li>Ministère fédéral de l’Intérieur : <a href="http://www.zuwanderung.de/" class="spip_out" rel="external">www.zuwanderung.de/</a> et <a href="http://www.bmi.bund.de/" class="spip_out" rel="external">www.bmi.bund.de</a></li>
<li><a href="http://www.bamf.de/" class="spip_out" rel="external">Bureau fédéral des migrations et des réfugiés</a></li></ul>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-entree-et-sejour-article-animaux-domestiques.md" title="Animaux domestiques">Animaux domestiques</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-entree-et-sejour-article-vaccination.md" title="Vaccination">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-entree-et-sejour-article-demenagement.md" title="Déménagement">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-entree-et-sejour-article-passeport-visa-permis-de-travail-109105.md" title="Passeport, visa, permis de travail">Passeport, visa, permis de travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/entree-et-sejour/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
