# Vie pratique

<h2 class="rub22937">Logement</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/vie-pratique/#sommaire_1">Où se loger ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/vie-pratique/#sommaire_2">Conditions de location</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/vie-pratique/#sommaire_3">Hôtels</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/vie-pratique/#sommaire_4">Electricité</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/vie-pratique/#sommaire_5">Electroménager</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Où se loger ?</h3>
<p>Les prix du logement sont donnés à titre indicatif.</p>
<p><strong>Agadir</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Loyer mensuel quartier résidentiel</strong></td>
<td>Dirhams</td>
<td>Euros</td></tr>
<tr class="row_even even">
<td>Studio</td>
<td>4000</td>
<td>360</td></tr>
<tr class="row_odd odd">
<td>3 pièces</td>
<td>6000 à 7000</td>
<td>540 à 630</td></tr>
<tr class="row_even even">
<td>5 pièces</td>
<td>10 000 à 15 000</td>
<td>900 à 1350</td></tr>
<tr class="row_odd odd">
<td>Villa</td>
<td>15 000 à 50 000</td>
<td>1350 à 4500</td></tr>
</tbody>
</table>
<p><strong>Casablanca</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Loyer mensuel quartier résidentiel</strong></td>
<td>Dirhams</td>
<td>Euros</td></tr>
<tr class="row_even even">
<td>Studio</td>
<td>5000 à 8000</td>
<td>450 à 720</td></tr>
<tr class="row_odd odd">
<td>3 pièces</td>
<td>6000 à 15 000</td>
<td>540 à 1350</td></tr>
<tr class="row_even even">
<td>5 pièces</td>
<td>10 000 à 20 000</td>
<td>900 à 1800</td></tr>
<tr class="row_odd odd">
<td>Villa</td>
<td>15 000 à 50 000</td>
<td>1350 à 4500</td></tr>
</tbody>
</table>
<p><strong>Fès</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Loyer mensuel quartier résidentiel</strong></td>
<td>Dirhams</td>
<td>Euros</td></tr>
<tr class="row_even even">
<td>Studio</td>
<td>Difficile à trouver</td>
<td></td></tr>
<tr class="row_odd odd">
<td>3 pièces</td>
<td>4000 à 8000</td>
<td>360 à 720</td></tr>
<tr class="row_even even">
<td>5 pièces</td>
<td>6000 à 12 000</td>
<td>540 à 1080</td></tr>
<tr class="row_odd odd">
<td>Villa</td>
<td>8000 à 15 000</td>
<td>720 à 1350</td></tr>
</tbody>
</table>
<p><strong>Marrakech</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Loyer mensuel quartier résidentiel</strong></td>
<td>Dirhams</td>
<td>Euros</td></tr>
<tr class="row_even even">
<td>Studio</td>
<td>5 000</td>
<td>450</td></tr>
<tr class="row_odd odd">
<td>3 pièces</td>
<td>8000 à 11 000</td>
<td>720 à 990</td></tr>
<tr class="row_even even">
<td>5 pièces</td>
<td>12 000 à 15 000</td>
<td>1080 à 1350</td></tr>
<tr class="row_odd odd">
<td>Villa</td>
<td>17 000 à 30 000</td>
<td>1530 à 2700</td></tr>
</tbody>
</table>
<p><strong>Rabat</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Loyer mensuel quartier résidentiel</strong></td>
<td>Dirhams</td>
<td>Euros</td></tr>
<tr class="row_even even">
<td>Studio</td>
<td>-</td>
<td>-</td></tr>
<tr class="row_odd odd">
<td>3 pièces</td>
<td>7500 à 10 000</td>
<td>675 à 900</td></tr>
<tr class="row_even even">
<td>5 pièces</td>
<td>12 000 à 15 000</td>
<td>1080 à 1350</td></tr>
<tr class="row_odd odd">
<td>Villa</td>
<td>30 000 à 40 000</td>
<td>2700 à 3600</td></tr>
</tbody>
</table>
<p><strong>Tanger</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Loyer mensuel quartier résidentiel</strong></td>
<td>Dirhams</td>
<td>Euros</td></tr>
<tr class="row_even even">
<td>Studio</td>
<td>6000</td>
<td>540</td></tr>
<tr class="row_odd odd">
<td>3 pièces</td>
<td>11 000</td>
<td>990</td></tr>
<tr class="row_even even">
<td>5 pièces</td>
<td>15 000</td>
<td>1350</td></tr>
<tr class="row_odd odd">
<td>Villa</td>
<td>25 000</td>
<td>2250</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_2"></a>Conditions de location</h3>
<p>La recherche d’un logement se fait par l’intermédiaire d’une des nombreuses agences de location implantées dans toutes les grandes villes, ou, éventuellement, par le « bouche à oreille ». On trouve aussi bien villas et appartements, vides pour la plupart, les logements meublés étant très rares.</p>
<p>Les baux de location sont en général de trois ans (quelquefois un an, tacitement reconductible). Le loyer est payable d’avance (un à deux mois). Une caution représentant un mois de loyer est également exigée. Dans le cas du recours à une agence, la commission représente un mois de loyer. Une augmentation peut être appliquée à partir de la 3ème année de location, à raison de 10% tous les trois ans. Il est conseillé de procéder à un état des lieux.</p>
<p>Les taxes et impôts locaux doivent être inclus dans les clauses du bail de location. Il est important de bien se mettre d’accord avec le propriétaire pour éviter les surprises.</p>
<p>Pour en savoir plus : <a href="http://www.selektimmo.com/magazine/conseils/fr/" class="spip_out" rel="external">Selektimmo.com</a></p>
<p><strong>A Rabat</strong>, les quartiers résidentiels se situent dans le centre-ville (notamment les quartiers de l’Agdal, Hassan et les Orangers) pour les appartements et les villas, et en périphérie pour les villas, dans les quartiers de Hay Riad, Souissi et Ambassadors.</p>
<p><strong>A Casablanca </strong>le quartier résidentiel se situe à Anfa, au sud-ouest du centre-ville à environ quinze à vingt minutes de voiture. Mais d’autres quartiers sont également agréables et offrent des loyers moins élevés (Californie, Bourgogne…). Les autres quartiers résidentiels pour les villas sont : Anfa supérieur, Val d’Anfa, Longchamps, CIL, Chantilly, Aïn Diab, Oasis. Pour les appartements : Gauthier, Racine et extension, Beauséjour, Palmiers.</p>
<p><strong>A Marrakech</strong>, le centre de la ville nouvelle (Guéliz, Hivernage), le centre de la vieille ville (Riads en Médina), La Palmeraie, la Targa et la périphérie Nord/Nord Est de la ville, sont les quartiers les plus résidentiels.</p>
<p><strong>A Fes</strong>, les quartiers résidentiels se situent dans la ville nouvelle et à la périphérie de la ville.</p>
<p><strong>A Tanger</strong>, les quartiers résidentiels se situent au Nord et Nord-ouest du centre ville (California, Montagne) ainsi qu’à l’Est (Malabata).</p>
<p>L’immobilier à Tanger est en pleine expansion et les prix à l’achat ont fortement augmenté en deux ans, entraînant une forte hausse des loyers.</p>
<p><strong>A Agadir</strong>, le centre ville ainsi que les quartiers nouveaux de la périphérie immédiate sont les plus agréables (cité suisse, Charaf, Taddart, secteur mixte, Talborjt, la Sonaba, Illigh).</p>
<p>Nécessité de chauffage l’hiver sur l’ensemble du territoire et de la climatisation l’été dans les villes situées à l’intérieur des terres. Les charges (eau, climatisation), assurées par l’occupant, sont variables suivant les saisons : entre 2 000 et 3 000 MAD/mois.</p>
<h3 class="spip"><a id="sommaire_3"></a>Hôtels</h3>
<h4 class="spip">Auberges de jeunesse</h4>
<p>En montagne et dans le bled, les habitants sont très accueillants. A Marrakech et à Tanger il existe une auberge de jeunesse.</p>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li><a href="http://www.french.hostelworld.com/country.php/Auberges-de-Jeunesse/Maroc" class="spip_out" rel="external">Auberges de jeunesse au Maroc sur www.hostelworld.com</a></li>
<li><a href="http://www.hihostels.com/dba/country-MA.fr.htm" class="spip_out" rel="external">Auberges de jeunesse au Maroc sur www.hihostels.com</a></li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Electricité</h3>
<p>Equipement électrique : courant alternatif 220 volts (quelquefois 110 volts dans les immeubles anciens), prises de courant de type européen.</p>
<h3 class="spip"><a id="sommaire_5"></a>Electroménager</h3>
<p>Les logements sont loués vides ou meublés et les cuisines sont parfois équipées dans les immeubles de construction très récente. Tout l’équipement ménager est disponible sur place auprès de nombreux commerces et grandes surfaces dans les villes.</p>
<p>Le chauffage est assuré par radiateurs électriques. Dans les villas et les immeubles anciens, on trouve des cheminées, et de rares immeubles disposent d’un chauffage central à mazout. La climatisation n’est pas indispensable sur la côte, elle est nécessaire durant les mois d’été dans les villes de l’intérieur.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-maroc-vie-pratique-article-loisirs-et-culture.md" title="Loisirs et culture">Loisirs et culture</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-maroc-vie-pratique-article-communications.md" title="Communications">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-maroc-vie-pratique-article-transports.md" title="Transports">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-maroc-vie-pratique-article-cout-de-la-vie-110853.md" title="Coût de la vie">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-maroc-vie-pratique-article-scolarisation.md" title="Scolarisation">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-maroc-vie-pratique-article-sante.md" title="Santé">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-maroc-vie-pratique-article-logement-110850.md" title="Logement">Logement</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/vie-pratique/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
