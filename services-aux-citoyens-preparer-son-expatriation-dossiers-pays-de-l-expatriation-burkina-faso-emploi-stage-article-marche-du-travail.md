# Marché du travail

<h4 class="spip">Secteurs à fort potentiel</h4>
<p>Les secteurs de la banque et de l’industrie notamment, sont des secteurs à fort potentiel avec une exigence de haute technicité (ingénieurs et techniciens spécialisés).</p>
<h4 class="spip">Secteurs à faible potentiel</h4>
<p>Le petit commerce, en revanche, n’offre pas la même ouverture.</p>
<h4 class="spip">Professions règlementées</h4>
<p>Le secteur médical, réservé aux ressortissants burkinabé, est inaccessible aux étrangers.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/emploi-stage/article/marche-du-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
