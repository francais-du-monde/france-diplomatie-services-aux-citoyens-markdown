# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/emploi-stage/article/reglementation-du-travail#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/emploi-stage/article/reglementation-du-travail#sommaire_2">Fêtes légales</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/emploi-stage/article/reglementation-du-travail#sommaire_3">Contrat de travail – spécificités</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/emploi-stage/article/reglementation-du-travail#sommaire_4">Emploi du conjoint</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/emploi-stage/article/reglementation-du-travail#sommaire_5">Création d’entreprises – spécificités</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<p>L’actuel Code du Travail est entré en vigueur le 17 juin 1992, et encadre tous les principes et obligations qui régissent la relation entre l’employé et l’employeur. Cette législation s’applique sans distinction entre dominicains et étrangers (Principe IV du Code du travail). Il devrait cependant être prochainement modifié.</p>
<p><strong>Le salaire</strong></p>
<p>Le montant du salaire est convenu dans le contrat de travail. Toutefois, il existe des salaires minima. Les salaires minima sont fixés par décision du Comité National des Salaires, entérinée par le Secrétaire d’Etat au Travail. Ils varient selon les branches d’activités (hôtel, zones franches, construction…).</p>
<p>Les salaires minima relevant de la branche la plus générale, celle des « travailleurs du secteur privé non sectorialisé » ont été fixés par la résolution de février 2013.</p>
<p>Les salaires minima mensuels en pesos se présentent donc comme suit : pour les entreprises dont le capital est</p>
<ul class="spip">
<li>Supérieur à 4 millions de Pesos : 11292 DOP</li>
<li>Compris entre 2 et 4 millions de Pesos : 7763 DOP</li>
<li>Inférieur à 2 millions de Pesos : 6880 DOP</li></ul>
<p>Pour connaitre les salaires minimum pour chaque secteur d’activité, se référer aux résolutions publiées par le Comité National des salaires.</p>
<p><strong>Autres avantages :</strong></p>
<ul class="spip">
<li>La loi du travail impose un 13ème mois et des primes.</li>
<li>Toute entreprise doit verser une participation plafonnée à 10 % des bénéfices de l’entreprise. Son montant est calculé en fonction de l’ancienneté.</li></ul>
<p><strong>Exceptions :</strong></p>
<p>La participation aux bénéfices n’est pas obligatoire pour :</p>
<ul class="spip">
<li>les entreprises installées en zone franche</li>
<li>les entreprises du secteur agricole, agro-industriel, industriel, forestier et minier durant les trois premières années (sauf convention contraire)</li>
<li>les entreprises agricoles dont le capital n’excède pas 1 million de pesos</li></ul>
<p><strong>Durée légale du temps de travail</strong></p>
<p>La durée légale du travail est de 44 heures. En pratique, la semaine de travail varie entre 40 et 44 heures. La grande majorité des compagnies travaillent du lundi au vendredi (plafonné à huit heures par jour) et parfois le samedi matin (quatre heures).</p>
<p>La législation dominicaine prévoit qu’au-delà de 44 heures par semaine (durée légale), l’employeur doit verser au salarié :</p>
<ul class="spip">
<li>35 % du salaire de base en plus, à concurrence de 68 heures par semaine.</li>
<li>100 % du salaire de base au-delà.</li></ul>
<p>Le travail de nuit (de 21 heures à 7 heures) est payé 15 % de plus que le salaire de base. Le salaire est doublé lorsque le salarié travaille le dimanche et les jours fériés.</p>
<p><strong>L’âge légal du départ à la retraite</strong> est fixé à 65 ans.</p>
<p><strong>Les congés payés et jours fériés</strong></p>
<p>Après une année entière de travail et jusqu’à cinq ans d’ancienneté, l’employé a droit à 14 jours de vacances au minimum. Après cinq ans, les vacances sont portées à 18 jours. Les jours non ouvrables et jours fériés ne peuvent pas être pris en considération pour calculer la période de vacances.</p>
<p>Certaines absences sont payées :</p>
<ul class="spip">
<li>Cinq jours pour cause de mariage ;</li>
<li>Trois jours lors du décès d’un membre de la famille proche ;</li>
<li>Deux jours pour le père à l’occasion de la naissance d’un enfant de l’épouse</li>
<li>Six semaines pour le congé de maternité.</li></ul>
<p>Enfin, les jours déclarés non ouvrables par la Constitution ou le législateur rentrent dans la catégorie des absences payées (sauf s’ils correspondent à de jours de repos hebdomadaires).</p>
<h3 class="spip"><a id="sommaire_2"></a>Fêtes légales</h3>
<p><strong>En République Dominicaine, il y a 12 jours fériés nationaux (Loi n ° 139-97).</strong></p>
<p>Voici les dates pour l’année 2014 :</p>
<ul class="spip">
<li><strong>1er janvier</strong> : Jour de l’An</li>
<li><strong>6 janvier</strong> : Jour des Rois</li>
<li><strong>21 janvier</strong> : Jour de Notre Dame de l’Altagracia, patronne du pays</li>
<li><strong>26 janvier</strong> : Jour de Duarte</li>
<li><strong>27 février</strong> : Fête de l’Indépendance (fête nationale)</li>
<li><strong>18 avril</strong> : Vendredi Saint</li>
<li><strong>1er mai</strong> : Fête du travail (déplacée au 5 mai en 2014, en vertu de la Loi No. 139-97)</li>
<li><strong>19 juin</strong> : Jour de Corpus Christi</li>
<li><strong>16 août</strong> : Jour de la Restauration</li>
<li><strong>24 septembre</strong> : Fête de Notre Dame de Las Mercedes</li>
<li><strong>6 novembre</strong> : Jour de la Constitution (déplacé au 10 novembre en 2014, en vertu de la Loi No. 139-97)</li>
<li><strong>25 décembre</strong> : Noël</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Contrat de travail – spécificités</h3>
<p><strong>Les conditions de fond</strong></p>
<p><strong>Le principe qui s’applique est celui du respect du libre consentement des parties. </strong>S’agissant du recrutement d’une personne de nationalité étrangère, en principe, le travailleur étranger jouit des mêmes droits que le travailleur dominicain.</p>
<p>La législation exige néanmoins qu’au moins 80 % des travailleurs soient dominicains. Mais ce quota souffre quelques exceptions (les personnes qui occupent des fonctions de dirigeants ou d’administration, les techniciens qui ne peuvent être substitués par des Dominicains, les personnes mariées avec un(e) Dominicain(e) depuis deux ans minimum et résidant continuellement dans le pays depuis plus de trois ans et enfin, les personnes ayant des enfants dominicains qui résident continuellement dans le pays depuis plus de cinq ans). Un quota différent s’applique aux entreprises de moins de 10 salariés.</p>
<p><strong>Les conditions de forme</strong></p>
<p>En principe, la simple expression du consentement des parties suffit à former le contrat. Qu’il soit oral ou écrit, le contrat est valable. Une seule exception : le contrat de travail conclu avec une personne de nationalité étrangère doit être formalisé par écrit, sous peine de nullité.</p>
<p>Quatre exemplaires doivent être établis dont deux sont à remettre au secrétariat d’Etat au Travail (département du travail) dans un délai de trois jours à compter de la conclusion du contrat. L’existence d’un contrat de travail à durée indéterminée est présumée. La preuve du contenu du contrat se fait par tous moyens.</p>
<p><strong>Quatre types de contrats peuvent être conclus :</strong></p>
<ul class="spip">
<li>un contrat à durée indéterminée (avec une période d’essai de trois mois).</li>
<li>un contrat à durée déterminée.</li>
<li>un contrat pour la réalisation d’un ouvrage ou d’un service spécifique.</li>
<li>un contrat saisonnier.</li></ul>
<p>Il faut déclarer à l’inspection du travail, sous peine de sanction, le personnel permanent et temporaire, les restructurations du personnel. L’approbation de la composition du personnel doit être affichée.</p>
<p><strong>La rupture du contrat de travail</strong></p>
<p>Il existe quatre types de ruptures du contrat de travail :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>la résiliation unilatérale du contrat</strong> : [la résiliation correspond à une fin de contrat à durée indéterminée, notifiée par écrit, sans aucune justification. Dans ce cadre, si l’employeur met fin au contrat de travail, il devra verser une indemnité au salarié. Lorsque le salarié met fin au contrat de travail, il ne pourra prétendre à aucune indemnité, il faudrait pour cela démontrer une faute de l’employeur. Un délai de préavis doit être respecté et varie en fonction de l’ancienneté du salarié dans l’entreprise :</p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>De 3 à 6 mois</td>
<td>7 jours de préavis</td></tr>
<tr class="row_even even">
<td>De 6 mois à 1 an</td>
<td>14 jours</td></tr>
<tr class="row_odd odd">
<td>Plus d’un an</td>
<td>28 jours</td></tr>
</tbody>
</table>
<p>Limites : une femme enceinte ne peut être licenciée pendant la durée de sa grossesse ni lors des trois mois suivant son accouchement. De même le licenciement ne peut être effectué pendant la période de congés du salarié. Les représentants syndicaux ne peuvent être licenciés avant les huit mois qui suivent la cessation de leurs fonctions syndicales.</p>
<p>L’indemnité due à l’employé en raison de la résiliation unilatérale du contrat de travail est calculée en fonction de son ancienneté et elle n’est pas sujette à l’impôt sur le revenu :</p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>De 3 à 6 mois de travail continu</td>
<td>6 jours de salaire</td></tr>
<tr class="row_even even">
<td>De 6 mois à 1 an de travail continu</td>
<td>13 jours de salaire</td></tr>
<tr class="row_odd odd">
<td>De 1 an à 5 ans</td>
<td>21 jours de salaire</td></tr>
<tr class="row_even even">
<td>Plus de 5 ans</td>
<td>23 jours de salaire pour chaque année de travail continu</td></tr>
</tbody>
</table>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>le licenciement pour faute du salarié</strong> : ce licenciement permet à l’employeur de mettre fin au contrat en cas de faute du salarié et ce sans aucune indemnité. Les causes qui justifient un tel licenciement sont identifiées par la loi. Si l’employeur ne peut prouver une juste cause, le licenciement est qualifié de « non justifié » et devient passible de sanction (compensation). </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>la résiliation du contrat pour faute d’employeur (<i>la dimisión</i>)</strong> : il s’agit de l’acte par lequel le salarié met fin au contrat en justifiant une faute de l’employeur (prévue par l’article 97 du code du travail). Cette résiliation doit être notifiée, par écrit de préférence, au département du travail ainsi qu’à l’employeur dans un délai de 48 heures.Si la résiliation du contrat par le salarié est justifiée par les causes énoncées dans la loi, l’employeur devra alors lui verser les indemnités prévues en cas de licenciement non justifié. Dans le cas contraire, le contrat sera considéré comme résolu par la faute du salarié. Ce dernier devra alors payer à l’employeur une indemnité calculée sur la base du délai de préavis requis pour la résiliation unilatérale du contrat de travail.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>le licenciement économique</strong> : le législateur dominicain a admis que la conjoncture économique puisse être une cause de rupture du contrat de travail (article 82-5 du Code du Travail). L’impossibilité pour l’entrepreneur de faire face aux difficultés économiques peut entraîner la fermeture de l’entreprise ou une réduction du personnel. Dans les deux cas, l’employeur doit en informer le département du travail et obtenir de sa part une autorisation avant de procéder au licenciement. Aucune indemnité de licenciement, au sens strict, n’est due aux salariés. Toutefois, la loi impose à l’employeur de verser une « assistance économique » à chaque salarié licencié, calculée en fonction de l’ancienneté :</p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>De 3 à 6 mois de travail continu</td>
<td>5 jours de salaire</td></tr>
<tr class="row_even even">
<td>De 6 mois à 1 an de travail continu</td>
<td>10 jours de salaire</td></tr>
<tr class="row_odd odd">
<td>Plus d’un an de travail continu</td>
<td>15 jours de salaire pour chaque année passée dans l’entreprise</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_4"></a>Emploi du conjoint</h3>
<p>La législation locale permet à nos compatriotes d’exercer une activité professionnelle. Cependant les possibilités sont limitées et les salaires du marché local sont très faibles. La connaissance de l’espagnol est indispensable.</p>
<h3 class="spip"><a id="sommaire_5"></a>Création d’entreprises – spécificités</h3>
<p>Créer une entreprise en République Dominicaine ne pose de problème particulier. Il convient toutefois de se faire accompagner dans toutes ses démarches par un bon avocat (une liste est disponible à l’Ambassade de France).</p>
<p>En République Dominicaine, la constitution juridique des entreprises est régie par le Code du commerce et <i>la Ley General de las Sociedades Comerciales y Empresas Individuales de Responsabilidad Limitada</i> No. 479-08.</p>
<p>Les formes d’entreprises les plus répandues en République Dominicaine :</p>
<ul class="spip">
<li>La société anonyme (SA) : est une société de capitaux à responsabilité limitée formée par au moins deux associés et dont les pertes se limitent aux apports effectués. La Loi sur les sociétés prévoit un capital minimum autorisé de 30 millions de pesos.</li>
<li>Les sociétés anonymes simplifiées (SAS) dont le capital minimum est de 3 millions de pesos.</li>
<li>La société à responsabilité limitée (SARL) : doit être constituée par un minimum de deux et un maximum de cinquante associés. Le capital minimum des SARL est de 100 000 pesos. Les parts détenues dans le capital ne sont pas librement accessibles.</li>
<li>Entreprise individuelle à responsabilité limitée (EIRL) : est détenue par une personne physique et qui est dotée d’une personnalité juridique ce qui permet de délimiter le patrimoine privé du patrimoine professionnel.</li></ul>
<p><strong>Démarches à suivre pour la constitution d’une entreprise : </strong></p>
<ul class="spip">
<li>Enregistrer le nom commercial de l’entreprise au <a href="http://www.onapi.gov.do/" class="spip_out" rel="external">Bureau national de la propriété industrielle (ONAPI)</a> ;</li>
<li>Rédiger et signer des statuts de l’entreprise ;</li>
<li>Dresser une liste des actionnaires en indiquant leurs apports et le nombre d’actions dont ils disposent ;</li>
<li>Payer l’impôt sur la constitution de sociétés ;</li>
<li>Rédiger devant un notaire un document certifiant l’inventaire de tous les biens et fonds composant l’actif de la société ;</li>
<li>Réunir la première assemblée des actionnaires afin d’élire le conseil d’administration ;</li>
<li>Dépôt des statuts à la Chambre de commerce afin d’obtenir le Registre du commerce (<i>Registro mercantil</i>) ;</li>
<li>S’inscrire au Registre national des contribuables (RNC) auprès de la Direction générale des impôts afin de pouvoir opérer légalement dans le pays.</li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/emploi-stage/article/reglementation-du-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
