# Entrée et séjour

<h2 class="rub23010">Passeport, visa, permis de travail</h2>
<p>La Norvège fait partie de l’espace Schengen depuis le 25 mars 2001.</p>
<p>S’il n’existe plus de contrôle aux frontières pour toute personne d’un autre pays de l’espace Schengen, il est néanmoins indispensable d’être en mesure de produire un passeport ou une carte nationale d’identité en cours de validité durant son séjour.</p>
<p>Un salarié (travailleur saisonnier ou à durée déterminée) ou un demandeur d’emploi peut résider librement en Norvège pour une durée de trois mois sans avoir de permis de séjour et de travail à condition de pouvoir subvenir à ses besoins.</p>
<p>Depuis le 1er janvier 2009, il n’est plus obligatoire d’avoir un permis de séjour pour les salariés. Néanmoins, il faut <strong>s’enregistrer auprès de la police de sa commune</strong>. Le permis de séjour reste obligatoire pour certaines catégories de personnes (étudiants, femme au foyer, etc.)</p>
<p>Les Norvégiens et les étrangers résidant en Norvège de façon permanente disposent d’un numéro d’identification composé de 11 chiffres, le <i>fødelsnummer</i>, indispensable pour accomplir la plupart des formalités de la vie courante.</p>
<p>Les étrangers qui résident temporairement sur le sol norvégien, se voient attribuer un numéro d’identification temporaire, également composé de 11 chiffres, le <i>D-nummer</i>.</p>
<p>Ce <i>D-nummer</i> est requis lors de nombreuses formalités et son délai d’obtention est de quatre semaines environ. Il est donc essentiel d’entamer les procédures pour son obtention avant votre arrivée en Norvège, notamment par le biais de l’ouverture d’un compte bancaire. Les deux plus grandes banques en Norvège sont <a href="http://www.nordea.no/" class="spip_out" rel="external">Nordea</a> (délivre des cartes <i>Visa</i>) et <a href="https://www.dnb.no/" class="spip_out" rel="external">DNB</a> (délivre des cartes <i>Visa Electron</i>). Les procédures d’ouverture de compte à distance diffèrent pour les deux banques.</p>
<p><strong>A noter :</strong> une fois le <i>D-nummer</i> attribué par les autorités norvégiennes (prévoir deux à trois semaines) il faut compter environ une semaine pour qu’il soit transmis à la banque. Pour gagner du temps, il est possible d’aller directement le demander au centre pour les travailleurs étrangers (<i>Servicesenter for utenlandske arbeidstakere – SUA</i>) à l’adresse suivante : Schweigaards gt. 17 à Oslo.</p>
<p>Pour tous renseignements, veuillez contacter l’<a href="http://www.udi.no/" class="spip_out" rel="external">Office national norvégien de l’immigration (UDI)</a> :<br class="manualbr"><strong>UDI - Utlendingsdirektoratet</strong><br class="manualbr">Hausmannsgt.21<br class="manualbr">0182 Oslo<br class="manualbr">Tél. +47 23 35 16 00<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/norvege/entree-et-sejour/#ots#mc#udi.no#" title="ots..åt..udi.no" onclick="location.href=mc_lancerlien('ots','udi.no'); return false;" class="spip_mail">Courriel</a></p>
<p><strong>Police</strong><br class="manualbr">Schweisgaardgate 17<br class="manualbr">0191 Oslo<br class="manualbr">Tél : +47 22 66 12 00</p>
<p>Pour toute information, prendre l’attache des services consulaires norvégiens ou consulter le site de l’<a href="http://www.norvege.no/" class="spip_out" rel="external">Ambassade royale de Norvège en France</a></p>
<p><i>Mise à jour : janvier 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-norvege-entree-et-sejour-article-animaux-domestiques.md" title="Animaux domestiques">Animaux domestiques</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-norvege-entree-et-sejour-article-vaccination-111202.md" title="Vaccination">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-norvege-entree-et-sejour-article-demenagement.md" title="Déménagement">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-norvege-entree-et-sejour-article-passeport-visa-permis-de-travail.md" title="Passeport, visa, permis de travail">Passeport, visa, permis de travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/norvege/entree-et-sejour/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
