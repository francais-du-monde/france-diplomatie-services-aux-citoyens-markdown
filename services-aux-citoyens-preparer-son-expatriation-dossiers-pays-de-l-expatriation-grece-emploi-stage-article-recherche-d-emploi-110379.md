# Recherche d’emploi

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/emploi-stage/article/recherche-d-emploi-110379#sommaire_1">Outils pour la recherche d’emploi</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/emploi-stage/article/recherche-d-emploi-110379#sommaire_2">Organismes pour la recherche d’emploi</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Outils pour la recherche d’emploi</h3>
<h4 class="spip">Journaux</h4>
<p>Principaux journaux diffuseurs d’offres d’emploi :</p>
<ul class="spip">
<li>Ta Nea</li>
<li>To Bhma</li>
<li>Chrissi Efkeria</li>
<li>Eleftherotypia</li></ul>
<h4 class="spip">Sites internet</h4>
<ul class="spip">
<li><a href="http://www.adecco.gr/" class="spip_out" rel="external">Agence intérim Adecco</a> (seulement en langue grecque)</li>
<li><a href="http://www.oaed.gr/" class="spip_out" rel="external">Agence pour l’emploi grecque</a></li>
<li><a href="http://www.skywalker.gr/" class="spip_out" rel="external">Site d’annonces et de dépôt de CV en ligne</a> ou <a href="http://www.kariera.gr/" class="spip_out" rel="external">Kariera.gr</a></li></ul>
<h4 class="spip">Réseaux</h4>
<p>Les candidatures spontanées Elles constituent une méthode importante dans la recherche d’un emploi, particulièrement en Grèce du fait de l’importance des réseaux de relations</p>
<h3 class="spip"><a id="sommaire_2"></a>Organismes pour la recherche d’emploi</h3>
<h4 class="spip">Le bureau emploi-formation de la section consulaire de l’Ambassade de France en Grèce</h4>
<p>Le <a href="http://www.emploigrece.org/" class="spip_out" rel="external">bureau emploi-formation</a> offre un service gratuit et s’adresse aussi bien aux Français résidant en Grèce (y compris les double nationaux) demandeurs d’emploi ou à la recherche d’une formation professionnelle, qu’aux entreprises françaises et grecques qui recrutent.</p>
<p>Section consulaire de l’Ambassade de France en Grèce<br class="manualbr">5/7 avenue Vassileos Constantinou - 10674 Athènes<br class="manualbr">Téléphone : [30] 210 339 12 30 - Télécopie : [30] 210 339 12 09 / 210 339 12 19<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/emploi-stage/article/recherche-d-emploi-110379#bureau#mc#emploigrece.org#" title="bureau..åt..emploigrece.org" onclick="location.href=mc_lancerlien('bureau','emploigrece.org'); return false;" class="spip_mail">Courriel</a></p>
<h4 class="spip">L’Agence pour l’emploi locale : l’OAED</h4>
<p>L’<a href="http://www.oaed.gr/" class="spip_out" rel="external">Agence pour l’emploi en Grèce se nomme OAED</a> (Organisme pour l’emploi de la main d’œuvre).</p>
<p>La recherche d’un emploi, avec une rémunération satisfaisante et des perspectives d’avancement pour le chômeur, est une des priorités de l’OAED. Cette tâche relève de la compétence de la Direction pour l’emploi. Elle a une mission d’information des chômeurs sur les possibilités de formation et d’emploi et est chargée de l’application des mesures actives pour la création d’emplois.</p>
<p><i>Mise à jour : novembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/emploi-stage/article/recherche-d-emploi-110379). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
