# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/emploi-stage/article/reglementation-du-travail-111230#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/emploi-stage/article/reglementation-du-travail-111230#sommaire_2">Fêtes légales</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/emploi-stage/article/reglementation-du-travail-111230#sommaire_3">Emploi du conjoint</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<p><i>L’Employment Contracts Act </i>de 1991 et <i>l’Employment Relations Act </i>de juin 2000 établissent les fondements de l’organisation du travail en Nouvelle-Zélande et répertorient les clauses essentielles des contrats de travail.</p>
<h4 class="spip">La durée du travail</h4>
<p>La durée de la période d’essai est fixée contractuellement entre l’employeur et l’employé. Il n’existe pas de préavis en cas de rupture de contrat sauf si prévu contractuellement.</p>
<p>La semaine de travail est fixée à 40 heures. Le coût des heures supplémentaires dépend essentiellement du contrat de travail passé entre l’employeur et l’employé. Le repos hebdomadaire correspond à deux jours, les jours fériés à onze jours.</p>
<p>Le salaire minimum est de 13,75 NZ$ par heure, 110 NZ$ pour une journée de huit heures et 550 NZ$ pour une semaine de 40h.</p>
<h4 class="spip">Les congés</h4>
<p>Les congés annuels payés : le minimum légal est de quatre semaines de congés annuels.</p>
<p>Les congés maladie payés correspondent en fait aux cinq jours de congés annuels supplémentaires, répartis au choix de l’employé, cumulables sur cinq ans. Cette durée peut être allongée contractuellement.</p>
<p>Les congés parentaux payés sont de 14 semaines.</p>
<h4 class="spip">La retraite</h4>
<p>L’âge légal de départ à la retraite est fixé à 65 ans, un employé peut faire valoir ses droits à la retraite mais ne peut pas être légalement forcé de le faire.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.enz.org/new-zealand-salaries.html" class="spip_out" rel="external">Les salaires en Nouvelle-Zélande</a> ;</li>
<li><a href="http://www.dol.govt.nz/er/pay/minimumwage/" class="spip_out" rel="external">Ministry of Business, Innovation &amp; Employment</a>.</li></ul>
<p>Aucune cotisation patronale n’est obligatoire. Les employeurs peuvent (mais ne sont pas obligés) s’affilier à des fonds de pension et des assurances maladies privées, qu’ils doivent alors financer.</p>
<p>Aucune obligation n’est faite à l’employé de cotiser socialement. Cette décision relève de la décision de l’employé qui peut s’adresser au fond de pension qu’il souhaite. Les grandes compagnies prévoient souvent dans leur offre d’emploi des "packages sociaux".</p>
<h3 class="spip"><a id="sommaire_2"></a>Fêtes légales</h3>
<ul class="spip">
<li>1er et 2 janvier : Nouvel An</li>
<li>6 février : Waitangi Day – Fête nationale</li>
<li>Vendredi Saint, Pâques, Lundi de Pâques</li>
<li>25 avril : ANZAC Day</li>
<li>1er lundi de juin : Anniversaire de la Reine</li>
<li>4ème lundi d’octobre : Fête du Travail</li>
<li>25 décembre : Noël</li>
<li>26 décembre : Boxing Day / jour de la famille</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Emploi du conjoint</h3>
<p>Dans le secteur privé, les opportunités auprès des sociétés françaises sont en nombre restreint. La connaissance de l’anglais est indispensable.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/emploi-stage/article/reglementation-du-travail-111230). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
