# Enlèvements d’enfants vers l’étranger

<p class="chapo">
    Parent victime de l’enlèvement de votre enfant vers l’étranger, dans l’incapacité d’exercer votre droit de visite, ou qui, en raison de votre situation familiale/personnelle, craignez que votre conjoint(e)ou partenaire n’emmène votre enfant pour l’installer à l’étranger sans votre accord, ces pages vous sont destinées.
</p>
<p>Celles-ci ne prétendant pas refléter tous les cas et toutes les situations, il est vivement recommandé de contacter le <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/conseils-aux-familles/enlevements-d-enfants-vers-l-etranger/article/que-faire-en-cas-de-deplacement-illicite#MAEDI" class="spip_in">Bureau de la protection des mineurs et de la famille du ministère des Affaires étrangères et du Développement international</a> afin de l’informer de votre situation d’une part, et d’autre part, de vérifier si les informations en votre possession sont toujours d’actualité. Le bureau PMF s’efforcera de vous orienter vers les administrations et organismes appropriés à votre cas.</p>
<p><strong>Avertissement</strong> : cette rubrique ne concerne que les déplacements illicites d’enfants <strong>de la France vers l’étranger</strong>. Pour des informations sur les déplacements de l’étranger vers la France, contactez le <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/conseils-aux-familles/enlevements-d-enfants-vers-l-etranger/article/que-faire-en-cas-de-deplacement-illicite#BDIP" class="spip_in">Bureau du droit de l’Union, du droit international privé et de l’entraide civile (BDIP) du ministère de la Justice</a>.</p>
<p>Un <a href="services-aux-citoyens-conseils-aux-familles-enlevements-d-enfants-vers-l-etranger-article-glossaire-82752.md" class="spip_in">glossaire</a> des termes juridiques (mots marqués d’un astérisque) est à votre disposition à la fin de cette rubrique.</p>
<p>NB : pour des raisons de commodité d’écriture, « enfant » est mentionné au singulier dans toute la rubrique. Les conseils contenus dans ces pages s’appliquent également en cas d’enlèvement de plusieurs enfants (fratries par exemple).</p>
<p><i>Mise à jour : février 2015</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-conseils-aux-familles-enlevements-d-enfants-vers-l-etranger-article-comment-prevenir-un-enlevement.md" title="Comment prévenir un déplacement illicite">Comment prévenir un déplacement illicite</a></li>
<li><a href="services-aux-citoyens-conseils-aux-familles-enlevements-d-enfants-vers-l-etranger-article-que-faire-en-cas-de-deplacement-illicite.md" title="Que faire en cas de déplacement illicite">Que faire en cas de déplacement illicite</a></li>
<li><a href="services-aux-citoyens-conseils-aux-familles-enlevements-d-enfants-vers-l-etranger-article-ce-qu-il-faut-savoir.md" title="Ce qu’il faut savoir">Ce qu’il faut savoir</a></li>
<li><a href="services-aux-citoyens-conseils-aux-familles-enlevements-d-enfants-vers-l-etranger-article-glossaire-82752.md" title="Glossaire">Glossaire</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/conseils-aux-familles/enlevements-d-enfants-vers-l-etranger/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
