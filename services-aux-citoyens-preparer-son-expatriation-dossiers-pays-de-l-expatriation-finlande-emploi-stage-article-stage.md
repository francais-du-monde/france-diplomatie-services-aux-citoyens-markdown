# Stage

<p>La lecture de la page du site ci-dessous est vivement recommandée, car elle contient des informations à jour et pertinentes sur la recherche de stages en Finlande :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.euroguidance-france.org/stages-jobs-emplois/stages/faire-un-stage-en-finlande/" class="spip_out" rel="external">Euroguidance-france.org</a> </p>
<p><strong>Avez-vous pensé au volontariat international (VIE ou VIA) ?</strong></p>
<p>Le V.I.E. permet aux entreprises françaises de confier à un jeune ressortissant de l’espace économique européen, âgé de 18 à 28 ans, une mission professionnelle (rémunérée) à l’étranger durant une période modulable de 6 à 24 mois.</p>
<p>Pour en savoir plus :</p>
<ul class="spip">
<li><a href="http://www.civiweb.com/FR/index.aspx" class="spip_out" rel="external">Civiweb.com</a></li>
<li><a href="http://www.businessfrance.fr/" class="spip_out" rel="external">Business France.fr</a> - Courriel : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/emploi-stage/article/stage#infoVIE#mc#ubifrance.fr#" title="infoVIE..åt..ubifrance.fr" onclick="location.href=mc_lancerlien('infoVIE','ubifrance.fr'); return false;" class="spip_mail">infoVIE<span class="spancrypt"> [at] </span>ubifrance.fr</a></li></ul>
<p><i>Mise à jour : janvier 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/emploi-stage/article/stage). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
