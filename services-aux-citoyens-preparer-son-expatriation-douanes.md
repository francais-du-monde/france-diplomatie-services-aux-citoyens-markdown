# Douanes

<p>Si vous déménagez dans un pays de l’Union européenne, vous n’avez pas de formalités douanières à accomplir. Dans un pays n’appartenant pas à l’Union européenne, vous devrez effectuer des démarches administratives auprès des douanes lors de votre déménagement.</p>
<p><strong>Attention</strong> : Pensez à vous renseigner sur la législation du pays d’accueil, même au sein de l’Union Européenne, lorsque vous emmenez vos animaux domestiques avec vous.</p>
<p>Comment choisir mon entreprise de déménagement ?  Que dois-je faire pour amener mon animal de compagnie ? Quelles formalités administratives doit-on faire pour les transferts d’argent ?</p>
<p>Vous trouverez, dans ces rubriques, conseils et astuces ainsi que des contacts pour vous aider à préparer votre installation à l’étranger.</p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-douanes-article-animaux-domestiques.md" title="Animaux domestiques">Animaux domestiques</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-douanes-article-demenagement.md" title="Déménagement">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-douanes-article-transfert-d-argent.md" title="Transfert d’argent">Transfert d’argent</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/douanes/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
