# Passeport, visa, permis de travail

<p>Quel que soit le motif de votre séjour, renseignez-vous auprès de la section consulaire de l’<a href="http://www.ambassade-vietnam.com/" class="spip_out" rel="external">ambassade du Vietnam à Paris</a> qui vous informera sur la règlementation en matière d’entrée et de séjour au Vietnam, règlementation que vous devrez impérativement respecter. N’hésitez pas à consulter le site ministère de l’Intérieur du Vietnam afin de vous familiariser avec les modalités d’entrée et de séjour dans le pays.</p>
<p><strong>En aucun cas, vous n’êtes autorisé à travailler au Vietnam sans permis adéquat. </strong></p>
<p>Le Consulat de France au Vietnam n’est pas l’organisme compétent pour répondre à vos demandes d’information concernant votre séjour au Vietnam.</p>
<p>Un visa est nécessaire pour séjourner au Vietnam. La date d’expiration du passeport français ne doit pas être inférieure de six mois à la fin de validité du visa ou du séjour au Vietnam. Les étrangers peuvent entrer sur le territoire aussi bien par les aéroports internationaux (Hanoï, Hô Chi Minh-Ville, Danang) que par les postes-frontières terrestres suivants :</p>
<p><strong>Liste des postes frontières internationaux du Vietnam</strong></p>
<table class="spip">
<thead><tr class="row_first"><th id="ide2a8_c0"> </th><th id="ide2a8_c1">Nom de la province  </th><th id="ide2a8_c2">Poste frontière  </th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="ide2a8_c0">Frontière chinoise</td>
<td headers="ide2a8_c1">Quang Ninh</td>
<td headers="ide2a8_c2">Mong Cai</td></tr>
<tr class="row_even even">
<td headers="ide2a8_c0">Frontière chinoise</td>
<td headers="ide2a8_c1">Lang Son</td>
<td headers="ide2a8_c2">Huu Nghi- Dong Dang (voie ferrée)</td></tr>
<tr class="row_odd odd">
<td headers="ide2a8_c0">Frontière chinoise</td>
<td headers="ide2a8_c1">Lao Cai</td>
<td headers="ide2a8_c2">Lao Cai (terrestre et ferrée)</td></tr>
<tr class="row_even even">
<td headers="ide2a8_c0">Frontière laotienne</td>
<td headers="ide2a8_c1">Dien Bien</td>
<td headers="ide2a8_c2">Tay Trang</td></tr>
<tr class="row_odd odd">
<td headers="ide2a8_c0">Frontière laotienne</td>
<td headers="ide2a8_c1">Thanh Hoa</td>
<td headers="ide2a8_c2">Na Meo</td></tr>
<tr class="row_even even">
<td headers="ide2a8_c0">Frontière laotienne</td>
<td headers="ide2a8_c1">Nghe An</td>
<td headers="ide2a8_c2">Nam Can</td></tr>
<tr class="row_odd odd">
<td headers="ide2a8_c0">Frontière laotienne</td>
<td headers="ide2a8_c1">Ha Tinh</td>
<td headers="ide2a8_c2">Cau Treo</td></tr>
<tr class="row_even even">
<td headers="ide2a8_c0">Frontière laotienne</td>
<td headers="ide2a8_c1">Quang Binh</td>
<td headers="ide2a8_c2">Cha Lo</td></tr>
<tr class="row_odd odd">
<td headers="ide2a8_c0">Frontière laotienne</td>
<td headers="ide2a8_c1">Quang Tri</td>
<td headers="ide2a8_c2">Lao Bao</td></tr>
<tr class="row_even even">
<td headers="ide2a8_c0">Frontière cambodgienne</td>
<td headers="ide2a8_c1">Binh Phuoc</td>
<td headers="ide2a8_c2">Bo Nue</td></tr>
<tr class="row_odd odd">
<td headers="ide2a8_c0">Frontière cambodgienne</td>
<td headers="ide2a8_c1">Tay Minh</td>
<td headers="ide2a8_c2">Moc Bai et Sa Mat</td></tr>
<tr class="row_even even">
<td headers="ide2a8_c0">Frontière cambodgienne</td>
<td headers="ide2a8_c1">Dong Thap</td>
<td headers="ide2a8_c2">Thuong Phuoc</td></tr>
<tr class="row_odd odd">
<td headers="ide2a8_c0">Frontière cambodgienne</td>
<td headers="ide2a8_c1">An Giang</td>
<td headers="ide2a8_c2">Vinh Xuong et Tinh Bien</td></tr>
<tr class="row_even even">
<td headers="ide2a8_c0">Frontière cambodgienne</td>
<td headers="ide2a8_c1">Kien Giang</td>
<td headers="ide2a8_c2">Xa Xia</td></tr>
</tbody>
</table>
<p>Pour connaître l’ouverture de nouveaux postes-frontières, il est conseillé de se renseigner au préalable auprès des autorités compétentes.</p>
<p>Attention, en cas de transit pour un pays tiers, à l’aller comme au retour par le Vietnam, il faut prévoir un visa à entrées multiples. Aucune régularisation sur place, à l’entrée dans le pays n’est possible.</p>
<p>Pour obtenir un <strong>visa d’affaires</strong> valable de trois à six mois maximum, il faut être en possession d’une invitation de la part des autorités de tutelle vietnamiennes selon le secteur d’activité de l’entreprise. L’interlocuteur vietnamien se procurera un numéro d’autorisation auprès de la Direction générale de l’Immigration et le portera à la connaissance du demandeur de visa en France. Il est toujours possible pour l’homme d’affaires qui n’a pas d’invitation, de s’adresser à la <a href="http://www.vcci.com.vn/" class="spip_out" rel="external">Chambre de commerce vietnamienne</a> :</p>
<p>Building VCCI, <br class="manualbr">9 rue Dao Duy Anh, Hanoi <br class="manualbr">Tel : (84) 43 57 42 022 <br class="manualbr">Fax : (84) 43 57 42 020) ou de contacter les services de l’Ambassade pour rechercher un partenaire potentiel au Vietnam aux numéros suivants :</p>
<ul class="spip">
<li>Section politique : 01 44 14 64 27/10</li>
<li>Section culturelle et presse : 01 44 14 64 34</li>
<li>Section économique : 01 44 14 64 33</li>
<li>Section éducation et formation : 01 44 14 64 32</li>
<li>Section commerciale : 01 46 24 85 77</li>
<li>Section sciences et techniques : 01 44 14 64 30</li></ul>
<p>Les formalités de demande de visa s’effectuent auprès du service consulaire de l’ambassade du Vietnam à Paris. Après dépôt des pièces requises auprès du consulat, il faut compter cinq jours ouvrables pour l’obtention d’un visa de tourisme et deux jours ouvrables pour l’obtention d’un visa d’affaires. La procédure peut être accélérée dans ce dernier cas. Il est également possible de se procurer un visa dans de brefs délais auprès du Bureau d’information Vietnam - <a href="http://www.action-visas.com/" class="spip_out" rel="external">Service Action-Visas</a> - 69, rue de la Glacière - 75013 Paris, et en cas d’urgence un visa à l’arrivée (démarches auprès du service Action-visas), bien qu’aucune garantie ne puisse être donnée sur cette procédure.</p>
<p>Pour les <strong>séjours de plus de trois mois</strong>, un <strong>permis de résidence</strong> est obligatoire. Il s’obtient auprès d’un commissariat de police. Le demandeur d’un permis de résidence doit produire une preuve qu’il travaille au Vietnam. Ces permis sont valables un an ou pour la durée du contrat de travail.</p>
<p>Tout travailleur étranger au Vietnam doit être en possession d’un <strong>permis de travail</strong> délivré par le gouvernement vietnamien exception faite des étrangers membres d’un Conseil d’administration, Directeur général, adjoint Directeur général ou chef d’un bureau de représentation ou d’une succursale. Ces derniers doivent simplement produire une déclaration au Département du travail compétent, une semaine avant le début de leur activité.</p>
<p>Le permis de travail a une validité équivalente à la durée du contrat de travail et ne peut excéder 36 mois, cependant il peut être renouvelé à la demande de l’employeur. La demande s’effectue auprès du ministère du Travail.</p>
<p><a href="http://www.molisa.gov.vn/" class="spip_out" rel="external">Ministère du Travail, des Invalides de guerre et des Affaires sociales (MOLISA)</a> : <br class="manualbr">12 Ngo Quyen – HANOÏ. <br class="manualbr">Tél : (84) 43 82 69 557</p>
<p>Il est conseillé, pour tout renseignement complémentaire, de prendre l’attache de la section consulaire de l’Ambassade du Vietnam en France.</p>
<p><i>Mise à jour : février 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/entree-et-sejour/article/passeport-visa-permis-de-travail-111355). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
