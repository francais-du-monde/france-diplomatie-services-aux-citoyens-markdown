# Stages

<p>En Italie, la pratique des stages (<i>tirocini</i>) en entreprise est relativement récente ; de ce fait, les stages sont moins nombreux qu’en France et sont généralement peu ou pas rémunérés.</p>
<p>Pour les Italiens, un stage est souvent assimilé à une formation professionnelle, après l’obtention du diplôme. Les cursus italiens et français étant différents, il convient d’indiquer dans le CV les équivalences de diplômes. La nature du stage demandé doit également être spécifiée dans la lettre de motivation.</p>
<p>La durée des stages est généralement comprise entre quatre et douze mois. Pour mettre toutes les chances de votre côté, envoyez votre CV aux entreprises entre quatre et six mois avant la date de votre stage.</p>
<p><strong>Où chercher ?</strong></p>
<p>Vous pouvez envoyer des <strong>candidatures spontanées </strong>aux entreprises en consultant les répertoires d’entreprises :</p>
<ul class="spip">
<li><a href="http://www.ccifrance-international.org/un-reseau-mondial-dexperts/113-ccifi-dans-83-pays/europe/exporter-s-implanter-italie" class="spip_out" rel="external">Chambre de commerce franco-italienne</a></li>
<li><a href="http://www.unioncamere.it/" class="spip_out" rel="external">Unione Italiana Camere di Commercio</a></li>
<li><a href="http://www.ccielyon.com/" class="spip_out" rel="external">Chambre de commerce franco-italienne de Lyon</a></li></ul>
<p>La liste des entreprises françaises établies en Italie est consultable dans les locaux de la <a href="services-aux-citoyens.md" class="spip_out">MFE</a>.</p>
<p>Vous pouvez également répondre à des <strong>petites annonces</strong> publiées sur des sites proposant des offres de stage en Italie tels que :</p>
<ul class="spip">
<li><a href="http://www.cambiolavoro.it/" class="spip_out" rel="external">Cambiolavoro</a> rubrique Corsi e stage ;</li>
<li><a href="http://www.teli.asso.fr/" class="spip_out" rel="external">Teli.asso.fr</a> ;</li>
<li><a href="http://www.kapstages.com/" class="spip_out" rel="external">Kapstages.com</a> ;</li>
<li><a href="http://www.sportellostage.it/" class="spip_out" rel="external">Sportellostage.it</a>, site très complet pour consulter des offres de stage en Italie, inscrire son CV, envoyer son CV à des entreprises italiennes choisies ;</li>
<li><a href="http://www.eurodyssee.net/" class="spip_out" rel="external">Eurodyssee.net</a> : le programme communautaire Eurodyssée permet d’effectuer des stages en entreprise d’une durée de trois à douze mois. Les candidats doivent résider dans une région membre de l’assemblée des régions d’Europe qui envoie ou reçoit des stagiaires.</li></ul>
<p>Les sites du ministère des Affaires étrangères et de la Mission économique dans le pays vous apporteront des informations pour postuler à l’étranger :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.diplomatie.gouv.fr/fr/emplois-stages-concours/" class="spip_in">France-Diplomatie</a> rubrique Emplois, stages et concours ; </p>
<p><strong>Pour en savoir plus </strong> :</p>
<ul class="spip">
<li>Site <a href="http://www.euroguidance-france.org/" class="spip_out" rel="external">Euroguidance</a></li>
<li><a href="http://sportellostage.it/" class="spip_out" rel="external">Manuel du stage</a> (en italien)</li></ul>
<h4 class="spip">Avez-vous pensé au Volontariat International (V.I.E ou V.I.A.) ?</h4>
<p>Le V.I.E. permet aux entreprises françaises de confier à un jeune ressortissant de l’espace économique européen, âgé de 18 à 28 ans, une mission professionnelle (rémunérée) à l’étranger durant une période modulable de 6 à 24 mois.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/emplois-stages-concours/volontaires-internationaux/" class="spip_in">Page thématique sur le volontariat international sur notre site</a></li>
<li><a href="http://www.civiweb.com/FR/index.aspx" class="spip_out" rel="external">Civiweb.com</a></li>
<li><a href="http://export.businessfrance.fr/default.html" class="spip_out" rel="external">Business France.fr</a> - <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/emploi-stage/article/stages#infoVIE#mc#ubifrance.fr#" title="infoVIE..åt..ubifrance.fr" onclick="location.href=mc_lancerlien('infoVIE','ubifrance.fr'); return false;" class="spip_mail">courriel</a>.</li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/emploi-stage/article/stages). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
