# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/vie-pratique/article/cout-de-la-vie-111275#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/vie-pratique/article/cout-de-la-vie-111275#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/vie-pratique/article/cout-de-la-vie-111275#sommaire_3">Alimentation</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/vie-pratique/article/cout-de-la-vie-111275#sommaire_4">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>L’unité monétaire est le sol.</p>
<p>Le dollar américain est d’usage général et il est possible d’ouvrir un compte dans cette monnaie : (le montant des loyers, des billets d’avion est souvent fixé en dollars).</p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<p>La monnaie nationale est librement convertible. Il n’existe aucun problème de transfert. Il n’existe pas de banques françaises mais de nombreuses banques étrangères sont présentes. L’argent liquide (petites coupures pour la vie courante) et la carte bancaire sont les moyens de paiement les plus utilisés.</p>
<h3 class="spip"><a id="sommaire_3"></a>Alimentation</h3>
<p>Les conditions d’approvisionnement sont bonnes. Il n’est en principe pas nécessaire d’importer des produits. Le marché local offre la possibilité d’acheter des produits importés de France (compter environ le double du prix fixé en France).</p>
<p><strong>Prix moyen d’un repas dans un restaurant</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td></td>
<td>Soles</td>
<td>euros</td></tr>
<tr class="row_even even">
<td>Restaurant de qualité supérieure</td>
<td>160</td>
<td>43 euros</td></tr>
<tr class="row_odd odd">
<td>Restaurant de qualité moyenne</td>
<td>90</td>
<td>24 euros</td></tr>
</tbody>
</table>
<p> Le pourboire est conseillé ; il varie entre 5 et 10% du montant de l’addition.</p>
<h3 class="spip"><a id="sommaire_4"></a>Evolution des prix</h3>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/vie-pratique/article/cout-de-la-vie-111275). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
