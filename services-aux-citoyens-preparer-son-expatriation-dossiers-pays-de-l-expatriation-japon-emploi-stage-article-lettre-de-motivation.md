# Lettre de motivation

<h4 class="spip">Rédaction</h4>
<p>Que ce soit en anglais ou en japonais, vous devez être <strong>clair, concis et poli</strong>.</p>
<h4 class="spip">Pour une candidature en anglais</h4>
<p>Format classique d’une lettre de motivation – présentez votre argumentation en relation avec l’entreprise ou le poste visé.</p>
<h4 class="spip">Candidature par e-mail</h4>
<p>Pour un mail en japonais, il faut respecter les standards du mail « d’affaires ». Le style est moins formel qu’une lettre (les formules 「拝啓」 ou「敬具」 sont réservées aux lettres).</p>
<ul class="spip">
<li>Mettre en « objet du mail » un titre clair ;</li>
<li>Adresser son mail à une personne nommément désignée.</li></ul>
<p>En japonais, indiquer le nom de la société sur une ligne, et le titre (fonction dans l’entreprise) et nom de la personne sur la ligne suivante.</p>
<ul class="spip">
<li>Dire comment vous vous appelez ;</li>
<li>Indiquer le poste pour lequel vous postulez (référence de l’annonce) et les raisons de votre intérêt pour le poste ou l’entreprise ;</li>
<li>Il est possible de mettre en avant vos motivations (expliquez pourquoi vos compétences sont pertinentes pour le poste) ;</li>
<li>Mentionner les pièces jointes et évoquer ses disponibilités pour un entretien ;</li>
<li>Mettre des formules de politesse et de remerciement ;</li>
<li>Ne pas oublier de signer avec votre nom et vos coordonnées ;</li>
<li>Formule de politesse.</li>
<li>Corps de la lettre. Commencer par :
<br>— où avez-vous trouvé l’offre, et pourquoi postulez-vous ;
<br>— vos qualifications et votre expérience par rapport au poste ;
<br>— référence au <strong>rirekisho</strong> et <strong>shokumukeirekisho</strong>, demande d’entretien et disponibilités pour entretien ;</li>
<li>formules de conclusion ;</li>
<li>formule de politesse.</li></ul>
<h4 class="spip">Candidature par courrier</h4>
<p>La lettre d’accompagnement en japonais (送付状) peut être écrite verticalement de droite à gauche, ou bien horizontalement de gauche à droite. La mise en page respecte celle d’une lettre d’affaires japonaise (présentation, formules) :</p>
<ul class="spip">
<li>Date (la même que celle du rirekisho et shokumukeirekisho)</li>
<li>Nom de la société</li>
<li>Nom et fonction du destinataire<br class="manualbr">(si on ne connait pas le nom : 「人事部御中」 ou 「採用ご担当者様」)</li>
<li>Votre adresse</li>
<li>Votre nom (sceau)</li>
<li>Formule de salutation utilisées dans les lettres : 拝啓</li>
<li>Formule de politesse : par exemple : ますますご清祥のこととお喜び申し上げます。</li>
<li>Corps de la lettre. Commencer par さて
<br>— où avez-vous trouvé l’offre, et pourquoi postulez-vous
<br>— vos qualifications et votre expérience par rapport au poste</li>
<li>Référence au rirekisho et shokumukeirekisho, demande d’entretien et disponibilités pour entretien<br class="manualbr">Exemple : 履歴書、職務経歴書を同封いたしましたので、ご検討の上、是非ともご面談の機会を頂けましたら幸いと存じます。</li>
<li>Formules de conclusion : Par exemple : まずは、取り急ぎご連絡申しあげます</li>
<li>Formule de politesse « Veuillez agréer, Madame, Monsieur, mes salutations distinguées » : 敬具</li></ul>
<h4 class="spip">Modèles de lettre de motivation</h4>
<p><a href="http://www.hellowork.go.jp/html/greeting.html" class="spip_out" rel="external">Exemple d’une lettre manuscrite</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/emploi-stage/article/lettre-de-motivation). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
