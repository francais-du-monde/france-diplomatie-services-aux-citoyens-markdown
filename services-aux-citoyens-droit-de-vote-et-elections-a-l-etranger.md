# Droit de vote et élections à l’étranger

<p class="spip_document_84785 spip_documents spip_documents_center">
<img src="http://www.diplomatie.gouv.fr/fr/IMG/jpg/rubrique_elections_cle83326b.jpg" width="660" height="140" alt=""></p>
<p>Les Français de l’étranger votent, selon le cas, soit dans un bureau de vote ouvert auprès de leur consulat, soit dans leur commune d’inscription en France.</p>
<p>Pour s’inscrire sur une liste électorale, il faut avoir 18 ans au moins, jouir de ses droits civils et politiques et ne pas être en état d’incapacité.</p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-droit-de-vote-et-elections-a-l-etranger-article-agenda-dates-de-l-election-presidentielle-et-des-elections-legislatives-de-2017.md" title="Agenda : Dates de l’élection présidentielle et des élections législatives de 2017">Agenda : Dates de l’élection présidentielle et des élections législatives de 2017</a></li>
<li><a href="services-aux-citoyens-droit-de-vote-et-elections-a-l-etranger-article-quelles-elections-et-selon-quelles.md" title="Quelles élections et selon quelles modalités ?">Quelles élections et selon quelles modalités ?</a></li>
<li><a href="services-aux-citoyens-droit-de-vote-et-elections-a-l-etranger-article-demander-son-inscription-sur-une.md" title="Demander son inscription sur une liste électorale consulaire">Demander son inscription sur une liste électorale consulaire</a></li>
<li><a href="services-aux-citoyens-droit-de-vote-et-elections-a-l-etranger-article-liste-electorale-consulaire-et-communication-des-donnees-personnelles.md" title="Liste électorale consulaire et communication des données personnelles ">Liste électorale consulaire et communication des données personnelles </a></li>
<li><a href="services-aux-citoyens-droit-de-vote-et-elections-a-l-etranger-article-modalites-pratiques-du-vote-pour.md" title="Modalités pratiques du vote pour les Français établis hors de France">Modalités pratiques du vote pour les Français établis hors de France</a></li>
<li><a href="services-aux-citoyens-droit-de-vote-et-elections-a-l-etranger-article-demander-son-inscription-sur-une-liste-electorale-en-france.md" title="Demander son inscription sur une liste électorale en France">Demander son inscription sur une liste électorale en France</a></li>
<li><a href="services-aux-citoyens-droit-de-vote-et-elections-a-l-etranger-article-demander-sa-radiation-d-une-liste.md" title="Demander sa radiation d’une liste électorale consulaire">Demander sa radiation d’une liste électorale consulaire</a></li>
<li><a href="services-aux-citoyens-droit-de-vote-et-elections-a-l-etranger-article-modalites-pratiques-du-vote-par.md" title="Modalités pratiques du vote par procuration">Modalités pratiques du vote par procuration</a></li>
<li><a href="services-aux-citoyens-droit-de-vote-et-elections-a-l-etranger-article-les-elus-des-francais-de-l-etranger.md" title="Les élus des Français de l’étranger">Les élus des Français de l’étranger</a></li>
<li><a href="services-aux-citoyens-droit-de-vote-et-elections-a-l-etranger-article-les-conseillers-consulaires-de-110954.md" title="Les conseillers consulaires, de nouveaux représentants pour les Français de l’étranger">Les conseillers consulaires, de nouveaux représentants pour les Français de l’étranger</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/droit-de-vote-et-elections-a-l-etranger/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
