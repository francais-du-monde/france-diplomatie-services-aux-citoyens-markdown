# Vaccination

<p>Il est indispensable de se faire vacciner contre la fièvre jaune et l’hépatite A avant toute entrée dans le pays. Les vaccins contre la diphtérie, le tétanos et la poliomyélite doivent également être à jour.</p>
<p>Pour plus d’informations médicales concernant le Venezuela, consulter :</p>
<ul class="spip">
<li>notre rubrique thématique : <a href="services-aux-citoyens-preparer-son-expatriation-sante.md" class="spip_in">Santé</a></li>
<li><a href="http://www.cimed.org/" class="spip_out" rel="external">la fiche du CIMED (Comité d’Informations Médicales) spécifique à Caracas</a></li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/entree-et-sejour/article/vaccination). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
