# Protection sociale

<h2 class="rub23181">Régime local de sécurité sociale</h2>
<p>Vous trouverez une présentation détaillée du système de sécurité sociale canadien sur le site de notre partenaire, le <a href="http://www.cleiss.fr/" class="spip_out" rel="external">CLEISS</a> (Centre des liaisons européennes et internationales de sécurité sociale).</p>
<p><strong>En voici la table des matières : </strong></p>
<p><strong>Le régime des salariés</strong></p>
<ul class="spip">
<li><a href="http://www.cleiss.fr/docs/regimes/regime_canada_salaries.html#generalites" class="spip_out" rel="external">Généralités</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_canada_salaries.html#soinsdesante" class="spip_out" rel="external">Soins de santé</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_canada_salaries.html#assurancepension" class="spip_out" rel="external">Assurance pension</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_canada_salaries.html#adtmp" class="spip_out" rel="external">Assurance accidents du travail et maladies professionnelles</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_canada_salaries.html#chomage" class="spip_out" rel="external">Assurance chômage</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_canada_salaries.html#pf" class="spip_out" rel="external">Les prestations familiales ou prestations fiscales pour enfants</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_canada_salaries.html#psscdr" class="spip_out" rel="external">Prestations servies sous conditions de ressources</a></li></ul>
<p><strong>Le régime des travailleurs indépendants</strong></p>
<ul class="spip">
<li><a href="http://www.cleiss.fr/docs/regimes/regime_canada_independants.html#generalites" class="spip_out" rel="external">Généralités</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_canada_independants.html#sante" class="spip_out" rel="external">Soins de santé</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_canada_independants.html#pension" class="spip_out" rel="external">Assurance pension</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_canada_independants.html#chomage" class="spip_out" rel="external">Assurance chômage</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_canada_independants.html#pf" class="spip_out" rel="external">Les prestations familiales ou prestations fiscales pour enfants</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_canada_independants.html#psscdr" class="spip_out" rel="external">Prestations servies sous conditions de ressources</a></li></ul>
<p><i>Mise à jour : avril 2014</i></p>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/protection-sociale-23181/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
