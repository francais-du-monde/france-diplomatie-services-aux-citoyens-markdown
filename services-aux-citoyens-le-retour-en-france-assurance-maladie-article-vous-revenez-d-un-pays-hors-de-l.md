# Vous revenez d’un pays hors de l’Union européenne

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/assurance-maladie/article/vous-revenez-d-un-pays-hors-de-l#sommaire_1">Vous résidez à l’étranger et vous vous rendez en France pour un séjour temporaire</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/assurance-maladie/article/vous-revenez-d-un-pays-hors-de-l#sommaire_2">Vous vous installez durablement en France</a></li></ul>
<p>Les droits ouverts auprès de l’Assurance maladie de la sécurité sociale sont automatiquement échus du fait de l’expatriation.</p>
<h3 class="spip"><a id="sommaire_1"></a>Vous résidez à l’étranger et vous vous rendez en France pour un séjour temporaire</h3>
<p><strong>- Vous êtes adhérent à la Caisse des Français de l’étranger (CFE)</strong>. Vous bénéficiez pendant trois mois d’une prise en charge de vos soins en France. Cette protection peut aller jusqu’à six mois si vous avez souscrit l’option séjour de trois à six mois de la CFE.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Il existe une convention de sécurité sociale entre la France et votre pays d’activité </strong></p>
<p>Des <a href="http://www.cleiss.fr/docs/textes/index.html" class="spip_out" rel="external">conventions bilatérales de sécurité sociale</a> ont été conclues entre la France et certains pays hors Espace économique européen. Toutefois il faut noter que toutes ne contiennent pas des dispositions en matière d’Assurance maladie.</p>
<p>En outre, la Suisse, l’Islande, le Liechtenstein et la Norvège appliquent les règlements européens en matière de sécurité sociale depuis 2012 (se reporter à notre article <a href="services-aux-citoyens-le-retour-en-france-assurance-maladie-article-vous-revenez-d-un-pays-de-l-union-europeenne.md" class="spip_in">Vous revenez d’un pays de l’Union européenne</a>).</p>
<p>Vous pouvez bénéficier lors de vos séjours en France du remboursement de vos frais médicaux en cas d’urgence.</p>
<p>Il est donc important de se renseigner au préalable sur le contenu de la convention en demandant conseil auprès du :</p>
<p><a href="http://www.cleiss.fr/" class="spip_out" rel="external">Centre des liaisons européennes et internationales de sécurité sociale (CLEISS)</a><br class="manualbr">11 rue de la tour des Dames <br class="manualbr">75436 Paris cedex 09 <br class="manualbr">Tél. : 01 45 26 33 41 <br class="manualbr">Fax : 01 49 95 06 50</p>
<p>N’oubliez pas de demander à la sécurité sociale étrangère les formulaires adéquats pour obtenir le remboursement de vos soins en France. A défaut, vous pouvez, bien entendu, accéder aux soins mais leurs frais ne sont pas pris en charge par l’Assurance maladie française.</p>
<h3 class="spip"><a id="sommaire_2"></a>Vous vous installez durablement en France</h3>
<p><strong>Situations ouvrant droit à l’Assurance maladie </strong></p>
<p>Si vous n’avez pas retrouvé immédiatement un emploi au retour, vous pouvez ouvrir des droits à l’Assurance maladie dans 3 cas :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Si vous avez cotisé à la CFE </p>
<p>Les expatriés qui ont adhéré à l’Assurance maladie de la CFE bénéficient du maintien de leurs droits pendant trois mois au maximum à compter du 1er jour de résidence en France. A l’issue de cette période, ils pourront bénéficier de la CMU de base.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Si vous avez cotisé au Pôle Emploi Services  </p>
<p>Les expatriés qui ont cotisé au moins 18 mois à l’assurance chômage des expatriés obtiennent au retour une allocation chômage par leur Pôle Emploi. Cette allocation ouvre des droits à l’Assurance maladie.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Si vous avez travaillé dans un pays lié par une convention de sécurité sociale </p>
<p>Vous pouvez faire appel, en fonction des conventions, aux périodes d’assurances accomplies dans un autre Etat pour ouvrir immédiatement des droits aux prestations françaises.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Si vous n’avez aucune couverture sociale au retour </p>
<p>Vous demanderez à la Caisse primaire d’Assurance maladie (CPAM) de votre domicile - qui gère votre sécurité sociale - à bénéficier de la <strong>couverture maladie universelle </strong>(CMU).</p>
<p>Si vous retrouvez un emploi en France, votre nouvel employeur cotisera pour vous auprès de l’Assurance maladie. Alors la Caisse primaire d’Assurance maladie (CPAM) de votre domicile vous ouvrira des droits à partir de la fin de votre premier mois de travail, dès lors que vous avez travaillé au moins 60 heures.</p>
<p><strong>Pour plus d’information sur vos droits à votre retour en France : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.cleiss.fr/particuliers/retour_en_france.html" class="spip_out" rel="external">Site du CLEISS</a></p>
<p>(<i>Mise à jour : avril 2015</i>)</p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/assurance-maladie/article/vous-revenez-d-un-pays-hors-de-l). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
