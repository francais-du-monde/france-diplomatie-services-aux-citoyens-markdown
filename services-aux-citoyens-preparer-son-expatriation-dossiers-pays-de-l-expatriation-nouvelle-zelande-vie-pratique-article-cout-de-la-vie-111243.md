# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/vie-pratique/article/cout-de-la-vie-111243#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/vie-pratique/article/cout-de-la-vie-111243#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/vie-pratique/article/cout-de-la-vie-111243#sommaire_3">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>L’unité monétaire est le <strong>dollar néo-zélandais</strong>.</p>
<p>Au 30 décembre 2013, le dollar néo-zélandais vaut 0,59 euros c’est-à-dire qu’un euro équivaut à 1,69 dollars néo-zélandais. Le dollar néo-zélandais (NZD) est divisé en 100 cents.</p>
<p>La plupart des grandes banques sont australiennes. Aucune banque française n’est présente en Nouvelle-Zélande. Les chèques, les cartes de paiement internationales (Visa, American Express…), ainsi que les cartes EFTPOS (pour des comptes bancaires ouverts dans le pays) sont largement acceptés.</p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<p>La monnaie nationale est librement convertible. Les transferts de fonds sont libres. Un expatrié français peut solder son compte en fin de séjour.</p>
<h3 class="spip"><a id="sommaire_3"></a>Evolution des prix</h3>
<p>L’économie néo-zélandaise connaît une croissance forte. Les analystes néo-zélandais prévoient une « année dorée » pour 2014. Si les prix n’augmentent pas nécessairement, la nouvelle force du dollar néo-zélandais entraîne une baisse du pouvoir d’achat pour les personnes dont les revenus sont en euros, par rapport aux années précédentes.</p>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/vie-pratique/article/cout-de-la-vie-111243). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
