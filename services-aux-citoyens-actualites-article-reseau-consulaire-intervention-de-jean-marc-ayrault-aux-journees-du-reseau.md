# Réseau consulaire - Intervention de Jean-Marc Ayrault aux journées du réseau consulaire (21 juin 2016)

<p class="chapo">
      Jean-Marc Ayrault, ministre des affaires étrangères et du développement international, s’adressera le 21 juin 2016, aux consuls et agents consulaires réunis pour la deuxième édition des "journées du réseau consulaire".
</p>
<p><strong>Le ministre fera le point sur l’état d’avancement des projets menés pour mieux accompagner les Français qui voyagent ou qui s’établissent à l’étranger et pour rendre les services consulaires plus efficaces et plus rapides.</strong></p>
<ul class="rslides" id="slider_diapo">
<li><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L800xH533/001-7_cle0cac17-121-98271-18978.jpg" width="800" height="533" alt="Image Diaporama - Photo : B. Chapiron/MAEDI">
<p>
            Photo : B. Chapiron/MAEDI
</p></li>
<li><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L800xH533/002-7_cle0f17e4-119-0b897-c2d75.jpg" width="800" height="533" alt="Image Diaporama - Photo : B. Chapiron/MAEDI">
<p>
            Photo : B. Chapiron/MAEDI
</p></li>
<li><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L800xH533/003-8_cle045313-100-38f88-462f4.jpg" width="800" height="533" alt="Image Diaporama - Photo : B. Chapiron/MAEDI">
<p>
            Photo : B. Chapiron/MAEDI
</p></li>
<li><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L800xH533/004-9_cle088831-79-60348-cadc9.jpg" width="800" height="533" alt="Image Diaporama - Photo : B. Chapiron/MAEDI">
<p>
            Photo : B. Chapiron/MAEDI
</p></li>
<li><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L800xH533/005-8_cle03ae99-65-22dcd-3b824.jpg" width="800" height="533" alt="Image Diaporama - Photo : B. Chapiron/MAEDI">
<p>
            Photo : B. Chapiron/MAEDI
</p></li>
<li><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L800xH450/panorama-net_cle03aceb-b3c89-e1065.jpg" width="800" height="450" alt="Image Diaporama - Photo : B. Chapiron/MAEDI">
<p>
            Photo : B. Chapiron/MAEDI
</p></li>
</ul>
<p>Son discours sera ouvert à la presse.</p>
<p>Simplification, dématérialisation et efforts pour mieux communiquer avec le public sont au cœur des réformes de l’administration consulaire et de sa relation avec nos compatriotes, mises en œuvre dans les 217 services consulaires, consulats et consulats généraux, dans plus de 160 pays. Ces réformes portent notamment sur l’inscription en ligne sur le registre des Français établis hors de France, lancée le 15 juin, ou sur des projets comme l’envoi sécurisé des passeports, la dématérialisation des procédures d’état civil, ou encore l’amélioration de l’information des demandeurs de visa (France Visas).</p>
<p>Une table ronde au Quai d’Orsay, en partenariat avec TV5Monde, a permis au public le 20 juin de rencontrer trois de nos consuls et de mieux connaître les fonctions des consulats de France : <a href="services-aux-citoyens.md" class="spip_out">http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/</a></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/actualites/article/reseau-consulaire-intervention-de-jean-marc-ayrault-aux-journees-du-reseau). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
