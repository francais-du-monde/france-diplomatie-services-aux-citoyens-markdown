# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/emploi-stage/article/reglementation-du-travail-114429#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/emploi-stage/article/reglementation-du-travail-114429#sommaire_2">Contrat de travail – spécificités</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/emploi-stage/article/reglementation-du-travail-114429#sommaire_3">Fêtes légales</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/emploi-stage/article/reglementation-du-travail-114429#sommaire_4">Emploi des profils étrangers (européens)</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<p>Un salarié ne peut pas travailler plus de 8 heures par jour ou plus de 48 heures par semaine.</p>
<p>Tout salarié a droit à un jour de repos par semaine (en général le dimanche), six jours de congés payés par an et 13 jours fériés par an. Le salaire minimum est de 300 bahts par jour.</p>
<p>Il n’existe pas de règles régissant la période d’essai du salarié en Thaïlande. Il n’y a donc pas de limite légale pour la durée de la période d’essai. Néanmoins, dans la pratique, la période d’essai est de l’ordre de trois mois</p>
<h3 class="spip"><a id="sommaire_2"></a>Contrat de travail – spécificités</h3>
<p>En vertu du code du travail thaïlandais, un contrat de travail peut être conclu de manière écrite ou verbale. En effet à défaut de contrat de travail écrit, il sera au minimum appliqué les dispositions du code généralement favorables aux employés. Néanmoins, il est préférable, afin de se prémunir contre d’éventuels litiges à venir, d’établir un contrat sous forme écrite.</p>
<h3 class="spip"><a id="sommaire_3"></a>Fêtes légales</h3>
<p><strong>Les fêtes fixes</strong></p>
<ul class="spip">
<li>31 décembre et 1er janvier</li>
<li>6 avril (fête des Chakri)</li>
<li>15 avril (Songkran, Nouvel-An thaï)</li>
<li>1er mai (fête du travail)</li>
<li>5 mai (fête du couronnement)</li>
<li>mi-juillet (Carême bouddhiste)</li>
<li>12 août (anniversaire de la Reine)</li>
<li>23 octobre (Fête nationale en mémoire du roi Chulalongkorn - Rama V)</li>
<li>5 décembre (anniversaire du Roi) - Fête nationale</li>
<li>10 décembre (fête de la Constitution)</li></ul>
<p><strong>Les fêtes mobiles</strong></p>
<p>Elles sont des fêtes religieuses commandées par le calendrier lunaire : les jours de pleine lune (Makka Bucha Day), en février-mars, juin et août.</p>
<h3 class="spip"><a id="sommaire_4"></a>Emploi des profils étrangers (européens)</h3>
<p>Le marché de l’emploi est extrêmement étroit. Une bonne connaissance de l’anglais est indispensable, celle de la langue thaï sera également un atout. Bien que les entreprises privées étrangères soient nombreuses, celles-ci recrutent généralement du personnel bilingue anglo-thaï.</p>
<p>L’attribution d’un visa non immigrant B (pour les emplois) ou visa non immigrant ED (pour les stages) ainsi que d’un permis de travail <a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-thailande-entree-et-sejour-article-passeport-visa-permis-de-travail.md" class="spip_in">est impérativement requise pour travailler légalement sur le territoire</a>, que l’activité soit rémunérée ou non.</p>
<p><i>Mise à jour : juillet 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/emploi-stage/article/reglementation-du-travail-114429). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
