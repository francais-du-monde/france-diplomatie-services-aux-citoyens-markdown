# Protection sociale

<h2 class="rub22688">Régime local de sécurité sociale</h2>
<p>En tout état de cause, le système de sécurité continue à reposer sur <strong>deux grandes institutions publiques</strong> et est financé selon le principe de cotisations patronales et salariales, complétées par des subventions de l’Etat.</p>
<p>Les deux principales institutions de ce système sont :</p>
<ul class="spip">
<li>l’IMSS (Instituto Mexicano del Seguro Social) qui couvre les personnes travaillant dans le secteur privé ; il compte 16,8 millions adhérents à la fin décembre 2005, soit un total d’environ 44,6 millions de bénéficiaires ;</li>
<li>l’ISSSTE (Instituto Mexicano del Seguro Social de los Trabajadores del Estado) qui regroupe les fonctionnaires (2,4 millions d’adhérents, soit un total d’environ 10,6 millions de bénéficiaires).</li></ul>
<p>Les bénéficiaires de ces organismes dépendent de cliniques de quartier et <strong>ne déboursent pas les frais médicaux</strong>.</p>
<p>En ajoutant à ces deux institutions l’organisme propre à l’entreprise publique Pemex et celle du ministère de la Défense (SEDENA), on estime que <strong>65% de la population mexicaine</strong> bénéficie d’une couverture sociale.</p>
<p>Quatre autres organismes publics offrent des prestations médicales à un prix très bas à la population non affiliée ; ils possèdent et gèrent leurs établissements médicaux ; il s’agit :</p>
<ul class="spip">
<li>du ministère de la Santé (Secretaría de Salud ou SSA),</li>
<li>du DIF (Système national pour le développement intégral de la famille) qui est un service d’assistance sociale,</li>
<li>de l’ISSDF (Instituto de Servicios de Salud del Distrito Federal) pour les soins aux populations défavorisées dans la ville de Mexico,</li>
<li>de l’IMSS-SOLIDARIDAD, pour les soins aux populations rurales défavorisées. Ces établissements bénéficient à environ 15% de la population.</li></ul>
<p>Parallèlement, dans la mesure où il existe un important secteur informel (estimé à 30% des actifs) dont les acteurs n’ont pas accès au système de santé, le gouvernement a adopté en avril 2003 <strong>un programme de médecine sociale</strong>, entré en vigueur début 2004, en vue d’étendre l’accès aux soins à l’ensemble de cette population. Il s’agit d’un système de couverture « populaire » (« Seguro Popular ») visant à incorporer à terme plus de 43 millions de Mexicains qui n’ont aucune couverture sociale.</p>
<p>Dans le cadre du Seguro Popular, un « Fonds de protection contre des frais catastrophiques » a été également créé. Il est exclusivement consacré au diagnostic et au traitement d’affections lourdes ou de longue durée (cancer, problèmes cardio-vasculaires, sida, rééducation, soins intensifs en période néonatale, dialyses ou greffes d’organes).</p>
<p>Le développement du secteur privé a permis <strong>l’émergence d’assurances spécialisées</strong> dans le domaine médical. Ces nouveaux assureurs offrent, en association avec les hôpitaux, des forfaits de soins orientés notamment vers la médecine préventive. La prise en charge des soins est partielle (franchise à la charge de l’assuré).</p>
<p>Compte tenu des tarifs pratiqués par les médecins privés, <strong>on estime qu’environ 10% de la population seulement</strong> a recours aux services médicaux privés. En particulier, les grosses entreprises, en complément de l’affiliation obligatoire à l’IMSS ou les banques exemptées de l’affiliation obligatoire, offrent généralement à leurs employés une couverture privée faisant appel aux services de médecins et d’hôpitaux sous contrat. De nombreux acteurs du secteur informel, disposant de ressources suffisantes, pourraient à terme s’orienter vers une couverture privée.</p>
<p><i>Mise à jour : septembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-mexique-protection-sociale-22688-article-convention-de-securite-sociale-108398.md" title="Convention de sécurité sociale">Convention de sécurité sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-mexique-protection-sociale-22688-article-regime-local-de-securite-sociale-108397.md" title="Régime local de sécurité sociale">Régime local de sécurité sociale</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/protection-sociale-22688/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
