# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/emploi-stage/article/reglementation-du-travail#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/emploi-stage/article/reglementation-du-travail#sommaire_2">Contrat de travail – spécificités</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/emploi-stage/article/reglementation-du-travail#sommaire_3">Fêtes légales</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/emploi-stage/article/reglementation-du-travail#sommaire_4">Création d’entreprises – spécificités</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<p>Depuis le 1er janvier 2005, <strong>la durée hebdomadaire du travail est </strong><strong>de 45 heures</strong>. Ces heures doivent être réparties sur cinq jours au minimum et sur six jours au maximum. <strong>Les heures supplémentaires </strong>ne peuvent dépasser deux heures par jour et doivent être rémunérées au prix d’une heure normale augmenté de 50% (il s’agit du minimum fixé par la loi).</p>
<p>La loi prévoit un jour de <strong>repos hebdomadaire</strong> au minimum. Enfin, l’année compte 14 <strong>jours fériés payés</strong> et <strong>la durée des congés payés annuels</strong> est de 15 jours ouvrables.</p>
<p>Pour les congés maladie inférieurs à une durée de 10 jours, 25% du montant total du salaire est versé à partir du 4ème jour de congé. Le salaire n’est versé dans sa totalité que dans le cas d’un congé supérieur à 10 jours.</p>
<p>Le droit du travail chilien ne prévoit pas de période minimum d’essai en début de contrat ; la durée de cette période est laissée à la discrétion de l’entreprise.</p>
<p>Pour mettre fin à un contrat de travail le <strong>préavis</strong> est fixé à un mois. Si l’entreprise ne respecte pas ce délai, elle se voit dans l’obligation de payer à l’employé l’équivalent d’un mois de salaire. L’employeur doit verser une indemnisation équivalente à un mois de salaire par année de service.</p>
<h3 class="spip"><a id="sommaire_2"></a>Contrat de travail – spécificités</h3>
<h4 class="spip">Types de contrat</h4>
<p>Vous pourrez être embauché sous une des trois formes de contrat existantes au Chili :</p>
<ul class="spip">
<li><strong>« Contrato a plazo fijo »</strong> : contrat à durée déterminée (CDD). Sa durée ne peut excéder un an. Les dates de début et de fin du travail sont indiquées sur le contrat</li>
<li><strong>« Contrato indefinido »</strong> : l’équivalent du contrat à durée indéterminée (CDI). Il est important de savoir que si vous renouvelez deux fois un « Contrato a plazo fijo », celui-ci se transforme obligatoirement en CDI.</li>
<li><strong>« Contrato por obra o faena terminada »</strong> : contrat établi pour un travail de courte durée ou temporaire. Le contrat prend fin une fois le travail ou le service effectué.</li></ul>
<p>Le règlement du service de l’immigration (<i>extranjería</i>) prévoit qu’un contrat de travail pour étranger doit obligatoirement contenir les informations suivantes :</p>
<ul class="spip">
<li>le lieu et la date de signature du contrat ;</li>
<li>le nom, la nationalité et l’adresse de l´entreprise ou de l´employeur ;</li>
<li>l’état civil, la profession et le pays d´origine de l´employé ;</li>
<li>la nature du travail que l´employé exercera au Chili ;</li>
<li>les horaires et le lieu de travail ;</li>
<li>la rémunération en monnaie locale ou étrangère ;</li>
<li>l’obligation pour l´employeur de payer les cotisations retraite et assurance maladie, dont le calcul est basé sur la rémunération ;</li>
<li>la durée du contrat de travail ;</li>
<li>la date de début de l’activité professionnelle.</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Fêtes légales</h3>
<ul class="spip">
<li>1er janvier (Jour de l’An) ;</li>
<li>Vendredi Saint ;</li>
<li>1er mai (fête du travail) ;</li>
<li>21 mai (fête de la Marine / combat naval d’Iquique) ;</li>
<li>29 juin (fête de Saint-Pierre et Saint-Paul) ;</li>
<li>16 juillet (Jour de la Vierge saint Carmen) ;</li>
<li>15 août (Assomption) ;</li>
<li>Premier lundi de septembre (jour de l’Unité nationale) ;</li>
<li>18 septembre (fête de l’Indépendance / fête nationale) ;</li>
<li>19 septembre (jour de l’Armée) ;</li>
<li>12 octobre (Jour de la rencontre entre deux mondes / Día del encuentro de dosmundos) ;</li>
<li>1er novembre (Toussaint) ;</li>
<li>2 novembre (Jour des églises évangéliques) ;</li>
<li>8 décembre (Immaculée Conception) ;</li>
<li>25 décembre (jour de Noël).</li></ul>
<p>La date de certains jours fériés peut être reportée au lundi ou au vendredi suivant s’ils tombent au milieu de la semaine (Loi 20148).</p>
<h3 class="spip"><a id="sommaire_4"></a>Création d’entreprises – spécificités</h3>
<p><strong>Le service UbiFrance</strong> au Chili a publié un guide des affaires.</p>
<p>Il est également conseillé de se rapprocher de <a href="http://www.camarafrancochilena.cl/" class="spip_out" rel="external">la Chambre de commerce et d’industrie franco-chilienne</a> et des <a href="http://www.cnccef.org/" class="spip_out" rel="external">conseillers du commerce extérieur</a> (rubrique « En un clic » / Annuaire CCE).</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/emploi-stage/article/reglementation-du-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
