# Français de l’étranger - Sécurité (22 septembre 2014)

<p>Laurent Fabius, ministre des affaires étrangères et du développement international, a demandé à plusieurs ambassades de France à l’étranger d’inviter <strong>nos compatriotes résidant ou de passage à l’étranger à renforcer leur vigilance face au risque terroriste. </strong></p>
<p><strong>Nos compatriotes sont invités à prendre systématiquement connaissance des recommandations de sécurité de</strong> <a href="conseils-aux-voyageurs.md" class="spip_out">la rubrique « conseils aux voyageurs »</a>. Ces dernières font l’objet d’actualisations très fréquentes (près de 1300 par an), en fonction des analyses faites par nos ambassades et par le centre de crise du Quai d’Orsay. Ces actualisations sont effectuées en temps réel.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Les Français installés dans les pays concernés</strong> ont été invités à se signaler au consulat de France le plus proche si ce n’est pas déjà fait.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Les Français de passage</strong> sont, quant à eux, invités à se faire connaître en s’inscrivant en ligne dans le système <a href="https://pastel.diplomatie.gouv.fr/fildariane/dyn/public/login.html" class="spip_out" rel="external">Ariane</a></p>
<p class="spip_document_81922 spip_documents spip_documents_center">
<a href="conseils-aux-voyageurs.md" class="spip_in"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L315xH90/btn_CAV_cle04379f-237a7.jpg" width="315" height="90" alt=""></a></p>
<p class="spip_document_81921 spip_documents spip_documents_center">
<a href="https://pastel.diplomatie.gouv.fr/fildariane/dyn/public/login.html"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L315xH90/ariane_bloc_col_droite_cle0ab2cd-aec49.jpg" width="315" height="90" alt=""></a></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/actualites/article/francais-de-l-etranger-securite-22-115426). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
