# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/vie-pratique/article/cout-de-la-vie-109908#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/vie-pratique/article/cout-de-la-vie-109908#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/vie-pratique/article/cout-de-la-vie-109908#sommaire_3">Alimentation</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/vie-pratique/article/cout-de-la-vie-109908#sommaire_4">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>Le Luxembourg fait partie de la zone euro depuis le 1er janvier 1999 et a remplacé sa monnaie nationale, le franc luxembourgeois (LUF) par la monnaie européenne le 1er janvier 2002.</p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<p>Les transactions font l’objet de frais, quel que soit le mode utilisé.</p>
<p>Les cartes de crédit sont très répandues. <i>Visa</i>, <i>Mastercard </i>et <i>Eurocard </i>sont les plus acceptées. En revanche, elles ne sont pas attribuées aussi facilement qu’en France et nécessitent une période de surveillance de compte de trois ou six mois environ.</p>
<p>Les chèques ne sont pas utilisés au Luxembourg. La plupart des règlements se font soit par ordre de virement permanent (loyer, assurance, etc.), soit par web-banking, soit par un formulaire de virement standardisé (le plus souvent pré-imprimé par votre créancier).</p>
<p>Il n’y a aucune restriction au transfert d’actifs financiers à destination du Luxembourg.</p>
<h3 class="spip"><a id="sommaire_3"></a>Alimentation</h3>
<p><strong>Prix moyen d’un repas dans un restaurant</strong></p>
<p>Un menu du jour dans un restaurant de quartier : entre 15 et 20 euros.</p>
<p>Dans les autres restaurants, le prix moyen d’un repas est compris entre 30 et 45 euros, voire davantage selon le restaurant.</p>
<h3 class="spip"><a id="sommaire_4"></a>Evolution des prix</h3>
<p>En 2012 le taux d’inflation était de 2,7%. Un ralentissement est observé en 2013 (1,2% en novembre).</p>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/vie-pratique/article/cout-de-la-vie-109908). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
