# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/vie-pratique/article/transports#sommaire_1">Importation de véhicule</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/vie-pratique/article/transports#sommaire_2">Permis de conduire</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/vie-pratique/article/transports#sommaire_3">Code de la route</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/vie-pratique/article/transports#sommaire_4">Assurances et taxes</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/vie-pratique/article/transports#sommaire_5">Achat et location</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/vie-pratique/article/transports#sommaire_6">Immatriculation</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/vie-pratique/article/transports#sommaire_7">Entretien</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/vie-pratique/article/transports#sommaire_8">Carburant</a></li>
<li><a id="so_9" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/vie-pratique/article/transports#sommaire_9">Réseau routier</a></li>
<li><a id="so_10" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/vie-pratique/article/transports#sommaire_10">Transports en commun</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Importation de véhicule</h3>
<p>Il existe deux types d’importation : définitive ou temporaire pour un séjour touristique inférieur ou égal à 12 mois.</p>
<p><strong>Importation définitive</strong></p>
<p>L’importation définitive d’un véhicule à moteur (<i>motor vehicle</i>) est possible, mais nécessite d’accomplir de nombreuses formalités. Le véhicule étant généralement acheminé par la voie maritime, il est important que votre déménageur vous conseille quant aux démarches à effectuer et aux documents à fournir. Tous les accessoires et équipements de votre véhicule doivent être importés en même temps que ce dernier. Cette règle vaut également dans le cas d’une importation temporaire.</p>
<p>L’importation comporte trois étapes importantes et incontournables :</p>
<ul class="spip">
<li>l’obtention de l’autorisation d’importation du véhicule (<i>vehicle import approval</i>) auprès du <i>Vehicle Safety Standards Branch of the Department of Transport and Regional Services </i>(téléphone : (02) 62 74 75 06 - télécopie : (02) 62 74 60 13 - courriel : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/vie-pratique/article/transports#Vimports#mc#dotars.gov.au#" title="Vimports..åt..dotars.gov.au" onclick="location.href=mc_lancerlien('Vimports','dotars.gov.au'); return false;" class="spip_mail">Vimports<span class="spancrypt"> [at] </span>dotars.gov.au</a>). Ce service vérifiera que votre véhicule est conforme aux mesures de sécurité en vigueur dans le pays. La demande doit être impérativement déposée avant de se rendre en Australie.</li>
<li>le paiement des droits de douane calculés sur la valeur à l’argus du véhicule, de la taxe sur les biens et services GST (10%) et, pour les voitures de luxe, de la taxe (LCT) de 25%. Le paiement des droits et taxes s’effectue au port d’entrée en Australie.</li>
<li>l’obtention de l’autorisation du service d’inspection sanitaire (<i>Australian Quarantine and Inspection Service </i>- AQIS) dès l’arrivée du véhicule au port d’entrée.</li></ul>
<p>A l’arrivée, le véhicule sera soumis à une inspection sanitaire très rigoureuse qui pourra exiger un nettoyage du véhicule à la charge du propriétaire. Il est par conséquent préférable que votre véhicule soit impeccable et débarrassé des tapis de sol, des housses et de tout revêtement ayant pu recueillir des éléments naturels (terre, sable, herbe, feuilles, poils, cheveux…). Il fera aussi l’objet d’un contrôle douanier et doit donc être vide de tout effet personnel, à l’exception de ceux propres à son utilisation (matériel de camping, casque par exemple).</p>
<p>En cas de refus par les services de contrôle sanitaire, le véhicule sera définitivement réexpédié vers le pays d’origine du propriétaire.</p>
<p><strong>L’importation temporaire</strong></p>
<p>Elle concerne les touristes et les résidents temporaires qui souhaitent amener leur véhicule, moto ou camping-car en Australie pour un séjour n’excédant pas 12 mois. Dans certains cas, la durée du séjour pourra être supérieure à 12 mois.</p>
<p>En cas d’importation temporaire, le véhicule à moteur n’est pas soumis aux droits de douane, à condition que le véhicule soit exporté hors du territoire australien à l’expiration de la période accordée.</p>
<p><strong>Formalités obligatoires</strong></p>
<p>Le carnet de passages en douane est la meilleure façon de faciliter les démarches et justifie du retour en France du véhicule à l’expiration du séjour en Australie. Il s’obtient auprès de <a href="http://www.automobile-club.org/se-deplacer-mobilite/carnet-passages-en-douane.html" class="spip_out" rel="external">l’Automobile Club</a> en France. Son coût est de 210 euros auxquels vient s’ajouter une caution bancaire égale à 100% de la valeur à l’argus du véhicule (opération bancaire payante). Un virement ou un chèque de banque établi à l’ordre de l’Automobile Club vous évitera des frais. Le délai d’obtention est de quinze jours.</p>
<p>A défaut de carnet de passages en douane, un dépôt de garantie, en espèces ou sous forme d’empreinte bancaire, sera exigé. Il est égal au montant des droits de douane, de la GST et, dans le cas d’un véhicule de luxe, du montant de la taxe LCT.</p>
<p>En cas de vol ou de dégât causé à votre véhicule pendant votre séjour en Australie, vous devrez le signaler dans les plus brefs délais aux services de douane du port d’entrée du véhicule en Australie. Les cambriolages de voiture sont assez fréquents.</p>
<p>Vous devez également souscrire une assurance automobile couvrant la durée de votre séjour en Australie. L’inspection sanitaire sera effectuée dans les mêmes conditions décrites précédemment pour l’importation définitive.</p>
<p>Vous devrez également être titulaire d’un permis de conduire international (se reporter, dans cette même rubrique, à l’article sur le permis de conduire).</p>
<h3 class="spip"><a id="sommaire_2"></a>Permis de conduire</h3>
<p><strong>Séjour temporaire</strong></p>
<p>Pour pouvoir conduire en Australie, les ressortissants français doivent être en possession d’un permis de conduire international (<i>international driving licence</i>), accompagné de leur permis national. A noter que les autorités australiennes font une distinction entre le permis de conduire (<i>driving licence</i>) et l’autorisation de conduire (<i>driving permit</i>).</p>
<p>D’autre part, pour pouvoir louer une voiture, vous devez être en possession d’un permis de conduire délivré depuis plus d’un an et être âgé(e) de plus de 21 ans. Le permis international s’obtient auprès de la préfecture ou de la sous-préfecture de votre domicile en France. Il est gratuit et valable trois ans.</p>
<p><strong>Séjour définitif</strong></p>
<p>Si vous êtes résident permanent détenteur d’un visa, vous pouvez conduire, pendant les trois premiers mois qui suivent votre arrivée en Australie, avec votre permis de conduire français, à condition que celui-ci soit accompagné d’une traduction officielle en anglais. A l’issue de cette période de trois mois, vous devrez demander un permis de conduire australien. Pour se faire, vous devrez vous soumettre à un test de connaissances du code de la route, à un examen pratique de conduite et à un contrôle de la vue. En Australie, la délivrance des permis de conduire est de la compétence des différents Etats et Territoires. Vous devrez vous adresser à l’autorité compétente à raison du lieu de votre résidente permanente.</p>
<p><strong>Autorités australiennes chargées des transports et de la circulation par Etat et territoire</strong></p>
<ul class="spip">
<li>New South Wales (NSW) - <a href="http://www.rta.nsw.gov.au/" class="spip_out" rel="external">Roads and Trafic Authority</a></li>
<li>Victoria (VIC) - <a href="http://www.vicroads.vic.gov.au/" class="spip_out" rel="external">VicRoads</a></li>
<li>Queensland (QLD) - <a href="http://www.transport.qld.gov.au/" class="spip_out" rel="external">Queensland Transport</a></li>
<li>Tasmania (TAS) - <a href="http://www.transport.tas.gov.au/" class="spip_out" rel="external">Department of Infrastructure, Energy and Resources</a></li>
<li>Australian Capital Territory (ACT) - <a href="http://www.rego.act.gov.au/" class="spip_out" rel="external">Road Transport Authority</a></li>
<li>Northern Territory (NT) - <a href="http://www.nt.gov.au/transport/index.shtml" class="spip_out" rel="external">Northern Territory Transport Group</a></li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Code de la route</h3>
<p>Le code de la route (<i>road rules</i>) s’applique à l’ensemble du territoire australien. Cela n’a pas toujours été le cas. Depuis 1948, des mesures d’uniformisation ont été mises en œuvre pour arriver à un code de la route applicable au niveau national. Toutefois, ce n’est que dans les années 90 que les gouvernements des Etats et la Commission nationale du transport routier (<i>National Transport Commission</i>) ont vraiment commencé à coopérer en vue d’établir un code national de la route. Lorsque ce dernier est modifié, chaque Etat et Territoire doit en tenir compte et changer sa propre législation. Cependant, certaines règles ne sont pas encore appliquées dans une partie des Etats ou Territoires parce que ceux-ci tardent à le faire. C’est le cas, par exemple, du changement de limitation de vitesse de 60 km/h à 50km/h ou encore de la priorité aux carrefours signalisés.</p>
<p><strong>Quelques exemples de règles de circulation</strong></p>
<p>Elles s’appliquent en priorité aux véhicules à moteur et aux deux roues, mais également aux piétons et aux animaux empruntant les routes et les espaces de circulation (<i>road-related areas</i>) tels que les parkings, les trottoirs, etc. Une infraction (<i>offence</i>) au code de la route est sanctionnée par une amende ou une peine plus sévère pouvant aller jusqu’au retrait de permis.</p>
<ul class="spip">
<li>Limitation de vitesse (<i>speed-limit</i>) : la vitesse est généralement limitée à 60 km/h (50 km/h dans certaines régions). La limite est fixée à 40 km/h aux abords des écoles, à 25 km/h dans les zones traversées par des enfants (<i>children crossing</i>), à 10 km/h lors d’un croisement de plusieurs routes (<i>shared zone-limit) et entre 100 et 110 km/h sur les grands axes routiers selon les régions.</i></li>
<li>La conduite se fait à gauche.</li>
<li>Aux ronds-points, la priorité est à droite.</li>
<li>Il est interdit de doubler un véhicule qui tourne <i>(do not overtake turning vehicule).</i></li>
<li>Le panneau STOP des feux de signalisation <i>(traffic light) </i>est semblable à celui que nous connaissons en France.<i></i></li>
<li>Il faut céder le passage<i> (give way) </i>lorsque cela est obligatoire. Les véhicules de police, les ambulances et les camions de pompiers sont toujours prioritaires.</li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Assurances et taxes</h3>
<p><strong>Assurance</strong></p>
<p>L’assurance au tiers (<i>Green slip </i>ou <i>Compulsory Third Party insurance </i>- CTP) est obligatoire. Toutes les autres options sont facultatives, notamment l’assurance tous risques (<i>comprehensive insurance</i>). La souscription d’un contrat d’assurance-assistance auprès de <a href="http://www.aaa.asn.au/" class="spip_out" rel="external">l’Automobile Club d’Australie</a> ou de la <i><a href="http://www.nrma.com.au/" class="spip_out" rel="external">National Roads and Motorists’ Association</a> </i>(NRMA) est recommandée.</p>
<p>Le montant de la CTP est fonction de la catégorie et de la date de mise en circulation du véhicule, ainsi que de l’âge du conducteur et des risques assurés (vol, incendie…). L’assurance au tiers comporte plusieurs volets :</p>
<ul class="spip">
<li><i>compulsory third party </i> : assure une couverture en cas de dommages corporels causés aux passagers de votre véhicule. Son coût varie entre 300 et 450 dollars australiens.</li>
<li>certificat d’immatriculation (<i>certificate of registration</i>) ;</li>
<li><i>pink slip </i> : contrôle technique pour les véhicules de plus de cinq ans. La périodicité peut varier d’un Etat à l’autre. Le contrôle technique est obligatoire pour l’obtention du certificat d’immatriculation.</li></ul>
<p><strong>Vignette</strong> (<i>duty stamp</i>)</p>
<p>Le paiement de la vignette est imposé par les Etats et les territoires lors d’un changement de propriétaire ou de l’achat d’un véhicule neuf ou d’occasion. Son taux varie d’une région à l’autre entre 2 et 6,5% de la valeur du véhicule.</p>
<p>Chaque fois qu’un automobiliste vend sa voiture, la vignette est due et s’ajoute au prix de la voiture. La vignette s’applique également aux assurances au tiers. Son montant varie entre 5 et 11%.</p>
<h3 class="spip"><a id="sommaire_5"></a>Achat et location</h3>
<p><strong>Achat</strong></p>
<p>L’achat d’une voiture neuve s’effectue auprès d’un concessionnaire d’automobiles. Pour un véhicule d’occasion, vous pouvez vous adresser à un revendeur agréé, à un particulier ou encore l’acquérir lors d’une vente aux enchères (<i>auctions</i>). Prenez votre temps et comparez bien les différentes offres. Le <a href="http://rac.com.au/" class="spip_out" rel="external">Royal Automobile Club</a> (RAC) propose des conseils et une vérification du véhicule que vous souhaitez acquérir. Ce service est payant et est également accessible aux personnes qui ne sont pas membres du RAC.</p>
<p><strong>Marché automobile</strong></p>
<p>Les marques françaises représentées en Australie sont Peugeot, Citroën et Renault. Peu de modèles sont disponibles et les prix sont plus élevés qu’en France d’environ 10 %. Les marques étrangères représentées en France le sont également en Australie.</p>
<p>Le marché du 4x4 ou 4WD (<i>4 wheel drive</i>) et celui de l’occasion sont très développés (garages, concessionnaires, particuliers et ventes aux enchères). On trouve dans plusieurs villes des marchés de voitures d’occasion (<i>car markets</i>).</p>
<p><strong>Achat d’un véhicule d’occasion</strong></p>
<p>Si vous vous adressez à un revendeur agréé, vous aurez la garantie du titre de propriété du véhicule et serez couvert par le fonds de compensation des concessionnaires automobiles (<i>Motor Dealers Compensation Fund</i>). La durée de la garantie légale varie d’un Etat à l’autre. Une garantie de trois mois et 5 000 km pour tout véhicule de moins de dix ans avec un kilométrage inférieur à 160 000 km est tout à fait réglementaire. Pour un véhicule moins récent avec un kilométrage plus élevé, la garantie est inférieure, voire inexistante.</p>
<p>Si vous souhaitez acquérir une voiture d’occasion lors d’une vente aux enchères ou auprès d’un particulier, vous devrez vérifier si le véhicule est déjà gagé. Cette opération permet de savoir si le propriétaire est en mesure de vendre son véhicule. Si le véhicule n’est pas gagé, l’achat du certificat REVS vous coûtera 12 dollars australiens. Ceci vous évitera la saisie du véhicule acheté.</p>
<p><strong>Taxe sur la valeur ajoutée du véhicule</strong></p>
<p>L’achat d’une voiture est soumis à un impôt gouvernemental. Ce dernier comprend le <i>registration transfer fee</i> et la vignette.</p>
<p>Si vous achetez, auprès d’un concessionnaire d’automobiles, une voiture de luxe (<i>luxury car) </i>d’une valeur supérieure à 57 009 dollars australiens, vous serez redevable, en plus de la vignette, d’une taxe sur les véhicules de luxe. A titre d’exemple, cette taxe s’élèvera à 5 225 dollars australiens pour un véhicule d’une valeur de 80 000 dollars australiens et à environ 10 000 dollars australiens pour un véhicule coûtant 100 000 dollars australiens. Cette taxe ne sera, par contre, pas appliquée lors de l’achat d’un véhicule de luxe auprès d’un particulier.</p>
<p><strong>Location</strong></p>
<p>Les grandes marques internationales de location de véhicules sont représentées en Australie. Les véhicules se louent depuis une agence située en ville ou directement à l’aéroport, auquel cas une surtaxe est appliquée. Les véhicules sont en bon état et le service est de qualité. La gamme de véhicules proposés est vaste et comprend également les 4x4, les camping cars, les minibus et les véhicules utilitaires. La location par Internet est très répandue et permet de bénéficier de tarifs plus avantageux. N’hésitez pas à comparer les prix proposés par les différentes agences de location. Les prix sont fixés en fonction du loueur (parfois la différence est bien réelle, mais attention aux conditions de location), de la catégorie du véhicule, de la période et de la durée de location. Le prix comprend généralement le kilométrage illimité et l’assurance collision-dommages avec franchise, mais pas l’essence, certaines taxes locales et les assurances complémentaires.</p>
<p>Comme en France, lorsque vous prenez possession du véhicule, la prise d’une empreinte de votre carte bancaire est obligatoire. Les conditions d’assurance dépendent de la carte bancaire utilisée. Ainsi, il se peut que certains véhicules, tels que les 4x4 ou les camping cars, ne soient pas couverts par votre carte. Lorsque la franchise est élevée et que votre carte bancaire n’offre pas une bonne couverture, il peut être préférable d’en acheter une nouvelle moyennant quelques dollars supplémentaires par jour de location. Certaines agences le proposent. Sur place, vous devez vous faire préciser les conditions de location : kilométrage, assurance, nombre de conducteurs, etc. S’il s’agit d’un séjour touristique, pour pouvoir louer une voiture, vous devez être âgé(e) de plus de 21 ans et être en possession d’un permis de conduire international, accompagné de votre permis de conduire français. Ce dernier doit avoir été délivré depuis plus d’un an. Le permis international s’obtient auprès de la préfecture ou de la sous-préfecture de votre domicile en France. Il est gratuit et valable trois ans.</p>
<p>La déclaration du deuxième conducteur est gratuite dès lors que cette personne est domiciliée à la même adresse que le conducteur principal.</p>
<p>Si vous n’avez jamais conduit à gauche, il est plus prudent de demander un véhicule équipé d’une boîte automatique.</p>
<p>Avant de choisir le type de véhicule, renseignez-vous sur l’état des routes que vous comptez emprunter et sur la météo.</p>
<h3 class="spip"><a id="sommaire_6"></a>Immatriculation</h3>
<p>En Australie, l’immatriculation (<i>registration</i>) du véhicule est obligatoire pour tous les véhicules à moteur, y compris les deux roues et les poids lourds.</p>
<p>Le camping-car (<i>trailer</i>) appartient à la catégorie des poids lourds et est soumis à une réglementation spécifique. Il est également possible de l’immatriculer temporairement (<i>seasonal registration</i>), par exemple dans le cadre de la période estivale. Vous pouvez vous renseigner auprès du Centre de services aux consommateurs de votre ville dont vous trouverez les coordonnées sur le site de l’autorité chargée de l’immatriculation des véhicules dans votre Etat ou territoire de résidence (se reporter à l’article sur le permis de conduire dans cette même rubrique).</p>
<p>A noter que les 4x4 ne sont pas considérés comme des poids lourds.</p>
<p><strong>Immatriculation d’un premier véhicule acheté en Australie</strong></p>
<p>En Australie, le terme Rego se réfère au système australien d’immatriculation des véhicules. Lors de l’achat d’un véhicule, le concessionnaire effectue pour vous les formalités d’immatriculation. Toutefois, s’il s’agit d’un premier achat de véhicule en Australie et si vous n’avez jamais eu à faire à l’administration des routes et de la circulation (<i>Road and Traffic Authority </i>- RTA), vous devrez vous présenter au bureau d’enregistrement des automobiles (<i>motor registry) </i>de votre ville, muni d’un justificatif de votre identité. Ce bureau instruit aussi les demandes de nouvelle immatriculation à l’occasion d’un déménagement vers un autre Etat.</p>
<p>Si vous achetez un véhicule d’occasion à un particulier, vous disposez généralement d’un délai de 14 jours pour effectuer le transfert d’immatriculation et mettre le véhicule à votre nom. Il est toujours prudent de vérifier auprès du RTA que le véhicule mis en vente est bien immatriculé et assuré.</p>
<p><strong>Détail des frais</strong></p>
<ul class="spip">
<li>immatriculation (<i>registration fee</i>) ;</li>
<li>vignette (<i>duty stamp</i>) ;</li>
<li>plaques minéralogiques (<i>number plates</i>) ;</li>
<li>assurance (CTP <i>insurance premium/green slip</i>).</li></ul>
<p><strong>Immatriculation d’un véhicule importé définitivement</strong> (<i>overseas vehicle registration</i>)</p>
<p>L’immatriculation des véhicules est de la compétence de chaque Etat ou territoire. Pour connaître les conditions d’immatriculation d’un véhicule dans votre Etat de résidence, vous pouvez consulter les sites listés dans cette même rubrique dans l’article sur les permis de conduire.</p>
<p>Par exemple, certains Etats demandent, en plus de l’autorisation attestant que le véhicule est conforme aux normes australiennes, la preuve qu’il ne s’agit pas d’un véhicule volé. La démarche s’effectue auprès du poste chargé de l’évaluation de l’identité des véhicules (<i>Vehicle Identity Assessment Station) </i>de votre domicile. Enfin, l’ensemble des frais liés à l’immatriculation d’un véhicule importé sont plus élevés que ceux d’un véhicule acheté sur place.</p>
<p>Certaines régions, comme le Sud de l’Australie, exigent des détenteurs d’un véhicule immatriculé à l’étranger et importé temporairement en Australie l’obtention d’une autorisation de circulation territoriale.</p>
<p><strong>Détail des frais</strong></p>
<ul class="spip">
<li>immatriculation ;</li>
<li>vignette ;</li>
<li>plaques minéralogiques ;</li>
<li>inspection (<i>inspection fee</i>) ;</li>
<li>identification du véhicule (<i>vehicle identification inspection fee</i>) ;</li>
<li>assurance.</li></ul>
<h3 class="spip"><a id="sommaire_7"></a>Entretien</h3>
<p>En Australie, le marché des pièces détachées d’occasion est très développé. Il est désigné sous les termes suivants :</p>
<ul class="spip">
<li><i>car parts</i> ;</li>
<li><i>auto wreckers</i> ;</li>
<li><i>car breakers</i> ;</li>
<li><i>dismantlers</i>..</li></ul>
<p>Il est également possible de vendre ou d’acheter sur Internet tout type de pièces neuves, usagées ou recyclées de véhicules à moteur, tels que voitures, 4x4, motos, scooters, véhicules utilitaires, camions, engins agricoles, etc.</p>
<p>Vous trouverez dans les <a href="http://www.yellowpages.com.au/" class="spip_out" rel="external">pages jaunes</a> australiennes les coordonnées des magasins spécialisés en pièces détachées d’occasion ou recyclées.</p>
<h3 class="spip"><a id="sommaire_8"></a>Carburant</h3>
<p>Dans les stations-services, il est possible de se procurer de l’essence (<i>gasoline</i>) super, sans plomb (<i>unleaded gasoline</i>), diesel, ainsi que du gaz de pétrole liquéfié ou GPL (<i>liquefied petroleum gas</i>) et du biocarburant. Les biocarburants sont en plein essor en Australie. Les stations-services sont également équipées de bornes de recharge électrique.</p>
<p>De nombreux véhicules fonctionnent à l’essence sans plomb, ce carburant demeurant moins cher que le diesel.</p>
<p>En Australie, le principe du " Fuel Watch " est un service gratuit qui oblige les stations-services à indiquer chaque jour avant 14 heures les prix à la pompe qu’elles pratiqueront le lendemain et qu’elles sont tenues de respecter pendant 24 heures. Cette règle, qui doit permettre aux consommateurs de faire un choix avisé, est sensée s’appliquer à l’ensemble du territoire australien, mais est pour le moment testée dans l’Ouest du pays notamment à Perth.</p>
<p>Le prix de l’essence est un peu moins élevé dans les villes que dans l’arrière-pays.</p>
<h3 class="spip"><a id="sommaire_9"></a>Réseau routier</h3>
<p><strong>Sécurité</strong></p>
<p>La conduite sur l’ensemble des axes routiers australiens se fait à gauche. La plus grande prudence est de rigueur lorsque vous conduisez pour la première fois. Il est préférable de suivre préalablement un stage pratique. Il faut être également vigilant lorsque vous conduisez de nuit, les routes étant très souvent traversées par la faune, notamment par les kangourous.</p>
<p>Les limitations de vitesse et les distances de freinage doivent être strictement respectées, ainsi que le port de la ceinture de sécurité, sous peine de lourdes amendes. Le taux d’alcoolémie au volant est limité à 0,5 g/litre. Tout excès est très sévèrement puni par la loi.</p>
<p>L’<i>Australian Road Assessment Program</i> (AusRAP) est un programme d’information lancé par l’association automobile australienne (AAA). Son but est de fournir des évaluations sur les risques routiers dans toute l’Australie en mettant à disposition des cartes routières indiquant les axes à risque connus pour leurs accidents graves et mortels.</p>
<p><strong>En cas d’accident</strong></p>
<p>Lors d’un accident sans dommage corporel, il est toujours conseillé de dresser un constat en présence de la police. Celle-ci peut être jointe en composant le 000 d’un téléphone fixe ou portable. Ce numéro permet aussi de joindre les secours (ambulance, pompiers, police), il n’y pas de SAMU comme en France. L’opératrice vous demandera de préciser votre choix. Ces services ne concernent que les situations d’urgence. Il est important d’avoir sur soi sa carte de groupe sanguin.</p>
<p>Vous devrez ensuite prendre contact avec votre assurance et lui fournir les éléments suivants : le nom de l’autre conducteur, le numéro d’immatriculation de son véhicule, les informations relatives à son permis de conduire, son adresse, son téléphone, les coordonnées complètes de sa compagnie d’assurance, ainsi que le lieu, la date et l’heure exacts de l’accident et les noms et coordonnées des témoins, etc.</p>
<p>Dans toute l’Australie, il existe des centres de réparation d’accidents (<i>Accident Repair Centre</i>). Leurs coordonnées figurent dans les pages jaunes du bottin téléphonique. Lors de la souscription d’un contrat d’assurance, n’oubliez pas de vous faire préciser l’assistance offerte en cas d’accident.</p>
<p><strong>Etat du réseau routier</strong></p>
<p>L’état des routes est bon, surtout à proximité des grandes villes. A l’extérieur des villes et notamment dans le " bush ", il est préférable de circuler en 4X4. L’approvisionnement en carburant est correct, sauf en rase campagne.</p>
<p>Pour tout voyage dans les zones désertiques, il est préférable de voyager en groupe, de passer par une agence spécialisée, de se munir de réserves d’eau, de vivres et de carburant, d’être accompagné d’un guide, de prévenir les autorités locales afin que des recherches soient effectuées en cas de retard anormal dans l’itinéraire et, enfin, de consulter la météo. Dans ces régions, surtout dans le territoire du Nord (route Adélaïde-Darwin), circulent à grande vitesse des camions à plusieurs remorques (<i>road trains</i>) qu’il est dangereux de dépasser. En rase campagne, surtout à l’aube ou au crépuscule, il n’est pas rare de voir des animaux sauvages (kangourous, émeus, rongeurs) traverser la route : il convient donc de faire preuve de prudence et de respecter les limitations de vitesse.</p>
<p>La liberté de circulation est totale sauf dans certains sites aborigènes où des autorisations sont nécessaires. D’autre part, en Australie-Méridionale, certaines zones désertiques, encore contaminées par les essais nucléaires britanniques des années 50, restent fermées à la circulation et sont signalées.</p>
<p>Les centres d’information touristique sont de bonnes sources de renseignements. Les cartes touristiques sont très détaillées et il existe diverses brochures et guides. L’assurance des véhicules est obligatoire. Une assurance-assistance auprès de l’Automobile Club d’Australie ou de la NRMA est recommandée.</p>
<p>L’Australie compte également de nombreuses pistes cyclables. Pour en savoir plus, vous pouvez consulter le site Internet suivant : <a href="http://www.bikepaths.com.au/" class="spip_out" rel="external">www.bikepaths.com.au</a>.</p>
<h3 class="spip"><a id="sommaire_10"></a>Transports en commun</h3>
<p><strong>Transports urbains</strong></p>
<p>Les transports en commun (<i>public transportation</i>) disponibles sont :</p>
<ul class="spip">
<li>Dans toutes les villes, le bus et les taxis individuels ;</li>
<li>Le métro et le <a href="http://www.metrotransport.com.au/" class="spip_out" rel="external">monorail à Sydney</a> et à <a href="http://www.metlinkmelbourne.com.au/" class="spip_out" rel="external">Melbourne</a> ;</li>
<li>Le <a href="http://www.yarratrams.com.au/desktopdefault.aspx" class="spip_out" rel="external">tramway à Melbourne</a></li></ul>
<p><strong>Transport ferroviaire</strong></p>
<p>Le train reste certainement le moyen de transport le plus avantageux pour découvrir l’Australie et se rendre dans les principales villes. De plus, il est convivial et plaisant grâce à la variété et à la beauté des paysages qu’il permet de découvrir. Toutefois, les voyages en train peuvent être très longs et compliqués. La vitesse moyenne n’est que de 80 km/h et les pointes ne dépassent pas les 115 km/h, l’écartement des rails n’étant pas le même partout.</p>
<ul class="spip">
<li>Au départ de Melbourne : <a href="http://www.metlinkmelbourne.com.au/" class="spip_out" rel="external">www.metlinkmelbourne.com.au</a></li>
<li>Au départ de Perth : <a href="http://www.transperth.wa.gov.au/" class="spip_out" rel="external">www.transperth.wa.gov.au</a></li>
<li>Au départ de Sydney : <a href="http://www.cityrail.info/" class="spip_out" rel="external">www.cityrail.info</a></li></ul>
<p>Le <i>Countrylink East Coast Discovery Pass</i>, valable six mois, permet de visiter la côte Ouest de l’Australie à bord des trains et bus de cet important réseau (<i>CountryLink</i>) qui dessert 334 destinations.</p>
<p>Il est également possible de monter dans le train avec sa bicyclette.</p>
<p>Pour en savoir plus :</p>
<ul class="spip">
<li><a href="http://www.countrylink.info/" class="spip_out" rel="external">Country Link</a></li>
<li><a href="http://www.gsr.com.au/" class="spip_out" rel="external">Indian Pacific</a> (célèbre ligne reliant Sydney, Adelaide et Perth en trois jours et neuf heures)</li>
<li><a href="http://www.qr.com.au/" class="spip_out" rel="external">Queensland Rail</a></li></ul>
<p><strong>Transport routier</strong></p>
<p>Comme le train, l’autocar (<i>coach</i>) est un bon moyen pour découvrir l’Australie à un prix raisonnable. Les transports en autocar sont confortables. Plusieurs autres compagnies desservent les axes Sydney-Canberra et Sydney-Melbourne et proposent des tours touristiques à la carte tout en vous faisant découvrir les lieux intéressants.</p>
<p>Pour en savoir plus :</p>
<ul class="spip">
<li><a href="http://brisbanebuslines.com.au/" class="spip_out" rel="external">Société de transports individuels à Brisbane</a></li>
<li><a href="http://www.buslines.com.au/" class="spip_out" rel="external">Central de réservation pour trains, bus et autocars</a></li>
<li><a href="http://www.sitacoaches.com.au/" class="spip_out" rel="external">Réseau d’autocars à Sydney et Melbourne</a></li>
<li><a href="http://www.sunbus.com.au/" class="spip_out" rel="external">Réseau d’autocars dans le Nord du Queensland</a></li></ul>
<p><strong>Transport maritime</strong></p>
<p><a href="http://www.tt-line.com.au/" class="spip_out" rel="external">Compagnie maritime</a> desservant la Tasmanie.</p>
<p><strong>Transport aérien</strong></p>
<p>Les transports aériens sont excellents. La plupart des villes d’Australie possèdent deux aéroports, un international et un domestique pour les vols intérieurs entre les principales villes du pays. Un vol quasiment toutes les heures est proposé entre les villes les plus fréquentées. Ces aéroports sont généralement proches. Des navettes payantes les relient et un service de transport à destination des aéroports est assuré régulièrement. Compte tenu de l’étendue du pays, le voyage en avion est courant. Le marché du <i>low cost </i>est occupé par deux compagnies, Virgin Blue du groupe Virgin et Jetstar de Qantas. Il est facile de trouver des billets à un coût très abordable. Lors de l’achat de votre billet d’avion pour l’Australie, pensez à étudier les options incluant le vol intérieur. Les prix sont parfois plus intéressants lorsque la réservation est faite à l’avance.</p>
<p><i>Mise à jour : octobre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/vie-pratique/article/transports). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
