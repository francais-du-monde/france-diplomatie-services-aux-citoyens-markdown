# Communications

<h4 class="spip">Téléphone – Internet</h4>
<p>Les liaisons téléphoniques avec la France sont très bonnes.</p>
<p><strong>Indicatifs</strong></p>
<p>Depuis la France : 00 44 + le numéro d’abonné sans le "0" local (soit 10 chiffres).</p>
<p>Depuis le Royaume-Uni : 00 33 + le numéro d’abonné sans le "0" local (soit 9 chiffres).</p>
<p>Indicatifs pour Londres : 020 7 et 020 8.</p>
<p>Les téléphones publics acceptent les pièces de 10p, 20p, 50p, 1GBP. British Telecom propose une carte à puce (<i>BT Chargecard) </i>pour une <a href="http://www.payphones.bt.com/.%20La%20plupart%20des%20t%C3%A9l%C3%A9phones%20publics%20acceptent%20les%20cartes%20de%20cr%C3%A9dit.&lt;/p&gt;%0A%0A&lt;p&gt;Site%20des%20%5Bpages%20jaunes-&gt;http://www.yell.com/" class="spip_out" rel="external">utilisation plus large et plus facile des téléphones publics</a>.</p>
<h4 class="spip">Utilisation de votre portable français</h4>
<p>Vous pouvez utiliser votre téléphone portable partout en Europe et dans de nombreuses autres régions du monde, grâce à la norme technique GSM de l’Union européenne. Les personnes qui utilisent leur téléphone portable à l’étranger réalisent d’importantes économies – de l’ordre de 60 % – grâce au règlement de l’UE sur l’itinérance (« roaming »), qui plafonne le coût des appels téléphoniques effectués ou reçus dans un autre pays de l’UE (« eurotarif »). De plus, une facturation à la seconde a été introduite après les premières 30 secondes pour les appels effectués, et dès la première seconde pour les appels reçus. Le coût d’envoi d’un SMS depuis l’étranger a lui aussi nettement diminué. Renseignez-vous auprès de votre opérateur français pour les tarifs.</p>
<p>Les clients reçoivent un message automatique les informant du coût des appels, des SMS et des services de transmission de données à l’étranger (en cas de navigation sur internet ou de téléchargement d’un film, p. ex.). Les prix de gros que les opérateurs se facturent mutuellement sont plafonnés, ce qui devrait réduire les coûts supportés par les consommateurs. Un mécanisme a également été mis en place afin d’éviter aux consommateurs de devoir payer des factures astronomiques pour des services d’itinérance : après un premier avertissement, la connexion internet mobile est suspendue dès que le montant atteint un certain niveau. Une limite de 50 euros par mois s’applique par défaut si le client n’a pas défini un montant spécifique.</p>
<p>Vous trouverez sur un <a href="http://ec.europa.eu/information_society/activities/roaming/index_en.htm" class="spip_out" rel="external">site web de l’UE</a>, en anglais) les eurotarifs appliqués par les opérateurs des pays membres, ainsi que les liens vers leurs sites. Vous pourrez également y comparer les tarifs pratiqués pour l’envoi de SMS ou l’utilisation de services de données.</p>
<h4 class="spip">Téléphoner gratuitement par Internet </h4>
<p>Il est généralement très avantageux d’utiliser <strong>des logiciels de téléphonie sur IP</strong> (Skype, Google talk, Live messenger, Yahoo ! messenger, etc.) car les tarifs proposés à l’international sont souvent moindre que ceux des opérateurs locaux exploitant des lignes classiques, voire gratuits dans certaines conditions.</p>
<h4 class="spip">Poste</h4>
<p>Le courrier est acheminé dans un délai moyen de deux à quatre jours.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/vie-pratique/article/communications). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
