# Fiscalité

<h2 class="rub22819">Fiscalité du pays</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/fiscalite/#sommaire_1">Traitements, salaires, pensions et rentes</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/fiscalite/#sommaire_2">Situation des professeurs, chercheurs et étudiants</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/fiscalite/#sommaire_3">Autres catégories de revenus</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/fiscalite/#sommaire_4">Lieu de dépôt en Italie</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/fiscalite/#sommaire_5">Modalités de paiement des impôts</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/fiscalite/#sommaire_6">Barème de l’impôt</a></li></ul>
<p>La plupart des impôts et taxes en Italie sont calculés par le contribuable. Les déclarations et les paiements y afférents sont <strong>télétransmis à l’Agence des impôts </strong>(<i>Agenzia delle Entrate</i>).</p>
<p>Une série d’allégements fiscaux sont prévus, parmi lesquels :</p>
<ul class="spip">
<li>déductions pour les enfants à charge ou pour d’autres membres de la famille à charge, qui ont un revenu imposable inférieur au seuil fixé, à condition qu’ils fassent partie du noyau familial ;</li>
<li>déductions pour les familles nombreuses, comptant au moins quatre enfants ;</li>
<li>frais de scolarité pour la fréquentation de l’enseignement secondaire et universitaire ;</li>
<li>frais médicaux et de soins de santé ;</li>
<li>déduction à hauteur de 36% des frais de réhabilitation du patrimoine immobilier et à hauteur de 55% des frais engagés aux fins d’économiser l’énergie.</li></ul>
<p>Outre les traditionnelles mesures contenues dans le texte unique sur les impôts sur le revenu, d’autres mesures peuvent être introduites chaque année par les lois de finances ou par les dispositions visant les différentes formes d’incitations fiscales qui allient les besoins de la collectivité et la relance de certains secteurs productifs ou les formes particulières d’incitations fiscales ou de réduction des charges fiscales au niveau régional et qui pourront être introduites ou supprimées par le fédéralisme fiscal.</p>
<p>Pour cette raison, <strong>il est recommandé de vérifier chaque année quelles sont les mesures supprimées et quelles sont celles en vigueur, en étudiant de manière approfondie le traitement fiscal de chaque dépense supportée</strong>.</p>
<p>Les sociétés et autres personnes morales sont assujetties à un impôt sur les sociétés (IRES) et à une taxe assise sur la production (IRAP). Elles sont en général redevables de la TVA (IVA). <strong>Les personnes physiques sont assujetties à l’impôt sur le revenu (IRPEF).</strong></p>
<p>Les salaires, les pensions, les revenus du travail indépendant, certains revenus de capitaux <strong>sont soumis à une retenue à la source </strong>qui, en fonction des cas visés, est libératoire ou non. Celle-ci est opérée par l’employeur ou par l’organisme payeur, appelé sostituto d’imposta, qui la reverse au Trésor.</p>
<p>Il existe deux types de déclaration d’impôt sur le revenu : une <strong>déclaration simplifiée n° 730 </strong>(déclaration simplifiée concernant les titulaires de salaires, pensions, revenus immobiliers ayant des charges déductibles) et une <strong>déclaration normale « UNICO </strong> » (déclaration normale unifiée, concernant les personnes physiques titulaires d’autres revenus que ceux ci-dessus et tenus au respect d’obligations comptables).</p>
<p><strong>Modalités d’imposition des revenus catégoriels au regard des dispositions de la Convention fiscale franco-italienne signée à Venise le 5 octobre 1989.</strong></p>
<h3 class="spip"><a id="sommaire_1"></a>Traitements, salaires, pensions et rentes</h3>
<h4 class="spip">Rémunérations privées</h4>
<h5 class="spip">Principe</h5>
<p>L’article 15, paragraphe 1 de la convention prévoit, sous réserve des dispositions des articles suivants, que les traitements et salaires d’origine privée qu’un résident d’un Etat reçoit au titre d’un emploi salarié ne sont imposables que dans cet Etat. Toutefois, si l’emploi est exercé dans l’autre Etat contractant, les rémunérations perçues sont imposables dans cet autre Etat.</p>
<p>En pratique, l’impôt est donc dû dans l’Etat d’exercice de l’activité.</p>
<h5 class="spip">Exceptions à cette règle générale</h5>
<p>Le maintien de l’imposition dans l’Etat de résidence du bénéficiaire est prévu par le paragraphe 2 du même article, dans le cas d’exercice d’une activité de courte durée, sous réserve de trois conditions simultanément remplies :</p>
<ul class="spip">
<li>le séjour temporaire du bénéficiaire dans l’autre Etat ne dépasse pas une durée totale de 183 jours au cours de l’année fiscale écoulée ;</li>
<li>les rémunérations sont payées par un employeur qui n’est pas résident de l’Etat d’exercice ;</li>
<li>la rémunération ne doit pas être à la charge d’un établissement stable ou d’une base fixe de l’employeur dans l’Etat d’exercice.</li></ul>
<p>Il résulte des dispositions du paragraphe 3 de l’article 15 de la convention que les revenus professionnels des salariés exerçant à bord d’un navire ou d’un aéronef en trafic international ne sont imposables que dans l’Etat où se trouve la direction effective de l’entreprise.</p>
<h4 class="spip">Rémunérations publiques</h4>
<h5 class="spip">Principe</h5>
<p>L’article 19 paragraphes 1-a et 2-a indique que les traitements, salaires, rémunérations et <strong>pensions de retraite </strong>publics payés par un Etat ou une personne morale de droit public de cet Etat sont imposables dans l’Etat qui les verse.</p>
<p>Ne sont pas concernées par cette règle les rémunérations et pensions publiques correspondant à une activité industrielle ou commerciale.</p>
<h5 class="spip">Exceptions (art. 19 1-b et 19 2-b)</h5>
<p>Ces revenus publics sont imposables dans l’Etat où sont rendus les services si le bénéficiaire en est le résident et en possède la nationalité.</p>
<p>S’agissant des pensions publiques, elles ne sont pas imposables dans l’Etat d’où proviennent les pensions si le bénéficiaires est un résident de l’autre Etat et s’il en possède la nationalité sans avoir celle de l’Etat payeur.</p>
<h4 class="spip">Pensions et rémunérations similaires</h4>
<p>L’article 18 de la convention prévoit que les pensions de retraite de source privée, ainsi que les rémunérations similaires, sont imposables dans l’Etat dont le bénéficiaire est un résident, sauf pour les pensions de sécurité sociale. Ces dernières sont en effet imposables dans les deux Etats et l’Etat de résidence octroie un crédit d’impôt.</p>
<h3 class="spip"><a id="sommaire_2"></a>Situation des professeurs, chercheurs et étudiants</h3>
<h4 class="spip">Professeurs et chercheurs</h4>
<p>Les rémunérations qu’un professeur ou un chercheur résident d’un Etat et qui séjourne dans l’autre Etat à seule fin d’y enseigner ou de s’y livrer à des travaux de recherche entrepris dans l’intérêt public, ne sont pas imposables dans l’Etat d’accueil pendant deux ans (art. 20 de la convention) ), sous réserve qu’elles soient effectivement imposées dans l’Etat de résidence.</p>
<h4 class="spip">Etudiants</h4>
<p>Les étudiants, résidents d’un Etat, qui séjournent dans l’autre Etat à seule fin d’y poursuivre des études ou une formation n’y sont pas imposables si les sommes reçues sont d’origine étrangère (art. 21 de la convention).</p>
<h3 class="spip"><a id="sommaire_3"></a>Autres catégories de revenus</h3>
<h4 class="spip">Bénéfices industriels et commerciaux</h4>
<p>L’article 7 de la convention stipule que les bénéfices d’une entreprise sont imposables sur le territoire où est situé l’établissement stable. La notion d’établissement stable est définie à l’article 5 de la convention.</p>
<h4 class="spip">Bénéfices non commerciaux</h4>
<p>Aux termes de l’article 14, les revenus qu’un résident d’un Etat tire d’une profession libérale ne sont imposables que dans cet Etat sauf s’il dispose de façon habituelle d’une base fixe dans l’autre Etat.</p>
<h4 class="spip">Revenus immobiliers</h4>
<p>Les revenus provenant de biens immobiliers sont imposables dans l’Etat où ces biens sont situés (art. 6 de la convention). Il en est de même pour les gains provenant de la cession de ces biens (art. 13 de la convention). Ces revenus sont également imposables dans l’Etat de résidence où un crédit d’impôt est octroyé.</p>
<h4 class="spip">Revenus de capitaux mobiliers</h4>
<h5 class="spip">Dividendes</h5>
<p>Le terme « dividendes » , au sens de l’article 10 de la convention désigne les revenus provenant d’actions, actions ou bons de jouissance, parts de mine, parts de fondateur ou autres parts bénéficiaires à l’exception des créances ainsi que les revenus soumis au régime des distributions par la législation de l’Etat dont la société distributrice est un résident.</p>
<p>Toutefois, le paragraphe 2 du même article précise que ces dividendes sont aussi imposables dans l’Etat dont la société est le résident.</p>
<h5 class="spip">Intérêts</h5>
<p>Ce terme désigne les revenus de fonds publics, des obligations d’emprunts, des créances de toute nature ainsi que tous autres produits assimilables aux revenus de sommes prêtées par la législation fiscale de l’Etat d’où proviennent les revenus.</p>
<p>Selon l’article 11, paragraphe 1, les intérêts provenant d’un Etat et payés à un résident de l’autre Etat sont imposables dans cet autre Etat.</p>
<p>Le paragraphe 6 du même article stipule cependant que ces dispositions ne s’appliquent pas si le bénéficiaire des intérêts, résident d’un Etat, exerce dans l’autre Etat une activité industrielle, commerciale ou indépendante par l’intermédiaire d’un établissement ou d’une base stable qui y est situé et que la créance génératrice des intérêts s’y rattache effectivement.</p>
<h3 class="spip"><a id="sommaire_4"></a>Lieu de dépôt en Italie</h3>
<p>L’usager peut télédéclarer lui-même (internet) ou s’adresser soit à un intermédiaire habilité (centres d’assistance fiscale (CAF), professionnels de la fiscalité, guichets de poste ou de banques, employeurs…) soit à l’Agence des impôts locale.</p>
<p>De la même façon, les paiements peuvent être télétransmis par le contribuable lui-même (imprimé F24 online) ou effectués auprès des guichets de poste, de banques ou de concessionnaires du recouvrement (imprimé F24). Ces intermédiaires se chargent ensuite de la télétransmission à l’administration fiscale.</p>
<p>Pour accéder au téléservice <i>Fisconline</i>, il est nécessaire d’obtenir un code d’accès personnel (<i>codice pin</i>) qui permettra en suite de télédéclarer et d’accéder au dossier fiscal (<i>cassetto fiscale</i>). Toutes les informations nécessaires sont disponibles sur le site de l’Agence à la rubrique <i>servizi telematici</i>.</p>
<h3 class="spip"><a id="sommaire_5"></a>Modalités de paiement des impôts</h3>
<p>Ces informations sont valables pour une activité salariée ou non et pour les sociétés (retenue à la source paiement provisionnel et date de paiements).</p>
<h4 class="spip">Impôt sur le revenu</h4>
<p>Les salaires, les revenus du travail indépendant, certains revenus de capitaux sont soumis à une retenue à la source qui, en fonction des cas visés, est libératoire ou non.</p>
<p>En cas d’auto-liquidation, un acompte (99% de l’impôt de l’année précédente) doit être versé l’année de perception des revenus, en une ou deux échéances selon le montant. Le solde doit ensuite être acquitté l’année N+1.</p>
<p>Les dates limites de paiement des acomptes et du solde sont consultables sur le site de l’<a href="http://www.agenziaentrate.gov.it/" class="spip_out" rel="external">Agenzia delle Entrate</a>.</p>
<h4 class="spip">Impôt sur les sociétés</h4>
<p>L’impôt sur les sociétés donne également lieu au paiement d’un acompte (99% de l’impôt de l’année précédente)et d’un solde. Les dates limites sont les mêmes que celles appliquées aux personnes.</p>
<p>Le paiement s’effectue par internet (ou système Entratel), auprès des banques conventionnées ou agences postales, et auprès des concessionnaires du recouvrement. L’impôt des sociétés est de l’ordre de 27,5%. Il existe aussi un autre impôt pour les entreprises dénommé IRAP (impôt régional sur les activités productives) qui représente 3,9 % de la base imposable. Cet impôt varie selon les régions.</p>
<h3 class="spip"><a id="sommaire_6"></a>Barème de l’impôt</h3>
<p>Une déduction "générale" du revenu global et une déduction "supplémentaire" octroyée en fonction du niveau de ce revenu est appliqué à tous les contribuables et conduit à l’établissement d’un seuil de non-imposition.</p>
<p><strong>Sont exonérés d’impôt sur le revenu (IRPEF) :</strong></p>
<ul class="spip">
<li>les salariés sans charge de famille dont le revenu global est inférieur ou égal à 8 000 euros ;</li>
<li>les retraités dont le revenu global est inférieur ou égal à 7 500 euros ;</li>
<li>le travailleur indépendant dont le revenu global est inférieur ou égal à 4 800 euros.</li></ul>
<h4 class="spip">Taux d’imposition des plus-values</h4>
<p>Plus-values sur participations : elles sont exonérées sous conditions.<br class="manualbr">Plus-values immobilières : taux progressif de l’impôt sur le revenu.</p>
<p><strong>Coordonnées des centres d’information fiscale</strong></p>
<p>Numéro d’appel des centres d’appel téléphonique :</p>
<ul class="spip">
<li>848800444 (site de l’Agenzia delle Entrate), pour les appels depuis l’Italie</li>
<li>06-96668907, pour les appels à partir d’un portable</li>
<li>003906-96668933, pour les appels depuis l’étranger</li></ul>
<p>L’Agenzia delle Entrate propose un guide pratique (en italien) sur l’impôt.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-italie-fiscalite-article-convention-fiscale.md" title="Convention fiscale">Convention fiscale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-italie-fiscalite-article-fiscalite-du-pays.md" title="Fiscalité du pays">Fiscalité du pays</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/fiscalite/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
