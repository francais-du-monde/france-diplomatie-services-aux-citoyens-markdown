# Belgique

<p>Au <strong>31 décembre 2014, 117 782 Français</strong> étaient enregistrés auprès du consulat général de France à Bruxelles, au registre mondial des Français établis hors de France. Les ressortissants français en Belgique constituent l’une des plus importantes communautés françaises dans le monde : on l’évalue en effet à quelque 250 000 personnes, résidant majoritairement dans la circonscription consulaire de Bruxelles ; près de 80% exercent leur activité dans le secteur tertiaire. La communauté française occupe le second rang des communautés étrangères en Belgique derrière les Italiens.</p>
<p>Les échanges commerciaux franco-belges sont très importants et l’on estime à plus de 1500 le nombre des implantations françaises en Belgique (dont près de la moitié à Bruxelles) employant environ 200 000 personnes. Les grands groupes français occupent des places de premier plan en Belgique dans d’importants secteurs tels que l’énergie (Total et Suez en particulier), les industries d’équipement électro-mécaniques (Alcatel, Alstom, Snecma), les télécommunications mobiles (Orange), la distribution (Carrefour, Fnac), les banques (BNP Paribas Fortis, Axa, …)</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/dossiers-pays/belgique/" class="spip_in">Une description de la Belgique, de sa situation politique et économique</a> ;</li>
<li><a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/belgique/" class="spip_in">Des informations actualisées sur les conditions locales de sécurité en Belgique</a> ;</li>
<li><a href="http://www.ambafrance-be.org/" class="spip_out" rel="external">Ambassade de France en Belgique</a>.</li></ul>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-belgique-entree-et-sejour.md">Entrée et séjour</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-belgique-entree-et-sejour-article-passeport-visa-permis-de-travail.md">Passeport, visa, permis de travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-belgique-entree-et-sejour-article-demenagement-109794.md">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-belgique-entree-et-sejour-article-vaccination-109795.md">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-belgique-entree-et-sejour-article-animaux-domestiques-109796.md">Animaux domestiques</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-belgique-emploi-stage.md">Emploi, stage</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-belgique-emploi-stage-article-marche-du-travail.md">Marché du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-belgique-emploi-stage-article-reglementation-du-travail-109798.md">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-belgique-emploi-stage-article-recherche-d-emploi.md">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-belgique-emploi-stage-article-stages.md">Stages</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-belgique-protection-sociale.md">Protection sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-belgique-protection-sociale-article-regime-local-de-securite-sociale.md">Régime local de sécurité sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-belgique-protection-sociale-article-convention-de-securite-sociale-109805.md">Convention de sécurité sociale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-belgique-fiscalite-22794.md">Fiscalité</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-belgique-fiscalite-22794-article-fiscalite-du-pays-109806.md">Fiscalité du pays</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-belgique-fiscalite-22794-article-convention-fiscale-109807.md">Convention fiscale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-belgique-vie-pratique.md">Vie pratique</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-belgique-vie-pratique-article-logement.md">Logement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-belgique-vie-pratique-article-sante.md">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-belgique-vie-pratique-article-scolarisation.md">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-belgique-vie-pratique-article-cout-de-la-vie-109812.md">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-belgique-vie-pratique-article-transports.md">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-belgique-vie-pratique-article-communications-109811.md">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-belgique-vie-pratique-article-loisirs-et-culture.md">Loisirs et culture</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-belgique-vie-pratique-article-pour-en-savoir-plus-109839.md">Pour en savoir plus</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-belgique-presence-francaise.md">Présence française</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-belgique-presence-francaise-article-presence-francaise.md">Présence française</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
