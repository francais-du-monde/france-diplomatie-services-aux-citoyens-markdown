# Pour en savoir plus

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/vie-pratique/article/pour-en-savoir-plus-109839#sommaire_1">Bibliographie</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Bibliographie</h3>
<h4 class="spip">Des guides</h4>
<ul class="spip">
<li><i>Petit futé Belgique</i>. Petit futé. 2011</li>
<li><i>Le Routard Belgique</i>. Hachette. 2013</li>
<li><i>Guide vert Belgique - Luxembourg</i>. Michelin. 2013.</li>
<li><i>Bruxelles, Bruges, Gand, Anvers</i>. Hachette, 2001. (Coll. Voir).</li>
<li><i>« Une vie de pintade à Bruxelles » (2012)</i></li></ul>
<h4 class="spip">Histoire, politique, société </h4>
<ul class="spip">
<li><i>L’état de la Belgique</i> (J. Heinen). Ed. de Boeck Université, 2004. (Coll. Histoire et politique).</li>
<li><i>Comprendre la Belgique contemporaine</i>. (S. Jaumain). Ed. de Boeck Université, 2003.</li>
<li><i>Belgique : un jeu de cartes</i>. (Arnaud Huftier). Presses Universitaires de Valenciennes, 2003.</li>
<li><i>La Belgique</i>. (Georges-Henri Dumont). PUF, 12/11/2008. (Coll. Que sais-je ? N° 319).</li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/vie-pratique/article/pour-en-savoir-plus-109839). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
