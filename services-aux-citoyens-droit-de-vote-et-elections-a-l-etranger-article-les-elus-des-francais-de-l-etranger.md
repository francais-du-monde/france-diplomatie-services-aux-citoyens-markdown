# Les élus des Français de l’étranger

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/droit-de-vote-et-elections-a-l-etranger/article/les-elus-des-francais-de-l-etranger#sommaire_1">Députés des Français établis hors de France</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/droit-de-vote-et-elections-a-l-etranger/article/les-elus-des-francais-de-l-etranger#sommaire_2">Sénateurs représentant les Français établis hors de France</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/droit-de-vote-et-elections-a-l-etranger/article/les-elus-des-francais-de-l-etranger#sommaire_3">Conseillers de l’Assemblée des Français de l’étranger</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/droit-de-vote-et-elections-a-l-etranger/article/les-elus-des-francais-de-l-etranger#sommaire_4">Conseillers consulaires</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Députés des Français établis hors de France</h3>
<p><a href="http://www.assemblee-nationale.fr/14/tribun/fiches_id/610968.asp" class="spip_out" rel="external">M. AMIRSHAHI Pouria</a> - 9e circonscription</p>
<p><a href="http://www.assemblee-nationale.fr/14/tribun/fiches_id/610891.asp" class="spip_out" rel="external">M. CORDERY Philip</a> - 4e circonscription</p>
<p><a href="http://www.assemblee-nationale.fr/14/tribun/fiches_id/610860.asp" class="spip_out" rel="external">M. CORONADO Sergio</a> - 2e circonscription</p>
<p><a href="http://www.assemblee-nationale.fr/14/tribun/fiches_id/695100.asp" class="spip_out" rel="external">M. HABIB Meyer</a> - 8e circonscription</p>
<p><a href="http://www.assemblee-nationale.fr/14/tribun/fiches_id/610936.asp" class="spip_out" rel="external">M. LE BORGN’ Pierre-Yves</a> - 7e circonscription</p>
<p><a href="http://www.assemblee-nationale.fr/14/tribun/fiches_id/346886.asp" class="spip_out" rel="external">M. LEFEBVRE Frédéric</a> - 1re circonscription</p>
<p><a href="http://www.assemblee-nationale.fr/14/tribun/fiches_id/610905.asp" class="spip_out" rel="external">M. LEROY Arnaud</a> - 5e circonscription</p>
<p><a href="http://www.assemblee-nationale.fr/14/tribun/fiches_id/2073.asp" class="spip_out" rel="external">M. MARIANI Thierry</a> - 11e circonscription</p>
<p><a href="http://www.assemblee-nationale.fr/14/tribun/fiches_id/2089.asp" class="spip_out" rel="external">M. MARSAUD Alain</a> - 10e circonscription</p>
<p><a href="http://www.assemblee-nationale.fr/14/tribun/fiches_id/643069.asp" class="spip_out" rel="external">M. PREMAT Christophe</a> - 3e circonscription</p>
<p><a href="http://www.assemblee-nationale.fr/14/tribun/fiches_id/610924.asp" class="spip_out" rel="external">Mme SCHMID Claudine</a> - 6e circonscription</p>
<h3 class="spip"><a id="sommaire_2"></a>Sénateurs représentant les Français établis hors de France</h3>
<p><a href="http://www.senat.fr/senateur/cadic_olivier14255u.html" class="spip_out" rel="external">M. CADIC Olivier</a></p>
<p><a href="http://www.senat.fr/senateur/cantegrit_jean_pierre77002u.html" class="spip_out" rel="external">M. CANTEGRIT Jean-Pierre</a></p>
<p><a href="http://www.senat.fr/senateur/conway_mouret_helene11027h.html" class="spip_out" rel="external">Mme CONWAY-MOURET Hélène</a></p>
<p><a href="http://www.senat.fr/senateur/deromedi_jacky14041t.html" class="spip_out" rel="external">Mme DEROMEDI Jacky</a></p>
<p><a href="http://www.senat.fr/senateur/duvernois_louis01041n.html" class="spip_out" rel="external">M. DUVERNOIS Louis</a></p>
<p><a href="http://www.senat.fr/senateur/frassa_christophe_andre08018u.html" class="spip_out" rel="external">M. FRASSA Christophe-André</a></p>
<p><a href="http://www.senat.fr/senateur/garriaud_maylam_joelle04035d.html" class="spip_out" rel="external">Mme GARRIAUD-MAYLAM Joëlle</a></p>
<p><a href="http://www.senat.fr/senateur/kammermann_christiane04036e.html" class="spip_out" rel="external">Mme KAMMERMANN Christiane</a></p>
<p><a href="http://www.senat.fr/senateur/leconte_jean_yves11026g.html" class="spip_out" rel="external">M. LECONTE Jean-Yves</a></p>
<p><a href="http://www.senat.fr/senateur/lepage_claudine08017t.html" class="spip_out" rel="external">Mme LEPAGE Claudine</a></p>
<p><a href="http://www.senat.fr/senateur/del_picchia_robert98018t.html" class="spip_out" rel="external">M. del PICCHIA Robert</a></p>
<p><a href="http://www.senat.fr/senateur/yung_richard04034c.html" class="spip_out" rel="external">M. YUNG Richard</a></p>
<h3 class="spip"><a id="sommaire_3"></a>Conseillers de l’Assemblée des Français de l’étranger</h3><a class="spip_in" title="Doc:Liste des 90 conseillers AFE , 283.3 ko, 0x0" href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/liste_conseillers_afe_maj_04072016_cle81e35c.pdf"><img width="18" height="21" alt="Doc:Liste des 90 conseillers AFE , 283.3 ko, 0x0" src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L18xH21/pdf-7444b.png" style="vertical-align: middle;"></a>
<a href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/liste_conseillers_afe_maj_04072016_cle81e35c.pdf">Liste des 90 conseillers AFE - (PDF, 283.3 ko)</a>
<p>(Dernière actualisation le 4 juillet 2016)</p>
<h3 class="spip"><a id="sommaire_4"></a>Conseillers consulaires</h3><a class="spip_in" title="Doc:Liste des 442 conseillers consulaires , 316.5 ko, 0x0" href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/liste_conseillerscons_elus_maj-14062016_cle8cbc11.pdf"><img width="18" height="21" alt="Doc:Liste des 442 conseillers consulaires , 316.5 ko, 0x0" src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L18xH21/pdf-7444b.png" style="vertical-align: middle;"></a>
<a href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/liste_conseillerscons_elus_maj-14062016_cle8cbc11.pdf">Liste des 442 conseillers consulaires - (PDF, 316.5 ko)</a>
<p>(Dernière actualisation le 14 juin 2016)</p>
<p class="document_doc">
<a class="spip_in" title="Doc:Résultats complets des élections des conseillers consulaires 2014 , 468.2 ko, 0x0" href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/Resultats-Complets_Elections-ConseillersConsulaires_cle491d11.pdf"><img width="18" height="21" alt="Doc:Résultats complets des élections des conseillers consulaires 2014 , 468.2 ko, 0x0" src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L18xH21/pdf-7444b.png" style="vertical-align: middle;"></a>
<a href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/Resultats-Complets_Elections-ConseillersConsulaires_cle491d11.pdf">Résultats complets des élections des conseillers consulaires 2014 - (PDF, 468.2 ko)</a>
</p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/droit-de-vote-et-elections-a-l-etranger/article/les-elus-des-francais-de-l-etranger). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
