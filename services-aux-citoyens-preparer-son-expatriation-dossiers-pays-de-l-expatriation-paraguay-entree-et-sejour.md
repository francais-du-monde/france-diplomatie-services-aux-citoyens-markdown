# Entrée et séjour

<h2 class="rub23465">Passeport, visa, permis de travail</h2>
<p>Quel que soit le motif de votre séjour, renseignez-vous auprès de la section consulaire de l’ambassade du Paraguay à Paris qui vous informera sur la règlementation en matière d’entrée et de séjour au Paraguay, règlementation que vous devrez impérativement respecter. N’hésitez pas à consulter le site ministère de l’Intérieur du Paraguay afin de vous familiariser avec les modalités d’entrée et de séjour dans le pays.</p>
<p><strong>En aucun cas, vous n’êtes autorisé à travailler en au Paraguay sans permis adéquat. </strong></p>
<p>Le consulat de France au Paraguay n’est pas l’organisme compétent pour répondre à vos demandes d’information concernant votre séjour en au Paraguay</p>
<p>Aucun visa n’est requis pour un séjour de moins de 90 jours. Il est possible d’obtenir une prolongation de court séjour de 30 jours, par simple formalité auprès du service d’immigration.</p>
<p>Le montant des droits à acquitter est d’environ 50 euros.</p>
<p>Les démarches en vue d’obtenir un permis de travail sont accomplies par le futur employeur auprès du service de l’immigration. Les délais varient de un à six mois, voire davantage, selon la profession exercée. Une carte de résident temporaire est délivrée pour une période d’un an. Au-delà, le travailleur étranger peut solliciter un titre de résident permanent. Une autorisation de séjour peut être délivrée au conjoint du travailleur étranger titulaire d’un permis de travail.</p>
<p>L’immigration au Paraguay est régie par la loi 978/96 qui tend à favoriser l’entrée des investisseurs étrangers et, plus largement, de toute personne souhaitant s’installer sur le territoire dans le but d’y conduire des activités utiles au développement du pays. Un titre de résidence permanente ou temporaire peut être obtenu sous certaines conditions. Les documents produits à l’appui du dossier de demande doivent faire l’objet d’une traduction officielle en espagnol et d’une procédure de légalisation.</p>
<p>Tout renseignement portant sur les conditions de résidence et l’obtention d’un titre de séjour au Paraguay doit être demandé directement à la section consulaire de l’ambassade de la République du Paraguay à Paris ou, sur place, au service de l’immigration :</p>
<p><i>Ministerio del Interior - Dirección general de Migraciones</i> <br class="manualbr">Calle O’Leary c/ General Diaz Asunción <br class="manualbr">Tél. : (595) 21 446 066 / 492 908 <br class="manualbr">Fax : (595) 21 446 673</p>
<h4 class="spip">Les différents types de visa</h4>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Délivrés par le ministère de l’Intérieur – Direction Générale des Migrations
</p>
<p>Une seul type de visa nommé « visa touriste » est délivré afin de prolonger le séjour pour une période de 90 jours (au-delà des premiers 90 jours à partir de l’entrée sur le territoire paraguayen sans exigence de visa). Ce visa coûte, à ce jour, environ 50 euros. Il s’agit d’une démarche qui ne peut être effectuée qu’une seule fois.</p>
<p>Si la personne souhaite, par la suite, demeurer plus longtemps au Paraguay, elle doit se renseigner auprès de la Direction générale des migrations pour accomplir les formalités liées à une demande de résidence.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Délivrés par le ministère des Relations extérieures – Direction général du protocole :
</p>
<p>Il s’agit de visas délivrés aux personnes dépendant des missions diplomatiques ou officielles au Paraguay et dont la durée est définie selon la durée de mission de l’intéressé.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-etrangers-en-france/article/annuaire-des-representations-etrangeres-en-france" class="spip_in">Ambassade du Paraguay en France</a></p>
<p><i>Mise à jour : juin 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-paraguay-entree-et-sejour-article-animaux-domestiques.md" title="Animaux domestiques">Animaux domestiques</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-paraguay-entree-et-sejour-article-vaccination-113491.md" title="Vaccination">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-paraguay-entree-et-sejour-article-demenagement.md" title="Déménagement">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-paraguay-entree-et-sejour-article-passeport-visa-permis-de-travail.md" title="Passeport, visa, permis de travail">Passeport, visa, permis de travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/paraguay/entree-et-sejour/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
