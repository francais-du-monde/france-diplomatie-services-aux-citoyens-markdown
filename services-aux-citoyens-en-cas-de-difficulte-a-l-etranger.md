# En cas de difficulté à l’étranger

<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-en-cas-de-difficulte-a-l-etranger-article-vol-et-perte-de-documents-a-l.md" title="Vol et perte de documents à l’étranger">Vol et perte de documents à l’étranger</a></li>
<li><a href="services-aux-citoyens-en-cas-de-difficulte-a-l-etranger-article-accident-grave-maladie-deces.md" title="Accidents graves,  décès à l’étranger">Accidents graves,  décès à l’étranger</a></li>
<li><a href="services-aux-citoyens-en-cas-de-difficulte-a-l-etranger-article-agression-attentat.md" title="Agression, violences intrafamiliales, attentats">Agression, violences intrafamiliales, attentats</a></li>
<li><a href="conseils-aux-voyageurs-infos-pratiques-assistance-aux-francais-disparitions-inquietantes.md" title="Disparitions inquiétantes">Disparitions inquiétantes</a></li>
<li><a href="conseils-aux-voyageurs-infos-pratiques-assistance-aux-francais-incarceration-20987.md" title="Arrestation, incarcération à l’étranger">Arrestation, incarcération à l’étranger</a></li>
<li><a href="services-aux-citoyens-en-cas-de-difficulte-a-l-etranger-article-important-rappel-sur-l-assurance-rapatriement-a-l-attention-des-voyageurs.md" title="Important : rappel sur l’assurance-rapatriement à l’attention des voyageurs">Important : rappel sur l’assurance-rapatriement à l’attention des voyageurs</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/en-cas-de-difficulte-a-l-etranger/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
