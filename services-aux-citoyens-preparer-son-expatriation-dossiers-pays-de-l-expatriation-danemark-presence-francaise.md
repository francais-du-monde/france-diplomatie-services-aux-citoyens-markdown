# Présence française

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/presence-francaise/#sommaire_1">Ambassade et consulat de France</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/presence-francaise/#sommaire_2">Autorités françaises dans le pays</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/presence-francaise/#sommaire_3">Associations françaises</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Ambassade et consulat de France</h3>
<p>Consulter notre <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/article/annuaire-des-representations-francaises-a-l-etranger" class="spip_in">annuaire des ambassades et consulats de France à l’étranger</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Autorités françaises dans le pays</h3>
<h4 class="spip">Réseau français de coopération et d’action culturelle</h4>
<p>Les coordonnées des services du réseau de coopération et d’action culturelle français au Danemark ainsi que la description de l’activité de ces services se trouvent dans la rubrique culture du site internet de l’Ambassade ou sur le site internet du réseau culturel : <a href="http://latitudefrance.diplomatie.gouv.fr/AnnuaireCarto.html" class="spip_out" rel="external">Latitude France</a>.</p>
<h4 class="spip">Economie : réseau mondial</h4>
<p>Business France, l’Agence française pour le développement international des entreprises, est présente au Danemark. Sa mission est d’accompagner les entreprises françaises dans leur développement à l’international.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://export.businessfrance.fr/danemark/export-danemark-avec-notre-bureau.html" class="spip_out" rel="external">Site de Business France au Danemark</a></p>
<p>Les services économiques sont également présents. Ils sont une émanation de la Direction générale du Trésor et ont pour missions principales l’analyse macroéconomique et l’animation des relations économiques bilatérales entre la France et les pays où ils sont implantés.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="https://www.tresor.economie.gouv.fr/Pays/danemark" class="spip_out" rel="external">Site internet du service économique français au Danemark</a> </p>
<h4 class="spip">Vos élus à l’Assemblée des Français de l’étranger (AFE)</h4>
<p>L’AFE est l’assemblée représentative des Français établis hors de France. Elle est composée de 153 conseillers, élus au suffrage universel direct par les Français de l’étranger. Le ministre des Affaires étrangères préside cette assemblée, et les douze Sénateurs, représentant les Français établis hors de France, en sont membres de droit.12 personnalités, qualifiées en raison de leurs compétences, sont nommées par le ministre des Affaires étrangères.</p>
<p>L’AFE a pour mission de donner au gouvernement des avis sur les questions et projets intéressant les Français établis hors de France et le développement de la présence française à l’étranger.</p>
<p>Les deux élues depuis le 18 juin 2006 pour la circonscription Stockholm (Danemark, Estonie, Finlande, Islande, Lettonie, Lituanie, Norvège, Suède) sont :</p>
<p><a href="http://www.caron-afe.blogspot.com/" class="spip_out" rel="external">Mme Marie José CARON</a><br class="manualbr">Frederiksberg Allé 5 <br class="manualbr">1601 Copenhague V<br class="manualbr">Tél. : 36 75 90 02<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/presence-francaise/#mariejosecaron#mc#hotmail.com#" title="mariejosecaron..åt..hotmail.com" onclick="location.href=mc_lancerlien('mariejosecaron','hotmail.com'); return false;" class="spip_mail">Courriel</a></p>
<p><strong>Mme Nadine PRIPP</strong><br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/presence-francaise/#nadine_pripp#mc#hotmail.com#" title="nadine_pripp..åt..hotmail.com" onclick="location.href=mc_lancerlien('nadine_pripp','hotmail.com'); return false;" class="spip_mail">Courriel</a></p>
<p>Pour en savoir plus sur le site Internet :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.assemblee-afe.fr/" class="spip_out" rel="external">Assemblee-afe.fr</a></p>
<h4 class="spip">Les députés des Français de l’étranger</h4>
<p>La révision constitutionnelle du 23 juillet 2008, publiée au Journal officiel du 19 avril 2011, introduit la représentation à l’Assemblée nationale des Français établis hors de France. La création de onze circonscriptions législatives à l’étranger a permis aux Français expatriés d’élire pour la première fois leurs députés à l’Assemblée nationale en juin 2012.</p>
<p>La députée pour la 3ème circonscription (Danemark, Suède Norvège, Finlande, Royaume-Uni, Irlande, Estonie, Lettonie et Lituanie) est Mme <a href="http://www.axellelemaire.eu/" class="spip_out" rel="external">Axelle LEMAIRE</a>.</p>
<p>Consultez son profil et son action sur le site de l´<a href="http://www.assemblee-nationale.fr/" class="spip_out" rel="external">Assemblée nationale</a></p>
<h4 class="spip">Les sénateurs représentant les Français établis hors de France</h4>
<p>Pour toute information sur les Sénateurs qui représentent les Français établis hors de France dans votre circonscription, vous pouvez consulter la rubrique « Espace expatriés » sur le site du Sénat dans l´onglet « Europe et International » :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Sénat au service des Français de l’étranger : <a href="http://www.senat.fr/expatries/dossiers_pays/danemark.html" class="spip_out" rel="external">dossier spécifique au Danemark sur le site du Sénat</a>.</p>
<p>Pour plus d’information sur le vote des Français à l´étranger :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.votezaletranger.gouv.fr/" class="spip_out" rel="external">Votez à l’étranger</a> </p>
<h3 class="spip"><a id="sommaire_3"></a>Associations françaises</h3>
<ul class="spip">
<li><a href="http://www.ufe.org/" class="spip_out" rel="external">Union des Français de l’Etranger UFE</a></li>
<li><a href="http://www.francais-du-monde.org" class="spip_out" rel="external">Français du Monde-ADFE</a></li>
<li><a href="http://www.french.meetup.com" class="spip_out" rel="external">Association des francophones de Copenhague</a></li>
<li><a href="http://www.copenhagueaccueil.org/" class="spip_out" rel="external">Copenhague Accueil (Association des Français à Copenhague)</a></li>
<li><a href="http://www.alliancefrancaise.dk" class="spip_out" rel="external">Alliance française de Copenhague</a></li>
<li><a href="http://www.cnisf.dk/" class="spip_out" rel="external">C.N.I.S.F. (Conseil national des ingénieurs et scientifiques de France)</a></li>
<li><a href="http://www.francofionie.dk/" class="spip_out" rel="external">FrancoFionie.dk (Association de parents francophones à Odense)</a></li></ul>
<p>Pour plus d’information :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.ambafrance-dk.org/Associations-francaises" class="spip_out" rel="external">Liste des associations</a> tenue par l’Ambassade de France au Danemark.</p>
<p><i>Mise à jour : septembre 2015</i></p>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/presence-francaise/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
