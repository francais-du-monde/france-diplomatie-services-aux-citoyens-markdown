# Santé

<p>Les techniques et équipements sont d’excellente qualité en Finlande. Le personnel soignant parle généralement l’anglais.</p>
<p>Le coût d’une consultation d’un généraliste à son cabinet varie de 50 à 100€. Les praticiens ne se déplacent pas à domicile. Les soins sont moins coûteux dans les structures hospitalières publiques mais les délais d’attente y sont aussi plus longs.</p>
<p>Le centre et le nord du pays sont <strong>infestés de moustiques en été</strong> (juin, juillet, août). Il faut donc se protéger contre les piqûres par des vêtements à manches longues, des moustiquaires ainsi que des produits répulsifs.</p>
<p>Les randonneurs sont aussi invités à se protéger contre les <strong>tiques</strong>, qui sont de plus en plus nombreuses en Finlande (surtout au sud de la ligne Oulu-Joensuu) et peuvent transmettre des maladies infectieuses (encéphalite virale et maladie de Lyme). En cas de séjour prolongé en zone rurale ou forestière du printemps à l’automne, la vaccination préventive de l’encéphalite à tiques peut être envisagée.</p>
<p><strong>Pour un court séjour, se munir de la <a href="http://www.ameli.fr/assures/droits-et-demarches/a-l-etranger/vous-partez-en-vacances-a-l-etranger/vous-partez-en-vacances-en-europe.php" class="spip_out" rel="external">carte européenne d’assurance maladie</a> à demander à votre caisse d’assurance maladie au moins 15 jours avant le départ.</strong></p>
<p><strong>Numéros utiles :</strong></p>
<ul class="spip">
<li>Urgences et ambulances : <strong>112</strong> (appel gratuit)</li>
<li>Service de médecins : <strong>100 23</strong> (24h/24) <strong>- accessible seulement à ceux qui ont un numéro d’identification finlandais.</strong></li>
<li>Centre hospitalier universitaire d’Helsinki : <strong>4711 </strong> (24h/24)</li>
<li>Services des urgencesà Helsinki : l’hôpital de Maria <strong>(09) 310 67375</strong>, l’hôpital de Haartman <strong>(09) 310 63231</strong> ou <strong>(09) 310 5018</strong> (quartier de Töölö)</li></ul>
<p><strong>Centres médicaux privés</strong> (avec quelques médecins ou dentistes parlant français. Vérifier leur présence au standard) :</p>
<ul class="spip">
<li>Lääkärikeskus Aava, prise de rendez-vous 010380 3838 (Annankatu 32, centre ville)</li>
<li>Dextra : 0306060 (Raumantie 1, quartier de Munkkivuori)</li></ul>
<p><strong>Médecins</strong></p>
<p>Il est conseillé de prendre l’attache du Consulat de France à Helsinki pour obtenir une liste actualisée des médecins habituellement consultés par les Français.</p>
<p><strong>Hôpitaux</strong></p>
<p>Il y a des hôpitaux universitaires et régionaux sur tout le territoire ainsi que des cliniques privées. Toutefois, la densité de la population étant nettement plus faible au nord qu’au sud, les distances à parcourir pour rejoindre un centre médical dans le nord de la Finlande peuvent être très importantes.</p>
<p>La qualité des soins est excellente en ce qui concerne les techniques et le matériel. Un ressortissant français pourra cependant s’étonner de la faible utilisation de médicaments en général et d’antibiotiques en particulier.</p>
<p>En cas d’urgence, pharmaciens et réceptionnistes d’hôtel communiquent le numéro de l’hôpital le plus proche.</p>
<p><strong>Médicaments</strong></p>
<p>Toutes les grandes villes disposent d’une <strong>pharmacie ouverte la nuit</strong>. La pharmacie universitaire de Helsinki (Tél : 030020200) est ouverte tous les jours de 7h à 24h.</p>
<p>Il est vivement conseillé de consulter son médecin traitant avant le départ.</p>
<p><strong>Pharmacies</strong></p>
<ul class="spip">
<li>Pharmacie de l’Université, Mannerheimintie 5 (ouvert de 7h à minuit)</li>
<li>Pharmacie de l’Université, Mannerheimintie 96 (ouvert 24h/24h) – tel 0300 20 200</li></ul>
<p>Pour une présentation <strong>actualisée</strong> des risques épidémiologiques ainsi que des conseils sur la médecine au quotidien, consultez <a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/finlande/" class="spip_in">Finlande</a> (Conseils aux voyageurs).</p>
<p><i>Mise à jour : janvier 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/vie-pratique/article/sante). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
