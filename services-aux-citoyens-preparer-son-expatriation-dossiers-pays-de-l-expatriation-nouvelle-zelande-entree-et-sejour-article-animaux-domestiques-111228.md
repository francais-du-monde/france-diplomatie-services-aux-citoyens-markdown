# Animaux domestiques

<p>Si vous souhaitez emmener votre animal en Nouvelle Zélande, il est nécessaire d’anticiper les démarches au minimum six mois avant le départ (tests antirabiques, vaccins, etc). Il convient d’obtenir un permis sanitaire d’importation auprès du service de quarantaine du ministère de l’Agriculture et des Forêts (MAF), Import Management, P.O. Box 2526, Wellington. La durée de la période de quarantaine est spécifiée dans le règlement sur la santé en ce qui concerne les importations (Import Health Standards).</p>
<p>Les demandes de permis sanitaire d’importation peuvent être obtenues auprès du service des douanes à l’<a href="http://www.nzembassy.com/belgium" class="spip_out" rel="external">ambassade de Nouvelle-Zélande à Bruxelles</a> :</p>
<p><strong>Customs Service</strong><br class="manualbr">New Zealand Embassy square de Meeus, 1 level 7<br class="manualbr">1000 Brussels<br class="manualbr">BELGIQUE<br class="manualbr">Tél. : +32 2 550 1218<br class="manualbr">Fax : +32 2 513 4856</p>
<p><strong>Pour plus d’informations, contacter :</strong><br class="manualbr">Import Management Section<br class="manualbr">Animal Biosecurity Group<br class="manualbr">PO Box 2526<br class="manualbr">Wellington<br class="manualbr">Tél. : +64 4 498 9625<br class="manualbr">Fax : +64 4 474 4132</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.maf.govt.nz/" class="spip_out" rel="external">Ministère de l’Agriculture et de la Forêt</a></li>
<li><a href="http://www.biosecurity.govt.nz/enter/personal/pets/cats-and-dogs" class="spip_out" rel="external">Biosecurity.govt.nz</a></li></ul>
<p><strong>Pour connaître les conditions exactes d’importation de votre animal, vous devrez prendre contact :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  avec l’ambassade en France du pays de destination. Si, au cours de son transport de la France vers le pays de destination, l’animal doit transiter par un ou plusieurs pays, vous devrez également vous conformer à la réglementation du ou des pays de transit de l’animal. A noter que certains pays refusent le transit d’animaux.
</p>
<p>Des informations générales sur la réglementation de nombreux pays sont également disponibles sur le site Internet de l’<a href="http://www.iata.org/Pages/default.aspx" class="spip_out" rel="external">Association internationale du transport aérien (AITA)</a>.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  le cas échéant, avec la ou les compagnies aériennes pour connaître les conditions de transport de l’animal (en soute ou en cabine, normes des cages, nourriture, etc.).
</p>
<p>Pour connaître les normes internationales de transport des animaux et des cages, vous pouvez consulter le site Internet de l’<a href="http://www.iata.org/Pages/default.aspx" class="spip_out" rel="external">Association internationale du transport aérien (AITA)</a>.</p>
<p>Si l’ambassade dispose d’une information particulière, celle-ci doit être communiquée à votre vétérinaire traitant, titulaire d’un mandat sanitaire, qui vous guidera pour la suite.</p>
<p>Dans le cas où l’ambassade ne disposerait pas d’informations sur la réglementation sanitaire de son pays, vous devrez respecter, par défaut, les conditions suivantes :</p>
<ul class="spip">
<li>l’animal doit être en règle vis-à-vis des exigences réglementaires françaises en matière de santé et de protection animales. Pour plus de renseignements sur la réglementation française, vous pouvez prendre contact avec votre vétérinaire traitant, titulaire d’un mandat sanitaire, ou la Direction Départementale des Services Vétérinaires (DDSV) de votre département. Vous trouverez les coordonnées des DDSV sur le <a href="http://agriculture.gouv.fr/" class="spip_out" rel="external">site Internet du ministère de l’Agriculture</a>.</li>
<li>les documents suivants sont obligatoires pour l’animal et doivent être établis par le vétérinaire traitant, titulaire d’un mandat sanitaire :
<br>— identification par micropuce ou tatouage ;
<br>— certificat de vaccination contre la rage en cours de validité ;
<br>— certificat international de bonne santé, établi par le vétérinaire traitant, titulaire du mandat sanitaire, dans la semaine précédant le départ de France. Il est également conseillé de faire procéder à un titrage des anticorps anti-rabiques dans un laboratoire agréé et de se munir du carnet de vaccination tenu à jour de l’animal.</li></ul>
<p>Vous devrez ensuite prendre rendez-vous avec le service " santé et protection animales " de la Direction départementale des services vétérinaires (DDSV) dont relève le vétérinaire traitant, pour la validation des documents établis par celui-ci.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/entree-et-sejour/article/animaux-domestiques-111228). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
