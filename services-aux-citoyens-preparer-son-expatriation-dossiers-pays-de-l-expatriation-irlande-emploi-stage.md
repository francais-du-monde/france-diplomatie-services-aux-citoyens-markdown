# Emploi, stage

<h2 class="rub22891">Marché du travail</h2>
<p>Le marché de l’emploi est par nature assez fluctuant puisqu’il subit l’effet de la conjoncture économique. Dans le cas de l’Irlande, l’important essor qu’a connu le pays pendant les années du "Tigre Celtique" a laissé place, à partir de 2002, a une rupture du rythme de croissance.</p>
<p>Néanmoins, le marché du travail irlandais, stimulé par une économie prospère, a maintenu pendant l’année 2006 la dynamique initiée en 2005. Une forte poussée à la création d’emplois a été soutenue par la croissance rapide des secteurs non commerciaux (notamment le bâtiment et les services non marchands). Par ailleurs, afin d’assurer sa compétitivité sur le plan international, l’Irlande doit relever le défi d’une main-d’œuvre concurrentielle et à haut niveau de qualification.</p>
<p>Enfin, l’expérience a montré ces dernières années que le marché de l’emploi est dépendant également de l’équilibre géopolitique et des effets que certaines crises peuvent produire sur des secteurs plus exposés comme les finances ou le tourisme.</p>
<p>Cependant, le marché de l’emploi a été fortement perturbé suite à la crise qui a frappé sévèrement l’économie irlandaise en 2008 : en effet, la bulle immobilière spéculative et la progression excessive du crédit, due aux critères de prêt laxistes par les banques, et la forte augmentation de la dette publique se sont conjugués menant l’économie irlandaise au bord du précipice.</p>
<p>Ainsi les ménages ont été confrontés à des pertes d’emplois, une baisse des salaires, une hausse des impôts et une diminution des prix de l’immobilier (sur lequel étaient bien souvent basés les prêts). Par conséquent la crise a provoqué une forte hausse de chômage : au plus fort de l’essor économique le chômage était de 3,7%, alors qu’au pic de la crise (avril 2011) celui-ci a atteint le sommet de 14,7%. En janvier 2012, 14,2% de la population active en Irlande est au chômage.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.entemp.ie/" class="spip_out" rel="external">Site du ministère irlandais du travail, de l’entreprise et de l’innovation (<i>Department of Jobs, Enterprise and Innovation</i>)</a>.</p>
<h4 class="spip">Secteurs à fort potentiel</h4>
<ul class="spip">
<li>Informatique et nouvelles technologies de l’information</li>
<li>Télécommunications</li>
<li>Produits médicaux</li>
<li>Biotechnologie</li>
<li>Agroalimentaire</li>
<li>Matériaux de construction</li>
<li>Hôtellerie, métiers du vin et de la table</li>
<li>Bâtiment et travaux publics</li></ul>
<h4 class="spip">Rémunération</h4>
<h4 class="spip">Salaire minimum légal</h4>
<p>Au 1er janvier 2012, le salaire minimum légal s’élevait à 8,65 euros par heure, pour une moyenne de 39h par semaine.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-irlande-emploi-stage-article-stages.md" title="Stages">Stages</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-irlande-emploi-stage-article-entretien-d-embauche.md" title="Entretien d’embauche">Entretien d’embauche</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-irlande-emploi-stage-article-lettre-de-motivation.md" title="Lettre de motivation">Lettre de motivation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-irlande-emploi-stage-article-curriculum-vitae-110430.md" title="Curriculum vitae">Curriculum vitae</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-irlande-emploi-stage-article-recherche-d-emploi.md" title="Recherche d’emploi">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-irlande-emploi-stage-article-reglementation-du-travail.md" title="Réglementation du travail">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-irlande-emploi-stage-article-marche-du-travail.md" title="Marché du travail">Marché du travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/emploi-stage/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
