# Risques climatiques (cyclones / ouragans)

<p>Si vous demeurez dans une zone à risque, il est recommandé de prendre les précautions suivantes :</p>
<p><strong>Ayez en permanence à votre domicile</strong> :</p>
<ul class="spip">
<li>un éclairage de secours ;</li>
<li>une réserve de seaux et serpillières ;</li>
<li>une trousse à outils, un transistor à piles et plusieurs lampes-torches ainsi que les piles permettant de les faire fonctionner (évitez les bougies) ;</li>
<li>une réserve d’eau potable et de nourriture pour une période de sept jours, ainsi que du papier toilette et une trousse premiers secours (sans oublier un stock de votre traitement habituel) ;</li>
<li>des pastilles d’hydrochlonazone permettant de désinfecter l’eau à consommer ;</li></ul>
<p>Vérifiez la solidité de votre maison (gouttières, fenêtres, tôles… ne doivent pas pouvoir être arrachées par le vent).<br class="manualbr">Élaguez les arbres autour de chez vous. <br></p>
<h5 class="spip">Dès l’annonce d’un cyclone ou d’un ouragan :</h5>
<ul class="spip">
<li>maintenez vos enfants à l’intérieur du logement ;</li>
<li>écoutez les informations diffusées à la radio ;</li>
<li>respectez les consignes des autorités ;</li>
<li>mettez à l’abri tous les objets susceptibles d’être emportés par le vent ;</li>
<li>consolidez les portes et fenêtres de votre maison.
<br></li></ul>
<h5 class="spip">Première phase d’alerte :</h5>
<ul class="spip">
<li>si vous avez des volets, fermez-les et attachez-les ;</li>
<li>clouez des planches de bois sur vos portes et volets ; placez du contre-plaqué sur les baies vitrées. A défaut, fixez des bandes de papier collant en étoile, à l’intérieur et à l’extérieur. Si la baie vitrée est grande, démontez-la et videz la pièce ;</li>
<li>mettez des objets lourds sur le plancher de votre maison si elle n’est pas bien arrimée au sol ;</li>
<li>restez à l’écoute de la radio.
<br></li></ul>
<h5 class="spip">Deuxième phase d’alerte :</h5>
<ul class="spip">
<li>enfermez-vous dans la pièce la mieux abritée, de préférence sans fenêtre. Si votre maison n’est pas assez solide ou si elle est située au bord de la mer, réfugiez-vous dans l’un des abris indiqués par les secours ;</li>
<li>éteignez les flammes nues ;</li>
<li>restez à l’écoute de la radio.
<br></li></ul>
<h5 class="spip">Lorsque le danger est imminent :</h5>
<p>Éloignez-vous des vitres.<br class="manualbr">Ne tentez pas de sortir pour consolider les protections des fenêtres ou des toitures.<br class="manualbr">Débranchez les appareils électriques.<br class="manualbr">Si une ouverture cède dans la pièce ou vous êtes, entrouvrez une porte ou une fenêtre sur une façade opposée.</p>
<p><strong>Ne quittez pas votre abri avant la fin de l’alerte diffusée par la radio.</strong> <br></p>
<h5 class="spip">Après le cyclone ou l’ouragan</h5>
<p>Ne touchez pas aux câbles tombés à terre.</p>
<p>Le réseau téléphonique peut être très perturbé. Ne l’encombrez pas. L’ambassade pourrait tenter de vous joindre. Si vous avez de la famille en France, pensez à contacter un de vos proches et demandez-lui de faire passer le message aux autres.</p>
<p>Dans tous les cas, pensez à vous informer, auprès de l’ambassade ou du consulat le plus proche de votre résidence, des mesures prévues pour la sécurité de la communauté française.</p>
<p><strong>Pour en savoir plus :</strong><br class="manualbr"><a href="http://www.wmo.int/pages/index_fr.html" class="spip_out" rel="external">Organisation Métérologique mondiale</a> <br class="manualbr"><a href="http://comprendre.meteofrance.com/jsp/site/Portal.jsp?&amp;page_id=13752" class="spip_out" rel="external">Météofrance</a> <br class="manualbr"><a href="http://www.nhc.noaa.gov" class="spip_out" rel="external">National Hurricane Center</a> <br></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/infos-pratiques/risques/risques-naturels/article/risques-climatiques-cyclones). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
