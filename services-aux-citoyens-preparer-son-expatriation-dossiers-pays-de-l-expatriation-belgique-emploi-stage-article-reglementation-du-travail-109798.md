# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/emploi-stage/article/reglementation-du-travail-109798#sommaire_1">Droit du travail </a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/emploi-stage/article/reglementation-du-travail-109798#sommaire_2">Fêtes légales</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/emploi-stage/article/reglementation-du-travail-109798#sommaire_3">Emploi du conjoint </a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail </h3>
<p>En Belgique, les salaires sont fonction des régions. Les cotisations sociales représentent 13,07% du salaire brut qui est ensuite imposable ("précompte professionnel" mensuel).</p>
<p>Le droit aux congés payés est calculé au prorata des prestations du salarié. Le maximum est de 24 jours (en régime de 6 jours/semaine) et de 20 jours (régime de 5 jours/semaine).</p>
<p>La durée du travail ne peut excéder ni huit heures par jour ni 38 heures par semaine (en moyenne, sur base annuelle).</p>
<p>Pour toute information, s’adresser à :</p>
<p><a href="http://www.emploi.belgique.be/home.aspx" class="spip_out" rel="external">Service public fédéral emploi, travail et concertation sociale - Inspection des lois sociales</a><br class="manualbr">rue Ernest Blerot 1 à 1070 Bruxelles.<br class="manualbr">Tél. : 02 233 41 11 <br class="manualbr">Fax : 02 233 44 88</p>
<p>Le <a href="http://www.emploi.belgique.be/home.aspx" class="spip_out" rel="external">Service public fédéral emploi, travail et concertation sociale</a> met gratuitement à votre disposition une série de publications très utiles sur son site.</p>
<p>Pour en savoir plus :</p>
<ul class="spip">
<li>Vous trouverez de nombreux renseignements sur l’emploi (type de contrats - documents sociaux - durée du travail - licenciement, etc.) sur le portail <a href="http://www.belgium.be/fr/emploi/index.jsp" class="spip_out" rel="external">Belgium.be</a></li>
<li><a href="http://www.bruxelles.irisnet.be/travailler-et-entreprendre/la-legislation-du-travail" class="spip_out" rel="external">Portail de la région Bruxelles-Capitale</a></li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Fêtes légales</h3>
<ul class="spip">
<li>1er janvier</li>
<li>lundi de Pâques</li>
<li>1er mai</li>
<li>Ascension</li>
<li>lundi de Pentecôte</li>
<li>21 juillet : fête nationale belge</li>
<li>15 août : Assomption</li>
<li>1er novembre : Toussaint</li>
<li><i>2 novembre : Jour des morts (fermeture des administrations) </i></li>
<li>11 novembre</li>
<li><i>15 novembre : fête du roi (certains Ministères et écoles sont fermés) </i></li>
<li>25 décembre : Noël</li></ul>
<p>En outre, pour les fêtes des communautés (française, flamande, germanophone), il peut être institué un jour chômé.</p>
<p>Voir aussi le site <a href="http://www.tv5.org/TV5Site/voyageurs/accueil.php" class="spip_out" rel="external">TV5.org</a>.</p>
<h3 class="spip"><a id="sommaire_3"></a>Emploi du conjoint </h3>
<p>Le droit du travail n’oppose aucune restriction aux ressortissants français. Il existe cependant des difficultés concrètes pour trouver un emploi, compte tenu de l’étroitesse du marché et également de la nécessité fréquente de connaître le néerlandais.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/emploi-stage/article/reglementation-du-travail-109798). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
