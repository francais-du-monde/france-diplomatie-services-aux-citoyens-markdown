# Vaccination

<p>Aucune vaccination n’est exigée à l’entrée dans le pays sauf contre la fièvre jaune pour les personnes en provenance d’une zone infectée. Il est néanmoins recommandé, pour des raisons médicales, d’effectuer les vaccinations suivantes :</p>
<p><strong>Chez l’adulte</strong>, mise à jour des vaccinations contre la diphtérie, le tétanos et la poliomyélite ; vaccinations contre l’hépatite A à partir de 50 ans ; vaccination contre la typhoïde, l’hépatite B et la rage en cas de séjour long et/ou à risque.</p>
<p><strong>Chez l’enfant</strong>, vaccinations recommandées en France par le ministère de la Santé et en particulier : BCG et hépatite B dès la naissance, rougeole dès l’âge de neuf mois.</p>
<p>Tous les vaccins (d’origine française, italienne, hollandaise) peuvent se trouver sur place.</p>
<p>Pour en savoir plus, lisez <a href="services-aux-citoyens-preparer-son-expatriation-sante.md" class="spip_in">notre article thématique sur la vaccination</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/entree-et-sejour/article/vaccination-110514). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
