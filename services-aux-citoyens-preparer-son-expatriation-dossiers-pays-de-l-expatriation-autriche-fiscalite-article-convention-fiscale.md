# Convention fiscale

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/fiscalite/article/convention-fiscale#sommaire_1">Champ d’application de la convention</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/fiscalite/article/convention-fiscale#sommaire_2">Dispositions conventionnelles sur certaines catégories de revenus</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/fiscalite/article/convention-fiscale#sommaire_3">Elimination de la double imposition</a></li></ul>
<p>La France et l’Autriche ont signé à Vienne <strong>le 26 mars 1993</strong> une convention en vue d’éviter les doubles impositions et de prévenir l’évasion fiscale en matière d’impôts sur le revenu et sur la fortune. Cette convention est <strong>entrée en vigueur le 6 décembre 1994. </strong></p>
<p>Voici un résumé de ses points clés pour les particuliers :</p>
<h3 class="spip"><a id="sommaire_1"></a>Champ d’application de la convention</h3>
<p>Cet accord a pour objet de protéger <strong>les résidents de chacun des Etats contractants</strong> en matière d’impôts prélevés directement sur le revenu ou sur le bénéfice des sociétés.</p>
<p>La convention trouve donc à s’appliquer aux résidents de ces Etats.</p>
<p>L’article 4 paragraphe 1 de la convention s’applique aux personnes qui sont considérées comme "résidents d’un Etat contractant" ou de chacun de ces deux Etats.</p>
<p>D’après ce même article de la convention, une personne est considérée comme "résident d’un Etat contractant" lorsque, en vertu de la législation dudit Etat, elle se trouve assujettie à l’impôt à raison de son domicile, de sa résidence ou de critères analogues.</p>
<p>Au paragraphe 2, l’article 4 fournit des critères subsidiaires permettant de résoudre le cas de double résidence si l’assujettissement à l’impôt ne pouvait suffire.</p>
<p>Ces critères sont :</p>
<ul class="spip">
<li>un foyer d’habitation permanent (il s’agit, par exemple, du lieu de situation du conjoint ou des enfants) ;</li>
<li>l’Etat où la personne possède le centre de ses intérêts vitaux (tant professionnels que privés) ;</li>
<li>l’Etat dans lequel elle séjourne de façon habituelle (notion de 183 jours de présence physique sur le territoire au cours d’une année fiscale) ;</li>
<li>à défaut, l’Etat dont elle possède la nationalité.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Dispositions conventionnelles sur certaines catégories de revenus</h3>
<h4 class="spip">Traitements, salaires, pensions et rentes</h4>
<h5 class="spip">Rémunérations privées</h5>
<p>L’article 15, paragraphe 1 précise que les traitements et salaires d’origine privée ne sont, en règle générale, imposables que dans l’Etat où <strong>s’exerce l’activité personnelle</strong>.</p>
<p>Le maintien de l’imposition dans l’Etat de résidence du bénéficiaire est prévu par le paragraphe 2 du même article sous réserve de trois conditions simultanément remplies :- le séjour temporaire du bénéficiaire dans l’autre Etat ne dépasse pas une durée totale de 183 jours ;</p>
<ul class="spip">
<li>la rémunération est payée par un employeur qui n’est pas résident de l’Etat d’exercice ;</li>
<li>la rémunération ne doit pas être à la charge d’un établissement stable ou d’une base fixe de l’employeur dans l’Etat.</li></ul>
<p>Il résulte des dispositions du paragraphe 3 de l’article 15 de la convention que les revenus professionnels des salariés autres qu’intérimaires employés à bord d’un navire, d’un aéronef en trafic international ou à bord d’un bateau servant à la navigation intérieure ne sont imposables que dans l’Etat où se trouve la direction effective de l’entreprise.</p>
<h5 class="spip">Rémunérations publiques</h5>
<p>L’article 19, paragraphe 1-a, que les traitements, salaires et rémunérations analogues ainsi que les pensions de retraite payés par un Etat ou une personne morale de droit public de cet Etat restent imposables dans cet Etat.Toutefois, en vertu de l’article 19, paragraphe 1-b, cette règle ne s’applique pas lorsque le bénéficiaire possède la nationalité de l’autre Etat sans être en même temps ressortissant de l’Etat payeur ; l’imposition est réservée à l’Etat dont l’intéressé est le résident.</p>
<p>D’autre part, en vertu des dispositions du paragraphe 2 du même article, les règles fixées au paragraphe 1 dudit article ne sont pas applicables aux rémunérations ou aux pensions versées au titre de services rendus dans le cadre d’une activité industrielle ou commerciale exercée par un Etat ou une personne morale de droit public.</p>
<p>Les sommes versées à ce titre sont imposées soit dans l’Etat d’exercice de l’activité (article 15 de la convention), soit dans l’Etat de résidence du bénéficiaire (article 18 de la convention).</p>
<h5 class="spip">Pensions et rentes</h5>
<p>L’article 18 prévoit que les pensions de retraite de source privée ainsi que les rentes viagères restent imposables dans l’Etat dont le bénéficiaire est un résident.</p>
<p>Cette disposition couvre également les pensions publiques auxquelles les règles de l’article 19 ne sont pas applicables.</p>
<p>En outre, la convention ne comportant aucune disposition particulière concernant les prestations de sécurité sociale, il en résulte que les pensions privées versées par la Sécurité sociale française à des résidents d’Autriche échappent à l’impôt français et sont par conséquent imposées en Autriche.</p>
<h5 class="spip">Etudiants, stagiaires, enseignants</h5>
<p><strong>Etudiants, apprentis</strong></p>
<p>L’article 20, paragraphe 1-a, de la convention prévoit que les étudiants, les stagiaires d’un Etat qui séjournent dans l’autre Etat à seule fin d’y poursuivre leurs études ou leur formation et qui perçoivent des subsides d’origine étrangère à cet Etat sont exonérés d’impôt par ce dernier Etat.</p>
<p>Toutefois, l’alinéa b du même article dispose que la rémunération perçue par des étudiants, résidents d’un Etat, et employés dans une entreprise de l’autre Etat afin de recevoir une formation pratique pour une durée ne dépassant pas six mois pendant une année civile, n’est pas imposable dans ce dernier Etat.</p>
<p><strong>Enseignants</strong></p>
<p>L’article 20, paragraphe 2, précise que les rémunérations versées aux enseignants résidents d’un Etat se rendant temporairement dans l’autre Etat en vue d’y exercer une activité pédagogique pendant une période ne dépassant pas deux ans, dans une université, un collège, une école ou autre établissement restent imposables dans l’Etat de résidence.</p>
<h4 class="spip">Autres catégories de revenus</h4>
<h5 class="spip">Bénéfices industriels et commerciaux</h5>
<p>L’article 7, paragraphe 1, dispose que les entreprises industrielles et commerciales sont imposables sur le territoire où se trouve un établissement stable.</p>
<h5 class="spip">Bénéfices des professions non commerciales et des revenus non commerciaux</h5>
<p>L’article 14, paragraphe 1, stipule que les revenus provenant de l’exercice d’une profession libérale ou d’autres activités indépendantes sont imposables dans l’Etat sur le territoire duquel se trouve l’installation permanente ou base fixe où s’exerce de façon régulière l’activité personnelle.</p>
<p>Les revenus que les professionnels du spectacle ainsi que les sportifs réalisent en cette qualité dans l’un des deux Etats, à titre indépendant, restent imposables dans l’Etat d’exercice de l’activité selon les dispositions de l’article 17, paragraphe 1, de la convention.</p>
<p>L’article 12, paragraphe 1, pose en principe que les revenus non commerciaux (redevances et droits d’auteur) sont imposables dans l’Etat de résidence du bénéficiaire, sous couvert de la production de formulaires spécifiques remis auprès des autorités fiscales de l’Etat dont relève le créancier des revenus.</p>
<h5 class="spip">Revenus immobiliers</h5>
<p>L’article 6, paragraphe 1, dispose que les revenus des biens immobiliers y compris les bénéfices des exploitations agricoles sont imposables dans l’Etat où ils sont situés.</p>
<p>Cette règle s’applique également aux gains provenant de la cession ou de l’échange desdits biens ou droits selon les dispositions de l’article 13, paragraphe 1.</p>
<p>En ce qui concerne les gains provenant de l’aliénation de tous biens autres que mobiliers ou immobiliers, le paragraphe 6 de l’article 13 précise qu’ils restent imposables dans l’Etat de résidence du cédant.</p>
<h5 class="spip">Revenus de capitaux mobiliers</h5>
<p><strong>Les dividendes</strong></p>
<p>Ce terme désigne les revenus provenant d’actions, actions ou bons de jouissance, parts de mines, parts de fondateur ou autres parts bénéficiaires, à l’exception des créances et les revenus d’autres parts sociales assimilés aux revenus d’actions.</p>
<p>De manière générale, l’article 10 reprend d’une part la règle suivant laquelle les dividendes payés par une société qui est un résident d’un Etat contractant à un résident de l’autre Etat sont imposables dans cet autre Etat.</p>
<p>D’autre part, il maintient le droit au profit de l’Etat dont la société qui paie les dividendes est un résident, d’imposer ces dividendes à la source. L’Etat de la source peut imposer les dividendes à un taux qui n’excède pas 15%. Lorsque le bénéficiaire effectif est une société qui détient directement ou indirectement au moins 10% du capital de la société distributrice, l’Etat de la source ne prélève aucune retenue.</p>
<p>Par ailleurs, les dispositions du paragraphe 3 alinéas a et b de ce même article précisent que le transfert de l’avoir fiscal français(sous déduction de la retenue à la source de 15 %) s’effectue uniquement pour les distributions au bénéfice des personnes physiques résidentes d’Autriche et des personnes morales résidentes d’Autriche détenant directement ou indirectement moins de 10% du capital social de la société française distributrice, à condition que le bénéficiaire soit effectivement imposé en Autriche.</p>
<p>Le paragraphe 3 alinéa c précise par ailleurs que cette condition d’imposition effective ne s’applique pas aux caisses de retraite et de prévoyance ainsi qu’aux fonds de pension constitués en Autriche.</p>
<p><strong>Les intérêts</strong></p>
<p>Ce terme désigne les revenus des fonds publics, des obligations d’emprunts, des obligations d’emprunts et des créances de toute nature ainsi qu’aux intérêts produits par des titres négociables, les bons de caisse et les intérêts de créances ordinaires.</p>
<p>L’article 11 précise que les intérêts provenant d’un Etat et payés à un résident de l’autre Etat ne sont imposables que dans cet autre Etat si ce résident est le bénéficiaire effectif.</p>
<h5 class="spip">Cotisations de sécurité sociale</h5>
<p>L’article 24 de la convention permet à une personne physique résidente d’un Etat de déduire dans l’autre Etat les cotisations versées dans le cadre d’un régime légal de sécurité sociale ou d’un régime complémentaire d’assurance vieillesse ou décès de cet autre Etat.</p>
<h3 class="spip"><a id="sommaire_3"></a>Elimination de la double imposition</h3>
<p>L’élimination de la double imposition pour les résidents de France qui perçoivent des revenus de source autrichienne s’opère aux termes du paragraphe 1 de l’article 23 selon le régime de l’imputation. Le crédit d’impôt à imputer est égal :</p>
<ul class="spip">
<li>soit au montant de l’impôt payé en Autriche (cas des dividendes, gains en capital, rémunérations des artistes et sportifs) ;</li>
<li>soit au montant de l’impôt français correspondant à ces revenus.</li></ul>
<p>Le montant de l’impôt français sur lequel le crédit est imputable se détermine si l’impôt français est proportionnel, l’impôt à retenir est celui résultant de l’application du taux proportionnel aux revenus autrichiens visés ; s’il s’agit d’un barème progressif d’imposition, il convient de les prendre en compte pour la détermination du taux effectif.</p>
<p>Le calcul du taux effectif se décompose tout d’abord par la détermination d’une cotisation de base correspondant à l’ensemble des revenus de source française et(ou) étrangère passible de l’impôt français suivant les règles de la législation interne.</p>
<p>Ensuite, l’impôt exigible sera égal au produit de cette cotisation de base par le rapport entre le montant net total des revenus conventionnellement imposables en France et le montant total du revenu net d’après lequel le calcul de ladite cotisation de base a été effectué.</p>
<p>Au résultat ainsi obtenu peuvent être appliquées les réfactions prévues par la loi interne (crédit ou réduction d’impôt).</p>
<p><i>Mise à jour : février 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/fiscalite/article/convention-fiscale). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
