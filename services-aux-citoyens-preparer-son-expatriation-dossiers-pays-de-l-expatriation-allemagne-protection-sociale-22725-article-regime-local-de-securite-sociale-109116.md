# Régime local de sécurité sociale

<p>Vous trouverez une présentation détaillée du système de sécurité sociale allemand sur le site de notre partenaire, le <a href="http://www.cleiss.fr/" class="spip_out" rel="external">CLEISS</a> (Centre des liaisons européennes et internationales de sécurité sociale). En voici la table des matières :</p>
<ul class="spip">
<li><a href="http://www.cleiss.fr/docs/regimes/regime_allemagne0.html" class="spip_out" rel="external">Historique</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_allemagne1.html" class="spip_out" rel="external">I - Introduction</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_allemagne2.html" class="spip_out" rel="external">II - Financement</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_allemagne3.html" class="spip_out" rel="external">III - Assurance maladie, maternité</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_allemagne4.html" class="spip_out" rel="external">IV - Assurance dépendance</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_allemagne5.html" class="spip_out" rel="external">V - Assurance accidents</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_allemagne6.html" class="spip_out" rel="external">VI - Assurance pension</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_allemagne7.html" class="spip_out" rel="external">VII - Prestations familiales</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_allemagne8.html" class="spip_out" rel="external">VIII - Chômage</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_allemagne9.html" class="spip_out" rel="external">IX - Assistance sociale</a></li>
<li>X - Annexes :<br class="manualbr">- <a href="http://www.cleiss.fr/docs/regimes/regime_allemagne_caissesfamiliales.html" class="spip_out" rel="external">Liste des caisses familiales</a><br class="manualbr">- <a href="http://www.cleiss.fr/docs/cotisations/allemagne.html" class="spip_out" rel="external">Tableau de cotisations sociales</a><br class="manualbr">- <a href="http://www.cleiss.fr/docs/ol/allemagne.html" class="spip_out" rel="external">Institutions compétentes</a></li></ul>
<p><i>Mise à jour : juillet 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/protection-sociale-22725/article/regime-local-de-securite-sociale-109116). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
