# Loisirs et culture

<h4 class="spip">Activités culturelles</h4>
<p>La vie culturelle est assez limitée, en dehors du cinéma, de spectacles de musique (quelques concerts symphoniques sont notamment produits dans l’année) et des expositions de peinture.</p>
<p><strong>Activités culturelles en français</strong></p>
<ul class="spip">
<li><a href="http://www.alianzafrancesa.blogspot.fr/" class="spip_out" rel="external">Alliance française d’Assomption</a></li>
<li><a href="https://www.facebook.com/alianzafrancesa.deasuncion?fref=ts" class="spip_out" rel="external">Page facebook de l’Alliance française</a></li></ul>
<h4 class="spip">Télévision – Radio</h4>
<p>Il est possible d’accéder sur abonnement à des bouquets de programmes télévisés français (TV5) et étrangers.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/paraguay/vie-pratique/article/loisirs-et-culture). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
