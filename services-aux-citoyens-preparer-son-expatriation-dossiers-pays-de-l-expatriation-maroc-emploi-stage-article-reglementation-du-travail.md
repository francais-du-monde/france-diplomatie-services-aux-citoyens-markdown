# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/emploi-stage/article/reglementation-du-travail#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/emploi-stage/article/reglementation-du-travail#sommaire_2">Contrat de travail – spécificités</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/emploi-stage/article/reglementation-du-travail#sommaire_3">Fêtes légales</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/emploi-stage/article/reglementation-du-travail#sommaire_4">Emploi du conjoint</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/emploi-stage/article/reglementation-du-travail#sommaire_5">Création d’entreprises – spécificités</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<p>Le Maroc s’est doté depuis 2004 d’un Code du travail. Sa législation du travail est conforme aux conventions et recommandations du Bureau International du Travail. Il n’en demeure pas moins que, dans un pays où le secteur informel représente près de la moitié de l’activité, la question de l’application du Code du travail reste une préoccupation centrale.</p>
<p>Le salaire minimum est fixé à 12,24 dirhams de l’heure depuis le 1er juillet 2012, soit un salaire mensuel de 2 337,84 dirhams sur la base de 191 heures. Entre 2008 et 2012, le salaire minimum marocain est passé de 9,66 dirhams de l’heure à 12,24 dirhams de l’heure, soit une augmentation de 26,70 %.</p>
<p>Hors activités agricoles, la durée normale du travail est fixée à 2288 heures par an ou 44 heures par semaine avec un jour de repos hebdomadaire qui doit avoir une durée minimum de 24 heures consécutives. La durée normale du travail ne peut excéder 10 heures par jour. Le droit au congé payé est ouvert après six mois de travail effectif et continu. La durée des congés se calcule sur la base d’un jour et demi de travail effectif par mois de service, soit un minimum de 18 jours par an.</p>
<p>Le Code du travail prévoit également la majoration des heures supplémentaires.</p>
<p>L’âge de la retraite est fixé à 60 ans et à 55 ans pour les salariés du secteur minier. Des dérogations sont possibles. Le licenciement est réglementé.</p>
<p>Il est à noter que des conditions particulières plus favorables que celles prévues par le Code du Travail peuvent être en usage dans l’entreprise ou résulter d’une convention collective.</p>
<p>La législation marocaine sur l’emploi impose, pour un candidat étranger, que le contrat de travail soit accepté par le ministère marocain de l’Emploi, l’emploi à pourvoir devant être prioritairement attribué à un ressortissant marocain.</p>
<h4 class="spip">Cotisations sociales</h4>
<p>Des cotisations sociales, qui varient selon le régime adopté par l’entreprise, sont prélevées sur le salaire brut. L’IGR (impôt général sur le revenu) est également retenu à la source par l’employeur.</p>
<h3 class="spip"><a id="sommaire_2"></a>Contrat de travail – spécificités</h3>
<p><strong>Pour en savoir plus </strong> : <a href="http://www.rekrute.com/" class="spip_out" rel="external">Rekrute.com</a> conseils carrière / droit du travail / contrat de travail : ce que dit le code marocain.</p>
<h3 class="spip"><a id="sommaire_3"></a>Fêtes légales</h3>
<p><strong>Fêtes fixes</strong></p>
<ul class="spip">
<li>1er janvier : Jour de l’An</li>
<li>11 janvier : Célébration de la Déclaration d’Indépendance</li>
<li>1er mai : Fête du Travail</li>
<li>30 juillet : Fête du Trône</li>
<li>14 août : Allégeance au Wadi-Eddhahab</li>
<li>20 août : Jour de la Révolution du roi et du peuple</li>
<li>21 août : Fête de la jeunesse</li>
<li>6 novembre : Anniversaire de la "Marche Verte"</li>
<li>18 novembre : Fête de l’Indépendance</li></ul>
<p><strong>Fêtes mobiles</strong></p>
<ul class="spip">
<li>Aïd el Fitr : Fin du Ramadan (deux jours)</li>
<li>Aïd el Kebir : Fête du Sacrifice (deux jours)</li>
<li>Aïd el Mouloud : Naissance du Prophète nouvel an musulman</li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Emploi du conjoint</h3>
<p>Les possibilités d’emploi sont très réduites compte tenu de la législation marocaine sur le travail. Celle-ci impose un contrat accepté par le ministère de l’Emploi marocain, ce qui devient très rare, en raison du taux de chômage élevé. A diplômes égaux, la priorité est toujours donnée aux nationaux. A partir des fonctions de cadre supérieur et de direction, la préférence nationale devient plus souple. C’est à ce niveau que les postes sont le plus ouverts aux étrangers. Les secteurs les plus dynamiques offrant des possibilités d’emploi sont les transports/logistique, le tourisme, le BTP, l’environnement, la sous-traitance industrielle, l’ingénierie et les bureaux d’études, l’informatique et les technologies de l’information et de la communication, la distribution et la publicité.</p>
<p>Il existe quelques rares possibilités d’emploi dans les services français : établissements d’enseignement (sous réserve d’être titulaire d’un diplôme d’enseignement français) ou Instituts français.</p>
<h3 class="spip"><a id="sommaire_5"></a>Création d’entreprises – spécificités</h3>
<p>La première démarche dans la création de l’entreprise consiste dans le choix de la dénomination de la société et l’obtention du certificat négatif. Ce dernier peut être obtenu soit par le dépôt de la demande accompagnée des pièces requises directement auprès du centre régional d’investissement, ou par voie électronique en accédant aux services en ligne du site web de l’<a href="http://www.ompic.org.ma/fr" class="spip_out" rel="external">Office marocain de la propriété industrielle</a>.</p>
<p>Il convient de mentionner que cette démarche concerne toutes les sociétés commerciales à l’exception des entreprises individuelles qui n’optent pas pour une enseigne.</p>
<p>Toute la procédure est expliquée sur le site du <a href="http://www.service-public.ma/" class="spip_out" rel="external">service public marocain</a>, à la rubrique « je créé mon entreprise ».</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/emploi-stage/article/reglementation-du-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
