# Conseils par pays

<ul class="spip">
<li><strong>Renseignez-vous ! Certaines destinations peuvent être déconseillées pour votre sécurité.</strong></li></ul>
<ul class="spip">
<li><strong>Pendant votre voyage, <span class="alertcav">recevez nos alertes</span> en vous enregistrant sur <a href="https://pastel.diplomatie.gouv.fr/fildariane/dyn/protected/accueil/formAccueil.html" class="spip_out" rel="external">Ariane</a></strong></li></ul>
<ul class="spip">
<li><strong>Suivez-nous sur <a href="https://twitter.com/#!/ConseilsVoyages" class="spip_out" rel="external">Twitter</a></strong></li></ul>
<br class="nettoyeur">
<h2>Les cartes "Conseils aux Voyageurs" régionales</h2>
<ul>
<li><a href="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L1280xH886/20160727_fcvregional_monde_cle41332e-1-6dc7b.jpg" class="mediabox" title="Monde">
<img class="spip_logos" alt="" src="http://www.diplomatie.gouv.fr/fr/local/cache-gd2/723e82b6590b1fcac8778451007835e5.jpg" width="210" height="130">
</a></li>
</ul>
<ul>
<li><a href="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L1280xH1207/20160727_fcvregional_afrique_cle842df8-b5772.jpg" class="mediabox" title="Afrique">
<img class="spip_logos" alt="" src="http://www.diplomatie.gouv.fr/fr/local/cache-gd2/eed297b7676d5e07ad0fb113bb9b113b.jpg" width="210" height="130">
</a></li>
</ul>
<ul>
<li><a href="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L1280xH1832/20160623_fcvregional_ameriques_cle046158-eb1e8.jpg" class="mediabox" title="Amériques">
<img class="spip_logos" alt="" src="http://www.diplomatie.gouv.fr/fr/local/cache-gd2/1e15529b58ae7589aa8021a0b09f25ed.jpg" width="210" height="130">
</a></li>
</ul>
<ul>
<li><a href="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L1280xH534/20160727_fcvregional_anmo_2__cle8ba481-d1408.jpg" class="mediabox" title="Afrique du Nord / Moyen Orient">
<img class="spip_logos" alt="" src="http://www.diplomatie.gouv.fr/fr/local/cache-gd2/8591f9364ef827a5b4d35f15b316c162.jpg" width="210" height="130">
</a></li>
</ul>
<ul>
<li><a href="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L1280xH880/20160503_fcvregional_asieoceanie_cle05748c-91009.jpg" class="mediabox" title="Asie / Océanie">
<img class="spip_logos" alt="" src="http://www.diplomatie.gouv.fr/fr/local/cache-gd2/637832267047567d3a7ad27cdc663606.jpg" width="210" height="130">
</a></li>
</ul>
<ul>
<li><a href="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L1280xH1022/20160727_fcvregional_europe_cle0ee1a6-edf9f.jpg" class="mediabox" title="Europe">
<img class="spip_logos" alt="" src="http://www.diplomatie.gouv.fr/fr/local/cache-gd2/f326fe5f00cc928f1da2ed20dff3dc87.jpg" width="210" height="130">
</a></li>
</ul>
<ul class="liste_pays
       premier
      ">
<li><strong class="titrep">

          Afrique<i class="icon_fleche_noire"></i>
</strong></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/afrique-du-sud/" title="Afrique du Sud">Afrique du Sud</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/angola/" title="Angola">Angola</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/benin/" title="Bénin">Bénin</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/botswana/" title="Botswana">Botswana</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/burkina-faso/" title="Burkina Faso">Burkina Faso</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/burundi/" title="Burundi">Burundi</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/cabo-verde/" title="Cabo Verde">Cabo Verde</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/cameroun/" title="Cameroun">Cameroun</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/comores/" title="Comores">Comores</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/congo/" title="Congo">Congo</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/cote-d-ivoire/" title="Côte d'Ivoire">Côte d’Ivoire</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/djibouti/" title="Djibouti">Djibouti</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/erythree/" title="Erythrée">Erythrée</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/ethiopie/" title="Ethiopie">Ethiopie</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/gabon/" title="Gabon">Gabon</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/gambie/" title="Gambie">Gambie</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/ghana/" title="Ghana">Ghana</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/guinee/" title="Guinée">Guinée</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/guinee-equatoriale/" title="Guinée Equatoriale">Guinée Equatoriale</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/guinee-bissao/" title="Guinée-Bissao">Guinée-Bissao</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/kenya/" title="Kenya">Kenya</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/lesotho/" title="Lesotho">Lesotho</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/liberia/" title="Libéria">Libéria</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/madagascar/" title="Madagascar">Madagascar</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/malawi/" title="Malawi">Malawi</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/mali/" title="Mali">Mali</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/maurice/" title="Maurice">Maurice</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/mauritanie/" title="Mauritanie">Mauritanie</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/mozambique/" title="Mozambique">Mozambique</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/namibie/" title="Namibie">Namibie</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/niger/" title="Niger">Niger</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/nigeria/" title="Nigéria">Nigéria</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/ouganda/" title="Ouganda">Ouganda</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/republique-centrafricaine/" title="République centrafricaine">République centrafricaine</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/republique-democratique-du-congo/" title="République démocratique du Congo">République démocratique du Congo</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/rwanda/" title="Rwanda">Rwanda</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/sainte-helene/" title="Sainte-Hélène">Sainte-Hélène</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/sao-tome-et-principe/" title="Sao Tomé-et-Principe">Sao Tomé-et-Principe</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/senegal/" title="Sénégal">Sénégal</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/seychelles/" title="Seychelles">Seychelles</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/sierra-leone/" title="Sierra Leone">Sierra Leone</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/somalie/" title="Somalie">Somalie</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/soudan/" title="Soudan">Soudan</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/soudan-du-sud/" title="Soudan du Sud">Soudan du Sud</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/swaziland/" title="Swaziland">Swaziland</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/tanzanie/" title="Tanzanie">Tanzanie</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/tchad/" title="Tchad">Tchad</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/togo/" title="Togo">Togo</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/zambie/" title="Zambie">Zambie</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/zimbabwe/" title="Zimbabwe">Zimbabwe</a></li>
</ul>
<ul class="liste_pays

      ">
<li><strong class="titrep">

          Amériques<i class="icon_fleche_noire"></i>
</strong></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/antigua-et-barbuda/" title="Antigua-et-Barbuda">Antigua-et-Barbuda</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/argentine/" title="Argentine">Argentine</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/bahamas/" title="Bahamas">Bahamas</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/barbade/" title="Barbade">Barbade</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/belize/" title="Belize">Belize</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/bolivie/" title="Bolivie">Bolivie</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/bresil/" title="Brésil">Brésil</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/canada-quebec-compris/" title="Canada (Québec compris) ">Canada (Québec compris) </a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/chili/" title="Chili">Chili</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/colombie/" title="Colombie">Colombie</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/costa-rica/" title="Costa Rica">Costa Rica</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/cuba/" title="Cuba">Cuba</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/dominique/" title="Dominique">Dominique</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/equateur/" title="Equateur">Equateur</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/etats-unis/" title="Etats-Unis">Etats-Unis</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/grenade/" title="Grenade">Grenade</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/guatemala/" title="Guatemala">Guatemala</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/guyana/" title="Guyana">Guyana</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/haiti/" title="Haïti">Haïti</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/honduras/" title="Honduras">Honduras</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/jamaique/" title="Jamaïque">Jamaïque</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/mexique/" title="Mexique">Mexique</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/nicaragua/" title="Nicaragua">Nicaragua</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/panama/" title="Panama">Panama</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/paraguay/" title="Paraguay">Paraguay</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/perou/" title="Pérou">Pérou</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/republique-dominicaine/" title="République Dominicaine">République Dominicaine</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/saint-christophe-et-nieves/" title="Saint-Christophe-et-Niévès">Saint-Christophe-et-Niévès</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/saint-vincent-et-les-grenadines/" title="Saint-Vincent-et-les-Grenadines">Saint-Vincent-et-les-Grenadines</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/sainte-lucie/" title="Sainte-Lucie">Sainte-Lucie</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/salvador/" title="Salvador">Salvador</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/suriname/" title="Suriname">Suriname</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/trinite-et-tobago/" title="Trinité et Tobago">Trinité et Tobago</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/uruguay/" title="Uruguay">Uruguay</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/venezuela/" title="Venezuela">Venezuela</a></li>
</ul>
<ul class="liste_pays

      ">
<li><strong class="titrep">

          Afrique du Nord / Moyen-Orient<i class="icon_fleche_noire"></i>
</strong></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/algerie/" title="Algérie">Algérie</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/arabie-saoudite/" title="Arabie Saoudite">Arabie Saoudite</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/bahrein/" title="Bahreïn">Bahreïn</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/egypte/" title="Egypte">Egypte</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/emirats-arabes-unis/" title="Emirats arabes unis">Emirats arabes unis</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/irak/" title="Irak">Irak</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/iran/" title="Iran">Iran</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/israel-territoires-palestiniens/" title="Israël/Territoires palestiniens">Israël/Territoires palestiniens</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/jordanie/" title="Jordanie">Jordanie</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/koweit/" title="Koweït">Koweït</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/liban/" title="Liban">Liban</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/libye/" title="Libye">Libye</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/maroc/" title="Maroc">Maroc</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/oman/" title="Oman">Oman</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/qatar/" title="Qatar">Qatar</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/syrie/" title="Syrie">Syrie</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/tunisie/" title="Tunisie">Tunisie</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/yemen/" title="Yemen">Yemen</a></li>
</ul>
<ul class="liste_pays

      ">
<li><strong class="titrep">

          Asie<i class="icon_fleche_noire"></i>
</strong></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/afghanistan/" title="Afghanistan">Afghanistan</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/bangladesh/" title="Bangladesh">Bangladesh</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/bhoutan/" title="Bhoutan">Bhoutan</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/birmanie/" title="Birmanie">Birmanie</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/brunei/" title="Brunei">Brunei</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/cambodge/" title="Cambodge">Cambodge</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/chine/" title="Chine">Chine</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/coree-du-nord/" title="Corée du Nord">Corée du Nord</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/coree-du-sud/" title="Corée du Sud">Corée du Sud</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/chine/" title="Hong Kong">Hong Kong</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/inde/" title="Inde">Inde</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/indonesie/" title="Indonésie">Indonésie</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/japon/" title="Japon">Japon</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/kazakhstan/" title="Kazakhstan">Kazakhstan</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/kirghizstan/" title="Kirghizstan">Kirghizstan</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/laos/" title="Laos">Laos</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/chine/" title="Macao">Macao</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/malaisie/" title="Malaisie">Malaisie</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/maldives/" title="Maldives">Maldives</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/mongolie/" title="Mongolie">Mongolie</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/nepal/" title="Népal">Népal</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/ouzbekistan/" title="Ouzbékistan">Ouzbékistan</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/pakistan/" title="Pakistan">Pakistan</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/philippines/" title="Philippines">Philippines</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/singapour/" title="Singapour">Singapour</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/sri-lanka/" title="Sri Lanka">Sri Lanka</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/tadjikistan/" title="Tadjikistan">Tadjikistan</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/taiwan/" title="Taïwan">Taïwan</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/thailande/" title="Thaïlande">Thaïlande</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/timor-est/" title="Timor Est">Timor Est</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/turkmenistan/" title="Turkmenistan">Turkmenistan</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/vietnam/" title="Vietnam">Vietnam</a></li>
</ul>
<ul class="liste_pays

      ">
<li><strong class="titrep">

          Europe<i class="icon_fleche_noire"></i>
</strong></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/albanie/" title="Albanie">Albanie</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/allemagne/" title="Allemagne">Allemagne</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/ancienne-republique-yougoslave-de-macedoine-arym/" title="Ancienne République yougoslave de Macédoine (ARYM)">Ancienne République yougoslave de Macédoine (ARYM)</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/andorre/" title="Andorre">Andorre</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/armenie/" title="Arménie">Arménie</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/autriche/" title="Autriche">Autriche</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/azerbaidjan/" title="Azerbaïdjan">Azerbaïdjan</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/belgique/" title="Belgique">Belgique</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/bielorussie/" title="Biélorussie">Biélorussie</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/bosnie-herzegovine/" title="Bosnie-Herzégovine">Bosnie-Herzégovine</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/bulgarie/" title="Bulgarie">Bulgarie</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/chypre/" title="Chypre">Chypre</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/croatie/" title="Croatie">Croatie</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/danemark/" title="Danemark">Danemark</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/espagne/" title="Espagne">Espagne</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/estonie/" title="Estonie">Estonie</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/finlande/" title="Finlande">Finlande</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/georgie/" title="Géorgie">Géorgie</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/grece/" title="Grèce">Grèce</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/hongrie/" title="Hongrie">Hongrie</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/irlande/" title="Irlande">Irlande</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/islande/" title="Islande">Islande</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/italie/" title="Italie">Italie</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/kosovo/" title="Kosovo">Kosovo</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/lettonie/" title="Lettonie">Lettonie</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/lituanie/" title="Lituanie">Lituanie</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/luxembourg/" title="Luxembourg">Luxembourg</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/malte/" title="Malte">Malte</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/moldavie/" title="Moldavie">Moldavie</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/monaco/" title="Monaco">Monaco</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/montenegro/" title="Monténégro">Monténégro</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/norvege/" title="Norvège">Norvège</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/pays-bas/" title="Pays-Bas">Pays-Bas</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/pologne/" title="Pologne">Pologne</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/portugal/" title="Portugal">Portugal</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/republique-tcheque/" title="République Tchèque">République Tchèque</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/roumanie/" title="Roumanie">Roumanie</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/royaume-uni/" title="Royaume-Uni">Royaume-Uni</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/russie/" title="Russie">Russie</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/serbie/" title="Serbie">Serbie</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/slovaquie/" title="Slovaquie">Slovaquie</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/slovenie/" title="Slovénie">Slovénie</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/suede/" title="Suède">Suède</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/suisse/" title="Suisse">Suisse</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/turquie/" title="Turquie">Turquie</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/ukraine/" title="Ukraine">Ukraine</a></li>
</ul>
<ul class="liste_pays

      ">
<li><strong class="titrep">

          Océanie<i class="icon_fleche_noire"></i>
</strong></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/australie/" title="Australie">Australie</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/iles-cook/" title="Iles Cook">Iles Cook</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/iles-fidji/" title="Iles Fidji">Iles Fidji</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/iles-salomon/" title="Iles Salomon">Iles Salomon</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/nouvelle-zelande/" title="Nouvelle-Zélande">Nouvelle-Zélande</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/papouasie-nouvelle-guinee/" title="Papouasie-Nouvelle-Guinée">Papouasie-Nouvelle-Guinée</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/republique-des-palaos-12207/" title="République des Palaos">République des Palaos</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/samoa/" title="Samoa">Samoa</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/tonga/" title="Tonga">Tonga</a></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/vanuatu/" title="Vanuatu">Vanuatu</a></li>
</ul>
<ul class="liste_pays

       dernier">
<li><strong class="titrep">

          Antarctique<i class="icon_fleche_noire"></i>
</strong></li>
<li class="colonne_pays">
<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/antarctique-21201/" title="Antarctique">Antarctique</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
