# Passeport, visa, permis de travail

<p>Pour toute information relative aux formalités de séjour en Grèce, nous vous conseillons de contacter la <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-etrangers-en-france/article/annuaire-des-representations-etrangeres-en-france" class="spip_in">section consulaire de l’ambassade de Grèce en France</a>. Les informations suivantes sont données à titre indicatif.</p>
<p><strong>Formalités à accomplir auprès des autorités locales en cas d’établissement de votre résidence en Grèce :</strong></p>
<p>Conformément au décret présidentiel grec numéro 106, en vigueur depuis sa publication au Journal Officiel de la République Hellénique, le 21 juin 2007 relatif à la libre circulation et au séjour en Grèce des ressortissants de l’Union européenne et des membres de leurs familles, et fondé sur la directive européenne numéro 2004/38/CE du 29 avril 2004, les dispositions suivantes s’appliquent désormais aux citoyens français qui établissent leur résidence en Grèce :</p>
<ul class="spip">
<li>Les Français séjournant en Grèce pendant une <strong>période inférieure à trois mois</strong> doivent être munis de leur carte nationale d’identité ou de leur passeport.</li>
<li>Les Français établis en Grèce pour une <strong>période comprise entre trois mois et cinq ans</strong> doivent demander une <strong>attestation d’inscription</strong> à la police des étrangers de leur domicile.<br class="manualbr">Ce document, valable cinq ans, est délivré gratuitement sur production des justificatifs d’identité (carte d’identité ou passeport), de ressources (ou de statut) et de couverture sociale (la liste des documents à produire sera précisée par le service des étrangers du commissariat compétent).</li>
<li>Les Français qui ont établi leur résidence en Grèce pendant <strong>plus de cinq ans</strong> acquièrent un <strong>titre de séjour permanent</strong> qui n’est soumis à aucune condition de ressources.</li></ul>
<p>Les membres de la famille des ressortissants de l’Union européenne qui n’ont pas la nationalité d’un des pays membres de l’UE, doivent solliciter une carte de séjour de membre de famille d’un ressortissant de l’UE, dont la validité maximale est de cinq ans.</p>
<p>Il importe dès votre arrivée de vous procurer un numéro fiscal (<i>AFM/ΑΦΜ</i>), document indispensable pour ouvrir un compte bancaire, acheter un téléphone portable, etc. auprès du bureau des impôts (AIO) de votre lieu de résidence et d’obtenir un numéro de sécurité sociale <a href="http://www.amka.gr/aparaitita_en.html" class="spip_out" rel="external">(AMKA</a>)</p>
<p><i>Mise à jour : novembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/entree-et-sejour-22878/article/passeport-visa-permis-de-travail-110373). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
