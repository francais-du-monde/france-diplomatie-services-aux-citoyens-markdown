# Curriculum vitae

<p><strong>Rédaction</strong></p>
<p>La manière de rédiger un curriculum vitae (CV) en Suisse et, de manière générale, de présenter un dossier de candidature est très spécifique. Voici quelques-uns des points importants à retenir pour la rédaction du CV :</p>
<p>Le CV doit contenir des termes que le recruteur comprendra. En effet, certaines subtilités de vocabulaire, tant en français qu’en allemand, peuvent exister.</p>
<p>Selon le secteur d’activité, certains termes peuvent être propres à la Suisse et différer de ceux de ceux utilisés en France. Il est donc important de bien s’informer et de consulter les sites Internet des principaux acteurs du secteur.</p>
<p>Le CV doit retracer de manière vivante l’histoire du candidat et inclure des éléments tangibles, des réalisations concrètes. En effet, les " penseurs " sont peu appréciés en Suisse et la préférence va aux personnes qui agissent et qui apportent quelque chose à l’entreprise.</p>
<p>Le CV fait partie de ce qu’on appelle en Suisse le " dossier de candidature " lequel inclut, entre autres, les copies des certificats de travail, des diplômes, etc. En cas de candidature par courrier, il n’est pas obligatoire d’envoyer tous ces documents, mais ils devront être fournis lors du premier entretien.</p>
<p>Les candidats étrangers doivent indiquer leur nationalité. La mention de la situation familiale (célibataire, marié, nombre d’enfants, etc.) n’est pas obligatoire.</p>
<p>Il ne faut pas tout dire dans le CV. Pour chaque information présentée, le candidat doit se demander si elle est pertinente pour la candidature. Si la réponse est non, il est peut-être préférable d’éliminer certains détails.</p>
<p><strong>Sites Internet à consulter</strong></p>
<ul class="spip">
<li><a href="http://www.espace-emploi.ch/" class="spip_out" rel="external">SECO</a> - Direction du travail - Espace emploi : rubrique A la recherche d’un emploi &gt; conseils en matière de candidature.</li>
<li>Site <a href="http://www.travailler-en-suisse.ch/" class="spip_out" rel="external">Travailler en Suisse</a> : rubrique Travailler.</li></ul>
<p><strong>Modèles de CV</strong></p>
<p>Comme le CV français, le CV suisse est composé de plusieurs rubriques essentielles qui permettront de vous identifier et de guider correctement le lecteur. Le CV suisse demeure très classique, donc il faut éviter les distinctions superflues. Le but est de se distinguer tout en restant discret et crédible.</p>
<p>Il n’y a pas de normes qui s’appliquent à tout le monde. L’important est de faire apparaître les rubriques essentielles suivantes :</p>
<ul class="spip">
<li>Identité et coordonnées : vos nom et prénom, adresse de votre domicile, vos numéros de téléphone (fixe et/ou portable) et votre nationalité ainsi que le type de permis de travail si vous en disposez. Il n’est pas utile de préciser sur le CV le nom de cette rubrique car elle est suffisamment évidente. Vous n’êtes pas tenu non plus d’indiquer votre âge, votre état civil ni d’apposer votre photo.</li>
<li>Formation universitaire et diplômes : elle dévoilera par ordre décroissant votre parcours scolaire à compter du baccalauréat jusqu’à l’obtention de votre diplôme de fin d’études universitaires et les dates d’obtention. Les mentions et distinctions peuvent apparaître.</li>
<li>Expérience professionnelle : elle informera par ordre décroissant des emplois occupés, en précisant les dates, votre fonction et succinctement les tâches réalisées.</li></ul>
<p>L’ordre des rubriques peut varier. Certains introduiront leur CV par une présentation " télégraphique ", à l’aide de mots clés, de leur profil et de leur domaine professionnels afin de frapper rapidement et positivement le lecteur, et choisiront ensuite de parler de leur expérience professionnelle, puis de leur formation universitaire. En bas de page, une place sera réservée à quelques références clés ainsi qu’aux stages et/ou à toute autre information pertinente. Quant aux capacités linguistiques, elles apparaîtront en bas de page ou en-tête dans le même espace que le profil et projet et/ou domaine professionnels. Si vous maîtrisez plusieurs langues, il est plus judicieux d’opter pour l’en-tête.</p>
<p><strong>Diplôme (équivalence, légalisation)</strong></p>
<p>La Suisse et l’Union européenne se reconnaissent mutuellement les diplômes permettant d’exercer des professions dites " réglementées " dans chacun des États membres, validées par un diplôme national ou une carte professionnelle conformément à la Loi. Le Centre national d’information de l’Office fédéral de la formation professionnelle et de la technologie (OFFT), est, en Suisse, l’autorité compétente pour l’évaluation des équivalences étrangères avec des titres et diplômes suisses, dans les domaines de la formation professionnelle et des hautes écoles spécialisées. Cet organisme informe sur la procédure. Toutefois, l’OFFT n’est pas compétent pour le domaine des universités, des écoles polytechniques fédérales et de la formation des enseignants.</p>
<p>Domaines de compétence de l’OFFT</p>
<ul class="spip">
<li>Maturité professionnelle ;</li>
<li>Formations dans le domaine de la santé ;</li>
<li>Professions médicales non universitaires (infirmiers, physiothérapeutes, etc.) ;</li>
<li>Professions médicales universitaires (médecins, dentistes, vétérinaires, pharmaciens) ;</li>
<li>Formations dans le domaine de l’éducation ;</li>
<li>Reconnaissance des diplômes.</li></ul>
<p>La reconnaissance des diplômes et des prestations d’études est prévue par une convention cadre entre la France et la Suisse, conclue en 1994. Le 10 septembre 2008, l’accord-cadre a été revu et élargi aux hautes écoles spécialisées et aux hautes écoles pédagogiques en Suisse et aux écoles françaises d’ingénieurs.</p>
<p>Le baccalauréat est certes reconnu en Suisse, cependant la moyenne générale doit être de 12 sur 20. En ce qui concerne, les autres diplômes (Deug, licence, maîtrise, BTS ou DUT) étant donné qu’il n’y a pas d’équivalence à proprement parler, il faut s’adresser directement à l’université qui instruira votre demande d’admission. Dans tous les cas, il est essentiel d’être diplômé(e) du baccalauréat. Les diplômés des grandes écoles françaises sont admis à poursuivre des études en Suisse dès lors où ces établissements sont officiellement reconnus en France.</p>
<p><strong>Pour en savoir plus</strong></p>
<p><a href="http://www.bbt.admin.ch/" class="spip_out" rel="external">Office fédéral de la formation professionnelle et de la technologie</a> (OFFT) :  rubrique Thèmes &gt; Reconnaissance internationale des diplômes.</p>
<p><i>Mise à jour : février 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/emploi-stage/article/curriculum-vitae-105246). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
