# Centre national d’enseignement à distance (CNED)

<p class="spip_document_18199 spip_documents spip_documents_left">
<a href="http://www.cned.fr" class="spip_out"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L142xH40/CNED-44213.gif" width="142" height="40" alt="CNED"></a></p>
<p>Si vous partez vivre dans un pays où ne se trouve aucun établissement d’enseignement français, vous pourrez faire suivre à votre enfant des cours auprès du <strong>Centre national d’enseignement à distance (CNED)</strong>.</p>
<p>Le CNED est un organisme officiel du ministère de l’Éducation nationale qui dispense un enseignement conforme aux programmes français. Les passages de classes sont décidés par les professeurs du CNED et permettent l’admission des élèves concernés <strong>dans n’importe quel établissement français, en France ou à l’étranger.</strong></p>
<p>Si votre enfant ne suit pas en personne l’enseignement de l’un des établissements agréés par le ministère de l’Éducation nationale, vous pouvez l’inscrire <strong>individuellement</strong> au CNED. Certaines écoles inscrivent <strong>collectivement</strong> leurs élèves aux cours du CNED, des assistants pédagogiques s’occupant alors de les faire travailler.</p>
<p>Pour ce qui est de l’enseignement supérieur, le CNED, en partenariat avec des universités françaises, prépare à certaines formations à distance, le plus souvent sous la forme de cours et de tutorat en ligne.</p>
<p>Pour toute demande de renseignements concernant les prestations du CNED et les modalités d’inscription, adressez-vous au :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.cned.fr/" class="spip_out" rel="external">CNED</a> - Télé-Accueil<br class="manualbr">B.P 60200 <br class="manualbr">86980 Futuroscope Chasseneuil Cedex <br class="manualbr">Télécopie : 05 49 49 96 96</p>
<p><i>Mise à jour : mars 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/scolarite-en-francais/article/enseignement-a-distance). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
