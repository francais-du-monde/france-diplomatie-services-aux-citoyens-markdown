# Loisirs et culture

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/vie-pratique/article/loisirs-et-culture#sommaire_1">Tourisme</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/vie-pratique/article/loisirs-et-culture#sommaire_2">Activités culturelles</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/vie-pratique/article/loisirs-et-culture#sommaire_3">Sports</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/vie-pratique/article/loisirs-et-culture#sommaire_4">Télévision – Radio</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Tourisme</h3>
<p>En dehors de Londres qui constitue un haut lieu touristique, le Royaume-Uni recèle de nombreuses régions de grand intérêt. Les sites touristiques naturels et historiques sont multiples et aux charmes variés.</p>
<p>Le Pays de Galles et la région des Lacs (<i>Lake District</i>) offrent des paysages remarquables. La ville médiévale de Chester et la cité de York présentent un intérêt historique incontestable, de même que les villes universitaires de Cambridge et Oxford.</p>
<p>La vallée de la Tamise, la région Sud de l’Angleterre et la Cornouaille sont également à visiter, sans oublier bien sûr, les paysages d’Ecosse, ainsi que les îles de Jersey et Guernesey.</p>
<p><strong>Office de tourisme de Grande-Bretagne</strong><br class="manualbr">Boîte Postale 154<br class="manualbr">75363 Paris cedex 08<br class="manualbr">Tél. : 01 58 36 50 50</p>
<p>Un accueil est dorénavant strictement réservé aux professionnels. Pour tout renseignement veuillez envoyer un <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/vie-pratique/article/loisirs-et-culture#gbinfo#mc#visitbritain.org#" title="gbinfo..åt..visitbritain.org" onclick="location.href=mc_lancerlien('gbinfo','visitbritain.org'); return false;" class="spip_mail">courriel</a>  (réponse sous 24h) ou consulter le site internet de <a href="http://www.visitbritain.com/" class="spip_out" rel="external">l’Office de tourisme</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Activités culturelles</h3>
<h4 class="spip">Activités culturelles en français</h4>
<p>Le réseau culturel français est très développé au Royaume-Uni. Il comprend en particulier un Institut français basé à Londres, avec une antenne à Edimbourg, les associations de l’Alliance française et la Maison française d’Oxford.</p>
<p>Ces organismes dispensent des cours de français et déploient une vaste activité culturelle sous forme de conférences, de représentations théâtrales et de concerts. Ils mettent par ailleurs leurs bibliothèques et vidéothèques à la disposition du public.</p>
<h5 class="spip">Instituts Français</h5>
<p><strong>Ecosse</strong></p>
<p>13 Randolph Crescent<br class="manualbr">Edinburgh EH3 7TT<br class="manualbr">Tél. : [44] (0) 13 1225 5366<br class="manualbr">Fax : [44] (0) 13 1220 0648<br class="autobr">Internet : <a href="http://www.ifecosse.org.uk/" class="spip_out" rel="external">www.ifecosse.org.uk</a></p>
<p><strong>Royaume-Uni</strong></p>
<p>17 Queensberry Place<br class="manualbr">London SW7 2DT<br class="manualbr">Tél. : [44] (0) 20 7073 1350<br class="manualbr">Fax : [44] (0) 20 7073 1355<br class="manualbr">Internet : <a href="http://www.institut-francais.org.uk/" class="spip_out" rel="external">www.institut-francais.org.uk</a></p>
<p>Fondé en 1910, l’Institut français est installé dans un impressionnant bâtiment art-déco basé à South Kensington, à Londres. Au sein d’un vaste réseau de 150 instituts à travers le monde, il constitue le centre culturel et linguistique officiel du gouvernement français.</p>
<p>Ce complexe regroupe notamment :</p>
<ul class="spip">
<li><strong>le centre de langues </strong>où plus de 6000 étudiants sont inscrits chaque année à un programme dynamique de cours ;</li>
<li><strong>le "Ciné Lumière"</strong>, véritable vitrine du cinéma français, européen et même mondial, qui propose chaque année plus de 600 programmations, y compris des avant-premières, des films récents, des classiques et des films rares, avec des rencontres de réalisateurs, scénaristes et acteurs ;</li>
<li><strong>la bibliothèque multimédia</strong>, répartie sur deux étages, avec un important choix de livres, journaux, périodiques, vidéos, cédéroms et autres produits en français ;</li>
<li><strong>une médiathèque spécialisée pour enfants</strong>, un service d’information rapide et le bureau des études en France ;</li>
<li><strong>le "Bistrot"</strong>, café typiquement français, proposant des plats régionaux authentiques, des en-cas et une carte des vins variée.</li></ul>
<p>Des discussions et débats sont régulièrement organisés, ainsi que des dégustations de vins français.</p>
<h4 class="spip">Alliance française</h4>
<p>Depuis plus d’un siècle, l’Alliance française a pour mission d’étendre la langue et la culture françaises par-delà les frontières. Un réseau d’associations implantées dans le monde entier et bénéficiant du label "Alliance" diffuse la langue française auprès des publics étrangers.</p>
<p>Au Royaume-Uni, l’Alliance française est représentée dans les principales villes (Glasgow, <a href="http://www.alliancefrancaisemanchester.org/" class="spip_out" rel="external">Manchester</a>, et au travers des cercles français présents sur tout le territoire). Les activités des centres et associations, membres de l’organisation, sont coordonnées à partir du bureau de Londres :</p>
<p><a href="http://www.alliancefrancaise.org.uk/" class="spip_out" rel="external">Alliance française</a><br class="autobr">1, Dorset Square<br class="autobr">London NW1 6PU<br class="autobr">Tél. : [44] (0) 20 7723 6439<br class="autobr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/vie-pratique/article/loisirs-et-culture#info#mc#alliancefrancaise.org.uk#" title="info..åt..alliancefrancaise.org.uk" onclick="location.href=mc_lancerlien('info','alliancefrancaise.org.uk'); return false;" class="spip_mail">Courriel</a></p>
<h4 class="spip">Maison française d’Oxford</h4>
<p>La Maison française d’Oxford est une institution d’enseignement et de recherche, de diffusion culturelle et scientifique, établie auprès de l’université d’Oxford.</p>
<p>Elle assure la pérennité des liens entre l’université d’Oxford, ses facultés et ses collèges et l’ensemble des universités et grands établissements d’enseignement et de recherche français.</p>
<p>Elle accueille des étudiants doctorants et chercheurs pour des séjours et conduit des programmes de recherche, en coopération avec des institutions et chercheurs britanniques.</p>
<p>Elle organise des rencontres scientifiques (colloques, tables rondes, journées d’études, séries de conférences…) et participe à la publication et à la diffusion des travaux et rencontres.</p>
<p><a href="http://www.mfo.ac.uk/" class="spip_out" rel="external">Maison française d’Oxford</a><br class="manualbr">Norham Road<br class="manualbr">Oxford OX2 6SE<br class="manualbr">Tél. : [44] (0) 18 6527 4220<br class="manualbr">Fax : [44] (0) 18 6527 4225</p>
<h5 class="spip">Cinéma</h5>
<p>Le cinéma français est présent à Londres, mais également à Cambridge, Oxford, Manchester, Edimbourg (Festival du film français en décembre) et Glasgow. Les films sont en principe présentés en version originale.</p>
<p>Les clubs vidéo et certains magasins distribuent également des cassettes de films français.</p>
<h5 class="spip">Spectacles</h5>
<p>A Londres, les spectacles et pièces de théâtre français ou les expositions d’artistes français sont fréquents. La liste complète des films et des spectacles se trouve dans le magazine hebdomadaire <a href="http://www.timeout.com/london/free-mag" class="spip_out" rel="external">Time Out</a>.</p>
<h4 class="spip">Activités culturelles locales</h4>
<h5 class="spip">Cinéma, spectacles et festivals</h5>
<p>Londres, en raison de la concentration des théâtres, musées, galeries d’art, salles de concert, s’impose comme la capitale culturelle de la Grande-Bretagne et offre toute une gamme de spectacles de grande qualité. Les bibliothèques sont aussi très nombreuses (chaque quartier en possède une).</p>
<p>L’Ecosse, le Pays de Galles, l’Irlande du Nord ont également de fortes traditions culturelles qui contribuent à enrichir la vie britannique. Une saison culturelle avec cinq festivals (jazz, musique classique, théâtre, livres, cinéma) se tient à Edimbourg chaque année en août ou septembre, pendant trois semaines. Cette manifestation culturelle ne cesse de prendre de l’importance et sa réputation a depuis longtemps dépassé les frontières de la Grande-Bretagne.</p>
<p>Le cinéma présenté est international. Les très nombreuses salles de cinéma offrent un choix très vaste de films. Il en est de même pour les pièces de théâtre ou les concerts.</p>
<h3 class="spip"><a id="sommaire_3"></a>Sports</h3>
<p>Tous les sports peuvent être pratiqués. Il existe des clubs sportifs très divers et de nombreuses salles de sport. Les courses hippiques sont très suivies par les Anglais, surtout l’été.</p>
<p>Des rencontres de rugby, football, cricket se déroulent chaque semaine. La chasse et la pêche nécessitent l’obtention de permis.</p>
<h3 class="spip"><a id="sommaire_4"></a>Télévision – Radio</h3>
<p>Les programmes diffusés à la radio (200 stations) sont variés et généralement de bonne qualité. La télévision britannique propose des programmes de qualité.</p>
<p>Il existe cinq chaînes télévisées diffusant en fréquences hertziennes :</p>
<ul class="spip">
<li>BBC1 et BBC2, chaînes publiques financées par la redevance, elles ne diffusent aucune forme de publicité ;</li>
<li>trois chaînes privées, ouvertes à la publicité : Channel 3 (ITV), divisée en 15 chaînes régionales ; Channel 4, à vocation plus culturelle et Channel 5 qui propose un éventail d’émissions variées, dont beaucoup de films et de feuilletons américains.</li></ul>
<p>Il existe de nombreuses chaînes accessibles par câble (Cable  Wireless, Telewest et NTL) ou par satellite (BskyB et Sky Digital). La télévision interactive est en pleine expansion.</p>
<p>Il est possible de capter des chaînes étrangères avec une parabole.</p>
<p>La vidéo est très répandue. Le système adopté est PAL I (GB). Vous devez vous acquitter de la redevance audiovisuelle (TV license) dont le tarif annuel s’élève à 145,50 £ (162 €) pour un téléviseur couleur.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/vie-pratique/article/loisirs-et-culture). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
