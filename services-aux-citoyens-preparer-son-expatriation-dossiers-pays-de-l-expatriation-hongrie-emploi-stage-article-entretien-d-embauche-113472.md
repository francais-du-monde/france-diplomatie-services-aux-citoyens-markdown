# Entretien d’embauche

<p>Le processus de recrutement en Hongrie est aujourd’hui très similaire à celui pratiqué en France, avec un ou plusieurs entretiens suite à l’étude du CV et de la lettre de motivation.</p>
<p>Pour l’entretien, il convient de vous y préparer en prenant en considération les principaux points suivants :</p>
<ul class="spip">
<li>Conduite de l’entretien</li>
<li>Apparence et attitude</li>
<li>Négociation du salaire</li>
<li>Questions préférées des recruteurs</li>
<li>Erreurs à éviter</li>
<li>Après l’entretien</li></ul>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/emploi-stage/article/entretien-d-embauche-113472). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
