# Communications

<h4 class="spip">Téléphone – Internet</h4>
<p>L’indicatif téléphonique pour la République Dominicaine est le <strong>1</strong>.</p>
<p>Les liaisons téléphoniques sont bonnes.</p>
<p>Plusieurs opérateurs proposent des bouquets comportant une offre téléphonie – internet – télévision par câble à des prix alléchants. Certains bouquets permettent même d’appeler gratuitement sur les téléphones fixes de 40 pays (dont la France).</p>
<p>Les connexions Internet fonctionnent généralement bien, même s’il peut y avoir des coupures sur le réseau. Il est même proposé plusieurs tarifs en fonction de la vitesse de connexion.</p>
<h4 class="spip">Poste</h4>
<p>Les délais d’acheminement du courrier varient entre quinze jours et un mois. La réception est garantie pour les envois recommandés ou utilisant les services des agences de messageries internationales.</p>
<p>Par contre, la réception de colis en provenance de l’étranger est soumise au dédouanement et peut revenir cher.</p>
<p>A l’intérieur du pays, on peut utiliser les services des compagnies de bus (Metro / Caribe Tours) pour envoyer lettres et colis.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/vie-pratique/article/communications). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
