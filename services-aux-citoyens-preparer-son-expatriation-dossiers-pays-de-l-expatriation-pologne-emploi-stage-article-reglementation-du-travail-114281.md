# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/emploi-stage/article/reglementation-du-travail-114281#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/emploi-stage/article/reglementation-du-travail-114281#sommaire_2">Fêtes légales</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/emploi-stage/article/reglementation-du-travail-114281#sommaire_3">Emploi du conjoint</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<p>La durée de travail est de 8h/jour, 40h/semaine. Coût des heures supplémentaires : jours ouvrables – deux premières heures supplémentaires : 150%, chaque heure suivante : 200%. Jours de repos ou fériés : 200%.</p>
<p>Il existe 13 jours fériés payés et la durée des congés annuels payés est de 20 à 26 jours ouvrables en fonction de l’ancienneté.</p>
<p>Indemnité de licenciement – en fonction de l’ancienneté : l’équivalent d’un, de deux ou de trois mois de rémunération.</p>
<p>L’employé(e) peut faire valoir ses droits à la retraite à partir de son 55ème anniversaire pour une femme et 60ème anniversaire pour un homme.</p>
<p>Depuis janvier 2007, le marché du travail polonais a été ouvert aux ressortissants de l’Union européenne, qui n’ont désormais plus besoin d’une autorisation du travail. Les conditions d’accès au marché de l’emploi sont celles prévues par la réglementation de l’Union européenne et l’organisme à contacter est l’Office régional du travail.</p>
<h3 class="spip"><a id="sommaire_2"></a>Fêtes légales</h3>
<p>Ier janvier : Nouvel An,</p>
<ul class="spip">
<li>Lundi de Pâques,</li>
<li>1er Mai : Fête du travail,</li>
<li>3 Mai : Fête Nationale,</li>
<li>Fête Dieu : fête mobile autour du 22 juin,</li>
<li>15 août : assomption,</li>
<li>1er Novembre : Toussaint,</li>
<li>11 Novembre : Fête de la libération,</li>
<li>25 et 26 décembre : Noël.</li></ul>
<p><strong>Voir aussi : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.tv5.org/TV5Site/voyageurs/accueil.php" class="spip_out" rel="external">TV5.org</a></p>
<h3 class="spip"><a id="sommaire_3"></a>Emploi du conjoint</h3>
<p>Les opportunités sont limitées. Elles existent parfois dans les universités ou des sociétés polonaises à capitaux mixtes. Quelques rares possibilités de cours de français à l’Institut de Varsovie et de Cracovie sont offertes (si l’intéressé est titulaire d’un diplôme d’enseignement).</p>
<p>Le taux de chômage est élevé en Pologne (12% en 2010). Les possibilités sont assez faibles et la connaissance de la langue polonaise est la plupart du temps nécessaire.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/emploi-stage/article/reglementation-du-travail-114281). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
