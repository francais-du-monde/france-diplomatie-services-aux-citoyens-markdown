# Demander la rectification d’un acte d’état civil

<p class="chapo">
      En application de <a href="https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006421496&amp;cidTexte=LEGITEXT000006070721" class="spip_out" rel="external">l’article 99 du code civil</a>, un acte d’état civil ne peut être rectifié que sur instructions du Procureur de la République territorialement compétent.
</p>
<p>Pour les actes établis par le Service central d’état civil ou par les ambassades et consulats français à l’étranger, les demandes de rectification doivent être adressées au Procureur de la République près le Tribunal de grande Instance de Nantes à l’adresse suivante :</p>
<p>Tribunal de grande Instance de Nantes<br class="manualbr">Service civil du Parquet<br class="manualbr">Quai François Mitterrand<br class="manualbr">44921 NANTES Cedex 9</p>
<p>La demande doit être accompagnée de :</p>
<ul class="spip">
<li>la copie de l’acte à rectifier</li>
<li>la copie du ou des document(s) apportant la preuve de l’erreur ou de l’omission, avec leur traduction pour les documents étrangers : actes d’état civil, jugement, etc. Toutefois, dans des conditions spécifiques, le Service central d’état civil peut procéder à la rectification administrative des erreurs et omissions purement matérielles contenues dans certains actes qu’il a établis.</li></ul>
<p>Il convient de souligner que les actes de naissance établis par le Service central d’état civil pour les personnes naturalisées mentionnent leur adresse à la date d’acquisition de la nationalité française. Les éventuels changements ultérieurs de résidence du titulaire de l’acte ne donnent pas lieu à modification de cette adresse.</p>
<p><i>Mise à jour : mars 2016</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/etat-civil/article/rectification-d-un-acte-d-etat). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
