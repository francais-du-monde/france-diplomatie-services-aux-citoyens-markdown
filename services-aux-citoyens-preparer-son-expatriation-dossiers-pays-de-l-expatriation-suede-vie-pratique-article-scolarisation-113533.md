# Scolarisation

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/vie-pratique/article/scolarisation-113533#sommaire_1">Les établissements scolaires français en Suède</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/vie-pratique/article/scolarisation-113533#sommaire_2">Enseignement supérieur</a></li></ul>
<p>Pour toute information sur la scolarisation dans le système français à l’étranger, vous pouvez consulter notre thématique sur les <a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures.md" class="spip_in">Etudes supérieures</a> et la <a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais.md" class="spip_in">Scolarité en français</a>.</p>
<p>Vous y trouverez des renseignements sur :</p>
<ul class="spip">
<li>les établissements français du primaire et du secondaire à l’étranger ;</li>
<li>les bourses scolaires et la prise en charge des frais de scolarité à l’étranger ;</li>
<li>les possibilités qui s’offrent à vous si votre enfant ne peut être scolarisé à l’étranger dans le système français (enseignement à distance par le CNED, programme français langue maternelle (FLAM), internats en France) ;</li>
<li>les épreuves du baccalauréat à l’étranger ;</li>
<li>les bourses d’études supérieures en France et à l’étranger ;</li>
<li>l’équivalence des diplômes.</li></ul>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.aefe.fr/" class="spip_out" rel="external">Agence pour l’enseignement français à l’étranger</a></p>
<h3 class="spip"><a id="sommaire_1"></a>Les établissements scolaires français en Suède</h3>
<ul class="spip">
<li><a href="http://www.scola.education.gouv.fr/" class="spip_out" rel="external">Outil de recherche</a> d’établissements scolaires français à l’étranger sur le site du ministère de l’Education ;</li>
<li><a href="http://www.education.gouv.fr/cid258/les-etablissements-scolaires-d-enseignement-francais-a-l-etranger.html" class="spip_out" rel="external">Les établissements scolaires d’enseignement français à l’étranger</a> ;</li>
<li><a href="http://www.aefe.fr/" class="spip_out" rel="external">Agence pour l’enseignement Français à l’étranger</a> ;</li>
<li><a href="http://www.franskaspraket.eu/" class="spip_out" rel="external">Langue et culture française en Suède</a> ;</li>
<li><a href="http://www.lfsl.net/" class="spip_out" rel="external">Lycée Français Saint Louis à Stockholm</a> ;</li>
<li><a href="http://www.franskaskolan.se/" class="spip_out" rel="external">Ecole Française Stockholm</a>.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Enseignement supérieur</h3>
<ul class="spip">
<li><a href="http://www.studyinsweden.se/" class="spip_out" rel="external">Site spécialisé pour les études en Suède</a> : renseignements sur les formations disponibles, les formalités d’inscription et la vie étudiante en Suède ;</li>
<li>Site suédois sur les <a href="http://studera.nu/" class="spip_out" rel="external">études supérieures en Suède</a> <br class="manualbr">Il est possible de suivre des études universitaires à condition d’avoir un niveau suffisant en suédois et en anglais. Chaque cursus aura des critères de sélection spécifiques.</li>
<li>Le <a href="https://www.antagning.se/se/start" class="spip_out" rel="external">portail d’inscription aux universités en Suède</a>, pour rechercher un établissement, un cursus, et postuler ;</li>
<li>Rubrique <a href="http://www.swedenabroad.com/fr-FR/Embassies/Paris/Etudier-en-Suede/Etudier-en-Suede/" class="spip_out" rel="external">Etudier en Suède</a>, sur le site de l’ambassade de Suède en France.</li></ul>
<p>La Suède compte environ une soixantaine d’universités et établissements d’enseignement supérieur.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/vie-pratique/article/scolarisation-113533). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
