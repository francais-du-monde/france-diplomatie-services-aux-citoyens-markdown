# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/vie-pratique/article/transports-114782#sommaire_1">Importation de véhicule</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/vie-pratique/article/transports-114782#sommaire_2">Permis de conduire</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/vie-pratique/article/transports-114782#sommaire_3">Code de la route</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/vie-pratique/article/transports-114782#sommaire_4">Assurances et taxes</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/vie-pratique/article/transports-114782#sommaire_5">Achat et location</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/vie-pratique/article/transports-114782#sommaire_6">Entretien</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/vie-pratique/article/transports-114782#sommaire_7">Réseau routier</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/vie-pratique/article/transports-114782#sommaire_8">Transports en commun</a></li></ul>
<p>En cas d’accident, il est conseillé d’appeler la police (113). En cas de panne ou d’incident mineur, il est préférable d’appeler l’A.M.Z.S. (Automobile Club de Slovénie) au 987.</p>
<p>Les policiers slovènes ne sont plus autorisés à percevoir les amendes dues à des infractions au code de la route, sauf dans le cas où le contrevenant "n’est pas résident en Slovénie". Si le contrevenant "de passage en Slovénie" n’est pas en mesure ou refuse de payer l’amende, le policier est autorisé à lui confisquer provisoirement son titre de voyage. Ce document, joint au procès-verbal, sera adressé au juge des infractions compétent qui décidera de la suite de la procédure.</p>
<h3 class="spip"><a id="sommaire_1"></a>Importation de véhicule</h3>
<p>Il est plus simple d’acheter son véhicule, neuf ou d’occasion, sur place.</p>
<p>Les citoyens français peuvent faire immatriculer leur véhicule en Slovénie à condition d’y avoir résidé au moins six mois.</p>
<p>L’immatriculation des véhicules peut être effectuée :</p>
<ul class="spip">
<li>auprès de l’unité administrative de votre lieu de résidence ;</li>
<li>auprès des organisations qui effectuent des contrôles techniques des véhicules ;</li>
<li>auprès de certaines sociétés et entreprises privées qui s’occupent de la vente des véhicules.</li></ul>
<p>Les véhicules importés en Slovénie sont tous soumis à l’obtention d’un certificat slovène d’homologation (délivré par le service des mines) + contrôle technique. L’importation d’un véhicule d’occasion nécessite obligatoirement le paiement d’une taxe (DMV – taxe pour véhicule motorisé) + taxe écologique. L’institution compétente pour les immatriculations, les contrôles techniques et l’émission du certificat de conformité du véhicule est :</p>
<ul class="spip">
<li><a href="http://www.jh-lj.si/lpp" class="spip_out" rel="external">JAVNO PODJETJE LJUBLJANSKI POTNISKI PROMET d.o.o</a> (Société publique de transport)</li>
<li>Celovska cesta 160</li>
<li>1000 LJUBLJANA</li>
<li><strong>Tel. :</strong> + 386 1 582 24 60</li>
<li><strong>Fax :</strong> + 386 1 582 25 50</li></ul>
<p><strong>Liens utiles : </strong></p>
<ul class="spip">
<li><a href="http://www.amzs.si/?jezik=1033" class="spip_out" rel="external">Avto-moto zveza Slovenije (AMZS)</a> - Fédération auto-moto de Slovénie <br class="manualbr">Dunajska 128 <br class="manualbr">1000 LJUBLJANA <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/vie-pratique/article/transports-114782#info.center#mc#amzs.si#" title="info.center..åt..amzs.si" onclick="location.href=mc_lancerlien('info.center','amzs.si'); return false;" class="spip_mail">Courriel</a></li>
<li><a href="http://www.durs.gov.si/" class="spip_out" rel="external">Ministère des Finances</a> - <strong>Administration fiscale de la République de Slovénie</strong><br class="manualbr">Smartinska cest 55, 1000 Ljubljana<br class="manualbr">Tel. : + 386 1 478 2700</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Permis de conduire</h3>
<p>Les ressortissants des États membres de l’Union européenne ne sont plus tenus d’échanger leur permis de conduire. Le permis français et le permis international sont reconnus</p>
<p>Les ressortissants étrangers qui résident en Slovénie depuis plus de six mois dans l’intention d’y installer leur résidence permanente ou d’y résider pour une durée supérieure à six mois peuvent, dans deux ans qui suivent leur arrivée en Slovénie, demander à échanger leur permis de conduire étranger en cours de validité contre un permis de conduire slovène. L’échange, étant facultatif, se fait à la mairie sur présentation du passeport, du permis de conduire en cours de validité, d’une photographie, et de15 euros pour les frais d’échange.</p>
<p>Depuis le 1er juillet 2008, <strong>une vignette est obligatoire pour circuler sur les autoroutes et voies rapides de Slovénie</strong> (y compris sur le périphérique de Ljubljana) pour tout véhicule dont la charge autorisée ne dépasse pas 3,5 tonnes, y compris les deux-roues. <strong>Cette vignette doit être collée sur le pare-brise.</strong> Les tarifs 2013-2014 pour une voiture se présentent comme suit : 15 € pour une vignette hebdomadaire, 30€ pour une vignette mensuelle, et 110 € pour une vignette annuelle, avec une validité allant du 1er décembre de l’année précédente jusqu’au 31 janvier de l’année suivante - soit une durée de 14 mois).</p>
<p><strong>Toute personne circulant sans vignette valide est systématiquement punie d’une amende pouvant varier de 300 à 800 €.</strong> Les véhicules identifiables comme camionnette ou minibus de moins de 3,5 tonnes font désormais l’objet d’une tarification distincte des autres véhicules.</p>
<p>En outre, la législation locale autorise les agents de la DARS (société gérant les autoroutes slovènes) à confisquer tous les documents des contrevenants (carte nationale d’identité, passeport, carte grise du véhicule, permis de conduire) jusqu’à ce que l’amende soit payée. Il est fortement conseillé aux voyageurs d’acheter la vignette <strong>avant</strong> de pénétrer sur le territoire slovène : en effet, la vignette est en vente dans les pays limitrophes, Italie, Croatie et Autriche, dans les stations-services des autoroutes.</p>
<p>En revanche, <strong>les chauffeurs de poids lourds, autobus et autres véhicules dépassant les 3,5 tonnes pourront continuer à utiliser les moyens actuels de paiement</strong> (en liquide, par carte bancaire ou la carte Transporter de la Direction des autoroutes de Slovénie), en empruntant les voies latérales des postes de péage.</p>
<h3 class="spip"><a id="sommaire_3"></a>Code de la route</h3>
<p>La conduite est à droite. La priorité est normalement à droite, sauf indication contraire. La vitesse est limitée à 50 km/h en agglomération, à 90 km/h hors agglomération et à 130 km/h sur autoroute.</p>
<p>Il est obligatoire de circuler avec les codes allumés durant la journée et de porter sa ceinture de sécurité aussi bien à l’avant qu’à l’arrière.</p>
<p>L’usage de pneus neige est obligatoire pendant la période hivernale (du 15 novembre au 15 mars).</p>
<p>Le passage au feu orange est passible d’une amende. Les excès de vitesse sont également sévèrement sanctionnés. Le taux d’alcoolémie ne doit pas dépasser 0,5 mg. Les conducteurs étrangers ayant commis une infraction au code de la route sont généralement présentés à un juge. Sur la route, il est conseillé de se montrer très vigilant : la conduite des automobilistes slovènes peut être dangereuse.</p>
<h3 class="spip"><a id="sommaire_4"></a>Assurances et taxes</h3>
<p>L’assurance tiers - responsabilité civile est obligatoire. Les primes sont 20% environ plus élevées qu’en France. Il est possible de s’assurer tous risques sur place. Un expatrié doit être affilié à une compagnie d’assurance locale pour être autorisé à conduire son véhicule.</p>
<p>Le 1er contrôle technique s’effectue à partir de quatre ans, puis tous les deux ans entre la 4ème et la 8ème année, puis un contrôle par an, au-delà.</p>
<p>Le contrôle technique et l’assurance au tiers sont nécessaires pour l’obtention de la carte grise.</p>
<p>Les véhicules importés en Slovénie sont tous soumis à l’obtention d’un certificat slovène d’homologation (délivré par le service des mines) + contrôle technique. L’importation d’un véhicule d’occasion nécessite obligatoirement le paiement d’une taxe (DMV – taxe pour véhicule motorisé) + taxe écologique.</p>
<h3 class="spip"><a id="sommaire_5"></a>Achat et location</h3>
<p>En règle générale, aucun type particulier de véhicule n’est indispensable pour circuler en Slovénie.</p>
<p>Toutes les marques françaises et les principales marques européennes, asiatiques et américaines sont disponibles sur place.</p>
<p>En Slovénie, les véhicules doivent répondre aux exigences suivantes :</p>
<ul class="spip">
<li>les voitures doivent être équipées d’appuie-tête sur tous les sièges pourvus de ceintures de sécurité (y compris les sièges arrière),</li>
<li>le système de contrôle des émissions de gaz d’échappement des véhicules doit être conforme,</li>
<li>les camions doivent être équipés de dispositifs de protection latérale et arrière. Les principaux groupes internationaux de location de voiture sont présents en Slovénie (guichets à l’aéroport).</li></ul>
<h3 class="spip"><a id="sommaire_6"></a>Entretien</h3>
<p>Une voiture peut être entretenue sur place dans de bonnes conditions mais à un coût plus élevé qu’en France (de 10 à 20% plus cher en moyenne). La présence des marques françaises sur place permet d’obtenir sans délai les pièces détachées neuves de leurs modèles.</p>
<h3 class="spip"><a id="sommaire_7"></a>Réseau routier</h3>
<p>Le réseau routier est en bon état. La Slovénie possède un réseau dense de liaisons autoroutières. Les routes nationales sont correctes mais peuvent être surchargées. Le réseau secondaire est moins performant. Il existe de nombreuses pistes mais la circulation en 4X4 est réglementée.</p>
<h3 class="spip"><a id="sommaire_8"></a>Transports en commun</h3>
<p>En ville, outre son véhicule personnel, il est possible d’utiliser le bus, les taxis individuels ou tout simplement de se déplacer à pied (Ljubljana intra-muros se traverse en un quart d’heure). Ljubljana dispose également d’un service de location de vélo de courte durée.</p>
<p>Il est possible de circuler dans le pays en autocar ou en train. Le réseau ferroviaire est satisfaisant mais les liaisons sont assez lentes. Compte tenu de la dimension réduite du pays, il n’existe pas de liaisons aériennes intérieures.</p>
<p>S’agissant du transport aérien, la Slovénie compte deux aéroports desservis toute l’année par des vols internationaux : <a href="http://www.lju-airport.si/" class="spip_out" rel="external">Ljubljana</a> et <a href="http://www.maribor-airport.si/" class="spip_out" rel="external">Maribor</a>.</p>
<p>Il n’y a pas de réseau aérien intérieur.</p>
<p>La compagnie aérienne nationale est <a href="http://www.adria.si/" class="spip_out" rel="external">Adria Airways</a>. Elle organise des vols vers la plupart des villes européennes.</p>
<p>Des vols quotidiens entre Paris et Ljubljana sont effectués par Air France et cinq jours par semaine par <a href="http://www.adria.si/" class="spip_out" rel="external">Adria Airways</a>. Il existe un service régulier d’autocar entre l’aéroport et Ljubljana. Le temps de parcours est de 45 minutes.</p>
<p><i>Mise à jour : septembre 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/vie-pratique/article/transports-114782). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
