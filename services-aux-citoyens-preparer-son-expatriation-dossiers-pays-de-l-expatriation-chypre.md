# Chypre

<p>Au <strong>31 décembre 2014, 1464 Français</strong> étaient enregistrés auprès de la section consulaire de l’Ambassade de France.</p>
<p>On estime à près de 1500 le nombre total de Français résidant à Chypre, y compris les personnes non enregistrées auprès de l’ambassade de France.</p>
<p>Il s’agit d’une communauté jeune ; la tranche d’âge la plus représentative est située entre 30 et 45 ans.</p>
<p>La communauté française de l’île est concentrée principalement à Nicosie, capitale administrative, mais aussi à Limassol, capitale économique où se trouvent les banques, les hôtels et les compagnies <i>offshore</i>. Paphos accueille surtout les touristes et Larnaca ne connaît pas d’activités particulières en dehors de celles du port et de l’aéroport.</p>
<p>En février 2002 a été créée l’association d’affaires franco-chypriote, regroupant environ 80 adhérents. Il existe également des associations professionnelles par secteur (informatique, alimentaire, etc.) et par type d’activité (importateur-exportateur, etc.).</p>
<p>Les investissements directs français à Chypre se concentrent de plus en plus dans les secteurs de l’immobilier et des services aux entreprises.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/dossiers-pays/chypre/" class="spip_in">Une description de Chypre, de sa situation politique et économique</a></li>
<li><a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/chypre/" class="spip_in">Des informations actualisées sur les conditions locales de sécurité à Chypre</a></li>
<li><a href="http://www.ambafrance-cy.org/" class="spip_out" rel="external">Ambassade de France à Chypre</a></li></ul>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chypre-entree-et-sejour-22980.md">Entrée et séjour</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chypre-entree-et-sejour-22980-article-passeport-visa-permis-de-travail-111063.md">Passeport, visa, permis de travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chypre-entree-et-sejour-22980-article-demenagement-111064.md">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chypre-entree-et-sejour-22980-article-vaccination-111065.md">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chypre-entree-et-sejour-22980-article-animaux-domestiques-111066.md">Animaux domestiques</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chypre-emploi-stage.md">Emploi, stage</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chypre-emploi-stage-article-marche-du-travail-111067.md">Marché du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chypre-emploi-stage-article-reglementation-du-travail-111068.md">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chypre-emploi-stage-article-recherche-d-emploi.md">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chypre-emploi-stage-article-curriculum-vitae-111070.md">Curriculum vitae</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chypre-emploi-stage-article-stages.md">Stages</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chypre-protection-sociale-22982.md">Protection sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chypre-protection-sociale-22982-article-regime-local-de-securite-sociale-111075.md">Régime local de sécurité sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chypre-protection-sociale-22982-article-convention-de-securite-sociale-111076.md">Convention de sécurité sociale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chypre-fiscalite-22983.md">Fiscalité</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chypre-fiscalite-22983-article-fiscalite-du-pays-111077.md">Fiscalité du pays</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chypre-fiscalite-22983-article-convention-fiscale-111078.md">Convention fiscale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chypre-vie-pratique.md">Vie pratique</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chypre-vie-pratique-article-logement-111079.md">Logement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chypre-vie-pratique-article-sante.md">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chypre-vie-pratique-article-scolarisation.md">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chypre-vie-pratique-article-cout-de-la-vie-111082.md">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chypre-vie-pratique-article-transports.md">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chypre-vie-pratique-article-communications.md">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chypre-vie-pratique-article-loisirs-et-culture.md">Loisirs et culture</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chypre-presence-francaise.md">Présence française</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chypre-presence-francaise-article-presence-francaise.md">Présence française</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
