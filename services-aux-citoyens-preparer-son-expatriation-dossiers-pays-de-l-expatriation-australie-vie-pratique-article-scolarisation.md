# Scolarisation

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/vie-pratique/article/scolarisation#sommaire_1">Les établissements scolaires français en Australie</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/vie-pratique/article/scolarisation#sommaire_2">Crèches</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/vie-pratique/article/scolarisation#sommaire_3">Enseignement supérieur et recherche (doctorats) </a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Les établissements scolaires français en Australie</h3>
<p>Pour toute information sur la scolarisation dans le système français à l’étranger, vous pouvez consulter notre <a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais.md" class="spip_in">thématique</a> sur les études et la scolarisation.</p>
<p>Vous y trouverez des renseignements sur :</p>
<ul class="spip">
<li>Les établissements français du primaire et du secondaire à l’étranger ;</li>
<li>Les bourses scolaires et la prise en charge des frais de scolarité à l’étranger ;</li>
<li>Les possibilités qui s’offrent à vous si votre enfant ne peut être scolarisé à l’étranger dans le système français (enseignement à distance par le CNED, programme français langue maternelle (FLAM), internats en France) ;</li>
<li>Les épreuves du baccalauréat à l’étranger ;</li>
<li>Les bourses d’études supérieures en France et à l’étranger ;</li>
<li>L’équivalence des diplômes.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Crèches</h3>
<p>Tout comme dans certaines grandes villes françaises, la garde des tout petits est un vrai casse-tête pour les parents car l’offre de garde est nettement inférieure aux besoins. Il est fortement conseillé d’inscrire sur liste d’attente de plusieurs <i>child care centres</i> le nouveau-né dès sa conception. Les listes d’attentes sont nombreuses et très longues. Voici quelques liens très pratiques pour la recherche de différents moyens de garde et d’activités pour les plus jeunes :</p>
<ul class="spip">
<li><a href="https://commsatwork.org/" class="spip_out" rel="external">https://commsatwork.org/</a></li>
<li><a href="http://www.mychild.gov.au/" class="spip_out" rel="external">http://www.mychild.gov.au/</a></li>
<li><a href="http://www.careforkids.com.au/" class="spip_out" rel="external">http://www.careforkids.com.au/</a></li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Enseignement supérieur et recherche (doctorats) </h3>
<p>Les universités australiennes dispensent un enseignement de qualité et jouissent d’une bonne réputation. Elles attirent chaque année de plus en plus d’étudiants étrangers, non seulement asiatiques mais également européens et bien entendu français. Toutes les possibilités sont offertes à condition de posséder les diplômes requis et une parfaite connaissance et pratique de la langue anglaise. Bien que le coût de l’inscription pour les étudiants étrangers soit moins élevé qu’aux</p>
<p>Etats-Unis ou au Royaume-Uni, il demeure conséquent. Le coût des études varie selon le domaine et le niveau d’étude, l’établissement et la filière. Il se situe entre 12 000 et 35 000 dollars australiens par an (9 000 et 27 000 euros). Le montant pour les étudiants ayant le statut de résident permanent est le même que celui demandé aux étudiants australiens : suivant la filière d’études, 5000 à 15000 AUD par an. Il est toutefois très difficile d’obtenir un visa de résident permanent. Pour obtenir des informations sur les conditions d’obtention, voir le site de <a href="http://www.france.embassy.gov.au/parifrancais/Visas_and_Migration.html" class="spip_out" rel="external">l’ambassade d’Australie en France</a> ; L’enseignement est planifié sur deux semestres : le premier semestre commence en février et le deuxième en août. Deux périodes de vacances : les vacances d’hiver juillet-mi-août (6 semaines) et les vacances d’été de décembre-janvier (6 à 8 semaines) suivant le calendrier saisonnier de l’hémisphère sud.</p>
<p><strong>Universités (<i>higher education ou tertiary education</i>) </strong></p>
<p>L’Australie compte 39 universités (<i>Full universities</i>) (générales et technologiques) et cinq établissements d’enseignement supérieur ayant le droit d’adjoindre le titre « university » dans leur nom (Torrens University Australia qui ouvrira en janvier 2014 ; Carnegie Mellon University, University College London, MCD University of Divinity and Heriot-Watt University).</p>
<p>Les 39 <i>Full Universities</i> australiennes proposent un cursus LMD :</p>
<ul class="spip">
<li>Licence (<i>Bachelor’s degree</i>) en trois ans avec la possibilité de poursuivre une année supplémentaire pour obtenir un <i>Bachelor’s degree with honours</i>,</li>
<li>Master (<i>Master’s degree</i>) en deux ans</li>
<li>Doctorat (<i>Doctoral degree</i>) en trois ans</li></ul>
<p>Huit universités dites d’excellence (formation et recherche) sont regroupées au sein du <a href="http://www.go8.edu.au/" class="spip_out" rel="external">Group of Eight</a> (Go8) :</p>
<ul class="spip">
<li><a href="http://www.anu.edu.au/" class="spip_out" rel="external">The Australian National University</a> (ANU) (Canberra, Territoire de la capitale australienne)</li>
<li><a href="http://www.unimelb.edu.au/" class="spip_out" rel="external">The University of Melbourne</a> (Melbourne, état du Victoria)</li>
<li><a href="http://www.sydney.edu.au/" class="spip_out" rel="external">The University of Sydney</a> (Sydney, Nouvelle-Galles du Sud)</li>
<li><a href="http://www.unsw.edu.au/" class="spip_out" rel="external">The University of New South Wales</a> (Sydney, Nouvelle-Galles du Sud)</li>
<li><a href="http://www.monash.edu.au/" class="spip_out" rel="external">Monash University</a> (Melbourne, état du Victoria)</li>
<li><a href="http://www.uq.edu.au/" class="spip_out" rel="external">The University of Queensland</a> (Brisbane, Queensland)</li>
<li><a href="http://www.adelaide.edu.au/" class="spip_out" rel="external">The University of Adelaide</a> (Adélaide, Australie Méridionale)</li>
<li><a href="http://www.uwa.edu.au/" class="spip_out" rel="external">The University of Western Australia</a> (Perth, Australie Occidentale)</li></ul>
<p>Cinq universités font partie du réseau des grandes universités de technologie : <a href="http://www.atn.edu.au/" class="spip_out" rel="external">Australian Technology Network</a>, (ATN ) :</p>
<ul class="spip">
<li><a href="http://www.curtin.edu.au/" class="spip_out" rel="external">Curtin University</a> (Perth, Australie Occidentale)</li>
<li><a href="http://www.unisa.edu.au/" class="spip_out" rel="external">University of South Australia</a> (Adélaide, Australie Méridionale)</li>
<li><a href="http://www.rmit.edu.au/" class="spip_out" rel="external">RMIT University</a> (Melbourne, état du Victoria)</li>
<li><a href="http://www.uts.edu.au/" class="spip_out" rel="external">University of Technology Sydney</a> (Sydney, Nouvelle-Galles du Sud)</li>
<li><a href="http://www.qut.edu.au/" class="spip_out" rel="external">Queensland University of Technology</a> (Brisbane, Queensland)</li></ul>
<p>Pour connaître l’ensemble des universités, vous pouvez consulter le site Internet <a href="http://www.universitiesaustralia.edu.au/" class="spip_out" rel="external">Australian Universities</a>.</p>
<p><strong>Reconnaissance des diplômes</strong></p>
<p>Voir <a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-australie-emploi-stage-article-curriculum-vitae.md" class="spip_in">l’article Curriculum Vitae</a>.</p>
<p><strong>Conditions d’admission</strong> (<i>eligibility</i>)</p>
<p><strong> <i>Undergraduate (bachelor level)</i> </strong><br class="manualbr">La plupart des universités australiennes exigent de bons résultats au baccalauréat français : la moyenne obtenue est convertie en points selon le <a href="http://www.uac.edu.au/undergraduate/atar/" class="spip_out" rel="external">système ATAR</a>.</p>
<p>Les deux certificats de niveau d’anglais les plus fréquemment exigés sont le <a href="http://www.ets.org/" class="spip_out" rel="external">TOEFL</a> et <a href="http://www.ielts.org/" class="spip_out" rel="external">l’IELTS</a>. Il est recommandé de bien s’informer auprès des universités australiennes pour connaître le certificat qu’elles demandent.</p>
<p>Les critères d’admission généraux et s’appliquant à tous les étudiants étrangers (<i>international students</i>) :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  avoir accompli 12 années de scolarisation (<i>12 years of schooling</i> <i>from year 1</i> –équivalente du cours préparatoire, CP, de l’école primaire- <i>to year 12</i> – équivalente de la classe de Terminale) ;</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  avoir effectué un test de connaissances thématiques (<i>pre-requisite subjects</i>). Ce test n’est pas systématiquement exigé. Il dépend de la matière et du niveau d’études. </p>
<p>Certaines filières comme les arts et le design exigent la présentation d’un portfolio des travaux déjà réalisés.</p>
<p><strong>Postgraduate</strong> (<i>master’s  doctoral level</i>)<br class="manualbr">Pour entrer dans une université australienne au niveau du Master, la concurrence est forte. L’obtention d’un précédent bon diplôme (<i>first degree</i>) auprès d’une université de premier plan (française, australienne ou d’un autre pays) ou d’une grande école (française) est essentielle.</p>
<p>Quant aux diplômes de doctorat (PhD) et de 3ème cycle (<i>postgraduate</i>), les étudiants doivent posséder un master reconnu dans la matière correspondante. Chaque candidature fait l’objet d’une stricte sélection individuelle.</p>
<p><strong>Echanges universitaires</strong></p>
<p>En France, vous pouvez vous renseigner auprès du département des relations internationales de votre université sur les accords d’échanges universitaires existant entre la France et l’Australie et consulter le site de <a href="http://www.francaustralia.com/" class="spip_out" rel="external">Francaustralia Education</a> qui est une agence de conseils et d’orientation des études en Australie.</p>
<p>Vous pouvez également regarder sur les sites web des universités australiennes (sections <i>Study Abroad</i> ou <i>International Students)</i> quels sont leurs établissements d’enseignement supérieur français et partenaires.</p>
<p><strong>Les bourses d’études</strong></p>
<p>De façon générale, les universités australiennes ou le gouvernement ne possèdent pas de système de bourses ou de programmes de financement sauf pour les étudiants investis dans la recherche scientifique et donc inscrits dans un cursus de 3ème cycle (<i>postgraduate</i>). Le gouvernement accorde des prix universitaires du mérite aux étudiants étrangers issus de la zone Asie-Pacifique et de l’ensemble du globe. A titre d’exemple, on peut citer la bourse appelée <i>"</i> <i>International Postgraduate Research Scholarships </i>(IPRS) <i style="mso-bidi-font-style: normal">"</i>.</p>
<p><strong>Pour en savoir plus</strong> : le site Internet du ministère australien de l’Education, de l’emploi et des relations au travail (<i>Department of Education, Employment and Workplace Relations)</i></p>
<p>Le <i>Postgraduate Scholarship Database for Australia</i> en utilisant le moteur de recherche suivant : <a href="http://www.gooduniversitiesguide.com.au/scholarship/search#.VmhQBdmwTbk" class="spip_out" rel="external">http://www.gooduniversitiesguide.com.au/scholarship/search#.VmhQBdmwTbk</a>. Parmi les bourses répertoriées sur cette base de données, certaines peuvent être demandées par des étudiants français.</p>
<p>Le visa étudiant vous permet également d’exercer un emploi à condition de respecter un quota hebdomadaire d’heures, 40 heures par quinzaine. Pour plus d’information, veuillez consulter le site internet du <a href="http://www.immi.gov.au/students/students/working_while_studying/" class="spip_out" rel="external">ministère australien de l’Immigration</a>.</p>
<p><strong>Protection sociale</strong></p>
<p>En l’absence de convention de sécurité sociale entre la France et l’Australie, les étudiants français sont tenus de contracter, avant leur entrée sur le territoire australien, une couverture sociale appelée <i><a href="https://www.oshcworldcare.com.au/" class="spip_out" rel="external">Overseas Student Health Cover</a> </i>ou OSHC. Elle prend en charge les soins de santé et d’hospitalisation. Vous pouvez souscrire cette assurance auprès de l’assureur santé australien <a href="http://www.medibank.com.au/" class="spip_out" rel="external">Medibank Private</a> ou adhérer à la <a href="http://www.cfe.fr/" class="spip_out" rel="external">Caisse des Français de l’étranger</a>.</p>
<p><strong>Pour en savoir plus</strong></p>
<ul class="spip">
<li><a href="http://www.studyinaustralia.gov.au/" class="spip_out" rel="external">http://www.studyinaustralia.gov.au/</a></li>
<li>Consultez notre rubrique thématique sur <a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures.md" class="spip_in">les études</a> et la <a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais.md" class="spip_in">scolarisation à l’étranger</a></li></ul>
<p><i>Mise à jour : octobre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/vie-pratique/article/scolarisation). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
