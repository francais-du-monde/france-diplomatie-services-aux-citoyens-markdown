# Convention fiscale

<p>Une convention internationale ayant primauté sur la loi interne, les dispositions de la loi du 29 décembre 1976 modifiant les règles de territorialité et les conditions d’imposition des Français de l’étranger (Journal Officiel du 30 décembre 1976) ne sont applicables que si elles ne contreviennent pas aux dispositions de la convention.</p>
<p>Une convention fiscale conclue entre la France et la Suisse le 9 septembre 1966, publiée après ratification au Journal Officiel du 10 octobre 1967, suivie d’un avenant du 3 décembre 1969 (Journal Officiel du 3 novembre 1970), établit un partage d’imposition sur le revenu de la part de chaque partie contractante.</p>
<p>Le texte de la convention et de ses avenants peut être obtenu à la direction des Journaux Officiels, par courrier (26 rue Desaix - 75727 PARIS Cedex 15), par fax (01 40 58 77 80), par minitel 36 16 JOURNAL OFFICIEL.</p>
<p>Ses dispositions concernant un Français salarié en Suisse sont les suivantes :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  les salaires, traitements et autres rémunérations similaires qu’un résident d’un Etat contractant reçoit au titre d’un emploi salarié ne sont imposables que dans cet Etat, à moins que l’emploi ne soit exercé dans l’autre Etat contractant. Si l’emploi est exercé dans l’autre Etat contractant, les rémunérations reçues à ce titre sont imposables dans cet autre Etat.</p>
<p>Nonobstant les dispositions du paragraphe ci-dessus, les rémunérations qu’un résident d’un Etat contractant reçoit au titre d’un emploi salarié exercé dans l’autre Etat contractant ne sont imposables que dans le premier Etat si :</p>
<ul class="spip">
<li>le bénéficiaire séjourne dans l’autre Etat pendant une période ou des périodes n’excédant pas au total 183 jours au cours de l’année fiscale considérée ;</li>
<li>les rémunérations sont payées par un employeur ou au nom d’un employeur qui n’est pas résident de l’autre Etat ;</li>
<li>la charge des rémunérations n’est pas supportée par un établissement stable ou une base fixe que l’employeur a dans l’autre Etat.</li></ul>
<p><i>Mise à jour : février 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/fiscalite/article/convention-fiscale). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
