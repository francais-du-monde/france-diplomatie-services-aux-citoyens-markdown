# Santé

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/vie-pratique/article/sante#sommaire_1">Mesures sanitaires de base</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/vie-pratique/article/sante#sommaire_2">Vaccinations</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/vie-pratique/article/sante#sommaire_3">Grippe aviaire</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/vie-pratique/article/sante#sommaire_4">Chikungunya</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/vie-pratique/article/sante#sommaire_5">Paludisme</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/vie-pratique/article/sante#sommaire_6">Dengue</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/vie-pratique/article/sante#sommaire_7">Sida</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/vie-pratique/article/sante#sommaire_8">Troubles respiratoires</a></li></ul>
<p>Les conditions d’hygiène en Inde sont très nettement inférieures à celles que nous connaissons en France. De très bons médecins exercent malgré tout localement : ces derniers ont pour la plupart, obtenu leur diplôme à l’étranger (notamment Etats-Unis, Grande-Bretagne).</p>
<p>A New Delhi, plusieurs grands hôpitaux ont prouvé leur efficacité : le Max Hospitals, l’Apollo Hospitals ou encore le Fortis.</p>
<p>A Pondichéry, certains soins/examens peuvent nécessiter un déplacement à Chennai (3h de route). L’Hôpital P.I.M.S. (Pondicherry Institute of Medecine and Science), récent et de bon niveau, offre néanmoins de nombreuses possibilités de soins sur place.</p>
<p>A Bombay, le Breach Candy Hospital, le Hinduja Hospital et le Lilavati Hospital (privés) sont de bon niveau. Le coût des soins y sont très élevés.</p>
<p>En général, les coûts de soins sont sensiblement inférieurs aux prix pratiqués en France. Les médicaments génériques sont bon marché et il est possible de souscrire à des assurances privées qui remboursent la quasi-totalité des soins.</p>
<p>Un séjour en Inde implique impérativement pour tout voyageur de prendre diverses précautions de santé. La rubrique ci-dessous mentionne les indications essentielles. Toutefois, ces indications ne sauraient dispenser d’une consultation avant le départ auprès de son médecin traitant et/ou auprès d’un centre hospitalier spécialisé dans la médecine des voyages. Il est préférable de prévoir cette consultation assez longtemps avant la date de départ, notamment pour permettre les rappels de vaccins si besoin est. Autre point important : il est nécessaire de contracter en France une assurance couvrant les frais médicaux et ceux de rapatriement sanitaire.</p>
<p>Pour une présentation des risques épidémiologiques ainsi que des conseils sur la médecine au quotidien, consultez les pages suivantes :</p>
<ul class="spip">
<li><a href="http://www.ambafrance-in.org/spip.php" class="spip_out" rel="external">Conseils, liste de médecins et hôpitaux sur les sites des consulats de France en Inde</a> ;</li>
<li><a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/inde/" class="spip_in">Page dédiée à la santé en Inde</a> sur le site Conseils aux voyageurs ;</li>
<li>Fiche Bombay, Calcutta, New Delhi, Pondichéry sur le site du CIMED.</li></ul>
<p>Vous trouverez ci-après quelques informations sur le contexte sanitaire.</p>
<h3 class="spip"><a id="sommaire_1"></a>Mesures sanitaires de base</h3>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Se laver très fréquemment les mains, et avant chaque repas (il existe en pharmacie des solutions antimicrobiennes à utiliser sans rinçage, utiles en cas d’excursions).</p>
<ul class="spip">
<li>Veiller au maintien d’une hygiène corporelle parfaite et au soin attentif des plaies cutanées.</li>
<li>Ne jamais consommer l’eau du robinet, qui n’est pas potable, dans la plupart des cas. Il faut préférer de l’eau en bouteille et, en cas de besoin, ne boire que de l’eau bouillie 30 minutes puis filtrée. Evitez de boire des boissons naturelles type jus de fruits pressés.</li>
<li>Ne jamais consommer de crèmes glacées ou de <i>lassi</i> (boisson à base de lait) ou de Lime Soda (soda citronné) achetés dans la rue.</li>
<li>Viandes et poissons/crustacés doivent être bien cuits avant consommation (poissons et crustacés à proscrire dans la région de Delhi entre fin mars et début octobre à cause de la forte chaleur et des risques de rupture de la chaîne du froid).</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Vaccinations</h3>
<p>Aucune vaccination n’est obligatoire (sauf pour les voyageurs en provenance d’Afrique, d’Amérique latine et de Papouasie-Nouvelle-Guinée qui doivent présenter un certificat de vaccination contre la fièvre jaune). Il est cependant préférable de mettre à jour les vaccinations suivantes : diphtérie-tétanos-poliomyélite, fièvre typhoïde, hépatites virales A et B. La vaccination anti-méningococcique peut également être recommandée dans certains cas.</p>
<p>A la suite de la découverte de cas de méningite bactérienne à New Delhi, l’attention des voyageurs est attirée sur l’opportunité d’avoir une vaccination à jour. Les piscines publiques et les zones les plus densément peuplées sont plus particulièrement à éviter. En cas de découverte de symptômes de la maladie, un traitement antibiotique est efficace à condition d’être entrepris à très bref délai.</p>
<p>Pour des séjours en zone rurale, une vaccination contre l’encéphalite japonaise peut être nécessaire (à pratiquer dans un centre médical d’une grande ville en Inde, le vaccin étant introuvable ailleurs).</p>
<h3 class="spip"><a id="sommaire_3"></a>Grippe aviaire</h3>
<p>L’Inde fait partie, depuis février 2006, des pays touchés par l’épizootie de grippe aviaire. Toutefois, seuls des cas d’infection sur des volailles ont été détectés : aucun cas humain de grippe aviaire n’a été relevé. S’il n’existe donc, pour l’heure, aucune raison de différer tout déplacement dans ce pays, il est néanmoins recommandé aux voyageurs :</p>
<ul class="spip">
<li>de se laver régulièrement les mains avec de l’eau et du savon ou une solution hydro-alcoolique (à acheter en pharmacie) qu’il est conseillé d’emporter dans ses bagages ;</li>
<li>de ne pas consommer de volaille ou de produits à base d’œufs insuffisamment cuits ;</li>
<li>d’éviter tout contact avec des volailles vivantes ou mortes (élevages, marchés) et avec des surfaces souillées par des fientes ou des déjections animales. Pour de plus amples renseignements, consulter le site spécial mis en place par le ministère de la Santé : <a href="http://www.grippeaviaire.gouv.fr/" class="spip_out" rel="external">Grippeaviaire.gouv.fr</a></li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Chikungunya</h3>
<p>L’OMS a confirmé la présence du chikungunya dans plusieurs régions de l’Inde (Kerala, Goa, Andhra Pradesh, Gujarat, Karnataka, Maharashtra, Orissa, Territoire de Pondichéry). Cette maladie virale, qui sévit épisodiquement en Inde, se transmet par les piqures de moustiques et impose le recours à des mesures de protection individuelles (sprays, crèmes, diffuseurs électriques, moustiquaires…), nécessaires également pour la prévention du paludisme et de la dengue (cf. ci-dessous).</p>
<p>Des informations complètes et actualisées sont consultables sur le <a href="http://www.sante.gouv.fr/" class="spip_out" rel="external">site du ministère de la Santé</a> et sur celui de l’<a href="http://www.invs.sante.fr/" class="spip_out" rel="external">Institut national de veille sanitaire</a>.</p>
<h3 class="spip"><a id="sommaire_5"></a>Paludisme</h3>
<p>Le paludisme (ou malaria) est une maladie parasitaire transmise par les piqûres de moustiques. Ses conséquences peuvent être très sérieuses.</p>
<p>Il convient de noter que le risque est faible dans les grandes villes (Delhi, Bombay), mais il ne peut jamais être considéré comme nul, surtout durant les périodes de mousson (juin à septembre), propices à la recrudescence des moustiques. Le risque est plus fort hors des grandes villes. Classification de l’Inde par l’Organisation Mondiale de la Santé : zone 2 (Etat d’Assam : zone 3).</p>
<p>Il existe deux formes de prévention du paludisme : la protection contre les moustiques et le traitement médicamenteux. Les mesures classiques de protection contre les moustiques durant la soirée et la nuit sont fortement recommandées (sprays, crèmes, diffuseurs électriques, moustiquaires…). Pour ce qui est du traitement médicamenteux, il convient de s’adresser avant le départ à son médecin traitant ou à un centre hospitalier spécialisé dans la médecine des voyages. Le traitement devra être poursuivi après le retour en France durant une durée variable selon le produit utilisé.</p>
<h3 class="spip"><a id="sommaire_6"></a>Dengue</h3>
<p>La dengue est une maladie propagée par les moustiques, y compris durant la journée. Elle peut revêtir plusieurs formes, dont une forme hémorragique de pronostic grave (très rare en Inde). Elle se manifeste par un syndrome grippal, avec douleurs musculaires et articulaires généralisées, et parfois une éruption cutanée. La prise d’aspirine est déconseillée.</p>
<p>En liaison avec la période de fin de mousson, on constate une résurgence des cas de dengue dans différentes régions. Il convient de façon générale de respecter les mesures élémentaires de protection contre les moustiques (vêtements longs, utilisation de produits répulsifs, recours aux moustiquaires, etc.). Il n’existe pas de traitement médicamenteux préventif contre la dengue.</p>
<h3 class="spip"><a id="sommaire_7"></a>Sida</h3>
<p>Les cas de personnes atteintes du VIH/SIDA, bien que difficiles à chiffrer (plus de cinq millions de séro-positifs selon les estimations de l’agence gouvernementale indienne NACO), sont nombreux mais l’épidémie commence à être contenue. Des mesures de protection individuelles s’imposent.</p>
<h3 class="spip"><a id="sommaire_8"></a>Troubles respiratoires</h3>
<p>Il est recommandé aux personnes se rendant dans les zones montagneuses du nord de l’Inde (exemple : Ladakh) de faire preuve d’une grande prudence en cas d’apparition de troubles respiratoires. Une préparation physique préalable et adaptée est conseillée aux personnes souhaitant entreprendre une randonnée en altitude. En cas de trouble respiratoire, il convient de redescendre dans les zones de plus faible altitude et dans tous les cas de ne pas poursuivre l’ascension.</p>
<p>En raison de la pollution dans les grandes villes, la prudence est recommandée pour les personnes ayant des antécédents en matière de troubles respiratoires.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/vie-pratique/article/sante). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
