# Recherche d’emploi

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/emploi-stage/article/recherche-d-emploi-113522#sommaire_1">Outils pour la recherche d’emploi</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/emploi-stage/article/recherche-d-emploi-113522#sommaire_2">Annuaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/emploi-stage/article/recherche-d-emploi-113522#sommaire_3">Organismes pour la recherche d’emploi</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Outils pour la recherche d’emploi</h3>
<p><strong>Journaux</strong></p>
<p>Les journaux sont une source importante d’annonces d’emplois vacants. <i>Dagens Nyheter</i> est le plus grand quotidien du matin en Suède et la plupart des emplois vacants figurent dans ses annonces. <i>Svenska Dagbladet</i> et<i> Dagens Industri </i>sont des journaux d’affaires. Les journaux locaux et régionaux publient également des offres d’emplois. Ces dernières paraissent surtout le jeudi et le dimanche.</p>
<p><strong>Sites internet</strong></p>
<p>Les principaux sites Internet relatifs à l’emploi local :</p>
<ul class="spip">
<li><a href="http://www.dn.se/" class="spip_out" rel="external">Dn.se</a> : site du journal Dagens Nyheter, uniquement en suédois</li>
<li><a href="http://www.svd.se/" class="spip_out" rel="external">Svd.se</a> : site du Journal Svenska dagblaget, uniquement en suédois</li>
<li><a href="http://www.di.se/" class="spip_out" rel="external">Di.se</a> : site du Journal Dagens Industri, uniquement en suédois</li>
<li><a href="http://www.computersweden.idg.se/" class="spip_out" rel="external">Computersweden.idg.se</a> : site Computer Sweden, uniquement en suédois</li>
<li><a href="http://www.stepstone.se/" class="spip_out" rel="external">Stepstone.se</a> : uniquement en suédois</li>
<li><a href="http://www.monster.se/" class="spip_out" rel="external">Monster.se</a> : uniquement en suédois</li>
<li><a href="http://www.jobbet.se/" class="spip_out" rel="external">Jobbet.se</a> : uniquement en suédois</li></ul>
<p><i>Le signalement d’organismes sur ce site ne saurait engager la responsabilité de la MFE quant à la qualité de leurs services.</i></p>
<h3 class="spip"><a id="sommaire_2"></a>Annuaires</h3>
<p><strong>Les cabinets de recrutement</strong></p>
<p>Il en existe une dizaine à Stockholm. Aucun n’est spécialisé dans la recherche de personnels français. Ils s’adressent principalement aux cadres supérieurs et intermédiaires. Certains établissent leurs propres répertoires, d’autres acceptent d’être contactés directement par d’éventuels candidats aux emplois.</p>
<p>Le réseau <a href="http://ec.europa.eu/eures/" class="spip_out" rel="external">EURES</a> fournit des détails sur les emplois vacants dans toute l’UE. Il résulte d’un partenariat entre tous les services publics de l’emploi des pays de l’UE et de l’EEE et s’efforce de faciliter la libre circulation des travailleurs.</p>
<h3 class="spip"><a id="sommaire_3"></a>Organismes pour la recherche d’emploi</h3>
<p><strong>Le service emploi de la chambre de commerce française en Suède (CCFS)</strong></p>
<p>Créée en mai 2006, l’Agence pour l’emploi (APE) de la CCFS a pour mission d’aider et d’orienter les Français établis en Suède dans leurs recherches d’emploi et de faciliter le recrutement par les entreprises de personnel au profil international et, notamment, francophone. Le service emploi de la CCFS a pour mission de mettre en relation des entreprises ayant des besoins en recrutement sur le marché suédois, avec des demandeurs d’emploi et/ou de stages, notamment français et francophones.</p>
<p>Son rôle est de faciliter le recrutement par les entreprises de personnel au profil international. Chaque année, ce service, subventionné par le ministère des Affaires étrangères français, place un nombre croissant de candidats. Cette activité permet de répondre aux besoins de recrutement des entreprises et des candidats.</p>
<p>Les informations relatives à l’inscription auprès du service emploi figurent sur le site de la CCFS, rubrique « Service emploi/espace candidats ».</p>
<p>L’Agence pour l’emploi vous offre les services suivants :</p>
<ul class="spip">
<li>entretien téléphonique avec une personne de l’APE pour déterminer vos objectifs et vous conseiller ;</li>
<li>diffusion d’offres d’emploi ;</li>
<li>diffusion de curriculum vitae aux entreprises qui contactent l’APE pour un besoin en recrutement ;</li>
<li>mise à disposition de l’annuaire des entreprises membres (environ 200) de la CCFS et de la liste des filiales françaises en Suède (service payant) ;</li>
<li>adhésion à la French Junior Entreprise (payante).</li></ul>
<p><strong>L’Agence pour l’emploi peut également traiter les offres de stages en entreprise (très peu nombreuses en Suède). En revanche elle n’est pas compétente pour des emplois d’été.</strong></p>
<p>Pour en savoir plus, vous pouvez contacter :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Mme Vérène Issautier<br class="manualbr"><a href="http://www.ccfs.se/" class="spip_out" rel="external">Chambre de commerce française en Suède</a><br class="manualbr">Service Emploi<br class="manualbr">Grev Turegatan 10 E - 11446 Stockholm <br class="manualbr">Téléphone : [46] (0) 8 442 54 41 ou 44 <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/emploi-stage/article/recherche-d-emploi-113522#emploi#mc#ccfs.se#" title="emploi..åt..ccfs.se" onclick="location.href=mc_lancerlien('emploi','ccfs.se'); return false;" class="spip_mail">Courriel</a></p>
<p><strong>Agence locale pour l’emploi</strong></p>
<p>Les agences nationales pour l’emploi (<i>arbetsförmedlingar</i>) jouent un rôle actif sur le marché suédois du travail et sont au service des demandeurs d’emploi comme des employeurs. Elles fournissent également des informations et d’autres services aux chômeurs (stages et conseils, par exemple). Il existe des agences pour l’emploi spécialisées pour certaines catégories professionnelles, notamment dans les secteurs des arts, de la comptabilité et de la finance, de la technologie et de l’informatique.</p>
<p>Les agences pour l’emploi suédoises publient deux revues : <a href="http://www.platsjournalen.se/" class="spip_out" rel="external">Platsjournalen</a>, en suédois uniquement et <i>Nytt Jobb</i> (plus spécialisée) qui répertorient les offres d’emplois et donnent d’autres renseignements utiles. Les demandeurs d’emploi peuvent s’abonner à ces magazines.</p>
<p>Les offres et demandes d’emplois sont publiées sur Internet. Les demandeurs d’emploi peuvent enregistrer leur CV dans une base de données. Les emplois vacants inscrits dans les agences suédoises pour l’emploi figurent également sur Internet.</p>
<p>Il n’est pas nécessaire d’être inscrit auprès d’une agence pour l’emploi suédoise pour obtenir des informations sur les emplois vacants ou sur d’autres sujets. L’inscription est cependant nécessaire pour pouvoir bénéficier des allocations de chômage ou participer à un stage. Il faut également avoir travaillé en Suède.</p>
<p>Pour en savoir plus, vous pouvez consulter le site Internet suivant :</p>
<ul class="spip">
<li><a href="http://www.arbetsformedlingen.se/" class="spip_out" rel="external">Agence nationale pour l’emploi (<i>Arbetsförmedlingen</i>)</a></li>
<li>Tél. : +46 0771-416416</li></ul>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/emploi-stage/article/recherche-d-emploi-113522). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
