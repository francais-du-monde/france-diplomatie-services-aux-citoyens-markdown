# La sécurité sociale

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/assurance-maladie/article/la-securite-sociale#sommaire_1">La couverture sociale obligatoire </a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/assurance-maladie/article/la-securite-sociale#sommaire_2">Les bénéficiaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/assurance-maladie/article/la-securite-sociale#sommaire_3">L’immatriculation </a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/assurance-maladie/article/la-securite-sociale#sommaire_4">La carte Vitale</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/assurance-maladie/article/la-securite-sociale#sommaire_5">Les remboursements </a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/assurance-maladie/article/la-securite-sociale#sommaire_6">La couverture complémentaire</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/assurance-maladie/article/la-securite-sociale#sommaire_7">La couverture maladie universelle (CMU)</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>La couverture sociale obligatoire </h3>
<p>La protection sociale obligatoire est couramment appelée « sécurité sociale ».</p>
<p>Le régime général est organisé en quatre branches :</p>
<ul class="spip">
<li><strong>la branche maladie, maternité, paternité, invalidité, décès</strong>, <strong>et branche accidents du travail et maladies professionnelles </strong>gérées de manière distincte par la Caisse nationale d’assurance maladie des travailleurs salariés (CNAMTS)</li>
<li><strong>la branche vieillesse </strong>gérée par la caisse nationale d’assurance vieillesse (CNAV)</li>
<li><strong>la branche famille </strong>gérée par la caisse nationale d’allocations familiales (CNAF)</li></ul>
<p>Institué en 1945, le régime général a vocation à protéger l’ensemble de la population.</p>
<p>Dès qu’un employeur embauche un salarié en France, il est tenu d’effectuer une déclaration préalable à l’embauche auprès de l’Union de recouvrement des cotisations de sécurité sociale et d’allocations familiales (URSSAF) dont il relève. Cette déclaration permet notamment de demander l’immatriculation à la sécurité sociale si l’intéressé ne possède pas de numéro d’immatriculation et l’affiliation à l’assurance chômage. S’agissant des retraites complémentaires, le salarié est affilié auprès de la caisse de retraite complémentaire à laquelle adhère son employeur en fonction de l’activité de l’entreprise ou du lieu d’implantation de l’entreprise.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.cleiss.fr/docs/regimes/regime_france.html" class="spip_out" rel="external">Régime français de sécurité sociale</a></p>
<h3 class="spip"><a id="sommaire_2"></a>Les bénéficiaires</h3>
<p>Il existe de nombreux cas et statuts particuliers qui donnent droit à l’Assurance maladie, par exemple : les salariés, les étudiants, les stagiaires de la formation professionnelle, les bénéficiaires du revenu de solidarité active (RSA)… Certaines personnes sans activité professionnelle peuvent être assurées sociales : les retraités, les chômeurs indemnisés, les accidentés du travail, etc.</p>
<p>Lorsqu’un individu est assuré, certaines personnes peuvent bénéficier des mêmes droits que lui : ce sont les <a href="http://www.ameli.fr/assures/droits-et-demarches/par-situation-professionnelle/vous-etes-sans-emploi/vous-etes-a-la-charge-d-8217-un-assure/etre-ayant-droit.php" class="spip_out" rel="external">ayants droit</a>. <br class="manualbr">Il s’agit, par exemple, des conjoints, concubins ou enfants de moins de 16 ans ou jusqu’à 20 ans s’ils poursuivent des études.</p>
<h3 class="spip"><a id="sommaire_3"></a>L’immatriculation </h3>
<p>L’<a href="http://www.ameli.fr/assures/droits-et-demarches/les-regles-d-immatriculation-des-assures.php" class="spip_out" rel="external">immatriculation</a> est l’opération qui permet de vous identifier et de vous inscrire auprès de la sécurité sociale.</p>
<h3 class="spip"><a id="sommaire_4"></a>La carte Vitale</h3>
<p>La <a href="http://www.ameli.fr/assures/soins-et-remboursements/comment-etre-rembourse/la-carte-vitale/mettre-a-jour-votre-carte-vitale.php" class="spip_out" rel="external">carte Vitale</a>, c’est votre carte d’assuré social. Elle est de couleur verte. Vous la recevez après votre immatriculation, accompagnée d’une attestation papier. La carte Vitale atteste de votre affiliation et de vos droits à l’Assurance maladie.</p>
<p>Vous trouverez sur l’attestation l’adresse de la caisse primaire d’Assurance maladie (CPAM) dont vous dépendez.</p>
<p>Si le praticien auquel vous vous adressez est informatisé pour utiliser la carte Vitale, vous n’avez pas de démarche à faire pour vous faire rembourser. Une feuille de soins électronique est transmise par le réseau informatique à votre CPAM. </p>
<p>Si le professionnel de santé ne possède pas l’équipement nécessaire, il établit une feuille de soins sur papier que vous remplirez et enverrez à votre CPAM. Votre carte Vitale n’a pas de date d’expiration, elle est valable sans limitation de durée, mais n’oubliez pas de la mettre à jour chaque année ou après chaque changement dans votre vie : maternité, déménagement, mariage, affection de longue durée, etc.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.ameli.fr/assures/soins-et-remboursements/comment-etre-rembourse/la-carte-vitale/mettre-a-jour-votre-carte-vitale.php" class="spip_out" rel="external">Site de l’Assurance maladie</a> </p>
<h3 class="spip"><a id="sommaire_5"></a>Les remboursements </h3>
<p>La CPAM rembourse, en partie, les frais médicaux des personnes qui y ont droit (médicaments, frais dentaires, analyses de laboratoires, hospitalisation, etc.).</p>
<p>Elle leur assure aussi, éventuellement, des indemnités journalières pour compenser l’absence de salaire en cas d’arrêt de travail.</p>
<p>Elle verse également des prestations en nature et des prestations en espèces qui compensent les pertes de revenu dans certains types de situations (invalidité, maternité, accidents du travail et maladies professionnelles, décès).</p>
<p>Les soins <strong>ne sont pas gratuits</strong>. Seulement une partie des frais est prise en charge par la sécurité sociale.</p>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li><a href="http://www.ameli.fr/assures/droits-et-demarches/index.php" class="spip_out" rel="external">Assurance maladie : droits et démarches</a></li>
<li><a href="http://www.ameli.fr/assures/soins-et-remboursements/index.php" class="spip_out" rel="external">Assurance maladie : soins et remboursements</a></li></ul>
<h3 class="spip"><a id="sommaire_6"></a>La couverture complémentaire</h3>
<p>Si vous souhaitez que tout ou partie des frais restant à votre charge soient remboursés, vous pouvez adhérer à une couverture complémentaire. Mais cette adhésion est facultative. La couverture complémentaire peut être soit une mutuelle, soit une assurance privée.</p>
<p>Attention, la mutuelle et certaines assurances ne vous remboursent que sur la base d’un tarif conventionné fixé par la sécurité sociale. Le dépassement de tarif reste à votre charge, sauf si vous avez souscrit à une « garantie plus ».</p>
<p>Une mutuelle fonctionne grâce à la solidarité et à l’entraide de ses membres. C’est un organisme qui ne peut réaliser de bénéfices à la différence des sociétés d’assurance à but lucratif.</p>
<p>A noter : la qualité des services offerts peut être très différente d’une organisation à l’autre. Il est important de les comparer pour choisir en toute connaissance.</p>
<h3 class="spip"><a id="sommaire_7"></a>La couverture maladie universelle (CMU)</h3>
<p>La <a href="http://www.ameli.fr/assures/soins-et-remboursements/cmu-et-complementaires-sante/cmu-de-base-une-assurance-maladie-pour-tous/objectif-l-acces-a-l-8217-assurance-maladie.php" class="spip_out" rel="external">CMU</a> permet aux personnes qui ne sont pas couvertes par un régime obligatoire d’Assurance maladie de bénéficier, sous certaines conditions, de la sécurité sociale pour la prise en charge de leurs dépenses de santé.</p>
<p>Pour en faire la demande, adressez-vous à la <a href="http://www.ameli.fr/assures/votre-caisse/index.php" class="spip_out" rel="external">caisse primaire d’assurance maladie (CPAM) de votre domicile.</a></p>
<p>La CMU comprend également une assurance complémentaire (<a href="http://www.ameli.fr/assures/soins-et-remboursements/cmu-et-complementaires-sante/cmu-complementaire/une-complementaire-sante-gratuite.php" class="spip_out" rel="external">CMU complémentaire</a>) laquelle prend en charge les dépenses restant à votre charge après l’intervention des régimes de base de sécurité sociale.</p>
<p>Vous devez faire la demande de CMU complémentaire auprès d’une caisse d’assurance maladie. Elle seule est habilitée à reconnaître vos droits. En fonction de votre situation personnelle, il peut s’agir d’une CPAM (Caisse primaire d’assurance maladie) ou CGSS (Caisse générale de la sécurité sociale), d’une caisse du RSI (Régime Social des Indépendants) ou d’une caisse de MSA (Mutualité sociale agricole).</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.cmu.fr/cmu-de-base.php" class="spip_out" rel="external">CMU de base</a></li>
<li><a href="http://www.cmu.fr/cmu-complementaire.php" class="spip_out" rel="external">CMU complémentaire</a></li></ul>
<p><i>Mise à jour : avril 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/assurance-maladie/article/la-securite-sociale). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
