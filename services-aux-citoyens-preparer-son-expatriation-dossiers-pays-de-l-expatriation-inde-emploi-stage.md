# Emploi, stage

<h2 class="rub22885">Marché du travail</h2>
<p>Si traditionnellement seules les filiales indiennes des sociétés étrangères d’une taille importante faisaient venir des expatriés expérimentés ou qualifiés, avec l’émergence de nombreux secteurs exportateurs indiens, cette situation est en train de changer.</p>
<p>Toutefois, en dehors d’une affectation par la société mère étrangère dans sa filiale indienne, trouver des opportunités de travail représente un effort considérable dans la mesure où les salaires pratiqués en Inde sont sensiblement moins élevés que ceux dans les pays industrialisés.</p>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/emploi-stage/#sommaire_1">Secteurs et profils à fort potentiel</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/emploi-stage/#sommaire_2">Secteurs et profils à faible potentiel</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/emploi-stage/#sommaire_3">Rémunération</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Secteurs et profils à fort potentiel</h3>
<p>Le marché indien est très demandeur de cadres très qualifiés (directeur pays, responsable de site) qui possèdent des expertises techniques dans les secteurs suivants : l’ingénierie de pointe, la pétrochimie, l’énergie, les infrastructures et les télécommunications. Il existe également des opportunités dans la gestion hospitalière et l’hôtellerie. Le secteur de la santé est également très porteur en Inde (pharmacie, appareils médicaux, spa et bien-être). Les grands centres commerciaux attirent les marques de luxe dans différents secteurs (prêt-à-porter, chaussures, habitat et décoration d’intérieur). La récente ouverture du secteur de la distribution pourrait en faire un secteur porteur. Il est encore un peu tôt pour distinguer une tendance forte, mais il y a sans doute du potentiel pour des profils expérimentés dans ce domaine.</p>
<p><strong>Exemple d’emplois proposés à des Français</strong></p>
<p>Supervision de chantiers de travaux publics (ponts, routes, barrages), traitement de l’eau, direction d’hôtels, etc. Dans l’ensemble, les cadres confirmés, de haut niveau, et ayant des compétences pointues sont appréciés.</p>
<h3 class="spip"><a id="sommaire_2"></a>Secteurs et profils à faible potentiel</h3>
<p>L’industrie agroalimentaire française peine à percer en Inde. Les normes peu favorables, les lourdes taxes et les goûts des Indiens rendent ce marché difficile à pénétrer pour nos entreprises, qui embauchent peu dans ce secteur.</p>
<p>Le marché du travail indien est très concurrentiel, même pour les jeunes diplômés indiens. Les jeunes diplômés français avec un ou deux ans d’expérience et ayant une connaissance de l’Inde peuvent néanmoins être embauchés par de grandes entreprises françaises, par des PME françaises implantées sur place, ou bien par des PME indiennes opérant en partenariat avec des sociétés françaises.</p>
<p>Ce marché est complexe pour les jeunes peu expérimentés, notamment dans le secteur des TIC. L’Inde regorge de techniciens bon marché et de bon niveau.</p>
<p><strong>Qualités pour réussir en Inde</strong></p>
<ul class="spip">
<li>Fortes capacités d’adaptation</li>
<li>Patience et persévérance</li>
<li>Grandes capacités de communication</li>
<li>Capacité à nouer des liens forts, y compris dans le cadre du travail</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Rémunération</h3>
<p>Le salaire minimum est fixé par corps de métier et par Etat. Pour la région de Delhi, le salaire minimum horaire évolue dans la fourchette 297 INR – 361 INR selon les qualifications (<i>Unskilled</i>, <i>Semi-Skilled</i>, <i>Skilled</i>). Les révisions semestrielles sont indexées sur l’inflation. Les augmentations à deux chiffres sont fréquentes. Les heures supplémentaires sont rémunérées le double.</p>
<p>En outre, toute société est tenue de verser à un ouvrier/employé une prime (bonus) correspondant à minimum 8,33% de la rémunération salariale annuelle perçue par lui, qu’elle ait réalisé des bénéfices ou non (<i>Payment of Bonus Act</i>). Le versement du bonus est obligatoire dans les entreprises de plus de 20 salariés et pour tout salarié ayant travaillé plus de 30 jours dans l’année disposant d’un salaire mensuel n’excédant pas 10000 INR. Le plafond de la prime est fixé à 20% de la rémunération salariale annuelle de l’employé concerné mais les salaires des cadres expatriés ou des ingénieurs confirmés étrangers dépassent en général largement ce plafond.</p>
<p>Le cadre réglementaire indien fixe des plafonds de rémunération de hauts dirigeants d’une société enregistrée en tant que <i>Private Limited Company</i> (PLC) en Inde (maximum 5% des bénéfices nets pour le directeur exécutif si une seule personne occupe une fonction de ce type, 10% des bénéfices nets au total si plusieurs personnes exercent de telles fonctions).</p>
<p>Les étrangers sous contrat local (c’est-à-dire n’ayant pas le statut d’expatrié), payent des impôts relativement élevés relativement à leur salaire.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-inde-emploi-stage-article-stages.md" title="Stages">Stages</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-inde-emploi-stage-article-entretien-d-embauche-110408.md" title="Entretien d’embauche">Entretien d’embauche</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-inde-emploi-stage-article-lettre-de-motivation-110407.md" title="Lettre de motivation">Lettre de motivation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-inde-emploi-stage-article-curriculum-vitae.md" title="Curriculum vitae">Curriculum vitae</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-inde-emploi-stage-article-recherche-d-emploi.md" title="Recherche d’emploi">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-inde-emploi-stage-article-reglementation-du-travail-110404.md" title="Réglementation du travail">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-inde-emploi-stage-article-marche-du-travail-110403.md" title="Marché du travail">Marché du travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/emploi-stage/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
