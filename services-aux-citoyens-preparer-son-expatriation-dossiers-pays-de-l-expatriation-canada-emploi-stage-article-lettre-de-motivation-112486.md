# Lettre de motivation

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/emploi-stage/article/lettre-de-motivation-112486#sommaire_1">Rédaction</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/emploi-stage/article/lettre-de-motivation-112486#sommaire_2">Modèles de lettre de motivation</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Rédaction</h3>
<p>La lettre de motivation se nomme en anglais <i>cover letter</i>. Elle doit être dactylographiée et personnalisée. Elle doit obligatoirement être jointe au curriculum</p>
<p>vitae et ne doit pas dépasser deux pages, l’idéal étant de s’en tenir à une seule page.</p>
<p>Elle doit démontrer votre capacité à communiquer par écrit et résumer les points suivants :</p>
<ul class="spip">
<li>Vos expériences et votre projet professionnels ;</li>
<li>Vos atouts et vos qualités. Il est inutile d’exagérer vos connaissances linguistiques ;</li>
<li>L’intérêt que vous portez à l’entreprise et ce que vous pourriez lui apporter ;</li>
<li>Votre disponibilité et votre mobilité, ainsi que vos souhaits.</li></ul>
<p>La lettre de motivation sera adressée nominativement à la personne chargée du recrutement, même en cas de candidature spontanée, et au bon service dans l’entreprise.</p>
<p>L’envoi par courriel de la lettre de motivation et du curriculum vitae est accepté. Il est cependant préférable d’envoyer ces documents par la voie postale.</p>
<p>Si, au bout de deux semaines, vous n’avez pas été contacté par l’entreprise, n’hésitez pas à appeler votre interlocuteur.</p>
<p>La lettre de motivation se nomme au Québec <strong>lettre de présentation</strong>.</p>
<p>Elle est jointe au curriculum vitae. Elle est très différente de son équivalent français.</p>
<p>La lettre de présentation doit être courte et aller droit au but. Il faut à tout prix éviter les effets de style, voire un français recherché, et opter pour l’efficacité et la sobriété. Le seul but de la lettre de présentation est de provoquer une entrevue avec un employeur et d’accompagner le curriculum vitae.</p>
<p>En quelques phrases, le chercheur d’emploi se porte candidat à un poste et démontre qu’il correspond parfaitement au profil de l’emploi proposé. Le candidat indique qu’il est très intéressé à rencontrer son recruteur et qu’il se permettra de lui téléphoner la semaine prochaine pour prendre contact.</p>
<p>Si vous répondez à une offre d’emploi, la candidature doit, sauf avis contraire, être envoyée par courriel. Le candidat joindra alors deux fichiers au format Word, format le plus utilisé dans les entreprises : le CV et la lettre de présentation. Il est préférable d’éviter d’envoyer ses fichiers au format PDF.</p>
<p>Afin d’éviter que le courriel ne soit assimilé à un virus informatique et détruit, il importe d’indiquer en objet la référence de l’offre d’emploi. N’oubliez pas dans votre message de mentionner votre nom, votre titre et vos coordonnées (courriel, téléphone fixe et mobile, voire votre adresse postale).</p>
<p>S’il s’agit d’une candidature spontanée, votre lettre doit, en revanche, être envoyée par courrier postal. Tout comme le curriculum vitae, la lettre de présentation ne doit pas être pliée. Elle doit être expédiée dans une grande enveloppe avec le curriculum vitae.</p>
<h3 class="spip"><a id="sommaire_2"></a>Modèles de lettre de motivation</h3>
<p>De nombreux sites Internet proposent des modèles de lettre de motivation. Vous pouvez vous en inspirer sans, toutefois, recopier intégralement les paragraphes. La lettre de motivation doit être avant tout personnalisée et refléter vos ambitions professionnelles et votre motivation à l’égard de l’entreprise.</p>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li><a href="https://www.jobsetc.gc.ca/eng/" class="spip_out" rel="external">Guichet emplois</a> du gouvernement canadien.</li>
<li><a href="http://emploiquebec.net/" class="spip_out" rel="external">Le site Internet d’Emploi-Québec</a> met à disposition un guide pratique de recherche d’emploi. Celui-ci contient de précieux conseils sur l’élaboration de la lettre de présentation, qui accompagnera le curriculum vitae, ainsi que des modèles de lettre. Dans tous les cas, la lettre devra être le reflet de votre personnalité.</li></ul>
<p><i>Mise à jour : avril 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/emploi-stage/article/lettre-de-motivation-112486). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
