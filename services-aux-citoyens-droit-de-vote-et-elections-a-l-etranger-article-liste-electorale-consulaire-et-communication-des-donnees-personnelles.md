# Liste électorale consulaire et communication des données personnelles

<p><strong>Quelles informations apparaissent sur la liste électorale consulaire (LEC) ?</strong></p>
<p>Les nom, prénom, date et lieu de naissance ; le domicile ou la résidence ; l’adresse électronique lorsqu’elle a été fournie et le cas échéant, le bureau de vote.</p>
<p><strong>Comment sont obtenues ces informations ?</strong></p>
<p>Elles sont tirées du Registre des Français établis hors de France. Vous les avez fournies lors de votre inscription au consulat.</p>
<p><strong>Qui peut obtenir communication des LEC ?</strong></p>
<p>Tout électeur de votre circonscription, tout candidat, tout parti ou groupement politique, les sénateurs et les députés représentant les Français établis hors de France ainsi que vos conseillers (AFE et consulaires).</p>
<p><strong>A quoi est supposée servir la communication des listes électorales consulaires (LEC) ?</strong></p>
<p>La liste électorale consulaire a vocation à être utilisée à des fins de communication politique. Elle permet également aux électeurs de vérifier la régularité des inscriptions.</p>
<p><strong>Que faire si vous ne souhaitez pas recevoir tout ou partie des messages adressés par ceux qui ont obtenu communication des listes électorales consulaires ?</strong></p>
<p>Vous pouvez tout d’abord, si vous le souhaitez, donner deux adresses électroniques au consulat. L’une destinée exclusivement à vos échanges administratifs. L’autre qui apparaîtra sur la liste électorale consulaire et sera donc dédiée à votre information politique.</p>
<p>Vous pouvez également, à la réception d’un message non désiré, cliquer sur le lien de désinscription. Les personnes qui utilisent vos données personnelles ont l’obligation de faciliter vos demandes d’opposition à une sollicitation par voie électronique.</p>
<p><strong>Et si la désinscription ne marche pas ou si le lien n’existe pas ?</strong></p>
<p>Vous avez la possibilité de contacter la Commission Nationale de l’Informatique et des Libertés (CNIL) qui s’assurera du respect de vos droits par le parti, élu, électeur ou le candidat concerné.</p>
<p>Vous pouvez à cet effet utiliser le formulaire « témoignage » de ce site ou adresser directement une plainte à la <a href="https://www.cnil.fr/elections/temoigner/" class="spip_out" rel="external">CNIL</a>.</p>
<p><strong>Ce qu’il ne faudrait vraiment pas faire….</strong></p>
<p>La tentation peut être grande de demander au consulat le retrait de l’information relative à votre adresse électronique.</p>
<p>Ce serait vraiment dommage car cette information est pour vous la garantie d’une information régulière de la part du consulat et, dans les pays concernés, d’une information rapide voire en temps réel en cas de crise.</p>
<p>En outre, la fourniture d’une adresse électronique est indispensable si vous souhaitez pouvoir voter par internet (élection des conseillers consulaires et des députés).</p>
<p><i>Mise à jour : octobre 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/droit-de-vote-et-elections-a-l-etranger/article/liste-electorale-consulaire-et-communication-des-donnees-personnelles). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
