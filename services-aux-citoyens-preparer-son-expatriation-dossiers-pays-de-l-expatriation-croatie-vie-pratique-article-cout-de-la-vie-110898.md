# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/vie-pratique/article/cout-de-la-vie-110898#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/vie-pratique/article/cout-de-la-vie-110898#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/vie-pratique/article/cout-de-la-vie-110898#sommaire_3">Alimentation</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/vie-pratique/article/cout-de-la-vie-110898#sommaire_4">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>La kuna (HRK), monnaie croate, est assez stable. Début 2014, 1 EUR était équivalent à environ 7,60 HRK.</p>
<p>Il existe des bureaux de change un peu partout dans le pays ainsi qu’un réseau dense de distributeurs automatiques de billets.</p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<p>Les différents types de cartes bancaires (Visa, Mastercard, American Express, etc.) sont acceptées partout. Quelques commerces n’acceptent toutefois que du numéraire.</p>
<h3 class="spip"><a id="sommaire_3"></a>Alimentation</h3>
<p>Prix moyen d’un repas dans un restaurant : environ 100 HRK.</p>
<h3 class="spip"><a id="sommaire_4"></a>Evolution des prix</h3>
<p>L’inflation devrait avoisiner les 2,7% en 2013, mais les prix sont en hausse modérée, notamment après les fortes hausses constatées en 2012, notamment sur les tarifs énergétiques, et en raison du passage du taux de TVA de 23 à 25%.</p>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/vie-pratique/article/cout-de-la-vie-110898). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
