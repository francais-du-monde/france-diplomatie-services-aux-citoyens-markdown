# Fiscalité du pays

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/fiscalite/article/fiscalite-du-pays#sommaire_1">Présentation </a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/fiscalite/article/fiscalite-du-pays#sommaire_2">Année fiscale</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/fiscalite/article/fiscalite-du-pays#sommaire_3">Barème de l’impôt</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/fiscalite/article/fiscalite-du-pays#sommaire_4">Coordonnées des centres d’information fiscale </a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Présentation </h3>
<h4 class="spip">L’impôt sur le revenu applicable aux personnes physiques résidentes en Autriche</h4>
<p>La fiscalité autrichienne opère deux distinctions :</p>
<ul class="spip">
<li>entre l’impôt sur le revenu (<i>Einkommensteuer - ESt</i>) payé par les non-salariés, et l’impôt salarial (<i>Lohnsteuer - LSt</i>) ; toutefois, le barème de ces deux impôts est identique ;</li>
<li>entre l’imposition illimitée (<i>Unbeschränkte</i>) qui concerne les personnes physiques domiciliées en Autriche ou y ayant leur résidence habituelle, et l’imposition limitée (<i>Beschränkt</i>) qui concerne les autres personnes physiques, et frappe seulement leurs revenus obtenus en Autriche.</li></ul>
<h4 class="spip">L’impôt sur le revenu salarial (<i>Lohnsteuer</i>)</h4>
<p>Il est prélevé à la source. Bien que le débiteur en soit le salarié, il revient à l’employeur de le percevoir, le retenir et le reverser au service des impôts. Les salariés bénéficient de certains avantages fiscaux particuliers (dégrèvement des heures supplémentaires et du travail de nuit, etc.).</p>
<p>Attention : un salarié ayant plusieurs emplois devra faire une déclaration de revenus (<i>Einkommenssteuererkärung</i>). L’impôt sur le revenu salarial (<i>Lohnsteuer</i>) sera déduit du montant total à payer.</p>
<p>Si le travailleur étranger séjourne plus de cent quatre-vingt-trois jours en Autriche au cours de l’année civile, ou s’il est domicilié dans ce pays, il est soumis à une imposition illimitée.</p>
<p>S’il n’a ni domicile, ni résidence habituelle en Autriche, mais que son salaire est pris en charge par une société résidente, il est soumis à une imposition limitée. L’impôt sur le revenu pour les assujettis « limités » se calcule en principe selon le barème normal, sous réserve des avantages généraux et des exonérations.</p>
<p>S’il n’a ni domicile, ni résidence habituelle en Autriche, et son salaire n’est pas versé par une société ayant son siège en Autriche ou par un établissement stable d’une entreprise étrangère, le droit d’imposition est exercé par le pays de domiciliation.</p>
<h4 class="spip">L’impôt sur le revenu (<i>Einkommenssteuer</i>)</h4>
<p>Les personnes physiques domiciliées ou résidant habituellement en Autriche ainsi que les entreprises qui n’ont pas la forme de société de capitaux sont soumises à l’impôt sur le revenu. L’obligation fiscale illimitée s’étend en général à l’ensemble des revenus nationaux ou internationaux. On considère qu’une personne est habituellement domiciliée en Autriche lorsqu’elle n’y séjourne pas uniquement de manière temporaire. En cas de séjour effectif de plus de six mois en Autriche, l’assujettissement à l’impôt en Autriche est illimité. Sont soumis à l’impôt sur le revenu, tous les revenus perçus au cours d’une année civile, énumérés dans la loi relative à l’impôt sur le revenu. En cas de revenus de source étrangère et en fonction des conventions fiscales de double imposition, ceux-ci entrent soit intégralement dans la base d’imposition – avec déduction des impôts libérés à l’étranger – (les intérêts et plus-values mobilières par exemple) soit juste dans le calcul du taux marginal d’imposition (à titre indicatif les revenus fonciers).</p>
<p>Il est établi sur la base d’une déclaration d’impôt. Le paiement se fait d’avance, trimestriellement, en référence à la déclaration de l’année fiscale précédente. Si nécessaire, les services fiscaux rembourseront un trop perçu, ou établiront un complément d’impôt, après réception de la déclaration.</p>
<p><strong>Réforme fiscale 2004/2005</strong></p>
<p>Afin de rendre le « Standort Österreich » plus attractif, le gouvernement autrichien a mis en oeuvre la première phase d’une réforme fiscale au 1er janvier 2004 (suppression du 13ème prélèvement de TVA, abaissement des charges salariales pour les travailleurs âgés etc.). La deuxième phase de cette réforme est entrée en vigueur le 1er janvier 2005 et bénéficie pour moitié aux ménages, pour moitié aux entreprises.</p>
<p>La principale mesure visant les personnes physiques, concernait la modification du barème de l’impôt sur le revenu. Pour leur part, les sociétés de capitaux ont vu le taux nominal de l’impôt sur les bénéfices diminuer de 34 à 25 %.</p>
<p><strong>Réforme fiscale 2009</strong></p>
<p>Cette réforme a été mise en place dans le cadre du « deuxième paquet conjoncturel ». Elle vise en premier lieu les ménages et leur revenu disponible. Le seuil imposable est relevé de 10 000 EUR à 11 000 EUR, le taux d’imposition des revenus moyens est baissé. La classe moyenne est ainsi la principale bénéficiaire de cette réforme. L’augmentation du pouvoir d’achat annuel est plafonné à € 1 350 (atteint pour un revenu brut annuel de € 81 200).</p>
<h5 class="spip">Avantages fiscaux</h5>
<p>Ils concernent notamment l’aide à la recherche (déduction de 25% des dépenses de développement ou d’amélioration d’inventions économiques), l’emploi d’apprentis (exemption d’impôt et primes), etc.</p>
<p>Les réserves latentes engendrées par la vente de biens économiques peuvent être déduites des coûts d’acquisitions d’autres biens, ou reversées sur des réserves non imposables.</p>
<p>Les pertes peuvent être reportées sur les années suivantes et être déduites (jusqu’à 75%) des revenus postérieurs positifs.</p>
<h4 class="spip">L’impôt sur les sociétés (<i>Körperschaftsteuer</i>)</h4>
<p>Les personnes morales sont soumises à l’impôt sur les sociétés (<i>KÖSt</i>). Cet impôt reprend les bases de la loi relative à l’impôt sur le revenu (<i>EStG</i>), mais les adapte aux personnes morales par les dispositions spécifiques de la loi <i>KStG</i> (<i>Körperschaftsteuergesetz</i>). Les organismes soumis à l’impôt sur les sociétés doivent disposer d’une personnalité morale. Il s’agit notamment des SA, des SARL, des coopératives et des associations.</p>
<p>Les sociétés siégeant en Autriche sont imposables de façon illimitée, sur la totalité de leurs revenus, qu’ils soient d’origine autrichienne ou étrangère. En revanche, les organismes ne siégeant pas en Autriche ne sont imposables que sur certains types de revenus ayant leur origine sur le territoire autrichien.</p>
<p>L’assiette fiscale est calculée sur le revenu obtenu au cours de la période d’imposition. Depuis le 1er janvier 2005, le revenu des sociétés de capitaux est taxé à hauteur de 25%, que les bénéfices soient distribués ou non. L’impôt forfaitaire minimum est de 5% du capital social. Il s’élève ainsi à 1750 EUR par an pour les SARL (<i>Gesellschaft mit beschränkter Haftung, GmbH</i>) et 3500 EUR pour une SA (<i>Aktiengesellschaft, AG</i>). Cet impôt est imputable indéfiniment sur des impôts futurs.</p>
<p>Les dividendes et participations au bénéfice d’autre nature, perçus par une entreprise en raison de sa participation à une SA ou à une SARL, sont exonérés de l’impôt sur les sociétés. Sous certaines conditions, l’Autriche accorde également l’exonération des revenus de participations en cas de participation à des sociétés étrangères qui appartiennent au moins pour 10% à des holdings (participation croisée internationale). Le taux d’imposition généralement appliqué aux dividendes versés aux résidents et aux non-résidents est de 25%, sous forme de retenue à la source.</p>
<h5 class="spip">Impôt communal</h5>
<p>Chaque chef d’entreprise qui emploie des salariés sur le territoire autrichien est soumis à l’impôt communal à hauteur de 3% du salaire brut de l’ensemble de son personnel. L’impôt communal, ayant valeur de charge, peut être déduit du calcul de l’impôt sur les bénéfices.</p>
<h5 class="spip">Taxe sur le chiffre d’affaires</h5>
<p>Le chiffre d’affaires réalisé sur des livraisons de biens, des prestations de services et des acquisitions intracommunautaires, effectuées à titres onéreux par des assujettis, est soumis à la taxe sur le chiffre d’affaires (<i>USt</i>). Cette taxe est établie sur les mêmes principes que ceux de la TVA française. Elle est exigible, que l’entreprise soit domiciliée en Autriche ou non. Le taux normal d’imposition est actuellement de 20%, le taux réduit est de 10%.</p>
<p>Les entreprises dont le chiffre d’affaires est soumis à la taxe sur le chiffre d’affaires peuvent déduire toute taxe sur le chiffre d’affaires facturée par d’autres entreprises pour leur livraison ou prestation comme taxe perçue en amont. La déduction de la taxe versée en amont n’est en principe pas autorisée si les marchandises ou prestations contribuent à réaliser un chiffre d’affaires exonéré de taxe.</p>
<p><i>Source : <a href="http://www.tresor.economie.gouv.fr/se/autriche/documents_new.asp?V=1_PDF_154656" class="spip_out" rel="external">site de la Mission économique française en Autriche</a></i>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Année fiscale</h3>
<p>L’année fiscale est l’année civile. L’impôt sur les bénéfices des sociétés se calcule sur les bénéfices de l’année économique, qui, en principe, est identique à l’année civile.</p>
<h3 class="spip"><a id="sommaire_3"></a>Barème de l’impôt</h3>
<h4 class="spip">Impôt sur les revenus</h4>
<p><strong>La classification des revenus est la suivante :</strong></p>
<ul class="spip">
<li>Revenus agricoles et forestiers ;</li>
<li>Bénéfices des professions indépendantes et d’autres activités exercées de manière indépendante ;</li>
<li>Bénéfices industriels et commerciaux ;</li>
<li>Revenus salariaux ;</li>
<li>Revenus des capitaux mobiliers ;</li>
<li>Revenus fonciers ;</li>
<li>Autres revenus divers comprenant les annuités et autres bénéfices et avantages récurrents, certaines plus-values ;</li>
<li>Du revenu total provenant de ces sept classes, il faut déduire les frais professionnels, les frais supplémentaires, les frais extraordinaires et, le cas échéant, les charges pour arriver à la base d’imposition.</li></ul>
<p>Le revenu des sociétés se calcule également selon la loi sur l’impôt sur les revenus ainsi que selon la loi sur l’impôt sur les bénéfices des sociétés.</p>
<p><strong>La détermination de l’imposition se fait en plusieurs étapes :</strong></p>
<ul class="spip">
<li>détermination de la base d’imposition (c’est à dire, des revenus perçus par l’assujetti au cours de l’année civile) ;</li>
<li>déduction des frais spéciaux, professionnels, exceptionnels, et le cas échéant des charges relatives à l’entreprise ;</li>
<li>le montant obtenu est ensuite soumis à un taux fiscal progressif :</li></ul>
<p><strong>Impôt sur le revenu des personnes physiques applicable depuis le 1er Janvier 2009</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Revenus en €</strong></td>
<td><strong>Impôt sur le revenu</strong>  <strong>(avant abattements)</strong></td>
<td><strong>Taux moyen d’imposition</strong></td>
<td><strong>Taux marginal d’imposition</strong></td></tr>
<tr class="row_even even">
<td>11 000 et en dessous</td>
<td>0</td>
<td>0%</td>
<td>0%</td></tr>
<tr class="row_odd odd">
<td>11 000-25 000</td>
<td>(Revenu-11 000) ×5110 / 14000</td>
<td>0-20,44</td>
<td>36,5%</td></tr>
<tr class="row_even even">
<td>25 000-60 000</td>
<td>(Revenu-25 000) ×15 125 / 35 000 + 5110</td>
<td>20,44-33,725</td>
<td>43,2143%</td></tr>
<tr class="row_odd odd">
<td>Au-delà de 60 000</td>
<td>(Revenu-60 000×0,5)+20 235</td>
<td>Supérieur à 33,725</td>
<td>50%</td></tr>
</tbody>
</table>
<ul class="spip">
<li>Abattement pour le soutien de famille : 364,00€ par an (montant pour un enfant)</li>
<li>Abattement pour le parent célibataire : 494,00€ par an</li>
<li>Abattement pour le salarié : 54,00€ par an</li>
<li>Abattement pour les frais professionnels : 132,00€</li>
<li>Abattement pour les frais de transport : 291,00 €</li>
<li>Abattement pour les retraités : entre 0 et 400,00€</li></ul>
<h4 class="spip">Autres impôts et taxes</h4>
<h5 class="spip">Droit de mutation de biens immobilier</h5>
<p>Toutes les transactions immobilières à titre onéreux entre vifs sont soumises à cet impôt y compris le transfert de terrains en relation avec la constitution d’une société ainsi que la reprise de toutes les parts et la réunion de toutes ces parts entre les mains d’une seule personne ou actionnaire. Le taux d’imposition s’élève à 3,5% de la contrepartie ou 3,5% de la valeur globale résultant de la réunion des parts. Pour certaines restructurations sociales, les droits de mutation sur les biens immobiliers s’élèvent à 3,5% du double de la valeur globale.</p>
<p>L’enregistrement du nouveau propriétaire au cadastre engendre une taxe d’enregistrement complémentaire de 1% de la base de calcul de l’assiette pour le droit de mutation des biens immobiliers.</p>
<h5 class="spip">Taxe de l’affection de capitaux</h5>
<p>La création d’une société de capitaux (ou d’une société de personnes avec une société de capitaux en tant qu’associé personnellement responsable) induit une taxe de 1% de la valeur des montants convenus ou versés par les associés (non responsables personnellement). Une augmentation du capital ou de la contribution des associés dans une société de capitaux existante est également soumise à une imposition à hauteur de 1%. En cas de dotation en capital d’une filiale autrichienne par sa maison mère (par subvention ou abandon de créance) soit cette filiale est réputée liquide alors la taxe de 1% s’applique, à défaut il s’agit d’un produit imposable à l’impôt sur les sociétés (25%).</p>
<h3 class="spip"><a id="sommaire_4"></a>Coordonnées des centres d’information fiscale </h3>
<p>Pour plus de renseignements, il vous appartient de consulter les sites Internet de l’administration fiscale autrichienne :</p>
<ul class="spip">
<li><a href="http://www.bmf.gv.at/" class="spip_out" rel="external">Administration fiscale autrichienne</a></li>
<li><a href="http://www.help.gv.at/" class="spip_out" rel="external">Le portail gouvernemental</a> (sous les mots clés : <i>Steuern</i>, <i>Lohnsteuer</i>, <i>Einkommensteuer</i>)</li></ul>
<p><i>Mise à jour : février 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/fiscalite/article/fiscalite-du-pays). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
