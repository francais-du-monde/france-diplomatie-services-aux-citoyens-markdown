# Vaccination

<p>Aucune vaccination n’est exigée à l’entrée dans le pays.</p>
<p>Les vaccinations suivantes sont recommandées pour des raisons médicales :</p>
<ul class="spip">
<li>pour les adultes : mise à jour des vaccinations contre la diphtérie, le tétanos et la poliomyélite ; hépatites A et B.</li>
<li>pour les enfants : toutes les vaccinations incluses dans le calendrier vaccinal français, en particulier pour les longs séjours : BCG dès le premier mois, rougeole dès l’âge de neuf mois.</li></ul>
<p>Il est préférable de réaliser toutes les vaccinations nécessaires avant de partir, car une fois sur place on peut rencontrer des difficultés d’approvisionnement.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="services-aux-citoyens-preparer-son-expatriation-sante.md" class="spip_in">Santé</a></p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/entree-et-sejour/article/vaccination-111333). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
