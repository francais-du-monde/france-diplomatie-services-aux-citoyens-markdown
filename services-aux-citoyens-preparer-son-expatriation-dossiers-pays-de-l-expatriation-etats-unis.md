# Etats-Unis

<p>Sur l’ensemble du territoire des Etats-Unis, la communauté française est estimée à environ 300 000 personnes.</p>
<p>Au <strong>31 décembre 2014</strong>, le nombre des inscrits au registre des Français établis aux Etats-Unis s’élevait à 135 837.</p>
<p>Cette population est répartie entre les différentes circonscriptions consulaires de la manière suivante :</p>
<ul class="spip">
<li>New York : 30 063 inscrits</li>
<li>San Francisco : 19 613 inscrits</li>
<li>Los Angeles : 22 654 inscrits</li>
<li>Washington : 13 634 inscrits</li>
<li>Miami : 11 262 inscrits</li>
<li>Chicago : 11 091 inscrits</li>
<li>Houston : 9361 inscrits</li>
<li>Boston : 7731 inscrits</li>
<li>Atlanta : 6460 inscrits</li>
<li>La Nouvelle Orléans : 995 inscrits</li></ul>
<p>La communauté française aux Etats-Unis se situe au troisième rang des communautés des pays européens, après celles de la Suisse et de l’Allemagne, et loin derrière les communautés des pays d’Amérique centrale et d’Amérique du sud.</p>
<p>La présence économique française aux Etats-Unis s’appuie sur près de 3000 implantations employant plus de 500 000 personnes.</p>
<p>En plus de ce dossier concernant les démarches administratives liées à l’expatriation, France Diplomatie met aussi à votre disposition :</p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/dossiers-pays/etats-unis/" class="spip_in">Une description des Etats-Unis, de sa situation politique et économique</a></li>
<li><a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/etats-unis/" class="spip_in">Des informations actualisées sur les récommandations de sécurité aux Etats-Unis</a></li>
<li><a href="http://fr.ambafrance-us.org/" class="spip_out" rel="external">Ambassade de France aux Etats-Unis</a>.</li></ul>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-etats-unis-entree-et-sejour-23321.md">Entrée et séjour</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-etats-unis-entree-et-sejour-23321-article-passeport-visa-permis-de-travail-113138.md">Passeport, visa, permis de travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-etats-unis-entree-et-sejour-23321-article-demenagement-113139.md">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-sante.md">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-etats-unis-entree-et-sejour-23321-article-animaux-domestiques-113141.md">Animaux domestiques</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-etats-unis-emploi-stage.md">Emploi, stage</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-etats-unis-emploi-stage-article-marche-du-travail-113142.md">Marché du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-etats-unis-emploi-stage-article-reglementation-du-travail-113143.md">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-etats-unis-emploi-stage-article-recherche-d-emploi-113144.md">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-etats-unis-emploi-stage-article-curriculum-vitae-113145.md">Curriculum vitae</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-etats-unis-emploi-stage-article-lettre-de-motivation-113146.md">Lettre de motivation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-etats-unis-emploi-stage-article-entretien-d-embauche-113147.md">Entretien d’embauche</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-etats-unis-emploi-stage-article-stages.md">Stages</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-etats-unis-protection-sociale.md">Protection sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-etats-unis-protection-sociale-article-regime-local-de-securite-sociale-113150.md">Régime local de sécurité sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-etats-unis-protection-sociale-article-convention-de-securite-sociale.md">Convention de sécurité sociale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-etats-unis-fiscalite.md">Fiscalité</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-etats-unis-fiscalite-article-fiscalite-du-pays.md">Fiscalité du pays</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-etats-unis-fiscalite-article-convention-fiscale-113153.md">Convention fiscale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-etats-unis-vie-pratique.md">Vie pratique</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-etats-unis-vie-pratique-article-logement-113154.md">Logement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-etats-unis-vie-pratique-article-sante-113155.md">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-etats-unis-vie-pratique-article-scolarisation-113156.md">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-etats-unis-vie-pratique-article-cout-de-la-vie.md">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-etats-unis-vie-pratique-article-transports.md">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-etats-unis-vie-pratique-article-communications.md">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-etats-unis-vie-pratique-article-loisirs-et-culture-113160.md">Loisirs et culture</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-etats-unis-presence-francaise.md">Présence française</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-etats-unis-presence-francaise-article-presence-francaise.md">Présence française</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
