# Animaux domestiques

<p>Le Portugal entre dans le cadre des destinations se trouvant dans l’Union européenne.</p>
<h4 class="spip">Animaux de compagnie (chiens, chats et furets)</h4>
<ul class="spip">
<li>être identifiés par tatouage ou par puce électronique ;</li>
<li>être valablement vaccinés contre la rage ;</li>
<li>être titulaires d’un passeport délivré par un vétérinaire habilité attestant de l’identification et de la vaccination contre la rage de l’animal ;</li></ul>
<p><strong>Attention</strong> : depuis le 1er octobre 2004, tout chien, chat ou furet voyageant dans l’Union européenne avec son propriétaire ou à titre commercial doit être identifié, vacciné contre la rage et être en possession d’un passeport européen fourni et rempli par un vétérinaire. Depuis le 3 juillet 2011 <strong>seule la puce électronique est reconnue comme moyen d’identification</strong> pour les voyages au sein de l’Union européenne, <strong>sauf pour les</strong> animaux <strong>identifiés par tatouage avant cette date</strong>.</p>
<h4 class="spip">Autres animaux</h4>
<p>Si vous souhaitez voyager avec un animal ne faisant pas partie de la catégorie précédente, veuillez prendre connaissance des démarches à suivre :</p>
<ul class="spip">
<li><a href="http://agriculture.gouv.fr/transport" class="spip_out" rel="external">ministère de l’Agriculture, de l’Agroalimentaire et de la Forêt</a>, (choisissez votre catégorie dans la rubrique <i>Santé et protection des animaux</i>) ;</li>
<li>Notre rubrique thématique <a href="services-aux-citoyens-preparer-son-expatriation-douanes.md" class="spip_in">Douanes</a>.</li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/entree-et-sejour/article/animaux-domestiques). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
