# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/vie-pratique/article/transports-110473#sommaire_1">Importation de véhicule</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/vie-pratique/article/transports-110473#sommaire_2">Permis de conduire</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/vie-pratique/article/transports-110473#sommaire_3">Code de la route</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/vie-pratique/article/transports-110473#sommaire_4">Assurances et taxes</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/vie-pratique/article/transports-110473#sommaire_5">Achat et location</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/vie-pratique/article/transports-110473#sommaire_6">Immatriculation</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/vie-pratique/article/transports-110473#sommaire_7">Entretien</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/vie-pratique/article/transports-110473#sommaire_8">Réseau routier</a></li>
<li><a id="so_9" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/vie-pratique/article/transports-110473#sommaire_9">Transports en commun</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Importation de véhicule</h3>
<p>L’importation d’un véhicule est soumise à son homologation aux normes locales (clignotants latéraux, air conditionné). Les droits de douane sont généralement supérieurs à 100% de la valeur du véhicule, qu’il soit importé ou acheté localement. L’importation d’un véhicule de plus de quatre ans est interdite.</p>
<h3 class="spip"><a id="sommaire_2"></a>Permis de conduire</h3>
<p>Le permis de conduire français n’est valable en Israël que pendant la première année du séjour. Il convient donc de le remplacer rapidement par un permis israélien en se présentant auprès de <strong>la direction de la circulation routière</strong> muni des documents suivants : original du passeport, original du permis de conduire. Le demandeur reçoit un récépissé qui lui permet de passer un contrôle d’acuité visuelle chez un ophtalmologiste, puis un contrôle de conduite pratique (qui peut être repassé une fois en cas d’échec).</p>
<p>Le permis de conduire israélien « Rishayon nehiga » doit être renouvelé tous les cinq ans jusqu’à l’âge de 65 ans puis tous les deux ans au-delà de cet âge. Le montant des droits exigés est de l’ordre de 50 shekels (environ 9 euros). Cette démarche est vivement recommandée car <strong>les compagnies d’assurances ne couvrent pas les conducteurs sans permis israélien en cas d’accident. </strong></p>
<h3 class="spip"><a id="sommaire_3"></a>Code de la route</h3>
<p>La conduite s’effectue à droite, la priorité est aussi à droite.</p>
<p>Les vitesses maximales autorisées, sauf indications contraires, sont : 100 km/h sur les autoroutes, 80 km/h hors agglomération, 50 km/h dans les agglomérations. La ceinture de sécurité est obligatoire.</p>
<p>Le réseau routier est en bon état et tous les véhicules circulent sans problème. Les véhicules sont climatisés et la majorité dispose d’une boîte de vitesses automatique.</p>
<h3 class="spip"><a id="sommaire_4"></a>Assurances et taxes</h3>
<p>Il existe une assurance d’Etat obligatoire (Hova) qui couvre les dommages corporels et aux tiers (environ 2000 shekels soit 400€ par an). Il est conseillé de la compléter par une assurance tous-risques (entre 2000 et 6000 shekels soit 400 à 1200€ par an) laquelle comprend une franchise pour les dommages aux véhicules. L’installation d’une alarme sonore est exigée dans tous les cas, et parfois celle d’un dispositif électronique de positionnement. De plus, de nombreuses compagnies d’assurance imposent que le véhicule soit équipé d’un anti-démarrage codé, faute de quoi elles refusent de l’assurer contre le vol.</p>
<h3 class="spip"><a id="sommaire_5"></a>Achat et location</h3>
<p>Toutes les principales marques françaises et étrangères sont représentées mais tous les modèles ne sont pas disponibles. Les prix des véhicules neufs restent généralement très supérieurs aux prix français en raison de lourdes taxes à l’importation.</p>
<p>On peut facilement louer un véhicule, de nombreuses agences de location existent : Avis, Budget, Rent-a-car, Eldan… Elles proposent des véhicules de toutes catégories à des prix concurrentiels : environ 45 euros par jour pour une berline. Une caution importante est généralement bloquée sur la carte de crédit, il est important de s’assurer qu’elle est bien levée lorsqu’on rend le véhicule à l’agence.</p>
<p>Les sociétés de location et leurs assurances refusent généralement que les véhicules loués se rendent dans les territoires palestiniens.</p>
<h3 class="spip"><a id="sommaire_6"></a>Immatriculation</h3>
<p>Seuls les véhicules homologués peuvent être immatriculés (achetés sur place ou homologués lors de leur importation, voir ci-dessus). Un contrôle technique anti-pollution annuel est obligatoire pour les véhicules de plus de deux ans.</p>
<h3 class="spip"><a id="sommaire_7"></a>Entretien</h3>
<p>Toutes les grandes marques ont leurs garages pour effectuer les révisions et les réparations, avec cependant un sérieux et une probité inégaux. Comme les pièces sont importées, les coûts de réparations ou d’entretien restent relativement élevés en raison de la forte taxation.</p>
<p>Il n’est conseillé d’importer son véhicule personnel que s’il n’est pas neuf (taxes calculées sur la base de la valeur estimée du véhicule) et que le modèle existe dans le pays. Il sera ainsi homologué et les pièces détachées seront facilement disponibles. Si le modèle est ancien ou non commercialisé, il faudra alors importer spécifiquement les pièces de France ou d’ailleurs, ce qui est une démarche longue, chère et compliquée.</p>
<h3 class="spip"><a id="sommaire_8"></a>Réseau routier</h3>
<p>Le réseau routier est globalement bon. La circulation peut parfois être dangereuse. Il n’existe pas de problèmes particuliers d’approvisionnement en carburants. Les panneaux de signalisation sont en hébreu et anglais (ainsi qu’en arabe dans certaines régions).</p>
<p>Le ministère des Transports a mis en place un plan quinquennal (2011-2015) afin de remédier aux embouteillages et ralentissements qui sont de plus en plus fréquents dans les villes. Outre l’ajout de 250 km au réseau existant et la création de 70 km de nouveaux tronçons, il prévoit la construction de 75 échangeurs, ponts et tunnels. Afin aussi de fluidifier le trafic, la vitesse maximale sera portée à 110km à l’heure sur autoroutes, contre 100 aujourd’hui.</p>
<h3 class="spip"><a id="sommaire_9"></a>Transports en commun</h3>
<p>Tel Aviv et Haïfa ont un métro (le Carmelit à Haïfa) mais le nombre de stations desservies est réduit. Des prolongements et constructions de lignes sont prévus jusqu’à 2020.</p>
<p><a href="http://www.nta.co.il/site/en/homepage.asp" class="spip_out" rel="external">Site officiel du métro à Tel Aviv</a></p>
<p>Les bus desservent l’ensemble du pays, mais le service est interrompu le samedi et les jours de fêtes juives. C’est le moyen de transport le moins coûteux. Pour plus de renseignements, consultez le <a href="http://www.bus.co.il/otobusim/Front2007/homepage.asp?LanguageID=20Design=2007" class="spip_out" rel="external">site officiel des transports publics en Israël</a>.</p>
<p>Le réseau ferré, moins dense que les lignes de bus, est aussi moins cher. Contrairement aux véhicules routiers, les trains circulent à gauche en Israël. Le réseau ferré israélien, centré sur Tel Aviv, comprend deux lignes longeant la côte : l’une en direction du nord vers Haïfa et Nahariya, l’autre vers le sud en direction d’Ashkelon, avec une antenne vers Rishon LeZion.</p>
<p>Pour plus de renseignements, consultez le <a href="http://www.rail.co.il/EN/Pages/HomePage.aspx" class="spip_out" rel="external">site officiel des chemins de fer israéliens</a></p>
<p>Les taxis sont répandus en Israël et assurent aussi bien les courses urbaines qu’interurbaines. Les Shirout (taxis collectifs de 7 à 10 places) font des liaisons interurbaines à un prix raisonnable. C’est souvent le seul moyen de transport disponible le samedi. Le prix d’un voyage est similaire à celui d’un trajet en bus. Contrairement aux restaurants, il n’est pas d’usage de laisser un pourboire aux chauffeurs de taxis.</p>
<p><strong>Source :</strong> Ambassade de France en Israël, Consulat général de France à Jérusalem, Chambre de commerce franco-israélienne , sites officiels indiqués, sites de tourisme.</p>
<p>Mise à jour : décembre 2013</p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/vie-pratique/article/transports-110473). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
