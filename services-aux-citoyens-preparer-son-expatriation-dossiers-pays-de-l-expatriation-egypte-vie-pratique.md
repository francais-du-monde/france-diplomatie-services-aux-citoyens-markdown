# Vie pratique

<h2 class="rub23158">Logement</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/vie-pratique/#sommaire_1">Conditions de location</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/vie-pratique/#sommaire_2">Hôtels</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/vie-pratique/#sommaire_3">Electricité</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/vie-pratique/#sommaire_4">Electroménager</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Conditions de location</h3>
<p>Les principaux quartiers résidentiels du Caire sont Maadi et Zamalek, très prisés par les expatriés, ainsi qu’Héliopolis, Mohandessin et Dokki.</p>
<p>Les principaux quartiers résidentiels d’Alexandrie sont Rusdhy, Agami et la rue des Pharaons en centre-ville.</p>
<p>Pour rechercher un logement, mai et juin sont les mois les plus favorables, septembre le mois le plus difficile. Il est préférable de passer par une agence immobilière. Les frais d’agence sont à la charge du propriétaire. On peut également interroger les concierges d’immeubles (<i>bawabs</i>), consulter les petites annonces dans les représentations françaises ou faire appel à un intermédiaire professionnel (<i>simsar</i>) rémunéré de l’équivalent d’un mois de loyer.</p>
<p>La durée du bail est généralement de deux ou trois ans. Le loyer peut être versé en livres égyptiennes, en dollars américains ou en euros. Il est souvent payable à l’avance et par trimestre. La caution correspond habituellement à un mois de loyer.</p>
<p>Il est recommandé d’établir le contrat de location en deux langues (anglais/arabe ou français/arabe) et de procéder à un état des lieux le plus complet possible, notamment en vérifiant bien l’état des installations (plomberie, électricité, climatisation, etc.).</p>
<p>Les charges comprennent l’eau et l’entretien de l’immeuble. Le chauffage est généralement nécessaire en hiver et la climatisation en été.</p>
<h3 class="spip"><a id="sommaire_2"></a>Hôtels</h3>
<p><strong>Auberges de jeunesse</strong></p>
<p><a href="https://www.hihostels.com/fr/destinations/eg/hostels?locale=fr" class="spip_out" rel="external">Association égyptienne des auberges de jeunesse</a><br class="manualbr">1, rue El Ibrahimy<br class="manualbr">Garden City - LE CAIRE</p>
<h3 class="spip"><a id="sommaire_3"></a>Electricité</h3>
<p>Le gaz coûte entre 10 et 20 euros par mois et l’électricité entre 50 et 100 euros par mois. Le courant électrique est, comme en France, alternatif, 220 volts - 50 Hz. Les appartements n’ont pas forcément de prise de terre.</p>
<h3 class="spip"><a id="sommaire_4"></a>Electroménager</h3>
<p>Les cuisines sont le plus souvent équipées (plaques de cuisson, four, réfrigérateur, lave-linge). Tous les équipements électroménagers sont disponibles localement mais ils sont de qualité médiocre. Les appareils importés sont très chers.</p>
<p>Il est nécessaire de climatiser l’été et de chauffer l’hiver. Peu d’immeubles sont équipés de chauffage central, mais un chauffage électrique d’appoint ou la climatisation en mode chauffage peuvent suffire.</p>
<p><i>Mise à jour : avril 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-egypte-vie-pratique-article-loisirs-et-culture.md" title="Loisirs et culture">Loisirs et culture</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-egypte-vie-pratique-article-communications-112203.md" title="Communications">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-egypte-vie-pratique-article-transports.md" title="Transports">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-egypte-vie-pratique-article-cout-de-la-vie-112201.md" title="Coût de la vie">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-egypte-vie-pratique-article-scolarisation-112200.md" title="Scolarisation">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-egypte-vie-pratique-article-sante.md" title="Santé">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-egypte-vie-pratique-article-logement.md" title="Logement">Logement</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/vie-pratique/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
