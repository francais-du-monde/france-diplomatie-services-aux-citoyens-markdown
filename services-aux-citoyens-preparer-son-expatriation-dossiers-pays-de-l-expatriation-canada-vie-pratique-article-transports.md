# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/vie-pratique/article/transports#sommaire_1">Importation de véhicule</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/vie-pratique/article/transports#sommaire_2">Permis de conduire</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/vie-pratique/article/transports#sommaire_3">Code de la route</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/vie-pratique/article/transports#sommaire_4">Assurances et taxes</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/vie-pratique/article/transports#sommaire_5">Achat et location</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/vie-pratique/article/transports#sommaire_6">Immatriculation</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/vie-pratique/article/transports#sommaire_7">Entretien</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/vie-pratique/article/transports#sommaire_8">Carburant</a></li>
<li><a id="so_9" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/vie-pratique/article/transports#sommaire_9">Réseau routier</a></li>
<li><a id="so_10" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/vie-pratique/article/transports#sommaire_10">Transports en commun</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Importation de véhicule</h3>
<p>Cet article ne traite que des conditions d’importation d’un véhicule en provenance de pays autres que le Canada et les Etats-Unis, des dispositions particulières s’appliquant pour ces deux pays.</p>
<p>Pour pouvoir être importé au Canada de façon permanente, un véhicule automobile doit être conforme aux Normes de sécurité des véhicules automobiles du Canada (NSVAC) en vigueur à la date de la fabrication du véhicule.</p>
<p><strong>Véhicules automobiles fabriqués selon les normes d’un pays autre que le Canada et les Etats-Unis</strong></p>
<p>Les véhicules fabriqués pour la vente dans des pays autres que le Canada et les États-Unis ne sont pas conformes aux exigences de la Loi sur la sécurité automobile du Canada. <strong>Ils ne peuvent pas être modifiés pour les rendre conformes et, par conséquent, ne peuvent pas être importés au Canada de façon permanente.</strong></p>
<ul class="spip">
<li>Les seules exceptions à cette règle sont :</li>
<li>les véhicules de 15 ans ou plus. L’âge du véhicule est calculé d’après le mois et l’année de sa fabrication.</li>
<li>les véhicules importés temporairement au Canada.</li>
<li>les véhicules conçus, fabriqués et certifiés conformes à toutes les normes de sécurité des véhicules automobiles du Canada ou aux <i>United States Federal Motor Vehicle Safety Standards</i> des États-Unis, à condition que le véhicule n’ait pas été modifié et que la certification du fabricant original soit maintenue.</li></ul>
<p><strong>Véhicules importés temporairement</strong></p>
<p>Un véhicule automobile peut être temporairement admis au Canada sans qu’il soit conforme au Règlement sur la sécurité des véhicules automobiles. Ces dispositions visent, entre autres, les cas suivants :</p>
<ul class="spip">
<li>les véhicules importés par les visiteurs pour une période ne dépassant pas 12 mois, par les résidents temporaires tels que les étudiants, pour la durée de leurs études au Canada, et par les personnes possédant un permis ou une autorisation de travail valide pour une période ne dépassant pas 36 mois ;</li>
<li>les véhicules importés par les diplomates, si ceux-ci ont reçu une autorisation écrite d’Affaires étrangères et Commerce international Canada pour la durée de leur affectation au Canada.</li></ul>
<p><strong>Attention</strong></p>
<p>Le véhicule admis à l’importation temporaire ne peut être ni vendu, ni aliéné pendant la durée du séjour au Canada. Il doit également être exporté à l’expiration du visa de travail ou d’études. Si vous changez de statut au cours de votre séjour au Canada, le véhicule devra être importé de façon permanente s’il est admissible (voir plus haut) ou être exporté s’il n’est pas admissible.</p>
<p><strong>Droits et taxes</strong></p>
<p>Les véhicules importés à des fins personnelles et fabriqués au Etats-Unis, au Canada ou au Mexique sont généralement exemptés du paiement de droits.</p>
<p>Les véhicules fabriqués dans les pays autres que ceux mentionnés au paragraphe précédent sont soumis au paiement de droits et taxes, calculés en fonction de la valeur en douane du véhicule. Celle-ci se base sur le prix du véhicule tel qu’il est indiqué sur la facture et avant déduction des réductions qu’a pu vous accorder le vendeur.</p>
<p>Les droits de douane s’élèvent actuellement à 6,1 %.</p>
<p>Une taxe d’accise de 100 CAD est exigée pour les véhicules munis d’un climatiseur, à laquelle peut venir s’ajouter un " écoprélèvement " si votre véhicule consomme au moins 13 litres au 100 et a été mis en service après le 19 mars 2007. Le montant de ce prélèvement est fonction de la consommation du véhicule en énergie et varie entre 1 000 et 4 000 CAD.</p>
<p>Pour plus d’information sur l’écoprélèvement, vous pouvez consulter le site de l’<a href="http://www.cra-arc.gc.ca/" class="spip_out" rel="external">Agence du Revenu du Canada</a>.</p>
<p>Vous devrez également régler la taxe sur les produits et services (TPS) de 5%. A celle-ci vient s’ajouter, pour la Nouvelle Ecosse, le Nouveau Brunswick et Terre-Neuve et Labrador, la taxe de vente harmonisée (TVH) de 8%.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.tc.gc.ca/" class="spip_out" rel="external">Transports Canada</a><br class="manualbr">Importation de véhicules<br class="manualbr">Place de Ville - Tour C - 330 rue Sparks <br class="manualbr">Ottawa (Ontario) K1A 0N5<br class="manualbr">Téléphone : [1] (613) 998 86 16 <br class="manualbr">Téléphone sans frais : [1] 800 333 03 71 <br class="manualbr">Télécopie : [1] (613) 998 48 31</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.cra-arc.gc.ca/" class="spip_out" rel="external">Agence du revenu du Canada</a><br class="manualbr">Direction générale des voyageurs<br class="manualbr">Édifice Sir Richard Scott - 14ème étage - 191 avenue Laurier Ouest 6 <br class="manualbr">Ottawa (Ontario) K1A 0L5<br class="manualbr">Téléphone : [1] 800 461 99 99 <br class="manualbr">Téléphone de l’étranger : [1] (204) 983 35 00 / [1] (506) 636 50 64 <br class="manualbr">Télécopie : [1] (613) 998 55 84 </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.cbsa-asfc.gc.ca/" class="spip_out" rel="external">Agence des services frontaliers du Canada (ASFC)</a><br class="manualbr">Service d’Information à la frontière (SIF)<br class="manualbr">Appels gratuits à l’intérieur du Canada : [1] 800 959 20 36 <br class="manualbr">Appels de l’étranger : [1] (204) 983 37 00 / [1] (506) 636 50 67</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Registraire des véhicules importés (RIV)</strong><br class="manualbr">405 the West Mall - 5ème étage <br class="manualbr">Toronto (Ontario) M9C 5K7<br class="manualbr">Appels gratuits à partir du Canada : [1] 888 848 82 40 <br class="manualbr">Téléphone de l’étranger : [1] (416) 626 68 12</p>
<h3 class="spip"><a id="sommaire_2"></a>Permis de conduire</h3>
<p>La délivrance des permis de conduire est au Canada de la compétence des provinces et des territoires du lieu de résidence.</p>
<p>Votre permis de conduire français ne sera valable que pour une courte période suivant votre arrivée au Canada (en général 3 mois). Vous devez donc procéder à son échange ou passer les épreuves du permis de conduire dans les plus brefs délais.</p>
<p>Les provinces suivantes ont passé un accord avec la France et procèdent à l’échange du permis de conduire français :</p>
<ul class="spip">
<li>l’Alberta (échange limité aux permis de catégorie B)</li>
<li>la Colombie britannique (échange limité aux permis de catégorie B)</li>
<li>l’Ile du Prince Edward (échange limité aux permis de catégorie B)</li>
<li>le Nouveau-Brunswick</li>
<li>l’Ontario (échange limité aux permis des catégories B et E (B))</li>
<li>les comtés nord et sud du Québec (échange limité aux permis de catégorie B).</li></ul>
<p>Pour les autres provinces, vous devrez vous soumettre aux épreuves du permis de conduire de votre lieu de résidence. Ces épreuves peuvent comprendre un examen pratique et un examen théorique sur le Code de la route.</p>
<p>A noter qu’il est illégal au Canada de conduire sans permis de conduire en cours de validité. Le permis de conduire canadien a une validité limitée et doit, par conséquent, être renouvelé régulièrement. Vous devez toujours l’avoir sur vous lorsque vous conduisez.</p>
<p>Enfin, les compagnies d’assurance exigent souvent la présentation d’un permis de conduire canadien avant d’accepter d’assurer un véhicule.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li>Site <a href="http://www.cic.gc.ca/francais/index.asp" class="spip_out" rel="external">Se rendre au Canada</a> du Gouvernement canadien.</li>
<li><a href="http://www.tc.gc.ca/" class="spip_out" rel="external">Transports Canada</a>. <br class="manualbr">Vous trouverez dans cette page une liste de liens vers les services compétents pour chaque province et territoire.</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Code de la route</h3>
<p>La conduite s’effectue à droite. Le port de la ceinture de sécurité est obligatoire pour le conducteur et les passagers.</p>
<p>Le Code de la route diffère d’une province à l’autre. Il est par conséquent conseillé de s’informer sur les règles locales de circulation. Pour connaître les règles en vigueur dans la province ou le territoire de votre lieu de résidence, vous pouvez consulter les sites Internet indiqués ci-dessous.</p>
<p>Les limites de vitesse sont affichées en kilomètres à l’heure. Elles indiquent aux conducteurs la vitesse la plus sûre à laquelle rouler le jour dans de bonnes conditions de circulation. En cas de dépassement de la limite de vitesse, vous serez passible d’une amende. Celle-ci aura une incidence sur votre dossier de conduite et augmentera le coût de votre assurance.</p>
<p>Les limites de vitesse approximatives au Canada sont de :</p>
<ul class="spip">
<li>100 km/h sur les grandes autoroutes ;</li>
<li>80 km/h sur les routes secondaires ;</li>
<li>60 km/h sur les routes urbaines fréquentées ;</li>
<li>50 km/h sur les rues résidentielles ou les routes qui traversent de petites villes ;</li>
<li>30 km/h près des écoles et des terrains de jeux.</li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Assurances et taxes</h3>
<p>Un véhicule automobile, neuf ou d’occasion, est soumis aux taxes suivantes :</p>
<ul class="spip">
<li>taxe de vente,</li>
<li>taxe fédérale,</li>
<li>taxe provinciale</li>
<li>vignette. Vous devez détenir une carte grise. Le véhicule doit obligatoirement être assuré au tiers et disposer d’une couverture responsabilité civile importante. Plusieurs facteurs influent sur le coût de la</li></ul>
<p>police d’assurance : l’âge et les antécédents routiers du conducteur, l’usage du véhicule, l’absence de contraventions au code de la route, etc.</p>
<p>L’assurance au tiers illimitée peut dépasser les 500 CAD par mois suivant le véhicule.</p>
<p>Il est préférable de souscrire une assurance tous risques. En effet, en cas d’accident, les conducteurs n’hésitent pas à porter l’affaire devant les tribunaux. De plus, les compagnies d’assurance tiennent rarement compte des antécédents du conducteur en dehors du Canada et réclament, par conséquent, la première année des primes d’assurance très élevées, identiques à celles d’un jeune conducteur. Il faut compter au minimum 1 500 CAD par an pour une assurance tous risques. Il est important de bien se renseigner sur la zone géographique (provinciale et territoriale) couverte par l’assurance.</p>
<p>Il est recommandé, avant de choisir une compagnie d’assurance, de se renseigner auprès de plusieurs assureurs et de comparer les prix et les prestations offertes.</p>
<h3 class="spip"><a id="sommaire_5"></a>Achat et location</h3>
<p><strong>Achat</strong></p>
<p>Toutes les marques nord-américaines et japonaises sont représentées au Canada. Parmi les marques européennes, il existe des concessionnaires pour Audi, BMW, Mercedes-Benz, Porsche, Volkswagen et Volvo. Aucune marque française n’est disponible.</p>
<p>Il existe un marché des voitures d’occasion chez les garagistes ou par annonces. Les véhicules d’occasion de grosse cylindrée américaine restent les moins chers.</p>
<p>La cession d’un véhicule nécessite un contrôle technique préalable.</p>
<p>A titre d’exemple, l’Ontario exige que les particuliers qui vendent leur véhicule remettent à l’acheteur une trousse d’information sur les véhicules d’occasion appelée TIVO. Cette trousse contient l’historique de l’enregistrement du véhicule, les différents propriétaires, les dettes existantes et les crédits contractés, le kilométrage, le prix à l’argus (<i>Red Book</i>) qui déterminera le montant minimal de la taxe, mais également des conseils destinés à l’acheteur, des renseignements sur l’inspection des normes de sécurité du véhicule, sur la taxe de vente, des fiches pratiques de constat et enfin le contrat de ventre. La TIVO est obligatoire en cas de transaction entre particuliers. Elle coûte 20 CAD et est en vente au Bureau d’immatriculation et de délivrance des permis de conduire. C’est auprès de ce service que l’acheteur devra ensuite se rendre, muni de cette trousse, pour le transfert d’enregistrement du véhicule.</p>
<p><strong>Quelques conseils</strong></p>
<p>Avant d’acheter un véhicule, il est plus prudent de le confier à un mécanicien qualifié et indépendant afin de déceler d’éventuels défauts techniques.</p>
<p>Les consommateurs pourront, en vertu de la Loi sur les sûretés mobilières, savoir si le vendeur ou un précédent propriétaire a contracté un prêt et si ce dernier conserve des droits sur le véhicule d’occasion. Il ne faut jamais conclure la vente sans avoir la preuve formelle du remboursement intégral du prêt.</p>
<p>Enfin, à partir du numéro d’identification du véhicule (NIV), généralement inscrit par le fabricant sur le tableau de bord et reporté également sur le certificat d’immatriculation du véhicule, il est possible de connaître l’historique complet du véhicule à une date donnée : le nom du propriétaire, la description du véhicule, la catégorie, le statut, la masse à vide dans le cas des véhicules utilitaires et des remorques, la marque, la déclaration du véhicule, les détails de la plaque d’immatriculation, le numéro de la vignette de validation, la déclaration, les dates de début et d’expiration de la validation, le poids brut enregistré dans le cas des véhicules utilitaires, le numéro du permis de conduire du propriétaire et la date de délivrance, etc.</p>
<p><strong>Location</strong></p>
<p>Au Canada, la location de véhicules de tourisme y compris de camping-cars (<i>trailers</i>) est très répandue et le service fourni est de qualité.</p>
<p>De nombreuses compagnies nationales et internationales proposent des véhicules, généralement avec boîte automatique, à la location avec d’intéressantes offres spéciales, notamment pour les week-ends. Les compagnies de location se trouvent dans les aéroports et dans toutes les villes et les villages importants.</p>
<p>Les tarifs varient selon la saison, le type de véhicule et la durée de la location. Vous devez être âgé d’au moins 21 ans (25 ans dans certains cas) et posséder une carte de crédit internationale. Les règlements peuvent différer d’une province à une autre.</p>
<h3 class="spip"><a id="sommaire_6"></a>Immatriculation</h3>
<p>L’immatriculation des véhicules relève, au Canada, de la compétence des provinces et des territoires. Les compagnies d’assurance, dont vous trouverez les coordonnées en consultant les pages jaunes canadiennes peuvent également vous renseigner sur les questions d’immatriculation des véhicules.</p>
<h3 class="spip"><a id="sommaire_7"></a>Entretien</h3>
<p>L’entretien et les réparations mécaniques sont de bonne qualité à condition qu’ils soient effectués dans un garage et par un professionnel. A noter que les pièces détachées pour un véhicule français sont plus difficiles à trouver.</p>
<p>Vous trouverez les coordonnées de détaillants en pièces détachées dans les <a href="http://www.yellowpages.ca/" class="spip_out" rel="external">pages jaunes canadiennes</a> en indiquant comme critère de recherche " automobiles-pièces et accessoires ".</p>
<h3 class="spip"><a id="sommaire_8"></a>Carburant</h3>
<p>Le prix de l’essence au Canada a connu récemment une forte hausse. Il varie toutefois selon les provinces et les villes. Il est généralement plus élevé à Vancouver qu’à Calgary, Montréal ou encore Toronto.</p>
<p>Ces disparités s’expliquent par le fait que depuis 1985, les prix du pétrole et du carburant ne sont plus réglementés au niveau fédéral. Certaines provinces (Ile du Prince Edouard, Terre-Neuve et Labrador, Québec, Nouvelle Ecosse et Nouveau Brunswick) ont cependant décider de continuer à réglementer ces prix.</p>
<p>Les différences de prix sont principalement attribuables à quatre facteurs : les taxes, la concurrence et les choix des consommateurs, la quantité d’essence vendue, ainsi que le type et l’emplacement des stations-service.</p>
<p>Les différentes taxes provinciales et municipales demeurent cependant le facteur le plus important qui influe sur le prix à la pompe. Il existe deux types de taxes :</p>
<p><strong>La taxe fixe </strong> : au niveau fédéral, une taxe d’accise de 10 cents le litre sur l’essence et de 4 cents le litre sur le carburant diesel et une taxe provinciale qui varie considérablement selon les provinces. Les villes de Vancouver, Victoria et Montréal, quant à elles, ajoutent respectivement des taxes de 6 cents, 3,5 cents et 1,5 cent le litre d’essence.</p>
<p><strong>La taxe de vente </strong> : la taxe sur les produits et les services (TPS) s’élèvent à 5 % sur tous les produits pétroliers, y compris l’essence. En outre, certaines provinces imposent une taxe de vente supplémentaire sur les produits pétroliers.</p>
<h3 class="spip"><a id="sommaire_9"></a>Réseau routier</h3>
<p><strong>Sécurité</strong></p>
<p>En hiver (d’octobre à avril), les conditions météorologiques (neige et températures très basses) causent la mort de nombreux automobilistes. L’hiver, il faut être prudent lorsque l’on se rend dans les régions peu habitées. Il est recommandé d’avoir toujours avec soi une trousse d’urgence afin de pouvoir faire face aux imprévus : une pelle, une trousse de secours, une lampe, des bottes, un grattoir à neige, une couverture de survie, etc. Certaines routes pouvant être fermées l’hiver, il est recommandé de toujours se renseigner sur les conditions de circulation avant de prendre la route.</p>
<p>Adhérer à une association ou à une assurance de dépannage est indispensable. La "Canadian Automobile Association " (CAA) est un organisme qui vient en aide aux automobilistes dans tous leurs problèmes quotidiens. Par exemple, vous pourrez faire appel à ses services pour être dépanné. La cotisation annuelle est fonction du niveau de protection choisi..</p>
<p><strong>Quelques conseils pour se déplacer en hiver</strong></p>
<ul class="spip">
<li>Conduisez prudemment ;</li>
<li>Rebroussez chemin ou abritez-vous si les conditions météorologiques se détériorent ;</li>
<li>Restez sur les routes principales autant que possible ;</li>
<li>Voyagez le jour et accompagné ;</li>
<li>Gardez la radio allumée et écoutez les prévisions et les alertes météorologiques ;</li>
<li>Ayez toujours un réservoir plein et ajoutez de l’antigel à essence chaque fois que vous faites le plein ;</li>
<li>Rajoutez de l’antigel, du liquide de transmission, du liquide de frein et de l’eau pour le lave-glace ;</li>
<li>Veillez à l’entretien de votre véhicule ;</li>
<li>Equipez votre véhicule de quatre pneus neige répondant aux normes canadiennes ;</li>
<li>Assurez-vous que la valve des pneus est équipée d’un capuchon empêchant la neige et la glace de s’y infiltrer ;</li>
<li>Gardez toujours une trousse d’urgence d’hiver dans votre auto.</li></ul>
<p><strong>En cas d’accident</strong></p>
<p>En cas d’accident, <strong>vous devez appeler le 911</strong>.</p>
<p>Vous devez obligatoirement appeler la police dans les cas suivants :</p>
<ul class="spip">
<li>des personnes sont blessées ;</li>
<li>l’un des conducteurs est vraisemblablement en infraction au Code criminel (par exemple, conduite en état d’ébriété) ;</li>
<li>les dommages matériels sont importants.</li></ul>
<p><strong>Il est conseillé</strong></p>
<ul class="spip">
<li>de noter tous les détails relatifs à l’accident ;</li>
<li>de prendre les coordonnées (nom et prénoms, adresse, numéro de téléphone) de toutes les personnes impliquées dans l’accident (conducteurs, passagers, témoins) ;</li>
<li>d’obtenir les renseignements sur l’assurance des conducteurs impliqués (numéros du permis de conduire, de la police d’assurance et des plaques d’immatriculation).</li>
<li>Il pourra être utile de faire un croquis du lieu de l’accident et d’avoir en permanence dans son véhicule un appareil photo jetable.</li></ul>
<p><strong>Etat du réseau routier</strong></p>
<p>Le réseau routier est en bon état et compte de nombreuses routes et autoroutes gratuites reliant les principaux centres urbains.</p>
<p>Pour connaître l’état des routes, vous pouvez appeler les numéros suivants :</p>
<ul class="spip">
<li>Colombie britannique : [1] (800) 550 49 97</li>
<li>Nouveau Brunswick : [1] (800) 561 40 63</li>
<li>Ontario : [1] (800) 268 46 86 ou 416 235 46 86</li></ul>
<p>ou consulter les sites Internet suivants :</p>
<ul class="spip">
<li><a href="http://www.tc.gc.ca/" class="spip_out" rel="external">Transports Canada</a>.</li>
<li><a href="http://www.securitepublique.gc.ca/index-fra.aspx" class="spip_out" rel="external">Sécurité publique Canada</a></li></ul>
<h3 class="spip"><a id="sommaire_10"></a>Transports en commun</h3>
<p>Pour les déplacements à l’intérieur du pays, les Canadiens utilisent leur véhicule personnel, l’autocar pour les trajets relativement courts (desserte Ottawa-Toronto-Québec-Montréal) ou encore l’avion qui, compte tenu des distances, est le mode de transport le plus utilisé. Les vols sont nombreux et à l’heure, sauf en cas de conditions météorologiques défavorables.</p>
<p>Le réseau ferroviaire est peu développé et peu rapide et, par conséquent, moins utilisé. Pour en savoir plus, vous pouvez consulter le site Internet du réseau <a href="http://www.viarail.ca/" class="spip_out" rel="external">Viarail</a>.</p>
<p>En ville, à Toronto, Vancouver, Moncton et Montréal, les moyens de transport utilisés sont le métro, l’autobus, le taxi et les deux roues lorsque les conditions climatiques le permettent. La voiture demeure un mode de transport urbain très développé.</p>
<p><strong>Informations par province et par territoire</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Alberta</strong>
<br>— <a href="http://www.servicealberta.gov.ab.ca/" class="spip_out" rel="external">Service Alberta</a>
<br>— <a href="http://www.transportation.alberta.ca/" class="spip_out" rel="external">Ministère des Transports / Ministère de l’Infrastructure</a>
<br>— <a href="http://www.saferoads.com/" class="spip_out" rel="external">Traffic safety in Alberta</a></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Colombie britannique</strong>
<br>— <a href="http://www.icbc.com/Pages/default.aspx" class="spip_out" rel="external">Insurance corporation of British Columbia (ICBC)</a> </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Ile du Prince Edward</strong>
<br>— <a href="http://www.gov.pe.ca/" class="spip_out" rel="external">Gouvernement de l’Ile du Prince Edward</a></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Manitoba</strong>
<br>— <a href="http://www.gov.mb.ca/" class="spip_out" rel="external">Province du Manitoba</a>
<br>— <a href="http://www.mpi.mb.ca/en/Pages/default.aspx" class="spip_out" rel="external">Société d’assurance publique du Manitoba</a> </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Nouveau Brunswick</strong>
<br>— <a href="http://www.snb.ca/" class="spip_out" rel="external">Service Nouveau Brunswick</a></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Nouvelle Ecosse</strong>
<a href="http://www.novascotia.ca/snsmr/access/french-languages-services-fr.asp" class="spip_out" rel="external">Gouvernement de la Nouvelle Ecosse</a></p>
<p><i>Mise à jour : avril 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/vie-pratique/article/transports). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
