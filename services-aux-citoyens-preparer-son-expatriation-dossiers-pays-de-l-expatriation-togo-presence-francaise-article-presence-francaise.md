# Présence française

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/presence-francaise/article/presence-francaise#sommaire_1">Ambassade et consulat de France</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/presence-francaise/article/presence-francaise#sommaire_2">Autorités françaises dans le pays</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/presence-francaise/article/presence-francaise#sommaire_3">Associations dans le pays</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Ambassade et consulat de France</h3>
<p>Consulter notre <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/article/annuaire-des-representations-francaises-a-l-etranger" class="spip_in">annuaire des ambassades et consulats de France à l’étranger</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Autorités françaises dans le pays</h3>
<p><strong>Réseau français de coopération et d’action culturelle</strong></p>
<p>Les coordonnées des services du réseau de coopération et d’action culturelle français au Togo ainsi que la description de l’activité de ces services se trouvent dans la rubrique Coopération française du site internet de l’<a href="http://www.ambafrance-tg.org/" class="spip_out" rel="external">Ambassade</a> ou sur le site internet du réseau culturel : <a href="http://latitudefrance.diplomatie.gouv.fr/AnnuaireCarto.html" class="spip_out" rel="external">Latitude France</a>.</p>
<p><strong>Economie : réseau mondial</strong></p>
<p>Le service économique est en charge des questions liées à la vie économique et commerciale togolaise. A ce titre, elle assure notamment l’information des investisseurs français potentiels sur l’état du marché et les conditions d’implantation dans le pays.</p>
<ul class="spip">
<li><a href="https://www.tresor.economie.gouv.fr/Pays/togo" class="spip_out" rel="external">Site internet du service économique régional au Togo</a></li>
<li>Voir la rubrique <a href="http://www.ambafrance-tg.org/-presence-economique-" class="spip_out" rel="external">Présence économique</a> sur le site internet de l’Ambassade de France au Togo.</li></ul>
<p><strong>Vos élus à l’Assemblée des Français de l’étranger</strong></p>
<p>Pour toute information sur l’Assemblée des Français de l’étranger et pour connaître les conseillers et les Sénateurs qui représentent les Français établis hors de France dans votre circonscription, vous pouvez consulter les sites Internet suivants :</p>
<ul class="spip">
<li><a href="http://www.assemblee-afe.fr/" class="spip_out" rel="external">Assemblée des Français de l’étranger (AFE)</a></li>
<li>Sénat au service des Français de l’étranger : <a href="http://www.senat.fr/expatries/dossiers_pays/togo.html" class="spip_out" rel="external">dossier spécifique au Togo</a> sur le site du Sénat</li></ul>
<p><strong>Députés des Français de l’étranger</strong></p>
<p>La révision constitutionnelle du 23 juillet 2008, a introduit la représentation à l’Assemblée nationale des Français établis hors de France en créant onze circonscriptions législatives à l’étranger. Le Togo fait partie de la 10ème circonscription</p>
<p>Pour en savoir plus :</p>
<ul class="spip">
<li><a href="http://www.assemblee-nationale.fr/" class="spip_out" rel="external">Assemblée nationale</a></li>
<li><a href="http://www.votezaletranger.com" class="spip_out" rel="external">Votez à l’étranger</a></li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Associations dans le pays</h3>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.ambafrance-tg.org/-Associations-de-Francais-" class="spip_out" rel="external">Associations françaises au Togo</a></p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/presence-francaise/article/presence-francaise). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
