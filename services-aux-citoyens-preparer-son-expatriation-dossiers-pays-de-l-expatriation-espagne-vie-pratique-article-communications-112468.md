# Communications

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/vie-pratique/article/communications-112468#sommaire_1">Téléphone – Internet</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/vie-pratique/article/communications-112468#sommaire_2">Poste</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Téléphone – Internet</h3>
<p>Les <strong>liaisons téléphoniques</strong> entre l’Espagne et la France sont bonnes.</p>
<p>Pour effectuer un appel en Espagne depuis l’étranger, composez le 00, puis le 34 (indicatif de l’Espagne) et enfin le numéro de téléphone composé de neuf chiffres. Pour appeler à l’étranger depuis l’Espagne, composez le 00, l’indicatif du pays, puis le numéro de téléphone. Vous pouvez passer vos appels d’une cabine téléphonique. Les cabines fonctionnent avec des pièces ou des cartes que l’on peut acheter dans les bureaux de tabac.</p>
<p>Pour effectuer un appel national en Espagne, il vous suffit de composer le numéro sans aucun indicatif. Ce numéro possède toujours 9 chiffres, qu’il s’agisse d’un téléphone fixe ou portable.</p>
<p>Les principaux opérateurs de téléphonie mobile sont Orange, Movistar, Vodafone. La liste des tarifs des opérateurs est disponible auprès de la Commission du marché des télécommunications (CMT).</p>
<p>Les clients reçoivent un message automatique les informant du coût des appels, des SMS et des services de transmission de données à l’étranger (en cas de navigation sur internet ou de téléchargement d’un film, p. ex.). Les prix de gros que les opérateurs se facturent mutuellement sont également plafonnés, ce qui devrait réduire les coûts supportés par les consommateurs. Un mécanisme a également été mis en place afin d’éviter aux consommateurs de devoir payer des factures astronomiques pour des services d’itinérance : après un premier avertissement, la connexion internet mobile est suspendue dès que le montant atteint un certain niveau.</p>
<p>Vous trouverez sur un <a href="http://ec.europa.eu/information_society/activities/roaming/index_en.htm" class="spip_out" rel="external">site web de l’UE</a> (en anglais) les eurotarifs appliqués par les opérateurs des 28 pays membres, ainsi que les liens vers leurs sites. Vous pourrez également y comparer les tarifs pratiqués pour l’envoi de SMS ou l’utilisation de services de données.</p>
<p><i>Source : <a href="http://europa.eu/travel/comm/index_fr.htm#phone" class="spip_out" rel="external">Europa.eu</a></i></p>
<p>Les <strong>liaisons Internet</strong> fonctionnent bien.</p>
<p>Les abonnements pour l’accès à internet à domicile restent toutefois relativement chers par rapport à la France.</p>
<p>Si vous désirez accéder à Internet pour consulter votre courrier électronique, vous trouverez différents établissements offrant ce service : téléphones publics, cybercafés, etc. Des connexions Internet sont également disponibles dans les aéroports, les principales gares de train et d’autobus et dans certains centres commerciaux. En général, ces services sont à pièces.</p>
<p>Voir aussi : <a href="http://www.tv5.org/TV5Site/voyageurs/accueil.php" class="spip_out" rel="external">TV5.org</a></p>
<p><strong>Téléphoner gratuitement par Internet</strong></p>
<p>Il est généralement très avantageux d’utiliser <strong>des logiciels de téléphonie sur IP</strong> (Skype, Google talk, Live messenger, Yahoo ! messenger, etc.) car les tarifs proposés à l’international sont souvent moindre que ceux des opérateurs locaux exploitant des lignes classiques, voire gratuits dans certaines conditions.</p>
<h3 class="spip"><a id="sommaire_2"></a>Poste</h3>
<p>Les liaisons postales entre l’Espagne et la France sont bonnes, comptez un délai de quatre jours environ.</p>
<p>Pour envoyer un colis, effectuer un mandat ou envoyer un télégramme, il vous faudra vous rendre dans un bureau de poste (Correos). Ces bureaux offrent aussi d’autres services – fax ou burofax par exemple. Le site de la <a href="http://www.correos.es/dinamic/plantillas/home1.asp" class="spip_out" rel="external">Poste</a> vous fournira une information complète sur ce type d’envois.</p>
<p><i>Mise à jour : avril 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/vie-pratique/article/communications-112468). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
