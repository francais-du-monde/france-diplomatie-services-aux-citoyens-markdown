# Emploi, stage

<h2 class="rub23527">Marché du travail</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/emploi-stage-23527/#sommaire_1">Contexte</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/emploi-stage-23527/#sommaire_2">Secteurs à fort potentiel</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/emploi-stage-23527/#sommaire_3">Secteurs à faible potentiel</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/emploi-stage-23527/#sommaire_4">Rémunération</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Contexte</h3>
<p>Le marché du travail est caractérisé par une informalité élevée, composée de travailleurs indépendants et de micro-entreprises (moins de 20 travailleurs). Le secteur informel représenterait 65 % de la population occupée.</p>
<p>Selon les dernières statistiques disponibles de l’Organisation Internationale du Travail (OIT) datant de 2010, la population en âge de travailler (15-64 ans) représenterait 64 % de la population totale. 35 % de la population active serait considérée comme étant au chômage.</p>
<p>Près de 40 % de la population travailleraient dans le secteur agricole, 25 % dans les activités commerciales, 15 % dans les services, 11 % dans la production industrielle, 7 % comme artisans indépendants et 2 % dans l’administration publique.</p>
<h3 class="spip"><a id="sommaire_2"></a>Secteurs à fort potentiel</h3>
<p>Tous les secteurs sont accessibles à un ressortissant étranger. Les Français sont le plus souvent employés dans la coopération ou sont travailleurs indépendants.</p>
<p>Le pays cherche aujourd’hui à placer l’investissement étranger au centre de son modèle économique notamment à travers la mise en place des zones économiques intégrées (ZEI). Haïti dispose d’un grand potentiel pour attirer les investissements notamment dans le secteur manufacturier (textile via l’accès préférentiel au marché américain) et de l’agro-industrie (proximité du marché US et accès préférentiel au marché européen accordé pour les pays ACP).</p>
<p>Les échanges commerciaux entre la France et Haïti sont très faibles. Il existe cependant un potentiel important à exploiter : les produits français bénéficient d’un avantage non négligeable sur le marché haïtien puisque la haute société conserve toujours des habitudes de consommation proches de celles des français (par exemple le beurre contre la margarine dans le reste des Caraïbes).</p>
<p>Surnommée la « perle des Antilles », Haïti souhaiterait devenir un pôle touristique majeur des Caraïbes à l’image de la République dominicaine. A moyen terme, le secteur du tourisme pourrait devenir l’un des piliers de l’économie haïtienne.</p>
<p>Enfin, les besoins en matière d’infrastructures sont immenses : eau, électricité, traitement des déchets, transport publics, urbanisme, etc.</p>
<h3 class="spip"><a id="sommaire_3"></a>Secteurs à faible potentiel</h3>
<p>Le marché du travail dans sa globalité offre peu d’opportunités pour des niveaux de rémunération comparables aux standards français et ne permet pas l’accès à une santé et une éducation pouvant satisfaire toutes les exigences européennes.</p>
<p>En revanche les entrepreneurs dotés d’une spécialité ou d’une compétence originale peuvent trouver des niches profitables.</p>
<p>Parmi les secteurs à faible rémunération, on peut mentionner l’agriculture, la construction, l’industrie.</p>
<h3 class="spip"><a id="sommaire_4"></a>Rémunération</h3>
<p>Les salaires minimums de référence pour une journée de 8 heures se situeront, selon les branches d’activités, entre 125 et 300 gourdes (arrêté présidentiel du 16 avril 2014).</p>
<p>Il n’existe pas de statistiques sur le salaire moyen en Haïti.</p>
<p><i>Mise à jour :  août 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-haiti-emploi-stage-23527-article-lettre-de-motivation-114253.md" title="Lettre de motivation">Lettre de motivation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-haiti-emploi-stage-23527-article-curriculum-vitae-114252.md" title="Curriculum vitae">Curriculum vitae</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-haiti-emploi-stage-23527-article-reglementation-du-travail-114250.md" title="Réglementation du travail">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-haiti-emploi-stage-23527-article-marche-du-travail-114249.md" title="Marché du travail">Marché du travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/emploi-stage-23527/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
