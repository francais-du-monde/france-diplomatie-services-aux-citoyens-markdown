# Emploi, stage

<h2 class="rub22993">Marché du travail</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/emploi-stage/#sommaire_1">Secteurs à fort potentiel</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/emploi-stage/#sommaire_2">Professions règlementées</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/emploi-stage/#sommaire_3">Rémunération</a></li></ul>
<p>L’Iran connaît un taux de chômage généralement supérieur à 10%. Encore ceci ne rend-il pas compte du sous-emploi, ni du nombre d’employés qui conservent un contrat de travail mais ne sont plus rémunérés, ni de ceux qui travaillent dans le secteur informel. Le taux d’emploi des femmes est particulièrement bas, alors qu’elles constituent désormais la majorité des jeunes diplômés.</p>
<p>Dans ces conditions l’emploi des étrangers est très contraint. Il est indispensable d’obtenir un permis de travail spécifique, délivré pour un an par le ministère du travail, qui doit également autoriser l’employeur à embaucher un étranger. La procédure est simplifiée quand l’embauche d’un étranger est liée à un investissement direct, lorsque le demandeur justifie de 10 ans de présence continue en Iran ou est le conjoint d’un ressortissant iranien.</p>
<h3 class="spip"><a id="sommaire_1"></a>Secteurs à fort potentiel</h3>
<p>Compte tenu des sanctions actuellement en vigueur, aucun secteur ne peut être considéré comme  « à fort potentiel » à court terme. Les entreprises françaises sont présentes notamment dans l’industrie agro-alimentaire et dans l’automobile. Par le passé elles ont également eu une forte présence dans le secteur pétrolier et gazier.</p>
<h3 class="spip"><a id="sommaire_2"></a>Professions règlementées</h3>
<p>Les secteurs stratégiques ne sont pas accessibles aux ressortissants étrangers.</p>
<h3 class="spip"><a id="sommaire_3"></a>Rémunération</h3>
<p>Cf. article <a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-iran-fiscalite-article-convention-fiscale.md" class="spip_in">Convention fiscale</a>.</p>
<p><i>Mise à jour : février 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-iran-emploi-stage-article-stages.md" title="Stages">Stages</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-iran-emploi-stage-article-recherche-d-emploi-111118.md" title="Recherche d’emploi">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-iran-emploi-stage-article-reglementation-du-travail-111117.md" title="Réglementation du travail">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-iran-emploi-stage-article-marche-du-travail-111116.md" title="Marché du travail">Marché du travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/emploi-stage/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
