# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/emploi-stage/article/reglementation-du-travail-109845#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/emploi-stage/article/reglementation-du-travail-109845#sommaire_2">Contrat de travail – spécificités</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/emploi-stage/article/reglementation-du-travail-109845#sommaire_3">Fêtes légales</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/emploi-stage/article/reglementation-du-travail-109845#sommaire_4">Emploi du conjoint</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<p>Le droit du travail japonais (Loi n° 49 du 07/04/1947 relative aux normes de travail) impose à toute entreprise de 10 salariés ou plus d’établir un règlement intérieur qui régit les conditions de travail et doit être déposé auprès de l’Inspection du travail. Tout nouvel employé se verra proposer une notification des conditions de travail reprenant les principaux éléments du règlement intérieur ainsi que l’ensemble des conditions dérogatoires au règlement. Seules les conditions plus favorables par rapport au règlement intérieur sont valables.</p>
<p>Il doit normalement exister un règlement intérieur par catégories d’employés et en l’absence de règlement intérieur spécifique, ce seront les conditions générales qui s’appliqueront.</p>
<p>La notification des conditions de travail doit notamment mentionner :</p>
<ul class="spip">
<li>le lieu de travail</li>
<li>les horaires de travail et de repos, les congés et les vacances</li>
<li>le salaire ainsi que la méthode de calcul des bonus, primes</li>
<li>les conditions de résiliation du contrat y compris la retraite et le montant du pécule de départ payé éventuellement par l’entreprise.</li></ul>
<p>Il est fait référence à ce règlement intérieur (proche de la Convention collective française) dans la lettre d’embauche (Rôdôjôkentsûchisho).</p>
<h4 class="spip">Principales conditions de travail</h4>
<ul class="spip">
<li>Les horaires sont de 40 heures par semaine ;</li>
<li>L’âge de départ à la retraite est graduellement repoussé à 65 ans (avril 2013) ;</li>
<li>Les congés annuels sont de 10 jours/an pour atteindre un maximum de 20 jours après six ans et demi d’ancienneté. Habituellement, ces congés sont pris lors des deux périodes habituelles de Shogatsu (début d’année) et d’Obon (mi-août) ;</li>
<li>Les jours fériés annuels sont au nombre de 15 ;</li>
<li>Des congés spéciaux (mariage, naissance, décès …) sont également accordés par coutume ;</li>
<li>Le préavis en cas de licenciement ou de démission est de 30 jours, ou à défaut l’équivalent en salaire et doit être déclaré au bureau de l’Inspection du travail compétent géographiquement.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Contrat de travail – spécificités</h3>
<p>Un <strong>contrats à durée déterminée</strong> peut être conclu pour une durée maximale de trois ans. Cette durée maximale peut être étendue à cinq ans dans deux cas précis :</p>
<ul class="spip">
<li>Besoins en qualifications particulières ou développement de nouveaux produits, technologies ou services ;</li>
<li>Emploi de salariés de plus de 60 ans.</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Fêtes légales</h3>
<ul class="spip">
<li>1er janvier (Nouvel an)</li>
<li>2ème lundi de janvier (Fête des nouveaux adultes)</li>
<li>11 février (anniversaire de la fondation de l’Etat)</li>
<li>21 mars (Equinoxe de printemps)</li>
<li>29 avril (Fête de la nature)</li>
<li>3 mai (Jour de la Constitution)</li>
<li>4 mai (Jour du Peuple)</li>
<li>5 mai (Jour des enfants)</li>
<li>3ème lundi de juillet (Fête de la mer)</li>
<li>3ème lundi de septembre (Journée des personnes âgées)</li>
<li>23 septembre (Equinoxe d’automne)</li>
<li>2ème lundi d’octobre (Jour du sport et de la santé)</li>
<li>3 novembre (Jour de la culture)</li>
<li>23 novembre (Jour du travail)</li>
<li>23 décembre (Anniversaire de l’Empereur)</li></ul>
<p>Il y a 15 jours fériés au Japon. Si l’un d’eux tombe un dimanche, le jour suivant est considéré comme férié. S’il n’y a qu’un jour entre deux jours fériés, ce jour-là est aussi considéré comme férié. Le 25 décembre n’est pas férié. Entre le 29 décembre et le 3 janvier, administrations et musées sont fermés.</p>
<h3 class="spip"><a id="sommaire_4"></a>Emploi du conjoint</h3>
<p>Seuls les conjoints <strong>mariés</strong> peuvent obtenir un titre de séjour.</p>
<p>Pour les personnes ne parlant pas japonais, il est très difficile de trouver un emploi.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <i>Cas des conjoints français de ressortissants japonais</i> : </p>
<p>La réglementation japonaise n’impose aucune limitation aux activités que les titulaires du statut de "conjoint de Japonais" sont autorisés à exercer au Japon.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <i>Cas des conjoints (non-japonais) de ressortissants français</i> : </p>
<p>Le visa de conjoint « Dependant Visa » n’autorise pas à travailler. Cependant, il est possible de demander une dérogation, auprès du bureau d’immigration local (Nyukokukanrikyoku), pour pouvoir travailler dans la limite de 28h par semaine. A noter que ceci est une tolérance et non un droit.</p>
<p><a href="http://www.immi-moj.go.jp/english/index.html" class="spip_out" rel="external">Bureau japonais d’immigration</a> <br class="manualbr">Tél. : 03-5796-7112</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/emploi-stage/article/reglementation-du-travail-109845). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
