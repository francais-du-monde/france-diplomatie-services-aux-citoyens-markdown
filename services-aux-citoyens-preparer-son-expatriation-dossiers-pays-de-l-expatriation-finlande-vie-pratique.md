# Vie pratique

<h2 class="rub22348">Logement</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/vie-pratique/#sommaire_1">Où se loger ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/vie-pratique/#sommaire_2">Conditions de location</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/vie-pratique/#sommaire_3">Electricité</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/vie-pratique/#sommaire_4">Electroménager</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Où se loger ?</h3>
<p><strong>Helsinki</strong></p>
<p><strong>Loyer mensuel dans un quartier résidentiel</strong> (euros)</p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Type de logement</td>
<td>Prix</td></tr>
<tr class="row_even even">
<td>Studio</td>
<td>800-1000</td></tr>
<tr class="row_odd odd">
<td>3 pièces</td>
<td>1200-2000</td></tr>
<tr class="row_even even">
<td>5 pièces</td>
<td>3000 et +</td></tr>
</tbody>
</table>
<p><strong>Loyer mensuel dans la banlieue</strong> (euros)</p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Type de logement</td>
<td>Prix</td></tr>
<tr class="row_even even">
<td>Studio</td>
<td>600-800</td></tr>
<tr class="row_odd odd">
<td>3 pièces</td>
<td>1000-1200</td></tr>
<tr class="row_even even">
<td>5 pièces</td>
<td>1200-3000</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_2"></a>Conditions de location</h3>
<p>Le marché locatif est assez important. La consultation des sites internet, ainsi qu’une annonce dans les journaux locaux sont des moyens efficaces pour trouver un appartement. Une caution pouvant représenter un à trois mois de loyer peut être réclamée par le propriétaire, qui, comme en France, s’entoure de toutes les garanties possibles.</p>
<p>A Helsinki, les principaux quartiers résidentiels sont les suivants :</p>
<ul class="spip">
<li>en ville : Kaivopuisto à environ 1,5 km du centre ville et 100 mètres de la mer ou à Eira à 1 km du centre ville.</li>
<li>A l’extérieur d’Helsinki : Lauttasaari à 4 km du centre ville, Kulosaari et Munkkiniemi à 5 km du centre ville, Haukilahti, Espoo et Westend à 10 km du centre ville.</li></ul>
<p>Il est assez facile de trouver à Helsinki et dans les alentours des appartements non meublés, comprenant une cuisine équipée, et de superficie correcte. Les villas se trouvent dans certains quartiers très résidentiels et le loyer en est généralement plus élevé. Les logements vides sont plus fréquents que les meublés.</p>
<p>Il faut, en moyenne, compter un délai de trois semaines, selon les exigences, pour trouver un logement.</p>
<p>La durée généralement admise des baux est d’une année avec tacite reconduction, mais il est possible d’obtenir des baux de trois années voire plus. Le propriétaire peut demander à récupérer son bien pour son usage personnel trois mois avant la fin du bail. Le versement de deux mois de loyer d’avance est en principe demandé. En général, il n’est pas demandé d’état des lieux mais il est préférable, pour éviter les surprises, d’en établir un dès l’arrivée et d’en envoyer copie au propriétaire.</p>
<p>L’eau, le chauffage ainsi que le gardiennage sont inclus dans le loyer. Les dépenses d’électricité, non incluses dans le loyer, sont fonction de leur utilisation et dépendent du contrat d’électricité. L’électricité est relativement peu chère en Finlande. Les impôts locaux ou fonciers ainsi que les taxes sur les ordures ménagères sont à la charge du propriétaire. Le coût de la location d’un garage est en général inférieur à 200€ par mois. Dans certains quartiers comme Kaivopuisto ou Kallio, il peut atteindre le double ou le triple.</p>
<h3 class="spip"><a id="sommaire_3"></a>Electricité</h3>
<p>Le courant est de 220 volts et les prises identiques aux prises françaises. Il est souhaitable de prévoir un humidificateur de l’air.</p>
<h3 class="spip"><a id="sommaire_4"></a>Electroménager</h3>
<p>L’équipement ménager est disponible sur place à des prix identiques à ceux pratiqués en France. La gamme proposée est très variée. Les cuisines sont généralement équipées.</p>
<p><i>Mise à jour : janvier 2015</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-finlande-vie-pratique-article-loisirs-et-culture.md" title="Loisirs et culture">Loisirs et culture</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-finlande-vie-pratique-article-communications.md" title="Communications">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-finlande-vie-pratique-article-transports.md" title="Transports">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-finlande-vie-pratique-article-cout-de-la-vie.md" title="Coût de la vie">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-finlande-vie-pratique-article-scolarisation.md" title="Scolarisation">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-finlande-vie-pratique-article-sante.md" title="Santé">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-finlande-vie-pratique-article-logement.md" title="Logement">Logement</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/vie-pratique/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
