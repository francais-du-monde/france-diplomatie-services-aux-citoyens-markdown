# La procédure de recouvrement de créances alimentaires à l’étranger

<p>En qualité d’autorité centrale, le bureau RCA est à la fois autorité requérante et autorité requise.</p>
<p>En effet, si la personne créancière réside en France et que la personne débitrice réside à l’étranger, le bureau RCA est <strong>autorité requérante</strong>. Dans ce cas, son rôle consiste à apporter son soutien à la constitution du dossier puis à l’adresser à l’autorité centrale de l’Etat dans lequel réside le débiteur.</p>
<p>Si la personne créancière réside à l’étranger et que le débiteur réside en France, le bureau RCA, qui est <strong>autorité requise</strong>, agit en deux temps que l’on appelle phase amiable et phase judiciaire.</p>
<p>La phase amiable consiste à tenter d’obtenir du débiteur le paiement volontaire de la créance alimentaire. Le bureau RCA saisit le Procureur de la République qui fait procéder à une enquête de personnalité.</p>
<p>En cas d’échec de la phase amiable, le bureau RCA met en œuvre la phase judiciaire ; celle-ci consiste à obtenir le caractère exécutoire en France de la décision de justice (procédure d’exequatur ou reconnaissance de la force exécutoire de la décision) et à saisir un huissier de justice pour le recouvrement forcé de la créance, notamment par le biais de saisie des biens et avoirs du débiteur.</p>
<p><i>Mise à jour : juin 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/conseils-aux-familles/recouvrement-de-creances/article/la-procedure-de-recouvrement-de). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
