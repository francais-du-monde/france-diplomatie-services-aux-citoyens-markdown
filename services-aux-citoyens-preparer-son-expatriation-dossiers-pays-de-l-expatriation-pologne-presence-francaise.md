# Présence française

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/presence-francaise/#sommaire_1">Ambassade et consulat de France</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/presence-francaise/#sommaire_2">Autorités françaises dans le pays</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/presence-francaise/#sommaire_3">Associations dans le pays </a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/presence-francaise/#sommaire_4">Associations franco-polonaises </a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Ambassade et consulat de France</h3>
<p>Consulter notre <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/article/annuaire-des-representations-francaises-a-l-etranger" class="spip_in">Annuaire des représentations françaises à l’étranger</a> .</p>
<h3 class="spip"><a id="sommaire_2"></a>Autorités françaises dans le pays</h3>
<p><strong>Réseau français de coopération et d’action culturelle</strong></p>
<p>Les coordonnées des services du réseau de coopération et d’action culturelle français en Pologne ainsi que la description de l’activité de ces services se trouvent dans la rubrique culture du site internet de l’<a href="http://www.ambafrance-pl.org/" class="spip_out" rel="external">Ambassade</a> ou sur le site internet du réseau culturel : <a href="http://latitudefrance.diplomatie.gouv.fr/AnnuaireCarto.html" class="spip_out" rel="external">Latitude France</a>.</p>
<p><strong>Economie : réseau mondial</strong></p>
<p>Business France, l’Agence française pour le développement international des entreprises, est présente en Pologne. Sa mission est d’accompagner les entreprises françaises dans leur développement à l’international.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Site de la <a href="http://export.businessfrance.fr/pologne/export-pologne-avec-notre-bureau.html" class="spip_out" rel="external">mission économique Business France en Pologne</a></p>
<p>Les services économiques sont également présents. Ils sont une émanation de la Direction générale du Trésor et ont pour missions principales l’analyse macroéconomique et l’animation des relations économiques bilatérales entre la France et les pays où ils sont implantés.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Site internet du <a href="https://www.tresor.economie.gouv.fr/Pays/pologne" class="spip_out" rel="external">service économique français en Pologne</a></p>
<p><strong>Vos élus à l’Assemblée des Français de l’étranger</strong></p>
<p>Pour toute information sur l’Assemblée des Français de l’étranger et pour connaître les conseillers et les Sénateurs qui représentent les Français établis hors de France dans votre circonscription, vous pouvez consulter les sites Internet suivants :</p>
<ul class="spip">
<li><a href="http://www.assemblee-afe.fr/" class="spip_out" rel="external">Assemblée des Français de l’étranger (AFE)</a></li>
<li>Sénat au service des Français de l’étranger : <a href="http://www.senat.fr/expatries/dossiers_pays/pologne.html" class="spip_out" rel="external">dossier spécifique à la Pologne</a> sur le site du Sénat.</li></ul>
<p><strong>Députés des Français de l’étranger</strong></p>
<p>La révision constitutionnelle du 23 juillet 2008, publiée au Journal officiel du 19 avril 2011, introduit la représentation à l’Assemblée nationale des Français établis hors de France. La création de onze circonscriptions législatives à l’étranger permet aux Français expatriés d’élire leur député à l’Assemblée nationale à partir du scrutin de 2012.</p>
<p><strong>Pour plus d’information : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.votezaletranger.gouv.fr/" class="spip_out" rel="external">Votez à l’étranger</a> </p>
<h3 class="spip"><a id="sommaire_3"></a>Associations dans le pays </h3>
<p><strong>Associations françaises</strong></p>
<p><a href="http://adfe.org/pologne/links.php?lng=fr" class="spip_out" rel="external">Association démocratique des Français à l’étranger - Français du monde (ADFE -FdM)</a><br class="manualbr">Ul. Brosniewskiego 66/34 <br class="manualbr">00854 Varsovie<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/presence-francaise/#a_guillet#mc#yahoo.fr#" title="a_guillet..åt..yahoo.fr" onclick="location.href=mc_lancerlien('a_guillet','yahoo.fr'); return false;" class="spip_mail">Courriel</a></p>
<p><strong>Union des Français de l’Etranger (UFE)</strong></p>
<ul class="spip">
<li><i>Section de Varsovie et de Cracovie :</i><br class="manualbr">Ul. Zaolzianska 3m29 - 02781 Varsovie<br class="manualbr">Téléphone : [48] 22 50 50 120 <br class="manualbr">Télécopie : [48] 22 50 50 400<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/presence-francaise/#ufe#mc#op.pl#" title="ufe..åt..op.pl" onclick="location.href=mc_lancerlien('ufe','op.pl'); return false;" class="spip_mail">Courriel</a></li>
<li><i>Section de Gdynia</i> :<br class="manualbr">Ul.Wroclawska 82 - 81530 Gdynia<br class="manualbr">Téléphone : [48] 58 664 64 44/45 <br class="manualbr">Télécopie : [48] 58 664 64 44/45<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/presence-francaise/#pol.gdynia#mc#ufe.asso.fr#" title="pol.gdynia..åt..ufe.asso.fr" onclick="location.href=mc_lancerlien('pol.gdynia','ufe.asso.fr'); return false;" class="spip_mail">Courriel</a></li></ul>
<p><strong>Accueils</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.cracovieaccueil.pl/" class="spip_out" rel="external">Cracovie Accueil</a><br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/presence-francaise/#cracovieaccueil#mc#gmail.com#" title="cracovieaccueil..åt..gmail.com" onclick="location.href=mc_lancerlien('cracovieaccueil','gmail.com'); return false;" class="spip_mail">Courriel</a></p>
<p>L’objectif de ces deux dernières associations est d’accueillir et d’aider les Français et les personnes francophones arrivant en Pologne.</p>
<p>De plus, elle a une publication mensuelle avec des informations diverses sur ses activités et distribue à ses adhérents un guide d’adresses utiles et des recommandations dans de nombreux domaines (mode de vie, possibilités de se loger, coût, ainsi que tout autre élément de nature pratique).</p>
<h3 class="spip"><a id="sommaire_4"></a>Associations franco-polonaises </h3>
<p><strong>Association France - Pologne pour l’Europe</strong><br class="manualbr">Ul. L. Narbutta 27 m.1 <br class="manualbr">02536 Varsovie<br class="manualbr">Téléphone : [48] 22 881 04 16 <br class="manualbr">Télécopie : [48] 22 646 33 23<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/presence-francaise/#wwwa#mc#fpenet.org#" title="wwwa..åt..fpenet.org" onclick="location.href=mc_lancerlien('wwwa','fpenet.org'); return false;" class="spip_mail">Courriel</a></p>
<p><a href="http://www.tppf.krakow.pl/" class="spip_out" rel="external">Association d’amitié Pologne - France à Cracovie</a><br class="manualbr">Al. Daszynskiego 7 <br class="manualbr">30 537 Cracovie <br class="manualbr">Téléphone / Télécopie : [48] 12 421 28 23 <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/presence-francaise/#tppf#mc#tppf.krakow.pl#" title="tppf..åt..tppf.krakow.pl" onclick="location.href=mc_lancerlien('tppf','tppf.krakow.pl'); return false;" class="spip_mail">Courriel</a></p>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li>Notre article <a href="services-aux-citoyens-preparer-son-expatriation-en-savoir-plus-article-associations-des-francais-de-l-etranger.md" class="spip_in">Associations des Français de l’étranger</a></li>
<li>Vous pouvez également consulter le site Internet de l’<a href="http://www.ambafrance-pl.org/Associations" class="spip_out" rel="external">Ambassade de France en Pologne</a>.</li></ul>
<p><i>Mise à jour : juin 2014</i></p>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/presence-francaise/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
