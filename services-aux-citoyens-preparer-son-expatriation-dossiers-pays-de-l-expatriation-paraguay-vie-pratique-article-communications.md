# Communications

<h4 class="spip">Téléphone – Internet</h4>
<h4 class="spip">Téléphone</h4>
<p><strong>Indicatifs</strong></p>
<ul class="spip">
<li>depuis la France vers le Paraguay : 00 595 + numéro à 8 chiffres</li>
<li>depuis le Paraguay vers la France : 00 2 33 + le numéro d’abonné, en ôtant le "0" de l’indicatif régional.</li></ul>
<p><strong>Tarifs </strong></p>
<ul class="spip">
<li>appel national : environ 0,02 €/mn</li>
<li>Mercosur (Argentine, Bolivie, Brésil, Chili, Uruguay) : environ 0,20 €/mn</li>
<li>reste du continent américain : environ 0,20 €/mn</li>
<li>reste du monde : environ 0,20 €/mn</li></ul>
<h4 class="spip">Internet</h4>
<p>L’accès au réseau Internet est possible, mais aucun service de "haut débit" n’est disponible et le nombre d’abonnés reste encore très limité.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Voir aussi : <a href="http://www.tv5.org/TV5Site/voyageurs/accueil.php" class="spip_out" rel="external">TV5.org</a></p>
<p>Téléphoner gratuitement par Internet Il est généralement très avantageux d’utiliser <strong>des logiciels de téléphonie sur IP</strong> (Skype, Google talk, Live messenger, Yahoo ! messenger, etc.) car les tarifs proposés à l’international sont souvent moindre que ceux des opérateurs locaux exploitant des lignes classiques, voire gratuits dans certaines conditions.</p>
<h4 class="spip">Poste</h4>
<p>Les liaisons postales entre le Paraguay et la France fonctionnent correctement mais les tarifs sont assez élevés.</p>
<p>Il est préférable de demander une boîte postale (<i>casilla de correo</i>) pour la réception du courrier.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/paraguay/vie-pratique/article/communications). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
