# Entretien d’embauche

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/emploi-stage/article/entretien-d-embauche#sommaire_1">Conduite de l’entretien</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/emploi-stage/article/entretien-d-embauche#sommaire_2">Apparence et attitude</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/emploi-stage/article/entretien-d-embauche#sommaire_3">Négociation du salaire</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/emploi-stage/article/entretien-d-embauche#sommaire_4">Questions préférées des recruteurs</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/emploi-stage/article/entretien-d-embauche#sommaire_5">Erreurs à éviter</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/emploi-stage/article/entretien-d-embauche#sommaire_6">Après l’entretien</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Conduite de l’entretien</h3>
<p>Le recruteur examinera le candidat sur les aspects suivants :</p>
<ul class="spip">
<li>La qualification, la formation</li>
<li>La capacité à exécuter le travail et à faire avancer les objectifs de l’entreprise</li>
<li>La capacité à s’intégrer dans l’entreprise et dans la culture d’entreprise</li>
<li>La capacité à s’exprimer, à prouver sa motivation et à réagir lors d’un entretien stratégique.</li></ul>
<p>Préparez-vous à l’entretien en recherchant des informations sur l’entreprise à travers son site web. Le recruteur sera impressionné si vous êtes capable de démontrer une connaissance des activités de l’entreprise. Vous poserez des questions intelligentes et pertinentes par rapport à la société et saurez rebondir plus facilement aux explications puisque vous saisirez d’emblée la portée de ses propos.</p>
<p>Si le recruteur ne vous voit pas, il vous évaluera par rapport à votre « entretien téléphonique » : la clarté de votre discours, le ton de votre voix, la compréhension de ses propos et le contenu de vos réponses.</p>
<p>Des guides sont disponibles en librairie, destinés à aider les candidats à préparer l’entretien d’embauche en anglais. Ils répertorient les questions généralement posées aux candidats, les points essentiels pour se présenter ainsi que les questions pièges et les réponses à apporter dans ce cas.</p>
<p><strong>Quelques conseils pour les entretiens en « face à face » ou téléphoniques :</strong></p>
<ul class="spip">
<li>Préparez votre entretien (recherchez des informations sur l’entreprise : produits vendus, chiffres clés, organigramme, principaux concurrents, fournisseurs et clients, politique d’entreprise…et sur le poste convoité).</li>
<li>Re-listez vos réalisations, vos connaissances et expériences afin de vous rafraîchir la mémoire.</li>
<li>Préparez des questions pertinentes mais aussi anticipez les questions qui pourront vous êtes posées et préparez vos réponses.</li>
<li>Entraînez-vous avec un ami pour avoir un avis sur la qualité de votre voix et de votre discours. Soyez clair et concis.</li>
<li>Ayez sous la main : votre CV, votre lettre de motivation, une copie de l’offre du poste, les points principaux concernant l’entreprise, les points que vous voulez aborder.</li>
<li>Evitez les termes techniques qui ne feraient pas partie du vocabulaire de votre interlocuteur.</li>
<li>Soyez prêts à illustrer vos réussites antérieures par des exemples concrets.</li>
<li>Proposez vos services ultérieurs afin de donner des informations supplémentaires ou de répondre à d’autres questions.</li>
<li>Demandez à votre interlocuteur quand il vous donnera une réponse : s’il va vous appeler ou s’il faut que vous le rappeliez. Remerciez-le d’avoir pris du temps pour parler avec vous : « Thank you for your time », « nice talking to you ».</li>
<li>Envoyez rapidement une lettre de remerciement.</li></ul>
<h4 class="spip">Le niveau d’anglais</h4>
<p>Il est nécessaire que vous possédiez un bon niveau d’anglais. Si votre niveau est faible, prenez des cours et préparez votre entretien. N’essayez pas d’improviser.</p>
<p>Le <i>British Council</i> est un établissement internationalement reconnu, qui est notamment installé à Singapour. La plupart des expatriés souhaitant se perfectionner en anglais font appel aux services de cette organisation. Les cours et notes obtenues sont reconnues par le milieu des affaires. Il existe par ailleurs d’autres établissements de langues installés sur Singapour.</p>
<p>Il est très bien perçu de fournir ses résultats aux examens des TOEFL, TOEIC, IAELTS et CAMBRIDGE. Votre niveau d’anglais sera alors reconnu d’emblée et ceci vous donnera un atout supplémentaire pour réussir votre entretien et prouver votre faculté d’adaptation dans un pays anglophone.</p>
<h4 class="spip">L’entretien téléphonique</h4>
<p>Ce type d’entretien est courant pour les étudiants étrangers et les diplômés récents. Il est par ailleurs souvent utilisé pour des recrutements à l’étranger. Dans ce cas, les premiers contacts se font en général par courriel et téléphone. Si le candidat est favorablement perçu, le recruteur demandera à le rencontrer lors d’un entretien final et décisif. Il est parfois possible de mettre en place des vidéos-conférences pour les candidats venant de l’étranger mais en général, le recruteur cherche quand même à le rencontrer avant de signer le contrat, si la procédure de recrutement est positive.</p>
<p>Considérez l’entretien téléphonique comme un entretien en face à face.</p>
<p>Suivez les conseils énoncés au paragraphe précédent. Ils s’appliquent à tous les types d’entretiens. De plus, veillez à :</p>
<ul class="spip">
<li>Etre installé dans une pièce calme avec un téléphone qui fonctionne bien</li>
<li>Fermer les fenêtres</li>
<li>Ne pas être dérangé</li></ul>
<p><strong>Remarques :</strong></p>
<p>Ayez confiance, ne vous donnez pas d’excuse, les employeurs préfèrent les attitudes positives, l’assurance, l’optimisme.</p>
<p>Les capacités de communication sont importantes. Vous devez paraître plaisant, intéressé et intéressant. Vous pouvez poliment demander à votre interlocuteur de parler plus doucement, de répéter ou d’expliquer une question : « What do you mean by… », « When you say…you mean that… ».</p>
<p>Parlez clairement de votre autorisation de travail (dans le cadre d’un stage ET/OU d’un emploi).</p>
<h4 class="spip">La question du visa</h4>
<p>Avant de passer votre entretien, renseignez-vous sur les visas que vous pouvez obtenir, leurs durées d’obtention et leurs coûts. Très succinctement :</p>
<ul class="spip">
<li>Si vous êtes déjà sur Singapour, expliquez sous quel statut vous résidez sur le territoire.</li>
<li>S’il s’agit d’un <i>Dependant Pass</i>, insistez sur le fait que vous pouvez travailler avec un <i>Letter Of Consent</i>, que les démarches administratives sont inexistantes et que vous êtes d’ores et déjà éligible pour travailler à Singapour.</li>
<li>Si vous n’avez pas encore de visa ou uniquement un visa Touriste <i>Social Pass</i>, précisez-le afin que d’emblée l’entreprise vous dise si elle est prête à vous sponsoriser pour l’obtention du permis de travail <i>Employment Pass</i>. Dans tous les cas (sauf si vous n’avez besoin que de la <i>Letter Of Consent</i>), la demande de permis de travail se fera par l’entreprise qui vous recrutera et qui ainsi deviendra votre sponsor local.</li></ul>
<p>La société peut éventuellement payer votre visa mais cela n’est pas une obligation. Pour avoir de plus amples renseignements, consultez le site Internet suivant :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.mom.gov.sg/" class="spip_out" rel="external">Ministry of Manpower</a></p>
<h4 class="spip">La prise de rendez-vous</h4>
<p>L’objet du rendez-vous doit être clairement défini selon les questions suivantes : qui êtes-vous, qui voulez-vous rencontrer, pourquoi, pendant combien de temps ? Le rendez-vous doit être confirmé la veille.</p>
<h3 class="spip"><a id="sommaire_2"></a>Apparence et attitude</h3>
<p>L’aspect soigné et professionnel est de rigueur. L’impression que le recruteur aura de vous est également basée sur le visuel. Si vous passez un entretien en face à face, soignez particulièrement votre apparence.</p>
<p>Restez assez classique, sans extravagance. Singapour est une « cité-business », la tenue costume/tailleur est la plus répandue. Mais il n’y a pas de code vestimentaire pré-établi. Tout dépend aussi du type de poste recherché : si vous êtes dans le design, l’art ou la mode, il faut pouvoir identifier votre côté créatif.</p>
<p>Dans la culture anglo-saxonne, le comportement et l’attitude sont privilégiés : soyez positif. Faites attention à votre apparence, à la façon dont vous vous tenez :</p>
<ul class="spip">
<li>épaules en arrière et tête relevée montrent que vous êtes confiants.</li>
<li>Maintenez toujours un comportement positif et énergique.</li>
<li>Souriez, donnez des « feedbacks » non verbaux à votre interlocuteur.</li></ul>
<p>Adaptez votre discours sur celui du recruteur (volume, rapidité).</p>
<h4 class="spip">La rencontre</h4>
<p>La première règle à respecter est la ponctualité, les Singapouriens y sont très sensibles. Le début d’une rencontre se caractérise toujours par un échange de cartes de visite. La carte doit être tendue à deux mains vers votre interlocuteur. Lorsque votre interlocuteur vous donne à son tour sa carte, regardez la avec attention, adressez-vous à votre interlocuteur en employant son titre suivi de son nom de famille. Enfin, il ne faut jamais couper la parole à son interlocuteur, même en cas de désaccord.</p>
<h4 class="spip">Le concept de « face à face »</h4>
<p>Il est très important pour un recruteur de ne jamais « perdre la face ». De même, vous ne devez pas perdre la face dans la mesure où cela peut vous discréditer aux yeux de votre interlocuteur. Perdre la face peut notamment s’associer au fait de vous emporter ou de perdre patience.</p>
<h3 class="spip"><a id="sommaire_3"></a>Négociation du salaire</h3>
<p>Dans le cadre d’un emploi ou d’un stage, vous devez anticiper les questions concernant votre salaire ou vos indemnités. Le recruteur pourra vous demander : Combien gagniez-vous lors de votre précédent emploi ? Quelles sont vos prétentions salariales ?</p>
<p>Pour formuler une réponse appropriée, ceci présuppose de prendre en compte plusieurs éléments :</p>
<ul class="spip">
<li>A Singapour, on ne parle pas de salaire brut ou net. Le salaire est NET. Il n’y pas de charges retirées.</li>
<li>Comment sont rémunérées les personnes à ce type de poste ?</li>
<li>S’agit-il d’un fixe, d’un fixe + commissions, d’une avance sur commissions ? (Assurez-vous de connaître le contexte dans lequel ces personnes étaient rémunérées : s’agissait-il d’un stage, d’un travail d’été, d’un travail à temps complet ou à mi-temps ?)</li>
<li>Tenez compte des autres variables : ce poste était-il accompagné d’avantages tels qu’une voiture, des remboursements de frais, des bonus, d’une couverture médicale, d’un billet A/R pour la France …</li></ul>
<p>Il faut savoir qu’en plus des indemnités de stage d’autres bénéfices peuvent être négociés (le logement par exemple).</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  La société peut éventuellement payer votre visa mais cela n’est pas une obligation.</p>
<p>Dans le cadre d’un stage, être rémunéré n’est en aucun cas une obligation, l’entreprise peut vous payer mais elle n’est pas tenue de le faire. En moyenne, les entreprises françaises installées à Singapour rémunèrent leurs stagiaires entre 1000 et 1500 SGD.</p>
<p>Si vous n’êtes pas rémunéré, certains frais peuvent être pris en charge par l’entreprise (loyer, voiture, transport…).</p>
<p><strong>Rappel </strong> : il n’y a pas de salaire minimum à Singapour.</p>
<h3 class="spip"><a id="sommaire_4"></a>Questions préférées des recruteurs</h3>
<p>Vous pouvez répondre à une question désarmante (<i>illegal question</i>), vous êtes libre de le faire, mais souvenez-vous que donner des informations qui ne sont pas en relation avec l’emploi pour lequel vous postulez, peuvent réduire vos chances de l’obtenir si vous donnez la « mauvaise » réponse.</p>
<p>Vous pouvez refuser de répondre aux questions, c’est dans votre droit. Malheureusement, cela dépendra de la formulation de votre refus ; vous courez le risque d’être perçu comme une personne non coopérante ou qui se confronte facilement à la hiérarchie.</p>
<p>Vous pouvez examiner les intentions se cachant derrière la question et donner une réponse qui peut s’appliquer à l’emploi pour lequel vous êtes candidat. Par exemple, à des questions telles que : « Etes-vous <i>Permanent Resident</i> ? » ou « De quel pays venez-vous ? », vous pouvez répondre : « Avec mon statut, il m’est possible de travailler à Singapour ».</p>
<p><strong>Les questions préférées des recruteurs :</strong></p>
<p>1. What do you see yourself doing five years from now ?</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Cette question peut vous être posée afin de déterminer si vous vous épanouirez dans ce nouvel emploi ou si vous postulez en attendant de trouver mieux ailleurs.</p>
<p>2. How do you make yourself indispensable to a company ?</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Une réponse évoquant vos compétences techniques et interpersonnelles est attendue.</p>
<p>3. What’s your greatest strength ?</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Ne parlez pas simplement de votre point fort relatif au job, laissez le recruteur savoir que vous êtes un candidat qualifié.</p>
<p>4. What’s your greatest weakness ?</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Présentez vos faiblesses mais aussi les stratégies que vous mettez en place afin de les contrecarrer. Par exemple, vous pouvez dire : “I’m not the most organized but, I always answer my e-mails and phone calls right away. I’m aware of the problem and I have strategies to deal with it.”</p>
<p>5. Tell me about a time when your course load was heavy. How did you complete all your work ?</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Les recruteurs attendent une réponse comme : “Last semester I was taking 21 credits, so I made sure I had a day planner and mapped out all my assignments”. Vous devez être amené à décrire une situation dans laquelle une tâche est à accomplir, les actions mises en place pour réaliser cette tâche et le résultat de ces actions.</p>
<p>6. Tell me about a time when you had to accomplish a task with someone who was particularly difficult to get along with.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Vous devez démontrer votre sensibilité aux besoins des autres mais aussi que vous pouvez toujours les influencer, y faire face. Ne répondez pas : “I just avoided them” or “They made me cry.”</p>
<p>7. How do you accept direction and, at the same time, maintain a critical stance regarding your ideas and values ?</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Evoquez vos expériences passées pour répondre à cette question.</p>
<p>8. What are some examples of activities and surroundings that motivate you ?</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Parlez des travaux d’équipe, que le fait de motiver d’autres personnes vous motive…</p>
<p>9. Tell me how you handled an ethical dilemma.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  “Suppose you worked at a bank and a long-time customer wanted a check cashed right away but didn’t have the fund balance in his account to cover the check. Explain that if the bank’s policy prohibited cashing checks in that manner, the teller would have a choice of violating bank policy or alienating a good customer.” La meilleure des politiques reste d’avertir un responsable, d’expliquer la situation et de demander conseil.</p>
<p>10. Tell me about a time when you had to resolve a problem with no rules or guidelines in place. On veut tester votre capacité à surmonter les obstacles dans l’urgence, sans aide.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Vous pouvez évoquer un problème relatif à la vente.</p>
<p>11. Give me the three main reasons why we should give you this job.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  As I already manage a …. with real success I can certainly demonstrate that I can do the job. In addition, I am good at coming up with and acting on new ideas.<br class="manualbr">For example I ….. team skills are one of my real strengths. It is so heavy when several different groups of professionals are involved for work situations to become divise or competitive in a unhelpul way. I am good at fostering understanding and cooperation between different work groups. On top of those reasons, I am really enthusiastic about this post, I would put a great deal of energy into it.</p>
<p>12. Can you say NO ?</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  If I think I have been asked to do something that is really beyond my responsibility or remit, or if I could do it but not within the time frame that is expected, then I will say no. It is better than agreeing to do something and then finding that you don’t deliver or that you have to spend time renegociating or getting behind with other work. People who have worked with me in the past have knowledge that I only say NO when I really do have a good reason, not just to avoid work or to be unhelpful. I am always open to discussing something and trying to help find a resolution to a situation though.</p>
<p>13. If we asked a friend of yours to describe your character, what would they say ?</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  I think they would say I am easy to get on with and quite out-going. They would say I have a good sense of humour and I am generally cheerful. I think they would also say I am quite good in a crisis. I am often the person who gets telephoned or called on if someone has a problem.</p>
<p>14. If we asked one of your friends to pick out a weakness of yours, what do you think they might sa ?</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  I suppose they might say that I can sometines be a bit impulsive. I get very fired up by a new idea and sometimes a little frustrated if I don’t get the chance to carry it through. The plus side of it is that I do have some exciting ideas and many of them do really work.</p>
<p>(Give an example here if this applies to you)</p>
<p><strong>D’autres types de questions :</strong></p>
<ul class="spip">
<li>Tell me about yourself</li>
<li>Of which personal accomplishments are you the most proud ?</li>
<li>Tell me about your best supervisors, and why you regard them that way.</li>
<li>Tell me about your worst supervisors, and why you regard them this way ? The best is to say that you’ve learned something from all of your supervisors.</li>
<li>Describe your toughest problem and how you handled it. Note : Focus on accomplishments, and do not blame or say anything negative about your associates.</li>
<li>Tell me about a conflict with a co-worker. How did you handle it ?</li>
<li>Tell me about a disagreement with your current or previous supervisor. How did you handle it ?</li>
<li>How do you react to negativity or gossip from co-workers ?</li>
<li>If you found out a co-worker was / is dishonest, what would you do ?</li>
<li>If you were unable to meet a commitment or deadline, what would you do ?</li>
<li>If a customer is disrespectful to you, what do you do ?</li>
<li>If you were unable to resolve a customer issue by the date promised because another department did not do its job, what would you do, and what would you say to the customer ?</li>
<li>What makes you the best candidate ?</li>
<li>Do you prefer to work alone or in a group ?</li>
<li>Tell me about your organization / time management style.</li>
<li>How well do you work with multiple people or vendors ?</li>
<li>Describe a project that you managed. Was the project completed on time and on/under budget ? Why / Why not ? What would you have done differently ?</li>
<li>Describe how you motivate your staff or fellow team members.</li>
<li>How would your staff or team members describe you ?</li>
<li>How would your current supervisor describe you ?</li>
<li>Where do you want to be in five years ?</li>
<li>Why do you want this job ? Note : Focus on benefits to the company and how your skills and goals fit the company’s needs and goals.</li></ul>
<h3 class="spip"><a id="sommaire_5"></a>Erreurs à éviter</h3>
<ul class="spip">
<li>Ne faites aucun commentaire négatif sur votre employeur actuel ou passé et/ou sur vos collègues.</li>
<li>Ne demandez pas à avoir des horaires ou des équipements spéciaux, seulement si vous avez un handicap qui nécessite des aménagements spécifiques.</li>
<li>Evitez les discussions concernant les salaires durant la première étape de l’entretien. Laissez l’employeur vous en parler.</li>
<li>Ne posez pas de question sur les jours de vacances, les congés maladie dans les premiers temps de l’entretien.</li>
<li>Ne donnez pas une liste de choses que vous ne voulez pas faire.</li>
<li>Ne demandez pas ce que l’entreprise fait d’autre.</li>
<li>Ne dites pas : "I don’t have any negative points".</li>
<li>Ne vous plaignez pas, ne faites pas de réclamations personnelles.</li>
<li>Ne faites pas de déclarations malhonnêtes ou trompeuses.</li></ul>
<h3 class="spip"><a id="sommaire_6"></a>Après l’entretien</h3>
<p>Dans ces deux lettres, le candidat remercie son interlocuteur pour l’entretien, retrace ses aptitudes et démontre que ses qualifications correspondent à l’emploi proposé. Il remontre son intérêt pour la position.</p>
<ul class="spip">
<li><p class="document_image">
<a class="spip_in" title="Doc:Première lettre , 13.7 ko, 0x0" href="http://www.diplomatie.gouv.fr/fr/IMG/docx/entretien_lettre1_cle0ddc81.docx"><img width="52" height="52" alt="Doc:Première lettre , 13.7 ko, 0x0" src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L52xH52/docx-1c5f7.png" style="vertical-align: middle;"></a>
<a href="http://www.diplomatie.gouv.fr/fr/IMG/docx/entretien_lettre1_cle0ddc81.docx">Première lettre - (Word, 13.7 ko)</a>
</p></li>
<li><p class="document_image">
<a class="spip_in" title="Doc:Seconde lettre , 13.6 ko, 0x0" href="http://www.diplomatie.gouv.fr/fr/IMG/docx/entretien_lettre2_cle0ae4ab.docx"><img width="52" height="52" alt="Doc:Seconde lettre , 13.6 ko, 0x0" src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L52xH52/docx-1c5f7.png" style="vertical-align: middle;"></a>
<a href="http://www.diplomatie.gouv.fr/fr/IMG/docx/entretien_lettre2_cle0ae4ab.docx">Seconde lettre - (Word, 13.6 ko)</a>
</p></li>
<li><p class="document_image">
<a class="spip_in" title="Doc:Lettre pour accepter une offre d'emploi , 13.6 ko, 0x0" href="http://www.diplomatie.gouv.fr/fr/IMG/docx/entretien_lettre3_cle0611ad.docx"><img width="52" height="52" alt="Doc:Lettre pour accepter une offre d’emploi , 13.6 ko, 0x0" src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L52xH52/docx-1c5f7.png" style="vertical-align: middle;"></a>
<a href="http://www.diplomatie.gouv.fr/fr/IMG/docx/entretien_lettre3_cle0611ad.docx">Lettre pour accepter une offre d’emploi - (Word, 13.6 ko)</a>
</p></li>
<li><p class="document_image">
<a class="spip_in" title="Doc:Lettre pour décliner une offre d'emploi , 13.5 ko, 0x0" href="http://www.diplomatie.gouv.fr/fr/IMG/docx/entretien_lettre4_cle01a921.docx"><img width="52" height="52" alt="Doc:Lettre pour décliner une offre d’emploi , 13.5 ko, 0x0" src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L52xH52/docx-1c5f7.png" style="vertical-align: middle;"></a>
<a href="http://www.diplomatie.gouv.fr/fr/IMG/docx/entretien_lettre4_cle01a921.docx">Lettre pour décliner une offre d’emploi - (Word, 13.5 ko)</a>
</p></li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/emploi-stage/article/entretien-d-embauche). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
