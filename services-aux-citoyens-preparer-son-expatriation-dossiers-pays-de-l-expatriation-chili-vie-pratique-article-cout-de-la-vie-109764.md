# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/vie-pratique/article/cout-de-la-vie-109764#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/vie-pratique/article/cout-de-la-vie-109764#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/vie-pratique/article/cout-de-la-vie-109764#sommaire_3">Alimentation</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/vie-pratique/article/cout-de-la-vie-109764#sommaire_4">Prix des biens de consommation</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/vie-pratique/article/cout-de-la-vie-109764#sommaire_5">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>L’unité monétaire est le peso chilien (CLP).</p>
<p>Les billets sont de $1 000 (« una luca »), $2 000, $5 000, $10 000 et $20 000.</p>
<p>Les pièces sont de $1, $5, $10, $50, $100 (una moneda) et $500.</p>
<p>L’euro est généralement accepté dans les bureaux de change. Si vous comptez voyager, préférez toutefois le dollar américain (US$), plus utilisé au Pérou, en Bolivie et en Argentine.</p>
<h4 class="spip">Usage du dollar américain</h4>
<p>Le dollar américain est très peu utilisé au Chili, il est donc nécessaire de payer en monnaie locale. Cependant, conséquence de nombreuses dévaluations passées, le dollar reste la monnaie de référence pour de nombreux Chiliens. A titre d’exemple, les prix des billets d’avion sont indiqués en dollars dans les agences de voyage. Un grand nombre de Chiliens des classes sociales élevées possèdent des comptes bancaires en dollars US (ex : Citibank).</p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<h4 class="spip">Paiement par carte bancaire</h4>
<p>Le paiement par carte de crédit internationale (type Mastercard ou Visa) est très répandu. Vous trouverez également de nombreux distributeurs automatiques de billets, notamment dans les pharmacies et les supermarchés. L’ensemble des distributeurs de billets au Chili appartient au réseau Redbank.</p>
<p>Pour retirer de l’argent avec une carte étrangère, vous devrez après avoir introduit votre carte dans le distributeur :</p>
<p>1. sélectionner l’option « Clientes extranjeros / Foreign clients » ;<br class="manualbr">2. sélectionner « Cuenta Corriente ».</p>
<p>Notez que dans la plupart des commerces, il n’est pas nécessaire de composer le code de votre carte bancaire. La signature et le numéro de passeport ou celui RUT (visa de résidence) suffisent. N’oubliez pas de vérifier auprès de votre banque en France les frais afférents aux retraits d’espèces et aux paiements à l’étranger.</p>
<h4 class="spip">Ouverture d’un compte bancaire</h4>
<p>L’ouverture d’un compte au Chili est particulièrement difficile pour les étrangers. Il est nécessaire de disposer :</p>
<ul class="spip">
<li>d’un <a href="http://www.sii.cl/portales/investors/registrese/inscripcion_rut_extranjeros.htm" class="spip_out" rel="external">RUT</a>, ce qui implique d’avoir un visa de résidence (temporaire, étudiant, travail) ;</li>
<li>de garanties parfois difficiles à réunir (notamment feuilles de paie d’une entreprise domiciliée au Chili).</li></ul>
<p>Armez-vous de patience.</p>
<p>Les principales banques sont : Santander, BBVA, Citibank et Banco de Chile. Il n’existe pas de banque française de détail.</p>
<p><strong>Attention</strong> : les banques ne sont ouvertes que du lundi au vendredi de 9h à 14h.</p>
<h3 class="spip"><a id="sommaire_3"></a>Alimentation</h3>
<p>Chaque quartier possède plusieurs supermarchés de taille moyenne.</p>
<p>De plus, les <i>malls</i> (galeries marchandes sur plusieurs étages) offrent une grande variété de services : habillement, alimentation, musique, restaurants, cinémas, centres médicaux. On observe toutefois, notamment pour les vêtements, un choix assez restreint comparativement à la France.</p>
<p>Les supermarchés de Santiago sont très bien approvisionnés, aussi bien en produits locaux qu’en produits importés.</p>
<p>Les principales enseignes d’hypermarchés et supermarchés locaux sont Jumbo et Lider (Wall Mart), Unimarc, Tottus et Santa Isabel. Les enseignes françaises ne sont pas représentées.</p>
<p>Vous trouverez des épiceries, boulangeries, des marchands de quatre saisons et autres commerces de proximité dans chaque quartier. « <i>La Vega</i><i> </i> » est le principal marché couvert de Santiago. Il est ouvert tous les jours, mais ferme assez tôt (aux alentours de 17 h). Vous y trouverez de nombreux fruits et légumes, viandes, fromages et tous types de produits alimentaires et ménagers. A Valparaiso, le marché principal s’appelle « El Cardonal ».</p>
<p>Sur une rive du rio Mapocho se trouvent le marché ancien (touristique architecture industrielle du XIXème siècle, où l’on trouve encore des étals de poissons et fruits de mer) et de nombreux restaurants ; sur l’autre se trouvent les halles, avec tous les produits de bouche frais en quantité, notamment les fruits et légumes.</p>
<p>Chaque commune de Santiago a son propre marché certains jours de la semaine.</p>
<h4 class="spip">Prix moyen d’un repas dans un restaurant</h4>
<p>Selon qu’il s’agisse d’un déjeuner dans une brasserie ou d’un dîner gastronomique, le prix moyen d’un repas peut varier de 10 à 70 €. On trouve tous les types de nourriture à Santiago, en province le choix est un peu plus réduit. Beaucoup de restaurants possèdent également un service de livraison à domicile (sushis, sandwichs, pizzas).</p>
<p>Vous trouverez une liste assez complète des restaurants sur le site <a href="http://restaurantes.emol.com/" class="spip_out" rel="external">Restaurantes.emol.com</a></p>
<p>De nombreuses cantines ou brasseries proposent des menus le midi à des prix très raisonnables (3 000 à 6 000 pesos, soit entre 5 et 10 €).</p>
<p>Les sandwiches ou les empanadas sont de véritables institutions chiliennes. Variés et inventifs, ils sont parfois de vraies trouvailles gastronomiques… et abordables. Pour les sandwiches demandez les <i>ave con palta</i> (volaille et avocat) ou les <i>Barro Luca</i>, (viande émincée et fromage). Quant aux empanadas, celles de <i>pino</i> sont les plus typiques (viande, œuf dur, oignon, raisins secs, olive).</p>
<h3 class="spip"><a id="sommaire_4"></a>Prix des biens de consommation</h3>
<p>Les prix de l’alimentation peuvent varier du simple au double suivant le lieu où vous achetez. Si vous désirez acheter les mêmes produits qu’en Europe, le coût sera le même ou plus élevé qu’en France.</p>
<p>Il est bien sûr plus économique d’acheter des produits locaux (viande, légumes et riz). Dans les régions agricoles, il est fréquent que les paysans offrent sur les bords des routes leurs productions (fleurs, fruits, avocats, légumes) à des prix bien moins élevés qu’en ville.</p>
<h3 class="spip"><a id="sommaire_5"></a>Evolution des prix</h3>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/vie-pratique/article/cout-de-la-vie-109764). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
