# Agenda : Dates de l’élection présidentielle et des élections législatives de 2017

<p>Pour les Français résidant à l’étranger, en application de l’article 330-11 du code électoral, les élections se tiendront aux dates suivantes :</p>
<p><strong>Élection présidentielle :</strong></p>
<p>1er tour</p>
<ul class="spip">
<li>Samedi 22 avril pour les Caraïbes et le continent américain</li>
<li>dimanche 23 avril pour tous les autres continents.</li></ul>
<p>2nd tour</p>
<ul class="spip">
<li>samedi 6 mai pour les Caraïbes et le continent américain</li>
<li>dimanche 7 mai pour tous les autres continents.</li></ul>
<p><strong>Élections législatives :</strong></p>
<p>A noter : Les Français résidant à l’étranger voteront une semaine plus tôt que les Français résidant en France pour le premier tour des élections législatives 2017.</p>
<p>1er tour</p>
<ul class="spip">
<li>samedi 3 Juin pour les Caraïbes et le continent américain</li>
<li>dimanche 4 Juin pour tous les autres continents.</li></ul>
<p>2nd tour</p>
<ul class="spip">
<li>samedi 17 Juin pour les Caraïbes et le continent américain</li>
<li>dimanche 18 juin pour tous les autres continents.</li></ul>
<p>Mise à jour : mai 2016</p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/droit-de-vote-et-elections-a-l-etranger/article/agenda-dates-de-l-election-presidentielle-et-des-elections-legislatives-de-2017). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
