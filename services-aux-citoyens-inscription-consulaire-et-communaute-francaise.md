# Inscription consulaire et Communauté française

<p class="spip_document_84790 spip_documents spip_documents_center">
<img src="http://www.diplomatie.gouv.fr/fr/IMG/jpg/rubrique_sinscrire_consulat_cle0b8c16-2.jpg" width="660" height="140" alt=""></p>
<p>On estime à 2.5 millions le nombre de Français résidant à l’étranger. Près d’1,7 millions d’entre eux sont inscrits auprès des services consulaires.</p>
<p>L’inscription au registre des Français établis hors de France n’est pas obligatoire mais <strong>elle est vivement recommandée</strong>. En effet, cette formalité rapide et gratuite facilite le travail des services consulaires en matière de sécurité et de protection consulaire ainsi que le traitement des démarches administratives.</p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-inscription-consulaire-et-communaute-francaise-article-lancement-de-l-inscription-en-ligne-au-registre-des-francais-etablis-a-l.md" title="Lancement de l’inscription en ligne au registre des Français établis à l’étranger (17.06.16)">Lancement de l’inscription en ligne au registre des Français établis à l’étranger (17.06.16)</a></li>
<li><a href="services-aux-citoyens-inscription-consulaire-et-communaute-francaise-article-l-inscription-aupres-des-services-consulaires-se-fait-desormais-en-ligne.md" title="L’inscription auprès des services consulaires se fait désormais en ligne !">L’inscription auprès des services consulaires se fait désormais en ligne !</a></li>
<li><a href="services-aux-citoyens-inscription-consulaire-et-communaute-francaise-article-pourquoi-et-comment-s-inscrire-au-registre-des-francais-etablis-hors-de-france.md" title="Pourquoi et comment s’inscrire au Registre des français établis hors de France ? ">Pourquoi et comment s’inscrire au Registre des français établis hors de France ? </a></li>
<li><a href="services-aux-citoyens-inscription-consulaire-et-communaute-francaise-article-la-communaute-francaise-inscrite-au-registre-des-francais-etablis-hors-de.md" title="La communauté française inscrite au registre des Français établis hors de France">La communauté française inscrite au registre des Français établis hors de France</a></li>
<li><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/inscription-consulaire-et-communaute-francaise/article/rapport-du-gouvernement-sur-la-situation-des-francais-etablis-hors-de-france" title="Rapport du gouvernement sur la situation des Français établis hors de France (2015)">Rapport du gouvernement sur la situation des Français établis hors de France (2015)</a></li>
<li><a href="services-aux-citoyens-inscription-consulaire-et-communaute-francaise-article-quel-est-le-role-d-un-consulat.md" title="Quel est le rôle d’un consulat ?">Quel est le rôle d’un consulat ?</a></li>
<li><a href="services-aux-citoyens-inscription-consulaire-et-communaute-francaise-article-pourquoi-s-inscrire-aupres-du.md" title="Pourquoi s’inscrire auprès du consulat ?">Pourquoi s’inscrire auprès du consulat ?</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/inscription-consulaire-et-communaute-francaise/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
