# Scolarisation

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/vie-pratique/article/scolarisation-111324#sommaire_1">Les établissements scolaires français</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/vie-pratique/article/scolarisation-111324#sommaire_2">Enseignement supérieur</a></li></ul>
<p>Pour toute information sur la scolarisation dans le système français à l’étranger, vous pouvez consulter notre thématique sur les <a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures.md" class="spip_in">Etudes supérieures</a> et la <a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais.md" class="spip_in">Scolarité en français</a>.</p>
<p>Vous y trouverez des renseignements sur :</p>
<ul class="spip">
<li>les établissements français du primaire et du secondaire à l’étranger ;</li>
<li>les bourses scolaires et la prise en charge des frais de scolarité à l’étranger ;</li>
<li>les possibilités qui s’offrent à vous si votre enfant ne peut être scolarisé à l’étranger dans le système français (enseignement à distance par le CNED, programme français langue maternelle (FLAM), internats en France) ;</li>
<li>les épreuves du baccalauréat à l’étranger ;</li>
<li>les bourses d’études supérieures en France et à l’étranger ;</li>
<li>l’équivalence des diplômes.</li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Les établissements scolaires français</h3>
<p>Il existe deux établissements français homologués, membres du réseau <a href="http://www.aefe.fr/" class="spip_out" rel="external">AEFE</a>, en République Dominicaine :</p>
<ul class="spip">
<li>le lycée français de Saint-Domingue scolarise, à la rentrée 2013, 647 élèves de la maternelle aux classes de Terminales S, L et ES. Plus de 50% de ses enseignants sont titulaires de l’Education nationale française.</li>
<li>l’école française Théodore Chassériau à Las Terrenas scolarise, à la rentrée 2013, 102 élèves de la maternelle au CM2.</li></ul>
<p>Les élèves poursuivent leurs études essentiellement en France, en Amérique du nord et en République Dominicaine.</p>
<p>Pour en savoir plus :</p>
<ul class="spip">
<li><a href="http://www.aefe.fr/" class="spip_out" rel="external">Agence pour l’enseignement français à l’étranger</a></li>
<li><a href="http://www.lfsd.edu.do/" class="spip_out" rel="external">Lycée français de Saint-Domingue</a></li>
<li><a href="http://ecolefrancaiselasterrenas.com" class="spip_out" rel="external">Ecole française Las Terrenas</a></li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Enseignement supérieur</h3>
<p>Outre l’université publique (Université Autonome de Santo Domingo, UASD), il existe des établissements privés dont certains ont bonne réputation (UNIBE, PCMM, INTEC …).</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/vie-pratique/article/scolarisation-111324). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
