# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/vie-pratique/article/cout-de-la-vie-111373#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/vie-pratique/article/cout-de-la-vie-111373#sommaire_2">Alimentation</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/vie-pratique/article/cout-de-la-vie-111373#sommaire_3">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>L’unité monétaire est le dong.</p>
<p>Au 1er janvier 2014, le dong vaut 0,0000352 euros c’est-à-dire qu’un euro équivaut à 28 717 dongs.</p>
<p>Le Dong est la monnaie nationale mais le dollar US est également utilisé.</p>
<p>Il est possible de s’approvisionner en devises soit par le change sur place, soit par la voie bancaire.</p>
<p>Les banques françaises installées au Vietnam ne traitent pas la clientèle privée.</p>
<p>Les particuliers peuvent ouvrir un compte auprès de l’ANZ Bank (Australian New Zealand Bank), HSBC EXIM Bank ou auprès des banques vietnamiennes telles que la Vietcombank-Hanoï, Vietcombank-Hô chi Minh-ville, banque vietnamienne pour le Commerce Extérieur, Asia Commercial Bank ou Sacombank.</p>
<p>D’une manière générale, le paiement en espèces est privilégié. L’utilisation des cartes bancaires est possible dans les hôtels, les restaurants et les boutiques ouverts à la clientèle internationale. Des distributeurs automatiques de dongs commencent à se généraliser mais restent limités aux grandes villes. Les chèques de voyage peuvent être présentés dans les banques et les hôtels internationaux, essentiellement dans les grandes villes.</p>
<p>Pour en savoir plus :</p>
<ul class="spip">
<li><a href="http://www.ambafrance-vn.org/article.php3?id_article=457" class="spip_out" rel="external">Ambassade de France à Hanoi</a></li>
<li><a href="http://www.consulfrance-hcm.org/article.php3?id_article=994" class="spip_out" rel="external">Consulat de France à Ho Chi Minh</a></li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Alimentation</h3>
<p>Les conditions d’approvisionnement sont satisfaisantes.</p>
<p>Des supermarchés et des centres commerciaux proposent de nombreux produits de consommation. . Il est à noter qu’en dehors des supermarchés, où les prix sont affichés, les prix demandés par les commerçants sont négociables et varient selon que le client est un étranger ou un ressortissant local.</p>
<h4 class="spip">Prix moyen d’un repas dans un restaurant</h4>
<p><strong>Ho Chi Minh-Ville</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td></td>
<td>dongs</td>
<td>euros</td></tr>
<tr class="row_even even">
<td>Restaurant de qualité supérieure</td>
<td>1 000 000</td>
<td>35</td></tr>
<tr class="row_odd odd">
<td>Restaurant de qualité moyenne</td>
<td>500 000</td>
<td>17,50</td></tr>
</tbody>
</table>
<p><strong>Hanoï</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td></td>
<td>dongs</td>
<td>euros</td></tr>
<tr class="row_even even">
<td>Restaurant de qualité supérieure</td>
<td>1 200 000</td>
<td>42</td></tr>
<tr class="row_odd odd">
<td>Restaurant de qualité moyenne</td>
<td>350 000</td>
<td>12,50</td></tr>
</tbody>
</table>
<p>Le pourboire n’est pas de pratique courante, surtout si le service est déjà compris dans la facture, mais il est accepté…</p>
<h3 class="spip"><a id="sommaire_3"></a>Evolution des prix</h3>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : février 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/vie-pratique/article/cout-de-la-vie-111373). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
