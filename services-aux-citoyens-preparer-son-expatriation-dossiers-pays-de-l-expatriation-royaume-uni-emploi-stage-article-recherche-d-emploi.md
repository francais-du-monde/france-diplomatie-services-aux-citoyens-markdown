# Recherche d’emploi

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/emploi-stage/article/recherche-d-emploi#sommaire_1">Outils pour la recherche d’emploi</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/emploi-stage/article/recherche-d-emploi#sommaire_2">Organismes pour la recherche d’emploi</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/emploi-stage/article/recherche-d-emploi#sommaire_3">Opportunités d’emploi et de stages</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Outils pour la recherche d’emploi</h3>
<h4 class="spip">Journaux</h4>
<p>Sur place, de nombreuses offres d’emploi sont publiées dans les principaux titres de la presse quotidienne (<strong>The Times, The Financial Times, The Guardian, The Independent, The Telegraph, The Evening Standard</strong>, etc.), ainsi que dans la presse locale et spécialisée.</p>
<p>Sur certains grands titres, des sélections sont proposées chaque jour de la semaine pour un secteur d’activité déterminé. De nombreuses bibliothèques locales permettent la consultation libre de la presse.</p>
<h4 class="spip">Sites internet</h4>
<p><strong>Sites français généraux</strong></p>
<ul class="spip">
<li><a href="http://www.pole-emploi-international.fr/www.eei.com.fr/eei/accueil.jsp" class="spip_out" rel="external">service international de Pôle Emploi</a></li>
<li><a href="http://www.afpa.fr/" class="spip_out" rel="external">Association pour la formation professionnelle des adultes</a></li>
<li><a href="http://www.cned.fr/" class="spip_out" rel="external">Centre national d’enseignement à distance</a></li></ul>
<p><strong>Sites anglophones</strong></p>
<p>Le site <a href="http://www.gov.uk/jobsearch" class="spip_out" rel="external">www.gov.uk/jobsearch</a> permet d’accéder à l’<i>Universal Jobmatch</i> qui est le nouvel outil d’intermédiation mis en place par le service public de l’emploi (<i>JobCenter Plus</i>).</p>
<p>Pour pouvoir accéder au service, le demandeur d’emploi doit créer son propre compte. Pour ce faire, il doit posséder une adresse mail et un compte individuel auprès des services fiscaux britanniques (l’identifiant et le mot de passe sont nécessaires pour accéder au service du <i>Universal Jobmatch</i>).</p>
<p>Une fois le compte créé (ce qui prend en pratique une dizaine de minutes), le demandeur d’emploi peut créer un CV ou télécharger jusqu’à six CV différents, faire une recherche des offres d’emploi et transmettre sa candidature, recevoir des alertes par mail lorsqu’une offre d’emploi correspondant à son profil est déposée. Le système fonctionne grâce à un mécanisme de mots clé (en fonction des compétences et des postes recherchés). Il est possible d’utiliser les services d’<i>Universal Jobmatch</i> sans créer de compte personnel, mais dans ce cas, le site peut être uniquement utilisé pour les recherches d’emploi mais ne permet pas au demandeur d’emploi de transmettre sa candidature.</p>
<p>D’autres sites peuvent être également consultés :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="https://bbc.co.uk/careers/vacancies" class="spip_out" rel="external">https://bbc.co.uk/careers/vacancies</a> </p>
<h3 class="spip"><a id="sommaire_2"></a>Organismes pour la recherche d’emploi</h3>
<p>Il est vivement recommandé de multiplier ses démarches, en envoyant des candidatures spontanées à des entreprises, en s’inscrivant dans des "job centres", des agences et des cabinets de recrutement ou encore selon le secteur d’activité visé (vente, hôtellerie, restauration) en se présentant directement à l’employeur.</p>
<p>Pour vous aider dans vos recherches, vous pouvez contacter les organismes suivants :</p>
<h5 class="spip">Le Centre Charles Péguy</h5>
<p>Le Comité consulaire pour l’emploi de Londres a fermé ses portes à l’été 2009. Désormais vous pouvez contacter le Centre Charles Péguy (CCP), qui a pour mission d’aider les jeunes Français âgés de 18 à 30 ans souhaitant s’installer au Royaume-Uni (cotisation annuelle de 60£).</p>
<p>Dans ce but, le CCP propose :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>un service Emploi </strong>(consultation d’annonces d’emploi locales du lundi au vendredi, avec l’aide de conseillers). Ces offres sont reparties dans tous les domaines d’activité : hôtellerie/restauration, vente, artisanat, services aux particuliers, administration, industrie.</p>
<p><i>Attention : le Centre Charles Péguy ne propose pas de stage en entreprise.</i></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>un service Logement </strong>(consultation d’annonces de logement, avec une réactualisation quotidienne des offres), l’offre porte notamment sur les propositions de colocation.</p>
<p>Le CCP offre en outre :</p>
<ul class="spip">
<li>l’organisation de réunions d’information hebdomadaires « Essential London » pour vous expliquer les démarches à engager lors de votre installation ainsi que des conseils pratiques sur la vie au Royaume-Uni (réglementation du travail, couverture sociale, ouverture d’un compte bancaire, adresses utiles, trouver un logement etc.)</li>
<li>une aide à la traduction de C.V. et de lettres de motivation par des bénévoles bilingues ;</li>
<li>un service de poste restante ;</li>
<li>une mise à disposition d’ordinateurs, imprimantes, photocopieuse, fax, téléphone et d’accès internet ;</li>
<li>la consultation de la liste des investisseurs français en Grande Bretagne ainsi qu’un annuaire des agences de recrutement</li></ul>
<p><a href="http://centrecharlespeguy.wordpress.com/" class="spip_out" rel="external">Centre Charles Péguy</a><br class="manualbr">114 – 116 Curtain Road EC2A 3AH LONDON Old Street – Exit 3<br class="manualbr">TEL : 0207 749 77 14<br class="manualbr">FAX : 0207 749 77 19<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/emploi-stage/article/recherche-d-emploi#centre.charlespeguy#mc#virgin.net#" title="centre.charlespeguy..åt..virgin.net" onclick="location.href=mc_lancerlien('centre.charlespeguy','virgin.net'); return false;" class="spip_mail">Courriel</a></p>
<h5 class="spip">National careers service</h5>
<p>Service du gouvernement britannique spécialisé dans l’aide à la recherche d’emploi pour les communautés non francophones au Royaume-Uni. Ce service propose notamment une assistance <strong>en français </strong>pour la rédaction du CV anglais, des conseils pour l’entretien d’embauche, pour la recherche d’emploi, la formation, etc.</p>
<p><strong>Internet </strong> : <a href="http://nationalcareersservice.direct.gov.uk/" class="spip_out" rel="external">http://nationalcareersservice.direct.gov.uk</a></p>
<p><strong>Téléphone (interlocuteur francophone) </strong> : (+44) 800 093 11 15 (du lundi au vendredi, de 9h à 17h).</p>
<h5 class="spip">Job Centre Plus</h5>
<p>Les <i>job centres</i> sont les services britanniques de l’emploi et sont l’équivalent de Pôle emploi en France. Ils fonctionnent par secteur géographique et assurent les services d’aide à la recherche d’emploi et d’information sur les demandes d’allocation chômage <i>(unemployment benefit)</i>. Les <i>job centres</i> proposent en règle générale des offres d’emploi sans grande qualification. Il n’existe pas au Royaume-Uni d’agence publique spécialisée dans l’emploi des cadres. Le site officiel des <a href="http://www.jobcentreplus.gov.uk/" class="spip_out" rel="external">job centres</a> permet la consultation en ligne d’offres d’emploi.</p>
<h5 class="spip">Les agences de recrutement</h5>
<p>Le recrutement par le biais des agences est incontournable. De nombreuses sociétés passent par les agences de recrutement pour répondre à leurs besoins. Le coût du service est intégralement à la charge de l’entreprise qui recrute et le demandeur ne supporte donc aucun frais.</p>
<p>Certaines agences sont spécialisées par domaines (finances, comptabilité, informatique, ingénierie, etc.). Elles peuvent proposer différents types de contrats : contrats d’intérim (<i>temporary jobs</i>), à durée déterminée (<i>contracts</i>) ou indéterminée (<i>permanent jobs</i>).</p>
<p>Outre la perspective de décrocher un emploi, le recours aux agences permet au candidat d’améliorer sa connaissance du marché du travail dans un secteur déterminé et d’évaluer son niveau d’anglais en situation, lors de l’entretien avec un consultant.</p>
<p>Il peut être utile de rechercher les coordonnées des agences, en fonction du secteur d’activité souhaité, en s’adressant au syndicat professionnel des agences de recrutement :</p>
<p><a href="http://www.rec.uk.com/home" class="spip_out" rel="external">Recruitment and Employment Confederation</a> (REC)<br class="manualbr">Dorset House<br class="manualbr">First Floor<br class="manualbr">27-45 Stamford Street<br class="manualbr">London<br class="manualbr">SE1 9NT<br class="manualbr">Tél. : [44] (0) 20 7009 2100<br class="manualbr">Fax : [44] (0) 20 7935 4112<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/emploi-stage/article/recherche-d-emploi#info#mc#rec.uk.com#" title="info..åt..rec.uk.com" onclick="location.href=mc_lancerlien('info','rec.uk.com'); return false;" class="spip_mail">Courriel</a></p>
<h5 class="spip">Le réseau Eures</h5>
<p><a href="http://ec.europa.eu/eures" class="spip_out" rel="external">EURES</a> a pour vocation d’offrir des informations, des conseils et des services de recrutement/placement aux travailleurs et aux employeurs, ainsi qu’à tout citoyen désireux de tirer profit du principe de la libre circulation des personnes. EURES met à disposition un réseau de conseillers pour fournir les informations requises par les chercheurs d’emploi et les employeurs dans le cadre de contacts personnels.</p>
<p>Outre des informations sur le marché du travail et sur les conditions de vie et de travail locales, le site EURES offre la possibilité de consulter les offres d’emploi et le calendrier des manifestations liées à l’emploi dans le pays.</p>
<h3 class="spip"><a id="sommaire_3"></a>Opportunités d’emploi et de stages</h3>
<p><strong>La connaissance de l’anglais est indispensable</strong>, avec quelques nuances toutefois selon le secteur d’activité visé. Un niveau moyen ou scolaire peut être suffisant dans les domaines de la restauration ou de l’hôtellerie ; en revanche pour des emplois plus qualifiés et à haute technicité une bonne maîtrise de l’anglais est nécessaire.</p>
<p>Il existe de très nombreux cours de langue, notamment au sein d’établissements privés ou auprès de certaines mairies de quartiers. Les cours ne sont jamais gratuits et peuvent parfois être assez onéreux. Pour plus de renseignements, il est recommandé de contacter le <a href="http://www.britishcouncil.org/" class="spip_out" rel="external">British Council</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/emploi-stage/article/recherche-d-emploi). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
