# Services aux citoyens

<ul class="cadre_gris">
<li><a href="services-aux-citoyens-preparer-son-expatriation.md" title="Préparer son expatriation">
<img class="spip_logos" alt="Préparer son expatriation" src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L320xH320/rubon22367-6b216.png" width="320" height="320">
</a></li>
</ul>
<ul class="cadre_gris">
<li><a href="services-aux-citoyens-inscription-consulaire-et-communaute-francaise.md" title="Inscription consulaire et Communauté française">
<img class="spip_logos" alt="Inscription consulaire et Communauté française" src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L320xH320/rubon23834-cda1d.png" width="320" height="320">
</a></li>
</ul>
<ul class="cadre_gris">
<li><a href="services-aux-citoyens-etat-civil-et-nationalite-francaise.md" title="Etat civil et nationalité française">
<img class="spip_logos" alt="Etat civil et nationalité française" src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L320xH320/rubon23835-1de3b.png" width="320" height="320">
</a></li>
</ul>
<ul class="cadre_gris cols4-1024-dernier cols4-980-1024-dernier">
<li><a href="services-aux-citoyens-documents-officiels-a-l-etranger.md" title="Documents officiels à l'étranger">
<img class="spip_logos" alt="Documents officiels à l'étranger" src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L320xH320/rubon23836-020af.png" width="320" height="320">
</a></li>
</ul>
<ul class="cadre_gris">
<li><a href="services-aux-citoyens-legalisation-et-notariat.md" title="Légalisation et notariat">
<img class="spip_logos" alt="Légalisation et notariat" src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L320xH320/rubon23837-aa7ce.png" width="320" height="320">
</a></li>
</ul>
<ul class="cadre_gris">
<li><a href="services-aux-citoyens-droit-de-vote-et-elections-a-l-etranger.md" title="Droit de vote et élections à l'étranger">
<img class="spip_logos" alt="Droit de vote et élections à l'étranger" src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L320xH320/rubon23838-ff7e4.png" width="320" height="320">
</a></li>
</ul>
<ul class="cadre_gris">
<li><a href="services-aux-citoyens-conseils-aux-familles.md" title="Conseils aux familles">
<img class="spip_logos" alt="Conseils aux familles" src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L320xH320/rubon3104-5128d.png" width="320" height="320">
</a></li>
</ul>
<ul class="cadre_gris cols4-1024-dernier cols4-980-1024-dernier">
<li><a href="services-aux-citoyens-le-retour-en-france.md" title="Le retour en France">
<img class="spip_logos" alt="Le retour en France" src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L320xH320/rubon22054-4371b.png" width="320" height="320">
</a></li>
</ul>
<h2>Dernières actualités</h2>
<ul class="derniere_actus">
<li><a href="services-aux-citoyens-actualites-article-reseau-consulaire-intervention-de-jean-marc-ayrault-aux-journees-du-reseau.md" title="Réseau consulaire - Intervention de Jean-Marc Ayrault aux journées du réseau consulaire (21.06.16)">Réseau consulaire - Intervention de Jean-Marc Ayrault aux journées du réseau consulaire (21.06.16)</a>
<p>
            Jean-Marc Ayrault, ministre des affaires étrangères et du développement international, s’adressera le 21 juin 2016, aux consuls et agents consulaires réunis pour la deuxième édition des "journées du réseau consulaire".
</p></li>
<li><a href="services-aux-citoyens-inscription-consulaire-et-communaute-francaise-article-lancement-de-l-inscription-en-ligne-au-registre-des-francais-etablis-a-l.md" title="Lancement de l'inscription en ligne au registre des Français établis à l'étranger (17.06.16)">Lancement de l’inscription en ligne au registre des Français établis à l’étranger (17.06.16)</a>
<p>
            Matthias Fekl, secrétaire d’Etat chargé du commerce extérieur, de la promotion du tourisme et des Français de l’étranger, a présenté hier le nouveau service d’inscription en ligne au registre des Français établis à l’étranger.
</p></li>
<li><a href="services-aux-citoyens-publications-article-guide-action-consulaire.md" title="Guide « L'action consulaire »">Guide « L’action consulaire »</a>
<p>
            Le ministère des Affaires étrangères et du Développement international accompagne la mobilité des ressortissants français à l’étranger à travers la Direction des Français à l’étranger et de l’administration consulaire (DFAE) qui pilote un vaste réseau de plus de 217 services consulaires. Ces derniers permettent aux 2,5 millions de Français résidant à l’étranger et aux 13 millions de voyageurs annuels de bénéficier d’un service public français dans plus de 160 pays.
</p></li>
<li><a href="services-aux-citoyens-etat-civil-et-nationalite-francaise-http-www-diplomatie-gouv-fr-fr-services-aux-citoyens-etat-civil-et-nationalite-francaise-article-le-service-central-d-etat-civil-adhere-a-la-plateforme-de-communication.md" title="Le SCEC élargit à nouveau le champ de COMEDEC">Le SCEC élargit à nouveau le champ de COMEDEC</a>
<p>
            Depuis le 1er juin 2016, la vérification de l’état civil des personnes nées à l’étranger pour les demandes de passeport effectuées dans les consulats se fait via la plateforme de COMmunication (...)
</p></li>
<li><a href="services-aux-citoyens-actualites-article-un-consul-pour-quoi-faire-regards-croises-de-nos-consuls-a-montreal-tunis-et.md" title="« Un consul pour quoi faire ? Regards croisés de nos consuls à Montréal, Tunis et Téhéran » (20.06.16)">« Un consul pour quoi faire ?  Regards croisés de nos consuls à Montréal, Tunis et Téhéran » (20.06.16)</a>
<p>
            Du Cap à Reykjavik, de New-York à Shanghai, le ministère des Affaires étrangères et du Développement international pilote un vaste réseau consulaire au service de la communauté française de l’étranger et de l’attractivité de notre pays.
</p></li>
<li><a href="services-aux-citoyens-droit-de-vote-et-elections-a-l-etranger-article-agenda-dates-de-l-election-presidentielle-et-des-elections-legislatives-de-2017.md" title="Agenda : Dates de l'élection présidentielle et des élections législatives de 2017">Agenda : Dates de l’élection présidentielle et des élections législatives de 2017</a>
<p>
            Pour les Français résidant à l’étranger, en application de l’article 330-11 du code électoral, les élections se tiendront aux dates suivantes :
</p></li>
<li><a href="services-aux-citoyens-inscription-consulaire-et-communaute-francaise-article-la-communaute-francaise-inscrite-au-registre-des-francais-etablis-hors-de.md" title="La communauté française inscrite au registre des Français établis hors de France">La communauté française inscrite au registre des Français établis hors de France</a>
<p>
            Le nombre d’inscrits au registre au 31 décembre 2015 s’élève à 1 710 945, soit une progression de 1,8% par rapport à 2014. Le taux de croissance était de 2,4% entre 2013 et 2014. Les statistiques par pays sont consultables sur le site data.gouv.fr.
</p></li>
<li><a href="services-aux-citoyens-publications-article-expatriation-15-cles-pour-partir-l-esprit-tranquille.md" title="Expatriation : 15 clés pour partir l'esprit tranquille">Expatriation : 15 clés pour partir l’esprit tranquille</a>
<p>
            La nouvelle version du guide Expatriation - 15 clés pour partir et revenir l’esprit tranquille est parue !

Le Guide « Expatriation : 15 clés pour partir et revenir l’esprit tranquille » vous permet de retrouver les conseils pratiques et adresses indispensables pour vivre sereinement toutes les étapes de votre expatriation.
</p></li>
<li><a href="services-aux-citoyens-actualites-article-francais-de-l-etranger-presentation-du-simulateur-retour-en-france-par-matthias.md" title="Français de l'étranger - Présentation du simulateur « Retour en France » par Matthias Fekl (03.02.16)">Français de l’étranger - Présentation du simulateur « Retour en France » par Matthias Fekl (03.02.16)</a>
<p>
            Dans le cadre de la présentation des nouvelles mesures de simplification par le Premier ministre, Matthias Fekl, secrétaire d’État chargé du commerce extérieur, de la promotion du tourisme, et des Français de l’étranger, a présenté le 3 février dix mesures qui faciliteront le retour en France de nos compatriotes établis à l’étranger.
</p></li>
<li><a href="services-aux-citoyens-actualites-article-annonces-de-matthias-fekl-lors-de-son-discours-de-voeux-aux-francais-etablis.md" title="Annonces de Matthias Fekl lors de son discours de vœux aux Français établis hors de France">Annonces de Matthias Fekl lors de son discours de vœux aux Français établis hors de France</a>
<p>
            Le secrétaire d’État chargé du Commerce extérieur, de la Promotion du tourisme et des Français de l’étranger a présenté ses vœux à la communauté française à Munich.
</p></li>
<li><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/conseils-aux-familles/article/formalisee-par-le-protocole-d-accord-signe" title="Recouvrement international de créances alimentaires : Signature du protocole entre le MAEDI et la Chambre nationale des huissiers de justice">Recouvrement international de créances alimentaires : Signature du protocole entre le MAEDI et la Chambre nationale des huissiers de justice</a>
<p>
            Formalisée par le protocole d’accord signé le 10 décembre 2015, la collaboration entre le ministère des Affaires étrangères et les membres de la profession des huissiers de justice a déjà porté ses (...)
</p></li>
<li><a href="services-aux-citoyens-actualites-article-50e-anniversaire-de-l-implantation-a-nantes-du-ministere-des-affaires.md" title="50e anniversaire de l'implantation à Nantes du ministère des affaires étrangères et du développement international et de la création du service central d'état civil">50e anniversaire de l’implantation à Nantes du ministère des affaires étrangères et du développement international et de la création du service central d’état civil</a>
<p>
            Avec 15 millions d’actes conservés depuis sa création en 1965, le service central d’état civil est le plus important service d’état civil de France. Il délivre copies et extraits d’actes d’état civil aux Français dont un des épisodes de la vie civile s’est déroulé à l’étranger (naissance, mariage, reconnaissance, adoption, décès). Il est également compétent pour établir les actes d’état civil français de toutes les personnes qui acquièrent la nationalité française.
</p></li>
<li><a href="services-aux-citoyens-actualites-article-signature-d-une-convention-avec-le-conseil-superieur-du-notariat.md" title="Signature d'une Convention avec le Conseil supérieur du notariat">Signature d’une Convention avec le Conseil supérieur du notariat</a>
<p>
            Christophe Bouchard, Directeur des Français à l’étranger et de l’administration consulaire et Maître Pierre-Luc Vogel, Président du Conseil supérieur du Notariat, ont signé le 25 novembre, une convention (...)
</p></li>
<li><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/inscription-consulaire-et-communaute-francaise/article/rapport-du-gouvernement-sur-la-situation-des-francais-etablis-hors-de-france" title="Rapport du gouvernement sur la situation des Français établis hors de France (2015)">Rapport du gouvernement sur la situation des Français établis hors de France (2015)</a></li>
<li><a href="services-aux-citoyens-en-cas-de-difficulte-a-l-etranger-article-important-rappel-sur-l-assurance-rapatriement-a-l-attention-des-voyageurs.md" title="Important : rappel sur l'assurance-rapatriement à l'attention des voyageurs">Important : rappel sur l’assurance-rapatriement à l’attention des voyageurs</a>
<p>
            Le ministère des affaires étrangères et du développement international rappelle qu’il relève de la responsabilité individuelle de chacun de prévoir une assurance-rapatriement avant tout déplacement à l’étranger.
</p></li>
<li><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/article/pegase-la-page-de-l-expatriation" title="Pegase, la page de l'expatriation et des Français de l'étranger">Pegase, la page de l’expatriation et des Français de l’étranger</a>
<p>
            Vous y trouverez des informations pratiques et concrètes sur les démarches consulaires ainsi que des liens utiles pour répondre aux questions que vous vous posez quand vous résidez à l’étranger sur (...)
</p></li>
<li><a href="services-aux-citoyens-actualites-article-questions-reponses-etat-civil.md" title="Questions / réponses Etat-civil">Questions / réponses Etat-civil</a>
<p>
            Retrouvez toutes les réponses aux questions les plus fréquemment posées au Service Central d’État Civil.
</p></li>
<li><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/article/delivrance-de-copies-d-acte-d-etat-civil-detenus-par-le-service-central-d-etat" title="Délivrance de copies d'actes d'état-civil détenus par le service central d'état-civil (SCEC) - Ce service n'a pas de guichet d'accueil">Délivrance de copies d’actes d’état-civil détenus par le service central d’état-civil (SCEC)  - Ce service n’a pas de guichet d’accueil</a>
<p>
            Veuillez noter que le service central d’état civil (SCEC) n’a pas de guichet d’accueil, aucun document n’est délivré sur place.

Vous êtes invité à effectuer vos demandes d’actes en ligne via le site (...)
</p></li>
<li><a href="services-aux-citoyens-actualites-article-changement-en-matiere-de.md" title="Changement en matière de successions internationales à partir du 17 août 2015 (24.04.2015)">Changement en matière de successions internationales à partir du 17 août 2015 (24.04.2015)</a>
<p>
            A compter du 17 août 2015, les règles en matière de succession internationale vont changer avec l’entrée en application du règlement (UE) n° 650/2012 du 4 juillet 2012 sur les successions.
</p></li>
</ul>
<h2>VOIR AUSSI</h2>
<ul class="inline_block-1024">
<li class="boite_navigation inline_block-1024 inline_block-980-1024 inline_block-768-979 inline_block-767">
<a href="http://www.assemblee-afe.fr" title="Assemblée des Français à l'étranger"> <img src="http://www.diplomatie.gouv.fr/fr/plugins/fdiplo_themes/images/logo_afe.png" alt="Assemblée des Français à l'étranger" width="60" height="60"> <span>Assemblée des Français à l’étranger</span></a></li>
<li class="boite_navigation inline_block-1024">
<a href="conseils-aux-voyageurs.md" title="Conseils aux voyageurs"> <img src="http://www.diplomatie.gouv.fr/fr/plugins/fdiplo_themes/images/logo_cav.png" alt="Conseils aux voyageurs" width="60" height="60"> <span>Conseils aux voyageurs</span></a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
