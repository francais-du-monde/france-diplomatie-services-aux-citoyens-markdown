# Communications

<h4 class="spip">Téléphone – Internet</h4>
<p>Les liaisons téléphoniques sont bonnes (indicatif pour la France depuis Maurice : 020 33, indicatif pour Maurice depuis la France : 00 230) mais les tarifs internationaux sont élevés.</p>
<p>La téléphonie mobile et les liaisons via Internet sont développées et la connexion est satisfaisante.</p>
<p>Il est généralement plus avantageux d’utiliser <strong>des logiciels de téléphonie sur IP</strong> (Skype, Google talk, Live messenger, Yahoo ! messenger, etc.) car les tarifs proposés à l’international sont souvent moindre que ceux des opérateurs locaux exploitant des lignes classiques, voire gratuits dans certaines conditions (notamment via Internet).</p>
<h4 class="spip">Poste</h4>
<p>Les liaisons postales sont quotidiennes. Le délai d’acheminement du courrier est de cinq à huit jours. Pour les colis, les délais sont souvent plus longs (formalités de dédouanement).</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/vie-pratique/article/communications). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
