# Entretien d’embauche

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/emploi-stage/article/entretien-d-embauche-110408#sommaire_1">Préparation de l’entretien</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/emploi-stage/article/entretien-d-embauche-110408#sommaire_2">Collecte d’informations</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/emploi-stage/article/entretien-d-embauche-110408#sommaire_3">Construisez votre propre confiance</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/emploi-stage/article/entretien-d-embauche-110408#sommaire_4">Questions préalables à l’entretien</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/emploi-stage/article/entretien-d-embauche-110408#sommaire_5">Comportement pendant l’entretien</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/emploi-stage/article/entretien-d-embauche-110408#sommaire_6">Après l’entretien</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Préparation de l’entretien</h3>
<p><strong>Identifier les raisons pour lesquelles vous vous rendez à un entretien</strong> : certes, vous souhaitez obtenir ce travail mais vous devez pouvoir détailler les raisons qui vous amènent à vouloir ce poste en particulier. Car cela correspond aux questions que l’employeur vous posera.</p>
<p>Soyez honnête envers vous-même.</p>
<h3 class="spip"><a id="sommaire_2"></a>Collecte d’informations</h3>
<p><strong>Menez des investigations complémentaires sur la société dans laquelle vous souhaitez travailler</strong>, de manière à être entièrement informé de sa réputation. Obtenez les noms et les titres des décisionnaires que vous allez rencontrer. Si possible, essayez d’obtenir les informations internes sur les postes et les personnes avec lesquels vous serez amenés à collaborer. Visitez le site internet de la société.</p>
<p>Sur la base de vos recherches, <strong>taillez sur mesure la présentation de vos antécédents professionnels en fonction de la société et de position souhaitée</strong> et préparez vos propres questions sur la société que vous pourriez poser pendant l’entretien.</p>
<p><strong>Déterminez précisément qui conduira l’entretien</strong>, notamment le titre, la division et la position hiérarchique, soit avant, soit dans les premières minutes de l’entretien.</p>
<p>Prenez vos notes sur les recherches concernant l’entreprise, vos correspondances précédentes, votre liste de questions et plusieurs copies de votre CV lors de l’entretien. Prenez aussi une copie des votre liste de recommandations, au cas où elle vous serait demandée.</p>
<h3 class="spip"><a id="sommaire_3"></a>Construisez votre propre confiance</h3>
<p>Focalisez-vous sur les solutions que vous avez trouvées pour surmonter vos échecs et sur les causes de vos réussites.</p>
<p>Respectez vos propres compétences, capacités et limites. Ne vous sous ou surestimez pas.</p>
<p>Partez d’une approche optimiste et bannissez le pessimisme (efforts conscients pour contrôler vos pensées)</p>
<p>Souriez et tenez-vous droit.</p>
<h3 class="spip"><a id="sommaire_4"></a>Questions préalables à l’entretien</h3>
<p>Imaginez que vous êtes la personne qui mène l’entretien. Pensez aux questions que vous poseriez à un candidat qui a le même profil que vous. Préparez une liste de ces questions et répondez-y.</p>
<p>Parlez avec des professionnels de ce secteur d’activité pour anticiper des questions basées sur ce type de marché.</p>
<p>Quelques exemples de questions posées durant un entretien :</p>
<ul class="spip">
<li>Parlez-moi un petit plus de vous-même.</li>
<li>Quels sont vos forces et vos faiblesses ?</li></ul>
<h3 class="spip"><a id="sommaire_5"></a>Comportement pendant l’entretien</h3>
<p>Les recruteurs font souvent des jugements immédiats sur les candidats, parfois dans les 30 à 60 premières secondes. Il est évident que la première impression que vous devez donner est celle d’un professionnel qualifié. Saluez vos interlocuteurs en entrant dans la salle d’entretien.</p>
<p>Trouvez une position stable sur votre chaise et tenez vous bien droit afin de pouvoir respirer. Ne croisez pas les bras ou les jambes et adoptez une position d’ouverture. Posez vos pieds fermement sur le sol et évitez de gesticuler. Au fur et à mesure de l’entretien, vous pourrez parfois utiliser votre langage corporel pour mieux argumenter un point ou une idée, en accompagnant votre discours de gestes.</p>
<p>Parlez d’une voix calme, posée et suffisamment audible. Contrôlez votre respiration. N’hésitez pas à prendre des pauses. Essayez de moduler votre ton de voix pour éviter un son monotone.</p>
<p>Regardez bien votre ou vos interlocuteurs dans les yeux, sans le dévisager et non de manière soutenue. Si l’entretien se passe avec plusieurs personnes, veillez à les regarder chacun de manière équilibrée. Evitez de baisser les yeux de manière trop prolongée.</p>
<p>Répondez de manière claire, concise et précise aux questions qui vous sont posées. Evitez d’esquiver les questions ou d’utiliser des traits d’humour, des tournures vulgaires, des tics verbaux ou un vocabulaire trop savant. N’hésitez pas à dire que vous ne savez pas répondre à une question si vous ne connaissez pas la réponse. Essayez de mettre en valeur votre expérience professionnelle dans vos réponses.</p>
<p>Concluez en résumant les points évoqués pendant l’entretien, en essayant de démontrer comment vos compétences professionnelles peuvent correspondre au profil recherché par votre recruteur. Prenez congés de votre auditoire en le regardant (ne pas baisser la tête) et en le remerciant de son attention.</p>
<h3 class="spip"><a id="sommaire_6"></a>Après l’entretien</h3>
<p>Si l’on vous a demandé de fournir des informations complémentaires à la suite de l’entretien, fournissez là à votre recruteur dans les meilleurs délais.</p>
<p>Si votre recruteur ne vous a pas contacté dans un délai d’une semaine et que vous souhaitez connaître les résultats de l’entretien, il est possible de le relancer une fois par mail ou par téléphone.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/emploi-stage/article/entretien-d-embauche-110408). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
