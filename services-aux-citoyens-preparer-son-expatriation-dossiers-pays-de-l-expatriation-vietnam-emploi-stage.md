# Emploi, stage

<h2 class="rub23047">Marché du travail</h2>
<p class="chapo">
    Selon le décret n° 105 du 17 septembre 2003, les entreprises à capitaux étrangers ne peuvent recruter que 3 % de travailleurs étrangers sur la totalité de la main d’œuvre employée, avec un maximum de 50 salariés étrangers. Cependant ce seuil peut être étendu sur autorisation du Comité populaire de ville ou de province.
</p>
<p>Selon le décret n° 105 du 17 septembre 2003, les entreprises à capitaux étrangers ne peuvent recruter que 3 % de travailleurs étrangers sur la totalité de la main d’œuvre employée, avec un maximum de 50 salariés étrangers. Cependant ce seuil peut être étendu sur autorisation du Comité populaire de ville ou de province.</p>
<p>Les entreprises étrangères ou vietnamiennes sont encouragées à employer une main d’œuvre locale. L’emploi de personnels étrangers n’est justifié que pour des postes nécessitant des compétences techniques ou de gestion non disponibles localement (pour lesquelles un ressortissant vietnamien ne serait pas qualifié). Cependant, quelques opportunités existent. La Chambre de Commerce et d’Industrie au Vietnam permet de mettre en relation candidats à l’emploi et entreprises implantées sur le territoire vietnamien.</p>
<p>Pour travailler au Vietnam, il convient de présenter un <i>dossier de candidature à l’emploi</i> composé des pièces suivantes :</p>
<ul class="spip">
<li>Une lettre de motivation ;</li>
<li>Un extrait de casier judiciaire. Au cas où le travailleur étranger séjourne au Vietnam depuis au moins 6 mois, il lui faut encore une attestation de même nature établie par le Service judiciaire de la province où il a sa résidence ;</li>
<li>Un curriculum vitæ accompagné d’une photo ;</li>
<li>Un certificat médical délivré à l’étranger. Au cas où le travailleur étranger séjourne au Vietnam, le certificat médical est délivré suivant les procédures définies par le Ministère vietnamien de la Santé ;</li>
<li>La copie des attestations et diplômes certifiant le niveau de qualification professionnelle du travailleur étranger<br class="manualbr">Pour les artisans et les salariés n’ayant aucun certificat mais qui sont expérimentés dans une profession, dans la gestion de production ou dans le management, il leur faut une fiche d’évaluation de leur compétence certifiée par l’administration du pays dont ils ont la nationalité.</li>
<li>Trois photos en couleur de format 3cm x 4cm</li></ul>
<p>Les pièces délivrées par les organismes et administrations étrangers ou certifiées par eux doivent faire l’objet d’une légalisation consulaire et traduites en vietnamien. Toute traduction et copie est obligatoirement authentifiée conformément à la législation vietnamienne.</p>
<h4 class="spip">Délivrance du permis de travail</h4>
<p>Le salarié étranger travaillant pour le compte des entreprises, des organismes et des organisations installés au Vietnam doivent être muni d’un permis de travail, sauf les cas suivants :</p>
<ul class="spip">
<li>Le salarié étranger entre au Vietnam pour un travail d’une durée inférieure à trois mois ou pour le règlement de cas urgents.</li>
<li>Le travailleur étranger est membre du Conseil d’Administration, Directeur général, directeur général adjoint, directeur, directeur adjoint des entreprises constituées conformément à la loi vietnamienne et dotées de la personnalité morale.</li>
<li>Le salarié étranger est chef d’un bureau de représentation ou d’une filiale installé au Vietnam.</li>
<li>Les avocats étrangers qui disposent d’un certificat d’exercice de la profession d’avocat au Vietnam délivré par le Ministère vietnamien de la Justice conformément aux dispositions de la loi vietnamienne.</li></ul>
<p>Le <strong>service du Travail, des Invalides de Guerre et des Affaires sociales</strong> de province et de ville relevant du pouvoir central est chargé de délivrer aux travailleurs étrangers le permis de travail selon le formulaire émis par le ministère du Travail, des Invalides de Guerre et des Affaires sociales.</p>
<p>La durée de validité du permis de travail est égale à celle du contrat de travail (au cas où le travailleur étranger a un contrat de travail) ou à celle de la mission décidée par la partie étrangère qui a envoyé le salarié, sans toutefois dépasser 36 mois.</p>
<p><i>Source : Décret gouvernemental n° 105/2003/ND-CP en date du 17 septembre 2003, fixant les modalités d’application de certains articles du code du travail relatifs à l’embauche et à la gestion des travailleurs étrangers au Vietnam</i></p>
<p>Les possibilités d’emploi sont limitées au Vietnam. Il est conseillé d’approcher directement les entreprises susceptibles d’embaucher. La connaissance du vietnamien est un avantage considérable. Il est possible de prendre des cours sur place. Pour les non vietnamophones, l’anglais est la langue des affaires au Vietnam.</p>
<p>L’implantation française au Vietnam est particulièrement significative dans le secteur de la santé, la filière agricole et agro-alimentaire, les infrastructures, la distribution et les services.</p>
<p>Le tourisme, l’export d’artisanat, les services financiers, les hautes technologies, la restauration et l’hôtellerie représentent les secteurs à fort potentiel.</p>
<h4 class="spip">Rémunération</h4>
<p>Le salaire mensuel pour un expatrié est très variable, allant de 550 à 3300 €.</p>
<p><i>Mise à jour : février 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-vietnam-emploi-stage-article-stages.md" title="Stages">Stages</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-vietnam-emploi-stage-article-lettre-de-motivation-111363.md" title="Lettre de motivation">Lettre de motivation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-vietnam-emploi-stage-article-curriculum-vitae.md" title="Curriculum vitae">Curriculum vitae</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-vietnam-emploi-stage-article-recherche-d-emploi.md" title="Recherche d’emploi">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-vietnam-emploi-stage-article-reglementation-du-travail.md" title="Réglementation du travail">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-vietnam-emploi-stage-article-marche-du-travail.md" title="Marché du travail">Marché du travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/emploi-stage/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
