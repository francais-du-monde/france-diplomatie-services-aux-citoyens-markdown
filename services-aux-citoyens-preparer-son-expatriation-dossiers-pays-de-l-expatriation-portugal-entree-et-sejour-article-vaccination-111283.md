# Vaccination

<p>Aucun vaccin n’est obligatoire <strong>pour se déplacer dans l’Union européenne</strong> si l’on vient de la France métropolitaine. La fièvre jaune est obligatoire à partir de l’âge d’un an pour les voyageurs en provenance d’une zone à risque. Les vaccinations suivantes sont conseillées pour des raisons médicales :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>pour les adultes</strong>, la mise à jour des vaccinations contre la diphtérie, le tétanos, la poliomyélite et la typhoïde (pour les longs séjours). Il est aussi conseillé de se faire vacciner contre l’hépatite A (à partir de 50 ans) et l’hépatite B (pour les longs séjours ou les séjours à risques).</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>pour les enfants</strong>, le ministère de la Santé recommande de se faire vacciner contre l’hépatite B dès le premier mois (longs séjours), B.C.G., la rougeole dès l’âge de neuf ans, l’hépatite A (possible à partir d’un an) et la typhoïde à partir de cinq ans (longs séjours).</p>
<p>Pour en savoir plus, lisez notre article thématique <a href="services-aux-citoyens-preparer-son-expatriation-sante.md" class="spip_in">Santé</a>.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://europa.eu/travel/healthy/index_fr.htm" class="spip_out" rel="external">EUROPA (Voyager en Europe, rubrique Santé)</a></p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/entree-et-sejour/article/vaccination-111283). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
