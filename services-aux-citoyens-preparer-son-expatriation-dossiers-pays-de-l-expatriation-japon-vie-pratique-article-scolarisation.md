# Scolarisation

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/vie-pratique/article/scolarisation#sommaire_1">Les établissements scolaires français au Japon</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/vie-pratique/article/scolarisation#sommaire_2">Enseignement supérieur</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Les établissements scolaires français au Japon</h3>
<p>Pour toute information sur la scolarisation dans le système français à l’étranger, vous pouvez consulter nos thématiques sur <a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures.md" class="spip_in">les études</a> et la <a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais.md" class="spip_in">scolarisation</a>.</p>
<p>Vous y trouverez des renseignements sur :</p>
<ul class="spip">
<li>les établissements français du primaire et du secondaire à l’étranger ;</li>
<li>les bourses scolaires et la prise en charge des frais de scolarité à l’étranger ;</li>
<li>les possibilités qui s’offrent à vous si votre enfant ne peut être scolarisé à l’étranger dans le système français (enseignement à distance par le CNED, programme français langue maternelle (FLAM), internats en France) ;</li>
<li>les épreuves du baccalauréat à l’étranger ;</li>
<li>les bourses d’études supérieures en France et à l’étranger ;</li>
<li>l’équivalence des diplômes.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Enseignement supérieur</h3>
<p>Il est possible de poursuivre au Japon des études supérieures,</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Soit comme un natif, à condition de passer le test national d’entrée dans les universités et de réussir le concours d’admission de l’université recherchée qui s’avèrent souvent difficiles. L’Option Internationale du Baccalauréat (OIB) présentée en japonais (première session mise en place en 2003) ne dispense pas du concours d’entrée dans les universités japonaises mais constitue un avantage lors de l’examen des dossiers de candidature.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Soit en tant qu’étudiant étranger, justifiant de 12 ans de scolarité minimum, grâce aux nombreux programmes mis en place par les universités publiques et privées japonaises, à condition de prouver que l’on est déjà inscrit dans une université en France (ou que l’on peut en intégrer une) et de réussir le test de compétence en langue japonaise (EJU) : <a href="http://www.jasso.go.jp/index_e.html" class="spip_out" rel="external">www.jasso.go.jp/index_e.html</a>(<strong>Japan student services Organization</strong>)</p>
<p>Les services culturels et scientifiques de l’ambassade du Japon en France proposent des bourses du gouvernement japonais destinés aux étudiants. La Maison universitaire France-Japon à Strasbourg renseigne sur les échanges possibles entre universités :</p>
<p><a href="http://www.fr.emb-japan.go.jp/" class="spip_out" rel="external">Ambassade du Japon en France</a><br class="manualbr">Service culturel<br class="manualbr">7, Avenue Hoche<br class="manualbr">75008 PARIS <br class="autobr">Tél. : 01 48 88 63 76</p>
<p><a href="http://mufrancejapon.u-strasbg.fr/" class="spip_out" rel="external">Maison universitaire France-Japon</a><br class="manualbr">42a, avenue de la Forêt Noire<br class="manualbr">67000 Strasbourg<br class="manualbr">Tél. : 03 90 24 20 12<br class="manualbr">Télécopie : 03 90 24 20 19<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/vie-pratique/article/scolarisation#mu#mc#japon.u-strasbg.fr#" title="mu..åt..japon.u-strasbg.fr" onclick="location.href=mc_lancerlien('mu','japon.u-strasbg.fr'); return false;" class="spip_mail">Courriel</a></p>
<p>Le site du ministère des Affaires étrangères du Japon <a href="http://www.studyjapan.go.jp/" class="spip_out" rel="external">Study in Japan</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/vie-pratique/article/scolarisation). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
