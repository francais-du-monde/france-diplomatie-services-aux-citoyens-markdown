# Santé

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/vie-pratique/article/sante#sommaire_1">Vaccinations</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/vie-pratique/article/sante#sommaire_2">Précautions à prendre dans la vie pratique</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/vie-pratique/article/sante#sommaire_3">Médecine de soins</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/vie-pratique/article/sante#sommaire_4">Médecins</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/vie-pratique/article/sante#sommaire_5">Hôpitaux</a></li></ul>
<p>Pour une présentation des risques épidémiologiques ainsi que des conseils sur la médecine au quotidien, consultez les pages suivantes :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/suede/" class="spip_in">Page dédiée à la santé en Suède</a> sur le site Conseils aux voyageurs.</p>
<p><strong>Guide général de la santé en Suède : </strong> <i>Vårdguiden</i></p>
<ul class="spip">
<li><a href="http://www.1177.se/Stockholm/Other-languages/Franska/" class="spip_out" rel="external">Informations en français</a></li>
<li><a href="http://www.1177.se/Stockholm/Other-languages/Engelska/" class="spip_out" rel="external">Informations en anglais</a>.</li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Vaccinations</h3>
<p>Selon l’<a href="http://www.who.int/countries/swe/en/" class="spip_out" rel="external">Organisation Mondiale de la Santé</a>, il n’est pas nécessaire de se vacciner contre une maladie particulière pour entrer en Suède.</p>
<p>Cependant, les vaccinations de base doivent être à jour. Cela inclut : rougeole-oreillons-rubéole (ROR), le vaccin diphtérie-tétanos-coqueluche, la varicelle, la polio et le vaccin annuel contre la grippe.</p>
<p>En Suède, il existe un programme de vaccination systématique des enfants, et ce contre neuf maladies :</p>
<p>Diphtérie, coqueluche, poliomélyte, rougeole, oreillons, rubéole, les infections causées par l’haemophilus grippal de type B, les pneumocoques, et maintenant, pour les filles, la vaccination contre le HPV (Human Papillomavirus).</p>
<p>Voir également la <a href="http://wwwnc.cdc.gov/travel/destinations/traveler/none/sweden" class="spip_out" rel="external">fiche Suède</a> du Center for Disease Control  Prevention</p>
<p><strong>Les maladies et leur fréquence en Suède</strong></p>
<p><a href="http://www.vaccination.nu/vaccin/SE.cfm" class="spip_out" rel="external">Vaccination</a></p>
<p><strong>Les maladies à risque faible/modéré en Suède :</strong></p>
<ul class="spip">
<li><strong>Diphtérie (rare)</strong></li>
<li><strong>Hépatite A</strong></li>
<li><strong>Hépatite B</strong></li>
<li><strong>Méningocoque</strong></li>
<li><strong>Pneumocoque</strong></li>
<li><strong>Poliomyélite (rare)</strong></li>
<li><strong>Tétanos</strong></li>
<li><strong>Tuberculose</strong></li>
<li><strong>Turista</strong></li></ul>
<p><strong>Maladies inexistantes en Suède :</strong></p>
<ul class="spip">
<li><strong>Fièvre jaune</strong></li>
<li><strong>Encéphalite japonaise</strong></li>
<li><strong>Choléra</strong></li>
<li><strong>Malaria</strong></li>
<li><strong>Rage</strong></li>
<li><strong>Fièvre typhoïde</strong></li></ul>
<p>Il existe un risque d’encéphalite à tiques (TBE) sur la côte Ouest de la Suède, dans l’archipel de Stockholm, sur la côte Ouest du Lac Mälaren, sur les bords des lacs Vänern et Vättern et dans les zones rurales en été, qui est transmise par les tiques.</p>
<p><strong>Il est recommandé de se vacciner contre le TBE.</strong></p>
<h3 class="spip"><a id="sommaire_2"></a>Précautions à prendre dans la vie pratique</h3>
<p>Le SIDA est présent partout dans le monde et les mesures de prévention doivent être appliquées où que l’on soit. La surveillance des personnes ayant contracté le SIDA est très stricte et le dépistage systématique est fréquemment effectué.</p>
<ul class="spip">
<li><a href="http://www.karolinska.se/?splitoption=skip" class="spip_out" rel="external">Centre de dépistage MST/SIDA</a></li>
<li><a href="http://www.rfsu.se/sv/RFSU-kliniken/" class="spip_out" rel="external">Clinique RSFU (santé sexuelle)</a></li>
<li><a href="http://www.rfsl.se/halsa/?p=5706" class="spip_out" rel="external">Santé sexuelle</a></li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Médecine de soins</h3>
<p>Toutes les personnes domiciliées en Suède sont couvertes par le système national d’assurance maladie (<i>Försäkringskassan</i>). Les soins de santé publique sont placés sous la responsabilité des conseils généraux (<i>Landsting</i>) des régions et des communes.</p>
<p>La sécurité sociale en Suède fonctionne sur le principe des forfaits annuels : chaque citoyen paie ses soins, ses soins dentaires, ses soins hospitaliers et ses médicaments jusqu’à un certain plafond à partir duquel ils sont pris en charge par <i>Försäkringskassan</i> pendant 12 mois à compter de la première consultation : les consultations ne sont intégralement prises en charge qu’en cas de dépassement d’un seuil annuel de dépenses de 1100 SEK.</p>
<p>La prise en charge des médicaments par <i>Försäkringskassan</i> des médicaments est également soumise à l’acquittement préalable d’une franchise (dépense minimum de 2200 SEK par an).</p>
<p>Dans les centres médicaux et les services d’urgence hospitaliers, le ticket modérateur est de 200 SEK pour les adultes de + de 18 ans. Il est de 350 SEK pour un spécialiste. Ces honoraires peuvent être dépassés chez les praticiens du secteur privé.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.1177.se/Stockholm/Regler-och-rattigheter/Patientavgifter-i-Stockholm/" class="spip_out" rel="external">Forfaits médicaux</a> dans la région de Stockholm</p>
<p>Pour les citoyens de l’UE, il faut être muni de sa carte européenne d’assurance maladie pour bénéficier des mêmes tarifs que les citoyens suédois.</p>
<p>Le coût d’une consultation sans cette carte européenne est de 1775 SEK.</p>
<p>Les soins dentaires sont entièrement payants pour les adultes de plus de 30 ans. Cependant il existe un système de palier, où passée une franchise de 3000 SEK, <i>Försäkringskassan</i> prend en charge 50% des frais dentaires pour le palier 3000 SEK à 15 000 SEK, puis prend en charge 85% des frais au-delà de 15 000 SEK, sur une période de 12 mois.</p>
<p>Les contrôles périodiques sont partiellement pris en charge pour les personnes de 19 à 29 ans, ils peuvent être pris en charge lorsque le patient a souscrit une assurance privée. Les soins sont gratuits pour les mineurs. Toutefois pour les soins d’orthodontie de ces derniers, un agrément préalable est nécessaire.</p>
<p>En cas d’hospitalisation, le forfait journalier est de 80 SEK (environ 9 €).</p>
<h3 class="spip"><a id="sommaire_4"></a>Médecins</h3>
<p>La Suède est un des pays les plus avancés au monde dans le domaine médical et pour la recherche. Les hôpitaux sont très bien équipés en matériel moderne. La qualité des soins, examens ou analyses est en principe très bonne, mais le manque de personnel médical (médecins, infirmières) oblige à prendre des rendez-vous longtemps à l’avance (surtout avec les spécialistes) et à de longues heures d’attente dans les services hospitaliers, même aux urgences.</p>
<h3 class="spip"><a id="sommaire_5"></a>Hôpitaux</h3>
<p><strong>Numéros utiles</strong></p>
<ul class="spip">
<li><a href="http://www.varden.se/kategori/566/akutsjukv%C3%A5rd/42/stockholm" class="spip_out" rel="external">Centres d’urgence de soins à Stockholm</a></li>
<li><a href="http://www.cityakuten.se/" class="spip_out" rel="external">Médecine générale</a></li>
<li><a href="http://www.citytandakut.se/" class="spip_out" rel="external">Urgences dentaires</a><br class="manualbr">Tél. : 08 32 45 00 (soins dentaires)</li></ul>
<p><strong>Grands hôpitaux de Stockholm : </strong></p>
<ul class="spip">
<li><a href="http://www.ds.se/" class="spip_out" rel="external">Hôpital de Danderyd</a> <br class="manualbr">Tél. : 0812355000</li>
<li><a href="http://www.karolinska.se/en/" class="spip_out" rel="external">Hôpital Karolinska</a> <br class="manualbr">Tél. : 08 517 700 00  <br class="manualbr">Huddinge +46 8-585 800 00 <br class="manualbr">Solna : +46 8-517 700 00</li></ul>
<ul class="spip">
<li><a href="http://www.karolinska.se/AstridLindgrensBarnsjukhus/" class="spip_out" rel="external">Hôpital pour enfants Astrid Lindgren</a></li>
<li><a href="http://www.sankterik.se/en/home/" class="spip_out" rel="external">Hôpital Sankt Erik</a><br class="manualbr">Tél. : 08 672 31 00 (ophtalmologie)</li></ul>
<ul class="spip">
<li><a href="http://www.sodersjukhuset.se/Functions/InEnglish/" class="spip_out" rel="external">Hôpital Sodersjukhus</a> : <br class="manualbr">Tél. : 08 616 10 00</li>
<li><a href="http://www.sodertaljesjukhus.se/" class="spip_out" rel="external">Hôpital Södertälje</a> <br class="manualbr">Tél. : 08 550 24 000</li>
<li><a href="http://capiostgoran.se/english/" class="spip_out" rel="external">Hôpital Sankt Göran</a> <br class="manualbr">Tél. : 08 58 70 10 00</li></ul>
<p>Il existe des dispensaires (<i>vårdcentraler</i>) dans tous les quartiers de Stockholm.</p>
<p><strong>A Göteborg : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.sahlgrenska.se/sv/SU/Sahlgrenska-Universitetssjukhuset/" class="spip_out" rel="external">Hôpital Sahlgrenska</a> <br class="manualbr">Tél. : 031 342 10 00  </p>
<p><strong>A Malmö :</strong></p>
<ul class="spip">
<li><a href="http://www.skane.se/sus/" class="spip_out" rel="external">Hôpital de Scanie</a></li>
<li>Malmö Almänna Sjukhus ("M.A.S.")<br class="manualbr">Tél. : 040 33 10 00</li></ul>
<ul class="spip">
<li><a href="http://www.akademiska.se/" class="spip_out" rel="external">Uppsala</a></li>
<li><a href="https://www.vll.se/default.aspx?id=25719refid=1926" class="spip_out" rel="external">Hôpital du Norrland</a> (Umeå)</li>
<li><a href="http://www.nll.se/sv/Halsa-och-sjukvard/Sjukhus/Sunderby-sjukhus/" class="spip_out" rel="external">Hôpital Luleå</a></li></ul>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li><a href="http://www.1177.se/Stockholm/" class="spip_out" rel="external">Varguiden</a></li>
<li><a href="http://www.forsakringskassan.se/" class="spip_out" rel="external">Forsakringskassan (sécurité sociale)</a><br class="manualbr">Tél. : +46 0771-524524</li></ul>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/vie-pratique/article/sante). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
