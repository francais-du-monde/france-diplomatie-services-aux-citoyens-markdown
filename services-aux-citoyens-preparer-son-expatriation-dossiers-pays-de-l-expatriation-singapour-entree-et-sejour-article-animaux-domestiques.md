# Animaux domestiques

<h4 class="spip">Chiens et chats</h4>
<p>l’Autorité de Singapour chargée du secteur agroalimentaire et des affaires vétérinaires (AVA : Agri-Food and Veterinary Authority) est l’organisme compétent en matière d’importation d’animaux sur le sol singapourien.</p>
<p>Depuis le 1er janvier 2010, les nouvelles conditions pour l’importation de chiens et de chats distinguent quatre catégories de pays, au lieu des deux catégories précédentes :</p>
<ul class="spip">
<li><strong>catégorie A</strong> (pays historiquement indemnes de rage) ;</li>
<li><strong>catégorie B</strong> (pays présentant un risque négligeable de rage) ;</li>
<li><strong>catégorie C </strong>(pays présentant un risque maîtrisé à l’égard de la rage ou ayant une faible incidence de cette maladie) ;</li>
<li><strong>catégorie D</strong> (autres pays).</li></ul>
<p>A l’exception des pays classés dans la catégorie A, tous les pays sont soumis à une nouvelle exigence de réalisation de tests sérologiques avant export.</p>
<p><strong>La France a été classée en catégorie C, au regard des critères sanitaires retenus par l’AVA, au même titre que l’Autriche, la Belgique, le Canada, l’Espagne, les Etats-Unis et les Pays-Bas</strong>.</p>
<p>En conséquence, les démarches préalables à l’exportation sont désormais les suivantes :</p>
<table class="spip">
<thead><tr class="row_first"><th id="id344d_c0">Action</th><th id="id344d_c1">Délai avant exportation</th><th id="id344d_c2">Remarques</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="id344d_c0">Identification électronique</td>
<td headers="id344d_c1"></td>
<td headers="id344d_c2"></td></tr>
<tr class="row_even even">
<td headers="id344d_c0">Réservation d’une place dans la station de quarantaine</td>
<td headers="id344d_c1">au minimum deux mois</td>
<td headers="id344d_c2"></td></tr>
<tr class="row_odd odd">
<td headers="id344d_c0">Licence d’importation à obtenir auprès de l’AVA</td>
<td headers="id344d_c1">30 jours</td>
<td headers="id344d_c2"></td></tr>
<tr class="row_even even">
<td headers="id344d_c0">Vaccinations autres que rage</td>
<td headers="id344d_c1">au moins 14 jours</td>
<td headers="id344d_c2"><strong>chiens</strong> : maladie de Carré, hépatite infectieuse canine parvovirose canine
<p><strong>chats</strong> : calicivirus félin, rhinotrachéite féline, panleucopénie féline, Chlamydophila felis</p>
</td></tr>
<tr class="row_odd odd">
<td headers="id344d_c0">Traitement contre les parasites externes (puces et tiques)</td>
<td headers="id344d_c1">entre deux et dix jours</td>
<td headers="id344d_c2"></td></tr>
<tr class="row_even even">
<td headers="id344d_c0">Traitement contre les parasites internes (nématodes et cestodes)</td>
<td headers="id344d_c1">dans les quatre jours précédant l’exportation</td>
<td headers="id344d_c2"></td></tr>
<tr class="row_odd odd">
<td headers="id344d_c0">Certificat sanitaire</td>
<td headers="id344d_c1">entre deux et sept jours avant l’exportation</td>
<td headers="id344d_c2">selon le modèle de l’AVA (tout autre modèle de certificat sera refusé)</td></tr>
<tr class="row_even even">
<td headers="id344d_c0"><strong>Catégorie C1</strong></td>
<td headers="id344d_c1"></td>
<td headers="id344d_c2"></td></tr>
<tr class="row_odd odd">
<td headers="id344d_c0">Vaccination antirabique</td>
<td headers="id344d_c1">1e injection au moins quatre mois avant l’exportation et 2e injection au moins un mois après la première et au moins un mois avant l’exportation</td>
<td headers="id344d_c2"></td></tr>
<tr class="row_even even">
<td headers="id344d_c0">Contrôle sérologie des anticorps rabiques</td>
<td headers="id344d_c1">au moins un mois après la 1e vaccination et dans les 6 mois avant l’exportation</td>
<td headers="id344d_c2"></td></tr>
<tr class="row_odd odd">
<td headers="id344d_c0"><strong>Catégorie C2</strong></td>
<td headers="id344d_c1"></td>
<td headers="id344d_c2"></td></tr>
<tr class="row_even even">
<td headers="id344d_c0">Vaccination antirabique</td>
<td headers="id344d_c1">au minimum trois mois avant l’exportation</td>
<td headers="id344d_c2"></td></tr>
<tr class="row_odd odd">
<td headers="id344d_c0">Contrôle sérologie des anticorps rabiques</td>
<td headers="id344d_c1">au moins 1 mois après la vaccination et dans les six mois avant l’exportation</td>
<td headers="id344d_c2"></td></tr>
</tbody>
</table>
<p>A l’arrivée, une <strong>quarantaine obligatoire</strong> sera appliquée d’au moins :</p>
<ul class="spip">
<li><strong>10 jours</strong> pour les animaux ayant rempli les conditions associées à la <strong>catégorie C1</strong> ;</li>
<li><strong>30 jours</strong> pour les animaux ayant rempli les conditions associées à la <strong>catégorie C2</strong>.</li></ul>
<p>Le recours à un transitaire local peut faciliter les démarches préalables à l’arrivée.</p>
<p>Pour télécharger la version bilingue du certificat sanitaire exigé par l’AVA, <a href="http://www.ambafrance-sg.org/IMG/doc/Certificat_veterinaire_CatC_FR-EN_2_.doc" class="spip_out" rel="external">cliquer ici</a>.</p>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li>Notre article thématique <a href="services-aux-citoyens-preparer-son-expatriation-douanes-article-animaux-domestiques.md" class="spip_in">Animaux domestiques</a> ;</li>
<li><a href="http://www.ambafrance-sg.org/Conditions-d-importation-d-animaux" class="spip_out" rel="external">Conditions d’importation d’animaux</a> sur le site de l’Ambassade de France à Singapour ;</li>
<li>le <a href="http://www.ava.gov.sg/" class="spip_out" rel="external">site de l’AVA</a>.</li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/entree-et-sejour/article/animaux-domestiques). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
