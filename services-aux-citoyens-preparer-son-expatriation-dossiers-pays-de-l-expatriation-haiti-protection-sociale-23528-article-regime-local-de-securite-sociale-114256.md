# Régime local de sécurité sociale

<p>La protection sociale des travailleurs inclut l’assurance vieillesse, l’assurance maladie, l’assurance maternité, l’assurance contre les accidents de travail, les prestations légales en cas de licenciement abusif.</p>
<p>Toute entreprise opérant en Haïti doit assumer certaines charges sociales, notamment :</p>
<ul class="spip">
<li>Le bonus annuel, soit 1/12 de la rémunération totale annuelle, payable décembre de chaque année ;</li>
<li>Le congé annuel de quinze jours payés auquel tous les travailleurs ont droit ;</li>
<li>Le congé de maternité de six semaines payées auquel la femme enceinte est éligible ;</li>
<li>Le congé de maladie de quinze jours payés, lequel peut courir jusqu’à trois mois au maximum, moyennant présentation d’un certificat médical émanant d’un Service de Santé Publique ou du médecin de l’entreprise ;</li>
<li>Le service médical de l’entreprise à la seule charge de l’employeur ;</li>
<li>L’assurance contre les accidents de travail, la maladie et la maternité (OFATMA), sous la responsabilité exclusive de l’employeur, 2 % des salaires payés pour les entreprises commerciales, 3 % pour les entreprises agricoles, industrielles, de construction et les agences des lignes de navigation, 6 % pour les entreprises minières ;</li>
<li>L’assurance vieillesse et incapacité (ONA) payable mensuellement à part égale par l’employeur et l’ouvrier 2 à 6 % pour chacun, suivant les niveaux de salaire.</li></ul>
<p><strong>Adresse utiles :</strong></p>
<p><strong>OFATMA </strong><br class="manualbr">Office d’assurance accidents du travail, maladie et maternité<br class="manualbr">Cité Militaire<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/protection-sociale-23528/article/regime-local-de-securite-sociale-114256#contact#mc#ofatma.gouv.ht#" title="contact..åt..ofatma.gouv.ht" onclick="location.href=mc_lancerlien('contact','ofatma.gouv.ht'); return false;" class="spip_mail">Courriel</a> <br class="manualbr">Tél. : 3402-0426 / 3815-8668 / 3786-5759 3874-0770</p>
<p><strong>ONA</strong><br class="manualbr">Office National d’Assurance Vieillesse<br class="manualbr">Autoroute de Delmas (en face Delmas 17)<br class="manualbr">Tél. : 2813-1214</p>
<p><strong>Ministère des Affaires social et du Travail</strong><br class="manualbr">7, Avenue Charles Summer <br class="manualbr">Tél. : (509) 29 40 09 28<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/protection-sociale-23528/article/regime-local-de-securite-sociale-114256#maffairesociale#mc#yahoo.fr#" title="maffairesociale..åt..yahoo.fr" onclick="location.href=mc_lancerlien('maffairesociale','yahoo.fr'); return false;" class="spip_mail">Courriel</a></p>
<p><i>Mise à jour : août 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/protection-sociale-23528/article/regime-local-de-securite-sociale-114256). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
