# Fiscalité du pays

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/fiscalite/article/fiscalite-du-pays#sommaire_1">Année fiscale</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/fiscalite/article/fiscalite-du-pays#sommaire_2">Déclarations de revenus</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/fiscalite/article/fiscalite-du-pays#sommaire_3">Barème de l’impôt</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/fiscalite/article/fiscalite-du-pays#sommaire_4">Solde du compte en fin de séjour</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Année fiscale</h3>
<p>L’année fiscale en Inde commence le 1er avril d’une année et se termine le 31 mars de l’année suivante.</p>
<h3 class="spip"><a id="sommaire_2"></a>Déclarations de revenus</h3>
<p>Une société est tenue de déposer sa déclaration de revenus (<i>income tax return</i>) pour l’année fiscale, <strong>au plus tard le 31 octobre de l’année suivante</strong>. L’imprimé à utiliser est le formulaire N° ITR -7.</p>
<p>En ce qui concerne un particulier, il doit déposer sa déclaration de revenus pour l’année fiscale se terminant le 31 mars, <strong>au plus tard le 30 juin suivant sauf s’il dispose également de revenus émanant des affaires ou d’une profession</strong> (<i>income from a business or profession</i>). Dans ce dernier cas, la date limite fixée est le 31 août.</p>
<h4 class="spip">Lieu de dépôt des déclarations de revenus</h4>
<p>Il est désormais possible de payer l’<i>Income Tax</i> en ligne, en remplissant le formulaire soi-même ou via une tierce personne. Les personnes aux revenus dépassant 500 000 INR par an, toutes sources de revenus confondues, doivent obligatoirement payer leurs impôts en ligne. Les personnes dont les revenus sont inférieurs à 500 000 INR et dont l’épargne est inférieure à 10 000 INR peuvent procéder à une déclaration physique de leurs revenus.</p>
<p>L’administration fiscale indienne dispose de bureaux dans chaque ville indienne principale. Les déclarations de revenus doivent être fournies au bureau ayant juridiction sur le quartier où réside le contribuable.</p>
<p>Toutefois, dans certaines villes importantes il peut exister des bureaux autorisés à réceptionner les déclarations relatives à une branche particulière de l’activité sans tenir compte de la juridiction géographique.</p>
<h4 class="spip">Les modalités de paiement des impôts pour un salarié, pour une activité non salariée et pour une société</h4>
<p>En vertu de l’article 208 de l’<i>Income Tax Act</i>, dès lors que le contribuable, <strong>qu’il soit une société ou un particulier</strong>, estime que le montant des impôts à acquitter par lui pour une année fiscale sera de 10 000 roupies indiennes ou plus, il lui appartiendra d’effectuer les paiements d’impôt à l’avance et conformément à un calendrier fixé par l’administration indienne. Il s’agit du schéma d’<i>Advance Tax</i>.</p>
<h4 class="spip">Sociétés</h4>
<p>S’agissant des sociétés, elles doivent verser des impôts ainsi estimés selon le calendrier suivant :</p>
<ul class="spip">
<li>Un montant n’étant pas inférieur à 15% du montant total de l’impôt annuel estimé : 15 juin de l’année fiscale au plus tard ;</li>
<li>Un montant n’étant pas inférieur à 45% du montant total de l’impôt annuel estimé : 15 septembre au plus tard ;</li>
<li>Un montant n’étant pas inférieur à 75% du montant total de l’impôt annuel estimé : 15 décembre au plus tard ;</li>
<li>La totalité de l’impôt annuel estimé : 15 mars de l’année suivante au plus tard.</li></ul>
<p>Au cas où le montant de l’impôt annuel estimé et versé par la société est inférieur à 90% du montant définitivement déterminé par l’administration comme étant l’impôt dû pour l’année fiscale en question, la société devra acquitter des intérêts sur la différence de l’impôt dû au taux de 1% par mois.</p>
<h4 class="spip">Personnes physiques</h4>
<p>S’agissant des salaires, l’employeur est tenu de <strong>déduire à la source</strong> l’impôt à acquitter par le salarié et le verser à intervalles fixés par l’administration fiscale.</p>
<p>Pour le restant de leur impôt, les contribuables doivent respecter le calendrier suivant :</p>
<ul class="spip">
<li>Un montant n’étant pas inférieur à 30% du montant total de l’impôt annuel estimé : 15 septembre de l’année fiscale au plus tard ;</li>
<li>Un montant n’étant pas inférieur à 60% du montant total de l’impôt annuel estimé : 15 décembre au plus tard ;</li>
<li>La totalité de l’impôt annuel estimé : 15 mars de l’année suivante au plus tard.</li></ul>
<p>Au cas où le montant de l’impôt annuel estimé et versé par le particulier est inférieur à 90% du montant définitivement déterminé par l’administration comme étant l’impôt dû pour l’année fiscale en question, il devra acquitter des intérêts sur la différence de l’impôt dû au taux de 1% par mois.</p>
<h3 class="spip"><a id="sommaire_3"></a>Barème de l’impôt</h3>
<h4 class="spip">Personnes physiques</h4>
<p>Les personnes physiques sont assujetties à l’<i>Income Tax</i> selon le barème détaillé ci-dessous :</p>
<table class="spip">
<thead><tr class="row_first"><th id="idbac6_c0">Montants à déclarer en roupies indiennes (INR)</th><th id="idbac6_c1">Taux d’imposition</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="idbac6_c0">0- 200 000</td>
<td headers="idbac6_c1">nul</td></tr>
<tr class="row_even even">
<td headers="idbac6_c0">200 001- 500 000</td>
<td headers="idbac6_c1">10 % de la somme dépassant le seuil de 200 000 INR</td></tr>
<tr class="row_odd odd">
<td headers="idbac6_c0">500 001 – 1 000 000</td>
<td headers="idbac6_c1">30 000 INR + 20 % de la somme dépassant le seuil de 500 000 INR</td></tr>
<tr class="row_even even">
<td headers="idbac6_c0">au-delà de 1 000 000</td>
<td headers="idbac6_c1">130 000 INR + 30 % de la somme dépassant 1 000 000 INR</td></tr>
</tbody>
</table>
<p>Une taxe dite <i>Education Cess</i> d’un montant de 3 % est également appliquée. Enfin une surtaxe de 10 % s’ajoute lorsque le montant global des revenus dépasse 10 millions de roupies.</p>
<p>Au moment du calcul de l’impôt, il convient de prendre en considération les différentes déductions que la législation indienne permet. Elles sont de divers ordres :</p>
<ul class="spip">
<li>prise en charge des loyers (plafonné normalement à 10 % du salaire), des frais de transport, des dépenses personnelles des cadres sous réserve des plafonds fixés ;</li>
<li>prise en charge par la société des billets d’avion pour les vacances et frais de scolarité des enfants, sous réserve des plafonds fixés ;</li>
<li>prise en charge par la société des frais de maladie/hospitalisation, sous réserve des plafonds fixés ;</li>
<li>les intérêts sur les comptes épargne sont exemptés d’<i>Income Tax </i>à hauteur de 10 000 INR maximum ;</li>
<li>l’entreprise déduit 12% du salaire de l’employé au titre de l’<i>Employees’ Provident Fund</i> (un système de sécurité sociale fournissant une assurance et une pension de retraite pour les salariés) ;</li>
<li>assurances vie souscrites (maximum 20% de la somme déposée), fonds communs de placement, placements à terme sous réserve des plafonds fixés ;</li>
<li>crédit lié à la construction ou à l’achat d’un logement en Inde, sous réserve des plafonds fixés.</li></ul>
<h4 class="spip">Personnes morales</h4>
<p>Société de droit étranger (ex : société non-résidente, succursale, agence bancaire, chantier) :</p>
<ul class="spip">
<li>en règle générale, les règlements dus aux sociétés étrangères sont imposés à la source ;</li>
<li>le taux de l’impôt sur les sociétés est de 42,02 % ;</li>
<li>rémunération du transfert de savoir-faire ou pour l’assistance technique : 10,56% sur le montant brut.</li></ul>
<p>Selon la Convention franco-indienne, l’Inde est dans l’obligation d’étendre à une entreprise française le taux d’imposition le plus avantageux accordé par le pays à un Etat tiers, membre de l’OCDE. La Convention indo-allemande fixant le taux à 10 % pour ce type de revenu, ce dernier taux s’applique à une société française.</p>
<p>La société française peut bénéficier d’un crédit d’impôt en France pour cette catégorie de recettes. Il est recommandé d’approcher un expert-comptable ou un avocat en France pour s’assurer de pouvoir bénéficier de cet avantage.</p>
<p>Il est possible pour une société française souhaitant investir en Inde de connaître à l’avance le régime fiscal applicable. Un comité officiel appelé <i>Authority for Advance Rulings</i> et présidé par un juge se prononcera sur les questions relevant de la fiscalité directe, qui lui seront soumises. Cet <i>advance ruling</i> devra être rendu par le comité dans un délai de six mois à compter de la date de la demande.</p>
<p>Société de droit indien (ex. : société créée selon le <i>Companies Act</i> de 1956 même si son capital est détenu à 100 % par une entreprise étrangère) : le taux de l’impôt sur les sociétés est de 32,45 %.</p>
<h4 class="spip">Taux d’imposition des plus-values</h4>
<p>Impôt sur les plus-values à long terme : 20 %.</p>
<h4 class="spip">Taux d’imposition des revenus financiers pour les personnes physiques et les entreprises</h4>
<p>Impôt sur les dividendes : l’actionnaire qui perçoit les dividendes ne paie plus d’impôt sur les dividendes en Inde. En revanche, la société indienne déclarant et distribuant des bénéfices sera imposée à 16,22 %.</p>
<h3 class="spip"><a id="sommaire_4"></a>Solde du compte en fin de séjour</h3>
<p>Les banques ont été autorisées à convertir le solde du compte bancaire d’un touriste étranger (en pratique les formalités d’ouverture de compte bancaire sont souvent longues) au moment du départ à condition que le compte ait été maintenu pour une période n’excédant pas six mois et qu’il n’ait pas été crédité de fonds locaux, autres que les intérêts courus. Hormis ce cas, les expatriés ne peuvent solder leur compte en fin de séjour et doivent écouler leur solde de roupies localement.</p>
<p>Les touristes ayant effectué un long séjour en Inde doivent donc conserver tous les documents prouvant qu’ils ont changé des devises à la banque.</p>
<h4 class="spip">Reconversion de la roupie indienne vers l’euro</h4>
<p>Afin de reconvertir en sa monnaie nationale ses avoirs, la personne étrangère pourra se voir réclamer la déclaration du changeur initial, attestant que la première transaction avait été effectuée par un agent agréé par le gouvernement.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.finmin.nic.in/" class="spip_out" rel="external">Ministry of Finance à New Delhi</a></p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/fiscalite/article/fiscalite-du-pays). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
