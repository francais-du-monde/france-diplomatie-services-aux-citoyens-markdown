# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/vie-pratique/article/transports#sommaire_1">Importation de véhicule</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/vie-pratique/article/transports#sommaire_2">Permis de conduire</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/vie-pratique/article/transports#sommaire_3">Code de la route</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/vie-pratique/article/transports#sommaire_4">Assurances et taxes</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/vie-pratique/article/transports#sommaire_5">Achat et location</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/vie-pratique/article/transports#sommaire_6">Immatriculation</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/vie-pratique/article/transports#sommaire_7">Entretien</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/vie-pratique/article/transports#sommaire_8">Réseau routier</a></li>
<li><a id="so_9" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/vie-pratique/article/transports#sommaire_9">Transports en commun</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Importation de véhicule</h3>
<p>L’importation de véhicules de plus de huit ans est interdite. Ne peuvent être autorisés à pénétrer au Sénégal, sous le couvert d’une autorisation temporaire d’admission (ATA) ou d’un carnet de passage en douane, que les véhicules de plus de cinq ans munis d’un carnet international de circulation. Ce document doit être délivré par l’Automobile club du pays d’immatriculation du véhicule, moyennant adhésion de son propriétaire et versement d’une caution restituée au retour du véhicule dans son pays d’origine. Au terme du délai légal de présence sur le territoire sénégalais, ces véhicules doivent faire l’objet d’une réexportation.</p>
<p>Cette interdiction ne vise pas les véhicules immatriculés dans les pays voisins du Sénégal, notamment la Mauritanie.</p>
<p>Il n’y a aucune restriction particulière quant à la marque ou modèle du véhicule importé. L’autorisation d’importation du véhicule est nécessaire. Les droits de douane sont calculés en faisant référence à un taux d’un montant de 57,98% (obtenu en ajoutant aux 700 000 FCFA de taux de fret les 80% de la valeur argus français du véhicule).</p>
<p><strong>Pour en savoir plus</strong> :</p>
<ul class="spip">
<li><a href="http://www.demarches.gouv.sn/" class="spip_out" rel="external">Demarches.gouv.sn</a></li>
<li><a href="http://www.senegalaisement.com/NOREF/importation_voiture_senegal.html" class="spip_out" rel="external">Dédouanement du véhicule</a></li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Permis de conduire</h3>
<p>Tout titulaire du permis de conduire étranger doit obligatoirement demander l’échange de son permis de conduire français contre un permis de conduire sénégalais un an après l’acquisition de sa résidence normale au Sénégal.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.demarches.gouv.sn/" class="spip_out" rel="external">Demarches.gouv.sn</a> </p>
<h3 class="spip"><a id="sommaire_3"></a>Code de la route</h3>
<p>Le code de la route en vigueur est le code français mais il est rare qu’il soit respecté. Il est fortement conseillé de conduire avec prudence et d’éviter de conduire la nuit (véhicules non éclairés, charrettes, animaux). La conduite en état d’ivresse est réprimée par la loi.</p>
<p>La vitesse est réglementée en ville à 50 km/h, hors agglomération à 90 km/h et sur autoroute à 100 km/h</p>
<p>Pour en savoir plus sur <a href="http://www.au-senegal.com/IMG/pdf/code-route-senegal.pdf" class="spip_out" rel="external">le code de la route en vigueur au Sénégal</a>.</p>
<h3 class="spip"><a id="sommaire_4"></a>Assurances et taxes</h3>
<p>L’assurance au tiers auprès d’une compagnie locale est obligatoire.</p>
<p>Pour en savoir plus sur les assureurs à Dakar : <a href="http://www.expatdakar.org/guide/index.htm" class="spip_out" rel="external">Expatdakar.org</a> / Services / assurances.</p>
<h3 class="spip"><a id="sommaire_5"></a>Achat et location</h3>
<p>Toutes les marques françaises sont représentées à Dakar ainsi que de nombreuses autres marques étrangères.</p>
<p>Il est conseillé d’utiliser un véhicule robuste équipé de la climatisation ; une visite technique annuelle est obligatoire.</p>
<p>Il est préférable d’acquérir son véhicule sur place, les frais de transitaire et de formalités étant élevés.</p>
<p>Des véhicules d’occasion sont achetables de particuliers à particuliers ou chez les concessionnaires, à tous les prix. Il convient de s’assurer de l’origine du véhicule pour éviter le recel de voitures volées en Europe.</p>
<p><strong>Pour savoir plus sur la visite technique</strong> :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.demarches.gouv.sn/" class="spip_out" rel="external">Demarches.gouv.sn</a>.</p>
<p>Toutes les sociétés de location de véhicules sont représentées à Dakar (coût à la journée : entre 30.000 FCFA pour une Citroën C3 et 65.000 FCFA pour une Mitsubishi Pajero).</p>
<p>Pour 100km/jour, les assurances prévoient une franchise de 1 000 000 FCFA HTVA en cas d’accident à tort.</p>
<p><strong>Pour en savoir plus</strong> :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.expatdakar.org/guide/index.htm" class="spip_out" rel="external">Expatdakar.org</a>. </p>
<h3 class="spip"><a id="sommaire_6"></a>Immatriculation</h3>
<p>Le certificat d’immatriculation et d’aptitude technique communément appelé « carte grise » est indispensable à toute mise en circulation d’un véhicule. Il permet l’identification du véhicule et de son propriétaire. C’est sur ce document que sont apposés les dates de fin de validité des visites techniques.</p>
<p>Il convient de fournir les documents suivants :</p>
<ul class="spip">
<li>Une demande de mise en circulation adressée à la <strong>division régionale des Transports terrestres</strong> revêtue du cachet et du visa du service des Impôts et domaines (DGID) obtenu après paiement d’une taxe dont le montant varie suivant la puissance du véhicule et sa destination (transport ou civil) ;</li>
<li>Une attestation de paiement de la taxe d’immatriculation au service fiscal régional ;</li>
<li>Une attestation d’assurance ;</li>
<li>Une copie certifiée conforme de la carte nationale d’identité ;</li>
<li>Un certificat de mise à la consommation (CMC) délivré par la direction des Douanes.</li></ul>
<p>Pour un véhicule neuf, fournir aussi :</p>
<ul class="spip">
<li>Un procès-verbal de réception du véhicule fourni par le représentant du constructeur et approuvé par la direction des Transports terrestres ;</li>
<li>Un acte de vente du représentant du constructeur.</li></ul>
<p>Pour un véhicule importé d’occasion, fournir aussi :</p>
<ul class="spip">
<li>Une déclaration de douane avec numéro du CMC et le cachet de la douane ;</li>
<li>L’original de la carte grise étrangère ;</li>
<li>Un acte de vente par le précédent propriétaire.</li></ul>
<p>La présentation du véhicule pour vérification du numéro de série et visite technique préalable est obligatoire.</p>
<p>Source : <a href="http://www.demarches.gouv.sn/" class="spip_out" rel="external">Demarches.gouv.sn</a>.</p>
<h3 class="spip"><a id="sommaire_7"></a>Entretien</h3>
<p>Les réparations ou l’entretien d’une voiture s’effectuent à Dakar où les garages sont nombreux ; s’il est assez aisé de se procurer des pièces détachées, les réparations de l’électronique peuvent être délicates : les pièces sont importées avec un important délai.</p>
<h3 class="spip"><a id="sommaire_8"></a>Réseau routier</h3>
<p>En dehors des grands axes, le réseau routier est mal entretenu. Les routes sont dangereuses et les pistes praticables seulement en dehors de la saison des pluies (de novembre à avril). A l’intérieur du pays et en dehors des grands axes, le véhicule 4X4 est recommandé.</p>
<p>La plupart des véhicules automobiles au Sénégal ne sont pas ou mal assurés, beaucoup sont en état vétuste, une vitesse modérée est donc recommandée.</p>
<p>En cas d’accident à Dakar, il convient de contacter, selon la zone considérée, soit le commissariat central (823.71.49/ 823.25.29), soit la gendarmerie (800.20.20).</p>
<p>Dans le reste du pays, en cas d’accident de la route, il convient de se rendre au premier poste de police ou de gendarmerie, afin d’éviter tout incident sur le lieu même de l’accident.</p>
<p>Se méfier des constats ou dédommagements "à l’amiable" proposés.</p>
<h3 class="spip"><a id="sommaire_9"></a>Transports en commun</h3>
<ul class="spip">
<li>Les voyages en <strong>autocar</strong> sont très inconfortables et très risqués, vu l’absence totale de fiabilité technique. De même, les minibus (appelés « cars rapides ») sont peu confortables et assez dangereux. Les taxis de brousse sont plus fiables.</li>
<li>En ville, le moyen de transport le plus courant est le véhicule personnel. Les <strong>bus et les taxis collectifs</strong> sont généralement bondés et à déconseiller. Le prix de la course en taxi individuel démarre à 300 FCFA. Les tarifs varient suivant la fluidité de la circulation : comptez autour de 2 000 francs CFA pour une course en ville.</li>
<li>La <strong>liaison maritime</strong> par bateau ferry-boat entre Dakar et Ziguinchor dure environ six heures.</li>
<li>Les déplacements en chemin de fer se limitent à la ligne Dakar/Bamako</li>
<li>Les liaisons aériennes sont assurées pour Ziguinchor et Cap Skirring (en saison). L’aéroport international Léopold Sédar Senghor de Dakar accueille tous les vols long courrier en provenance d’Europe et d’Afrique. Certains charters peuvent organiser, à certaines périodes, des vols directs jusqu’à Saint-Louis ou Cap Skirring.</li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/vie-pratique/article/transports). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
