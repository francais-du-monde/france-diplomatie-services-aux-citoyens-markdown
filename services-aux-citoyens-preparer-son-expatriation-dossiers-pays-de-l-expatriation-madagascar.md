# Madagascar

<p>Au <strong>31 décembre 2014</strong>, <strong>18 532 Français</strong> résidant à Madagascar étaient enregistrés au registre des Français établis hors de France.</p>
<p>La communauté française, jeune, comprend environ 25 000 personnes, avec une majorité de binationaux. C’est la seconde communauté étrangère après la communauté comorienne.</p>
<p>Les entreprises françaises sont très présentes à Madagascar : on dénombre en effet plus de 730 entreprises à capitaux français.</p>
<p>Les entreprises de moyenne et petite taille constituent la majeure partie des implantations françaises. Les entreprises de moins de 50 employés représentent à elles seules la moitié du total. Celles-ci sont particulièrement présentes dans le secteur des services. Les plus grands groupes français sont représentés dans des secteurs très diversifiés.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/dossiers-pays/madagascar/" class="spip_in">Une description de Madagascar, de sa situation politique et économique</a></li>
<li>Des informations actualisées sur les <a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/madagascar/" class="spip_in">conditions locales de sécurité à Madagascar</a></li>
<li><a href="http://www.ambafrance-mada.org/" class="spip_out" rel="external">Ambassade de France à Madagascar</a></li></ul>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-madagascar-entree-et-sejour.md">Entrée et séjour</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-madagascar-entree-et-sejour-article-passeport-visa-permis-de-travail.md">Passeport, visa, permis de travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-madagascar-entree-et-sejour-article-demenagement-111177.md">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-madagascar-entree-et-sejour-article-vaccination-111178.md">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-madagascar-entree-et-sejour-article-animaux-domestiques-111179.md">Animaux domestiques</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-madagascar-emploi-stage.md">Emploi, stage</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-madagascar-emploi-stage-article-marche-du-travail.md">Marché du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-madagascar-emploi-stage-article-reglementation-du-travail-111181.md">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-madagascar-emploi-stage-article-recherche-d-emploi-111182.md">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-madagascar-emploi-stage-article-curriculum-vitae-111183.md">Curriculum vitae</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-madagascar-emploi-stage-article-lettre-de-motivation-111184.md">Lettre de motivation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-madagascar-emploi-stage-article-stages.md">Stages</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-madagascar-protection-sociale.md">Protection sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-madagascar-protection-sociale-article-regime-local-de-securite-sociale-111187.md">Régime local de sécurité sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-madagascar-protection-sociale-article-convention-de-securite-sociale.md">Convention de sécurité sociale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-madagascar-fiscalite.md">Fiscalité</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-madagascar-fiscalite-article-fiscalite-du-pays.md">Fiscalité du pays</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-madagascar-fiscalite-article-convention-fiscale.md">Convention fiscale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-madagascar-vie-pratique.md">Vie pratique</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-madagascar-vie-pratique-article-logement-111191.md">Logement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-madagascar-vie-pratique-article-sante.md">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-madagascar-vie-pratique-article-scolarisation-111193.md">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-madagascar-vie-pratique-article-cout-de-la-vie-111194.md">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-madagascar-vie-pratique-article-transports.md">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-madagascar-vie-pratique-article-communications.md">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-madagascar-vie-pratique-article-loisirs-et-culture-111197.md">Loisirs et culture</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-madagascar-vie-pratique-article-pour-en-savoir-plus.md">Pour en savoir plus</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-madagascar-presence-francaise.md">Présence française</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-madagascar-presence-francaise-article-presence-francaise.md">Présence française</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
