# Vie pratique

<h2 class="rub22990">Logement</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/vie-pratique/#sommaire_1">Conditions de location</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/vie-pratique/#sommaire_2">Hôtels</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/vie-pratique/#sommaire_3">Electricité</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/vie-pratique/#sommaire_4">Electroménager</a></li></ul>
<h4 class="spip">Marché de l’immobilier</h4>
<p>Le prix des logements a considérablement augmenté ces dernières années et le nombre de biens mis en location est restreint, surtout dans les grandes villes. Les raisons sont multiples : l’accession à la propriété de nombreux locataires, le remplacement, dans les villes, des logements vétustes et bon marché par de nouvelles constructions modernes et chères, la nécessité de loger un nombre croissant de familles monoparentales.</p>
<p>Il est très difficile de trouver à se loger à Copenhague, surtout pour les personnes disposant d’un petit budget, et dans les villes universitaires où de nombreux logements sont occupés par les étudiants. Les offres de logements sont plus nombreuses à la périphérie de la capitale. Il faut compter entre un et 3 mois pour trouver un logement.</p>
<p>Bon nombre de Danois sont propriétaires de leur logement ou ont recours à la colocation. Les expatriés optent généralement pour la location d’un logement, surtout si la durée du séjour est limitée. Les personnes souhaitant s’installer durablement au Danemark et disposant de moyens financiers suffisants peuvent envisager d’acheter un logement.</p>
<h4 class="spip">Types de logement</h4>
<p>Il existe plusieurs types de logements au Danemark :</p>
<ul class="spip">
<li>les biens à la location : il peut s’agir d’appartements, de maisons ou de chambres loués par des particuliers ou des sociétés ;</li>
<li>la copropriété : il s’agit de bâtiments acquis par les locataires qui les gèrent, les louent et les entretiennent ;</li>
<li>la propriété. Les logements sont généralement confortables et spacieux. Il est possible de trouver des logements meublés.</li></ul>
<h4 class="spip">Recherche d’un logement</h4>
<p>Si vous disposez de moyens financiers suffisants et/ou si vous souhaitez trouver un logement avant votre arrivée au Danemark, vous pouvez recourir aux services d’un <strong>spécialiste en relocation</strong> qui se chargera de vous conseiller et de trouver, en fonction de vos besoins et de vos critères, le logement qui vous convient.</p>
<p>Le plus simple et le plus rapide est souvent de consulter les annonces immobilières publiées sur Internet. Les sites sont le plus souvent en danois. La consultation des annonces est libre, mais il faut souvent s’enregistrer et payer pour accéder aux annonces complètes et aux coordonnées des bailleurs.</p>
<p>Quelques termes à connaître pour vous aider dans vos recherches :</p>
<ul class="spip">
<li><i>Bolig</i> : logement</li>
<li><i>Leje </i> : location</li>
<li><i>Søge</i> : chercher</li>
<li><i>Købe</i> : acheter</li>
<li><i>Fremleje </i> : sous-location</li>
<li><i>Max leje</i> : loyer maximum</li>
<li><i>Boligtype</i> : type de logement</li>
<li><i>Lejlighed </i> : appartement</li>
<li><i>Kollektiv</i> : colocation</li>
<li><i>Andelsbolig</i> : co-propriété</li>
<li><i>Hus / Villa</i> : maison / villa</li>
<li><i>Storkobenhavn</i> : grand Copenhague</li>
<li><i>Værelse</i> : chambre, pièce</li>
<li><i>Badested ou badeværelse </i> : salle de bains</li>
<li><i>Køkken</i> : cuisine</li>
<li><i>Kælder</i> : cave</li>
<li><i>Loft</i> : grenier</li>
<li><i>Husdyr</i> : animal domestique</li></ul>
<p>Vous pouvez consulter les sites suivants :</p>
<ul class="spip">
<li><a href="http://lejebolig.dk/" class="spip_out" rel="external">Lejebolig</a> (site en anglais)</li>
<li><a href="http://www.boligbasen.dk/" class="spip_out" rel="external">Boligbasen</a> (site en anglais)</li>
<li><a href="http://www.boligportal.dk/" class="spip_out" rel="external">Bolig portal</a> (site en danois)</li>
<li><a href="http://www.keybo.dk/" class="spip_out" rel="external">Keybo</a> (site en danois)</li>
<li><a href="http://www.boliga.dk/" class="spip_out" rel="external">Boliga</a> (site en danois)</li></ul>
<p>Les <strong>journaux</strong> et les éditions du vendredi, samedi et du dimanche des <strong>quotidiens nationaux</strong> (<i>Berlingske Tidende</i>, <i>Jyllands-Posten</i>, <i>Politiken</i>) offrent un supplément consacré au logement dans lequel vous trouverez des annonces immobilières émanant généralement d’agences. Les journaux locaux et de quartier, ainsi que les journaux gratuits publient également des annonces immobilières.</p>
<p><strong>Vous trouverez de nombreuses annonces sur le site internet suivant : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.dba.dk/" class="spip_out" rel="external">Den Blå Avis</a> (Le journal bleu) (site en danois) </p>
<p>Vous pouvez également vous adresser à une <strong>agence de location</strong>. Le locataire n’a généralement pas de frais d’agence à régler. Il existe peu d’agences et les délais d’attente peuvent être très longs. Vous trouverez ci-dessous une liste, non exhaustive, de sites Internet à consulter :</p>
<ul class="spip">
<li><a href="http://www.danskboligformidling.dk/" class="spip_out" rel="external">Dansk Boligformidling</a> (site en danois)</li>
<li><a href="http://www.cphbolig.dk/" class="spip_out" rel="external">CPH Home</a> (site en anglais). Agence spécialisée sur Copenhague et sa banlieue.</li></ul>
<h4 class="spip">Prix</h4>
<p>Les prix des loyers sont très élevés et parfois surévalués par rapport à la qualité du bien proposé. Ils dépendent du type (appartement ou maison) et de la taille du logement, de son état, du quartier et de la durée du bail. A noter qu’un logement meublé se loue 10 à 20 % plus cher qu’un logement vide.</p>
<p>Ainsi, pour un appartement de 50 m<sup>2</sup>, le montant du loyer varie entre 6000 DKK (810 Euros), charges non comprises, et 25 000 DKK (3375 Euros) pour le très haut de gamme et, pour une maison, entre 12 000 DKK (1620 Euros), charges non comprises, et 60 000 DKK (8100 Euros). Pour une chambre il faut compter 3500 couronnes (475 Euros) au minimum.</p>
<p>Les charges ne sont pas comprises dans le loyer. Elles comprennent toujours l’eau et le chauffage, parfois l’abonnement au câble et, pour les appartements, la cotisation au syndic de copropriété.</p>
<p>Certains employeurs logent leur personnel expatrié. D’autres prennent en charge en partie ou en totalité le loyer de leurs employés. Dans ce cas, l’employeur peut soit verser une allocation pour le logement, allocation qui sera imposable, soit fournir gratuitement un logement. L’imposition se fera alors sur la base de la valeur du logement qui aura été déterminée par les autorités fiscales danoises. Cette valeur est généralement inférieure au loyer réel.</p>
<p>Les quartiers résidentiels de Copenhague sont situés à Frederiksberg, Hellerup, Gentofte et Charlottenlund.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.visitdenmark.com/frankrig/fr-fr/menu/turist/turistforside.htm" class="spip_out" rel="external">Office de tourisme</a> ;</li>
<li><a href="http://www.ambafrance-dk.org/" class="spip_out" rel="external">Ambassade de France au Danemark</a>.</li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Conditions de location</h3>
<h4 class="spip">Le contrat de location </h4>
<p>L’établissement d’un contrat de location (udlejningskontrakt) est obligatoire que vous louez un appartement, une maison ou simplement une chambre. Vous pouvez vous procurer un contrat type dans les librairies ou sur le site <a href="http://www.boligportal.dk/" class="spip_out" rel="external">Boligportal.dk</a>.</p>
<p>Il faut apporter une attention particulière aux clauses ajoutées ou modifiées. Celles-ci peuvent en effet être contraires aux textes en vigueur et vous être défavorables.</p>
<p>En ce qui concerne la location d’une chambre, toutes les conditions relatives à l’usage de la chambre doivent être mentionnées dans le contrat. Il doit être clairement précisé si vous devez ou non payer en plus pour le chauffage et l’électricité, si vous pouvez utiliser la cuisine et la salle de bains, si vous avez le droit d’avoir un animal domestique et si vous pouvez utiliser les équipements de la buanderie.</p>
<h4 class="spip">La sous-location</h4>
<p>Un locataire a le droit de sous-louer une propriété en partie ou en totalité pendant une période pouvant aller jusqu’à deux ans si son absence est temporaire et justifiée. Vous devez établir un contrat de location avec le nouveau locataire et en transmettre une copie au propriétaire du logement avant le début de la sous-location. Le contrat de location doit clairement mentionner les dates de début et de fin de la sous-location.</p>
<h4 class="spip">La caution</h4>
<p>Le montant de la caution ne peut pas excéder trois mois de loyer. Le propriétaire peut également exiger le paiement de trois mois de loyer d’avance. Cette avance pourra servir à payer le loyer dans les mois précédant immédiatement la libération du logement. La caution et les loyers payés en avance ne peuvent excéder en tout six mois de loyer.</p>
<p>Il est conseillé de demander un reçu justifiant du versement de la caution et de l’avance de loyer, ainsi que pour le paiement du loyer. Si le propriétaire refuse d’en délivrer, il est possible de payer par virement ou par chèque afin de garder trace des paiements effectués.</p>
<h4 class="spip">L’état général du logement</h4>
<p>La loi oblige le propriétaire à fournir un logement en bon état c’est à dire qu’il doit être propre, les fenêtres ne doivent pas être endommagées et les portes extérieures équipées de verrous et de clés.</p>
<h4 class="spip">L’état des lieux</h4>
<p>Il est fortement conseillé d’établir par écrit un état des lieux <strong>au moment de l’emménagement</strong> afin que les parties vérifient ensemble l’état de la propriété. Vous n’êtes pas tenu de le signer si vous n’êtes pas d’accord avec son contenu ou s’il ne correspond pas aux clauses du contrat de location.</p>
<h4 class="spip">Les défauts</h4>
<p>Dans un délai de 14 jours suivant l’emménagement, le locataire doit informer le propriétaire par écrit de tout défaut, même ceux qu’il ne veut pas faire réparer. Si vous ne mentionnez pas ces défauts au propriétaire, vous risquez d’être tenu responsable et d’avoir à payer les réparations.</p>
<p>Si le contrat de location prévoit que vous avez la charge de l’entretien de certains équipements, il est préférable de vérifier au préalable que ces derniers fonctionnent correctement.</p>
<p>Il est recommandé de conserver des copies des courriers signalant les défauts au propriétaire et de prendre des photos comme preuve lors de votre emménagement.</p>
<h4 class="spip">Le paiement du loyer et des charges</h4>
<p>Le loyer doit être réglé chaque mois, en général le 1er jour du mois. Peuvent venir s’y ajouter les charges pour le chauffage, l’électricité, l’utilisation du câble ou d’une antenne.</p>
<p>Les charges liées au chauffage sont calculées soit par la lecture des compteurs fixés sur les radiateurs, soit par une évaluation en fonction de la superficie du logement. Le chauffage doit être réglé tous les mois et une régularisation est effectuée une fois par an.</p>
<p>L’électricité doit être directement réglée auprès du fournisseur. Le montant à payer est basé sur une évaluation de la consommation annuelle. Le compteur est relevé une fois par an. Si les sommes versées excèdent votre consommation, le trop-perçu vous sera remboursé.</p>
<p>Si le propriétaire souhaite augmenter le loyer, il doit respecter un préavis de 3 mois. Ce préavis doit spécifier le montant de l’augmentation, son motif et indiquer que le locataire dispose d’un délai de 6 semaines pour formuler ses objections. Si le préavis ne contient pas ces indications, il est considéré comme nul.</p>
<p>L’augmentation du loyer peut entraîner l’augmentation de la caution et des loyers versés d’avance.</p>
<h4 class="spip">L’assurance</h4>
<p>Il est recommandé de contracter une assurance habitation pour ses effets personnels. Celle-ci couvre tous les équipements de l’habitation s’ils sont endommagés ou détruits suite à un incendie, un cambriolage ou une inondation. La prime d’assurance dépend de la valeur des équipements et de vos effets personnels.</p>
<p>Voici une liste non-exhaustive de compagnies d´assurance : TopDanmark, Almbrand, Findforsikring, Codan,Tryg.</p>
<p>Il appartient au propriétaire de contracter une assurance sur l’immeuble lui-même.</p>
<h4 class="spip">L’entretien des lieux</h4>
<p>L’entretien de l’extérieur et de l’intérieur (peintures, boiseries, papier peint, sols) du logement est, sauf dispositions contraires du contrat de location, à la charge du propriétaire. Le locataire n’est en principe tenu que de veiller au bon état des serrures et des clés.</p>
<p>Si le propriétaire se charge de l’entretien de l’intérieur du logement, vous devrez payer annuellement 38 DKK par mètre carré pour les frais d’entretien.</p>
<p>Vous ne pouvez procéder à des aménagements dans le logement sans l’accord du propriétaire.</p>
<p>Vous devez informer par avance le propriétaire de l’installation d’équipements ménagers (téléphone, machine à laver, lave-vaisselle, réfrigérateur, antenne radio ou de télévision, etc.).</p>
<h4 class="spip">Le préavis</h4>
<p>Le préavis doit être fait par écrit, si possible par courrier recommandé avec accusé de réception, 3 mois avant la date de libération du logement, à moins que le contrat de location ne prévoit un délai différent. Le préavis peut aller de un à trois mois selon que le propriétaire ou le locataire souhaitent mettre fin au contrat, parfois même au-delà.</p>
<h4 class="spip">Le départ du logement</h4>
<p>Vous devez laisser le logement dans le même état que lorsque vous avez emménagé. Si un état des lieux est fait, vous n’êtes pas tenu de le signer si vous n’êtes pas d’accord avec son contenu. Les recommandations sont les mêmes que lors de l’emménagement. Pensez à demander un reçu lorsque vous rendez les clés.</p>
<p>Huit jours avant votre départ, vous devez communiquer votre nouvelle adresse au propriétaire.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.ambafrance-dk.org/" class="spip_out" rel="external">Ambassade de France au Danemark</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Hôtels</h3>
<h4 class="spip">Hôtels et meublés</h4>
<p>Prix moyen d’une chambre d’hôtel à Copenhague pour deux personnes petit déjeuner compris :</p>
<ul class="spip">
<li>1 étoile : de 595 à 850 DKK</li>
<li>2 étoiles : de 500 à 1495 DKK</li>
<li>3 étoiles : de 495 à 2995 DKK</li>
<li>4 étoiles : de 580 à 2790 DKK</li>
<li>5 étoiles : de 1000 à 4630 DKK</li></ul>
<p>Le prix d’un appartement meublé pour deux personnes varie de 900 à 2 700 DKK.</p>
<h4 class="spip">Auberges de jeunesse</h4>
<p>Le prix d’une nuitée dans une auberge de jeunesse est fonction de la période de l’année, de l’équipement de la chambre (avec ou sans salle de bains) et du nombre d’occupants. La fourchette de prix dans les auberges de jeunesse de Copenhague est comprise entre 300 à 580 pour une chambre pour deux personnes et entre 80 et 300 DKK pour un lit en dortoir.</p>
<p>Vous trouverez des informations sur les sites internet suivants :</p>
<ul class="spip">
<li><a href="http://www.hihostels.com/" class="spip_out" rel="external">Hostelling International (fédération internationale des auberges de jeunesse)</a></li>
<li><a href="http://www.danhostel.dk/" class="spip_out" rel="external">Danhostel</a> (site en anglais)</li></ul>
<h4 class="spip">Bed  breakfast</h4>
<p>Le prix d’une nuitée dans un <i>bed &amp; breakfast</i> varie entre 400 et 600 DKK pour deux personnes.</p>
<p>Vous trouverez des informations sur le site internet suivant :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.bedandbreakfast.dk/" class="spip_out" rel="external">Bed and breakfast</a> (site en français). </p>
<p>Pour en savoir plus, vous pouvez consulter les sites internet suivants :</p>
<ul class="spip">
<li><a href="http://www.visitdenmark.com/" class="spip_out" rel="external">Office du tourisme du Danemark</a></li>
<li><a href="http://www.visitcopenhagen.com/" class="spip_out" rel="external">Office du tourisme de Copenhague</a></li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Electricité</h3>
<p>Le voltage et les prises de courant des équipements électriques sont identiques aux normes françaises.</p>
<h3 class="spip"><a id="sommaire_4"></a>Electroménager</h3>
<p>Les disponibilités du marché danois en terme d’électroménager sont excellentes.</p>
<p>Les disponibilités du marché danois en terme d’équipements audiovisuels sont excellentes.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-danemark-vie-pratique-article-loisirs-et-culture.md" title="Loisirs et culture">Loisirs et culture</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-danemark-vie-pratique-article-communications.md" title="Communications">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-danemark-vie-pratique-article-transports.md" title="Transports">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-danemark-vie-pratique-article-cout-de-la-vie.md" title="Coût de la vie">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-danemark-vie-pratique-article-scolarisation.md" title="Scolarisation">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-danemark-vie-pratique-article-sante-111104.md" title="Santé">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-danemark-vie-pratique-article-logement.md" title="Logement">Logement</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/vie-pratique/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
