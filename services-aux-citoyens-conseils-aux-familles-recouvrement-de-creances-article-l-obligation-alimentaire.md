# L’obligation alimentaire

<p>L’obligation alimentaire trouve son origine dans le devoir de secours et d’assistance au sein des familles.</p>
<p>C’est une aide matérielle qui est due à un membre de sa famille proche (ascendant, descendant) dans le besoin et qui n’est pas en mesure d’assurer sa subsistance.</p>
<p>Selon les dispositions du code civil, la notion d’aliments se définit de la manière suivante : « <i>Choses nécessaires à la vie, qu’en vertu d’un devoir de solidarité familiale, celui qui le peut doit fournir à son parent dans le besoin, en général sous la forme d’une pension, compte tenu des besoins et des ressources du créancier et du débiteur</i> ».</p>
<p>Lorsque cette obligation légale n’est pas remplie, elle peut être fixée judiciairement et se traduire par le paiement d’une pension alimentaire pour un enfant afin de participer à son entretien et son éducation, pour un ex conjoint, au titre du secours mutuel, pour un ascendant.</p>
<p>La règlementation européenne définit la notion d’aliments comme toutes les obligations alimentaires découlant des relations de famille, de parenté, de mariage ou d’alliance qui existent dans les États membres.</p>
<p>L’attribution d’une pension alimentaire, fixée par le juge dans son principe et dans son montant, répond à un certain nombre de critères qui résultent de la situation matérielle des parties, de leurs besoins tout autant que de leur facultés contributives.</p>
<p>La pension alimentaire est le plus souvent indexée de façon à suivre la variation du coût de la vie.</p>
<p>Le montant de la pension alimentaire peut à tout moment faire l’objet d’une demande de révision auprès du juge, à la hausse ou à la baisse en fonction de l’évolution de la situation de chaque partie.</p>
<p><i>Mise à jour : juin 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/conseils-aux-familles/recouvrement-de-creances/article/l-obligation-alimentaire). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
