# Vie pratique

<h2 class="rub23002">Logement</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/vie-pratique/#sommaire_1">Conditions de location</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/vie-pratique/#sommaire_2">Hôtels</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/vie-pratique/#sommaire_3">Electricité</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/vie-pratique/#sommaire_4">Electroménager</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Conditions de location</h3>
<p>Des agences immobilières proposent des locations. Elles sont rémunérées par le seul loueur. On peut également recourir aux petites annonces. Il est recommandé de procéder à un état des lieux. La durée du bail est variable selon le choix du locataire.</p>
<p>Il est parfois plus aisé de trouver un logement meublé que vide. Il faut distinguer les appartements rénovés, au standard occidental et vides pour la plupart, des appartements anciens, souvent meublés avec un goût parfois discutable et présentant fréquemment des problèmes de chauffage, de distribution d’eau et d’isolation. A surface égale, les prix peuvent varier du simple au double pour ces deux types de logements. L’offre est importante.</p>
<p>Les quartiers résidentiels se situent dans la vieille ville et ses abords, autour de l’avenue Gedimino, artère centrale, sur la colline d’Antakalnis au nord-est de la ville, ainsi que dans le quartier Zverynas (ouest, très proche du centre).</p>
<p><strong>Prix d’une location (<i>fourchette de prix en Litas</i>)</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td></td>
<td>Prix du m2 en LT</td>
<td>Studio</td>
<td>3 Pièces</td>
<td>5 Pièces</td>
<td>Villa</td></tr>
<tr class="row_even even">
<td>Quartier résidentiel</td>
<td>19 - 61</td>
<td>700 - 3000</td>
<td>2500 - 8500</td>
<td>4500 - 10 000</td>
<td>7500 – 50 000</td></tr>
<tr class="row_odd odd">
<td>Banlieue</td>
<td>16 - 34</td>
<td>700 - 1700</td>
<td>1100 - 2100</td>
<td>2500 - 4000</td>
<td>4000 – 10 000</td></tr>
</tbody>
</table>
<p>Pour en savoir plus : <a href="http://www.infotourlituanie.fr/index.php?id=710" class="spip_out" rel="external">site de l’office de tourisme de Lituanie</a></p>
<h3 class="spip"><a id="sommaire_2"></a>Hôtels</h3>
<p>Il existe à Vilnius et dans toute la Lituanie une multitude d’hôtels à tous les prix et de toutes catégories.</p>
<h3 class="spip"><a id="sommaire_3"></a>Electricité</h3>
<p>Le courant est alternatif (220 v) et les prises de courant sont au standard allemand "Shuko", avec prise de terre. Il faut prévoir, le cas échéant, les adaptateurs nécessaires pour les équipements pourvus d’une prise française (avec connecteur de terre apparent).</p>
<h3 class="spip"><a id="sommaire_4"></a>Electroménager</h3>
<p>Les cuisines sont généralement équipées et l’on trouve sur place tous les appareils électroménagers.</p>
<p>La ville de Vilnius dispose d’un système de chauffage urbain opéré par une compagnie française, Dalkia, filiale de Veolia. Il peut être activé tardivement en début de saison et être désactivé prématurément au printemps, il n’est pas inutile en conséquence, de prévoir un chauffage d’appoint. Il faut privilégier les logements qui comportent un chauffage individuel au gaz.</p>
<p>En été, notamment dans les appartements en soupente, la climatisation est nécessaire.</p>
<p>En raison des risques de cambriolage, il est recommandé de blinder la porte d’entrée, voire de s’équiper d’un système d’alarme.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-lituanie-vie-pratique-article-loisirs-et-culture.md" title="Loisirs et culture">Loisirs et culture</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-lituanie-vie-pratique-article-communications.md" title="Communications">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-lituanie-vie-pratique-article-transports-111157.md" title="Transports">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-lituanie-vie-pratique-article-cout-de-la-vie-111156.md" title="Coût de la vie">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-lituanie-vie-pratique-article-scolarisation.md" title="Scolarisation">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-lituanie-vie-pratique-article-sante.md" title="Santé">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-lituanie-vie-pratique-article-logement-111153.md" title="Logement">Logement</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/vie-pratique/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
