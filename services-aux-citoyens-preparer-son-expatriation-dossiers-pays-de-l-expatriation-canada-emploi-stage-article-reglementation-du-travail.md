# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/emploi-stage/article/reglementation-du-travail#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/emploi-stage/article/reglementation-du-travail#sommaire_2">Contrat de travail – spécificités</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/emploi-stage/article/reglementation-du-travail#sommaire_3">Fêtes légales – Jours fériés</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/emploi-stage/article/reglementation-du-travail#sommaire_4">Emploi du conjoint</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/emploi-stage/article/reglementation-du-travail#sommaire_5">Création d’entreprises – spécificités</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<p>Le droit du travail canadien définit les droits et obligations des travailleurs, des employeurs et des membres des syndicats et couvre les aspects suivants :</p>
<ul class="spip">
<li>les relations de travail telles que l’activité syndicale, les relations patronales et syndicales, la négociation collective et les pratiques de travail illégales ;</li>
<li>la santé et la sécurité au travail ;</li>
<li>les conditions de travail telles que les jours fériés, les congés annuels, les horaires de travail, les licenciements abusifs et injustifiés, le salaire minimum, les mises à pied et l’indemnité de départ.</li></ul>
<p>Certains secteurs d’activité relèvent de la réglementation fédérale : c’est le cas du secteur bancaire, des télécommunications ou du transport routier. Dans ce cas, le Code du travail fédéral canadien s’applique. Les entreprises à compétence provinciale sont régies par les lois propres à chaque province.</p>
<p><strong>Au Québec</strong>, les principales sources pour le droit du travail sont le Code civil, la Loi sur les normes du travail, la Loi sur la santé et la sécurité du travail. L’employeur dispose d’un mois pour verser le premier salaire d’un employé. Ensuite, le salaire sera versé au plus tard tous les 16 jours, à l’exception des cadres qui peuvent être payés une fois par mois.</p>
<p>Lorsque le jour de paie tombe un jour férié, le salaire doit être versé le jour précédent. Chaque paie doit être accompagnée d’un bulletin permettant au travailleur de calculer son salaire. C’est ainsi que sur un bulletin de paie, la mention des informations suivantes est obligatoire : les nom et prénom de l’employé, le nom de l’employeur, la qualification de l’emploi, la date du paiement et la période correspondant à la paie, le nombre d’heures payées avec, le cas échéant, les heures supplémentaires et les primes versées, le montant du salaire brut, la nature et le montant des retenues opérées, le salaire net.</p>
<p><strong>Durée du travail</strong></p>
<p>Elle est de huit heures par jour et de 48 heures par semaine au maximum. Les heures supplémentaires sont payées au moins 1,5 fois le taux normal du salaire et s’appliquent à tous les types d’emplois : temps partiel ou temps plein, étudiants ou travailleurs occasionnels ("casual").</p>
<p>La Loi oblige les employeurs à accorder 24 heures consécutives de repos par semaine de travail et 48 heures consécutives de repos par période de deux semaines de travail consécutives. Un employé ne peut pas travailler plus de cinq heures d’affilée sans prendre une pause repas de trente minutes.</p>
<p><strong>Au Québec</strong>, la semaine légale de travail est, depuis le 1er octobre 2000, de 40 heures. Au delà de 40 heures, il s’agit d’heures supplémentaires. Ces dernières doivent être rémunérées à temps et demi, c’est à dire 50 % de plus que l’heure normale de travail. Il existe des exceptions (industrie du vêtement, entreprises forestières ou de gardiennage).</p>
<p><strong>Congés</strong></p>
<p>Leur durée est de deux semaines par an. Une semaine supplémentaire peut être accordée en fonction de l’ancienneté et de l’employeur. Conformément à la Loi sur les normes d’emploi, le congé maternité autorise jusqu’à 17 semaines de congé non rémunéré. Dans certains cas, le congé peut être plus long. Toutefois, l’employeur n’est pas tenu de rémunérer une employée en congé maternité. Les personnes qui prennent un congé maternité ont aussi le droit de prendre jusqu’à 35 semaines de congé parental. Tout autre parent aura le droit à un congé parental de 37 semaines. En ce qui concerne le congé maladie, appelé aussi congé d’urgence personnelle (maladie, deuil, accident), la Loi autorise une absence annuelle de dix jours et le versement du salaire n’est pas obligatoire.</p>
<p><strong>Au Québec,</strong> le nombre de jours de congé est fonction de l’ancienneté dans l’emploi. Les congés sont calculés sur une période de douze mois consécutifs, la période de référence pour le calcul s’étend du 1er mai de chaque année au 30 avril de l’année suivante.</p>
<ul class="spip">
<li>ancienneté inférieure à un an : un jour de congé par mois de travail, indemnité égale à 4 % du salaire annuel brut.</li>
<li>ancienneté comprise entre un et cinq ans : deux semaines de congés, indemnité égale à 4 % du salaire annuel brut.</li>
<li>ancienneté supérieure à cinq ans : trois semaines de congés, indemnité égale à 6 % du salaire annuel brut.</li></ul>
<p>Les salariés peuvent aussi demander une semaine de congé sans solde. Elle est souvent accordée entre Noël et le Jour de l’an.</p>
<p><strong>Licenciement et mise à pied</strong></p>
<p>Le licenciement peut intervenir très facilement et rapidement. Un employé peut être licencié peu de temps après avoir été embauché. La mise à pied est l’interruption temporaire d’un contrat de travail. Celle-ci intervient généralement pendant les périodes où une entreprise a un besoin moins important de main d’œuvre. La mise à pied ne doit pas être confondue avec le congédiement ou le licenciement, qui sont une interruption définitive du contrat de travail.</p>
<p>Un salarié licencié ou mis à pied pourra prétendre aux indemnités de licenciement prévues par la Loi sur les normes d’emploi provinciales et territoriales. Le préavis de licenciement est fonction de l’ancienneté et peut varier de zéro à huit semaines. L’indemnité de cessation d’emploi est calculée à raison d’une semaine de salaire multipliée par le nombre d’années complètes de service (au prorata pour les années partielles), avec un maximum de 26 semaines de salaire normal. Toutefois, un employé qui bénéficie d’une ancienneté inférieure à trois mois chez le même employeur ne peut prétendre à l’indemnité de cessation d’emploi prévue par la Loi et versée par l’employeur. L’indemnité de cessation d’emploi est différente de l’assurance emploi qui correspond à l’assurance chômage en France et est gérée au Canada par le ministère canadien du Travail (<a href="http://www.rhdsc.gc.ca/" class="spip_out" rel="external">Ressources humaines et Développement social Canada</a>).</p>
<p><strong>Embauche</strong></p>
<p>La période d’essai est généralement d’un mois.</p>
<p><strong>Syndicats</strong></p>
<p>Les syndicats sont très développés et impliqués dans la culture d’entreprise. A l’image des Codes du Travail fédéraux, provinciaux et territoriaux, certains syndicats ont une compétence principalement provinciale et d’autres sont présents au niveau national.</p>
<p><strong>Autres particularités du Québec : </strong></p>
<p><strong>Accident du travail</strong></p>
<p>Si vous êtes victime d’un accident du travail ayant provoqué un dommage corporel ou psychologique, vous avez droit à une indemnité forfaitaire compensatrice, dont le montant est fonction de la gravité du préjudice subi. Votre médecin sera chargé d’évaluer le préjudice. La Commission de la santé et de la sécurité du travail (CSST) déterminera l’indemnité à laquelle vous avez droit.</p>
<p><strong>Le cadre légal</strong></p>
<p>La <strong>Loi sur les normes du travail</strong> s’applique en principe à l’ensemble des salariés du Québec. Dans la pratique, de nombreux travailleurs ne peuvent s’en prévaloir. C’est ainsi que les salariés d’une ferme, les gardes d’enfants, de personnes âgées, certains salariés du secteur de la construction, les cadres supérieurs ainsi que les personnes salariées d’entreprises régies par le Code canadien du travail (Banques à charte, sociétés de transport internationales…) n’y sont pas soumis.</p>
<p><strong>Le taux du salaire</strong></p>
<p>Tout salarié assujetti à la <strong>Loi sur les normes du travail</strong> a droit au moins à un salaire minimum. Son montant est fixé périodiquement (habituellement le 1er octobre ou le 1er mai de chaque année) par le Gouvernement du Québec. La Commission des normes du travail est chargée de veiller au respect de ce salaire minimum. Au 1er mai 2010, le taux général était de 9,50 dollars de l’heure et de 8,25 dollars de l’heure pour les salariés qui reçoivent des pourboires. Les salariés de l’industrie du vêtement et du textile perçoivent pour leur part 9,50 dollars de l’heure.</p>
<h3 class="spip"><a id="sommaire_2"></a>Contrat de travail – spécificités</h3>
<p>Au Canada, les contrats de travail de type CDI (contrat à durée indéterminée) et CDD (contrat à durée déterminée) n’existent pas.</p>
<p>Les salariés dépendent généralement de contrats de travail collectifs propres à chaque entreprise et régis par les syndicats. Le contrat de travail doit respecter la législation provinciale ou territoriale et indiquer clairement les modalités et les conditions d’emploi.</p>
<p>Lors de missions professionnelles de courte durée ou temporaires de type <i>casual</i>, aucun contrat de travail n’est établi dès lors que l’entente mutuelle est verbale. Il est préférable de recourir à un contrat écrit sur lequel seront précisés les éléments suivants : le nom de l’entreprise et de l’employé, le lieu de travail, les principales fonctions et responsabilités, les heures supplémentaires, l’examen médical, les cours de formation continue, les clauses de non-concurrence, le préavis de cessation d’emploi ou de démission, la nature du contrat (à durée déterminée ou indéterminée), la durée de la période d’essai, le temps de travail, la rémunération brute et les avantages (primes…), la durée des congés payés, les avantages sociaux (mutuelle, indemnité de déménagement, régime de retraite, etc.), la législation applicable et le tribunal compétent en cas de différent, la signature des parties.</p>
<p>Dans d’autres cas, à défaut de contrat de travail, une lettre d’accord (<i>letter of agreement</i>) est rédigée afin d’officialiser l’accord entre les deux parties dans le cadre, par exemple, d’un mandat professionnel. Semblable à un contrat de travail, bien que plus synthétique, la lettre d’accord précise les conditions générales d’emploi.</p>
<p>Au Canada, l’établissement d’un climat de confiance et de respect est essentiel pour permettre la négociation et le dialogue.</p>
<h3 class="spip"><a id="sommaire_3"></a>Fêtes légales – Jours fériés</h3>
<ul class="spip">
<li>1er janvier : Jour de l’An</li>
<li>2 janvier : Repos du Nouvel An</li>
<li>Vendredi Saint</li>
<li>Lundi de Pâques</li>
<li>Le lundi précédant le 25 mai : Fête de la Reine (commémoration de l’anniversaire de la Reine Victoria) ou Journée nationale des Patriotes au Québec (commémoration des Québécois morts lors de la rébellion contre la couronne britannique en 1837-1838)</li>
<li>24 juin (Saint Jean-Baptiste)</li>
<li>1er juillet : Fête du Canada (Lorsque le 1er juillet tombe un dimanche, le jour de fête légale est reporté au 2 juillet).</li>
<li>1er lundi de septembre : Fête du travail</li>
<li>2ème lundi d’octobre : jour de l’Action de Grâce (Thanksgiving)</li>
<li>11 novembre : Jour du souvenir</li>
<li>25 décembre : Noël</li>
<li>26 décembre : lendemain de Noël</li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Emploi du conjoint</h3>
<p>Le conjoint est soumis à la législation locale du travail.</p>
<p>Le conjoint - l’époux, l’épouse ou conjoint de fait depuis plus d’un an - qui accompagne le travailleur ou l’étudiant étranger et qui souhaite travailler au Canada a besoin d’un permis de travail et devra en faire lui-même la demande. Le permis de travail peut être délivré sans avoir reçu au préalable la promesse d’une embauche de <a href="http://www.rhdcc-hrsdc.gc.ca/fra/accueil.shtml" class="spip_out" rel="external">Ressources humaines et Développement des Compétences Canada (RHDCC)</a>. Cette disposition s’applique aux deux catégories suivantes de conjoint :</p>
<ul class="spip">
<li><strong>conjoint de travailleur spécialisé temporaire </strong> : la validité du permis de travail ne peut dépasser celle du permis de travail ou la durée de l’emploi du demandeur principal. Le conjoint peut obtenir un permis de travail « ouvert », c’est-à-dire sans employeur spécifique. Pour l’exercice de certaines professions, un examen médical peut être exigé avant la délivrance du permis.</li>
<li><strong>conjoint d’étudiant étranger </strong> : le demandeur doit fournir la preuve qu’il est le conjoint du titulaire d’un permis d’études et que ce dernier fréquente à plein temps un établissement d’enseignement supérieur (université, collège communautaire, école technique ou de commerce financée par le secteur public ou un établissement privé autorisé par une loi provinciale à délivrer des diplômes). Le permis de travail sera établi pour une durée égale à celle du permis d’étude du demandeur principal.</li></ul>
<p>Les demandes de permis de travail des conjoints peuvent être déposées directement au Canada auprès du Télécentre de <a href="http://www.cic.gc.ca/" class="spip_out" rel="external">Citoyenneté et Immigration Canada (CIC)</a> en téléphonant au 1 888 242 2100 ou en consultant son site Internet. En France, vous devrez vous adresser à l’Ambassade du Canada à Paris.</p>
<p><strong>Au Québec :</strong> lorsqu’un couple s’installe au Québec, il est souvent indispensable que les deux conjoints aient un emploi. Les formalités à remplir sont les mêmes pour les deux époux. Au Québec, les concubins sont reconnus conjoints de fait au bout d’une période d’un an. Auparavant, le conjoint doit obtenir un certificat de concubinage auprès du <a href="http://www.mfe.org/default.aspx?SID=12291amp;DYN_VIEW=DETAILamp;PAYS=CANADA" class="spip_out" rel="external">consulat de France</a>le plus proche. Si votre emploi est peu spécialisé, votre concubin devra d’abord déposer une demande de certificat d’acceptation du Québec auprès de la <a href="http://www.gouv.qc.ca/portail/quebec/international/france" class="spip_out" rel="external">Délégation générale du Québec à Paris</a>, puis obtenir un permis de travail à l’<a href="http://www.canadainternational.gc.ca/france/index.aspx?lang=fra" class="spip_out" rel="external">Ambassade du Canada à Paris</a>.</p>
<p>Comme le soulignent les procédures d’immigration québécoises : « Si votre conjoint a l’intention de travailler au Québec et que vous venez occuper un emploi spécialisé pour une durée de six mois ou plus, il peut déposer directement une demande de permis de travail auprès du bureau canadien des visas (à Paris) ». Le certificat d’acceptation du Québec n’est alors pas exigé.</p>
<p>Enfin, si vous ou votre conjoint rencontrez des difficultés pour trouver du travail, sachez qu’il existe des programmes d’aide à l’intégration salariale. Emploi Québec propose ainsi le Programme d’aide à l’intégration des immigrants et des minorités visibles en emploi (<a href="http://www.emploiquebec.gouv.qc.ca/" class="spip_out" rel="external">PRIIME</a>). Ce programme accorde un soutien financier pendant une période de 30 semaines maximum. Il s’adresse uniquement aux résidents permanents.</p>
<h3 class="spip"><a id="sommaire_5"></a>Création d’entreprises – spécificités</h3>
<p>Si créer une entreprise au Canada est moins coûteux, plus simple et plus rapide qu’en France, ce n’est pas pour autant une garantie de réussite. Il importe donc de s’informer sur l’environnement d’affaires, le marché et de contacter des organismes susceptibles de vous aider.</p>
<p>Tout projet de création d’entreprise au Canada doit s’articuler autour de plusieurs volets essentiels :</p>
<ul class="spip">
<li>Elaboration d’un projet solide en accumulant le plus d’informations possibles sur le pays et le secteur choisi ;</li>
<li>Connaissance du marché provincial et national ;</li>
<li>Repérage, puis découverte du point de chute et y séjour en observateur ;</li>
<li>Se familiariser avec la culture, les us et coutumes ;</li>
<li>Identification des programmes, aides logistiques, subventions disponibles au Canada pour les investisseurs étrangers ;</li>
<li>Développement d’un réseau ("réseautage") ;</li>
<li>Connaissance linguistique indispensable (anglais minimum).</li></ul>
<p>Dans le but de favoriser le développement d’une économie canadienne forte et prospère, le Gouvernement du Canada encourage la venue d’hommes d’affaires dans le cadre du " programme fédéral d’immigration des gens d’affaires ". Ce dernier s’adresse aux profils suivants :</p>
<ul class="spip">
<li>Investisseurs</li>
<li>Entrepreneurs</li>
<li>Travailleurs indépendants.</li></ul>
<p><strong>Culture d’entreprise</strong></p>
<p>Il est essentiel de comprendre que le Canada fait partie de l’Amérique du Nord et que le mode de pensée et de vie de ses habitants est plus proche de celui des Etats-Unis que de celui de l’Europe.</p>
<p>Outre le fait que l’accès à l’emploi diffère d’une province ou d’un territoire à l’autre et que certains secteurs sont réservés uniquement aux Canadiens, il est important de se familiariser avec la culture d’entreprise canadienne.</p>
<p>L’établissement d’un réseau (<i>réseautage</i>) est un outil indispensable. Il faut se faire connaître à l’occasion de rencontres informelles et au moyen, par exemple, d’une simple carte de visite. Le principe de solidarité est aussi un des moteurs de l’intégration. Immigrer au Canada peut se traduire par un sacrifice du statut professionnel que l’on avait en France, sauf pour certains profils recherchés. En contrepartie, l’ascension professionnelle peut être très rapide et est reconnue comme une valeur de l’individu. La réussite professionnelle s’affiche naturellement.</p>
<p>Les relations professionnelles sont moins hiérarchisées qu’en France et au sein de cette hiérarchie la communication demeure plus fluide. Le Code vestimentaire est en général plus <i>casual</i>, et le tutoiement est souvent de rigueur. Le Canada se caractérise comme un pays d’opportunités qu’il vous est possible de saisir à condition d’être prêt à faire face à une compétition “ rude ” dans certains secteurs porteurs. C’est pourquoi la notion d’expérience professionnelle est fondamentale. Le bénévolat et la participation civile sont très valorisées et considérées comme un passage nécessaire dans tout parcours professionnel. Le profil idéal serait celui d’un employé qui s’investit sans compter dans l’entreprise.</p>
<p>Les Canadiens ne sont pas réputés pour être conflictuels (attitude <i>layback</i>) et savent être discrets mais aussi directs. Ils favorisent le dialogue et l’échange de points de vue mais ne sont pas partisans des rapports autoritaires et agressifs. Aussi, ils ont le souci du respect de la sphère privée et des diverses communautés qui composent le pays et qui participent à sa vitalité économique.</p>
<p><strong>Adresses utiles : </strong></p>
<ul class="spip">
<li>Site <a href="http://www.investiraucanada.ca/" class="spip_out" rel="external">Investir au Canada</a> du gouvernement canadien ;</li>
<li><a href="http://www.cic.gc.ca/" class="spip_out" rel="external">Citoyenneté et Immigration Canada</a></li>
<li>Le <a href="http://www.rdee.ca/" class="spip_out" rel="external">Réseau de développement économique et d’employabilité (RDEE)</a>. RDEE Canada aide, en collaboration avec ses partenaires provinciaux et territoriaux, le développement économique et la création d’emplois afin d’assurer la vitalité et la pérennité des communautés francophones et acadiennes du Canada.</li>
<li><a href="http://www.wd.gc.ca/" class="spip_out" rel="external">Diversification de l’économie de l’Ouest Canada</a></li>
<li><a href="http://www.bdc.ca/" class="spip_out" rel="external">Banque de développement du Canada</a></li>
<li><a href="http://www.entreprisescanada.ca/" class="spip_out" rel="external">Entreprises Canada</a> - services aux entrepreneurs ;</li>
<li><a href="http://www.tresor.economie.gouv.fr/pays/canada" class="spip_out" rel="external">Service économique de l’Ambassade de France au Canada</a></li>
<li><a href="http://www.ccfc-france-canada.com/" class="spip_out" rel="external">Chambre de commerce France-Canada</a>.</li></ul>
<p><i>Mise à jour : avril 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/emploi-stage/article/reglementation-du-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
