# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/vie-pratique/article/transports-111398#sommaire_1">Importation de véhicule</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/vie-pratique/article/transports-111398#sommaire_2">Permis de conduire</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/vie-pratique/article/transports-111398#sommaire_3">Code de la route</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/vie-pratique/article/transports-111398#sommaire_4">Assurances et taxes</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/vie-pratique/article/transports-111398#sommaire_5">Achat et location</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/vie-pratique/article/transports-111398#sommaire_6">Immatriculation</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/vie-pratique/article/transports-111398#sommaire_7">Entretien</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/vie-pratique/article/transports-111398#sommaire_8">Réseau routier</a></li>
<li><a id="so_9" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/vie-pratique/article/transports-111398#sommaire_9">Transports en commun</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Importation de véhicule</h3>
<p>Tout véhicule construit après 1996 dans un pays de l’UE répond aux normes européennes et seul un contrôle technique local est exigé en vue de son immatriculation.</p>
<p>Pour les personnes voulant importer leur véhicule personnel : la taxe diffère selon qu’il s’agit d’une voiture neuve ou d’occasion. S’il s’agit d’une voiture neuve (véhicule livré dans les six mois de sa première mise en circulation ou qui a parcouru moins de 6000 km, vous devez payer la taxe sur la valeur ajoutée autrichienne (20%). L’importation de voitures d’occasion en Autriche n’est pas soumise à la TVA. Reste à payer une taxe pour l’environnement, appelée NOVA dont le montant varie en fonction du type de carburant et de la puissance du moteur. La réforme de la Nova, qui est entrée en vigueur le 1er juillet 2008, a introduit pour tout achat d’un nouveau véhicule un système de bonus-malus, en fonction des émissions de CO2.</p>
<h3 class="spip"><a id="sommaire_2"></a>Permis de conduire</h3>
<p>Les permis de conduire français et internationaux sont reconnus. Le permis autrichien, disponible en un mois auprès des services de la police fédérale, n’est plus obligatoire pour les titulaires d’un permis délivré par un autre pays de l’UE.</p>
<h3 class="spip"><a id="sommaire_3"></a>Code de la route</h3>
<p>La liberté de circulation est totale en Autriche. La conduite s’effectue à droite, la priorité est aussi à droite.</p>
<p>L’affichage sur le pare-brise d’une <strong>vignette est obligatoire pour emprunter les autoroutes</strong>. Il est vivement recommandé de l’acquérir dès le passage de la frontière. Son coût s’élève à 82,70 euros pour l’année, à 24,80 euros pour deux mois ou encore à 8,50 euros pour dix jours. L’absence de vignette est sévèrement réprimée et l’amende peut coûter de 300 à 3 000 euros en plus du coût de la vignette (facturée alors 240 euros) et uniquement valable 24 heures dans ce cas. La voiture et une partie de son contenu peuvent être retenues à titre de garantie en cas de demande de paiement différé.</p>
<p><strong>La vignette moto est également obligatoire pour emprunter les autoroutes</strong>. Son coût s’élève à 32,90 euros pour l’année, à 12,40 euros pour deux mois ou à 4,90 euros pour dix jours (en 2014).</p>
<p>Les vitesses maximales autorisées, sauf indications contraires, sont : 130 km/h sur les autoroutes, 100 km/h hors agglomération, 50 km/h dans les agglomérations et souvent 30 km/h en zone résidentielle.</p>
<p>Les excès de vitesse sont très facilement verbalisés, à partir d’un dépassement de 5%. De nombreux radars automatiques de contrôle sont installés dans les agglomérations et sur les routes autrichiennes.</p>
<p>Des informations complémentaires peuvent être obtenues sur les sites Internet suivants :</p>
<ul class="spip">
<li><a href="http://www.oeamtc.at/" class="spip_out" rel="external">österreichische Automobil-, Motorrad- und Touringclub (automobile club)</a> ;</li>
<li><a href="http://www.arboe.at/" class="spip_out" rel="external">Auto-, Motor- und Radfahrerbund Österreichs (automobile club)</a> ;</li>
<li><a href="http://www.vignette.at/" class="spip_out" rel="external">société "ASFINAG" qui est en charge de l’exploitation des autoroutes</a>.</li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Assurances et taxes</h3>
<p>Seule la responsabilité civile est obligatoire. Elle peut bien entendu être complétée par une assurance "au tiers" ou "tout risques".</p>
<h3 class="spip"><a id="sommaire_5"></a>Achat et location</h3>
<p>Il existe de nombreux centres de vente de véhicules d’occasion. Les prix sont très variables. Les prix des véhicules neufs sont plus élevés en Autriche qu’en France (le surcoût moyen est d’environ 8% ; il s’explique d’une part en raison du taux supérieur de la TVA en Autriche (20%) et de la "Nova", taxe pour l’environnement).</p>
<h3 class="spip"><a id="sommaire_6"></a>Immatriculation</h3>
<p>La loi autrichienne prévoit que les premières démarches pour l’immatriculation de votre véhicule doivent être entreprises dans le premier mois de votre installation en Autriche.</p>
<p>Pour le contrôle technique à Vienne vous devez vous rendre avec le véhicule (après prise de rendez-vous) à la Magistratsabteilung 46, Landesfahrzeugprüfstelle (Schlechtastrasse 4 -1030 Wien), tél. : 79 775.</p>
<p>Après ce contrôle vous sera remise la "Einzelgenehmigung" (frais variable selon l’état du véhicule). Une taxe basée sur la valeur commerciale de votre véhicule et sa consommation (NOVA = Normverbrauchssteuer, téléchargement du formulaire) devra être réglée au centre des impôts (Finanzamt) de votre arrondissement.</p>
<p>Si vous avez acheté votre véhicule il y a moins de 6 mois ou que son kilométrage est inférieur à 6000 km, il n’est pas considéré comme faisant partie de votre mobilier, mais comme voiture neuve. Il faut alors régler la TVA autrichienne de 20%.</p>
<p>Votre assurance en Autriche s’occupera d’ailleurs - si vous le souhaitez - de l’immatriculation auprès de la police et vous fournira les plaques minéralogiques.</p>
<p>Pour tous renseignements adressez-vous à l’adresse suivante :</p>
<ul class="spip">
<li>à Vienne : au <i>Verkehrsamt</i> (Josef-Holaubek-Platz 1, 1090 Vienne, Tél. : 01/313 45-0)</li>
<li>en province, si vous résidez au chef-lieu du <i>Bundesland</i> : à la <i>Polizeidirektion</i>, service <i>Verkehrsamt</i> de la direction de police.</li>
<li>en province, si vous résidez dans une petite ville : à la <i>Bezirkshauptmannschaft</i> de la circonscription.</li></ul>
<h3 class="spip"><a id="sommaire_7"></a>Entretien</h3>
<p>L’entretien du véhicule sur place ne pose pas de problème particulier. La qualité du service rendu est bonne, à un coût supérieur toutefois à celui pratiqué en France.</p>
<h3 class="spip"><a id="sommaire_8"></a>Réseau routier</h3>
<p>Le réseau routier est bon. Toutefois, les chantiers de réfection et d’entretien sont nombreux en été.</p>
<p>Des informations régulièrement mises à jour sur l’état des routes sont accessibles sur les sites Internet suivants :</p>
<ul class="spip">
<li><a href="http://www.oeamtc.at/" class="spip_out" rel="external">österreichische Automobil-, Motorrad- und Touringclub (automobile club)</a> ;</li>
<li><a href="http://www.arboe.at/" class="spip_out" rel="external">Auto-, Motor- und Radfahrerbund Österreichs (automobile club)</a> ;</li>
<li><a href="http://www.vignette.at/" class="spip_out" rel="external">société "ASFINAG" qui est en charge de l’exploitation des autoroutes</a>.</li></ul>
<p>Un bulletin d’information routière est diffusé toutes les 30 minutes par la fréquence de radio Ö3. Lors de situations potentiellement dangereuses - comme par exemple la présence d’un conducteur roulant à contre-sens – la chaîne interrompt immédiatement les programmes de toutes les fréquences pour avertir les conducteurs du danger.</p>
<p>Pour obtenir des informations 24h/24h par téléphone, vous pouvez composer le 0800 600 601 (gratuit pour l’Autriche). Un autre service payant peut également vous renseigner par téléphone au 0900 600 600.</p>
<h3 class="spip"><a id="sommaire_9"></a>Transports en commun</h3>
<p>Les modes de transport sont semblables à ceux utilisés en France.</p>
<p>Les transports urbains bénéficient d’un très bon réseau, en particulier à <a href="http://www.wienerlinien.at/" class="spip_out" rel="external">Vienne</a>. Le billet de tram, de bus ou métro à l’unité coûte 2,20 €. Il existe différentes formules permettant de voyager moins cher : carte multi-trajets (7,20 € pour quatre trajets), cartes à la semaine, au mois, à l’année (<i>Jahreskarte</i> 375€), etc.</p>
<p>Des voitures de location peuvent être louées dans tous les aéroports, grandes gares et toutes les villes (Budget, Avis, Europcar, Hertz etc.). Il est également possible de réserver une voiture de location depuis son pays par l’intermédiaire de bureaux permettant la location à l’étranger et une prestation à des prix avantageux.</p>
<p><strong>Conditions</strong> : en théorie, le conducteur doit être titulaire depuis minimum un an d’un permis de conduire valable en Autriche. Pour certaines catégories de véhicules, l’âge minimum est de 21 ou 25 ans.</p>
<p><strong>Attention</strong> : la vignette est obligatoire sur les autoroutes et voies rapides. Les voitures louées en Suisse sont soumises à un régime spécial sur le territoire autrichien.</p>
<p>Pour en savoir plus : <a href="http://www.ambafrance-at.org/spip.php?rubrique616" class="spip_out" rel="external">site de l’ambassade de France à Vienne</a></p>
<p><i>Mise à jour : 13/01/2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/vie-pratique/article/transports-111398). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
