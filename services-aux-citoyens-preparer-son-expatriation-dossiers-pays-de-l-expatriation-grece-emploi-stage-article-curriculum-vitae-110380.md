# Curriculum vitae

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/emploi-stage/article/curriculum-vitae-110380#sommaire_1">Rédaction</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/emploi-stage/article/curriculum-vitae-110380#sommaire_2">Diplômes</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/emploi-stage/article/curriculum-vitae-110380#sommaire_3">Modèles de CV</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Rédaction</h3>
<p>Pour la Grèce, il est conseillé de suivre les règles de rédaction du curriculum vitae français ou du modèle Europass. Il en est de même pour la lettre de motivation.</p>
<p>Le CV ne doit jamais être manuscrit. Il doit être bien présenté, facile à comprendre et doit mettre en valeur les compétences professionnelles en rapport avec le poste recherché. Il peut être rédigé en français (si l’interlocuteur maîtrise la langue), en grec (si votre niveau de connaissance de la langue est suffisant) ou encore en anglais.</p>
<p>Le candidat peut joindre des lettres de références à son curriculum vitae. Il est préférable qu’elles se rapportent à sa dernière activité professionnelle.</p>
<p>Les entreprises multinationales exigent des lettres de recommandation (une ou deux en fonction de la durée des postes précédemment occupés par le candidat), éléments-clés pour la confirmation d’un curriculum vitae. Les entreprises grecques de petite taille n’exigent pas ce type de lettre.</p>
<h4 class="spip">Une bonne façon de présenter ses références</h4>
<p>Une lettre de recommandation tient en une page et se compose de trois paragraphes. Il existe plusieurs modèles de lettre de recommandation utilisés par les entreprises, les universités ou les institutions. La lettre doit impérativement être tapée sur du papier à en-tête de l’entreprise qui vous recommande.</p>
<h5 class="spip">Introduction</h5>
<p>La personne signataire (l’ancien employeur ou un représentant autorisé de l’entreprise) précise la position occupée dans l’entreprise, le titre et la durée de l’emploi.</p>
<h5 class="spip">Partie centrale</h5>
<p>Elle peut se composer de trois parties :</p>
<ul class="spip">
<li>les qualités professionnelles et personnelles de l’employé : compétences techniques, dynamisme, esprit d’initiative, etc. ;</li>
<li>la description du poste et les responsabilités assumées ;</li>
<li>la confirmation que le postulant sera un bon collaborateur pour son nouvel employeur.</li></ul>
<h5 class="spip">Conclusion</h5>
<p>L’ancien employeur donne son opinion sur la personne qu’elle recommande et précise ses coordonnées pour un éventuel contact avec le recruteur.</p>
<h4 class="spip">Comment et quand utiliser ses références</h4>
<p>La lettre de recommandation ne décrit pas seulement les compétences professionnelles de l’intéressé, mais aussi ses qualités de coopération, de communication et d’acceptation par le milieu de travail. Il pourra être très utile à un débutant de produire des lettres de références rédigées par des anciens professeurs ou responsables de stage.</p>
<p>Il ne faut pas joindre les photocopies des lettres de références avec le curriculum vitae. Il est préférable d’ajouter une rubrique intitulée " références " et d’y indiquer les noms, les titres et les coordonnées des personnes qui acceptent d’appuyer la candidature. Avant de donner cette liste, il faut préalablement obtenir l’accord des personnes concernées. Si le candidat ne souhaite pas mentionner ses références, il peut noter " Références : disponibles sur demande ".</p>
<h3 class="spip"><a id="sommaire_2"></a>Diplômes</h3>
<p><a href="http://www.doatap.gr/en/tertiary.php" class="spip_out" rel="external">Service de Reconnaissance des diplômes</a> - <strong>DOATAP</strong><br class="manualbr">54, rue Ag. Konstantinou - Athènes <br class="manualbr">Tél. : (+30) (210) 5281000 <br class="manualbr">Fax : (+30) (210) 5239525 ; <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/emploi-stage/article/curriculum-vitae-110380#information#mc#doatap.gr#" title="information..åt..doatap.gr" onclick="location.href=mc_lancerlien('information','doatap.gr'); return false;" class="spip_mail">Courriel</a></p>
<h3 class="spip"><a id="sommaire_3"></a>Modèles de CV</h3>
<p>Modèles de curriculum vitae en français et en grec à télécharger sur le site Internet du <a href="http://www.emploigrece.org/" class="spip_out" rel="external">Bureau emploi-formation</a>  de l’Ambassade de France en Grèce.</p>
<p><i>Mise à jour : novembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/emploi-stage/article/curriculum-vitae-110380). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
