# Logement

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/vie-pratique/article/logement-103431#sommaire_1">Electricité</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/vie-pratique/article/logement-103431#sommaire_2">Electroménager</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/vie-pratique/article/logement-103431#sommaire_3">Conditions de location</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/vie-pratique/article/logement-103431#sommaire_4">Hôtels</a></li></ul>
<p>A Bogota, les quartiers résidentiels se trouvent au nord de la ville, moins dangereux que la partie sud. Il est préférable de vivre en appartement plutôt qu’en maison, les appartements étant gardés 24/24h.</p>
<table class="spip" summary="">
<caption>Coût locatif moyen par type d’habitation et de quartier</caption>
<thead><tr class="row_first"><th id="idb8d4_c0">Location</th><th id="idb8d4_c1">Studio</th><th id="idb8d4_c2">3 pièces</th><th id="idb8d4_c3">5 pièces</th><th id="idb8d4_c4">Villa</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="idb8d4_c0">Centre et périphéries</td>
<td headers="idb8d4_c1">650 €</td>
<td headers="idb8d4_c2">900 €</td>
<td headers="idb8d4_c3">2000 €</td>
<td headers="idb8d4_c4">à éviter</td></tr>
<tr class="row_even even">
<td headers="idb8d4_c0">Quartiers résidentiels</td>
<td headers="idb8d4_c1">800 €</td>
<td headers="idb8d4_c2">1800€</td>
<td headers="idb8d4_c3">4000 €</td>
<td headers="idb8d4_c4">à éviter</td></tr>
<tr class="row_odd odd">
<td headers="idb8d4_c0">Banlieue</td>
<td headers="idb8d4_c1">à éviter</td>
<td headers="idb8d4_c2">à éviter</td>
<td headers="idb8d4_c3">à éviter</td>
<td headers="idb8d4_c4">à éviter</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_1"></a>Electricité</h3>
<p>Le courant électrique est livré sous une tension de 110 volts et 60 Hertz. Les prises de courant sont de type américain, avec deux pattes plates. Un adaptateur est nécessaire.</p>
<h3 class="spip"><a id="sommaire_2"></a>Electroménager</h3>
<p>La disponibilité locale en électroménager ne pose pas de problèmes particuliers. Il est déconseillé de déménager avec ses électroménagers qui ne fonctionneront pas avec du 110 volts.</p>
<p>L’équipement vidéo en Colombie est au standard américain, le système utilisé est le NTSC.</p>
<h3 class="spip"><a id="sommaire_3"></a>Conditions de location</h3>
<p>La durée des baux est d’un an renouvelable. L’augmentation annuelle des loyers est officiellement indexée sur le taux de l’inflation mais il est courant que les propriétaires demandent une augmentation nettement supérieure.</p>
<p>En général il n’y a pas d’avance sur le loyer à payer et les frais d’agence sont à régler par le propriétaire.</p>
<p>Les charges correspondent à l’administration de l’immeuble qui comprend le gardiennage et l’entretien. Les charges annexes sont l’électricité, l’eau et le gaz. Elles sont à régler par le locataire et s’élèvent environ à 200 euros pour un loyer de 1000 euros.</p>
<h3 class="spip"><a id="sommaire_4"></a>Hôtels</h3>
<p>Les établissements économiques, le plus souvent appelés « residencial » et « hospedaje », se trouvent à proximité des gares routières et du marché de Bogota. On peut négocier un tarif dégressif pour plusieurs nuits. Le petit-déjeuner est rarement inclus. Le confort est sommaire, mais acceptable avec des sanitaires à l’extérieur.</p>
<p>Les « hostales » sont d’un confort supérieur pour un tarif raisonnable. Les chambres sont souvent agréablement disposées autour d’un patio fleuri avec salle de bains privée. Ils sont bien situés dans le centre-ville, à proximité de la place principale.</p>
<p>La plupart des hôtels de bon standing de la capitale se trouvent dans les quartiers nord. Mieux vaut éviter les hôtels ou maisons d’hôtes bon marché qui n’offrent pas des conditions de sécurité satisfaisante</p>
<table class="spip" summary="">
<caption>Prix moyen d’une chambre d’hôtel (chambre double)</caption>
<tbody>
<tr class="row_odd odd">
<td>Bon marché</td>
<td>Entre 15 et 30 €</td></tr>
<tr class="row_even even">
<td>Moyen</td>
<td>Environ 40 €</td></tr>
<tr class="row_odd odd">
<td>Luxueux</td>
<td>A partir de 100€</td></tr>
</tbody>
</table>
<h4 class="spip">Appart-hôtels</h4>
<p>Grande spécialité sud américaine, les apparts’ hôtels sont des hôtels qui proposent des appartements (cuisine, salon, salle de bain, chambre double) tout en offrant un véritable service hôtelier. Les locations se font à la journée, à la semaine ou au mois. Compter environ 75 euros par nuit et 1500 euros pour un mois.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/vie-pratique/article/logement-103431). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
