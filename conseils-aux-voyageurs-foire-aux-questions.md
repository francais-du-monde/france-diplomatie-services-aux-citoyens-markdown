# Foire aux questions

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/foire-aux-questions/#sommaire_1">1. Quel est le rôle d’une fiche Conseils aux voyageurs (FCV) ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/foire-aux-questions/#sommaire_2">2. A qui s’adressent les FCV ?</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/foire-aux-questions/#sommaire_3">3. Quelles sont les sources des FCV ?</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/foire-aux-questions/#sommaire_4">4. A quelle fréquence les FCV sont-elles mises à jour ?</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/foire-aux-questions/#sommaire_5">5. Puis-je m’abonner aux mises à jour des FCV ?</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/foire-aux-questions/#sommaire_6">6. Que signifient les couleurs des cartes figurant sur les FCV ?</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/foire-aux-questions/#sommaire_7">7. Que contient la rubrique « infos pratiques » ?</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/foire-aux-questions/#sommaire_8">8. Puis-je obtenir une attestation du ministère des Affaires étrangères et du Développement international lorsque les risques ont changé et que mon agence de voyage ne veut pas me rembourser le coût de mon séjour annulé ?</a></li>
<li><a id="so_9" href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/foire-aux-questions/#sommaire_9">9. Que se passe-t-il si je me mets en danger en connaissance de cause et que l’Etat me vient en aide ?</a></li>
<li><a id="so_10" href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/foire-aux-questions/#sommaire_10">10. Où puis-je trouver plus d’informations sur un pays et sur les relations que ce dernier entretient avec la France ?</a></li>
<li><a id="so_11" href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/foire-aux-questions/#sommaire_11">11. Je souhaite m’expatrier, où puis-je trouver des informations utiles pour préparer mon départ ?</a></li>
<li><a id="so_12" href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/foire-aux-questions/#sommaire_12">12. Je souhaite m’entretenir avec quelqu’un sur mon pays de destination, que mon voyage soit touristique ou d’affaires.</a></li>
<li><a id="so_13" href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/foire-aux-questions/#sommaire_13">13. Puis-je voyager avec ma carte d’identité qui bénéficie d’une prolongation de 5 ans ?</a></li>
<li><a id="so_14" href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/foire-aux-questions/#sommaire_14">14. A l’étranger, si j’ai un problème, qui puis-je contacter ?</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>1. Quel est le rôle d’une fiche Conseils aux voyageurs (FCV) ?</h3>
<p>Les fiches pays délivrent des informations sur la situation du pays de votre destination dans le but de faciliter la préparation et le bon déroulement de votre séjour à l’étranger. Ces informations sont réparties en six catégories :</p>
<ul class="spip">
<li>Dernière minute : cette rubrique ne s’affiche qu’en cas d’évènements importants récents.</li>
<li>Sécurité : renseignements sur la situation d’un pays qui pourrait affecter la sécurité des personnes.</li>
<li>Entrée et séjour : informations administratives relatives aux conditions d’entrée, de séjour et de sortie dans le pays de destination (validité du passeport, nécessité d’un visa, etc.).</li>
<li>Santé : informations sur les vaccinations obligatoires ou recommandées et le risque sanitaire du pays, ainsi que des recommandations en matière sanitaire.</li>
<li>Infos utiles : informations sur les usages et coutumes du pays,  la sûreté et la sécurité des moyens de transport disponibles, les spécificités de la législation locale, le climat, etc.</li>
<li>Voyages d’affaires : informations pratiques destinées aux voyageurs d’affaires et aux professionnels, visant à faciliter leur séjour et leurs démarches dans le pays de destination.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>2. A qui s’adressent les FCV ?</h3>
<p>Les fiches pays sont destinées aux personnes effectuant un séjour touristique, un séjour d’affaires ou une mission dans le pays considéré. Pour les Français établis à l’étranger, ces informations peuvent être utilement complétées par la consultation du<br class="autobr">site Internet de l’ambassade de France du pays de leur résidence ainsi que sur le site internet France Diplomatie, rubrique « <a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation.md" class="spip_out">Services aux citoyens</a> » :</p>
<p>   - <a href="http://www.mfe.org/index.php/Annuaires/Ambassades-et-consulats-francais-a-l-etranger" class="spip_out" rel="external">Coordonnées des ambassades et consulats de France à l’étranger</a></p>
<h3 class="spip"><a id="sommaire_3"></a>3. Quelles sont les sources des FCV ?</h3>
<p>Nos ambassades et consulats dans le monde recueillent des informations et adressent des propositions au Centre de crise et de soutien du ministère des Affaires étrangères et du Développement international (MAEDI). Ces propositions sont analysées, synthétisées et complétées le cas échéant, en concertation avec d’autres Directions du MAEDI ainsi qu’avec les ministères de la Défense et de l’Intérieur, en tant que de besoin. Les Conseils sont ensuite finalisés et publiés sur le site.</p>
<h3 class="spip"><a id="sommaire_4"></a>4. A quelle fréquence les FCV sont-elles mises à jour ?</h3>
<p>Les Conseils aux voyageurs sont actualisés en temps réel, lorsque les conditions de sécurité d’un pays ou d’une région changent significativement.</p>
<p>Les informations présentes dans les fiches Conseils aux voyageurs n’ont donc pas pour but de relayer chaque nouvelle information comme pourrait le faire un journal, ni d’établir l’historique des événements se déroulant dans un pays.</p>
<p>Le Centre de crise et de soutien s’est engagé depuis 2011 dans une démarche de système de management de la qualité, visant à sécuriser et améliorer ses procédures d’élaboration et de mise à jour des fiches Conseils aux voyageurs. Nos informations sont donc certifiées ISO 9001.</p>
<h3 class="spip"><a id="sommaire_5"></a>5. Puis-je m’abonner aux mises à jour des FCV ?</h3>
<p>Les alertes concernant la situation particulière d’un pays font l’objet d’un message dans la rubrique « <a href="conseils-aux-voyageurs-dernieres-minutes.md" class="spip_out">Dernières minutes</a> ».</p>
<p>Des messages d’alertes inspirés de ces « Dernières minutes » peuvent également être adressés aux voyageurs inscrits sur le <a href="https://pastel.diplomatie.gouv.fr/fildariane/dyn/public/login.html;jsessionid=3F35FA3B6B551892CD2B23DA558831AC.jvm01945-1" class="spip_out" rel="external">portail Ariane</a>.</p>
<p>Pour permettre de suivre l’évolution des conditions de voyage dans un pays donné, le ministère des Affaires étrangères et du Développement international met à disposition <a href="http://www.diplomatie.gouv.fr/fr/spip.php?page=backend_fcv" class="spip_out">le fil RSS « Conseils aux voyageurs »</a>, un <a href="https://twitter.com/ConseilsVoyages" class="spip_out" rel="external">compte Twitter dédié</a> ainsi qu’une <a href="http://www.diplomatie.gouv.fr/fr/politique-etrangere-de-la-france/diplomatie-numerique/l-offre-de-service-public-en-ligne-du-ministere/" class="spip_out">application</a> « Conseils voyageurs » pour smartphone et tablette.</p>
<h3 class="spip"><a id="sommaire_6"></a>6. Que signifient les couleurs des cartes figurant sur les FCV ?</h3>
<ul class="spip">
<li><strong>Vert</strong> : Une vigilance normale doit être observée dans la zone. Il doit cependant être tenu compte des conseils précisés dans les onglets Sécurité et Dernière minute de la fiche du pays.</li>
<li><strong>Jaune</strong> : Une vigilance renforcée doit être observée dans la zone. Le « zonage jaune » correspond à une série de risques limités, à savoir les risques naturels dont la probabilité d’occurrence est en général très aléatoire, les risques humains de basse intensité telle la petite délinquance, les risques sanitaires maîtrisés, donc susceptibles d’être prévenus par vaccination ou traitement prophylactique ou correctement soignés. Ces risques sont jugés, par nature, compatibles avec le tourisme.</li>
<li><strong>Orange</strong> : La zone est déconseillée, sauf pour des raisons impératives d’ordre professionnel, familial, ou autres. Cette énumération n’est pas limitative. Il appartient à chaque voyageur de déterminer si son déplacement revêt un caractère impératif ou non.</li>
<li><strong>Rouge</strong> : La zone est formellement déconseillée. Les voyages sont proscrits.</li></ul>
<h3 class="spip"><a id="sommaire_7"></a>7. Que contient la rubrique « infos pratiques » ?</h3>
<p>La rubrique « infos pratiques » met à votre disposition des informations relatives aux démarches et aux situations que vous pourrez rencontrer avant ou durant un séjour à l’étranger. Vous y trouverez des renseignements répartis en quatre catégories :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  préparer son départ :
<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> conseils sur le type d’assurance à souscrire,
<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> s’inscrire sur le Portail Ariane,
<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> formalités administratives (documents d’identités, visas, formalités pour enfants mineurs, transport animalier, transport et retrait d’argent, formalités concernant l’usage de l’automobile),
<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> transport aérien,
<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> avertissement avant tout voyage. </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  législation :
<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> mineurs à l’étranger (conseils aux familles et aux organisateurs),
<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> législation locale,
<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> actes pédophiles,
<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> contrefaçon.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  assistance aux Français :
<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> le rôle d’un consulat,
<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> incarcération,
<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> victimes à l’étranger,
<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> disparitions inquiétantes,
<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> décès à l’étranger,
<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> mariages forcés.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  risques :
<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> naturels,
<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> sanitaires,
<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> nucléaires,
<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> piraterie maritime,
<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> cybercriminalité. </p>
<h3 class="spip"><a id="sommaire_8"></a>8. Puis-je obtenir une attestation du ministère des Affaires étrangères et du Développement international lorsque les risques ont changé et que mon agence de voyage ne veut pas me rembourser le coût de mon séjour annulé ?</h3>
<p>Non. Le Ministère des Affaires étrangères et du Développement international ne délivre pas d’attestation. Il peut vous adresser la Fiche Conseils aux voyageurs mise en ligne au moment où vous avez décidé d’annuler votre voyage, si celle-ci a été modifiée depuis.</p>
<h3 class="spip"><a id="sommaire_9"></a>9. Que se passe-t-il si je me mets en danger en connaissance de cause et que l’Etat me vient en aide ?</h3>
<p>L’Etat peut exiger le remboursement de tout ou partie des dépenses qu’il a engagées ou dont il serait redevable à l’égard de tiers à l’occasion d’opérations de secours à l’étranger au bénéfice de personnes s’étant délibérément exposées (Article 22 de la LOI n° 2010-873 du 27 juillet 2010 relative à l’action extérieure de l’Etat).</p>
<h3 class="spip"><a id="sommaire_10"></a>10. Où puis-je trouver plus d’informations sur un pays et sur les relations que ce dernier entretient avec la France ?</h3>
<p>Des <a href="http://www.diplomatie.gouv.fr/fr/dossiers-pays/" class="spip_out">dossiers pays</a> existent et fournissent des informations sur la relation bilatérale entre la France et un pays donné.</p>
<h3 class="spip"><a id="sommaire_11"></a>11. Je souhaite m’expatrier, où puis-je trouver des informations utiles pour préparer mon départ ?</h3>
<p>Un onglet « <a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation.md" class="spip_out">Dossiers pays de l’expatriation</a> » vous permet d’obtenir des informations plus précises sur le pays dans lequel vous souhaitez vous expatrier et sur les démarches à entreprendre.</p>
<h3 class="spip"><a id="sommaire_12"></a>12. Je souhaite m’entretenir avec quelqu’un sur mon pays de destination, que mon voyage soit touristique ou d’affaires.</h3>
<p>Si vous ne trouvez pas de réponse à une question spécifique sur votre pays de destination, vous pouvez contacter le Centre de crise et de soutien : +33 1.53.59.11.00 (appel non surtaxé).</p>
<h3 class="spip"><a id="sommaire_13"></a>13. Puis-je voyager avec ma carte d’identité qui bénéficie d’une prolongation de 5 ans ?</h3>
<p>Un <a href="services-aux-citoyens-actualites-article-extension-de-la-duree-de-validite-de-la-carte-nationale-d-identite-decembre.md" class="spip_out">article</a> vous permet d’obtenir de plus amples informations sur l’extension de la durée de validité de la carte nationale d’identité.</p>
<h3 class="spip"><a id="sommaire_14"></a>14. A l’étranger, si j’ai un problème, qui puis-je contacter ?</h3>
<p>Depuis l’étranger, vous pouvez contacter l’ambassade ou le consulat de votre pays de destination (<a href="http://www.mfe.org/index.php/Annuaires/Ambassades-et-consulats-francais-a-l-etranger" class="spip_out" rel="external">Coordonnées des ambassades et consulats de France à l’étranger</a>). Vous pouvez également appeler le Centre de crise et de soutien qui vous répond et conseille 24h/24h au : +33 1.53.59.11.00 (appel non surtaxé).</p>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/foire-aux-questions/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
