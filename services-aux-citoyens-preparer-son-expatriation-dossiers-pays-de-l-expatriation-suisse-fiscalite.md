# Fiscalité

<h2 class="rub22405">Fiscalité du pays</h2>
<p><strong>Présentation</strong></p>
<p>En Suisse, le pouvoir fiscal comprend trois grands volets d’imposition que la Confédération, les 26 cantons et les 3000 communes prélèvent sous forme d’impôts, de contributions et de taxes, appelés communément impôts directs et indirects :</p>
<ul class="spip">
<li>Impôt sur le revenu et sur la fortune ;</li>
<li>Impôt sur la consommation (TVA, impôt sur le tabac, la bière, les automobiles, etc.) ;</li>
<li>Impôt sur la propriété et sur la dépense (impôt sur les véhicules à moteur, les chiens, les divertissements, etc.).</li></ul>
<p>De plus, la législation fiscale suisse est caractérisée par les principes suivants, inscrits dans la Constitution fédérale :</p>
<ul class="spip">
<li>Principe de l’égalité de droit (égalité devant la loi) (art. 8 Cst.) ;</li>
<li>Principe de la liberté économique (art. 27 Cst.) ;</li>
<li>Principe de la garantie de la propriété (art. 26 Cst.) ;</li>
<li>Principe de la liberté de conscience et de croyance (art. 15 Cst.) ;</li>
<li>Interdiction de la double imposition inter cantonale (art. 127, al. 3 Cst.) ;</li>
<li>Interdiction des avantages fiscaux injustifiés (art. 129, al. 3 Cst.).</li></ul>
<p>L’impôt sur le revenu et sur la fortune reste la plus importante des contributions. Il concerne les personnes physiques (les salariés, les professions indépendantes, les rentiers et les retraités), les personnes morales (entreprises, sociétés, organisations…) ainsi que les revenus immobiliers et enfin les gains de loterie. Quant aux déductions, elles concernent l’ensemble des dépenses permettant l’acquisition du revenu. Leur calcul et leur montant varient d’un canton à l’autre. Les revenus sont imposés par paliers et le taux est fonction de votre état civil (marié(e) ou célibataire). On entend par revenus imposables les revenus bruts moins les déductions.</p>
<p>La déclaration des impôts porte toujours sur l’année précédente.</p>
<p><strong>La fiscalité pour les travailleurs étrangers : principes généraux</strong></p>
<p><strong>L’impôt sur le revenu</strong></p>
<p>Les travailleurs étrangers exerçant une activité en Suisse sont soumis à des règles fiscales particulières qui dépendent de plusieurs facteurs : <strong>le type d’activité (salariée ou indépendante), le type de permis de travail et de résidence, le lieu d’habitation (canton, pays), certaines conditions spécifiques (par exemple, la situation du conjoint) et le montant du salaire.</strong></p>
<p>Selon la manière dont se combinent ces facteurs, le travailleur étranger paiera ses impôts soit en Suisse, soit en France et sera soumis au barème d’impôt à la source ou au barème ordinaire.</p>
<p>Vous trouverez sur le site <a href="http://www.travailler-en-suisse.ch/" class="spip_out" rel="external">Travailler en Suisse</a> un tableau récapitulant les principaux cas dans lesquels un étranger peut se trouver au regard de la fiscalité en Suisse.</p>
<p>Le travailleur étranger qui paie ses impôts en Suisse peut être soumis au barème d’impôt à la source ou au barème d’impôt ordinaire. Dans ce dernier cas, la commune d’habitation influera sur le montant de l’impôt à payer, certaines communes proposant un impôt communal plus faible que d’autres.</p>
<p>En Suisse, l’impôt sur le revenu se décompose en impôt fédéral, impôt cantonal et impôt communal. Un impôt sur la fortune (en pour mille) est également appliqué. C’est le canton qui prélève tous ces impôts et qui se charge ensuite de les redistribuer. Les contribuables n’ont donc en général de relation qu’avec l’administration fiscale du canton.</p>
<p><strong>Quelques exemples</strong></p>
<ul class="spip">
<li>Un travailleur frontalier exerçant son activité à Genève paiera ses impôts en Suisse et sera imposé au barème d’impôt à la source ;</li>
<li>Un travailleur frontalier, avec retour quotidien à son domicile en France, exerçant une activité dans le canton de Vaud paiera ses impôts en France ;</li>
<li>Un travailleur frontalier, avec retour hebdomadaire à son domicile en France, exerçant une activité dans le canton de Vaud paiera ses impôts en Suisse.</li></ul>
<p><strong>L’imposition à la source</strong></p>
<p>L’imposition à la source est le prélèvement direct sur votre bulletin de salaire par votre employeur. Ce type d’imposition concerne les catégories de personnes suivantes :</p>
<ul class="spip">
<li>B : autorisation de séjour pour ressortissants étrangers ;</li>
<li>CI : autorisation de travail destinée aux membres de la famille d’un fonctionnaire international ;</li>
<li>F : autorisation pour requérant d’asile ;</li>
<li>G : autorisation pour frontalier ;</li>
<li>L : autorisation pour séjour limité ;</li>
<li>120 jours : autorisation pour séjours de durée limitée en faveur d’étrangers exerçant une activité lucrative en Suisse sans y prendre résidence ;</li>
<li>Frontaliers, quelle que soit leur nationalité (exemple : les contribuables de nationalité suisse, résidant en France et travaillant à Genève) ;</li>
<li>Enfants mineurs quelle que soit leur nationalité ;</li>
<li>Artistes, sportifs, conférenciers domiciliés à l’étranger et se produisant dans le canton de Genève ;</li>
<li>Membres de conseils d’administration, d’organes de direction, de contrôle des personnes morales, domiciliés à l’étranger ;</li>
<li>Bénéficiaires de prestations versées par une institution de prévoyance professionnelle (2ème pilier) ou selon des formes reconnues de prévoyance individuelle liée (3ème pilier A), s’ils sont domiciliés à l’étranger ;</li>
<li>Créanciers hypothécaires domiciliés à l’étranger, si leur créance est garantie par un immeuble sis dans le canton ;</li>
<li>Salariés domiciliés à l’étranger et exerçant leur activité dans le trafic international (bateau, aéronef, transport routier), si leur employeur se trouve en Suisse.</li></ul>
<p>Les personnes qui ne répondent pas à ces catégories relèveront du barème ordinaire et effectueront obligatoirement une déclaration "papier" annuelle.</p>
<p><strong>Date et lieu de dépôt des déclarations</strong></p>
<p>La déclaration d’impôt, qui comporte une déclaration de revenus et une déclaration de fortune, doit être adressée par la poste à l’administration fiscale du canton de résidence. Elle est effectuée sur un formulaire spécifique à chaque canton et à une date qui dépend également du canton (généralement, entre février et mars).</p>
<p>Les travailleurs étrangers soumis au barème à la source et payant, par conséquent, leurs impôts en Suisse n’ont pas de déclaration obligatoire à remplir, sauf si leur fortune dépasse un certain seuil, variable selon les cantons. Dans ce cas, ils devront effectuer une déclaration sur la fortune.</p>
<p><strong>Modalités de paiement</strong></p>
<p>Elles relèvent des prérogatives de chaque canton qui émet les bordereaux d’imposition. Ceux-ci ont force de facture à régler dans les 30 jours. Dans de nombreux cantons, les services fiscaux sont à l’écoute des usagers et prennent le temps de vous expliquer en détail votre situation. Il est par ailleurs souvent possible de négocier, notamment pour l’étalement du paiement.</p>
<p>En règle générale, les paiements, qui se font par virement bancaire ou postal, peuvent être effectués à des dates très variables selon les 26 cantons. Cela va du paiement en une seule fois au paiement en dix fois, avec entre ces deux extrêmes de nombreuses autres possibilités intermédiaires.</p>
<p>Pour les étrangers soumis au barème d’impôt standard, l’impôt étant prélevé à la source, les services fiscaux effectuent un ajustement qui tient compte de la déclaration du contribuable. Si celui-ci a trop versé d’impôt, la différence est reportée en crédit d’impôt ou remboursée. A l’inverse, le contribuable devra régler la différence.</p>
<p><strong>La TVA</strong></p>
<p>L’impôt sur les biens et les services (taxe sur la valeur ajoutée) est régie par une loi fédérale. C’est donc l’administration fédérale des contributions qui est chargée de prélever cette taxe. Depuis le 1er janvier 2011, le taux normal pour la quasi-totalité des biens et services est de 8 %. Quant aux biens de consommation courante (les produits alimentaires à l’exception des alcools, médicaments, journaux, livres et magazines), le taux réduit est de 2,45%. Le secteur de l’hôtellerie affiche quant à lui un taux spécial de 3,6 %.</p>
<p><strong>La fraude fiscale</strong></p>
<p>La législation suisse sur le secret bancaire ne pourra plus faire obstacle au transfert de renseignements demandés par le fisc français. Par conséquent, il est vivement conseillé de faire preuve de la plus grande transparence lorsqu’il s’agit de déclarer ses revenus et sa fortune en Suisse. A ce titre, la France et la Suisse ont signé le 27 août 2009 (entrée en vigueur en 2010), une nouvelle convention de double imposition permettant l’échange d’informations dans le cas de fraude fiscale censée normaliser leurs relations conformément au standard de l’OCDE. Se reporter également à l’article <a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-suisse-vie-pratique-article-cout-de-la-vie.md" class="spip_in">Coût de la vie</a> dans ce portail pays.</p>
<p><strong>Pour en savoir plus</strong></p>
<ul class="spip">
<li><a href="http://www.ch.ch/" class="spip_out" rel="external">Le portail suisse</a> : rubrique Etat et droit.</li>
<li><a href="http://www.ge.ch/" class="spip_out" rel="external">Site internet de la République et du Canton de Genève</a> : rubrique Thèmes / Impôts.</li>
<li><a href="http://www.estv.admin.ch/" class="spip_out" rel="external">Administration fédérale des contributions</a> (ESTV) : rubrique Documentation / Publications.</li></ul>
<p><strong>Année fiscale</strong></p>
<p>L’année fiscale est alignée sur l’année civile. La méthode appliquée est la méthode "postnumerendo", c’est-à-dire que l’imposition s’effectue sur le revenu effectivement perçu durant l’année fiscale. La déclaration des impôts porte toujours sur l’année précédente</p>
<p><strong>Barème de l’impôt</strong></p>
<p>Il existe trois barèmes d’impôt : fédéral, cantonal et communal.</p>
<p>Au sein de la Confédération, les barèmes diffèrent d’un canton à l’autre. Au sein d’un même canton, les barèmes communaux différent d’une commune à l’autre.</p>
<p>Les travailleurs étrangers soumis au barème d’impôt ordinaire verront donc le montant de leur impôt varier d’une commune à l’autre, ce qui ne sera pas le cas des travailleurs étrangers soumis au barème d’impôt à la source. Dans ce dernier cas, quel que soit le lieu d’habitation, le montant de l’impôt sera le même dans un même canton.</p>
<p>A titre d’exemple, un célibataire ayant un salaire brut annuel de 150 000 CHF paiera :</p>
<ul class="spip">
<li>un peu plus de 37 600 CHF d’impôt dans le canton de Bâle-Campagne ;</li>
<li>un peu plus de 37 800 CHF d’impôt dans le canton de Vaud ;</li>
<li>un peu plus de 35 100 CHF d’impôt dans le canton de Berne.</li></ul>
<p>(source : données 2008 de l’<a href="http://www.estv.admin.ch/" class="spip_out" rel="external">Administration fédérale des contributions</a> (AFC)</p>
<p><strong>Quitus fiscal</strong></p>
<p>Il n’est pas exigé de quitus fiscal lorsque l’on quitte le territoire suisse. <strong>Il est cependant fortement conseillé de demander ce document car certains impôts seront payés au " prorata temporis ". </strong>De plus, si vous possédez en Suisse, un capital sous forme de 2ème et/ou 3ème piliers, un quitus fiscal vous sera demandé pour son retrait et/ou son transfert sur un compte bancaire en France.</p>
<p><strong>Coordonnées des centres d’information fiscale</strong></p>
<p><strong>Le principal guichet d’information est celui de l’administration fiscale du canton de résidence.</strong> Tous les sites Internet des cantons proposent des informations fiscales. L’administration fédérale et cantonale répond par courriel aux demandes des contribuables lorsque celles-ci sont formulées correctement. Une permanence téléphonique et un accueil sur place sont également mis en place. Le site internet de ces administrations est complet et correctement illustré.</p>
<p><a href="http://www.estv.admin.ch/" class="spip_out" rel="external">Administration fédérale des contributions</a> (AFC)<br class="manualbr">Eigerstrasse 65 - 3003 Berne<br class="manualbr">Téléphone : [41] 31 332 71 06 <br class="manualbr">Télécopie : [41] 31 322 73 49<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/fiscalite/#sd#mc#estv.admin.ch#" title="sd..åt..estv.admin.ch" onclick="location.href=mc_lancerlien('sd','estv.admin.ch'); return false;" class="spip_mail">Courriel</a></p>
<p>Le personnel de cette administration fédérale parle le français.</p>
<p><strong>Solde du compte en fin de séjour</strong></p>
<p>En cas de départ définitif de la Suisse ou de changement de Canton, il faut impérativement contacter l’administration fiscale cantonale dont vous dépendez. Elle dispose d’un accueil téléphonique et d’une permanence sur place dans ses bureaux. Elle peut être consultée également par écrit.</p>
<p>Au moins deux semaines avant votre départ, il faut vous présenter en personne afin de remplir un formulaire de départ. Ce document est généralement mis en ligne sur le site de l’administration fiscale cantonale compétente. Il est conseillé d’en prendre connaissance au préalable et de vous munir des pièces et justificatifs à fournir. Ceci concerne toutes les personnes qu’elles soient salariées et non au moment de leur départ.</p>
<p>Si vous projetez de quitter la Suisse momentanément avec l’intention de vous y établir à nouveau, il est préférable d’en informer l’administration fiscale.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://ge.ch/" class="spip_out" rel="external">Administration fiscale cantonale de Genève</a> et de son Canton</li>
<li><a href="http://www.vd.ch/" class="spip_out" rel="external">Administration fiscale cantonale de Vaud</a></li>
<li><a href="http://www.vs.ch/" class="spip_out" rel="external">Administration du Canton du Valais</a></li>
<li><a href="http://www.steueramt.zh.ch" class="spip_out" rel="external">Administration cantonale de Zurich</a> (en anglais et allemand)</li></ul>
<p><i>Mise à jour : février 2013</i></p>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/fiscalite/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
