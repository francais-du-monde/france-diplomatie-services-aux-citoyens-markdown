# Lettre de motivation

<h4 class="spip">Rédaction</h4>
<p>Votre lettre doit être concise (environ une page) et bien présentée. Vous pouvez la rédiger dans un style relativement informel. Réservez détails et chiffres pour votre CV. N’oubliez pas de mentionner la date, votre nom, votre adresse et votre numéro de téléphone ainsi que l’emploi auquel la lettre se réfère. Décrivez votre expérience précédente, expliquez la raison pour laquelle vous pensez pouvoir convenir pour cet emploi précis, précisez si vous posez votre candidature par recommandation personnelle, etc. Terminez la lettre par la formule de politesse « Je vous prie d’agréer, Madame, Monsieur, en l’expression de mes sentiments les meilleurs » et votre signature à la main.</p>
<p>Les formules de politesse en Suède sont bien moins formelles qu’en France et les suédois utilisent un style plus familier.</p>
<p>Notamment, on ne vouvoie pas les personnes (<i>ni</i>), on tutoie tout le monde (<i>du</i>). Ce qui, en anglais, ne fait aucune différence (<i>you</i>).</p>
<p>L’introduction peut être un simple <i>Hej</i> et la formule de conclusion est souvent « <i>Med vänliga hälsningar</i> » (avec mes salutations amicales) que l’on trouve très souvent en version <i>mvh</i> dans la vie de tous les jours.</p>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li><a href="http://www.iagora.com/work/en/resumes/letter_model_sweden.html" class="spip_out" rel="external">Modèles de lettre de motivation</a></li>
<li><a href="http://www.babla.fr/phrases/candidature/lettre-de-motivation/francais-suedois/" class="spip_out" rel="external">Quelques formules pour rédiger votre lettre</a></li></ul>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/emploi-stage/article/lettre-de-motivation). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
