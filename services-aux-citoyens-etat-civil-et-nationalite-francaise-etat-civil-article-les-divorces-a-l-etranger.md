# Faire reconnaitre un divorce prononcé à l’étranger

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/etat-civil/article/les-divorces-a-l-etranger#sommaire_1">Cas où la vérification d’opposabilité est nécessaire : pays hors Union européenne et Danemark</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/etat-civil/article/les-divorces-a-l-etranger#sommaire_2">Cas où la vérification d’opposabilité n’est plus systématiquement nécessaire : pays de l’Union européenne sauf le Danemark</a></li></ul>
<p>En règle générale, le divorce prononcé par une autorité étrangère doit faire l’objet d’une vérification d’opposabilité du Procureur de la République dont dépend l’officier d’état civil qui a célébré le mariage pour les mariages célébrés en France, et le Procureur de la République près le Tribunal de grande Instance de Nantes pour les mariages célébrés à l’étranger.</p>
<p>Avertissement important : Si le mariage a été célébré, à l’étranger, à compter du 1er mars 2007, l’acte correspondant doit être préalablement transcrit sur les registres de l’état civil français.</p>
<h3 class="spip"><a id="sommaire_1"></a>Cas où la vérification d’opposabilité est nécessaire : pays hors Union européenne et Danemark</h3>
<p>La demande de vérification d’opposabilité adressée au Procureur de la République doit être accompagnée des justificatifs ci-après :</p>
<ul class="spip">
<li>une copie intégrale de la décision en original ou en copie certifiée conforme. Si ladite décision n’est pas motivée, il convient de produire également l’acte de saisine du tribunal ou tout autre acte comportant l’exposé de la demande ;</li>
<li>la preuve du caractère définitif de la décision étrangère (certificat de non-recours, acte d’acquiescement, certificat établi par l’avocat ou l’avoué, ou par toute autre autorité habilitée, ou à défaut tout autre acte étranger portant mention de la décision) ;</li>
<li>la traduction, par un traducteur expert, des pièces établies en langue étrangère, éventuellement légalisées ;</li>
<li>la preuve du domicile des parties au jour de l’introduction de l’instance devant l’autorité étrangère (si elle ne résulte pas de la lecture même de la décision) ;</li>
<li>la preuve de la nationalité des parties au jour de l’introduction de l’instance devant l’autorité étrangère ;</li>
<li>la copie intégrale des actes de l’état civil conservés par une autorité française, en marge desquels doit être apposée, le cas échéant, la mention de la décision étrangère.Si la décision étrangère est jugée opposable en France, le Procureur de la République donne instruction aux officiers d’état civil concernés d’apposer la mention de divorce en marge des actes dont ils sont détenteurs.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Cas où la vérification d’opposabilité n’est plus systématiquement nécessaire : pays de l’Union européenne sauf le Danemark</h3>
<p>En application du <a href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/Bruxelles-IIbis_cle055e21.pdf" class="spip_out">règlement n°2201/2003 du Conseil de l’Union européenne</a>, sont dispensés de la procédure de vérification d’opposabilité les divorces prononcés dans les pays de l’Union européenne sauf le Danemark, à condition que :</p>
<ul class="spip">
<li>la procédure ait été engagée après le 1er mars 2001 pour un divorce prononcé dans l’un des pays suivants : Allemagne, Autriche, Belgique, Espagne, Finlande, Grèce, Irlande, Italie, Luxembourg, Pays-Bas, Portugal, Royaume-Uni, Suède.</li>
<li>la procédure ait été engagée après le 1er mai 2004 pour un divorce prononcé dans l’un des pays suivants : Chypre, Estonie, Hongrie, Lettonie, Lituanie, Malte <i>(En l’état actuel de sa législation, Malte ne permet pas le divorce. Néanmoins, les jugements de divorce rendus à l’étranger y sont reconnus)</i>, Pologne, République Tchèque, Slovaquie, Slovénie.</li>
<li>la procédure ait été engagée après le 1er janvier 2007 pour un divorce prononcé en Roumanie ou en Bulgarie.Pour les divorces qui ne sont pas soumis à la procédure de vérification d’opposabilité, les intéressés doivent demander directement aux officiers d’état civil détenteurs de leurs actes de naissance et de mariage, l’apposition de la mention correspondante.</li></ul>
<p>Pour ce faire, ils doivent présenter ou remettre à l’officier de l’état civil :</p>
<ul class="spip">
<li>une demande écrite, datée et signée, indiquant leur identité et celle de leur ancien conjoint (nom, prénom(s), date et lieu de naissance) et précisant les actes de naissance ou de mariage dont la mise à jour est sollicitée ;</li>
<li>une copie intégrale de la décision ou de l’acte étrangèr(e) en original ou en copie certifiée conforme ;</li>
<li>un certificat dûment rempli par la juridiction ou l’autorité étrangère compétente de l’Etat membre de l’Union européenne dans lequel la décision a été rendue. Ce certificat, prévu aux articles 37.1b) et 39 du règlement précité du Conseil de l’Union européenne, doit être rédigé, daté et signé, conformément à l’annexe I du même règlement ;</li>
<li>à défaut de certificat ou le cas échéant de copie d’un acte étranger de naissance ou de mariage portant mention de la décision, tout document lui permettant de disposer des renseignements qui figureraient dans le certificat ;</li>
<li>si la décision a été rendue par défaut, tout document visé à l’article 39 du règlement précité, à moins que la demande n’émane du défendeur défaillant ;</li>
<li>la traduction, par un traducteur expert, des pièces établies en langue étrangère ;</li>
<li>la copie intégrale ou l’extrait de tous les actes de l’état civil français dont ils sollicitent la mise à jour ;</li>
<li>si le mariage a été célébré à l’étranger et si l’acte de mariage n’est pas conservé par une autorité française, la copie ou l’extrait de l’acte de mariage éventuellement légalisé et traduit.</li></ul>
<p><i>Mise à jour : mars 2016</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/etat-civil/article/les-divorces-a-l-etranger). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
