# Important : rappel sur l’assurance-rapatriement à l’attention des voyageurs

<p>Dans des situations d’urgence où un risque vital est avéré pour l’un de nos compatriotes, le ministère des affaires étrangères peut, malgré l’absence d’assurance, conduire à titre exceptionnel le rapatriement.</p>
<p>Nos compatriotes et leurs familles sont informés en amont des modalités budgétaires de ces opérations, qui donnent lieu à une avance par le ministère des affaires étrangères et à un engagement de remboursement de la dette ainsi contractée envers l’Etat.</p>
<p>Le recouvrement des frais permet de préserver notre capacité à venir en aide, dans le futur, à nos compatriotes en difficulté.</p>
<p><i>Mise à jour : juillet 2016</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/en-cas-de-difficulte-a-l-etranger/article/important-rappel-sur-l-assurance-rapatriement-a-l-attention-des-voyageurs). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
