# Vaccination

<p>Aucune vaccination n’est exigée à l’entrée du pays pour un voyageur en provenance de France. Les vaccinations suivantes sont conseillées :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>pour les adultes</strong> : mise à jour des vaccinations contre la diphtérie le tétanos et la poliomyélite ; vaccinations contre la typhoïde, le choléra, l’hépatite A, l’hépatite B et la rage (pour les séjours de longue durée).</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>pour les enfants </strong> : vaccinations recommandées en France par le ministère de la Santé et en particulier B.C.G. et hépatite B dès le 1er mois, rougeole dès l’âge de neuf mois. Hépatite A possible à partir d’un an. Typhoïde à partir de cinq ans.</p>
<p>Pour en savoir plus, lisez notre article thématique sur la <a href="services-aux-citoyens-preparer-son-expatriation-sante.md" class="spip_in">Santé</a>.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/entree-et-sejour/article/vaccination-110837). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
