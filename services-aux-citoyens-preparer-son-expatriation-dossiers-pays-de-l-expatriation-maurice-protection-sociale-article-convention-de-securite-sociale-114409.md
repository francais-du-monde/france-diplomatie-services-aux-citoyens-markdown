# Convention de sécurité sociale

<p><strong>Il n’existe pas de convention de sécurité sociale entre la France et Maurice</strong> Vous pouvez donc vous trouver dans l’une des deux situations suivantes :</p>
<ul class="spip">
<li>Travailleurs salariés détachés dans le cadre de la législation française</li>
<li>Travailleurs français expatriés (salariés, non-salariés, retraités, autres catégories) relevant de la législation locale</li></ul>
<p>Pour plus d’information, consultez notre rubrique thématique <a href="services-aux-citoyens-preparer-son-expatriation-protection-sociale.md" class="spip_in">Protection sociale</a>.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/protection-sociale/article/convention-de-securite-sociale-114409). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
