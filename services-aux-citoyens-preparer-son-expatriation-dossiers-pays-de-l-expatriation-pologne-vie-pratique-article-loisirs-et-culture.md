# Loisirs et culture

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/vie-pratique/article/loisirs-et-culture#sommaire_1">Tourisme</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/vie-pratique/article/loisirs-et-culture#sommaire_2">Activités culturelles</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/vie-pratique/article/loisirs-et-culture#sommaire_3">Instituts français </a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/vie-pratique/article/loisirs-et-culture#sommaire_4">Sports</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/vie-pratique/article/loisirs-et-culture#sommaire_5">Télévision – Radio</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/vie-pratique/article/loisirs-et-culture#sommaire_6">Presse française</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Tourisme</h3>
<p>Pour plus d’informations, contacter :</p>
<p><a href="http://www.pologne.travel/fr/" class="spip_out" rel="external">Office National polonais du tourisme</a> <br class="manualbr">9, rue de la Paix,<br class="manualbr">75002 Paris<br class="manualbr">tél. : +(33) 01 42 44 19 00</p>
<p>Internet : <a href="http://www.pot.gov.pl/" class="spip_out" rel="external">POT.gov.pl</a> (site en polonais)</p>
<h3 class="spip"><a id="sommaire_2"></a>Activités culturelles</h3>
<p><strong>Activités culturelles en français</strong></p>
<p>La culture française est bien représentée et de nombreuses activités ont lieu.</p>
<p>Les Instituts français de Varsovie et de Cracovie et les Alliances françaises réparties dans plusieurs villes dont Byalystok, Bydgoszcz, Gdansk, Katowice, Lodz, Lublin, Poznan, Torun, Szczecin, Gorzow, Opole, Rzeszow, Rybnik, Walbrzych et Wroclaw, programment de multiples manifestations culturelles (théâtre, concert, conférences, expositions, séminaires) co-organisés pour la plupart avec des partenaires locaux. Ces institutions disposent également de bibliothèques et de médiathèques. Pour plus d’informations concernant les Alliances françaises :</p>
<p><a href="http://www.af-enpologne.pl/" class="spip_out" rel="external">Service de coopération et d’action culturelle</a><br class="manualbr">Ambassade de France <br class="manualbr">Ul. Piekna 1 <br class="manualbr">00-477 Varsovie <br class="manualbr">Tél. : (48) 22 529 30 84 <br class="manualbr">Fax : (48) 22 529 30 08</p>
<h3 class="spip"><a id="sommaire_3"></a>Instituts français </h3>
<p><strong>Varsovie </strong></p>
<p><strong>Institut français</strong><br class="manualbr">ul. Widok 12 <br class="manualbr">00-023 Warszawa<br class="manualbr">Téléphone : [48] (22) 505 98 00 <br class="manualbr">Télécopie : [48] (22) 505 98 73 <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/vie-pratique/article/loisirs-et-culture#ifv#mc#ifvf.pl#" title="ifv..åt..ifvf.pl" onclick="location.href=mc_lancerlien('ifv','ifvf.pl'); return false;" class="spip_mail">Courriel</a></p>
<p><strong>Cracovie </strong></p>
<p><a href="http://www.cracovie.org.pl/" class="spip_out" rel="external">Institut Français</a><br class="manualbr">ul. Stolarska 15 <br class="manualbr">31-043 Krakow <br class="manualbr">Tél. : (12) 424 53 50 <br class="manualbr">Fax : (12) 424 53 70</p>
<p>Des films français sont présentés dans les salles de cinéma et à la télévision, avec un doublage sonore en polonais se superposant à la version originale.</p>
<p><strong>Activités culturelles locales</strong></p>
<p>Dans le domaine culturel, il convient de signaler plusieurs manifestations. Parmi elles : le <i>Festival musical du Vieux Cracovie</i> au mois d’août. Varsovie draine, elle aussi, de multiples événements culturels, notamment musicaux avec l’organisation de nombreux concerts. En janvier, les <i>Rencontres théâtrales de Varsovie</i> regroupent les meilleures créations des mois passés. Au mois de novembre, à Gdynia, se tient un excellent festival de cinéma.</p>
<p>Dans toutes les villes, il existe de nombreuses salles de cinéma. Les films projetés sont en majorité américains en version originale avec sous-titrage. Prix : 25 PLN (tarifs réduits pour les étudiants, retraités…).</p>
<p>La musique est bien représentée : les salles de concert proposent des manifestations de qualité, l’Opéra National de Varsovie programme toute l’année des ballets et des opéras. De nombreux festivals ont lieu, dont le festival Mozart à Varsovie durant l’été.</p>
<p>Les théâtres offrent un répertoire varié.</p>
<p>Des expositions de peinture, sculpture, photographies ont lieu dans les musées, les galeries d’Etat et privées. Les musées offrent des collections riches en œuvres polonaises principalement.</p>
<p>La télévision propose des programmes d’assez bonne qualité. Les clubs vidéo sont nombreux. Le système adopté est PAL.</p>
<p>A Cracovie :</p>
<p>Dans cette ville de nombreuses activités sont à la disponibilité des habitants : 33 salles de cinéma assez confortables projettent des films en grande majorité américains. De nombreux spectacles musicaux sont organisés (musique classique, jazz, ballets, opéras).</p>
<p>Il existe de nombreux sites internet dédiés aux activités culturelles accessibles aux personnes maîtrisant la langue polonaise et notamment :</p>
<ul class="spip">
<li><a href="http://www.mkidn.gov.pl/" class="spip_out" rel="external">site du Ministère de la culture et du patrimoine national</a>,</li>
<li><a href="http://www.chopin.pl/" class="spip_out" rel="external">Chopin.pl</a>, site dédié à la musique classique et à ce compositeur,</li>
<li><a href="http://www.info.galerie.art.pl/" class="spip_out" rel="external">Info.galerie.art.pl</a>, site dédié à l’art contemporain,</li>
<li><a href="http://www.teatry.art.pl/" class="spip_out" rel="external">Teatry.art.pl</a>, site concernant l’actualité théâtrale du pays,</li>
<li><a href="http://www.filmpolski.pl/" class="spip_out" rel="external">www.filmpolski.pl</a> (site du cinéma polonais).</li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Sports</h3>
<p>Tous les sports, individuels ou d’équipe, peuvent être pratiqués en Pologne. Il est possible de se procurer sur place l’équipement nécessaire.</p>
<p>La pratique de la chasse est particulièrement appréciée dans les régions de Bieszczady et Walbrzych. Les périodes d’ouverture et de fermeture sont à peu près les mêmes qu’en France. Il est nécessaire de posséder un permis de chasse qu’on ne peut obtenir sans le permis français. Il est aussi possible de s’adonner à la pêche en s’adressant aux bureaux de tourisme. Le permis est nécessaire.</p>
<h3 class="spip"><a id="sommaire_5"></a>Télévision – Radio</h3>
<p>Il est possible de capter tous les jours Radio France Internationale, avec une qualité de réception moyenne.</p>
<p>Télévision locale : trois chaînes de télévision publique et plusieurs chaînes privées.</p>
<p>Possibilité de recevoir la télévision par parabole ou par câble. La plate-forme numérique (décodeur) CYFRA + propose plusieurs chaînes (Canal+ polonais, HBO, EuroNews, TV5, France 24…. Et des chaînes radio : France Inter, France Info…). Coût d’abonnement : 50 – 150 PLN/mois.</p>
<h3 class="spip"><a id="sommaire_6"></a>Presse française</h3>
<p>Quotidiens et presse hebdomadaire sont disponibles dans les magasins spécialisés (chaîne EMPIK notamment) et les centres commerciaux, grands hôtels, gares, aéroports.</p>
<p>Possibilité de livraison à domicile.</p>
<p><i>Mise à jour : juin 2014 </i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/vie-pratique/article/loisirs-et-culture). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
