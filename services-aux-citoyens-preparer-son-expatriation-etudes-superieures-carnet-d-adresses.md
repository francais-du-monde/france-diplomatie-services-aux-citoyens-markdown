# Carnet d’adresses

<h2 class="rub15455">Sommaire</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/etudes-superieures/carnet-d-adresses/#sommaire_1">Sites Internet</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/etudes-superieures/carnet-d-adresses/#sommaire_2">Adresses utiles</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Sites Internet</h3>
<ul class="spip">
<li><a href="http://ec.europa.eu/ploteus/home.jsp?language=fr" class="spip_out" rel="external">Portail européen</a>  sur les opportunités d’études et de formation en Europe</li>
<li><a href="http://ec.europa.eu/stages" class="spip_out" rel="external">Site d’information sur les stages</a>  dans les instances européennes</li>
<li>Site de l’<a href="http://www.unosel.com/" class="spip_out" rel="external">Union Nationale des Organisations de Séjours Éducatifs, Linguistiques et des Écoles de Langues (UNOSEL)</a></li>
<li><a href="http://www.braintrack.com/" class="spip_out" rel="external">Portail</a>  d’accès aux sites des universités du monde entier</li>
<li><a href="http://www.touteleurope.eu/les-politiques-europeennes/education-et-formation.html" class="spip_out" rel="external">Site</a> délivrant notamment des informations pratiques sur les programmes européens et des informations très accessibles sur l’Union européenne</li>
<li><a href="http://www.cnous.fr/" class="spip_out" rel="external">Centre national des œuvres universitaires et scolaires</a></li>
<li><a href="http://www.fulbright-france.org/" class="spip_out" rel="external">Informations sur les études aux États-Unis</a></li>
<li><a href="http://www.acenet.edu/Pages/default.aspx" class="spip_out" rel="external">American Council on Education</a></li>
<li><a href="http://www.education.gouv.fr/" class="spip_out" rel="external">Ministère de l’Éducation nationale</a> :</li>
<li><a href="http://www.euroguidance-france.org/" class="spip_out" rel="external">Site des centres nationaux de ressources</a></li>
<li><a href="http://www.diplomatie.gouv.fr/fr/" class="spip_out">Ministère des Affaires étrangères et du Développement international</a></li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Adresses utiles</h3>
<table class="spip">
<thead><tr class="row_first"><th id="id8e15_c0">Organismes </th><th id="id8e15_c1">Adresse </th><th id="id8e15_c2">Téléphone </th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="id8e15_c0"><a href="http://www.cci.fr/web/organisation-du-reseau" class="spip_out" rel="external">Chambres françaises de commerce et d’industrie</a></td>
<td headers="id8e15_c1">45, avenue d’Iéna
<p>BP 448-16</p>
<p>75769 Paris cedex 16</p>
</td>
<td headers="id8e15_c2">Tél. 01 40 69 37 00
<p>Fax : 01 47 20 61 28</p>
</td></tr>
<tr class="row_even even">
<td headers="id8e15_c0"><a href="http://www.acenet.edu/Pages/default.aspx" class="spip_out" rel="external">American Council on Education</a></td>
<td headers="id8e15_c1">One Dupont Circle
<p>NW, Washington DC 20036</p>
<p>USA</p>
</td>
<td headers="id8e15_c2">Tél. (202) 939 93 00</td></tr>
<tr class="row_odd odd">
<td headers="id8e15_c0"><a href="http://www.andes.asso.fr/" class="spip_out" rel="external">Association Nationale des Docteurs Es Sciences (ANDES)</a></td>
<td headers="id8e15_c1">16, rue Claude Bernard
<p>75231 Paris cedex 05</p>
</td>
<td headers="id8e15_c2">Tél. 01 43 37 51 12
<p>Fax : 01 43 37 18 42</p>
</td></tr>
<tr class="row_even even">
<td headers="id8e15_c0"><a href="http://www.britishcouncil.fr/en" class="spip_out" rel="external">British Council</a></td>
<td headers="id8e15_c1">9-11, rue de Constantine
<p>75007 Paris</p>
</td>
<td headers="id8e15_c2">Tél. 01 49 55 73 00
<p>Fax : 01 47 05 77 01</p>
</td></tr>
<tr class="row_odd odd">
<td headers="id8e15_c0"><a href="http://www.cca-paris.com/" class="spip_out" rel="external">Centre culturel algérien</a></td>
<td headers="id8e15_c1">171, rue de la Croix Nivert
<p>75015 Paris</p>
</td>
<td headers="id8e15_c2">Tél. 01 45 54 95 31
<p>Fax : 01 44 26 30 90</p>
</td></tr>
<tr class="row_even even">
<td headers="id8e15_c0"><a href="http://www.goethe.de/ins/fr/par/deindex.htm?wt_sc=paris" class="spip_out" rel="external">Centre culturel allemand</a></td>
<td headers="id8e15_c1">17, avenue d’Iéna
<p>75116 Paris</p>
</td>
<td headers="id8e15_c2">Tél. 01 44 43 92 30
<p>Fax : 01 44 43 92 40</p>
</td></tr>
<tr class="row_odd odd">
<td headers="id8e15_c0"><a href="http://www.cca-anatolie.com/" class="spip_out" rel="external">Centre culturel Anatolie</a></td>
<td headers="id8e15_c1">77, rue La Fayette
<p>75009 Paris</p>
</td>
<td headers="id8e15_c2">Tél. 01 42 80 04 74
<p>Fax : 01 42 80 61 12</p>
</td></tr>
<tr class="row_even even">
<td headers="id8e15_c0"><strong>Centre culturel arabe syrien</strong></td>
<td headers="id8e15_c1">12, avenue de Tourville
<p>75007 Paris</p>
</td>
<td headers="id8e15_c2">Tél. 01 47 05 30 11
<p>Fax : 01 47 05 23 11</p>
</td></tr>
<tr class="row_odd odd">
<td headers="id8e15_c0"><a href="http://www.casaargentinaenparis.org/fra/home.php" class="spip_out" rel="external">Centre culturel argentin</a></td>
<td headers="id8e15_c1">6, rue Cimarosa
<p>75116 Paris</p>
</td>
<td headers="id8e15_c2">Tél. 01 47 27 15 11
<p>Fax : 01 47 04 61 51</p>
</td></tr>
<tr class="row_even even">
<td headers="id8e15_c0"><a href="http://www.coree-culture.org/" class="spip_out" rel="external">Centre culturel coréen</a></td>
<td headers="id8e15_c1">2, avenue d’Iéna
<p>75116 Paris</p>
</td>
<td headers="id8e15_c2">Tél. 01 47 20 83 86
<p>Fax : 01 47 23 58 97</p>
</td></tr>
<tr class="row_odd odd">
<td headers="id8e15_c0"><a href="http://www.bureaucultureleg.fr/" class="spip_out" rel="external">Centre culturel d’Égypte</a></td>
<td headers="id8e15_c1">111, boulevard Saint Michel
<p>75005 Paris</p>
</td>
<td headers="id8e15_c2">Tél. 01 46 33 75 67
<p>Fax : 01 43 26 18 83</p>
</td></tr>
<tr class="row_even even">
<td headers="id8e15_c0"><a href="http://www.russie.net/" class="spip_out" rel="external">Centre culturel de Russie</a></td>
<td headers="id8e15_c1">61, rue Boissière
<p>75116 Paris</p>
</td>
<td headers="id8e15_c2">Tél. 01 44 34 79 79
<p>Fax : 01 44 34 79 74</p>
</td></tr>
<tr class="row_odd odd">
<td headers="id8e15_c0"><strong>Centre culturel de Taipei</strong></td>
<td headers="id8e15_c1">78, rue de l’Université
<p>75007 Paris</p>
</td>
<td headers="id8e15_c2">Tél. 01 44 39 88 66
<p>Fax : 01 44 39 88 79</p>
</td></tr>
<tr class="row_even even">
<td headers="id8e15_c0"><a href="http://icm.sre.gob.mx/francia/" class="spip_out" rel="external">Centre culturel du Mexique</a></td>
<td headers="id8e15_c1">119, rue Vieille du Temple
<p>75003 Paris</p>
</td>
<td headers="id8e15_c2">Tél. 01 44 61 84 44
<p>Fax : 01 44 61 84 45</p>
</td></tr>
<tr class="row_odd odd">
<td headers="id8e15_c0"><a href="http://www.cchel.org/" class="spip_out" rel="external">Centre culturel hellénique</a></td>
<td headers="id8e15_c1">23, rue Galilée
<p>75116 Paris</p>
</td>
<td headers="id8e15_c2">Tél. 01 47 23 39 06
<p>Fax : 01 47 20 84 97</p>
</td></tr>
<tr class="row_even even">
<td headers="id8e15_c0"><strong>Centre culturel indo-français</strong></td>
<td headers="id8e15_c1">12, rue Notre Dame de Nazareth
<p>75003 Paris</p>
</td>
<td headers="id8e15_c2">Tél. 01 42 78 80 53</td></tr>
<tr class="row_odd odd">
<td headers="id8e15_c0"><strong>Centre culturel iranien</strong></td>
<td headers="id8e15_c1">6, rue Jean Bart
<p>75006 Paris</p>
</td>
<td headers="id8e15_c2">Tél. 01 45 49 19 20
<p>Fax : 01 45 49 31 34</p>
</td></tr>
<tr class="row_even even">
<td headers="id8e15_c0"><a href="http://www.icr.ro/paris/" class="spip_out" rel="external">Centre culturel roumain</a></td>
<td headers="id8e15_c1">1, rue de l’Exposition
<p>75007 Paris</p>
</td>
<td headers="id8e15_c2">Tél. 01 47 05 15 31
<p>Fax : 01 47 05 15 50</p>
</td></tr>
<tr class="row_odd odd">
<td headers="id8e15_c0"><a href="https://eng.si.se/" class="spip_out" rel="external">Centre culturel suédois</a></td>
<td headers="id8e15_c1">11, rue Payenne
<p>75003 Paris</p>
</td>
<td headers="id8e15_c2">Tél. 01 44 78 80 20
<p>Fax : 01 44 78 80 27</p>
</td></tr>
<tr class="row_even even">
<td headers="id8e15_c0"><a href="http://www.ccsparis.com/" class="spip_out" rel="external">Centre culturel suisse</a></td>
<td headers="id8e15_c1">32-34, rue des Francs-Bourgeois
<p>75003 Paris</p>
</td>
<td headers="id8e15_c2">Tél. 01 42 71 44 50
<p>Fax : 01 42 71 51 24</p>
</td></tr>
<tr class="row_odd odd">
<td headers="id8e15_c0"><strong>Centre culturel yougoslave</strong></td>
<td headers="id8e15_c1">123, rue Saint Martin
<p>75004 Paris</p>
</td>
<td headers="id8e15_c2">Tél. 01 42 72 50 50
<p>Fax : 01 42 72 52 80</p>
</td></tr>
<tr class="row_even even">
<td headers="id8e15_c0"><a href="http://www.ciep.fr/enic-naric-france" class="spip_out" rel="external">Centre ENIC-NARIC</a> (information et reconnaissance des diplômes étrangers en France)</td>
<td headers="id8e15_c1"><a href="http://www.education.gouv.fr/" class="spip_out" rel="external">Ministère de l’Éducation nationale</a>
<p>Délégation aux Relations Internationales et à la Coopération</p>
<p>110, rue de Grenelle</p>
<p>75007 Paris</p>
</td>
<td headers="id8e15_c2">Tél. 01 55 55 04 15
<p>Fax : 01 55 55 04 23</p>
</td></tr>
<tr class="row_odd odd">
<td headers="id8e15_c0"><a href="http://paris.czechcentres.cz/" class="spip_out" rel="external">Centre tchèque</a></td>
<td headers="id8e15_c1">18, rue Bonaparte
<p>75006 Paris</p>
</td>
<td headers="id8e15_c2">Tél. 01 53 73 00 25
<p>Fax : 01 43 29 57 67</p>
</td></tr>
<tr class="row_even even">
<td headers="id8e15_c0"><strong>Centre Wallonie-Bruxelles Information - Études</strong></td>
<td headers="id8e15_c1">7, rue de Venise
<p>75004 Paris</p>
</td>
<td headers="id8e15_c2">Tél. 01 53 01 96 96
<p>Fax : 01 48 04 90 85</p>
</td></tr>
<tr class="row_odd odd">
<td headers="id8e15_c0"><a href="http://www.cidj.com/" class="spip_out" rel="external">CIDJ</a></td>
<td headers="id8e15_c1">101, quai Branly
<p>75740 Paris cedex 15</p>
</td>
<td headers="id8e15_c2">Tél. 01 44 49 12 25
<p>Fax : 01 40 65 02 61</p>
</td></tr>
<tr class="row_even even">
<td headers="id8e15_c0"><a href="http://www.cnous.fr/" class="spip_out" rel="external">Centre National des Œuvres Universitaires et Scolaires (CNOUS)</a></td>
<td headers="id8e15_c1">6, rue Jean Calvin BP 49
<p>75222 Paris cedex 05</p>
</td>
<td headers="id8e15_c2">Tél. 01 44 18 53 00</td></tr>
<tr class="row_odd odd">
<td headers="id8e15_c0"><a href="http://www.fulbright-france.org/gene/main.php" class="spip_out" rel="external">Commission franco-américaine d’échanges universitaires et culturels</a></td>
<td headers="id8e15_c1">9, rue Chardin
<p>75016 Paris</p>
</td>
<td headers="id8e15_c2">Tél. 01 44 14 53 60
<p>Fax : 01 42 88 04 79</p>
</td></tr>
<tr class="row_even even">
<td headers="id8e15_c0"><a href="http://www.campusfrance.org/" class="spip_out" rel="external">Campus France - Etablissement public à caractère industriel et commercial</a></td>
<td headers="id8e15_c1">28, rue de la Grange aux Belles
<p>75010 Paris</p>
</td>
<td headers="id8e15_c2">Tél. 01 40 40 58 58
<p>Fax : 01 42 00 70 08</p>
</td></tr>
<tr class="row_odd odd">
<td headers="id8e15_c0"><strong>Espace culturel tunisien
<p>Consulat général tunisien</p></strong>
</td>
<td headers="id8e15_c1">25, rue Fortuny
<p>75017 Paris</p>
</td>
<td headers="id8e15_c2">Tél. 01 53 70 69 10</td></tr>
<tr class="row_even even">
<td headers="id8e15_c0"><a href="http://eacea.ec.europa.eu/education/eurydice/index_fr.php" class="spip_out" rel="external">EURYDICE</a> - Le réseau d’information sur l’éducation en Europe</td>
<td headers="id8e15_c1">Rue d’Arlon 15
<p>B - 1050 Bruxelles</p>
</td>
<td headers="id8e15_c2">Tél. (32-2) 238 30 11
<p>Fax : (32-2) 230 65 62</p>
<p>Courriel : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/etudes-superieures/carnet-d-adresses/#info#mc#eurydice.org#" title="info..åt..eurydice.org" onclick="location.href=mc_lancerlien('info','eurydice.org'); return false;" class="spip_mail">info<span class="spancrypt"> [at] </span>eurydice.org</a></p>
</td></tr>
<tr class="row_odd odd">
<td headers="id8e15_c0"><strong>Fondation de France</strong></td>
<td headers="id8e15_c1">40, avenue Hoche
<p>75008 Paris</p>
</td>
<td headers="id8e15_c2">Tél. 01 44 21 31 00</td></tr>
<tr class="row_even even">
<td headers="id8e15_c0"><strong>Association internationale pour les stages techniques à l’étranger</strong> (IAESTE)</td>
<td headers="id8e15_c1">_ École des Mines d’Albi
<p>Campus Jarlard - route du Teillet</p>
<p>81013 Albi cedex</p>
</td>
<td headers="id8e15_c2">Tél. 05 63 49 30 02
<p>Fax : 05 63 49 30 96</p>
<p>Courriel : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/etudes-superieures/carnet-d-adresses/#iaeste#mc#enstimac.fr#" title="iaeste..åt..enstimac.fr" onclick="location.href=mc_lancerlien('iaeste','enstimac.fr'); return false;" class="spip_mail">iaeste<span class="spancrypt"> [at] </span>enstimac.fr</a></p>
</td></tr>
<tr class="row_odd odd">
<td headers="id8e15_c0"><a href="http://www.instituto-camoes.pt/" class="spip_out" rel="external">Institut Camoës</a></td>
<td headers="id8e15_c1">Rue Raffet
<p>75016 Paris</p>
</td>
<td headers="id8e15_c2">Tél. 01 53 92 01 00
<p>Fax : 01 45 24 64 78</p>
</td></tr>
<tr class="row_even even">
<td headers="id8e15_c0"><a href="http://www.cervantes.es/default.htm" class="spip_out" rel="external">Institut Cervantes</a></td>
<td headers="id8e15_c1">7, rue Quentin Bauchart
<p>75008 Paris</p>
</td>
<td headers="id8e15_c2">Tél. : 01 40 70 92 92
<p>Fax : 01 47 20 27 49</p>
</td></tr>
<tr class="row_odd odd">
<td headers="id8e15_c0"><a href="http://www.fca-fr.com/" class="spip_out" rel="external">Institut culturel autrichien</a></td>
<td headers="id8e15_c1">17, avenue de Villars
<p>75007 Paris</p>
</td>
<td headers="id8e15_c2">Tél. 01 47 05 27 10
<p>Fax : 01 47 05 26 42</p>
</td></tr>
<tr class="row_even even">
<td headers="id8e15_c0"><a href="http://www.institut-finlandais.fr/" class="spip_out" rel="external">Institut culturel finlandais</a></td>
<td headers="id8e15_c1">60, rue des Écoles
<p>75005 Paris</p>
</td>
<td headers="id8e15_c2">Tél. : 01 40 51 89 09
<p>Fax : 01 40 46 09 33</p>
</td></tr>
<tr class="row_odd odd">
<td headers="id8e15_c0"><a href="http://www.iicparigi.esteri.it/IIC_Parigi" class="spip_out" rel="external">Institut culturel italien</a></td>
<td headers="id8e15_c1">50, rue de Varenne
<p>75007 Paris</p>
</td>
<td headers="id8e15_c2">Tél. 01 44 39 49 39
<p>Fax : 01 42 22 37 88</p>
</td></tr>
<tr class="row_even even">
<td headers="id8e15_c0"><a href="http://www.institutneerlandais.com/sluiting/" class="spip_out" rel="external">Institut culturel néerlandais</a></td>
<td headers="id8e15_c1"></td>
<td headers="id8e15_c2"></td></tr>
<tr class="row_odd odd">
<td headers="id8e15_c0"><a href="http://www.imarabe.org/" class="spip_out" rel="external">Institut du monde arabe</a></td>
<td headers="id8e15_c1">1, rue des Fossés Saint Bernard
<p>75005 Paris</p>
</td>
<td headers="id8e15_c2">Tél. 01 40 51 38 38
<p>Fax : 01 43 54 76 45</p>
</td></tr>
<tr class="row_even even">
<td headers="id8e15_c0"><a href="http://www.parizs.balassiintezet.hu/fr/" class="spip_out" rel="external">Institut hongrois</a></td>
<td headers="id8e15_c1">92, rue Bonaparte
<p>75006 Paris</p>
</td>
<td headers="id8e15_c2">Tél. 01 43 26 06 44
<p>Fax : 01 43 26 89 92</p>
</td></tr>
<tr class="row_odd odd">
<td headers="id8e15_c0"><strong>Institut kurde</strong></td>
<td headers="id8e15_c1">106, rue La Fayette
<p>75010 Paris</p>
</td>
<td headers="id8e15_c2">Tél. 01 48 24 64 64
<p>Fax : 01 48 24 64 66</p>
</td></tr>
<tr class="row_even even">
<td headers="id8e15_c0"><a href="http://www.institutpolonais.fr/#/menu" class="spip_out" rel="external">Institut polonais</a></td>
<td headers="id8e15_c1">31, rue Jean Goujon
<p>75008 Paris</p>
</td>
<td headers="id8e15_c2">Tél. 01 53 93 90 10
<p>Fax : 01 45 62 07 90</p>
</td></tr>
<tr class="row_odd odd">
<td headers="id8e15_c0"><a href="http://mal217.org/" class="spip_out" rel="external">Maison de l’Amérique Latine</a></td>
<td headers="id8e15_c1">217, boulevard Saint Germain
<p>75007 Paris</p>
</td>
<td headers="id8e15_c2">Tél. 01 49 54 75 00
<p>Fax : 01 45 49 06 33</p>
</td></tr>
<tr class="row_even even">
<td headers="id8e15_c0"><a href="http://www.mcjp.fr/" class="spip_out" rel="external">Maison de la Culture du Japon</a></td>
<td headers="id8e15_c1">101 bis, quai Branly
<p>75740 Paris cedex 15</p>
</td>
<td headers="id8e15_c2">Tél. 01 44 37 95 00
<p>Fax : 01 44 37 95 15</p>
</td></tr>
<tr class="row_odd odd">
<td headers="id8e15_c0"><a href="http://frankrig.um.dk/fr" class="spip_out" rel="external">Maison du Danemark</a></td>
<td headers="id8e15_c1">77, avenue Marceau
<p>75008 Paris</p>
</td>
<td headers="id8e15_c2">Tél. 01 44 31 21 21
<p>Fax : 01 44 31 21 88</p>
</td></tr>
<tr class="row_even even">
<td headers="id8e15_c0"><a href="http://www.diplomatie.gouv.fr/fr/" class="spip_out">Ministère des Affaires étrangères</a></td>
<td headers="id8e15_c1">27 rue de la Convention
<p>CS 91533 -75732 Paris cedex 15</p>
</td>
<td headers="id8e15_c2"></td></tr>
<tr class="row_odd odd">
<td headers="id8e15_c0"><a href="http://www.europarl.europa.eu/portal/fr" class="spip_out" rel="external">Parlement Européen</a></td>
<td headers="id8e15_c1">Bureau des Stages
<p>KAD 2 A007 L - 2929 Luxembourg</p>
</td>
<td headers="id8e15_c2">Tél. (352) 430 02 48 82</td></tr>
<tr class="row_even even">
<td headers="id8e15_c0"><a href="http://fr.chineseembassy.org/fra/zgzfg/zgsg/whc/bmjj/t398355.htm" class="spip_out" rel="external">Service culturel Ambassade de Chine</a></td>
<td headers="id8e15_c1">19, rue Van Loo
<p>75116 Paris</p>
</td>
<td headers="id8e15_c2">Tél. 01 53 92 28 00</td></tr>
<tr class="row_odd odd">
<td headers="id8e15_c0"><a href="http://www.canada-culture.org/accueil_home-fr.html" class="spip_out" rel="external">Service culturel canadien</a></td>
<td headers="id8e15_c1">5, rue de Constantine
<p>75007 Paris</p>
</td>
<td headers="id8e15_c2">Tél. 01 44 43 21 90
<p>Fax : 01 44 43 21 99</p>
</td></tr>
<tr class="row_even even">
<td headers="id8e15_c0"><a href="http://fr.unesco.org/" class="spip_out" rel="external">UNESCO</a></td>
<td headers="id8e15_c1">7, place Fontenoy
<p>75352 Paris</p>
</td>
<td headers="id8e15_c2">Tél. 01 45 68 10 00</td></tr>
<tr class="row_odd odd">
<td headers="id8e15_c0"><a href="http://www.braintrack.com/" class="spip_out" rel="external">Université du monde entier</a></td>
<td headers="id8e15_c1">Portail d’accès aux sites des Universités</td>
<td headers="id8e15_c2"></td></tr>
</tbody>
</table>
<p><i>Mise à jour : 10 février 2015</i></p>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/etudes-superieures/carnet-d-adresses/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
