# Recherche d’emploi

<h4 class="spip">Outils pour la recherche d’emploi</h4>
<h4 class="spip">Journaux</h4>
<p>Il existe plusieurs journaux locaux en vente dans les kiosques mais peu possèdent une rubrique dédiée aux offres et demandes d’emploi. Le plus répandu est le <a href="http://www.straitstimes.com/" class="spip_out" rel="external">Straits Times</a>.</p>
<p>Tous les samedis, un supplément emploi « Recruit » paraît avec les nouvelles offres de la semaine. Il s’agit du journal le plus utilisé pour les chercheurs d’emploi.</p>
<h4 class="spip">Sites internet</h4>
<p>Pour les annonces d`emploi locales ou internationales, plusieurs sites Internet sont en ligne et également répertoriés à la rubrique « HR Services – Recruitment Useful Links » du site de la <a href="http://fccsingapore.com/" class="spip_out" rel="external">Chambre de commerce française de Singapour</a>.</p>
<p>Par ailleurs, les sites Internet concernant l’emploi répertorient des offres à l’international.</p>
<p>Sur le site de l’<a href="http://www.aasingapore.com/" class="spip_out" rel="external">Association américaine</a>, vous trouverez à la rubrique « Career Resource Center  job listings » de nombreux liens vers des sites locaux proposant des offres ou demandes d’emplois.</p>
<h4 class="spip">Annuaires et réseaux </h4>
<h5 class="spip">Les filiales françaises</h5>
<p>La liste des filiales françaises est répertoriée dans l’annuaire de la Chambre de commerce française de Singapour. Il comprend toutes les sociétés francaises et étrangères membres de la Chambre. Il s’agit des <i>Corporate Members</i> (sociétés françaises dont une filiale est installée à Singapour), les <i>Individual Members</i> (Français travaillant dans une entreprise à Singapour, membre de la Chambre à titre personnel) et les <i>Associate Members </i> (sociétés étrangères membres de la chambre).</p>
<p>Ce guide est disponible à la Chambre de commerce de Singapour (541 orchard Road, #09-01 Liat Towers, Singapore 238881) au tarif de 42 SGD pour les membres de la Chambre et 84 SGD pour les non membres.</p>
<p>Il est par ailleurs possible de le commander sur Internet <a href="http://fccsingapore.com/" class="spip_out" rel="external">FCCSingapore.com</a> (Publications – FCCS Directory) et de le recevoir en France au prix de 150 SGD.</p>
<p>Cet annuaire offre une page par entreprise, avec un descriptif de la société et de ses activités, ainsi que sa localisation et les adresses des personnes à contacter.</p>
<h5 class="spip">Les entreprises locales</h5>
<p>La <a href="http://www.eurocham.org.sg/" class="spip_out" rel="external">Chambre européenne de commerce</a> est également installée à Singapour. Il suffit de la contacter par <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/emploi-stage/article/recherche-d-emploi#admin#mc#eurocham.org.sg#" title="admin..åt..eurocham.org.sg" onclick="location.href=mc_lancerlien('admin','eurocham.org.sg'); return false;" class="spip_mail">courriel</a> pour avoir accès à son annuaire regroupant toutes les entreprises membres.</p>
<p><strong>European Chamber of Commerce (Singapore)</strong><br class="manualbr">350 Orchard Road<br class="manualbr">#19-06 Shaw House<br class="manualbr">Singapore 238868<br class="manualbr">Tel. : +65 6836 6681<br class="manualbr">Fax : +65 6737 3660<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/emploi-stage/article/recherche-d-emploi#info#mc#eurocham.org.sg#" title="info..åt..eurocham.org.sg" onclick="location.href=mc_lancerlien('info','eurocham.org.sg'); return false;" class="spip_mail">Courriel</a></p>
<p>Les informations concernant les autres chambres de commerce européennes sont aussi disponibles sur ce site (rubriques « Resources » – « European Business Groups »). Vous trouverez les adresses Internet des autres chambres de commerce des pays d’Europe. Sur certaines d’entre elles, il est possible de mettre en ligne son CV, de consulter des offres d’emploi et l’annuaire des entreprises membres.</p>
<p>La <a href="http://www.austcham.org.sg/" class="spip_out" rel="external">Chambre australienne de commerce de Singapour</a> présente en ligne son « Business Directory », guide où sont répertoriés toutes les entreprises installées à Singapour et membres de la Chambre ainsi que leurs contacts.</p>
<p>Il existe par ailleurs <a href="http://www.sicc.com.sg/" class="spip_out" rel="external">The Singapore International Chamber of Commerce</a>. Cette structure n’offre pas de listes d’entreprises installées sur Singapour, ni en ligne ni sous forme de brochure. En revanche, elle propose de commander en ligne des guides comme, « le coût de la vie pour les expatriés à Singapour », « le guide de l’investisseur à Singapour », etc.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/emploi-stage/article/recherche-d-emploi). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
