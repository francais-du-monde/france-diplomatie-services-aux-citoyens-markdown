# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/emploi-stage/article/reglementation-du-travail-113440#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/emploi-stage/article/reglementation-du-travail-113440#sommaire_2">Conventions collectives de travail</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/emploi-stage/article/reglementation-du-travail-113440#sommaire_3">Durée légale du travail</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/emploi-stage/article/reglementation-du-travail-113440#sommaire_4">Congés</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/emploi-stage/article/reglementation-du-travail-113440#sommaire_5">Rémunération</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/emploi-stage/article/reglementation-du-travail-113440#sommaire_6">Rupture du contrat de travail</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/emploi-stage/article/reglementation-du-travail-113440#sommaire_7">Contrat de travail – spécificités</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/emploi-stage/article/reglementation-du-travail-113440#sommaire_8">Les différents types de contrat de travail</a></li>
<li><a id="so_9" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/emploi-stage/article/reglementation-du-travail-113440#sommaire_9">Modification du contrat de travail</a></li>
<li><a id="so_10" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/emploi-stage/article/reglementation-du-travail-113440#sommaire_10">Suspension du contrat de travail</a></li>
<li><a id="so_11" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/emploi-stage/article/reglementation-du-travail-113440#sommaire_11">Fêtes légales</a></li>
<li><a id="so_12" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/emploi-stage/article/reglementation-du-travail-113440#sommaire_12">Emploi du conjoint</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<p>Le droit du travail est régi par le code du Travail (loi n°028-2008/AN) en date du 13 mai 2008. Ce texte peut être consulté sur le site Internet de la <a href="http://www.legiburkina.bf/" class="spip_out" rel="external">Banque de données juridiques du Burkina Faso</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Conventions collectives de travail</h3>
<p>Des conventions collectives de travail peuvent être conclues entre, d’une part, les représentants d’un ou de plusieurs syndicats ou groupements professionnels de travailleurs et, d’autre part, une ou plusieurs organisations syndicales d’employeurs ou tous autres groupements d’employeurs ou un ou plusieurs employeurs pris individuellement.</p>
<p>Ces conventions peuvent contenir des clauses plus favorables aux travailleurs que celles prévues par les lois et règlements en vigueur. Elles peuvent s’appliquer au niveau national ou local.</p>
<p>Ces conventions sont passées pour une durée déterminée et contiennent, entre autres, des dispositions relatives aux salaires applicables par catégorie professionnelle, à la durée de l’engagement à l’essai et à celle du préavis, aux congés payés, aux primes d’ancienneté et à la formation continue.</p>
<h3 class="spip"><a id="sommaire_3"></a>Durée légale du travail</h3>
<p>La durée légale du travail est fixée à <strong>40 heures par semaine.</strong></p>
<p>Les heures effectuées au-delà de la durée légale sont considérées comme heures supplémentaires et donnent lieu à une majoration de salaire.</p>
<h3 class="spip"><a id="sommaire_4"></a>Congés</h3>
<p>Le repos hebdomadaire est obligatoire. Sa durée minimale est de 24 heures consécutives. Le jour de repos est en principe fixé le dimanche.</p>
<p>Le droit à congé est ouvert après une période minimale de service effectif de 12 mois. Le droit à congé payé est de deux jours et demi calendaires par mois de service effectif, soit, par an, 30 jours calendaires. Mais des conditions plus favorables peuvent être prévues dans la convention collective de travail ou le contrat de travail.</p>
<p>Cette durée de congé peut être augmentée de deux jours ouvrables après 20 ans de services continu ou non dans la même entreprise, de quatre jours après 25 ans de services et de six jours après 30 ans de services.</p>
<p>Des congés exceptionnels, dans la limite de 10 jours ouvrables par an, peuvent être accordés par l’employeur en cas d’évènements familiaux touchant directement le travailleur.</p>
<p>Un congé sans solde d’une durée de six mois, renouvelable une fois, peut être accordé par l’employeur pour l’entretien d’un enfant.</p>
<p>Le congé de maternité est de 14 semaines. Ce congé débute entre huit et quatre semaines avant la date présumée de l’accouchement. Sauf exception, le congé de maternité ne peut excéder 10 semaines à compter de la date de l’accouchement.</p>
<h3 class="spip"><a id="sommaire_5"></a>Rémunération</h3>
<p>A défaut de convention collective ou si celle-ci ne contient aucune disposition en matière de rémunération, le salaire est fixé d’un commun accord entre l’employeur et le travailleur.</p>
<p>Des décrets pris en Conseil des ministres fixent les salaires minima interprofessionnels garantis et, à défaut de convention collective, les salaires minima par catégories professionnelles.</p>
<p>Le salaire est payé en monnaie ayant cours légal au Burkina Faso. Il est versé tous les mois et au plus tard huit jours après la fin du mois qui donne droit à salaire. Un bulletin de paye doit être remis au salarié.</p>
<p>Le salaire comprend le salaire de base, l’allocation de congé payé, les primes, les indemnités et les prestations de toute nature.</p>
<h3 class="spip"><a id="sommaire_6"></a>Rupture du contrat de travail</h3>
<p>Le contrat de travail peut être rompu dans les cas suivants :</p>
<ul class="spip">
<li>d’un commun accord entre les parties ;</li>
<li>lors de la cessation d’activités de l’entreprise ;</li>
<li>l’annulation légale du contrat de travail ;</li>
<li>l’arrivée du terme du contrat à durée déterminée ;</li>
<li>la démission ;</li>
<li>le licenciement ;</li>
<li>le départ à la retraite ;</li>
<li>l’incapacité permanente totale de travail telle que définie par la réglementation ;</li>
<li>le décès du travailleur. En cas de rupture d’un contrat de travail à durée indéterminée, la durée du préavis est fixée à huit jours pour les travailleurs dont le salaire est fixé à l’heure ou à la journée, un mois pour les employés et à trois mois pour les agents de maîtrise, les cadres, les techniciens et assimilés.</li></ul>
<p>A l’expiration du contrat de travail, l’employeur est tenu de délivrer au travailleur un certificat de travail indiquant les dates de début et de fin d’emploi, ainsi que la nature et les dates des emplois successivement occupés. Ce certificat est exempt de tous droits de timbre et d’enregistrement.</p>
<h3 class="spip"><a id="sommaire_7"></a>Contrat de travail – spécificités</h3>
<p>Le contrat de travail peut être passé par écrit ou verbalement.</p>
<h3 class="spip"><a id="sommaire_8"></a>Les différents types de contrat de travail</h3>
<p><strong>Contrat de travail à l’essai</strong></p>
<p>Il doit être passé par écrit et conclu pour une durée limitée qui est fonction du type d’emploi. La durée maximale de la période d’essai est ainsi d’un mois pour les employés et de 3 mois pour les cadres, les agents de maîtrise, les techniciens et assimilés.</p>
<p>La période d’essai peut être renouvelée une seule fois et pour la même durée.</p>
<p>L’engagement à l’essai peut cesser à tout moment, sans préavis, ni indemnité, par la volonté de l’une ou l’autre des parties, sauf dispositions particulières prévues expressément dans le contrat.</p>
<p><strong>Contrat de travail à temps partiel</strong></p>
<p>Ce type de contrat est conclu lorsque la durée d’exécution du contrat est inférieure à la durée hebdomadaire légale.</p>
<p>Il peut s’agir d’un contrat à durée déterminée ou indéterminée.</p>
<p><strong>Contrat de travail à durée déterminée</strong></p>
<p>Le terme du contrat est précisé à l’avance par les deux parties. Il est renouvelable et est conclu par écrit.</p>
<p>Pour les personnes ne possédant pas la nationalité burkinabée, ce type de contrat ne peut être conclu pour une durée supérieure à trois ans et doit être visé et enregistré par l’inspection du travail compétente. Ces formalités sont effectuées par l’employeur au plus tard 30 jours après le début de l’exécution du contrat de travail.</p>
<p>Il ne peut être mis fin avant terme à un contrat de travail à durée déterminée qu’en cas d’accord des parties constaté par écrit, de force majeure ou de faute grave.</p>
<p><strong>Contrat de travail à durée indéterminée</strong></p>
<p>Le terme du contrat n’est pas précisé.</p>
<p>Pour les personnes ne possédant pas la nationalité burkinabée, ce type de contrat doit être visé par les services compétents du ministère burkinabé du Travail.</p>
<p>Il peut y être mis fin par la volonté d’une des parties, mais la rupture du contrat est subordonnée à un préavis (sauf en cas de faute lourde) notifié par écrit par la partie qui souhaite mettre fin au contrat. Le motif de la rupture du contrat doit être indiqué dans le préavis. La durée du délai de préavis est de huit jours pour les travailleurs dont le salaire est fixé à l’heure ou à la journée, d’un mois pour les employés et de trois mois pour les cadres, les agents de maîtrise, les techniciens et assimilés.</p>
<p>En cas de licenciement abusif ou de rupture irrégulière du contrat de travail, des dommages et intérêts peuvent être réclamés en saisissant le tribunal du travail compétent.</p>
<h3 class="spip"><a id="sommaire_9"></a>Modification du contrat de travail</h3>
<p>Toute modification substantielle du contrat de travail doit être faite par écrit et approuvée par le travailleur.</p>
<h3 class="spip"><a id="sommaire_10"></a>Suspension du contrat de travail</h3>
<p>Il existe de nombreuses causes de suspension du contrat de travail. Celui-ci peut être suspendu pour une durée maximale d’un an lorsque le travailleur est absent pour raison de maladie ou d’accident non professionnel constaté par un certificat médical. Dans ce cas, la durée et le montant de l’indemnisation du travailleur pendant son absence sont fonction de son ancienneté dans l’entreprise.</p>
<p>Le contrat peut être également suspendu lorsque le travailleur est indisponible suite à un accident du travail ou une maladie professionnelle ou en cas de congé maternité.</p>
<p><strong>Pour en savoir plus</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Site Internet de la <a href="http://www.legiburkina.bf/" class="spip_out" rel="external">Banque de données juridiques du Burkina</a> / rubrique Code du travail / titre III / chapitres II à VIII. </p>
<h3 class="spip"><a id="sommaire_11"></a>Fêtes légales</h3>
<ul class="spip">
<li>1er janvier, (Merci de préciser les noms des fêtes citées) Jour de l’an</li>
<li>3 janvier, Soulèvement populaire du 3 janvier 1966</li>
<li>8 mars, Journée internationale de la femme</li>
<li>1er mai, Fête du travail</li>
<li>5 août, Proclamation de l’indépendance</li>
<li>11 décembre (fête nationale) Célébration de l’indépendance</li></ul>
<p><strong>Fêtes religieuses à dates mobiles ou fixes : </strong></p>
<ul class="spip">
<li>Lundi de Pâques,</li>
<li>Assomption, 15 août,</li>
<li>Toussaint, 1er novembre</li>
<li>Noël, 25 décembre</li>
<li>Mouloud,</li>
<li>Ramadan,</li>
<li>Tabaski - Aïd el kébir.</li></ul>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Voir aussi : <a href="http://www.tv5.org/TV5Site/voyageurs/accueil.php" class="spip_out" rel="external">TV5.org</a> </p>
<h3 class="spip"><a id="sommaire_12"></a>Emploi du conjoint</h3>
<p>La législation locale autorise les conjoints à exercer une activité professionnelle. En matière d’emploi et de traitement, toute discrimination est interdite. A conditions égales de travail, de qualifications professionnelles et de rendement, le salaire est égal pour tous les travailleurs quels que soient leur origine, leur sexe, leur âge et leur statut (article 182 du code du travail).</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/emploi-stage/article/reglementation-du-travail-113440). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
