# Lettre de motivation

<p><strong>Rédaction</strong></p>
<p>La lettre de motivation accompagne, introduit et personnalise le curriculum-vitae mais n’est pas une répétition de ce dernier. Elle a pour but de retenir l’attention du lecteur, de le séduire et d’obtenir un entretien. Elle ne doit donc en aucun cas être négligée et sera rédigée avec pragmatisme.</p>
<p>En Suisse, la lettre de motivation est standardisée et suit un canevas auquel les recruteurs sont habitués.</p>
<p><strong>Contenu de la lettre de motivation</strong></p>
<p>La manière de communiquer à l’écrit donnera des indices sur la compréhension et l’idée que vous avez de la Suisse et de sa culture.</p>
<ul class="spip">
<li>Elle est personnalisée et s’adresse à un interlocuteur précis dont il est essentiel de connaître l’identité et la fonction ;</li>
<li>Elle sera manuscrite (pour autant que l’annonce le précise) ou dactylographiée en prenant soin d’utiliser un caractère sobre et absent de fantaisie. Elle pourra être envoyée par courriel ou par voie postale selon les instructions de l’offre. En Suisse, certains recruteurs procèdent à un test de graphologie ;</li>
<li>En cas de réponse à une offre d’emploi, il est préférable d’y faire aussitôt référence dès l’introduction puis d’enchaîner avec les motivations qui vous ont amenées à envoyer votre candidature ;</li>
<li>Ensuite, vous aurez la possibilité de vous présenter et de donner des indications concrètes sur vos parcours professionnels (sans oublier vos fonctions et réalisations) et académiques tout en gardant en tête l’objet de votre lettre : décrocher un entretien.</li></ul>
<p><strong>Conseils pour réussir votre lettre de motivation</strong></p>
<ul class="spip">
<li>La lettre est unique et concerne une offre d’emploi précise. Ne pas la photocopier ;</li>
<li>La qualité rédactionnelle est capitale : vérifiez l’orthographe et la syntaxe ;</li>
<li>Privilégier les phrases synthétiques et directes qui dynamisent et facilitent la lecture ;</li>
<li>Attention à la paraphrase et aux explications " farfelues " ;</li>
<li>Faire quelques recherches sur l’entreprise pour laquelle vous postulez ;</li>
<li>Eviter les banalités : il est par exemple inutile d’écrire que vous envoyez votre candidature pour le poste…. ;</li>
<li>Il est toujours conseillé de développer une idée par paragraphe ;</li>
<li>La lettre est nette et présentée sur une feuille format A4 ;</li>
<li>Elle n’occupe qu’un recto de page ;</li>
<li>Son pliage doit être soigné ;</li>
<li>Il est possible que la lettre de motivation soit exigée en anglais. Il faut bien évaluer vos capacités linguistiques. La bonne maîtrise d’une langue étrangère n’est pas uniquement orale. Il est donc inutile de tricher.</li></ul>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span> <a href="http://www.travailler-en-suisse.ch/" class="spip_out" rel="external">Site internet du guide pratique pour les résidents et les frontaliers</a></p>
<p><strong>Modèle de lettre de motivation</strong></p>
<p>Elle comprend les volets suivants :</p>
<ul class="spip">
<li>En haut à gauche - Identification de l’expéditeur
<br>— Nom et prénom
<br>— Adresse complète
<br>— Pays
<br>— Téléphone fixe et/ou portable (sans oublier le préfixe international)
<br>— En haut à droite (légèrement en contrebas)
<br>— Identification du destinataire
<br>— Nom et prénom : il est essentiel de connaître exactement le nom et la fonction du destinataire notamment en cas de candidature spontanée. En cas de doute, ne pas hésiter à appeler l’entreprise.
<br>— Fonction
<br>— Nom de l’entreprise
<br>— Adresse complète
<br>— Pays</li>
<li>En dessous à droite
<br>— Lieu et date y compris l’année</li>
<li>En dessous à gauche
<br>— Objet de la lettre</li>
<li>En contrebas centré
<br>— Corps de la lettre et formule de politesse</li>
<li>En bas à droite
<br>— Prénom et nom et signature</li>
<li>En bas à gauche
<br>— Mention des pièces jointes. Toutefois, il est possible d’indiquer directement dans le corps de la lettre la ou les pièces jointes.</li></ul>
<p><i>Mise à jour : février 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/emploi-stage/article/lettre-de-motivation-105247). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
