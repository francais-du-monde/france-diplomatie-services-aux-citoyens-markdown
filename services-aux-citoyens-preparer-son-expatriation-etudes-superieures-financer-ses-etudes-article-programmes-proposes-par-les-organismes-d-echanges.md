# Programmes proposés par les organismes d’échanges

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/etudes-superieures/financer-ses-etudes/article/programmes-proposes-par-les-organismes-d-echanges#sommaire_1">I. Programmes proposés par le British Council</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/etudes-superieures/financer-ses-etudes/article/programmes-proposes-par-les-organismes-d-echanges#sommaire_2">II. Office allemand d’échanges universitaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/etudes-superieures/financer-ses-etudes/article/programmes-proposes-par-les-organismes-d-echanges#sommaire_3">III. Programmes proposés par l’Office Franco-Allemand pour la Jeunesse (OFAJ)</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/etudes-superieures/financer-ses-etudes/article/programmes-proposes-par-les-organismes-d-echanges#sommaire_4">IV. Programmes proposés par la commission franco-américaine d’échanges universitaires et culturels</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>I. Programmes proposés par le British Council</h3>
<p><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> <strong>Bourses Entente Cordiale</strong></p>
<ul class="spip">
<li><strong>Public concerné</strong> : étudiants et jeunes chercheurs d’un niveau « postgraduate » (bac + et supérieur) qui souhaitent effectuer une année d’études ou de recherche au Royaume-Uni indépendamment de leur diplôme français.</li>
<li><strong>Disciplines</strong> : toutes</li>
<li><strong>Dépôt des dossiers</strong> : retrait sur le site Internet à partir novembre et dépôt au plus tard au mois d’avril de chaque année. Une première sélection a lieu en mai et des entretiens. La décision finale est prise au mois de juin.</li></ul>
<blockquote class="texteencadre-spip spip"><strong>Où s’adresser ?</strong> <br class="autobr">
<a href="http://www.britishcouncil.org/fr/France-education-scholarships-entente-cordiale.htm" class="spip_out" rel="external">British Council</a><br class="autobr">
Équipe programmes <br class="autobr">
9, rue de Constantine <br class="autobr">
75340 Paris cedex 07 <br class="autobr">
Tél. 01 49 55 73 00 <br class="autobr">
Fax : 01 47 05 77 02 <br class="autobr">
Site : [<a href="http://www.britishcouncil.org/fr/France-education-scholarships-entente-cordiale.htm" class="spip_url spip_out auto" rel="nofollow external">www.britishcouncil.org/fr/France-education-scholarships-entente-cordiale.htm</a><br class="autobr">
Courriel : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/etudes-superieures/financer-ses-etudes/article/programmes-proposes-par-les-organismes-d-echanges#ententecordiale#mc#britishcouncil.fr#" title="ententecordiale..åt..britishcouncil.fr" onclick="location.href=mc_lancerlien('ententecordiale','britishcouncil.fr'); return false;" class="spip_mail">ententecordiale<span class="spancrypt"> [at] </span>britishcouncil.fr</a></blockquote>
<h3 class="spip"><a id="sommaire_2"></a>II. Office allemand d’échanges universitaires</h3>
<p><i>Deutscher akademischer Austauschdienst (DAAD)</i></p>
<p><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> <strong>Bourses d’études ou de recherche de longue durée</strong></p>
<ul class="spip">
<li><strong>Public concerné</strong> : étudiants en troisième cycle des universités, diplômés des grandes écoles ou post-doctorants souhaitant effectuer une période d’études dans une université allemande. Âge limite : 32 ans Le dernier diplôme obtenu ne doit pas dater de plus de 6 ans (2 ans maximum pour la thèse dans le cas d’un post-doctorat).</li>
<li><strong>Disciplines</strong> : toutes (à l’exclusion des disciplines artistiques).</li>
<li><strong>Durée de la bourse : </strong>7 à 10 mois, renouvelables sous condition.</li>
<li><strong>Dépôt des dossiers</strong> : le 31 janvier pour un démarrage de la bourse au 1er octobre de chaque année.</li></ul>
<p><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> <strong>Bourses d’études de longue durée pour artistes et architectes</strong></p>
<ul class="spip">
<li><strong>Public concerné</strong> : : titulaires d’un diplôme de fin d’études en spécialité artistique (arts plastiques, design, danse, chorégraphie) ou architecture. Le dernier diplôme obtenu ne doit pas dater de plus de 6 ans.</li>
<li><strong>Durée de la bourse :</strong> 7 à 10 mois, renouvelables sous condition.</li>
<li><strong>Dépôt des dossiers</strong> : le 1er novembre de l’année précédant celle du départ. Les bourses démarrent obligatoirement au 1er octobre de chaque année.</li></ul>
<p><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> <strong>Bourses de recherche de courte durée</strong></p>
<ul class="spip">
<li><strong>Public concerné</strong> : titulaires d’un diplôme de niveau Master 1 minimum et doctorants souhaitant effectuer en Allemagne des recherches nécessaires à la réalisation d’une thèse de doctorat ; au moment de la candidature, l’étudiant doit avoir pris contact avec l’institution d’accueil en Allemagne. Âge limite : 32 ans. Le dernier diplôme obtenu ne doit pas dater de plus de 6 ans (4 ans maximum dans le cas d’une candidature pour des recherches doctorales).</li>
<li><strong>Durée de la bourse : </strong> 1 à 6 mois, non renouvelables, non prolongeables.</li>
<li><strong>Dépôt des dossiers</strong> :
31 janvier pour un séjour dont le départ est compris entre le 1er juillet et le 1er décembre. 15 septembre pour un séjour dont le départ est compris entre le 1er janvier et le 1er juin.</li></ul>
<p><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> <strong>Formation d’experts franco-allemands</strong></p>
<ul class="spip">
<li><strong>Public concerné</strong> : Étudiants germanistes LCE/LEA (allemand/anglais) inscrits en 2ème année de Licence.</li>
<li><strong>Dépôt des dossiers</strong> : au plus tard le 15 mai de l’année du séjour .</li></ul>
<p><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> <strong>Bourses pour un cours de langue en Allemagne</strong></p>
<ul class="spip">
<li><strong>Public concerné</strong> : être inscrit dans un établissement supérieur, avoir un niveau Licence 2 minimum. Etre âgé de plus de dix-huit ans. Le dernier diplôme obtenu ne doit pas dater de plus de 6 ans.</li>
<li><strong>Dépôt des dossiers</strong> : au plus tard le 15 novembre de l’année précédent l’année du départ.</li></ul>
<p><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> <strong>Stages d’enseignement en Allemagne pour les professeurs stagiaires</strong></p>
<ul class="spip">
<li>Public concerné : être admis aux épreuves théoriques du CAPES ou à l’agrégation d’histoire /géographie  ou avoir été titularisé comme professeur d’histoire / géographie  en 2012 ou 2011</li>
<li>Dépôt des dossiers : 15 octobre 2012 un séjour prévu du 1er janvier au 31 mars 2013.</li></ul>
<blockquote class="texteencadre-spip spip"><br class="autobr">
<strong>Où s’adresser ?</strong>
<p>DAAD - Office allemand d’échanges universitaires <br class="autobr">8, rue du Parc-Royal <br class="autobr">75003 Paris <br class="autobr">Tél. 01 44 17 02 00 <br class="autobr">Fax : 01 44 17 02 31 <br class="autobr">Site : <a href="http://paris.daad.de/" class="spip_out" rel="external">http://paris.daad.de</a><br class="autobr"></p>
</blockquote>
<h3 class="spip"><a id="sommaire_3"></a>III. Programmes proposés par l’Office Franco-Allemand pour la Jeunesse (OFAJ)</h3>
<p><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> <strong>Bourses de recherche sur un thème franco-allemand</strong></p>
<ul class="spip">
<li><strong>Public concerné</strong> : étudiants âgés de moins de 30 ans</li>
<li><strong>Disciplines</strong> : toute discipline.</li>
<li><strong>Dépôt des dossiers</strong> : 1 mois avant le début du séjour</li>
<li>Voir <a href="http://www.ofaj.org/" class="spip_out" rel="external">http://www.ofaj.org/</a></li></ul>
<p><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> <strong>Bourses de recherches documentaires</strong></p>
<ul class="spip">
<li>Public concerné : étudiants âgés de moins de 30 ans</li>
<li>Disciplines : toute discipline</li>
<li>Dépôt des dossiers : 1 mois avant le début du séjour</li>
<li><a href="http://www.ofaj.org/recherches-documentaires" class="spip_out" rel="external">www.ofaj.org/recherches-documentaires</a></li></ul>
<p><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> <strong>Bourses pour séjour d’études en Allemagne pour étudiants des écoles d’art et conservatoires</strong></p>
<ul class="spip">
<li>Public concerné : étudiants âgés de moins de 30 ans</li>
<li>Disciplines : toute discipline</li>
<li>Dépôt des dossiers : 15 juillet de chaque année pour le semestre d’hiver, 15 décembre de chaque année pour le semestre d’été</li>
<li><a href="http://www.ofaj.org/sejours-d-etudes" class="spip_out" rel="external">www.ofaj.org/sejours-d-etudes</a></li></ul>
<p><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> <strong>Bourses pour stages pratiques</strong></p>
<ul class="spip">
<li><strong>Public concerné</strong> : étudiants âgés de moins de 30 ans.</li>
<li><strong>Disciplines</strong> : toutes.</li>
<li><strong>Dépôt des dossiers</strong> : 1 mois avant le début du stage</li>
<li><a href="http://www.ofaj.org/stages-pratiques-pour-etudiants" class="spip_out" rel="external">www.ofaj.org/stages-pratiques</a></li></ul>
<p><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> <strong>Bourses linguistiques pour cours intensifs d’allemand, bourses « Apprendre l’allemand »</strong></p>
<ul class="spip">
<li><strong>Public concerné</strong> : étudiants de 18 à 30 ans préparant un séjour d’études ou un séjour professionnel, jeunes professionnels et animateurs d’associations de jeunesse.</li>
<li><strong>Dépôt des dossiers</strong> : les candidats doivent adresser leur demande de bourse à l’OFAJ deux mois avant le début du cours.</li></ul>
<blockquote class="texteencadre-spip spip"><br class="autobr">
<strong>Où s’adresser pour les bourses linguistiques ?</strong>
<p>Office franco-allemand pour la jeunesse<br class="autobr">Secteur linguistique<br class="autobr"> Molkenmarkt 1<br class="autobr"> D-1079 BERLIN<br class="autobr"> Tél. + 49 (0) 30 288 757 11<br class="autobr"> Site : <a href="http://www.ofaj.org/" class="spip_out" rel="external">www.ofaj.org</a><br class="autobr"> Courriel : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/etudes-superieures/financer-ses-etudes/article/programmes-proposes-par-les-organismes-d-echanges#info#mc#ofaj.org#" title="info..åt..ofaj.org" onclick="location.href=mc_lancerlien('info','ofaj.org'); return false;" class="spip_mail">info<span class="spancrypt"> [at] </span>ofaj.org</a><br class="autobr"></p>
</blockquote><blockquote class="texteencadre-spip spip"><br class="autobr">
<strong>Pour les programmes de l’OFAJ autres que les bourses linguistiques :</strong>
<p>Office franco-allemand pour la jeunesse <br class="autobr">51 rue de l’Amiral-Mouchez <br class="autobr">75013 Paris <br class="autobr">Tél. 01 40 78 18 18 <br class="autobr">Fax : 01 40 78 18 88 <br class="autobr">Site : <a href="http://www.ofaj.org/" class="spip_out" rel="external">www.ofaj.org</a><br class="autobr">Courriel : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/etudes-superieures/financer-ses-etudes/article/programmes-proposes-par-les-organismes-d-echanges#info#mc#ofaj.org#" title="info..åt..ofaj.org" onclick="location.href=mc_lancerlien('info','ofaj.org'); return false;" class="spip_mail">info<span class="spancrypt"> [at] </span>ofaj.org</a><br class="autobr"></p>
</blockquote>
<h3 class="spip"><a id="sommaire_4"></a>IV. Programmes proposés par la commission franco-américaine d’échanges universitaires et culturels</h3>
<p><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> <strong>Bourses Fulbright</strong></p>
<p><strong>1. Bourse de recherche</strong></p>
<ul class="spip">
<li><strong>Public concerné :</strong> chercheurs français ou ressortissants de l’Union Européenne titulaires d’un doctorat, d’une agrégation, posséder une habilitation à a diriger des recherches, être professeur d’université ; les candidats non-universitaires doivent posséder un diplôme professionnel et avoir exercé une activité pendant 5 ans. Les candidats doivent être officiellement invités par une université ou un centre de recherche américain et posséder un bon niveau d’anglais.</li>
<li><strong> Disciplines :</strong> arts, lettres, sciences humaines et sociales (priorité sera donné aux projets portant sur les USA), sciences exactes (programmes pluridisciplinaires et régionaux)</li>
<li><strong> Dépôt des dossiers :</strong> 1er février de chaque année</li></ul>
<p><strong>2. Bourses d’entretien pour études</strong></p>
<ul class="spip">
<li><strong> Public concerné :</strong><br class="manualbr">1. étudiants français ayant validé au minimum trois années d’études au moment du dépôt du dossier, ayant une très bonne connaissance de l’anglais et un projet d’études nécessitant une année complémentaire aux États-Unis. <br class="manualbr">2. Doctorants inscrits en thèse dans un laboratoire français ou en co-tutelle ou en co-direction, et souhaitant effectuer 2 à 9 mois de recherche dans une université américaine.</li>
<li><strong>Disciplines :</strong><br class="manualbr">Étudiants : tous domaines sauf sciences exactes, médecine, ingénierie<br class="manualbr">Doctorants : tous domaines</li>
<li><strong>Dépôt des dossiers :</strong> 1er février de chaque année</li></ul>
<p>La Commission franco-américaine gère également un programme pour des assistants de français aux États-Unis, des programmes d’été pour étudiants en 1ère et 2ème année, des programmes d’échanges entre professeurs de langue vivante, etc. Pour avoir la liste complète des programmes de bourses gérés par la Commission franco-américaine, veuillez consulter le site : <a href="http://www.fulbright-france.org/htm/page.php?id=81" class="spip_out" rel="external">www.fulbright-france.org, rubrique « Bourses de la Commission »</a>.</p>
<br class="autobr">
<strong>Où s’adresser ?</strong>
<p>Commission franco-américaine d’échanges universitaires et culturels <br class="autobr">9 rue Chardin <br class="autobr">75016 Paris <br class="autobr">Tél. 01 44 14 53 60 - 08 92 68 07 47 <br class="autobr">Fax : 01 42 88 04 79 <br class="autobr">Site : <a href="http://www.fulbright-france.org/" class="spip_out" rel="external">www.fulbright-france.org</a> <br class="autobr">Courriel : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/etudes-superieures/financer-ses-etudes/article/programmes-proposes-par-les-organismes-d-echanges#speyrichou#mc#fulbright-france.org#" title="speyrichou..åt..fulbright-france.org" onclick="location.href=mc_lancerlien('speyrichou','fulbright-france.org'); return false;" class="spip_mail">speyrichou<span class="spancrypt"> [at] </span>fulbright-france.org</a><br class="autobr"></p>
<p><i>Mise à jour : février 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/etudes-superieures/financer-ses-etudes/article/programmes-proposes-par-les-organismes-d-echanges). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
