# Questions / réponses Etat-civil

<p class="texte texte120167">

    Consultez la rubrique <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/questions-reponses/article/120153" class="spip_in">questions / réponses</a> du Service Central d’Etat-Civil
</p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/actualites/article/questions-reponses-etat-civil). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
