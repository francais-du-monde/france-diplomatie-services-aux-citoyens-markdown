# Communications

<h4 class="spip">Téléphone – Internet</h4>
<p>Les liaisons téléphoniques sont bonnes.</p>
<p>L’’indicatif téléphonique de la Pologne est le 48.</p>
<p>Pour téléphoner depuis la France en Pologne, il convient de composer le : 00 + 48 + indicatif de la ville sans le 0 + numéro de votre correspondant. Par exemple pour téléphoner à Varsovie : 00 + 48 + 22 + 856 24 33. L’indicatif de Varsovie est le 022, celui de Cracovie le 012.</p>
<p>Pour téléphoner depuis la Pologne à l’étranger, composez le : 00 + code pays + indicatif régional sans le zéro + numéro de votre correspondant.</p>
<p><strong>Téléphoner gratuitement par Internet </strong></p>
<p>Il est généralement très avantageux d’utiliser <strong>des logiciels de téléphonie sur IP</strong> (Skype, Google talk, Live messenger, Yahoo ! messenger, etc.) car les tarifs proposés à l’international sont souvent moindre que ceux des opérateurs locaux exploitant des lignes classiques, voire gratuits dans certaines conditions.</p>
<h4 class="spip">Poste</h4>
<p>Le délai d’acheminement du courrier est de trois à quatre jours environ de Varsovie, une semaine depuis Cracovie. Un colis peut nécessiter un mois. La bonne réception n’est pas garantie.</p>
<p><i>Mise à jour : juin 2014 </i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/vie-pratique/article/communications). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
