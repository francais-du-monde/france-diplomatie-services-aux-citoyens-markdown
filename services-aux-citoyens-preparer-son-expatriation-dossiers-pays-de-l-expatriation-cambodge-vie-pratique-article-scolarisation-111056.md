# Scolarisation

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/cambodge/vie-pratique/article/scolarisation-111056#sommaire_1">Les établissements scolaires français au Cambodge</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/cambodge/vie-pratique/article/scolarisation-111056#sommaire_2">Enseignement supérieur</a></li></ul>
<p>Pour toute information sur la scolarisation dans le système français à l’étranger, vous pouvez consulter notre thématique sur <a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures.md" class="spip_in">Etudes supérieures</a> et la <a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais.md" class="spip_in">Scolarité en français</a>.</p>
<p>Vous y trouverez des renseignements sur :</p>
<ul class="spip">
<li>les établissements français du primaire et du secondaire à l’étranger ;</li>
<li>les bourses scolaires et la prise en charge des frais de scolarité à l’étranger ;</li>
<li>les possibilités qui s’offrent à vous si votre enfant ne peut être scolarisé à l’étranger dans le système français (enseignement à distance par le CNED, programme français langue maternelle (FLAM), internats en France) ;</li>
<li>les épreuves du baccalauréat à l’étranger ;</li>
<li>les bourses d’études supérieures en France et à l’étranger ;</li>
<li>l’équivalence des diplômes.</li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Les établissements scolaires français au Cambodge</h3>
<ul class="spip">
<li><a href="http://www.descartes-cambodge.com/" class="spip_out" rel="external">Le lycée français de Phnom Penh René Descartes (LFRD)</a> : conventionné</li>
<li><a href="http://www.ecolefr-siemreap.org/html/" class="spip_out" rel="external">L’école française de Siem Reap (EFSR)</a> : homologuée</li>
<li><a href="http://www.ecolefrancaisedesihanoukville.org/" class="spip_out" rel="external">L’école française de Sihanoukville (EFS)</a> : homologuée</li>
<li><a href="http://www.ecolefrancaisedebattambang.org/" class="spip_out" rel="external">L’école française de Battambang (EFB)</a></li></ul>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.aefe.fr/" class="spip_out" rel="external">Agence pour l’enseignement français à l’étranger</a></p>
<h3 class="spip"><a id="sommaire_2"></a>Enseignement supérieur</h3>
<p>Il est possible de suivre des études supérieures au Cambodge à l’Institut Technologique, à l’Université Royale d’Agronomie, à la Faculté de Droit et de Sciences Economiques, ainsi qu’à l’Université des sciences de la santé.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/cambodge/vie-pratique/article/scolarisation-111056). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
