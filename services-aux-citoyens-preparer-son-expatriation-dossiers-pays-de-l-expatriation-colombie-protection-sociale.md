# Protection sociale

<h2 class="rub22096">Régime local de sécurité sociale</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/protection-sociale/#sommaire_1">Retraite</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/protection-sociale/#sommaire_2">Décès</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/protection-sociale/#sommaire_3">Invalidité</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/protection-sociale/#sommaire_4">Allocations familiales</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/protection-sociale/#sommaire_5">Accident du travail et maladies professionnelles</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/protection-sociale/#sommaire_6">Chômage</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/protection-sociale/#sommaire_7">Pour en savoir plus :</a></li></ul>
<p>La création du système de sécurité sociale en 1993 a permis de mettre en place un système de financement de la santé qui se caractérise par une assurance santé universelle composée de deux régimes :</p>
<ul class="spip">
<li>le régime « contributif » qui couvre toutes les personnes titulaires d’un contrat de travail, les employés publics, les travailleurs indépendants ainsi que les retraités ou pensionnés. Ce régime inclut tous les niveaux de soin. Il est administré par les Empresas Promotoras de Salud (EPS) ;</li>
<li>le régime « subventionné » pour toutes les personnes ne disposant pas de revenus suffisant pour acquitter le montant total de la cotisation. Ce régime permet l’accès à un « paquet minimum » de soins. Il est administré par les Administradoras del Regimen Subsidario (ARS).</li></ul>
<p>Le système de sécurité sociale est financé par le fonds de solidarité et de garantie, géré par le Conseil national de sécurité sociale, le ministère de la Santé et les directions départementales et municipales.</p>
<h3 class="spip"><a id="sommaire_1"></a>Retraite</h3>
<p>Il existe deux régimes distincts de retraite en Colombie :</p>
<h4 class="spip">Le régime de répartition</h4>
<p>Ce système est administré par l’Institut d’assurance sociale (ISS) et par les caisses du secteur public ou privé. Le montant des pensions est fixe et défini à l’avance par l’ISS. Les apports des affiliés constituent un fonds commun à caractère public permettant le versement des prestations, la constitution des réserves correspondantes et le financement des coûts de gestion avec la garantie de l’État.</p>
<h4 class="spip">Le régime d’épargne individuelle</h4>
<p>Ce système est administré par des caisses privées : les Administradoras de Fondos de Pensiones (AFP). Les prestations dépendent des apports des affiliés et de leurs employeurs, de leurs rendements financiers et des subventions de l’Etat.</p>
<p>L’affiliation à l’un de ces deux régimes est obligatoire pour tous les travailleurs sous contrat de travail et pour tous les fonctionnaires. Il est toutefois possible de changer de régime au bout de 5 ans.</p>
<h4 class="spip">Départ à la retraite et montant des cotisations</h4>
<p>L’âge légal de départ à la retraite est de 57 ans pour les femmes et 62 ans pour les hommes. Il est possible de continuer à travailler au-delà de cette limite.</p>
<p>Le pourcentage de cotisation est le même pour le régime de répartition et le régime d’épargne individuelle : 75% de la cotisation est payée par l’employeur et 25% par le salarié.</p>
<p>Le montant de la retraite est en fonction du capital accumulé pendant la vie active du travailleur. Les affiliés des deux régimes doivent obligatoirement faire des apports au fond de solidarité des retraites.</p>
<h3 class="spip"><a id="sommaire_2"></a>Décès</h3>
<p>En cas de décès du partenaire le survivant doit justifier de cinq ans de vie commune pour bénéficier d’une pension de réversion.</p>
<p>Le montant de la pension s’élève au minimum à 45% du salaire moyen de la personne décédée. Le salaire moyen est calculé en fonction du salaire touché durant les dix dernières années d’activité.</p>
<h3 class="spip"><a id="sommaire_3"></a>Invalidité</h3>
<p>Est reconnu invalide l’assuré qui, en raison d’un handicap, est incapable de subvenir à ses besoins par le biais d’un travail lui procurant au moins 25 % du salaire moyen ou qui voit sa capacité de gain réduite de plus de 50 % à la suite du handicap dont il est atteint. Le salarié touche une pension d’invalidité quand le congé maladie dépasse trois mois.</p>
<p>La pension d’invalidité que l’assuré percevra est indexée sur le salaire moyen et fonction du taux d’invalidité. A titre d’exemple, une personne considérée invalide à 66% ou plus, touchera 75% de son salaire mensuel moyen. La pension est versée 13 ou 14 fois par an selon son montant.</p>
<h3 class="spip"><a id="sommaire_4"></a>Allocations familiales</h3>
<p>Il existe un système d’allocations familiales en Colombie. Pour pouvoir y avoir droit, les revenus de la famille ne doivent pas dépasser un maximum de quatre fois le montant du salaire minimum colombien. Les allocations familiales ne concernent que les enfants ayant moins de 18 ans ou 23 ans s’ils sont toujours étudiants.</p>
<h3 class="spip"><a id="sommaire_5"></a>Accident du travail et maladies professionnelles</h3>
<p>Les prestations pour accident du travail et maladie professionnelle couvrent l’incapacité à travailler suite à l’une de ces deux causes, pas la prise en charge médicale qui est assurée par la caisse de maladie. Sont couverts les accidents survenus à l’occasion du travail (y compris lors des trajets entre domicile et lieu de travail) ainsi que certaines maladies reconnues comme maladies professionnelles. En cas d’accident du travail ou de maladie professionnelle le salarié touche l’intégralité de son salaire.</p>
<h3 class="spip"><a id="sommaire_6"></a>Chômage</h3>
<p>Tous les employés du secteur privé sont automatiquement couverts par une assurance chômage en cas de licenciement. En revanche les employés du secteur public et les travailleurs indépendants sont libres de cotiser ou non.</p>
<h3 class="spip"><a id="sommaire_7"></a>Pour en savoir plus :</h3>
<ul class="spip">
<li><a href="http://www.who.int/countries/col/fr" class="spip_out" rel="external">OMS</a></li>
<li><a href="http://new.paho.org/col" class="spip_out" rel="external">Organisation panaméricaine de la santé</a></li>
<li><a href="http://www.gipspsi.org/Cooperation-internationale/Actions-par-pays/Colombie" class="spip_out" rel="external">Santé protection sociale internationale</a></li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-colombie-protection-sociale-article-convention-de-securite-sociale-103427.md" title="Convention de sécurité sociale">Convention de sécurité sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-colombie-protection-sociale-article-regime-local-de-securite-sociale.md" title="Régime local de sécurité sociale">Régime local de sécurité sociale</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/protection-sociale/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
