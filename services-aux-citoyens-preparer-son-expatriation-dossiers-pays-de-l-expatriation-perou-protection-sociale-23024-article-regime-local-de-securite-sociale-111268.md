# Régime local de sécurité sociale

<p>L’affiliation à ESSALUD est obligatoire et les cotisations versées par l’employeur représentent 9% du salaire brut.</p>
<p>L’employé peut s’affilier aux AFP (fonds de pensions), système privé de caisses de retraites dont les cotisations s’élèvent à 13% du salaire brut.</p>
<p>Au Pérou, l’employeur a pour obligation de verser sur un compte bloqué une compensation au temps de service « CTS », à défaut d’assurance chômage. L’employé peut toucher la somme accumulée le jour de son départ, en cas de licenciement, de démission ou de départ à la retraite. Le montant annuel correspond à un mois de salaire brut majoré d’un 1/6 du montant du salaire mensuel (soit 9,73 % du salaire annuel brut).</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/protection-sociale-23024/article/regime-local-de-securite-sociale-111268). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
