# Fiscalité du pays

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/norvege/fiscalite/article/fiscalite-du-pays-111214#sommaire_1">Année fiscale </a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/norvege/fiscalite/article/fiscalite-du-pays-111214#sommaire_2">Barème de l’impôt</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/norvege/fiscalite/article/fiscalite-du-pays-111214#sommaire_3">Coordonnées des centres d’information fiscale</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Année fiscale </h3>
<h4 class="spip">L’impôt sur le revenu applicable aux personnes physiques résidentes de Norvège</h4>
<h5 class="spip">Période de l’année fiscale</h5>
<p>L’année fiscale est alignée sur l’année civile et dure du 1er janvier au 31 décembre.</p>
<h5 class="spip">Date et lieu de dépôt des déclarations de revenus</h5>
<p>Une carte d’imposition est à demander (par l’employeur ou bien par la personne elle-même) auprès des services fiscaux du département où l’on s’établit. Selon la profession et selon l’estimation personnelle des revenus à venir, la carte d’imposition fixe un <i>tabell</i> (tableau dans lequel le montant à prélever sur le salaire brut est indiqué).</p>
<p>Un relevé des sommes ainsi versées est transmis, en fin d’année, à l’administration fiscale.</p>
<p>La déclaration d’impôts (<i>Selvangivelse</i>) est envoyée à chaque personne courant mars et elle est à rendre le 30 avril. En cas d’accord avec celle-ci, il n’est pas nécessaire de la retourner.</p>
<p>Chaque banque transmet au service des impôts le relevé de compte de chaque personne au 31 décembre. Ce montant apparait sur la déclaration d’impôts (<i>Formue</i>) et est soumis à une taxe.</p>
<h5 class="spip">Modalités de paiement des impôts pour un salarié et pour une activité non salariée</h5>
<p>Les impôts sur le revenu sont <strong>prélevés à la source</strong> pour les salariés. Ils sont versés tous les deux mois pour les professions libérales (janvier, mars, mai, juillet, septembre, novembre).</p>
<p>En cas d’augmentation en cours d’année, c’est au salarié de demander à son entreprise d’augmenter le prélèvement à la source pour éviter de devoir rembourser au mois de mai suivant.</p>
<p>Quand on quitte le pays, la régulation du trop ou trop-peu payé se fera au mois d’avril de l’année suivante.</p>
<h3 class="spip"><a id="sommaire_2"></a>Barème de l’impôt</h3>
<h4 class="spip">Barème de l’impôt sur le revenu des personnes physiques</h4>
<p>L’impôt sur le revenu varie entre 30 et 60% du salaire brut. L’impôt est basé sur le patrimoine du contribuable (logement, voitures, placements financiers). Un système d’abattement fiscal complexe s’applique sur les prêts immobiliers et sur l’outil de travail.</p>
<p>7,8% du salaire de l’employé est versé comme cotisation à la Sécurité sociale. Cette participation est comprise dans le prélèvement d’impôt opéré par l’employeur.</p>
<h3 class="spip"><a id="sommaire_3"></a>Coordonnées des centres d’information fiscale</h3>
<p><a href="http://www.skatteetaten.no/" class="spip_out" rel="external">Skatteetaten (ministère des Finances)</a> <br class="manualbr">Akersgaten 40 – N-0030 Oslo<br class="manualbr">Tél : 815 00 799</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="https://www.tresor.economie.gouv.fr/pays/norvege" class="spip_out" rel="external">Service économique français en Norvège</a>.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/norvege/fiscalite/article/fiscalite-du-pays-111214). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
