# Convention de sécurité sociale

<p>La France et les Etats-Unis sont liés par la convention de sécurité sociale du 2 mars 1987 entrée en vigueur au 1er juillet 1988.</p>
<p>Cet accord vise les ressortissants français et américains, les réfugiés et apatrides qui exercent une activité salariée ou non salariée dans l’un des deux Etats. Il vise également, sans conditions de nationalité, les ayants droit des personnes précitées.</p>
<p>Attention : l’accord franco-américain se limite à une coordination des assurances invalidité, vieillesse et décès (survivants). Il n’existe aucune disposition en matière d’assurances maladie, maternité, accidents du travail et de prestations familiales.</p>
<p>La convention prévoit l’affiliation obligatoire dans le pays du lieu de travail. Toutefois, dans le cadre du détachement, un assuré du régime français, quelle que soit sa nationalité, exerçant une activité professionnelle aux Etats-Unis pourra, sous certaines conditions, être maintenu au régime français de sécurité sociale durant son activité aux Etats-Unis et dispensé du paiement des cotisations aux Etats-Unis.</p>
<p>En application conjointe de la législation interne française et de la convention franco-américaine, les Français occupés aux Etats-Unis se trouvent généralement dans une des situations suivantes :</p>
<ul class="spip">
<li>travailleur détaché dans le cadre conventionnel ou dans le cadre de la législation interne ;</li>
<li>travailleur non détaché exerçant aux Etats-Unis une activité professionnelle et bénéficiant à ce titre des dispositions conventionnelles ;</li></ul>
<p>Le travailleur soumis à la législation américaine peut compléter la protection ainsi acquise par une adhésion à la Caisse des Français de l’étranger.</p>
<p>Tout renseignement complémentaire au sujet de l’accord franco-américain peut être obtenu auprès de l’organisme de liaison français désigné pour l’application de cet accord(article 19) :</p>
<p><a href="http://www.cleiss.fr/docs/textes/index.html" class="spip_out" rel="external">Centre des liaisons européennes et internationales de sécurité sociale (CLEISS)</a><br class="manualbr">11, rue de la Tour des Dames<br class="manualbr">75009 PARIS<br class="manualbr">Tél. : 01 45 26 33 41<br class="manualbr">Fax : 01 49 95 06 50</p>
<p><strong>Travailleurs non détachés</strong></p>
<p>En vertu du principe de l’égalité de traitement posé par l’article 4 de l’accord, les ressortissants français bénéficient, ainsi que leurs ayants droit résidant aux Etats-Unis, du régime américain dans les mêmes conditions que les ressortissants américains.</p>
<h4 class="spip">Droits du travailleur pour lui-même et ses ayants droit qui l’accompagnent</h4>
<p><strong>Généralités</strong></p>
<p>Le Français travaillant aux Etats-Unis relève du régime fédéral d’invalidité-vieillesse-décès (survivants) ainsi que du régime local d’assurance chômage et d’assurance contre les accidents du travail-maladies professionnelles. Il est généralement couvert, ainsi que sa famille, pour les soins de santé auprès d’une compagnie d’assurances privée (<i>Blue Cross</i> ou <i>Blue Shield</i>, par exemple).</p>
<p>Il lui appartient également de se garantir contre le risque maladie lors de ses séjours en France (à l’occasion de congés payés, d’une convalescence, etc.) ou d’un retour définitif : l’accord franco-américain ne prévoit pas de maintien de droit aux prestations dans ces différentes hypothèses.</p>
<p><strong>Pensions</strong></p>
<p>Dès lors qu’une personne aura accompli au moins six trimestres d’assurance aux Etats-Unis, l’institution américaine compétente pourra lui ouvrir un droit à pension de vieillesse ou d’invalidité du régime américain en tenant compte des périodes d’assurance accomplies en France.</p>
<p><strong>Assurance invalidité</strong></p>
<p>Si l’invalidité survient alors que l’intéressé était soumis à la législation américaine, il bénéficiera d’une pension du régime américain liquidée selon la législation américaine en tenant compte éventuellement des périodes d’assurance accomplies en France.</p>
<p>Si l’invalidité survient alors que l’intéressé était soumis à la législation française, la caisse française liquidera une pension française d’invalidité.</p>
<p>Si l’intéressé peut également prétendre à une pension du régime américain, la caisse française calculera au prorata le montant de la pension d’invalidité en fonction des années d’assurance en France. Toutefois, si le montant de la pension française, calculée sans application de l’accord, est supérieur aux montants des pensions française et américaine, l’institution française versera un complément différentiel.</p>
<p><strong>Assurance vieillesse</strong></p>
<p>En matière d’assurance vieillesse, comme en matière d’assurance invalidité, l’institution américaine pourra tenir compte des périodes d’assurance accomplies en France.</p>
<p>Du côté français, il pourra également être tenu compte des périodes d’assurance aux Etats-Unis pour la détermination du taux de liquidation de la pension à l’âge légal de la retraite (60 ou 62 ans en fonction de l’année de naissance).</p>
<p><strong>Membres de la famille demeurés en France</strong></p>
<p>L’accord franco-américain ne comprend aucune disposition prévoyant que le travailleur ouvre des droits aux soins ou aux prestations familiales, au titre de son activité aux Etats-Unis, en faveur des membres de la famille demeurés en France.</p>
<p>Dès lors et puisque les prestations familiales sont garanties en faveur des enfants résidant en France, sans condition d’activité professionnelle dans le cadre de la législation française, il ne reste plus qu’à couvrir à un titre ou à un autre les soins de santé des membres de la famille : le conjoint qui travaille en France peut prendre sur son compte les enfants. Si tel n’est pas le cas, il lui appartient de s’adresser à la caisse primaire de son lieu de résidence qui examinera ses droits au regard de la couverture maladie universelle.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/protection-sociale/article/convention-de-securite-sociale). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
