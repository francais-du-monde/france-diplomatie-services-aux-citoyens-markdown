# Fiscalité

<h2 class="rub23157">Fiscalité du pays</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/fiscalite/#sommaire_1">Impôt sur les bénéfices des sociétés</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/fiscalite/#sommaire_2">Impôt sur le revenu des personnes physiques</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/fiscalite/#sommaire_3">Imposition du patrimoine immobilier</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/fiscalite/#sommaire_4">Imposition des intérêts, dividendes, plus values</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/fiscalite/#sommaire_5">Taxe sur les ventes (General sales taxes, « GST »)</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/fiscalite/#sommaire_6">Autres taxes</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/fiscalite/#sommaire_7">Retenues à la source pour les opérations avec l’étranger et protection contre les doubles impositions</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/fiscalite/#sommaire_8">Barème de l’impôt </a></li>
<li><a id="so_9" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/fiscalite/#sommaire_9">Quitus fiscal </a></li>
<li><a id="so_10" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/fiscalite/#sommaire_10">Coordonnées de centres d’information fiscale </a></li></ul>
<p>Les autorités égyptiennes ont entamé une réforme importante de la fiscalité depuis 2004, qui s’est traduite notamment par une diminution importante des tarifs douaniers, et par une baisse des taux d’imposition et une simplification de la fiscalité.</p>
<h3 class="spip"><a id="sommaire_1"></a>Impôt sur les bénéfices des sociétés</h3>
<p>Toutes les sociétés commerciales sont assujetties à « l’impôt sur le revenu des personnes morales » (IRPM) : les sociétés de capitaux régies par la loi sur les sociétés 159/1981, aussi bien que les sociétés de personnes (sociétés en nom collectif et société en commandite simple). L’IRPM est au taux unique de 20 %, à l’exception des profits des entreprises du secteur pétrolier et gazier qui restent taxés à 40,55 %. Le bénéfice fiscal correspond au résultat comptable à quelques corrections près.</p>
<p>Auparavant négocié sur la base des déclarations des sociétés, le règlement de l’impôt a été considérablement simplifié. Un changement des mentalités a été opéré, et les entreprises ne sont plus présumées « fraudeuses ». Une retenue à la source est pratiquée sur les bénéfices industriels et commerciaux. La retenue, payée au fisc par les entreprises clientes/acheteuses, porte sur les factures de biens et services supérieures à 300 LE (déclarations trimestrielles). Ces retenues viennent, pour le fournisseur, en déduction de son impôt à payer sur les bénéfices (déclaration annuelle).</p>
<p>Le taux de retenue est de 0,5 % sur les travaux et achats de marchandises, de 2 % sur les services et de 5 % sur les commissions. La retenue a pour assiette le chiffre d’affaires du fournisseur, alors que la déduction s’opère sur l’impôt sur le bénéfice fiscal. Dès lors, le montant total des retenues à la source peut être supérieur à l’impôt sur les bénéfices. Ce dernier, dans ces conditions, ne devrait être à payer qu’à partir d’un résultat substantiel. Mais le mécanisme de retenue n’est pas parfaitement suivi, et le taux d’impôt sur les bénéfices de 20% reste significatif.</p>
<p>Un centre pour les grands contribuables a été installé au Caire en septembre 2005 pour faciliter et professionnaliser les opérations de déclaration et de paiement. Le ministère des finances expérimente aussi la délocalisation de cellules fiscales auprès de gros cabinets d’expertise comptable.</p>
<h3 class="spip"><a id="sommaire_2"></a>Impôt sur le revenu des personnes physiques</h3>
<p>Toutes les personnes physiques sont assujetties à l’impôt sur le revenu des personnes physiques (IRPP) pour leurs revenus d’activité (bénéfices commerciaux ou non commerciaux, salaires) ou leurs revenus fonciers. Les entreprises commerciales individuelles sont également soumises à l’IRPP pour leurs bénéfices, dont le calcul est identique a celui des sociétés commerciales (régime des bénéfices industriels et commerciaux).</p>
<p>L’impôt sur les salaires est <strong>retenu mensuellement à la source</strong> aux mêmes taux et tranches, et versé directement par l’employeur au Fisc (déclarations mensuelles). Les dirigeants des sociétés de capitaux (mandataires sociaux) sont assimilés aux salariés. En tant que résidents, les salariés expatriés sont imposés en Egypte dans les mêmes conditions, sous réserve des dispositions des conventions contre la double imposition.</p>
<p>L’année d’imposition correspond à l’année civile. Une déclaration de revenus doit être remplie et adressée à l’administration fiscale égyptienne avant le 1er avril de chaque année, accompagnée du paiement de l’impôt.</p>
<p>Les déclarations fiscales sont effectuées auprès du département des taxes, et le paiement de l’impôt intervient après la validation de cette déclaration.</p>
<h3 class="spip"><a id="sommaire_3"></a>Imposition du patrimoine immobilier</h3>
<p>En juin 2008, une taxe sur la propriété foncière a été instituée, effective à partir du 1er janvier 2009. Cette taxe s’élève à 10 % de la valeur locative du bien, présumée à 7 % de la valeur d’acquisition du bien immobilier. Toutefois, si le bien est loué, la valeur locative est égale au loyer perçu, si ce dernier est supérieur à 7 % de la valeur effective du bien.</p>
<h3 class="spip"><a id="sommaire_4"></a>Imposition des intérêts, dividendes, plus values</h3>
<p>Les intérêts et royalties reçus et versés rentrent dans la détermination du bénéfice imposable sous réserve des conventions fiscales. Pour l’IRPP comme pour l’IRPM, les revenus des capitaux mobiliers ne sont pas imposables. Les plus-values à long terme ne font pas l’objet de taux spécifiques d’imposition. Elles rentrent dans la détermination du bénéfice imposable au titre des profits exceptionnels.</p>
<h3 class="spip"><a id="sommaire_5"></a>Taxe sur les ventes (General sales taxes, « GST »)</h3>
<p>Une taxe sur les ventes (General sales tax - GST) a été instituée par la loi 11/1991 (dernièrement amendée en 2005). La GST s’assimile à une taxe sur la valeur ajoutée, à la différence que la déductibilité est écartée sur toute une série de vente de biens et sur les services. Le régime général fixe des taux compris entre 5 et 25 %, avec un taux de 10 % sur plupart des biens et services. Cependant divers taux à 50 %, 75 %, 100 %, 200 % existent (alcool, véhicules supérieurs à 1600 cc,…), ainsi que des montants en valeur absolue par quantités ou poids. Les déclarations et le versement des montants perçus doivent être effectués dans un délai de 60 jours maximum après la clôture de l’exercice comptable mensuel. Passé ce délai, le bureau local concerné estimera lui-même le montant à percevoir. Les excédents de taxe déductible sont reportables.</p>
<p>Le remplacement de la GST par une taxe sur la valeur ajoutée (TVA), avec déductibilité généralisée et des taux simplifiés, a été annoncé en 2007 par le Ministère de Finances, mais sa mise en place n’est toujours pas effective.</p>
<h3 class="spip"><a id="sommaire_6"></a>Autres taxes</h3>
<p>Divers droits de timbre et d’enregistrement s’appliquent aux actes et transactions qui ne relèvent pas de la GST, notamment sur les créations de société et cessions de capital et d’actifs. Ces droits ne sont pas aujourd’hui excessifs. Les taxes dues à l’occasion des mutations immobilières ont été ramenées de 12 % à 3 % en 2008. Cette nouvelle réforme tend à sécuriser la propriété foncière et à faciliter le développement du crédit hypothécaire.</p>
<h3 class="spip"><a id="sommaire_7"></a>Retenues à la source pour les opérations avec l’étranger et protection contre les doubles impositions</h3>
<p>N’étant pas imposés, les dividendes versés à des non-résidents ne font pas l’objet d’une retenue à la source. Sous réserve des conventions fiscales contre la double imposition, les intérêts et redevances sont soumis à une retenue à la source de 20 %. Pour les entreprises et ressortissants français, les doubles impositions sont corrigées dans le cadre de la convention fiscale entre la France et l’Egypte.</p>
<p><i>Source : site de la <a href="http://www.tresor.economie.gouv.fr/se/egypte/implantation.asp" class="spip_out" rel="external">mission économique française en Egypte</a></i></p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.salestax.gov.eg/ewhoregis1.html" class="spip_out" rel="external">Sales tax Department</a></p>
<h3 class="spip"><a id="sommaire_8"></a>Barème de l’impôt </h3>
<p><strong>Taux d’imposition</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Revenu annuel net imposable  (en livres égyptiennes)</td>
<td>Taux</td></tr>
<tr class="row_even even">
<td>0 à 5000</td>
<td>0%</td></tr>
<tr class="row_odd odd">
<td>5001 à 20 000</td>
<td>10%</td></tr>
<tr class="row_even even">
<td>20 001 à 40 000</td>
<td>15%</td></tr>
<tr class="row_odd odd">
<td>Au-delà de 40 000</td>
<td>20%</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_9"></a>Quitus fiscal </h3>
<p>Il n’est pas exigé de quitus fiscal avant de quitter le pays.</p>
<h3 class="spip"><a id="sommaire_10"></a>Coordonnées de centres d’information fiscale </h3>
<p><a href="http://www.salestax.gov.eg/edistrictname.html" class="spip_out" rel="external">Adresses et téléphones des bureaux fiscaux</a>.</p>
<p><i>Mise à jour : avril 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-egypte-fiscalite-article-convention-fiscale-112197.md" title="Convention fiscale">Convention fiscale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-egypte-fiscalite-article-fiscalite-du-pays.md" title="Fiscalité du pays">Fiscalité du pays</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/fiscalite/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
