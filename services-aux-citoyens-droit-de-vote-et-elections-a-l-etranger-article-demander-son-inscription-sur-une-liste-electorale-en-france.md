# Demander son inscription sur une liste électorale en France

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/droit-de-vote-et-elections-a-l-etranger/article/demander-son-inscription-sur-une-liste-electorale-en-france#sommaire_1">Prolongation exceptionnelle de l’inscription sur les listes électorales municipales</a></li></ul>
<p>Tout en étant établi(e) hors de France, vous pouvez aussi demander votre inscription sur la liste électorale d’une commune de France.</p>
<p><strong>Si vous avez une résidence en France</strong></p>
<p>Vous pouvez demander votre inscription sur la liste électorale de la commune correspondante.</p>
<p><strong>Si vous n’avez pas de résidence en France</strong></p>
<p>Il vous faut être inscrit(e) au registre des Français établis hors de France pour demander votre inscription sur la liste électorale d’une des communes suivantes :</p>
<ul class="spip">
<li>votre commune de naissance</li>
<li>celle de votre dernier domicile</li>
<li>celle de votre dernière résidence si elle a duré au moins six mois</li>
<li>celle où est inscrit votre conjoint</li>
<li>celle où est né(e), est/a été inscrit(e) un(e)de vos ascendant(e)s</li>
<li>celle où est/a été inscrit un de vos parents jusqu’au quatrième degré</li></ul>
<h4 class="spip">Comment et quand demander son inscription ?</h4>
<p>Pour être valable l’année suivante, vous devez demander votre inscription impérativement au plus tard le 31 décembre :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  soit à la mairie en vous déplaçant à ses guichets ou par correspondance en lui adressant directement <a href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/cerfa_election_1266901.pdf" class="spip_out">le formulaire de demande d’inscription sur les listes électorales à l’usage des citoyens français</a> (ref. Cerfa, n° 12669*01) accompagné des pièces justificatives.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  soit à l’ambassade ou au poste consulaire de votre résidence.</p>
<p><strong>Attention : </strong> dans ce cas, l’envoi ou le dépôt de la demande ne garantit pas sa réception à la mairie au plus tard le dernier jour ouvrable de l’année.</p>
<h3 class="spip"><a id="sommaire_1"></a>Prolongation exceptionnelle de l’inscription sur les listes électorales municipales</h3>
<p>En 2015, les listes électorales des communes en France font l’objet d’une procédure de révision exceptionnelle. <strong>Les demandes d’inscription sont recevables jusqu’au 30 septembre 2015</strong>. Cette mesure permettra aux personnes qui ne se sont pas inscrites avant le 31 décembre 2014 ou qui ont déménagé depuis le 1er janvier, de participer aux scrutins en France pour les élections régionales les 6 et 13 décembre 2015.</p>
<p>Les Français de l’étranger qui souhaitent s’inscrire sur une liste électorale communale pour participer aux élections régionales de décembre prochain doivent s’adresser directement aux services de la mairie en question.</p>
<p><i>Mise à jour : juillet 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/droit-de-vote-et-elections-a-l-etranger/article/demander-son-inscription-sur-une-liste-electorale-en-france). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
