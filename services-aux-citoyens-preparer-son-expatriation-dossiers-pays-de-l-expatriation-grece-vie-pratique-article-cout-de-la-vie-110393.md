# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/vie-pratique/article/cout-de-la-vie-110393#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/vie-pratique/article/cout-de-la-vie-110393#sommaire_2">Budget</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/vie-pratique/article/cout-de-la-vie-110393#sommaire_3">Alimentation</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/vie-pratique/article/cout-de-la-vie-110393#sommaire_4">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>L’unité monétaire est l’euro. La Grèce fait partie de la zone euro depuis le 1er janvier 2001 et a remplacé sa monnaie nationale par la monnaie européenne le 1er janvier 2002.</p>
<h4 class="spip">Le paiement en liquide </h4>
<p>Le paiement en liquide est encore très utilisé en Grèce ; les particuliers procèdent à des retraits fréquents dans les agences bancaires ou par l’intermédiaire de guichets et distributeurs automatiques. Ainsi, par exemple, 80 % des achats dans la grande distribution s’effectuent en argent liquide (contre 20 % en France).</p>
<h4 class="spip">Le paiement par chèque bancaire</h4>
<p>Le chèque est très peu utilisé par les particuliers pour des raisons de sécurité juridique, mais davantage par les entreprises, qui en ont fait un instrument de crédit dont l’usage est admis (chèque postdaté et pouvant être endossé par plusieurs créanciers successifs).</p>
<h4 class="spip">Le paiement par carte de crédit</h4>
<p>La carte de crédit a connu un développement important avec un effort marqué d’équipement des banques, du commerce et de la grande distribution.</p>
<h4 class="spip">Les transferts interbancaires </h4>
<p>La compensation et les transferts interbancaires sont aujourd’hui réalisés par le système informatique DIAS, auquel ont adhéré les principales banques commerciales.</p>
<p>Les transferts de ou vers la France (et le reste de l’Union européenne) sont libéralisés pour les particuliers comme pour les entreprises, sous réserve de présentation des documents requis à la banque (facture, bon de livraison, etc.).</p>
<p>Les autorités helléniques autorisent les virements bancaires "swift" vers tous les pays. Son avantage est le paiement immédiat. Il s’agit d’un mode de paiement courant en raison de l’inexistence du risque d’insolvabilité. Le paiement peut prendre un dizaine de jours. Il est nécessaire de détenir un compte en devises pour émettre un chèque en devises. Moyen de paiement peu usité pour les transactions courantes, il est utilisé par les entreprises comme titre de crédit dans la mesure où il est souvent post-daté et endossable par des porteurs successifs.</p>
<p>Plusieurs banques françaises sont installées à Athènes : BNP-Paribas, Société Générale (Geniki Bank), Crédit Agricole (Emboriki Bank).</p>
<p>Les documents indispensables à l’ouverture d’un compte simple par un Français, résident en Grèce, sont :</p>
<ul class="spip">
<li>La carte d’identité ou le passeport du titulaire,</li>
<li>Le numéro d’immatriculation fiscale « AFM » : ce numéro est indispensable à l’accomplissement de la plupart des démarches administratives et commerciales en Grèce et est indiqué sur la déclaration d’impôts grecque ou sur une attestation de l’administration fiscale grecque.</li>
<li>Un justificatif de domicile en Grèce : contrat de location enregistré à l’administration fiscale, facture de téléphone, eau ou électricité au nom du titulaire, permis de séjour…</li>
<li>Un justificatif de revenu en Grèce : certificat d’embauche de l’employeur, bulletin de salaire, contrat de travail, carte d’identité professionnelle…</li></ul>
<p>Pour les ouvertures de comptes par des Français non-résidents en Grèce, les mêmes justificatifs de domicile et de revenu (en France) sont exigés. Le numéro d’immatriculation fiscale ne les concerne pas.</p>
<p>Dans le cas d’un compte joint, les justificatifs devront être présentés par chacun des titulaires du compte.</p>
<p>Les originaux de ces documents devront être présentés accompagnés de copies, conservées par la banque.</p>
<p>Les banques sont ouvertes de 8h00 à 14h00 en jour de semaine, le vendredi de 8h00 à 13h30 et sont fermées le samedi et dimanche.</p>
<h4 class="spip">Opérations bancaires</h4>
<p>Les transferts de fonds sont libres.</p>
<h3 class="spip"><a id="sommaire_2"></a>Budget</h3>
<p>Le candidat à l’expatriation devra se garder de chercher à apprécier sa situation en convertissant tous les chiffres en euros, ou en tentant de déterminer le pouvoir d’achat en France du montant obtenu. Cette appréciation doit se faire avant tout sur le montant des dépenses dans le pays d’expatriation, qu’elles soient exprimées en monnaie locale ou en euros.</p>
<h3 class="spip"><a id="sommaire_3"></a>Alimentation</h3>
<h4 class="spip">Prix moyen d’un repas dans un restaurant</h4>
<p>Le prix moyen d’un repas dans un restaurant de qualité supérieure est de 70 euros, et de 30 euros dans un restaurant de qualité moyenne.</p>
<h3 class="spip"><a id="sommaire_4"></a>Evolution des prix</h3>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : novembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/grece/vie-pratique/article/cout-de-la-vie-110393). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
