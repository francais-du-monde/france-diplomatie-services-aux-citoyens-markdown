# Entrée et séjour

<h2 class="rub22845">Passeport, visa, permis de travail</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/entree-et-sejour/#sommaire_1">Court séjour</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/entree-et-sejour/#sommaire_2">Carte de résident</a></li></ul>
<p>Quel que soit le motif de votre séjour, renseignez-vous auprès du consulat général du Sénégal à Paris qui vous informera sur la règlementation en matière d’entrée et de séjour au Sénégal, règlementation que vous devrez impérativement respecter. N’hésitez pas à consulter le site ministère de l’Intérieur du Sénégal afin de vous familiariser avec les modalités d’entrée et de séjour dans le pays.</p>
<p><strong>En aucun cas, vous n’êtes autorisé à travailler au Sénégal sans permis adéquat.</strong></p>
<p>Le Consulat de France à Dakar n’est pas l’organisme compétent pour répondre à vos demandes d’information concernant votre séjour au Sénégal.</p>
<h3 class="spip"><a id="sommaire_1"></a>Court séjour</h3>
<p>De manière générale, pour toute information relative aux conditions de séjour au Sénégal, il est vivement conseillé de contacter <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-etrangers-en-france/article/annuaire-des-representations-etrangeres-en-france" class="spip_in">le consulat général du Sénégal à Paris</a>.</p>
<p>Des visas biométriques sont obligatoires depuis le <strong>1er juillet 2013</strong> pour tous les Français dont le séjour au Sénégal est inférieur à trois mois.</p>
<ul class="spip">
<li>La biométrie comporte la prise d’empreintes digitales et une photographie du requérant.</li>
<li>Pour de plus amples informations, vous pouvez consulter le site Internet <a href="http://www.visasenegal.sn/" class="spip_out" rel="external">Visasenegal.sn</a> (renvoi vers le site de la SNEDAI) sur lequel vous pouvez effectuer votre pré-enrôlement.</li>
<li>Le coût du visa est de 50 €. Le montant des frais de traitement varie en fonction de la carte bancaire utilisée et sera compris entre 2,5 et 5 €. Le paiement s’effectue par carte bancaire en ligne via le site <a href="http://www.visasenegal.sn/" class="spip_out" rel="external">Visasenegal.sn</a>.</li>
<li>L’enrôlement peut s’effectuer dans un des quatre consulats généraux en France (Paris, Bordeaux, Lyon, Marseille) ou l’aéroport. Il est à noter qu’un touriste ne peut débarquer au poste frontière sans autorisation préalable, le <strong>pré-enrôlement est obligatoire</strong>.</li>
<li>La prorogation de visa au-delà des 90 jours, lorsque le requérant est au Sénégal, relève de la police des étrangers et des titres de voyage (DPETV) du ministère de l’Intérieur.</li>
<li>Les détenteurs de passeports diplomatiques, de service ou ordinaires, en mission officielle au Sénégal (ordre de mission, invitation officielle, note verbale), pourront bénéficier d’un visa de courtoisie. Leurs conjoints et membres de la famille qui ne disposent pas de passeport diplomatique ou de service devront acquitter le paiement du visa.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Carte de résident</h3>
<p>Pour un <strong>séjour supérieur à trois mois</strong>, une demande de <strong>carte de résident</strong> (renouvelable chaque année) doit être effectuée auprès de la direction de la police des étrangers et des titres de voyage (DPETV) - Allée Khalifa Ababacar Sy Dieupeul - tél : (221) 33.869.30.01). Le visa de moins de trois mois est obligatoire et devra être régularisé auprès de la DPETV si le séjour dépasse cette période.</p>
<p>Les pièces à fournir sont les suivantes :</p>
<ul class="spip">
<li>demande manuscrite (ou dactylographiée) adressée au ministre de l’Intérieur ;</li>
<li>photocopie du passeport en cours de validité, comportant la date d’arrivée au Sénégal ;</li>
<li>casier judiciaire du pays d’origine, datant de moins de trois mois ;</li>
<li>extrait d’acte de naissance ;</li>
<li>certificat médical de moins de trois mois ;</li>
<li>caution de rapatriement de 75.000 FCFA ( 114€) pour les ressortissants français ;</li>
<li>timbres fiscaux d’une valeur globale de 15.000 FCFA (23€) ;</li>
<li>3 photos d’identité ;</li>
<li>des références professionnelles (contrat de travail, statut de la société, registre du commerce, attestation de la mission de Coopération, carte de commerçant…).</li></ul>
<p>Le renouvellement du visa annuel coûte 10.000 FCFA.</p>
<p>L’autorisation d’établissement donne droit à une <strong>carte d’identité d’étranger (CIE), valable cinq ans.</strong></p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.demarches.gouv.sn/" class="spip_out" rel="external">Démarches administratives au Sénégal</a>  </p>
<p>Les contrats d’expatriés doivent être soumis au visa de la direction du travail. Ils peuvent être retirés dans les 15 jours qui suivent le dépôt de la demande.</p>
<p>Il n’y a pas d’autorisation spéciale pour un contrat local. Il est toutefois à noter que seules les candidatures pour les postes nécessitant des compétences spécifiques introuvables au Sénégal peuvent aboutir. La main d’œuvre sénégalaise reste prioritaire.</p>
<p><strong>Sites à consulter :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.consulsen-paris.fr/" class="spip_out" rel="external">Consulat général du Sénégal à Paris</a></p>
<p><i>Mise à jour : décembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-senegal-entree-et-sejour-article-animaux-domestiques.md" title="Animaux domestiques">Animaux domestiques</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-senegal-entree-et-sejour-article-vaccination-110053.md" title="Vaccination">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-senegal-entree-et-sejour-article-demenagement-110052.md" title="Déménagement">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-senegal-entree-et-sejour-article-passeport-visa-permis-de-travail-110051.md" title="Passeport, visa, permis de travail">Passeport, visa, permis de travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/entree-et-sejour/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
