# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/emploi-stage/article/reglementation-du-travail-111181#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/emploi-stage/article/reglementation-du-travail-111181#sommaire_2">Contrat de travail – spécificités</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/emploi-stage/article/reglementation-du-travail-111181#sommaire_3">Fêtes légales</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<p>Le droit du travail est régi par la loi n° 2003-044 du 28 juillet 2004 portant Code du travail. Le Code du travail fixe la durée légale de travail à <strong>173,33 heures par mois. </strong>Les heures effectuées au-delà de cette durée légale constituent des heures supplémentaires qui donnent lieu à une majoration.</p>
<p>Les <strong>charges patronales </strong>sont de <strong>18 %</strong>. Les <strong>charges salariales</strong> sont de <strong>2,90 %.</strong></p>
<p>Le repos hebdomadaire est obligatoire et d’une durée minimum de vingt-quatre heures consécutives. Il est donné principalement le dimanche. Les jours fériés sont chômés et payés.</p>
<p>La loi n° 2003-044 portant Code du travail et son décret d’application n° 2008-246 du 19 mars 2007 fixent les modalités de rémunération salariale. En 2008, le <strong>salaire minimum d’embauche</strong> est fixé à <strong>70 025,40 MGA par mois</strong> (25,43 €). Le salaire d’un travail effectué à Madagascar doit être versé en ariary. Le paiement en nature n’est autorisé que s’il porte sur un logement ou de la nourriture.</p>
<p><strong>Les femmes et les mineurs bénéficient d’un traitement spécial</strong>. Leur repos quotidien doit avoir une <strong>durée consécutive de douze heures</strong>, et il leur est interdit de travailler la nuit (sauf exceptions).</p>
<h3 class="spip"><a id="sommaire_2"></a>Contrat de travail – spécificités</h3>
<p><strong>À Madagascar, le droit du travail est bâti sur le modèle français. </strong>Il existe différentes formes de contrat :</p>
<ul class="spip">
<li>le contrat de travail à durée déterminée</li>
<li>le contrat de travail à durée indéterminée</li>
<li>le contrat d’apprentissage</li>
<li>le contrat de travail journalier</li>
<li>le contrat de travail saisonnier</li>
<li>le contrat de travail intérimaire</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Fêtes légales</h3>
<ul class="spip">
<li><strong>1er janvier</strong> : jour de l’an</li>
<li><strong>8 mars</strong> (demi-journée chômée pour les femmes) : journée mondiale de la femme</li>
<li><strong>29 mars</strong> : journée commémorative du 29 mars 1947 (fête de l’insurrection)</li>
<li><strong>Lundi de Pâques</strong></li>
<li><strong>1er mai</strong> : fête du travail</li>
<li><strong>Jour de l’Ascension</strong></li>
<li><strong>Lundi de Pentecôte</strong></li>
<li><strong>25 mai</strong> : journée de l’Organisation de l’Unité africaine</li>
<li><strong>26 juin</strong> : fête nationale malgache</li>
<li><strong>15 août</strong> : Assomption</li>
<li><strong>1er novembre</strong> : Toussaint</li>
<li><strong>25 décembre</strong> : Noël</li></ul>
<p>Voir aussi : <a href="http://www.tv5.org/cms/chaine-francophone/voyageurs/Outils/p-8228-Jours-feries.htm" class="spip_out" rel="external">TV5.org</a></p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/emploi-stage/article/reglementation-du-travail-111181). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
