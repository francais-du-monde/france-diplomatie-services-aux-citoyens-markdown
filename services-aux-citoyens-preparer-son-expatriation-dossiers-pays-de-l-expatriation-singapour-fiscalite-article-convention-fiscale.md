# Convention fiscale

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/fiscalite/article/convention-fiscale#sommaire_1">Champ d’application de la convention</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/fiscalite/article/convention-fiscale#sommaire_2">Règles d’imposition</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/fiscalite/article/convention-fiscale#sommaire_3">Revenus non commerciaux</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/fiscalite/article/convention-fiscale#sommaire_4">Elimination de la double imposition</a></li></ul>
<p>La France et Singapour ont signé <strong>le 9 septembre</strong> une convention en vue d’éviter les doubles impositions et de prévenir l’évasion fiscale en matière d’impôts sur le revenu et sur la fortune. Cette convention est <strong>entrée en vigueur le 3 octobre 1975. </strong></p>
<p>Voici un résumé de ses points clés pour les particuliers :</p>
<h3 class="spip"><a id="sommaire_1"></a>Champ d’application de la convention</h3>
<p>Cet accord a pour objet de protéger <strong>les résidents de chacun des Etats contractants </strong>en matière d’impôts prélevés directement sur le revenu ou sur le bénéfice des sociétés.</p>
<p>La convention trouve donc à s’appliquer aux résidents de ces Etats.</p>
<h4 class="spip">Notion de résidence</h4>
<p>L’article 4 paragraphe 1 de la convention s’applique aux personnes qui sont considérées comme "résidents d’un Etat contractant" ou de chacun de ces deux Etats. Au paragraphe 2, l’article 4 fournit les critères permettant de déterminer la notion de résidence.</p>
<p>Ces critères sont :</p>
<ul class="spip">
<li>un foyer d’habitation permanent (il s’agit, par exemple, du lieu de situation du conjoint ou des enfants) ;</li>
<li>l’Etat où la personne possède le centre de ses intérêts vitaux (tant professionnels que privés) ;</li>
<li>l’Etat dans lequel elle séjourne de façon habituelle (notion de 183 jours de présence physique sur le territoire au cours d’une année fiscale) ;</li>
<li>à défaut, les autorités compétentes de ces Etats tranchent ensemble la question.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Règles d’imposition</h3>
<h4 class="spip">Rémunérations privées et activité libérale</h4>
<p>L’article 15, paragraphe 1 précise que les traitements et salaires d’origine privée ainsi que les revenus d’une profession libérale ne sont, en règle générale, imposables que dans l’Etat où <strong>s’exerce l’activité personnelle</strong>.</p>
<p>Exceptions à cette règle générale :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Le maintien de l’imposition dans l’Etat de résidence du bénéficiaire est prévu par le paragraphe 2 du même article sous réserve de trois conditions simultanément remplies :
<br>— le séjour temporaire du bénéficiaire dans l’autre Etat ne dépasse pas une durée totale de 183 jours ;
<br>— la rémunération ou le revenu sont payés par une personne qui n’est pas résident de l’Etat d’exercice ;
<br>— la rémunération ou le revenu ne doivent pas être à la charge d’un établissement stable ou d’une base fixe de cette personne dans l’Etat.</p>
<p>Exemple : Monsieur X est envoyé trois mois à Singapour, soit quatre-vingt-dix jours (mai, juin, juillet de l’année n) par une PME située en France en vue de prospecter le marché singapourien. Cette entreprise ne dispose d’aucune succursale ni bureau à Singapour et rémunère l’intéressé. Dans un tel cas, Monsieur X devra déclarer ses revenus en France.</p>
<p>Au contraire, si Monsieur X est envoyé du mois de février au mois de novembre inclus, son séjour de plus de 183 jours à Singapour entraîne son imposition dans ce pays.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Il résulte des dispositions du paragraphe 3 de l’article 15 de la convention que les revenus professionnels des salariés employés à bord d’un navire ou d’un aéronef en trafic international ne sont imposables que dans l’Etat de résidence du bénéficiaire.</p>
<h4 class="spip">Rémunérations publiques</h4>
<p>L’article 19, paragraphe 1-a, précise que les traitements, salaires et rémunérations analogues ainsi que les <strong>pensions de retraite </strong>payés par un Etat ou une personne morale de droit public de cet Etat à une personne qui a la nationalité de cet Etat restent imposables dans cet Etat.</p>
<p>Exemple : Monsieur X est fonctionnaire de l’Etat Français. Il résidait pendant son activité en France et décide d’aller prendre sa retraite à Singapour. Les montants de ses pensions resteront imposés en France ; son dossier est alors pris en charge par un centre des impôts spécial, le Centre des Impôts des Non-résidents.</p>
<p>Exception :</p>
<p><strong>Toutefois</strong>, en vertu des dispositions du paragraphe 2 du même article, les règles fixées au paragraphe 1 dudit article ne sont pas applicables aux rémunérations ou aux pensions versées au titre <strong>de services rendus dans le cadre d’une activité industrielle ou commerciale exercée par un Etat ou une personne morale de droit public.</strong></p>
<p>Les sommes versées à ce titre sont imposées soit dans l’Etat d’exercice de l’activité (article 15 de la convention), soit dans l’Etat de résidence du bénéficiaire (article 18 de la convention).</p>
<p>Exemples :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Monsieur X, agent E.D.F., est envoyé à Singapour afin d’effectuer des travaux de conception avec les services locaux singapouriens.<br class="manualbr">Monsieur X est rémunéré par E.D.F. France.<br class="manualbr">E.D.F. étant un établissement de droit public à caractère industriel et commercial, les rémunérations allouées à Monsieur X seront imposées à Singapour.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Monsieur X, retraité de la S.N.C.F., perçoit une pension de la caisse vieillesse de la S.N.C.F. Or, Monsieur X, résident en France, a décidé de vivre à Singapour. Cette retraite se verra ainsi imposable à Singapour puisque la S.N.C.F. a le caractère d’un établissement public, industriel et commercial.</p>
<h4 class="spip">Pensions et rentes</h4>
<p>L’article 18 prévoit que les pensions de retraite de source privée ainsi que les rentes viagères restent imposables dans l’Etat dont le bénéficiaire est un résident. Cette disposition couvre également les pensions publiques auxquelles les règles de l’article 19 ne sont pas applicables.</p>
<p>En outre, la convention ne comportant aucune disposition particulière concernant les prestations de sécurité sociale, il en résulte que les pensions privées versées par la Sécurité sociale française à des résidents de Singapour échappent à l’impôt français et sont par conséquent imposées à Singapour.</p>
<p>Exemple : Monsieur X, citoyen français qui a exercé une activité salariée à Singapour, décide de venir prendre sa retraite en France. Les pensions versées par Singapour au titre de cette activité sont imposables en France.</p>
<h4 class="spip">Etudiants, stagiaires</h4>
<p>L’article 20, paragraphe 1, prévoit que les étudiants ou les stagiaires d’un Etat contractant qui séjournent temporairement dans l’autre Etat dans une université, un collège, un établissement public ou au sein d’une entreprise et qui perçoivent des subsides d’origine étrangère à cet Etat afin de poursuivre leurs études ou leur formation ainsi que des rémunérations complémentaires de cet Etat sont exonérés d’impôt dans cet Etat.</p>
<p>Le paragraphe 2 du même article dispose qu’une personne d’un Etat qui séjourne dans l’autre Etat <strong>pendant une période allant jusqu’à trois ans à seule fin d’y poursuivre ses études ou ses recherches </strong>et qui est <strong>bénéficiaire d’une bourse ou d’une allocation </strong>provenant d’une organisation charitable, scientifique, éducative, religieuse ou d’un programme d’assistance technique mené par l’un des deux Etats, est exonéré d’impôt dans cet Etat pour le montant de cette aide ainsi que pour celui des rémunérations versées dans cet Etat dans le cadre de sa formation ou de ses études provenant de services personnels.</p>
<p>Par ailleurs, le paragraphe 3 indique qu’une personne, résident d’un Etat, qui séjourne dans l’autre Etat <strong>pendant douze mois maximum uniquement en tant qu’employé </strong>de cet autre Etat ou d’une entreprise de cet autre Etat afin de recevoir une formation pratique et qui perçoit des subsides pour couvrir ses frais d’entretien du premier Etat ainsi que des rémunérations provenant de services rendus même de manière accessoire en relation avec ses études de l’autre Etat, est exonérée d’impôt dans cet Etat.</p>
<h4 class="spip">Enseignants</h4>
<p>L’article 21 précise que les rémunérations versées aux enseignants ou aux chercheurs résidents d’un Etat se rendant dans l’autre Etat en vue d’y exercer une activité pédagogique ou de recherches pendant une période <strong>ne dépassant pas deux ans</strong>, dans une université, un collège, une école ou un autre établissement restent imposables dans l’Etat d’origine.</p>
<h4 class="spip">Autres catégories de revenus</h4>
<h5 class="spip">Bénéfices industriels et commerciaux</h5>
<p>L’article 7, paragraphe 1, dispose que les entreprises industrielles et commerciales sont imposables sur le territoire où se trouve un établissement stable.</p>
<h3 class="spip"><a id="sommaire_3"></a>Revenus non commerciaux</h3>
<p>Les revenus que les professionnels du spectacle ainsi que les sportifs réalisent en cette qualité dans l’un des deux Etats restent imposables dans l’Etat d’exercice de l’activité selon les dispositions de l’article 17, paragraphe 1 de la convention.</p>
<p>L’article 13, paragraphe 1, pose en principe que les revenus non commerciaux (redevances et droits d’auteur) sont imposables dans l’Etat de résidence du bénéficiaire, à l’exception toutefois soulevée au paragraphe 3 du même article concernant les redevances de droit d’auteur sur une oeuvre littéraire ou artistique incluant les films cinématographiques, les films et les bandes de télévision ou de radio ou pour des informations ayant trait à une expérience acquise dans le domaine commercial, qui restent imposables dans l’Etat d’origine de ces revenus.</p>
<h5 class="spip">Revenus immobiliers</h5>
<p>L’article 6, paragraphe 1, dispose que les revenus des biens immobiliers sont imposables dans l’Etat où ils sont situés. Le paragraphe 2 du même article détermine la notion de biens immobiliers en précisant qu’elle englobe également l’exploitation ou la concession de l’exploitation de mines, de puits de pétrole, de carrières. Les dispositions du paragraphe 1 s’appliquent aux revenus de biens agricoles.</p>
<p>Cette règle s’applique également aux gains provenant de la cession ou de l’échange desdits biens ou droits selon les dispositions de l’article 14, paragraphe 1.</p>
<h5 class="spip">Revenus de capitaux mobiliers</h5>
<p><strong>Les dividendes</strong></p>
<p>Ce terme désigne les revenus provenant d’actions, actions ou bons de jouissance, parts de mines, parts de fondateur ou autres parts bénéficiaires, à l’exception des créances et les revenus d’autres parts sociales assimilés aux revenus d’actions.</p>
<p>De manière générale, l’article 10, paragraphe 1, reprend la règle suivant laquelle les dividendes payés par une société qui est un résident d’un Etat contractant à un résident de l’autre Etat sont imposables dans cet autre Etat.</p>
<p>Toutefois, au paragraphe 2-a et b, cet article permet à une société, résident de France, qui est à l’origine du versement des dividendes auprès de bénéficiaires résidents de Singapour de les imposer à un taux qui n’excède pas 15%. Lorsque le bénéficiaire effectif est une société qui détient directement ou indirectement au moins 10% du capital de la société distributrice, l’Etat de la source prélève une retenue de 10%.</p>
<p>Par ailleurs, les dispositions du paragraphe 4 précisent que les dividendes distribués par des sociétés résidentes de Singapour à un résident de France sont exonérées à Singapour de tout impôt qui s’ajouterait à l’impôt sur le revenu perçu sur les bénéfices de la société distributrice. Au cas où une imposition spécifique serait instituée à l’avenir à Singapour, l’impôt qui pourrait être perçu s’élèverait à 15% en général et 10% s’il s’agit d’un actionnaire détenant 10% du capital de la société distributrice.</p>
<p><strong>Les intérêts</strong></p>
<p>Ce terme désigne les revenus des fonds publics, des obligations d’emprunts, des obligations d’emprunts et des créances de toute nature ainsi qu’aux intérêts produits par des titres négociables, les bons de caisse et les intérêts de créances ordinaires.</p>
<p>L’article 12, paragraphe 1 précise que les intérêts provenant d’un Etat et payés à un résident de l’autre Etat ne sont imposables que dans cet autre Etat si ce résident est le bénéficiaire effectif. Toutefois, le paragraphe 2 prévoit que les intérêts peuvent être imposés dans le pays de la source à un taux ne pouvant dépasser 10%.</p>
<h3 class="spip"><a id="sommaire_4"></a>Elimination de la double imposition</h3>
<p>L’élimination de la double imposition pour les résidents de France qui perçoivent des revenus de source singapourienne s’opère aux termes du paragraphe 2 de l’article 24 selon soit la méthode du crédit d’impôt, soit celle de l’exemption.</p>
<ul class="spip">
<li>Le crédit d’impôt à déduire est égal au montant de l’impôt payé à Singapour (cas des dividendes, des intérêts, redevances, rémunérations des artistes et sportifs) ;</li>
<li>L’exemption s’applique aux autres revenus de source singapourienne pour lesquels la méthode du crédit ne s’applique pas. Dans ce cas et afin de déterminer le taux de l’impôt applicable aux autres revenus des contribuables qui sont imposables en France, il peut être tenu compte des revenus exonérés par l’application du taux effectif ou moyen.</li></ul>
<p>Le calcul du taux effectif se décompose tout d’abord par la détermination d’une cotisation de base correspondant à l’ensemble des revenus de source française et(ou) étrangère passible de l’impôt français suivant les règles de la législation interne.</p>
<p>Ensuite, l’impôt exigible sera égal au produit de cette cotisation de base par le rapport entre le montant net total des revenus conventionnellement imposables en France et le montant total du revenu net d’après lequel le calcul de ladite cotisation de base a été effectué.</p>
<p>Au résultat ainsi obtenu peuvent être appliquées les réfactions prévues par la loi interne (crédit ou réduction d’impôt).</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/fiscalite/article/convention-fiscale). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
