# Vie pratique

<h2 class="rub23008">Logement</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/vie-pratique/#sommaire_1">Conditions de location</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/vie-pratique/#sommaire_2">Electricité</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/vie-pratique/#sommaire_3">Electroménager</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Conditions de location</h3>
<p><strong>Tananarive</strong></p>
<p>Le marché locatif est étroit ; plusieurs semaines de recherche peuvent être nécessaires pour trouver un logement correspondant à ses souhaits. Le plus souvent, ce logement sera non meublé. Le coût d’une location peut varier considérablement d’un quartier à l’autre, voire d’un propriétaire à l’autre.</p>
<p>Il n’y a pas de versement de pas de porte et il n’est pas besoin de procéder à une reprise. Un état des lieux doit être effectué.</p>
<p>La durée d’un bail est de deux ou trois ans. Le règlement du loyer s’effectue mensuellement ou trimestriellement. Dans le cas d’une location par agence, le montant de la commission correspond généralement à un mois de loyer.</p>
<p>Pour prévenir le cambriolage du domicile, de plus en plus fréquent, il est conseillé de ne pas résider dans une villa isolée, de veiller à ce que portes et fenêtres soient équipées de grilles, de prévoir éventuellement un système d’alarme et de s’assurer un service de gardiennage.</p>
<p><strong>Montant moyen des loyers réglés par la communauté expatriée : </strong>de 400 à 1500 € pour un appartement ; de 1000 à 2500 € pour une villa.</p>
<p>Le chauffage en hiver est nécessaire. L’eau et l’électricité sont à la charge du locataire. Les charges représentent une moyenne mensuelle de 350 € pour un couple avec deux enfants.</p>
<p><strong>Diego-Suarez</strong></p>
<p>Les quartiers résidentiels se trouvent Cité BTM (à l’entrée de la ville, sur la route de l’aéroport) ou rue Colbert (au centre-ville). Il n’existe pas de marché immobilier organisé : en l’absence d’agences immobilières, les propriétaires cherchent eux-mêmes des locataires et négocient directement avec ceux-ci. La durée des baux, variable, est généralement d’un an renouvelable par tacite reconduction.</p>
<p>Exception faite des quartiers populaires (Tanambao, Grand-Pavois, …), les loyers mensuels moyens sont compris dans une fourchette allant de 500 € pour une maison très petite à plus de 2500 € pour une villa de standing. Le montant du loyer est établi d’abord en fonction de la superficie et du confort et seulement ensuite de la localisation, avec une surcote pour les maisons situées dans le centre historique ou en bord de mer. Les charges ne sont pas intégrées au loyer.</p>
<p><strong>Tamatave</strong></p>
<p>Les quartiers résidentiels se situent sur le front de mer, au nord de la ville (Salazamay, Tahiti kely). Le marché local est restreint mais suffisant pour la demande.</p>
<p>Le bail, rare, est négociable. Le loyer se paye d’avance, trimestriellement.</p>
<p>Le coût du loyer, pour une villa sise dans un quartier résidentiel, oscille de 1000 à 3000 €. Le montant des charges mensuelles, pour une villa de trois ou quatre pièces, est évalué à 150 € pour l’électricité et 35 € pour l’eau.</p>
<p><strong>Pour en savoir plus</strong> (notamment agences immobilières, quartiers, etc) : site de <a href="http://www.tana-accueil.com/" class="spip_out" rel="external">Tana Accueil</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Electricité</h3>
<p>Le courant est en 220 volts ; les régulateurs de tension sont indispensables pour les appareils électriques en raison des fortes variations.</p>
<h3 class="spip"><a id="sommaire_3"></a>Electroménager</h3>
<p>Les cuisines sont assez rarement équipées.</p>
<p>Pour en savoir plus sur l’équipement sur place (adresses, etc) : site de <a href="http://www.tana-accueil.com/" class="spip_out" rel="external">Tana accueil</a>.</p>
<p>Si la climatisation n’est pas nécessaire à Tananarive, chauffages électriques et cheminées à bois y sont utilisés de mai à octobre.</p>
<p>A Tamatave et Diego-Suarez, la climatisation est recommandée de novembre à avril.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-madagascar-vie-pratique-article-pour-en-savoir-plus.md" title="Pour en savoir plus">Pour en savoir plus</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-madagascar-vie-pratique-article-loisirs-et-culture-111197.md" title="Loisirs et culture">Loisirs et culture</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-madagascar-vie-pratique-article-communications.md" title="Communications">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-madagascar-vie-pratique-article-transports.md" title="Transports">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-madagascar-vie-pratique-article-cout-de-la-vie-111194.md" title="Coût de la vie">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-madagascar-vie-pratique-article-scolarisation-111193.md" title="Scolarisation">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-madagascar-vie-pratique-article-sante.md" title="Santé">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-madagascar-vie-pratique-article-logement-111191.md" title="Logement">Logement</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/vie-pratique/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
