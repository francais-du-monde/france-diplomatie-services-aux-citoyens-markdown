# Vaccination

<h4 class="spip">Les vaccinations recommandées pour la République Dominicaine</h4>
<p>Aucune vaccination n’est obligatoire pour les voyageurs venant de France métropolitaine.</p>
<p>Néanmoins il est recommandé d’être à jour de toutes les vaccinations incluses dans le calendrier vaccinal français.</p>
<p>Il est préférable de réaliser toutes les vaccinations nécessaires avant de partir (prévoir 10 jours avant).</p>
<p>Pour les voyageurs en provenance d’une zone infectée par la fièvre jaune, il est indispensable de s’être vacciné à partir de l’âge d’un an.</p>
<p><strong>Pour des séjours courts :</strong></p>
<ul class="spip">
<li>Mise à jour des vaccinations contre la diphtérie, tétanos, poliomyélite (DTP)</li>
<li>Hépatite A ; à partir de l’âge de 50 ans, une recherche préalable des anticorps sériques (IgG) est justifiée.</li></ul>
<p><strong>Pour des séjours prolongés et/ou à risques :</strong></p>
<ul class="spip">
<li>Hépatite A</li>
<li>Hépatite B</li>
<li>BCG pour les nouveaux nés</li>
<li>Rougeole, oreillons et rubéole (ROR) dès l’âge de 12 mois</li></ul>
<p><strong>Remarque :</strong></p>
<p>Sur place, tous les vaccins sont disponibles en cas de besoin. Le ministère de la Santé de la République Dominicaine dispose de centres agréés pour la vaccination gratuite contre la fièvre jaune et la rage dans tout le pays.</p>
<p>Pour en savoir plus, lisez notre rubrique thématique sur la <a href="services-aux-citoyens-preparer-son-expatriation-sante.md" class="spip_in">Santé</a>.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/entree-et-sejour/article/vaccination). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
