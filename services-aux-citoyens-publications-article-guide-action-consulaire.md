# Guide « L’action consulaire »

<iframe src="http://www.diplomatie.gouv.fr/fr/spip.php?page=pdfjs&amp;id_document=93666" width="100%" height="600" class="lecteurpdf lecteufpdf-93666spip_documents"></iframe>
<p>Pour imprimer le document, téléchargez la version PDF :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/action_consulaire_cle075f4d.pdf" class="spip_in" type="application/pdf">Guide « L’action consulaire » (PDF - 700 Ko)</a></p>
<p><i>Mise à jour : juin 2016</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/publications/article/guide-action-consulaire). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
