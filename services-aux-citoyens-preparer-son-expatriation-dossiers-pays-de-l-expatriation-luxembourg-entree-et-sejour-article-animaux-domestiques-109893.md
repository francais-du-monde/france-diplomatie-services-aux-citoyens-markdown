# Animaux domestiques

<p>L’expédition vers un autre Etat membre de l’Union européenne (à l’exception de l’Irlande, Malte, la Suède et le Royaume-Uni) ou le transit vers la France, est soumise à plusieurs règles.</p>
<p>Les chiens, les chats et les furets doivent satisfaire aux conditions suivantes :</p>
<ul class="spip">
<li>Identification par tatouage ou par puce électronique (transpondeur conforme à la norme ISO 11784 ou à l’annexe A de la norme ISO 11785) ;</li>
<li>Vaccination antirabique en cours de validité (primo-vaccination et rappels)</li>
<li>Passeport délivré par un vétérinaire habilité par l’autorité compétente (en France, un vétérinaire titulaire d’un mandat sanitaire) attestant de l’identification et de la vaccination antirabique de l’animal.</li></ul>
<p>Au Luxembourg tout chien doit être déclaré par son détenteur auprès de l’administration communale de son lieu de résidence et fournir les documents suivants :</p>
<ul class="spip">
<li>un certificat délivré par un vétérinaire agréé attestant l’identification de la race ou du genre et l’identification électronique du chien ainsi que sa vaccination antirabique en cours de validité ;</li>
<li>une pièce attestant qu’un contrat d’assurance a été conclu avec une société agréée ou autorisée à opérer au Luxembourg garantissant la responsabilité civile du détenteur du chien pour les dommages causés aux tiers par l’animal.</li></ul>
<p>Une taxe annuelle (impôt) sur les chiens est perçue dans toutes les communes. En plus du contrôle de la vaccination antirabique en cours et de l’existence d’un contrat d’assurance, une déclaration est à faire, le 15 octobre de chaque année, sur un formulaire délivré par l’administration communale.</p>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-douanes-article-animaux-domestiques.md" class="spip_in">Animaux domestiques</a></li>
<li><a href="http://www.ma.public.lu/" class="spip_out" rel="external">Le site du ministère de l’Agriculture, de la Viticulture et du Développement rural du Grand-Duché de Luxembourg</a></li>
<li><a href="http://www.guichet.lu/citoyens/fr/loisirs-benevolat/animaux-compagnie/index.html" class="spip_out" rel="external">Guichet.lu - animaux de compagnie</a></li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/entree-et-sejour/article/animaux-domestiques-109893). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
