# Animaux domestiques

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/entree-et-sejour-22980/article/animaux-domestiques-111066#sommaire_1">Identification</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/entree-et-sejour-22980/article/animaux-domestiques-111066#sommaire_2">Documents à fournir</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/entree-et-sejour-22980/article/animaux-domestiques-111066#sommaire_3">Informations générales</a></li></ul>
<p>Les chiens et les chats <strong>en provenance d’un pays membre de l’Union européenne</strong> ne sont pas soumis à la mise en quarantaine à leur arrivée à Chypre s’ils remplissent les conditions suivantes :</p>
<h3 class="spip"><a id="sommaire_1"></a>Identification</h3>
<p>L’animal doit avoir un tatouage lisible ou une puce électronique (obligatoire pour les animaux identifiés après le 3 juillet 2011) répondant aux normes internationales. Dans ce dernier cas, il faudra pouvoir prouver que la puce électronique a été placée avant la première vaccination contre la rage.</p>
<h3 class="spip"><a id="sommaire_2"></a>Documents à fournir</h3>
<p>L’animal doit avoir un passeport européen délivré par un vétérinaire agréé. Celui-ci doit prouver que l’animal a été vacciné contre la rage avant l’âge de trois mois et que la (re)vaccination a eu lieu au moins un mois avant son arrivée à Chypre. Ce vaccin doit répondre aux normes internationales.</p>
<p>Le carnet de vaccination à jour devra être présenté.</p>
<p>Il faudra présenter le(s) document(s) prouvant que l’animal arrive à Chypre en provenance <strong>directe</strong> d’un pays membre de l’Union européenne.</p>
<p>Dans le cas où ces conditions ne seraient pas remplies, l’animal serait placé en quarantaine et le propriétaire devrait s’acquitter des taxes en vigueur.</p>
<p><strong>L’entrée d’animaux âgés de moins de trois mois n’est pas autorisée quel que soit le pays d’origine.</strong></p>
<p>En cas d’arrivée par avion, il est conseillé de réserver la place de l’animal sur le vol car les animaux ne sont pas acceptés sur certains vols.</p>
<p><strong>Dans tous les cas, il convient d’adresser par fax ou par mail au moins 48 heures à l’avance aux services vétérinaires chypriotes la date et l’heure d’arrivée de l’animal, le nom de la compagnie aérienne et le numéro du vol.</strong></p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Contactez la <a href="http://www.mfa.gov.cy/" class="spip_out" rel="external">Direction des services vétérinaires (Veterinary Services Department)</a> <br class="manualbr">Tél : 00 357 22 80 51 52 <br class="manualbr">Fax : 00 357 22 80 51 76<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/entree-et-sejour-22980/article/animaux-domestiques-111066#animal.health#mc#vs.moa.gov.cy#" title="animal.health..åt..vs.moa.gov.cy" onclick="location.href=mc_lancerlien('animal.health','vs.moa.gov.cy'); return false;" class="spip_mail">Courriel</a> </p>
<h3 class="spip"><a id="sommaire_3"></a>Informations générales</h3>
<p><strong>Le pays de destination se trouve dans l’Union européenne (sauf Irlande, Malte, Suède et Royaume-Uni) </strong></p>
<p>Une information très détaillée est disponible sur le site Internet du <a href="http://agriculture.gouv.fr/" class="spip_out" rel="external">ministère de l’Agriculture</a>.</p>
<p>Les chiens, les chats et les furets doivent satisfaire aux conditions suivantes :</p>
<ul class="spip">
<li>être identifiés par tatouage ou par puce électronique ;</li>
<li>être valablement vaccinés contre la rage ;</li>
<li>être titulaires d’un passeport délivré par un vétérinaire habilité attestant de l’identification et de la vaccination contre la rage de l’animal ; Le système d’identification électronique, ainsi que la reconnaissance de la validité de la vaccination contre la rage, peuvent varier d’un Etat membre à l’autre. Il est donc vivement recommandé de prendre contact avec l’<a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-etrangers-en-france/article/annuaire-des-representations-etrangeres-en-france" class="spip_in">ambassade du pays de destination</a> .</li></ul>
<p>En France, la réalisation de la primo-vaccination antirabique n’est considérée comme valable qu’à partir de 21 jours après la fin du protocole de vaccination prescrit par le fabricant. La vaccination antirabique de rappel est considérée en cours de validité le jour de sa réalisation.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Notre article thématique <a href="services-aux-citoyens-preparer-son-expatriation-douanes-article-animaux-domestiques.md" class="spip_in">Animaux domestiques</a>.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/entree-et-sejour-22980/article/animaux-domestiques-111066). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
