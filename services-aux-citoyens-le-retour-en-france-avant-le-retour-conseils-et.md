# Formalités avant le retour

<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-le-retour-en-france-avant-le-retour-conseils-et-article-demarches-utiles.md" title="Démarches utiles">Démarches utiles</a></li>
<li><a href="services-aux-citoyens-le-retour-en-france-avant-le-retour-conseils-et-article-conjoint-etranger.md" title="Conjoint étranger">Conjoint étranger</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/avant-le-retour-conseils-et/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
