# Démarches utiles

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/avant-le-retour-conseils-et/article/demarches-utiles#sommaire_1">Signaler mon départ au consulat</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/avant-le-retour-conseils-et/article/demarches-utiles#sommaire_2">M’assurer que mes documents d’état civil sont à jour</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/avant-le-retour-conseils-et/article/demarches-utiles#sommaire_3">Signaler mon changement d’adresse aux services postaux locaux</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/avant-le-retour-conseils-et/article/demarches-utiles#sommaire_4">Avoir en ma possession tous les documents qui pourront m’être utiles en France</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Signaler mon départ au consulat</h3>
<p>Avant de quitter votre pays de résidence, vous devez demander votre radiation du registre des Français auprès de votre consulat. Cette démarche peut se faire par courrier. Un certificat de radiation vous sera délivré par le consulat. Vous pouvez également solliciter une attestation de changement de résidence pour faciliter les formalités douanières concernant votre déménagement.</p>
<p>Demandez également votre radiation de la liste électorale consulaire afin de pouvoir vous inscrire facilement sur la liste électorale de la ville dans laquelle vous allez vous installer.</p>
<h3 class="spip"><a id="sommaire_2"></a>M’assurer que mes documents d’état civil sont à jour</h3>
<p>Vérifiez que les événements familiaux survenus pendant votre séjour (naissance, mariage) ont bien été transcrits sur les registres d’état civil de l’ambassade ou du consulat territorialement compétent. En cas de divorce, une mention devra être apposée sur votre acte de naissance et votre acte de mariage. Si le divorce a été prononcé à l’étranger, il doit faire l’objet d’une vérification d’opposabilité du procureur de la République dont dépend l’officier d’état civil qui a célébré le mariage pour les mariages célébrés en France, et du procureur de la République près le tribunal de grande instance de Nantes pour les mariages célébrés à l’étranger. Dans certaines conditions, les divorces prononcés dans un pays de l’Union européenne (à l’exception du Danemark) sont dispensés de la procédure de vérification d’opposabilité de la décision.</p>
<h3 class="spip"><a id="sommaire_3"></a>Signaler mon changement d’adresse aux services postaux locaux</h3>
<p>Certains organismes (banque, par exemple) ou institutions administratives locales peuvent avoir besoin de vous contacter après votre départ du pays. Il est donc conseillé de signaler votre départ à la poste locale ainsi qu’aux institutions administratives pertinentes (services fiscaux, registre de population le cas échéant).</p>
<h3 class="spip"><a id="sommaire_4"></a>Avoir en ma possession tous les documents qui pourront m’être utiles en France</h3>
<p>Il est souvent difficile d’obtenir certains justificatifs d’institutions étrangères, à distance, une fois rentré en France. Vous devez donc veiller à les obtenir avant votre départ et les garder soigneusement pendant votre déménagement.</p>
<p>Pensez notamment à conserver vos contrats et certificats de travail, vos bulletins de salaire ainsi que vos avis d’imposition locaux.</p>
<p>Si vous revenez d’un pays de l’Union européenne ou d’un pays ayant signé une convention de sécurité sociale avec la France, demandez également auprès des organismes de protection sociale de votre pays de résidence, les formulaires qui attesteront de la portabilité de vos droits.</p>
<p>Si la loi locale le permet, il peut être utile de demander une copie de vos dossiers médicaux. Il est indispensable de conserver la liste des vaccinations obligatoires reçues par vos enfants mineurs.</p>
<p><i>Mise à jour : janvier 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/avant-le-retour-conseils-et/article/demarches-utiles). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
