# Fiscalité

<h2 class="rub22905">Fiscalité du pays</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/fiscalite/#sommaire_1">Inscription auprès du fisc</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/fiscalite/#sommaire_2">Quitus fiscal</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/fiscalite/#sommaire_3">Solde du compte en fin de séjour</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/fiscalite/#sommaire_4">Impôts locaux</a></li></ul>
<p>Afin d’assurer la compétitivité et l’attractivité de la Cité-Etat, la fiscalité de Singapour est particulièrement favorable aux contribuables, qu’ils soient des particuliers ou des entreprises.</p>
<p>Les personnes physiques et morales résidant en France peuvent télécharger le formulaire permettant d’éviter la double imposition via le site Internet de l’administration fiscale de Singapour, l’<a href="http://www.iras.gov.sg/" class="spip_out" rel="external">Inland Revenue Authority Of Singapore (IRAS)</a>.</p>
<h3 class="spip"><a id="sommaire_1"></a>Inscription auprès du fisc</h3>
<p>En fonction de la durée de présence à Singapour, l’administration fiscale singapourienne définit le statut de résidence fiscale.</p>
<p>§ Les personnes dont le nombre de jours de présence physique à Singapour est inférieur à 60 jours sont considérées comme non résidentes et sont exemptées d’impôt.</p>
<ul class="spip">
<li>Les personnes dont le nombre de jours de présence physique à Singapour se situe entre 60 et 183 jours sont également considérées comme non résidentes et sont soumises à un taux d’imposition fixe de 15 %.</li>
<li>Les personnes qui séjournent plus de 183 jours sur le sol singapourien sont considérées comme résidentes et sont soumises à un taux d’imposition progressif de 0 à 20 %.</li></ul>
<p>Comme en France, les revenus d’une année sont imposables l’année suivante. Les nouveaux contribuables doivent se faire enregistrer auprès de l’IRAS. Un formulaire est envoyé par l’IRAS avant le 30 mars de l’année suivante. Cette déclaration doit être remplie avant le 15 avril suivant. Des imprimés téléchargeables sont également disponibles <a href="https://mytax.iras.gov.sg/ESVWeb/default.aspx" class="spip_out" rel="external">en ligne</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Quitus fiscal</h3>
<p>Un quitus fiscal est exigé avant de quitter Singapour.</p>
<h3 class="spip"><a id="sommaire_3"></a>Solde du compte en fin de séjour</h3>
<p>Un expatrié doit solder son compte en fin de séjour à Singapour.</p>
<h3 class="spip"><a id="sommaire_4"></a>Impôts locaux</h3>
<h4 class="spip">Impôt sur le revenu des sociétés</h4>
<p>Depuis mars 2010, le taux d’imposition sur les sociétés est fixé à 17 % alors qu’il s’établissait encore à 26% en 2000. Il est calculé en appliquant le taux légal au bénéfice imposable. Il s’agit d’un impôt définitif, ce qui signifie que les actionnaires ne sont pas imposés sur les dividendes versés par la société, dans la mesure où celle-ci a déjà payé l’impôt correspondant.</p>
<p>Outre ce taux d’imposition réduit, les sociétés peuvent bénéficier de certaines déductions fiscales et de taux réduits d’imposition. Par exemple, toute société récemment immatriculée à Singapour bénéficie pour chacune de ses trois premières années d’imposition d’une franchise d’impôt sur les premiers 100 000 SGD de revenus imposables et d’un abattement de 50% sur les 200 000 SGD de revenus imposables suivants.</p>
<p>Pour en savoir plus sur les <a href="https://www.iras.gov.sg/irasHome/default.aspx" class="spip_out" rel="external">conditions d’éligibilité aux mesures fiscales incitatives</a>.</p>
<p>Singapour n’impose pas le capital des sociétés.</p>
<p>Applicable pour les années d’imposition 2011 à 2015, le <i>Productivity and Innovation Credit</i> permet aux sociétés éligibles de bénéficier de déductions fiscales massives équivalentes à 250% des dépenses effectuées dans le cadre d’activités de recherche et développement, d’enregistrement ou d’acquisition de droits de propriété intellectuelle, d’investissements dans l’automatisation, la formation professionnelle et les investissements effectués dans le domaine du design à Singapour. Cette mesure est particulièrement destinée aux PME.</p>
<h4 class="spip">Impôt sur le revenu des personnes physiques</h4>
<p>Les revenus de source singapourienne sont imposables à Singapour quel que soit le lieu de paiement ou la localisation géographique de l’employeur. Les revenus de source non singapourienne sont également imposables à Singapour s’ils sont rapatriés mais ils bénéficient généralement d’un crédit d’impôt.</p>
<p>L’assiette de l’impôt sur le revenu prend en compte :</p>
<ul class="spip">
<li>les salaires,</li>
<li>les indemnités,</li>
<li>les allocations et les commissions,</li>
<li>les avantages en nature (mise à disposition d’un logement, de meubles, d’une voiture, de billets d’avion, etc.). Pour les salaires versés en France, c’est le salaire brut qui doit être déclaré et qui sera pris en compte dans la base imposable.</li></ul>
<p>A l’image de l’imposition des sociétés, la pression fiscale sur les personnes physiques est également très allégée. L’intégralité des démarches et des conditions d’application est accessible <a href="https://www.iras.gov.sg/irasHome/default.aspx" class="spip_out" rel="external">en ligne</a>.</p>
<p><strong>Année d’imposition, déclaration et modalités de paiement</strong></p>
<p><strong>Tableau récapitulatif</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Impôt des personnes physiques</td>
<td>Paramètre de calcul</td></tr>
<tr class="row_even even">
<td>Définition des revenus imposables</td>
<td>La base imposable comprend :<br> <br>
<ul class="spip">
<li>les salaires, indemnités, allocations, commissions, avantages en nature</li>
<li>les bénéfices industriels et commerciaux provenant d’une activité indépendante exercée à Singapour</li>
<li>les revenus de placement</li>
<li>les loyers</li>
<li>les redevances</li>
<li>les pensions de retraites</li>
<li>les plus-values immobilières à court terme (supérieur à 3 ans)</li></ul></td></tr>
<tr class="row_odd odd">
<td>Taux de l’impôt</td>
<td>Résident : taux progressif de 0 à 20 % applicable aux résidents
<p>Non résidents : taux fixe de 15 %</p>
</td></tr>
<tr class="row_even even">
<td>Paiement de l’impôt</td>
<td>- en une seule fois dans le mois qui suit l’émission de l’avis d’imposition
<p>- par acomptes</p>
</td></tr>
</tbody>
</table>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.ambafrance-sg.org/Service-Economique-Regional" class="spip_out" rel="external">Service économique français à Singapour</a></li>
<li><a href="http://www.mof.gov.sg/" class="spip_out" rel="external">Ministry of Finance</a><br class="manualbr">Tél. : (65) 62 25 99 11 - Télécopie : (65) 63 32 74 35</li>
<li><a href="http://www.iras.gov.sg/" class="spip_out" rel="external">Inland Revenue Authority of Singapore (IRAS)</a><br class="manualbr">Tél. : (65) 63 56 82 33 - Fax : (65) 63 51 21 31</li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-singapour-fiscalite-article-convention-fiscale.md" title="Convention fiscale">Convention fiscale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-singapour-fiscalite-article-fiscalite-du-pays.md" title="Fiscalité du pays">Fiscalité du pays</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/fiscalite/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
