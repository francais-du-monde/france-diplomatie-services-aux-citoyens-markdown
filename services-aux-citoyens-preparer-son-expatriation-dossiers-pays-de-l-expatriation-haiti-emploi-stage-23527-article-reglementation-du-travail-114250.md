# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/emploi-stage-23527/article/reglementation-du-travail-114250#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/emploi-stage-23527/article/reglementation-du-travail-114250#sommaire_2">Contrat de travail – spécificités</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/emploi-stage-23527/article/reglementation-du-travail-114250#sommaire_3">Fêtes légales</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<p>Les droits du travail sont déterminés et réglementés par le Code du travail actualisé par décret en 1984.</p>
<p>Le ministère des Affaires sociales est chargé de faire respecter les dispositions de cette législation et de maintenir un climat de confiance entre le patronat et les ouvriers.</p>
<p>Le code du travail est disponible en librairie.</p>
<h3 class="spip"><a id="sommaire_2"></a>Contrat de travail – spécificités</h3>
<p>La législation haïtienne en vigueur en matière de travail et, plus particulièrement, le Code du travail haïtien, article 16, mentionne que :<i>« Le contrat de travail est exprès ou tacite, verbal ou écrit, et pourra être conclu pour une durée soit déterminée, soit indéterminée ».</i></p>
<p>Pour plus de détails, vous pouvez vous referez chapitre I, articles 15-25 du Code du travail haïtien.</p>
<h3 class="spip"><a id="sommaire_3"></a>Fêtes légales</h3>
<p>Il y a environ 12 jours fériés par an, déterminés chaque année par le gouvernement haïtien.</p>
<p>Si l’employé doit travailler durant un jour férié, il est payé avec une majoration de 50%, sans préjudice de l’augmentation prévue pour le travail de nuit, les heures supplémentaires et le paiement du repos hebdomadaire.</p>
<p><i>Mise à jour : août 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/emploi-stage-23527/article/reglementation-du-travail-114250). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
