# Vie pratique

<h2 class="rub23177">Logement</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/vie-pratique/#sommaire_1">Où se loger ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/vie-pratique/#sommaire_2">Conditions de location</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/vie-pratique/#sommaire_3">Electricité</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/vie-pratique/#sommaire_4">Electroménager</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Où se loger ?</h3>
<p>Trouver un logement à louer à Madrid est aussi compliqué que dans n’importe quelle capitale européenne. Les prix des logements et les loyers varient considérablement selon la région et la ville. Cependant, les loyers sont généralement plus élevés dans les principales villes du pays : Madrid, Barcelone, Bilbao, Vitoria et Saint-Sébastien. En outre, vivre dans le centre des plus grandes villes est toujours plus cher que de vivre dans les quartiers ou villes périphériques qui sont généralement bien desservies par les trains ou les bus qui permettent de se rendre facilement dans le centre-ville en moins de 30 minutes. Le marché locatif en Espagne est par ailleurs extrêmement restreint et concerne près de 10 % du parc immobilier total.</p>
<p>Il convient de consulter les <strong>annonces dans les journaux </strong>locaux et les magazines, publiées sous la rubrique "Alquiler". Les meilleurs jours pour l’achat du journal sont les vendredi et samedi :</p>
<ul class="spip">
<li><a href="http://www.elpais.com/" class="spip_out" rel="external">El Pais</a></li>
<li><a href="http://www.elmundo.es/" class="spip_out" rel="external">El Mundo</a></li>
<li><a href="http://www.lavanguardia.es/" class="spip_out" rel="external">Lavanguardia</a></li></ul>
<p>La meilleure source de recherche est désormais internet. Les deux sites les plus consultés (ils n’existent qu’en espagnol) sont :</p>
<ul class="spip">
<li><a href="http://www.idealista.com/" class="spip_out" rel="external">Idealista.com</a></li>
<li><a href="http://www.fotocasa.es/" class="spip_out" rel="external">Fotocasa.es</a></li></ul>
<p>On trouve sur ces sites beaucoup d’appartements proposés par des agences immobilières et quelques-uns directement par des particuliers.</p>
<p>A Madrid, les quartiers résidentiels se situent à la fois au centre de la ville (Salamanca), dans la proche banlieue (Arturo Soria) ou la grande banlieue (Mirasierra Majadahonda, Pozuelo, Aravaca). Le montant des locations dépend plus de la qualité du logement que de sa situation.</p>
<p>A Barcelone, il y a plusieurs quartiers résidentiels, dont Pedralbes, Sarria et San Gervasio.</p>
<p>A Bilbao, la plupart des étrangers résident en dehors de la ville, à Guecho où la qualité de vie est meilleure.</p>
<p>De même à Séville, les quartiers résidentiels se situent à la périphérie de la ville (Sevilla Este, Helipolis) et dans certaines communes autour de Séville dans la région de l’Aljarafe et à Dos Hermanas.</p>
<p>Les <strong>agences immobilières </strong>(<i>Agentes Propriedad Inmobiliaria</i>) permettent de traiter avec un agent plutôt que directement, surtout concernant les contrats de bail. La commission d’une agence est habituellement une fois le montant mensuel du loyer.</p>
<p>Il existe des possibilités de logement en <a href="http://www.reaj.com/" class="spip_out" rel="external">auberges de jeunesse</a> (<i>albergues juveniles</i>).</p>
<p>Comme dans de nombreux pays, la <strong>colocation </strong>(piso compartido) est très répandue (environ 350 € par mois pour une chambre).</p>
<p>Sites de colocation (en espagnol) :</p>
<ul class="spip">
<li><a href="http://www.compartepiso.com/" class="spip_out" rel="external">Compartepiso.com</a></li>
<li><a href="http://www.pisocompartido.com/" class="spip_out" rel="external">Pisocompartido.com</a></li>
<li><a href="http://www.compartir-piso.com/" class="spip_out" rel="external">Compartir-piso.com</a></li></ul>
<p>Les <strong>cités universitaires</strong> peuvent proposer des logements pour un prix intéressant (environ 300 € par mois). Se renseigner auprès des universités et <a href="http://www.residenciascampus.com/" class="spip_out" rel="external">Residenciascampus.com</a>.</p>
<p>Les <strong>mairies</strong> disposent également d’un service logement pouvant vous aider dans vos recherches :</p>
<ul class="spip">
<li><a href="http://www.bcn.cat/castella/ehome.htm" class="spip_out" rel="external">Mairie de Barcelone</a></li>
<li><a href="http://www.munimadrid.es/" class="spip_out" rel="external">Mairie de Madrid</a></li>
<li><a href="http://www.sevilla.org/" class="spip_out" rel="external">Mairie de Séville</a></li></ul>
<p>Le programme de "bourse de logement jeune" (<i><a href="http://www.provivienda.org/programas/alquiler_jovenes.php" class="spip_out" rel="external">Bolsa de Vivienda joven en Alquiler</a></i>) offre une information générale sur la location des logements, une information juridique spécialisée, ainsi qu’une bourse d’appartements en location pour les jeunes (18/35 ans) à des prix inférieurs à ceux du marché. Se renseigner auprès des services de la jeunesse des communautés autonomes.</p>
<p>Enfin de nombreux sites internet proposent des annonces immobilières :</p>
<ul class="spip">
<li><a href="http://www.segundamano.es/" class="spip_out" rel="external">Segundamano.es</a></li>
<li><a href="http://www.bcnclub.net/" class="spip_out" rel="external">Bcnclub.net</a> (site français proposant de visionner les quartiers de Barcelone)</li>
<li><a href="http://www.mviv.es/" class="spip_out" rel="external">Site du ministère du logement</a></li></ul>
<p><strong>Exemples de prix moyen de location - Bilbao</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Type de logement</td>
<td>loyer mensuel en quartier résidentiel</td>
<td>loyer en banlieue</td></tr>
<tr class="row_even even">
<td>studio (très peu de studio ou petits appartements)</td>
<td>850 €</td>
<td>800 €</td></tr>
<tr class="row_odd odd">
<td>3 pièces</td>
<td>1150 €</td>
<td>1000 €</td></tr>
<tr class="row_even even">
<td>5 pièces</td>
<td>1500 €</td>
<td>1400 €</td></tr>
</tbody>
</table>
<p>Très peu de studios ou petits appartements sont disponibles à Bilbao. Les propriétaires ont coutume de louer des appartements meublés. Il est donc très difficile de trouver des appartements vides dans cette ville. Les cuisines sont équipées (cuisinière, lave-linge, réfrigérateur, parfois lave-vaisselle).</p>
<p><strong>Barcelone</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Type de logement</td>
<td>loyer en quartier résidentiel</td>
<td>loyer en banlieue</td></tr>
<tr class="row_even even">
<td>studio</td>
<td>600 €</td>
<td>500€</td></tr>
<tr class="row_odd odd">
<td>3 pièces</td>
<td>1500 €</td>
<td>1200 €</td></tr>
<tr class="row_even even">
<td>5 pièces</td>
<td>de 3000 à 4000 €</td>
<td>de 3000 à 4000 €</td></tr>
</tbody>
</table>
<p><strong>Madrid</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Type de logement</td>
<td>loyer mensuel en quartier résidentiel</td>
<td>loyer en banlieue</td></tr>
<tr class="row_even even">
<td>studio</td>
<td>900 €</td>
<td>800 €</td></tr>
<tr class="row_odd odd">
<td>3 pièces</td>
<td>1100 €</td>
<td>1000 €</td></tr>
<tr class="row_even even">
<td>5 pièces</td>
<td>1700 €</td>
<td>1600 €</td></tr>
<tr class="row_odd odd">
<td>villa</td>
<td>2800 €</td>
<td>2500 €</td></tr>
</tbody>
</table>
<p><strong>Séville</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Type de logement</td>
<td>loyer mensuel en quartier résidentiel</td>
<td>loyer en banlieue</td></tr>
<tr class="row_even even">
<td>studio</td>
<td>600 €</td>
<td>450 €</td></tr>
<tr class="row_odd odd">
<td>3 pièces</td>
<td>1400 €</td>
<td>900 €</td></tr>
<tr class="row_even even">
<td>5 pièces</td>
<td>2000 €</td>
<td>1300 €</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_2"></a>Conditions de location</h3>
<p><strong>Bail</strong></p>
<p>Pour louer un logement il convient de conclure un contrat de bail avec le propriétaire. Bien que le contrat oral soit entièrement valable et légal, il est toujours conseillé de conclure un contrat écrit dans lequel figureront l’identité du propriétaire et du locataire, une description du logement, la durée du contrat, le montant du loyer et autres clauses applicables. En principe, le locataire et le propriétaire peuvent fixer librement la durée du bail. Si le bail est conclu pour une durée inférieure à cinq ans, il sera reconduit chaque année pendant cinq ans sauf si le locataire fait part au propriétaire (moyennant un préavis d’au moins 30 jours avant l’échéance) de son intention de quitter les lieux. En l’absence de durée établie, le contrat de bail sera réputé conclu pour une durée d’un an, reconductible d’année en année jusqu’à un maximum de cinq ans.</p>
<p>A la signature du bail, le locataire doit verser au propriétaire une caution en espèces équivalente à une ou deux mensualités. Le loyer se négocie librement entre les deux parties. Le loyer versé chaque mois est indexé sur l’indice des prix à la consommation.</p>
<h3 class="spip"><a id="sommaire_3"></a>Electricité</h3>
<p>L’équipement électrique : courant alternatif, 220 V, 50 Hz, les prises de courant sont de type européen.</p>
<h3 class="spip"><a id="sommaire_4"></a>Electroménager</h3>
<p>Les cuisines sont en général équipées. On y trouve fréquemment plaques de cuisson et four, réfrigérateur et congélateur, et parfois lave-linge et lave-vaisselle.</p>
<p><strong>Chauffage et climatisation</strong></p>
<p>Le moyen de chauffage le plus couramment utilisé est le gaz de ville ou l’électricité.</p>
<p>A Bilbao, La climatisation n’est pas nécessaire.</p>
<p>A Barcelone, Climatisation et chauffage sont nécessaires.</p>
<p>A Madrid, Climatisation nécessaire en été et chauffage en hiver.</p>
<p>A Séville, climatisation nécessaire en été.</p>
<p><i>Mise à jour : avril 2014</i></p>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/vie-pratique/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
