# Loisirs et culture

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/vie-pratique/article/loisirs-et-culture#sommaire_1">Tourisme</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/vie-pratique/article/loisirs-et-culture#sommaire_2">Activités culturelles</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/vie-pratique/article/loisirs-et-culture#sommaire_3">Sports</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/vie-pratique/article/loisirs-et-culture#sommaire_4">Télévision – Radio</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/vie-pratique/article/loisirs-et-culture#sommaire_5">Presse Française</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Tourisme</h3>
<p>L’Australie, sixième pays du monde par sa superficie, est une destination touristique très prisée. Les visiteurs viennent principalement de Nouvelle-Zélande, du Japon, de Singapour, de Chine, de Corée, de Malaisie, de Hong Kong, des Etats-Unis, du Canada, du Royaume-Uni, d’Allemagne et de France.</p>
<p>Pays jeune et tardivement colonisé, l’Australie regorge de somptueux sites et paysages naturels. De par l’étendue du pays, les différents Etats sont très diversifiés : très belles plages de sable blanc, espaces désertiques, forêts tropicales et montagnes, parcs nationaux riches en flore et faune locales. De plus, le climat largement tempéré avec un taux élevé d’ensoleillement permet de nombreuses activités de plein air. Les lieux les plus visités sont l’opéra de Sydney, Uluru et la grande barrière de corail.</p>
<p>Chaque Etat présente des intérêts touristiques particuliers :</p>
<ul class="spip">
<li><a href="http://www.visitnsw.com/" class="spip_out" rel="external">Nouvelle-Galles du Sud</a> : Sydney et ses environs, Blue Mountains et Broken Hill</li>
<li><a href="http://www.visitcanberra.com.au/" class="spip_out" rel="external">Canberra</a> : Snowy Mountains et parc national du Mont Kosciusko</li>
<li><a href="http://www.queenslandholidays.com.au/" class="spip_out" rel="external">Queensland</a> et la barrière de corail</li>
<li><a href="http://en.travelnt.com/" class="spip_out" rel="external">Territoire du nord</a> : Ayers Rock, Alice Springs et Kakadu National Park</li>
<li><a href="http://www.westernaustralia.com/" class="spip_out" rel="external">Australie occidentale</a> : massif des Kimberley, Kalgoorlie et les " Gold Fields "</li>
<li><a href="http://fr.southaustralia.com/" class="spip_out" rel="external">Australie méridionale</a> : Adélaïde et Kangaroo Island</li>
<li><a href="http://www.visitvictoria.com/" class="spip_out" rel="external">Victoria</a> : Melbourne et de nombreux parcs nationaux</li>
<li><a href="http://fr.discovertasmania.com/" class="spip_out" rel="external">Tasmanie</a> : parc national de Cradle Mountain</li></ul>
<p>Il est recommandé de prévenir les autorités de police de tout déplacement dans le " bush ". Il convient également de vérifier son niveau d’essence, d’emporter de l’eau, quelques provisions et une protection solaire. Le pays est vaste et faiblement peuplé. Par conséquent, le risque de n’être pas secouru à temps en cas de problème est très élevé. Ces précautions élémentaires sont prises par tout Australien voyageant à l’intérieur du pays. La compagnie d’assurances de <a href="http://www.nrma.com.au/" class="spip_out" rel="external">l’association des automobilistes et des routes nationales</a> (<i>National Roads and Motorists’ Association </i>- NMRA) édite des brochures très précieuses.</p>
<p><strong>Principales routes touristiques</strong></p>
<p><strong>La route de l’Est de Cairns à Sydney</strong><br class="manualbr">La durée moyenne de ce circuit est de deux semaines. Il comprend la visite de plusieurs îles paradisiaques et très souvent trois jours de voile avec plongée sous marine au large des côtes. Après une étape dans l’intérieur du pays (<i>outback</i>), plusieurs villes sont visitées (Brisbane ou encore <i>Surfers Paradise</i>). La dernière étape, avant de rejoindre Sydney, inclut les Blue Mountains.</p>
<p><strong>La route du centre de Sydney à Uluru</strong><br class="manualbr">La durée moyenne de ce circuit est de deux semaines. La première étape est la capitale Canberra, suivie d’un parcours longeant plusieurs rivières et lacs et se termine par Melbourne. Vous rejoignez ensuite Adelaide par la <i>Great Ocean Road</i>. Puis d’Adélaïde jusqu’à Alice Springs, le visiteur est plongé au cœur d’un paysage très différent composé d’une zone aride et d’un désert rouge. Uluru se situe à environ deux heures en voiture d’Alice Springs.</p>
<p><strong>La route de l’Ouest de Perth à Darwin</strong><br class="manualbr">La durée moyenne de ce circuit est de quatre semaines. Il est généralement très apprécié des touristes puisqu’il s’agit d’un complet retour aux sources incluant camping dans la brousse, découverte de la faune sauvage et visites d’immenses parcs nationaux et de magnifiques gorges.<br class="manualbr">Le passé aborigène remonte à plus de 50 000 ans mais il est encore mal connu car il n’a légué aux historiens que quelques peintures rupestres, des outils de bois ou de pierre, les vestiges de quelques constructions, ainsi que des mythes et légendes.<br class="manualbr">En Australie, le matériel de camping est très fourni et nettement moins cher qu’en Europe.</p>
<h3 class="spip"><a id="sommaire_2"></a>Activités culturelles</h3>
<p><strong>Activités culturelles en français</strong></p>
<p>Il existe 31 <a href="http://www.alliancefrancaise.com.au/" class="spip_out" rel="external">Alliances françaises</a> en Australie. Outre leurs activités d’enseignement, les Alliances de Sydney, Melbourne, Adélaïde, Brisbane, Perth et Canberra participent activement à la diffusion de la culture française. Elles travaillent en étroite collaboration avec des partenaires locaux et avec l’ambassade de France pour organiser diverses manifestations culturelles (expositions, théâtre, concerts, conférences).</p>
<p><strong>Cinéma</strong></p>
<p>Les nouveaux films sortent en général le jeudi et certaines salles proposent, tous le mardi, des tarifs réduits. Des films français sont proposés de temps à autres dans la plupart des cinémas et à l’occasion des principaux festivals australiens (à Perth, Adélaïde, Melbourne, Canberra et Sydney).</p>
<p>Les cinémas proposant des films français sont, à Sydney, les salles Dendy, les cinémas Verona et Palace Academy à Paddington, au Valhalla à Glebe. A Melbourne, il s’agit des cinémas Kino et Astor et à Adélaide, du Chelsea et à Perth, de l’Astor.</p>
<p>Vous trouverez des cassettes et des DVDs de films français dans les boutiques de location de vidéos, mais aussi dans la plupart des Alliances françaises.</p>
<p>Il existe un <a href="http://www.frenchfilmfestival.org/" class="spip_out" rel="external">festival du film français</a> organisé par l’Alliance française en mars dans six villes : Sydney, Canberra, Perth, Brisbane, Adélaïde et Melbourne.</p>
<p><strong>Expositions</strong><br class="manualbr">Les œuvres d’artistes français sont régulièrement exposées dans des festivals ou les Alliances françaises. Pour être tenu au courant des manifestations culturelles françaises en Australie, vous pouvez consulter le site Internet de <a href="http://www.ambafrance-au.org/" class="spip_out" rel="external">l’ambassade de France en Australie</a>.</p>
<p><strong>Activités culturelles locales</strong></p>
<p><strong>Cinéma</strong><br class="manualbr">De nombreuses salles de cinéma de bon confort présentent des films de tout genre, dont beaucoup de films américains. Il existe trois importants festivals internationaux de cinéma qui ont lieu chaque année à Sydney, (<a href="http://www.sff.org.au/" class="spip_out" rel="external">Sydney film festival</a>), Melbourne (<a href="http://www.miff.com.au/" class="spip_out" rel="external">Melbourne international film festival</a>) et Brisbane (<a href="http://biff.com.au/" class="spip_out" rel="external">Brisbane international film festival</a>).</p>
<p><strong>Théâtre</strong><br class="manualbr">A Sydney, plusieurs salles de théâtre offrent un choix de pièces tirées du répertoire anglais et américain. L’Institut national d’art dramatique présente des pièces étrangères jouées en anglais. Il existe un seul théâtre à Canberra et plusieurs petites salles de type " art et essai ".</p>
<p><strong>Spectacle vivant et arts de la scène</strong><br class="manualbr">Canberra propose une grande offre de concerts de musique classique. Un festival international de jazz se tient chaque année à Melbourne (<a href="http://www.melbournejazz.com/" class="spip_out" rel="external">Melbourne international jazz festival</a>). Opéras et ballets sont rares. Les spectacles de qualité internationale ont généralement lieu à Sydney et Melbourne. Le complexe culturel de l’opéra de Sydney programme tout au long de l’année des concerts de musique classique, des opéras et des ballets. Il existe un conservatoire réputé et de nombreuses sociétés de musique.</p>
<p><strong>Expositions, bibliothèques</strong><br class="manualbr">Des expositions de niveau international sont organisées à Canberra, Sydney et Melbourne. De nombreuses bibliothèques publiques municipales offrent un choix considérable de livres, y compris en langues étrangères.</p>
<h3 class="spip"><a id="sommaire_3"></a>Sports</h3>
<p>L’Australie est le paradis des sportifs. Le sport et les loisirs font partie intégrante de la vie des Australiens lesquels, quel que soit leur âge, y consacrent la majorité de leur temps libre. Les sports aquatiques et d’équipe connaissent un grand succès en particulier le cricket, le <i>footy</i> ou encore <i>Aussie rules</i> (football australien), le rugby, le football (<i>soccer</i>), le <i>netball</i> (ce sport principalement féminin est une variante du basket-ball qui n’autorise aucun contact physique), le golf, etc. Le pays compte le plus grand nombre de terrains de golf au monde. Les sports mécaniques, la course de chevaux et le tennis sont aussi pratiqués ou suivis par bon nombre d’Australiens.</p>
<p>Les équipements sportifs sont de très bonne qualité et très abordables. Il existe des camps pour les enfants et les adolescents.</p>
<p>La <a href="http://www.ausport.gov.au/" class="spip_out" rel="external">commission australienne des sports</a> (<i>Australian Sports Commission </i>- ASC) subventionne, conseille l’activité sportive et gère l’Institut des sports australien. Chaque Etat dispose de sa propre agence responsable de la politique des sports et des loisirs et dirige ses propres instituts et académies du sport.</p>
<p><strong>Ministères des sports et des loisirs par Etat</strong></p>
<ul class="spip">
<li><a href="http://www.dsr.nsw.gov.au/" class="spip_out" rel="external">Nouvelle-Galles du Sud</a></li>
<li><a href="http://www.act.gov.au/browse/topics/sport-and-recreation" class="spip_out" rel="external">Canberra</a></li>
<li><a href="http://www.sportrec.qld.gov.au/" class="spip_out" rel="external">Queensland</a></li>
<li><a href="http://www.sportandrecreation.nt.gov.au/" class="spip_out" rel="external">Territoire du nord</a></li>
<li><a href="http://www.dsr.wa.gov.au/" class="spip_out" rel="external">Australie occidentale</a></li>
<li><a href="http://www.ors.sa.gov.au/" class="spip_out" rel="external">Australie méridionale</a></li>
<li><a href="http://www.sport.vic.gov.au/" class="spip_out" rel="external">Victoria</a></li></ul>
<p><strong>Sports aquatiques</strong></p>
<p>Les sports de plage tels que le surf et ses dérivés ou encore le <i>ironman</i> (alliant nage, <i>board paddling</i> ou planche de surf avec pagaie, <i>ski paddling</i> ou kayak surf et course) sont très populaires. En raison des risques présentés par les requins, il est préférable de ne fréquenter que les plages aménagées, dans ou à proximité des agglomérations, et équipées de filets de protection. Pour la plongée sous-marine, il faut se renseigner auprès des clubs locaux. Le contact avec certaines méduses (<i>blue bottle</i>) présentes au large des plages de la grande barrière de corail à certaines saisons peut être dangereux, voire mortel. De même, certaines rivières et plages au nord du Queensland et dans le Territoire du Nord sont interdites à la baignade en raison de la présence de crocodiles. Il faut suivre scrupuleusement les indications fournies localement.</p>
<p><strong>Sports d’hiver</strong></p>
<p>Les sports suivants sont très répandus : ski de fond, ski alpin, <i>snowboard</i>, hockey sur glace, luge, curling… De nombreuses villes disposent d’une patinoire.</p>
<p><strong>Chasse et pêche</strong></p>
<p>La chasse est quasiment inexistante et extrêmement réglementée. L’importation d’armes de chasse est interdite et la vente sur place strictement réglementée. La pêche est autorisée toute l’année en mer et sur les lacs, mais réglementée en rivière. Les magasins vendant du matériel de pêche peuvent vous renseigner.</p>
<h3 class="spip"><a id="sommaire_4"></a>Télévision – Radio</h3>
<p><strong>Radio</strong></p>
<p>Il existe une dizaine de stations de radio en modulation de fréquence dont une classique.</p>
<p>SBS Radio en Français est accessible dans toute l’Australie soit par le biais de la radio numérique, SBS Radio 1, à partir d’un poste de TV numérique ou sur les ondes hertziennes FM ou AM selon les cas.</p>
<p><strong>SBS</strong> diffuse actuellement en français quatre fois par semaine : les mardis, jeudis, samedis et dimanches de 13h00 à 14h00 sur les fréquences suivantes :</p>
<ul class="spip">
<li>Adélaide 106.3 FM</li>
<li>Brisbane 93.3 FM</li>
<li>Canberra 1440 AM</li>
<li>Hobart 105.7 FM</li>
<li>Melbourne 1224 AM</li>
<li>Newcastle 1413 AM</li>
<li>Perth 96.9 FM</li>
<li>Sydney 1107 AM</li>
<li>Wollongong 1485 AM</li></ul>
<p>A noter que seules les villes de Canberra, Melbourne et Sydney reçoivent les émissions en ondes herziènes les quatres jours. Pour les autres villes et régions, certains jours ne sont accessibles que via la radio numérique (SBS Radio 1) sur poste TV.</p>
<p>Pour connaître le détail des diffusions et la fréquence de votre région <a href="http://www.sbs.com.au/radio/schedule/preferences/change" class="spip_out" rel="external">cliquez ici</a>.</p>
<p><strong>Télévision</strong></p>
<p>Il existe cinq chaînes de télévision à Sydney et à Canberra, dont deux publiques (SBS, la chaîne culturelle, et ABC, plus générale) et trois commerciales (Channel</p>
<p>7, Channel 9 et Channel 10 à Sydney - Win, Prime et Ten Capital à Canberra).</p>
<p>Des chaînes payantes proposent différents bouquets de programmes. Ceux-ci comportent beaucoup de sport, de très nombreuses séries américaines et d’excellents documentaires sur la nature, la vie des animaux et les sciences.</p>
<p>Le système adopté est le PAL. La vidéo est très répandue. De nombreuses stations-service et magasins de quartier louent des cassettes. Il est possible d’apporter son magnétoscope à condition qu’il soit multistandard ou standard PAL-SECAM car ils sont très chers sur place et souvent introuvables. Concernant la lecture des DVD, l’Australie est située en zone 4 (la France se trouve en zone 2).</p>
<p><strong>CANALSAT Australie</strong> propose un accès à plus de 40 chaînes et services en langue française par réception satellite. Cinéma, sport, programmes pour enfants, chaînes découvertes, informations en direct, chaînes généralistes, divertissement, musique, radios  services. Canalsat offrent une grande diversité de programmes destinés à toute la famille. Pour plus d’informations, contactez le bureau Canalsat Australie au +61 (0)2 9252 6577.</p>
<p>La chaîne publique <strong>SBS One</strong> diffuse tous les matins à 8h40 le JT de 20h de France 2 de la veille et du lundi au vendredi à 15h le journal international de France 24. Cette même chaîne propose de nombreux films français en version originale, films que vous pouvez retrouver gratuitement en ligne sur le site <a href="http://www.sbs.com.au/ondemand/" class="spip_out" rel="external">SBS ONDEMAND</a>.</p>
<p>La chaîne <strong>SBS 2</strong> diffuse quant à elle le JT de 20h de France 2 du lundi au samedi à 5h du matin.</p>
<p>La chaîne publique <strong>ABC</strong> propose également des films et des documentaires français.</p>
<p><strong>TV5</strong>, la télévision internationale diffuse 24h/24 des programmes de TF1, France 2 ou France 3 et d’autres chaînes de télévision francophones. Outre le Bouquet français, la chaîne est disponible sur le bouquet du satellite Asiasat 2 (numérique) ou Palapa C2. Il faut être équipé d’une antenne parabolique de 2 mètres environ pour la recevoir.</p>
<p><strong>MCM</strong>, la télévision musicale française est également disponible sur le bouquet d’Asiasat 2.</p>
<p><strong>RFO</strong> (Réseau France Outre-mer) peut être capté via CANALSAT Australie.</p>
<h3 class="spip"><a id="sommaire_5"></a>Presse Française</h3>
<p>Les grands quotidiens et magazines français sont proposés dans la plupart des grandes villes du pays, dans les Alliances françaises et, en général, dans les principales maisons de la presse.</p>
<p>La publication du Courrier Australien, journal de la communauté française en Australie depuis 1892, est aujourd’hui interrompue. Des projets de relance sont actuellement à l’étude.</p>
<p><strong>Le Petit Journal</strong> renseigne sur les derniers événements, donne des bons plans et des petites annonces aux expatriés français et francophones en Australie. Il existe trois "éditions" en ligne, <a href="http://www.lepetitjournal.com/sydney" class="spip_out" rel="external">Sydney</a> (depuis 2010), <a href="http://www.lepetitjournal.com/melbourne" class="spip_out" rel="external">Melbourne</a> et <a href="http://www.lepetitjournal.com/melbourne" class="spip_out" rel="external">Perth</a> (depuis 2012).</p>
<p><i>Mise à jour : octobre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/vie-pratique/article/loisirs-et-culture). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
