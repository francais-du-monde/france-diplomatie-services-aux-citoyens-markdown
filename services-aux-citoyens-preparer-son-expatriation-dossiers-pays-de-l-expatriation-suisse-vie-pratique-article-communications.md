# Communications

<h4 class="spip">Téléphone - Internet</h4>
<p>Les liaisons téléphoniques sont bonnes.</p>
<p>Le groupe <a href="http://www.swisscom.ch/" class="spip_out" rel="external">Swisscom</a>, leader du marché suisse des télécommunications offre toute une gamme de produits et services qui comprend la téléphonie fixe et mobile ainsi que l’équipement internet.</p>
<p>L’indicatif du pays est le 41. Il est suivi de l’indicatif de la ville :</p>
<ul class="spip">
<li>22 pour Genève</li>
<li>44 pour Zurich</li>
<li>31 pour Berne</li></ul>
<p>Les personnes déménageant d’une ville à l’autre pouvant, depuis 2002, conserver leur numéro de téléphone, les indicatifs téléphoniques ont aujourd’hui perdu de leur pertinence.</p>
<p>Il existe toutefois sur le marché des opérateurs privés permettant d’alléger le coût des appels vers l’international. Le <a href="https://www.konsum.admin.ch/fr/" class="spip_out" rel="external">bureau fédéral de la consommation</a> liste les associations de consommateurs en Suisse qui peuvent vous aider à comparer les diverses offres.</p>
<h4 class="spip">Poste</h4>
<p>Les liaisons postales intérieures et internationales sont bonnes et sécurisées. Le délai moyen d’acheminement du courrier vers la France est de deux à trois jours.</p>
<p>L’antenne PostFinance de la poste suisse offre aux particuliers toute une gamme de services bancaires.</p>
<p>Les factures domestiques peuvent être payées directement aux bureaux de poste suisses.</p>
<p><strong>Pour en savoir plus</strong></p>
<ul class="spip">
<li><a href="http://www.poste.ch" class="spip_out" rel="external">Site internet de la poste suisse</a></li>
<li><a href="http://www.swisspost.com" class="spip_out" rel="external">Service international</a></li>
<li><a href="http://www.postfinance.ch" class="spip_out" rel="external">PostFinance</a></li></ul>
<p><i>Mise à jour : février 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/vie-pratique/article/communications). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
