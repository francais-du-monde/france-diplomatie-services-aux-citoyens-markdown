# Entrée et séjour

<h2 class="rub22808">Passeport, visa, permis de travail</h2>
<p>Quel que soit le motif de votre séjour, renseignez-vous auprès de la section consulaire de l’ambassade du Luxembourg à Paris qui vous informera sur la règlementation en matière d’entrée et de séjour au Luxembourg, règlementation que vous devrez impérativement respecter. N’hésitez pas à consulter le <a href="http://www.etat.lu/" class="spip_out" rel="external">site du gouvernement luxembourgeois</a> et de l’<a href="http://paris.mae.lu/fr" class="spip_out" rel="external">ambassade du Luxembourg à Paris</a> afin de vous familiariser avec les modalités d’entrée et de séjour dans le pays.</p>
<p>Une fois sur place c’est l’administration communale qui prend le relais. <strong>En aucun cas, vous n’êtes autorisé à travailler au Luxembourg sans permis adéquat. </strong></p>
<p>La section consulaire de l’ambassade de France à Luxembourg n’est pas l’organisme compétent pour répondre à vos demandes d’information concernant votre séjour au Luxembourg.</p>
<p>Les ressortissants d’un Etat membre de l’Union européenne ou d’un pays assimilé (Norvège, Islande, Liechtenstein, et Confédération suisse) peuvent se rendre au Luxembourg sous le couvert d’une carte d’identité ou d’un passeport en cours de validité pour une période n’excédant pas trois mois.</p>
<p>Ils ont le droit d’y exercer un emploi dans les mêmes conditions que les nationaux.</p>
<p>Pour un séjour de plus de trois mois, chaque ressortissant de l’Union européenne et assimilé (Norvège, Liechtenstein, Islande et Confédération suisse) doit solliciter, dans les trois mois ouvrables de son arrivée, la délivrance d’une attestation d’enregistrement auprès de l’administration communale de son lieu de résidence.</p>
<p>La carte de séjour n’est plus obligatoire.</p>
<p>Après un séjour ininterrompu de cinq ans, le citoyen européen tout comme le ressortissant d’un pays assimilé, ont droit au séjour permanent.</p>
<p>Depuis le 1er janvier 2009 (loi du 23 octobre 2008), la période de résidence obligatoire dans le pays pour demander la double nationalité est passée de cinq à sept ans. Les candidats devront réussir un examen de langue luxembourgeoise et suivre un cours d’instruction civique mais ne seront plus obligés d’abandonner leur nationalité d’origine.</p>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li><a href="http://paris.mae.lu/fr" class="spip_out" rel="external">Section consulaire de l’ambassade de Luxembourg à Paris</a></li>
<li><a href="http://www.ambafrance-lu.org/" class="spip_out" rel="external">Section consulaire de l’Ambassade de France à Luxembourg</a></li>
<li><a href="http://www.vdl.lu/" class="spip_out" rel="external">Bierger Center, bureau d’assistance de l’Etat à Luxembourg-ville</a></li>
<li><a href="http://www.guichet.public.lu/" class="spip_out" rel="external">Guichet public</a></li>
<li><a href="http://www.etat.lu/" class="spip_out" rel="external">Etat.lu</a></li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-luxembourg-entree-et-sejour-article-animaux-domestiques-109893.md" title="Animaux domestiques">Animaux domestiques</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-luxembourg-entree-et-sejour-article-vaccination-109892.md" title="Vaccination">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-luxembourg-entree-et-sejour-article-demenagement-109891.md" title="Déménagement">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-luxembourg-entree-et-sejour-article-passeport-visa-permis-de-travail.md" title="Passeport, visa, permis de travail">Passeport, visa, permis de travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/entree-et-sejour/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
