# Lettre de motivation

<h4 class="spip">Rédaction</h4>
<p><strong>Contrairement à l’usage en France, la lettre de motivation doit être dactylographiée</strong>. En effet, les Anglo-saxons ont rarement recours aux techniques d’analyse graphologique dans le cadre du recrutement et une lettre manuscrite pourrait être interprétée comme un manque de rigueur.</p>
<p>Bien que très soignée dans le style et le fond, <strong>la lettre doit généralement rester assez succincte. </strong>D’une approche plus directe qu’en France, la lettre sera individualisée en fonction du destinataire et de la connaissance qu’aura le candidat de l’entreprise visée.</p>
<p>La lettre de motivation peut être construite selon le plan suivant :</p>
<ul class="spip">
<li>référence à l’emploi souhaité et, le cas échéant, à la source d’information ayant permis de connaître l’offre ;</li>
<li>présentation des qualifications et de l’expérience du candidat, en évitant les redites avec le CV ;</li>
<li>mise en relief des points forts du candidat, en s’appuyant si possible sur des exemples concrets (résultats obtenus lors des expériences antérieures, réalisations particulières, etc) ;</li>
<li>disponibilités du candidat pour un rendez-vous.La candidature peut être adressée au <i>Recruiting Manager</i> ou <i>Human Resource Manager</i>.</li></ul>
<p>Lorsqu’il existe un contact précis pour l’emploi recherché, il est souhaitable de personnaliser l’envoi en veillant à respecter l’orthographe du nom ainsi que l’intitulé des fonctions.</p>
<p>Enfin, <strong>il est très important d’effectuer un "suivi" de la candidature</strong>, en relançant son destinataire par téléphone après l’envoi. Un délai d’une semaine est recommandé. C’est souvent, pour le recruteur potentiel, l’occasion de juger la détermination du candidat et sa capacité à engager directement le contact.</p>
<p><strong>Ce suivi augmente de fait les chances de décrocher un entretien</strong>, aussi bien à l’occasion d’une candidature spontanée qu’en cas de réponse à une offre. De la même façon, adresser un mot de remerciements au recruteur à l’issue de l’entretien est un "plus" qui peut faire la différence.</p>
<h4 class="spip">Modèles de lettre de motivation</h4>
<p>Plusieurs guides peuvent vous aider dans la rédaction de votre lettre de motivation. Vous pouvez consulter par exemple :</p>
<ul class="spip">
<li><i>Rédiger une lettre de motivation en anglais</i>, collectif, éd. Studyrama, coll. Guides J, 2008</li>
<li><i>Comment rédiger une lettre de candidature en anglais</i>, Timothy CAREER, éd. De Vecchi, coll. Guides juridiques, 2003.</li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/emploi-stage/article/lettre-de-motivation). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
