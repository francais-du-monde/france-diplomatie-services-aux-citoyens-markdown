# Régime local de sécurité sociale

<p>Vous trouverez une présentation détaillée du système de sécurité sociale tchèque sur le site de notre partenaire, le <a href="http://www.cleiss.fr/" class="spip_out" rel="external">CLEISS</a> (Centre des liaisons européennes et internationales de sécurité sociale). En voici la table des matières :</p>
<ul class="spip">
<li><a href="http://www.cleiss.fr/docs/regimes/regime_republique_tcheque_salaries.html" class="spip_out" rel="external">Le régime des salariés</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_republique_tcheque_nonsalaries.html" class="spip_out" rel="external">Le régime des non salariés</a> (travailleurs indépendants)</li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/protection-sociale/article/regime-local-de-securite-sociale-111342). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
