# Règlementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/emploi-stage/article/reglementation-du-travail-103729#sommaire_1">Période d’essai et préavis</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/emploi-stage/article/reglementation-du-travail-103729#sommaire_2">Durée du travail</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/emploi-stage/article/reglementation-du-travail-103729#sommaire_3">Coût des heures supplémentaires</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/emploi-stage/article/reglementation-du-travail-103729#sommaire_4">Jours de repos hebdomadaires</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/emploi-stage/article/reglementation-du-travail-103729#sommaire_5">Jours fériés</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/emploi-stage/article/reglementation-du-travail-103729#sommaire_6">Congés annuels</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/emploi-stage/article/reglementation-du-travail-103729#sommaire_7">Congés maladie</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/emploi-stage/article/reglementation-du-travail-103729#sommaire_8">Congés de maternité et paternité</a></li>
<li><a id="so_9" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/emploi-stage/article/reglementation-du-travail-103729#sommaire_9">Main d’œuvre expatriée</a></li></ul>
<p>Une nouvelle loi du travail (Loi organique du travail, de la travailleuse et du travailleur : LOTTT) visant à renforcer la protection des salariés a été promulguée en avril 2012. Les principaux changements introduits concernent la réduction du temps de travail, la rétroactivité des prestations sociales pour les travailleurs actifs, l’allongement des périodes de congés parentaux, la réduction de la période d’essai à l’embauche et la participation des travailleurs à la gestion de l’entreprise.</p>
<h3 class="spip"><a id="sommaire_1"></a>Période d’essai et préavis</h3>
<p>La réforme fait passer la période d’essai de 3 à 1 mois et offre au travailleur à l’essai la protection juridique correspondant aux contrats à durée indéterminée. Ainsi, les travailleurs reçoivent des prestations sociales dès le premier jour de travail et non plus à l’issue de la période d’essai.</p>
<h3 class="spip"><a id="sommaire_2"></a>Durée du travail</h3>
<p>Le nombre d’heures légales de travail est fixé à 8 heures par jour maximum. Le nouveau texte de loi fait passer la journée de travail de 44h à 40h par semaine (sans réduction de salaire). Dans certains cas spécifiques nécessitant 6 jours de travail hebdomadaires, un jour de congé par semaine travaillée doit être ajouté aux congés de l’employé.</p>
<h3 class="spip"><a id="sommaire_3"></a>Coût des heures supplémentaires</h3>
<p>Les heures supplémentaires sont payées à 150% du taux horaire de base.</p>
<h3 class="spip"><a id="sommaire_4"></a>Jours de repos hebdomadaires</h3>
<p>La loi impose deux jours consécutifs de repos par semaine.</p>
<h3 class="spip"><a id="sommaire_5"></a>Jours fériés</h3>
<p>Les jours fériés sont :</p>
<ul class="spip">
<li>le Jour de l’An,</li>
<li>Carnaval,</li>
<li>les jeudi et vendredi saint,</li>
<li>le jour de la déclaration de l’indépendance (19 avril),</li>
<li>le jour de l’Émancipation (19 avril),</li>
<li>la fête du travail (1er mai),</li>
<li>l’ascension,</li>
<li>la bataille de Carabobo (24 juin),</li>
<li>la fête de l’indépendance (5 juillet),</li>
<li>la fête de Simon Bolivar (24 juillet),</li>
<li>la célébration de la Résistance Indigène (12 octobre)</li>
<li>Noël (25 décembre).</li></ul>
<p>A noter que les activités sont également réduites durant la semaine du carnaval et celle précédant Pâques.</p>
<p>Pour en savoir plus, consultez <a href="http://www.tv5.org/cms/chaine-francophone/voyageurs/Outils/p-8228-Jours-feries.htm" class="spip_out" rel="external">le calendrier des jours fériés au Venezuela</a></p>
<h3 class="spip"><a id="sommaire_6"></a>Congés annuels</h3>
<p>Les travailleurs ont droit à 15 jours de vacances rémunérées par an, à partir d’un an de travail pour le même employeur, puis à un jour supplémentaire pour chaque année de travail effectuée pour le même employeur (dans une limite de 15 jours). Au titre des gratifications, la réforme de la LOTTT établit que chaque travailleur doit recevoir, en plus du salaire correspondant à ses jours de congés, une rémunération équivalente à 15 jours de salaire (contre 7 auparavant), augmentée d’un jour de salaire par an jusqu’à atteindre le seuil de 30 jours de salaires. Par ailleurs, il recevra, dans les 15 premiers jours du mois de décembre, une gratification de fin d’année (13ème mois) équivalente à au moins 30 jours de salaire (contre 15 auparavant).</p>
<h3 class="spip"><a id="sommaire_7"></a>Congés maladie</h3>
<p>La loi stipule que les congés maladie sont ouverts à partir du 4ème jour et pour 52 semaines au maximum, avec une indemnisation correspondant aux deux tiers du salaire. En pratique, pourtant, la sécurité sociale n’indemnise rien.</p>
<h3 class="spip"><a id="sommaire_8"></a>Congés de maternité et paternité</h3>
<p>La mère bénéficie de 6 semaines de congés avant l’accouchement et de 20 semaines après celui-ci (contre 6 et 12 auparavant). La nouvelle loi a par ailleurs introduit 14 jours de congés paternité après l’accouchement et l’inamovibilité salariale des deux membres du couple à partir de la déclaration de grossesse jusqu’à deux ans après l’accouchement.</p>
<h3 class="spip"><a id="sommaire_9"></a>Main d’œuvre expatriée</h3>
<p>La législation vénézuélienne impose d’employer 90% d’employés nationaux dans les entreprises de plus de 10 salariés. Les autorités peuvent vérifier que la rémunération des employés expatriés n’excédera pas plus de 20% celle des employés locaux de même niveau de compétence.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/emploi-stage/article/reglementation-du-travail-103729). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
