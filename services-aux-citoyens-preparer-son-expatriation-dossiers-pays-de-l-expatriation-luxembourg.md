# Luxembourg

<p><strong>Au 31 décembre 2014</strong>, 33 378 Français étaient inscrits au registre des Français établis hors de France tenu par le consulat de France à Luxembourg. La communauté française est estimée à 34 000 personnes. A noter que 78 000 frontaliers français viennent travailler quotidiennement au Luxembourg. Un taux de chômage relativement bas (mais en légère hausse 7 %) et un marché de l’emploi dynamique expliquent l’intérêt croissant des Français pour ce pays.</p>
<p>La communauté française <strong>se compose à 99 % d’expatriés</strong>. Il s’agit principalement d’employés et de cadres. De nombreux Français travaillent dans le secteur financier. A noter la présence de près de 1800 à 2000 Français au sein des institutions européennes et internationales présentes au Luxembourg. C’est une population jeune, principalement installée dans la capitale et sa banlieue.</p>
<p>On estime le nombre des entreprises et/ou filiales d’entreprises françaises installées au Luxembourg entre 120 et 150, un tiers dans les services financiers (banque et assurance notamment), moins du quart dans l’industrie (sidérurgie, industries électriques, informatiques, construction et génie civil) et plus de 40% dans le commerce, la distribution agro-alimentaire et les services non financiers.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/dossiers-pays/luxembourg/" class="spip_in">Une description du Luxembourg, de sa situation politique et économique</a> ;</li>
<li><a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/luxembourg/" class="spip_in">Des informations actualisées sur les conditions locales de sécurité au Luxembourg</a></li>
<li><a href="http://www.ambafrance-lu.org/" class="spip_out" rel="external">Ambassade de France au Luxembourg</a>.</li></ul>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-luxembourg-entree-et-sejour.md">Entrée et séjour</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-luxembourg-entree-et-sejour-article-passeport-visa-permis-de-travail.md">Passeport, visa, permis de travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-luxembourg-entree-et-sejour-article-demenagement-109891.md">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-luxembourg-entree-et-sejour-article-vaccination-109892.md">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-luxembourg-entree-et-sejour-article-animaux-domestiques-109893.md">Animaux domestiques</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-luxembourg-emploi-stage.md">Emploi, stage</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-luxembourg-emploi-stage-article-marche-du-travail-109894.md">Marché du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-luxembourg-emploi-stage-article-reglementation-du-travail.md">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-luxembourg-emploi-stage-article-recherche-d-emploi.md">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-luxembourg-emploi-stage-article-curriculum-vitae-109897.md">Curriculum vitae</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-luxembourg-emploi-stage-article-lettre-de-motivation-109898.md">Lettre de motivation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-luxembourg-emploi-stage-article-entretien-d-embauche-109899.md">Entretien d’embauche</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-luxembourg-emploi-stage-article-stages.md">Stages</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-luxembourg-protection-sociale-22810.md">Protection sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-luxembourg-protection-sociale-22810-article-regime-local-de-securite-sociale-109901.md">Régime local de sécurité sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-luxembourg-protection-sociale-22810-article-convention-de-securite-sociale-109902.md">Convention de sécurité sociale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-luxembourg-fiscalite.md">Fiscalité</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-luxembourg-fiscalite-article-fiscalite-du-pays.md">Fiscalité du pays</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-luxembourg-fiscalite-article-convention-fiscale.md">Convention fiscale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-luxembourg-vie-pratique.md">Vie pratique</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-luxembourg-vie-pratique-article-logement-109905.md">Logement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-luxembourg-vie-pratique-article-sante.md">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-luxembourg-vie-pratique-article-scolarisation-109907.md">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-luxembourg-vie-pratique-article-cout-de-la-vie-109908.md">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-luxembourg-vie-pratique-article-transports.md">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-luxembourg-vie-pratique-article-communications.md">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-luxembourg-vie-pratique-article-loisirs-et-culture-109911.md">Loisirs et culture</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-luxembourg-presence-francaise.md">Présence française</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-luxembourg-presence-francaise-article-presence-francaise.md">Présence française</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
