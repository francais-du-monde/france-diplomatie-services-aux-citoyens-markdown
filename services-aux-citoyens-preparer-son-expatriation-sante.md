# Santé

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/sante/#sommaire_1">Vaccinations</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/sante/#sommaire_2">Prévention médicale</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/sante/#sommaire_3">Conditions sanitaires dans le pays de résidence</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Vaccinations</h3>
<p>Certaines vaccinations ont un caractère obligatoire, d’autres sont facultatives. Il est important de vous informer assez longtemps avant votre départ sur ce qui est recommandé selon votre destination.</p>
<p>Les Français se rendant à l’étranger sont invités à consulter notre rubrique <a href="conseils-aux-voyageurs.md" class="spip_in">Conseils aux Voyageurs</a>.</p>
<p><strong>Les centres de vaccination</strong></p>
<p><strong>La vaccination contre la fièvre jaune</strong>, exigée à l’entrée de certains pays, doit être inscrite sur un carnet international. Elle ne peut être pratiquée que dans des centres agréés par le ministère de la Santé.</p>
<p>Pour connaître les coordonnées des centres habilités à effectuer la vaccination antiamarile et à délivrer les certificats internationaux contre la fièvre jaune consultez l’article <a href="conseils-aux-voyageurs-infos-pratiques-risques-risques-sanitaires-article-sante-vaccinations.md" class="spip_in">Santé / Vaccinations</a>.</p>
<p>Les autres vaccinations peuvent être réalisées par votre médecin traitant ou éventuellement par le Centre de Vaccinations Internationales (CVI).</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.pasteur-lille.fr/fr/sante/conseil_voyageurs.html" class="spip_out" rel="external">Institut Pasteur</a> </p>
<h3 class="spip"><a id="sommaire_2"></a>Prévention médicale</h3>
<p><strong>La visite médicale</strong></p>
<p>La visite médicale est indispensable pour préciser l’aptitude du salarié à occuper son futur poste de travail. Elle s’accompagne en règle générale d’un ensemble d’examens complémentaires (ex : biologie…).</p>
<p><strong>La prescription d’un traitement prophylactique du paludisme</strong></p>
<p>Le paludisme est une maladie grave, potentiellement mortelle, transmise par les moustiques particulièrement actifs entre le crépuscule et l’aube. Elle est très répandue en zone tropicale.</p>
<p>Des médicaments peuvent être pris pour prévenir une crise de paludisme ; ils vous seront prescrits par votre médecin traitant ou lors d’une consultation dans un centre de conseils aux voyageurs. Le traitement tient compte des zones visitées, de la durée du voyage, de l’âge et de l’état de la personne (intolérance, grossesse).</p>
<p>Ce traitement doit être complété par des mesures de protection contre les piqûres de moustiques (moustiquaire imprégnée, insecticide, aérosol spécial tropiques).</p>
<p><strong>La trousse médicale à emporter</strong></p>
<p>Outre vos traitements réguliers -à emporter si possible en quantité suffisante pour trois mois- (contraceptifs, antidiabétiques etc…), le contenu de la trousse médicale dépend de la destination. Votre médecin en établira la liste : antipaludique, antidiarrhéique, antalgique, antiseptique cutané, pansements, compresses, seringues à usage unique, préservatifs…</p>
<p><strong>Accompagnement, soutien et suivi psychologiques</strong></p>
<p>Il est désormais possible d’obtenir une consultation à distance avec un psychologue ou un psychiatre de langue française depuis son pays d’expatriation.</p>
<h3 class="spip"><a id="sommaire_3"></a>Conditions sanitaires dans le pays de résidence</h3>
<p>En France, il existe dans toutes les grandes villes des centres spécialisés de conseils aux voyageurs. En outre, votre médecin traitant ou le médecin du travail de votre entreprise est en mesure de répondre aux nombreuses questions que vous vous posez sur l’état sanitaire de votre futur pays de résidence concernant :</p>
<ul class="spip">
<li>L’hygiène alimentaire et le traitement de l’eau</li>
<li>Le climat et l’environnement (soleil, chaleur, altitude, grand froid, morsures ou piqûres de serpents ou d’insectes, etc.) ;</li>
<li>Les maladies infectieuses ;</li>
<li>Les maladies spécifiques de certains pays, comme le paludisme, la bilharziose, etc. ;</li>
<li>Les maladies sexuellement transmissibles et sur le SIDA en particulier ;</li>
<li>Les risques transfusionnels éventuels.</li></ul>
<p>Il est utile aussi de connaître les <strong>loisirs </strong>proposés et les <strong>risques</strong> qu’ils peuvent comporter.</p>
<p><strong>Autres sites à consulter :</strong></p>
<ul class="spip">
<li><a href="http://www.pasteur.fr/sante/" class="spip_out" rel="external">Institut Pasteur</a>. Vous y trouverez des recommandations générales et par pays, ainsi que des actualités.</li>
<li><a href="http://www.who.int/fr/" class="spip_out" rel="external">Organisation mondiale de la Santé</a></li></ul>
<p><i>Mise à jour : juillet 2016</i></p>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/sante/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
