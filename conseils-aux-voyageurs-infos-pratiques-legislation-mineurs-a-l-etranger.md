# Mineurs à l’étranger

<h2 class="rub20981">Conseils aux familles</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/infos-pratiques/legislation/mineurs-a-l-etranger/#sommaire_1">Avant le départ</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/infos-pratiques/legislation/mineurs-a-l-etranger/#sommaire_2">Pendant le séjour</a></li></ul>
<p><strong>Ces conseils s’adressent aux familles dont les enfants mineurs partent en séjours collectifs organisés à l’étranger, quel que soit le mode d’hébergement.</strong> Ne sont toutefois visés ni les séjours se déroulant dans le cadre scolaire ni les "séjours de vacances dans une famille".<span class="spip_note_ref"> [<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/infos-pratiques/legislation/mineurs-a-l-etranger/#nb1" class="spip_note" rel="footnote" title="Les séjours de vacances dans une famille sont strictement définis comme étant (...)" id="nh1">1</a>]</span></p>
<h3 class="spip"><a id="sommaire_1"></a>Avant le départ</h3>
<p><strong>Il est recommandé aux familles :</strong></p>
<ul class="spip">
<li>de consulter le site Internet du <a href="conseils-aux-voyageurs.md" class="spip_in">ministère des Affaires étrangères et du Développement international</a> qui donne pour chaque pays des informations actualisées sur les événements d’ordres sanitaire et sécuritaire ;</li>
<li>d’inscrire le mineur participant au séjour sur le télé-service <a href="https://pastel.diplomatie.gouv.fr/fildariane/dyn/public/login.html" class="spip_out" rel="external">Ariane</a> du ministère des Affaires étrangères et du Développement international ;</li>
<li>de consulter le site Internet du <a href="http://www.sante.gouv.fr/" class="spip_out" rel="external">ministère chargé de la Santé</a> pour des informations concernant les événements sanitaires (canicule, épidémie, contaminations…) ;</li>
<li>de communiquer au responsable du séjour un document comportant toute information relative aux vaccinations obligatoires ou à leur contre-indication, aux antécédents médicaux ou chirurgicaux ou à tout autre élément d’ordre médical susceptible d’avoir des répercussions sur le déroulement du séjour, aux pathologies chroniques ou aigües en cours (si un traitement est à prendre l’ordonnance doit être jointe et les médicaments confiés au responsable) ;</li>
<li>d’adapter les effets personnels de son enfant au pays dans lequel il va se rendre ; vêtements chaud, habit de pluie, chapeau, crème solaire, anti-moustique… ;</li>
<li>de sensibiliser son enfant participant au fait qu’il va découvrir une autre culture et devoir changer ses habitudes (alimentaires, rythme de vie…) ;</li>
<li>d’avoir pris connaissance très attentivement du projet éducatif, du projet pédagogique et, le cas échéant, du règlement intérieur et/ou des conditions particulières de vente du séjour ;</li>
<li>de prendre l’attache de l’organisateur du séjour pour connaître les responsabilités éventuelles en cas de manquements ou de comportements infractionnels de son enfant (exclusion du séjour, rapatriement en France, prise en charge des mineurs dans le cas de poursuites judiciaires, d’arrestation…).</li></ul>
<p>Il peut être utile de <strong>laisser à l’organisateur du séjour les informations suivantes :</strong></p>
<ul class="spip">
<li>le numéro de passeport du mineur et le lieu de sa délivrance ;</li>
<li>les coordonnées exactes et actuelles de ses représentants légaux ;</li>
<li>un numéro de téléphone permettant de les joindre à tout moment ;</li>
<li>les coordonnées exactes de leur assureur en responsabilité civile ;</li>
<li>une copie de la carte européenne d’assurance maladie si le séjour a lieu en Europe.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Pendant le séjour</h3>
<p><strong>En cas de survenance d’un incident ou accident</strong> :</p>
<p>Exemple : incident ou accident ayant nécessité l’intervention des forces de l’ordre ou de sécurité, ayant entraîné un dépôt de plainte, ayant mis en péril la sécurité physique ou morale de mineurs (infraction, affaire de mœurs…) ou ayant nécessité une hospitalisation de plusieurs jours, etc.</p>
<p><strong>Contacter immédiatement :</strong></p>
<ul class="spip">
<li>l’organisateur</li>
<li>ou l’ambassade de France du pays dans lequel se déroule le séjour</li>
<li>ou le bureau de la protection des mineurs du ministère des Affaires étrangères et du Développement international : 01 43 17 80 32</li></ul>
<p><strong>A savoir :</strong> Les familles ont l’obligation de mettre tout en œuvre pour prendre à leur charge les mineurs en cas d’incident majeur interrompant le séjour et, le cas échéant, de se déplacer dans le pays où se déroule le séjour. <br class="manualbr"></p>
<p><strong>En cas d’arrestation du mineur</strong></p>
<p>Il est conseillé de faire prévenir l’ambassade ou le consulat le plus proche afin qu’elle/il puisse exercer la protection consulaire de la France. Les autorités étrangères qui arrêtent un Français ont le devoir, dès que sa qualité d’étranger est établie, de l’informer de son droit d’en avertir les services consulaires français. Si elles négligent de le faire, l’intéressé peut revendiquer ce droit en se référant à la Convention de Vienne de 1963 sur les relations consulaires (article 36). Il est donc recommandé aux mineurs voyageant à l’étranger de conserver sur eux un document (ou photocopie) prouvant leur nationalité : passeport, carte nationale d’identité.</p>
<p><strong>En savoir plus</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/infos-pratiques/assistance-aux-francais/incarceration-20987/article/incarceration" class="spip_in">Incarcération</a></p>
<p><span class="spip_note_ref">[<a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/infos-pratiques/legislation/mineurs-a-l-etranger/#nh1" class="spip_note" title="Notes 1" rev="footnote">1</a>] </span>Les séjours de vacances dans une famille sont strictement définis comme étant des séjours où les enfants sont confiés à une ou plusieurs familles durant toute la durée du séjour, sans qu’aucune équipe d’encadrement ne soit présente sur place, ni n’organise d’activité collective (cours de langue, pratiques sportives, activités culturelles, …) durant la journée.</p>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/infos-pratiques/legislation/mineurs-a-l-etranger/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
