# Déménagement

<p>S’agissant d’un déménagement international, il est primordial de s’entourer d’un maximum de garanties en faisant appel à un professionnel disposant de certifications reconnues (marque NF Service, ISO 9002, etc.).</p>
<p><a href="http://www.csdemenagement.fr/" class="spip_out" rel="external">Chambre syndicale du déménagement</a><br class="manualbr">Téléphone : 01 49 88 61 40 <br class="manualbr">Télécopie : 01 49 88 61 46<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/entree-et-sejour/article/demenagement-114276#contact#mc#csdemenagement.fr#" title="contact..åt..csdemenagement.fr" onclick="location.href=mc_lancerlien('contact','csdemenagement.fr'); return false;" class="spip_mail">Courriel</a></p>
<p>Importation en franchise du mobilier et de la voiture en provenance d’un pays extracommunautaire.</p>
<p>La meilleure solution pour le déménagement est d’utiliser la route.</p>
<p>Il convient de procéder à un inventaire très complet de son déménagement. Il est conseillé de photographier les objets et meubles anciens (avant 1940).</p>
<p>Le prix du déménagement dépend de la taille du conteneur et du mode de transport. Le délai moyen d’acheminement varie entre trois et sept jours par la route.</p>
<p>Il existe de très nombreuses compagnies locales et étrangères.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Notre article <a href="services-aux-citoyens-preparer-son-expatriation-douanes-article-demenagement.md" class="spip_in">Déménagement</a>.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/entree-et-sejour/article/demenagement-114276). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
