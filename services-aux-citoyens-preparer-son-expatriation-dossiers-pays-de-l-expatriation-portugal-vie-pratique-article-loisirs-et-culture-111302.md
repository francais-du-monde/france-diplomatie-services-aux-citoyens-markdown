# Loisirs et culture

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/vie-pratique/article/loisirs-et-culture-111302#sommaire_1">Tourisme</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/vie-pratique/article/loisirs-et-culture-111302#sommaire_2">Activités culturelles</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/vie-pratique/article/loisirs-et-culture-111302#sommaire_3">Sports</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/vie-pratique/article/loisirs-et-culture-111302#sommaire_4">Télévision – Radio</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/vie-pratique/article/loisirs-et-culture-111302#sommaire_5">Presse française</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Tourisme</h3>
<p>Le Portugal offre un grand intérêt touristique : littoral attrayant avec ses nombreuses plages de sable (voir Sports), notamment à Lisbonne, à Porto et dans les régions du Minho et de l’Algarve ; vestiges de l’époque romaine (à Conimbriga et à Evora) ; innombrables palais, anciennes résidences royales et monuments religieux aux différents styles (roman, gothique, Renaissance, baroque). Le Portugal possède néanmoins un style propre, développé vers la fin du 15ème siècle, le style manuélin - du nom de Manuel Ier le Grand - où les formes gothiques intègrent l’apport musulman et s’inspirent notamment de thèmes marins (cordages, coquillages…). Les murs de nombreux monuments anciens sont ornés d’azulejos, carreaux de faïence émaillée d’origine arabe, généralement bleus, parfois polychromes.</p>
<p>Le Portugal est le pays du fado, musique populaire chantée, empreinte de nostalgie.</p>
<p>Parmi les différentes sortes d’hébergement, on distingue les pousadas, établissements hôteliers qui préservent l’identité et les traditions des régions où ils sont implantés, ainsi que le tourisme d’habitation qui connaît une expansion importante depuis quelques années.</p>
<p>Pour tous renseignements complémentaires, s’adresser à :</p>
<p><a href="http://www.visitportugal.com/" class="spip_out" rel="external">Office du tourisme du Portugal</a><br class="manualbr">135, boulevard Haussmann<br class="manualbr">75008 PARIS<br class="manualbr">Tél. : 01 56 88 30 80<br class="manualbr">Fax : 01 56 88 30 89</p>
<h3 class="spip"><a id="sommaire_2"></a>Activités culturelles</h3>
<h4 class="spip">Activités culturelles en français</h4>
<p>L’Institut français du Portugal propose des activités culturelles variées : bibliothèque, cinéma, conférences, spectacles, expositions, concerts.</p>
<p><a href="http://www.ifp-lisboa.com/" class="spip_out" rel="external">Institut français du Portugal</a><br class="manualbr">Avenida Luis Bivar, 91<br class="manualbr">1050-143 Lisbonne<br class="manualbr">Tél. : (351) 213 111 400<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/vie-pratique/article/loisirs-et-culture-111302#infos#mc#ifp-lisboa.com#" title="infos..åt..ifp-lisboa.com" onclick="location.href=mc_lancerlien('infos','ifp-lisboa.com'); return false;" class="spip_mail">Courriel</a></p>
<p>La médiathèque de l’Institut organise également des débats avec des auteurs français et portugais.</p>
<p>Des salles de cinéma présentent, notamment à Lisbonne et à Porto, des films français en version originale sous-titrée.</p>
<p>Les troupes de théâtre et de spectacles musicaux françaises se produisent plusieurs fois par an.</p>
<p>Quelques expositions d’artistes français sont organisées conjointement dans les principaux musées, centres culturels et galeries.</p>
<p><a href="http://www.alliancefr.pt/" class="spip_out" rel="external">La Délégation de l’Alliance Française est située dans le bâtiment de l’Institut français du Portugal</a><br class="manualbr">Avenida Luis Bivar, 91<br class="manualbr">1050-143 Lisbonne<br class="manualbr">Tél. : (351) 213 111 484<br class="manualbr">Fax : (352) 213 157 950<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/vie-pratique/article/loisirs-et-culture-111302#aflisbonne#mc#gmail.com#" title="aflisbonne..åt..gmail.com" onclick="location.href=mc_lancerlien('aflisbonne','gmail.com'); return false;" class="spip_mail">Courriel</a></p>
<p>Les Alliances françaises du Portugal sont des associations culturelles de droit local, à but non lucratif pour certaines, comme à Lisbonne et à Coimbra, déclarées d’utilité publique et qui jouissent d’une complète autonomie. Elles ont pour objectif la diffusion de la langue française. Les cours sont sanctionnés par des diplômes visés par le ministère français de l’éducation nationale et reconnus par le ministère portugais de l’éducation.</p>
<h4 class="spip">Activités culturelles locales</h4>
<p>Il existe de nombreuses salles de cinéma, en cours de numérisation, notamment dans l’enceinte des grands centres commerciaux (salles multiplexes à Lisbonne, Porto, Coïmbra, Aveiro, Braga), qui proposent des films américains pour la plupart (90 % de la programmation), mais aussi européens, dont portugais et français.</p>
<p>Le théâtre propose surtout des œuvres d’auteurs portugais et du grand répertoire international classique.</p>
<p>Des concerts sont présentés à Lisbonne, notamment aux fondations Gulbenkian et Culturgest, au théâtre São Carlos et au Centre culturel de Belém, et à Porto (Coliseu, Europarque, théâtres, fondations). Des festivals annuels sont organisés en Algarve, à Estoril, Sintra, de même que dans le nord (y compris festivals de théâtre, de marionnettes, de cinéma…).</p>
<p>De grandes expositions ont régulièrement lieu à la fondation Gulbenkian, au Centre culturel de Belém, au musée d’art ancien, ainsi qu’au musée du Chiado (musée d’art contemporain). Hors de Lisbonne, de grandes expositions ont lieu à Porto à la fondation Serralves (art contemporain), à Alfandega ("la douane") et au musée d’art contemporain de Sintra.</p>
<p>On visitera aussi à Lisbonne les collections permanentes du musée d’art ancien, du musée Calouste Gulbenkian, ainsi que la maison-musée de la fondation Medeiros e Almeida. A Porto, le musée Soares dos Reis est le plus ancien de la ville.</p>
<p><strong>Lectures</strong></p>
<p>Plusieurs bibliothèques disposent de fonds intéressants : Bibliothèque nationale, Bibliothèque Gulbenkian à Lisbonne, Bibliothèque de l’Université de Coimbra, Bibliothèque Almeida Garett à Porto.</p>
<p>Le Portugal dispose également d’un réseau de librairies, souvent de bonne qualité, avec des fonds en français. Il existe beaucoup de bouquinistes qui vendent des livres français.</p>
<h3 class="spip"><a id="sommaire_3"></a>Sports</h3>
<p>Le football est le sport national, mais l’on peut pratiquer tous les sports, même les plus récents et les plus extrêmes (à l’exception du ski). Il existe de nombreux clubs de sport et l’on peut se procurer sur place tous les équipements nécessaires. Un certain nombre de rencontres sportives internationales se déroulent au Portugal : matchs de la Ligue des Champions, tournoi de tennis d’Estoril, Volta (tour cycliste du Portugal), surf, concours hippiques, courses automobiles…</p>
<p>Un permis est nécessaire pour la chasse et la pêche en rivière (s’adresser aux autorités locales ou régionales pour l’obtenir). Les périodes d’ouverture et de fermeture sont variables selon les espèces.</p>
<p><strong>Les très belles plages du Portugal peuvent être dangereuses, notamment sur la façade atlantique, du fait de la force des vagues et des courants.</strong></p>
<h3 class="spip"><a id="sommaire_4"></a>Télévision – Radio</h3>
<p>La télévision dispose de deux chaînes publiques (RTP1, RTP2) et de deux chaînes privées (SIC, TVI). Elle diffuse beaucoup de programmes étrangers d’origine anglo-américaine ou brésilienne, sous-titrés. Le système adopté est PAL, en cours de numérisation. La vidéo est très répandue et les clubs vidéo sont encore nombreux.</p>
<p>Il est possible de recevoir des chaînes de télévision française avec un équipement adéquat. Dans le nord, bonne réception avec un équipement de base de Eutelsat 2 - F1 (TV5, la Cinquième/ARTE, France 2, France 3, TF1, M6). Réception plus complexe à Lisbonne et dans le sud, avec un équipement coûteux.</p>
<p>Toutes les grandes villes sont câblées. Le câble propose, pour les chaînes françaises : Cinquième/ARTE, TV5, M6, Mezzo et MCM ; pour les chaînes étrangères : TVE, Galicia, RAI, Eurosport, BBC, etc. et une vingtaine de chaînes portugaises.</p>
<h3 class="spip"><a id="sommaire_5"></a>Presse française</h3>
<p>Pratiquement tous les journaux et périodiques français sont disponibles sur place dans nombre de kiosques dans les principales villes et dans certaines librairies spécialisées :</p>
<ul class="spip">
<li>Librairie Leitura <br class="manualbr">Rua de Ceuta, 88 <br class="manualbr">4000 – Porto <br class="manualbr">Tél. : (351) 22 200 45 76</li>
<li>ainsi que dans la plupart des grandes librairies, dont les FNAC et les librairies Bertrand.</li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/vie-pratique/article/loisirs-et-culture-111302). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
