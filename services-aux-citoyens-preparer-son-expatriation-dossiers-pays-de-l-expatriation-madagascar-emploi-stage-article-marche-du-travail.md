# Marché du travail

<p>Depuis l’année 2009, l’économie de Madagascar est tributaire d’une situation politique instable qui a grandement affaibli le pays. Le revenu en 2013, de 447 USD/habitant (source Banque Mondiale), est revenu à son niveau de 2003. Environ 92% de la population vit sous le seuil de pauvreté avec moins de 2 dollars par jour. Au niveau du marché du travail, cette situation s’est traduite par le développement du secteur informel, qui représente en 2013 plus de 90% des emplois.</p>
<h4 class="spip">Secteurs à fort potentiel</h4>
<p>L’économie malgache est aujourd’hui au point mort. La mise en place attendue d’un nouveau gouvernement début 2014 est porteuse de grands espoirs. De nombreux secteurs comme les technologies de l’information et de la communication, les mines, le textile,  l’énergie, les infrastructures, l’agro-alimentaire offrent un fort potentiel de développement.</p>
<h4 class="spip">Barèmes de rémunération</h4>
<p>Sauf pour des postes d’expatriés ou de détachés à responsabilités qui suivent les barèmes classiques internationaux, les barèmes de rémunération malgaches sont les suivants :</p>
<p>Salaire minimum d’embauche mensuel (SME)<br class="autobr">2011 : 90 235,60 AR (32 € env.)<br class="autobr">2012 : 100 011 AR (34 € env.)<br class="autobr">2013 : 108 019 AR (36 € env.)<br class="autobr"><i>Source Caisse nationale de prévoyance sociale (CNAPS)</i></p>
<p><strong>Salaires mensuels moyens donnés à titre indicatif</strong></p>
<ul class="spip">
<li><strong>Secrétaire</strong> : 500 000 à 700 000 AR (164 à 230 EUR env.)</li>
<li><strong>Technicien (bac+2)</strong> : 600 000 à 700 000 AR (197 à 230 EUR env.)</li>
<li><strong>Ingénieur</strong> : 1M AR (328 EUR env.)</li>
<li><strong>Docteur en médecine (secteur privé)</strong> : 1 à 2 M AR (328 à 657 EUR env.)</li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/emploi-stage/article/marche-du-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
