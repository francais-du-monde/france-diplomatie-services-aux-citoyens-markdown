# Curriculum vitae

<p>Les modalités d’embauche sont sensiblement les mêmes qu’en Europe : les CV, lettres de motivation se font généralement en anglais. Il existe toutefois la possibilité de faire traduire les CV et lettres de motivation par des traducteurs.</p>
<p><i>Mise à jour : avril 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/emploi-stage/article/curriculum-vitae-112189). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
