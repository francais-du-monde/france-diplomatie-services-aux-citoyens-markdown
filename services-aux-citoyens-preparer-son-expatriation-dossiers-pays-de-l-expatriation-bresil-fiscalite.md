# Fiscalité

<h2 class="rub22828">Fiscalité du pays</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/fiscalite/#sommaire_1">Quitus fiscal</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/fiscalite/#sommaire_2">Coordonnées des centres d’information fiscale</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/fiscalite/#sommaire_3">Impôts directs et indirects</a></li></ul>
<p>Des points de communication sont mis en place, dans les grandes ville, par le ministère des Finances, au moment des déclarations d’impôt, afin de faciliter le dépôt de ces dernières et l’information du public.</p>
<p>Les imprimés doivent être retirés et déposés auprès de ces points de communication (à Brasilia, dans tous les centres commerciaux) et directement auprès dudit ministère ou téléchargés sur internet</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.receita.fazenda.gov.br/" class="spip_out" rel="external">site de l’administration fédérale des impôts</a>.</p>
<p>La date de dépôt est la fin du mois d’avril.</p>
<p>Depuis 1999, les professionnels étrangers arrivés au Brésil avec un visa temporaire et un contrat de travail sont imposés dès leur arrivée comme s’ils résidaient au Brésil (même chose pour les personnes munies d’un visa permanent) sur tous les revenus perçus en contrepartie de leur travail effectué au Brésil. Les personnes arrivées au Brésil avec un visa temporaire mais sans contrat de travail sont considérées non résidents pendant les six premiers mois de leur séjour. Au delà de ce délai, ils basculent dans le régime de droit commun.</p>
<p>L’employeur est tenu de déclarer au Trésor public les salaires qu’il a versés pendant l’année.</p>
<h3 class="spip"><a id="sommaire_1"></a>Quitus fiscal</h3>
<p>Un expatrié peut solder son compte en fin de séjour ; il est cependant nécessaire de fournir un quitus fiscal.</p>
<h3 class="spip"><a id="sommaire_2"></a>Coordonnées des centres d’information fiscale</h3>
<p>L’information sur la fiscalité est disponible sur le <a href="http://www.receita.fazenda.gov.br/" class="spip_out" rel="external">site de l’administration fédérale des impôts</a>.</p>
<h3 class="spip"><a id="sommaire_3"></a>Impôts directs et indirects</h3>
<h4 class="spip">Impôt sur le revenu des personnes physiques</h4>
<p>Au Brésil, la détermination de la base imposable est très large et comprend les salaires, mais aussi la plupart des avantages octroyés par l’employeur tels que la prise en charge des frais de scolarité, de déménagement, les primes de logement, les primes d’expatriation. En revanche, les prélèvements mensuels sur salaires, de l’ordre de 8% (FGTS), capitalisés sur le compte nominatif et bloqués en faveur du salarié, ne sont pas imposables.</p>
<table class="spip">
<thead><tr class="row_first"><th id="id7921_c0">Revenus mensuels (en reals)</th><th id="id7921_c1">Taux</th><th id="id7921_c2">Part déductible de l’assiette</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="id7921_c0">de 0 à 1372,81</td>
<td headers="id7921_c1">0 %</td>
<td class="numeric virgule" headers="id7921_c2">-</td></tr>
<tr class="row_even even">
<td headers="id7921_c0">de 1372,81 à 2326</td>
<td headers="id7921_c1">15 %</td>
<td class="numeric virgule" headers="id7921_c2">205,92</td></tr>
<tr class="row_odd odd">
<td headers="id7921_c0">au-delà de 2743,25</td>
<td headers="id7921_c1">27,5 %</td>
<td class="numeric virgule" headers="id7921_c2">548,82</td></tr>
</tbody>
</table>
<p>Le taux d’imposition pour le revenu des étrangers considérés non résidents est passé de 15 à 25%. Ainsi, les personnes physiques étrangères qui se trouvent à l’étranger au service d’entreprises sises au Brésil, avec ou sans contrat de travail, aussi bien que celles arrivées au Brésil sans contrat de travail et qui y séjourneront moins de six mois, voient leurs revenus imposés à la source avec un taux de 25%.</p>
<p><strong>Déductions possibles</strong></p>
<ul class="spip">
<li>Cotisations de sécurité sociale ;</li>
<li>Participation à des fonds de pension brésiliens ;</li>
<li>Plans de santé privés brésiliens agréés ;</li>
<li>Dépenses médicales ;</li>
<li>Dépenses de scolarité.</li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-bresil-fiscalite-article-convention-fiscale.md" title="Convention fiscale">Convention fiscale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-bresil-fiscalite-article-fiscalite-du-pays-109991.md" title="Fiscalité du pays">Fiscalité du pays</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/fiscalite/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
