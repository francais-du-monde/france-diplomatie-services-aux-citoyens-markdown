# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/emploi-stage/article/reglementation-du-travail-112452#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/emploi-stage/article/reglementation-du-travail-112452#sommaire_2">Les conditions salariales</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/emploi-stage/article/reglementation-du-travail-112452#sommaire_3">Contrat de travail – spécificités</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/emploi-stage/article/reglementation-du-travail-112452#sommaire_4">Fêtes légales</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/emploi-stage/article/reglementation-du-travail-112452#sommaire_5">Emploi du conjoint</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<p>En Espagne, la législation du travail est constituée au niveau national par un ensemble de lois et de décrets relatifs à l’emploi qui constitue la « <a href="http://www.mtin.es/es/guia/texto/index.htm" class="spip_out" rel="external">Guía Laboral</a> », consultable sur le site du <a href="http://www.empleo.gob.es/" class="spip_out" rel="external">ministère de l’Emploi et de la Sécurité sociale espagnol</a> (<i>Ministerio de Empleo y Seguridad Social</i>).</p>
<p>Les conditions de travail du travailleur salarié, quant à elles, sont principalement réglementées par le <strong>Statut des travailleurs</strong> (<i>Estatuto de los Trabajadores</i>) approuvé par le Real Decreto Legislativo 1/1995, du 24 mars 1995.</p>
<p>Ces dispositions légales constituent la base minimale législative du droit du travail espagnol, à laquelle il ne peut être dérogé.</p>
<p>A l’échelle des Communautés autonomes, des conventions collectives sont conclues entre les organisations patronales et syndicales de chaque secteur d’activité afin d’en réguler les conditions de travail. De la même façon, des accords sur les conditions de travail des employés peuvent être passés au sein des entreprises.</p>
<p>Vous pouvez consulter les caractéristiques de tous les <a href="http://www.convenios.juridicas.com/" class="spip_out" rel="external">convenios</a> en vigueur, selon la catégorie professionnelle et la Communauté.</p>
<p>Ces conventions et accords peuvent prévoir des conditions de travail plus favorables que celles octroyées au niveau national mais ne peuvent en aucun cas être moins favorables.</p>
<h3 class="spip"><a id="sommaire_2"></a>Les conditions salariales</h3>
<p><strong>Modalités de rétribution</strong></p>
<p>La loi prévoit que le salarié doit bénéficier de deux primes annuelles (en plus des 12 mois de salaire), l’une en juillet et l’autre en décembre, chacune équivalente à un mois de salaire. La convention collective dont dépend le salarié peut prévoir le prorata de ces primes en 12 mensualités.</p>
<p>L’employeur verse le salaire net, c’est-à-dire une fois effectuées les retenues des cotisations de sécurité sociale et de l’impôt sur le revenu qui est prélevé à la source. Dans le cas d’un contrat à durée déterminée, le salaire sera grevé de 6,40 % en raison des charges sociales, et de 6,35 % dans le cas d’un contrat à durée indéterminée.</p>
<p><strong>Montant du salaire</strong></p>
<p>Il existe un salaire minimum (SMI, Salario Mínimo Interprofesional) dont le montant est fixé annuellement. En 2014, il s’élève à 645,30 euros nets mensuels.</p>
<p><strong>Durée du travail</strong></p>
<p>La durée hebdomadaire légale du travail est de 40 heures. Toutefois la convention collective ou le contrat de travail peuvent prévoir des conditions plus favorables. Ainsi, les horaires d’été ou la journée continue le vendredi sont des pratiques assez fréquentes en Espagne. Il est à noter que la distribution irrégulière de ces 40 heures hebdomadaires sur l’année est permise par la loi dans certaines limites.</p>
<p>Les heures supplémentaires doivent être récupérées par un temps de repos équivalent ou rémunérées, en fonction de ce que prévoit la convention collective ou à défaut le contrat de travail. Il faut souligner que la loi ne prévoit pas que la rémunération des heures supplémentaires doive être nécessairement supérieure à celles des heures ordinaires. Quoi qu’il en soit, ces règles sont peu respectées en pratique. Réaliser de nombreuses heures supplémentaires est une pratique professionnelle ancrée dans la culture de travail du pays et il est très rare que celles-ci soient compensées de quelque manière que ce soit.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://ec.europa.eu/eures/" class="spip_out" rel="external">Eures</a> Espagne  Vivre et travailler</li>
<li>Site du <a href="http://www.sepe.es/" class="spip_out" rel="external">SEPE</a>(servicio publico de empleo estatal)</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Contrat de travail – spécificités</h3>
<p><strong>Les principaux types de contrat</strong></p>
<p>Les contrats doivent en principe être écrits. Si le contrat est seulement verbal, il est supposé être à durée indéterminée (sauf dans l’hypothèse où ce contrat aurait une durée inférieure à quatre semaines). Il existe quatre grands types de contrats de travail :</p>
<p><strong>Le contrat à durée indéterminée</strong></p>
<p>C’est le contrat de droit commun. Il existe une présomption de droit selon laquelle les contrats conclus en infraction de la loi sont des contrats à durée indéterminée.</p>
<p>En cas de licenciement collectif ou individuel pour motifs économiques, l’employé a droit à des indemnités de licenciement payées par l’employeur équivalentes à vingt jours de salaire par année de service dans l’entreprise. Cette règle ne s’applique pas si le licenciement est dû à une faute de l’employé, à son inaptitude ou à sa mauvaise adaptation au poste de travail. L’employeur doit prévenir le salarié de son licenciement en respectant un préavis qui varie selon la cause du licenciement. Néanmoins, ce préavis est en général payé par l’employeur mais non réalisé. Quand la décision de licenciement est prise, l’employeur va exiger que l’employé quitte son poste très rapidement.</p>
<p>D’un autre côté, si l’employé souhaite démissionner, il est à signaler qu’il n’a besoin d’alléguer aucune cause et qu’il n’existe pas de préavis légal. Il doit toutefois respecter, le cas échéant, les clauses de la convention collective dont il dépend ou la coutume du lieu. Le préavis qui peut être prévu est peu contraignant puisqu’il dépasse rarement quinze jours, et la sanction de son non-respect se limite, sauf cas particulier, au non-paiement des jours de préavis correspondants.</p>
<p><strong>Les contrats temporaires</strong></p>
<p>Ces contrats doivent répondre à des causes précises, ces contrats sont de durée déterminée dans certains cas et non déterminée dans d’autres. Dans le cas le plus répandu du contrat pour « obra de servicio », il ne peut excéder trois années. Les différents types de contrats sont consultables sur le site du <a href="http://www.sepe.es/" class="spip_out" rel="external">SEPE</a>.</p>
<p><strong>Le contrat de formation et apprentissage</strong></p>
<p>Ce contrat vise à favoriser l’insertion et la formation des jeunes. Il est effectué dans un régime d’alternance entre des périodes d’activité rémunérée en entreprise et de formation reçue dans le cadre de la formation professionnelle pour l’emploi ou du système éducatif espagnol.</p>
<p><strong>Le contrat de stage</strong></p>
<p>Il existe également des contrats destinés à faciliter l’insertion des jeunes diplômés sur le marché de l’emploi. Dans ce cadre, il est important d’effectuer une distinction entre le <i>contrato en prácticas</i> et <i>las prácticas</i>.</p>
<p>Le contrat <i>en prácticas</i> constitue un véritable contrat de travail et son titulaire bénéficie donc de toutes les garanties qui lui sont propres, notamment en matière de protection sociale. C’est un contrat à durée déterminée de six mois à deux années destiné aux jeunes diplômés dans une limite de quatre ans suivant l’obtention du diplôme. Les conditions salariales sont avantageuses pour l’employeur mais celui-ci doit néanmoins respecter certains minima, à savoir, durant la première année du contrat, 60 % du salaire fixé par la convention collective pour un poste équivalent et 75 % de ce même salaire la seconde année. Cette rémunération ne peut être en aucun cas inférieure au salaire minimum.</p>
<p>En revanche les <i>prácticas</i> seraient l’équivalent du stage français et son statut juridique est plus flou. Elles sont normalement destinées aux étudiants dans leur dernière ou avant-dernière année d’études. Une convention tripartite signée entre l’employeur, l’étudiant et l’établissement dont dépend ce dernier régit les conditions de réalisation de l’activité. Les personnes ayant achevé leurs études et souhaitant effectuer un stage peuvent néanmoins faire appel à des associations habilitées, sous certaines conditions, à conclure des conventions de stage. Tel est le cas de l’association franco-espagnole <a href="http://www.dialogo.es/" class="spip_out" rel="external">Dialogo</a> à laquelle les Français souhaitant s’insérer sur le marché du travail espagnol peuvent s’adresser. La personne <i>en prácticas</i> ne bénéficie pas d’un contrat de travail et n’a pas le statut de salarié. Elle ne jouit donc pas de la protection sociale commune aux travailleurs salariés et doit s’assurer de disposer personnellement d’une</p>
<p>assurance-maladie et d’une assurance couvrant les accidents du travail. L’employeur ne lui versera pas de salaire même si des aides ou bourses <i>becas</i> sont fréquemment accordées. Celles-ci sont généralement d’un montant limité, avoisinant les 300 euros mensuels.</p>
<h3 class="spip"><a id="sommaire_4"></a>Fêtes légales</h3>
<ul class="spip">
<li>1er janvier (Jour de l’an),</li>
<li>6 janvier (Epiphanie),</li>
<li>Jeudi et Vendredi Saint,</li>
<li>Lundi de Pâques,</li>
<li>1er mai (Fête du travail),</li>
<li>Pentecôte,</li>
<li>15 août (Assomption),</li>
<li>12 octobre (fête nationale),</li>
<li>1er novembre (Toussaint),</li>
<li>6 décembre (jour de la Constitution),</li>
<li>8 décembre (Fête de l’Immaculée Conception),</li>
<li>25 décembre (Noël).</li></ul>
<p>A ces fêtes, s’ajoutent de nombreuses fêtes religieuses et locales :</p>
<ul class="spip">
<li>Madrid : 2 mai (Comunidad de Madrid), 15 mars (San Isidro),</li>
<li>Bilbao : 25 Juillet (Santiago, Saint Patron de Bilbao),</li>
<li>Barcelone : Saint Jean (24 juin), Diada (11 septembre), Merced (24 septembre) et San Esteban (26 décembre),</li>
<li>Séville : 28 février (Constitution de l’Andalousie), 30 mai (San Fernando), Corpus Christi (Fête Dieu) - date variable, 12 octobre (Fête de l’Hispanité).</li></ul>
<p>Les jours fériés sont soumis à des changements par les autorités locales (Communautés autonomes).</p>
<h3 class="spip"><a id="sommaire_5"></a>Emploi du conjoint</h3>
<p>Si le conjoint est lui-même ressortissant d’un Etat membre de l’Union européenne, le principe de libre circulation des travailleurs s’applique.</p>
<p>Si le conjoint est ressortissant d’un pays tiers, il sera nécessaire de solliciter une carte de résidence en tant que parent d’un citoyen de l’UE. Il pourra exercer une activité économique, salariée ou indépendante, ou de prestation de services, dans les mêmes conditions que les citoyens espagnols.</p>
<p><i>Mise à jour : avril 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/emploi-stage/article/reglementation-du-travail-112452). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
