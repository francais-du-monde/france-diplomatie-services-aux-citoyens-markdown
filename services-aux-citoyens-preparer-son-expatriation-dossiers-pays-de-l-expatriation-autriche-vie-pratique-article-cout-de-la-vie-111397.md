# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/vie-pratique/article/cout-de-la-vie-111397#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/vie-pratique/article/cout-de-la-vie-111397#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/vie-pratique/article/cout-de-la-vie-111397#sommaire_3">Alimentation</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/vie-pratique/article/cout-de-la-vie-111397#sommaire_4">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>L’unité monétaire est l’euro.</p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<p>En principe, tout citoyen d’un Etat partie à l’EEE peut effectuer librement des transferts de fonds. Le transfert de capital dans une banque autrichienne ne devrait pas en principe s’accompagner d’aucun frais. Il convient d’être vigilant, aussi bien avec la banque française qu’autrichienne, afin que cette réglementation soit respectée. Des informations et des conseils sur l’octroi de crédits, les hypothèques, les garanties et les investissements peuvent être obtenus auprès de n’importe quelle banque.</p>
<p>Trois grandes banques françaises sont présentes sur le marché autrichien : Société Générale, BNP-Paribas, Crédit Agricole, mais aucun de ces établissements ne gère de comptes de particuliers.</p>
<p>Parmi les moyens de paiement, le virement est largement utilisé. Dans les magasins, hôtels et restaurants, l’utilisation de la carte bancaire ou du numéraire est préférée. Le chèque est rarement utilisé et peut être refusé. Il est important de souligner que les Autrichiens préfèrent payer et recevoir en liquide plutôt qu’avec une carte de crédit. Pour cela, les distributeurs de billets sont nombreux et mettent à disposition des coupures de 100 €, 50 € et 10 € en général. Ils acceptent par exemple les cartes "visa" ou "mastercard" et American express. Pour cette dernière carte, une commission est prélevée par l’organisme.</p>
<p>Les horaires d’ouverture des banques sont différents de celles des magasins, il faut faire attention car celles-ci ferment tôt (vers 16h en général) sauf parfois le jeudi où elles peuvent repousser leurs horaires de fermeture d’une heure à une heure et demie.</p>
<p><strong>Ouvrir un compte en Autriche :</strong></p>
<p>La banque vous demandera une pièce d’identité (carte d’identité ou passeport) et une déclaration de résidence (Meldezettel). Le compte le plus courant en Autriche est le compte courant (Girokonto). En général, un compte courant vous permet de :</p>
<ul class="spip">
<li>Retirer de l’argent de votre compte en utilisant une carte-EC (carte bancaire). Ces retraits sont gratuits sur le DAB de votre banque, mais dans le cas où vous l’utilisez sur le DAB d’une autre banque, on vous facturera.</li>
<li>Effectuer et recevoir des virements ponctuels</li>
<li>Effectuer et recevoir des virements réguliers</li>
<li>Pouvoir payer vos factures par prélèvements bancaires. Dans la plupart des banques vous pouvez choisir entre plusieurs types de compte courants offrant des services différents (ex : banque en ligne, intérêts, carte de crédit gratuite, etc.) et les frais sont aussi différents. Le compte est payant et peut porter des intérêts (très faibles).</li></ul>
<p>Le 1er février 2014 aurait dû entrer en vigueur l’Espace unique de paiement en euros (Single Euro, Payments Area, SEPA), afin de favoriser la libre concurrence des entreprises dans l’Espace économique européen en harmonisant les règles et les moyens de paiements. Cette entrée en vigueur devrait finalement n’intervenir que le 1er juillet 2014.</p>
<h3 class="spip"><a id="sommaire_3"></a>Alimentation</h3>
<p>Les grandes surfaces sont moins répandues en Autriche qu’en France. Les courses se font généralement dans les moyennes surfaces. Les magasins ferment généralement entre 18h30 et 19h30 en semaine et à 18h au plus tard le samedi. Ils peuvent fermer plus tôt dans les petites villes de province.</p>
<h4 class="spip">Prix moyen d’un repas dans un restaurant</h4>
<p>Il faut compter de 12 à 15 € pour un repas de midi simple dans un self-service ou un petit restaurant (boisson et café compris). Pour le déjeuner, la plupart des restaurants proposent un menu du jour au tarif modéré (moins de 15€). Pour un dîner dans un bon restaurant, il faudra compter plus de 50 € par personne.</p>
<p>On peut utilement consulter le site <a href="http://www.gastro-wien.at/bezirke/lokale.htm" class="spip_out" rel="external">Gastro-wien.at</a>, qui, selon l’établissement, permet aussi de consulter le menu.</p>
<p>Un pourboire, de l’ordre de 10%, est d’usage.</p>
<h3 class="spip"><a id="sommaire_4"></a>Evolution des prix</h3>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la [Banque mondiale-<a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL</a>]</p>
<p><i>Mise à jour : février 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/vie-pratique/article/cout-de-la-vie-111397). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
