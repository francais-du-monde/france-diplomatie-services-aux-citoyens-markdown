# Marché du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/emploi-stage/article/marche-du-travail-113142#sommaire_1">Secteurs porteurs</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/emploi-stage/article/marche-du-travail-113142#sommaire_2">Professions règlementées</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/emploi-stage/article/marche-du-travail-113142#sommaire_3">Rémunération</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Secteurs porteurs</h3>
<ul class="spip">
<li>Les domaines du luxe, des cosmétiques et de la coiffure</li>
<li>L’alimentation et la restauration</li>
<li>La communication et les médias (publicité, cinéma, graphisme)</li>
<li>Le tourisme</li>
<li>Les nouvelles technologies</li>
<li>L’enseignement du français</li>
<li>Secteur infirmier</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Professions règlementées</h3>
<p>Il y a plus de 50 domaines professionnels réglementés aux Etats-Unis. La plupart demandent un diplôme de l’enseignement supérieur, mais les pré-requis peuvent différer d’un Etat à un autre : il faut donc consulter directement les associations professionnelles et les autorités qualifiées.</p>
<ul class="spip">
<li>Comptabilité</li>
<li>Architecture</li>
<li><a href="http://www.babla.fr/francais-anglais/chiropraxie" class="spip_out" rel="external">Chiropraxie</a></li>
<li>Soins dentaires</li>
<li>Nutrition</li>
<li>Ingénierie</li>
<li>Droit</li>
<li>Médecine</li>
<li>Professions paramédicales</li>
<li>Secteur infirmier</li>
<li>Ergothérapie</li>
<li>Ophtalmologie</li>
<li>Ostéopathie</li>
<li>Pharmacie</li>
<li>Kinésithérapie</li>
<li>Assistance sociale</li>
<li>Enseignement du primaire</li>
<li>Médecine vétérinaire</li></ul>
<p>Cette liste est non exhaustive, pour plus d’informations, veuillez contacter le <a href="http://www2.ed.gov/about/offices/list/ous/international/usnei/us/edlite-visitus-forrecog.html" class="spip_out" rel="external">ministère de l’éducation américain</a> et consulter le site <a href="http://enic-naric.net/index.aspx?c=USA" class="spip_out" rel="external">Enic-Naric</a>.</p>
<h3 class="spip"><a id="sommaire_3"></a>Rémunération</h3>
<p>L’échelle des salaires est beaucoup plus large aux Etats-Unis qu’en Europe.</p>
<p>La rémunération moyenne d’un Américain, toutes catégories confondues est de 821,79 USD par semaine. Cette somme peut varier selon le poste et le salaire minimum établi dans l’Etat de résidence.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/emploi-stage/article/marche-du-travail-113142). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
