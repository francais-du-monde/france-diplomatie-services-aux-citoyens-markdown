# Stages

<p><strong>Recherche individuelle</strong></p>
<p>Si vous ne souhaitez pas faire appel à des organismes de recherche de stage, vous pouvez solliciter directement les entreprises nationales et/ou internationales du secteur qui vous intéresse et leur proposer vos services dans le cadre d’une expérience pratique sous forme de stage. La garantie d’obtenir un stage rémunéré est faible.</p>
<p><strong>Programmes d’échange et de stage pratique</strong> (<i>employment work experience and internship</i>)</p>
<p>Il existe plusieurs façons d’acquérir une expérience professionnelle en Australie, notamment par le biais de programmes d’échange et de stages pratiques. Les organismes compétents assistent généralement les candidats dans la procédure de demande de visa, pour le transfert à l’aéroport et l’hébergement et leur trouvent un lieu de stage. Ces services ne sont pas nécessairement gratuits, mais offrent un réseau de contacts immédiat.</p>
<p><strong>Bénévolat</strong> (<i>voluntary help</i>)</p>
<p>Ce type d’expérience est également envisageable en Australie et est valorisée. Elle n’offre pas de compensation financière. Pensez à la mentionner dans votre curriculum vitae.</p>
<p><strong>Volontariat</strong> (volunteering)</p>
<p>Il se décline sous différentes formes. Cette activité est généralement rémunérée et offre des avantages spécifiques tels que l’exemption d’impôt (<i>tax break, tax saving or tax exemption</i>).</p>
<p><strong>Sites Internet à consulter :</strong></p>
<ul class="spip">
<li><a href="http://www.bunac.org/" class="spip_out" rel="external">British Universities North America Club</a> (BUNAC)</li>
<li><a href="http://www.govolunteer.com.au/" class="spip_out" rel="external">Go Volunteer</a> est un portail Internet, à but non lucratif, de recrutement de volontaires</li></ul>
<h4 class="spip">Avez-vous pensé au volontariat international (V.I.E ou V.I.A.) ?</h4>
<p>Le V.I.E. permet aux entreprises françaises de confier à un jeune ressortissant de l’espace économique européen, âgé de 18 à 28 ans, une mission professionnelle (rémunérée) à l’étranger durant une période modulable de 6 à 24 mois.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.civiweb.com/FR/index.aspx" class="spip_out" rel="external">Civiweb</a></li>
<li><a href="http://export.businessfrance.fr/default.html" class="spip_out" rel="external">Business France</a></li>
<li>e-mail : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/emploi-stage/article/stages#infoVIE#mc#ubifrance.fr#" title="infoVIE..åt..ubifrance.fr" onclick="location.href=mc_lancerlien('infoVIE','ubifrance.fr'); return false;" class="spip_mail">infoVIE<span class="spancrypt"> [at] </span>ubifrance.fr</a></li></ul>
<p><i>Mise à jour : octobre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/emploi-stage/article/stages). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
