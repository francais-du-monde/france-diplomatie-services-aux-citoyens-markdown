# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/emploi-stage/article/reglementation-du-travail-108390#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/emploi-stage/article/reglementation-du-travail-108390#sommaire_2">Contrat de travail – spécificités</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/emploi-stage/article/reglementation-du-travail-108390#sommaire_3">Fêtes légales</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/emploi-stage/article/reglementation-du-travail-108390#sommaire_4">Le visa de travail</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<p>La loi fédérale s’applique à tous les employés recrutés localement. Le personnel expatrié peut cependant bénéficier d’un statut particulier, en fonction du contrat souscrit avec l’entreprise française qui le recrute.</p>
<p><strong>Syndicats</strong> : La loi fédérale du travail et la Constitution disposent que 20 salariés au moins peuvent mettre en place un syndicat dans l’entreprise afin de représenter leurs intérêts. Les syndicats sont particulièrement puissants dans les secteurs du pétrole, de l’énergie électrique, de la mine, des télécommunications et de l’édition. Deux centrales, fortement politisées, contrôlent le mouvement syndical : la Confédération des travailleurs mexicains (CTM) et le Congrès du travail (CT). Une nouvelle centrale est apparue en 1997, l’Union nationale des travailleurs (UNT).</p>
<p>Les négociations collectives, qui portent sur les conditions de travail et les salaires, doivent avoir lieu tous les ans ou tous les deux ans. Le refus de négocier de la part de l’employeur est une cause légitime de grève.</p>
<p><strong>Durée du travail</strong> : La durée journalière de travail varie en fonction du type d’horaire de travail effectué par le salarié. La journée ordinaire de travail est de 8 heures si le travail est effectué entre 6h et 20h et de 7 heures, entre 20h et 6h. Dans le cas d’une journée de travail continue, une demi-heure de repos au minimum doit être accordée au travailleur. Pour 6 jours de travail, il doit être accordé au travailleur une journée de repos payée intégralement, si possible le dimanche. La semaine de travail est donc de 48 heures maximum.</p>
<p><strong>Congés payes</strong> : La loi mexicaine prévoit que la durée des congés payés ne peut être inférieure à 6 jours ouvrés par année. La durée des congés payés est ensuite augmentée en fonction de l’ancienneté dans l’entreprise.</p>
<p><strong>Retraite</strong> : La retraite peut être prise à partir de 65 ans ou après 30 années effectives de travail. Le Mexique est progressivement passé d’un système par répartition à un système par capitalisation.</p>
<p><strong>Cotisations sociales</strong> : Les employeurs ont l’obligation d’inscrire leurs salariés à la sécurité sociale (IMSS). Le poids des charges sociales oscille entre 25 % et 35 % suivant le niveau du salaire mensuel. Plus de la moitié est supportée par l’employeur.</p>
<p>L’employeur a l’obligation de verser 5 % des salaires au Fonds national de logement des travailleurs (INFONAVIT), créé en 1972. Le but de ce fonds est d’octroyer des crédits à taux préférentiels pour l’acquisition de logements sociaux.</p>
<p><strong>Formation professionnelle continue</strong> : La loi prévoit le droit de chaque salarié à recevoir une formation continue adaptée à sa capacité et à ses besoins. Les employeurs ont l’obligation de mettre en place des comités de formation professionnelle, composés de membres de la direction et de représentants des salariés, dans chaque entreprise. Dans la pratique ce système se révèle de faible ampleur.</p>
<h3 class="spip"><a id="sommaire_2"></a>Contrat de travail – spécificités</h3>
<p>Au Mexique, l’établissement d’un contrat de travail écrit est obligatoire pour toutes les embauches. Les conditions de travail doivent y être stipulées avec la plus grande précision. L’embauche de salariés de moins de 16 ans doit se faire avec l’accord des parents. Les enfants de moins de 12 ans n’ont officiellement pas le droit de travailler.</p>
<p>La loi fédérale du travail dispose que l’employeur ne pourra procéder à un licenciement que pour une « cause réelle et sérieuse » (art. 47). Les salariés ayant plus de 20 ans d’ancienneté dans l’entreprise ne pourront être licenciés que pour faute grave. Le licenciement pour motif économique n’existe pas au Mexique. En fait, les licenciements prennent généralement la forme d’une démission négociée. Les employés licenciés ont le droit à une indemnité correspondant à 20 jours de salaire par année de présence.</p>
<h3 class="spip"><a id="sommaire_3"></a>Fêtes légales</h3>
<p>Il existe au Mexique deux sortes de jours fériés :</p>
<ul class="spip">
<li>les jours fériés officiels : l’employeur est obligé de les donner à ses employés ;</li>
<li>les jours fériés non-officiels : l’employeur se réserve le droit de les donner ou non.</li></ul>
<p><strong>Jours fériés officiels 2013 :</strong></p>
<ul class="spip">
<li>1er janvier : Nouvel an</li>
<li>5 février : Jour de la Constitution</li>
<li>21 mars : anniversaire de Benito Juárez</li>
<li>1er mai : fête du travail</li>
<li>16 septembre : fête de l’Indépendance</li>
<li>20 novembre : Révolution mexicaine</li>
<li>25 décembre : Noël</li></ul>
<p><strong>Jours fériés non-officiels 2013 :</strong></p>
<ul class="spip">
<li>28 mars : Jeudi Saint.</li>
<li>29 mars : Vendredi Saint.</li>
<li>31 mars : Pâques.</li>
<li>10 mai : fête des mères</li>
<li>2 novembre : commémoration des défunts</li>
<li>24 décembre : réveillon de Noël</li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Le visa de travail</h3>
<p>Tous les étrangers qui effectuent une activité professionnelle sur le territoire mexicain doivent obtenir un permis de travail délivré par les autorités mexicaines d’immigration. Bien souvent les entreprises souhaitant embaucher des candidats français par exemple, privilégieront les candidats déjà en possession d’un visa de travail. Avec l’entrée en vigueur le 9 novembre 2012 de la nouvelle réglementation migratoire (loi du 29 avril 2011), il n’est plus désormais possible, pour un étranger ayant intégré le pays avec un visa de touriste, de changer son statut migratoire sur place. Il est encore possible d’explorer le marché du travail en tant que touriste, mais une fois une offre d’emploi écrite obtenue de l’entreprise, l’intéressé devra se rendre dans un Consulat Mexicain de son choix (par exemple aux États-Unis ou au Guatemala) et y solliciter à ce moment-là son visa de travail. Une fois de retour au Mexique, le candidat devra se rendre à l’Institut d’Immigration pour échanger cette autorisation de visa contre un visa de résident temporaire valable jusqu’à 4 ans. <strong>Il est bien entendu conseillé de rentrer au Mexique avec un visa de travail.</strong></p>
<p>En général comptez un mois pour l’obtention de votre visa de travail. Pour le renouvellement de votre visa (permis de travail) nous vous conseillons également de vous y prendre un mois avant expiration de votre visa en cours.</p>
<p>A ce sujet, consultez également notre article <a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-mexique-entree-et-sejour-article-passeport-visa-permis-de-travail-108383.md" class="spip_in">"Passeport, visa, permis de travail"</a>.</p>
<p><i>Mise à jour : septembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/emploi-stage/article/reglementation-du-travail-108390). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
