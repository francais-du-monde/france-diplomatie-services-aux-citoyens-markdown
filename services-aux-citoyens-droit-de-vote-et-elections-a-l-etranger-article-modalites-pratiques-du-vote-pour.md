# Modalités pratiques du vote pour les Français établis hors de France

<h4 class="spip">Election des conseillers consulaires</h4>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Si j’ai une carte consulaire, je suis inscrit(e) sauf décision contraire de ma part.</p>
<h4 class="spip">Election du président de la République, référendums, élections législatives et européennes</h4>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Je ne suis pas inscrit(e) sur une liste électorale en France :</strong> je ne vote qu’à l’étranger (scrutins nationaux)</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Je suis inscrit(e) ou je demande mon inscription sur la liste électorale d’une commune en France : </strong> </p>
<ul class="spip">
<li>si je choisis de voter à l’étranger pour les scrutins nationaux, je vote en France uniquement pour les scrutins locaux.</li>
<li>si je choisis de voter à l’étranger uniquement pour l’élection des conseillers consulaires, je vote en France pour les scrutins nationaux et locaux.</li></ul>
<p><i>Mise à jour : janvier 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/droit-de-vote-et-elections-a-l-etranger/article/modalites-pratiques-du-vote-pour). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
