# Déménagement

<p>Les formalités de dédouanement ne peuvent être effectuées qu’après avoir reçu du ministère des Affaires intérieures la carte de séjour (10 jours de délai). Le transitaire turc désigné se chargera alors de toutes les formalités de dédouanement (10 à 15 jours). Il est nécessaire de fournir un inventaire détaillé.</p>
<p>Les formalités sont les mêmes quel que soit le mode de transport choisi (avion ou bateau). Pour un déménagement conséquent, le bateau (trois à six semaines) ou la route (une à deux semaines) sont préférables. L’avion, plus onéreux, est à réserver aux déménagements réduits.</p>
<p>Pour en savoir plus :</p>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-douanes.md" class="spip_in">Douanes</a></li>
<li><a href="http://www.ambafrance-tr.org/spip.php?article40" class="spip_out" rel="external">Ambassade de France en Turquie</a></li></ul>
<p>S’agissant d’un déménagement international, il est primordial de s’entourer d’un maximum de garanties en faisant appel à un professionnel disposant de certifications reconnues (marque NF Service, ISO 9002, etc.)</p>
<p><a href="http://www.csdemenagement.fr/" class="spip_out" rel="external">Chambre syndicale du déménagement</a> <br class="manualbr">Téléphone : 01 49 88 61 40 <br class="manualbr">Télécopie : 01 49 88 61 46<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/entree-et-sejour/article/demenagement-110014#contact#mc#csdemenagement.fr#" title="contact..åt..csdemenagement.fr" onclick="location.href=mc_lancerlien('contact','csdemenagement.fr'); return false;" class="spip_mail">Courriel</a></p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/entree-et-sejour/article/demenagement-110014). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
