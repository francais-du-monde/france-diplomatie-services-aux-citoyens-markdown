# Vie pratique

<h2 class="rub23463">Logement</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/vie-pratique/#sommaire_1">Où se loger ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/vie-pratique/#sommaire_2">Conditions de location</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/vie-pratique/#sommaire_3">Hôtels</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/vie-pratique/#sommaire_4">Auberges de jeunesse</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/vie-pratique/#sommaire_5">Electricité</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/vie-pratique/#sommaire_6">Electroménager</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Où se loger ?</h3>
<p><strong>Exemples de loyers (mensuels) à Budapest </strong></p>
<p>Les bons quartiers sont Vizivaros et Lipotvaros. Les très bons quartiers sont Pasaret et Huvosvolgy et les meilleurs quartiers sont Rozsadomb, Svabhegy et Zuglo.</p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Lieu</td>
<td>Studio</td>
<td>3 pièces</td>
<td>5 pièces</td>
<td>Villa</td></tr>
<tr class="row_even even">
<td>Quartier résidentiel</td>
<td>700€</td>
<td>1200€</td>
<td>1800€</td>
<td>4000€</td></tr>
<tr class="row_odd odd">
<td>Périphérie</td>
<td>500€</td>
<td>900€</td>
<td>1500€</td>
<td>3000€</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_2"></a>Conditions de location</h3>
<p>Le marché locatif à destination des étrangers reste relativement restreint, d’où des prix élevés. Les quartiers résidentiels se trouvent plutôt du côté de Buda, dans les 1er (quartier du château), 2<sup>ème</sup>, 11<sup>ème</sup> et 12<sup>ème</sup> arrondissements. La recherche d’un logement se fait soit par petites annonces, soit par l’intermédiaire d’une agence. Il faut compter environ deux semaines de recherches. Un appartement est un peu plus facile a trouvé qu’une maison.</p>
<p>La durée des baux est variable, avec un préavis de résiliation de 30 à 90 jours. On demande généralement un à trois mois de loyer d’avance ainsi qu’une caution équivalant à un ou deux mois. Il est nécessaire de procéder à un état des lieux. En cas de location par l’intermédiaire d’une agence, il convient de prévoir une commission égale le plus souvent à un mois de loyer.</p>
<p>Pour un séjour de longue durée ou comme investissement, il est envisageable d’acheter, mais la procédure reste assez complexe et s’effectue devant un avocat en général.</p>
<p>Le montant des charges est relativement élevé même si depuis 2013, la politique du gouvernement a permis une diminution d’environ 20% de celles-ci, après plusieurs années d’augmentation régulière. La climatisation est conseillée, compte tenu des étés particulièrement torrides ; le chauffage est nécessaire d’octobre à avril, il est souvent collectif. Les charges comprennent généralement l’eau, le gaz et l’électricité.</p>
<h3 class="spip"><a id="sommaire_3"></a>Hôtels</h3>
<p><strong>Budapest</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Prix moyen d’une chambre d’hôtel (chambre double)</strong></td>
<td>forints</td>
<td>Euros</td></tr>
<tr class="row_even even">
<td>5 étoiles (à partir de)</td>
<td>60 000</td>
<td>200</td></tr>
<tr class="row_odd odd">
<td>3-4 étoiles</td>
<td>18 000 – 60 000</td>
<td>60 - 200</td></tr>
<tr class="row_even even">
<td>1-2 étoiles et pension</td>
<td>8000 – 18 000</td>
<td>26 - 60</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_4"></a>Auberges de jeunesse</h3>
<p>Il y a également des possibilités d’hébergement en auberge de jeunesse :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.hostelscentral.com/pays-29.html" class="spip_out" rel="external">Hostelscentral.com</a> ou chez l’habitant. </p>
<h3 class="spip"><a id="sommaire_5"></a>Electricité</h3>
<p>Le courant est de 220 volts (50Hz) et les prises sont aux normes européennes standard.</p>
<h3 class="spip"><a id="sommaire_6"></a>Electroménager</h3>
<p>Les cuisines sont le plus souvent équipées de plaques de cuisson et d’un four, ainsi que moins systématiquement d’un réfrigérateur et d’une machine à laver le linge, plus rarement d’un congélateur ou d’une machine à laver la vaisselle.</p>
<p>Le chauffage est le plus souvent au gaz ou parfois à l’électricité, ou bien encore collectif dans certains appartements de grands immeubles. La climatisation n’est pas indispensable mais elle reste appréciable en été lorsque la température dépasse trente degrés.</p>
<p><i>Mise à jour : juin 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-hongrie-vie-pratique-article-pour-en-savoir-plus.md" title="Pour en savoir plus">Pour en savoir plus</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-hongrie-vie-pratique-article-loisirs-et-culture.md" title="Loisirs et culture">Loisirs et culture</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-hongrie-vie-pratique-article-communications.md" title="Communications">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-hongrie-vie-pratique-article-transports.md" title="Transports">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-hongrie-vie-pratique-article-cout-de-la-vie-113481.md" title="Coût de la vie">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-hongrie-vie-pratique-article-scolarisation-113480.md" title="Scolarisation">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-hongrie-vie-pratique-article-sante.md" title="Santé">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-hongrie-vie-pratique-article-logement-113478.md" title="Logement">Logement</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/vie-pratique/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
