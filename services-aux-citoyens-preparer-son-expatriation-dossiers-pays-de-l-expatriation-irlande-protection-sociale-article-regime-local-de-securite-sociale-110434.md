# Régime local de sécurité sociale

<p>Le ministère des Affaires sociales et de la Famille (<i>Department of Social Protection - </i>Áras Mhic Dhiarmada, Store Street, Dublin 1) est responsable de la gestion et de l’administration du régime de protection sociale en Irlande (maladie-maternité, vieillesse, invalidité, survivants, chômage, accidents du travail et maladies professionnelles, les prestations familiales et les aides sociales).</p>
<p>Le service de la protection sociale (<i>Social Welfare Service</i>) administre toutes les assurances sociales et sert les prestations. La direction générale des services de santé (<i>Health service executive head office</i>) gère les services de santé régionaux.</p>
<p>Vous trouverez une <a href="http://www.cleiss.fr/docs/regimes/regime_irlande.html" class="spip_out" rel="external">présentation détaillée du système de sécurité sociale irlandais</a> sur le site de notre partenaire, le <a href="http://www.cleiss.fr/" class="spip_out" rel="external">CLEISS</a> (Centre des liaisons européennes et internationales de sécurité sociale). En voici la table des matières :</p>
<ul class="spip">
<li><a href="http://www.cleiss.fr/docs/regimes/regime_irlande-salaries.html" class="spip_out" rel="external">Le régime des salariés</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_irlande-nonsalaries.html" class="spip_out" rel="external">Le régime des non-salariés</a></li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/protection-sociale/article/regime-local-de-securite-sociale-110434). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
