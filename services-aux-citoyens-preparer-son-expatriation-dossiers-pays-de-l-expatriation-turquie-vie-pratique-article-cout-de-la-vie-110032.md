# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/vie-pratique/article/cout-de-la-vie-110032#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/vie-pratique/article/cout-de-la-vie-110032#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/vie-pratique/article/cout-de-la-vie-110032#sommaire_3">Alimentation</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/vie-pratique/article/cout-de-la-vie-110032#sommaire_4">Prix moyen d’un repas dans un restaurant </a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/vie-pratique/article/cout-de-la-vie-110032#sommaire_5">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>La livre turque est, depuis le 1er janvier 2009, l’unité monétaire de la Turquie.</p>
<p>La livre turque est divisée en 100 kurus.</p>
<p>En dépit de son appellation officielle de « Türk lirasi » en Turquie, ou « Turkish Lira » pour beaucoup de pays étrangers, les Français parlent de « livres turques ».</p>
<p>A noter que bien qu’abandonnée au 1er janvier 2005, de nombreux Turcs parlent encore en millions de livres (un peu comme les anciens francs français).</p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<p>La livre turque est librement convertible.</p>
<p>Il n’existe aucune disposition en matière de transfert de fonds : chacun est libre de transférer des fonds de Turquie en France à tout moment et réciproquement. Le transfert peut être effectué par la voie bancaire classique.</p>
<p>Les cartes bancaires internationales sont acceptées dans les grandes villes et les centres touristiques. Il est possible de procéder à des retraits de liquidités dans les distributeurs automatiques qui couvrent dorénavant l’ensemble du pays.</p>
<p>Les banques turques proposent des cartes bancaires et cartes de débit.</p>
<h4 class="spip">Banques </h4>
<p>Les principales banques turques sont : Is Bankasi - Garanti Bankasi - TEB (filiale de BNP en Turquie) - Yapi Kredi – Akbank – Finans Bank – Ziraatbank</p>
<p>Les principales banques étrangères sont : HSBC ING – Denizbank (ancienne filiale de Dexia, rachetée par le groupe russe Sberbank)</p>
<p>Les grandes villes et les villes touristiques disposent de nombreux bureaux de change qui ferment généralement plus tard que les banques. A noter que la plupart des bureaux de poste (PTT) possèdent également un comptoir de change.</p>
<h4 class="spip">Ouvrir un compte bancaire</h4>
<p>L’ouverture d’un compte bancaire en Turquie nécessite l’attribution d’un numéro de « citoyen » délivré par la Direction de la police lors des formalités de demande de carte de résident.</p>
<p>Il est possible d’ouvrir un compte en euros ou en dollars et de recevoir des virements dans cette monnaie.</p>
<h3 class="spip"><a id="sommaire_3"></a>Alimentation</h3>
<p>Il n’y a pas de pénuries et les marchés sont bien approvisionnés. Les prix peuvent, au cours de l’année, fluctuer. Ils dépendent, comme ailleurs, des saisons, des arrivages et des lieux d’achat.</p>
<h3 class="spip"><a id="sommaire_4"></a>Prix moyen d’un repas dans un restaurant </h3>
<p>Bien que le service soit inclus dans la note, il est d’usage de laisser environ 10% de pourboire.</p>
<h3 class="spip"><a id="sommaire_5"></a>Evolution des prix</h3>
<p>En 2013, l’inflation s’est maintenue entre 7,0 et 7,3% au cours du 1er trimestre avant de passer à 6,1% en avril. Elle a ensuite repris un mouvement à la hausse pour monter jusqu’à 8,9% en juillet. Fin septembre (dernière statistique disponible), elle s’établit à 7,88% en glissement annuel.</p>
<p>Le gouvernement turc prévoit un taux d’inflation de 6,8% pour la fin 2013 alors que le Fonds monétaire international se montre plus optimiste avec une prévision de 6,6%. Pour fin 2014, l’inflation pourrait s’établir à 5,3% (prévision du gouvernement et celle du FMI).</p>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/vie-pratique/article/cout-de-la-vie-110032). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
