# Emploi, stage

<h2 class="rub22826">Marché du travail</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/emploi-stage/#sommaire_1">Secteurs à fort potentiel</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/emploi-stage/#sommaire_2">Secteurs à faible potentiel</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/emploi-stage/#sommaire_3">Rémunération</a></li></ul>
<p>Outre une recherche en amont, en France, il est possible de passer une petite annonce dans le bulletin de la <a href="http://www.ccfb.com.br/fr/" class="spip_out" rel="external">Chambre de commerce France-Brésil</a>. Les candidatures spontanées sont très fréquemment utilisées au Brésil. Toutefois il convient de disposer d’un solide réseau de relations. Il convient de souligner le peu de possibilités d’embauche à Brasilia, ou très peu d’entreprises ou d’industries sont installées.</p>
<p>Les grandes entreprises internationales se situent généralement dans la partie sud du Brésil, de Rio de Janeiro à Porto Alegre, en passant par Curitiba. São Paulo joue le rôle de la capitale industrielle du pays, la plupart des entreprises françaises y sont en effet basées.</p>
<h3 class="spip"><a id="sommaire_1"></a>Secteurs à fort potentiel</h3>
<ul class="spip">
<li>technologies de l’information et de la communication</li>
<li>alimentation et restauration (nombre d’expatriés français installés à Brasilia travaillent dans ce domaine : restaurants, traiteurs, pâtisserie…)</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Secteurs à faible potentiel</h3>
<p><strong>Il n’y a pas véritablement de secteurs à déconseiller</strong>, mais si l’emploi recherché ne présente pas de spécificité technique particulière ou ne nécessite pas l’usage de la langue française, il faut pouvoir arguer d’un différentiel.</p>
<h3 class="spip"><a id="sommaire_3"></a>Rémunération</h3>
<p>Au 1er janvier 2010, le salaire minimum mensuel est de 510 R$ (220€).</p>
<p>Les cadres, dont le niveau de qualification est souvent élevé, sont généralement bien rémunérés. A fonction égale, <strong>il existe toutefois de grandes disparités régionales dans les niveaux de rémunération</strong>.</p>
<p>C’est à Sao Paulo que les salaires sont les plus élevés mais il en est de même pour le coût de la vie.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-bresil-emploi-stage-article-stages.md" title="Stages">Stages</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-bresil-emploi-stage-article-entretien-d-embauche-109987.md" title="Entretien d’embauche">Entretien d’embauche</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-bresil-emploi-stage-article-lettre-de-motivation-109986.md" title="Lettre de motivation">Lettre de motivation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-bresil-emploi-stage-article-curriculum-vitae-109985.md" title="Curriculum vitae">Curriculum vitae</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-bresil-emploi-stage-article-recherche-d-emploi.md" title="Recherche d’emploi">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-bresil-emploi-stage-article-reglementation-du-travail-109983.md" title="Réglementation du travail">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-bresil-emploi-stage-article-marche-du-travail-109982.md" title="Marché du travail">Marché du travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/emploi-stage/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
