# Modalités pratiques du vote par procuration

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/droit-de-vote-et-elections-a-l-etranger/article/modalites-pratiques-du-vote-par#sommaire_1">Procuration dressée à l’étranger</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/droit-de-vote-et-elections-a-l-etranger/article/modalites-pratiques-du-vote-par#sommaire_2">Procuration dressée en France</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Procuration dressée à l’étranger</h3>
<p><strong>Autorité devant laquelle une procuration peut être dressée</strong></p>
<ul class="spip">
<li>Ambassadeur pourvu d’une circonscription consulaire</li>
<li>Chef de poste consulaire</li>
<li>Consul honoraire de nationalité française habilité</li></ul>
<p><strong>Conditions à remplir pour le mandant</strong><br class="autobr">Justifier de son identité et attester sur l’honneur ne pouvoir se rendre au bureau de vote le jour du scrutin (sans justification)</p>
<p><strong>Durée de validité</strong></p>
<ul class="spip">
<li>soit un scrutin (un ou deux tours)</li>
<li>soit jusqu’à trois ans pour les Français établis hors de France à condition que la procuration soit établie par l’autorité consulaire du lieu de résidence</li></ul>
<p><strong>Formulaire de procuration</strong><br class="autobr">Vous pouvez désormais remplir le formulaire CERFA de demande de vote par procuration sur votre ordinateur, l’imprimer et l’apporter à votre consulat. <br class="autobr">Toutes les explications et le formulaire CERFA sont disponibles sous le lien suivant :<br class="autobr"><a href="https://www.formulaires.modernisation.gouv.fr/gf/cerfa_14952.do" class="spip_out" rel="external">Formulaire Cerfa n° 14952*01</a>  <br class="autobr"><a href="http://www.interieur.gouv.fr/Elections/Comment-voter/Le-vote-par-procuration/Explications-prealables-adressees-au-mandant" class="spip_out" rel="external">Explications préalables adressées au mandant</a></p>
<p>Pour les électeurs qui ne disposent pas d’un ordinateur connecté à internet et d’une imprimante, il est toujours possible d’obtenir le formulaire de procuration au guichet de votre consulat.</p>
<p><strong>La demande concerne une procuration pour voter à l’étranger</strong></p>
<ul class="spip">
<li><strong>Conditions à remplir pour le mandataire :</strong> être inscrit(e) sur la même liste électorale consulaire que le mandant</li>
<li><strong>Nombre de procurations pouvant être reçues par un mandataire :</strong> trois procurations au plus par mandataire, dont une seule établie en France</li></ul>
<p><strong>La demande concerne une procuration pour voter en France </strong></p>
<ul class="spip">
<li><strong>Conditions à remplir pour le mandataire :</strong> être inscrit(e) sur la liste électorale de la même commune que le mandant</li>
<li><strong>Nombre de procurations pouvant être reçues par un mandataire :</strong> deux procurations au plus par mandataire dont une seule établie en France</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Procuration dressée en France</h3>
<p><strong>Autorité devant laquelle une procuration peut être dressée</strong></p>
<ul class="spip">
<li>Tribunal d’instance</li>
<li>Commissariat de police, gendarmerie</li></ul>
<p><strong>Conditions à remplir pour le mandant</strong><br class="autobr">Justifier de son identité et attester sur l’honneur ne pouvoir se rendre au bureau de vote le jour du scrutin pour l’une des raisons suivantes :</p>
<ul class="spip">
<li>obligations professionnelles</li>
<li>handicap</li>
<li>raisons de santé</li>
<li>assistance à une personne malade</li>
<li>obligations de formation</li>
<li>vacances</li>
<li>résidence dans une commune différente de celle de l’inscription sur la liste électorale</li></ul>
<p><strong>Durée de validité</strong></p>
<ul class="spip">
<li>soit un scrutin (un ou deux tours)</li>
<li>soit un an</li></ul>
<p><strong>La demande concerne une procuration pour voter à l’étranger</strong></p>
<ul class="spip">
<li><strong>Conditions à remplir pour le mandataire : </strong> être inscrit(e) sur la même liste électorale consulaire que le mandant</li>
<li><strong>Nombre de procurations pouvant être reçues par un mandataire :</strong> trois procurations au plus par mandataire, dont une seule établie en France</li></ul>
<p><strong>La demande concerne une procuration pour voter en France </strong></p>
<ul class="spip">
<li><strong>Conditions à remplir pour le mandataire : </strong>être inscrit(e) sur la liste électorale de la même commune que le mandant</li>
<li><strong>Nombre de procurations pouvant être reçues par un mandataire :</strong> deux procurations au plus par mandataire dont une seule établie en France</li></ul>
<p><strong>Remarque :</strong></p>
<p>Vous pouvez, à tout moment, résilier la procuration que vous avez donnée devant l’autorité qui l’a dressée.<br class="autobr">Même si vous avez donné procuration, vous pouvez voter en personne à condition de vous présenter au bureau de vote avant votre mandataire.<br class="autobr">Lorsqu’un mandataire reçoit un nombre de procurations supérieur à celui qui est autorisé, les deux procurations les plus anciennes sont seules prises en considération.</p>
<p><i>Mise à jour : janvier 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/droit-de-vote-et-elections-a-l-etranger/article/modalites-pratiques-du-vote-par). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
