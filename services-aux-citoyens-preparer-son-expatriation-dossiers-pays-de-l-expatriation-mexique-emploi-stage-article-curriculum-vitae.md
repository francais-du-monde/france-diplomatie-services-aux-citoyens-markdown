# Curriculum vitae

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/emploi-stage/article/curriculum-vitae#sommaire_1">Rédaction</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/emploi-stage/article/curriculum-vitae#sommaire_2">CV - Rubriques à respecter</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/emploi-stage/article/curriculum-vitae#sommaire_3">Diplômes</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/emploi-stage/article/curriculum-vitae#sommaire_4">Modèles de CV</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Rédaction</h3>
<p>Le curriculum vitae adressé aux entreprises, même françaises ou internationales, doit <strong>toujours être rédigé en espagnol</strong>, sauf demande spéciale de l’entreprise.</p>
<p>Contrairement au CV « français » qui doit tenir sur une page, le CV mexicain est un peu plus libre, sans toutefois exagérer. Nous conseillons de respecter un format de deux pages maximum.</p>
<p>Pensez à envoyer votre CV en format PDF sous le nom « CV Prénom NOM ».</p>
<p>Ne négligez pas non plus l’importance des CV en ligne et notamment le site <a href="http://www.linkedin.com" class="spip_out" rel="external">Linkedin</a>. Cet outil est utilisé par une <strong>grande majorité des recruteurs mexicains</strong> qui consulteront votre CV en ligne avant de vous contacter.</p>
<p>Enfin, le responsable emploi et formation de la Chambre franco-mexicaine de commerce et d’industrie (CFMCI) peut vous recevoir sur rendez-vous afin de vous conseiller dans l’élaboration de votre CV et de son adaptation pour cibler le marché mexicain. Ce service est <strong>entièrement gratuit</strong> pour les ressortissants français.</p>
<p>Pour plus de renseignements ou pour convenir d’un rendez-vous avec le responsable emploi et formation : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/emploi-stage/article/curriculum-vitae#emploi2#mc#cfmci.com#" title="emploi2..åt..cfmci.com" onclick="location.href=mc_lancerlien('emploi2','cfmci.com'); return false;" class="spip_mail">emploi2<span class="spancrypt"> [at] </span>cfmci.com</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>CV - Rubriques à respecter</h3>
<p><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> TITULO : il est de coutume de mettre un titre à son CV. Ex : "Ingeniero químico con 5 años de experiencia en industria farmacéutica", <br class="autobr">"Desarrollo de negocios" etc.<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> DATOS PERSONALES <i>(exemple de données à mentionner)</i></p>
<ul class="spip">
<li>Apellido</li>
<li>Nombre</li>
<li>Correo electrónico</li>
<li>Celular</li>
<li>Tel. casa</li>
<li>Dirección</li>
<li>Skype</li>
<li>Nacionalidad (si vous avez un visa de travail mexicain, n’oubliez pas de mentionner : <i>Visa de trabajo vigente</i>)</li>
<li>Fecha de nacimiento</li>
<li>Licencia de conducir
<i>La photo n’est pas obligatoire.</i></li></ul>
<p><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> OBJETIVO PROFESIONAL. Il est très courant au Mexique d’écrire 2 ou 3 lignes afin de décrire brièvement votre objectif professionnel, objectif de carrière. <br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> FORMACIÓN. Mentionnez vos formations académiques en commençant par la plus récente. De manière générale, remontez jusqu’à l’obtention du baccalauréat français.<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> EXPERIENCA LABORAL. Mentionnez vos expériences professionnelles en commençant par la plus récente. N’oubliez pas de bien préciser la période de travail pour chaque poste, vos <strong>logros</strong> (réussites), faites ressortir des chiffres significatifs et des éléments concrets qui parleront aux recruteurs.<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> OTROS CURSOS. A ne mentionner que si vous avez suivi des cours en parallèle de votre formation professionnelle par exemple (ex : 40H de formation en ventes, etc..)<br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> HABILIDADES : IDIOMAS – INFORMÁTICA</p>
<ul class="spip">
<li>Idiomas : indiquez les langues que vous maitrisez en essayant d’être le plus précis et concrets possibles. Par exemple : <ul class="spip">
<li>Español : nivel avanzado (3 años en México)</li>
<li>Inglés : nivel avanzado (TOEIC : 950/990)</li>
<li>Francés : lengua materna</li></ul></li>
<li>Informatica : précisez vos compétences informatiques. Par exemple : <p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span> Dominio paquete Office, etc. C’est également le moment pour les ingénieurs, techniciens, informaticiens, de préciser toutes vos compétences et les logiciels spécifiques que vous maitrisez (ex : CATIA, programmation C+, Matlab, etc..)</p></li></ul>
<p><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> OTROS INTERESES. Ne pas trop s’attarder sur cette section, cependant elle permet aux recruteurs de vous cerner un peu mieux, et les intérêts que vous partagerez sur votre CV pourront faire l’objet de questions lors d’un entretien. <br><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L8xH11/puce-32883.gif" width="8" height="11" class="puce" alt="-"> REFERENCIAS. Ne pas oublier une rubrique <strong>referencias</strong> (références) qui donne les noms de contacts et d’entreprises qui seront en mesure de recommander le candidat.</p>
<h3 class="spip"><a id="sommaire_3"></a>Diplômes</h3>
<p>Vos diplômes <strong>apostillés</strong> peuvent vous être demandés au Mexique, notamment lors des démarches d’obtention d’un visa de travail. Pensez donc avant votre départ à les faire apostiller en France. <br class="manualbr">Pour plus de renseignements : <a href="http://www.justice.gouv.fr/europe-et-international-10045/lapostille-comment-faire-23077.html" class="spip_out" rel="external">http://www.justice.gouv.fr/europe-et-international-10045/lapostille-comment-faire-23077.html</a></p>
<h3 class="spip"><a id="sommaire_4"></a>Modèles de CV</h3>
<p>Plusieurs sites conseillent sur l’élaboration du CV. Nous vous recommandons :</p>
<ul class="spip">
<li><a href="http://www.cnnexpansion.com/mi-carrera/2009/03/23/haz-un-cv-de-alto-impacto" class="spip_out" rel="external">http://www.cnnexpansion.com/mi-carrera/2009/03/23/haz-un-cv-de-alto-impacto</a></li>
<li><a href="http://blog.occeducacion.com/blog/bid/244043/Aprende-a-Hacer-tu-Curr%C3%ADculum-Paso-a-Paso" class="spip_out" rel="external">http://blog.occeducacion.com/blog/bid/244043/Aprende-a-Hacer-tu-Curr%C3%ADculum-Paso-a-Paso</a></li>
<li><a href="http://www.aliato.mx/" class="spip_out" rel="external">http://www.aliato.mx/</a></li>
<li><a href="http://www.dgoserver.unam.mx/portaldgose/bolsa-trabajo/htmls/bolsa-tips-curriculum.html" class="spip_out" rel="external">http://www.dgoserver.unam.mx/portaldgose/bolsa-trabajo/htmls/bolsa-tips-curriculum.html</a></li>
<li><a href="http://www.kellyservices.com.mx/MX/Careers/Career-Forward/Como-elaborar-un-Curriculum-Vitae/" class="spip_out" rel="external">http://www.kellyservices.com.mx/MX/Careers/Career-Forward/Como-elaborar-un-Curriculum-Vitae/</a></li>
<li><a href="http://www.laeconomia.com.mx/como-hacer-un-curriculum-vitae/" class="spip_out" rel="external">http://www.laeconomia.com.mx/como-hacer-un-curriculum-vitae/</a></li>
<li><a href="http://www.modelocurriculum.net/" class="spip_out" rel="external">http://www.modelocurriculum.net/</a></li></ul>
<p><i>Mise à jour : septembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/emploi-stage/article/curriculum-vitae). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
