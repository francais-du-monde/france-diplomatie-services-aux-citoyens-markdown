# Protection sociale

<h2 class="rub23036">Régime local de sécurité sociale</h2>
<p>Le système dominicain de Sécurité sociale a été mis en place le 1er juin 2003.</p>
<p>Il rend obligatoire pour les affiliés les retenues à la source pour l’assurance maladie, les accidents du travail, la formation professionnelle, la retraite et l’impôt sur le revenu.</p>
<p>Toutes les cotisations sont versées à un seul organisme : la <a href="http://www.tss.gov.do/" class="spip_out" rel="external">Trésorerie dominicaine de sécurité sociale (TSS)</a> chargée de les répartir aux différentes administrations devant assurer la protection des travailleurs et percevoir l’impôt.</p>
<p><strong>Les taux de cotisation sont les suivants :</strong></p>
<ul class="spip">
<li>Assurance maladie (ARS) : 3,04% part salariale et 7,09% part patronale. Il est important de signaler que les taux de remboursement des frais de santé du système public est notoirement très insuffisant et que la plupart des salariés doivent souscrire à une assurance privée complémentaire pour parvenir à une protection acceptable.</li>
<li>Accidents du travail (SRL) : cette cotisation (1,2%) ne concerne que la part patronale.</li>
<li>Formation professionnelle (INFOTEP) : comme pour les accidents du travail, cette cotisation de 1% ne concerne que la part patronale.</li>
<li><a href="http://www.sipen.gov.do/" class="spip_out" rel="external">Retraite (AFP)</a> : les taux applicables pour les cotisations au fonds de pension (AFP) sont de 2,87% pour la part salariale et 7,10% pour la part patronale.<br class="manualbr">Le droit à la retraite ne s’ouvre que lorsque le salarié a atteint 60 ans <strong>et</strong> qu’il a cotisé durant 30 ans.</li>
<li>Impôts sur le revenu (ISR) : le calcul de la retenue à la source au titre de l’impôt sur le revenu s’effectue sur la base du salaire mensuel brut diminué des retenues concernant la maladie (ARS) et la retraite (AFP) suivant un barème actualisé périodiquement (cf. site officiel de la <a href="http://www.dgii.gov.do/Paginas/Index.aspx" class="spip_out" rel="external">Direction générale des impôts</a>).</li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-dominicaine-protection-sociale-23036-article-convention-de-securite-sociale-111319.md" title="Convention de sécurité sociale">Convention de sécurité sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-dominicaine-protection-sociale-23036-article-regime-local-de-securite-sociale-111318.md" title="Régime local de sécurité sociale">Régime local de sécurité sociale</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/protection-sociale-23036/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
