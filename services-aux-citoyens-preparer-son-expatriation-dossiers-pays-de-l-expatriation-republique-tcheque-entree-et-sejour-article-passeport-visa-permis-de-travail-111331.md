# Passeport, visa, permis de travail

<p>Quel que soit le motif de votre séjour, renseignez-vous auprès de la section consulaire de l’ambassade de République tchèque, à Paris qui vous informera sur la règlementation en matière d’entrée et de séjour en République tchèque, règlementation que vous devrez impérativement respecter. N’hésitez pas à consulter le site ministère tchèque de l’Intérieur afin de vous familiariser avec les modalités d’entrée et de séjour dans le pays.</p>
<p><strong>En aucun cas, vous n’êtes autorisé à travailler en République tchèque sans permis adéquat. </strong></p>
<p>L’Ambassade de France en République tchèque n’est pas l’organisme compétent pour répondre à vos demandes d’information concernant votre séjour en République tchèque.</p>
<p>Les ressortissants français peuvent entrer et circuler en République tchèque sur présentation d’une carte nationale d’identité ou d’un passeport en cours de validité.</p>
<h4 class="spip">Procédures d’inscription et titres de séjour</h4>
<p>Si vous êtes citoyen de certains pays membres de l’UE/EEE ou de la Suisse et que vous arrivez en République tchèque, vous devrez effectuer en arrivant dans le pays les procédures suivantes :</p>
<h5 class="spip">Enregistrement du séjour</h5>
<p>Tout étranger désirant demeurer plus de 30 jours sur le territoire tchèque <strong>doit s’enregistrer auprès de la Police des étrangers tchèque</strong>.</p>
<p>Si vous souhaitez rester <strong>plus de 30 jours en République tchèque</strong>, vous êtes tenu de vous présenter à la police des étrangers territorialement compétente dans un délai de 30 jours, muni d’une photo d’identité, d’un passeport ou d’une carte d’identité et d‘un justificatif de domicile sur le territoire.</p>
<p>Le service de la police des étrangers (<i>cizinecká policie</i>) à Prague est situé à Konevova 32 à Prague 3, ouvert lundi et mercredi 7h30-18h, mardi 7h30-14h, jeudi 7h30-15h, fermé le vendredi. Il est joignable au 00420 974 820 207 ou 409.</p>
<p>Pour les adresses et coordonnées téléphoniques des autres villes et davantage de précisions, vous pouvez consulter <a href="http://www.mvcr.cz/" class="spip_out" rel="external">le site internet du ministère de l’Intérieur tchèque</a> (en tchèque).</p>
<h5 class="spip">Autorisation de séjour</h5>
<p>Si vous souhaitez rester <strong>plus de trois mois en République tchèque</strong>, vous pouvez demander, à la place du simple enregistrement, <strong>une attestation de séjour temporaire</strong>, qui est délivrée gratuitement, à durée indéterminée et dans les 60 jours à compter du dépôt du dossier. Cette attestation n‘est pas obligatoire mais est demandée par diverses autorités tchèques comme par exemple les banques, lors de l’examen du permis de conduire ou encore de l‘achat d‘un bien immobilier.</p>
<p>Les pièces à fournir sont les suivantes :</p>
<ul class="spip">
<li>2 photos d‘identité ;</li>
<li>passeport ou carte d‘identité ;</li>
<li>justificatif de domicile sur le territoire (contrat de location, contrat d’achat) ;</li>
<li>justificatif du motif de la résidence (contrat de travail, certificat de scolarité, convention de stage…) ;</li>
<li>justificatif d’assurance sur le territoire (sauf en cas d’emploi, d’activités professionnelles ou lucratives).</li></ul>
<p>Tout changement concernant les renseignements fournis doit faire l’objet d’une déclaration auprès de la police des étrangers. Les coordonnées et adresses sont identiques à celles indiquées pour le dépôt du simple enregistrement.</p>
<h5 class="spip">Permis de séjour permanent</h5>
<p>Les personnes justifiant d‘un lien familial avec un citoyen tchèque ou celles pouvant se prévaloir d’un séjour temporaire ininterrompu d’au moins cinq ans sur le territoire, peuvent demander <strong>un permis de séjour permanent</strong>. Outre ce justificatif, les autres pièces à fournir restent identiques à celles requises pour le permis de séjour temporaire.</p>
<p>Tous les justificatifs doivent être traduits en tchèque par un traducteur assermenté et datés de moins de six mois. Tout changement concernant les renseignements fournis doit faire l’objet d’une déclaration auprès de la police des étrangers. Le permis de séjour permanent sera délivré dans un délai de 60 jours à compter du dépôt du dossier, gratuitement et pour une durée de 10 ans.</p>
<p>Il est possible de déposer une demande de permis de séjour permanent auprès des services compétents (<i>Odbor azylové a migracni politiky</i>), indifféremment à Nad Stolou à Prague 7 (Letna) ouvert lundi et mercredi de 8h00 à14h30, mardi et jeudi de 8h00 à15h30 ou à Nad Vrsovskou Horou - 88/4 à Prague 10 (Bohdalec), ouvert lundi et mercredi de 8h00 à16h30, mardi et jeudi de 8h00 à 14h30. Les services sont fermés le vendredi et sont joignables au 00420 974 847 715 ou sur <a href="http://www.mvcr.cz/" class="spip_out" rel="external">le site du ministère</a>.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.mzv.cz/paris/fr/service_consulaire/index.html" class="spip_out" rel="external">Section consulaire de l’ambassade de République tchèque à Paris</a></li>
<li><a href="http://www.mvcr.cz/mvcren/" class="spip_out" rel="external">Ministère tchèque de l’Intérieur</a> (en anglais)</li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/entree-et-sejour/article/passeport-visa-permis-de-travail-111331). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
