# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/emploi-stage/article/reglementation-du-travail-111068#sommaire_1">Législation du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/emploi-stage/article/reglementation-du-travail-111068#sommaire_2">Droit du travail</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/emploi-stage/article/reglementation-du-travail-111068#sommaire_3">Fêtes légales</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Législation du travail</h3>
<p><strong>La pièce maîtresse du dispositif sur la législation du travail est la loi sur les organisations syndicales de 1965</strong>, qui reprend dans ses grandes lignes les directives et orientations de l’organisation internationale du Travail (O.I.T.). Le taux de syndicalisation est très élevé : plus de 80% des salariés sont adhérents à un syndicat, rattaché le plus souvent à l’une des deux confédérations suivantes qui regroupent les salariés du secteur privé :</p>
<ul class="spip">
<li>Pancyprian Federation of Labour (orienté à gauche) ;</li>
<li>Cyprus Workers Confederation (centre-droite).</li></ul>
<p>Les fonctionnaires, les enseignants et les employés de banque ont leurs propres organisations.</p>
<p>Du côté employeur, il existe 36 associations patronales qui sont pour la plupart membres de la <a href="http://www.oeb.org.cy/" class="spip_out" rel="external">Cyprus Employers and Industrialists Federation</a>.</p>
<p><strong>A la différence de l’abondante législation française, les textes législatifs et réglementaires chypriotes apparaissent très lacunaires.</strong> Ce système juridique fondé sur le modèle anglo-saxon laisse une large place aux partenaires sociaux et aux cocontractants dans l’élaboration des règles applicables. Le droit du travail et de la sécurité sociale est conçu comme un minimum auquel doivent s’ajouter les initiatives privées.</p>
<p>Ce sont donc surtout les conventions collectives librement négociées entre les partenaires, les règlements intérieurs et toutes les pratiques inspirées d’usages répandus qui régissent le monde du travail. Ainsi, en dehors de la fonction publique où les agents bénéficient depuis longtemps d’un statut privilégié, les entreprises chypriotes ont-elles été amenées sous la pression des mouvements syndicaux à remédier aux insuffisances de la législation, en accordant à leurs employés des avantages sociaux complémentaires.</p>
<p>Dans les entreprises d’une certaine importance, des "fonds de prévoyance" ont été créés, abondés grâce à des cotisations fixées à partir d’un pourcentage des salaires et acquittées généralement par les employeurs à la hauteur des deux tiers et d’un tiers par les employés. Ces fonds s’articulent autour d’un large éventail de prestations. Dans des structures plus petites, des avantages divers sont accordés directement par les employeurs à leurs salariés : compléments de rémunération en cas de congé maladie de longue durée, indemnités de fin de fonction en cas de rupture des liens contractuels (retraite, licenciement pour raisons économiques…).</p>
<p><strong>De manière générale, le climat social est assez serein et les cas de conflits sociaux sont relativement rares</strong>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Droit du travail</h3>
<h4 class="spip">Généralités</h4>
<p>Le droit de grève est garanti par la Constitution. Les différends individuels ou collectifs doivent être dans toute la mesure du possible réglés à l’amiable. En cas d’échec, les parties concernées ont la possibilité d’avoir recours soit à la médiation, soit à l’arbitrage obligatoire.</p>
<h4 class="spip">Durée hebdomadaire du travail </h4>
<p>Les horaires de travail varient selon les secteurs et les professions. La plupart des bureaux et entreprises travaillent entre 8 heures et 17h30, avec une pause entre 13 heures et 14h30. Souvent, une après-midi est libre (généralement le mercredi) et dans ce cas, la journée de travail se termine à 13 heures.</p>
<p>Une loi votée fin 2013, permet l’ouverture des magasins le dimanche ainsi que le mercredi après-midi. Les horaires d’ouverture dépendent des régions, mais de manière générale les magasins sont ouverts de 9h00 à 19h30 sans interruption.</p>
<p>Les conventions collectives déterminent les heures de travail (nombre d’heures maximal). En général, les heures de travail par semaine varient entre 38 et 40 pour la majorité des entreprises. Des indemnités sont versées pour les heures de travail supplémentaires.</p>
<p><strong>Quelques détails à savoir : </strong></p>
<ul class="spip">
<li>Tout travailleur doit disposer d’une période de repos continu minimale de 11 heures, par période de 24 heures, et d’un minimum de 35 heures de repos continu par semaine.</li>
<li>La durée maximale de travail par semaine est fixée à 48 heures (44 pour la fonction publique), y compris les heures supplémentaires.</li>
<li>Le travail nocturne est celui pratiqué entre 23 heures et 6 heures. La durée de travail nocturne ne doit pas excéder huit heures par période de 24 heures.</li>
<li>Si la durée journalière de travail dépasse les six heures, le travailleur a droit à une pause de 15 minutes.</li>
<li>Les dérogations éventuelles doivent être convenues et apparaître dans les conventions collectives.</li>
<li>Une loi spéciale régit la durée hebdomadaire de travail, les heures supplémentaires et le repos hebdomadaire des employés de magasins, des employés de bureaux, des conducteurs de véhicules et des employés d’hôtels et de lieux de divertissement.Pour des clarifications et des renseignements supplémentaires, veuillez vous adresser au département des relations de travail (<i>Τμήμα Εργασιακών Σχέσεων</i>) au +357 22 803 100.</li></ul>
<h4 class="spip">Congés payés </h4>
<p>Selon la législation, les travailleurs ont droit à 20 ou 24 jours ouvrables de congés, selon qu’ils travaillent cinq ou six jours par semaine, ceci pour une période d’un an de travail. Il est possible que des jours de congés payés supplémentaires vous soient octroyés par votre contrat de travail ou par conventions collectives. Les employés soumettent une demande de congé de repos, et ce congé est accordé en fonction des possibilités de l’entreprise.</p>
<p>Les jours fériés officiels, pendant lesquels bureaux et organismes sont fermés, varient en général de 14 à 17 par an.</p>
<p>Pour plus de renseignements, vous pouvez vous adresser au département des relations de travail (<i>Τμήμα Εργασιακών Σχέσεων</i>) : +357 22 451 500. Le département des relations de travail (numéro de téléphone +357 22 803100) répondra à vos questions sur les congés de maladie, les congés parentaux, congés pour cause de force majeure, etc.</p>
<p>Les congés de maladie sont définis suite à un accord entre l’employeur et le travailleur au moyen de conventions collectives ou de contrats personnels. Le travailleur ne percevant pas d’allocation maladie de la part de son employeur a droit à une allocation de la part de la caisse d’assurances sociales sous certaines conditions.</p>
<p>Le congé de maternité a une durée de 18 semaines, dont 9 sont obligatoirement prises à partir de la 2ème semaine avant la date présumée de l’accouchement. La mère adoptive a également droit à un congé d’une durée de 14 semaines à partir du moment où elle prend en charge un enfant.</p>
<p>Le congé parental est accordé aussi bien à la mère qu’au père. Un parent travaillant depuis au moins six mois chez un employeur a droit à un congé parental d’une durée maximale de 13 semaines en raison de la naissance ou de l’adoption d’un enfant, afin de s’en occuper et de l’élever. La loi prévoit également le congé pour des raisons de force majeure (par exemple, maladie d’une personne à charge), jusqu’aux six ans révolus de l’enfant.</p>
<p>Enfin, si vous étudiez en vue d’obtenir un diplôme, sachez qu’il existe des entreprises qui accordent des congés de formation. La décision pertinente est prise par l’employeur. En général, un tel congé vous sera accordé afin d’acquérir des compétences professionnelles (par exemple, expert-comptable), ainsi que des compétences qui amélioreront vos capacités de travail. Il convient de vous mettre d’accord sur ce point avec votre employeur, si vous avez l’intention de continuer vos études, avant de prendre toute décision en la matière.</p>
<h4 class="spip">Rupture des contrats de travail </h4>
<p>Le contrat de travail peut être résilié soit par l’employeur soit par le travailleur. Les 26 premières semaines constituent une période d’essai pendant laquelle chacune des deux parties peut résilier le contrat de travail sans préavis et sans indemnités.</p>
<p>Au-delà de la période d’essai, si vous êtes licencié par votre employeur vous avez droit à une indemnité qui est fonction de la durée totale de travail chez cet employeur. Le travailleur licencié en tant que personnel excédentaire avant d’avoir atteint l’âge de la retraite et qui a travaillé pendant 104 semaines chez le même employeur a droit à une indemnité spéciale versée par la caisse du personnel excédentaire (<i>Ταμείο για Πλεονάζον Προσωπικό</i>). Il convient également de noter que le travail d’un employé saisonnier travaillant chez le même employeur au moins 15 semaines par an est considéré comme travail continu.</p>
<p>En ce qui concerne les licenciements collectifs :</p>
<ul class="spip">
<li>Est considéré comme licenciement collectif celui concernant 10 personnes pour une entreprise employant entre 21 et 99 travailleurs, 10% des travailleurs pour une entreprise employant entre 100 et 299 travailleurs et 30 personnes pour une entreprise employant plus de 300 travailleurs.</li>
<li>L’employeur est tenu de consulter les représentants des travailleurs à temps et de les informer de sa décision, du montant de l’indemnité, etc.</li>
<li>L’employeur est tenu d’en informer le ministère du travail et des assurances sociales (<i>Υπουργείο Εργασίας και Κοινωνικών Ασφαλίσεων</i>).</li></ul>
<p><strong>Maintien de la couverture par le régime français de Sécurité Sociale</strong></p>
<p>Les ressortissants européens détachés par leur entreprise peuvent rester affiliés au régime de leur pays d’origine pendant la durée de leur mission, dans une limite de 12 mois renouvelable (jusqu’à 36 mois) en demandant une exception d’enregistrement à la sécurité sociale locale.</p>
<p><strong>Accès des ressortissants communautaires au marché du travail local</strong></p>
<p>Les ressortissants européens ont libre accès au marché du travail chypriote sous contrat de droit local. Ils doivent s’inscrire à la sécurité sociale locale et se faire enregistrer au bureau de migration du ministère de l’intérieur.</p>
<p>Les contributions sociales s’élèvent à 6,8% de part salariale et 10,5% de part patronale, dont :</p>
<ul class="spip">
<li>6,8% au titre de la sécurité sociale,</li>
<li>1,2% pour l’assurance chômage</li>
<li>0,5% pour le développement de la formation.</li></ul>
<p>L’employeur doit également acquitter une taxe salariale de 2%, dont la recette est destinée au Fonds de cohésion sociale.</p>
<p>Les contributions à la sécurité sociale, au fonds d’assurance chômage et au développement de la formation sont calculés sur la base d’un plafond de 4333 € par mois. Le fonds de cohésion sociale est calculé sur le salaire réel, même s’il est supérieur à 4333 €.</p>
<p>La couverture sociale chypriote étant insuffisante en matière de protection médicale, les travailleurs doivent recourir à l’assurance privée (ou la Caisse des français de l’étranger).</p>
<p><i>Source : Ubifrance Athènes</i></p>
<h3 class="spip"><a id="sommaire_3"></a>Fêtes légales</h3>
<ul class="spip">
<li><strong>1er janvier</strong> : Jour de l’An</li>
<li><strong>6 janvier</strong> : Epiphanie</li>
<li><strong>Lundi vert</strong> : 1er lundi de carême <i>(50 jours avant les Pâques orthodoxes)</i></li>
<li><strong>Mars-Mai</strong> : dimanche et lundi de Pâques orthodoxes <i>(fête mobile)</i></li>
<li><strong>25 mars</strong> : Fête nationale de la Grèce</li>
<li><strong>Avril</strong> : Pâques <i>(fête mobile)</i></li>
<li><strong>1er mai</strong> : Fête du Travail</li>
<li><strong>Juin</strong> : Kataklysmos – célébration du Déluge biblique <i>(fête mobile)</i></li>
<li><strong>15 août</strong> : Ascension</li>
<li><strong>1er octobre</strong> : Fête nationale de Chypre <i>(anniversaire de l’indépendance)</i></li>
<li><strong>28 octobre</strong> : Jour du Ochi <i>(seconde fête nationale grecque)</i></li>
<li><strong>25-26 décembre</strong> : Noël et lendemain de Noël</li></ul>
<p>Pour la fonction publique, il convient de rajouter le Vendredi Saint et le 24 décembre.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/emploi-stage/article/reglementation-du-travail-111068). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
