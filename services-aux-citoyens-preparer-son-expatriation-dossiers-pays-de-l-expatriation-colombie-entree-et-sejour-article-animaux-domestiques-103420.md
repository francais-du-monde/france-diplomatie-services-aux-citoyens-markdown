# Animaux domestiques

<p>Pour prendre connaissance des principes généraux concernant l’exportation des animaux domestiques, référez-vous à notre rubrique <a href="services-aux-citoyens-preparer-son-expatriation-douanes.md" class="spip_in">Douanes</a>.</p>
<p>Pour pouvoir faire entrer un animal domestique sur le sol colombien, il faut un certificat vétérinaire de bonne santé datant de moins de 48 heures avant l’embarquement et un carnet de vaccinations à jour. A l’arrivée en Colombie vous devez vous présenter au bureau ICA (<strong>Instituto Colombiano Agropecuario</strong>) de l’aéroport avec les documents mentionnés.</p>
<p>Pour plus d’informations, visitez le <a href="http://www.ica.gov.co/getdoc/67809a6d-d08e-4d91-bd0e-f17611927a7e/Requisitos-para-importar-mascotas.aspx" class="spip_out" rel="external">site de l’ICA</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/entree-et-sejour/article/animaux-domestiques-103420). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
