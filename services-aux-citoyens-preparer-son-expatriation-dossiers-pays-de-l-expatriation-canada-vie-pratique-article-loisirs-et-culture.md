# Loisirs et culture

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/vie-pratique/article/loisirs-et-culture#sommaire_1">Tourisme</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/vie-pratique/article/loisirs-et-culture#sommaire_2">Activités culturelles</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/vie-pratique/article/loisirs-et-culture#sommaire_3">Sports</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/vie-pratique/article/loisirs-et-culture#sommaire_4">Télévision - Radio</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Tourisme</h3>
<p><strong>Ottawa</strong></p>
<p>La capitale fédérale compte de nombreux musées et monuments historiques. Les sites touristiques les plus fréquentés sont le Parc naturel de la Gatineau, la région des Grands lacs, la chaîne des Laurentides et la vallée du Saint Laurent.</p>
<p><strong>Edmonton</strong></p>
<p>Il existe de nombreux parcs nationaux ou provinciaux, notamment dans les Montagnes Rocheuses, permettant de pratiquer le ski et la randonnée. Jasper et Banff, situées à environ 350 km d’Edmonton, sont les principales stations de montagne. Les nombreux lacs des environs immédiats d’Edmonton (30 à 50 km) sont très fréquentés par les citadins. Des manifestations locales se déroulent chaque année. Ainsi, Calgary organise au mois de juin les journées du Stampede (courses de chariots et rodéos) pour les nostalgiques de la conquête de l’ouest. En juillet, à Edmonton, les journées du Klondike, sorte de carnaval, évoquent la ruée vers l’or à la fin du 19ème siècle.</p>
<p><strong>Halifax</strong></p>
<p>Halifax est la première ville fondée et colonisée par les Anglais. Grâce à son port et à ses infrastructures bancaires, Halifax est la métropole incontestée des provinces de l’Atlantique.</p>
<p><strong>Moncton</strong></p>
<p>L’une des curiosités de la ville est la " Colline magnétique " située au nord-ouest de la ville. Il s’agit d’une illusion d’optique très curieuse : moteur au point mort et frein desserré, les voitures semblent remonter la pente comme sous l’effet d’une attraction magnétique. 250 000 mille voitures font l’expérience chaque année.</p>
<p>A ne pas manquer :</p>
<ul class="spip">
<li>Shediac, située sur la côte est, est un lieu de villégiature apprécié pour ses plages ;</li>
<li>les parcs nationaux de Fundy et Kouchibouguac proches de la ville et ouverts à la belle saison ;</li>
<li>le Grand Pré, lieu de départ de la déportation des Acadiens ;</li>
<li>la reconstitution de la forteresse de Louisbourg.</li></ul>
<p><strong>Province du Québec</strong></p>
<p>L’ensemble de la province est très visité. Outre les villes de Montréal et Québec (la plus ancienne ville d’Amérique du Nord et la seule possédant des fortifications), certaines régions sont particulièrement appréciées des touristes : la Gaspésie, le Saguenay et le Charlevoix, mais aussi l’Outaouais, proche d’Ottawa.</p>
<p>L’été, la région des Cantons de l’Est (Sherbrooke) et ses petites localités typiques sont particulièrement appréciées des habitants du Québec. Ces derniers savent également profiter du million de lacs qui constituent la Belle province. À l’automne, les touristes vont admirer les changements de couleurs des forêts d’érable.</p>
<p>L’hiver, le choix de beaucoup de touristes se porte sur les Laurentides et ses stations de ski alpin, comme Mont Tremblant.</p>
<p>L’accueil touristique est bien structuré et le Québec compte de nombreux offices du tourisme dans toute la province. Les deux Centres Infotouristes de Montréal et de Québec vous fourniront des informations sur toutes les destinations québécoises et vous pourrez y effectuer des réservations d’hébergement.</p>
<p><strong>Toronto</strong></p>
<p>De nombreux sites méritent une visite, notamment les célèbres chutes du Niagara, les lacs, les parcs provinciaux (parc Algonquin) et la baie géorgienne. A voir également les forêts au nord de la province. La ville elle-même possède un assez grand nombre de musées et de galeries d’art de qualité.</p>
<p>Les principaux sites historiques sont : le fort York, le village historique " Black Creek " et Casa Loma. La ville dispose également d’équipements de prestige (tour CN et stade du "Skydome").</p>
<p><strong>Vancouver</strong></p>
<p>Le tourisme occupe une place importante dans l’économie de la province. Elle est dotée d’un excellent réseau routier permettant l’accès aux nombreux parcs nationaux et provinciaux (plus de 160), aux stations de montagne des Rocheuses et aux sites d’intérêt touristique (Fort Langley, Fort Rodo Hill). Victoria est riche en musées et en curiosités artisanales.</p>
<p>Vancouver est bâtie sur un site superbe. Il convient de visiter notamment le quartier chinois, les jardins et parcs (dont Stanley park), le musée d’anthropologie et de se promener sur les nombreuses plages le long du Pacifique.</p>
<h3 class="spip"><a id="sommaire_2"></a>Activités culturelles</h3>
<p><strong>Alliances françaises</strong></p>
<p>Il existe un portail Internet permettant d’accéder au <a href="http://www.af.ca/" class="spip_out" rel="external">réseau d’Alliances françaises</a> présentes au Canada (Moncton, Halifax, Ottawa-Gatineau,</p>
<p>Toronto, Winnipeg, Regina, Saskatoon, Victoria, Vancouver, Calgary, Edmonton et Montréal).</p>
<p><strong>Théâtre</strong></p>
<p>La majorité des pièces proposées sont en langue anglaise. A l’exception du théâtre universitaire, elles empruntent le plus souvent au répertoire du théâtre de boulevard. Ottawa dispose d’une salle francophone " La Nouvelle scène " et le Centre National des Arts organise chaque année une programmation de théâtre francophone. Toronto abrite le <a href="http://www.theatrefrancais.com/" class="spip_out" rel="external">Théâtre français</a>.</p>
<p>Des troupes françaises de théâtre et de spectacles musicaux (ballets, concerts, opéras) se produisent dans le cadre de festivals tels que le Coup de Cœur francophone, le Festival d’été de Québec, les Francofolies de Montréal, le festival de la Lanaudière ou encore au domaine Forget, le festival Juste pour Rire à Montréal. Les auteurs français de pièces classiques sont souvent à l’affiche.</p>
<p>Le théâtre québécois, très vivant et résolument ouvert, puise à la fois dans le répertoire classique et contemporain. Les créations québécoises sont souvent imaginatives et de qualité.</p>
<p><strong>Danse</strong></p>
<p>La Canada possède plusieurs troupes de ballet : le Ballet Royal de Winnipeg, le ballet national du Canada, ainsi que les grands ballets canadiens de Montréal. Des festivals, canadiens et internationaux, de danse contemporaine se tiennent chaque année à Toronto, Vancouver, Montréal et Calgary. Toronto accueille également le <a href="http://www.canab.com/" class="spip_out" rel="external">Canadian Arboriginal Festival</a>.</p>
<p>Pour en savoir plus, vous pouvez consulter le site Internet du <a href="http://www.canadacouncil.ca/" class="spip_out" rel="external">Conseil des Arts du Canada</a>. Vous y trouverez le calendrier des festivals des arts de la scène au Canada.</p>
<p><strong>Musique</strong></p>
<p>L’Orchestre du Centre National des Arts (CNA), l’Orchestre symphonique d’Ottawa, ainsi que les orchestres symphoniques de Toronto, Vancouver, Edmonton et Calgary proposent de nombreux concerts. L’opéra est surtout représenté à Toronto (" Canadian Opera Compagny " et " Opera Atelier").</p>
<p>Des concerts classiques, des ballets, des opéras et des spectacles de variétés et de folklore sont régulièrement donnés. La programmation est variée et aborde tous les genres. A Montréal, comme à Québec, la vie musicale est extrêmement féconde. On citera le <a href="http://www.montrealjazzfest.com/default-fr.aspx" class="spip_out" rel="external">festival international de jazz à Montréal</a>.</p>
<p>La ville compte quelques importantes figures de la danse contemporaine internationale (compagnies Maryse Chouinard et La La La Human Steps) et de cirque (Cirque du Soleil et Cirque Eloize). La province est de façon générale très active dans le domaine des arts de la scène.</p>
<p><strong>Cinéma</strong></p>
<p>Le cinéma américain domine le marché, surtout dans les salles appartenant aux deux principaux distributeurs canadiens. Ceux-ci ne diffusent que les films français ayant déjà connu un large succès en France. Toutes les villes comptent de nombreuses salles de cinéma. A Vancouver, un festival international se tient chaque année et il existe une cinémathèque de qualité (" Pacific "). Le festival international du film de Toronto est un important rendez-vous cinématographique. Toronto accueille également la fête du cinéma international francophone <a href="http://2014.cinefranco.com/splash/" class="spip_out" rel="external">Cinéfranco</a>.</p>
<p>On emploie l’expression " movie theater " en référence au cinéma.</p>
<p>Les principaux films français sont diffusés, quelques semaines après leur sortie en France, dans les salles de Québec et Montréal. Il existe par ailleurs des festivals de cinéma où sont présentés de nombreux films français.</p>
<p>Québec dispose de plus de 40 salles de cinéma, modernes et très confortables. Tous les films internationaux en langue française ou anglaise sont présentés dans une centaine de salles à Montréal. Les films américains sortent d’abord en version originale, puis doublés sans sous-titres. Parmi les festivals de cinéma les plus réputés, on citera :</p>
<ul class="spip">
<li>le <a href="http://www.image-nation.org/" class="spip_out" rel="external">festival international de cinéma LGBT de Montréal</a></li>
<li>le <a href="http://www.carrousel.qc.ca/" class="spip_out" rel="external">festival international de cinéma jeunesse de Rimouski</a></li>
<li>le <a href="http://www.festivalcinema.ca/" class="spip_out" rel="external">festival du cinéma international en Abitibi-Témiscamingue à Rouyn-Noranda</a></li></ul>
<p><strong>Expositions</strong></p>
<p>Ottawa possède de nombreux musées dont le Musée canadien des Civilisations à Gatineau. Des expositions sont régulièrement organisées au <a href="http://www.gallery.ca/en/" class="spip_out" rel="external">Musée des Beaux-Arts du Canada</a> ;</p>
<p>Toronto possède cinq grands musées, parmi lesquels le musée royal de l’Ontario et l’Art Gallery of Ontario, ainsi qu’une cinquantaine de galeries d’art.</p>
<p>A signaler également : à Vancouver, la Vancouver Art Gallery, le Vancouver Museum, le Musée d’Anthropologie de l’Université de Colombie Britannique (UBC), à Calgary, le Glenbow Museum et, à Edmonton, le Musée de l’Alberta qui dispose de plusieurs galeries où se tiennent des expositions d’artistes locaux (art indien et inuit). La Galerie d’Art de l’Université de Moncton expose aussi régulièrement de l’artisanat local et des créations d’artistes locaux.</p>
<p>De nombreuses expositions de peinture et de photographie sont organisées dans les musées et les galeries. Le Québec est réputé pour sa façon à la fois didactique et originale de concevoir les expositions.</p>
<p><strong>Bibliothèques</strong></p>
<p>Le Canada compte de nombreuses bibliothèques municipales et universitaires de qualité. On en dénombre à ce jour plus de 21000.</p>
<h3 class="spip"><a id="sommaire_3"></a>Sports</h3>
<p>De nombreux sports peuvent être pratiqués au Canada, été comme hiver. Les parcs provinciaux et nationaux offrent de belles et longues randonnées au cœur d’une nature splendide.</p>
<p>Le sport préféré des Canadiens est le hockey sur glace. Viennent ensuite le football américain, le basket-ball et le base-ball. La place et la pratique du football (<i>soccer</i>) au sein des diverses communautés canadiennes connaissent une forte augmentation.</p>
<p>Montréal organise un Grand Prix de Formule Un.</p>
<p>Bien que tous les sports pratiqués en France puissent l’être au Québec, les sports de glace et de neige (ski de fond, ski alpin, raquette, motoneige, hockey, patinage,etc.) sont particulièrement prisés des Québécois. Il existe autour de la ville de Québec plusieurs stations de ski alpin et de nombreux sentiers de ski de randonnée, dont un dans la ville même. Les patinoires extérieures et intérieures et les pistes de motoneige sont nombreuses. L’hiver, de nombreux Québécois profitent des patinoires publiques en plein air, mais aussi des sentiers de randonnées de ski de fond. Le hockey sur glace demeure l’un des sports les plus populaires et l’hiver les patinoires se remplissent de joueurs amateurs.</p>
<p>Les Québécois apprécient aussi beaucoup les sports en salle, notamment le badminton et le volley-ball. Toutes les villes du Québec proposent des activités sportives.</p>
<p>Il est tout à fait possible de se procurer l’équipement sur place. Il existe de nombreux clubs sportifs privés.</p>
<h3 class="spip"><a id="sommaire_4"></a>Télévision - Radio</h3>
<p><strong>Télévision</strong></p>
<p>On compte pas moins de 70 chaînes de télévision généralistes, la plus connue étant le réseau de service public Radio-Canada, et thématiques accessibles au Québec par le câble. Certaines diffusent uniquement en langue française et d’autres proposent régulièrement des productions françaises (Télé-Québec, Canal D, Music Plus, Musimax). Il est possible de capter les chaînes américaines (ABC, CBS, CNN, NBC, PBS). La qualité des programmes est très variable (peu de documentaires, beaucoup de films américains et des émissions de variétés). Il est possible de capter des chaînes généralistes de langue anglaise.</p>
<p><i>Mise à jour : avril 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/vie-pratique/article/loisirs-et-culture). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
