# Lettre de motivation

<p>Il n’y a pas de recommandations particulières à connaître, eu égard au contexte vietnamien, en matière de rédaction de lettre de motivation. Il est conseillé d’adopter des méthodes similaires à ce qui se fait en France. La lettre de motivation doit être, dans la mesure du possible, claire, lisible, structurée et concise.</p>
<p>A toutes fin utiles, pour toutes les petites et moyennes entreprises françaises et entrepreneurs individuels désireux de s’implanter au Vietnam, le Guide des affaires Vietnam publié par les bureaux Ubifrance au Vietnam présente les principales caractéristiques de l’économie du pays et propose des clés opérationnelles pour une approche pragmatique de ce marché : contexte économique et politique, secteurs porteurs, climat des affaires, réglementations à connaître, renseignements pratiques pour se rendre et s’implanter sur place.</p>
<p><i>Mise à jour : février 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/emploi-stage/article/lettre-de-motivation-111363). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
