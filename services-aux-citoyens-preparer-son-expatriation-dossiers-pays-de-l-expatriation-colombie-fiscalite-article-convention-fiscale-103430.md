# Convention fiscale

<p>La France et la Colombie négocient actuellement deux accords importants : une convention permettant d’éviter la double imposition et un accord de protection des investissements (ce dernier devant être signé au cours du 1<sup>er</sup> semestre 2014). La Colombie est également membre de la Convention de l’Agence multilatérale des garanties des investissements, élaborée dans le cadre de la Banque Mondiale, qui couvre les risques politiques et favorise les flux d’investissements vers les pays membres.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/fiscalite/article/convention-fiscale-103430). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
