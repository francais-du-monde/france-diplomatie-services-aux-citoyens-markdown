# Fiscalité

<h2 class="rub22971">Fiscalité du pays</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/fiscalite-22971/#sommaire_1">Année fiscale</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/fiscalite-22971/#sommaire_2">Impôt sur le revenu des personnes physiques et autres taxes</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/fiscalite-22971/#sommaire_3">Impôts sur les sociétés et autres taxes</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/fiscalite-22971/#sommaire_4">Taxe sur la valeur ajoutée (T.V.A.) et autres taxes</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/fiscalite-22971/#sommaire_5">Quitus fiscal </a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/fiscalite-22971/#sommaire_6">Solde du compte en fin de séjour</a></li></ul>
<p>Jusqu’à une date récente, le système fiscal sud-africain était fondé sur la notion de source de revenu : seuls les revenus d’origine sud-africaine ou considérés comme tels étaient imposés.</p>
<p>Depuis le 1er janvier 2001, l’Afrique du Sud a recours au système de la base mondiale d’imposition. Les résidents sud-africains, aux termes de l’<i>Income Tax Act</i>, voient désormais leur revenu total, quelle qu’en soit la source, imposé en Afrique du Sud. Ainsi, la notion de résidence, ou de siège permanent pour une entreprise, devient prépondérante pour l’assujettissement à l’impôt en Afrique du Sud.</p>
<p>Cependant, il convient de nuancer les conséquences de cette réforme, en précisant, d’une part, que les conventions fiscales internationales ratifiées par l’Afrique du Sud continuent de s’appliquer et, d’autre part, que les entreprises non-résidentes, ne bénéficiant pas de conventions fiscales, demeurent imposées en Afrique du Sud pour les revenus originaires de ce pays.</p>
<p>Depuis le 1er octobre 2001, une nouvelle taxe, <i>Capital Gains Tax</i> (CGT), a été introduite, portant sur l’imposition des plus-values afin que le système fiscal sud-africain se conforme aux normes internationales en la matière.</p>
<p>Enfin, la convention fiscale conclue entre la France et l’Afrique du Sud évite la double imposition des revenus des ressortissants des deux pays.</p>
<h3 class="spip"><a id="sommaire_1"></a>Année fiscale</h3>
<p>Elle s’étend du 1er mars au 28 février. La déclaration annuelle de revenus doit être déposée la 1ère quinzaine de juin auprès du service des impôts compétent (bureaux du <i>South Africain Revenue Service</i>). L’employeur doit fournir le certificat IRP5 indiquant la rémunération annuelle versée et l’impôt acquitté. Le certificat est joint à la déclaration annuelle de revenus faite sur l’imprimé IT 12S.</p>
<h3 class="spip"><a id="sommaire_2"></a>Impôt sur le revenu des personnes physiques et autres taxes</h3>
<h4 class="spip">L’impôt sur le revenu (income tax)</h4>
<p>Toute rémunération d’un employé (salaire, bonus, pension, avantages en nature, etc.) doit faire l’objet d’une retenue à la source (<i>Pay as you earn -</i> PAYE) sur la base des taux inscrits dans le tableau ci-dessous avant le 7 de chaque mois</p>
<h5 class="spip">Seuil minimum de revenu annuel net imposable </h5>
<ul class="spip">
<li>R 67 111 pour tout particulier de moins de 65 ans</li>
<li>R 104 611 pour les contribuables âgés de 65 à 74 ans</li>
<li>R 117 111 pour les plus de 75 ans</li></ul>
<h5 class="spip">Abattements (tax rebates)</h5>
<p>Toutes les personnes physiques bénéficient d’un abattement d’un montant de R 12080. Les personnes âgées de 65 à 74 ans bénéficient d’un abattement additionnel de R 6750 et celles de plus de 75 ans bénéficient de R 2250 supplémentaires.</p>
<h4 class="spip">L’imposition des avantages en nature</h4>
<p>Les avantages en nature (<i>fringe benefits</i>) alloués par les employeurs font l’objet d’une taxation spécifique selon s’il s’agit d’une couverture médicale, d’une voiture ou d’un logement de fonction, d’une prime de déplacement ou encore d’une cotisation à un fonds de pension.</p>
<h3 class="spip"><a id="sommaire_3"></a>Impôts sur les sociétés et autres taxes</h3>
<h4 class="spip">L’impôt sur les sociétés (company income tax)</h4>
<p>Selon la convention fiscale sud-africaine, toute personne morale dont le siège de la direction effective est situé en Afrique du Sud est considérée comme résidente et, par conséquent, imposable. Une entreprise française ne sera donc pas imposable en Afrique du Sud à moins qu’elle n’y exerce une activité par le biais d’un établissement stable tel que défini dans l’article 5. Selon l’article 5 de la convention fiscale franco-sud-africaine, l’expression établissement stable désigne une installation fixe d’affaires par l’intermédiaire de laquelle une entreprise exerce tout ou partie de son activité.</p>
<p>Les entreprises dans les secteurs des mines d’or, d’extraction de gaz, de pétrole ou de l’assurance-vie sont soumises à des régimes spécifiques. Les fonds de régime de retraite sont imposés selon le <i>Tax on retirement funds</i> à 25 %.</p>
<h4 class="spip">La taxe sur les plus-values (Capital Gains Tax)</h4>
<p>Elle est régie par la <i>Capital Gain Tax</i> (CGT) depuis le 1er octobre 2001.</p>
<p>Les sociétés résidentes en Afrique du Sud sont soumises à un impôt sur les plus-values réalisées lors de la cession d’actifs, sur l’ensemble de leurs actifs, y compris à l’étranger. La base taxable est de 66% des plus-values depuis le 1er mars 2002 (50% auparavant) et le taux d’imposition est de 18,65%.</p>
<p>Les entreprises non résidentes ne sont concernées par la CGT que si elles disposent de biens immobiliers ou d’actifs par l’intermédiaire d’une filiale dans le pays.</p>
<p>Cet impôt concerne également les personnes physiques.</p>
<h4 class="spip">L’impôt sur les dividendes (dividends withholding tax)</h4>
<p>L’impôt sur les dividendes, qui remplace depuis le 1er avril 2012 la <i>Secondary Tax on Companies</i>, s’élève à 15% des dividendes déclarés par les entreprises résidentes. Il est retenu à la source par l’entreprise qui les distribue.</p>
<h4 class="spip">Les redevances</h4>
<p>Le versement de redevances à un bénéficiaire non-résident est soumis à une autorisation préalable des autorités de change (<i>Reserve Bank</i>) et fait l’objet d’une retenue à la source,</p>
<h4 class="spip">L’imposition par les collectivités locales</h4>
<p>La plupart des provinces lèvent des impôts spécifiques, administrés par les <i>Regional Services Councils </i> :</p>
<ul class="spip">
<li>un impôt dit de service ;</li>
<li>un impôt dit d’établissement.</li></ul>
<h4 class="spip">La taxe destinée au développement des compétences (Skills Development Levy)</h4>
<p>Tout employeur enregistré auprès du <i>South African Revenue Service</i> (SARS), pour la retenue à la source obligatoire sur les salaires (PAYE) et versant plus de 550 000 rands de salaires par an, est soumis à une taxe spéciale, reversée à 80% à la <i>Sectorial Education  Training Authority</i> (SETA) du secteur considéré. Elle représente 1 % des salaires versés par l’employeur. Les employeurs, lorsqu’ils établissent un programme de formation continue, peuvent se voir rembourser une partie de leurs contributions.</p>
<h4 class="spip">La taxe au profit du fonds d’assurance chômage (Unemployment Insurance Fund)</h4>
<p><strong>Employeurs et employés doivent cotiser à l’UIF. L’employeur a la charge de régler directement auprès du SARS sa quote-part et celle de l’employé de 1% du salaire de base.</strong></p>
<h4 class="spip">La taxe carbone</h4>
<p>A partir du 1er janvier 2015, une taxe de R120 par tonne de CO² émise sera appliquée aux sociétés. Des exemptions seront prévues pour les industries fortement émettrices.</p>
<h3 class="spip"><a id="sommaire_4"></a>Taxe sur la valeur ajoutée (T.V.A.) et autres taxes</h3>
<h4 class="spip">La taxe sur les carburants </h4>
<p>Elle s’élève à environ 30% du prix à la pompe pour l’essence et pour le diesel. Une partie est reversée au fonds d’indemnisation des victimes d’accidents de la route.</p>
<h4 class="spip">La Value Added Tax</h4>
<p>Elle s’applique à tous les biens et services destinés à une utilisation finale ou à la revente. Elle est de 14 % pour les produits d’importation et perçue lors du passage en douane. Les exportations de biens et de services à partir de l’Afrique du Sud en sont en revanche exemptés. Toutes les entités dont le chiffre d’affaires annuel est supérieur à R1M doivent se faire enregistrer auprès du SARS.</p>
<h4 class="spip">Les droits de mutation de propriété (<i>transfer duty</i>)</h4>
<p>Toutes les transactions non soumises à la TVA sont soumises à cette taxe. Le montant perçu par le SARS dépend du montant du transfert et du bénéficiaire (de 0 à 8%).</p>
<h4 class="spip">La taxe sur les donations</h4>
<p>Cette taxe dont le donateur est redevable s’élève à 20 % du montant de la donation par des résidents et des entreprises (autres que les <i>public companies</i>) résidentes. Toutefois sont exemptées les donations dont le montant annuel n’excède pas R100 000 lorsque le donateur est une personne physique, ou R10 000 lorsque le donateur est une entreprise.</p>
<h3 class="spip"><a id="sommaire_5"></a>Quitus fiscal </h3>
<p>Aucun quitus fiscal n’est exigé avant de quitter le pays, compte tenu de la convention de non-double imposition signée avec la France.</p>
<h3 class="spip"><a id="sommaire_6"></a>Solde du compte en fin de séjour</h3>
<p>Un expatrié français relevant du secteur privé peut solder son compte en fin de séjour à condition de pouvoir justifier de l’origine des fonds à transférer.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.sars.gov.za/" class="spip_out" rel="external">South African Revenue Service (SARS)</a><br class="manualbr">Tél. : 0800 00 7277 (0800 00 SARS) (de 8h à 17h)<br class="manualbr">Depuis la France : +27 11 602 2093 (de 8h à 16h)</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Bureau Ubifrance à Johannesburg</strong><br class="manualbr">Téléphone : (+00.27) 011.303.7150<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/fiscalite-22971/#johannesburg#mc#ubifrance.fr#" title="johannesburg..åt..ubifrance.fr" onclick="location.href=mc_lancerlien('johannesburg','ubifrance.fr'); return false;" class="spip_mail">Courriel</a></p>
<p><i>Mise à jour : janvier 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-fiscalite-22971-article-convention-fiscale-111017.md" title="Convention fiscale">Convention fiscale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-fiscalite-22971-article-fiscalite-du-pays-111016.md" title="Fiscalité du pays">Fiscalité du pays</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/fiscalite-22971/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
