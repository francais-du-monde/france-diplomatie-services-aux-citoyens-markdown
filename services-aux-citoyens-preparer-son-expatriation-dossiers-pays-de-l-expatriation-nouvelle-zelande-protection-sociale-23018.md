# Protection sociale

<h2 class="rub23018">Régime local de sécurité sociale</h2>
<p>Des informations sur le régime de sécurité sociale en Nouvelle-Zélande sont disponibles sur les sites suivants :</p>
<ul class="spip">
<li>Informations générales sur le site de l’<a href="http://www.issa.int/fr/country-details?countryId=NZregionId=ASIfiltered=false" class="spip_out" rel="external">AISS</a> (Association internationale de la sécurité sociale)</li>
<li><a href="http://www.ssa.gov/policy/docs/progdesc/ssptw/2012-2013/asia/new-zealand.html" class="spip_out" rel="external">Guide complet en anglais</a> sur le site de la <i>Social Security Administration</i> des Etats-Unis.</li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-nouvelle-zelande-protection-sociale-23018-article-convention-de-securite-sociale-111237.md" title="Convention de sécurité sociale">Convention de sécurité sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-nouvelle-zelande-protection-sociale-23018-article-regime-local-de-securite-sociale-111236.md" title="Régime local de sécurité sociale">Régime local de sécurité sociale</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/protection-sociale-23018/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
