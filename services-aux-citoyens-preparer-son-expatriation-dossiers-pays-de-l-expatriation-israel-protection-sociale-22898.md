# Protection sociale

<h2 class="rub22898">Régime local de sécurité sociale</h2>
<p>Vous trouverez une présentation détaillée du système de sécurité sociale chilien sur le site de notre partenaire, le <a href="http://www.cleiss.fr/" class="spip_out" rel="external">CLEISS</a> (Centre des liaisons européennes et internationales de sécurité sociale). En voici la table des matières :</p>
<ul class="spip">
<li><a href="http://www.cleiss.fr/docs/regimes/regime_israel.html#a" class="spip_out" rel="external">Généralités</a></li>
<li>Assurance médicale obligatoire</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_israel.html#b" class="spip_out" rel="external">Assurance nationale obligatoire</a>
<br>— <a href="http://www.cleiss.fr/docs/regimes/regime_israel.html#b1" class="spip_out" rel="external">Pensions (Vieillesse - Invalidité - Survivants)</a>
<br>— <a href="http://www.cleiss.fr/docs/regimes/regime_israel.html#b3" class="spip_out" rel="external">Soins de longue durée</a>
<br>— <a href="http://www.cleiss.fr/docs/regimes/regime_israel.html#b5" class="spip_out" rel="external">Allocation de mobilité</a>
<br>— <a href="http://www.cleiss.fr/docs/regimes/regime_israel.html#b9" class="spip_out" rel="external">Maternité</a>
<br>— <a href="http://www.cleiss.fr/docs/regimes/regime_israel.html#b9" class="spip_out" rel="external">Allocations pour enfants</a>
<br>— <a href="http://www.cleiss.fr/docs/regimes/regime_israel.html#b11" class="spip_out" rel="external">Accidents professionnels</a> et <a href="http://www.cleiss.fr/docs/regimes/regime_israel.html#b12" class="spip_out" rel="external">non professionnels</a>
<br>— <a href="http://www.cleiss.fr/docs/regimes/regime_israel.html#b13" class="spip_out" rel="external">Chômage</a>
<br>— Solidarité</li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-israel-protection-sociale-22898-article-convention-de-securite-sociale-110465.md" title="Convention de sécurité sociale">Convention de sécurité sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-israel-protection-sociale-22898-article-regime-local-de-securite-sociale-110464.md" title="Régime local de sécurité sociale">Régime local de sécurité sociale</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/protection-sociale-22898/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
