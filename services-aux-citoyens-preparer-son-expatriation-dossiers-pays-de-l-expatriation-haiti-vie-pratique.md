# Vie pratique

<h2 class="rub23530">Logement</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/vie-pratique/#sommaire_1">Où se loger ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/vie-pratique/#sommaire_2">Conditions de location</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/vie-pratique/#sommaire_3">Hôtels</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/vie-pratique/#sommaire_4">Electricité</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/vie-pratique/#sommaire_5">Electroménager</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Où se loger ?</h3>
<p>Depuis le départ des d’ONG d’Haïti, beaucoup de maisons et appartements ont été libérés dans Port-au-Prince et ses environs. On trouve des zones résidentielles à Pacot et Canapé vert. Il est possible de se loger également à Pétion-Ville et à Vivy Michel.</p>
<h3 class="spip"><a id="sommaire_2"></a>Conditions de location</h3>
<p>En général, les propriétaires exigent dans leur contrat de location le paiement du premier mois de loyer, d’un mois de caution et du dernier mois.</p>
<p>Ce contrat stipule les obligations et conditions lors de la sortie du logement. Si ces conditions ne sont pas respectées, le montant cautionné ne sera pas remboursé.</p>
<h3 class="spip"><a id="sommaire_3"></a>Hôtels</h3>
<p>De nombreux hôtels de normes internationales sont disponibles à Port-au-Prince et ses environs. Le prix des chambres varie entre 130 US dollars pour une chambre simple à 175 US dollars pour une suite Exécutive.</p>
<p>Les réservations peuvent se faire par internet, mail ou téléphone.</p>
<h3 class="spip"><a id="sommaire_4"></a>Electricité</h3>
<p>A Port-au-Prince, la fourniture d’électricité par Electricité d’Haïti (EDH) est presque non existante. Par conséquent, toutes les maisons sont équipées d’un groupe électrogène pour l’alimentation en électricité et d’inverteurs dotés de batteries en cas de coupure d’électricité.</p>
<h3 class="spip"><a id="sommaire_5"></a>Electroménager</h3>
<p>Les magasins à Port-au-Prince et ses environs sont bien approvisionnés. Aucune difficulté pour se procurer sur place des appareils tels que : four électrique ou gazinière, réfrigérateur, lave-linge, sèche-linge, micro-onde, machine à café et autres.</p>
<p><i>Mise à jour : août 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-haiti-vie-pratique-article-pour-en-savoir-plus-114268.md" title="Pour en savoir plus">Pour en savoir plus</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-haiti-vie-pratique-article-loisirs-et-culture-114267.md" title="Loisirs et culture">Loisirs et culture</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-haiti-vie-pratique-article-communications-114266.md" title="Communications">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-haiti-vie-pratique-article-transports-114265.md" title="Transports">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-haiti-vie-pratique-article-cout-de-la-vie-114264.md" title="Coût de la vie">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-haiti-vie-pratique-article-scolarisation.md" title="Scolarisation">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-haiti-vie-pratique-article-sante.md" title="Santé">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-haiti-vie-pratique-article-logement-114261.md" title="Logement">Logement</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/vie-pratique/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
