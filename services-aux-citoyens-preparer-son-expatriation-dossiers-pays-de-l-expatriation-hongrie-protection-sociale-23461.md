# Protection sociale

<h2 class="rub23461">Régime local de sécurité sociale</h2>
<p>Vous trouverez une présentation détaillée du système de sécurité sociale en Hongrie sur le site de notre partenaire, le <a href="http://www.cleiss.fr/" class="spip_out" rel="external">CLEISS</a> (Centre des liaisons européennes et internationales de sécurité sociale). En voici la table des matières :</p>
<ul class="spip">
<li><a href="http://www.cleiss.fr/docs/regimes/regime_hongrie.html#I" class="spip_out" rel="external">Généralités</a> (Organisation, financement, taux de cotisation sur les salaires…)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_hongrie.html#II" class="spip_out" rel="external">Assurance maladie-maternité</a> (Prestations en nature et en espèces)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_hongrie.html#III" class="spip_out" rel="external">Accidents du travail et maladies professionnelles</a> (Régime indemnitaire, y compris pour les survivants)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_hongrie.html#IV" class="spip_out" rel="external">Invalidité, vieillesse, décès</a><a href="http://www.cleiss.fr/docs/regimes/regime_argentine.html#pensions" class="spip_out" rel="external">(survivants)</a> (Pensions de vieillesse, d’invalidité, de survivant, aide sociale)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_hongrie.html#V" class="spip_out" rel="external">Chômage</a> (Indemnités et assistance chômage)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_hongrie.html#VI" class="spip_out" rel="external">Prestations familiales</a> (Allocations familiales, de naissance, d’adoption…)</li></ul>
<p>Depuis le 1er janvier 2013, le système de sécurité social hongrois distingue en deux catégories les bénéficiaires du régime général en fonction de leur présence permanente ou non sur le territoire hongrois et ce indépendamment de leur nationalité.</p>
<p>Ainsi au sens de la sécurité sociale, sont étrangers (au système) toutes les personnes physiques qui ne sont pas résident. Ceux-ci sauf dispositions particulières ne peuvent prétendre au régime général de la sécurité sociale et doivent passer une convention et payer des droits spécifiques pour bénéficier de certaines prestations.</p>
<p>Au sens de la sécurité sociale, les résidents, qui bénéficient du régime général sont :</p>
<ul class="spip">
<li>Les citoyens hongrois qui justifient d’un domicile en Hongrie</li>
<li>Les immigrés</li>
<li>Les personnes physiques ayant le statut de résident permanent</li>
<li>Les personnes ayant le statut de réfugié</li>
<li>Les personnes physiques, qui dans le cadre des principes de la libre circulation et d’établissement sont domiciliés en pratique et enregistrés comme tel selon la législation hongroise (en sus de la période de trois mois au cours desquels ils peuvent librement séjourner en Hongrie). Il s’agit plus précisément :
<br>— des citoyens européens, non hongrois, ayant un permis de séjour, une attestation d’enregistrement
<br>— des membres de la famille non hongrois, accompagnant un ressortissant européen, et ayant un permis de séjour, une attestation d’enregistrement
<br>— des membres de la famille non hongrois, accompagnant un ressortissant hongrois, et ayant un permis de séjour, une attestation d’enregistrement
<br>— des citoyens européens dépendant ou vivant depuis un an minimum avec un citoyen hongrois ou une personne ayant un permis de séjour en Hongrie, ou bien une personne qui prend soin en personne d’un citoyen hongrois/européen ayant de grave problèmes de santé, si les autorités hongroises l’ont inscrit comme membre de la famille et lui ont octroyé un permis de séjour.
<br>— des personnes ayant des droits assimilés à ceux de citoyens européens, sans avoir la citoyenneté européenne et si elles ont un permis de séjour
<br>— des personnes dépendant ou vivant avec un citoyen européen dans le pays d’origine et que les autorités hongroises ont inscrites comme membre de la famille et lui ont octroyé un permis de séjour.</li></ul>
<p>Les personnes qui ont un emploi en Hongrie sont affiliées au régime général de sécurité social par leur employeur, à l’exception de salariés ou fonctionnaires dépendant d’organismes d’autres Etats. Les étudiants, qui justifient d’une bourse sont également affiliés au régime général de sécurité social par l’établissement d’enseignement qui les accueille.</p>
<p>Pour un court séjour ou dans l’attente de la régularisation de votre situation auprès de la sécurité sociale hongroise, nous vous conseillons de demander en France auprès de votre caisse de sécurité sociale la carte d’assurance maladie européenne.</p>
<p><i>Mise à jour : juin 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-hongrie-protection-sociale-23461-article-convention-de-securite-sociale-113475.md" title="Convention de sécurité sociale">Convention de sécurité sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-hongrie-protection-sociale-23461-article-regime-local-de-securite-sociale-113474.md" title="Régime local de sécurité sociale">Régime local de sécurité sociale</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/protection-sociale-23461/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
