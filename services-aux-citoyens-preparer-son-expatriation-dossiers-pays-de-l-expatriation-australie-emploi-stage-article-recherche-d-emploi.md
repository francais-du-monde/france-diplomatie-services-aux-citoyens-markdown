# Recherche d’emploi

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/emploi-stage/article/recherche-d-emploi#sommaire_1">Outils pour la recherche d’emploi</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/emploi-stage/article/recherche-d-emploi#sommaire_2">Organismes pour la recherche d’emploi</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Outils pour la recherche d’emploi</h3>
<p><strong>Journaux</strong></p>
<p>Les éditions du samedi et du mercredi sont les meilleures façons de connaître les emplois vacants permanents et/ou temporaires. Les journaux australiens sont généralement mis à disposition gratuitement dans les salles de lecture des bibliothèques et dans certains cafés et restaurants. Des offres d’emploi sont également publiées dans des magazines et journaux spécialisés ayant trait au commerce et à l’industrie. Dès lors que votre profil correspond à un secteur d’activité cherchant de la main d’œuvre, n’hésitez pas à publier votre demande d’emploi dans un des journaux ou magazines spécialisés.</p>
<ul class="spip">
<li><a href="http://www.thewest.com.au/" class="spip_out" rel="external">The West Australian</a> (Perth)</li>
<li><a href="http://www.theaustralian.news.com.au/" class="spip_out" rel="external">The Australian</a></li>
<li><a href="http://www.smh.com.au/" class="spip_out" rel="external">The Sydney Morning Herald</a></li>
<li><a href="http://www.news.com.au/dailytelegraph/" class="spip_out" rel="external">The Daily Telegraph</a> (Sydney)</li>
<li><a href="http://www.sunherald.com.au/" class="spip_out" rel="external">The Sun Herald</a> (Sydney)</li>
<li><a href="http://www.news.com.au/heraldsun/" class="spip_out" rel="external">The Herald Sun</a> (Melbourne)</li>
<li><a href="http://www.canberratimes.com.au/" class="spip_out" rel="external">The Canberra Times</a></li>
<li><a href="http://www.weeklytimes.com.au/" class="spip_out" rel="external">The Weekly Times</a></li>
<li><a href="http://www.news.com.au/adelaidenow/" class="spip_out" rel="external">The Advertiser</a> (Adelaïde)</li>
<li><a href="http://www.news.com.au/couriermail/" class="spip_out" rel="external">The Courier-Mail</a> (Brisbane)</li></ul>
<p><strong>Réseaux</strong></p>
<p>Les chambres de commerce et d’industrie mettent généralement en ligne sur leur site la liste de leurs membres et collaborent avec les organisations professionnelles en Australie. Il est donc possible de les solliciter afin de connaître celle qui concerne votre domaine professionnel pour ensuite la contacter en vue d’obtenir des conseils et quelques pistes.</p>
<ul class="spip">
<li><a href="http://www.facci.com.au/" class="spip_out" rel="external">Chambre de commerce et d’industrie franco-australienne</a></li>
<li><a href="http://www.cciwa.com/" class="spip_out" rel="external">Chambre de commerce et d’industrie de l’Ouest Australien</a> (<i>Chamber of Commerce and Industry Western Australia</i>)</li>
<li><a href="http://www.acci.asn.au/" class="spip_out" rel="external">Chambre de commerce et d’industrie australienne</a> (<i>Australian Chamber of Commerce and Industry</i>)</li>
<li><a href="http://www.thechamber.com.au/" class="spip_out" rel="external">Chambre de commerce de Sydney</a> (<i>Sydney Chamber of Commerce</i>)</li>
<li><a href="http://www.nswbusinesschamber.com.au/" class="spip_out" rel="external">Chambre des affaires de la Nouvelle-Galles du Sud</a> (<i>New South Wales Business Chamber</i>)</li>
<li><a href="http://www.amcham.com.au/" class="spip_out" rel="external">Chambre de commerce américaine en Australie</a> (<i>The American Chamber of Commerce in Australia</i>)</li>
<li><a href="http://www.accci.com.au/" class="spip_out" rel="external">Chambre de commerce et d’industrie chinoise de Nouvelle-Galles du Sud</a> (<i>The Australia-China Chamber of Commerce and Industry of New South Wales</i>)</li>
<li><a href="http://www.eabc.com.au/" class="spip_out" rel="external">European Australian Business Council</a></li>
<li><a href="http://ceda.com.au/" class="spip_out" rel="external">Comité pour le développement économique de l’Australie</a> (<i>Committee for economic development of Australia</i>)</li>
<li><a href="http://www.journoz.com/ausproforgs.html" class="spip_out" rel="external">Sociétés, associations et organisations professionnelles en Australie</a></li></ul>
<p><strong>Annuaires</strong></p>
<p>Afin de faciliter vos démarches de recherche d’emploi, il est conseillé de se renseigner sur le secteur d’activité, le type d’entreprise, sa taille et sa notoriété.</p>
<ul class="spip">
<li><a href="http://www.enterprisesearch.com.au/" class="spip_out" rel="external">Annuaire des entreprises</a> (<i>Australian Business Directory</i>)</li>
<li><a href="http://www.yellowpages.com.au/" class="spip_out" rel="external">Les pages jaunes de l’annuaire téléphonique</a>) sont également un bon outil de recherche</li></ul>
<p>La chambre de commerce et d’industrie franco-australienne publie un annuaire de ses membres.</p>
<p>La mission économique de Sydney a publié en 2008 un répertoire des entreprises françaises en Australie intitulé « La présence française en Australie - répertoires des implantations françaises ». Ce répertoire payant peut être acheté sur le site d’<a href="http://www.ubifrance.fr/" class="spip_out" rel="external">Ubifrance</a></p>
<h3 class="spip"><a id="sommaire_2"></a>Organismes pour la recherche d’emploi</h3>
<p>L’organisme de droit public <a href="http://www.centrelink.gov.au/" class="spip_out" rel="external">Centrelink</a> gère le versement des allocations chômage et vous aide dans votre recherche d’emploi. Consultez les rubriques « contact us » et « find us » pour connaître les coordonnées de l’agence la plus proche de votre domicile et pour obtenir des renseignements par téléphone. Les personnes ne maîtrisant pas la langue anglaise peuvent appeler le numéro suivant : 13 12 02.</p>
<p>Le gouvernement fédéral est également à l’initiative de plusieurs réseaux nationaux pour l’emploi :</p>
<ul class="spip">
<li><a href="http://www.jobsearch.gov.au/" class="spip_out" rel="external">Australian JobSearch</a></li>
<li><a href="http://www.jobaccess.gov.au/" class="spip_out" rel="external">JobAccess</a></li></ul>
<p>Parmi les organismes de recherche d’emploi figurent également la <a href="http://www.nesa.com.au/" class="spip_out" rel="external">National Employment Services Association</a> (NESA) et <a href="http://www.ja.com.au/" class="spip_out" rel="external">Jobs Australia</a>, qui aident les chômeurs, en particulier de longue durée, à trouver un emploi.</p>
<p>Parmi les nombreuses agences de placement en Australie (<i>recruitment agencies</i>, <i>employment agencies</i> ou <i>job hunting agencies</i>), nous pouvons citer :</p>
<ul class="spip">
<li><a href="http://adecco.com.au/" class="spip_out" rel="external">Adecco</a></li>
<li><a href="http://www.chandlermacleod.com/" class="spip_out" rel="external">Chandler Macleod</a></li>
<li><a href="http://www.hays.com.au/" class="spip_out" rel="external">Hays recruitment</a></li>
<li><a href="http://www.manpower.com.au/" class="spip_out" rel="external">Manpower</a></li>
<li><a href="http://www.rossjuliaross.com/" class="spip_out" rel="external">Ross Julia Ross Human Resources</a></li>
<li><a href="http://www.selectappointments.com.au/" class="spip_out" rel="external">Select Appointments</a></li></ul>
<p>Vous trouverez d’autres agences dans les <a href="http://www.yellowpages.com.au/" class="spip_out" rel="external">pages jaunes</a> de l’annuaire téléphonique.</p>
<p><i>Mise à jour : octobre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/emploi-stage/article/recherche-d-emploi). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
