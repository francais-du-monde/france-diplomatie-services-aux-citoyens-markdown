# Protection sociale

<h2 class="rub23455">Régime local de sécurité sociale</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/protection-sociale-23455/#sommaire_1">Branches d’assurance</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/protection-sociale-23455/#sommaire_2">Obligation d’assurance</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/protection-sociale-23455/#sommaire_3">Cotisations</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/protection-sociale-23455/#sommaire_4">Soins de santé</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/protection-sociale-23455/#sommaire_5">Assurance vieillesse</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/protection-sociale-23455/#sommaire_6">Assurance invalidité</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/protection-sociale-23455/#sommaire_7">Prestations familiales</a></li></ul>
<p>La Caisse nationale de sécurité sociale (CNSS) est un organisme de l’Etat ayant pour mission de gérer le régime de sécurité sociale institué au Burkina Faso. Ce régime de sécurité sociale est un système de protection sociale obligatoire, institué par l’Etat pour les travailleurs salariés et assimilés soumis au code du travail.</p>
<p>Toutefois, l’article 4 de la loi 15/2006 du 11 mai 2006 portant régime de sécurité sociale applicable aux travailleurs salariés et assimilés au Burkina Faso institue un régime non obligatoire de sécurité sociale au profit des personnes exerçant une activité professionnelle indépendante (ex. avocat, notaire, huissier de justice etc.).</p>
<h3 class="spip"><a id="sommaire_1"></a>Branches d’assurance</h3>
<p>La CNSS comprend trois branches :</p>
<ul class="spip">
<li>la branche des prestations familiales, chargée du service des prestations familiales et des prestations de maternité ;</li>
<li>la branche des risques professionnels, chargée de la prévention et du service des prestations en cas d’accident du travail et de maladie professionnelle ;</li>
<li>la branche des pensions, chargée du service des prestations de vieillesse, d’invalidité et de survivants.</li></ul>
<p>Ces branches sont complétées par une action sanitaire et sociale.</p>
<h3 class="spip"><a id="sommaire_2"></a>Obligation d’assurance</h3>
<p>Doivent être obligatoirement déclarés à la CNSS :</p>
<ul class="spip">
<li>tous les travailleurs soumis aux dispositions du code du travail burkinabé, quelle que soit leur nationalité, résidant au Burkina Faso et travaillant dans le secteur privé ou le secteur public.</li>
<li>Les élèves et étudiants des écoles ou centre de formation professionnelle et les apprentis ;</li>
<li>les apprentis et les élèves des établissements d’enseignement technique.</li></ul>
<p>Les personnes exerçant une activité non salariée peuvent devenir assurés volontaires de même que toute personne ayant été obligatoirement affiliée au régime de sécurité sociale pendant six mois consécutifs et qui cesse de remplir les conditions d’assujettissement, a la faculté de souscrire à une assurance volontaire, à condition d’en faire la demande dans les cinq ans qui suivent la date à laquelle son affiliation obligatoire a pris fin.</p>
<p>Pour les étrangers, la cotisation pour les trois branches est obligatoire à la CNSS.</p>
<h3 class="spip"><a id="sommaire_3"></a>Cotisations</h3>
<table class="spip">
<thead><tr class="row_first"><th id="id7883_c0"> </th><th id="id7883_c1">Vieillesse  </th><th id="id7883_c2">Risque professionnel  </th><th id="id7883_c3">Prestations familiales  </th><th id="id7883_c4">Total  </th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="id7883_c0">Part patronale</td>
<td headers="id7883_c1">5,5%</td>
<td headers="id7883_c2">3,5%</td>
<td headers="id7883_c3">7%</td>
<td headers="id7883_c4">16%</td></tr>
<tr class="row_even even">
<td headers="id7883_c0">Part salariale</td>
<td headers="id7883_c1">5,5%</td>
<td headers="id7883_c2">0</td>
<td headers="id7883_c3">0</td>
<td headers="id7883_c4">5,50%</td></tr>
<tr class="row_odd odd">
<td headers="id7883_c0">Total</td>
<td headers="id7883_c1">11%</td>
<td headers="id7883_c2">3,5%</td>
<td headers="id7883_c3">7%</td>
<td headers="id7883_c4">21,50%</td></tr>
</tbody>
</table>
<p>Les cotisations sont calculées sur un salaire brut plafonné de 600 000 FCFA.</p>
<h3 class="spip"><a id="sommaire_4"></a>Soins de santé</h3>
<p>La CNSS ne couvre pas le risque santé. Seules les compagnies d’assurance privées proposent des assurances santé.</p>
<p>Par exemple, l’une d’entre elles propose une assurance maladie destinée aux entreprises pour des groupes d’au moins 10 personnes, selon deux options :</p>
<ul class="spip">
<li>les soins sont dispensés localement au Burkina ;</li>
<li>les soins peuvent être dispensés, si nécessaire, en Côte d’Ivoire ou en France.</li></ul>
<h3 class="spip"><a id="sommaire_5"></a>Assurance vieillesse</h3>
<p>L’assuré qui atteint l’âge de départ à la retraite a droit à une pension de vieillesse s’il remplit les conditions suivantes :</p>
<ul class="spip">
<li>avoir cotisé pendant au moins 180 mois ;</li>
<li>avoir cessé toute activité salariée.</li></ul>
<p>Le montant de la pension de vieillesse est fixé en fonction de la rémunération mensuelle moyenne définie comme la 60ème partie du total des rémunérations soumises à cotisation au cours des cinq meilleures années d’assurance (article 86 de la loi 015/2006).</p>
<p>Au décès de l’assuré, les ayants droits peuvent percevoir une pension de :</p>
<ul class="spip">
<li>50 % pour le conjoint (en cas de pluralité de veuves, le montant est réparti entre elles par parts égales) ;</li>
<li>50 % pour l’orphelin (en cas de pluralité d’orphelins, le montant est réparti entre eux par parts égales)</li>
<li>25 % pour chaque ascendant en ligne directe du célibataire sans enfant (article 91 de la loi 015/2006).</li></ul>
<p>Les étrangers qui remplissent les conditions d’ouverture des droits peuvent, soit percevoir leur pension de vieillesse au Burkina, soit opter pour le transfert de cette pension dans leur pays d’origine.</p>
<h3 class="spip"><a id="sommaire_6"></a>Assurance invalidité</h3>
<p>Est considéré comme accident du travail, quelle qu’en soit la cause, l’accident survenu à un travailleur par le fait ou à l’occasion du travail, qu’il y ait ou non faute de sa part.</p>
<p>Sont considérés comme accidents du travail :</p>
<ul class="spip">
<li>l’accident survenu à un travailleur pendant le trajet d’aller et de retour entre sa résidence ou le lieu où il prend ordinairement ses repas et son lieu de travail ou de rémunération ;</li>
<li>l’accident survenu pendant les voyages dont les frais sont supportés par l’employeur.</li></ul>
<p>Les dispositions relatives aux accidents du travail sont applicables aux maladies professionnelles.</p>
<p>Il est procédé périodiquement à la mise à jour de la liste de ces maladies par décret, sur proposition conjointe du ministère de la Santé publique et du ministère du Travail.</p>
<p>Les prestations comprennent :</p>
<ul class="spip">
<li>les soins médicaux nécessités par les lésions résultant de l’accident (médicaments, hospitalisation, examens radio ou biologiques, etc…)</li>
<li>des indemnités journalières en cas d’incapacité temporaire de travail (le montant de cette indemnité est égal aux deux tiers de la rémunération journalière moyenne de la victime) ;</li>
<li>en cas de décès, une allocation de frais funéraires (montant égal à la moitié du salaire mensuel) et une rente aux survivants (calculées en pourcentage de la rémunération servant de base au calcul de l’allocation d’incapacité permanente).</li></ul>
<h3 class="spip"><a id="sommaire_7"></a>Prestations familiales</h3>
<p>Les prestations familiales comprennent les allocations prénatales, les allocations familiales ainsi que l’aide à la mère et au nourrisson sous forme de prestations en nature.</p>
<ul class="spip">
<li>les allocations prénatales sont attribuées à toute femme salariée ou conjointe d’un salarié. Le montant en est de 1000 FCFA par mois de grossesse. Ce droit est subordonné à l’observation, par la mère, de visites médicales.</li>
<li>les allocations familiales sont attribuées à l’assuré pour chacun des enfants à charge jusqu’à l’âge de 14 ans, dans la limite de six enfants. La limite d’âge des enfants est portée à 18 ans pour les enfants placés en apprentissage, et à 21 ans si l’enfant poursuit ses études. Le montant de cette allocation est de 2000 FCFA par mois et par enfant.</li>
<li>les prestations de maternité consistent en une indemnité journalière destinée à compenser la perte de salaire pendant la durée du congé de maternité (14 semaines), et en prestation en nature. Le montant de cette indemnité journalière est égal à la rémunération soumise à cotisation perçue au moment de la suspension du travail. La fraction de rémunération non soumise à cotisation est à la charge de l’employeur. La Caisse prend en charge la totalité des frais d’accouchement et des soins médicaux nécessaires pendant le congé maternité s’ils sont dispensés dans un établissement sanitaire agréé et sur présentation des pièces justificatives.</li></ul>
<p><i>Mise à jour : juin 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-burkina-faso-protection-sociale-23455-article-convention-de-securite-sociale-113448.md" title="Convention de sécurité sociale">Convention de sécurité sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-burkina-faso-protection-sociale-23455-article-regime-local-de-securite-sociale-113447.md" title="Régime local de sécurité sociale">Régime local de sécurité sociale</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/protection-sociale-23455/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
