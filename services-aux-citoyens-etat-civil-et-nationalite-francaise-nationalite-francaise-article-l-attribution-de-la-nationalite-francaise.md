# L’attribution de la nationalité française

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/nationalite-francaise/article/l-attribution-de-la-nationalite-francaise#sommaire_1">Par filiation (droit du sang)</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/nationalite-francaise/article/l-attribution-de-la-nationalite-francaise#sommaire_2">Par la double naissance en France (droit du sol)</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Par filiation (droit du sang)</h3>
<p>Est français l’enfant, dont l’un des parents au moins est français au moment de sa naissance. La filiation adoptive ne produit d’effet en matière d’attribution de la nationalité française que si l’adoption est plénière.</p>
<p>Par ailleurs, la filiation de l’enfant n’a d’effet sur la nationalité de celui-ci que si elle est établie durant sa minorité.</p>
<p>L’enfant qui n’est pas né en France et dont un seul des parents est français peut, sous certaines conditions, répudier la nationalité française.</p>
<h3 class="spip"><a id="sommaire_2"></a>Par la double naissance en France (droit du sol)</h3>
<p>Est français l’enfant, né en France lorsque l’un de ses parents au moins y est lui-même né.</p>
<p>La simple naissance en France ne vaut attribution de la nationalité française que pour l’enfant né de parents inconnus ou apatrides, ou de parents étrangers qui ne lui transmettent pas leur nationalité.</p>
<p>L’enfant né en France avant le 1er janvier 1994, d’un parent né sur un ancien territoire français d’outre-mer avant son accession à l’indépendance, est français de plein droit.</p>
<p>Il en est de même de l’enfant né en France après le 1er janvier 1963, d’un parent né en Algérie avant le 3 juillet 1962. Si un seul des parents est né en France, l’enfant peut, sous certaines conditions, répudier la nationalité française.</p>
<p>NB : l’attribution de la nationalité française est régie par le texte en vigueur avant que l’intéressé n’atteigne sa majorité. En effet, les lois nouvelles s’appliquent aux personnes encore mineures à la date de leur entrée en vigueur.</p>
<p><i>Mise à jour : mars 2016</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/nationalite-francaise/article/l-attribution-de-la-nationalite-francaise). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
