# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/emploi-stage/article/reglementation-du-travail-109983#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/emploi-stage/article/reglementation-du-travail-109983#sommaire_2">Fêtes légales</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/emploi-stage/article/reglementation-du-travail-109983#sommaire_3">Emploi du conjoint</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<p>Les principales dispositions, en matière de réglementation du travail, sont les suivantes :</p>
<ul class="spip">
<li><strong>la période d’essai </strong>est de trois mois ;</li>
<li><strong>durée du travail : </strong>44 heures hebdomadaires (soit huit heures par jour et quatre heures supplémentaires le samedi matin de 8h à 12h). Ces horaires dépendent des conventions collectives signées dans les entreprises ;</li>
<li><strong>repos hebdomadaire : </strong>deux jours (samedi et dimanche). S’agissant du personnel de maison, commercial et de restauration, le samedi est travaillé jusqu’à midi. Au-delà de cet horaire, le temps de travail est comptabilisé en heures supplémentaires ;</li>
<li><strong>coût des heures supplémentaires : </strong>50% les deux premières heures. Le travail dominical est payé double mais il est soumis à l’accord de l’administration compétente ;</li>
<li><strong>la durée des congés payés </strong>annuels est de 30 jours ;</li>
<li><strong>le nombre de jours fériés payés </strong>s’élève à 11 (au niveau fédéral) auxquels s’ajoutent les jours fériés spécifiques à chaque Etat ;</li>
<li><strong>congés maladie : </strong>les 15 premiers jours sont à la charge de l’employeur et payés plein tarif ;</li>
<li><strong>congés maternité : </strong>il est accordé quatre mois de congés à plein tarif auxquels s’ajoutent 15 jours pour allaitement ;</li>
<li><strong>en cas de rupture de contrat, </strong>le préavis est d’un mois ; en cas de licenciement sans faute professionnelle, l’employeur paye une indemnité correspondant à 50% de tous les versements au FGTS.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Fêtes légales</h3>
<p><strong>Fêtes nationales</strong></p>
<ul class="spip">
<li>1er janvier : Jour de l’An ;</li>
<li>carnaval ;</li>
<li>Vendredi Saint ;</li>
<li>21 avril : Fête de Tiradentes (héros national) ;</li>
<li>1er mai : Fête du travail ;</li>
<li>11 juin : Fête Dieu ;</li>
<li>7 septembre : Fête nationale du Brésil (Indépendance) ;</li>
<li>12 octobre : "Nossa Senhora de Aparecida" (patronne du Brésil) ;</li>
<li>2 novembre : Fête des défunts ;</li>
<li>15 novembre : Proclamation de la République ;</li>
<li>24 décembre : veille de Noël (1/2 journée) ;</li>
<li>25 décembre : Noël ;</li>
<li>31 décembre : après-midi férié.</li></ul>
<p><strong>Fêtes locales</strong></p>
<ul class="spip">
<li>20 janvier : "Sao Sebastiao" patron de la ville de Rio de Janeiro ;</li>
<li>25 janvier : Anniversaire de la ville de Sao Paulo ;</li>
<li>30 novembre : "Dia do Evangelico" (Brasilia)</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Emploi du conjoint</h3>
<p>Si l’expatrié a un visa temporaire avec contrat de travail, son conjoint a alors droit à un visa temporaire sans contrat de travail, ce dernier étant subordonné au premier.</p>
<p>Si l’expatrié a un visa permanent, son conjoint a droit au même visa.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/emploi-stage/article/reglementation-du-travail-109983). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
