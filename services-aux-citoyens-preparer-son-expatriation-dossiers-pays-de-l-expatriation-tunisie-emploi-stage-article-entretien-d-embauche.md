# Entretien d’embauche

<h4 class="spip">Apparence et attitude</h4>
<p>La présentation doit être particulièrement soignée et la tenue vestimentaire classique de préférence. Il est conseillé de préparer l’entretien en se renseignant sur l’entreprise au préalable.</p>
<p>Répondre aux questions posées de façon précise et exacte en restant souriant. Ne pas hésiter à poser les questions qui permettront de découvrir les besoins réels de l’entreprise.</p>
<h4 class="spip">Négociation du salaire</h4>
<p>En général, les aspects pécuniaires ne sont abordés qu’à la fin du parcours de sélection.</p>
<p><i>Mise à jour : novembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/emploi-stage/article/entretien-d-embauche). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
