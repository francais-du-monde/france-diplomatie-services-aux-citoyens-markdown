# Curriculum vitae

<p>Pour la rédaction de votre curriculum vitae, vous pouvez consulter et vous aider des sites suivants :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Le site Europass de l’Union européenne propose des modèles de <a href="http://europass.cedefop.europa.eu/fr/documents/curriculum-vitae" class="spip_out" rel="external">CV en français</a> et <a href="https://europass.cedefop.europa.eu/editors/hu/cv/compose" class="spip_out" rel="external">en hongrois</a>.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/emploi-stage/article/curriculum-vitae-113470). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
