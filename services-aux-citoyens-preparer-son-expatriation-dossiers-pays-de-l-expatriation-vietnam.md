# Vietnam

<p>Au <strong>31 décembre 2014</strong>, <strong>7035 Français </strong>étaient inscrits au registre des Français établis hors de France. 1336 Français étaient immatriculés à Hanoï, à la section consulaire de l’Ambassade et 5699 auprès du consulat général de France à Hô Chi Minh-Ville.</p>
<p>Plus de 200 entreprises françaises, P.M.E./P.M.I. ou filiales de grands groupes, sont installées au Vietnam (dont 110 filiales et succursales et 74 bureaux de représentation). 75% d’entre elles sont implantées à Hô Chi Minh-Ville et dans sa région. Ces sociétés françaises emploient près de 34 000 personnes. On estime, par ailleurs, entre 80 et 120 le nombre de sociétés de droit local créées et gérées par des Français.</p>
<p>Parmi les principales entreprises françaises implantées au Vietnam, on compte : Sanofi Aventis (médicaments de prescription, santé grand public et vaccins), Big C (distribution), Canal+, Accor, BNP Paribas, Natixis, Alstom, EDF, Total, Evialis-Guyomarch (agroalimentaire et agriculture), Schneider Electric, Air Liquide, Vinci, Bouygues, Saint-Gobain, Lafarge Cement (BTP), Technip (énergie), Gras Savoye (services), Groupama, CMA CGM (transport maritime), Perenco (exploitation pétrolière et gazière) , etc.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/dossiers-pays/vietnam/" class="spip_in">Une description du Vietnam, de sa situation politique et économique</a> ;</li>
<li><a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/vietnam/" class="spip_in">Des informations actualisées sur les conditions locales de sécurité au Vietnam</a> ;</li>
<li><a href="http://www.ambafrance-vn.org/" class="spip_out" rel="external">Ambassade de France au Vietnam</a>.</li></ul>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-vietnam-entree-et-sejour.md">Entrée et séjour</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-vietnam-entree-et-sejour-article-passeport-visa-permis-de-travail-111355.md">Passeport, visa, permis de travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-vietnam-entree-et-sejour-article-demenagement.md">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-vietnam-entree-et-sejour-article-vaccination-111357.md">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-vietnam-entree-et-sejour-article-animaux-domestiques-111358.md">Animaux domestiques</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-vietnam-emploi-stage.md">Emploi, stage</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-vietnam-emploi-stage-article-marche-du-travail.md">Marché du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-vietnam-emploi-stage-article-reglementation-du-travail.md">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-vietnam-emploi-stage-article-recherche-d-emploi.md">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-vietnam-emploi-stage-article-curriculum-vitae.md">Curriculum vitae</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-vietnam-emploi-stage-article-lettre-de-motivation-111363.md">Lettre de motivation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-vietnam-emploi-stage-article-stages.md">Stages</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-vietnam-protection-sociale.md">Protection sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-vietnam-protection-sociale-article-regime-local-de-securite-sociale.md">Régime local de sécurité sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-vietnam-protection-sociale-article-convention-de-securite-sociale-111367.md">Convention de sécurité sociale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-vietnam-fiscalite-23049.md">Fiscalité</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-vietnam-fiscalite-23049-article-fiscalite-du-pays-111368.md">Fiscalité du pays</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-vietnam-fiscalite-23049-article-convention-fiscale-111369.md">Convention fiscale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-vietnam-vie-pratique.md">Vie pratique</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-vietnam-vie-pratique-article-logement-111370.md">Logement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-vietnam-vie-pratique-article-sante.md">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-vietnam-vie-pratique-article-scolarisation.md">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-vietnam-vie-pratique-article-cout-de-la-vie-111373.md">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-vietnam-vie-pratique-article-transports.md">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-vietnam-vie-pratique-article-communications.md">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-vietnam-vie-pratique-article-loisirs-et-culture-111376.md">Loisirs et culture</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-vietnam-presence-francaise.md">Présence française</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-vietnam-presence-francaise-article-presence-francaise.md">Présence française</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
