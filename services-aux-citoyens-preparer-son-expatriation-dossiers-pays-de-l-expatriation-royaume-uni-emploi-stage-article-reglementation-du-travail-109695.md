# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/emploi-stage/article/reglementation-du-travail-109695#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/emploi-stage/article/reglementation-du-travail-109695#sommaire_2">Contrat de travail – spécificités</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/emploi-stage/article/reglementation-du-travail-109695#sommaire_3">Fêtes légales</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/emploi-stage/article/reglementation-du-travail-109695#sommaire_4">Emploi du conjoint</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<p>Les ressortissants de l’Union européenne qui désirent travailler au Royaume-Uni bénéficient des mêmes droits que les nationaux en ce qui concerne l’accès à l’emploi et à la formation professionnelle.</p>
<p>Le marché du travail au Royaume-Uni est libéral. Les contrats ne sont pas toujours écrits, mais passent par un accord systématique entre l’employeur et le salarié. Ainsi, même dans le cas d’un accord verbal, le contrat est réputé exister entre les deux parties.</p>
<p>Pour en savoir plus sur le contrat de travail : site <a href="https://ec.europa.eu/eures/main.jsp?countryId=UKacro=lwlang=frparentId=0catId=0regionIdForAdvisor=regionIdForSE=regionString=UK0%7C%20:" class="spip_out" rel="external">EURES</a></p>
<h4 class="spip">Période de préavis</h4>
<p>La période de préavis varie selon le contrat. A défaut de convention particulière, la durée de préavis est calculée sur la base d’une semaine par année de service, avec un minimum d’une semaine et un maximum de 12 semaines.</p>
<h4 class="spip">Durée du travail</h4>
<p>La législation fixe le nombre maximal d’heurs de travail selon le groupe d’âge. D’une manière générale, les jeunes de 16 à 18 ans ne peuvent travailler plus de 40 heures hebdomadaires ou huit heures par jour. Pour les travailleurs de plus de 18 ans, la limite est de 48 heures par semaine.</p>
<p>Cependant, des exceptions peuvent exister dans certaines catégories socio-professionnelles. C’est le cas dans l’hôtellerie, la restauration, les emplois de nuit ou encore dans les professions libérales. La durée effective est arrêtée sur le contrat de travail. Les entreprises fixent elles-mêmes les modalités de rémunération des heures supplémentaires.</p>
<p>Le temps de repos hebdomadaire minimum correspond à un jour plein par semaine ou à deux jours par période de deux semaines. Dans le cas d’un contrat de type <i>"Young Worker"</i>, le temps de repos hebdomadaire est fixé à deux jours.</p>
<p>Tout salarié a droit à au moins quatre semaines de congés payés par an (soit 20 jours de congés par an si vous travaillez cinq jours par semaine).</p>
<h4 class="spip">Autres dispositions</h4>
<p>La quasi-totalité des travailleurs ont droit au Royaume-Uni au salaire minimum national (<i>National Minimum Wage</i>), quels que soit la profession, le type de contrat de travail (temps partiel, contrat à durée déterminée ou indéterminée, etc.) ou la taille et le type de l’entreprise.</p>
<p>Le salaire minimum national est fixé chaque année au mois d’octobre par le gouvernement sur les recommandations de la commission indépendante pour les bas salaires (<i>Low Pay Commission </i>ou LCP).</p>
<p>Le salaire minimum national est fonction de l’âge du travailleur.</p>
<p>Le salaire horaire plancher (taux adulte) est de <strong>6,31£.</strong></p>
<p>Le taux pour les 18-20 ans est de <strong>5,03 £</strong> ;<strong> </strong>Pour les 16-17 ans, ce taux est de <strong>3,72 £ </strong></p>
<p><strong>L’âge légal </strong>de départ à la retraite, qui correspond à l’âge du versement de la <i>State Pension</i>, est établi entre 61 et 68 ans. Il dépend de votre date de naissance et de votre sexe.</p>
<p><strong>La durée des congés </strong>maladie payés ne peut dépasser 28 semaines.</p>
<p>La durée des congés payés de maternité correspond à une période de 26 semaines.</p>
<h4 class="spip">Recours en cas de problème</h4>
<p>Il est possible de s’adresser au <a href="http://www.citizensadvice.org.uk/" class="spip_out" rel="external">Citizens Advice Bureau</a> local. Ces bureaux défendent les droits de toute personne résidant au Royaume-Uni. Leurs services sont gratuits.</p>
<p>En cas de litige avec un employeur, il est également possible de contacter l’<i>"Advisory Conciliation and Arbitration Service"</i> :</p>
<p><a href="http://www.acas.org.uk/" class="spip_out" rel="external">Advisory Conciliation and Arbitration Service</a> (ACAS)</p>
<p>Assistance par téléphone (numéro national) : 08 457 47 47 47</p>
<p><strong>Bureau de Londres</strong><br class="manualbr">Euston Tower<br class="manualbr">286 Euston Road<br class="manualbr">London NW1 3JJ</p>
<p>Sur Internet, un site propose des informations pratiques sur les principales dispositions réglementaires et les aides possibles en matière d’emploi et affaires sociales : <a href="http://www.adviceguide.org.uk/" class="spip_out" rel="external">www.adviceguide.org.uk/</a></p>
<h3 class="spip"><a id="sommaire_2"></a>Contrat de travail – spécificités</h3>
<h4 class="spip">Cadre du contrat : détachement ou expatriation</h4>
<p>Deux cas de figure peuvent se présenter. Le contrat de travail peut être fait soit dans le cadre d’un détachement, soit dans le cadre d’une expatriation.</p>
<p>Dans le cadre du détachement, l’entreprise emploie un salarié en France puis le détache à l’étranger, pour une période variable mais limitée, au terme de laquelle l’employé retrouve son poste en France. Ce contrat sera soumis au droit français.</p>
<p>Dans le cadre de l’expatriation, le salarié est recruté soit en France, soit à l’étranger pour le compte d’une entreprise implantée à l’étranger ou d’une société locale. Les conditions de négociation ne seront pas les mêmes selon que l’employeur fait venir le salarié de France ou qu’il l’engage selon les modalités d’un contrat de travail local. Dans le premier cas, si les parties en décident ainsi, le contrat pourra être soumis au droit français. S’il s’agit d’un contrat local, les relations de travail seront régies par le droit local.</p>
<h4 class="spip">Règles locales applicables au Royaume-Uni en matière de contrat</h4>
<p>Le marché du travail au Royaume-Uni est libéral. Les contrats ne sont pas toujours écrits, mais passent par un accord systématique entre l’employeur et le salarié. Ainsi, même dans le cas d’un accord verbal, le contrat est réputé exister entre les deux parties.</p>
<p>L’employeur est cependant tenu (loi de 1996 relative aux droits des travailleurs -E<i>mployment Rights Act</i>-) de remettre au salarié, avant la fin du deuxième mois d’embauche, un document écrit <i>(written statement) </i>sur lequel doivent figurer, au minimum, les mentions suivantes :</p>
<ul class="spip">
<li>le nom de l’employeur et de l’employé,</li>
<li>la date d’embauche,</li>
<li>la nature du poste occupé,</li>
<li>le salaire ainsi que la manière dont celui-ci est versé (règlement hebdomadaire, mensuel),</li>
<li>la durée du temps de travail ainsi que les horaires,</li>
<li>le nombre de jours de congés payés,</li>
<li>la durée de la période de préavis en cas de rupture de contrat à l’initiative de l’employé ou de l’employeur,</li>
<li>le lieu d’exercice, notamment en cas de travail sur plusieurs sites,</li>
<li>les droits de l’employé en matière de congés maladie,</li>
<li>la possibilité ou non pour l’employé de souscrire à une retraite complémentaire au sein de l’entreprise,</li>
<li>les procédures disciplinaires mises en place par l’employeur.</li></ul>
<p>Au fil des ans, les salariés ont acquis de nombreux droits statutaires émanant de lois ou de mesures parlementaires et touchant la relation de travail. Ils ne peuvent généralement pas faire l’objet d’une renonciation. On citera notamment :</p>
<ul class="spip">
<li>le droit à l’égalité salariale ;</li>
<li>le droit de s’affilier à l’organisation syndicale de son choix ;</li>
<li>le droit de réintégrer son emploi après un congé maternité ;</li>
<li>le droit à l’aménagement du temps de travail et au congé parental ;</li>
<li>la protection contre les discriminations ;</li>
<li>la protection contre les licenciements abusifs.</li></ul>
<h4 class="spip">Négociation du contrat de travail</h4>
<p>La négociation du contrat de travail est différente selon qu’il s’agit d’un contrat de détachement avec un employeur en France, d’un contrat d’expatriation (avant le départ au Royaume-Uni) ou d’un contrat local. Dans ce dernier cas, il est important d’être vigilant quant à la nature du contrat ; s’il est oral, le contrat est soumis au droit britannique et la marge de négociation de l’employé s’en trouvera considérablement réduite. Lorsque cela est possible, il est préférable de solliciter un contrat écrit sur lequel figureront les clauses arrêtées par les parties.</p>
<h5 class="spip">Clauses obligatoires</h5>
<p>Dans tous les cas, il est indispensable que figurent certaines clauses essentielles au contrat de travail :</p>
<ul class="spip">
<li>l’identification et la qualité des parties ;</li>
<li>la législation applicable au contrat et la juridiction compétente en cas de litige ;</li>
<li>la durée du contrat (durée déterminée ou indéterminée, conditions de la reconduction éventuelle) ;</li>
<li>l’existence d’une période d’essai et sa durée ;</li>
<li>le lieu de travail ;</li>
<li>la date du début de l’activité.</li></ul>
<p>Les parties au contrat sont tenues de déterminer certaines clauses obligatoires telles que :</p>
<ul class="spip">
<li>la fonction à exercer, l’intitulé du poste, la qualification et la classification du poste ainsi que les liens de subordination ;</li>
<li>le montant de la rémunération, des primes et indemnités, et les modalités de révision de la rémunération ;</li>
<li>le régime de prévoyance, de chômage et de retraite, sans oublier les régimes complémentaires de prévoyance, de retraite ou d’assurance chômage ;</li>
<li>les modalités d’ancienneté, d’avancement, de promotion ou de mutation ;</li>
<li>les modalités de rupture du contrat (préavis, indemnité de rupture), clauses de non-concurrence ;</li>
<li>la durée des congés.</li></ul>
<p>Les parties sont libres d’ajouter d’autres clauses, selon le cas considéré et selon la législation applicable : des clauses déterminant les conditions de séjour, les modalités de départ et de rapatriement.</p>
<h5 class="spip">Clauses soumises à l’autonomie de la volonté</h5>
<p>Certaines clauses sont soumises à l’autonomie de la volonté des parties : c’est à dire que les parties seront libres d’aménager leur contrat à leur guise et de décider du contenu de certaines clauses. Cette volonté des parties a force de loi pourvu qu’elle ne décide pas de conditions d’emploi plus défavorables que celles prévues par la loi en vigueur.</p>
<h5 class="spip">Autres négociations possibles</h5>
<ul class="spip">
<li>la durée du séjour ;</li>
<li>les frais de voyage (mode de voyage et classe), les frais de rapatriement en fin de séjour, la prise en charge du voyage aller et du voyage retour pour l’intéressé et sa famille, ainsi que des voyages en France à l’occasion des congés et éventuellement en cas de maladie grave de l’intéressé, d’un membre de sa famille ou en cas de décès d’un ascendant direct de l’intéressé ou de son conjoint ;</li>
<li>la prise en charge des dépenses de déménagement à l’aller et au retour (frais de douane, assurances, transport) ;</li>
<li>les indemnités d’installation et de réinstallation ;</li>
<li>le recyclage éventuel au retour en France ;</li>
<li>la répartition de la rémunération entre versements locaux et versements en France ;</li>
<li>les avantages éventuellement accordés (logement, employés de maison, voiture de fonction ou indemnités d’utilisation, etc.) ;</li>
<li>la prise en charge des frais de scolarité des enfants ;</li>
<li>la visite médicale d’aptitude de l’intéressé et de sa famille avant le départ et à chaque congé.</li></ul>
<p>Toute <strong>modification du contrat de travail </strong>exige l’accord de l’employeur et du salarié. Il peut s’agir d’un accord verbal ou bien d’un acte écrit, ou avenant, constatant les modifications apportées (l’avenant est préférable, car il permet d’éviter toute contestation ultérieure).</p>
<h4 class="spip">Perte d’emploi</h4>
<p>En cas de <strong>rupture du contrat de travail</strong>, l’employeur (s’il s’agit d’un licenciement) ou le salarié (si celui-ci démissionne) sont tenus de respecter un préavis minimum. La durée du préavis de licenciement dépend de votre ancienneté. Si l’employeur ne la respecte pas, le salarié peut déposer une plainte devant les tribunaux pour licenciement abusif. De même, si le salarié quitte son emploi sans observer le préavis requis, l’employeur a le droit d’opérer une retenue de salaire ou de demander des dommages-intérêts. Dans certaines circonstances, le préavis ne s’applique pas, par exemple en cas de licenciement pour faute grave ou en cas de démission forcée.</p>
<p>Si vous estimez que vous avez fait l’objet d’un licenciement abusif, demandez conseil à l’<a href="http://www.acas.org.uk/index.aspx?articleid=1461" class="spip_out" rel="external">ACAS</a> (tél. : 08457 47 47 47), qui joue le rôle de médiateur dans les conflits du travail. La plainte officielle pour licenciement abusif doit être déposée devant un tribunal du travail les trois mois suivant le départ du salarié.</p>
<p>En cas de <strong>perte d’emploi</strong>, vous pouvez bénéficier d’une assistance juridique si vous estimez que vous avez fait l’objet d’un licenciement abusif ou si vous souhaitez des conseils au sujet de votre indemnité de licenciement. Vous pouvez commencer par en parler à votre syndicat, au <a href="http://www.citizensadvice.org.uk/" class="spip_out" rel="external">Citizens Advice Bureau</a> (bureau de consultation pour les citoyens) le plus proche de votre domicile ou à un <i>Law Centre</i> (centre de consultation juridique) : vous ne disposez généralement que de trois mois, à compter de votre licenciement, pour déposer une plainte. Votre syndicat, le bureau local du <i>Citizens Advice Bureau</i>, un <i>Law Centre</i> ou un <i>solicitor</i> (conseil juridique) vous donneront de plus amples informations.</p>
<p>Si vous êtes au <strong>chômage</strong>, il est possible que vous soyez éligible à la <i>Jobseeker’s Allowance</i> (allocation de recherche d’emploi). Votre <i>Jobcentre Plus</i> local vous aidera à constituer votre dossier pour être indemnisé.</p>
<p>Pour plus de renseignements, adressez-vous au <i>Citizen’s Advice Bureau</i> le plus proche de chez vous ou consultez le site Internet du service <a href="http://www.jobcentreplus.gov.uk/JCP/index.html" class="spip_out" rel="external">Jobcentre Plus</a>.</p>
<h3 class="spip"><a id="sommaire_3"></a>Fêtes légales</h3>
<ul class="spip">
<li>Jour de l’An, <i>New Year’s Day</i></li>
<li>2 janvier :<i> Bank Holiday </i>(en Ecosse)</li>
<li>17 mars : Fête de Saint Patrick, <i>St Patrick’s Day </i>(en Irlande du Nord)</li>
<li>Vendredi Saint, <i>Good Friday</i></li>
<li>Lundi de Pâques, <i>Easter Monday </i>(excepté en Ecosse)</li>
<li>Premier lundi de mai : <i>Early May Bank Holiday</i></li>
<li>Dernier lundi de mai : <i>Spring Bank Holiday</i></li>
<li>Dernier lundi d’août : <i>Summer Bank Holiday (célébré en Ecosse le premier lundi d’août)</i></li>
<li>Noël : <i>Christmas Day</i></li>
<li>Lendemain de Noël : <i>Boxing Day</i></li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Emploi du conjoint</h3>
<p>Du fait de la réglementation européenne, les Français peuvent travailler au Royaume-Uni sans qu’un permis de travail soit nécessaire.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/emploi-stage/article/reglementation-du-travail-109695). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
