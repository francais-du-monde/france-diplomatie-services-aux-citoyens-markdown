# Régime local de sécurité sociale

<p>L’organisation de sécurité sociale (Tamin), organisme public, gère les risques santé, chômage, accidents du travail et retraite. Des régimes spécifiques existent pour le risque retraite dans des secteurs publics ou parapublics (fonds de pension des employés du pétrole ou de la compagnie Iran Air, des forces armées par exemple). La couverture n’est pas universelle, mais elle comprend la quasi-totalité des salariés, et une bonne part des travailleurs indépendants.</p>
<p>La part patronale des cotisations s’élève à 20% du salaire, la part employé s’élève à 7%, l’Etat verse une contribution égale à 3%.</p>
<p>Dans la pratique les remboursements au titre du risque maladie ne couvrent pas la totalité des dépenses.</p>
<p><i>Mise à jour : février 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/protection-sociale-22994/article/regime-local-de-securite-sociale-111123). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
