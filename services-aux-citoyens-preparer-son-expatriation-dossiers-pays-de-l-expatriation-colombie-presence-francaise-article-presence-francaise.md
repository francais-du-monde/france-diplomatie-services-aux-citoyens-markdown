# Présence française

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/presence-francaise/article/presence-francaise#sommaire_1">Autorités françaises dans le pays</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/presence-francaise/article/presence-francaise#sommaire_2">Associations dans le pays</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Autorités françaises dans le pays</h3>
<h4 class="spip">Ambassade et consulat de France </h4>
<p>Consultez notre article <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/article/annuaire-des-representations-francaises-a-l-etranger" class="spip_in">Annuaire des représentations françaises à l’étranger</a>.</p>
<h4 class="spip">Réseau français de coopération et d’action culturelle</h4>
<p>Les coordonnées des services du réseau de coopération et d’action culturelle français en Colombie ainsi que la description de l’activité de ces services se trouvent sur <a href="http://www.latitudefrance.org/AnnuaireCarto.html" class="spip_out" rel="external">Latitude France</a>.</p>
<h4 class="spip">Economie : réseau mondial</h4>
<p>Business France, l’Agence française pour le développement international des entreprises, <a href="http://export.businessfrance.fr/colombie/nos-bureaux-a-votre-service.html" class="spip_out" rel="external">est présente en Colombie</a>. Sa mission est d’accompagner les entreprises françaises dans leur développement à l’international.</p>
<p>Les services économiques sont présent à l’ambassade de France en Colombie. Ils sont une émanation de la direction générale du Trésor et ont pour missions principales l’analyse macroéconomique et l’animation des relations économiques bilatérales entre la France et les pays où ils sont implantés.</p>
<ul class="spip">
<li><a href="http://www.tresor.economie.gouv.fr/pays/colombie" class="spip_out" rel="external">Service économique français en Colombie</a></li>
<li><a href="http://www.ccef-colombie.org/" class="spip_out" rel="external">Conseillers du commerce extérieur français installés en Colombie</a></li>
<li><a href="http://www.france-colombia.com/" class="spip_out" rel="external">Chambre franco-colombienne de commerce et d’industrie</a></li></ul>
<h4 class="spip">Vos élus à l’Assemblée des Français de l’étranger et au Sénat</h4>
<p>Pour toute information sur l’Assemblée des Français de l’étranger et pour connaître les conseillers et les Sénateurs qui représentent les Français établis hors de France dans votre circonscription, vous pouvez consulter les sites Internet suivants :</p>
<ul class="spip">
<li><a href="http://www.assemblee-afe.fr/" class="spip_out" rel="external">l’Assemblée des Français de l’étranger (AFE)</a></li>
<li><a href="http://www.expatries.senat.fr" class="spip_out" rel="external">le Sénat au service des Français de l’étranger</a></li>
<li><a href="http://www.senat.fr/expatries/dossiers_pays/colombie.html" class="spip_out" rel="external">dossier spécifique à la Colombie sur le site du Sénat</a></li></ul>
<h4 class="spip">Députés des Français de l’étranger</h4>
<p>Les Français expatriés peuvent élire leur député à l’Assemblée nationale. Ils sont répartis en onze circonscriptions législatives. Pour plus d’information, consulter la page dédiée aux <a href="http://www.assemblee-nationale.fr/14/qui/circonscriptions/099.asp" class="spip_out" rel="external">députés des Français établis hors de France sur le site de l’Assemblée Nationale</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Associations dans le pays</h3>
<ul class="spip">
<li><a href="http://adfecolombie.blogspot.com/" class="spip_out" rel="external">Association des Français de l’Étranger, section Colombie</a></li>
<li><a href="http://www.bogota-accueil.com/" class="spip_out" rel="external">Bogota Accueil</a></li>
<li><a href="http://www.ambafrance-co.org/AFCA-Colombie" class="spip_out" rel="external">AFCA Colombie</a></li>
<li><a href="http://www.france-colombie.com/" class="spip_out" rel="external">La Chambre franco-colombienne de commerce et d’industrie</a></li></ul>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.ccef-colombie.org/" class="spip_out" rel="external">Section colombienne des conseillers du commerce extérieur de la France</a></p>
<p>Pour en savoir plus :</p>
<ul class="spip">
<li>notre article thématique : <a href="services-aux-citoyens-preparer-son-expatriation-en-savoir-plus-article-associations-des-francais-de-l-etranger.md" class="spip_in">Associations des Français de l’étranger</a></li>
<li><a href="http://www.ambafrance-co.org/" class="spip_out" rel="external">page consacrée aux associations sur le site de l’Ambassade de France en Colombie</a></li></ul>
<p><i>Mise à jour : septembre 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/presence-francaise/article/presence-francaise). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
