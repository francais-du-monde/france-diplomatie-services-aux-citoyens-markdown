# République Dominicaine

<p><strong>Au 31 décembre 2013</strong>, le nombre d’inscrits au registre des Français résidant en République dominicaine s’élève à <strong>3994</strong>.</p>
<p>A la fin des années 2000, la communauté française a connu une <strong>importante augmentation</strong>. Le nombre de résidents semble s’être stabilisé depuis 2010. La population française en République dominicaine reste toutefois inférieure à celle d’autres communautés européennes (espagnole et italienne notamment) ou nord-américaines (Canada, États-Unis).</p>
<p>Cette communauté se compose en grande partie d’expatriés travaillant dans le secteur tertiaire (tourisme, commerce, artisanat) et de retraités. Ils sont majoritairement implantés à <strong>Saint-Domingue</strong> (près de la moitié) ainsi qu’au Nord-Est sur la <strong>péninsule de Samana</strong> (<strong>Las Terrenas</strong>) (près du quart). Les zones de <strong>Bavaro / Punta Cana</strong> à l’Est, ainsi que la côte Sud de <strong>Boca Chica</strong> à <strong>Bayahibe</strong>, attirent elles aussi les ressortissants français désireux de profiter des revenus du tourisme.</p>
<p>En revanche, les Français installés à l’intérieur des terres sont rares (quelques dizaines à <strong>Santiago</strong>).</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/dossiers-pays/republique-dominicaine/" class="spip_in">Une description de la République Dominicaine de sa situation politique et économique</a> ;</li>
<li><a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/republique-dominicaine/" class="spip_in">Des informations actualisées sur les conditions locales de sécurité en République Dominicaine</a> ;</li>
<li><a href="http://www.ambafrance.org.do/" class="spip_out" rel="external">Ambassade de France en République Dominicaine</a>.</li></ul>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-dominicaine-entree-et-sejour.md">Entrée et séjour</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-dominicaine-entree-et-sejour-article-passeport-visa-permis-de-travail.md">Passeport, visa, permis de travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-dominicaine-entree-et-sejour-article-demenagement-111307.md">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-dominicaine-entree-et-sejour-article-vaccination.md">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-dominicaine-entree-et-sejour-article-animaux-domestiques.md">Animaux domestiques</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-dominicaine-emploi-stage.md">Emploi, stage</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-dominicaine-emploi-stage-article-marche-du-travail-111311.md">Marché du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-dominicaine-emploi-stage-article-reglementation-du-travail.md">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-dominicaine-emploi-stage-article-recherche-d-emploi.md">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-dominicaine-emploi-stage-article-stages.md">Stages</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-dominicaine-protection-sociale-23036.md">Protection sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-dominicaine-protection-sociale-23036-article-regime-local-de-securite-sociale-111318.md">Régime local de sécurité sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-dominicaine-protection-sociale-23036-article-convention-de-securite-sociale-111319.md">Convention de sécurité sociale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-dominicaine-fiscalite.md">Fiscalité</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-dominicaine-fiscalite-article-fiscalite-du-pays.md">Fiscalité du pays</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-dominicaine-fiscalite-article-convention-fiscale-111321.md">Convention fiscale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-dominicaine-vie-pratique.md">Vie pratique</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-dominicaine-vie-pratique-article-logement-111322.md">Logement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-dominicaine-vie-pratique-article-sante.md">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-dominicaine-vie-pratique-article-scolarisation-111324.md">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-dominicaine-vie-pratique-article-cout-de-la-vie-111325.md">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-dominicaine-vie-pratique-article-transports-111326.md">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-dominicaine-vie-pratique-article-communications.md">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-dominicaine-vie-pratique-article-loisirs-et-culture-111328.md">Loisirs et culture</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-dominicaine-presence-francaise.md">Présence française</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-dominicaine-presence-francaise-article-presence-francaise.md">Présence française</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
