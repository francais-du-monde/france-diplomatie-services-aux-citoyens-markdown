# Lettre de motivation

<p>Lors de la rédaction de la lettre de motivation, le candidat à l’embauche devra suivre les mêmes principes et règles qu’en France.</p>
<p><i>Mise à jour septembre 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/emploi-stage/article/lettre-de-motivation-114771). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
