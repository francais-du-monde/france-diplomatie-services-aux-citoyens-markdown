# Programmes proposés par les Conseils départementaux

<p><img src="http://www.diplomatie.gouv.fr/fr/IMG/jpg/france_departements.jpg" width="550" height="751" border="0" usemap="#Map"></p>
<map name="Map">
<area shape="rect" coords="277,278,293,293" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/yvelines">
<area shape="rect" coords="341,323,366,348" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/yonne">
<area shape="rect" coords="456,297,480,321" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/vosges">
<area shape="rect" coords="217,399,243,426" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/vienne-44123">
<area shape="rect" coords="147,400,177,423" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/vendee">
<area shape="rect" coords="415,558,442,580" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/vaucluse">
<area shape="rect" coords="468,597,493,617" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/var">
<area shape="rect" coords="46,64,76,81" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/val-d-oise">
<area shape="rect" coords="490,334,508,356" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/territoire-de-belfort">
<area shape="rect" coords="246,566,271,584" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/tarn-et-garonne">
<area shape="rect" coords="287,581,307,606" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/tarn">
<area shape="rect" coords="282,193,310,218" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/somme">
<area shape="rect" coords="72,92,105,112" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/seine-saint-denis">
<area shape="rect" coords="233,212,268,239" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/seine-maritime">
<area shape="rect" coords="322,280,343,302" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/seine-et-marne">
<area shape="rect" coords="475,470,497,495" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/savoie">
<area shape="rect" coords="210,313,233,338" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/sarthe">
<area shape="rect" coords="380,402,418,428" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/saone-et-loire">
<area shape="rect" coords="395,446,415,473" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/rhone">
<area shape="rect" coords="12,678,70,740" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/reunion">
<area shape="rect" coords="299,663,332,680" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/pyrenees-orientales">
<area shape="rect" coords="129,605,192,630" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/pyrenees-atlantiques">
<area shape="rect" coords="320,455,347,480" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/puy-de-dome">
<area shape="rect" coords="279,161,319,187" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/pas-de-calais">
<area shape="rect" coords="42,111,65,129" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/paris-44098">
<area shape="rect" coords="205,276,227,294" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/orne">
<area shape="rect" coords="300,229,330,251" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/oise">
<area shape="rect" coords="334,166,355,193" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/nord">
<area shape="rect" coords="342,373,370,398" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/nievre">
<area shape="rect" coords="469,251,492,274" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/moselle">
<area shape="rect" coords="85,318,110,348" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/morbihan">
<area shape="rect" coords="417,245,440,274" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/meuse">
<area shape="rect" coords="447,265,467,291" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/meurthe-et-moselle">
<area shape="rect" coords="175,305,197,335" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/mayenne">
<area shape="rect" coords="12,522,70,587" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/martinique-44087">
<area shape="rect" coords="365,245,392,277" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/marne">
<area shape="rect" coords="144,235,167,268" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/manche">
<area shape="rect" coords="170,360,209,383" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/maine-et-loire">
<area shape="rect" coords="340,531,372,555" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/lozere">
<area shape="rect" coords="212,540,242,565" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/lot-et-garonne">
<area shape="rect" coords="265,521,290,548" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/lot">
<area shape="rect" coords="296,315,323,343" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/loiret">
<area shape="rect" coords="119,353,166,377" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/loire-atlantique">
<area shape="rect" coords="375,455,390,480" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/loire">
<area shape="rect" coords="242,336,282,358" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/loir-et-cher">
<area shape="rect" coords="219,365,255,385" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/indre-et-loire">
<area shape="rect" coords="152,561,187,590" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/landes">
<area shape="rect" coords="435,385,462,415" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/jura">
<area shape="rect" coords="429,476,459,511" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/isere">
<area shape="rect" coords="264,383,290,413" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/indre">
<area shape="rect" coords="135,301,164,328" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/ille-et-vilaine">
<area shape="rect" coords="339,594,367,616" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/herault">
<area shape="rect" coords="17,113,38,137" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/hauts-de-seine">
<area shape="rect" coords="498,312,523,332" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/haut-rhin">
<area shape="rect" coords="192,635,227,652" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/hautes-pyrenees">
<area shape="rect" coords="463,518,505,543" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/hautes-alpes">
<area shape="rect" coords="243,441,275,465" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/haute-vienne">
<area shape="rect" coords="467,429,504,456" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/haute-savoie">
<area shape="rect" coords="448,329,480,353" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/haute-saone">
<area shape="rect" coords="407,299,442,328" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/haute-marne">
<area shape="rect" coords="355,493,387,518" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/haute-loire">
<area shape="rect" coords="257,599,282,623" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/haute-garonne">
<area shape="rect" coords="508,668,545,698" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/haute-corse">
<area shape="rect" coords="10,601,72,663" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/guyane-44053">
<area shape="rect" coords="5,440,75,512" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/guadeloupe-44052">
<area shape="rect" coords="164,510,199,543" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/gironde">
<area shape="rect" coords="212,578,242,612" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/gers">
<area shape="rect" coords="377,560,404,592" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/gard">
<area shape="rect" coords="37,280,64,317" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/finistere">
<area shape="rect" coords="252,284,275,319" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/eure-et-loir">
<area shape="rect" coords="242,249,267,274" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/eure">
<area shape="rect" coords="30,159,75,191" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/essonne">
<area shape="rect" coords="420,519,442,546" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/drome">
<area shape="rect" coords="462,362,492,379" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/doubs">
<area shape="rect" coords="222,496,257,524" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/dordogne">
<area shape="rect" coords="185,403,208,431" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/deux-sevres">
<area shape="rect" coords="283,429,312,464" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/creuse">
<area shape="rect" coords="76,289,125,311" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/cotes-d-armor">
<area shape="rect" coords="391,346,425,373" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/cotes-d-or">
<area shape="rect" coords="505,712,540,738" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/corse-du-sud">
<area shape="rect" coords="273,478,303,500" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/correze">
<area shape="rect" coords="305,358,330,392" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/cher">
<area shape="rect" coords="154,445,197,470" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/charente-maritime">
<area shape="rect" coords="210,453,232,481" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/charente">
<area shape="rect" coords="310,493,342,515" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/cantal">
<area shape="rect" coords="187,248,214,273" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/calvados">
<area shape="rect" coords="410,595,449,616" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/bouches-du-rhone">
<area shape="rect" coords="505,263,530,295" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/bas-rhin">
<area shape="rect" coords="308,544,335,579" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/aveyron">
<area shape="rect" coords="295,621,335,651" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/aude">
<area shape="rect" coords="372,294,397,316" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/aube">
<area shape="rect" coords="382,213,417,238" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/ardennes">
<area shape="rect" coords="392,510,412,537" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/ardeche">
<area shape="rect" coords="504,558,544,580" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/alpes-maritimes">
<area shape="rect" coords="460,548,497,578" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/alpes-de-haute-provence">
<area shape="rect" coords="325,408,363,437" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/allier">
<area shape="rect" coords="340,210,372,241" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/aisne">
<area shape="rect" coords="422,430,450,455" href="http://www.diplomatie.gouv.fr/fr/services-et-formulaires/espace-etudiants/etudier-a-l-etranger/financer-ses-etudes/conseils-generaux/article/ain">
</map>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/etudes-superieures/financer-ses-etudes/article/programmes-proposes-par-les-13243). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
