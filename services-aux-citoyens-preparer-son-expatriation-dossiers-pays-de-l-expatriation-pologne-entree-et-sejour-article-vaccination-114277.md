# Vaccination

<p>Aucune vaccination n’est exigée à l’entrée dans le pays.</p>
<p>Pour des raisons médicales la mise à jour des vaccinations contre la diphtérie, la tuberculose, le tétanos et la poliomyélite est recommandée, la typhoïde et l’hépatite B (en cas de long séjour). Concernant l’hépatite A une recherche préalable d’anticorps sériques totaux est justifiée pour les personnes âgées de plus de 50 ans.</p>
<p><strong>Pour les enfants :</strong> les vaccinations recommandées en France par le ministère de la Santé doivent être à jour et en particulier : BCG et hépatite B dès le 1er mois (pour les longs séjours), rougeole dès l’âge de neuf mois, hépatite A possible à partir d’un an et la typhoïde à partir de cinq ans (pour les longs séjours).</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Notre rubrique <a href="services-aux-citoyens-preparer-son-expatriation-sante.md" class="spip_in">Santé</a>.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/entree-et-sejour/article/vaccination-114277). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
