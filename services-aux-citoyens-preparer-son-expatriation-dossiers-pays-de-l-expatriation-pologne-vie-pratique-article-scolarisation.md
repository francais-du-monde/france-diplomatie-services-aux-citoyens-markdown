# Scolarisation

<p>Pour toute information sur la scolarisation dans le système français à l’étranger, vous pouvez consulter notre thématique sur les <a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures.md" class="spip_in">Etudes supérieures</a> et la <a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais.md" class="spip_in">Scolarité en français</a> à l’adresse suivante.</p>
<p><strong>Vous y trouverez des renseignements sur : </strong></p>
<ul class="spip">
<li>les établissements français du primaire et du secondaire à l’étranger ;</li>
<li>les bourses scolaires et la prise en charge des frais de scolarité à l’étranger ;</li>
<li>les possibilités qui s’offrent à vous si votre enfant ne peut être scolarisé à l’étranger dans le système français (enseignement à distance par le CNED, programme français langue maternelle (FLAM), internats en France) ;</li>
<li>les épreuves du baccalauréat à l’étranger ;</li>
<li>les bourses d’études supérieures en France et à l’étranger ;</li>
<li>l’équivalence des diplômes.</li></ul>
<h4 class="spip">Les établissements scolaires français en Pologne</h4>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.aefe.fr/" class="spip_out" rel="external">Agence pour l’enseignement français à l’étranger</a></p>
<h4 class="spip">Enseignement supérieur</h4>
<p>La Pologne compte près de deux millions d’étudiants et plus de 400 établissements d’enseignement supérieur.</p>
<p>Le pays compte aujourd’hui 18 universités pluridisciplinaires, de taille importante, réparties sur tout le territoire, parfois avec des antennes délocalisées. Certaines sont de création récente (Zielona Gora, Rzeszow), résultat de la fusion de divers établissements. D’autres ont une ancienneté prestigieuse, comme l’Université Jagellonne de Cracovie : créée en 1364, elle fut, après Prague, la seconde université d’Europe Centrale.</p>
<p>Les académies sont des établissements publics spécialisés : agriculture, économie, médecine, etc. On doit ajouter à cette liste d’établissements les « collèges de formation des maîtres » (NKJO) et les très nombreuses écoles supérieures professionnelles, généralement privées. Ces établissements délivrent un diplôme au niveau « bac+3 » (<i>licencjat</i>) ; quelques-uns (en 3,5 ou 4 ans) un diplôme d’ingénieur (terme polonais : <i>inzynier</i>). Un système de passerelles permet une poursuite d’études au niveau supérieur, dans un autre établissement.</p>
<p>L’élève entrant dans l’enseignement supérieur est titulaire de la <i>matura</i>, l’équivalent de notre baccalauréat. Les élèves se portent candidats dans des établissements d’enseignement supérieur, en commençant logiquement par le secteur public. Ils ont, avant même les épreuves du bac, déposé des demandes d’inscription dans plusieurs universités, moyennant un droit (généralement) non remboursable d’une dizaine d’euros par dossier. Les étudiants sélectionnés accèdent aux « cours du jour » : cinq années d’études gratuites (absence de droits d’inscription) pour obtenir le <i>magister</i>.</p>
<p>L’un des principaux atouts du système universitaire polonais réside dans sa réactivité. Elle tient à l’autonomie des établissements qui sont à même de monter rapidement des formations nouvelles.</p>
<p>Les étudiants polonais ont généralement un excellent niveau. L’enseignement secondaire leur donne une culture générale qui trouve à s’exprimer remarquablement dans notre système universitaire. En outre, payant souvent personnellement leurs études, ils ont une forte incitation à la réussite.</p>
<p>La francophonie du pays n’est pas très développée, mais il faut noter le développement de l’enseignement bilingue français-polonais dans certains collèges et lycées. Actuellement dispensé dans 29 sections appartenant à 22 établissements, il est en progression constante et est de nature à préparer de futurs bons étudiants pour les formations supérieures francophones en Pologne ou pour les établissements français.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/vie-pratique/article/scolarisation). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
