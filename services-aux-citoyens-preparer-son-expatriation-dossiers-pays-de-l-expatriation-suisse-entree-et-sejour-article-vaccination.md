# Vaccination

<p>Aucune vaccination n’est exigée pour l’entrée en Suisse.</p>
<p>En cas d’établissement en Suisse, vous êtes libre de vous faire vacciner ou de faire vacciner votre enfant. Seuls quelques cantons (FR, GE, NE) ont rendu obligatoires les vaccinations contre la diphtérie.</p>
<p>Plus d’information :</p>
<ul class="spip">
<li><a href="http://www.bag.admin.ch/themen/medizin/00682/00685/index.html?lang=fr" class="spip_out" rel="external">Page dédiée à la vaccination sur le site de l’Office fédéral de la Santé publique</a></li>
<li>Notre article thématique : <a href="services-aux-citoyens-preparer-son-expatriation-sante.md" class="spip_in">Santé</a></li></ul>
<p><i>Mise à jour : février 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/entree-et-sejour/article/vaccination). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
