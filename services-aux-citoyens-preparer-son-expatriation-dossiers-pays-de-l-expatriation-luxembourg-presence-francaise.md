# Présence française

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/presence-francaise/#sommaire_1">Ambassade de France</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/presence-francaise/#sommaire_2">Autorités françaises dans le pays</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/presence-francaise/#sommaire_3">Associations dans le pays</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Ambassade de France</h3>
<p>Consultez notre article <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/article/annuaire-des-representations-francaises-a-l-etranger" class="spip_in">Annuaire des représentations françaises à l’étranger</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Autorités françaises dans le pays</h3>
<h4 class="spip">Réseau français de coopération et d’action culturelle</h4>
<p>Les coordonnées des services du réseau de coopération et d’action culturelle français au Luxembourg ainsi que la description de l’activité de ces services se trouvent dans la rubrique culture du site internet de l’Ambassade ou sur le site internet du réseau culturel : <a href="http://www.latitudefrance.org/AnnuaireCarto.html" class="spip_out" rel="external">Latitude France.</a></p>
<h4 class="spip">Economie : réseau mondial</h4>
<p>Les services économiques sont également présents. Ils sont une émanation de la Direction générale du Trésor et ont pour missions principales l’analyse macroéconomique et l’animation des relations économiques bilatérales entre la France et les pays où ils sont implantés.</p>
<p>Ambassade de France<br class="manualbr">Service économique régional<br class="manualbr">38 rue de la Loi<br class="manualbr">B – 1040 Bruxelles<br class="manualbr">Tél. : 0032 2404 30 55, <br class="manualbr">Fax : 00 32 2 40430 90<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/presence-francaise/#bruxelles#mc#missio.eco.org#" title="bruxelles..åt..missio.eco.org" onclick="location.href=mc_lancerlien('bruxelles','missio.eco.org'); return false;" class="spip_mail">Courriel</a> ;</p>
<h4 class="spip">Vos élus à l’Assemblée des Français de l’étranger</h4>
<p>Pour toute information sur l’Assemblée des Français de l’étranger et pour connaître les conseillers et les Sénateurs qui représentent les Français établis hors de France dans votre circonscription, vous pouvez consulter les sites Internet suivants :</p>
<ul class="spip">
<li><a href="http://www.assemblee-afe.fr/" class="spip_out" rel="external">Assemblée des Français de l’étranger (AFE)</a></li>
<li>Sénat au service des Français de l’étranger : <a href="http://www.senat.fr/expatries/dossiers_pays/luxembourg.html" class="spip_out" rel="external">dossier spécifique au Luxembourg sur le site du Sénat</a>.</li></ul>
<p>Le conseiller AFE pour le Luxembourg est Monsieur Pierre GIRAULT.</p>
<h4 class="spip">Députés des Français de l’étranger</h4>
<p>La révision constitutionnelle du 23 juillet 2008, publiée au Journal officiel du 19 avril 2011, introduit la représentation à l’Assemblée nationale des Français établis hors de France. La création de onze circonscriptions législatives à l’étranger permet aux Français expatriés d’élire leur député à l’Assemblée nationale à partir du scrutin de 2012.</p>
<p>Le député pour la 4ème circonscription (Belgique/Luxembourg/Pays-Bas) est Monsieur Philip CORDERY.</p>
<p>Pour plus d’information : <a href="http://www.assemblee-nationale.fr/" class="spip_out" rel="external">Assemblée nationale</a></p>
<h3 class="spip"><a id="sommaire_3"></a>Associations dans le pays</h3>
<p><a href="http://www.adfe.org/luxembourg" class="spip_out" rel="external">Français du Monde - ADFE</a> (ADFE-FdM)<br class="autobr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/presence-francaise/#adfe_Lux#mc#hotmail.com#" title="adfe_Lux..åt..hotmail.com" onclick="location.href=mc_lancerlien('adfe_Lux','hotmail.com'); return false;" class="spip_mail">Courriel</a></p>
<p><a href="http://www.ufe.org/fr/representations-par-pays/95_luxembourg.html" class="spip_out" rel="external">Union des Français de l’étranger</a> (UFE)<br class="manualbr">Téléphone : [352] 4991 5023 <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/presence-francaise/#krebse57z-ufe#mc#yahoo.fr#" title="krebse57z-ufe..åt..yahoo.fr" onclick="location.href=mc_lancerlien('krebse57z-ufe','yahoo.fr'); return false;" class="spip_mail">Courriel</a></p>
<p><a href="http://www.luxembourgaccueil.com/" class="spip_out" rel="external">Luxembourg Accueil Information</a><br class="manualbr">Téléphone : [352] 24 17 17<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/presence-francaise/#luxaccueil#mc#yahoo.fr#" title="luxaccueil..åt..yahoo.fr" onclick="location.href=mc_lancerlien('luxaccueil','yahoo.fr'); return false;" class="spip_mail">Courriel</a></p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="services-aux-citoyens-preparer-son-expatriation-en-savoir-plus-article-associations-des-francais-de-l-etranger.md" class="spip_in">Notre article dédié aux associations</a></p>
<p><i>Mise à jour : décembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-luxembourg-presence-francaise-article-presence-francaise.md" title="Présence française">Présence française</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/presence-francaise/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
