# Recherche d’emploi

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/norvege/emploi-stage/article/recherche-d-emploi#sommaire_1">Outils pour la recherche d’emploi</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/norvege/emploi-stage/article/recherche-d-emploi#sommaire_2">Organismes pour la recherche d’emploi</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Outils pour la recherche d’emploi</h3>
<p><strong>Sites internet</strong></p>
<ul class="spip">
<li>Site de l’<a href="http://www.nav.no/" class="spip_out" rel="external">administration de l’emploi, et de la sécurité sociale</a> ;</li>
<li><a href="http://ec.europa.eu/eures" class="spip_out" rel="external">Ec.europa.eu</a> : site européen sur la mobilité dans la recherche d’un emploi. Le réseau EURES, présent sur toute l’Europe, et donc également en Norvège pourra être une source locale intéressante d’informations sur beaucoup de secteurs ;</li>
<li>Site du <a href="http://www.regjeringen.no/" class="spip_out" rel="external">gouvernement norvégien</a> ;</li>
<li>Site officiel des <a href="http://www.norway.no/" class="spip_out" rel="external">services publics norvégiens</a> ;</li>
<li>Site de l’<a href="http://www.ambafrance-no.org/" class="spip_out" rel="external">ambassade de France à Oslo</a> ;</li>
<li>Site de l’<a href="http://www.norvege.no/" class="spip_out" rel="external">ambassade de Norvège en France</a>. Sur ce site, la rubrique intitulée <strong>Travailler en Norvège</strong> constitue un véritable mode d’emploi à l’attention des candidats à l’installation en Norvège ;</li>
<li><a href="http://www.norvege-fr.com/" class="spip_out" rel="external">Norvege-fr.com</a> : site avec des informations et des forums sur la Norvège ;</li>
<li>Site du <a href="http://www.finn.no/" class="spip_out" rel="external">journal Aftenposten</a>, avec de nombreuses annonces :</li>
<li>Site de l’<a href="http://www.skatteetaten.no/" class="spip_out" rel="external">administration fiscale norvégienne</a> ;</li>
<li><a href="http://www.bedin.no/" class="spip_out" rel="external">Bedin.no</a> : site avec des informations concernant la création d’entreprises</li>
<li><a href="http://www.stepstone.no/" class="spip_out" rel="external">Stepstone.no</a></li>
<li><a href="http://www.jobline.no/" class="spip_out" rel="external">Jobline.no</a></li>
<li><a href="http://web.tu.no/jobb/" class="spip_out" rel="external">Web.tu.no/jobb</a></li></ul>
<p><strong>Réseaux</strong></p>
<p>Quatre voies sont peut-être à suivre dans votre recherche d’emploi :</p>
<ul class="spip">
<li><strong>sites Internet</strong> : site officiel du <a href="http://www.nav.no/" class="spip_out" rel="external">service public de l’emploi</a>, site du <a href="http://www.finn.no/" class="spip_out" rel="external">journal Aftenposten</a> (avec de nombreux postes vacants, ainsi que des logements etc), et tous les sites des entreprises. <strong>Important</strong> : environ 50 % du marché du travail est visible par ce canal d’informations.</li>
<li><strong>agences intérimaires</strong> : elles tiennent une part importante du marché du travail et peuvent également être une porte d’entrée sur des CDI.</li>
<li>votre <strong>réseau</strong>… qu’il vous faudra peut être créer !</li>
<li><strong>candidatures spontanées</strong> : peut être votre meilleure façon de décrocher un emploi. Ne pas hésiter à contacter directement les entreprises qui vous intéressent et votre suivi sera à ce niveau important.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Organismes pour la recherche d’emploi</h3>
<p><strong>Centre de services de la NAV (Norwegian Labour and Welfare Administration)</strong></p>
<p>Vous pouvez contacter la NAV pour obtenir des informations sur les emplois vacants. Ce service permet également d’envoyer des candidatures, mais uniquement en norvégien.</p>
<p>Avant d’appeler, réfléchissez bien au type d’emploi que vous souhaitez exercer et à la région de Norvège dans laquelle vous souhaitez vivre et travailler.</p>
<p>Vous pouvez également consulter les offres d’emploi sur le site Internet de la <a href="http://www.nav.no/" class="spip_out" rel="external">NAV</a> (en norvégien et en anglais).</p>
<p><strong>Comité consulaire pour l’emploi et la formation professionnelle (CCPEFP) d’Oslo</strong></p>
<p>En partenariat avec le ministère des Affaires étrangères et afin de répondre aux besoins en recrutement des entreprises en Norvège, la <a href="http://www.ccfn.no/" class="spip_out" rel="external">Chambre de commerce franco-norvégienne (CCFN)</a> a développé ses services en lançant un Service emploi. Celui-ci permet de mettre en relation les entreprises ayant des offres d’emploi en Norvège avec des demandeurs d’emploi et/ou de stages, français ou francophones.</p>
<p><strong>Agences de recrutement</strong></p>
<p>Il est possible de s’inscrire auprès d’une agence de recrutement. Celles-ci recrutent en priorité des demandeurs d’emplois intérimaires et à durée déterminée. Plusieurs d’entre elles se sont spécialisées dans certaines professions et certains secteurs. Vous les trouverez dans les pages jaunes de l’annuaire <a href="http://www.gulesider.no/" class="spip_out" rel="external">Gulesider.no</a> .</p>
<p><i>Source : <a href="http://ec.europa.eu/eures/" class="spip_out" rel="external">EURES</a></i></p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/norvege/emploi-stage/article/recherche-d-emploi). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
