# Vie pratique

<h2 class="rub23469">Logement</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/paraguay/vie-pratique/#sommaire_1">Où se loger ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/paraguay/vie-pratique/#sommaire_2">Conditions de location</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/paraguay/vie-pratique/#sommaire_3">Hôtels</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/paraguay/vie-pratique/#sommaire_4">Electricité</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/paraguay/vie-pratique/#sommaire_5">Electroménager</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Où se loger ?</h3>
<p>Les quartiers résidentiels sont dispersés et leur situation peut varier. On peut citer, dans un périmètre de 5 à 6 km autour de l’ancien centre-ville, à proximité du fleuve, les principaux quartiers de Villa Mora, Banco Central et Los Laureles. Un nouveau centre se développe : quartier Manora, Carmelitas, Madame Lynch, Mburucuya et Yena Sati, situéà mi-chemin entre l’aéroport Silvio Pettirossi et le centre-ville Un nouveau centre se développant aussi vers le Shopping del Sol (Centre Commercial) et l’Avenue Santa Teresa.</p>
<p><strong>Prix moyen des loyers en quartier résidentiel</strong></p>
<ul class="spip">
<li>3 pièces : 1800 à 2500 Dollars</li>
<li>5 pièces : 2500 à 3500 Dollars</li>
<li>Villa : 2500 à 5000 Dollars</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Conditions de location</h3>
<p>Il est assez aisé de trouver un logement avec un bon niveau de confort à Assomption.</p>
<p>Les loyers sont en règle générale payables en dollars et les baux signés pour un an, avec une caution d’un mois.</p>
<p>Les frais d’agence sont de l’ordre de 10 à 30% du loyer mensuel.</p>
<h3 class="spip"><a id="sommaire_3"></a>Hôtels</h3>
<p><strong>Prix moyen d’une chambre d’hôtel, petit déjeuner inclus </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Environ 320 000 PYG.</p>
<h3 class="spip"><a id="sommaire_4"></a>Electricité</h3>
<p>L’alimentation électrique se fait sur du courant alternatif 220 V - 50 Hz, avec des prises aux normes françaises (sauf prise de terre) et américaines (adaptateurs disponibles localement).</p>
<p>Pour certains équipements sensibles, tels que les ordinateurs, il est impératif d’acquérir un stabilisateur de tension.</p>
<h3 class="spip"><a id="sommaire_5"></a>Electroménager</h3>
<p>S’agissant du matériel électroménager, tout l’équipement est disponible sur place.</p>
<p>Les standards acceptés pour l’équipement vidéo sont le système NTSC et les standards brésiliens, ainsi que les systèmes PAL-M et PAL-N.</p>
<p><i>Mise à jour : juin 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-paraguay-vie-pratique-article-loisirs-et-culture.md" title="Loisirs et culture">Loisirs et culture</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-paraguay-vie-pratique-article-communications.md" title="Communications">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-paraguay-vie-pratique-article-transports.md" title="Transports">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-paraguay-vie-pratique-article-cout-de-la-vie-113507.md" title="Coût de la vie">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-paraguay-vie-pratique-article-scolarisation.md" title="Scolarisation">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-paraguay-vie-pratique-article-sante.md" title="Santé">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-paraguay-vie-pratique-article-logement-113504.md" title="Logement">Logement</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/paraguay/vie-pratique/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
