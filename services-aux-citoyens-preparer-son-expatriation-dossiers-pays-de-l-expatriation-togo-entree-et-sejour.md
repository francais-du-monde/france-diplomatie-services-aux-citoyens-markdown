# Entrée et séjour

<h2 class="rub23477">Passeport, visa, permis de travail</h2>
<p>Quel que soit le motif de votre séjour, renseignez-vous auprès du consulat du Togo à Paris qui vous informera sur la règlementation en matière d’entrée et de séjour au Togo, règlementation que vous devrez impérativement respecter.</p>
<p><strong>En aucun cas, vous n’êtes autorisé à travailler au Togo sans permis adéquat. </strong></p>
<p>Le Consulat de France au Togo n’est pas l’organisme compétent pour répondre à vos demandes d’information concernant votre séjour au Togo.</p>
<p>Pour toute information relative aux conditions de séjour au Togo, il est vivement conseillé de prendre l’attache du Consulat du Togo en France.</p>
<p>Une convention judiciaire Franco-togolaise du 23 mars 1976 publiée au Journal Officiel du 25 février 1982 établit les bases d’une réciprocité en matière civile et judiciaire entre les deux pays.</p>
<p>Une convention Franco-togolaise du 13 juin 1996 relative à la circulation et au séjour des personnes (loi 98.237 du 1er avril 1998) a été ratifiée par le Togo le 22 octobre 2001.</p>
<p>Concernant les formalités d’entrée et de séjour, il convient d’être muni d’un passeport en cours de validité revêtu d’un visa d’entrée de court séjour (de 1 à 90 jours) délivré par les représentations diplomatiques et consulaires de la République togolaise, ou, en l’absence de représentation, par les ambassades ou consulats français (dans ce dernier cas, le visa est limité à 48 heures).</p>
<p>Exceptionnellement, un visa de sept jours peut être obtenu à l’aéroport de Lomé contre paiement d’une somme de 10 000 F CFA (15,24€).</p>
<p>Les visas d’entrée délivrés pour un séjour de 48 heures ou de sept jours peuvent être prorogés (jusqu’à un mois) directement à l’arrivée à Lomé auprès de la gendarmerie nationale - service des passeports et des étrangers - sur présentation des justificatifs de séjour, de deux photographies d’identité et, le cas échéant, du règlement d’une taxe de 10 000 F CFA (15,24€).</p>
<p>Pour un séjour supérieur à 90 jours, un visa long séjour, débouchant sur l’attribution d’une carte de séjour pour étranger, est à solliciter au service des passeports et des étrangers.</p>
<p>Un certificat de vaccination contre la fièvre jaune est exigé de tous les voyageurs à l’entrée du pays.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.consulatogo.org/" class="spip_out" rel="external">Consulat du Togo à Paris</a></p>
<p><i>Mise à jour : juin 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-togo-entree-et-sejour-article-animaux-domestiques.md" title="Animaux domestiques">Animaux domestiques</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-togo-entree-et-sejour-article-vaccination-113542.md" title="Vaccination">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-togo-entree-et-sejour-article-demenagement-113541.md" title="Déménagement">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-togo-entree-et-sejour-article-passeport-visa-permis-de-travail-113540.md" title="Passeport, visa, permis de travail">Passeport, visa, permis de travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/entree-et-sejour/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
