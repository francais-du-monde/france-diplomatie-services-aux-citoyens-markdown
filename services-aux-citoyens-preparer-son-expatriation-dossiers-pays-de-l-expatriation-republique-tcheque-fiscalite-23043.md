# Fiscalité

<h2 class="rub23043">Fiscalité du pays</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/fiscalite-23043/#sommaire_1">La loi 586/1992 assujettit les résidents et non-résidents</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/fiscalite-23043/#sommaire_2">L’impôt sur le revenu est réglé sous forme de retenue à la source</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/fiscalite-23043/#sommaire_3">Coordonnées des centres d’information fiscale</a></li></ul>
<p>Les personnes physiques doivent payer l’impôt sur le revenu sur tout ou partie de leurs revenus en fonction de leur statut.</p>
<h3 class="spip"><a id="sommaire_1"></a>La loi 586/1992 assujettit les résidents et non-résidents</h3>
<p>Les personnes physiques ayant le statut de non-résidents doivent payer l’impôt sur le revenu uniquement sur la partie d’origine tchèque de leurs revenus. Sont considérés comme revenus de source tchèque, les revenus d’une activité dépendante exercée sur le territoire tchèque. <strong>Si le salaire constitue leur seul revenu de source tchèque, les non-résidents seront imposés par retenue à la source pratiquée par l’employeur et donc dispensés de déclaration.</strong></p>
<p>Les personnes physiques ayant le statut de résidents sont soumises à l’impôt sur le revenu sur l’ensemble de leurs revenus mondiaux. Sont considérés comme résidents les individus qui ont leur domicile permanent en République tchèque ainsi que ceux qui y vivent plus de 183 jours par an. Les conventions internationales limitent toutefois cette interprétation dans le cas notamment où le centre des intérêts vitaux du contribuable reste à l’étranger.</p>
<p>Les experts détachés en République tchèque par un employeur français pour une mission de plus de 183 jours sont soumis à l’impôt sur la base de leurs revenus de source tchèque.</p>
<p>Les entrepreneurs individuels inscrits au registre du commerce sont assujettis à l’impôt sur le revenu des personnes physiques.</p>
<h3 class="spip"><a id="sommaire_2"></a>L’impôt sur le revenu est réglé sous forme de retenue à la source</h3>
<p>Pour les salaires, cette opération mensuelle est effectuée directement par l’employeur qui établit une déclaration annuelle des salaires et procède aux régularisations éventuelles.</p>
<p>Les autres revenus (y compris les salaires obtenus par des salariés dans le cadre d’un détachement en République tchèque) doivent faire l’objet d’une déclaration et d’un paiement pour le 31 mars de l’année suivante. Si le contribuable fait appel à l’assistance d’un conseil fiscal mandaté à cet effet, la date de dépôt est prolongée au 30 juin.</p>
<p>Après le dépôt de la première déclaration fiscale, un système d’acompte d’impôt similaire à celui des personnes morales se met en place, dont la fréquence varie avec le montant de l’impôt à payer.</p>
<p>Les acomptes provisionnels à verser au cours d’un exercice se calculent en fonction du montant de l’impôt dû au titre de l’exercice précédent, à savoir le montant de l’impôt indiqué dans la dernière déclaration déposée.</p>
<p>Les pénalités sont identiques au régime de l’impôt sur le revenu des sociétés.</p>
<p>L’impôt peut être payé :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  en espèces auprès du centre des impôts territorialement compétent. Un bordereau-avis de versement sera alors remis au contribuable.<br class="manualbr">- par mandat postal. L’impôt est réputé payé au jour de la remise au contribuable par la poste du bordereau-avis de versement.<br class="manualbr">- par virement bancaire sur le compte du centre des impôts territorialement compétent détenu auprès de la banque nationale tchèque. Le jour de paiement de l’impôt correspond au jour où le compte bancaire du contribuable est débité. </p>
<p>La base de calcul pour l’impôt est désormais le salaire dit « super-brut », incluant les cotisations sociales (parts patronale et salariale).</p>
<p><strong>On notera que les notions de foyer fiscal et de quotient familial sont absentes du droit fiscal tchèque- en pratique, les époux peuvent déclarer les revenus et payer les impôts individuellement ou ensemble (il y a des abattements par conjoint/-e et par enfant à charge).</strong></p>
<h3 class="spip"><a id="sommaire_3"></a>Coordonnées des centres d’information fiscale</h3>
<p>L’administration fiscale comprend le ministère des Finances, les autorités financières régionales et les autorités financières locales.</p>
<p><a href="http://www.mfcr.cz/" class="spip_out" rel="external">Ministère des Finances</a><br class="manualbr">Letenska 15<br class="manualbr">118 10 Praha 1<br class="manualbr">Tél : (420) 257 04 11 11<br class="manualbr">Fax : (420) 257 04 27 88<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/fiscalite-23043/#Podatelna#mc#mfcr.cz#" title="Podatelna..åt..mfcr.cz" onclick="location.href=mc_lancerlien('Podatelna','mfcr.cz'); return false;" class="spip_mail">Courriel</a></p>
<p><i>Mise à jour : janvier 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-tcheque-fiscalite-23043-article-convention-fiscale-111345.md" title="Convention fiscale">Convention fiscale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-republique-tcheque-fiscalite-23043-article-fiscalite-du-pays-111344.md" title="Fiscalité du pays">Fiscalité du pays</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/fiscalite-23043/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
