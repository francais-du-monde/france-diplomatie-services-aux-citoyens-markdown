# Vie pratique

<h2 class="rub22944">Logement</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/vie-pratique/#sommaire_1">Où se loger ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/vie-pratique/#sommaire_2">Conditions de location</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/vie-pratique/#sommaire_3">Hôtels</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/vie-pratique/#sommaire_4">Electricité</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/vie-pratique/#sommaire_5">Electroménager</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Où se loger ?</h3>
<p>De nombreuses agences immobilières proposent leurs services.</p>
<h3 class="spip"><a id="sommaire_2"></a>Conditions de location</h3>
<p>L’emplacement est le critère principal pour fixer le montant du loyer.</p>
<p>Dans les quartiers résidentiels, les prix des meublés et ceux des appartements loués vides ne sont pas très différents. Le meublé se trouve plutôt dans l’immobilier ancien. Les logements de construction récente sont bien souvent aussi chers que les meublés.</p>
<p>Les loyers sont généralement payables en euros et versés en liquide ou par virement bancaire, par avance. Un mois de loyer est souvent exigé en caution.</p>
<p>La commission des agences immobilières est d’un mois de loyer.</p>
<p>Le propriétaire est généralement déjà destinataire des factures relatives aux charges (eau, électricité, gaz, frais de voierie, redevance TV…), qu’il refacture ensuite au locataire. En Croatie, ces factures restent au nom du propriétaire et non à celui du locataire.</p>
<h3 class="spip"><a id="sommaire_3"></a>Hôtels</h3>
<p>La Croatie est un pays touristique, qui dispose d’un réseau étendu d’hôtels et d’appartements pour court séjour.</p>
<ul class="spip">
<li><a href="http://croatia.hr/fr-FR/Representation-nationale/France" class="spip_out" rel="external">Office de tourisme de Croatie</a></li>
<li><a href="http://www.zagreb-touristinfo.hr/?l=f" class="spip_out" rel="external">Office de tourisme de Zagreb</a></li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Electricité</h3>
<p>Le voltage est du 220 V et les prises électriques sont les mêmes qu’en France.</p>
<h3 class="spip"><a id="sommaire_5"></a>Electroménager</h3>
<p>Les centres commerciaux et grands magasins d’ameublement et de bricolage offrent de nombreux magasins susceptibles de répondre à vos besoins en matière d’équipements ménagers, à des prix similaires à ceux du marché français.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-croatie-vie-pratique-article-pour-en-savoir-plus-110902.md" title="Pour en savoir plus">Pour en savoir plus</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-croatie-vie-pratique-article-loisirs-et-culture.md" title="Loisirs et culture">Loisirs et culture</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-croatie-vie-pratique-article-communications-110900.md" title="Communications">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-croatie-vie-pratique-article-transports-110899.md" title="Transports">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-croatie-vie-pratique-article-cout-de-la-vie-110898.md" title="Coût de la vie">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-croatie-vie-pratique-article-scolarisation-110895.md" title="Scolarisation">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-croatie-vie-pratique-article-sante.md" title="Santé">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-croatie-vie-pratique-article-logement-110897.md" title="Logement">Logement</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/vie-pratique/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
