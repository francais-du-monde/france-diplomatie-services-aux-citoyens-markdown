# Fiscalité du pays

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/fiscalite/article/fiscalite-du-pays#sommaire_1">Impôt sur le revenu</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/fiscalite/article/fiscalite-du-pays#sommaire_2">Impôt sur les sociétés</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/fiscalite/article/fiscalite-du-pays#sommaire_3">TVA</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/fiscalite/article/fiscalite-du-pays#sommaire_4">Année fiscale</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/fiscalite/article/fiscalite-du-pays#sommaire_5">Barème de l’impôt</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/fiscalite/article/fiscalite-du-pays#sommaire_6">Quitus fiscal</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/fiscalite/article/fiscalite-du-pays#sommaire_7">Pour en savoir plus</a></li></ul>
<p>Depuis 2008, les nouveaux immigrants et les résidents de retour (Israéliens qui ont résidé plus de 10 ans à l’étranger) bénéficient d’allègements d’impôts : exemption d’impôt sur une période de 10 ans.</p>
<h3 class="spip"><a id="sommaire_1"></a>Impôt sur le revenu</h3>
<h4 class="spip">Principe de personnalité</h4>
<p>Depuis une réforme de 2003, le principe est que la totalité des revenus d’un résident en Israël est soumise à l’impôt israélien, quelle qu’en soit la source, y compris si celle-ci se trouve dans un autre pays. Toutefois, cette disposition ne remet pas en cause les modes d’imposition spécifiques à certains revenus prévus dans la <a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-israel-fiscalite-article-convention-fiscale-110467.md" class="spip_in">Convention fiscale</a> que la France a signée avec l’Israël.</p>
<h4 class="spip">Revenu imposable</h4>
<p>Les revenus imposables en Israël comprennent aussi bien les revenus du travail (salaires, revenus des travailleurs indépendants, retraites complémentaires) que les revenus du capital (revenus de capitaux mobiliers tels que dividendes et intérêts, revenus de capitaux immobiliers, royalties, etc.).</p>
<p>Pour les salariés, l’impôt est prélevé mensuellement à la source sur la base des salaires bruts versés par l’employeur. C’est à ce dernier qu’incombe la charge du versement de l’impôt tous les 15 du mois ou tous les deux mois en fonction de la taille de la société.</p>
<p>Les travailleurs indépendants et les sociétés à responsabilité limitée déclarent quant à eux annuellement leurs revenus. Ils doivent ensuite payer l’impôt en avance sur la base des revenus perçus l’année précédente.</p>
<p>Les revenus du capital font l’objet d’une imposition séparée et soumis à des taux spécifiques, ils ne sont donc pas intégrés dans l’impôt sur le revenu.</p>
<h4 class="spip">Exemptions, abattements, charges déductibles</h4>
<p>Certaines catégories de revenus échappent à l’impôt du fait de leur caractère social (ces exemptions sont soumises à conditions) :</p>
<ul class="spip">
<li>revenus des aveugles et handicapés ;</li>
<li>allocations versées par le ministère de la défense ;</li>
<li>allocations d’invalidité et pensions de vieillesse versées par la sécurité sociale israélienne ;</li>
<li>intérêts perçus sur certains fonds de pension.</li></ul>
<p>En matière d’assurance vie et de fonds de prévoyance et de secours, la réduction d’impôt se fait à hauteur de 16% du montant déterminé par la loi qui est à ce jour de 110 000 ILS.</p>
<h4 class="spip">Prime pour l’emploi</h4>
<p>Un dispositif comparable à la prime pour l’emploi française appelé « impôt négatif » existe depuis 2007. Il concerne les salariés dont les revenus se situent entre 1730 et 5000 shekels par mois et leur octroie une prime de 275 à 400 shekels.</p>
<h4 class="spip">Plafonnement des cotisations sociales</h4>
<p>Les niveaux de cotisations sociales sont calculés en fonction du salaire minimum, cependant, pour les très hauts revenus, ces cotisations sont plafonnées à cinq fois le salaire moyen.</p>
<h4 class="spip">Prise en compte de la situation personnelle</h4>
<p>Il n’existe pas de notion de foyer fiscal en Israël, l’imposition individuelle est la règle (une exception existe si la source de revenus des deux époux est interdépendante). Un contribuable se voit cependant attribuer un certain nombre de points en fonction de sa situation personnelle : enfants, nouvel arrivant, etc. Ces points ouvrent droit à un crédit d’impôt déductible, la valeur du point est révisée annuellement.</p>
<h4 class="spip">Populations prioritaires</h4>
<p>Certaines populations, considérées comme prioritaires, se voient attribuer un crédit d’impôt déductible. Ce dispositif concerne notamment les nouveaux immigrants et les résidents de localités considérées comme prioritaires par l’Etat (Néguev, Galilée…).</p>
<h4 class="spip">Déductions pour dons</h4>
<p>Des crédits d’impôts pour don à des organismes à but non lucratif sont prévus si leur montant est supérieur ou égal à 400 NIS.</p>
<h4 class="spip">Revenus du capital</h4>
<p>Les intérêts provenant de produits financiers indexés à l’indice des prix à la consommation ou à des devises étrangères sont imposés à 20%, (de même que les dividendes perçus par une société israélienne ou étrangère). Les intérêts non indexés sont, quant à eux, imposés à 15%.</p>
<p>Les plus-values réalisées sur les valeurs mobilières sont imposées au taux uniforme de 20%, (sauf celles réalisées sur les valeurs mobilières non indexées : 15%). Pour les actionnaires importants (au moins 25% des parts d’une société), ces taux peuvent être majorés, de même que ceux appliqués aux dividendes.</p>
<p>Les plus-values sur la vente de biens immobiliers sont soumises à un taux d’imposition variant de 20 à 25 % selon la situation du contribuable (est exonérée la vente d’un appartement résidentiel unique ou possédé depuis plus de quatre années).</p>
<p>Les revenus perçus sur la location résidentielle sont soumis à un taux de 10% au delà d’un certain seuil.</p>
<p>L’achat de locaux d’habitation est exonéré de taxe si le prix est inférieur à 92 500 ILS. Le taux d’imposition du prix d’achat est de 3,5% entre 92 500 et 1300 000 ILS et de 5% au-delà.</p>
<h4 class="spip">La taxe communale ou <i>arnona</i></h4>
<p>Chaque propriétaire doit s’acquitter d’une taxe municipale imposée sur les biens immobiliers. Cette taxe est fonction de la taille, de l’emplacement et de l’utilisation des biens.</p>
<h3 class="spip"><a id="sommaire_2"></a>Impôt sur les sociétés</h3>
<p>En Israël, il existe peu de différences entre les régimes fiscaux applicables aux diverses formes et structures sociales des sociétés. La taxation des entreprises se base dans un premier temps sur l’imposition des revenus à l’échelle de l’entreprise, puis au niveau des actionnaires lorsque les dividendes sont distribués.</p>
<h4 class="spip">Déclaration et prélèvement</h4>
<p>Le régime de prélèvement de l’impôt israélien sur les sociétés est comparable à celui pratiqué en France : les entreprises effectuent une déclaration sur la base des revenus de l’année n-1, puis versent une avance d’impôt (montant fixe ou pourcentage du chiffre d’affaire déclaré pour l’année n-1) qui sera régularisée ensuite.</p>
<h4 class="spip">Imputation des pertes et des charges</h4>
<p>Les pertes commerciales peuvent être imputées sur l’ensemble des sources de revenus, y compris les plus-values en capital, et peuvent être reportées sans limitation de durée ("carry forward").</p>
<p>Les pertes en capital ne peuvent généralement être déduites que des plus values en capital et peuvent être reportées sur sept ans.</p>
<p>Comme en France, les déductions pour frais sont souvent limitées pour éviter les abus (cadeaux, dons, voyages à l’étranger…).</p>
<h4 class="spip">Amortissements</h4>
<p>Les amortissements des immobilisations peuvent être déduits du revenu imposable des entreprises à un taux fixé par catégorie de matériel.</p>
<h4 class="spip">Taux d’imposition</h4>
<p>Le taux standard d’imposition des sociétés est de 25%. Des dérogations existent cependant, notamment pour les entreprises agréées détenues en partie par des résidents étrangers dont le taux d’imposition peut être réduit jusqu’à 0% en fonction des situations. D’autre part, les dividendes versés par une filiale israélienne à une société mère israélienne sont exemptés de taxes.</p>
<h3 class="spip"><a id="sommaire_3"></a>TVA</h3>
<p>La TVA s’applique à hauteur de 16 % sur les transactions commerciales conduites en Israël et sur les importations (en plus des droits de douane).</p>
<p>Les exportations sont généralement imposées au taux zéro, de même que la vente de services et de biens immatériels à des non résidents, les services touristiques, les transports de chargements à destination et à partir d’Israël, les ventes de biens et services à la zone de libre échange d’Eilat et la vente de fruits et légumes frais.</p>
<p>Les biens et services exemptés de TVA comprennent notamment les loyers résidentiels de moins de 10 ans et les loyers sur des terrains non bâtis. Les établissements financiers et organismes à but non lucratif, qui sont soumis à une taxe spécifique, échappent également à la TVA.</p>
<h3 class="spip"><a id="sommaire_4"></a>Année fiscale</h3>
<p>Du 1e janvier au 31 décembre.</p>
<h3 class="spip"><a id="sommaire_5"></a>Barème de l’impôt</h3>
<p>Le taux d’imposition sur les revenus du travail varie de 10 à 46 % en fonction des tranches de salaire brut. Le principe est le prélèvement mensuel à la source de l’impôt, net des exemptions et déductions (qui sont nombreuses : 46 % des contribuables seraient non imposables, les 7 % de salariés de la tranche haute assurent plus de la moitié du produit de l’impôt).</p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Revenus mensuels</td>
<td>Taux d’imposition appliqué  (incluant les prélèvements sociaux)</td></tr>
<tr class="row_even even">
<td>de 1 à 4590</td>
<td>10 %</td></tr>
<tr class="row_odd odd">
<td>de 4591 à 8160</td>
<td>15 %</td></tr>
<tr class="row_even even">
<td>de 8161 à 12 250</td>
<td>23 %</td></tr>
<tr class="row_odd odd">
<td>de 12 251 à 17 600</td>
<td>30 %</td></tr>
<tr class="row_even even">
<td>de 17 601 à 37 890</td>
<td>34 %</td></tr>
<tr class="row_odd odd">
<td>à partir de 37891</td>
<td>46 %</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_6"></a>Quitus fiscal</h3>
<p>Il n’existe pas de bordereau officiel de situation fiscale équivalent au quitus. Dès lors que le demandeur est en règle et à jour du paiement de ses impôts vis-à-vis du <i>Israel Tax Authority,</i> il peut s’adresser à l’une de ses représentations locales et demander une attestation remplissant le rôle de quitus, prouvant la régularité de sa situation.</p>
<p><i>Source :<a href="http://ozar.mof.gov.il/customs/eng/mainpage.htm" class="spip_out" rel="external">Israel Tax Authority</a></i></p>
<h3 class="spip"><a id="sommaire_7"></a>Pour en savoir plus</h3>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.tresor.economie.gouv.fr/pays/israel" class="spip_out" rel="external">Service économique français de Tel Aviv</a></p>
<p><a href="http://ozar.mof.gov.il/customs/eng/mailus.htm" class="spip_out" rel="external">Contacter par email le service des impôts israélien</a> (Israel tax authority)</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://ozar.mof.gov.il/customs/eng/mainpage.htm" class="spip_out" rel="external">Israel tax authority</a></li>
<li><a href="http://www.mof.gov.il/" class="spip_out" rel="external">Ministère des Finances israélien</a></li>
<li><a href="http://export.businessfrance.fr/israel/export-israel-avec-nos-bureaux.html" class="spip_out" rel="external">Business France Israël</a> (source)</li>
<li><a href="http://www.investinisrael.gov.il/" class="spip_out" rel="external">Centre israélien d’investissement</a></li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/fiscalite/article/fiscalite-du-pays). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
