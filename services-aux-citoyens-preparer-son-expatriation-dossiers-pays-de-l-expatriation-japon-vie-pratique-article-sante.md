# Santé

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/vie-pratique/article/sante#sommaire_1">Médecine de soins</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/vie-pratique/article/sante#sommaire_2">Vaccinations</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/vie-pratique/article/sante#sommaire_3">Importation de médicaments</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/vie-pratique/article/sante#sommaire_4">Fukushima</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Médecine de soins</h3>
<p>Les conditions d’hygiène et l’état sanitaire du pays sont bons. Il est possible de recevoir des soins sur place mais se pose souvent le problème de la compréhension linguistique. Il est possible de consulter des médecins francophones ou anglophones. Les visites à domicile sont très rares. Outre le prix de la consultation, il faut souvent ajouter des frais supplémentaires : frais de dossier, frais de première consultation, certificat médical le cas échéant (3 000 yens).</p>
<p>Le coût des soins est élevé : compter environ 5 000 yens pour un généraliste, 10 000 yens pour un spécialiste et environ 3 000 yens dans un hôpital.</p>
<h3 class="spip"><a id="sommaire_2"></a>Vaccinations</h3>
<p>Aucune vaccination n’est obligatoire, sauf pour les personnes arrivant de régions contaminées.</p>
<p>On note actuellement une recrudescence des cas de rubéole au Japon avec 254 cas observés en janvier 2013, soit 13 fois le nombre constaté en janvier 2012. Il est recommandé aux voyageurs de vérifier que leurs vaccinations sont bien à jour.</p>
<h3 class="spip"><a id="sommaire_3"></a>Importation de médicaments</h3>
<p>En cas de traitement médical hors substance narcotique, il est normalement possible d’importer sans autorisation le médicament prescrit sur ordonnance pour une durée d’un mois. Au delà de cette quantité, une autorisation est nécessaire.</p>
<p>Pour tout renseignement complémentaire, s’adresser (de préférence en anglais) au : <a href="http://www.mhlw.go.jp/english/index.html" class="spip_out" rel="external">Ministry of health and welfare, Inspection and guidance division, Pharmaceutical and medical safety</a> bureau. Tél : 5353-1111 (poste 2768), Fax : 3501.0034.</p>
<p>L’importation de substances narcotiques telles que morphine, codéine, oxycodone, pethidine, hydrocodone… relève de la loi sur le contrôle des narcotiques et psychotropes et ne peut intervenir sans une autorisation préalable nécessitant un délai d’instruction d’un mois minimum.</p>
<p>L’importation de méthadone est strictement interdite et le produit ne peut être acquis localement.</p>
<p><strong>Consulter le médecin traitant avant le départ et contracter une assurance de rapatriement sanitaire.</strong></p>
<p>Pour une présentation des risques épidémiologiques ainsi que des conseils sur la médecine au quotidien, consultez les pages suivantes :</p>
<ul class="spip">
<li>Conseils, liste de médecins et hôpitaux sur le site <a href="http://www.ambafrance-jp.org/Liste-de-notoriete-medicale" class="spip_out" rel="external">du consulat de France à Tokyo</a></li>
<li>Page dédiée à la santé au Japon sur le site <a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/japon/" class="spip_in">Conseils aux voyageurs</a></li>
<li>Fiches Osaka-Kobé et Tokyo sur le site <a href="http://www.cimed.org" class="spip_out" rel="external">du CIMED</a>.</li></ul>
<p>Plusieurs organismes publics ou associatifs disposent de services d’information médicale à même de renseigner et d’assister des étrangers. La liste ci-dessous concerne principalement mais non exclusivement la région de Tokyo.</p>
<p>1. <a href="http://www.himawari.metro.tokyo.jp/qq/qq13enmnlt.asp" class="spip_out" rel="external">Himawari - Centre d’information médicale et d’assurance du gouvernement de Tokyo</a> comprenant :</p>
<ul class="spip">
<li>un service d’information en langue étrangère <br class="manualbr">Tél. : (03) 5285.8181 - 7 jours sur 7 de 9h à 20h ;</li>
<li>un service de traduction en urgence pour les médecins et hôpitaux <br class="manualbr">Tél. : (03) 5285.8185 - 7 jours sur 7, de 9h à 20h.</li></ul>
<p>2. Service d’information téléphonique du département des pompiers de Tokyo, 7 jours sur 7, 24h/24h, renseignements en anglais. <br class="manualbr">Tel : (03) 3212.2111 pour les 23 arrondissements (Ku) de Tokyo <br class="manualbr">Tel : (042) 521.2323 pour le district de Tama, à l’ouest de la préfecture de Tokyo.</p>
<p>3. Centre international d’information médicale Amda Service, du lundi au vendredi, de 9h à 17h, en sept langues étrangères à même de donner des informations et adresses de médecins parlant une langue étrangère sur l’ensemble du Japon (plus de 20 000 médecins recensés sur Tokyo dont 105 francophones). <br class="manualbr">Tél. : (03) 5285.8086<br class="manualbr">Fax : (03) 5285.8087 (Centre de Tokyo) <br class="manualbr">Tél. : (042) 799.3759 (Bureau de Machida) <br class="manualbr">Tél. : (06)4395.0555 (Centre du Kansai).</p>
<p>4. - Tokyo English Life Line (avec quelques bénévoles francophones) 7 jours sur 7, de 9h à 16h et de 19h à 23h. <br class="manualbr">Tél. : (03) 5774.0992.</p>
<h3 class="spip"><a id="sommaire_4"></a>Fukushima</h3>
<p><strong>Suites des catastrophes du 11 mars 2011</strong></p>
<p>La vie a repris son cours normal à Tokyo et dans l’ensemble du Japon, à l’exception d’une zone clairement identifiée proche du réacteur accidenté.</p>
<p>Rien ne s’oppose à se rendre au Japon, ni ne s’oppose aux déplacements sur le territoire japonais à l’exception de cette zone.</p>
<p>Autour de la centrale nucléaire de Fukushima-1 (un site industriel isolé sur le littoral de la préfecture de Fukushima) est définie une zone interdite et des zones d’accès restreint.</p>
<p>Les consignes données par les autorités japonaises évoluent en fonction de l’actualisation de la cartographie des zones contaminées et des mesures de décontamination. Il convient à l’heure actuelle de se référer aux consignes de sécurité édictées par les autorités japonaises.</p>
<p><a href="http://www.ambafrance-jp.org/spip.php?article5841" class="spip_out" rel="external">Une carte du zonage de sécurité est disponible et consultable sur le site de l’Ambassade de France au Japon</a></p>
<p>Dans tous les cas, il convient de respecter scrupuleusement les consignes données par les autorités japonaises. Dans les zones d’accès restreint, il est recommandé de se référer localement à la mairie concernée.</p>
<p>Pour une information précise (radioactivité d’une zone déterminée, aliments, actions de décontamination), ne pas hésiter à contacter l’ambassade via le site internet. En outre, un numéro de téléphone est mis à disposition par les autorités japonaises : 0120 988 359 (horaires d’ouverture : de lundi à vendredi 08h30 à 20h00, week-end et jours fériés de 08h30 à 18h00, en japonais).</p>
<p>Les ressortissants français résidents ou de passage peuvent utilement se référer au site de l’<a href="http://www.asn.fr/" class="spip_out" rel="external">ASN</a> (Autorité de sûreté nucléaire).</p>
<p><strong>Suivi de la contamination alimentaire après l’accident de Fukushima</strong></p>
<p>Depuis l’accident nucléaire de la centrale de Fukushima, des contrôles sanitaires sont effectués sur les produits agricoles provenant des régions où la mesure de la contamination a montré un dépassement des seuils légaux, plus particulièrement dans les préfectures de Fukushima, Iwate et Miyagi. Depuis le 1er avril 2012, les seuils ont été abaissés (plus sévères). En cas de contrôle positif, le produit est retiré du circuit de distribution.</p>
<p>Au Japon, l’étiquetage des denrées indique obligatoirement leur lieu de production. Le consommateur peut en outre s’adresser au commerçant.</p>
<p>Les notes publiées sur son site par l’Institut national de radioprotection et de sûreté nucléaire (IRSN) constituent également une source utile d’information.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/vie-pratique/article/sante). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
