# Stages

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/emploi-stage/article/stages#sommaire_1">Stage professionnel</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/emploi-stage/article/stages#sommaire_2">Stage d’étude</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/emploi-stage/article/stages#sommaire_3">La recherche de stage</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/emploi-stage/article/stages#sommaire_4">Le déroulement et la fin du stage : conseils</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/emploi-stage/article/stages#sommaire_5">Avez-vous pensé au volontariat international (V.I.E ou V.I.A.) ?</a></li></ul>
<p>Les stages professionnels ont pris de plus en plus d’importance dans le cursus des étudiants jusqu’à devenir indispensables pour s’insérer dans le monde du travail. Le stage se définit comme une formation pratique qui permet de compléter la formation théorique ou académique.</p>
<p>Il est devenu un élément essentiel de la formation initiale et constitue une véritable passerelle entre les études et le premier emploi.</p>
<p>Cette étude tente de répondre aux questions que peuvent se poser les étudiants ou les jeunes diplômés français à la recherche de stages en Espagne. Législation et modalités pratiques de recherche des stages seront abordées successivement.</p>
<p><strong>La réglementation des stages</strong></p>
<p>Il convient de faire clairement la distinction entre le <i>período de prácticas</i>, équivalent du stage français qui s’adresse aux étudiants, et le contrat de stage appelé <i>contrato en prácticas</i> qui a pour objectif de faciliter l’insertion des jeunes diplômés sur le marché de l’emploi.</p>
<h3 class="spip"><a id="sommaire_1"></a>Stage professionnel</h3>
<p>Ce contrat s’adresse aux jeunes diplômés. Il est utilisé par les entreprises espagnoles. En répondant à certains critères, elles peuvent percevoir bonifications et aides financières de l’Etat.</p>
<p>Ce contrat se définit comme la prestation d’un travail rétribué permettant une pratique professionnelle adaptée à son niveau d’études. Il est réglementé par l’article 11 du statut des travailleurs, la loi 63/97 et le Décret royal n° 488/98.</p>
<p><strong>Conditions</strong></p>
<p>Pour le travailleur :</p>
<ul class="spip">
<li>il doit être titulaire d’un diplôme universitaire ou de formation professionnelle de niveau moyen ou supérieur ou titulaire d’un diplôme officiellement reconnu et autorisant la pratique d’une activité professionnelle.
<p>Exemple : diplômé universitaire, ingénieur, architecte, technicien ou technicien spécialisé de professions réglementées.</p></li>
<li>Ce contrat est limité à quatre ans après la fin des études ou après l’obtention d’une équivalence d’un diplôme étranger et à six ans pour un travailleur handicapé. Pour l’entreprise : le poste de travail doit permettre une pratique professionnelle adaptée aux études.</li></ul>
<p>Les conventions collectives définissent les postes, groupes et les catégories professionnelles qui peuvent faire l’objet de ce contrat.</p>
<p><strong>Le contrat <i>en prácticas</i> </strong></p>
<p>Forme du contrat : il est écrit selon le formulaire officiel ; la durée du contrat et le poste de travail y sont précisés. Période d’essai : elle ne peut être supérieure à :</p>
<ul class="spip">
<li>un mois pour les titulaires de diplôme universitaire de premier ou second cycle (<i>diplomados</i> et techniciens)</li>
<li>deux mois pour les titulaires de diplômes universitaires de 3ème cycle (<i>licenciados</i> et techniciens supérieurs). Si le salarié est embauché par la suite, il est exempté de la période d’essai.</li></ul>
<p>La durée du contrat est comprise entre six mois et deux ans. Dans ces limites, les conventions collectives peuvent fixer les durée des contrats en fonction des caractéristiques de la branche d’activité.</p>
<p>Si le contrat a été conclu pour une durée inférieure à la durée maximale, les parties peuvent prolonger deux fois le contrat dans le cadre de cette limite. Le contrat peut être à temps complet ou à temps partiel.</p>
<p>Sont présumés être conclus à durée indéterminée les contrats qui n’ont pas fait l’objet d’écrit et sont présumés être à temps complet, les contrats à temps partiel qui n’ont pas fait l’objet d’écrit.</p>
<p>La fin du contrat :</p>
<p>si le contrat a eu une durée supérieure à une année, la fin du contrat doit être communiquée par écrit avec un préavis de 15 jours. L’entreprise devra fournir au salarié un certificat attestant la durée du stage, le poste occupé et les principales taches réalisées.</p>
<p><strong>La rémunération</strong></p>
<p>Le salaire est fixé par la convention collective mais ne peut être inférieur :</p>
<ul class="spip">
<li>la 1ère année : à 60 % du salaire fixe d’un salarié ayant un poste équivalent</li>
<li>la 2ème année : à 75 %</li></ul>
<p>En aucun cas le salaire ne peut être inférieur au salaire minimum interprofessionnel. Le SMI au 1er janvier 2014 est de 645,30 € nets /mois.</p>
<p>Pour les contrats à temps partiel, le salaire est réduit en proportion.</p>
<h3 class="spip"><a id="sommaire_2"></a>Stage d’étude</h3>
<p>La personne en <i>período de prácticas</i> ne bénéficie pas de contrat de travail et ne possède pas le statut de salarié. <strong>A noter</strong> : ce type de stage peut aussi être appelé <i>becario</i>.</p>
<p>Pour les étudiants issus d’universités, écoles ou centres de formation français, la réglementation est la suivante :</p>
<p><strong>Convention de stage</strong></p>
<p>Il est recommandé d’obtenir une convention de stage de son établissement d’origine, avant le départ à l’étranger.</p>
<p>Elle permet de fixer l’ensemble des conditions et modalités du stage (dates de début du stage, lieu, durée, horaires, missions, coordonnées de l’entreprise, désignation du maître de stage, rémunération…). Il ne s’agit pas d’une relation de travail. Les entreprises et les organismes ont en général leurs propres conventions.</p>
<p>Les stages réalisés par les étudiants sont complètement différents des <i>Contratos laborales de pràcticas</i> destinés aux jeunes diplômés.</p>
<p><strong>Couverture sociale et assurances</strong></p>
<p>Il est nécessaire de vérifier que l’assurance scolaire, universitaire couvre la période de stage à l’étranger. L’étudiant en stage à l’étranger doit être couvert par une assurance responsabilité civile.</p>
<p>Les étudiants français doivent se procurer auprès de leur organisme de sécurité sociale la carte européenne d’assurance maladie (CEAM). Elle remplace l’ancien formulaire E111 et permet de bénéficier des soins du système public de santé espagnol tout en se faisant rembourser par la sécurité sociale française.</p>
<p>Pour les étudiants issus d’universités, d’écoles ou centres de formation espagnols, les stages sont réglementés par le Décret royal n° 1497/1981 et le Décret royal du 9/09/1994 (cf. annexes).</p>
<p>Selon ces décrets, peuvent réaliser des stages, les étudiants de dernière ou avant-dernière année d’étude. La durée peut être de six mois maximum par année d’études ou 50 % de l’année universitaire.</p>
<p><strong>A noter</strong> : certaines universités espagnoles ne fonctionnent pas par année universitaire mais par crédits nécessaires à l’obtention du diplôme.</p>
<p>Les étudiants de moins de 28 ans disposent de l’assurance scolaire, cette assurance couvre en principe les risques d’accidents ou de maladie pendant le stage même si les prestations ne sont pas clairement définies.</p>
<p>Beaucoup d’universités disposent d’assurances collectives pour leurs étudiants.</p>
<p>Il est vivement recommandé de se renseigner sur ces droits et sa couverture sociale avant le début du stage. Ne s’agissant pas d’une relation de travail, un accident dans l’entreprise ne pourrait être qualifié d’accident du travail.</p>
<p><strong>Rémunération</strong></p>
<p>Il est fréquent que les entreprises octroient aux étudiants une aide économique, il s’agit simplement d’une aide et non d’une rémunération.</p>
<h3 class="spip"><a id="sommaire_3"></a>La recherche de stage</h3>
<p><strong>Présentation de la candidature</strong></p>
<p>La recherche d’un stage est identique au mode de recherche d’un emploi. Quelques conseils :</p>
<ul class="spip">
<li>Se renseigner</li>
<li>Le CV d’un étudiant doit tenir en une seule page.</li>
<li>Il est préférable de soigner la présentation et faire figurer la photo en couleur.</li>
<li>Indiquer, en en-tête, le diplôme et la spécialisation.</li>
<li>Rédiger une lettre expliquant sa motivation et l’apport pour l’entreprise.</li></ul>
<p>Vous avez pris soin au préalable de vous informer sur l’entreprise et son domaine d’activité</p>
<p><strong>Les modes de recherche</strong></p>
<p>La recherche peut s’avérer plus ou moins difficile en fonction de l’activité de l’entreprise, sa taille, le type de mission souhaité ou la période demandée.</p>
<p>Le choix de l’entreprise peut être déterminant pour une future recherche d’emploi, le stagiaire peut acquérir une expérience plus spécialisée dans les grandes entreprises mais il peut parfois y avoir plus d’opportunités d’emploi dans les PME.</p>
<p>La durée du stage peut être très variable, certaines entreprises proposent parfois des stages à temps partiels.</p>
<p>La journée complète est parfois préférable au temps partiel, ainsi qu’une période continue plutôt que l’alternance, afin de permettre une meilleure intégration dans l’entreprise.</p>
<p>En revanche, un stage à temps partiel permet de combiner un emploi à temps partiel (lorsque les stages ne sont pas rémunérés).</p>
<p>La question peut parfois se poser des tâches et des fonctions que vont accomplir les stagiaires. Il est conseillé de négocier une mission précise à mener pendant le stage. L’objectif est de compléter sa formation académique.</p>
<p><strong>Où chercher un stage ?</strong></p>
<ul class="spip">
<li>Les centres d’études et centres d’orientation des universités</li>
<li>Les bourses d’emploi des écoles (certaines ont du personnel dédié à la gestion des stages)</li>
<li>Les sites Internet spécialisés (cf. liste suivante)</li>
<li>La presse et revues spécialisées</li>
<li>Auprès des professeurs et de leurs contacts</li>
<li>Les programmes européens</li>
<li>Les sites internet institutionnels</li>
<li>Les associations d’étudiants et d’anciens élèves</li></ul>
<p>Vous trouverez ci-dessous des adresses utiles pour la recherche de votre stage</p>
<p><strong>Les forums</strong></p>
<ul class="spip">
<li><a href="http://www.parisetudiant.com/" class="spip_out" rel="external">Parisetudiant.com</a></li>
<li><a href="http://www.le-forum-emploi.com/" class="spip_out" rel="external">Le-forum-emploi.com</a></li></ul>
<p><strong>Les sites internet</strong></p>
<ul class="spip">
<li><a href="http://www.studyrama.com/" class="spip_out" rel="external">Studyrama.com</a></li>
<li><a href="http://www.jobetudiant.com/" class="spip_out" rel="external">Jobetudiant.com</a></li>
<li><a href="http://www.kapstages.com/" class="spip_out" rel="external">Kapstages.com</a></li>
<li><a href="http://www.letudiant.fr/" class="spip_out" rel="external">Letudiant.fr</a></li>
<li><a href="http://www.lepetitjournal.com/" class="spip_out" rel="external">Lepetitjournal.com</a></li>
<li><a href="http://www.emploi-etudiant.com/" class="spip_out" rel="external">Emploi-etudiant.com</a></li>
<li><a href="http://www.euroguidance-france.org/" class="spip_out" rel="external">Euroguidance-france.org</a></li></ul>
<p><strong>Organismes spécialisés</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.dialogo.es/" class="spip_out" rel="external">Association franco-espagnole Diálogo</a><br class="manualbr">Cette association franco-espagnole gère une bourse de stages : elle met en relation candidats et entreprises. L’association Diálogo établit la convention de stage.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.aiesec.org/" class="spip_out" rel="external">AIESEC France (Association des étudiants en sciences économiques)</a><br class="manualbr">14, rue de Rouen - 75 019 Paris<br class="manualbr">Tel. : 01 40 36 22 33</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.loffice.org/" class="spip_out" rel="external">Office national de garantie des séjours et stages linguistiques</a><br class="manualbr">8 rue César Franck- 75015 Paris<br class="manualbr">Tél. : 01 42 73 36 70</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.eurodyssee.net/" class="spip_out" rel="external">Eurodyssée</a><br class="manualbr">Ce programme d’échanges de jeunes entre Régions d’Europe permet aux demandeurs d’emploi de 18 à 30 ans de bénéficier d’un stage dans une entreprise d’une autre région participante à l’étranger, d’une durée de trois à sept mois.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Association Tramontana, délégation de la ville de Perpignan</strong><br class="manualbr">Diputación 280, Baixos – <br class="manualbr">08009 Barcelona<br class="manualbr">Tél. : 93.317.36.21<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/emploi-stage/article/stages#tramontana#mc#perpinya.org#" title="tramontana..åt..perpinya.org" onclick="location.href=mc_lancerlien('tramontana','perpinya.org'); return false;" class="spip_mail">Courriel</a></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.cocef.com/" class="spip_out" rel="external">Chambre de commerce franco-espagnole</a><br class="manualbr">22 rue Saint Augustin- 75002 Paris<br class="manualbr">Tel. : 01 42 61 33 10</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.ies-consulting.es/index.php" class="spip_out" rel="external">IES Consulting</a> (Agence de sélection et de placement de stagiaires européens au sein d’entreprises partenaires présentes sur tout le territoire espagnol)<br class="manualbr">22, calle dels Santjoanistes, entr.2 <br class="manualbr">08006 Barcelona<br class="manualbr">Tel. : 93 241 79 02</p>
<h3 class="spip"><a id="sommaire_4"></a>Le déroulement et la fin du stage : conseils</h3>
<p><strong>Le début du stage</strong></p>
<p>Identifier la personne en charge des stagiaires dans l’entreprise et de leurs démarches administratives.</p>
<p>Dès le début, il est conseillé de se fixer des objectifs, d’organiser ses activités et de prévoir des moments d’évaluation. Par la suite, vous comparerez l’apport du stage par rapport aux objectifs fixés.</p>
<p><strong>Le tuteur</strong></p>
<p>C’est un personnage clé du stage, une personne experte, habituée à accompagner les étudiants. Le tuteur sert de guide dans l’apprentissage. Il aide l’étudiant tout au long de sa formation.</p>
<p><strong>La finalité</strong></p>
<p>Le stage permet de découvrir l’entreprise par le biais d’un poste de travail particulier : fonction, moyens techniques particuliers, qualifications requises pour le poste, collaborateurs, relations formelles ou informelles, culture d’entreprise.</p>
<p>Le stage permet d’avancer dans la construction de son projet professionnel et de déterminer quels sont les postes les plus adaptés à son parcours, le secteur d’activité, les critères de choix d’un poste…</p>
<p>Il permet aussi de découvrir les aspects pratiques d’un espace de travail :</p>
<ul class="spip">
<li>le temps de travail</li>
<li>l’environnement physique</li>
<li>la sécurité</li>
<li>l’organisation du travail et la répartition des tâches</li>
<li>le style de relations et de communication</li>
<li>la qualification et la formation</li>
<li>les rémunérations</li>
<li>l’environnement social et culturel du travail</li>
<li>les instances de représentation et d’expression collective des travailleurs.</li></ul>
<p><strong>Le rapport de stage</strong></p>
<p>L’objectif du stage n’est pas d’acquérir des connaissances théoriques mais d’avoir un premier contact direct avec le milieu professionnel et une expérience.</p>
<p>Le rapport de stage est souvent obligatoire dans le cadre d’écoles ou d’université mais même si ce n’est pas le cas, il est conseillé d’en rédiger un afin de consigner ses observations.</p>
<p>Il existe deux types de rapport de stage :</p>
<ul class="spip">
<li>le compte rendu, c’est en général le plus fréquent. L’étudiant relate ce qu’il a fait et décrit ce qu’il a pu découvrir dans l’entreprise en rapport avec sa formation.</li>
<li>Le rapport d’étude. Dans ce cas, le rapport porte sur un thème particulier, il peut s’agit d’un véritable travail de conseil ou d’analyse d’une problématique donnée. Ce sont des mémoires de fin de formation pour les étudiants sur le point d’être diplômés, ils sont plus élaborés.</li></ul>
<p>Le rapport de stage doit être, dans tous les cas, synthétique, clair et rigoureux. Il peut parfois faire l’objet d’une présentation orale.</p>
<p><strong>La fin du stage</strong></p>
<p>A la fin du stage, l’entreprise fournit, en principe un certificat de stage attestant de la période de stage, du lieu, de la mission. Ce document permettra d’accréditer cette première expérience professionnelle et de la valoriser dans le CV de l’étudiant.</p>
<p>Il faut noter que certains centres de formations demandent une évaluation du stage par le tuteur.</p>
<p><strong>Sources :</strong></p>
<ul class="spip">
<li><a href="http://www.empleo.gob.es/" class="spip_out" rel="external">Ministère de l’Emploi et de la Sécurité sociale</a></li>
<li>Guía de las empresas que ofrecen empleo - Cámara de Madrid 2005</li>
<li><a href="http://www.gencat.es/" class="spip_out" rel="external">Generalitat de Catalunya</a></li>
<li>Manual de Prácticas ESEC Barcelona</li>
<li><a href="http://www.ameli.fr/" class="spip_out" rel="external">Caisse d’assurance maladie</a></li></ul>
<h3 class="spip"><a id="sommaire_5"></a>Avez-vous pensé au volontariat international (V.I.E ou V.I.A.) ?</h3>
<p>Le V.I.E. permet aux entreprises françaises de confier à un jeune ressortissant de l’espace économique européen, âgé de 18 à 28 ans, une mission professionnelle (rémunérée) à l’étranger durant une période modulable de 6 à 24 mois.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/emplois-stages-concours/volontaires-internationaux/" class="spip_in">Page thématique sur le volontariat international</a></li>
<li><a href="http://www.civiweb.com/FR/index.aspx" class="spip_out" rel="external">Civiweb.com</a></li>
<li><a href="http://export.businessfrance.fr/default.html" class="spip_out" rel="external">Business France.fr</a> - <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/emploi-stage/article/stages#infoVIE#mc#ubifrance.fr#" title="infoVIE..åt..ubifrance.fr" onclick="location.href=mc_lancerlien('infoVIE','ubifrance.fr'); return false;" class="spip_mail">Courriel</a></li></ul>
<p><i>Mise à jour : avril 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/emploi-stage/article/stages). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
