# Santé

<p>Pour une présentation des risques épidémiologiques ainsi que des conseils sur la médecine au quotidien, consultez les pages suivantes :</p>
<ul class="spip">
<li>Conseils, liste de médecins et hôpitaux sur le site du <a href="http://www.ambafrance-au.org/Medecins" class="spip_out" rel="external">consulat de France à Sydney</a></li>
<li>Page dédiée à la santé en Australie sur le site <a href="http://publication.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/australie-12202/" class="spip_out" rel="external">Conseils aux voyageurs</a></li>
<li>Fiche Australie sur le site du <a href="http://www.cimed.org/" class="spip_out" rel="external">CIMED</a></li></ul>
<p><strong>Santé de l’enfant</strong></p>
<p>Le programme vaccinal est globalement le même que celui de la France. La différence concerne l’espacement des vaccinations plus long en Australie (notamment les toutes premières vaccinations du nourrisson) et celle concernant la vaccination contre la varicelle (effectuée vers 18 mois) qui est "obligatoire" en Australie alors qu’elle ne se pratique pas en France. Pour plus d’informations sur le programme vaccinal australien, voir le lien suivant sur le site du <a href="http://www.immunise.health.gov.au/" class="spip_out" rel="external">ministère de la Santé</a>.</p>
<p>Il est à noter par ailleurs que la supplémentation en vitamine D (type <i>Uvestérol D </i>en flacon<i>) </i>importante notamment pour la santé osseuse et fortement recommandée en France chez le nourrisson et l’enfant jusqu’à l’âge de 3 ans ne se trouve pas en Australie.</p>
<p>De même, il n’est pas possible de trouver de sérum physiologique sous forme d’unidoses (très pratique) pour l’hygiène des yeux et du nez du nourrisson.</p>
<p><i>Mise à jour : octobre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/vie-pratique/article/sante). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
