# Vie pratique

<h2 class="rub22894">Logement</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/vie-pratique/#sommaire_1">Où se loger ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/vie-pratique/#sommaire_2">Conditions de location</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/vie-pratique/#sommaire_3">Electricité</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/vie-pratique/#sommaire_4">Electroménager</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Où se loger ?</h3>
<p>Il existe un large éventail de logements à louer en Irlande. Leur disponibilité et leurs prix dépendent de la situation géographique. A titre d’exemple, le prix des logements à Dublin est élevé en raison de la forte demande.</p>
<p>Les nouveaux arrivants optent pour la location d’un logement. Des listes de chambres chez des particuliers ou de maisons à louer sont publiées dans les annonces immobilières des quotidiens du soir tels que The Evening Herald. Les agences immobilières proposent également des listes de locations dans la localité moyennant parfois des honoraires. La plupart des logements sont loués meublés.</p>
<p>Les hôtels entre deux et cinq étoiles sont nombreux et le prix d’une chambre double varie entre 30 et 245 euros environ. Les <i>Bed and Breakfast </i>(BB) offrent une gamme de services à des prix variés. Enfin, les auberges de jeunesse et les pensions sont recommandées pour les petits budgets. Les prix oscillent entre 15 et 30 euros selon que la chambre est partagée ou non.</p>
<p><strong>Pour en savoir plus</strong></p>
<ul class="spip">
<li><a href="http://www.herald.ie/" class="spip_out" rel="external">The Evening Herald</a></li>
<li>L’organisation <a href="http://www.hihostels.com/" class="spip_out" rel="external">Hosteling International</a> (liée à la <a href="http://www.fuaj.org/" class="spip_out" rel="external">Fédération unie des auberges de jeunesse)</a></li>
<li><a href="http://www.discoverireland.com/fr/accommodation/" class="spip_out" rel="external">Rubrique "hébergements" sur le site de l’Office du tourisme irlandais</a></li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Conditions de location</h3>
<p>Les quartiers résidentiels de Dublin sont principalement situés au sud du centre ville. La durée des baux (<i>lease</i>) est en règle générale d’un an. Les appartements sont généralement meublés.</p>
<p>A titre indicatif, les prix de la location hors charges à Dublin se situent dans les fourchettes suivantes (par mois) :</p>
<ul class="spip">
<li>en centre ville : un F2 est loué entre 950 et 3000 euros ;</li>
<li>en banlieue : environ 645 euros pour un studio correct, de 1200 euros pour un trois pièces à 1500 euros pour un cinq pièces ;</li>
<li>dans les quartiers résidentiels : de 1600 euros pour un trois pièces à 5000 euros pour un quatre pièces ;</li>
<li>dans le quartier des ambassades : de 2000 euros pour un trois pièces à 3400 euros pour un cinq pièces et jusqu’à 6000 euros pour une villa ;</li>
<li>Le prix d’une chambre en colocation à Dublin varie en fonction des quartiers entre 300 et 1000 euros par mois.</li></ul>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.citizensinformation.ie/en/housing/" class="spip_out" rel="external">Rubrique "Housing" du portail officiel du service public irlandais</a> (pour consulter notamment vos droits et devoirs en tant que locataire). </p>
<h3 class="spip"><a id="sommaire_3"></a>Electricité</h3>
<p>Le courant électrique est alternatif, monophasé (220 Volts). La fréquence est de 50 Hertz. Les prises de courant sont de type anglo-saxon à trois broches et nécessitent un adaptateur que l’on peut se procurer facilement sur place.</p>
<h3 class="spip"><a id="sommaire_4"></a>Electroménager</h3>
<p>Bien que les logements soient en général meublés, en Irlande tout l’équipement de maison est disponible sur place. L’offre est importante et diversifiée. Les prix des appareils domestiques sont très variables et dépendent pour la plupart du temps de la marque.</p>
<p>Le matériel audiovisuel fonctionne sur le système PAL I, spécifique à l’Irlande.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-irlande-vie-pratique-article-loisirs-et-culture.md" title="Loisirs et culture">Loisirs et culture</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-irlande-vie-pratique-article-communications.md" title="Communications">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-irlande-vie-pratique-article-transports.md" title="Transports">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-irlande-vie-pratique-article-cout-de-la-vie.md" title="Coût de la vie">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-irlande-vie-pratique-article-scolarisation.md" title="Scolarisation">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-irlande-vie-pratique-article-sante.md" title="Santé">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-irlande-vie-pratique-article-logement-110438.md" title="Logement">Logement</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/vie-pratique/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
