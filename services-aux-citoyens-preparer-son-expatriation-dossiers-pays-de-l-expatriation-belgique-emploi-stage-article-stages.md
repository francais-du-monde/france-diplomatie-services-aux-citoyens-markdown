# Stages

<p>Différents moyens de recherche s’offrent à vous, lesquels passent principalement par :</p>
<ul class="spip">
<li>le réseau relationnel personnel ;</li>
<li>le réseau des anciens de votre université ;</li>
<li>les répertoires d’entreprises et d’organismes (et leur sites web) comme Kompass ;</li>
<li>les candidatures spontanées auprès d’organismes bien déterminés ;</li>
<li>les chambres de commerce belges ;</li>
<li>les nombreux sites d’offres de stage.</li></ul>
<h4 class="spip">En Belgique francophone</h4>
<ul class="spip">
<li><a href="http://www.kapstages.com/" class="spip_out" rel="external">Kapstages.com</a></li>
<li><a href="http://www.belgique.enligne-fr.com/annonces_os.php" class="spip_out" rel="external">Belgique.enligne-fr.com</a></li>
<li><a href="http://www.monstage.be/" class="spip_out" rel="external">Monstage.be</a></li>
<li><a href="http://emploi.efinancialcareers.be/Jeunes_Diplomes_%7C_Stages.htm" class="spip_out" rel="external">Efinancialcareers.be</a> (banque, finances)</li>
<li><a href="http://www.biowin.org/Biowin/fr/stages-en-entreprises.html" class="spip_out" rel="external">Biowin.org</a> (domaine de la santé)</li></ul>
<p>A Bruxelles, le site de <a href="http://www.bruxellesformation.be/" class="spip_out" rel="external">Bruxelles Formation</a> donne une série de conseils et de liens pour trouver un stage (avec ou sans bourse, dans un organisme privé, dans une institution européenne, etc.).</p>
<p>En Wallonie, le <a href="http://www.leforem.be/" class="spip_out" rel="external">FOREM</a> (Accueil / Particuliers / Se former / Stages en entreprise) propose également des stages sur son site internet. Cette section est principalement réservée aux demandeurs d’emploi.</p>
<p><i>Source</i> : portail <a href="http://www.belgium.be/fr/formation/enseignement/superieur/recherche_d_un_stage/" class="spip_out" rel="external">Belgium.be</a> qui vous informera également sur les possibilités de stages dans les services publics belges.</p>
<h4 class="spip">En Belgique germanophone</h4>
<p>Le <a href="http://www.dglive.be/desktopdefault.aspx/tabid-266/496_read-1022/" class="spip_out" rel="external">Arbeitsamt</a> pourra vous fournir des informations au sujet des possibilités de stage.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p><a href="http://europa.eu/youth/working/traineeships/index_be_fr.html" class="spip_out" rel="external">Le portail européen de la jeunesse</a> référence les principaux sites offrant des stages à l’attention de la communauté française, germanophone ou néerlandophone.</p>
<p><strong>Avez-vous pensé au Volontariat International (V.I.E ou V.I.A.) ?</strong></p>
<p>Le V.I.E. permet aux entreprises françaises de confier à un jeune ressortissant de l’espace économique européen, âgé de 18 à 28 ans, une mission professionnelle (rémunérée) à l’étranger durant une période modulable de 6 à 24 mois.</p>
<p>Pour en savoir plus :</p>
<ul class="spip">
<li><a href="http://publication.diplomatie.gouv.fr/fr/emplois-stages-concours/volontaires-internationaux/" class="spip_out" rel="external">Page thématique sur le volontariat international sur notre site</a></li>
<li><a href="http://www.civiweb.com/FR/index.aspx" class="spip_out" rel="external">Civiweb.com</a></li>
<li><a href="http://www.businessfrance.fr/" class="spip_out" rel="external">Business France.fr</a> - <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/emploi-stage/article/stages#infoVIE#mc#ubifrance.fr#" title="infoVIE..åt..ubifrance.fr" onclick="location.href=mc_lancerlien('infoVIE','ubifrance.fr'); return false;" class="spip_mail">courriel</a></li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/emploi-stage/article/stages). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
