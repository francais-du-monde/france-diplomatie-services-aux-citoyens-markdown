# Actualiser son livret de famille

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/etat-civil/article/livrets-de-famille#sommaire_1">Première délivrance</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/etat-civil/article/livrets-de-famille#sommaire_2">Mise à jour</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/etat-civil/article/livrets-de-famille#sommaire_3">Duplicata</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Première délivrance</h3>
<p>Le livret de famille est délivré, selon les situations, par l’officier de l’état civil qui célèbre le mariage ou par celui qui dresse l’acte de naissance du premier enfant. En revanche, la conclusion d’un PACS ne donne pas lieu à délivrance d’un livret de famille.</p>
<p>Le livret de famille est ensuite complété par :</p>
<ul class="spip">
<li>la naissance d’un enfant,</li>
<li>le décès de l’un des conjoints, d’un enfant mineur,</li>
<li>le divorce ou la séparation.Le livret de famille peut aussi être mis à jour pour prendre en compte la rectification de l’un des actes d’état civil dont l’extrait y a été inscrit.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Mise à jour</h3>
<p>La mise à jour du livret de famille ne peut intervenir qu’après que l’événement d’état civil correspondant ait été enregistré par l’officier d’état civil compétent, soit par l’établissement d’un acte (naissance, décès) soit par l’apposition d’une mention (divorce ou séparation). Par exemple, une mention de divorce ne peut être inscrite sur le livret de famille qu’après avoir été apposée sur l’acte de mariage.</p>
<p>Pour les événements d’état civil qui sont survenus à l’étranger, cette mise à jour peut être assurée par le Service central d’état civil ou par l’ambassade ou le consulat qui détient l’acte dans ses registres.</p>
<h3 class="spip"><a id="sommaire_3"></a>Duplicata</h3>
<p>En cas de séparation, divorce, vol ou perte, une demande de duplicata d’un livret de famille peut être sollicitée par l’un des époux auprès de la mairie du lieu de sa résidence ou, pour les personnes qui résident à l’étranger, auprès de l’ambassade ou du consulat de France territorialement compétent.</p>
<p>La mairie, l’ambassade ou le consulat se chargeront de transmettre la demande au service d’état civil détenteur de l’acte de mariage, et de coordonner l’inscription dans le livret de l’ensemble des extraits d’actes devant y figurer (notamment les actes de naissance des enfants), par les services d’état civil dépositaires des actes correspondants.</p>
<p>Le duplicata du livret de famille est ensuite retourné à la mairie de résidence (à l’ambassade ou au consulat de France si vous résidez à l’étranger) pour remise au conjoint qui l’a demandé.</p>
<p>Aucun duplicata ne peut être délivré en cas de décès des deux époux ou parents.</p>
<p><i>Mise à jour : mars 2016</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/etat-civil-et-nationalite-francaise/etat-civil/article/livrets-de-famille). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
