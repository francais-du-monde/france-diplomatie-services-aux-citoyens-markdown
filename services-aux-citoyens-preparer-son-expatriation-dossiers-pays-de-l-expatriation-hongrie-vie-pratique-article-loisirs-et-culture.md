# Loisirs et culture

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/vie-pratique/article/loisirs-et-culture#sommaire_1">Tourisme</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/vie-pratique/article/loisirs-et-culture#sommaire_2">Activités culturelles</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/vie-pratique/article/loisirs-et-culture#sommaire_3">Sports</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/vie-pratique/article/loisirs-et-culture#sommaire_4">Télévision – Radio</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/vie-pratique/article/loisirs-et-culture#sommaire_5">Presse française</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Tourisme</h3>
<p>La Hongrie est un pays relativement petit par sa superficie, mais très agréable pour le tourisme. Outre ses richesses historiques, on y trouve de nombreux sites intéressants, notamment dans la région du lac Balaton, le long du Danube et dans les montagnes du Bükk.</p>
<p>Pour toutes informations touristiques, s’adresser à :</p>
<p><a href="http://fr.gotohungary.com/" class="spip_out" rel="external">Office du tourisme de Hongrie</a><br class="manualbr">140, avenue Victor Hugo <br class="manualbr">75116 Paris <br class="manualbr">Tél. : 01 53 70 67 17 <br class="manualbr">Fax : 01 47 04 83 57 <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/vie-pratique/article/loisirs-et-culture#info#mc#hongrietourisme.com#" title="info..åt..hongrietourisme.com" onclick="location.href=mc_lancerlien('info','hongrietourisme.com'); return false;" class="spip_mail">Courriel</a><br class="manualbr"><a href="http://fr.gotohungary.com/" class="spip_out" rel="external">Facebook</a></p>
<p>L’Office du tourisme de Hongrie en France est fermé au public depuis le 1er septembre 2011 et ouvert pour les retraits de document sur place, uniquement sur rendez-vous. Pour cela, nous vous remercions de bien vouloir appeler au 01 53 70 67 17. Vous pouvez commander des brochures par téléphone, sur le site Internet (rubrique : Commandez vos brochures en direct) ou par courrier.</p>
<p>Pour des informations sur les offices du tourisme en Hongrie veuillez consulter le site internet suivant :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.canalmonde.fr/r-annuaire-tourisme/monde/guides/offices-de-tourisme.php?p=hu" class="spip_out" rel="external">Canalmonde.fr</a> </p>
<h3 class="spip"><a id="sommaire_2"></a>Activités culturelles</h3>
<p><strong>Activités culturelles en français</strong></p>
<p>Des films français (sous-titrés en hongrois) sont présentés à l’Institut français et dans certaines salles (réseau art et essai notamment), ainsi que durant le festival du film francophone (une semaine par an au mois de mars).</p>
<p>A Budapest, la plupart des activités culturelles françaises et francophones sont proposées par l’Institut Français : concerts, danse, théâtre, expositions, conférences, le plus souvent à l’Institut même, ou dans des espaces culturels de la ville.</p>
<p>En province, cinq Alliances françaises (Debrecen, Györ, Miskolc, Pécs, Szeged) proposent des films français, des expositions, des concerts, différents types de spectacles, des conférences, etc.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.inst-france.hu/" class="spip_out" rel="external">Institut français</a><br class="manualbr">Fö utca 17 <br class="manualbr">1011 Budapest <br class="manualbr">Tél. : (36) 1 489 42 00 <br class="manualbr">Fax : (36) 1 489 42 22 </p>
<p><strong>Activités culturelles locales</strong></p>
<p>Les salles de cinéma de Budapest et des grandes villes de province, nombreuses et confortables (importance des multiplexes), projettent essentiellement des films américains, ainsi que des films français le plus souvent doublés en hongrois, ainsi que des films hongrois, non traduits.</p>
<p>Les pièces de théâtre en hongrois, les concerts, les opéras et les ballets sont très nombreux et de grande qualité.</p>
<p>Les musées de Budapest et de province proposent des expositions permanentes d’un grand intérêt.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.hongrietourisme.com/main.php?folderID=947" class="spip_out" rel="external">Evènements culturels</a> (site de l’office de tourisme de Hongrie) </p>
<h3 class="spip"><a id="sommaire_3"></a>Sports</h3>
<p>Tous les sports peuvent être pratiqués. Il est possible de se procurer l’équipement sur place, car on trouve de plus en plus de matériel importé. Les clubs sportifs sont très nombreux et des rencontres sont organisées chaque fin de semaine dans toutes les disciplines.</p>
<p>La pratique de la chasse nécessite une affiliation à une société d’Etat, qui organise la chasse et fournit les permis et autorisations nécessaires. L’importation d’une arme de chasse est soumise à une déclaration préalable à la douane et à la police. La période d’ouverture varie selon les régions.</p>
<p>La carte de pêche journalière s’obtient auprès des bureaux de pêche communaux. La période varie selon les espèces et les régions.</p>
<h3 class="spip"><a id="sommaire_4"></a>Télévision – Radio</h3>
<p>Des stations de radios émettant en FM sur Budapest et en province diffusent des programmes d’informations et de divertissement, ainsi que quelques bulletins d’information en anglais. Deux radios publiques émettent sur ondes moyennes. Il est possible de capter RFI sur les ondes courtes, mais la qualité de réception est moyenne. Les programmes de RFI, couplés à ceux de la BBC, sont accessibles sur 92,1 MHz.</p>
<p>Il existe trois chaînes de télévision publiques et de nombreuses chaînes hongroises privées, accessibles sur le réseau hertzien en cours de remplacement par la TNT, le réseau câblé ou par satellite). Il est possible de recevoir des chaînes étrangères, soit par le câble (abonnement modique), soit par satellite via des opérateurs.</p>
<p>La plupart des quartiers de Budapest et des villes de provinces sont équipés d’un réseau câblé et il est possible de capter par satellite des chaînes de télévision françaises et francophones, telles que TV5, TF1, France 2 et M6.</p>
<h3 class="spip"><a id="sommaire_5"></a>Presse française</h3>
<p>La presse française est disponible avec un jour de décalage en général dans certaines librairies du centre-ville de Budapest et dans certaines grandes villes de province, ainsi qu’à la médiathèque située dans les locaux de l’Institut français, ou encore à la librairie française Latitudes où l’on peut également acheter des livres en français.</p>
<p><a href="http://www.latitudes.hu/" class="spip_out" rel="external">Librairie Latitudes</a><br class="manualbr">Wesselenyi utca 11<br class="manualbr">1077 Budapest <br class="manualbr">Tél : 06-1-488 04 30</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.budapestinfo.hu/" class="spip_out" rel="external">Budapestinfo.hu</a> </p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/vie-pratique/article/loisirs-et-culture). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
