# Loisirs et culture

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/vie-pratique/article/loisirs-et-culture#sommaire_1">Tourisme</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/vie-pratique/article/loisirs-et-culture#sommaire_2">Activités culturelles</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/vie-pratique/article/loisirs-et-culture#sommaire_3">Sports</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/vie-pratique/article/loisirs-et-culture#sommaire_4">Télévision – Radio</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/vie-pratique/article/loisirs-et-culture#sommaire_5">Presse française</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Tourisme</h3>
<p>Les sites touristiques sont très nombreux et d’un grand intérêt : Machu Picchu (sanctuaire historique construit à plus de 2000 m d’altitude au XVème siècle près de Cuzco, classé au patrimoine mondial de l’humanité par l’UNESCO en 1982), Iquitos (Amazonie), Puerto Maldonado (réserve nationale de Tambopata), Ica (Paracas, Iles Ballestas), Arequipa (Vallée du Colca), Puno (lac Titicaca et ses îles), Trujillo (site de Chan Chan), les plages du Nord (Punta Sal, Puerto Pizarro, Tumbes), Cajamarca, Kuélap (forteresse des Chachapoyas).</p>
<p>La capitale, Lima est une ville avec de nombreux attraits : gastronomiques (trois restaurants ont été classés en 2013 par William Reed Business Media parmi les <a href="http://www.theworlds50best.com/latinamerica/es/" class="spip_out" rel="external">10 meilleurs établissements d’Amérique latine</a>), historiques (centre historique, église et couvent de San Francisco de Jésus, etc.) et culturels (Musée national d’archéologie, d’anthropologie et d’histoire, Musée de l’or du Pérou, Musée Larco, Musée d’art de Lima, Musée de la Nation, Musée Pedro de Osma, etc.).</p>
<p>Pour plus d’informations, contactez l’<a href="http://www.peru.travel/fr/" class="spip_out" rel="external">office du tourisme péruvien</a><br class="manualbr">Ambassade du Pérou <br class="manualbr">50, avenue Kléber <br class="manualbr">75016 Paris <br class="manualbr">Tél. : 01 53 70 42 00</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.conper.fr/" class="spip_out" rel="external">Consulat du Pérou à Paris</a> </p>
<h3 class="spip"><a id="sommaire_2"></a>Activités culturelles</h3>
<h4 class="spip">Activités culturelles en français</h4>
<p>Le cinéma français est représenté au travers des circuits des <a href="http://alianzafrancesa.org.pe/" class="spip_out" rel="external">Alliances françaises</a> (à Lima et en région). Il existe des festivals, des cycles organisés avec des ciné-clubs locaux (deux salles : cinematografo, cinemateca). De plus en plus de salles commerciales programment des films français (notamment le cinéma Pacifico à Miraflores).</p>
<p>A Lima, trois à quatre spectacles de théâtre sont organisés par an, parfois par l’Alliance française.</p>
<p>Certaines galeries privées et les Alliances françaises exposent des artistes français.</p>
<p>Enfin l’Alliance française dispose d’une médiathèque où l’on peut trouver des livres, DVD, bande dessinées et des CD d’œuvres françaises.</p>
<p>Outre ces activités animées par le service culturel de l’ambassade et les Alliances françaises implantées à Lima, Arequipa, Chiclayo, Cuzco, Piura et Trujillo, la présence française se manifeste à travers deux organismes :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  L’Institut français d’études andines (IFEA), dont le siège est à Lima, favorise les échanges entre les milieux scientifiques français et ceux des pays andins. L’institut dispose d’une bibliothèque au siège. </p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.ifeanet.org/" class="spip_out" rel="external">Institut français des études andines</a></li>
<li><a href="http://www.perou.ird.fr/" class="spip_out" rel="external">Institut de recherche pour le développement (IRD)</a> travaille depuis 1967 dans le cadre d’accords de coopération, notamment avec les universités péruviennes.</li></ul>
<h4 class="spip">Activités culturelles locales</h4>
<p>Les salles de cinéma de Lima présentent essentiellement des films américains mais de plus en plus de films français sont distribués. Les salles sont peu confortables dans les quartiers populaires et dans le centre historique mais de nombreux complexes modernes ont été ouverts récemment (<a href="http://www.uvkmulticines.com/" class="spip_out" rel="external">UVK Multicines</a>, <a href="http://www.cineplanet.com.pe/" class="spip_out" rel="external">Cineplanet</a>).</p>
<p>Le théâtre est varié ; l’adaptation de pièces étrangères est faite en espagnol.</p>
<p>Des concerts de musique classique sont organisés par la Société philharmonique de Lima (cf. lien suivant : <a href="http://www.sociedadfilarmonica.com.pe/" class="spip_out" rel="external">http://www.sociedadfilarmonica.com.pe/</a>) et <i>prolírica</i>. La programmation de concerts est également riche avec la venue croissante d’artistes internationaux à Lima.</p>
<p>De nombreuses galeries d’art et centres culturels proposent des activités riches et variées. Pour en savoir plus consultez les sites suivants :</p>
<ul class="spip">
<li><a href="http://www.centroculturalpucp.com/" class="spip_out" rel="external">Centre culturel PUCP</a> ;</li>
<li><a href="http://www.ccincagarcilaso.gob.pe/" class="spip_out" rel="external">Centre culturel Inca Garcilaso</a>.</li></ul>
<p>La programmation des concerts (ou spectacles) et les billets correspondants sont disponibles sur le site Internet suivant :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.teleticket.com.pe/tlk_formularios/frm_Home.aspx" class="spip_out" rel="external">Teleticket.com.pe</a> </p>
<p>Enfin, de nombreux pays disposent de centres culturels à Lima :</p>
<ul class="spip">
<li><a href="http://www.ccelima.org/web/" class="spip_out" rel="external">Centro Cultural Español</a> ;</li>
<li><a href="http://centrocultural.britanico.edu.pe/" class="spip_out" rel="external">Centro Peruano Británico</a> ;</li>
<li><a href="http://www.iiclima.esteri.it/IIC_Lima" class="spip_out" rel="external">Instituto Italiano di Cultura</a> ;</li>
<li><a href="http://www.icpna.edu.pe/" class="spip_out" rel="external">IPCNA</a> ;</li>
<li><a href="http://www.apj.org.pe/" class="spip_out" rel="external">Centro Peruano Japonés</a>.</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Sports</h3>
<p>Tous les sports peuvent être pratiqués au Pérou. Il existe trois clubs de golf à Lima, de nombreux clubs de tennis, de gymnastique, des piscines couvertes et à l’air libre, des clubs d’aérobic, d’arts martiaux, de football, basket, volley, etc., mais il faut parfois être parrainé pour devenir membre.</p>
<p>Les sports nautiques sont assez répandus (surf, planche à voile). Il est possible de se procurer les équipements nécessaires sur place mais ils sont assez onéreux.</p>
<p>Pour chasser, un permis de port d’arme est obligatoire et il faut une autorisation pour pouvoir importer une arme. Un permis de chasse est également exigé ; pour l’obtenir, il est nécessaire de présenter une demande à la direction de contrôle des armes, munitions et explosifs (DISCAMEC) du ministère de l’Intérieur en joignant les caractéristiques de l’arme. Les démarches peuvent quelquefois être longues.</p>
<p>Les périodes d’ouverture et de fermeture de la chasse sont variables selon les espèces.</p>
<p>Un permis n’est pas demandé pour pouvoir pêcher.</p>
<h3 class="spip"><a id="sommaire_4"></a>Télévision – Radio</h3>
<p>Il existe une quinzaine de chaînes de télévision au Pérou, deux réseaux câblés (Movistar TV, Claro TV) et un bouquet par satellite (DirecTV). Le câble et le satellite permet de capter de nombreuses chaînes étrangères (nord-américaines, japonaises, espagnoles, francophones, italiennes, allemandes). Les programmes sont variés. On peut ainsi suivre des opéras, des téléfilms comme des films relativement récents. A noter la diffusion de nombreux dessins animés sur les chaînes enfants.</p>
<p>L’achat et la location de DVD sont répandus.</p>
<p>RFI est retransmise par Radio Miraflores, sur CPN et DirecTV, tous les jours, le matin et en soirée. La qualité de réception en ondes courtes est bonne.</p>
<p>Des émissions françaises télévisées, magazines politiques et d’information, 20 heures de France 2, sont diffusées sur la chaîne francophone TV5 (disponible sur Movistar TV et Claro TV).</p>
<h3 class="spip"><a id="sommaire_5"></a>Presse française</h3>
<p>Les kiosques des grands hôtels proposent, avec retard, des revues et journaux français. Quelques libraires à Lima vendent des ouvrages en français : Euromatex et Vicens Vives (calle Schell 435 à Miraflores)</p>
<p>A noter que les démarches à accomplir pour dédouaner journaux, livres et cassettes sont assez longues (cinq à six semaines).</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/vie-pratique/article/loisirs-et-culture). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
