# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/emploi-stage/article/reglementation-du-travail-110056#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/emploi-stage/article/reglementation-du-travail-110056#sommaire_2">Les fêtes légales au Sénégal</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/emploi-stage/article/reglementation-du-travail-110056#sommaire_3">Emploi du conjoint</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/emploi-stage/article/reglementation-du-travail-110056#sommaire_4">Création d’entreprises</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<p>Les relations entre employeurs et travailleurs sont régies par la loi n°97-17 du 1er décembre 1997, la convention collective interprofessionnelle du 27 mai 1982 et les conventions collectives propres à chaque secteur d’activité.</p>
<p><a href="http://www.droit-afrique.com/images/textes/Senegal/Senegal%20-%20Code%20du%20travail.pdf" class="spip_out" rel="external">Le code du travail sénégalais</a> fixe certaines conditions à l’emploi des expatriés : leur recrutement est subordonné à une autorisation de la Direction du travail. Les professions libérales sont également inaccessibles aux expatriés mais il est possible, dans certains cas, de créer une société commerciale de nationalité sénégalaise.</p>
<p>Le recrutement des travailleurs de nationalité sénégalaise est libre mais doit être déclaré auprès du service de la main-d’œuvre ; les employés doivent être immatriculés auprès des organismes sociaux, qui sont :</p>
<ul class="spip">
<li>l’Institut de prévoyance retraite du Sénégal (IPRES) ;</li>
<li>la Caisse de sécurité sociale (CSS).</li></ul>
<p>En ce qui concerne les charges sociales sur salaires, il faut noter :</p>
<ul class="spip">
<li>les cotisations à titre de prestations familiales, qui se font à la CSS :
<br>— Taux employeur : 7 % (plafond annuel des salaires soumis à cotisation : 720 000 FCFA).</li>
<li>les cotisations retraites, qui se font à la CSS l’IPRES :
<br>— Régime rénéral : 8,4% (taux employeurs) et 5,6% (taux employés) avec un plafond annuel de 2 400 000 FCFA).
<br>— Régime complémentaire cadre : 3,6 % (taux employeurs) et 2,4% (taux employés) avec un plafond annuel de 7 200 000 FCFA.</li>
<li>les cotisations accidents du travail : elles peuvent varier selon la nature de l’activité et les risques d’accidents ou de maladies professionnelles. Elles se situent généralement entre 1,3 et 5% avec un plafond annuel de 720 000 FCFA.</li></ul>
<p>La durée légale du travail hebdomadaire est de 40 heures ; une majoration progressive du salaire est appliquée pour les heures supplémentaires. La législation sénégalaise fixe le salaire minimum interprofessionnel garanti (SMIG) à 209,10 FCFA l’heure. Le travailleur a droit aux congés payés à raison de 2 jours ouvrables par mois de service effectif, après une période de 12 mois.</p>
<p>Le régime juridique des différents types de contrats (à durée déterminée, indéterminée, contrat pour l’exécution d’un ouvrage ou d’une tâche déterminée, travail journalier ou hebdomadaire) est défini par le code du travail au Sénégal.</p>
<p>Deux formes d’engagement sont reconnues par le code : l’engagement écrit et l’engagement verbal.</p>
<h3 class="spip"><a id="sommaire_2"></a>Les fêtes légales au Sénégal</h3>
<ul class="spip">
<li>1er janvier : Jour de l’An</li>
<li>4 avril : Fête de l’Indépendance</li>
<li>Pâques et Lundi de Pâques</li>
<li>1er mai : Fête internationale du Travail</li>
<li>Ascension</li>
<li>Pentecôte et Lundi de Pentecôte</li>
<li>15 août : Assomption</li>
<li>1er novembre : Toussaint</li>
<li>25 décembre : Noël</li></ul>
<p>En outre, les fêtes musulmanes (Korité, Tabaski ou Fête du mouton, Tamkharit et Maouloud) sont célébrées selon le calendrier lunaire et donnent lieu à la prise d’un jour de repos à titre de jour férié.</p>
<h3 class="spip"><a id="sommaire_3"></a>Emploi du conjoint</h3>
<p>Le contrat de travail d’expatrié suit la procédure normale de validation par la Direction du travail.</p>
<p>Il n’y a pas d’autorisation spéciale pour un contrat local. Il est toutefois à noter que seules les candidatures pour les postes nécessitant des compétences spécifiques introuvables au Sénégal peuvent aboutir. La main d’œuvre sénégalaise reste prioritaire.</p>
<h3 class="spip"><a id="sommaire_4"></a>Création d’entreprises</h3>
<p>Il existe des structures d’aide à l’investissement au Sénégal comme l’Agence nationale chargée de la promotion des investissements et des grands travaux (APIX) et l’Agence de développement et d’encadrement des petites et moyennes entreprises (ADEPME).</p>
<p>Il existe au sein de l’APIX, un bureau d’appui à la création d’entreprise (<a href="http://www.creationdentreprise.sn/" class="spip_out" rel="external">BCE – APIX</a>) qui fournit toutes les informations relatives aux formalités de création d’entreprise au Sénégal.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/emploi-stage/article/reglementation-du-travail-110056). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
