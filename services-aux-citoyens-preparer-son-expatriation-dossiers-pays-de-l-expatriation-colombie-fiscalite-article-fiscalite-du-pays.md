# Fiscalité du pays

<p>La fiscalité colombienne se décompose en trois catégories :</p>
<ul class="spip">
<li>Les impôts nationaux qui comprennent : l’impôt sur le revenu et les impôts complémentaires, l’impôt sur les gains occasionnels et plus-values ainsi que l’IVA (équivalent de notre TVA).</li></ul>
<ul class="spip">
<li>Les impôts départementaux gérés localement, tels que : les droits d’enregistrement et les droits d’accises.</li></ul>
<ul class="spip">
<li>Les impôts municipaux : la taxe sur l’industrie et le commerce et les impôts fonciers</li></ul>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/fiscalite/article/fiscalite-du-pays#sommaire_1">Année fiscale</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/fiscalite/article/fiscalite-du-pays#sommaire_2">Impôt sur le revenu</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/fiscalite/article/fiscalite-du-pays#sommaire_3">TVA</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/fiscalite/article/fiscalite-du-pays#sommaire_4">Les zones franches </a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/fiscalite/article/fiscalite-du-pays#sommaire_5">Pour en savoir plus : </a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Année fiscale</h3>
<p>Du 1e janvier au 31 décembre.</p>
<h3 class="spip"><a id="sommaire_2"></a>Impôt sur le revenu</h3>
<p>L’impôt sur le revenu concerne aussi bien les personnes physiques que morales. Un grand nombre de dispositions relatives à la fiscalité des personnes morales sont étendues aux personnes physiques. Pour les personnes physiques les taux sont progressifs et réajustés annuellement. <strong>Le taux de l’impôt sur les contribuables peut aller jusqu’à 33%.</strong></p>
<h4 class="spip">La retenue à la source</h4>
<p>L’état colombien a adopté le mécanisme de la retenue à la source, qui lui permet de percevoir des recettes fiscales dès la réalisation d’un fait générateur d’impôt. Les retenues à la source s’appliquent notamment au transfert de tout paiement à l’étranger.</p>
<h4 class="spip">La détermination de l’impôt sur le revenu</h4>
<p>Il existe deux régimes de détermination de l’impôt : le régime du bénéfice réel et le régime du forfait.</p>
<h4 class="spip">Le régime du forfait</h4>
<p>La loi fiscale colombienne présume que le revenu net des contribuables d’un exercice ne doit pas être inférieur à 3% de leur patrimoine net au dernier jour de l’exercice fiscal antérieur. Chaque année les contribuables doivent donc calculer le revenu présumé et le revenu imposable ordinaire. L’impôt sur le revenu réellement dû résultera de l’application du taux de 33% au montant le plus élevé.</p>
<h4 class="spip">Déductions et crédits d’impôt</h4>
<p>Un résident peut déduire les intérêts hypothécaires payés en Colombie. Les travailleurs à faible revenu peuvent choisir de déduire les intérêts hypothécaires ou les frais d’assistance médicale et d’éducation. La plupart des dons sont déductibles des impôts. <strong>Il n’y a pas de régime fiscal particulier pour les expatriés. </strong></p>
<h4 class="spip">Barème de l’impôt</h4>
<p>Le système fiscal colombien est exprimé en unités de valeur fiscale (UVT). Le taux d’imposition sur les revenus est progressif et varie entre 0 et 33 % en fonction des UVT.</p>
<h3 class="spip"><a id="sommaire_3"></a>TVA</h3>
<p>L’IVA (Impuesto sobre el Valor Agregado) s’inspire du régime français de la TVA. L’IVA acquitté sur les achats est déductible de celui collectée sur les ventes. Toutes les personnes qui réalisent des actes de commerce ou des opérations assimilables sont responsables du recouvrement de l’IVA. Il s’applique à hauteur de 16% sur tous les biens et services qui ne bénéficient pas d’un tarif différencié. Des taux réduits sont appliqués sur le transport commercial aérien (10%) et sur les denrées alimentaires (7%). Sont exonérés les produits d’assurance, les produits liés aux soins médicaux, le crédit-bail de propriété pour le logement et le crédit-bail financier.</p>
<h3 class="spip"><a id="sommaire_4"></a>Les zones franches </h3>
<p>Un certain nombre de mécanismes juridiques destinés à promouvoir les exportations sont prévues par la législation colombienne. Il s’agit notamment des zones franches, zones géographiques délimitées du territoire national bénéficiant d’un régime douanier et fiscal spécial, dont l’objet principal est de promouvoir le processus d’industrialisation des biens et services destinés aux marchés externes. Les principaux avantages de ces régimes sont : la réduction du tarif de l’impôt sur le revenu des sociétés (15% au lieu de 33%), l’absence de TVA sur les matières premières et les équipements importés, l’absence de droits de douanes au moment de l’importation des marchandises, la réduction des tarifs des services publics.</p>
<h3 class="spip"><a id="sommaire_5"></a>Pour en savoir plus : </h3>
<ul class="spip">
<li><a href="http://www.dian.gov.co/" class="spip_out" rel="external">Direction des impôts de Colombie</a></li>
<li><a href="http://www.superfinanciera.gov.co" class="spip_url spip_out" rel="external">http://www.superfinanciera.gov.co</a></li>
<li><a href="http://www.monografias.com/trabajos46/impuestos-colombia/impuestos-colombia.shtml" class="spip_url spip_out" rel="external">http://www.monografias.com/trabajos...</a></li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/fiscalite/article/fiscalite-du-pays). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
