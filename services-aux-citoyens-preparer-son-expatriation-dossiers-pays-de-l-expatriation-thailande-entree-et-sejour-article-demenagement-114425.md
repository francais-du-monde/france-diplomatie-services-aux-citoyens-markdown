# Déménagement

<p>La voie maritime est la moins onéreuse. Il faut compter 800$ pour un conteneur 20’ et 1500$ pour un conteneur 40’. Le délai de transit est approximativement de 20-25 jours.</p>
<p>Par avion, le coût est élevé mais le délai n’est que d’une semaine. Le tarif pour le transport aérien avoisine les 7,50$ par kilo.</p>
<p>S’agissant d’un déménagement international, il est primordial de s’entourer d’un maximum de garanties en faisant appel à un professionnel disposant de certifications reconnues (marque NF Service, ISO 9002, etc.)</p>
<p><a href="http://www.csdemenagement.fr/" class="spip_out" rel="external">Chambre syndicale du déménagement</a> <br class="manualbr">Téléphone : 01 49 88 61 40 <br class="manualbr">Télécopie : 01 49 88 61 46<br class="manualbr">Courriel : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/entree-et-sejour/article/demenagement-114425#contact#mc#csdemenagement.fr#" title="contact..åt..csdemenagement.fr" onclick="location.href=mc_lancerlien('contact','csdemenagement.fr'); return false;" class="spip_mail">contact<span class="spancrypt"> [at] </span>csdemenagement.fr</a></p>
<p><i>Mise à jour : juillet 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/entree-et-sejour/article/demenagement-114425). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
