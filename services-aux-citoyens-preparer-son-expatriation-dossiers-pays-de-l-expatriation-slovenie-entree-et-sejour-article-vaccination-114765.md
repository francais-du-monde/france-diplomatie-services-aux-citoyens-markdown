# Vaccination

<p>Aucune vaccination n’est exigée à l’entrée du pays. Il est recommandé de mettre à jour la vaccination anti-diphtérique en raison du contexte épidémique de la région. Sont également conseillées pour des raisons médicales :</p>
<ul class="spip">
<li>chez l’adulte, mise à jour des vaccinations contre le tétanos et la poliomyélite ; vaccination contre l’hépatite A (à partir de 50 ans, une recherche préalable des anticorps sériques (lgG) est justifiée) et l’hépatite B en cas de séjour prolongé et/ou à risque.</li>
<li>chez l’enfant, vaccinations recommandées en France par le ministère de la Santé, en particulier pour les longs séjours : B.C.G. dès le premier mois, rougeole dès l’âge de neuf mois.</li></ul>
<p>Veuillez trouver ci-dessous les vaccinations recommandées d’un point de vue médical et basées sur des critères épidémiologiques :</p>
<p><strong>Systématiquement :</strong></p>
<ul class="spip">
<li>Mise à jour des vaccinations incluses dans <strong>le calendrier vaccinal français</strong><br class="manualbr">NB : chez les enfants, les vaccinations contre l’hépatite B et la tuberculose peuvent être réalisées dès la naissance et la vaccination contre la rougeole, la rubéole et les oreillons, dès l’âge de neuf mois.</li>
<li>Hépatite A* (pour les enfants : à partir de l’âge de un an)</li></ul>
<p><strong>En fonction de la durée et des modalités du séjour </strong> :</p>
<p>Encéphalite à tiques : pour des activités de plein air du printemps à l’automne. (pour les enfants : à partir de l’âge d’un ans ; dosage pédiatrique).</p>
<p><strong>En fonction de la saison et des facteurs de risques individuels</strong> :</p>
<p>Grippe : pour tous les adultes et enfants (à partir de six mois) faisant l’objet d’une recommandation dans le calendrier vaccinal français, participant à un voyage notamment en groupe, ou en bateau de croisière</p>
<p><strong>Disponibilité des vaccins sur place</strong></p>
<ul class="spip">
<li><strong>Diphtérie</strong> : oui.</li>
<li><strong>Hépatite A</strong> : oui.</li>
<li><strong>Hépatite B</strong> : oui.</li>
<li><strong>Méningite</strong> : oui.</li>
<li><strong>Poliomyélite</strong> : oui.</li>
<li><strong>Tétanos</strong> : oui.</li>
<li><strong>Typhoïde</strong> : oui.</li></ul>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Rubrique thématique <a href="services-aux-citoyens-preparer-son-expatriation-sante.md" class="spip_in">Santé</a>.</p>
<p><i>Mise à jour : septembre 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/entree-et-sejour/article/vaccination-114765). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
