# Règlementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/emploi-stage/article/reglementation-du-travail#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/emploi-stage/article/reglementation-du-travail#sommaire_2">Fêtes légales</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/emploi-stage/article/reglementation-du-travail#sommaire_3">Emploi du conjoint</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/emploi-stage/article/reglementation-du-travail#sommaire_4">Création d’entreprises – spécificités</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<p>La référence en matière de droit du travail est la loi sur les contrats de travail (<i>Employment Contracts Act</i>), adoptée en 1970, puis remaniée en juin 2001. Cette loi est complétée par les conventions collectives.</p>
<p>La législation finlandaise prévoit que <strong>la durée hebdomadaire de travail en Finlande est de 40 heures</strong>. Des conventions collectives conclues par branche d’activité permettent néanmoins d’y déroger. Ainsi, dans la majorité des cas le temps hebdomadaire de travail s’élève à 37h30. La durée des congés annuels payés est de cinq semaines (jours ouvrables).</p>
<p>Au-delà de 40 heures hebdomadaires de travail, l’employé a le droit à une compensation. La majoration des heures supplémentaires s’élève à 150 % du salaire horaire pour les deux premières heures, et à 200 % au-delà, dans une limite de 16 heures pour une période de deux semaines.</p>
<p>Un dimanche travaillé est rémunéré avec une prime de 100 % par rapport au salaire normal.</p>
<p>Les travailleurs ont droit au moins à deux jours de congé par mois pour chaque mois de travail. De manière générale, la majeure partie des travailleurs prend ses congés annuels en été, et environ une semaine en hiver.</p>
<p>Des renseignements sur les questions relatives à la relation de travail et à la protection des travailleurs : contrat de travail, salaire, heures de travail, congés etc. sont fournis par les districts de la protection des travailleurs.</p>
<p>La durée des congés maladie payés varie selon les conventions collectives. Toutefois, l’organisme de sécurité sociale qui reverse à l’employeur des indemnités journalières (l’employeur est en effet tenu, dans cette situation, d’assurer le paiement de l’intégralité du salaire), cesse ses virements après une année d’arrêt maladie. Il s’agit là, de facto, d’une durée maximum.</p>
<p>En Finlande, le congé parental dure 263 jours. Les 106 jours de congé maternité ne sont accordés qu’à la mère. Les 158 jours suivants peuvent être au choix pour la mère ou pour le père. Un père peut bénéficier de 6 à 12 jours d’un congé de paternité au moment de la naissance plus 6 autres jours au moment qu’il jugera opportun.</p>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li><a href="http://www.te-services.fi/te/en/index.html" class="spip_out" rel="external">Te-services.fi</a></li>
<li><a href="http://www.tem.fi/?l=en" class="spip_out" rel="external">Tem.fi</a></li>
<li><a href="http://www.tyosuojelu.fi/fi/workingfinland/" class="spip_out" rel="external">Tyosuojelu.fi</a></li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Fêtes légales</h3>
<ul class="spip">
<li><strong>1er janvier </strong> : Nouvel an,</li>
<li><strong>6 janvier</strong> : Epiphanie,</li>
<li>Lundi de Pâques,</li>
<li><strong>1er mai</strong> : Fête du Travail,</li>
<li>Jeudi de l’Ascension,</li>
<li>Lundi de Pentecôte,</li>
<li><strong>23 et 24 juin</strong> : Saint Jean,</li>
<li>la Toussaint,</li>
<li><strong>6 décembre</strong> : Fête de l’Indépendance,</li>
<li><strong>25 décembre</strong> : Noël,</li>
<li><strong>26 décembre</strong> : Saint Etienne.</li></ul>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Voir aussi : <a href="http://www.tv5.org/TV5Site/voyageurs/accueil.php" class="spip_out" rel="external">TV5.org</a></p>
<h3 class="spip"><a id="sommaire_3"></a>Emploi du conjoint</h3>
<p>Les possibilités d’emploi au sein des entreprises françaises sont rares. L’emploi dans un groupe international où l’anglais est la langue de travail est en revanche plus aisé, la pratique du finnois restant indispensable pour tout autre type d’emploi.</p>
<p>Les salaires minimaux sont fixés dans le cadre des conventions collectives par branche sectorielle. Les salaires en Finlande sont dans la moyenne des pays de l’OCDE.</p>
<p>Pour des informations complémentaires, se reporter aux informations relatives à l’emploi.</p>
<h3 class="spip"><a id="sommaire_4"></a>Création d’entreprises – spécificités</h3>
<p>En Finlande la conclusion d’un contrat de travail ne requiert aucune forme spécifique, il peut être écrit ou oral. Toutefois, lorsque celui-ci est conclu pour une durée supérieure à un mois, l’employeur est tenu d’informer le salarié, par écrit, des conditions de travail (lieu, durée, salaire, etc.).</p>
<p>Pour les démissions et les licenciements, les délais de préavis varient en fonction de la durée de la relation de travail ; ils sont fixés par la loi (<i>Employment Contracts Act</i>) ou les dispositions contractuelles. Au minimum, l’employeur est par exemple tenu de respecter le délai d’un mois, ce délai est porté à six mois après 15 années de service. Il n’existe pas d’indemnité de fin de contrat. La seule obligation de l’employeur est de régulariser les droits à congés et l’indemnité annuelle de congé.</p>
<p><i>Mise à jour : janvier 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/emploi-stage/article/reglementation-du-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
