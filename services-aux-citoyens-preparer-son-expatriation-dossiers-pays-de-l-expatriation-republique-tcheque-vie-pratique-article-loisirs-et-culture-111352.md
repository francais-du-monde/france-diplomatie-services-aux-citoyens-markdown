# Loisirs et culture

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/vie-pratique/article/loisirs-et-culture-111352#sommaire_1">Tourisme</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/vie-pratique/article/loisirs-et-culture-111352#sommaire_2">Activités culturelles</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/vie-pratique/article/loisirs-et-culture-111352#sommaire_3">Sports</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/vie-pratique/article/loisirs-et-culture-111352#sommaire_4">Télévision – Radio</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/vie-pratique/article/loisirs-et-culture-111352#sommaire_5">Presse française</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Tourisme</h3>
<p>L’ancienne ville royale de Prague a été inscrite au patrimoine mondial par l’Unesco. Sa diversité architecturale exceptionnelle (mêlant les styles roman, gothique, renaissance, baroque, art nouveau…) qui se découvre au fil d’innombrables ruelles et passages, en font une destination très prisée : Prague accueille chaque année quelque trois millions de touristes.</p>
<p>La Bohême du sud abrite une réserve naturelle et de nombreux châteaux.</p>
<p>Il existe également des sites naturels pittoresques dans les régions montagneuses : Mont des Géants, Jizerske, Hory, Beskydy, Sumava …</p>
<p>Pour plus d’informations :</p>
<p><a href="http://www.czechtourism.com/" class="spip_out" rel="external">Office national tchèque du tourisme</a> <br class="manualbr">18, rue Bonaparte<br class="manualbr">75006 Paris<br class="manualbr">Tél. : 01.53.73.00.32<br class="manualbr">Fax : 01 53 73 00 33<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/vie-pratique/article/loisirs-et-culture-111352#info-fr#mc#czechtourism.com#" title="info-fr..åt..czechtourism.com" onclick="location.href=mc_lancerlien('info-fr','czechtourism.com'); return false;" class="spip_mail">Courriel</a></p>
<h3 class="spip"><a id="sommaire_2"></a>Activités culturelles</h3>
<h4 class="spip">Activités culturelles en français</h4>
<p>Des manifestations culturelles sont organisées par le service culturel et l’Institut français (conférences, films, pièces de théâtre, spectacles de danse et concerts), parfois en association avec des partenaires tchèques. Les cinq Alliances françaises établies dans le pays à Brno, Ostrava, Liberec, Pardubice et Zlin organisent également des évènements culturels.</p>
<p><a href="http://www.ifp.cz/" class="spip_out" rel="external">Institut français</a> <br class="manualbr">Stepanska 35 <br class="manualbr">P.O. Box 850 <br class="manualbr">111 21 Praha 1<br class="manualbr">Tél : (420 2) 21 40 10 11<br class="manualbr">Fax : (420 2) 22 23 05 79</p>
<p><a href="http://www.alliancefrancaise.cz/" class="spip_out" rel="external">Coordination des Alliances françaises en République tchèque</a> <br class="manualbr">Institut français de Prague<br class="manualbr">Štepánská 35 - 111 21 Praha 1<br class="manualbr">Tél : (+420) 221 401 004<br class="manualbr">Fax : (+420) 222 230 576</p>
<h4 class="spip">Activités culturelles locales</h4>
<p>Prague offre une vie culturelle riche, notamment dans le domaine musical. Le point culminant de la saison est le Printemps de Prague, festival (mai-juin) qui met à l’honneur les grands compositeurs classiques tchèques (Dvorak, Janacek, Smetana, Martinu) mais la ville propose toute l’année une multitude de concerts classiques, aussi bien dans des lieux prestigieux que dans des petites églises de quartier.</p>
<p>Les deux chaînes publiques de télévision (CT1 et CT2) et les deux chaînes privées (Nova TV et Prima TV) produisent des programmes de qualité, en particulier des documentaires. Sur le réseau numérique hertzien, la chaîne publique CT 24 propose de l’information en continu. Il est également possible de capter des chaînes étrangères par satellite (Astra, Eutelsat et Telecom 2). Le système adopté est PAL. Il existe de nombreux clubs vidéo.</p>
<p>Le théâtre joue également un rôle important et tient une place prépondérante dans la culture tchèque. La grande majorité des pièces sont en langue tchèque.</p>
<p>A Prague, une cinquantaine de salles de cinéma projette essentiellement des films américains grand public.</p>
<p>Des programmes très diversifiés sont proposés par Radio Zurnal et <a href="http://www.radio.cz/en" class="spip_out" rel="external">Radio Praha</a>.</p>
<p>Les bibliothèques sont nombreuses à Prague et dans le reste du pays.</p>
<p>Deux publications : "Stepanska 35" (revue trimestrielle éditée par l’Institut Français) et <a href="http://www.praguepost.com/" class="spip_out" rel="external">Prague Post</a> (hebdomadaire en anglais), sont une mine d’informations sur les manifestations locales.</p>
<p>Pour plus d’information sur les manifestations et lieux culturels en République tchèque, consulter la rubrique "leisure" du site <a href="http://www.czech.cz/" class="spip_out" rel="external">Welcome to the Czech Republic</a>.</p>
<h3 class="spip"><a id="sommaire_3"></a>Sports</h3>
<p>De nombreux sports collectifs ou individuels peuvent être pratiqués dans des clubs. Les rencontres sportives sont fréquentes.</p>
<p>La pratique de la chasse nécessite un permis de port d’arme que l’on doit se procurer auprès du consulat de la République tchèque à Paris. Il conviendra par la suite de demander un permis de chasse au service de la protection de l’environnement de la préfecture du district (Obvodni Rurad). La chasse est ouverte, de façon générale, du début du mois d’août à la fin de l’année. Les périodes diffèrent légèrement selon le type de gibier.</p>
<p>La pêche nécessite également un permis qui s’obtient soit auprès de l’Union des pêcheurs à Prague, soit dans les districts.</p>
<h3 class="spip"><a id="sommaire_4"></a>Télévision – Radio</h3>
<p>A Prague, Radio France Internationale (RFI) peut-être captée sur 99.3 FM. </p>
<p>TV5 est accessible sur la plupart des réseaux câblés et peut être également reçue par satellite en réception directe.</p>
<p><a href="http://www.radio.cz/fr" class="spip_out" rel="external">Radio Prague</a>, radio en ligne, diffuse des informations de qualité en français.</p>
<h3 class="spip"><a id="sommaire_5"></a>Presse française</h3>
<p>La presse française est disponible en kiosque et dans les grands magasins.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/vie-pratique/article/loisirs-et-culture-111352). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
