# Présence française

<h4 class="spip">Autorités françaises dans le pays</h4>
<h4 class="spip">Ambassade et consulat de France </h4>
<p>Consultez notre article <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/article/annuaire-des-representations-francaises-a-l-etranger" class="spip_in">Annuaire des représentations françaises à l’étranger</a>.</p>
<p>A noter que seuls les consulats généraux de France à Genève et à Zurich sont compétents pour tout ce qui concerne les formalités administratives des Français (inscription au registre des Français établis hors de France, délivrance de documents d’identité, état civil, etc.…).</p>
<h4 class="spip">Réseau français de coopération et d’action culturelle</h4>
<p>Les coordonnées des services du réseau de coopération et d’action culturelle français en Suisse ainsi que la description de l’activité de ces services se trouvent sur <a href="http://www.latitudefrance.org/AnnuaireCarto.html" class="spip_out" rel="external">Latitude France</a>.</p>
<h4 class="spip">Economie : réseau mondial</h4>
<p>Les <a href="http://www.tresor.economie.gouv.fr/Pays/suisse" class="spip_out" rel="external">services économiques</a>, émanation de la direction générale du Trésor, sont présents à l’ambassade de France en Suisse. Ils ont pour missions principales l’analyse macroéconomique et l’animation des relations économiques bilatérales entre la France et les pays où ils sont implantés.</p>
<p>La <a href="http://export.businessfrance.fr/suisse/export-suisse-avec-notre-bureau.html" class="spip_out" rel="external">mission économique Business France en Suisse</a> a pour mission d’aider les entreprises françaises à se développer dans le pays.</p>
<h4 class="spip">Vos élus à l’étranger</h4>
<p>Pour toute information et pour connaître les conseillers AFE, les députés et les sénateurs qui représentent les Français établis hors de France dans votre circonscription, vous pouvez consulter les sites Internet suivants :</p>
<ul class="spip">
<li><a href="http://www.assemblee-afe.fr" class="spip_out" rel="external">l’Assemblée des Français de l’étranger (AFE)</a></li>
<li><a href="http://www.expatries.senat.fr" class="spip_out" rel="external">le Sénat au service des Français de l’étranger</a></li>
<li><a href="http://www.senat.fr/expatries/dossiers_pays/suisse.html" class="spip_out" rel="external">dossier spécifique à la Suisse sur le site du Sénat</a></li></ul>
<h4 class="spip">Députés des Français de l’étranger</h4>
<p>Les Français expatriés peuvent élire leur député à l’Assemblée nationale. Ils sont répartis en onze circonscriptions législatives. Pour plus d’information, consulter la page dédiée aux <a href="http://www.assemblee-nationale.fr/14/qui/circonscriptions/099.asp" class="spip_out" rel="external">députés des Français établis hors de France sur le site de l’Assemblée nationale</a>.</p>
<h4 class="spip">Associations dans le pays</h4>
<h4 class="spip">Association des Français en Suisse (AFS) : section suisse des français du Monde - ADFE</h4>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.ambafrance-ch.org/Association-des-Francais-en-Suisse" class="spip_out" rel="external">Coordonnées détaillées des différents groupes régionaux de l’AFS</a> sur le site de l’Ambassade de France en Suisse.</p>
<h4 class="spip">Français de Suisse : section suisse de l’Union des Français de l’étranger (UFE)</h4>
<ul class="spip">
<li><a href="http://www.francaisdesuisse.ch" class="spip_out" rel="external">Site internet de la section suisse de l’UFE</a></li>
<li><a href="http://www.ambafrance-ch.org/Union-des-Francais-de-l-Etranger,323" class="spip_out" rel="external">Coordonnées détaillées des différents groupes régionaux de l’UFE Suisse</a> sur le site de l’Ambassade de France en Suisse.</li></ul>
<h4 class="spip">Fédération internationale des accueils français et francophones à l’étranger (FIAFE)</h4>
<ul class="spip">
<li><a href="http://www.berneaccueil.ch" class="spip_out" rel="external">Berne Accueil</a></li>
<li><a href="http://www.geneve-accueil.org" class="spip_out" rel="external">Genève Accueil</a></li>
<li><a href="http://www.lausanne-accueil.org" class="spip_out" rel="external">Lausanne Accueil</a></li>
<li><a href="http://www.zurichaccueil.ch" class="spip_out" rel="external">Zurich Accueil</a></li></ul>
<p><a href="http://www.cfsci.ch" class="spip_out" rel="external">Chambre franco-suisse pour le commerce et l’industrie</a></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Siège de Genève</strong><br class="manualbr">5 Route de Chêne - Case postale 6298 - 1211 Genève 6<br class="manualbr">Téléphone : [41] (0)22 849 05 70 - Télécopie : [41] (0)22 7301 33<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/presence-francaise/#info#mc#cfsci.ch#" title="info..åt..cfsci.ch" onclick="location.href=mc_lancerlien('info','cfsci.ch'); return false;" class="spip_mail">Courriel</a></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Antenne de Zurich</strong><br class="manualbr">Rennweg 42 - Postfach 2170 - 8022 Zurich<br class="manualbr">Téléphone : [41] (0)44262 10 70 <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/presence-francaise/#infozurich#mc#csfci.ch#" title="infozurich..åt..csfci.ch" onclick="location.href=mc_lancerlien('infozurich','csfci.ch'); return false;" class="spip_mail">Courriel</a></p>
<p><a href="http://www.cafs.ch" class="spip_out" rel="external">Cercle d’affaires franco-suisse (CAFS)</a><br class="manualbr">Kappelergasse 15 - Case postale 2400 - 8022 Zurich 1<br class="manualbr">Téléphone : [41] (0)43 244 99 10 <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/presence-francaise/#office#mc#cafs.ch#" title="office..åt..cafs.ch" onclick="location.href=mc_lancerlien('office','cafs.ch'); return false;" class="spip_mail">Courriel</a></p>
<p><strong>Pour en savoir plus</strong></p>
<ul class="spip">
<li>Lire notre article thématique <a href="services-aux-citoyens-preparer-son-expatriation-en-savoir-plus-article-associations-des-francais-de-l-etranger.md" class="spip_in">Associations des Français de l’étranger</a></li>
<li><a href="http://www.ambafrance-ch.org/-Associations-francaises-en-Suisse-" class="spip_out" rel="external">Liste complète des associations françaises</a> présentes en Suisse sur le site de l’ambassade de France en Suisse</li>
<li>Associations françaises présentes dans les cantons de Genève, de Fribourg, du Jura, de Neuchâtel, du Valais et de Vaud sur le site du <a href="http://www.consulfrance-geneve.org/" class="spip_out" rel="external">consulat de France à Genève</a></li>
<li><a href="http://www.consulatfrance-zurich.org/Associations,320" class="spip_out" rel="external">Associations françaises</a> présentes dans les cantons d’Argovie, Appenzell, Bâle-Ville et Bâle-Campagne, Berne, Glaris, Grisons, Lucerne, Nidwald, Obwald, Saint-Gall, Schaffhouse, Schwyz, Soleure, Tessin, Thurgovie, Uri, Zoug et Zurich sur le site du consulat de France à Zurich.</li></ul>
<p><i>Mise à jour : février 2013</i></p>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/presence-francaise/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
