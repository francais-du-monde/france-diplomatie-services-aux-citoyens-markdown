# Passeport, visa, permis de travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/entree-et-sejour/article/passeport-visa-permis-de-travail#sommaire_1">Pour un séjour de moins de trois mois</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/entree-et-sejour/article/passeport-visa-permis-de-travail#sommaire_2">Pour un séjour de plus de trois mois</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/entree-et-sejour/article/passeport-visa-permis-de-travail#sommaire_3">Visa d’affaires</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/entree-et-sejour/article/passeport-visa-permis-de-travail#sommaire_4">Carte de séjour ou de résident malgache</a></li></ul>
<p>Quel que soit le motif de votre séjour, renseignez-vous auprès de la section consulaire de l’ambassade de Madagascar à Paris qui vous informera sur la règlementation en matière d’entrée et de séjour à Madagascar, règlementation que vous devrez impérativement respecter. N’hésitez pas à consulter le site ministère de l’Intérieur malgache afin de vous familiariser avec les modalités d’entrée et de séjour dans le pays.</p>
<p><strong>En aucun cas, vous n’êtes autorisé à travailler à Madagascar sans permis adéquat.</strong></p>
<p>Le consulat de France à Tananarive n’est pas l’organisme compétent pour répondre à vos demandes d’information concernant votre séjour dans le pays.</p>
<p>Les informations suivantes sont données à titre indicatif.</p>
<p>La durée de séjour mentionnée sur le visa d’entrée doit impérativement être respectée. Tout dépassement de cette durée peut entraîner une mesure de rétention administrative, sinon une peine d’emprisonnement, suivie d’une expulsion de Madagascar.</p>
<h3 class="spip"><a id="sommaire_1"></a>Pour un séjour de moins de trois mois</h3>
<p>Outre un passeport en cours de validité (et dont la durée de validité doit impérativement être, au moment de l’entrée sur le sol malgache d’au moins six mois après la date de retour prévue), <strong>le visa d’entrée et de séjour est obligatoire</strong>. Il peut être sollicité à l’arrivée à l’aéroport de Tananarive. Ce visa de séjour dit de non-immigration inférieur à trois mois est prorogeable auprès du ministère de l’intérieur <strong>à titre tout à fait exceptionnel</strong> et dûment motivé et pour une période cumulée n’excédant pas trois mois. <strong>Ce visa n’est pas transformable en visa de long séjour</strong> et ne donne pas droit à l’exercice d’une activité rémunérée ou lucrative.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.ambassade-madagascar.com/" class="spip_out" rel="external">Ambassade de Madagascar en France</a></p>
<h3 class="spip"><a id="sommaire_2"></a>Pour un séjour de plus de trois mois</h3>
<p>Si vous souhaitez vous rendre à Madagascar en qualité d’immigrant pour une durée supérieure à trois mois, vous devez être titulaire d’un visa d’entrée et de séjour d’un mois prorogeable et transformable, délivré soit par les représentations diplomatiques ou consulaires malgaches, soit exceptionnellement par correspondance auprès du ministère des Affaires étrangères.</p>
<p>L’octroi d’un visa d’immigration relève de la compétence du ministère de l’Intérieur. Vous devez souscrire, dans les sept jours ouvrables suivant votre arrivée, une déclaration d’identité et de nationalité auprès de l’autorité administrative du lieu où vous fixez votre résidence. Cette formalité est obligatoire pour toutes les personnes de plus de quinze ans. Par ailleurs, vous devez verser au Trésor ou à une caisse publique un cautionnement égal au montant du prix d’un billet d’avion pour le retour en France. A titre exceptionnel, une dispense de versement de cautionnement peut être délivrée par le ministère de l’Intérieur.</p>
<p>Sont également nécessaires :</p>
<ul class="spip">
<li>carnet international de vaccination ;</li>
<li>un extrait de casier judiciaire de moins de six mois ;</li>
<li>la carte de séjour pour tous les Français immigrants de plus de vingt-et-un ans.</li></ul>
<p>En outre, vous devrez vous conformer aux lois et règlements concernant les diverses activités professionnelles et formuler une demande de renouvellement de votre visa, trois mois avant l’expiration du délai qui vous a été accordé.</p>
<p>Le Français titulaire d’un visa de long séjour qui devient ainsi résident doit posséder également une carte de séjour. Le visa de long séjour vaut visa permanent de sortie et de retour pendant la durée de sa validité. Le fait de contracter un mariage avec une personne résidant à Madagascar ne confère pas automatiquement le droit de s’établir dans ce pays au-delà du temps de séjour accordé. Il est alors nécessaire d’introduire une nouvelle demande en indiquant cette nouvelle situation matrimoniale.</p>
<p>Pour toute précision, il est indispensable de prendre l’attache des services consulaires malgaches.</p>
<h3 class="spip"><a id="sommaire_3"></a>Visa d’affaires</h3>
<p>Les hommes d’affaires dont les activités nécessitent des déplacements fréquents à Madagascar peuvent demander, soit auprès des représentations diplomatiques ou consulaires malgaches, soit auprès du ministère de l’Intérieur, des visas permanents d’entrée et sortie, valables pour trois ans au plus. La durée du séjour à chaque entrée ne doit cependant pas excéder trois mois.</p>
<h3 class="spip"><a id="sommaire_4"></a>Carte de séjour ou de résident malgache</h3>
<p>La loi n° 62-006 du 6 juin 1962 dispose que : "Tout étranger, s’il doit séjourner à Madagascar pour une période de plus de trois mois, doit être muni d’une carte de séjour délivrée par le ministère de l’Intérieur".</p>
<p>Cette carte est exigée en de nombreuses circonstances (établissement d’une carte grise, ouverture d’un compte bancaire, inscription au registre du commerce…).</p>
<p>Outre les pièces communes exigées (photos, demande motivée, formulaire de renseignements, extrait de casier judiciaire, photocopie de passeport et de visa…), les <strong>travailleurs salariés </strong>doivent produire un certain nombre de pièces complémentaires :</p>
<ul class="spip">
<li>autorisation de travail délivrée à Madagascar par le ministère du Travail ;</li>
<li>attestation d’emploi de l’employeur à Madagascar ;</li>
<li>carte de numéro d’identification fiscale ;</li>
<li>photocopie de la carte d’identité nationale ou carte d’identité d’étranger ;</li>
<li>attestation de paiement d’impôts (renouvellements).</li></ul>
<p>D’autres documents seront demandés en fonction de la classe socio-professionnelle ou du type d’immigration retenu.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.ambafrance-mada.org/Sejour-inferieur-ou-superieur-a-3,1796" class="spip_out" rel="external">Ambassade de France à Madagascar</a></p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/entree-et-sejour/article/passeport-visa-permis-de-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
