# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/vie-pratique/article/transports#sommaire_1">Importation de véhicule</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/vie-pratique/article/transports#sommaire_2">Permis de conduire</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/vie-pratique/article/transports#sommaire_3">Code de la route</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/vie-pratique/article/transports#sommaire_4">Assurances et taxes</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/vie-pratique/article/transports#sommaire_5">Achat et location</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/vie-pratique/article/transports#sommaire_6">Entretien</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/vie-pratique/article/transports#sommaire_7">Réseau routier</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/vie-pratique/article/transports#sommaire_8">Transports en commun</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Importation de véhicule</h3>
<p>Il est déconseillé d’importer son véhicule, les garagistes japonais font des difficultés à assurer la maintenance ou effectuer des réparations s’ils ne sont pas les vendeurs de la voiture. De plus des normes locales sont exigées à l’entrée : résistance aux chocs, éclairage, position des feux, rétroviseurs, etc. Les droits de douanes sont par ailleurs importants.</p>
<h3 class="spip"><a id="sommaire_2"></a>Permis de conduire</h3>
<p>Le permis international délivré en France n’est pas reconnu au Japon. Depuis le 1er octobre 1994, le permis de conduire français est reconnu au Japon et son titulaire est <strong>autorisé à conduire pendant un an avec ce permis accompagné d’une traduction certifiée en japonais </strong>qui peut-être obtenue auprès du Consulat de France ou auprès des services de la <a href="http://www.jaf.or.jp/" class="spip_out" rel="external">JAF</a> (Japan Automobile Federation).</p>
<p>Cette traduction sera demandée en cas de contrôle de police et les visiteurs français qui n’en sont pas munis ne peuvent pas louer de véhicule. Passé ce délai d’un an, il convient d’effectuer une demande de permis de conduire japonais. A Tokyo, il est délivré au bureau du permis de conduire :</p>
<p><strong>Samezu Shiken-Jo</strong><br class="manualbr">1-12-5, Higashi-Oi<br class="manualbr">Schinagawu-Ku<br class="manualbr">Tokyo 140, <br class="manualbr">Tél. : (81) 3 3474 1374.</p>
<p>Les principales différences entre le Japon et la France pour l’obtention du permis de conduire :</p>
<ul class="spip">
<li>les leçons et épreuves de code et de conduite ont lieu simultanément ;</li>
<li>la validité du permis au Japon est de trois ans, renouvelable automatiquement auprès de la préfecture après participation à un cours obligatoire, si le titulaire n’a pas eu d’accident. Dans ce cas, il devra alors repasser des tests avant d’obtenir le renouvellement.</li></ul>
<p>Plus d’informations sur l’échange de permis de conduire sont disponibles sur le site : <a href="http://www.keishicho.metro.tokyo.jp/foreign/submenu.htm" class="spip_out" rel="external">Metropolitan Police Department</a>.</p>
<h3 class="spip"><a id="sommaire_3"></a>Code de la route</h3>
<p>L’utilisation du véhicule personnel est possible. Les places de parking doivent être respectées. La conduite s’effectue à gauche et la priorité est à gauche. La vitesse en ville est limitée entre 20 et 50-40 km/h selon la largeur des rues, 80-60 km/h sur routes, 100 km/h sur autoroutes.</p>
<h3 class="spip"><a id="sommaire_4"></a>Assurances et taxes</h3>
<p>Un contrôle périodique (<i>Shaken</i>), très cher, doit être effectué tous les deux ans pour les véhicules de moins de 10 ans et tous les ans pour les véhicules de plus de 10 ans.</p>
<p>Toute personne propriétaire d’un véhicule à moteur est tenue de souscrire une assurance gouvernementale obligatoire, destinée à indemniser les dommages corporels causés aux tiers victimes d’accident de la circulation.</p>
<p>Le montant de l’indemnisation est limité à un certain seuil. Ce montant peut s’avérer totalement insuffisant en cas de très graves dommages corporels ; c’est pourquoi il est recommandé, voire indispensable de souscrire une assurance complémentaire.</p>
<p><strong>Deux contrats sont alors possibles :</strong></p>
<ul class="spip">
<li>Garantie "PAP" (Package Automobile Policy) : contrat "Responsabilité Civile" étendue, recommandé si la valeur commerciale du véhicule est peu élevée ;</li>
<li>Garantie "SAP" (Spécial Automobile Policy) : assurance "Tous risques dommages" soit couverture PAP plus dommage au véhicule assuré. Les conditions et les tarifs d’assurance sont fixés et imposés par le ministère des finances.</li></ul>
<p>Le coût de la vignette auto est de 40 000 à 160 000 yens suivant la cylindrée.</p>
<h3 class="spip"><a id="sommaire_5"></a>Achat et location</h3>
<h4 class="spip">Achat</h4>
<p>Toutes les marques de voitures européennes et américaines sont représentées au Japon. Il est donc possible d’acquérir une voiture française en s’adressant à un concessionnaire de la marque désirée. Le règlement se fait en espèces ou par virement. Les marques japonaises (Nissan, Honda, Toyota…) sont bien sûr très présentes.</p>
<p>Il est possible d’acheter une voiture d’occasion, dans les garages et chez les revendeurs spécialisés, à partir de 400 000 yens en moyenne.</p>
<h3 class="spip"><a id="sommaire_6"></a>Entretien</h3>
<p>Le prix des pièces détachées est multiplié par trois sans compter la main-d’œuvre ; le délai pour se les procurer est de trois à quatre semaines.</p>
<h3 class="spip"><a id="sommaire_7"></a>Réseau routier</h3>
<p>La circulation sur le territoire est totalement libre. L’état général du réseau routier est bon, bien que les voies, y compris les autoroutes, y soient étroites et encombrées. On peut conduire son véhicule si l’on sait lire un plan ou une carte routière, les indications en dehors de Tokyo étant rédigées en idéogrammes. Outre les autoroutes, de nombreuses routes sont également à péage.</p>
<h3 class="spip"><a id="sommaire_8"></a>Transports en commun</h3>
<p>Les Japonais n’utilisent pas leur véhicule pour les longues distances.</p>
<p>L’autocar est le moyen de transport le moins coûteux, mais aussi le plus lent.</p>
<p>L’avion constitue le moyen le plus rapide pour se déplacer entre les grandes villes, mais les aéroports sont éloignés des centres villes. Depuis 2011, un certain nombre de compagnies aériennes low cost ont été créés qui proposent des vols à des prix tout à fait avantageux.</p>
<p>Différentes liaisons sont possibles entre l’aéroport international de Narita et le centre de Tokyo (60 km) :</p>
<ul class="spip">
<li>par le train : Sky Liner « Narita – Ueno » : 2 400 yens le trajet.</li>
<li>par le bus : limousine bus qui dessert les grands hôtels de Tokyo : 3 000 yens le trajet.</li></ul>
<p>Le réseau ferroviaire est moderne et dense, villes et villages sont bien desservis. C’est le moyen de transport le plus commode, les gares étant situées dans le centre des villes.</p>
<p>Les Shinkansen ou “trains super express” proposent des liaisons ultrarapides et extrêmement fiables dans tout le pays. Le coût d’un aller et retour Tokyo-Osaka est de 30 000 yens (236 €).</p>
<p>Cependant, il existe plusieurs types de <i>pass</i> ferroviaires, notamment le <strong>Japan Rail Pass</strong>, réservé aux touristes, qui est valable pour un trajet illimité sur le réseau des chemins de fer du Japon (JR) pour une période de 7 à 21 jours. Et est très vite rentabilisé.</p>
<p>Pour plus d’informations, merci de consulter les liens suivants :</p>
<ul class="spip">
<li><a href="http://www.japan-guide.com/e/e2361.html" class="spip_out" rel="external">Japan-guide.com</a></li>
<li><a href="http://www.japan-guide.com/e/e2357.html" class="spip_out" rel="external">Japan-guide.com</a></li>
<li><a href="http://www.japanrailpass.net/" class="spip_out" rel="external">Japanrailpass.net</a></li></ul>
<p>En ville, les modes de transport sont le métro (à Tokyo, très efficace et rapide), l’autobus et le taxi (onéreux).</p>
<ul class="spip">
<li>coût d’un billet de métro : entre 160 et 290 yens selon la distance, site : <a href="http://www.tokyometro.jp/en/index.html" class="spip_out" rel="external">http://www.tokyometro.jp/en/index.html</a></li>
<li>coût du transport en autobus : 200 yens,</li>
<li>coût du transport en taxi : prise en charge d’environ 650/700 yens (les deux premiers kilomètres).</li></ul>
<p>Concernant le métro tokyoïte, il existe des <i>pass</i> permettant de simplifier les trajets. Il existe une carte mensuelle, trimestrielle ou semestrielle (Teiki-ken), proposée aux salariés et valide pour le seul trajet domicile-lieu de travail. Le coût de la carte mensuelle, équivaut grosso modo à une vingtaine d’aller-retour. Il faudra donc compter entre 10 000 yens et 20 000 yens par mois en fonction de la distance parcourue.</p>
<p>Couplée à un porte-monnaie électronique semblable au passe Navigo de la RATP, cette carte permet un gain de temps non négligeable lors des correspondances entre deux lignes de compagnies différentes en évitant à son titulaire le passage obligé aux machines de vente automatiques.</p>
<p>Il existe également des pass (suica et pasmo) qui permettent d’éviter de faire l’appoint à chaque station.</p>
<p>Pour plus de renseignements, consulter le site de l’<a href="http://www.tourisme-japon.fr/Organisez-votre-voyage" class="spip_out" rel="external">office du tourisme</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/vie-pratique/article/transports). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
