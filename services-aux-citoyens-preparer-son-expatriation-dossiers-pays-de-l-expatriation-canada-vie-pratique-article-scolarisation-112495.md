# Scolarisation

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/vie-pratique/article/scolarisation-112495#sommaire_1">Les établissements scolaires français au Canada</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/vie-pratique/article/scolarisation-112495#sommaire_2">Système éducatif québécois</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/vie-pratique/article/scolarisation-112495#sommaire_3">Enseignement supérieur</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Les établissements scolaires français au Canada</h3>
<p>Pour toute information sur la scolarisation dans le système français à l’étranger, vous pouvez consulter notre thématique sur les <a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures.md" class="spip_in">Etudes supérieures</a> et la <a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais.md" class="spip_in">Scolarité en français</a>.</p>
<p>Vous y trouverez des renseignements sur :</p>
<ul class="spip">
<li>les établissements français du primaire et du secondaire à l’étranger ;</li>
<li>les bourses scolaires et la prise en charge des frais de scolarité à l’étranger ;</li>
<li>les possibilités qui s’offrent à vous si votre enfant ne peut être scolarisé à l’étranger dans le système français (enseignement à distance par le CNED,</li>
<li>programme français langue maternelle (FLAM), internats en France) ;</li>
<li>les épreuves du baccalauréat à l’étranger ;</li>
<li>les bourses d’études supérieures en France et à l’étranger ;</li>
<li>l’équivalence des diplômes.</li></ul>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.aefe.fr/" class="spip_out" rel="external">Agence pour l’enseignement français à l’étranger</a></p>
<h3 class="spip"><a id="sommaire_2"></a>Système éducatif québécois</h3>
<p><strong>Le système éducatif québécois </strong>se distingue à plusieurs niveaux de celui des autres provinces canadiennes. Il dépend du <a href="http://www.mels.gouv.qc.ca/" class="spip_out" rel="external">ministère de l’Education, du loisir et du Sport du Québec (MELS)</a>.</p>
<p>L’enseignement public au Québec est gratuit de la maternelle au secondaire. Parallèlement au système d’enseignement public, il existe des établissements d’enseignement privés reconnus par le MELS. Il faut cependant s’acquitter de frais de scolarité. Le réseau public est fréquenté par environ 92 % des élèves /étudiants et le réseau privé par 8 %. L’obligation scolaire commence à l’âge de 6 ans et la scolarisation est obligatoire jusqu’à l’âge de 16 ans.</p>
<p>Le système éducatif québécois comprend cinq grandes étapes : l’enseignement maternel, primaire, secondaire, collégial et universitaire.</p>
<p><strong>Enseignement maternel et élémentaire</strong> (ou enseignement préscolaire). Il comprend la pré-maternelle et la maternelle et est souvent rattaché aux écoles du primaire. Les enfants scolarisés en maternelle sont âgés en moyenne de cinq ans. A ce jour, la pré-maternelle n’est pas obligatoire, mais la majorité des enfants la fréquente. Sa durée est de deux ans.</p>
<p><strong>Enseignement primaire </strong> : il commence à partir de l’âge de six ans. La durée normale du cours primaire est de six années réparties en deux cycles égaux de trois ans. Le MELS impose aux élèves de sixième année un examen de passage pour le secondaire. En cas d’échec, il y a redoublement.</p>
<p><strong>Enseignement secondaire</strong>. L’école secondaire s’étale sur cinq ans et va de la 1ère à la 5ème année (de la sixième à la seconde en France). L’enseignement secondaire est divisé en deux étapes : une formation générale pendant les trois premières années, suivie d’une formation professionnelle (deux ans à partir de la 4ème année). Tous les élèves suivent une formation générale jusqu’à la 3ème ou 4ème année. Ils peuvent ensuite décider de continuer un enseignement général qui les mènera au collège, puis à l’université. Mais ils peuvent aussi opter pour une formation professionnelle de deux ans qu’ils pourront compléter ensuite par trois années collégiales techniques.</p>
<p><strong>Enseignement collégial (enseignement général et professionnel ou CEGEP)</strong> qui correspond, en France, à la première et à la terminale. Le collège dure soit deux ans en vue d’acquérir une formation pré-universitaire, soit trois ans lorsqu’il s’agit d’une formation technique. A l’issue d’un programme technique, les élèves peuvent intégrer l’université ou choisir d’entrer sur le marché du travail.</p>
<p><strong>Enseignement universitaire. </strong>Les études universitaires sont divisées en trois cycles distincts :</p>
<p><strong>Le premier cycle</strong> conduit généralement à l’obtention d’un baccalauréat ou d’un diplôme après habituellement six trimestres à temps plein, nécessitant généralement trois années d’études. Il prépare à remplir un emploi sur le marché du travail ou à poursuivre des études avancées. Le certificat est obtenu après une ou deux années d’études. Le baccalauréat sanctionne trois années d’études (l’équivalent de la licence française).</p>
<p><strong>Le deuxième cycle</strong> est destiné à la spécialisation dans un champ d’études ou à l’initiation à la recherche dans une ou plusieurs disciplines. On y acquiert un grade de maîtrise après environ quatre trimestres d’études s’échelonnant habituellement sur deux années. Des programmes d’un an sont également offerts au deuxième cycle : ils conduisent à l’obtention d’un Diplôme d’études supérieures spécialisées (DESS).</p>
<p><strong>Le troisième cycle</strong> prépare à une carrière de chercheur. Les étudiants peuvent y acquérir un doctorat (Ph.D.) après six trimestres d’études et de travaux, dont une thèse, qui s’étendent en général sur trois ans.</p>
<p>À l’intérieur des deux premiers cycles se trouvent aussi des programmes courts qui nécessitent, en général, deux trimestres d’études à temps plein. La réussite à ces programmes est sanctionnée par un certificat ou un diplôme. Ces cycles dépendent de commissions scolaires (en anglais " school board "), organismes indépendants qui administrent au niveau local les établissements primaires et secondaires publics anglophones ou francophones d’un territoire géographique déterminé ou d’après un mode de découpage linguistique ou confessionnel.</p>
<h3 class="spip"><a id="sommaire_3"></a>Enseignement supérieur</h3>
<p>Le Canada possède un large choix d’établissements d’enseignement supérieur qui jouissent d’une bonne réputation : enseignement de qualité, sécurité sur le campus, équipements de communication et d’information nombreux et modernes, coût abordable, etc. Pays bilingue, le Canada propose des programmes d’études dans en français et en anglais. Enfin, un grand nombre d’universités canadiennes offrent aux étudiants quelle que soit leur nationalité la possibilité d’aller au-delà de la préparation d’un diplôme en participant à des échanges internationaux et à des programmes d’alternance travail-études ou à des activités parascolaires et sportives. Les programmes d’alternance travail-études permettent aux étudiants de combiner leur semestre universitaire avec un travail directement lié à leur domaine d’études.</p>
<p>Le site Internet de l’<a href="http://www.aucc.ca/fr/" class="spip_out" rel="external">Association des Universités et Collèges du Canada (AUCC)</a> informe sur l’enseignement supérieur canadien, offre une information destinée aux étudiants étrangers, ainsi qu’un répertoire détaillé des établissements membres avec leurs programmes d’études.</p>
<h4 class="spip">Le cas du Québec</h4>
<p><strong>Le crédit universitaire</strong></p>
<p>Dans le système universitaire québécois, les cours sont mesurés en crédits. Un cours équivaut à trois crédits, soit un total de 45 heures. Le montant des frais universitaires de base est fonction du nombre de crédits universitaires.</p>
<p><strong>Les établissements universitaires québécois</strong></p>
<p>Parmi les nombreux établissements universitaires, on distingue une majorité d’établissements à statut linguistique francophone. Le gouvernement du Québec possède un réseau d’universités publiques : l’Université du Québec. Cette dernière possède des établissements dans plusieurs villes de la province.</p>
<ul class="spip">
<li><a href="http://www2.ulaval.ca/accueil.html" class="spip_out" rel="external">Université Laval (Québec)</a></li>
<li><strong>Université de Montréal et ses écoles affiliées :</strong>
<br>— <a href="http://www.umontreal.ca/" class="spip_out" rel="external">Université de Montréal (Montréal)</a>
<br>— <a href="http://www.hec.ca/" class="spip_out" rel="external">École des hautes études commerciales de Montréal</a>
<br>— <a href="http://www.polymtl.ca/" class="spip_out" rel="external">École polytechnique de Montréal</a></li>
<li><a href="http://www.usherbrooke.ca/" class="spip_out" rel="external">Université de Sherbrooke</a></li>
<li><strong>Réseau des Universités du Québec :</strong>
<br>— <a href="http://www.uquebec.ca/reseau/" class="spip_out" rel="external">Université du Québec (siège social) (Québec)</a>
<br>— <a href="http://uqat.ca/index.asp" class="spip_out" rel="external">Université du Québec en Abitibi-Témiscamingue (Rouyn-Noranda)</a>
<br>— <a href="http://www.uqac.ca/" class="spip_out" rel="external">Université du Québec à Chicoutimi (Saguenay)</a>
<br>— <a href="http://uqo.ca/" class="spip_out" rel="external">Université du Québec en Outaouais (Gatineau)</a>
<br>— <a href="http://www.uqam.ca/" class="spip_out" rel="external">Université du Québec à Montréal (UQAM)</a>
<br>— <a href="http://www.uqar.ca/" class="spip_out" rel="external">Université du Québec à Rimouski (Rimouski)</a>
<br>— <a href="http://www.uqtr.uquebec.ca/" class="spip_out" rel="external">Université du Québec à Trois-Rivières (Trois-Rivières)</a>
<br>— <a href="http://www.enap.ca/enap/fr/accueil.aspx" class="spip_out" rel="external">École nationale d’administration publique - ENAP (Québec)</a>
<br>— <a href="http://www.etsmtl.ca/" class="spip_out" rel="external">École de technologie supérieure - ETS (Montréal)</a>
<br>— <a href="http://www.inrs.ca/" class="spip_out" rel="external">Institut national de la recherche scientifique (Québec)</a></li>
<li><a href="http://www.teluq.uquebec.ca/" class="spip_out" rel="external">La télé-Université - TELUQ</a></li>
<li><strong>Principales universités à statut linguistique anglophone :</strong>
<br>— <a href="http://www.concordia.ca/" class="spip_out" rel="external">Université Concordia</a>
<br>— <a href="http://www.mcgill.ca/" class="spip_out" rel="external">Université McGill</a>
<br>— <a href="http://www.ubishops.ca/" class="spip_out" rel="external">Université Bishop’s</a></li></ul>
<p>La France et le Canada sont engagés depuis plusieurs années dans une coopération qui couvre plusieurs orientations, dont la coopération universitaire (par exemple, le Fonds France-Canada pour la recherche créé en 2000). Par ailleurs, de nombreuses ententes ont été signées entre des universités et écoles françaises et québécoises. Enfin, il existe des bourses destinées aux étudiants étrangers (par exemple, l’Université de Québec à Montréal propose une bourse d’exemption des droits scolaires supplémentaires pour étudiants étrangers). Pour plus d’informations sur les programmes d’échanges internationaux et les bourses, vous pouvez consulter la rubrique " international " des sites Internet des universités québécoises listés ci-dessus.</p>
<p><strong>Les frais universitaires</strong></p>
<p>Bien que les frais universitaires soient beaucoup moins élevés qu’aux Etats-Unis, ils demeurent onéreux en comparaison de ceux pratiqués en France. Les frais varient en fonction de la situation de l’étudiant. L’étudiant québécois ne sera soumis qu’à des frais fixes (droits de base) et autres frais exigibles relatifs à la gestion, aux services fournis aux étudiants et à l’équipement technologique, etc. Les étudiants canadiens non-résidents au Québec et les étudiants étrangers doivent acquitter des frais supplémentaires forfaitaires. Les principaux frais universitaires (droits de base) sont calculés en fonction du nombre de crédits universitaires et du statut :</p>
<ul class="spip">
<li>temps plein (15 crédits),</li>
<li>mi-temps (7,5 crédits),</li>
<li>temps partiel (coût unitaire du crédit). Le coût annuel des études universitaires au Québec est variable (de 10 000 à 25 000 CAD) selon l’établissement, le nombre de crédits, le statut et le programme. Certains programmes universitaires (par exemple, au sein de l’Université McGill) ne perçoivent aucun financement extérieur gouvernemental.</li></ul>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.mels.gouv.qc.ca/" class="spip_out" rel="external">Ministère de l’Education, du Loisir et du Sport du Québec</a></li>
<li><a href="http://www.infobourg.com/" class="spip_out" rel="external">Infobourg, l’agence de presse pédagogique</a></li>
<li><a href="http://www.immigration-quebec.gouv.qc.ca/fr/index.php" class="spip_out" rel="external">Immigration Québec</a></li>
<li><a href="http://feuq.qc.ca/" class="spip_out" rel="external">Fédération étudiante universitaire du Québec (FEUQ)</a>. Cette fédération représente 120 000 étudiants et compte plus d’une quinzaine d’associations d’étudiants</li>
<li><a href="http://www.educationau-incanada.ca/educationau-incanada/index.aspx?lang=eng" class="spip_out" rel="external">Canada international</a>, site officiel du Gouvernement canadien destiné aux étudiants étrangers</li>
<li><a href="http://www.jeunesse.gc.ca/fra/accueil.shtml" class="spip_out" rel="external">Jeunesse Canada</a></li>
<li><a href="http://www.cicic.ca/" class="spip_out" rel="external">Centre d’information canadien sur les diplômes internationaux</a></li></ul>
<p><i>Mise à jour : avril 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/vie-pratique/article/scolarisation-112495). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
