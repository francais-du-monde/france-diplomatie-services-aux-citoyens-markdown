# Loisirs et culture

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/vie-pratique/article/loisirs-et-culture#sommaire_1">Tourisme</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/vie-pratique/article/loisirs-et-culture#sommaire_2">Activités culturelles</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/vie-pratique/article/loisirs-et-culture#sommaire_3">Radio et télévision </a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/vie-pratique/article/loisirs-et-culture#sommaire_4">Presse</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/vie-pratique/article/loisirs-et-culture#sommaire_5">Sports</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/vie-pratique/article/loisirs-et-culture#sommaire_6">Télévision – Radio</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/vie-pratique/article/loisirs-et-culture#sommaire_7">Presse française</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Tourisme</h3>
<p>En dehors de l’attrait évident de la mer et la montagne, Chypre possède un patrimoine archéologique et religieux (monastères) important. Une tenue correcte est exigée lors de la visite des monastères et des lieux saints, dont certains sont interdits d’accès aux femmes.</p>
<p>L’<a href="http://www.visitcyprus.com/" class="spip_out" rel="external">Office du tourisme de Chypre</a> a conçu un site internet officiel très complet.</p>
<p><strong>Restrictions de passage en zone nord</strong></p>
<p>Depuis l’intervention militaire de la Turquie en 1974, l’île est divisée et la partie nord n’est plus contrôlée par la République de Chypre. La présence d’importantes troupes turques au nord, le niveau élevé des armements sur l’ensemble de l’île ainsi que la permanence de la division politique suscitent des tensions toujours susceptibles de donner lieu à des incidents.</p>
<p>En raison de la partition de fait, l’ambassade ne peut assurer la protection consulaire dans la partie nord que dans des conditions difficiles, et ne peut dans certains cas l’assurer dans son intégralité. Il est à noter que les relations téléphoniques entre le Nord et le Sud sont erratiques. Les liaisons Nord/Sud par téléphone mobile sont actuellement impossibles.</p>
<p>Les touristes visitant la partie nord de l’île (contrôlée par la "République turque de Chypre nord" non reconnue internationalement) doivent impérativement respecter les interdictions de prises de vue aux abords des zones militaires protégées. Il arrive en effet que des personnes soient arrêtées pour n’avoir pas respecté ces interdictions.</p>
<blockquote class="texteencadre-spip spip">Avant d’organiser un séjour touristique et afin d’accéder aux consignes de sécurité les plus récentes, il est vivement recommandé de consulter le site <a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/chypre/" class="spip_in">Conseils aux voyageurs</a>.</blockquote>
<h3 class="spip"><a id="sommaire_2"></a>Activités culturelles</h3>
<h4 class="spip">Activités culturelles en français</h4>
<p>En dehors des activités proposées par l’<a href="http://www.ifchypre.org/index.php/fr/" class="spip_out" rel="external">institut français de Chypre</a>, les manifestations culturelles en langue française sont assez rares à Chypre.</p>
<h4 class="spip">Activités culturelles locales</h4>
<p>L’<a href="http://www.visitcyprus.com/wps/portal" class="spip_out" rel="external">Office du tourisme chypriote</a> diffuse dans ses différents bureaux, basés dans les principaux centres touristiques de l’île, plusieurs magazines présentant l’actualité culturelle de Chypre. Celle-ci comprend de nombreuses manifestations et festivals, rythmés par les différentes fêtes de l’île.</p>
<h3 class="spip"><a id="sommaire_3"></a>Radio et télévision </h3>
<p>Il existe trois stations nationales et plusieurs radios privées, dont Radio One qui diffuse en anglais.</p>
<p>Plusieurs chaînes de télévision locales et grecques sont présentes sur l’île.</p>
<h3 class="spip"><a id="sommaire_4"></a>Presse</h3>
<p>La presse locale est très fournie, une quinzaine de titres (quotidiens et hebdomadaires) est disponible dans les kiosques.</p>
<h3 class="spip"><a id="sommaire_5"></a>Sports</h3>
<p>On peut pratiquer à Chypre tous les sports nautiques (ski nautique, planche à voile, plongée…) et trouver des clubs d’équitation, de golf, etc. Tous les équipements sont disponibles sur place, y compris en location.</p>
<p>Pour la pêche, un permis doit être obtenu dans l’un des bureaux du Department of Fisheries ; à Nicosie : 13 odos Aiolou, tél. (357) 22 30 35 26.</p>
<h3 class="spip"><a id="sommaire_6"></a>Télévision – Radio</h3>
<p>La plupart des chaînes de radio et de télévision françaises sont accessibles par satellite, mais il n’est pas simple de s’abonner aux chaînes cryptées. La meilleure solution est de prendre un abonnement en France et d’apporter ses propres cartes et décodeur (l’installation d’une parabole sur place coûte environ 600 euros).</p>
<h3 class="spip"><a id="sommaire_7"></a>Presse française</h3>
<p>Il n’existe pas de publication locale en langue française ; en revanche, on trouve en anglais deux quotidiens, le <a href="http://cyprus-mail.com/" class="spip_out" rel="external">Cyprus Mail</a> et le <a href="http://www.cyprusdailynews.info/news/" class="spip_out" rel="external">Cyprus Daily</a>, et un hebdomadaire, le <a href="http://www.incyprus.com.cy" class="spip_out" rel="external">Cyprus Weekly</a>.</p>
<p>La presse internationale est disponible dès le lendemain de la parution dans les kiosques et hôtels des principaux centres touristiques.</p>
<p>Par ailleurs, il existe des librairies spécialisées dans les ouvrages français :</p>
<p><strong>La boîte à lire</strong><br class="manualbr">Prevezis, 8E <br class="manualbr">1065 Nicosie <br class="manualbr">Tél. : (357) 22 67 01 84</p>
<p><strong>Apostrophe</strong><br class="manualbr">Omiro street, 38<br class="manualbr">Pacova Center <br class="manualbr">3095 Limassol<br class="manualbr">Tél. : (357) 25 59 15 17</p>
<p>L’institut français de Chypre permet également la consultation d’ouvrages en français, de bandes dessinées, de magazines, et aussi d’emprunter des CD et DVD.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/vie-pratique/article/loisirs-et-culture). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
