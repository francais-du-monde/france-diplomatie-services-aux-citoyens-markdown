# Protection sociale

<h2 class="rub22138">Régime local de sécurité sociale</h2>
<p class="chapo">
    La sécurité sociale vénézuélienne a été mise en place en 1940. Le système d’assurance couvre : les employés du secteur public et privé, les membres de coopératives, les femmes au foyer ainsi que les employés saisonniers et autres travailleurs temporaires.
</p>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/protection-sociale-22138/#sommaire_1">Retraite</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/protection-sociale-22138/#sommaire_2">Décès</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/protection-sociale-22138/#sommaire_3">Invalidité</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/protection-sociale-22138/#sommaire_4">Orphelins</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/protection-sociale-22138/#sommaire_5">Congés maternité </a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/protection-sociale-22138/#sommaire_6">Arrêt maladie</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/protection-sociale-22138/#sommaire_7">Couverture médicale</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/protection-sociale-22138/#sommaire_8">Accidents du travail et maladies professionnelles</a></li>
<li><a id="so_9" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/protection-sociale-22138/#sommaire_9">Chômage</a></li>
<li><a id="so_10" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/protection-sociale-22138/#sommaire_10">Pour plus d’informations : </a></li></ul>
<p>Les travailleurs indépendants peuvent s’affilier sur la base du volontariat tout comme les femmes enceintes sans emplois et les personnes qui étaient auparavant couvertes. Leurs contributions sont calculées selon leur salaire perçu dans les 100 dernières semaines dans une limite de cinq fois le salaire minimum.</p>
<p>Un système parallèle est mis en place uniquement pour les militaires.</p>
<h3 class="spip"><a id="sommaire_1"></a>Retraite</h3>
<p>L’âge légal de départ à la retraite est de 60 ans pour les hommes et de 55 ans pour les femmes. Il faut, de plus, avoir cotisé au moins 750 semaines, avoir résidé au minimum 10 ans au Venezuela et être détenteur d’une Cédula de identidad (carte d’identité). L’âge de départ peut être avancé pour les travailleurs dont l’état de santé est mauvais et pour certains métiers particulièrement difficiles. Il est possible de continuer à travailler au-delà de l’âge légal de départ à la retraite sans limite.</p>
<p>Le montant de la pension équivaut au montant du salaire minimum auquel s’ajoute 30% du salaire de référence. Le salaire de référence est calculé sur la base de 20% des salaires perçus dans les cinq dernières années ou de 10% sur les dix dernières années.</p>
<p>Les hommes et les femmes ne répondant pas aux conditions d’attributions de retraite peuvent tout de même bénéficier d’une allocation vieillesse qui s’élève à 10% du salaire moyen de l’assuré.</p>
<h3 class="spip"><a id="sommaire_2"></a>Décès</h3>
<p>Pour bénéficier d’une pension de réversion, le conjoint survivant doit être âgé d’au moins 45 ans, à moins d’avoir un enfant à charge. Sont considérés comme bénéficiaires d’une pension de réversion : les époux, les partenaires ayant vécu avec le défunt pendant au moins deux ans ainsi que les enfants non mariés âgés de moins de 18 ans (pas de limite d’âge pour les enfants handicapés). S’il n’y pas d’autres survivants, les frères et sœurs âgés de moins de 14 ans et les parents sont éligibles. Le montant de la pension s’élève à 40% du salaire ou de la retraite que percevait la personne décédée. La pension de réversion cesse d’être versée quand le survivant se remarie, un paiement unique correspondant à deux ans de pension est alors effectué.</p>
<p>En dessous de 45 ans et pour les personnes ne répondant pas aux conditions citées ci-dessus il est possible d’obtenir une aide d’un montant correspondant à deux années de salaires du défunt.</p>
<h3 class="spip"><a id="sommaire_3"></a>Invalidité</h3>
<p>Est reconnu invalide l’assuré qui, en raison d’un handicap, voit sa capacité de travail réduite d’au moins 66%. Pour bénéficier d’une pension d’invalidité le travailleur doit avoir cotisé au moins 250 semaines sauf si le handicap est causé par un accident. Dans les cas de grave invalidité il est possible pour l’assuré d’obtenir une aide à domicile payée par l’état.</p>
<p>Le montant de la pension est égal au salaire minimum auquel s’ajoute 30% du salaire de référence.</p>
<p>Les personnes invalides à moins de 66% peuvent, toutefois, bénéficier d’une subvention moindre selon leur degré d’invalidité.</p>
<h3 class="spip"><a id="sommaire_4"></a>Orphelins</h3>
<p>Les enfants orphelins âgés de moins de 14 ans (ou de 18 ans pour les étudiants) reçoivent chacun 20% de la pension du défunt, 40% pour les enfants uniques. Il n’y a aucune limite d’âge pour les enfants handicapés.</p>
<h3 class="spip"><a id="sommaire_5"></a>Congés maternité </h3>
<p>L’intégralité du salaire est garantie à partir de six semaines avant l’accouchement et jusqu’à 20 semaines après la naissance de l’enfant (art. 11 Loi sécurité sociale).</p>
<h3 class="spip"><a id="sommaire_6"></a>Arrêt maladie</h3>
<p>Au quatrième jour d’arrêt une subvention correspondent à 66% du salaire quotidien moyen est versée à l’employé pendant 52 semaines maximum (ce délai peut être étendu sous certaines conditions).</p>
<h3 class="spip"><a id="sommaire_7"></a>Couverture médicale</h3>
<p>Les soins médicaux sont fournis gratuitement par les centres médicaux dépendants de l’Institut vénézuélien de sécurité sociale (IVSS) pendant au moins 52 semaines. Les frais couverts correspondent aux consultations chez des généralistes et spécialistes, aux frais d’hospitalisation, de laboratoire et de maternité ainsi qu’aux frais de transports.</p>
<h3 class="spip"><a id="sommaire_8"></a>Accidents du travail et maladies professionnelles</h3>
<p>Attention : les travailleurs indépendants ne sont pas couverts par la sécurité sociale pour les accidents du travail et maladies professionnelles.</p>
<p>Pour les maladies ou accidents temporaires l’intégralité du salaire est versé à partir de trois jours d’arrêt et ce jusqu’à ce que la personne soit guérie ou soit reconnue comme invalide.</p>
<h3 class="spip"><a id="sommaire_9"></a>Chômage</h3>
<p>L’assurance chômage couvre les employés du secteur public et privé, les travailleurs indépendants, les membres de coopératives ainsi que les apprentis. Pour pouvoir bénéficier d’une assurance chômage il est nécessaire d’avoir cotisé pendant au moins 12 mois durant les 18 mois antérieur au début de la période de chômage et être déclaré apte à travailler. Dans le cas des travailleurs indépendants ils doivent avoir perdu involontairement leurs revenus.</p>
<p>L’assurance s’élève à 60% du dernier salaire et est versée durant cinq mois. Les chômeurs peuvent bénéficier de la couverture maladie pendant 26 semaines.</p>
<h3 class="spip"><a id="sommaire_10"></a>Pour plus d’informations : </h3>
<ul class="spip">
<li><a href="http://www.who.int/countries/ven/fr/" class="spip_out" rel="external">Site de l’OMS</a></li>
<li><a href="http://new.paho.org/col" class="spip_out" rel="external">Organisation panaméricaine de la santé</a></li>
<li><a href="https://www.socialsecurity.gov/policy/docs/progdesc/ssptw/2008-2009/americas/venezuela.html" class="spip_out" rel="external">Social security organization (USA)</a></li>
<li><a href="http://www.ivss.gov.ve/" class="spip_out" rel="external">Institut vénézuélien de sécurité sociale (IVSS)</a></li>
<li><a href="http://www.minpptrass.gob.ve/" class="spip_out" rel="external">Ministère du travail et de la protection sociale</a></li>
<li><a href="http://www.cleiss.fr" class="spip_out" rel="external">CLEISS</a></li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-venezuela-protection-sociale-22138-article-convention-de-securite-sociale-103735.md" title="Convention de sécurité sociale">Convention de sécurité sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-venezuela-protection-sociale-22138-article-regime-local-de-securite-sociale-103734.md" title="Régime local de sécurité sociale">Régime local de sécurité sociale</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/protection-sociale-22138/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
