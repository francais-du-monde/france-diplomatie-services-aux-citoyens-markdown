# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/vie-pratique/article/cout-de-la-vie-114781#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/vie-pratique/article/cout-de-la-vie-114781#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/vie-pratique/article/cout-de-la-vie-114781#sommaire_3">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>L’unité monétaire est l’euro.</p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<p>Les cartes bancaires, Eurocard-MasterCard et American Express en particulier, sont acceptées par la plupart des commerces. Les chèques sont en revanche inutilisables.</p>
<p>La Société Générale exerce une activité de banque de détail en Slovénie par l’intermédiaire de sa filiale SKB Banka.Les banques autrichiennes Kredit Anstalt et Spakasse sont présentes.</p>
<p>Pour ouvrir un compte bancaire, les documents à fournir sont les suivants :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  un document d’identité </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  un numéro fiscal slovène (à demander auprès du ministère des finances) </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  une attestation de déclaration de résidence pour les citoyens de l’EEE </p>
<p>La procédure est assez rapide. Le compte bancaire sera un compte « non-résident » si le séjour en Slovénie est inférieur à six mois ou « résident » si le séjour en Slovénie est supérieur à six mois.</p>
<p><strong>Transfert de fonds :</strong></p>
<p>Il n’y a aucun problème particulier pour les transferts de fonds vers la France.</p>
<h3 class="spip"><a id="sommaire_3"></a>Evolution des prix</h3>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : septembre 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/vie-pratique/article/cout-de-la-vie-114781). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
