# Recherche d’emploi

<p class="chapo">
      Il est conseillé de démarcher individuellement auprès des entreprises. Les journaux locaux disposent également d’offres d’emplois dans les pages travail.
</p>
<h4 class="spip">Sites internet</h4>
<h4 class="spip">En français</h4>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.expat.com/fr/destination/amerique-du-sud/venezuela" class="spip_url spip_out" rel="external">http://www.expat.com/fr/destination...</a></p>
<h4 class="spip">En anglais</h4>
<ul class="spip">
<li><a href="http://ve.3wjobs.com" class="spip_url spip_out" rel="external">http://ve.3wjobs.com</a></li>
<li><a href="http://www.jobofmine.com/job/search/country/Venezuela" class="spip_url spip_out" rel="external">http://www.jobofmine.com/job/search...</a></li>
<li><a href="http://www.learn4good.com/jobs/language/english/list/country/venezuela" class="spip_url spip_out" rel="external">http://www.learn4good.com/jobs/lang...</a></li></ul>
<h4 class="spip">En espagnol :</h4>
<ul class="spip">
<li><a href="http://www.computrabajo.com.ve" class="spip_url spip_out" rel="external">http://www.computrabajo.com.ve</a></li>
<li><a href="http://www.bumeran.com.ve" class="spip_url spip_out" rel="external">http://www.bumeran.com.ve</a></li>
<li><a href="http://www.opcionempleo.com.ve/ofertas-empleo-venezuela-114374.html" class="spip_url spip_out" rel="external">http://www.opcionempleo.com.ve/ofer...</a></li></ul>
<h4 class="spip">Organismes spécialisés</h4>
<ul class="spip">
<li><a href="http://www.cciavf.com.ve" class="spip_out" rel="external">Chambre de commerce et d’industrie franco-vénézuélienne</a></li>
<li><a href="http://www.pole-emploi-international.fr/www.eei.com.fr/eei/accueil.jsp" class="spip_out" rel="external">Pôle emploi international</a></li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/emploi-stage/article/recherche-d-emploi). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
