# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/vie-pratique/article/transports#sommaire_1">Permis de conduire</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/vie-pratique/article/transports#sommaire_2">Code de la route</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/vie-pratique/article/transports#sommaire_3">Assurances et taxes</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/vie-pratique/article/transports#sommaire_4">Achat et location</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/vie-pratique/article/transports#sommaire_5">Immatriculation</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/vie-pratique/article/transports#sommaire_6">Entretien</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/vie-pratique/article/transports#sommaire_7">Réseau routier</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/vie-pratique/article/transports#sommaire_8">Transports en commun</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Permis de conduire</h3>
<p>Les citoyens français résidant en Espagne peuvent solliciter l’échange de leur permis français contre le document espagnol équivalent.</p>
<p>Cet échange s’effectue à la Direction de la Circulation (<i>Jefatura provincial de Trafico</i>), sur présentation du formulaire remis par la Jefatura, accompagné d’un document d’identité ou passeport en cours de validité, du cerficat d’inscription au registre central des étrangers, du permis de conduire français en cours de validité, d’une photographie. Vous devrez acquitter une taxe (environ 35 € en 2013).</p>
<p><strong>Attention :</strong></p>
<p>Le Décret Royal 818/2009 du 8 mai 2009 (art. 15.4 ; art. 16.1) instaure de nouvelles dispositions de validité pour les permis délivrés par les états membres de l’UE : « S’agissant de permis de conduire non soumis a une date de validité, le titulaire devra, renouveler le permis, dans les deux ans suivants sa date d’installation en Espagne ».</p>
<p><strong>Pour en savoir plus :</strong></p>
<p>Site <a href="http://www.dgt.es/es/" class="spip_out" rel="external">Direction générale de la Circulation</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Code de la route</h3>
<p>Depuis 2006, les Espagnols ont instauré un permis de conduire à 12 points ; par ailleurs, la nouvelle législation a durci les règles du code de la route :</p>
<ul class="spip">
<li>L’usage de téléphones mobiles est interdit pendant la conduite (sauf à disposer d’un système "mains libres") ;</li>
<li>Les véhicules doivent être munis dans le coffre de deux triangles de signalisation à surface réfléchissante. Ces triangles sont destinés à signaler les véhicules immobilisés sur la chaussée et doivent être disposés, le cas échéant, à une distance minimale de 50 mètres à l’avant et à l’arrière du véhicule. La police espagnole n’hésite pas à verbaliser les véhicules qui ne respectent pas cette règle ;</li>
<li>Tout automobiliste amené à quitter son véhicule sur le bord d’une route doit être en possession d’un gilet de sécurité fluorescent ;</li>
<li>Le port du casque et de la ceinture de sécurité (à l’avant comme à l’arrière) est obligatoire sous peine d’amendes (entre 90 et 300 €)</li>
<li>Taux d’alcoolémie maximium autorisé : 0,5 g/l (amendes entre 300 et 600 €)</li>
<li>Limitation de vitesse : 120 km/h sur autoroute, 100 km/h sur les nationales et 50 km/h en ville</li>
<li>En cas de vol, il convient de remplir un formulaire auprès de la police locale et prendre contact avec le consulat de France concerné ;</li>
<li>Les sanctions pour non respect du code de la route sont sévères :
<br>— les amendes sont élevées et les étrangers doivent les régler immédiatement et en espèces ;
<br>— la mise en fourrière pour mauvais stationnement est systématique. Le montant des amendes à payer est d’environ 100 euros ;
<br>— par ailleurs, une infraction grave peut être sanctionnée par une suspension du permis de conduire de trois mois ;
<br>— enfin, le dépassement de 50 % de la vitesse autorisée est considéré comme une infraction "très grave" ; le permis peut être retiré suite à trois infractions "très graves" au cours des deux dernières années.</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Assurances et taxes</h3>
<p>Au sein de l’UE, le principe est que tout propriétaire d’une automobile est dans l’obligation de souscrire une assurance en responsabilité civile, couvrant les dommages matériels et corporels causés à autrui, y compris aux passagers de son véhicule. La carte verte ou le certificat d’assurance remis par l’assureur constitue la preuve que l’obligation d’assurance a été remplie.</p>
<p>On peut choisir de s’assurer auprès de la compagnie d’assurance de son choix, dans n’importe quel pays de l’Union européenne, même si ce n’est pas son pays d’origine ou de résidence. Si vous possédez une assurance tous risques dans votre pays, il convient de s’assurer qu’elle couvre également les déplacements à l’étranger.</p>
<h3 class="spip"><a id="sommaire_4"></a>Achat et location</h3>
<p>L’achat et l’entretien d’un véhicule sur place ne pose aucun problème.</p>
<p>Toutes les marques françaises ou étrangères sont représentées. Il existe des usines de montage Renault, Peugeot et Citroën. Les normes sont identiques à la France. Il est possible d’acheter sur place une voiture d’occasion à des prix comparables et parfois plus intéressants qu’en France.</p>
<p>L<a href="http://www.dgt.es/portal/es/oficina_virtual/vehiculos/itv/" class="spip_out" rel="external">’ITV</a> (Inspection technique des véhicules) est obligatoire et varie selon le type de véhicule et son âge. Son tarif varie selon les communautés autonomes, en 2011, il était en moyenne de 33 €.</p>
<p>Sur un véhicule neuf, une première inspection doit être effectuée quatre ans après l’achat, puis tous les deux ans jusqu’à 10 ans d’âge et au-delà, une inspection annuelle. Pour les véhicules d’occasion, l’inspection est obligatoire au moment du transfert de propriété, ensuite la périodicité varie selon l’âge du véhicule.</p>
<p><strong>Location</strong></p>
<p>La location de véhicule s’effectue dans les mêmes conditions et aux mêmes prix qu’en France.</p>
<p>De nombreuses compagnies de location exigent une ancienneté du permis de conduire d’un ou deux ans. Rappelez-vous également que pour pouvoir effectuer la location, vous devez posséder une carte de crédit.</p>
<h3 class="spip"><a id="sommaire_5"></a>Immatriculation</h3>
<p>Si vous emménagez en Espagne avec votre véhicule personnel, vous devrez obtenir des plaques minéralogiques espagnoles.</p>
<p>Venant d’un pays de l’U.E. où vous avez résidé depuis plus d’un an, vous pouvez être exonéré de la taxe dénommée <i>Declaración-Liquidación para el Impuesto Especial sobre Determinados Medios de Transporte</i> (7% ou 12% sur la valeur actuelle, argus, de la voiture ou moto), si vous faites la demande de l’exonération <strong>dans les 30 jours suivant l’entrée du véhicule en Espagne</strong>. Par ailleurs, il vous appartiendra d’apporter la preuve que vous étiez en possession du véhicule depuis au moins six mois avant la date de votre déménagement.</p>
<p>Pour immatriculer votre véhicule en plaques espagnoles, il convient se rendre à la <i>Jefatura Provincial de Trafico</i> compétente dans la circonscription</p>
<p>(Provincia) de votre lieu de résidence, munis des documents suivants :</p>
<ul class="spip">
<li>Demande de l’imprimé officiel <a href="http://www.dgt.es/was6/portal/contenidos/documentos/oficina_virtual/vehiculos/matriculacion" class="spip_out" rel="external">solicitud de matriculacion</a></li>
<li>Certificat de contrôle technique (ITV)</li>
<li>Documentation originale du véhicule (carte grise)</li>
<li>NIE : Tout document officiel comportant mention du NIE - Numéro d’Identification Fiscal pour étrangers- (Carte de résident, certificat etc…).</li>
<li>C.N.I. ou passeport</li>
<li><i>Declaración-Liquidación para el Impuesto Especial sobre Determinados Medios de Transporte</i> (modèle 576) : vous devez présenter le récépissé de paiement de cette taxe (ou le certificat d’exonération idoine, que vous obtiendrez à la <i>Delegación de Economia y Hacienda</i> (site Internet : <a href="http://www.aeat.es/" class="spip_out" rel="external">Aeat.es</a>). Pour faire la demande de cette exonération, une attestation de résidence spécifique délivrée par le consulat, est nécessaire.</li>
<li>IVTM : (<i>Impuesto sobre Vehículos de Tracción Mecánica</i>) : équivalent à l’ancienne <strong>vignette</strong> en France. Cette taxe municipale doit être payée à la mairie de la ville du lieu où vous avez votre domicile fiscal. Les tarifs varient selon la commune et la puissance fiscale du véhicule.</li>
<li>Taxe d’immatriculation : à payer à la caisse de la Jefatura Provincial de Trafico.</li></ul>
<p><strong>Adresse de la Delegacion de Trafico à Madrid </strong> : <br class="manualbr">Calle Arturo Soria, 143<br class="manualbr">28043 Madrid<br class="manualbr">Tél. : 913 018 500</p>
<p><strong>Adresse de la Delegacion de Trafico à Barcelone </strong> : <br class="manualbr">Gran Via Corts Catalanes, 180-184<br class="manualbr">Tél. : (93) 298 65 00 / 296 60 06<br class="manualbr">Fax : (93) 422 88 43</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.mir.es/" class="spip_out" rel="external">Ministère de l’Intérieur</a></li>
<li><a href="http://www.consulfrance-barcelone.org/" class="spip_out" rel="external">Consulat de France à Barcelone</a></li></ul>
<h3 class="spip"><a id="sommaire_6"></a>Entretien</h3>
<p>Il existe des concessionnaires de toutes les marques internationales de véhicules.</p>
<h3 class="spip"><a id="sommaire_7"></a>Réseau routier</h3>
<p>Le réseau routier est bon et les villes principales sont desservies par des autoroutes ou des routes à quatre voies. Les routes à quatre voies, <i>autovias</i>, sont gratuites, les autoroutes <i>autopistas</i> sont payantes (limitées à 120 km/h).</p>
<p>La prudence sur les routes espagnoles s’impose toutefois (vitesse excessive des conducteurs, trafic dense de poids lourds, chaussée en mauvaise état sur certaines routes, etc.).</p>
<p>Le site web de la <a href="http://www.dgt.es/" class="spip_out" rel="external">direction générale espagnole des Transports</a> fournit (en espagnol et en anglais) des informations sur l’état des routes, du trafic, etc.</p>
<p>Pour en savoir plus :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/espagne/" class="spip_in">Conseils aux voyageurs</a></p>
<h3 class="spip"><a id="sommaire_8"></a>Transports en commun</h3>
<p><strong>Transports urbains</strong></p>
<p>En ville le réseau des transports en commun est bien développé. Les déplacements peuvent s’effectuer en métro (les villes les plus importantes disposent de réseaux de transport souterrain très efficaces qui fonctionnent en principe de 6h00 du matin à 1h30 du matin.), en autobus dans toutes les villes, en taxis (prix similaires à ceux de la France). Le coût d’un ticket simple de métro (en Zone 1) est de 2€ à Madrid, 1,45 € à Barcelone. Il existe des tickets pour 10 trajets (9,30 € à Madrid ; 7,85 € à Barcelone) et différentes formules au tarif avantageux (coupon touristique, etc.).</p>
<p>Pour connaître les tarifs :</p>
<ul class="spip">
<li>Site Internet du <a href="http://www.metromadrid.es/" class="spip_out" rel="external">Metro de Madrid</a></li>
<li>Site du <a href="http://www.tmb.net/es_ES/home.jsp" class="spip_out" rel="external">Metro de Barcelone</a></li>
<li>Site du <a href="http://www.metrobilbao.net/" class="spip_out" rel="external">Metro de Bilbao</a></li></ul>
<p><strong>Transport par car</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Autobus <a href="http://www.eurolines.es/" class="spip_out" rel="external">Eurolines</a></strong><br class="manualbr">Barcelone<br class="manualbr">Est. de Aut. de Sants. C/ Viriato, s/n. <br class="manualbr">Tel. : +34 934 904 000<br class="manualbr">Fax : +34 934 904 312
</p>
<p>Estación de Autobuses del Norte<br class="manualbr">C/ Ali Bei, 80 local 25-26<br class="manualbr">Tel. +34 932 321 092</p>
<p>Madrid<br class="manualbr">Estación Sur de Autobuses. Local 10<br class="manualbr">C/ Méndez Álvaro, 83.<br class="manualbr">Tel. : +34 915 063 360 <br class="manualbr">Fax : +34 915 063 365</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Iberbus Linebus</strong><br class="manualbr">Autotransportes Iberbus S.l. - Autocares de Barcelona<br class="manualbr">Herrero, 30<br class="manualbr">CP : 08902<br class="manualbr">L´hospitalet De Llobregat<br class="manualbr">Tel : +34 932 632 561</p>
<p><strong>Réseau ferroviaire</strong></p>
<p>Très bon.</p>
<p><a href="http://www.renfe.es/" class="spip_out" rel="external">Renfe</a> (chemins de fer espagnols) :</p>
<ul class="spip">
<li>Informations internationales, tél. : 902 24 34 02</li>
<li>Informations, réservations, vente, tél. : 902 24 02 02</li></ul>
<p><strong>Réseau maritime</strong></p>
<p>Liaisons quotidiennes par bacs et navires rapides : Malaga/Melilla, Almeria/Melilla, Algesiras/Ceuta (Melilla possède un aéroport, mais pas Ceuta). Deux fois par semaine : liaison Alicante/Oran (tous les jours en été).</p>
<p><a href="http://www.trasmediterranea.es/" class="spip_out" rel="external">Trasmediterranea</a><br class="manualbr">Moll de San Bertran, 3 - Estacion maritima<br class="manualbr">Tél. : 93 295 91 82</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Site Internet de l’<a href="http://ambafrance-es.org/" class="spip_out" rel="external">Ambassade de France en Espagne</a>.</p>
<p><i>Mise à jour : avril 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/vie-pratique/article/transports). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
