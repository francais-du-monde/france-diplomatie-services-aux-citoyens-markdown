# Animaux domestiques

<p>Voyager avec des animaux en République tchèque nécessite de suivre les règles du pays d’où provient l’animal et également les conditions de transport des animaux applicables en République tchèque.</p>
<h4 class="spip">Conditions pour voyager avec des animaux</h4>
<ul class="spip">
<li>passeport (passeport pour les animaux de compagnie - Pet Passeport) ou un certificat attestant les conditions d’importation signé par un vétérinaire datant au maximum de trois jours avant le départ.</li>
<li>identification de l´animal par une micro puce ou un tatouage</li>
<li>vaccination antirabique Plus d’informations sur le site Internet <a href="http://www.svscr.cz/index.php?art=1559" class="spip_out" rel="external">du service des vétérinaires d’Etats</a>.</li></ul>
<p><i>Source : <a href="http://www.czechtourism.com/fr/home/" class="spip_out" rel="external">Czechtourism.com</a></i></p>
<h4 class="spip">Informations générales</h4>
<h5 class="spip">Le pays de destination se trouve dans l’Union européenne (sauf Irlande, Malte, Suède et Royaume-Uni)</h5>
<p>Une information très détaillée est disponible sur le site Internet du <a href="http://agriculture.gouv.fr/" class="spip_out" rel="external">ministère de l’Agriculture</a>.</p>
<p>Les chiens, les chats et les furets doivent satisfaire aux conditions suivantes :</p>
<ul class="spip">
<li>être identifiés par tatouage ou par puce électronique ;</li>
<li>être valablement vaccinés contre la rage ;</li>
<li>être titulaires d’un passeport délivré par un vétérinaire habilité attestant de l’identification et de la vaccination contre la rage de l’animal.</li></ul>
<p>Le système d’identification électronique, ainsi que la reconnaissance de la validité de la vaccination contre la rage, peuvent varier d’un Etat membre à l’autre. Il est donc vivement recommandé de prendre contact avec l’<a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-etrangers-en-france/article/annuaire-des-representations-etrangeres-en-france" class="spip_in">ambassade du pays de destination</a>.</p>
<p>En France, la réalisation de la primo-vaccination antirabique n’est considérée comme valable qu’à partir de 21 jours après la fin du protocole de vaccination prescrit par le fabricant. La vaccination antirabique de rappel est considérée en cours de validité le jour de sa réalisation.</p>
<p><strong>Pour plus d’informations :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Notre article thématique sur les <a href="services-aux-citoyens-preparer-son-expatriation-douanes-article-animaux-domestiques.md" class="spip_in">Animaux domestiques</a> </p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/entree-et-sejour/article/animaux-domestiques-111334). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
