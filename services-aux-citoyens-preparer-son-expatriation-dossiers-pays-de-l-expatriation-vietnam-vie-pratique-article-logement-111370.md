# Logement

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/vie-pratique/article/logement-111370#sommaire_1">Où se loger ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/vie-pratique/article/logement-111370#sommaire_2">Conditions de location</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/vie-pratique/article/logement-111370#sommaire_3">Hôtels</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/vie-pratique/article/logement-111370#sommaire_4">Hô Chi Minh-Ville</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/vie-pratique/article/logement-111370#sommaire_5">Hanoï</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/vie-pratique/article/logement-111370#sommaire_6">Auberges de jeunesse </a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/vie-pratique/article/logement-111370#sommaire_7">Electricité</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/vie-pratique/article/logement-111370#sommaire_8">Electroménager</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Où se loger ?</h3>
<h4 class="spip">Hô Chi Minh-Ville</h4>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Type d’appartement </strong> (quartier résidentiel)</td>
<td>Euros</td></tr>
<tr class="row_even even">
<td>Studio</td>
<td>815</td></tr>
<tr class="row_odd odd">
<td>3 pièces</td>
<td>1100</td></tr>
<tr class="row_even even">
<td>5 pièces</td>
<td>1700</td></tr>
<tr class="row_odd odd">
<td>Villa</td>
<td>2100</td></tr>
<tr class="row_even even">
<td><strong>Type d’appartement</strong> (banlieue)</td>
<td>Euros</td></tr>
<tr class="row_odd odd">
<td>Studio</td>
<td>520</td></tr>
<tr class="row_even even">
<td>3 pièces</td>
<td>900</td></tr>
<tr class="row_odd odd">
<td>5 pièces</td>
<td>1100</td></tr>
<tr class="row_even even">
<td>Villa</td>
<td>1300</td></tr>
</tbody>
</table>
<h4 class="spip">Hanoï</h4>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Type d’appartement</strong> (quartier résidentiel)</td>
<td>Euros</td></tr>
<tr class="row_even even">
<td>Studio</td>
<td>950</td></tr>
<tr class="row_odd odd">
<td>3 pièces</td>
<td>2000</td></tr>
<tr class="row_even even">
<td>5 pièces</td>
<td>4000</td></tr>
<tr class="row_odd odd">
<td>Villa</td>
<td>de 2000 à 5000</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_2"></a>Conditions de location</h3>
<p>A Hanoï, les quartiers résidentiels se situent essentiellement à proximité du lac de l’ouest (Ho Tay) et du lac Ngoc Khanh. A Hô Chi Minh-Ville, il s’agit des quartiers 1 et 3 (centre-ville), de Phu Nhuan, An Phu et Phu My Hung (proche banlieue).</p>
<p>Il est possible de louer des appartements dans les résidences hôtelières pour étrangers. Celles-ci sont dotées d’infrastructures réservées aux activités sportives, à la garde d’enfants et à la restauration. Le tarif de location y est très élevé. .</p>
<p>Des appartements ou maisons de type moderne ou traditionnel sont également accessibles à la location. Il existe à Hanoï des petites maisons - parfois dotées d’un petit jardin – dans les quartiers résidentiels. Ces quartiers ne sont pas épargnés par le bruit, Hanoi est une capitale très bruyante.</p>
<p>Les offres des agences immobilières sont consultables sur Internet. Le bouche à oreille est une alternative pour connaître les logements disponibles à Hanoï et à Hô Chi Minh-Ville. Si vous avez recours à un particulier, il est essentiel de s’assurer que le bailleur dispose d’une autorisation adéquate délivrée par les autorités locales pour louer à un étranger. Le loyer peut être négocié. Attention, le contrat ou l’accord écrit peut être remis en cause par le propriétaire, il est donc nécessaire de bien s’entendre au préalable sur les clauses de la location. Les équipements (climatisation, …) sont fournis par le propriétaire. La durée du bail varie d’un à cinq ans. Une avance d’un à trois mois de loyer est exigée. Cependant, il n’existe aucune règle préétablie, le locataire peut être amené à régler six mois de loyer d’avance. La commission d’agence est réglée par le propriétaire. En général, l’occupant doit s’acquitter de toutes les charges. La climatisation induit une forte consommation d’électricité et par conséquent une facturation élevée, de l’ordre de 500 euros mensuels minimum pour une villa de taille moyenne.</p>
<p>Le Vietnam pratique encore une politique de la double tarification des services, notamment liés à l’habitation (eau, électricité, loyers), entre les nationaux et les étrangers. Les prix diffèrent généralement selon les régions et les villes.</p>
<p><strong>Pour obtenir une liste des agences immobilières :</strong></p>
<ul class="spip">
<li><a href="http://www.internationalrealestatedirectory.com/country/vietnam.htm" class="spip_out" rel="external">Internationalrealestatedirectory.com</a></li>
<li><a href="http://en.metvuong.com/" class="spip_out" rel="external">Metvuong.com</a></li></ul>
<p>Un modèle de contrat de location est disponible sur le site de l’<a href="http://www.chaocom.com/" class="spip_out" rel="external">agence immobilière Chao Co</a> (en anglais).</p>
<h3 class="spip"><a id="sommaire_3"></a>Hôtels</h3>
<h3 class="spip"><a id="sommaire_4"></a>Hô Chi Minh-Ville</h3>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Prix moyen d’une chambre d’hôtel (chambre double)</strong></td>
<td>dongs</td>
<td>euros</td></tr>
<tr class="row_even even">
<td>Grand tourisme</td>
<td>3 700 000</td>
<td>130</td></tr>
<tr class="row_odd odd">
<td>Moyen tourisme</td>
<td>1 400 000</td>
<td>50</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_5"></a>Hanoï</h3>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Prix moyen d’une chambre d’hôtel (chambre double)</strong></td>
<td>dongs</td>
<td>euros</td></tr>
<tr class="row_even even">
<td>Grand tourisme</td>
<td>3 700 000</td>
<td>130</td></tr>
<tr class="row_odd odd">
<td>Moyen tourisme</td>
<td>1 400 000</td>
<td>50</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_6"></a>Auberges de jeunesse </h3>
<p>La <a href="http://www.hostels.com/fr/vm.html" class="spip_out" rel="external">liste des auberges de jeunesse au Vietnam</a> est disponible en ligne.</p>
<p>On trouve de nombreuses offres de logements dans tous les journaux vietnamiens (en vietnamien) et dans le Vietnam News (en anglais). Vous pouvez aussi vous procurer le magazine hebdomadaire « Mua Ban ».</p>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li><a href="http://www.consulfrance-hcm.org/Logement" class="spip_out" rel="external">consulat général de France à Ho Chi Minh Ville</a></li>
<li><a href="http://www.livinginvietnam.com/index.php/fr/" class="spip_out" rel="external">Livinginvietnam.com</a></li>
<li><a href="http://www.thenest-vietnam.com/" class="spip_out" rel="external">Thenest-vietnam.com</a></li></ul>
<h3 class="spip"><a id="sommaire_7"></a>Electricité</h3>
<p>Le courant électrique est de 220 volts. Certaines prises de courant nécessitent des adaptateurs disponibles dans le pays. Des coupures d’eau ou d’électricité peuvent survenir accidentellement.</p>
<h3 class="spip"><a id="sommaire_8"></a>Electroménager</h3>
<p>Des marques européennes, japonaises ou coréennes d’électroménager se trouvent sur place à des prix supérieurs de 20% à ceux du marché français. Les lave-vaisselle ne sont pas un produit courant.</p>
<p>La climatisation et/ou des déshumidificateurs sont indispensables.</p>
<p><i>Mise à jour : février 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/vie-pratique/article/logement-111370). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
