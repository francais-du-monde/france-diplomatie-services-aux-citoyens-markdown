# Loisirs et culture

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/vie-pratique/article/loisirs-et-culture#sommaire_1">Tourisme</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/vie-pratique/article/loisirs-et-culture#sommaire_2">Activités culturelles</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/vie-pratique/article/loisirs-et-culture#sommaire_3">Sports</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/vie-pratique/article/loisirs-et-culture#sommaire_4">Télévision – Radio</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/vie-pratique/article/loisirs-et-culture#sommaire_5">Presse française</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Tourisme</h3>
<p>Pour tous renseignements d’ordre touristique, s’adresser à :<br class="autobr"><strong><br class="autobr"><a href="http://www.tourism-mauritius.mu/fr/" class="spip_out" rel="external">Mauritius Tourism Promotion Authority (MTPA)</a></strong><br class="manualbr">4-5th Floor, Victoria House<br class="manualbr">St Louis Street<br class="manualbr">Port Louis<br class="manualbr">Tel. : (230) 210 1545<br class="autobr">Fax : (230) 212 5142</p>
<p><strong>Bureau du MTPA en France</strong><br class="manualbr">Interface Tourism<br class="manualbr">11, Bis Rue Blanche<br class="manualbr">75009 Paris<br class="manualbr">Tel. : +33 1 53 25 11 11<br class="manualbr">Fax : +33 1 53 25 11 12<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/vie-pratique/article/loisirs-et-culture#ilemaurice#mc#interfacetourism.com#" title="ilemaurice..åt..interfacetourism.com" onclick="location.href=mc_lancerlien('ilemaurice','interfacetourism.com'); return false;" class="spip_mail">Courriel</a></p>
<h3 class="spip"><a id="sommaire_2"></a>Activités culturelles</h3>
<h4 class="spip">Activités culturelles en français</h4>
<p>L’Institut français de Maurice (IFM) possède, tout comme <strong>l’Alliance française</strong>, une <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/vie-pratique/article/loisirs-et-culture#mediatheque#mc#ifmaurice.org#" title="mediatheque..åt..ifmaurice.org" onclick="location.href=mc_lancerlien('mediatheque','ifmaurice.org'); return false;" class="spip_mail">médiathèque</a> qui propose une riche collection d’ouvrages sur tous supports : papier, audio mais aussi numériques. Des DVD et des disques compacts peuvent également être empruntés.</p>
<p>L’IFM est un lieu de rencontre et d’échange interculturel unique à Maurice. De nombreuses expositions, concerts, films, conférences ou ateliers y sont ainsi proposés.</p>
<p>Un espace de restauration permet par ailleurs de se détendre.</p>
<p><strong>Institut français de Maurice</strong><br class="manualbr">30 avenue Julius Nyerere. Rose Hill.<br class="manualbr">Tel : (230) 467 42 22- Fax : (230) 467 72 22<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/vie-pratique/article/loisirs-et-culture#info#mc#ifmaurice.org#" title="info..åt..ifmaurice.org" onclick="location.href=mc_lancerlien('info','ifmaurice.org'); return false;" class="spip_mail">Courriel</a></p>
<p>L’Institut est ouvert le lundi de 9h à 16h30, du mardi au vendredi de 9h à 17h30, le samedi de 9h à 16h. La Médiathèque est ouverte du mardi au vendredi de 10h à 17h30, le samedi de 9h à 16h.</p>
<p><strong>Alliance Française</strong><br class="manualbr">1, rue Victor Hugo - Bell Village <br class="manualbr">Port-Louis<br class="manualbr">Téléphone : (230) 212 29 49 <br class="manualbr">Télécopie : (230) 212 28 12<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/vie-pratique/article/loisirs-et-culture#afmaurice#mc#afmaurice.com#" title="afmaurice..åt..afmaurice.com" onclick="location.href=mc_lancerlien('afmaurice','afmaurice.com'); return false;" class="spip_mail">Courriel</a></p>
<p>L’Alliance française de Port-Louis possède différentes antennes sur l’île (à Goodlands, Mahebourg, Rivière Noire, Souillac et Triolet) mais aussi à Rodrigues (Port-Mathurin). On peut obtenir les coordonnées de ces annexes en s’adressant à l’Alliance française de Port-Louis.</p>
<h4 class="spip">Activités culturelles locales</h4>
<p><strong>Cinéma</strong></p>
<p>Les cinémas sont assez nombreux à Maurice. On retiendra notamment les complexes multisalles Star du Caudan (Port-Louis), de Bagatelles et de La Croisette(Grand Baie). Les divers cinémas présents à Maurice diffusent des productions américaines, bollywoodiennes et également françaises.</p>
<p><strong>Théâtre</strong></p>
<p>Port Louis possède un des tous premiers théâtres de l’hémisphère sud (inauguré en 1822). Toutefois, l’activité théâtrale à Maurice n’est pas très intense. Certains théâtres comme le théâtre Serge Constantin à Vacoas propose de temps en temps des spectacles.</p>
<p>Certaines salles de spectacles polyvalentes offrent régulièrement des spectacles à caractère ethno-culturel (danses indiennes, chinoises, etc.) mais accueillent également des spectacles de musique lyrique.</p>
<p>Enfin, le Conservatoire François Mitterrand propose régulièrement des concerts, souvent en partenariat avec l’IFM.</p>
<h3 class="spip"><a id="sommaire_3"></a>Sports</h3>
<p>On pratique naturellement tous les sports nautiques : ski, planche à voile, stand-up paddle, natation, plongée sous-marine, pêche au gros, etc…</p>
<p>L’île offre des parcours de golf de 18 trous - dont les tarifs sont assez élevés - et de nombreux parcours 9 trous ainsi que des terrains de tennis dans tous les hôtels.</p>
<p>Les clubs sportifs sont nombreux et de style britannique. Parmi eux, il y a notamment le Dodo Club, le Gymkana ou encore les deux complexes sportifs Riverland (Tamarin, Grand Baie). On trouve également de nombreuses salles de sports dans l’île à des prix relativement raisonnables.</p>
<p>Des courses hippiques attirent les foules à Port Louis tous les week-ends pendant la saison fraiche (mai à octobre).</p>
<p>La chasse peut être pratiquée de juin à septembre, notamment la chasse du cerf, dans des "chassés" privés sur invitation. L’importation d’une arme de chasse est soumise à autorisation de la police. Il est nécessaire de se procurer un permis auprès des autorités policières locales.</p>
<p>La pêche est autorisée toute l’année mais la pêche sous-marine est interdite.</p>
<p>Les montagnes de Maurice offrent également la possibilité de faire de belles randonnées, à condition d’être accompagné par un guide.</p>
<h3 class="spip"><a id="sommaire_4"></a>Télévision – Radio</h3>
<p>La télévision mauricienne (MBC) est constituée de trois chaînes analogiques (MBC 1, 2 et 3) et propose 17 chaînes digitales dont <i>France 24</i> et des chaînes dites « linguistiques » en créole, hindi, tamil, telegu, mandarin et hakka. La MBC diffuse des programmes en treize langues différentes.</p>
<p>La télévision payante est présente à Maurice au travers des groupes <i>Canalsat</i>, <i>Parabole Maurice</i> (filiale de Parabole Réunion), My.T (Orange, Mauritius Telecom), <i>London Satellite Systems</i> et <i>DSTV</i>.</p>
<p>Les chaines d’informations (Skynews, Euronews, BBC et France 24) et généralistes sont émises.</p>
<p>Radio France Internationale (RFI) est diffusée sur les ondes radio.</p>
<p>La MBC, groupe public, regroupe aussi six chaînes radios réparties selon les langues : Radio Maurice et Kool FM (français, anglais et créole), <i>Radio Mauritius</i> et <i>Taal FM</i> (langues asiatiques : hindi, tamoul, telugu, ourdou, marathi, gujarati, maithili, bojpuhri, mandarin et hakka), auxquelles viennent s’ajouter <i>Music FM</i>, d’une part, qui émet en anglais et en français et se consacre aux musiques en général ainsi qu’à la culture, et <i>Best FM</i>, d’autre part, qui diffuse de la musique bollywoodienne.</p>
<p>Radio One, Radio Plus et Top FM sont des radios privées qui émettent des programmes essentiellement en français.</p>
<h3 class="spip"><a id="sommaire_5"></a>Presse française</h3>
<p>Pratiquement tous les journaux français sont disponibles à Maurice, mais avec au minimum trois jours de retard par rapport à la France (coût plus élevé qu’en France).</p>
<p>Librairies assurant la diffusion de la presse et d’ouvrages français :</p>
<ul class="spip">
<li>Allot (Curepipe)</li>
<li>Le Trèfle (Curepipe et Port-Louis)</li>
<li>Papyrus (Grand-Baie)</li>
<li>Le Printemps (Vacoas)</li>
<li>Bookcourt (Caudan, Port Louis / La Croisette, Grand Baie)</li></ul>
<p>La presse française est aussi distribuée dans les grandes surfaces ("Continent" à Phoenix, "Super U" à Grand Baie, "London Supermarket" à Rivière Noire …).</p>
<p>A noter que la presse locale traite abondamment des questions françaises.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/vie-pratique/article/loisirs-et-culture). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
