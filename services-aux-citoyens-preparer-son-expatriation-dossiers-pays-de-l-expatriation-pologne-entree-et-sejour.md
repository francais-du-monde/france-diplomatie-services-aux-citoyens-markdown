# Entrée et séjour

<h2 class="rub23532">Passeport, visa, permis de travail</h2>
<p>Quel que soit le motif de votre séjour, renseignez-vous auprès de la section consulaire de l’ambassade de Pologne à Paris qui vous informera sur la règlementation en matière d’entrée et de séjour en Pologne, règlementation que vous devrez impérativement respecter. N’hésitez pas à consulter le site du ministère de l’Intérieur polonais afin de vous familiariser avec les modalités d’entrée et de séjour dans le pays.</p>
<p><strong>En aucun cas, vous n’êtes autorisé à travailler en Pologne sans permis adéquat. </strong></p>
<p>La Pologne fait partie de l’espace Schengen. Il n’y a donc plus, en temps normal, de contrôle aux frontières avec les autres états membres de l’espace Schengen.</p>
<p>Les ressortissants de l’Union européenne peuvent accéder au territoire polonais sur présentation d’un passeport ou d’une carte nationale d’identité en cours de validité.</p>
<p>Les enfants doivent être impérativement munis d’un passeport individuel ou d’une carte nationale d’identité.</p>
<p>Pour un <strong>séjour de plus de trois mois</strong>, il est nécessaire de procéder à l’<strong>enregistrement du séjour auprès des autorités polonaises</strong>, il faut se présenter à l’<strong>office de voïvodie</strong> (Urzad Wojewodzki).</p>
<p><strong>Pour les personnes résidant à Varsovie : </strong></p>
<p><a href="http://www.mazowsze.uw.gov.pl/" class="spip_out" rel="external">Mazowiecki Urzad Wojewodzki</a><br class="manualbr">Wydzial Spraw Obywatelskich i Migracji<br class="manualbr">Oddzial d/s Cudzoziemcow<br class="manualbr">Warszawa, ul Dluga 5<br class="manualbr">Tél. bureau pour les ressortissants des pays membres de l’Union européenne<br class="manualbr">Tél. : 48 (22) 695 67 52</p>
<p>Les personnes majeures se présentent personnellement ou sont représentées par leurs mandataires (une procuration doit être faite devant un notaire polonais).</p>
<p>Le dossier doit comprendre les documents suivants :</p>
<ul class="spip">
<li>4 formulaires de demande remplis et signés ;</li>
<li>copie d’une pièce d’identité (passeport ou carte nationale d’identité) ;</li>
<li>attestation d’enregistrement à la mairie (<i>zameldowanie</i>) ;</li>
<li>assurance (carte européenne) ;</li>
<li>ressources (relevé de compte) ;</li>
<li>si travail - contrat de travail ou promesse d’embauche ;</li>
<li>si études - inscription à l’école.</li></ul>
<p>Pour les membres de la famille (époux, épouse, enfants), il convient de présenter le livret de famille avec traduction assermentée.</p>
<p>Au bout de cinq ans de présence en Pologne, il est possible de demander la <strong>carte de séjour permanent</strong>.</p>
<p>Les ressortissants des pays membres de l’UE, ainsi que les membres de leur famille sont dispensés du permis de travail.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.paryz.msz.gov.pl/fr/paryz_fr_a_18" class="spip_out" rel="external">Ambassade de Pologne en France</a> </p>
<p><i>Mise à jour : juin 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-pologne-entree-et-sejour-article-animaux-domestiques-114279.md" title="Animaux domestiques">Animaux domestiques</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-pologne-entree-et-sejour-article-vaccination-114277.md" title="Vaccination">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-pologne-entree-et-sejour-article-demenagement-114276.md" title="Déménagement">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-pologne-entree-et-sejour-article-passeport-visa-permis-de-travail.md" title="Passeport, visa, permis de travail">Passeport, visa, permis de travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/entree-et-sejour/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
