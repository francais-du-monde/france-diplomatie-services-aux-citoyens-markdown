# Loisirs et culture

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/vie-pratique/article/loisirs-et-culture-111197#sommaire_1">Tourisme</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/vie-pratique/article/loisirs-et-culture-111197#sommaire_2">Activités culturelles</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/vie-pratique/article/loisirs-et-culture-111197#sommaire_3">Sports</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/vie-pratique/article/loisirs-et-culture-111197#sommaire_4">Télévision – Radio</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/vie-pratique/article/loisirs-et-culture-111197#sommaire_5">Presse française</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Tourisme</h3>
<p>Le patrimoine naturel de Madagascar en fait une candidate idéale à l’écotourisme encore peu développé. Outre les parcs nationaux, il existe sur tout le territoire plus d’une vingtaine de réserves spéciales accessibles avec un droit d’entrée.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.parcs-madagascar.com/" class="spip_out" rel="external">Les parcs de Madagascar</a>.</p>
<p>De nombreux sites d’intérêt culturel ou propices aux loisirs s’offrent également au visiteur.</p>
<p>A Tananarive, il faut citer le Palais de la Reine, les collines sacrées, le lac Itasy, le lac de Mantasoa, la table d’orientation d’Ambohipotsy, le Rova, le palais d’Andafiavaratra et le parc zoologique de Tsimbazaza. Les alentours de la ville ne sont pas en reste avec Ambohimanga, le Palais des Rois, le lac et le barrage de la Varahina.</p>
<p>A Tamatave, on pourra se rendre à l’ancien fort Hova de la région de Foulpointe, réputée pour sa plage sans requins, à l’île Sainte-Marie pour les activités nautiques, au parc de l’Ivoloina, à Fenerive-Est, à l’îlot Prune, à Ambila-Lemaintso où la baignade est protégée et à la plage de Mahambo.</p>
<p>A Diego-Suarez, les touristes ne manqueront pas de visiter le lac Sacré d’Anivorano, les grottes de l’Ankaratra, l’Ile de Nosy-Be, les plages environnantes, la montagne d’Ambre, réserve forestière et zoologique et l’île de Nosy-Boraha.</p>
<p>Pour en savoir plus :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  site de l’<a href="http://www.madagascar-tourisme.com/" class="spip_out" rel="external">Office National du tourisme de Madagascar</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Activités culturelles</h3>
<p><strong>Activités culturelles en français</strong></p>
<p>Une programmation riche et variée, y compris pour les enfants et les jeunes, concerts, spectacles de danse et théâtre, films, expositions, conférences et débats, rencontres avec des chercheurs, ateliers, etc. est offerte par l’Institut français de Madagascar (IFM), à Analakely. Une médiathèque propose une offre documentaire diversifiée et régulièrement actualisée, avec près de 40 000 documents sur tous supports.</p>
<p>L’Alliance française de Tananarive et le réseau des Alliances de province offrent de nombreuses activités culturelles et sociales ainsi que des médiathèques.</p>
<p>RFI et RLI sont les principales stations radio émettant en français.</p>
<p>Nombreux clubs et bars où l’on peut entendre de la musique malgache et du jazz.</p>
<p>Informations dans les magazines gratuits <a href="http://www.nocomment.mg/" class="spip_out" rel="external">No Comment</a> et Tana Planète et programme de l’<a href="http://www.institutfrancais-madagascar.com/" class="spip_out" rel="external">Institut français de Madagascar</a>.</p>
<h3 class="spip"><a id="sommaire_3"></a>Sports</h3>
<p>Différents sports, aussi bien collectifs qu’individuels, peuvent être pratiqués dans les clubs existants : rugby, football, tennis, golf, équitation, natation… Les côtes offrent des possibilités de sports nautiques (voile, planche à voile, kitesurf, plongée sous-marine et pêche au gros).</p>
<p>Se procurer des équipements sur place est à la fois difficile et coûteux ; il est donc préférable de s’en munir avant le départ.</p>
<p>Pour détenir un fusil de chasse, il est nécessaire d’obtenir une autorisation d’achat, d’introduction et de première détention ainsi qu’un permis de port d’arme.</p>
<p>Chasser requiert un permis dont la demande s’effectue auprès du Président du Comité exécutif du "Faritany" (province). La période de chasse se situe entre le premier dimanche de mai et le premier dimanche d’octobre.</p>
<p>La pêche est autorisée toute l’année sans permis.</p>
<h3 class="spip"><a id="sommaire_4"></a>Télévision – Radio</h3>
<p>Radio France Internationale (RFI) et RFO émettent quotidiennement et sont accessibles sur l’ensemble du territoire. A Tananarive, la réception de RFI est excellente (96.00 FM).</p>
<p>Alliance FM 92, partenaire de l’Alliance française d’Antananarivo, émet quelques heures par jour.</p>
<p>A Tananarive, deux chaînes locales de télévision hertziennes émettent en différé les journaux d’information des chaînes françaises. Les fournisseurs de programmes par satellite représentés à Madagascar sont Canal Satellite, Parabole Madagascar, TPS. Ils permettent de recevoir plus d’une centaine de chaînes françaises et étrangères.</p>
<h3 class="spip"><a id="sommaire_5"></a>Presse française</h3>
<p>La presse française est disponible sur place, à prix élevé toutefois. Les importations de livres français sont restreintes pour des raisons économiques. Livres ou journaux peuvent se trouver dans les lieux suivants à Tananarive :</p>
<ul class="spip">
<li>Librairie de Madagascar</li>
<li>Tout pour l’école</li>
<li>kiosque de l’Hôpital Militaire</li>
<li>réception de l’hôtel Hilton</li>
<li>Bibliomad et enfin supermarchés (presse).</li></ul>
<p><strong>Tamatave :</strong></p>
<ul class="spip">
<li>Librairie Fakra</li>
<li>Librairie Hachette.</li></ul>
<p><strong>Diego-Suarez :</strong></p>
<ul class="spip">
<li>Librairie Léong Hoi</li>
<li>Librairie Colbert.</li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/vie-pratique/article/loisirs-et-culture-111197). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
