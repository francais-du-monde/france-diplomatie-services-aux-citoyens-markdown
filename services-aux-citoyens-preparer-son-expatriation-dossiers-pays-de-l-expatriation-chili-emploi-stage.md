# Emploi, stage

<h2 class="rub22782">Marché du travail</h2>
<p>D’après les statistiques de l’<a href="http://www.ine.cl/" class="spip_out" rel="external">Institut national des statistiques chilien</a> (INE) de mars 2013, la population active s’élève à 8,2 millions, soit une augmentation de 1,1% par rapport au premier trimestre 2012. Ce chiffre comprend 7,7 millions d’actifs (4,6 millions d’hommes et 3,1 millions de femmes) et 510 000 de chômeurs. Au niveau national, le taux de chômage était de 6,2% au premier trimestre 2013. Après avoir atteint des niveaux historiquement faibles (niveau de 1973), une légère remontée du taux de chômage est anticipée.</p>
<p>Depuis mars 2007, le Chili est découpé administrativement du Nord au Sud en quinze régions.</p>
<p>En termes d’emplois les régions d’« Arica y Parinacota » et de « Valparaíso » sont les plus défavorisées. Celles du « Bío Bío » et de « la Araucanía » regroupent historiquement davantage de populations d’origines autochtones, qui demeurent les plus défavorisées. Notons l’existence de deux zones franches à chaque extrémité du pays dans la région de Tarapacá (Iquique) et dans la région de Magallanes y Antártica Chilena (Punta Arenas).</p>
<p>Les régions d’Atacama (mines de cuivre), du Maule (agriculture) et d’Aisén (tourisme, pèche, élevage, industrie forestière) comptent parmi les plus riches du pays.</p>
<p>Du fait de l’importance de l’agriculture au Chili, la période des récoltes (octobre - janvier) se traduit chaque année par une baisse temporaire du nombre de chômeurs (-100 000 personnes). Le taux d’emploi diffère également selon les catégories de population. Les plus jeunes, les moins diplômés et les femmes sont, comme en France, les catégories qui connaissent un taux de chômage supérieur à la moyenne.</p>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/emploi-stage/#sommaire_1">Secteurs à fort potentiel</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/emploi-stage/#sommaire_2">Secteurs à faible potentiel</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/emploi-stage/#sommaire_3">Rémunération</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Secteurs à fort potentiel</h3>
<ul class="spip">
<li>Industrie agroalimentaire, viticole et vinicole ;</li>
<li>Environnement ;</li>
<li>Hôtellerie / restauration ;</li>
<li>Recherche scientifique (chimie, géologie) ;</li>
<li>Informatique et nouvelles technologies de l’information ;</li>
<li>Finance, assurance ;</li>
<li>Biens de consommation, produits de luxe.</li></ul>
<h4 class="spip">Exemples de professions</h4>
<ul class="spip">
<li><strong>Cuisinier (chef, pâtissier)</strong> : traditionnellement, les cuisiniers français sont appréciés. Les bons restaurants à Santiago se réclament souvent de la tradition culinaire française. Signalons, par exemple, l’existence d’une Ecole de cuisine française à Santiago.</li>
<li><strong>Secrétaire / Assistant de direction </strong> : les emplois administratifs peuvent s’avérer intéressants pour les Français possédant l’expérience appropriée, à condition qu’ils maîtrisent parfaitement la langue espagnole et qu’ils s’adaptent à la culture chilienne.</li>
<li><strong>Transport / Logistique / Import-export </strong> : centrées autour des principaux ports et de l’aéroport de Santiago, les activités logistiques entraînent une demande importante de personnel expérimenté et multilingue.</li>
<li><strong>Informaticien </strong> : le Chili est le pays le plus informatisé d’Amérique latine. L’offre est abondante (logiciels d’entreprise) et il existe de bonnes opportunités pour les informaticiens réseau / maintenance / développement.</li>
<li><strong>Secteur minier </strong> : le secteur minier chilien (cuivre, mobdylène) tire profit de l’accroissement de la demande mondiale et il existe une forte demande en personnel qualifié dans ce domaine (géologie, chimie, ingénieur). A noter que pour les postes sur le terrain, les principales mines sont très éloignées de Santiago et dans des endroits le plus souvent isolés.</li>
<li><strong>Finance </strong> : du fait notamment de sa stabilité politique et monétaire, Santiago tend à devenir la plaque tournante financière de l’Amérique latine. Les personnes maîtrisant plusieurs langues et ayant des compétences dans le domaine financier peuvent faire le lien entre l’Amérique latine, l’Europe et l’Amérique du Nord.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Secteurs à faible potentiel</h3>
<p><strong>Publicité, communication </strong> : il existe une offre locale abondante. Travailler dans les métiers de la communication suppose au préalable de maîtriser parfaitement la langue, la culture et les réseaux de communication locaux.</p>
<p><strong>Professions juridiques et médicales </strong> : du fait de l’absence de reconnaissance des diplômes, il est difficile pour un juriste, un médecin français ou un spécialiste des ressources humaines (droit du travail) de s’établir au Chili.</p>
<h3 class="spip"><a id="sommaire_3"></a>Rémunération</h3>
<p>Au Chili, l’usage veut que vous négociez avec votre employeur le salaire net (<i>sueldo liquido</i>). En effet, les charges salariales sont retenues directement à la source et payées par l’employeur. A noter que les charges représentent un surcoût pour l’entreprise d’environ 20%.</p>
<h4 class="spip">Montant</h4>
<p>En juillet 2012, le salaire minimum brut a été fixé en 193.000 pesos chiliens ($), soit environ 294 €.</p>
<p>Au sein d’une même entreprise, les écarts de salaires sont globalement plus importants qu’en Europe. De nombreux jeunes arrivent actuellement sur le marché du travail, tirant ainsi les salaires vers le bas.</p>
<p>Le montant du salaire dépend donc de la profession, de la façon de préparer son voyage et des capacités de chacun à se vendre.</p>
<p><strong>Quelques exemples de salaires (bruts mensuels, en pesos chiliens et en euros)</strong></p>
<p>Il n’existe pas vraiment de norme au Chili dans ce domaine. Les salaires peuvent varier du simple au double en fonction des facteurs suivants : la taille de l’entreprise, la nationalité de l’entreprise (chilienne ou étrangère) et l’expérience du candidat. Cette grille n’a donc qu’une valeur indicative.</p>
<table class="spip">
<thead><tr class="row_first"><th id="id447d_c0">Fonctions</th><th id="id447d_c1">Salaire en pesos</th><th id="id447d_c2">Equivalent en euros</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="id447d_c0">Secrétaire bilingue</td>
<td headers="id447d_c1">de 400 000
<p>à 900 000</p>
</td>
<td headers="id447d_c2">625
<p>1400</p>
</td></tr>
<tr class="row_even even">
<td headers="id447d_c0">Ingénieur</td>
<td headers="id447d_c1">de 1 000 000
<p>à 2 500 000</p>
</td>
<td headers="id447d_c2">1550
<p>3900 (le salaire double dans le secteur minier)</p>
</td></tr>
<tr class="row_odd odd">
<td headers="id447d_c0">Assistante de direction trilingue</td>
<td headers="id447d_c1">de 800 000
<p>à 1 500 000</p>
</td>
<td headers="id447d_c2">1250
<p>1800</p>
</td></tr>
<tr class="row_even even">
<td headers="id447d_c0">Comptable</td>
<td headers="id447d_c1">de 400 000
<p>à 1 000 000</p>
</td>
<td headers="id447d_c2">625
<p>1550</p>
</td></tr>
<tr class="row_odd odd">
<td headers="id447d_c0">Responsable financier</td>
<td headers="id447d_c1">de 1 500 000
<p>à 2 000 000</p>
</td>
<td headers="id447d_c2">2350
<p>3125 €</p>
</td></tr>
<tr class="row_even even">
<td headers="id447d_c0">Chef de produit</td>
<td headers="id447d_c1">de 1 500 000
<p>à 2 200 000</p>
</td>
<td headers="id447d_c2">2350 €
<p>3425 €</p>
</td></tr>
<tr class="row_odd odd">
<td headers="id447d_c0">Assistant marketing / administratif / export</td>
<td headers="id447d_c1">de 800 000
<p>à 1 000 000</p>
</td>
<td headers="id447d_c2">1250
<p>1550</p>
</td></tr>
<tr class="row_even even">
<td headers="id447d_c0">Directeur commercial</td>
<td headers="id447d_c1">supérieur à 1 500 000</td>
<td headers="id447d_c2">supérieur à 2350</td></tr>
<tr class="row_odd odd">
<td headers="id447d_c0">Traducteur (tarifs à négocier selon l’urgence et la technicité du document à traduire)</td>
<td headers="id447d_c1">entre 28 et 40 pesos par mot</td>
<td headers="id447d_c2">près de 500 les 10 000 mots</td></tr>
<tr class="row_even even">
<td headers="id447d_c0">Enseignement du français dans une école de langues</td>
<td headers="id447d_c1"></td>
<td headers="id447d_c2">entre 9 et 18 € l’heure</td></tr>
<tr class="row_odd odd">
<td headers="id447d_c0">Interprète</td>
<td headers="id447d_c1"></td>
<td headers="id447d_c2">environ 340 € pour la journée</td></tr>
</tbody>
</table>
<p><i>Mise à jour : décembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chili-emploi-stage-article-stages.md" title="Stages">Stages</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chili-emploi-stage-article-entretien-d-embauche.md" title="Entretien d’embauche">Entretien d’embauche</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chili-emploi-stage-article-lettre-de-motivation.md" title="Lettre de motivation">Lettre de motivation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chili-emploi-stage-article-curriculum-vitae-109753.md" title="Curriculum vitae">Curriculum vitae</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chili-emploi-stage-article-recherche-d-emploi.md" title="Recherche d’emploi">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chili-emploi-stage-article-reglementation-du-travail.md" title="Réglementation du travail">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-chili-emploi-stage-article-marche-du-travail.md" title="Marché du travail">Marché du travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/emploi-stage/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
