# Logement

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/vie-pratique/article/logement-113557#sommaire_1">Conditions de location</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/vie-pratique/article/logement-113557#sommaire_2">Hôtels</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/vie-pratique/article/logement-113557#sommaire_3">Electricité</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/vie-pratique/article/logement-113557#sommaire_4">Electroménager</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Conditions de location</h3>
<p>Il n’existe pas à proprement parler de quartier résidentiel à Lomé. Il est plus facile de trouver une villa qu’un appartement. Le délai moyen de recherche est d’environ un mois. Les logements sont généralement loués vides, les cuisines sont rarement équipées. La durée du bail est la plupart du temps fixée à deux ans. Une avance de trois à six mois de loyer est demandée par le propriétaire. Un état des lieux est recommandé. De nombreuses villas sont disponibles sur le marché, la plupart nécessite des travaux assez importants avant emménagement. En cas de recherche d’un logement par l’intermédiaire d’une agence de location, prévoir une commission égale à un demi mois de loyer.</p>
<p><strong>Coût du marché locatif à Lomé (en euros)</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td></td>
<td>Studio</td>
<td>3 pièces</td>
<td>Villa 4 pièces</td>
<td>Villa 5 à 8 pièces</td></tr>
<tr class="row_even even">
<td>Près de l’ambassade</td>
<td>305</td>
<td>600</td>
<td>650</td>
<td>1200 à 2250</td></tr>
<tr class="row_odd odd">
<td>Quartier résidentiel</td>
<td>400</td>
<td>1000</td>
<td>1200</td>
<td>1300 a 2250</td></tr>
</tbody>
</table>
<p>Selon la taille de la maison, l’ensemble des charges varient de 350 à 1000 €, comprenant : eau, gaz, climatisation et électricité, gardiennage, impôts locaux, taxes sur le ramassage des ordures ménagères.</p>
<p>Les services tels que l’eau, l’électricité et le téléphone fonctionnent dans l’ensemble correctement. Toutefois, selon le quartier, la distribution de l’eau est problématique à certaines heures de la journée ; prévoir, le cas échéant, un surpresseur. Il existe aussi des problèmes réguliers de surtension.</p>
<h3 class="spip"><a id="sommaire_2"></a>Hôtels</h3>
<p><strong>Lomé</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Prix moyen d’une chambre d’hôtel (chambre double)</strong></td>
<td>francs CFA</td>
<td>Euros</td></tr>
<tr class="row_even even">
<td>Grand tourisme</td>
<td>99 705</td>
<td>152</td></tr>
<tr class="row_odd odd">
<td>Moyen tourisme</td>
<td>51 850</td>
<td>80</td></tr>
</tbody>
</table>
<p><strong>Auberges de jeunesse</strong></p>
<p>Il est possible de trouver dans la plupart des grandes agglomérations togolaises des centres communautaires d’hébergement qui peuvent s’apparenter à des auberges de jeunesse.</p>
<p>L’hygiène et les services proposés sont d’une qualité très variable mais les prix très abordables.</p>
<h3 class="spip"><a id="sommaire_3"></a>Electricité</h3>
<p>Le courant est alternatif, 220 volts, 50 Hz. Les prises de courant sont de type bipolaires et européennes tri-polaires.</p>
<h3 class="spip"><a id="sommaire_4"></a>Electroménager</h3>
<p>On trouve sur place tous les équipements ménagers nécessaires, de même que la vaisselle, le linge de maison et le mobilier. Concernant l’électroménager, les mêmes marques qu’en France sont disponibles.</p>
<p>La climatisation est nécessaire (à défaut, les appareils électriques, photographiques, vidéo, tapis, livres, objets en cuir, en tissu, ou métalliques, disques, peuvent être endommagés).</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/togo/vie-pratique/article/logement-113557). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
