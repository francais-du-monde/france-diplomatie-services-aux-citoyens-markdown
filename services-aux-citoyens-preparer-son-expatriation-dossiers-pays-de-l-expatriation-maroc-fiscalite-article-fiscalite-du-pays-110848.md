# Fiscalité du pays

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/fiscalite/article/fiscalite-du-pays-110848#sommaire_1">L’impôt sur le revenu (IR)</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/fiscalite/article/fiscalite-du-pays-110848#sommaire_2">Barème de l’impôt</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/fiscalite/article/fiscalite-du-pays-110848#sommaire_3">Impôt sur les sociétés</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/fiscalite/article/fiscalite-du-pays-110848#sommaire_4">Taxe sur la valeur ajoutée (TVA)</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/fiscalite/article/fiscalite-du-pays-110848#sommaire_5">Fiscalité liée à l’acquisition d’un bien immobilier</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/fiscalite/article/fiscalite-du-pays-110848#sommaire_6">Quitus fiscal</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/fiscalite/article/fiscalite-du-pays-110848#sommaire_7">Solde du compte en fin de séjour</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/fiscalite/article/fiscalite-du-pays-110848#sommaire_8">Coordonnées des centres d’information fiscale</a></li></ul>
<p>Le régime fiscal marocain se divise entre impôts directs, taxe sur la valeur ajoutée et droits d’enregistrement et de timbre.</p>
<p>Les principaux impôts sont l’impôt sur les sociétés (IS), qui concerne les revenus et bénéfices réalisés par les sociétés et autres personnes morales, l’impôt sur le revenu (IR), qui concerne les revenus et bénéfices des personnes physiques et des sociétés de personnes, et la taxe sur la valeur ajoutée (TVA), qui s’applique aux dépenses de consommation.</p>
<p>L’<strong>année fiscale</strong> correspond à l’année civile.</p>
<h3 class="spip"><a id="sommaire_1"></a>L’impôt sur le revenu (IR)</h3>
<p>L’IR s’applique aux revenus et profits des personnes physiques et des sociétés de personnes n’ayant pas opté pour l’IS. Sont concernés :</p>
<ul class="spip">
<li>les revenus salariaux et assimilés ;</li>
<li>les revenus professionnels ;</li>
<li>les revenus et profits fonciers ;</li>
<li>les revenus et profits de capitaux mobiliers ;</li>
<li>les revenus agricoles.</li></ul>
<p>Sont considérés comme revenus salariaux : les traitements, les indemnités et émoluments, les salaires, les pensions et les rentes viagères, les allocations spéciales, les remboursements forfaitaires de frais et autres rémunérations allouées aux dirigeants de sociétés. Les avantages en argent ou en nature accordés en sus des revenus précités sont assimilés à des revenus salariaux.</p>
<p>L’IR sur les salaires est généralement <strong>prélevé par retenue à la source</strong>, le montant de l’impôt étant versé directement par l’employeur au Trésor.</p>
<h4 class="spip">Détermination du revenu brut imposable</h4>
<p>Le revenu brut imposable est formé du montant total des traitements et salaires perçus, ainsi que de tous les avantages en argent ou en nature accordés en sus du traitement.</p>
<p>Les principales exonérations sont :</p>
<ul class="spip">
<li>les indemnités destinées à couvrir certains frais engagés dans l’exercice de la profession (sur justificatif), dans la mesure où elles correspondent aux frais réels (frais de déplacement, de représentation, etc.) ;</li>
<li>les allocations familiales ;</li>
<li>les pensions alimentaires ;</li>
<li>les pensions d’invalidité ;</li>
<li>les rentes viagères ;</li>
<li>les indemnités journalières de maladie, d’accident et de maternité ;</li>
<li>la part patronale des cotisations de retraite et de sécurité sociale ;</li>
<li>la part patronale des primes d’assurance-groupe couvrant les risques de maladie, maternité, invalidité et décès ;</li>
<li>les indemnités de licenciement dans la limite fixée par la législation et la réglementation en vigueur.</li></ul>
<h4 class="spip">Détermination du revenu net imposable</h4>
<p>Le revenu net imposable (RNI) s’obtient par déduction du revenu brut imposable (RBI) :</p>
<ul class="spip">
<li>d’une somme forfaitaire pour frais inhérents à la profession. Cette somme s’élève à 20% du revenu brut imposable (sauf cas particuliers, tels que les journalistes, les ouvriers mineurs, le personnel navigant de la marine marchande, etc.) avec plafonnement à 30 000 MAD ;</li>
<li>des retenues supportées pour la constitution des pensions ou retraites.</li></ul>
<p>Les personnes de nationalité étrangère déduisent leurs cotisations aux organismes de retraite étrangers dans la limite du taux des retenues supportées par le personnel de l’entreprise ou de l’administration marocaine dont dépendent lesdites personnes.</p>
<p>Le revenu net imposable constitue l’assiette de l’IR.</p>
<p>Pour plus d’informations, vous pouvez consulter le site Internet du <a href="http://www.finances.gov.ma/" class="spip_out" rel="external">ministère de l’Economie et des Finances marocain</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Barème de l’impôt</h3>
<p><strong>Calcul de l’impôt général sur le revenu (IR)</strong></p>
<table class="spip">
<thead><tr class="row_first"><th id="id900e_c0">  Tranches de revenu net global imposable </th><th id="id900e_c1">  Taux applicable à chaque tranche </th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="id900e_c0">de 1 à 30 000 MAD</td>
<td headers="id900e_c1">0 %</td></tr>
<tr class="row_even even">
<td headers="id900e_c0">de 30 001 à 50 000 MAD</td>
<td headers="id900e_c1">10 % (abattement = 3000 MAD)</td></tr>
<tr class="row_odd odd">
<td headers="id900e_c0">de 50 001 à 60000 MAD</td>
<td headers="id900e_c1">20 % (abattement = 8000 MAD)</td></tr>
<tr class="row_even even">
<td headers="id900e_c0">de 60 001 à 80 000 MAD</td>
<td headers="id900e_c1">30 % (abattement = 14 000 MAD)</td></tr>
<tr class="row_odd odd">
<td headers="id900e_c0">de 80 001 à 180 000 MAD</td>
<td headers="id900e_c1">34 % (abattement = 17 200 MAD)</td></tr>
<tr class="row_even even">
<td headers="id900e_c0">au-delà de 180 001 MAD</td>
<td headers="id900e_c1">38 % (abattement = 24 400 MAD)</td></tr>
</tbody>
</table>
<h4 class="spip">Déductions du montant de l’impôt au titre des charges de famille</h4>
<p>Réduction pour charge de famille : 360 MAD par personne à charge (le conjoint et les enfants de moins de 25 ans) par an dans la limite de 2 160 MAD. Sont considérées comme personnes à charge l’épouse, les enfants du contribuable et les enfants légalement recueillis, lorsqu’ils ne disposent pas d’un revenu global annuel supérieur à 20 000 MAD. Il n’y a pas de conditions d’âge pour les enfants handicapés à charge.</p>
<h4 class="spip">Information sur la fiscalité des étrangers retraités</h4>
<p>Dès lors que vous transférez, au Maroc à titre définitif, les sommes correspondant à vos retraites dans un compte en dirhams non convertibles, vous bénéficierez d’une réduction de 80% du montant de l’impôt dû au titre de votre pension globale correspondant aux sommes transférées.</p>
<p>Pour bénéficier de cette atténuation, vous devez produire avant le 31 mars de chaque année une déclaration du revenu global, prévue par les dispositions de l’article 82 du Code général des impôts (CGI), accompagnée des documents ci-après : une attestation de versement des pensions établie par le débirentier ou tout autre document en tenant lieu ; une attestation indiquant le montant en devises reçu pour le compte du pensionné et la contre valeur en dirhams au jour du transfert délivrée par l’établissement de banque ou de crédit ou par tout autre organisme intervenant dans le paiement des pensions susvisées.</p>
<p>Il est à rappeler qu’une personne physique a son domicile fiscal au Maroc lorsqu’elle a au Maroc son foyer permanent d’habitation, le centre de ses intérêts économiques ou lorsque la durée continue ou discontinue de ses séjours au Maroc dépasse 183 jours pour toute période de 365 jours.</p>
<h4 class="spip">Taux d’imposition des plus-values</h4>
<p>Le taux de l’IR applicable aux revenus et profits immobiliers s’élève à 20% et ne peut être inférieur à 3% du prix de cession.</p>
<h3 class="spip"><a id="sommaire_3"></a>Impôt sur les sociétés</h3>
<p>L’impôt sur les sociétés (IS) s’applique obligatoirement aux revenus et profits des sociétés de capitaux, des établissements publics et autres personnes morales qui réalisent des opérations lucratives et sur option irrévocable aux sociétés de personnes.</p>
<p>Le résultat fiscal imposable est égal à l’excédent des produits d’exploitation, profits et gains sur les charges d’exploitation, modifié, le cas échéant, conformément à la législation fiscale en vigueur.</p>
<p>Le taux de l’IS est de 30 %. Des taux spécifiques existent, tout comme des exonérations totales et temporaires (en faveur notament des promoteurs immobiliers, des entreprises hôtelières, des entreprises minières).</p>
<h3 class="spip"><a id="sommaire_4"></a>Taxe sur la valeur ajoutée (TVA)</h3>
<p>Le taux normal est de 20%. Les taux réduits sont de 7% pour certains produits de grande consommation, de 10% sur certains produits alimentaires, boissons et l’hôtellerie notamment et de 14% pour d’autres produits.</p>
<h3 class="spip"><a id="sommaire_5"></a>Fiscalité liée à l’acquisition d’un bien immobilier</h3>
<p>L’acquisition d’un bien immobilier bâti ou non bâti est ouverte à tout étranger qu’il soit personne physique ou morale à l’exception des terrains à vocation agricole. Tout étranger souhaitant acquérir un bien immobilier au Maroc devra au préalable ouvrir auprès d’une banque marocaine un compte en dirhams convertibles en devises. Ce compte permettra le transfert de l’argent nécessaire à l’achat vers le Maroc, et facilitera le rapatriement en cas de revente éventuelle, du produit de la vente et de la plus-value.</p>
<p>Les droits et taxes liés à l’acquisition du bien immobilier sont dus par l’acquéreur. Ces droits varient selon le type de bien.</p>
<p>L’acquisition d’un bien immobilier entraîne l’obligation d’acquitter un certain nombre d’impôts et taxes. Généralement, le type d’impôts à payer est fonction de la nature du bien immobilier (ces pourcentages s’appliquent à la valeur du bien acquis, TVA comprise).</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Maison à usage d’habitation, pour une durée supérieure à trois ans ou terrain non bâti avec engagement de bâtir dans un délai de sept ans :
<br>— Droits d’enregistrement : 2,5% Taxe notariale : 0,5% Conservation foncière : 1%
<br>— Conservation foncière : 1% + 150 DH (certificat de propriété).
</p>
<p>A noter qu’en cas d’acquisition d’un bien non titré il faut ajouter les frais de titrage. Ces frais dépendent de la superficie du sol et du prix du bien acquis. Ils peuvent parfois être partagés avec le vendeur.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Terrains non bâti sans engagement de bâtir
<br>— Droit d’enregistrement : 5% Taxe notariale : 0,5% Conservation foncière : 1%</p>
<p><strong>Source </strong> : <a href="http://www.amb-maroc.fr/questions/foire-aux-questions.htm#l5" class="spip_out" rel="external">Ambassade du Maroc en France</a></p>
<h3 class="spip"><a id="sommaire_6"></a>Quitus fiscal</h3>
<p>Un quitus fiscal est exigé avant de quitter le pays.</p>
<h3 class="spip"><a id="sommaire_7"></a>Solde du compte en fin de séjour</h3>
<p>Un expatrié français peut solder son compte en fin de séjour, particulièrement s’il a ouvert un compte en dirhams convertibles, alimenté par ses revenus en dirhams.</p>
<h3 class="spip"><a id="sommaire_8"></a>Coordonnées des centres d’information fiscale</h3>
<p>Vous trouverez les coordonnées du bureau d’accueil de votre lieu de résidence sur le site Internet de la <a href="http://portail.tax.gov.ma/" class="spip_out" rel="external">Direction générale des impôts</a>.</p>
<p><strong>Pour en savoir plus</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="https://www.tresor.economie.gouv.fr/Pays/maroc" class="spip_out" rel="external">Service économique français au Maroc</a></p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/fiscalite/article/fiscalite-du-pays-110848). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
