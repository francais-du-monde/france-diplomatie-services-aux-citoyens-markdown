# Régime local de sécurité sociale

<p>Vous trouverez une présentation détaillée du système de sécurité sociale lituanien sur le site de notre partenaire, le <a href="http://www.cleiss.fr/" class="spip_out" rel="external">CLEISS</a> (Centre des liaisons européennes et internationales de sécurité sociale). En voici la table des matières :</p>
<ul class="spip">
<li><a href="http://www.cleiss.fr/docs/regimes/regime_lituanie.html#intro" class="spip_out" rel="external">Introduction</a></li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_lituanie.html#generalites" class="spip_out" rel="external">Généralités</a> (Organisation, financement, taux de cotisation sur les salaires…)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_lituanie.html#maladie" class="spip_out" rel="external">Maladie-maternité</a> (Prestations en nature et en espèces)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_lituanie.html#invalidite" class="spip_out" rel="external">Invalidité</a>, <a href="http://www.cleiss.fr/docs/regimes/regime_lituanie.html#vieillesse" class="spip_out" rel="external">Vieillesse</a>, <a href="http://www.cleiss.fr/docs/regimes/regime_lituanie.html#deces" class="spip_out" rel="external">Décès - survivants</a> (Pensions de vieillesse, d’invalidité, de survivant, aide sociale)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_lituanie.html#adtmp" class="spip_out" rel="external">Accidents du travail - maladies professionnelles</a> (Régime indemnitaire, y compris pour les survivants)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_lituanie.html#chomage" class="spip_out" rel="external">Chômage</a> (Indemnités et assistance chômage)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_lituanie.html#pf" class="spip_out" rel="external">Prestations familiales</a> (Allocations familiales, de naissance, d’adoption…).</li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/protection-sociale/article/regime-local-de-securite-sociale-111149). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
