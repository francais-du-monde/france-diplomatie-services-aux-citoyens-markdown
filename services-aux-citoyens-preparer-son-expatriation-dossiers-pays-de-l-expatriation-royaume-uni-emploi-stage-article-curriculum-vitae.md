# Curriculum vitae

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/emploi-stage/article/curriculum-vitae#sommaire_1">Rédaction</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/emploi-stage/article/curriculum-vitae#sommaire_2">Diplômes</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/emploi-stage/article/curriculum-vitae#sommaire_3">Modèles de CV</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/emploi-stage/article/curriculum-vitae#sommaire_4">Pour en savoir plus</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Rédaction</h3>
<p>Les indications suivantes sont fournies à titre indicatif et constituent des recommandations d’ordre général. Une approche différente peut être demandée selon le poste recherché, les compétences du candidat et sa personnalité.</p>
<ul class="spip">
<li>le CV doit être traduit en anglais, dactylographié et présenté dans l’ordre chronologique inversé en ce qui concerne la formation et l’expérience professionnelle ;</li>
<li>il doit être détaillé tout en restant sobre ;</li>
<li>le CV doit mentionner les activités extra-professionnelles. Il est préférable de ne pas mentionner les noms et adresses d’anciens employeurs susceptibles d’apporter un témoignage sur les réalisations et les compétences professionnelles du candidat. Par contre, il est possible d’indiquer "references available upon request".</li></ul>
<p>S’il n’existe pas de règle générale quant à la longueur du C.V. (une à trois pages), il est conseillé de se limiter à deux pages A4 (recto-verso).</p>
<h4 class="spip">Le C.V. est organisé en rubriques</h4>
<ul class="spip">
<li><i>Objective</i> ou <i>Personal Profile</i> : une courte introduction (quatre lignes maximum), rédigée à la première personne qui présente l’expérience acquise, ainsi que le poste ou la fonction recherchée ;</li>
<li><i>Employment</i>, <i>Career History</i> ou <i>Experience</i> : cette rubrique présente les expériences de la plus récente à la plus ancienne et décrit avec précision les tâches réalisées et les résultats obtenus (indiquez des chiffres, des exemples de réalisation, etc). Il est préférable d’éviter les vides chronologiques ;</li>
<li><i>Education and Qualification</i> : cette rubrique décrit la formation scolaire et universitaire. Pour connaître les équivalences entre diplômes français et britanniques, il est possible de s’adresser au British Council ;</li>
<li><i>Languages and Other Skills</i> : les compétences informatiques et linguistiques sont précisées sous la rubrique, qui doit laisser apparaître le niveau de pratique pour chaque langue ou environnement informatique ;</li>
<li><i>Additional Information</i> ou <i>Interests</i> : cette rubrique peut mettre en valeur des expériences complémentaires, susceptibles d’attirer l’attention de l’employeur dans la perspective du poste recherché. Certaines activités associatives peuvent par exemple constituer un plus ;</li>
<li><i>References</i> ou <i>Referees</i> : si vous ne souhaitez pas faire figurer directement l’idéntité d’anciens employeurs sur le CV, vous pouvez écrire <i>References available on request</i>.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Diplômes</h3>
<p>Pour connaître les équivalences entre diplômes français et britanniques, il est possible de s’adresser aux organismes suivants :</p>
<p><a href="http://www.britishcouncil.fr/" class="spip_out" rel="external">British Council France</a><br class="manualbr">9 rue de Constantine<br class="manualbr">75340 Paris Cedex 07<br class="manualbr">Tél : (0)1 49 55 73 00<br class="manualbr">Fax : (0)1 47 05 77 02<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/emploi-stage/article/curriculum-vitae#Information#mc#britishcouncil.fr#" title="Information..åt..britishcouncil.fr" onclick="location.href=mc_lancerlien('Information','britishcouncil.fr'); return false;" class="spip_mail">Courriel</a></p>
<p>Voir le lien : <a href="http://www.britishcouncil.org/fr/france-educationuk-equivalence-diplome.htm" class="spip_out" rel="external">www.britishcouncil.org/fr/france-educationuk-equivalence-diplome.htm</a></p>
<p><a href="http://www.naric.org.uk/" class="spip_out" rel="external">UK NARIC</a><br class="manualbr">Oriel House Oriel Road Cheltenham<br class="manualbr">Glos, GL50 1XP<br class="manualbr">Tél : +44 (0)871 330 7033 <br class="manualbr">Fax : +44 (0)871 330 7005<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/emploi-stage/article/curriculum-vitae#info#mc#naric.org.uk#" title="info..åt..naric.org.uk" onclick="location.href=mc_lancerlien('info','naric.org.uk'); return false;" class="spip_mail">Courriel</a></p>
<p>Vous trouverez également des informations sur le site d<a href="http://www.ucas.com/" class="spip_out" rel="external">’UCAS</a>(Universities and Colleges Admissions Service).</p>
<h3 class="spip"><a id="sommaire_3"></a>Modèles de CV</h3>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  CV <a href="http://www.europass.cedefop.europa.eu/" class="spip_out" rel="external">Europass</a>.</p>
<h3 class="spip"><a id="sommaire_4"></a>Pour en savoir plus</h3>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://nationalcareersservice.direct.gov.uk/" class="spip_out" rel="external">National careers service</a> (conseils pour la rédaction du CV en anglais, l’entretien…)</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/emploi-stage/article/curriculum-vitae). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
