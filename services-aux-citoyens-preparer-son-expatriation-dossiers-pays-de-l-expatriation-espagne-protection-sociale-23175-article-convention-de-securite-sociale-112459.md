# Convention de sécurité sociale

<p>L’Espagne, qui est l’un des 28 États membres de l’Union européenne, est liée à la France par un certain nombre d’accords de sécurité sociale dont les principaux sont les règlements (CEE) n°1408/71 et 574/72, relatifs à l’application des régimes de sécurité sociale aux travailleurs salariés et non salariés et aux membres de leur famille qui se déplacent à l’intérieur de la Communauté et (CE) 883/2004 et 987/2009, portant sur la coordination des systèmes de sécurité sociale.</p>
<p>Dans le cadre de l’accord sur l’Espace économique européen (E.E.E.) et de l’accord entre l’Union européenne et la Suisse, les règlements communautaires 1408/71 et 574/72 sont applicables aux territoires et aux ressortissants de l’Islande, de la Norvège, du Liechtenstein et de la Suisse.</p>
<p>Les Français occupés relèvent, en principe, obligatoirement du régime local de protection sociale. Ils bénéficient, par ailleurs, des règlements communautaires de sécurité sociale leur permettant, en quelque sorte, le passage du régime français au régime de sécurité sociale espagnole et réciproquement.</p>
<p>Les Français occupés peuvent, s’ils le désirent, adhérer à l’assurance volontaire "expatriés" auprès de la Caisse des Français de l’étranger. Il convient de préciser qu’une telle adhésion ne dispense pas les intéressés des obligations d’assurance existant dans le pays de travail.</p>
<p>Les Français travaillant peuvent aussi être maintenus au régime français de protection sociale, c’est-à-dire détachés dans le cadre des seuls règlements communautaires de sécurité sociale.</p>
<p>Bien entendu, les Français se trouvant en tant que touristes, étudiants, retraités ou chômeurs cherchant un emploi peuvent bénéficier également des règlements communautaires.</p>
<p>Pour plus d’information, consultez la page internet du <a href="http://www.cleiss.fr/" class="spip_out" rel="external">Centre des liaisons européennes et internationales de sécurité sociale (CLEISS)</a>.</p>
<p><i>Mise à jour : avril 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/protection-sociale-23175/article/convention-de-securite-sociale-112459). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
