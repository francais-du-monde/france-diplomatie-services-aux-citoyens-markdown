# Santé

<p>Pour une présentation des risques épidémiologiques ainsi que des conseils sur la médecine au quotidien, consultez les pages suivantes :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Fiches Diego Suarez, Majunga et Tananarive sur le site du <a href="http://www.cimed.org" class="spip_out" rel="external">CIMED</a>.</p>
<h4 class="spip">Médecine de soins</h4>
<p><strong>Conditions d’hygiène et état sanitaire</strong></p>
<p>Les possibilités de soins à <strong>Tananarive</strong> sont très limitées et toute intervention sérieuse implique une évacuation vers l’île de La Réunion. Il existe un centre médico-social à Tananarive pour la médecine courante ainsi qu’un hôpital militaire (sous équipé et manque d’hygiène). Le CMS envoie les patients à l’Institut Pasteur pour les analyses nécessaires.</p>
<p>Une consultation chez un médecin généraliste coûte 18 € et de 35 à 50 € chez un spécialiste. Pour les soins dentaires, comptez de 25 à 45 € selon la nature des soins.</p>
<p>Les médicaments - importés de France - coûtent de 15 à 25% plus chers par rapport à la métropole.</p>
<p><strong>A Diego Suarez</strong>, les conditions d’hygiène et de santé sont médiocres. L’eau distribuée au robinet n’est pas potable. Le seul établissement hospitalier à la disposition du public est l’hôpital "Be" (sous-équipement, absence d’entretien des locaux, manque de spécialistes). Un déplacement vers la capitale ou La Réunion (un seul vol par semaine) est recommandé lorsque l’état de santé l’exige.</p>
<p><strong>A Tamatave</strong>, les soins sont très insuffisants : le seul hôpital (très vétuste et sans matériel) et les dispensaires manquent de tout. Seules les analyses de laboratoires très basiques peuvent être réalisées sur place. En cas d’accident ou de maladie, la meilleure option est de partir vers La Réunion (trois vols/semaine) ou à défaut à Tananarive (évacuation possible en hélicoptère).</p>
<p><strong>A Majunga</strong>, l’état sanitaire et les conditions d’hygiène sont très insuffisantes. Beaucoup de quartiers de la ville sont insalubres. Les soins qu’il est possible de recevoir sur place sont extrêmement limités, par l’absence de structures fiables et le manque de compétence des personnels médicaux.</p>
<h4 class="spip">Avant le départ</h4>
<p>Consultez votre médecin (éventuellement votre dentiste) et souscrivez à une compagnie d’assistance couvrant les frais médicaux et le rapatriement sanitaire. Les structures sanitaires malgaches ne répondant pas aux normes européennes, cette mesure est indispensable.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Dengue :</strong><br class="manualbr">Il n’y a actuellement aucun cas de dengue signalé à Madagascar mais une épidémie est toujours possible et la vigilance s’impose.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Paludisme (malaria) :</strong><br class="manualbr">Maladie parasitaire transmise par les piqûres de moustiques qui impose le recours à des mesures de protection individuelle (sprays, crèmes, diffuseurs électriques, moustiquaires…). A ces mesures doit s’ajouter un traitement médicamenteux adapté à chaque individu : il convient de s’adresser à votre médecin habituel ou à un centre de conseils aux voyageurs. Le traitement devra être poursuivi après le retour en France durant une durée variable selon le produit utilisé.
</p>
<p><strong>Classification</strong> : zone 2.</p>
<p>Durant votre séjour, et durant les deux mois qui suivent votre retour, en cas de fièvre, un avis médical doit être pris rapidement, pour mettre en œuvre dès que possible un traitement antipaludique éventuel.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Arboviroses :</strong><br class="manualbr">Comme dans d’autres pays de l’Océan indien, les moustiques peuvent être vecteurs de maladies virales comme la dengue ou le chikungunya. En l’absence de traitement préventif ou de vaccin disponibles, le recours à des mesures de protection individuelles (sprays, crèmes, diffuseurs électriques…) et l’attention des personnes les plus vulnérables s’imposent. Des informations plus complètes et actualisées sont consultables auprès des autorités sanitaires :
<br>— <a href="http://www.sante.gouv.fr/" class="spip_out" rel="external">Ministère des Affaires sociales</a>
<br>— <a href="http://www.invs.sante.fr/" class="spip_out" rel="external">Institut de veille sanitaire</a></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Sida :</strong><br class="manualbr">Prévalence non négligeable du VIH - sida. Les mesures de prévention habituelles sont indispensables.</p>
<p><strong>Vaccinations</strong></p>
<p>La mise à jour de la vaccination diphtérie-tétanos-poliomyélite est indispensable.</p>
<p>Autres vaccinations conseillées : fièvre typhoïde, hépatites virales A et B.</p>
<p>La vaccination contre la rage peut également être proposée dans certains cas.</p>
<p>Rage : risques de transmission par les chiens errants. En cas de griffure ou de morsure, consulter un médecin ou le dispensaire de l’Institut Pasteur à Tananarive. Il existe des centres de vaccination antirabiques à Madagascar : s’adresser sur place aux médecins inspecteurs.</p>
<p>Demandez conseil à votre médecin ou à un centre de vaccinations internationales.</p>
<p><strong>Hygiène alimentaire : (prévention de la diarrhée du voyageur et du choléra)</strong></p>
<p>Il est conseillé de ne pas boire l’eau du robinet : préférez les eaux en bouteilles capsulées. A défaut, consommez de l’eau filtrée, bouillie et décontaminée.</p>
<p>Evitez l’ingestion de glaçons, de jus de fruits frais, de légumes crus et de fruits non pelés.</p>
<p>Evitez la consommation d’aliments (poisson, viande, volaille, lait) insuffisamment cuits.</p>
<p>Veillez à un lavage régulier et soigneux des mains avant chaque repas.</p>
<p><strong>Quelques règles simples :</strong></p>
<ul class="spip">
<li>Maintenez une hygiène corporelle stricte (risque de mycoses),</li>
<li>Evitez la pratique du sport aux heures chaudes de la journée en été,</li>
<li>Evitez les baignades dans les eaux stagnantes (risque d’infection parasitaire),</li>
<li>Evitez de marcher pieds nus sur le sable et les sols humides,</li>
<li>Ne caressez pas les animaux que vous rencontrez,</li>
<li>Ne consommez jamais de médicaments achetés dans la rue,</li>
<li>Emportez dans vos bagages les médicaments dont vous pourriez avoir besoin.</li></ul>
<p><strong>Informations utiles</strong></p>
<p>Pas d’infrastructure opératoire fiable et bien équipée sur place. Seule possibilité : transfert sur un centre hospitalier spécialisé à La Réunion ou en France.</p>
<p><strong>Numéros d’urgence</strong></p>
<p>DIEGO SUAREZ :<br class="manualbr">Hôpital militaire : + 261 34 14586 41 / + 261 34 14586 42 / + 261 34 14586 45 (entrées) + 261 34 15586 51 (médecin chef)</p>
<p>TAMATAVE :<br class="manualbr">Hôpital : + 261 20533 20 18 / 53 320 21</p>
<p>TANANARIVE :<br class="manualbr">Hôpital Soavinandrimo (hôpital militaire) : + 261 20 23397 51</p>
<p><strong>Pour en savoir plus </strong> :</p>
<ul class="spip">
<li><a href="http://www.ambafrance-mada.org/Votre-sante-et-votre-couverture" class="spip_out" rel="external">Conseils sanitaires aux résidents ou expatriés français et européens à Madagascar</a> ;</li>
<li><a href="http://www.tana-accueil.com/" class="spip_out" rel="external">Tana Accueil</a>.</li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/vie-pratique/article/sante). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
