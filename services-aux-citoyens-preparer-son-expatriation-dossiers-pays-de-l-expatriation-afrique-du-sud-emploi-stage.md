# Emploi, stage

<h2 class="rub22969">Marché du travail</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/emploi-stage/#sommaire_1">Secteurs à fort potentiel</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/emploi-stage/#sommaire_2">Secteurs à faible potentiel</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/emploi-stage/#sommaire_3">Professions règlementées</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/emploi-stage/#sommaire_4">Rémunération</a></li></ul>
<p>L’Afrique du Sud est un pays à taux de chômage élevé (officiellement environ 25%, touchant principalement la population noire majoritaire).</p>
<p>Depuis 1994, les autorités sud-africaines ont mis en œuvre une politique d’<i>affirmative action</i> visant à promouvoir une meilleure représentation de la majorité noire dans les différents secteurs du pays (administration, services publics et parapublics, sociétés nationalisées et privées). Cette politique est appliquée à travers le programme <i>Black Economic Empowerment(<i>BEE</i>), transformé en 2003 en <i>Broad-Based Black Economic Empowerment</i> (<i>B-BBEE</i>).</i></p>
<p>Si l’on ne dispose pas de qualifications "pointues", les possibilités de trouver individuellement un emploi sont très limitées en raison de la politique de protection du marché de l’emploi local et de <i>l’affirmative action</i> (priorité à l’emploi donnée aux ressortissants sud-africains). Ainsi, les entreprises souhaitant employer un ressortissant étranger doivent apporter la preuve qu’elles n’ont pu trouver de candidat sud-africain.</p>
<h3 class="spip"><a id="sommaire_1"></a>Secteurs à fort potentiel</h3>
<ul class="spip">
<li>technologies de l’information</li>
<li>informatique</li>
<li>ingénierie</li>
<li>restauration</li>
<li>tourisme</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Secteurs à faible potentiel</h3>
<ul class="spip">
<li>industrie</li>
<li>agriculture</li>
<li>services</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Professions règlementées</h3>
<p>Secteurs non-accessibles à un étranger : l’agriculture.</p>
<h3 class="spip"><a id="sommaire_4"></a>Rémunération</h3>
<p>Les salaires sont très variables en fonction des emplois, des secteurs et des qualifications requises.</p>
<p>En raison de la fiscalité locale, les employeurs préfèrent limiter le montant des salaires et verser aux employés des primes non imposables (prime de transport, prime de logement, prise en charge de la scolarité des enfants, prise en charge de l’assurance maladie etc.).</p>
<p><i>Mise à jour : janvier 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-emploi-stage-article-stages.md" title="Stages">Stages</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-emploi-stage-article-lettre-de-motivation.md" title="Lettre de motivation">Lettre de motivation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-emploi-stage-article-curriculum-vitae-111009.md" title="Curriculum vitae">Curriculum vitae</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-emploi-stage-article-recherche-d-emploi-111008.md" title="Recherche d’emploi">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-emploi-stage-article-reglementation-du-travail-111007.md" title="Réglementation du travail">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-afrique-du-sud-emploi-stage-article-marche-du-travail-111006.md" title="Marché du travail">Marché du travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/emploi-stage/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
