# Lettre de motivation

<p><strong>Rédaction</strong></p>
<p><strong>Sa forme</strong></p>
<p>Au Danemark, la lettre de motivation ou "Cover letter" n’est <strong>JAMAIS manuscrite</strong>, la graphologie étant peu usitée. Utilisez le correcteur d’orthographe et faites-la corriger afin de vous assurer qu’elle ne contient pas d’erreur.</p>
<ul class="spip">
<li>Elle doit être <strong>rédigée en danois</strong> si possible ou en anglais et adressée au <i>Recruiting Manager</i> ou <i>Human Resource Manager</i>. Si vous disposez du nom de la personne, assurez-vous de bien l’orthographier et de vérifier son poste. Idéalement, vous devez contacter la personne en charge de votre recrutement, qui peut-être le directeur technique ou le directeur commercial. Si vous ne savez pas à qui l’adresser ou s’il s’agit d’une organisation de taille modeste, envoyez votre courrier directement au <i>President</i> (Directeur) ou <i>CEO</i> (PDG).</li>
<li>Le nom de la compagnie et l’adresse doivent être complets et corrects.</li>
<li>Il est d’usage de ne pas dépasser plus <strong>d’une à deux pages</strong>. Adoptez un style formel et soyez <strong>clair et concis</strong>. Votre lettre doit illustrer clairement votre motivation pour l’emploi et montrer comment votre expérience antérieure et votre engagement font de vous le meilleur candidat possible. Utilisez autant d’espace que nécessaire pour cette description.</li>
<li>Evitez que votre lettre soit monotone, soyez créatif afin d’éveiller l’attention du lecteur, employez des phrases telles que : " My responsibilities included … " (Mes domaines de responsabilité étaient…) et mentionnez les résultats de vos projets.</li>
<li>Signez manuellement vos lettres, en bleu ou noir.</li>
<li>Vous pouvez inclure un PS afin de <strong>mettre en valeur un point fort</strong> très illustratif. ex :" J’ai été le premier stagiaire à recevoir la récompense du meilleur employé du mois chez Ernst Young’s New York. Je l’ai reçu non pas une fois mais deux. Je compte apporter ce même niveau de travail personnel au sein de votre service ". Si vous voulez un maximum d’impact, écrivez ce PS à la main, ce sera la première chose lue sur votre lettre de motivation.</li>
<li>Assurez-vous que l’impression de vos documents est de bonne qualité.</li>
<li>Placez le CV derrière la lettre.</li>
<li>Tapez ou imprimez l’adresse sur l’enveloppe.</li>
<li>Si vous postez votre CV et votre lettre, assortissez votre enveloppe à votre papier et privilégiez les grandes enveloppes cartonnées pour protéger les documents.</li>
<li>Optez pour un timbre sobre.</li></ul>
<p><strong>Sa composition </strong></p>
<ul class="spip">
<li>Votre lettre sera composée idéalement de quatre paragraphes :</li>
<li>Une brève introduction qui explique qui vous êtes et pourquoi vous écrivez.</li>
<li>Un ou deux paragraphes pour souligner les points forts de votre cursus et / ou de vos expériences professionnelles qui sont en relation avec le poste que vous convoitez. Vous devez faire passer le message suivant : vous êtes le candidat idéal pour ce poste !</li>
<li>Un paragraphe de fermeture indiquant que vous restez à la disposition de l’employeur et le moyen par lequel il peut vous contacter.</li>
<li>Une formule finale de politesse.</li></ul>
<p><strong>Modèles de lettre de motivation</strong></p>
<p>Exemple : <a href="http://www.diplomatie.gouv.fr/fr/IMG/docx/Modeles_de_lettre_de_motivation_cle43dafc.docx" class="spip_in" type="application/vnd.openxmlformats-officedocument.wordprocessingml.document">candidature spontanée en anglais</a></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/emploi-stage/article/lettre-de-motivation). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
