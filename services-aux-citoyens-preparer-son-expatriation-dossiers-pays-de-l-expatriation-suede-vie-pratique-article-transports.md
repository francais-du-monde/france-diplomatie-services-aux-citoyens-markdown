# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/vie-pratique/article/transports#sommaire_1">Importation de véhicule</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/vie-pratique/article/transports#sommaire_2">Permis de conduire</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/vie-pratique/article/transports#sommaire_3">Code de la route</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/vie-pratique/article/transports#sommaire_4">Assurances et taxes</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/vie-pratique/article/transports#sommaire_5">Achat et location</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/vie-pratique/article/transports#sommaire_6">Entretien</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/vie-pratique/article/transports#sommaire_7">Réseau routier</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/vie-pratique/article/transports#sommaire_8">Transports en commun</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Importation de véhicule</h3>
<p>Si vous souhaitez importer un véhicule en Suède depuis un pays de l’UE, il convient de vous renseigner auprès de l’administration suédoise des routes (<i>Vägverket</i>) et de l’administration des Mines (<i>Bilprovningen</i>). Vous devez vous munir des certificats d’immatriculation et de conformité originaux. Une fois l’inspection réalisée avec succès, vous recevrez le certificat d’immatriculation et les plaques minéralogiques par la poste.</p>
<p>Informations et services concernant les véhicules, l’importation des véhicules, les règlements routiers ainsi que les péages de congestion et autres taxes et droits se trouvent sur le site de la <a href="http://www.transportstyrelsen.se/" class="spip_out" rel="external">Direction générale des transports</a> récemment constituée.</p>
<p><a href="http://www.korkortsportalen.se/" class="spip_out" rel="external">Informations et services</a> concernant les permis de conduire et les certificats et licences de conduite sont disponibles en ligne.</p>
<h3 class="spip"><a id="sommaire_2"></a>Permis de conduire</h3>
<p>Le permis de conduire français étant reconnu par les autres Etats membres de l’Union européenne, il n’est plus obligatoire de l’échanger contre un permis suédois. Il est néanmoins possible, pour des questions de commodité (expatriés en Suède sur une longue durée), d’échanger son permis français contre un permis suédois.</p>
<p>Pour en savoir plus :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.korkortsportalen.se/upload/dokument/Ovriga_dokument_TS/International/engelska.pdf" class="spip_out" rel="external">Document d’information</a> sur les permis de conduire étrangers et les échanges en Suède </p>
<h3 class="spip"><a id="sommaire_3"></a>Code de la route</h3>
<p>La conduite est à droite et les feux de croisement doivent être allumés de jour comme de nuit. Les pneus neige sont obligatoires du 1er décembre au 31 mars et conseillées du 1er octobre au 15 avril. Il faut veiller à respecter les places réservées aux invalides et la règle des dix mètres à laisser libres à chaque carrefour urbain. La vitesse maximale autorisée est de 50 km/h en agglomération, 90 km/h sur route et 110 km/h sur autoroute.</p>
<p>Les Suédois sont très respectueux du code de la route et très attentifs aux piétons et cyclistes.</p>
<p>L’alcoolisme au volant est puni de manière sévère. La limite légale <strong>est 0,10 mg</strong> d’alcool par litre d’air expiré, ce qui correspond à 0,2 mg par litre de sang.</p>
<p>Un taux d’alcoolémie compris entre 0,2 mg et 1 g (par litre de sang) est passible d’une peine de prison ferme pouvant aller jusqu’à six mois et d’une forte amende. Si le taux excède 1 g (qualification d’état d’ivresse grave) le juge peut prononcer une peine de deux années d’emprisonnement. Dans tous les cas, le permis de conduire est en outre suspendu.</p>
<p>Le stationnement est presque toujours payant et les sanctions en cas d’infraction sont élevées.</p>
<h3 class="spip"><a id="sommaire_4"></a>Assurances et taxes</h3>
<p>L’assurance "Trafikförsäkring", tiers limité, à une compagnie locale est obligatoire. Les prix sont équivalents aux prix français et le système bonus/malus existe.</p>
<p>Les principales compagnies d’assurance sont :</p>
<ul class="spip">
<li><a href="http://www.if.se/" class="spip_out" rel="external">IF.se</a></li>
<li><a href="http://www.folksam.se/" class="spip_out" rel="external">Folksam.se</a></li>
<li><a href="http://www.trygghansa.se/" class="spip_out" rel="external">Trygghansa.se</a></li></ul>
<h3 class="spip"><a id="sommaire_5"></a>Achat et location</h3>
<p>Les principales marques françaises : Peugeot, Citroën, Renault sont représentées ainsi que des marques étrangères (allemandes et japonaises en particulier).</p>
<p>Les normes suédoises sont très exigeantes, notamment en ce qui concerne l’équipement anti-pollution et certains équipements spécifiques aux pays nordiques (allumage obligatoire des phares, lave-phares, batterie et allumage renforcés …). Bien qu’aucun droit de douane ne soit exigé, il est plutôt recommandé d’acquérir une voiture (française) aux normes suédoises une fois arrivé dans le pays. La TVA à l’achat d’un véhicule est de 25 %.</p>
<p>Le coût d’un véhicule d’occasion est sensiblement plus cher qu’en France. Quelques exemples de prix sont mentionnés sur le <a href="http://www.bilweb.se/" class="spip_out" rel="external">site spécialisé</a>. Attention, le kilométrage est indiqué en <i>mil</i>, équivalent à 10 km.</p>
<p>Le marché pour les voitures de location est assez concurrentiel. On peut louer un véhicule dans les nombreuses agences de location ou dans les stations-service. Le prix varie entre 200 et 300 SEK par jour suivant le modèle.</p>
<h3 class="spip"><a id="sommaire_6"></a>Entretien</h3>
<p>Les coûts d’entretien d’un véhicule sont sensiblement plus élevés qu’en France mais le service fourni est de qualité. Il n’y a aucun problème d’approvisionnement en pièces détachées pour les modèles courants.</p>
<p>Un contrôle technique pour une voiture coûte environ 530 SEK et s’effectue aux trois ans du véhicule, puis à ses cinq ans, puis chaque année. Renseignement auprès de <a href="http://www.bilprovningen.se/" class="spip_out" rel="external">Svenska Bilprovningen</a> (équivalent suédois du contrôle des mines).</p>
<h3 class="spip"><a id="sommaire_7"></a>Réseau routier</h3>
<p>L’état des routes est très bon. Il existe assez peu d’autoroutes. En hiver, le réseau fait l’objet de fréquents dégagements par chasse-neige et le sablage est généralisé en agglomération. En revanche, les trottoirs restent souvent mal dégagés et glissants.</p>
<h3 class="spip"><a id="sommaire_8"></a>Transports en commun</h3>
<p><strong>Transports urbains</strong></p>
<p>Les transports urbains (métro et bus) sont pratiques.</p>
<p>On peut acheter un ticket à l’unité ou bien acheter une carte <strong>Access</strong> que l’on recharge, ce qui est plus économique. Avec la <i>Reskassa</i> sur la carte <strong>Access</strong>, un ticket une zone est à 25 SEK, un ticket deux zones à 37,50 SEK par exemple. Le ticket acheté à l’unité vaut 36 SEK pour une zone.</p>
<p>Les informations (en anglais) sur les <a href="http://www.sl.se/" class="spip_out" rel="external">transports locaux</a> sont disponibles en ligne.</p>
<p>Le prix des taxis est assez élevé (compter 250-300 SEK/10 km en journée du lundi au vendredi, (environ 9 et 18 sek par km selon le nombre de personnes occupant le taxi)) et très variable pour les courses à l’extérieur de la ville (trajet pour l’aéroport par exemple), le secteur étant déréglementé. Il est fortement recommandé de demander les tarifs ou le prix de la course avant de monter à bord. Il est possible de solliciter le paiement d’un forfait.</p>
<p>Pour se rendre aux aéroports de la ville de Stockholm, la plupart des compagnies de taxi proposent un tarif fixe entre Stockholm et les aéroports Arlanda, Bromma et Skavsta :</p>
<ul class="spip">
<li>Taxi Stockholm : 520 SEK</li>
<li>Airport Cab : 390/475 SEK</li>
<li>Green Cab : 390/520 SEK</li></ul>
<p>L’Arlanda Express est un train/navette rapide entre Stockholm et Arlanda. L’aller prend 20 minutes. Le billet simple adulte coûte 260 SEK, l’aller-retour adulte 490 SEK.</p>
<p>Des <a href="http://www.swebus.se/Flygtransfer/" class="spip_out" rel="external">liaisons régulières</a> sont assurées en bus vers les différents aéroports.</p>
<p>Il est agréable de se déplacer en vélo dans la ville, de nombreuses pistes cyclables sont présentes à Stockholm. Il existe plusieurs navettes de bateaux qui relient les îles de la ville entre elles (fonctionnement essentiellement durant l’été).</p>
<p><strong>Réseau ferroviaire </strong></p>
<p>Le réseau ferroviaire est étendu et moderne.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.sj.se/" class="spip_out" rel="external">Chemins de fer suédois</a> (version anglaise disponible).</p>
<p><strong>Transport aérien</strong></p>
<p>Dans tous les aéroports, voyageurs et bagages sont contrôlés avant l’embarquement, même s’il s’agit d’un vol intérieur. L’état d’entretien des flottes aériennes intérieures n’appelle pas de remarques particulières.</p>
<p>Informations sur les aéroports du pays : <a href="http://www.lfv.se/" class="spip_out" rel="external">Lfv.se</a> et <a href="http://www.swedavia.com/" class="spip_out" rel="external">Swedavia.com</a>.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/vie-pratique/article/transports). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
