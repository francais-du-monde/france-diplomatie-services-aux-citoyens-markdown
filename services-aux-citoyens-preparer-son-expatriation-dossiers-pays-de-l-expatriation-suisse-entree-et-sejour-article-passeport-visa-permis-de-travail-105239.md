# Passeport, visa, permis de travail

<p>Pour vous établir en Suisse, vous devez être en possession d’un passeport ou d’une carte nationale d’identité en cours de validité. Aucun visa n’est nécessaire.</p>
<p>La Suisse faisant désormais partie de l’espace Schengen, les contrôles aux frontières terrestres sont supprimés depuis le 12 décembre 2008.</p>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/entree-et-sejour/article/passeport-visa-permis-de-travail-105239#sommaire_1">Séjour de moins de 3 mois</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/entree-et-sejour/article/passeport-visa-permis-de-travail-105239#sommaire_2">Séjour de plus de 3 mois</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/entree-et-sejour/article/passeport-visa-permis-de-travail-105239#sommaire_3">Pour en savoir plus</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Séjour de moins de 3 mois</h3>
<p>Pour un séjour sans activité lucrative d’une durée maximale de 90 jours, une autorisation de séjour n’est pas nécessaire. En revanche, si vous souhaitez travailler, votre employeur devra vous déclarer à l’administration cantonale. Cette procédure, appelée " procédure d’annonce " est très réglementée. En sont exclus certains métiers et certaines activités sensibles.</p>
<h3 class="spip"><a id="sommaire_2"></a>Séjour de plus de 3 mois</h3>
<p>Depuis l’entrée en vigueur, le 1er juin 2002, des accords sectoriels entre la Suisse et l’Union européenne, l’accord sur la libre circulation des personnes définit le nouveau régime d’autorisation de séjour.</p>
<p>Cet accord facilite l’entrée, le séjour et l’exercice d’une activité lucrative en Suisse pour les ressortissants de l’Union européenne. Depuis juin 2007, toutes les contraintes légales qui entravaient le libre accès au marché du travail en Suisse pour les ressortissants de l’Union européenne ont disparu. Depuis cette date, les ressortissants des 15 pays " historiques " de l’Union européenne (dont la France), de Chypre, de Malte et des pays membres de l’Association européenne de libre-échange (AELE) ont les mêmes droits que les travailleurs locaux et les entrées ne sont plus limitées par des quotas de permis.</p>
<p>Les accords bilatéraux suivent un calendrier précis, initié en 2004, qui devrait, en principe, être définitivement adopté en juin 2014.</p>
<p>Depuis le 1er juin 2004, les formalités pour les permis de résident peuvent être accomplies par l’employé auprès des autorités compétentes. Selon les cantons, il peut s’agir de l’administration cantonale et/ou du service des étrangers de la commune de résidence. En général, les formulaires de demande de permis sont mis à disposition du futur employé par l’entreprise et le dossier est constitué ensemble.</p>
<p>Pour les travailleurs frontaliers, les démarches administratives se font en général auprès de l’administration cantonale. C’est en général l’employeur qui se charge d’effectuer ces démarches.</p>
<p>Il est obligatoire de détenir un permis de travail pour exercer une activité lucrative en Suisse.</p>
<h4 class="spip">Principaux types d’autorisations de séjour</h4>
<p><strong>Autorisation de courte durée (livret L) </strong> : sa validité est en fonction de la durée du contrat de travail et ne peut excéder 12 mois. Elle concerne les personnes disposant d’un contrat de travail d’une durée inférieure à un an. Elle est renouvelable.</p>
<p><strong>Autorisation de séjour (livret B) </strong> : elle est accordée sur présentation d’une déclaration d’engagement d’un employeur ou d’une attestation de travail de durée indéterminée ou d’au moins 12 mois. Elle s’adresse également aux personnes s’établissant à leur compte en Suisse ou s’y installant sans exercer d’activité lucrative, pourvu qu’elles disposent des moyens financiers suffisants. Sa validité est de 5 ans et peut être renouvelée, sous conditions, pour la même durée.</p>
<p><strong>Autorisation d’établissement (livret C) </strong> : elle concerne les personnes pouvant justifier d’un séjour régulier et ininterrompu en Suisse de 5 ans. La durée du séjour est illimitée.</p>
<p><strong>Autorisation frontalière (livret G) </strong> : elle concerne les travailleurs frontaliers. Sa validité est de cinq ans pour les personnes justifiant d’un contrat de travail d’une durée indéterminée ou supérieure à un an ou limitée à la durée du contrat de travail si la durée de celui-ci est inférieure à un an.</p>
<p>Le conjoint et sa famille bénéficient de la même autorisation de séjour que le travailleur étranger.</p>
<h4 class="spip">Etudiants</h4>
<p>Vous devez remplir les conditions suivantes :</p>
<ul class="spip">
<li>disposer d’une assurance maladie et de moyens financiers suffisants pour subvenir à vos besoins ;</li>
<li>la poursuite d’études doit être le but principal du séjour en Suisse ;</li>
<li>justifier d’une inscription auprès d’une université ou d’un établissement d’enseignement supérieur reconnu.</li></ul>
<p>L’autorisation est valable pour la durée de la formation ou, si les études se poursuivent sur une longue durée, pour une année, renouvelable d’année en année pour la même durée.</p>
<h4 class="spip">Personnes en recherche d’emploi</h4>
<p>Les demandeurs d’emploi peuvent séjourner pendant 3 mois sans autorisation de séjour, mais doivent se présenter au contrôle des habitants de la commune de résidence.</p>
<p>Si le séjour se prolonge au-delà de 3 mois, une autorisation de séjour de courte durée (livret L) est nécessaire.</p>
<h4 class="spip">Travailleurs indépendants</h4>
<p>Vous devez vous annoncer auprès de la commune de résidence et demander une autorisation de séjour pour indépendant. Vous devez justifier de l’exercice d’une activité indépendante vous permettant de subvenir à vos besoins. L’autorisation établie est valable 5 ans.</p>
<h4 class="spip">Retraités</h4>
<p>Vous devez, ainsi que les membres de votre famille, remplir les conditions suivantes :</p>
<ul class="spip">
<li>disposer de moyens financiers suffisants pour subvenir à vos besoins ;</li>
<li>disposer d’une assurance maladie et accidents couvrant tous les risques.</li></ul>
<p>La première autorisation est valable 5 ans, renouvelable, sous conditions, pour la même durée.</p>
<h3 class="spip"><a id="sommaire_3"></a>Pour en savoir plus</h3>
<p>Pour des informations détaillées et régulièrement mises à jour sur les conditions d’entrée et de séjour en Suisse, consulter les sites Internet suivants :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.bfm.admin.ch/bfm/fr/home.html" class="spip_out" rel="external">Office fédéral des migrations</a><br class="manualbr">Quellenweg 6 - 3003 Berne-Wabern<br class="manualbr">Téléphone : [41] 31 325 11 11 <br class="manualbr">Télécopie : [41] 31 325 93 79</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.travailler-en-suisse.ch" class="spip_out" rel="external">Vivre et travailler en Suisse</a></p>
<p><i>Mise à jour : février 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/entree-et-sejour/article/passeport-visa-permis-de-travail-105239). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
