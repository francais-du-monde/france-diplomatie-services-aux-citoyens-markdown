# Fiscalité

<h2 class="rub22951">Fiscalité du pays</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/fiscalite/#sommaire_1">Modalités de paiement de l’impôt</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/fiscalite/#sommaire_2">Année fiscale </a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/fiscalite/#sommaire_3">Barème de l’impôt </a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/fiscalite/#sommaire_4">Coordonnées des centres d’information fiscale </a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/fiscalite/#sommaire_5">Impôts directs et indirects</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/fiscalite/#sommaire_6">Impôts locaux</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/fiscalite/#sommaire_7">Droits et taxes sur le commerce international</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Modalités de paiement de l’impôt</h3>
<p>Tout contribuable doit déposer une déclaration de revenus auprès de la Commission de l’impôt sur le revenu au plus tard, le troisième mois suivant la fin de l’année fiscale correspondante, soit pour une déclaration au titre de l’année N, au 31 mars N+1.</p>
<p>Le montant de l’impôt calculé par le contribuable devient exigible et doit être réglé au moment du dépôt de la déclaration ; la régularisation se fait dans les 30 jours après réception de l’avis d’imposition.</p>
<p>Au titre des salaires, une retenue à la source est prélevée tous les mois par l’employeur (système <i>PAYE "Pay As You Earn</i>) tant pour les résidents que les non-résidents.</p>
<h3 class="spip"><a id="sommaire_2"></a>Année fiscale </h3>
<p>L’année fiscale correspond à l’année civile et va du 1er janvier au 31 décembre.</p>
<h3 class="spip"><a id="sommaire_3"></a>Barème de l’impôt </h3>
<h4 class="spip">Barème de l’impôt sur le revenu</h4>
<p>Selon le montant du revenu de l’intéressé, le barème de l’impôt sur le revenu des personnes physiques varie de 2% à 30% ; le taux d’imposition des personnes morales est fixé à 32,5%.</p>
<h3 class="spip"><a id="sommaire_4"></a>Coordonnées des centres d’information fiscale </h3>
<p><strong>Internal Revenue Service </strong> <br class="manualbr">P.O. Box 2202 - Accra<br class="manualbr">Téléphone : [233] 21 67 57 01 / 10 - Télécopie : [233] 21 66 49 38<br class="manualbr">Courriel : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/fiscalite/#irstaxgh#mc#hotmail.com#" title="irstaxgh..åt..hotmail.com" onclick="location.href=mc_lancerlien('irstaxgh','hotmail.com'); return false;" class="spip_mail">irstaxgh<span class="spancrypt"> [at] </span>hotmail.com</a> et <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/fiscalite/#irstaxgh#mc#yahoo.com#" title="irstaxgh..åt..yahoo.com" onclick="location.href=mc_lancerlien('irstaxgh','yahoo.com'); return false;" class="spip_mail">irstaxgh<span class="spancrypt"> [at] </span>yahoo.com</a></p>
<p><strong>The Chief Executive</strong><br class="manualbr">Ghana Investment Promotion Centre<br class="manualbr">POBox M 193 <br class="manualbr">Accra<br class="manualbr">Tél : (233-21) 66 51 25 / 9<br class="manualbr">Fax : (233 21) 66 38 01<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/fiscalite/#gipc#mc#Ghana.com#" title="gipc..åt..Ghana.com" onclick="location.href=mc_lancerlien('gipc','Ghana.com'); return false;" class="spip_mail">Courriel</a></p>
<p><strong>The Secretary</strong><br class="manualbr">Bank of Ghana<br class="manualbr">POBox 2674 <br class="manualbr">Accra<br class="manualbr">Tél : (233 21) 66 69 02 / 8</p>
<p><strong>The Registrar-General</strong><br class="manualbr">Registrar-General’s Department<br class="manualbr">POBox 118 <br class="manualbr">Accra<br class="manualbr">Tél. : (233 21) 66 20 43 / 66 46 91</p>
<h3 class="spip"><a id="sommaire_5"></a>Impôts directs et indirects</h3>
<p>Toutes les entreprises doivent s’inscrire auprès du Ghana Revenue Authority pour le paiement de taxe statutaire.</p>
<p>Les différents types des impôts au Ghana sont : la Corporate Tax, Withholding Tax, Capital Gains Tax, Value Added Tax/NHIL, Employment Tax,et Dividend Tax.</p>
<h3 class="spip"><a id="sommaire_6"></a>Impôts locaux</h3>
<p><strong>Taux d’imposition unique sur les revenus des individuels non-résidents : 15%</strong></p>
<h3 class="spip"><a id="sommaire_7"></a>Droits et taxes sur le commerce international</h3>
<p>Taux d’imposition sur les revenus des entreprises : 25% en général.</p>
<p><strong>Exceptions :</strong></p>
<ul class="spip">
<li>Produits non traditionnels (horticulture, agro-industrie, matières premières, produits artisanaux…) : 8%</li>
<li>Compagnies listées sur le marché boursier ghanéen, pour les 3 premières années : 22%</li>
<li>Revenu issu d’un prêt d’une entreprise de leasing : 20%</li>
<li>Entreprise local dont les revenus proviennent de l’agro-industrie ou de l’économie de cacao, située à Accra et Tema : 20%</li>
<li>Située dans les autres villes régionales à l’exception du nord, du nord-est et nord-ouest : 10%</li>
<li>Reste du pays : 0%</li>
<li>Entreprise dont les revenus sont issus d’une compagnie de fabrication : Accra et Tema : 25%</li>
<li>Autres capitales régionales : 18,75%</li>
<li>Reste du pays : 12,50%</li></ul>
<p><strong>Pour en savoir plus :</strong></p>
<p><a href="http://www.gra.gov.gh/" class="spip_out" rel="external">Ghana Revenue Authority</a><br class="manualbr">Off Starlets’91 Road<br class="manualbr">Near Accra Sport Stadium<br class="manualbr">Postal : P. O. Box 2202, <br class="manualbr">Accra <br class="manualbr">Ghana <br class="manualbr">Tél. : +233-(0)302-675701-10 / +233-(0)302-686106 / +233-(0)302-684363 / +233-(0)302-681163</p>
<p><i>Mise à jour : janvier 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-ghana-fiscalite-article-convention-fiscale-110992.md" title="Convention fiscale">Convention fiscale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-ghana-fiscalite-article-fiscalite-du-pays.md" title="Fiscalité du pays">Fiscalité du pays</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/fiscalite/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
