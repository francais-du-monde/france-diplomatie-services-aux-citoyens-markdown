# Loisirs et culture

<p><strong>Tourisme</strong></p>
<p>Le public peut obtenir des informations de l’Office du tourisme suédois en composant le 01 70 70 84 58 ou en écrivant à france<span class="spancrypt"> [at] </span>visitsweden.com. De nombreuses informations et brochures sont disponibles via le site <a href="http://www.visitsweden.com/suede/" class="spip_out" rel="external">Visitsweden.com</a>.</p>
<p><strong>Sites Internet touristiques : </strong></p>
<ul class="spip">
<li><a href="http://www.visitsweden.com/suede/" class="spip_out" rel="external">Le site officiel du tourisme et du voyage en Suède</a> (version française disponible) ;</li>
<li><a href="http://www.ticnet.se/" class="spip_out" rel="external">Ticnet.se</a> (achat de billets en ligne pour des événements culturels - version anglaise disponible).</li></ul>
<p><i>Mise à jour : juin 2014 </i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/vie-pratique/article/loisirs-et-culture-113537). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
