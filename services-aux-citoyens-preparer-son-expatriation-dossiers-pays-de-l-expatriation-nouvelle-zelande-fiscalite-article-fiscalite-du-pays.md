# Fiscalité du pays

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/fiscalite/article/fiscalite-du-pays#sommaire_1">L’impôt sur le revenu des particuliers </a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/fiscalite/article/fiscalite-du-pays#sommaire_2">L’impôt sur les sociétés</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/fiscalite/article/fiscalite-du-pays#sommaire_3">L’impôt sur les revenus du capital</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/fiscalite/article/fiscalite-du-pays#sommaire_4">Année fiscale</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/fiscalite/article/fiscalite-du-pays#sommaire_5">Barème de l’impôt</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/fiscalite/article/fiscalite-du-pays#sommaire_6">Solde du compte en fin de séjour</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/fiscalite/article/fiscalite-du-pays#sommaire_7">Coordonnées des centres d’information fiscale</a></li></ul>
<p>Réformé en profondeur dans les années 1980, le système fiscal néo-zélandais est aujourd’hui considéré comme l’un des plus simples et des plus efficaces de l’OCDE.</p>
<h3 class="spip"><a id="sommaire_1"></a>L’impôt sur le revenu des particuliers </h3>
<p>(salariés et personnes établies à leur compte)</p>
<h4 class="spip">Principes de fonctionnement</h4>
<p>Au regard de la loi fiscale, l’impôt sur le revenu s’applique :</p>
<ul class="spip">
<li>aux personnes résidentes en Nouvelle-Zélande, sur l’ensemble de leurs revenus ;</li>
<li>aux personnes non-résidentes en Nouvelle-Zélande, sur leurs revenus tirés d’une activité exercée en Nouvelle-Zélande.</li></ul>
<p>Tout individu possédant un lieu de résidence permanent en Nouvelle-Zélande ou séjournant au moins 182 jours à l’année en Nouvelle-Zélande est considéré comme résident néo-zélandais. Celui-ci demeure résident néo-zélandais au regard de la loi d’imposition mais son statut de contribuable résident cessera s’il est absent de Nouvelle-Zélande pendant plus de 325 jours sur une période de 12 mois consécutifs.</p>
<p>La déclaration d’impôts est individuelle, la notion de foyer fiscal n’existe pas. La fiscalité néo-zélandaise distingue les revenus du travail et du capital :</p>
<ul class="spip">
<li>les impôts sur les revenus du travail (salaires et traitements pour les personnes physiques et bénéfices tirés de l’activité productive pour les entreprises) qui sont soumis à l’<strong>Income tax</strong>.</li>
<li>les impôts sur les revenus des capitaux (intérêts, dividendes et royalties) qui sont soumis à la <strong>Withholding Tax</strong>. Celle-ci est divisée en différentes catégories selon le type de capitaux taxés ainsi que l’origine du receveur et de l’émetteur de ces capitaux.</li></ul>
<h4 class="spip">Règles d’imposition et déductions fiscales</h4>
<p>Les salariés sont imposables sur l’ensemble des revenus (salaires, primes, allocations, indemnités de toute sorte…). Les frais relevant de l’activité professionnelle ne sont pas déductibles. L’impôt des salariés et des personnes établies à leur compte est <strong>retenu à la source</strong>, avant établissement définitif de la déclaration de revenus. C’est le système <i>pay as you earn</i> ou PAYE. Les employeurs doivent établir le montant du revenu imposable des employés et payer l’impôt correspondant à l’administration. Le revenu imposable regroupe l’ensemble des revenus perçus au cours de l’année, diminué des déductions autorisées par la loi fiscale.</p>
<p>A la différence de la France, il n’y a ni cotisations sociales patronales ni cotisations salariales obligatoires. Certaines dépenses, notamment pour les soins médicaux et les retraites, sont prises partiellement et directement en charge par les ménages. Environ 1,7% des revenus des particuliers est versé aux fonds de <i>l’Accident Compensation Corporated</i> qui couvre les accidents corporels.</p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Taux d’imposition (avant avril 2009)</strong></td>
<td>12,5%</td>
<td>21%</td>
<td>33%</td>
<td>39%</td></tr>
<tr class="row_even even">
<td>Plafonds ((NZ$)</td>
<td>14 000</td>
<td>14 001 à 40 000</td>
<td>40 001 à 70 000</td>
<td>70 001</td></tr>
<tr class="row_odd odd">
<td><strong>Nouveaux taux (à partir d’avril 2009)</strong></td>
<td>12,5%</td>
<td>21%</td>
<td>33%</td>
<td>38%</td></tr>
<tr class="row_even even">
<td>Plafonds avril 2009</td>
<td>14 000</td>
<td>14 001 à 48 000</td>
<td>48 001 à 70 000</td>
<td>70 001</td></tr>
<tr class="row_odd odd">
<td><strong>Nouveaux taux (à partir d’avril 2010)</strong></td>
<td>12,5%</td>
<td>21%</td>
<td>33%</td>
<td>37%</td></tr>
<tr class="row_even even">
<td>Plafonds avril 2010</td>
<td>14 000</td>
<td>14 001 à 50 000</td>
<td>50 001 à 70 000</td>
<td>70 001</td></tr>
<tr class="row_odd odd">
<td><strong>Nouveaux taux (à partir d’avril 2011)</strong></td>
<td>12,5%</td>
<td>20%</td>
<td>33%</td>
<td>37%</td></tr>
<tr class="row_even even">
<td>Plafonds avril 2011</td>
<td>14 000</td>
<td>14 001 à 50 000</td>
<td>50 001 à 70 000</td>
<td>70 001</td></tr>
<tr class="row_odd odd">
<td><strong>Nouveaux taux (à partir d’avril 2012)</strong></td>
<td>10,5%</td>
<td>17,5%</td>
<td>30%</td>
<td>33%</td></tr>
<tr class="row_even even">
<td>Plafonds avril 2012</td>
<td>14 000</td>
<td>14 001 à 48 000</td>
<td>48 001 à 70 000</td>
<td>70 001</td></tr>
</tbody>
</table>
<p>L’impôt reste par ailleurs progressif : pour un salaire annuel égal à 15 000 NZD, un individu est soumis à un taux d’imposition égal à 10,5% sur les 14 000 premiers dollars puis 17,5% sur les 1 000 dollars restant. Le taux d’imposition maximal est l’un des plus bas au monde.</p>
<p>Des <strong>déductions fiscales</strong> ont cours pour :</p>
<ul class="spip">
<li>Les donations à des œuvres caritatives</li>
<li>Les travailleurs indépendants</li></ul>
<h4 class="spip">Imposition des non-résidents</h4>
<p>Outre l’impôt sur le revenu, assis sur les revenus d’activités exercées en Nouvelle-Zélande, que paye aussi le résident néo-zélandais, <strong>les personnes morales et physiques non-résidentes</strong> en Nouvelle-Zélande sont soumises à la <i>Non-Resident Withholding Tax (NRWT</i>).</p>
<p>La NRWT s’applique aux dividendes, intérêts et redevances. Son taux est de 15% pour les intérêts et redevances et de 30% pour les dividendes. Dans le cas d’accord de non-double imposition (c’est le cas avec la France) les taux s’élèvent à 10% pour les intérêts et redevances et à 15% maximum sur les dividendes.</p>
<p>Les actionnaires fiscalement non-résidents ne sont admis qu’à une seule déduction : sur le montant de la retenue à la source sur le dividende (« <i>Non-Resident Withholding Tax »</i>), une fois imputés les « crédits relatifs au paiement de l’impôt sur la distribution de dividendes », appelés « <i>Dividend Withholding Payment Credits </i> » ou « <i>DWP Credits </i> ».</p>
<h4 class="spip">Absence de taxe sur les plus-values</h4>
<p>Il n’existe, en dépit des recommandations de l’OCDE, pas de taxes sur les plus-values mobilières et immobilières. Toutefois, les dispositions de l’<i>Income Tax Act</i> <i>1976 </i>s’appliquent à certains profits réalisés lors de la cession de terres. Par exemple, le bénéfice réalisé sera taxé si :</p>
<ul class="spip">
<li>le terrain est acheté à des fins spéculatives</li>
<li>le terrain est acheté par un « professionnel » (promoteur immobilier, professionnel du bâtiment…) ou une personne qui lui est associée et la revente intervient sous moins de 10 ans.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>L’impôt sur les sociétés</h3>
<h4 class="spip">Principes de fonctionnement</h4>
<p>Une société est considérée comme établie en Nouvelle-Zélande si :</p>
<ul class="spip">
<li>elle s’est constituée en société commerciale en Nouvelle-Zélande ;</li>
<li>sa principale filiale est en Nouvelle-Zélande ;</li>
<li>son centre de gestion est en Nouvelle-Zélande ;</li>
<li>le contrôle de la société par ses dirigeants se fait depuis la Nouvelle-Zélande.</li></ul>
<p><strong>Le taux d’imposition sur les bénéfices de sociétés (IS) est de 28% depuis avril 2011</strong>. Dans le cadre du Plan de relance, le gouvernement cherche à accompagner, par la fiscalité, la reprise du secteur privé : le passage de 30% à 28% rend le taux d’imposition en Nouvelle-Zélande plus bas que le taux d’imposition en Australie. En 2009, ce même taux d’imposition était passé de 33% à 30%.</p>
<p>Les sociétés enregistrées en Nouvelle-Zélande sont imposables sur l’ensemble de leurs revenus, qu’ils soient d’origine néo-zélandaise ou étrangère. L’impôt est acquitté <strong>par tiers provisionnels</strong>, calculés sur une estimation du revenu annuel. Les nouvelles entreprises doivent souvent payer l’impôt de leur première année au moment où les tiers provisionnel sont mis en place pour la deuxième année : il est recommandé de prévoir son budget en conséquence. Il est par ailleurs nécessaire de s’enregistrer auprès d’<i>Inland Revenue</i> pour obtenir un identifiant (<i>IRD number)</i>.</p>
<p>A l’inverse, <strong>une société étrangère ne paie d’impôt que sur ses revenus d’origine néo-zélandaise, au même taux que les entreprises néo-zélandaises, soit</strong> <strong>28%</strong> à partir d’avril 2011 (contre 30% pour les exercices précédents).</p>
<p>Pour les entreprises unipersonnelles, la personne en question n’est pas distincte de son entreprise : l’entreprise et la personne sont donc imposées en tant qu’individu (voir « <a href="http://www.business.govt.nz/tax-and-reporting/business-tax-levies/getting-started-with-tax/overview-of-taxes-and-levies" class="spip_out" rel="external">l’impôt sur le revenu des particuliers</a> »). Comme en France, les pertes peuvent être reportées d’une année sur l’autre. De plus, l’imputation sur les bénéfices à venir n’est pas limitée dans le temps.</p>
<p>En matière d’amortissement, l’entreprise peut choisir entre un mode linéaire ou dégressif. Un élément d’actif amortissable est défini de façon large comme un élément d’actif dont la durée de vie est limitée, supérieure à un an, et dont on peut raisonnablement s’attendre à ce qu’il perde de la valeur dans le temps. Certains actifs sont exclus de cette définition, comme les stocks commerciaux ou la plupart des terrains. L’entreprise peut choisir de ne pas amortir certains actifs.</p>
<h4 class="spip">Taxation sur les avantages en nature</h4>
<p>Tout employeur est assujetti à la <i>Fringe Benefits Tax</i>. Cet impôt est assis sur l’estimation des avantages en nature accordés à ses employés (voiture de fonction, téléphone portable, mutuelle complémentaire d’assurance-maladie, etc.) et à certains de ses actionnaires.</p>
<p>L’employeur a le choix entre différentes méthodes de calcul, plus ou moins avantageuses selon le salaire annuel de chaque employé :</p>
<ul class="spip">
<li>un taux forfaitaire de 49,25% sur l’ensemble des avantages en nature accordés (solution avantageuse lorsque l’employé qui bénéficie d’avantages en nature gagne plus de 70 000 NZD/an) ;</li>
<li>un taux forfaitaire de 49,25% (pour les actionnaires et associés) ou de 42,86% (pour les autres) sur les bénéfices non-attribués et un taux forfaitaire de 49,25% sur les bénéfices alloués ;</li>
<li>des taux ajustés sur les taux d’imposition marginaux des employés.</li></ul>
<h4 class="spip">Déductions fiscales</h4>
<p>Sont déductibles du revenu imposable des sociétés :</p>
<ul class="spip">
<li>Les frais de déplacement (sous certaines conditions) ;</li>
<li>Les frais d’utilisation du domicile pour les besoins de l’entreprise ;</li>
<li>Le montant de la <i>Fringe Benefits Tax</i> ;</li>
<li>Les dépenses en équipement, les dépenses en loisirs et les dépenses réalisées en faveur de la protection de l’environnement ;</li>
<li>Les donations à des œuvres caritatives. Depuis 2009, la déduction maximale possible est uniquement limitée au montant du revenu net de l’entreprise ;</li>
<li>De plus, une déduction fiscale liée aux frais de recherche et développement entre en vigueur à partir de l’exercice fiscal 2008-2009 : le taux s’élève à 15%. Les critères auxquels répondent les investissements « éligibles » sont détaillés sur le site d’<i>Inland Revenue</i>.</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>L’impôt sur les revenus du capital</h3>
<h4 class="spip">Les dividendes</h4>
<p><strong>Les dividendes</strong> versés par <strong>un résident néo-zélandaise à un autre résident</strong> sont soumis au régime général d’imposition : la <i>Resident Withholding Tax (RWT). </i>Le taux d’imposition sur les revenus de dividendes s’élève à 28% du dividende brut au maximum (33% si le résident n’a pas de <i>IRD number</i>). Le prélèvement s’effectue à la source.</p>
<p>Le système de crédit d’impôt permet aux actionnaires privés résidents (et à eux seuls) de déduire de leur propre impôt sur les dividendes une partie du montant de l’impôt sur les dividendes payé par les sociétés dont ils sont actionnaires. Ce crédit d’impôt ne peut toutefois être supérieur au montant de leur contribution.</p>
<p>S’agissant des <strong>investissements à l’étranger des résidents néo-zélandais</strong>, la RWT a été réformée en avril 2007. Si la somme des dividendes et des plus-values est inférieure à 5% de l’investissement, alors les revenus des différents portefeuilles ne sont pas taxés. Quand cette somme est supérieure à 5%, <a href="https://www.ird.govt.nz/toii/fif/calc-methods/calc-method-choose/" class="spip_out" rel="external">sept méthodes de calcul différentes existent, selon votre situation</a>.</p>
<p><strong>Les dividendes reçus de sociétés étrangères</strong> sont soumis à la <i>Foreign Dividend Withholding Payment</i> (<i>FDWP</i>). Cette taxe est assise sur le dividende brut, et tient compte des prélèvements à la source ainsi que des crédits d’impôt en vigueur à l’étranger.</p>
<p><strong>Les dividendes payés par un résident à un non-résident</strong> sont soumis à la <i>Non resident withholding tax (NRWT)</i> - voir ci-dessous.</p>
<p>Les sociétés étrangères établies en Nouvelle-Zélande sont exonérées de l’impôt néo-zélandais si elles l’ont payé (le même) dans leur pays d’origine ou si elles peuvent se prévaloir d’un accord de non-double imposition. Toutefois cette exonération est plafonnée au montant de l’impôt néo-zélandais qu’aurait payé l’entreprise sur les recettes fiscalisées dans le pays étranger.</p>
<h4 class="spip">Les intérêts</h4>
<p>L’impôt sur les intérêts perçus par les sociétés résidentes est prélevé à la source, à un taux de 28% (33% si l’entreprise n’a pas d’identifiant IRD, voir <i>supra</i>).</p>
<p>Pour les autres entités, personnes physiques et morales (commerçants indépendants, organisations à but non lucratif, partenariats, trust), les taux ci-contre s’appliquent.</p>
<p>Les intérêts payés à un non résident sont soumis à la <i>Non resident withholding tax (NRWT)</i> – cf. infra.</p>
<p>Les taux d’imposition appliqués aux intérêts reçus peuvent être trouvés <a href="http://www.ird.govt.nz/rwt/receiving/how-much/" class="spip_out" rel="external">ici</a>.</p>
<h4 class="spip">Taxes sur la valeur ajoutée et autres taxes</h4>
<p><strong>La GST (Good and Services Tax)</strong></p>
<p>La <i>GST</i> est une taxe sur la valeur ajoutée qui s’applique sur tous les biens et services fournis en Nouvelle-Zélande. Cette taxe est de 15%. Afin que la <i>GST</i> ne soit collectée qu’une seule fois sur le bien ou service, il existe un système de déductibilité pour les entreprises. La <i>GST</i> frappe également les biens et services en provenance de l’étranger. Collectée alors par les Douanes, elle porte (à hauteur de 15%) sur la valeur en douane : valeur des marchandises augmentée du coût du transport, des assurances et des droits de douane. A cela peuvent s’ajouter un prélèvement de $25,30 (tarif d’entrée sur le territoire) + $12.77 (coûts de vérification de conformité des produits avec les mesures de biosécurité en vigueur en Nouvelle-Zélande).</p>
<p>Un certain nombre de transactions en sont exonérées : certaines ventes de marchandises à l’exportation (notamment par internet), cession de fonds de commerce en activité, services financiers, baux résidentiels…</p>
<p>Les entreprises ayant un chiffre d’affaires supérieur à 60.000 NZ$ sont automatiquement assujetties à la GST et doivent s’enregistrer auprès de l’<i>Inland Revenue Department</i>. Celles ayant un CA inférieur à 60.000 NZ$ pourront bénéficier de la déductibilité de la GST en se faisant enregistrer de manière volontaire auprès du <i>Commissionner of Inland Revenue</i>. Une <a href="http://www.ird.govt.nz/help/demo/intro-bus-vids/#05" class="spip_out" rel="external">vidéo</a> mise en ligne par l’<i>Inland Revenue Department</i> donne toutes les informations nécessaires concernant la GST d’une manière simple et concise. Pour davantage de détails, <a href="http://www.ird.govt.nz/resources/f/2/f24f29004ba3d52fb085bd9ef8e4b077/ir375.pdf?bcsi_scan_1fe59ba8c561fa18=1" class="spip_out" rel="external">le formulaire IR 375</a> est aussi librement consultable.</p>
<h4 class="spip">Autres taxes indirectes</h4>
<p><strong>Les droits de douane</strong> (<i>Customs duties</i>) : dus à l’entrée des biens en Nouvelle-Zélande, ils varient selon les produits. Les taux s’étalent généralement de 3% à 10%. Le site des douanes néo-zélandaises précise les formalités à accomplir ainsi que les droits exigibles.</p>
<p><strong>Les droits d’accise</strong> (<i>Excise duty</i>) : frappent l’alcool, le tabac et leurs dérivés ainsi que certains produits pétroliers (carburant, gaz naturel et GPL) avant application de la GST. Le montant de ces droits est variable et est calculé par référence au poids, à l’efficacité énergétique et à la quantité du produit.</p>
<p><strong>Taxe de propriété et taxe d’habitation</strong> : Les taux dépendent des municipalités où sont situées les propriétés et de différents critères (valeur du capital, valeur du terrain, etc.). Pour une maison familiale type, on estime le montant de la taxe entre 1000 et 2000 NZ$.</p>
<p><strong>Droits sur les donations</strong> (gift duty) : avant le mois d’octobre 2011, une taxe s’appliquait sur toute donation excédant 27 000 NZD. Cet impôt n’existe plus.</p>
<p><i>Sources : fiche "la fiscalité en Nouvelle-Zélande" de la Mission économique française en NZ (mai 2009), site de <a href="http://www.ird.govt.nz/resources/f/2/f24f29004ba3d52fb085bd9ef8e4b077/ir375.pdf?bcsi_scan_1fe59ba8c561fa18=1" class="spip_out" rel="external">l’Inland Revenue Department</a> (consulté le 10/12/2013).</i></p>
<h3 class="spip"><a id="sommaire_4"></a>Année fiscale</h3>
<p>L’année fiscale court du 1er avril au 31 mars.</p>
<p>Une dérogation peut être demandée pour adopter un autre calendrier.</p>
<h3 class="spip"><a id="sommaire_5"></a>Barème de l’impôt</h3>
<p>Les salariés sont imposables sur l’ensemble des revenus (salaires, primes, allocations, indemnités de toute sorte…). Les frais relevant de l’activité professionnelle ne sont pas déductibles. <a href="http://www.ird.govt.nz/" class="spip_out" rel="external">L’impôt des salariés et des personnes établies à leur compte</a> est <strong>retenu à la source</strong>, avant établissement définitif de la déclaration de revenus. C’est le système <i>pay as you earn</i> ou PAYE.</p>
<table class="spip">
<thead><tr class="row_first"><th id="id9f73_c0">Tranche de revenu imposable</th><th id="id9f73_c1">Taux marginal d’imposition</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="id9f73_c0">Jusqu’à 14 000 NZ$</td>
<td headers="id9f73_c1">12,20%</td></tr>
<tr class="row_even even">
<td headers="id9f73_c0">De 14 001 NZ$ à 48 000 NZ$</td>
<td headers="id9f73_c1">19,20%</td></tr>
<tr class="row_odd odd">
<td headers="id9f73_c0">De 48 001 NZ$ à 70 000 NZ$</td>
<td headers="id9f73_c1">31,70%</td></tr>
<tr class="row_even even">
<td headers="id9f73_c0">Au-delà de 70 001 NZ$</td>
<td headers="id9f73_c1">34,70%</td></tr>
<tr class="row_odd odd">
<td headers="id9f73_c0">Déclaration non remplie</td>
<td headers="id9f73_c1">46,70%</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_6"></a>Solde du compte en fin de séjour</h3>
<p>Il est possible de <a href="http://www.ird.govt.nz/yoursituation-ind/travelling/travellingtoandfromnz-leavinglongterm.html" class="spip_out" rel="external">solder son compte avant un départ définitif</a>, même en cours d’année fiscale, en remplissant le formulaire IR3.</p>
<h3 class="spip"><a id="sommaire_7"></a>Coordonnées des centres d’information fiscale</h3>
<ul class="spip">
<li><a href="http://www.ird.govt.nz/contact-us/postal/" class="spip_out" rel="external">Adresses postales et physiques des différents centres</a>.</li>
<li><a href="http://www.ird.govt.nz/contact-us/phone/" class="spip_out" rel="external">Numéros de téléphone des différents services.</a></li>
<li><a href="http://www.ird.govt.nz/contact-us/online/" class="spip_out" rel="external">Adresses électroniques</a>.</li></ul>
<p><strong>Informations complémentaires</strong></p>
<p><a href="http://www.ird.govt.nz/" class="spip_out" rel="external">Inland Revenue</a>, organisme chargé de la collecte et de la redistribution de l’impôt en Nouvelle-Zélande, fournit des informations adaptées, guides et formulaires en ligne.</p>
<p>Voir notamment le <a href="http://www.ird.govt.nz/resources/5/a/5a8c22004ba3d5a5b38dbf9ef8e4b077/ir294.pdf" class="spip_out" rel="external">guide complet destiné aux visiteurs</a>.</p>
<p>Le site du <a href="http://www.immigration.govt.nz/" class="spip_out" rel="external"><strong>service de l’immigration</strong> en Nouvelle-Zélande (<i>Immigration NZ</i>)</a> résume par mots-clefs les questions relatives à l’impôt :</p>
<p>Voir également le <strong>site des douanes et du Trésor</strong> :</p>
<ul class="spip">
<li><a href="http://www.customs.govt.nz/" class="spip_out" rel="external">Customs.govt.nz</a></li>
<li><a href="http://www.treasury.govt.nz/" class="spip_out" rel="external">Treasury.govt.nz</a></li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/fiscalite/article/fiscalite-du-pays). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
