# Règlementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/emploi-stage/article/reglementation-du-travail#sommaire_1">Période d’essai et préavis</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/emploi-stage/article/reglementation-du-travail#sommaire_2">Durée du travail</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/emploi-stage/article/reglementation-du-travail#sommaire_3">Coût des heures supplémentaires</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/emploi-stage/article/reglementation-du-travail#sommaire_4">Jours de repos hebdomadaires</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/emploi-stage/article/reglementation-du-travail#sommaire_5">Congés annuels</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/emploi-stage/article/reglementation-du-travail#sommaire_6">Congés maladie</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/emploi-stage/article/reglementation-du-travail#sommaire_7">Congé de maternité</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/emploi-stage/article/reglementation-du-travail#sommaire_8">Fêtes légales</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Période d’essai et préavis</h3>
<p>La période d’essai est de deux mois maximum. En cas de rupture de contrat la période de préavis est de 30 jours.</p>
<h3 class="spip"><a id="sommaire_2"></a>Durée du travail</h3>
<p>Le temps de travail hebdomadaire est fixé par la loi à <strong>48 heures</strong> réparties sur une période maximum de six jours par semaine. Le ministère de la protection sociale accorde cependant des autorisations de travailler jusqu’à 12 heures supplémentaires par semaine. Les employés à des postes de managers ne sont pas sujets à de telles restrictions.</p>
<h3 class="spip"><a id="sommaire_3"></a>Coût des heures supplémentaires</h3>
<p>En semaine les heures supplémentaires sont payées 25% de plus. Les jours fériés, dimanches et heures supplémentaires de nuit, 75% de plus.</p>
<h3 class="spip"><a id="sommaire_4"></a>Jours de repos hebdomadaires</h3>
<p>Le jour de repos hebdomadaire a lieu le dimanche ou un autre jour de la semaine suivant la convention d’entreprise. Le minimum de « une journée » est souvent porté à deux jours dans l’administration et les grandes entreprises.</p>
<h3 class="spip"><a id="sommaire_5"></a>Congés annuels</h3>
<p>Chaque employé a droit à un minimum de 15 jours ouvrables de congés payés.</p>
<h3 class="spip"><a id="sommaire_6"></a>Congés maladie</h3>
<p>Chaque employé a le droit à 180 jours calendaires de congés maladie.</p>
<h3 class="spip"><a id="sommaire_7"></a>Congé de maternité</h3>
<p>Le congé de maternité est de trois mois en Colombie. La femme en congé maternité touchera la totalité de son salaire durant 84 jours.</p>
<h3 class="spip"><a id="sommaire_8"></a>Fêtes légales</h3>
<p>Les jours fériés sont :</p>
<ul class="spip">
<li>le Jour de l’an : 1er janvier,</li>
<li>la Fête du travail : 1er mai,</li>
<li>la Fête de l’Indépendance de Colombie : 20 juillet,</li>
<li>la Fête du Boyacá : 7 aout,</li>
<li>la Toussaint : 1er novembre,</li>
<li>la Fête de l’Immaculée Conception : 8 décembre</li>
<li>le Noël : 25 décembre.</li></ul>
<p>A ces jours fixes s’ajoutent l’Epiphanie, les jeudis et vendredi saints, saint Joseph, la Fête du « Corpus Christi », la Fête du Sagrado Corazón, les Fêtes des Saints Pierre et Paul, l’Assomption, la Fête de la découverte de l’Amérique et celle de l’Indépendance de Carthagène.</p>
<p>A noter que les activités sont également plus réduites durant la semaine sainte (qui a lieu en mars ou avril) et durant la période du 15 décembre au 15 janvier.</p>
<p>Pour en savoir plus : <a href="http://www.tv5.org/cms/chaine-francophone/voyageurs/Outils/p-8228-Jours-feries.htm" class="spip_out" rel="external">les jours fériés dans le monde sur le site de TV5</a></p>
<p><strong>Sources</strong> :</p>
<ul class="spip">
<li><a href="http://www.ambafrance-co.org/" class="spip_out" rel="external">Ambassade de France à Bogota</a>,</li>
<li><a href="http://export.businessfrance.fr/default.html" class="spip_out" rel="external">Business France</a>,</li>
<li><a href="http://www.dane.gov.co" class="spip_out" rel="external">DANE (statistiques officielles de l’Etat colombien)</a>,</li>
<li><a href="http://www.lemoci.com/Colombie/14-Legislation-du-travail.htm" class="spip_out" rel="external">le MOCI</a>,</li>
<li><a href="https://www.cia.gov/library/publications/the-world-factbook/docs/didyouknow.html" class="spip_out" rel="external">CIA World factbook</a></li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/emploi-stage/article/reglementation-du-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
