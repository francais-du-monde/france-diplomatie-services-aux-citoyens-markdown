# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/emploi-stage/article/reglementation-du-travail#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/emploi-stage/article/reglementation-du-travail#sommaire_2">Contrat de travail – spécificités</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/emploi-stage/article/reglementation-du-travail#sommaire_3">Fêtes légales</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/emploi-stage/article/reglementation-du-travail#sommaire_4">Emploi du conjoint</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<h4 class="spip">Période de préavis</h4>
<p>Il n’y a pas de règle arrêtée quant à la durée de la période d’essai, mais elle est généralement de six mois pour un CDI. Les travailleurs à l’essai sont protégés par le <i>Minimum Notice and Terms of Employment Act</i>, qui prévoit notamment que tout salarié employé depuis plus de 13 semaines a droit à un préavis de licenciement d’au moins une semaine.</p>
<h4 class="spip">Durée du travail</h4>
<p>La durée légale du travail est régie par l’<i>Organization of Working Time Act, 1997</i>. La limite maximale est de 13 heures par jour et de 48 heures par semaine. En pratique, elle est fixée contractuellement selon les différents secteurs d’activité et calculée sur une période de 4,6 ou 12 mois. Elle est en général de 42,5 heures dans l’industrie et de 37,5 heures dans le secteur tertiaire.</p>
<p>La législation irlandaise limite la durée des heures supplémentaires à deux heures par jour, 12 heures par semaine, 36 heures par mois ou 240 heures par an. Le travail supplémentaire est rétribué sur la base horaire, majorée au minimum de 25%.</p>
<p>L’employé est autorisé à prendre une pause de 15 minutes s’il a travaillé 4,5 heures ou une pause de 30 minutes s’il a travaillé six heures. La durée minimum des congés payés est, depuis le 1er avril 1999, de quatre semaines pour une période de 12 mois de travail, sans compter les jours fériés chômés (neuf jours par an).</p>
<h4 class="spip">Autres dispositions</h4>
<p><strong>Le salaire minimum national a été introduit le 1er avril 2000</strong>. Son taux horaire standard était de 5,97 euros. Il a été réévalué à 8,65 euros en 2008. Par ailleurs, plusieurs catégories d’employés bénéficient, pour des raisons historiques, d’un régime particulier fixant le montant minimum de leurs salaires (<i>Industrial Relations Act, 1946</i>).</p>
<p>Pour plus d’informations, il est possible de télécharger la brochure <i>National Minimum Wage Act, 2000</i> sur le site internet du <i>Department of Jobs, Enterprise and Innovation</i>.</p>
<p>Il est à noter que le <i>Carer’s Leave Act </i>entré en vigueur le 2 juillet 2001, permet aux employés d’obtenir un congé sans solde de 104 semaines au maximum (13 semaines au minimum) afin de s’occuper de personnes dépendantes.</p>
<p><a href="http://www.enterprise.gov.ie/en/" class="spip_out" rel="external">Department of Jobs, Enterprise and Innovation</a><br class="manualbr">23 Kildare Street - Dublin 2<br class="manualbr">Tél. : [353] (0)1 631 2121 - LoCall : 1890 220 222<br class="manualbr">Fax : [353] (0)1 631 2827<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/emploi-stage/article/reglementation-du-travail#webmaster#mc#entemp.ie#" title="webmaster..åt..entemp.ie" onclick="location.href=mc_lancerlien('webmaster','entemp.ie'); return false;" class="spip_mail">Courriel</a></p>
<h3 class="spip"><a id="sommaire_2"></a>Contrat de travail – spécificités</h3>
<h4 class="spip">Cadre du contrat : détachement ou expatriation</h4>
<p>Deux cas de figure peuvent se présenter. Le contrat de travail peut être fait soit dans le cadre d’un détachement, soit dans le cadre d’une expatriation.</p>
<ul class="spip">
<li>Dans le cadre du détachement, l’entreprise emploie un salarié en France puis le détache à l’étranger, pour une période variable mais limitée, au terme de laquelle l’employé retrouve son poste en France. Ce contrat sera soumis au droit français.</li>
<li>Dans le cadre de l’expatriation, le salarié est recruté soit en France, soit à l’étranger pour le compte d’une entreprise implantée à l’étranger ou d’une société locale.</li></ul>
<p>Les conditions de négociation ne seront pas les mêmes selon que l’employeur fait venir le salarié de France ou qu’il l’engage selon les modalités d’un contrat de travail local. Dans le premier cas, si les parties en décident ainsi, le contrat pourra être soumis au droit français. S’il s’agit d’un contrat local, les relations de travail seront régies par le droit local.</p>
<h4 class="spip">Règles locales applicables à l’Irlande en matière de contrat</h4>
<p>En Irlande, les contrats ne sont pas toujours écrits, mais passent par un accord systématique entre l’employeur et le salarié. Ainsi, même dans le cas d’un accord verbal, le contrat est réputé exister entre les deux parties.</p>
<p>L’employeur est cependant tenu de présenter au salarié, sous 28 jours après la prise de poste, un document écrit (<i>written statement</i>) sur lequel doivent figurer, au minimum, les mentions suivantes :</p>
<ul class="spip">
<li>le nom complet de l’employeur et du salarié ;</li>
<li>l’intitulé du poste et la nature du travail exécuté ;</li>
<li>la date d’embauche ;</li>
<li>les taux de rémunération ou la méthode de calcul utilisée ;</li>
<li>les modalités du versement de la rémunération ;</li>
<li>les conditions relatives au temps de travail et aux heures supplémentaires ;</li>
<li>les conditions applicables aux congés payés, à l’incapacité de travail due à une maladie ou à un accident, aux prestations de maladie, à la retraite et aux régimes de retraite ;</li>
<li>la durée du préavis en cas de cessation du contrat décidée par l’une ou l’autre partie (généralement d’une semaine pour un employé présent depuis moins d’un an, et d’un mois pour les employés avec plus d’un an ancienneté).</li></ul>
<p>Les conditions de recrutement étant souvent normalisées au sein d’un même secteur d’activité, il arrive que certaines de ces mentions obligatoires soient remplacées par des références aux textes réglementaires, sous réserve que ceux-ci soient facilement accessibles à l’employé.</p>
<p>L’employeur doit, le cas échéant, mentionner dans le <i>written statement</i> les conditions applicables lors des déplacements professionnels à l’étranger. Tout changement des termes du <i>written statement</i> doit être notifié à l’employé dans un délai d’un mois après sa prise d’effet.</p>
<h4 class="spip">Négociation du contrat de travail</h4>
<p>La négociation du contrat de travail sera différente selon qu’il s’agira d’un contrat de détachement avec un employeur en France, d’un contrat d’expatriation (avant le départ en Irlande) ou d’un contrat local. Dans ce dernier cas, il convient en outre d’être vigilant quant à la nature du contrat ; en effet, s’il est oral, le contrat est soumis au droit irlandais et la marge de négociation de l’employé s’en trouvera considérablement réduite. Il convient par conséquent, lorsque cela est possible, de solliciter un contrat écrit sur lequel figureront les clauses arrêtées par les parties.</p>
<h4 class="spip">Rupture de contrat de travail</h4>
<p>Que la décision soit prise par l’employé ou l’employeur et comme mentionné plus haut, la durée du préavis doit être notifiée dans le contrat de travail. Selon la loi (<i>Minimum Notice and Terms of Employment Act</i>), tout salarié employé depuis plus de 13 semaines a droit à un préavis de licenciement d’au moins une semaine.</p>
<p>La période de préavis varie en fonction de l’ancienneté du salarié. Cependant, ces lois n’empêchent pas un salarié ou un employeur de renoncer à ses droits en terme de préavis ou d’accepter un paiement à la place du préavis. Un employeur peut également résilier un contrat sans préavis en cas de faute du salarié.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Notre rubrique thématique <a href="services-aux-citoyens-preparer-son-expatriation-emploi.md" class="spip_in">Emploi</a></p>
<h3 class="spip"><a id="sommaire_3"></a>Fêtes légales</h3>
<p>L’Irlande a deux catégories de jours fériés : les fêtes religieuses et les "Bank Holidays".</p>
<ul class="spip">
<li>1er janvier : jour de l’an</li>
<li>17 mars : St Patrick’s day (fête nationale)</li>
<li>dimanche et lundi de Pâques</li>
<li>1er lundi de mai : Bank Holiday</li>
<li>1er lundi de juin : Bank Holiday</li>
<li>1er lundi d’août : Bank Holiday</li>
<li>dernier lundi d’octobre : Bank Holiday</li>
<li>25 décembre : Noël</li>
<li>26 décembre : St Stephen’s day Les jours fériés tombant un dimanche sont chômés le lundi.</li></ul>
<p>Le Vendredi Saint n’est pas férié, mais généralement chômé</p>
<h3 class="spip"><a id="sommaire_4"></a>Emploi du conjoint</h3>
<p>Du fait de la réglementation européenne, les Français peuvent travailler en Irlande sans permis de travail ni de permis de séjour. Ainsi, les citoyens de l’Union européenne jouissent des mêmes droits professionnels que les citoyens irlandais.</p>
<p>Pour trouver un emploi en Irlande, il existe des moyens similaires à ceux des autres Etats membres de l’Union européenne. Vous pouvez naturellement adresser des candidatures spontanées aux entreprises ou organismes qui vous intéressent mais aussi répondre aux offres d’emploi publiées dans la presse. Cependant, et à côté des agences intérimaires, des cabinets de recrutement et des centres d’orientation professionnelle, les services publics de l’emploi constituent des intermédiaires spécialisés susceptibles d’aider dans la recherche.</p>
<p>Les services publics de l’emploi sont administrés par le FAS (<i>Foras Aiseanna Saothair</i>) qui relève du ministère de l’Entreprise, du Commerce et de l’Emploi (<i>Department of Enterprise, Trade and Employment</i>). Le FAS gère à la fois un réseau d’agences publiques pour l’emploi et un réseau de centres de formation (bientôt remplacé par la nouvelle agence SOLAS). Le réseau d’agences couvre tout le pays.</p>
<p>Le <i>Labour Service Act</i> de 1987 lui assigne les fonctions suivantes :</p>
<ul class="spip">
<li>formation et reconversion ;</li>
<li>dispositifs pour l’emploi ;</li>
<li>service de placement et d’orientation ;</li>
<li>assistance aux groupements associatifs et coopératives ouvrières souhaitant créer des emplois ;</li>
<li>assistance aux personnes qui recherchent un emploi dans un autre Etat membre ;</li>
<li>fourniture de services d’expert et de gestion des ressources humaines, sur une base commerciale à l’extérieur du pays.L’inscription au FAS donne accès à de nombreux programmes de formation et d’emploi portant sur le choix et la préparation d’un métier et l’obtention d’un emploi correspondant. Toutefois, il n’est pas nécessaire d’être inscrit pour obtenir informations et conseils.</li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/emploi-stage/article/reglementation-du-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
