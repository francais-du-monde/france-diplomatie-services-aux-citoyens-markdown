# Protection sociale

<h2 class="rub22950">Régime local de sécurité sociale</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/protection-sociale/#sommaire_1">L’assurance médicale</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/protection-sociale/#sommaire_2">L’assurance vieillesse-invalidité-décès</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>L’assurance médicale</h3>
<p>L’assurance médicale nationale est gérée par le <a href="http://www.nhis.gov.gh/" class="spip_out" rel="external">National Health Insurance Scheme (NHIS)</a>, auquel la SSNIT reverse 2,5% des cotisations patronales qu’elle a collectées.</p>
<p>Cette assurance comporte de nombreuses exclusions, et les expatriés présents au Ghana bénéficient généralement par le biais de leur employeur d’assurances médicales privées (GLICO, Med-X, Nationwide, etc…).</p>
<h3 class="spip"><a id="sommaire_2"></a>L’assurance vieillesse-invalidité-décès</h3>
<p>Le régime en vigueur est celui décrit au <i>National pensions Act</i> de 2008 (Act 766).</p>
<p>Le régime de protection sociale au Ghana est géré par le Trust de l’assurance nationale et de la sécurité sociale (<i>Social Security and National Insurance Trust</i>). Il existe des bureaux du SSNIT dans toutes les régions. Le <i>Trust</i> dispose d’un fonds dans lequel sont versées toutes les cotisations.</p>
<p>Le régime couvre l’assurance invalidité, vieillesse et décès (pension). Le régime est applicable à tous les salariés travaillant pour le compte d’un employeur. Les travailleurs non salariés peuvent adhérer volontairement au régime.</p>
<p>Il existe une possibilité d’assurance pour les personnes qui ne relèvent plus obligatoirement du régime.</p>
<p>Le fait qu’il existe un système de pension privé ou un système de pension mis en place par la société, un fonds de prévoyance, un régime complémentaire ou un régime prévoyant une prime de départ, ne dispense pas l’employeur de cotiser au fonds.</p>
<p>L’employeur est responsable du paiement des cotisations auprès du <i>Trust</i>.</p>
<p>Le taux de cotisations est fixé à 18,5 % (13 % à la charge de l’employeur et 5,5 % à la charge du salarié). Les cotisations sont payées sur la totalité du salaire et elles doivent être réglées au "Trust" dans les quatorze jours qui suivent la fin du mois.</p>
<p>Le travailleur non salarié cotise quant à lui au taux de 18,5 % de ses revenus d’activité.</p>
<h4 class="spip">L’assurance-vieillesse</h4>
<p>Pour pouvoir bénéficier d’une pension complète, l’assuré doit être âgé d’au moins soixante ans et avoir versé un minimum de 180 mois de cotisation auprès du Trust de l’assurance nationale et de sécurité sociale".</p>
<p>Le montant de la pension complète est égal à 37,5 % de la moyenne des trois meilleures années de salaire pour 180 mois (15 ans) de cotisations. Chaque mois supplémentaire au-delà de 180 donne droit à un supplément de 0,09375 % par mois (ou 1,125 % par année). Ainsi, pour 300 mois (25 ans) de cotisation, le taux de la pension est fixé à 48,75 %.  Et le maximum de retraite est versé après 36 ans de contribution (et audelà), soit 60% de la moyenne des 36 meilleurs mois précités.</p>
<p>Si l’assuré a cotisé durant au moins 180 mois et qu’il demande la liquidation de sa pension entre cinquante-cinq ans et soixante ans, seulement un pourcentage de la pension complète est liquidé (ex : à 55 ans, il reçoit 60% de la pension complète à laquelle il aurait pu prétendre à 60 ans).</p>
<h4 class="spip">L’assurance-invalidité</h4>
<p>Pour pouvoir prétendre à une pension d’invalidité, il faut avoir cotisé durant au moins 12 mois au cours des 36 derniers mois précédant la réalisation du risque, être atteint d’une invalidité permanente reconnue par un médecin agréé, et être incapable d’exercer une activité rémunérée normale quelle qu’elle soit.</p>
<p>Si l’assuré a versé au moins 180 mois de cotisations, il a droit à la liquidation de sa pension selon les droits qu’il a acquis auprès du régime.</p>
<p>S’il n’a pas versé la cotisation minimale, le montant de sa pension est égal à 37,5 % de la moyenne des salaires des trois meilleures années.</p>
<h4 class="spip">L’assurance-décès</h4>
<p>Les prestations servies aux survivants de pensionnés âgés de moins de 75 ans sont versées sous forme de capital.</p>
<p>Les survivants sont ceux qui ont été officiellement désignés par l’assuré à l’institution. Si les ayants droits ne sont pas désignés, le capital est servi aux personnes susceptibles d’y ouvrir droit, sous réserve d’avoir cotisé un minimum de 12 mois pendant les 36 derniers mois.</p>
<p>Si l’assuré décédé satisfaisait à la période minimale de cotisations, le capital versé aux ayants droit correspond au montant de la pension qui aurait été servie à l’assuré durant douze ans.</p>
<p>Si l’assuré ne remplissait pas les conditions de durée minimale, la somme versée correspond au versement de douze années de pension calculée sur 50 % des trois meilleures années de salaire.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-ghana-protection-sociale-article-convention-de-securite-sociale.md" title="Convention de sécurité sociale">Convention de sécurité sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-ghana-protection-sociale-article-regime-local-de-securite-sociale.md" title="Régime local de sécurité sociale">Régime local de sécurité sociale</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/protection-sociale/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
