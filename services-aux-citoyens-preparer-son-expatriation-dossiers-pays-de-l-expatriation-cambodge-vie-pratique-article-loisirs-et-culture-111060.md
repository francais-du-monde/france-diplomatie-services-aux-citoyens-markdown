# Loisirs et culture

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/cambodge/vie-pratique/article/loisirs-et-culture-111060#sommaire_1">Tourisme</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/cambodge/vie-pratique/article/loisirs-et-culture-111060#sommaire_2">Activités culturelles</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/cambodge/vie-pratique/article/loisirs-et-culture-111060#sommaire_3">Sports</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/cambodge/vie-pratique/article/loisirs-et-culture-111060#sommaire_4">Télévision – Radio</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/cambodge/vie-pratique/article/loisirs-et-culture-111060#sommaire_5">Presse française</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Tourisme</h3>
<p>Les zones touristiques traditionnelles : environs de Phnom-Penh (Oudong, Tonle Bati) et la plage de Sihanouk ville ne présentent que peu de problèmes de sécurité. A Phnom-Penh, persistance de vols à main armée visant les étrangers circulant isolément de nuit.</p>
<p>Pour visiter les temples d’Angkor, il est conseillé de se rendre à Siem Reap par avion, car certaines portions des voies terrestres qui y mènent sont encore peu praticables et les bateaux empruntant la voie fluviale entre Phnom-Penh et Siem Reap sont dépourvus d’équipements de sécurité (un de ces bateaux a coulé entraînant la noyade d’un touriste). En outre les passagers de l’un d’entre eux ont été dépouillés par des voleurs armés qui ont pris le contrôle du bateau une heure après son départ de Phnom-Penh. Pour se rendre au temple de Banteay Srei, utiliser de préférence les services d’une agence de tourisme et, en tout cas, ne pas s’écarter de la route ni de la zone sécurisée autour du temple : risques de mines et de banditisme. Pour accéder aux Monts Kulen, passer par les services d’un guide et emprunter la piste (payante) qui mène à la rivière des mille Lingas et au Bouddha couché. Ne pas s’écarter de cette piste ni de ces deux sites : risques de mines.</p>
<p>Capitale du royaume khmer du 9ème au 15ème siècle, connu du monde occidental depuis le milieu du dix-neuvième siècle et inscrit sur la liste du patrimoine mondial de l’Unesco, le site d’Angkor réunit plus d’une centaine de temples dont l’ensemble le plus célèbre, Angkor Vat, occupe à lui seul une superficie de 9 km².</p>
<p>Pour visiter les temples d’Angkor, il est conseillé de se rendre à Siem Reap par avion même si la ville n’est située qu’à 350 km de Phnom-Penh. Il est en outre conseillé de ne pas s’aventurer la nuit hors de la ville de Siem Reap (y compris dans les temples) car il existe un risque d’agression. D’une manière générale, lors de visites de sites, il est avisé de s’en tenir aux lieux ouverts par les autorités cambodgiennes et aux parcours balisés en raison de la présence de mines (au début de l’année 2000, il existait encore de très nombreux champs de mines non répertoriés dans la seule province de Siem Reap).</p>
<p>Il est à noter que l’acquisition et l’exportation illicites de "biens culturels" (notamment les pièces archéologiques : sculptures, poteries, etc.) sont interdites et punies de six mois à dix ans de prison.</p>
<h3 class="spip"><a id="sommaire_2"></a>Activités culturelles</h3>
<p><strong>Activités culturelles en français</strong></p>
<p>Livres en français : Les livres français peuvent être trouvés au Centre Culturel Français (adresse ci-dessous) et à la Mekong Libris (Librairie et Presse française) située Place de la Poste Centrale à Phnom-Penh.</p>
<p>L’Institut Français du Cambodge (IFC) organise plusieurs séances de cinéma par semaine en version française ainsi qu’une dizaine de spectacles et autant d’expositions, photographiques surtout, durant l’année. L’IFC dispose également d’une bibliothèque et d’une médiathèque.</p>
<p><strong>Institut Français du Cambodge</strong><br class="manualbr">218, rue Kéo Chéa (rue 184) <br class="manualbr">Phnom-Penh <br class="manualbr">Tél. : (855) 23 213 124 <br class="manualbr">Fax : (855) 23 721 382 <br class="manualbr">Courriel :</p>
<p><strong>Activités culturelles locales</strong></p>
<p>Il existe cinq chaînes de télévision khmères, accessibles par la voie hertzienne.</p>
<p>Le câble permet d’accéder à la BBC, à Deutsche Welle, à CNN, à des chaînes chinoises, indiennes, thaïlandaises et vietnamiennes. La qualité en est variable cependant. Le système vidéo utilisé est PAL / NTSC. La vidéo est très répandue, la vidéothèque la plus importante étant celle du Centre Culturel Français. On peut apporter son magnétoscope s’il est "multistandard".</p>
<h3 class="spip"><a id="sommaire_3"></a>Sports</h3>
<p>Il existe deux clubs à Phnom-Penh : l’un pour le rugby, l’autre pour le football.</p>
<p>Tennis, natation et musculation peuvent également être pratiqués mais le choix et la qualité des équipements sont très limités.</p>
<p>La chasse est pratiquée mais n’a aucune existence officielle en l’absence totale de réglementation. Une loi sur les armes est en cours d’élaboration. Elle devrait inclure des dispositions sur les armes de chasse.</p>
<p>Il n’est pas nécessaire de disposer d’un permis pour la pêche, possible en toutes saisons.</p>
<h3 class="spip"><a id="sommaire_4"></a>Télévision – Radio</h3>
<p>La chaîne nationale de télévision TVK propose une reprise des programmes de CFI (films et documentaires doublés en khmer) 25h par mois. Des informations en français sont diffusées dans l’émission "Rendez-vous" tous les soirs de la semaine à 21h30. <a href="http://www.tv5.org/asie" class="spip_out" rel="external">TV5</a> est également accessible.</p>
<p><a href="http://www.rfi.fr/" class="spip_out" rel="external">Radio France Internationale (RFI)</a> peut être captée 24h/24 en modulation de fréquence avec une excellente réception à Phnom-Penh et à Siem Reap (92 FM).</p>
<h3 class="spip"><a id="sommaire_5"></a>Presse française</h3>
<p>Il n’y a ni censure ni restrictions à l’importation de journaux, livres ou cassettes. La presse française est disponible dans différents kiosques à Siem Reap, et dans des grands hôtels de Phnom-Penh (Hôtel Cambodiana et Hôtel Intercontinental) et à Siem Reap (Sofitel Royal Angkor), des magasins d’alimentation, dans certains restaurants, à l’aéroport et au Centre Culturel Français.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/cambodge/vie-pratique/article/loisirs-et-culture-111060). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
