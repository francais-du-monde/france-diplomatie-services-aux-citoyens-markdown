# Fiscalité du pays

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/fiscalite/article/fiscalite-du-pays-110526#sommaire_1">Régime fiscal des personnes morales</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/fiscalite/article/fiscalite-du-pays-110526#sommaire_2">Régime fiscal des personnes physiques</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/fiscalite/article/fiscalite-du-pays-110526#sommaire_3">Instauration d’un régime fiscal de faveur pour les pensions et les rentes viagères de source étrangère</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/fiscalite/article/fiscalite-du-pays-110526#sommaire_4">Modalités de paiement des impôts : obligations déclaratives</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/fiscalite/article/fiscalite-du-pays-110526#sommaire_5">Année fiscale</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/fiscalite/article/fiscalite-du-pays-110526#sommaire_6">Barème de l’impôt</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/fiscalite/article/fiscalite-du-pays-110526#sommaire_7">Quitus fiscal</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/fiscalite/article/fiscalite-du-pays-110526#sommaire_8">Solde du compte en fin de séjour</a></li>
<li><a id="so_9" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/fiscalite/article/fiscalite-du-pays-110526#sommaire_9">Coordonnées des centres d’information fiscale</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Régime fiscal des personnes morales</h3>
<p>Le régime de droit commun s’applique aux entreprises françaises et à leur personnel qui exercent en Tunisie une activité commerciale. Le système fiscal en vigueur s’articule essentiellement autour du dispositif suivant :</p>
<h4 class="spip">L’impôt sur les sociétés (IS)</h4>
<p>Toutes les entreprises sont en principe imposables. Les bénéfices provenant des opérations d’exportation sont toutefois exonérées en totalité pendant les 10 premières années pour les sociétés totalement exportatrices. Ce régime d’exonération totale a été maintenu jusqu’au 31 décembre 2010.</p>
<p>Depuis le 1er janvier 2007, le taux de l’IS est passé de 35 à 30% sauf pour certains secteurs (pétrolier, financier, et télécommunications). Il existe par ailleurs un taux réduit de 10% s’appliquant aux entreprises exerçant une activité artisanale de pêche ou agricole.</p>
<p>Quelque soit le résultat net imposable, l’entreprise est soumise à un minimum légal de 0,1% du chiffre d’affaires brut, avec un minimum d’impôts de 250 dinars pour les entreprises soumises à un taux de 30 à 35% et de 100 dinars pour celles soumises à un taux de 10%.</p>
<h4 class="spip">TVA (Taxe sur la valeur ajoutée)</h4>
<p>En principe, toutes les opérations de vente ou de prestations de service sont imposables en Tunisie. Sont assujetties les personnes physiques ou morales réalisant des opérations imposables. Toutefois les commerçants détaillants qui réalisent un chiffre d’affaires annuel inférieur à 100.000 dinars ne sont pas assujettis.</p>
<p>Il existe un taux normal de 18% et deux taux réduits de 12% et 6% applicables à certaines opérations définies par la loi. Le taux majoré de 29% pour certains produits de luxe a été supprimé le 1er janvier 2007.</p>
<p>Les entreprises totalement ou partiellement exportatrices peuvent acquérir en suspension de TVA tous les biens et services nécessaires aux opérations d’exportation.</p>
<h4 class="spip">Taxe parafiscale assise sur les produits de certains secteurs industriels</h4>
<h5 class="spip">Droits d’enregistrement et de timbre</h5>
<p>Les actes portant constitution de société, augmentation de capital, fusion et généralement tout acte intéressant la vie juridique de la société et ses différentes transformations sont soumis à un droit fixe de 100 TND par acte. Les mutations immobilières sont soumises à un droit proportionnel de 5% et les mutations de fonds de commerce à un droit proportionnel de 2,5%, indépendamment de la qualité des parties. Les entreprises totalement exportatrices en sont exonérées.</p>
<h5 class="spip">Taxes sur les salaires</h5>
<p>Taxe de formation professionnelle (TFP) : elle est due par toute personne exerçant une activité dans l’industrie, le commerce, et l’agriculture.. Elle est prélevée au taux de 2%. Il existe un taux particulier de 1% pour les industries manufacturières ;</p>
<p>La contribution au fonds de promotion du logement pour les salariés : elle est due par les employeurs quelle que soit la nature de leur activité. Elle est prélevée au taux de 1%. Les entreprises totalement exportatrices en sont exonérées.</p>
<h5 class="spip">La taxe sur les établissements à caractère industriel, commercial ou professionnel (TCL)</h5>
<p>Elle est due par les personnes morales soumises à l’IS au taux de 0,2% du chiffre d’affaires brut local (plafond de 60 000 dinars).</p>
<h5 class="spip">La taxe sur les immeubles bâtis (TIB)</h5>
<p>Elle est calculée sur la base de la superficie des immeubles abritant l’activité de l’entreprise. Les entreprises totalement exportatrices sont exonérées de la TCL mais sont soumises à la TIB.</p>
<h3 class="spip"><a id="sommaire_2"></a>Régime fiscal des personnes physiques</h3>
<p>Sont soumises à l’impôt sur le revenu de façon globale les personnes :</p>
<ul class="spip">
<li>ayant une résidence habituelle en Tunisie (disposition d’une habitation permanente en Tunisie) ;</li>
<li>séjournant en Tunisie plus de 183 jours de façon continue ou discontinue (la durée de 183 jours s’apprécie par année civile).</li></ul>
<p>Ces personnes sont imposables sur leurs revenus de source tunisienne et sur leurs revenus de source étrangère qui n’ont pas été soumis à l’impôt à l’étranger. Les personnes employées (dans la limite de quatre personnes par entreprise) par des entreprises non-résidentes, totalement exportatrices, bénéficient de l’imposition forfaitaire au taux de 20% sur leurs rémunérations brutes (tout avantage en nature inclus). Elles peuvent opter pour le droit commun si celui-ci leur est plus favorable.</p>
<h3 class="spip"><a id="sommaire_3"></a>Instauration d’un régime fiscal de faveur pour les pensions et les rentes viagères de source étrangère</h3>
<p>L’article 35 de la loi de finances pour l’année 2007 a relevé le taux de déduction pour la détermination du revenu net pour les pensions et les rentes viagères de source étrangère de 25% à <strong>80% </strong>à condition :</p>
<ul class="spip">
<li>de les transférer à un compte bancaire ou postal en Tunisie ou de les déclarer à l’importation,</li>
<li>de joindre à la déclaration annuelle de l’impôt les justificatifs de leur transfert ou de leur importation en Tunisie.</li></ul>
<p>Cette mesure s’applique à tous les résidents de la Tunisie qui reçoivent des pensions ou des rentes viagères de l’étranger et ce nonobstant leur nationalité y compris les tunisiens.</p>
<p>L’article 36 de la loi de finances pour l’année 2007 a dispensé les bénéficiaires de rentes viagères de source étrangère d’opérer la retenue à la source au titre des montants leur revenant à ce titre.</p>
<p>Les nouvelles dispositions s’appliquent aux pensions et aux rentes viagères de source étrangère perçues en 2006 et déclarées en 2007 et aux pensions et rentes perçues au cours des années ultérieures. (article 88)</p>
<h3 class="spip"><a id="sommaire_4"></a>Modalités de paiement des impôts : obligations déclaratives</h3>
<p>Pour les personnes physiques l’impôt est retenu à la source par l’employeur. De manière générale, les entreprises commerciales sont tenues :</p>
<ul class="spip">
<li>de retenir et de reverser au Trésor, mensuellement, les impôts sur le revenu des salariés qu’elles emploient, sur la base d’un barème fourni par l’administration ;</li>
<li>de retenir et de reverser les retenues effectuées sur les honoraires, loyers, commissions, courtages, etc., payés à des tiers ;</li>
<li>de déclarer, avant le 1er février de chaque année, les salaires, commissions, courtages, loyers, etc., payés à des tiers et les retenues d’impôt correspondantes effectuées sur ces différents éléments (art. 52 du Code de l’impôt).</li></ul>
<p>En ce qui concerne plus particulièrement l’IS, la déclaration doit être souscrite et les droits payés annuellement dans les trois mois de la clôture des comptes et ce, le 25ème jour du 3ème mois au plus tard, soit le 25 mars pour les sociétés qui clôturent leurs comptes au 31 décembre. Sauf pour la première année d’exploitation, l’IS est payable par voie d’acomptes provisionnels (trois au total) égaux chacun à 30% de l’IS de l’année précédente. Les acomptes provisionnels sont payables pendant les 25 premiers jours du sixième, neuvième ou douzième mois qui suivent la date de clôture des comptes. Ainsi, les acomptes provisionnels pour les entreprises qui clôturent leurs comptes au 31 décembre sont payables au plus tard : le 25 juin, le 25 septembre et le 25 décembre de chaque année.</p>
<p>Pour sa part, la déclaration de TVA est faite mensuellement :</p>
<ul class="spip">
<li>avant le 28 de chaque mois pour les personnes morales,</li>
<li>le 15 de chaque mois pour les personnes physiques, sur les livraisons de biens, comme sur les prestations encaissées partiellement ou totalement, au cours du mois précédent.</li></ul>
<p>Les deux taxes assises sur les salaires, déductibles de l’assiette de lis, sont payables avant le 28 de chaque mois.</p>
<p>La taxe sur les établissements à caractère industriel et commercial est due mensuellement, avant le 28 de chaque mois, et payable à la recette du siège de l’entreprise.</p>
<h3 class="spip"><a id="sommaire_5"></a>Année fiscale</h3>
<p>L’année fiscale correspond à l’année civile.</p>
<h3 class="spip"><a id="sommaire_6"></a>Barème de l’impôt</h3>
<p>Barème de l’impôt sur le revenu appliqué aux revenus annuels nets des personnes physiques :</p>
<ul class="spip">
<li>jusqu’à 1500 dinars : 0% ;</li>
<li>de 1501 à 5000 dinars : 15% ;</li>
<li>de 5001 à 10 000 dinars : 20% ;</li>
<li>de 10 001 à 20 000 dinars : 25% ;</li>
<li>de 20 001 à 50 000 dinars : 30% ;</li>
<li>plus de 50 000 dinars : 35%.</li></ul>
<h3 class="spip"><a id="sommaire_7"></a>Quitus fiscal</h3>
<p>Après accord de la Banque centrale de Tunisie, un quitus fiscal est exigé.</p>
<h3 class="spip"><a id="sommaire_8"></a>Solde du compte en fin de séjour</h3>
<p>Il est possible pour un expatrié français de solder son compte en fin de séjour.</p>
<h3 class="spip"><a id="sommaire_9"></a>Coordonnées des centres d’information fiscale</h3>
<p>Au niveau national :</p>
<p><strong>La Direction Générale des Impôts (DGI)</strong><br class="manualbr">93 Avenue Hédi Chaker - 1002 Tunis<br class="manualbr">Tél. : (+216) 71 78 09 40 / (+216) 71 78 66 50 - Fax : (+216) 71 79 90 10</p>
<p>Au niveau local :</p>
<p><strong>Le Centre régional de contrôle des impôts de Tunis I</strong><br class="manualbr">11 rue Borj Bourguiba - 1002 Tunis<br class="manualbr">Tél. : (+216) 71 25 57 11</p>
<p><strong>Le Centre régional de contrôle des impôts de Tunis II</strong><br class="manualbr">14 rue Asdrubal - 1002 Tunis<br class="manualbr">Tél. : (+216) 71 83 10 08</p>
<p><i>Mise à jour : novembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/fiscalite/article/fiscalite-du-pays-110526). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
