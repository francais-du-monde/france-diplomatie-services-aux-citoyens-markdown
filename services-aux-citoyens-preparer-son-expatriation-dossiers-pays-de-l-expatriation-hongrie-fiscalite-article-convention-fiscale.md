# Convention fiscale

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/fiscalite/article/convention-fiscale#sommaire_1">Champ d’application de la convention</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/fiscalite/article/convention-fiscale#sommaire_2">Dispositions conventionnelles sur certaines catégories de revenus</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/fiscalite/article/convention-fiscale#sommaire_3">Etudiants, stagiaires, enseignants, chercheurs</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/fiscalite/article/convention-fiscale#sommaire_4">Elimination de la double imposition</a></li></ul>
<p><strong>La France et la Hongrie ont signé, le 28 avril 1980, une Convention en matière de fiscalité publiée</strong> au Journal Officiel du 6 janvier 1982.</p>
<p>Cette Convention tend à éviter les doubles impositions en matière d’impôts sur le revenu et sur la fortune qui pourraient résulter de l’application des législations de ces deux Etats.</p>
<p>Le texte de la convention peut être obtenu auprès de la Direction des Journaux Officiels, 26 rue Desaix 75727 Paris cedex 15, par courrier, par fax (01 40 58 77 80), ou sur le site Internet du ministère des Finances.</p>
<p>Comme les dispositions conventionnelles ont primauté sur les dispositions du droit interne, selon l’article 55 de la Constitution Française, cette convention permet de répartir entre les deux Etats, le droit d’imposer les revenus perçus par leurs résidents respectifs.</p>
<h3 class="spip"><a id="sommaire_1"></a>Champ d’application de la convention</h3>
<p>Cet accord a pour objet de protéger les résidents de chacun des Etats contractants en matière d’impôts prélevés directement sur le revenu ou sur la fortune.</p>
<h4 class="spip">Personnes concernées</h4>
<p>L’article 1 de la Convention s’applique aux personnes qui sont des résidents d’un Etat contractant ou des deux Etats contractants.</p>
<h4 class="spip">Notion de résidence </h4>
<p>L’article 4, paragraphe 2 de la Convention s’applique aux personnes qui sont considérées comme "résidents d’un Etat contractant" ou de chacun de ces deux Etats.</p>
<p>D’après ce même article de la Convention, une personne est considérée comme "résident d’un Etat contractant" lorsque, en vertu de la législation dudit Etat, elle se trouve assujettie à l’impôt à raison de son domicile, de sa résidence ou de critères analogues.</p>
<p>Au paragraphe 2, l’article 4 fournit des critères subsidiaires permettant de résoudre le cas de double résidence si l’assujettissement à l’impôt ne pouvait suffire.</p>
<p>Ces critères sont :</p>
<ul class="spip">
<li>un foyer d’habitation permanent (il s’agit, par exemple, du lieu de domiciliation du conjoint ou des enfants)</li>
<li>l’Etat où la personne possède le centre de ses intérêts vitaux (tant professionnels que privés)</li>
<li>l’Etat dans lequel elle séjourne de façon habituelle (notion de 183 jours de présence physique sur le territoire au cours d’une année fiscale)</li>
<li>à défaut, l’Etat dont elle possède la nationalité.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Dispositions conventionnelles sur certaines catégories de revenus</h3>
<h4 class="spip">Traitements, salaires, pensions et rentes</h4>
<h5 class="spip">Rémunérations privées</h5>
<p><strong>Principe</strong></p>
<p>L’article 15, paragraphe 1 précise que les traitements et salaires d’origine privée ne sont, en règle générale, imposables que dans l’Etat où <strong>s’exerce l’activité personnelle</strong>.</p>
<p><strong>Exceptions à cette règle générale :</strong></p>
<p>1) Le maintien de l’imposition dans l’Etat de résidence du bénéficiaire est prévu par le paragraphe 2 du même article sous réserve de trois conditions simultanément remplies :</p>
<ul class="spip">
<li>le séjour temporaire du bénéficiaire dans l’autre Etat ne dépasse pas une durée totale de 183 jours (au cours de l’année civile)</li>
<li>la rémunération est payée par un employeur qui n’est pas résident de l’Etat d’exercice ;</li>
<li>la rémunération ne doit pas être à la charge d’un établissement stable ou d’une base fixe de l’employeur dans l’Etat.
<p><strong>Exemple</strong> : Monsieur X est envoyé à Budapest trois mois, soit 90 jours (mai, juin, juillet de l’année N) par une PME fabriquant de la maroquinerie en vue de prospecter le marché hongrois. Cette entreprise ne dispose d’aucune succursale ni bureau en Hongrie. Dans un tel cas, Monsieur X devra déclarer ses revenus en France.</p>
<p>Au contraire, si Monsieur X est envoyé du mois de février inclus au mois de novembre inclus, son séjour de plus de 183 jours en Hongrie entraîne son imposition dans ce pays.</p></li></ul>
<p>2) Il résulte des dispositions du paragraphe 3 de l’article 15 de la Convention que les rémunérations des salariés, autres qu’intérimaires employés à bord d’un navire, d’un aéronef en trafic international ou à bord d’un bateau servant à la navigation intérieure, ne sont imposables que dans l’Etat où se trouve la direction effective de l’entreprise.</p>
<h5 class="spip">Rémunérations publiques</h5>
<p><strong>Principe</strong></p>
<p>L’article 19 paragraphes 1 et 2 indique que les traitements, salaires et rémunérations analogues ainsi que les <strong>pensions de retraite</strong> payés par un Etat ou une personne morale de droit public de cet Etat restent imposables dans cet Etat.</p>
<p><strong>Exemple</strong> : Monsieur X est fonctionnaire de l’Etat Français, résident en activité en France et décide d’aller prendre sa retraite chez l’un de ses enfants en Hongrie. Les montants de ses pensions resteront imposés en France ; son dossier étant pris en charge par un Centre des Impôts spécial, le Centre des Impôts des Fonctionnaires et Agents de l’Etat Hors de France.</p>
<p><strong>Exception</strong></p>
<p>Toutefois, en vertu des dispositions du paragraphe 3 du même article, les règles fixées aux paragraphes 1 et 2 dudit article ne sont pas applicables aux rémunérations ou aux pensions versées au titre <strong>de services rendus dans le cadre d’une activité industrielle ou commerciale exercée par un Etat ou une personne morale de droit public</strong>.</p>
<p>Les sommes versées à ce titre sont imposées soit dans l’Etat d’exercice de l’activité (article 15 de la Convention), soit dans l’Etat de résidence du bénéficiaire (article 18 de la Convention), hormis les pensions liées au régime local de sécurité sociale.</p>
<p><strong>Exemple :</strong></p>
<p>1) Monsieur X, agent E.D.F. est envoyé en Hongrie afin d’effectuer des travaux de conception avec les services locaux. Monsieur X est rémunéré par E.D.F. France. <br class="manualbr">E.D.F. étant un établissement de droit public à caractère industriel et commercial, les rémunérations allouées à Monsieur X seront imposées en Hongrie.</p>
<p>2) Monsieur X, retraité de la S.N.C.F., perçoit une pension de la Caisse vieillesse de la S.N.C.F. Or, Monsieur X, résidant en France a décidé de vivre à Budapest. Cette retraite se verra ainsi imposable en Hongrie puisque la S.N.C.F. a le caractère d’un établissement public, industriel et commercial.</p>
<h5 class="spip">Pensions et rentes</h5>
<p>L’article 18, paragraphe 1 prévoit que les pensions de retraite de source privée ainsi que les rentes viagères restent imposables dans l’Etat dont le bénéficiaire est un résident.</p>
<p>Toutefois, le paragraphe 2 dispose que ces mêmes pensions payées en application de la législation sur la sécurité sociale d’un Etat ne sont imposables que dans cet Etat.</p>
<p>Cette disposition couvre également les pensions publiques auxquelles les règles de l’article 19 ne sont pas applicables.</p>
<p><strong>Exemple</strong> : Monsieur X, citoyen français qui a exercé une activité salariée en Hongrie, décide de venir prendre sa retraite en France. Les pensions versées par la Hongrie au titre de cette activité sont imposables en France, à l’exception des pensions liées au régime de la sécurité sociale hongroise, qui sont imposables en Hongrie.</p>
<h3 class="spip"><a id="sommaire_3"></a>Etudiants, stagiaires, enseignants, chercheurs</h3>
<p><strong>Etudiants, stagiaires</strong></p>
<p>L’article 20, paragraphe 1 de la Convention prévoit que les étudiants, les stagiaires d’un Etat qui séjournent dans l’autre Etat à seule fin d’y poursuivre leurs études ou leur formation et qui perçoivent des subsides d’origine étrangère à cet Etat sont exonérés d’impôt par ce dernier Etat.</p>
<p><strong>Enseignants, chercheurs</strong></p>
<p>L’article 21, paragraphe 1 précise que les rémunérations versées aux enseignants ou aux chercheurs résidents d’un Etat se rendant temporairement dans l’autre Etat en vue d’y exercer une activité pédagogique pendant une période <strong>ne dépassant pas deux ans</strong>, dans une université, un collège, une école, un établissement de recherche ou autre, restent imposables dans l’Etat de résidence.</p>
<h4 class="spip">Autres catégories de revenus</h4>
<h5 class="spip">Bénéfices industriels et commerciaux</h5>
<p>L’article 7, paragraphe 1 dispose que les entreprises industrielles et commerciales sont imposables sur le territoire où se trouve un établissement stable.</p>
<h5 class="spip">Bénéfices des professions non commerciales et des revenus non commerciaux</h5>
<p>L’article 14, paragraphe 1 dispose que les revenus provenant de l’exercice d’une profession libérale ou d’autres activités indépendantes sont imposables dans l’Etat sur le territoire duquel se trouve l’installation fixe où s’exerce de façon régulière l’activité personnelle.</p>
<p>Les revenus que les professionnels du spectacle ainsi que les sportifs réalisent en cette qualité dans l’un des deux Etats, à titre indépendant, restent imposables dans l’Etat d’exercice de l’activité selon les dispositions de l’article 17-1 de la Convention.</p>
<p>L’article 12, paragraphe 1 pose en principe que les revenus non commerciaux (redevances et droits d’auteur) sont imposables dans l’Etat de résidence du bénéficiaire.</p>
<h5 class="spip">Revenus immobiliers</h5>
<p>L’article 6, paragraphe 1 dispose que les revenus des biens immobiliers y compris les bénéfices des exploitations agricoles sont imposables dans l’Etat où ils sont situés.</p>
<p>Cette règle s’applique également aux gains provenant de la cession ou de l’échange desdits biens ou droits selon les dispositions de l’article 13, paragraphe 1.</p>
<p>En ce qui concerne les gains provenant de l’aliénation de tous biens autres que mobiliers ou immobiliers, le paragraphe 4 de l’article 13 précise qu’ils restent imposables dans l’Etat de résidence du cédant.</p>
<h5 class="spip">Revenus de capitaux mobiliers</h5>
<p><strong>Les dividendes</strong></p>
<p>Ce terme désigne selon les dispositions de l’article 10, paragraphe 3, les revenus provenant d’actions, actions ou bons de jouissance, parts de mine, parts de fondateur ou autres parts à l’exception des créances, ainsi que les revenus d’autres parts sociales assimilés aux revenus d’actions.</p>
<p>De manière générale, l’article 10, paragraphe 1, reprend d’une part la règle suivant laquelle les dividendes payés par une société qui réside dans un Etat contractant à un résident de l’autre Etat sont imposables dans cet autre Etat.</p>
<p>D’autre part, le paragraphe 2 maintient le droit à l’Etat d’imposer les dividendes à la source aux sociétés résidentes qui les versent. L’Etat, où se situe la source des dividendes peut les imposer à un taux qui n’excède pas 15 %. Lorsque le bénéficiaire effectif est une société qui détient directement au moins 25 % du capital de la société distribuant les dividendes, l’Etat où se situe la source de ceux-ci prélève 5 %.</p>
<p>En outre, le paragraphe 5 prévoit qu’un résident hongrois peut demander le remboursement du précompte afférent aux dividendes distribués par une société française.</p>
<p><strong>Les intérêts</strong></p>
<p>L’article 11, paragraphe 2, définit ce terme comme désignant les revenus des créances de toute nature, les revenus des fonds publics et des obligations d’emprunts.</p>
<p>Le paragraphe 1 de l’article 11 précise que les intérêts provenant d’un Etat et versés à une personne résidente de l’autre Etat restent imposables dans cet autre Etat.</p>
<h5 class="spip">Imposition de la fortune</h5>
<p>L’article 23, paragraphe 1, dispose que la fortune constituée par des biens immobiliers est imposable dans l’Etat de situation de ces biens.</p>
<p>Au titre des biens mobiliers faisant partie de l’actif d’un établissement stable ou d’une base fixe, le paragraphe 2 prévoit l’imposition au lieu de situation de cet établissement ou de cette base.</p>
<p>Restent imposables au lieu de situation du siège de direction effective de l’entreprise selon le paragraphe 3 du même article, les éléments de fortune constitués par des navires, aéronefs ou véhicules routiers exploités en trafic international ainsi que les bateaux servant à la navigation intérieure.</p>
<p>Les autres éléments de la fortune restent imposables aux termes du paragraphe 4 dans l’Etat de résidence du bénéficiaire.</p>
<h3 class="spip"><a id="sommaire_4"></a>Elimination de la double imposition</h3>
<p>L’élimination de la double imposition pour les résidents de France qui perçoivent des revenus de source hongroise s’opère aux termes du paragraphe 2 de l’article 24 selon le régime de l’imputation.</p>
<p>Les revenus de source française ou hongroise pour lesquels le droit d’imposer est dévolu à titre exclusif à la Hongrie doivent être maintenus en dehors de la base de l’impôt français (article 24, paragraphe 2-a), réserve faite toutefois de leur prise en compte pour la détermination du taux effectif.</p>
<p>Le calcul du taux effectif consiste à calculer l’impôt sur les seuls revenus imposables en France en appliquant le taux d’imposition correspondant à l’ensemble des revenus de sources françaises et étrangères.</p>
<p>Au résultat ainsi obtenu peuvent être appliquées les réfactions prévues par la loi interne (crédit ou réduction d’impôt).</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/fiscalite/article/convention-fiscale). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
