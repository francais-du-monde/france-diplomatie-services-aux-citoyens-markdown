# Logement

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/vie-pratique/article/logement-111127#sommaire_1">Où se loger ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/vie-pratique/article/logement-111127#sommaire_2">Conditions de location</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/vie-pratique/article/logement-111127#sommaire_3">Hôtels</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/vie-pratique/article/logement-111127#sommaire_4">Electricité</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/vie-pratique/article/logement-111127#sommaire_5">Electroménager</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Où se loger ?</h3>
<p>Le marché locatif est bien pourvu à Téhéran. L’offre d’appartements et de villas est importante dans les quartiers résidentiels du nord de la ville, les plus agréables et les moins pollués. C’est dans la zone nord de Téhéran que logent la majorité des expatriés. Les logements sont le plus souvent meublés. L’offre de studios et de petits appartements (2/3 pièces) est quasi inexistante. Il est conseillé de faire appel à une agence immobilière. La commission de celle-ci est égale à un mois de loyer.</p>
<h3 class="spip"><a id="sommaire_2"></a>Conditions de location</h3>
<p>La durée des baux varie d’un à trois ans. Les locataires iraniens versent en général une forte somme en caution, dont les intérêts sont perçus par le propriétaire à titre de loyer. Les mécanismes classiques de paiement mensuel ou trimestriel pour solde de tout compte ne sont donc pas la norme en Iran. Les loyers sont très élevés et payés pour la plupart en devises, en liquide ou par virement bancaire sur un compte à l’étranger. Il est possible de régler de son loyer en €, en espèces, les modalités de paiement devant être fixées en accord avec le propriétaire. Le paiement est annuel, trimestriel ou semestriel. La durée des baux est renouvelable avec une augmentation annuelle qui peut atteindre 10 à 20%. La négociation et le respect du contrat de bail peuvent réserver des moments difficiles. Certains propriétaires refusent la location aux détenteurs d’animaux.</p>
<p>En général, les services chargés de l’eau et de l’électricité sont convenables. La pénurie d’eau en Iran a toutefois fait augmenter de 25 % son coût, voire jusqu’à 38 % en 2014. La plupart des quartiers de Téhéran sont soumis à des coupures d’eau pendant la période estivale. Certains immeubles sont équipés de pompes et de groupes électrogènes autonomes.</p>
<p><strong>Exemples de loyers mensuels moyens : </strong></p>
<p>Les appartements dans les quartiers résidentiels sont généralement meublés.</p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Appartement 1 chambre (entre 50 et 80 m²)</td>
<td>entre 1000 et 1500 €</td></tr>
<tr class="row_even even">
<td>Appartement 2 chambres (entre 80 et 250 m²)</td>
<td>entre 1000 et 4500 €</td></tr>
<tr class="row_odd odd">
<td>Appartement 3 chambres (entre 200 et 300 m²)</td>
<td>entre 1000 et 4500€</td></tr>
<tr class="row_even even">
<td>Appartement 4 chambres (entre 200 et 300 m²)</td>
<td>entre 1000 et 4500€</td></tr>
</tbody>
</table>
<p>Quelques appartements vides sont disponibles, plutôt en centre-ville :</p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Appartement 1 chambre (entre 50 et 70 m²)</td>
<td>entre 500 et 1000€</td></tr>
<tr class="row_even even">
<td>Appartement 2 chambres (entre 200 et 300 m²)</td>
<td>entre 500 et 1500€</td></tr>
<tr class="row_odd odd">
<td>Appartement 4 chambres (entre 200 et 300 m²)</td>
<td>entre 3000€ et 4000 €</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_3"></a>Hôtels</h3>
<p><strong>Téhéran</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Prix moyen d’une chambre d’hôtel (chambre double)</strong></td>
<td>rials</td>
<td>euros</td></tr>
<tr class="row_even even">
<td>Grand tourisme (5 étoiles)</td>
<td>4 150 000</td>
<td>105</td></tr>
<tr class="row_odd odd">
<td>Moyen tourisme (3 étoiles)</td>
<td>1 900 000</td>
<td>50</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_4"></a>Electricité</h3>
<p>Le courant est alternatif, 220 v, 50 hz. Les prises sont de type européen. Le coût mensuel moyen d’une maison de 200 m² revient à 2 500 000 de rials (soit environ 70 euros).</p>
<h3 class="spip"><a id="sommaire_5"></a>Electroménager</h3>
<p>Les cuisines sont le plus souvent équipées ; seuls les lave-vaisselles peuvent manquer. L’équipement électro-ménager est disponible sur place, à des prix supérieurs de 20 à 40% au prix français s’il est importé. Les équipements locaux sont moins chers, mais de qualité souvent médiocre. Le chauffage est au fioul, au gaz ou à l’électricité. La climatisation est indispensable durant les mois d’été. Le courant est alternatif, 220 v, 50 hz. Les prises sont de type européen.</p>
<p><i>Mise à jour : février 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/vie-pratique/article/logement-111127). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
