# Présence française

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/presence-francaise/article/presence-francaise#sommaire_1">Ambassade et consulat de France</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/presence-francaise/article/presence-francaise#sommaire_2">Autorités françaises dans le pays</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/presence-francaise/article/presence-francaise#sommaire_3">Associations dans le pays</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/presence-francaise/article/presence-francaise#sommaire_4">Associations franco-burkinabé</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Ambassade et consulat de France</h3>
<p>Consulter notre <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/article/annuaire-des-representations-francaises-a-l-etranger" class="spip_in">annuaire des ambassades et consulats de France à l’étranger</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Autorités françaises dans le pays</h3>
<p><strong>Réseau français de coopération et d’action culturelle</strong></p>
<p>Les coordonnées des services du réseau de coopération et d’action culturelle français au Burkina Faso ainsi que la description de l’activité de ces services se trouvent dans la rubrique culture du site internet de l’<a href="http://www.ambafrance-bf.org/" class="spip_out" rel="external">Ambassade</a> ou sur le site internet du réseau culturel : <a href="http://latitudefrance.diplomatie.gouv.fr/AnnuaireCarto.html" class="spip_out" rel="external">Latitude France</a>.</p>
<p><strong>Economie : réseau mondial</strong></p>
<p>Business France, l’Agence française pour le développement international des entreprises, <a href="http://export.businessfrance.fr/burkina-faso/export-burkina-faso-avec-notre-bureau.html" class="spip_out" rel="external">est présente au Burkina Faso</a>. Sa mission est d’accompagner les entreprises françaises dans leur développement à l’international.</p>
<p>Les services économiques sont également présents. Ils sont une émanation de la Direction générale du Trésor et ont pour missions principales l’analyse macroéconomique et l’animation des relations économiques bilatérales entre la France et les pays où ils sont implantés.</p>
<p><strong>Vos élus à l’Assemblée des Français de l’étranger</strong></p>
<p>Pour toute information sur l’Assemblée des Français de l’étranger et pour connaître les conseillers et les Sénateurs qui représentent les Français établis hors de France dans votre circonscription, vous pouvez consulter les sites Internet suivants :</p>
<ul class="spip">
<li><a href="http://www.assemblee-afe.fr/" class="spip_out" rel="external">Assemblée des Français de l’étranger (AFE)</a></li>
<li>Sénat au service des Français de l’étranger : <a href="http://www.senat.fr/expatries/dossiers_pays/burkina_faso.html" class="spip_out" rel="external">dossier spécifique au Burkina Faso</a> sur le site du Sénat</li></ul>
<p><strong>Députés des Français de l’étranger</strong></p>
<p>La révision constitutionnelle du 23 juillet 2008, publiée au Journal officiel du 19 avril 2011, introduit la représentation à l’Assemblée nationale des Français établis hors de France. La création de onze circonscriptions législatives à l’étranger permet aux Français expatriés d’élire leur député à l’Assemblée nationale à partir du scrutin de 2012.</p>
<p>Pour plus d’information :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.votezaletranger.gouv.fr/" class="spip_out" rel="external">Votez à l’étranger</a> </p>
<h3 class="spip"><a id="sommaire_3"></a>Associations dans le pays</h3>
<p><strong>Associations françaises</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.francais-du-monde.org/site-section/burkina-faso/" class="spip_out" rel="external">Association Démocratique des Français du Burkina (ADFB – Section locale de l’ADFE)</a><br class="manualbr">Téléphone : +226 70 21 72 58<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/presence-francaise/article/presence-francaise#contact.adfb#mc#gmail.com#" title="contact.adfb..åt..gmail.com" onclick="location.href=mc_lancerlien('contact.adfb','gmail.com'); return false;" class="spip_mail">Courriel</a></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Union des Français de l’étranger (UFE)</strong><br class="manualbr">01 BP 6555 Ouagadougou 01<br class="manualbr">Tél. : (226) 50 31 58 98</p>
<h3 class="spip"><a id="sommaire_4"></a>Associations franco-burkinabé</h3>
<p><strong>Association des Familles Franco-Burkinabé </strong><br class="manualbr">BP 2782 <br class="manualbr">Ouagadougou 01 <br class="manualbr">Tél : (226) 50 34 01 74</p>
<p><strong>France-Solidarité </strong><br class="manualbr">BP 9262 <br class="manualbr">Ouagadougou 06 <br class="manualbr">Tél : (226) 50 20 36 31</p>
<p><strong>Club des hommes d’affaires franco-burkinabé </strong><br class="manualbr">Mission économique <br class="manualbr">Ambassade de France au Burkina <br class="manualbr">01 BP 4382 Ouagadougou <br class="manualbr">Tél : (226) 50 30 67 74 <br class="manualbr">Fax : (226) 50 31 32 81 <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/presence-francaise/article/presence-francaise#chafb#mc#liptinfor.bf#" title="chafb..åt..liptinfor.bf" onclick="location.href=mc_lancerlien('chafb','liptinfor.bf'); return false;" class="spip_mail">Courriel</a></p>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li>Liste des associations françaises maintenue à jour par le <a href="http://www.ambafrance-bf.org/Associations" class="spip_out" rel="external">consulat de France à Ouagadougou</a></li>
<li>Notre article <a href="services-aux-citoyens-preparer-son-expatriation-en-savoir-plus-article-associations-des-francais-de-l-etranger.md" class="spip_in">Associations des Français de l’étranger</a></li></ul>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/presence-francaise/article/presence-francaise). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
