# Stages

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/emploi-stage/article/stages#sommaire_1">Stage professionnel</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/emploi-stage/article/stages#sommaire_2">Stage d’étude</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/emploi-stage/article/stages#sommaire_3">Volontariat</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/emploi-stage/article/stages#sommaire_4">Volontariat de service civique</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/emploi-stage/article/stages#sommaire_5">Service volontaire européen – SVE</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/emploi-stage/article/stages#sommaire_6">Le volontariat de solidarité internationale – VSI</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Stage professionnel</h3>
<p>Pour des offres de stages, il convient de consulter le site de la <a href="http://www.ccifh.hu/" class="spip_out" rel="external">Chambre de commerce et d’industrie franco-hongroise</a>, qui sera aussi en mesure de faciliter vos contacts avec les entreprises françaises intéressées par des stagiaires.</p>
<h3 class="spip"><a id="sommaire_2"></a>Stage d’étude</h3>
<p>Pour effectuer un stage dans les services de l’ambassade de France à Budapest, il convient de s’inscrire sur le site du <a href="http://www.diplomatie.gouv.fr/fr/emplois-stages-concours/stagiaires/" class="spip_in">ministère des Affaires étrangères</a>.</p>
<p>Pour effectuer un stage au sein d’une <a href="http://af.org.hu/" class="spip_out" rel="external">Alliance française de Hongrie</a>, il convient de prendre contact directement avec ses établissements.</p>
<h3 class="spip"><a id="sommaire_3"></a>Volontariat</h3>
<h4 class="spip">Volontariat international en entreprise - V.I.E.</h4>
<p>Le V.I.E. permet aux entreprises françaises de confier à un jeune ressortissant de l’espace économique européen, âgé de 18 à 28 ans, une mission professionnelle (rémunérée) à l’étranger durant une période modulable de 6 à 24 mois.</p>
<p><strong>Dans quelles conditions ?</strong></p>
<p>Le volontaire international est rémunéré, entre 1200 et 3000 euros nets par mois, selon le pays. Tous les métiers sont concernés : finances, marketing, commerce international, contrôle de gestion, comptabilité, mécanique, électronique, télécommunications, informatique, BTP, agronomie, tourisme, droit, ressources humaines…</p>
<h4 class="spip">Le volontariat international en administration – VIA</h4>
<p>Ambassades, alliances françaises, instituts de recherche, missions économiques… Le volontariat international en administration (VIA) permet de partir travailler à l’étranger pour des services de l’État français.</p>
<p><strong>Dans quelles conditions ?</strong></p>
<p>La personne en VIA perçoit des allocations équivalant à une indemnisation comprise entre 1200 € et 2800 € par mois, suivant le pays d’affectation. Les missions durent entre 6 et 24 mois.</p>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/emplois-stages-concours/volontaires-internationaux/" class="spip_in">Volontaires internationaux</a></li>
<li><a href="http://www.civiweb.com/FR/index.aspx" class="spip_out" rel="external">Civiweb.com</a></li>
<li><a href="http://www.businessfrance.fr/" class="spip_out" rel="external">Business France.fr</a> - <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/emploi-stage/article/stages#infoVIE#mc#ubifrance.fr#" title="infoVIE..åt..ubifrance.fr" onclick="location.href=mc_lancerlien('infoVIE','ubifrance.fr'); return false;" class="spip_mail">Courriel</a></li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Volontariat de service civique</h3>
<p>Toute personne âgée de plus de 25 ans peut effectuer un volontariat de service civique pour mener à bien, sur des périodes de 6 à 24 mois des missions d’intérêt général auprès d’associations, de fondations reconnues d’utilité publique ou, dans les départements et collectivités d’outre-mer, de personnes morales de droit public. Les volontaires bénéficient d’une indemnisation et d’une couverture sociale complète financées par l’organisme d’accueil.</p>
<h3 class="spip"><a id="sommaire_5"></a>Service volontaire européen – SVE</h3>
<p>Programme de l’Union européenne, le SVE est destiné à encourager la mobilité des jeunes de 18 à 30 ans.</p>
<p>Il leur permet de se mettre au service d’un projet d’intérêt général à l’étranger durant 2 à 12 mois.</p>
<p>Le SVE offre une expérience formatrice, développe la citoyenneté, permet de faire preuve de solidarité, de découvrir une autre culture, une autre langue, etc.</p>
<p><strong>Dans quelles conditions ?</strong></p>
<p>Le volontaire relève d’un statut particulier. Il bénéficie d’une prise en charge totale sur place et d’une indemnité dont le montant varie selon les pays.</p>
<p>L’expérience acquise lors du SVE est reconnue par un certificat de SVE (évaluation de l’activité volontaire et mention des compétences acquises).</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.jeunesseenaction.fr/" class="spip_out" rel="external">Jeunesseenaction.fr</a></p>
<h3 class="spip"><a id="sommaire_6"></a>Le volontariat de solidarité internationale – VSI</h3>
<p>Le volontariat de solidarité internationale (VSI) a pour objet l’accomplissement à temps plein d’une mission d’intérêt général dans les pays en voie de développement dans les domaines de la coopération au développement et de l’action humanitaire. Toute personne majeure sans activité professionnelle peut effectuer un VSI.</p>
<p><strong>Dans quelles conditions ?</strong></p>
<p>Les missions sont obligatoirement effectuées auprès d’associations internationales agréées par le ministre des affaires étrangères. Une indemnité comprise entre 106,06 € et 710,10 € est versée, hors prise en charge du transport, du logement et de la nourriture, auquel s’ajoute le montant de l’indemnité supplémentaire liée à l’affectation à l’étranger (montant variable selon les pays).</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.france-volontaires.org/" class="spip_out" rel="external">France-volontaires.org</a></p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/emploi-stage/article/stages). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
