# Entrée et séjour

<h2 class="rub23471">Passeport, visa, permis de travail</h2>
<p>Quel que soit le motif de votre séjour, renseignez-vous auprès de la section consulaire de l’ambassade de Suède à Paris qui vous informera sur la règlementation en matière d’entrée et de séjour en Suède, règlementation que vous devrez impérativement respecter. N’hésitez pas à consulter le site de <a href="http://www.migrationsverket.se/Privatpersoner.html" class="spip_out" rel="external">l’Office des migrations (<i>Migrationsverket</i>)</a> afin de vous familiariser avec les modalités d’entrée et de séjour dans le pays.</p>
<p>Une fois sur place ce sont les autorités suédoises qui prennent le relais.</p>
<p><strong>En aucun cas, vous n’êtes autorisé à travailler en Suède sans permis adéquat. </strong></p>
<p><strong>Séjourner en Suède</strong></p>
<p>De manière générale, pour toute information relative aux conditions de séjour en Suède, il convient de contacter la <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-etrangers-en-france/article/annuaire-des-representations-etrangeres-en-france" class="spip_in">section consulaire de l’Ambassade de Suède à Paris</a>. Les informations suivantes sont données <strong>à titre indicatif</strong>.</p>
<p>Les ressortissants de l’UE et de l’EEE ainsi que leur famille qui souhaitent séjourner plus de trois mois en Suède doivent s’enregistrer, au plus tard dans les trois mois de leur arrivée en Suède, auprès de l’<strong>Office des migrations</strong> (<i>Migrationsverket</i>). Ils n’ont plus besoin de permis de séjour.</p>
<p>Suite à cette démarche, <i>Migrationsverket</i> délivre une attestation d’enregistrement, qui devra être présentée pour l’octroi du numéro d’identification personnel (<i>personnummer</i> ; voir ci-après).</p>
<p>Après cinq ans de résidence, les ressortissants européens peuvent obtenir un droit permanent de résidence en Suède sous certaines conditions.</p>
<p>Pour les citoyens d’autres pays, le permis de séjour est obligatoire et doit être obtenu avant l’arrivée en Suède.</p>
<p><strong>Travailler en Suède</strong></p>
<p>Au sein de l’Union européenne, vous pouvez, en tant que ressortissant d’un Etat membre, travailler librement dès votre arrivée en Suède sans attendre d’être enregistré auprès de <i>Migrationsverket</i>. Pour un Français qui souhaite s’installer en Suède, l’enregistrement auprès du <i>Migrationsverket</i> vaut permis de travail.</p>
<p>Vous avez le droit de vous rendre en Suède et être demandeur d’emploi. Vous devrez cependant justifier de ressources suffisantes pour subvenir à vos besoins pendant cette période de recherche d’emploi. A partir de six mois, l’Office des migrations pourra également vous demander de prouver que vous avez cherché activement un emploi et que vous avez toutes les chances d’en obtenir un.</p>
<p>Le conjoint/concubin devra également effectuer une demande d’enregistrement puis de numéro d’identification personnel.</p>
<p><strong>Numéro d’identification personnel</strong></p>
<p>Dès votre arrivée en Suède, il est indispensable d’obtenir un numéro d’identification personnel (<i>personnummer)</i>, obligatoire pour entreprendre toutes démarches telles que l’accès aux soins médicaux, l’ouverture d’un compte bancaire, l’ouverture d’une ligne téléphonique, la souscription d’un contrat d’assurance…</p>
<p>Le <i>personnumer</i> est un nombre composé de 10 chiffres, les six premiers chiffres sont ceux de votre date de naissance (Année/Jours/Mois), les quatre autres n’ont aucune signification particulière (ils sont aléatoires). Chaque membre de la famille possède son propre numéro.</p>
<p>Vous devez prendre l’attache du centre de taxation (<i>Skatteverket</i>) de votre lieu de résidence et présenter votre passeport + livret de famille +attestation d’enregistrement.</p>
<p>Votre centre de taxation vous transmettra par voie postale votre <i>personnummer</i> sous deux semaines si votre dossier est complet.</p>
<p>Pour bénéficier de la sécurité sociale suédoise, il vous faut contacter <i>Försäkringskassan</i>.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.swedenabroad.com/fr-FR/Embassies/Paris/" class="spip_out" rel="external">Section consulaire de l’ambassade de Suède à Paris</a></li>
<li><a href="http://www.migrationsverket.se/Privatpersoner.html" class="spip_out" rel="external">Migrationsverket</a> <br class="manualbr">Tél. : +46 771-235235</li>
<li><a href="http://www.skatteverket.se/" class="spip_out" rel="external">Skatteverket</a><br class="manualbr">Tél. : +46 771-567567</li></ul>
<p><i>Mise à jour : juin 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-suede-entree-et-sejour-article-animaux-domestiques.md" title="Animaux domestiques">Animaux domestiques</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-suede-entree-et-sejour-article-vaccination-113518.md" title="Vaccination">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-suede-entree-et-sejour-article-demenagement-113517.md" title="Déménagement">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-suede-entree-et-sejour-article-passeport-visa-permis-de-travail-113516.md" title="Passeport, visa, permis de travail">Passeport, visa, permis de travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/entree-et-sejour/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
