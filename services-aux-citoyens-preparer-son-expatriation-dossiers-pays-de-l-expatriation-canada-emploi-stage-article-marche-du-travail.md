# Marché du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/emploi-stage/article/marche-du-travail#sommaire_1">Secteurs à fort potentiel</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/emploi-stage/article/marche-du-travail#sommaire_2">Secteurs à faible potentiel</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/emploi-stage/article/marche-du-travail#sommaire_3">Professions règlementées</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/emploi-stage/article/marche-du-travail#sommaire_4">Rémunération</a></li></ul>
<p><strong>Particularité du Québec :</strong></p>
<p>Chaque année, plus de 3 000 Français en moyenne immigrent au Québec. Plusieurs autres milliers viennent y suivre des études ou y travailler temporairement. On recense également environ 14 000 jeunes qui y séjournent via les accords de mobilité (dont 7 000 PVT). Près de neuf Français sur dix choisissent Montréal.</p>
<p>Contrairement à certaines idées reçues, il n’est pas facile d’obtenir rapidement un emploi au Québec. Il est donc impératif de posséder un petit capital afin de pouvoir vivre sur place pendant environ six mois. La maîtrise de l’anglais s’avère par ailleurs de plus en plus importante à Montréal pour la recherche d’un emploi, du fait de la proximité avec les États-Unis.</p>
<p>Attention : S’il est vrai que le gouvernement du Québec privilégie l’immigration française, le Québec étant une Amérique en français, il n’est en aucune façon la France en Amérique.</p>
<p>Si le Québec a opté pour le libéralisme, il a cependant renoncé à financer un système de protection sociale tel que les Français le connaissent. Le système de santé de la Belle province n’offre que le strict minimum à ses assurés, et les congés payés sont le plus souvent limités à deux semaines par an. En revanche, les employeurs québécois ont su créer une relation privilégiée avec leurs employés qui permet à tout un chacun de travailler sans stress.</p>
<h3 class="spip"><a id="sommaire_1"></a>Secteurs à fort potentiel</h3>
<p>Au Canada, l’emploi en février 2010 s’est accru dans l’hébergement et les services de restauration, dans le secteur des services aux entreprises et des services relatifs aux bâtiments et autres services de soutien. L’emploi a aussi augmenté dans le secteur de la fabrication, dans les soins de santé et de l’assistance sociale, ainsi que dans les ressources naturelles.</p>
<p><strong>Principaux secteurs porteurs</strong></p>
<ul class="spip">
<li>Industrie aérospatiale</li>
<li>Industrie agroalimentaire</li>
<li>Industrie automobile</li>
<li>Industrie chimique</li>
<li>Technologies de l’information et des communications (TIC)</li>
<li>Pétrole et gaz</li>
<li>Biotechnologie</li>
<li>Santé et soins à la personne</li>
<li>Appareils médicaux</li>
<li>Industrie pharmaceutique</li>
<li>Matières plastiques</li></ul>
<p>En plus de la Province de Québec, le Canada compte d’autres communautés francophones dynamiques qui restent peu ou mal connues : les Franco-ontariens (Ottawa, Toronto), les francophones du Manitoba, de la Saskatchewan et les Acadiens du Nouveau Brunswick. Les possibilités d’emplois y sont réelles.</p>
<p><strong>Secteurs porteurs par province - Alberta</strong></p>
<ul class="spip">
<li>la métallurgie,</li>
<li>le bâtiment,</li>
<li>l’exploitation du pétrole et du gaz (techniciens et ouvriers qualifiés),</li>
<li>l’automobile,</li>
<li>la plomberie,</li>
<li>l’architecture,</li>
<li>l’ingénierie mécanique et</li>
<li>la restauration.</li></ul>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li><a href="http://alberta.ca/home/" class="spip_out" rel="external">Gouvernement de l’Alberta</a></li>
<li><a href="http://www.lecdea.ca/" class="spip_out" rel="external">Conseil de développement économique de l’Alberta</a></li>
<li><a href="http://www.wd.gc.ca/" class="spip_out" rel="external">Diversification de l’économie de l’Ouest Canada</a></li></ul>
<p><strong>Secteurs porteurs par province - Québec</strong></p>
<p>Dans l’ensemble du Québec, les professions les plus en demande actuellement sont :</p>
<ul class="spip">
<li>ingénieur</li>
<li>informaticien</li>
<li>analyste financier</li>
<li>machiniste</li>
<li>soudeur</li></ul>
<p><a href="http://emploiquebec.net/index.asp" class="spip_out" rel="external">Liste</a> des professions où il existe actuellement es possibilités d’emploi.</p>
<p><strong>Secteurs porteurs par province - Manitoba</strong></p>
<ul class="spip">
<li>les communications,</li>
<li>le marketing,</li>
<li>les NTIC,</li>
<li>l’industrie,</li>
<li>l’alimentation,</li>
<li>l’agriculture,</li>
<li>la comptabilité,</li>
<li>la construction (charpentiers et menuisiers),</li>
<li>la restauration,</li>
<li>la mécanique automobile,</li>
<li>les transports routiers,</li>
<li>l’administration et l’éducation (petite enfance et maternelle).</li></ul>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li><a href="http://www.gov.mb.ca/" class="spip_out" rel="external">Gouvernement du Manitoba</a></li>
<li><a href="http://www.cdem.com/fr" class="spip_out" rel="external">Conseil de développement économique des municipalités bilingues du Manitoba</a></li>
<li><a href="http://www.animcanada.com/" class="spip_out" rel="external">Agence nationale et internationale du Manitoba</a></li></ul>
<p><strong>Secteurs porteurs par province - Ontario</strong></p>
<ul class="spip">
<li>Aérospatiale</li>
<li>Biotechnologie en agriculture, foresterie, pêcherie et pharmaceutique</li>
<li>Apprentissage à distance</li>
<li>Environnement</li>
<li>Technologies de l’énergie verte</li>
<li>Informatique de la santé</li>
<li>Multimédia, surtout pour les communications mobiles</li></ul>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li><a href="http://www.gov.on.ca/" class="spip_out" rel="external">Gouvernement de l’Ontario</a></li>
<li><a href="http://www.ontarioimmigration.ca/" class="spip_out" rel="external">Immigration en Ontario</a></li>
<li><a href="http://www.rdee-ont.ca/" class="spip_out" rel="external">Réseau de développement économique et d’employabilité de l’Ontario</a></li></ul>
<p><strong>Secteurs porteurs par province - Nouvelle Ecosse</strong></p>
<ul class="spip">
<li>l’enseignement,</li>
<li>la santé,</li>
<li>l’industrie,</li>
<li>l’aérospatiale,</li>
<li>les technologies de l’information</li>
<li>la comptabilité.</li></ul>
<p><strong>Secteurs porteurs par province - Colombie Britannique</strong></p>
<ul class="spip">
<li>l’enseignement,</li>
<li>la biotechnologie,</li>
<li>les technologies de l’information et de la communication,</li>
<li>l’industrie aérospatiale,</li>
<li>le bâtiment et la construction.</li></ul>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li><a href="http://www.sdecb.com/" class="spip_out" rel="external">Société de développement économique de la Colombie britannique</a></li>
<li><a href="http://www.deo.gc.ca/" class="spip_out" rel="external">Diversification de l’économie de l’Ouest Canada</a></li></ul>
<p><strong>Secteurs porteurs par province - Nouveau Brunswick</strong></p>
<ul class="spip">
<li>l’ingénierie informatique,</li>
<li>la programmation et le développement des médias interactifs,</li>
<li>les métiers de la santé,</li>
<li>l’enseignement universitaire</li>
<li>le travail social.</li></ul>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.gnb.ca/" class="spip_out" rel="external">Gouvernement du Nouveau Brunswick</a></p>
<p><strong>Secteurs porteurs par province - Ile-du-Prince-Edouard</strong></p>
<ul class="spip">
<li>l’aéronautique,</li>
<li>l’ingénierie travaux publics,</li>
<li>le bâtiment, l’électricité,</li>
<li>la mécanique,</li>
<li>le travail social,</li>
<li>la santé,</li>
<li>l’informatique et les métiers de l’habitat.</li></ul>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.gov.pe.ca/" class="spip_out" rel="external">Gouvernement de l’Ile-du-Prince-Edouard</a></p>
<p><strong>Secteurs porteurs par province - Saskatchewan</strong></p>
<ul class="spip">
<li>la santé,</li>
<li>les transports routiers,</li>
<li>la mécanique industrielle</li>
<li>l’électricité.</li></ul>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.gov.sk.ca/" class="spip_out" rel="external">Gouvernement de Saskatchewan</a></p>
<p><strong>Secteurs porteurs par province - Yukon</strong></p>
<ul class="spip">
<li>la santé,</li>
<li>le travail social</li>
<li>l’exploitation minière,</li>
<li>le tourisme,</li>
<li>la construction,</li>
<li>la restauration et l’hôtellerie,</li>
<li>l’éducation</li>
<li>l’administration.</li></ul>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.gov.yk.ca/" class="spip_out" rel="external">Gouvernement du Yukon</a></p>
<h3 class="spip"><a id="sommaire_2"></a>Secteurs à faible potentiel</h3>
<p>Des reculs ont été observés notamment dans les commerces de détail et de gros, dans le secteur de la finance, des assurances, de l’immobilier et de la location.</p>
<h3 class="spip"><a id="sommaire_3"></a>Professions règlementées</h3>
<p>Selon un rapport de l’Organisation de coopération et de développement économiques (OCDE), la diminution de la réglementation des professions est l’un des cinq principaux moyens d’accroître la future prospérité du Canada. D’après l’OCDE, le Canada est l’un des pays où les régimes de réglementation des professions sont les plus lourds, ce qui peut influer sur le niveau d’innovation et la productivité.</p>
<p>Le Canada offre de nombreuses possibilités d’emploi et connait une pénurie de main d’œuvre qualifiée et de techniciens, mais aussi de médecins généralistes. Paradoxalement, certaines professions (médecins, infirmiers, pharmaciens, enseignants, ingénieurs, juristes…) sont réglementées par des Ordres professionnels et ne sont donc pas accessibles aux étrangers et nouveaux immigrants.</p>
<p>L’Accord fédéral sur le commerce intérieur (ACI) qui a pour but de faciliter la libre circulation des personnes, des produits et des services à l’intérieur du Canada, stipule que tout travailleur compétent pour exercer un métier ou une profession dans une province ou un territoire aura accès au même l’emploi dans le reste du Canada dès lors que la compétence professionnelle est mutuellement reconnue. Ceci revêt une importance particulière pour les professions libérales (médecin, ingénieur, etc.) et les métiers réglementés (machiniste, mécanicien industriel, etc.).</p>
<p>Il est donc essentiel de bien s’informer et de préparer son séjour professionnel. Il pourra être demandé au candidat de suivre une formation, réussir un examen professionnel ou encore obtenir un diplôme. <strong>Pour gagner du temps, il est vivement recommandé de procéder à la reconnaissance de ses diplômes avant de se rendre au Canada, en s’informant auprès de l’Ambassade du Canada à Paris et de faire traduire en anglais ses diplômes et lettres de recommandation professionnelle.</strong></p>
<p><strong>Au Québec : </strong></p>
<p>Selon l’Office des Professions du Québec, le Québec compte 45 ordres professionnels qui regroupent environ 300 000 adhérents. Le rôle de ces ordres est de "s’assurer, dans le domaine qui leur est propre, que les professionnels dispensent les meilleurs services possibles au public ".</p>
<p>Dans les faits, de nombreuses professions se protègent derrière des barrières corporatistes. Même si, par exemple, le Québec manque de médecins et d’infirmières, l’Ordre des médecins interdit aux confrères étrangers de pratiquer. Toutefois, depuis 2005, le Gouvernement du Québec a conclu des ententes avec une vingtaine d’ordres professionnels en vue de faciliter les démarches de reconnaissance des diplômes et des compétences des personnes immigrantes.</p>
<p>A noter que l’Entente France-Québec de reconnaissance mutuelle des qualifications professionnelles est en vigueur depuis le 17 octobre 2008, date de sa signature.</p>
<p>Pour en savoir plus sur les conséquences pratiques de cette Entente, vous pouvez consulter le site Internet du <a href="http://www.consulfrance-montreal.org/" class="spip_out" rel="external">consulat général de France à Montréal</a>.</p>
<p>L’ensemble des projets réalisés par le ministère de l’Immigration et des Communautés culturelles (MICC) sont disponibles <a href="http://www.immigration-quebec.gouv.qc.ca/fr/index.php" class="spip_out" rel="external">en ligne</a>. Il est, par conséquent, impératif de contacter, avant le départ de France, l<a href="http://www.opq.gouv.qc.ca/" class="spip_out" rel="external">’ordre des professions du Québec (OPQ)</a> et de consulter la rubrique relative à la validation des acquis sur le site du MICC.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.bureaudelaconcurrence.gc.ca/" class="spip_out" rel="external">Bureau de la Concurrence du Gouvernement du Canada</a></li>
<li><a href="http://www.workingincanada.gc.ca/" class="spip_out" rel="external">Évaluation des compétences et des qualifications professionnelles : Travailler au Canada</a></li>
<li><a href="http://www.cic.gc.ca/" class="spip_out" rel="external">Citoyenneté et Immigration Canada</a>.</li>
<li><a href="http://www.cicdi.ca/" class="spip_out" rel="external">Centre d’information sur les diplômes internationaux</a>. Ce site recense les documents nécessaires à la préparation au marché de l’emploi canadien.</li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Rémunération</h3>
<p>Le salaire horaire brut minimum fixé par les Provinces et les Territoires est en moyenne de 8 dollars canadiens. En ce qui concerne les entreprises fédérales, le taux horaire est fixé par le Code canadien du travail et avoisine également 8 CAD.Les salaires sont habituellement indiqués en taux horaire ou en montant annuel. Les bulletins de salaire sont généralement bimensuels.</p>
<p><strong>Salaire horaire moyen en CAD</strong></p>
<table class="spip">
<thead><tr class="row_first"><th id="id23d5_c0"> </th><th id="id23d5_c1">Canada</th><th id="id23d5_c2">Ontario</th><th id="id23d5_c3">Québec</th><th id="id23d5_c4">Colombie britannique</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<th headers="id23d5_c0" id="id23d5_l0">Homme</th>
<td class="numeric virgule" headers="id23d5_c1 id23d5_l0">24,52</td>
<td class="numeric virgule" headers="id23d5_c2 id23d5_l0">25,25</td>
<td class="numeric virgule" headers="id23d5_c3 id23d5_l0">22,42</td>
<td class="numeric virgule" headers="id23d5_c4 id23d5_l0">25,32</td></tr>
<tr class="row_even even">
<th headers="id23d5_c0" id="id23d5_l1">Femme</th>
<td class="numeric virgule" headers="id23d5_c1 id23d5_l1">21,09</td>
<td class="numeric virgule" headers="id23d5_c2 id23d5_l1">21,86</td>
<td class="numeric virgule" headers="id23d5_c3 id23d5_l1">20,24</td>
<td class="numeric virgule" headers="id23d5_c4 id23d5_l1">20,88</td></tr>
</tbody>
</table>
<p><i>Mise à jour : avril 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/emploi-stage/article/marche-du-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
