# Passeport, visa, permis de travail

<p>Quel que soit le motif de votre séjour, renseignez-vous auprès de la section consulaire de l’ambassade de Slovénie à Paris qui vous informera sur la règlementation en matière d’entrée et de séjour en Slovénie, règlementation que vous devrez impérativement respecter. N’hésitez pas à consulter le site ministère de l’Intérieur slovène afin de vous familiariser avec les modalités d’entrée et de séjour dans le pays.</p>
<p>Une fois sur place c’est le ministère des Affaires intérieures qui prend le relais. <strong>En aucun cas, vous n’êtes autorisé à travailler en Slovénie sans permis adéquat. </strong></p>
<p>La section consulaire de l’ambassade de France en Slovénie n’est pas l’organisme compétent pour répondre à vos demandes d’information concernant votre séjour en Slovénie.</p>
<p>La Slovénie est membre de l’Union européenne depuis le <strong>1er mai 2004</strong>. Elle a rejoint l’espace Schengen le <strong>21 décembre 2007</strong>. En conséquence, le contrôle des voyageurs en provenance des autres pays de l’espace Schengen a été supprimé aux frontières terrestres de la Slovénie depuis cette date.</p>
<p>Les conditions d’entrée et de séjour en Slovénie sont définies dans la loi sur les étrangers (<i>Aliens Act</i>) n°107/2006.</p>
<p>Les ressortissants français ne sont pas soumis au visa quelle que soit la durée de leur séjour en Slovénie. En revanche, ils doivent obligatoirement être en possession <strong>d’un passeport ou d’une carte nationale d’identité en cours de validité</strong>. Ces conditions s’appliquent également aux mineurs (un simple livret de famille ne suffit donc pas).</p>
<p>Les cartes nationales d’identité délivrées entre le 1er janvier 2004 et le 31 décembre 2013 seront encore valables cinq ans après la date de fin de validité indiquée au verso, mais aucune modification matérielle de la carte plastifiée n’en attestera.</p>
<p>En conséquence, de façon à éviter tout désagrément pendant votre voyage, il vous est fortement recommandé de privilégier l’utilisation d’un passeport valide à une CNI portant une date de fin de validité dépassée, même si elle est considérée par les autorités françaises comme étant toujours en cours de validité.</p>
<p>Si vous voyagez uniquement avec votre carte nationale d’identité, vous pouvez télécharger et imprimer <a href="http://media.interieur.gouv.fr/interieur/cni-15ans/document-a-telecharger-slovenie.pdf" class="spip_out" rel="external">une notice multilingue</a> expliquant ces nouvelles règles.</p>
<p>Dans certains cas, la législation slovène peut prévoir de lourdes amendes pour tout ressortissant européen qui voyagerait avec un titre d’identité ou de voyage périmé et une amende allant de 250 à 500 € en cas d’absence totale de document d’identité. Ces sanctions sont effectivement appliquées.</p>
<h4 class="spip">Séjours de moins de trois mois</h4>
<p>Un passeport ou une carte nationale d’identité en cours de validité suffit pour entrer en Slovénie et y séjourner pour une durée n’excédant pas trois mois, quelle que soit la raison du séjour. Pour un séjour n’excédant pas trois mois, il n’est pas nécessaire d’être en possession d’un visa, ni de solliciter un permis de séjour auprès des autorités slovènes.</p>
<p><strong>Formalités de séjour en Slovénie :</strong></p>
<ul class="spip">
<li><i>Séjour à l’hôtel ou au camping</i> : la structure d’accueil se charge des formalités de déclaration du séjour temporaire auprès des autorités slovènes ;</li>
<li><i>Séjour chez des amis, de la famille, pour une durée inférieure à 90 jours</i> : il est <strong>obligatoire</strong> de signaler sa présence au poste de police le plus proche dans un délai de trois jours à compter de son arrivée en Slovénie. Au-delà, le contrevenant s’expose à une contravention de 400 euros, sanction de plus en plus appliquée. L’hébergeant doit accompagner le voyageur au poste de police pour effectuer cet enregistrement. <strong>Il est également obligatoire de signaler au poste de police tout changement d’adresse ainsi que son départ définitif du pays.</strong></li></ul>
<h4 class="spip">Séjours de plus de trois mois</h4>
<p>Le titre de séjour en Slovénie peut être refusé aux ressortissants des États membres de l’Union européenne s’ils représentent un danger pour l’ordre, la tranquillité et la sécurité publics ou s’ils constituent une charge financière pour l’État.</p>
<p><strong>Certificat d’enregistrement</strong> (<i>registration certificate</i>)</p>
<p><strong>Avant l’expiration de la période de trois mois</strong> pendant laquelle vous êtes autorisé à séjourner en Slovénie sans visa, ni permis de séjour, vous devrez déposer une demande de certificat d’enregistrement auprès de l’unité administrative compétente à raison de votre domicile.</p>
<p>Vous pouvez séjourner en Slovénie pour les motifs suivants : pour y occuper un emploi salarié ou travailler à votre compte, en qualité de prestataire de services, pour y poursuivre des études, pour regroupement familial ou pour tout autre motif. Les documents à présenter seront fonction du motif de votre séjour.</p>
<p>Les documents à présenter (liste non exhaustive) sont les suivants :</p>
<ul class="spip">
<li>un document d’identité en cours de validité (passeport ou carte nationale d’identité) ;</li>
<li>en cas d’activité salariée, une attestation de votre employeur confirmant qu’il a l’intention de vous embaucher ou bien qu’il vous emploie déjà ;</li>
<li>si vous séjournez en Slovénie pour études, en qualité de prestataire de services ou pour y passer votre retraite, tout document attestant du motif de votre séjour et de votre situation ;</li>
<li>pour les étudiants et pour les personnes résidant en Slovénie pour tout autre motif, des justificatifs de revenus attestant que vous êtes en mesure de subvenir à vos besoins ;</li>
<li>pour les étudiants et pour les personnes résidant en Slovénie pour tout autre motif, une attestation d’assurance maladie obligatoire. Le certificat d’enregistrement est délivré, sous la forme d’une carte, pour une durée de cinq ans ou pour la durée de l’activité si celle-ci est inférieure à cinq ans. Il est ensuite renouvelable. Son coût s’élève à 105 points (la valeur du point est variable).</li></ul>
<p>Une fois en possession de ce certificat, vous devrez faire enregistrer votre résidence auprès de l’autorité compétente.</p>
<p><strong>Permis de travail</strong></p>
<p>Les ressortissants des Etats membres de l’Union européenne, des pays de l’Espace économique européen et de la Suisse bénéficient du <strong>libre accès au marché du travail slovène</strong>. Depuis le <strong>Le 25 mai 2006</strong>, la Slovénie n’applique aucune restriction à l’accès à son marché du travail aux ressortissants français, <strong>lesquels ne sont pas tenus de solliciter un permis de travail.</strong></p>
<p>Toutefois, les autorités slovènes désirant se tenir au courant des tendances sur leur marché du travail, les ressortissants français doivent leur communiquer leur situation professionnelle. C’est à l’employeur qu’il appartient d’effectuer cette formalité auprès du service pour l’emploi compétent <strong>dans les 10 jours</strong> suivant le début de l’emploi.</p>
<p>Il est également nécessaire de communiquer au service de l’emploi le début d’une prestation de service. Cette démarche doit être effectuée par le bénéficiaire du service ayant son siège ou sa résidence en Slovénie et ce, <strong>trois jours au moins</strong> avant le début de la prestation.</p>
<p>(Source : <a href="http://ec.europa.eu/eures/" class="spip_out" rel="external">EURES</a> / Vivre et travailler).</p>
<p><strong>Permis de séjour permanent</strong> (<i>dovoljenje za stalno prebivanje</i>)</p>
<p>Les personnes séjournant de façon continue et légale (c’est à dire titulaires d’un certificat d’enregistrement) depuis cinq ans (dans certains cas pour une durée inférieure) peuvent solliciter un permis de séjour permanent auprès de l’unité administrative compétente à raison de leur domicile.</p>
<p>Il vous sera, entre autres, demandé de justifier de revenus suffisants pour subvenir à vos besoins, de votre protection sociale et des motifs de votre demande.</p>
<p>Le permis de séjour permanent est délivré, sous la forme d’une carte, pour une durée illimitée. Le délai d’obtention est de deux mois et son coût s’élève à 105 points (la valeur du point est variable).</p>
<p>Une fois en possession de ce permis de séjour, vous devrez faire enregistrer votre résidence auprès de l’autorité compétente.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li>Section consulaire de l’<a href="http://www.mzz.gov.si/en/representations_abroad/europe/france/" class="spip_out" rel="external">Ambassade de Slovénie en France</a> <br class="manualbr">28 rue Bois-le-Vent - 75116 Paris <br class="manualbr">Téléphone : 01 44 96 50 66 - Télécopie : 01 45 24 67 05 <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/entree-et-sejour/article/passeport-visa-permis-de-travail#vpa#mc#gov.si#" title="vpa..åt..gov.si" onclick="location.href=mc_lancerlien('vpa','gov.si'); return false;" class="spip_mail">Courriel</a></li>
<li><a href="http://e-uprava.gov.si/" class="spip_out" rel="external">Portail du gouvernement slovène</a> (site également en anglais) / <i>Life events for individuals / Entry into and exit from the country</i> ;</li>
<li><a href="http://www.mnz.gov.si/en/" class="spip_out" rel="external">Ministère slovène de l’Intérieur</a> (site en anglais) ;</li>
<li><a href="http://www.mddsz.gov.si/" class="spip_out" rel="external">Ministère slovène du Travail, de la Famille et des Affaires sociales</a> (site en anglais) / <i>Areas of work / Labour market and employment / Labour migration</i> ;</li></ul>
<p>(Source : <a href="http://ec.europa.eu/eures/" class="spip_out" rel="external">EURES</a> / Vivre et travailler)</p>
<p><i>Mise à jour : septembre 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/entree-et-sejour/article/passeport-visa-permis-de-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
