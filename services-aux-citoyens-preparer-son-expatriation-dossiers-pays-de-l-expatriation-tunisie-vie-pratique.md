# Vie pratique

<h2 class="rub22912">Logement</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/vie-pratique/#sommaire_1">Où se loger ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/vie-pratique/#sommaire_2">Conditions de location</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/vie-pratique/#sommaire_3">Hôtels</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/vie-pratique/#sommaire_4">Electricité</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/vie-pratique/#sommaire_5">Electroménager</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Où se loger ?</h3>
<p>A Tunis, les quartiers résidentiels sont localisés dans l’agglomération même (Mutuelleville, le Belvédère et Notre-Dame), certaines rues des quartiers d’El Menzah et dans la banlieue nord (Gammarth, la Marsa, Carthage, Sidi Bou Saïd). A Sfax, les quartiers résidentiels sont situés dans la proche périphérie. Il est plus facile de trouver à louer une villa qu’un appartement.</p>
<p>Pour trouver un logement, vous pouvez consulter les <strong>petites annonces de la presse locale</strong>, notamment l’édition du dimanche du journal <i>La Presse</i><i>, </i>consulter les <strong>agences immobilières </strong>(les frais appliqués lors de la signature d’un contrat de location sont généralement de 5 % du loyer annuel). Enfin le bouche à oreille reste un bon moyen de trouver un logement par le biais des gardiens de villas toujours au fait des dernières mises en location.</p>
<p><strong>Tunis</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>loyer en dinars tunisiens</strong></td>
<td>studio</td>
<td>3 pièces</td>
<td>5 pièces</td>
<td>villa</td></tr>
<tr class="row_even even">
<td>Tunis</td>
<td>250</td>
<td>575</td>
<td>800</td>
<td>—</td></tr>
<tr class="row_odd odd">
<td>quartiers résidentiels</td>
<td>500</td>
<td>900</td>
<td>1300</td>
<td>2500 à 4000</td></tr>
<tr class="row_even even">
<td>banlieue</td>
<td>120</td>
<td>350</td>
<td>600</td>
<td>1200</td></tr>
</tbody>
</table>
<p><strong>Sfax</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>loyer en dinars tunisiens</strong></td>
<td>studio</td>
<td>3 pièces</td>
<td>5 pièces</td>
<td>villa</td></tr>
<tr class="row_even even">
<td>quartier résidentiel</td>
<td>500</td>
<td>750</td>
<td>1000</td>
<td>1500</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_2"></a>Conditions de location</h3>
<p>On trouve aisément un logement grâce aux petites annonces et aux agences mais il faut souvent effectuer des travaux. Le délai de recherche varie de un à deux mois, suivant le type d’habitation recherché et la saison (il est préférable d’éviter la période estivale). Il est plus aisé de trouver un logement vide que meublé. Les cuisines ne sont généralement pas équipées. Les baux sont la plupart du temps d’un an renouvelable et une caution doit être versée (deux mois de loyer). Il est recommandé d’établir un état des lieux détaillé. Une fois le bail signé, le locataire doit le faire enregistrer à la municipalité. Le loyer est payable d’avance, de un à trois mois. Il est financièrement plus intéressant de louer.</p>
<p>Il est nécessaire de chauffer les appartements l’hiver (les moyens utilisés sont le gaz ou le fuel) et d’avoir recours à la climatisation l’été. Equipement électrique : 220 volts 50 périodes, prises de courant de type français.</p>
<p>Les charges mensuelles suivantes ont été relevées à Tunis :</p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Charges mensuelles en DT</td>
<td>2/3 pièces</td>
<td>3/4 pièces</td>
<td>5/6 pièces</td></tr>
<tr class="row_even even">
<td>Eau</td>
<td>20</td>
<td>25</td>
<td>75</td></tr>
<tr class="row_odd odd">
<td>Gaz / électricité (chauffage, climatisation)</td>
<td>70</td>
<td>250</td>
<td>450</td></tr>
</tbody>
</table>
<p>Pour tout renseignement et conseil à l’attention des personnes de nationalité française et possédant des biens immobiliers en Tunisie, ou toutes personnes désirant acquérir un bien immobilier situé en Tunisie, il convient de consulter le <a href="http://www.consulfrance-tunis.org/-Francais-" class="spip_out" rel="external">site du consulat de France à Tunis</a>.</p>
<h3 class="spip"><a id="sommaire_3"></a>Hôtels</h3>
<h4 class="spip">Auberges de jeunesse</h4>
<p>La Tunisie dispose de quatre auberges de jeunesse affiliées à la Fédération Internationale des Auberges de Jeunesse. La liste peut être consultée sur le site Internet : <a href="http://www.hostels.com/" class="spip_out" rel="external">Hostels.com</a></p>
<h3 class="spip"><a id="sommaire_4"></a>Electricité</h3>
<p>Les services d’approvisionnement en énergie fonctionnent (société tunisienne d’électricité et de gaz - STEG). Voltage : 220 volts, de type français deux fiches. De rares secteurs sont alimentés en 110 volts.</p>
<h3 class="spip"><a id="sommaire_5"></a>Electroménager</h3>
<p>Pratiquement toutes les marques sont disponibles, il est possible de s’équiper à l’identique de la France y compris en Hifi.</p>
<p><i>Mise à jour : novembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-tunisie-vie-pratique-article-loisirs-et-culture.md" title="Loisirs et culture">Loisirs et culture</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-tunisie-vie-pratique-article-communications.md" title="Communications">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-tunisie-vie-pratique-article-transports-110532.md" title="Transports">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-tunisie-vie-pratique-article-cout-de-la-vie-110531.md" title="Coût de la vie">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-tunisie-vie-pratique-article-scolarisation.md" title="Scolarisation">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-tunisie-vie-pratique-article-sante.md" title="Santé">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-tunisie-vie-pratique-article-logement.md" title="Logement">Logement</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/vie-pratique/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
