# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/vie-pratique/article/cout-de-la-vie-114442#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/vie-pratique/article/cout-de-la-vie-114442#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/vie-pratique/article/cout-de-la-vie-114442#sommaire_3">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>L’unité monétaire est le <i>baht</i> (THB).</p>
<p>Le baht se divise en 100 <i>satangs</i>.</p>
<p>Compte tenu du très grand nombre de distributeurs automatiques de billets, le plus simple est d’utiliser sa carte de crédit pour retirer des Baths. Une commission est perçue pour chacun des retraits effectués.</p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<p>Il est possible d’ouvrir un compte dans n’importe quelle banque commerciale thaïlandaise. Dans la majorité des cas, il est nécessaire de fournir une photocopie du passeport et attester d’un permis de travail.</p>
<p>Dans de nombreuses banques, le permis de travail peut être remplacé par un certificat de résidence. Celui-ci est délivré par l’ambassade de France aux personnes sans activité professionnelle, à condition qu’elles soient inscrites au registre des Français établis à l’étranger.</p>
<p>Deux types de contrats sont accessibles aux ressortissants étrangers :</p>
<ul class="spip">
<li>Les <i>saving accounts</i>, proches des comptes de dépôt, sont les plus répandus. Très pratiques pour les expatriés temporaires, ils donnent accès à une carte de retrait et de paiement, ne nécessitent qu’une somme minimum de dépôt assez faible. Pour obtenir une carte de crédit, il est nécessaire de justifier d’un certain niveau de revenus.</li>
<li>Les <i>fix account</i> sont les plus recommandés pour les résidents de longue date, afin de faire fructifier leurs économies. Attention toutefois : l’argent y est automatiquement bloqué, durant un minimum de trois mois, à partir de la date d’ouverture du compte.</li></ul>
<p>La loi prévoit une règlementation stricte concernant les opérations de devises étrangères : les dépôts, retraits et virement d’une monnaie autre que le baht, sont systématiquement contrôlés et limités.</p>
<p>L’usage des cartes de crédit est très répandu et les chèques sont très rarement utilisés.</p>
<p><strong>Transfert de fonds</strong></p>
<p>La monnaie nationale est librement convertible.</p>
<p>Aucune déclaration à la Banque Centrale n’est nécessaire.</p>
<p>Les devises étrangères doivent être placées sur un compte ou échangées sept jours au maximum après leur entrée sur le territoire. Le solde quotidien d’un compte en devise ne doit pas excéder 500 000 USD.</p>
<h3 class="spip"><a id="sommaire_3"></a>Evolution des prix</h3>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : juillet 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/vie-pratique/article/cout-de-la-vie-114442). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
