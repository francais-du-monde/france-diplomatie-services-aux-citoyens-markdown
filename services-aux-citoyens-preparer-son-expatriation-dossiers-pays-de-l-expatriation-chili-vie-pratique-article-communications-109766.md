# Communications

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/vie-pratique/article/communications-109766#sommaire_1">Téléphone – Internet</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/vie-pratique/article/communications-109766#sommaire_2">Poste</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Téléphone – Internet</h3>
<h4 class="spip">Téléphone</h4>
<p>Les communications téléphoniques entre le Chili et la France sont bonnes.</p>
<p>Il existe de nombreux commerces appelés « centro de llamadas » qui permettent de réaliser des appels nationaux et internationaux à coût modéré.</p>
<p>Par ailleurs, certains opérateurs proposent des tarifs spéciaux que ce soit à travers l’achat d’une carte téléphonique ou d’options d’abonnement.</p>
<h5 class="spip">Indicatifs téléphoniques</h5>
<p>Depuis la France vers le Chili, pour les téléphones fixes, vous devez composer le : <strong>00 56 </strong>+ code ville + numéro de téléphone.</p>
<p>Par exemple : pour joindre la Chambre de commerce franco-chilienne (n° 225 55 47), située à Santiago, il faut composer le 00 56 – 2 – 225 55 47.</p>
<p>Pour appeler du Chili vers la France : <strong>00 + 33 </strong>+ numéro à 9 chiffres (sans le 0).</p>
<p><strong>Téléphones portables </strong> : les numéros de téléphone comportent 7 chiffres et commencent par 09 ou 08 et 07.</p>
<p>Les téléphones portables français ne fonctionnent que s’ils sont tri-bandes.</p>
<p>Pour appeler de cellulaire à cellulaire, vous composez le 9, le 8 ou le 7 précédé du chiffre 9. Pour appeler de cellulaire à fixe, vous composez le 02. Pour appeler de fixe à cellulaire, il convient de composer le 099, 098 ou 097.</p>
<p>Par exemple : pour appeler depuis la France l’Alliance française de Viña del Mar, vous devez composer le : 00 56 + 32 + 2 + 83 80 90</p>
<p>Pour appeler un portable chilien depuis la France, vous devez composer le : 00 56 + 9 + numéro de portable commençant par 9 ou 8.</p>
<h5 class="spip">Codes des opérateurs</h5>
<p><strong>110 </strong> : 110 ATT<br class="manualbr"><strong>112 </strong> : Convergia Chile<br class="manualbr"><strong>113 </strong> : Transam Comunicaciones<br class="manualbr"><strong>116 </strong> : Carrier 116<br class="manualbr"><strong>117 </strong> : Carrier 117<br class="manualbr"><strong>118 </strong> : IFX Larga distancia<br class="manualbr"><strong>120 </strong> : Globus 120<br class="manualbr"><strong>121 </strong> : 121 Telefonica del sur<br class="manualbr"><strong>122 </strong> : Manquehue LD<br class="manualbr"><strong>123 </strong> : ENTEL<br class="manualbr"><strong>127 </strong> : Sur Comunicaciones<br class="manualbr"><strong>154 </strong> : Micarrier<br class="manualbr"><strong>155 </strong> : Carrier 155<br class="manualbr"><strong>159 </strong> : GTD Larga Distancia<br class="manualbr"><strong>171 </strong> : Telmex<br class="manualbr"><strong>181 </strong> : Bellsouth<br class="manualbr"><strong>188 </strong> : 188 Mundo</p>
<h5 class="spip">Tarifs</h5>
<p>L’installation est gratuite, comptez environ 11 000 pesos pour l’abonnement. Coût des communications :</p>
<ul class="spip">
<li>locale (Santiago) : 40 pesos/min (9h-20h) - 6 pesos (20h-9h)</li>
<li>entre villes chiliennes : 35-40 pesos/min (période pleine) selon la distance</li>
<li>internationale : France : 500 pesos/min</li></ul>
<h4 class="spip">Téléphoner gratuitement par Internet</h4>
<p>Il est généralement très avantageux d’utiliser <strong>des logiciels de téléphonie sur IP</strong> (Skype, Google talk, Live messenger, Yahoo ! messenger, etc.) car les tarifs proposés à l’international sont souvent moindre que ceux des opérateurs locaux exploitant des lignes classiques, voire gratuits dans certaines conditions.</p>
<h4 class="spip">Internet</h4>
<p>Le réseau Internet est bien développé au Chili. Il est possible d’obtenir une connexion à haut-débit dans la plupart des grandes villes. Les principaux fournisseurs d’accès sont Telefónica et VTR.</p>
<p>Les services sont globalement plus chers qu’en France pour des prestations identiques.</p>
<p>Installer le téléphone et Internet dans son logement peut parfois s’avérer difficile pour un étranger sans résidence définitive. Certaines compagnies comme Telefónica refusent d’établir un contrat dans ces conditions. Une des solutions est de passer par un distributeur sous-traitant (kiosques présents dans les centres commerciaux) qui sera moins regardant sur les pièces justificatives.</p>
<p>Des cybers-cafés existent dans pratiquement toutes les villes chiliennes. Les tarifs sont très abordables.</p>
<h3 class="spip"><a id="sommaire_2"></a>Poste</h3>
<p>Il faut compter une dizaine de jours pour l’acheminement d’un courrier entre le Chili et la France.</p>
<p>Pour connaître la liste de tous les tarifs, vous pouvez consulter le site de la <a href="http://www.correos.cl/" class="spip_out" rel="external">poste chilienne</a> ("Correos de Chile").</p>
<p>Les adresses se présentent généralement au Chili sous la forme suivante :</p>
<p>« Kennedy 5454, piso 3, Of. 301 »</p>
<p>Ce qui signifie : avenue Kennedy, au 3ème étage du numéro 5454, dans le bureau 301.</p>
<p>Pour les objets de valeur, il est préférable d’avoir recours à un service de colis rapide et sécurisé.</p>
<p>Au niveau local, il existe plusieurs services de messagerie privés, offrant plus de sécurité et de rapidité que les services postaux de Correos de Chile. A titre d’exemple : <a href="http://www.chilexpress.cl/" class="spip_out" rel="external">Chilexpress</a></p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/vie-pratique/article/communications-109766). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
