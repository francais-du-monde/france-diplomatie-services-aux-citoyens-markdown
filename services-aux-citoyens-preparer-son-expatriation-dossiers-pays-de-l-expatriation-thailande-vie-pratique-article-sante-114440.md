# Santé

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/vie-pratique/article/sante-114440#sommaire_1">Conditions d’hygiène et état sanitaire</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/vie-pratique/article/sante-114440#sommaire_2">Grippe aviaire</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Conditions d’hygiène et état sanitaire</h3>
<p>La pollution est importante à Bangkok. Dans cette ville, les structures hospitalières sont bonnes dans le secteur privé et public. Ce dernier accueille les meilleurs spécialistes, de très haut niveau mais pose des problèmes de communication (seuls certains médecins parlent anglais) et offre des prestations « hôtelières » médiocres. Le secteur privé dispose de structures d’accueil excellentes et d’un très bon plateau médical.</p>
<p>En province, le niveau de l’offre hospitalière reste variable.</p>
<p>En moyenne le coût des soins, dans le secteur privé, est le suivant : 25€ chez un généraliste, 35€ chez un spécialiste, 60€ chez un dentiste.</p>
<p><strong>Avant le départ :</strong></p>
<p>Consultez votre médecin (éventuellement votre dentiste) et souscrivez à un contrat d’assistance couvrant les frais médicaux et de rapatriement sanitaire.</p>
<p><strong>Entrée en Thaïlande avec des médicaments personnels :</strong></p>
<p>L’attention des voyageurs est appelée sur la réglementation thaïlandaise qui soumet l’importation de médicaments à des règles de déclaration, voire d’autorisation préalable.</p>
<p>Ainsi, les personnes qui justifient d’un traitement médical en cours doivent s’assurer auprès de l’ambassade de Thaïlande que les médicaments emportés sont autorisés. Des règles, en effet, s’appliquent selon la nature des médicaments et la quantité admissible. Pour certains médicaments, et pour une prescription de courte durée, l’ambassade de Thaïlande recommandera de justifier d’une prescription rédigée en anglais et de déclarer spontanément les produits lors du passage en douane. Pour d’autres, l’importation peut être interdite ou limitée.</p>
<p>Les manquements aux règles pourraient conduire à différentes mesures allant de la simple confiscation des médicaments jusqu’à, dans certains cas, des peines d’emprisonnement.</p>
<p>Il est donc vivement conseillé, avant d’engager un voyage sous traitement médical, de se renseigner auprès de l’ambassade de Thaïlande à Paris.</p>
<p><strong>Hygiène alimentaire, hygiène de l’eau :</strong></p>
<p>L’eau distribuée par les réseaux d’alimentation ordinaire n’est pas potable : préférer les eaux en bouteilles capsulées. A défaut, filtrer l’eau et la faire bouillir avant consommation. Eviter la consommation de poisson de viande et de volailles crus.</p>
<p>Affections gastro-intestinales : il est recommandé de veiller à la qualité des aliments et à leur bonne cuisson, d’éviter la consommation de crudités, de coquillages et de tous fruits ou légumes non pelés ou non lavés. Il est également conseillé de préférer l’eau en bouteille plutôt que l’eau du robinet et de se laver régulièrement les mains.</p>
<p>Se protéger efficacement contre les moustiques, vecteurs du paludisme, de la dengue et de l’encéphalite japonaise en utilisant des produits répulsifs adaptés aux pays tropicaux (applications répétées sur les vêtements et les parties de peau découvertes).</p>
<p>Pour le paludisme, selon le lieu et la durée du séjour, doit s’ajouter un traitement préventif médicamenteux adapté à chaque individu : il convient de s’adresser à votre médecin habituel ou à un centre de conseils aux voyageurs. Le traitement devra être poursuivi après le retour en France durant une durée variable selon le produit utilisé. Les frontières avec le Cambodge, le Laos, le Myanmar et la Malaisie sont classées en zone 3.</p>
<p>Quant à la dengue : il convient de respecter les mesures habituelles de protection (vêtements longs, produits anti-moustiques à utiliser sur la peau et sur les vêtements, diffuseurs électriques). La dengue pouvant prendre une forme potentiellement grave il est vivement recommandé de consulter un médecin en cas de fièvre (la prise d’aspirine est déconseillée).</p>
<h3 class="spip"><a id="sommaire_2"></a>Grippe aviaire</h3>
<p>La grippe aviaire est présente de façon diffuse en Thaïlande depuis la fin de l’année 2003, avec des périodes d’apparente accalmie et de reprise. Il s’agit d’une maladie virale animale (volailles) exceptionnellement transmissible à l’homme.</p>
<p>Il est recommandé aux voyageurs d’accorder une attention particulière aux observations ci-après.</p>
<p>La Direction générale de la Santé recommande aux voyageurs d’éviter tout contact avec les volailles et les oiseaux, en évitant notamment de se rendre dans des élevages ou les marchés aux volatiles. Les recommandations générales d’hygiène, qui visent à se protéger des infections microbiennes, sont préconisées :</p>
<ul class="spip">
<li>éviter de consommer des produits alimentaires crus ou peu cuits, en particulier les viandes et les œufs ;</li>
<li>se laver régulièrement les mains à l’eau et au savon ou un soluté hydro-alcoolique.</li></ul>
<p>Le virus se transmet par voie aérienne (voie respiratoire) soit par contact direct, notamment avec les sécrétions respiratoires et les matières fécales des animaux malades, soit de façon indirecte par l’exposition à des matières contaminées (par l’intermédiaire de la nourriture, de l’eau, du matériel et des mains ou des vêtements souillés). Les espaces confinés favorisent la transmission du virus.</p>
<p>Sur place, ou après le retour en France, la survenue de fièvre doit inciter à consulter un médecin en mentionnant le séjour à l’étranger.</p>
<p><strong>Grippe AH7N9</strong> : pas de cas déclaré à ce jour.</p>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li><a href="http://www.ambafrance-th.org/-Securite-de-la-communaute-" class="spip_out" rel="external">Conseils, liste de médecins et hôpitaux</a> sur le site du consulat de France à Bangkok</li>
<li>Page dédiée à la santé en Thaïlande sur le <a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/thailande/" class="spip_in">site Conseils aux voyageurs</a></li>
<li>Fiche Bangkok sur le site du <a href="http://www.cimed.org/" class="spip_out" rel="external">CIMED</a></li></ul>
<p><i>Mise à jour : juillet 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/vie-pratique/article/sante-114440). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
