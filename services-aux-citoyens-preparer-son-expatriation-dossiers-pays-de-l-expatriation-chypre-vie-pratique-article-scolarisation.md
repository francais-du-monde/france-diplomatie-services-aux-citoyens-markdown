# Scolarisation

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/vie-pratique/article/scolarisation#sommaire_1">Les établissements scolaires français à Chypre</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/vie-pratique/article/scolarisation#sommaire_2">Enseignement supérieur</a></li></ul>
<p>Pour toute information sur la scolarisation dans le système français à l’étranger, vous pouvez consulter notre thématique sur les <a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures.md" class="spip_in">Etudes supérieures</a> et la <a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais.md" class="spip_in">Scolarité en français</a>.</p>
<p>Vous y trouverez des renseignements sur :</p>
<ul class="spip">
<li>les établissements français du primaire et du secondaire à l’étranger ;</li>
<li>les bourses scolaires à l’étranger ;</li>
<li>les possibilités qui s’offrent à vous si votre enfant ne peut être scolarisé à l’étranger dans le système français (enseignement à distance par le CNED, programme français langue maternelle (FLAM), internats en France) ;</li>
<li>les épreuves du baccalauréat à l’étranger ;</li>
<li>les bourses d’études supérieures en France et à l’étranger ;</li>
<li>l’équivalence des diplômes.</li></ul>
<p>Pour plus d’information, <a href="http://www.aefe.fr/" class="spip_out" rel="external">consultez le site de l’Agence pour l’enseignement français à l’étranger</a>.</p>
<h3 class="spip"><a id="sommaire_1"></a>Les établissements scolaires français à Chypre</h3>
<p><strong>L’école française</strong></p>
<p>A Nicosie, l’Ecole Franco-Chypriote de Nicosie (EFCN), conventionnée par l’Agence pour l’enseignement français à l’étranger, dispense les programmes français de l’Education nationale et scolarise les enfants <strong>dès l’âge de deux ans et demi, de la maternelle à la Terminale</strong>. Le nouvel établissement a été inauguré en septembre 2009.</p>
<p>A noter qu’une crèche, située dans l’enceinte de l’école, accueille également les enfants <strong>dès l’âge de 18 mois</strong>.</p>
<p>L’enseignement est direct jusqu’à la fin du collège et sur la base des programmes du CNED (Centre national d’enseignement à distance, enseigné au sein de l’école) pour les classes de lycée.</p>
<p>Les élèves préparent le Baccalauréat ou à l’<i>Appolytirio</i> (équivalent chypriote), ce qui ouvre aux élèves l’accès à toutes les universités, à Chypre, en France et dans le monde entier. Les élèves peuvent choisir de suivrele programme français ou le programme bilingue franco-grec. En outre, l’anglais est enseigné à haut niveau dès la maternelle et au collège, les élèves étudient également l’allemand ou l’espagnol en classe de 4ème.</p>
<h3 class="spip"><a id="sommaire_2"></a>Enseignement supérieur</h3>
<p>L’enseignement supérieur, en République de Chypre, est principalement composé de la manière suivante :</p>
<p><strong>Trois universités publiques :</strong></p>
<ul class="spip">
<li>L’<a href="http://www.ucy.ac.cy/" class="spip_out" rel="external">Université de Chypre (University of Cyprus)</a> : créée en 1989, elle a reçu ses premiers étudiants en 1992 ; elle compte six facultés et 21 départements ou assimilés ; c’est le principal établissement d’enseignement supérieur du pays.</li>
<li>La <strong>Technological University of Cyprus</strong> : créée en 2003 et ouverte en 2004, elle est basée à Limassol : Elle compte cinq facultés et dix départements.</li>
<li><strong>L’Open University of Cyprus</strong> créée en 2002, elle a reçu ses premiers étudiants en 2006 ; dans le cadre de l’enseignement tout au long de la vie, elle se consacre principalement à l’enseignement à distance.</li></ul>
<p><strong>Trois universités privées bénéficiant d’une reconnaissance de l’état chypriote depuis l’année académique 2007-2008 :</strong></p>
<ul class="spip">
<li><a href="http://www.unic.ac.cy/" class="spip_out" rel="external">University of Nicosia</a></li>
<li><a href="http://www.euc.ac.cy/" class="spip_out" rel="external">European University OF CYPRUS</a></li>
<li><a href="http://www.frederick.ac.cy/" class="spip_out" rel="external">Frederick University</a></li></ul>
<p>Une série d’établissements privés (généralement de type « collège »). Même si, à la différence des précédents, ces établissements ne bénéficient pas d’une reconnaissance de l’état, certaines des filières qu’ils proposent peuvent être accréditées.</p>
<h4 class="spip">Organisation des études</h4>
<p>Les universités publiques ainsi que les universités privées reconnues par l’état ont adopté le système ECTS ; les cours y sont semestriels et le premier diplôme est délivré à l’issue de la quatrième année d’études (240 crédits ECTS). Il peut être suivi d’un master et d’un doctorat.</p>
<p>Depuis l’année universitaire 1998-1999, l’Université de Chypre participe au programme européen Socrates/Erasmus. Dans ce cadre elle a signé 210 accords bilatéraux de coopération. Depuis 2005, elle participe également au programme Leonardo da Vinci.</p>
<h4 class="spip">Principaux atouts du système d’enseignement supérieur</h4>
<p>Ce sont les atouts de la plupart des jeunes universités : disponibilité, souplesse, ouverture aux échanges internationaux.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/vie-pratique/article/scolarisation). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
