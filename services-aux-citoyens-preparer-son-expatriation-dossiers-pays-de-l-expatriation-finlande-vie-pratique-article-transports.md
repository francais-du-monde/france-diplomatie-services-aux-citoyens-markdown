# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/vie-pratique/article/transports#sommaire_1">Importation de véhicule</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/vie-pratique/article/transports#sommaire_2">Permis de conduire</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/vie-pratique/article/transports#sommaire_3">Code de la route</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/vie-pratique/article/transports#sommaire_4">Assurances et taxes</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/vie-pratique/article/transports#sommaire_5">Achat et location</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/vie-pratique/article/transports#sommaire_6">Immatriculation</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/vie-pratique/article/transports#sommaire_7">Entretien</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/vie-pratique/article/transports#sommaire_8">Réseau routier</a></li>
<li><a id="so_9" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/vie-pratique/article/transports#sommaire_9">Transports en commun</a></li></ul>
<p>Tout responsable d’un accident doit le signaler au bureau central finlandais qui établit une déclaration en double exemplaire dont l’une est à adresser sous cinq jours à l’assureur. En cas d’accident avec un renne, il est nécessaire d’avertir la police afin que le propriétaire puisse être indemnisé.</p>
<h3 class="spip"><a id="sommaire_1"></a>Importation de véhicule</h3>
<p>La mise aux normes finlandaises est obligatoire à l’entrée du véhicule dans le pays.</p>
<p>Les Finlandais roulent dans des véhicules pourvus de lumières (codes) permanentes, d’un système de lave-phare (jets d’eau et/ou balais), d’un témoin lumineux (indicateur de direction sur les ailes), quatre ceintures de sécurité (port obligatoire), de batteries plus puissantes qu’en France, d’une couche de protection renforcée sous la voiture (traitement anticorrosion), d’un jeu supplémentaire de jantes pour les pneus à clous (jeu de quatre pneus obligatoires du 1er novembre au 1er mars), d’un chauffage adapté au climat (en option également un chauffage moteur et de l’habitacle sur secteur avec mise en marche automatique et à distance) ainsi que d’un pot catalytique (depuis le 1er janvier 1991 sur les véhicules neufs).</p>
<p>Les véhicules en provenance de l’Union européenne ne supportent pas de taxe pour les séjours de moins de six mois, ou sept mois pour les entrepreneurs, ou encore pendant la durée des études. Il faut néanmoins déclarer le véhicule auprès des <a href="http://www.tulli.fi/en/index.jsp?language=en" class="spip_out" rel="external">douanes (TULLI)</a>. Passé ce délai, le paiement de la taxe sur les voitures est exigé ainsi que l’enregistrement auprès de l’administration des véhicules (<a href="http://www.ake.fi/AKE_EN/" class="spip_out" rel="external">AKE</a>).</p>
<p>Le contrôle technique est obligatoire à la troisième année d’utilisation. A partir de la cinquième année, il devient annuel.</p>
<h3 class="spip"><a id="sommaire_2"></a>Permis de conduire</h3>
<p>Les permis de conduire français et international sont reconnus.</p>
<h3 class="spip"><a id="sommaire_3"></a>Code de la route</h3>
<p>La conduite s’effectue à droite. La priorité également, même pour certains ronds-points. La conduite d’un véhicule en Finlande ne pose aucun problème particulier, hormis les jours de neige abondante et de verglas.</p>
<p>Les automobilistes sont tenus d’allumer leurs feux de croisement de jour comme de nuit. Sur les routes, de nombreux panneaux signalent la présence possible d’élans ou de rennes.</p>
<p>Les sanctions encourues en cas d’accident sont les mêmes qu’en France. Toutefois, elles sont plus sévères en cas d’ébriété. La conduite en état d’ivresse (taux d’alcoolémie supérieur à 0,5%) entraîne une suspension immédiate du permis de conduire. Les contrôles du taux d’alcoolémie sont systématiques. Certaines contraventions peuvent se révéler très coûteuses car proportionnelles aux revenus et à la gravité de la faute.</p>
<p>Les excès de vitesse sont sévèrement sanctionnés. De nombreux radars fixes ou "volants" sont installés sur les axes Helsinki-Turku et Tampere. Rappel : 80 km/h sur route, 120 km/h sur autoroute (100 km/h en hiver), 50 km/h dans les agglomérations.</p>
<h3 class="spip"><a id="sommaire_4"></a>Assurances et taxes</h3>
<p>L’assurance au tiers est obligatoire.</p>
<h3 class="spip"><a id="sommaire_5"></a>Achat et location</h3>
<p>Renault, Peugeot et Citroën sont les marques françaises représentées dans le pays. On trouve la plupart des marques européennes, américaines et japonaises.</p>
<p>Il n’y a pas de véhicule particulier à conseiller. Chaque voiture est assemblée ou importée en fonction des normes du pays. Pour les longues distances et spécialement en hiver un modèle spacieux traction avant (pour la neige) est bien adapté. Les Finlandais préfèrent généralement le haut de gamme (revente plus aisée).</p>
<p>Il est possible d’acheter sur place le véhicule de son choix, soit en prix HT, si l’on remplit les conditions, soit TTC. Dans ce cas, la différence avec les prix français sera substantielle du fait du taux de taxes internes (30% environ) et d’un taux de TVA de 22%.</p>
<p>Pour ce qui concerne les taxes à l’entrée d’un véhicule d’occasion elles sont calculées, conformément au droit communautaire, sur la valeur marchande du véhicule.</p>
<p>On peut acheter un véhicule d’occasion dans les nombreux garages mais en raison des taxes locales (TVA élevée), les prix sont plus élevés qu’en France. En règle générale, l’importation est préférable à un achat sur place (même d’occasion) sous réserve d’être propriétaire du véhicule depuis au moins un an.</p>
<p>La location de voitures est possible dans les grands garages et chez les loueurs (Avis, Hertz, etc.). Les tarifs à la journée sont supérieurs aux tarifs français.</p>
<h3 class="spip"><a id="sommaire_6"></a>Immatriculation</h3>
<p>Pour toute information, consultez le site <a href="http://www.trafi.fi/en/road/registration" class="spip_out" rel="external">Trafi.fi</a>.</p>
<h3 class="spip"><a id="sommaire_7"></a>Entretien</h3>
<p>Le coût de l’entretien est beaucoup plus élevé qu’en France. Le service est excellent. On peut se procurer des pièces détachées pour les marques françaises dans des délais assez rapides.</p>
<h3 class="spip"><a id="sommaire_8"></a>Réseau routier</h3>
<p>Les routes principales sont en excellent état et sont praticables toute l’année avec des pneus d’hiver, obligatoires de novembre à fin avril. Certaines routes secondaires dans le nord peuvent être momentanément impraticables entre novembre et mars.</p>
<h3 class="spip"><a id="sommaire_9"></a>Transports en commun</h3>
<p><strong>Infrastructure routière </strong></p>
<p>Environ 90 % du réseau routier est emprunté par les lignes régulières de cars qui desservent les plus petits hameaux.</p>
<p>En raison de l’existence d’un très vaste réseau de pistes cyclables dans tout le pays, il est recommandé d’être vigilant vis-à-vis des deux roues.</p>
<p><strong>Réseau ferroviaire</strong></p>
<p>Le réseau des chemins de fer de Finlande couvre quelque 6000 km, dont un quart est électrifié. En Laponie, le réseau atteint Rovaniemi et Kolari, à la frontière suédoise.</p>
<p>Les trains sont rapides, confortables et sûrs. Il existe des trains à grande vitesse (Pendolino) entre Helsinki et les grandes villes atteignant 200 km/h.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.vr.fi/" class="spip_out" rel="external">Chemins de fer finlandais</a> (également en anglais). </p>
<p><strong>Transports aériens </strong></p>
<p>Toutes les villes moyennes sont reliées par air à la capitale (Finnair,) à des prix relativement élevés. La compagnie Norwegian offre des tarifs meilleur marché vers certaines destinations (Laponie) mais avec des horaires parfois plus contraignants.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.finnair.com/fr/fr/information-services/at-the-airport/other-services-helsinki-airport" class="spip_out" rel="external">Aéroport d’Helsinki</a> (également en anglais) </p>
<p><strong>Transports urbains</strong></p>
<p>Un trajet dans les transports helsinkiens coûte 2,50 euros. Il est plus économique d’acheter par exemple des tickets « touristiques » valables un ou plusieurs jours (un jour : 8 euros, trois jours : 16 euros et cinq jours : 24 euros).</p>
<p>A Helsinki il y a une ligne de métro est-ouest, des autobus, le tramway ou taxi collectif pour aller ou revenir de l’aéroport. Les taxis individuels sont nombreux mais assez chers. Les transports urbains sont de bonne qualité et fonctionnent de façon régulière (peu de grèves et passages fréquents).</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.hel.fi/wps/portal/HKL_en/?WCM_GLOBAL_CONTEXT=/HKL/en/Etusivu" class="spip_out" rel="external">Helsinki City Transport</a> </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Site Internet de la compagnie <a href="http://www.matkahuolto.fi/" class="spip_out" rel="external">Oy Matkahuolto Ab</a> de transport en autocar (également en anglais).</p>
<p><i>Mise à jour : janvier 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/vie-pratique/article/transports). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
