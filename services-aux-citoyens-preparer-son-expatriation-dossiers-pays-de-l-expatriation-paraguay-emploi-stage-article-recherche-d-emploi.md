# Recherche d’emploi

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/paraguay/emploi-stage/article/recherche-d-emploi#sommaire_1">Outils pour la recherche d’emploi</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/paraguay/emploi-stage/article/recherche-d-emploi#sommaire_2">Organismes pour la recherche d’emploi</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/paraguay/emploi-stage/article/recherche-d-emploi#sommaire_3">Opportunités d’emploi et de stages (bourses mises en place par les autorités locales)</a></li></ul>
<p>Il n’existe pas d’agence pour l’emploi ou de site internet spécialisé. Il est possible de recourir aux sociétés d’intérim présentes localement (Manpower, par exemple) ou aux quelques annonces publiées dans la presse.</p>
<p>La mairie d’Assomption dispose d’une foire à l’emploi (<i>bolsa de trabajo</i>) mettant en contact futurs employeurs et employés. Ces dernières années certaines associations d’entrepreneurs organisent deux à trois fois par an des foires à l’emploi dénommées <i>ferias de empleo</i> (présence de nombreux jeunes).</p>
<h3 class="spip"><a id="sommaire_1"></a>Outils pour la recherche d’emploi</h3>
<p><strong>Journaux</strong></p>
<ul class="spip">
<li><a href="http://www.empleos.ultimahora.com/" class="spip_out" rel="external">Empleos.ultimahora.com</a> ;</li>
<li><a href="http://clasipar.paraguay.com/categoria/empleos_4.html" class="spip_out" rel="external">Clasipar.paraguay.com</a></li>
<li><a href="http://www.bolsatrabajo.com.py/buscar-empleo/?gclid=CPmN073o9L0CFcjjwgodEpcATQ" class="spip_out" rel="external">Bolsatrabajo.com.py</a></li></ul>
<p><strong>Annuaires</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.ccpf.com.py/" class="spip_out" rel="external">Chambre de Commerce Paraguayo-Française</a></p>
<h3 class="spip"><a id="sommaire_2"></a>Organismes pour la recherche d’emploi</h3>
<ul class="spip">
<li><a href="http://www.jobs.com.py/" class="spip_out" rel="external">Jobs.com.py</a></li>
<li><a href="http://www.manpower.com.py/" class="spip_out" rel="external">Manpower.com.py</a></li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Opportunités d’emploi et de stages (bourses mises en place par les autorités locales)</h3>
<p><a href="http://webmail.stp.gov.py/stpnew/becas-disponibles.php" class="spip_out" rel="external">Webmail.stp.gov.py</a></p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/paraguay/emploi-stage/article/recherche-d-emploi). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
