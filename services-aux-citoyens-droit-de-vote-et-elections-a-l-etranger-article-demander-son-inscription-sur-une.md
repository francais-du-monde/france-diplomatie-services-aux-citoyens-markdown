# Demander son inscription sur une liste électorale consulaire

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/droit-de-vote-et-elections-a-l-etranger/article/demander-son-inscription-sur-une#sommaire_1">Demander son inscription sur une liste électorale consulaire</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Demander son inscription sur une liste électorale consulaire</h3>
<h4 class="spip">Comment et quand demander son inscription ?</h4>
<p>Établi(e) hors de France, vous pouvez vous trouver dans une des trois situations suivantes :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Vous n’êtes pas inscrit(e) au registre des Français établis hors de France :</strong> vous pouvez demander votre inscription sur la liste électorale consulaire, l’inscription au registre n’étant pas obligatoire.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Vous demandez votre inscription au registre des Français établis hors de France :</strong> sauf opposition de votre part, cette formalité va entrainer votre inscription sur la liste électorale du consulat.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Vous êtes déjà inscrit(e) au registre des Français établis hors de France mais vous vous étiez opposé(e) à votre inscription sur la liste électorale consulaire :</strong> vous pouvez changer d’avis et demander votre inscription sur la liste électorale consulaire.</p>
<p><strong>Vous pouvez demander votre inscription à l’ambassade ou au poste consulaire de votre résidence jusqu’au dernier jour ouvrable de décembre.</strong> Cette inscription sera valable à partir du 10 mars de l’année suivante.</p>
<p>Si vous souhaitez modifier le choix d’exercice de votre droit de vote, vous pouvez également le faire au moyen <a href="https://monconsulat.diplomatie.gouv.fr/monconsulat/dyn/public/headerPublic/introduction.html" class="spip_out" rel="external">du portail internet MonConsulat.fr</a>.</p>
<p><strong>Si vous atteignez l’âge de 18 ans dans l’année ou au plus tard le dernier jour de février de l’année suivante vous pouvez, sans attendre, demander votre inscription.</strong> Si vous ne vous manifestez pas et que vous êtes déjà inscrit(e) au registre des Français établis hors de France, vous recevrez une lettre vous annonçant que, sauf opposition de votre part au plus tard le dernier jour ouvrable de l’année, vous serez automatiquement inscrit(e) sur la liste électorale consulaire.</p>
<p><i>Mise à jour : janvier 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/droit-de-vote-et-elections-a-l-etranger/article/demander-son-inscription-sur-une). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
