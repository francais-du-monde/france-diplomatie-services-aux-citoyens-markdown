# Communications

<h4 class="spip">Téléphone – Internet</h4>
<p>Les liaisons téléphoniques sont relativement faciles. Il existe des abonnements mensuels incluant des heures de communications vers la France.</p>
<p>Le fonctionnement d’Internet est globalement bon. L’ADSL est largement accessible. Maroc Telecom assure un abonnement Internet.</p>
<p>Téléphoner gratuitement par Internet Il est généralement très avantageux d’utiliser <strong>des logiciels de téléphonie sur IP</strong> (Skype, Google talk, Live messenger, Yahoo messenger, etc.) car les tarifs proposés à l’international sont souvent moindre que ceux des opérateurs locaux exploitant des lignes classiques, voire gratuits dans certaines conditions.</p>
<h4 class="spip">Poste</h4>
<p>Les liaisons postales sont parfois aléatoires. Le délai d’acheminement du courrier vers la France est d’une semaine à 10 jours. La garantie de réception du courrier est assez bonne. L’envoi des colis est déconseillé car ils se perdent souvent et il faut par ailleurs effectuer des démarches personnelles de dédouanement à la poste centrale.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/vie-pratique/article/communications). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
