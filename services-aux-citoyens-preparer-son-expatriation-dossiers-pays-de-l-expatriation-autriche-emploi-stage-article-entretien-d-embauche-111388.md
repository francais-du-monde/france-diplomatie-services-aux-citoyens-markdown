# Entretien d’embauche

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/emploi-stage/article/entretien-d-embauche-111388#sommaire_1">Conduite de l’entretien</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/emploi-stage/article/entretien-d-embauche-111388#sommaire_2">Négociation du salaire</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Conduite de l’entretien</h3>
<p>Lors de l’entretien il faut donner des réponses claires et précises. Il faut aussi convaincre l’interlocuteur de la stabilité professionnelle et de la volonté de rester pendant une période prolongée en Autriche.</p>
<h3 class="spip"><a id="sommaire_2"></a>Négociation du salaire</h3>
<p>Le salaire est une question délicate à aborder. Proposez une fourchette (voir plus loin les salaires moyens) mais n’insistez pas si l’employeur propose un plafond. Demander plus serait éliminatoire. Les négociations se font en général sur le salaire brut mensuel.</p>
<p><i>Mise à jour : février 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/emploi-stage/article/entretien-d-embauche-111388). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
