# Fiscalité

<h2 class="rub22794">Fiscalité du pays</h2>
<h4 class="spip">Impôt sur le revenu</h4>
<p>Il existe quatre types d’impôts sur les revenus :</p>
<ul class="spip">
<li>un impôt sur le revenu global des <strong>habitants de Belgique</strong>, appelé « impôt des personnes physiques » (IPP)</li>
<li>un impôt sur le revenu global des <strong>sociétés résidentes</strong>, appelé « impôt des sociétés » (ISOC)</li>
<li>un impôt sur les revenus des <strong>non-résidents</strong>, appelé « impôt des non-résidents » (INR)</li>
<li>un impôt sur les revenus des <strong>personnes morales autres que les sociétés</strong>, appelé « impôt des personnes morales » (IPM).</li></ul>
<p>Ces impôts sont perçus de manière anticipée par voie de précomptes (immobilier, mobilier et professionnel).</p>
<p>L’exercice d’imposition commence le <strong>1er janvier</strong> et se finit le <strong>31 décembre</strong> suivant.</p>
<p>L’impôt dû pour un exercice d’imposition est donc établi sur les revenus que le contribuable a réalisés ou recueillis pendant la période imposable.</p>
<p><strong>Pour plus d’informations : </strong></p>
<ul class="spip">
<li>Consulter les thématiques « <a href="http://finances.belgium.be/fr/particuliers/" class="spip_out" rel="external">Particulier</a> » et « <a href="http://finances.belgium.be/fr/entreprises/" class="spip_out" rel="external">Entreprises</a> » sur le site officiel du Service public fédéral – Finances.</li>
<li>Consulter la thématique « <a href="http://www.belgium.be/fr/impots/impot_sur_les_revenus/" class="spip_out" rel="external">Impôt sur le revenu</a> » sur le portail <a href="http://www.belgium.be/" class="spip_out" rel="external">www.belgium.be</a></li></ul>
<h4 class="spip">Taxe sur la valeur ajoutée (TVA)</h4>
<p>La TVA est un impôt sur les biens et les services supporté par le consommateur final et qui est perçu par étapes successives, à savoir à chaque étape dans le processus de production et de distribution. C’est donc la valeur ajoutée qui est taxée à chaque étape.</p>
<p>Les taux appliqués varient selon la nature du bien ou du service taxés. Les taux sont fixés par arrêté royal. Ils sont actuellement de :</p>
<ul class="spip">
<li><strong>6%</strong> principalement pour les biens de première nécessité et pour les prestations de services à caractère social (ex : les produits de première nécessité, le transport de personne, les services agricoles,…) ;</li>
<li><strong>12%</strong> pour certains biens et prestations de services qui d’un point de vue économique ou social sont importants (ex : le charbon, la margarine, les abonnements à la télévision payante,…) ;</li>
<li><strong>21%</strong> pour les opérations se rapportant à des biens ou à des services qui ne sont pas dénommés ailleurs (ex : les voitures neuves, les appareils ménagers électriques, les articles de parfumeries,…).</li></ul>
<p><strong>Pour plus d’informations :</strong></p>
<ul class="spip">
<li>Consultez les thématiques « <a href="http://finances.belgium.be/fr/entreprises/tva/" class="spip_out" rel="external">TVA</a> » sur le site officiel du Service public fédéral – Finances.</li>
<li>Consultez la thématique « <a href="http://www.belgium.be/fr/impots/tva/" class="spip_out" rel="external">TVA</a> » sur le portail <a href="http://www.belgium.be/" class="spip_out" rel="external">www.belgium.be</a></li></ul>
<h4 class="spip">Autres taxes et impôts</h4>
<p>Tout comme il serait impossible de le faire pour la France, il est également très compliqué de dresser une liste exhaustive de toutes les taxes et impôts en vigueur en Belgique.</p>
<p>Pour un rapide aperçu :</p>
<ul class="spip">
<li>Les droits d’enregistrement : impôt perçu par l’Etat lors de l’enregistrement d’un acte ou d’un écrit dans un registre (exemples : acte notarié, baux…).</li>
<li>Successions : après un décès, un impôt, appelé droit de succession, est perçu à l’occasion du transfert du patrimoine de la personne décédée à ses héritiers.</li>
<li>Donation.</li></ul>
<p>Pour bénéficier de toutes informations nécessaires sur la fiscalité, ainsi que sur les procédures à suivre :</p>
<ul class="spip">
<li>Consultez le <a href="http://finances.belgium.be/fr/" class="spip_out" rel="external">site officiel du Service public fédéral – Finances</a>.</li>
<li>Consultez le portail <a href="http://www.belgium.be/" class="spip_out" rel="external">www.belgium.be</a></li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-belgique-fiscalite-22794-article-convention-fiscale-109807.md" title="Convention fiscale">Convention fiscale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-belgique-fiscalite-22794-article-fiscalite-du-pays-109806.md" title="Fiscalité du pays">Fiscalité du pays</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/fiscalite-22794/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
