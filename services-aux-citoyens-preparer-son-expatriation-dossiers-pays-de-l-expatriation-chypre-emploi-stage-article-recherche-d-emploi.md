# Recherche d’emploi

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/emploi-stage/article/recherche-d-emploi#sommaire_1">Outils pour la recherche d’emploi</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Outils pour la recherche d’emploi</h3>
<h4 class="spip">Journaux</h4>
<p>Sur place, des annonces peuvent être consultées dans la presse en langue anglaise : Cyprus Mail (quotidien), Cyprus Daily (quotidien), Cyprus Weekly (hebdomadaire), Financial Mirror (hebdomadaire).</p>
<h4 class="spip">Sites internet</h4>
<p>Vous pouvez également consulter les sites d’emploi, tels que :</p>
<ul class="spip">
<li><a href="http://www.jobincyprus.com/english/" class="spip_out" rel="external">Job in Cyprus</a></li>
<li><a href="http://www.cyprusjobs.com/" class="spip_out" rel="external">Cyprusjobs.com</a></li>
<li><a href="http://www.cyprusrecruiter.com/" class="spip_out" rel="external">Cyprusrecruiter.com</a></li>
<li><a href="http://www.jobscyprus.com/" class="spip_out" rel="external">Jbscyprus.com</a>/</li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/emploi-stage/article/recherche-d-emploi). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
