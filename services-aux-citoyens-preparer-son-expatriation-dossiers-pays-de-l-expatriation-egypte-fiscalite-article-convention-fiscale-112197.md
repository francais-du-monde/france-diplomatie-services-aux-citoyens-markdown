# Convention fiscale

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/fiscalite/article/convention-fiscale-112197#sommaire_1">Champ d’application de la convention</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/fiscalite/article/convention-fiscale-112197#sommaire_2">Dispositions conventionnelles sur certaines catégories de revenus</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/fiscalite/article/convention-fiscale-112197#sommaire_3">Elimination de la double imposition</a></li></ul>
<p>La France et l’Egypte ont signé, le 19 juin 1980, une convention en matière de fiscalité publiée au Journal Officiel du 25 janvier 1983.</p>
<p>Cette convention tend à éviter les doubles impositions en matière d’impôt sur les revenus et sur la fortune qui pourraient résulter de l’application des législations de ces deux Etats.</p>
<p>Le texte de la convention peut être obtenu auprès de la Direction des Journaux Officiels par courrier (26, rue Desaix 75727 PARIS Cedex 15), par télécopie (01 40 58 75 00) ou sur le site Internet du <a href="http://www.impots.gouv.fr/" class="spip_out" rel="external">ministère des Finances</a>.</p>
<p>Les dispositions conventionnelles qui ont primauté sur les dispositions du droit interne, selon l’article 55 de la Constitution française, répartissent entre les deux Etats le droit d’imposer les revenus perçus par leurs résidents respectifs.</p>
<h3 class="spip"><a id="sommaire_1"></a>Champ d’application de la convention</h3>
<p>Cet accord a pour objet de protéger les résidents de chacun des Etats contractants en matière d’impôts prélevés directement sur le revenu ou sur les sociétés.</p>
<p>La convention trouve donc à s’appliquer aux résidents de ces Etats.</p>
<p>L’article 4, paragraphe 1 de la convention s’applique aux personnes qui sont considérées comme « résidents d’un Etat contractant » ou de chacun de ces deux Etats.</p>
<p>D’après ce même article de la convention, une personne est considérée comme « résident d’un Etat contractant » lorsque, en vertu de la législation dudit Etat, elle se trouve assujettie à l’impôt en raison de son domicile, de sa résidence ou de critère analogue.</p>
<p>Au paragraphe 2, l’article 4 fournit des critères subsidiaires permettant de résoudre le cas de double résidence si l’assujettissement à l’impôt ne pouvait suffire.</p>
<p>Ces critères sont :</p>
<ul class="spip">
<li>un foyer d’habitation permanent ;</li>
<li>l’Etat où la personne possède le centre de ses intérêts vitaux ;</li>
<li>l’Etat dans lequel elle séjourne de façon habituelle ;</li>
<li>à défaut, l’Etat dont elle possède la nationalité.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Dispositions conventionnelles sur certaines catégories de revenus</h3>
<h4 class="spip">Traitements, salaires, pensions et rentes</h4>
<h5 class="spip">Rémunérations privées</h5>
<p>L’article 15, paragraphe 1 précise que les traitements et salaires d’origine privée ne sont, en règle générale, imposables que dans l’Etat où s’exerce l’activité personnelle.</p>
<p>Exceptions à cette règle générale :</p>
<p>Le maintien de l’imposition dans l’Etat de résidence du bénéficiaire est prévu par le paragraphe 2 du même article sous réserve de trois conditions simultanément remplies :</p>
<ul class="spip">
<li>le séjour temporaire du bénéficiaire dans l’autre Etat ne dépasse pas une durée totale de 183 jours ;</li>
<li>la rémunération est payée par un employeur qui n’est pas résident de l’Etat d’exercice ;</li>
<li>la rémunération ne doit pas être à la charge d’un établissement stable ou d’une base fixe de l’employeur dans l’Etat.</li></ul>
<p>Exemple : Monsieur X est envoyé en Egypte trois mois, soit quatre-vingt-dix jours (mai, juin, juillet de l’année n) par une PME fabriquant de la maroquinerie en vue de prospecter le marché. Cette entreprise ne dispose d’aucune succursale ni bureau dans ce pays. Dans un tel cas, Monsieur X devra déclarer ses revenus en France.</p>
<p>Au contraire, si Monsieur X est envoyé du mois de février inclus au mois de novembre inclus, son séjour de plus de 183 jours en Egypte entraîne son imposition dans ce pays.</p>
<p>Il résulte des dispositions du paragraphe 3 de l’article 15 de la convention que les revenus professionnels des salariés autres qu’intérimaires employés à bord d’un navire, d’un aéronef en trafic international ou à bord d’un bateau servant à la navigation intérieure ne sont imposables que dans l’Etat où se trouve la direction effective de l’entreprise.</p>
<h5 class="spip">Rémunérations publiques</h5>
<p><strong>Principe</strong></p>
<p>L’article 19, paragraphes 1 et 2 indique que les rémunérations ainsi que les pensions de retraite payées par un Etat ou une personne morale de droit public de cet Etat restent imposables dans cet Etat.</p>
<p>Exemple : Monsieur X est fonctionnaire de l’Etat français ; il est résident en activité en France et décide d’aller prendre sa retraite chez l’un de ses enfants au Caire. Les montants de ses pensions resteront imposés en France ; son dossier étant pris en charge par un Centre des Impôts spécial, le Centre des Impôts des Non Résidents.</p>
<p><strong>Exception</strong></p>
<p>Toutefois, en vertu des dispositions du deuxième alinéa du paragraphe 2 du même article, les règles fixées aux paragraphes 1 et 2 dudit article ne sont pas applicables aux rémunérations ou aux pensions versées au titre de services rendus dans le cadre d’une activité industrielle ou commerciale exercée par un Etat ou une personne morale de droit public.</p>
<p>Les sommes versées à ce titre sont imposées soit dans l’Etat d’exercice de l’activité (article 15 de la convention), soit dans l’Etat de résidence du bénéficiaire (article 18 de la convention), hormis les pensions liées au régime de sécurité sociale.</p>
<p>Exemples :</p>
<p>Monsieur X, agent E.D.F. est envoyé en Egypte afin d’effectuer des travaux de conception avec les services locaux. Monsieur X est rémunéré par E.D.F. France.</p>
<p>E.D.F. étant un établissement de droit public à caractère industriel et commercial, les rémunérations allouées à Monsieur X seront imposées en Egypte.</p>
<p>Monsieur X, retraité de la S.N.C.F., perçoit une pension de la Caisse vieillesse de la S.N.C.F. Or, Monsieur X, résident en France a décidé de vivre au Caire. Cette retraite se verra ainsi imposable en Egypte puisque la S.N.C.F. a le caractère d’un établissement public, industriel et commercial.</p>
<h5 class="spip">Pensions et rentes</h5>
<p>L’article 18, paragraphe 1 prévoit que les pensions de retraite de source privée ainsi que les rentes viagères restent imposables dans l’Etat dont le bénéficiaire est un résident.</p>
<p>Toutefois, le paragraphe 2 du même article dispose que les pensions payées en application de la législation sur la sécurité sociale d’un Etat sont imposables dans cet Etat.</p>
<p>Cette disposition couvre également les pensions publiques auxquelles les règles de l’article 19, paragraphe 2-2 ne sont pas applicables.</p>
<p>Exemple : Monsieur X, citoyen français qui a exercé une activité salariée en Egypte, décide de venir prendre sa retraite en France. Les pensions versées par l’Egypte au titre de cette activité sont imposables en France, hormis celles liées au régime de sécurité sociale égyptien.</p>
<h5 class="spip">Etudiants, stagiaires, enseignants, chercheurs</h5>
<p><strong>Etudiants, stagiaires</strong></p>
<p>L’article 20, paragraphe 1 de la convention prévoit que les étudiants, les stagiaires d’un Etat qui séjournent dans l’autre Etat à seule fin d’y poursuivre leurs études ou leur formation et qui perçoivent des subsides d’origine étrangère à cet Etat sont exonérés d’impôt par ce dernier Etat.</p>
<p><strong>Professeurs, chercheurs</strong></p>
<p>L’article 21-1 précise que les rémunérations versées aux professeurs ou aux chercheurs résidents d’un Etat se rendant temporairement dans l’autre Etat en vue d’y exercer une activité pédagogique ou de recherches pendant une période ne dépassant pas deux ans, dans une université, un collège, une école, un établissement de recherche ou autre, restent imposables dans l’Etat de résidence.</p>
<h4 class="spip">Autres catégories de revenus</h4>
<h5 class="spip">Bénéfices industriels et commerciaux</h5>
<p>L’article 7, paragraphe 1 dispose que les entreprises industrielles et commerciales sont imposables sur le territoire où se trouve un établissement stable.</p>
<h5 class="spip">Bénéfices des professions non commerciales et des revenus non commerciaux</h5>
<p>L’article 14, paragraphe 1, alinéas a et b, dispose que les revenus provenant de l’exercice d’une profession libérale ou d’autres activités indépendantes sont imposables dans l’Etat sur le territoire duquel se trouve l’installation permanente où s’exerce de façon régulière l’activité personnelle ou bien, lorsque la personne physique est présente sur ce territoire pour y exercer son activité pendant cent vingt jours au cours de l’année fiscale.</p>
<p>Les revenus que les professionnels du spectacle ainsi que les sportifs réalisent en cette qualité dans l’un des deux Etats restent imposables dans l’Etat d’exercice de l’activité selon les dispositions de l’article 17 de la convention, paragraphe 1.</p>
<p>L’article 12, paragraphe 1 pose en principe que les revenus non commerciaux (redevances et droits d’auteur) sont imposables dans l’Etat de résidence du bénéficiaire. Toutefois, ces redevances restent imposables dans l’Etat, source des revenus au taux de 25% pour le droit d’usage de marques de fabrique ou de 15% pour les autres cas.</p>
<h5 class="spip">Revenus immobiliers</h5>
<p>L’article 6, paragraphe 1 dispose que les revenus des biens immobiliers y compris les bénéfices des exploitations agricoles sont imposables dans l’Etat où ils sont situés.</p>
<p>Cette règle s’applique également aux gains provenant de la cession ou de l’échange desdits biens ou droits selon les dispositions de l’article 13, paragraphe 1.</p>
<p>En ce qui concerne les gains provenant de l’aliénation de tous biens autres que mobiliers ou immobiliers, le paragraphe 4 de l’article 13 précise qu’ils restent imposables dans l’Etat de résidence du cédant.</p>
<h5 class="spip">Revenus de capitaux mobiliers</h5>
<p><strong>Les dividendes</strong></p>
<p>Ce terme désigne les revenus provenant d’actions, actions ou bons de jouissance, parts de mines, parts de fondateur ou autres parts bénéficiaires, à l’exception des créances et les revenus d’autres parts sociales assimilés aux revenus d’actions.</p>
<p>De manière générale, l’article 10, paragraphe 1 permet à l’Etat de résidence du bénéficiaire d’imposer ces revenus. Toutefois, le paragraphe 2 du même article maintient le droit d’imposer au profit de l’Etat, source des revenus. Les termes du paragraphe 3 précisent que du côté français, il s’agit d’une retenue à la source de 15% en général et de 5% lorsque le bénéficiaire détient une participation d’au moins 10% dans le capital de la société distributrice. Le paragraphe 4-b prévoit du côté égyptien que le taux de la retenue est limité à 20% dans le cas où le bénéficiaire serait une personne physique.</p>
<p><strong>Les intérêts</strong></p>
<p>Ce terme désigne les revenus des fonds publics, des obligations d’emprunts, des obligations d’emprunts et des créances de toute nature ainsi qu’aux intérêts produits par des titres négociables, les bons de caisse et les intérêts de créances ordinaires.</p>
<p>L’article 11, paragraphe 1, précise que les intérêts provenant d’un Etat et payés à un résident de l’autre Etat restent imposables dans l’Etat de résidence du bénéficiaire tout en permettant au paragraphe 2, à l’Etat d’origine de prélever un impôt au taux de 25%.</p>
<h3 class="spip"><a id="sommaire_3"></a>Elimination de la double imposition</h3>
<p>L’élimination de la double imposition pour les résidents de France qui perçoivent des revenus de source égyptienne s’opère aux termes du paragraphe 2 de l’article 24 selon le régime de l’exonération ou de l’imputation.</p>
<p>Les revenus de source française ou égyptienne pour lesquels le droit d’imposer est dévolu à titre exclusif à l’un des deux Etats doivent être maintenus en dehors de la base de l’impôt français (article 24, paragraphe 2shy ;a), réserve faite toutefois de leur prise en compte pour la détermination du taux effectif.</p>
<p>Le calcul du taux effectif se décompose par la détermination d’une cotisation de base correspondant à l’ensemble des revenus de source française ou étrangère passible de l’impôt français suivant les règles de la législation interne en l’absence de convention.</p>
<p>L’impôt exigible est donc égal au produit de la cotisation de base par le rapport entre le montant net total des revenus conventionnellement imposables en France et le montant total du revenu net d’après lequel le calcul de ladite cotisation de base a été effectué.</p>
<p>Au résultat ainsi obtenu peuvent être appliquées les réfactions prévues par la loi interne.</p>
<p><i>Mise à jour : avril 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/fiscalite/article/convention-fiscale-112197). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
