# Emploi, stage

<h2 class="rub22941">Marché du travail</h2>
<p class="chapo">
    La Croatie compte parmi les cinq pays de l’Union européenne dont les taux de chômage sont les plus élevés (18,6% en novembre 2013, Eurostat) et où le taux d’activité demeure le plus faible (44,4%). Dans ces conditions, <strong>il semble relativement difficile de trouver un emploi en Croatie</strong>. Les salaires moyens sont de l’ordre de 300 à 800 EUR par mois.
</p>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/emploi-stage/#sommaire_1">Secteurs à fort potentiel</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/emploi-stage/#sommaire_2">Professions règlementées</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/emploi-stage/#sommaire_3">Rémunération</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Secteurs à fort potentiel</h3>
<h4 class="spip">Le marché du commerce de détail</h4>
<p>Ce secteur, qui a connu une période de forte transformation au cours de la dernière décennie, subit les aléas de la crise et notamment de la baisse de la consommation des ménages. Toutefois, certaines opportunités continuent de s’ouvrir aux exportateurs ou même à la grande distribution sur des secteurs plus spécifique (équipement spécialisé). Les grandes surfaces ‘discount’ continuent par ailleurs de se développer et remporte un succès significatif.</p>
<h4 class="spip">Le marché des biens de consommation</h4>
<p>Il existe une demande, soit pour les produits de bon rapport qualité / prix, soit pour les marques de prestige. Si les parfums et cosmétiques sont déjà très bien représentés, les grandes marques en particulier, il n’en va pas de même pour les secteurs suivants : le textile et le prêt-à-porter, le mobilier et l’équipement de maison, les objets de décoration et tous les produits conditionnés vendus en grandes surfaces (bricolage, droguerie, etc.).</p>
<h4 class="spip">Le marché de l’automobile</h4>
<p>Ce secteur qui avait également connu une forte expansion jusqu’à 2008 a vu ses ventes baisser de manière conséquente (entre -50% et -60% en cinq ans). Toutefois, le marché de l’occasion résiste, voire se développe tout comme celui de l’entretien (garages, accessoires et pièces détachées).</p>
<h4 class="spip">Le multimédia, l’électronique et les télécommunications</h4>
<p>Il s’agit d’un secteur pouvant s’appuyer sur le savoir-faire, l’expérience technique et professionnelle de ses cadres. Cette branche d’activité peut notamment s’avérer intéressante sous forme d’investissement via principalement des prises de participation/rachat dans une entreprise locale</p>
<h4 class="spip">L’agroalimentaire</h4>
<p>L’offre bien que déjà importante (locale ou étrangère) continue à la fois de se consolider et de s’élargir. Des opportunités dans le secteur des surgelés, des produits diététiques et à forte valeur ajoutée existent de même que pour les vins fins/spiritueux et le champagne.</p>
<h4 class="spip">Le tourisme</h4>
<p>Activité saisonnière qui s’étend principalement des mois de juin à septembre et se concentre essentiellement sur la côte de la mer Adriatique, elle génère annuellement des revenus estimés à 6,5 Mds EUR/an. Accueillant 10 millions de touristes étrangers par an, l’offre croate peut revendiquer et s’appuyer sur des ressources naturelles et un patrimoine peu commun à l’échelle mondiale, par sa localisation en Europe continentale et son fort degré de sécurité. D’importants investissements continuent d’être nécessaires pour poursuivre la mise à niveau de l’offre aux standards internationaux notamment sur les segments du tourisme d’élite et d’affaires.</p>
<h3 class="spip"><a id="sommaire_2"></a>Professions règlementées</h3>
<p>Depuis le 1er juillet 2013, la Croatie a commencé à mettre en œuvre les dispositions concernant les professions réglementées et la reconnaissance des qualifications professionnelles étrangères (Journal officiel no. 124/2009, Journal officiel n° 45/2011).</p>
<p>Depuis l’entrée en vigueur de ces lois, les personnes étrangères souhaitant exercer des <a href="http://reguliraneprofesije.azvo.hr/en/professions" class="spip_out" rel="external">professions réglementées</a> en Croatie et ayant des qualifications professionnelles acquises à l’étranger, peuvent s’adresser aux administrations croates - associations professionnelles (chambres) ou ministères de tutelle - donnant accès à la procédure de reconnaissance des qualifications professionnelles étrangères.</p>
<p>L’office national ENIC / NARIC (Agency for Science and Higher Education) joue le rôle de point de contact au niveau national pour la fourniture d’informations sur la reconnaissance des qualifications professionnelles.</p>
<p>La loi sur les professions réglementées et la reconnaissance des qualifications professionnelles étrangères est en conformité avec la directive 2005/36/CE.</p>
<h3 class="spip"><a id="sommaire_3"></a>Rémunération</h3>
<p>Salaire moyen net (I-X 2013) = 5499 HRK soit environ 723 EUR.<br class="autobr">Salaire moyen brut (I-X 2013) = 7925 HRK soit environ 1043 EUR.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-croatie-emploi-stage-article-stages.md" title="Stages">Stages</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-croatie-emploi-stage-article-recherche-d-emploi.md" title="Recherche d’emploi">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-croatie-emploi-stage-article-reglementation-du-travail-110885.md" title="Réglementation du travail">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-croatie-emploi-stage-article-marche-du-travail-110884.md" title="Marché du travail">Marché du travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/emploi-stage/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
