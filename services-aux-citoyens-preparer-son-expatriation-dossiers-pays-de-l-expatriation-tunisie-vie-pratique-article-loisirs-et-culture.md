# Loisirs et culture

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/vie-pratique/article/loisirs-et-culture#sommaire_1">Tourisme</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/vie-pratique/article/loisirs-et-culture#sommaire_2">Activités culturelles</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/vie-pratique/article/loisirs-et-culture#sommaire_3">Sports</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/vie-pratique/article/loisirs-et-culture#sommaire_4">Télévision – Radio</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/vie-pratique/article/loisirs-et-culture#sommaire_5">Presse française</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Tourisme</h3>
<p>Destination très prisée, la Tunisie a accueilli 1,5 million de touristes français en 2010 (dont 50 % durant la période d’été et pour la seule destination de Djerba).</p>
<p>Ce chiffre qui était tombé à 785 000 en 2011 est remonté à 901 000 en 2012, soit 18 % du nombre total de touriste (5 millions en 2012).</p>
<p>Les stations balnéaires réputées émaillent un littoral de près de 1300 km (Tabarka, Hammamet, Sousse, l’île de Djerba, etc.).</p>
<p>Le patrimoine archéologique constitue un autre pôle d’attraction : nécropoles, temples, thermes, villas, ne se visitent pas seulement sur le site de Carthage mais peuvent se découvrir dans le pays tout entier (cité romaine d’Utique, site de Dougga - le plus étendu -, colysée d’el Jem).</p>
<p>Le musée du Bardo (ancien palais des beys de Tunis) abrite l’une des plus riches collections de mosaïques romaines au monde. Avec la céramique et l’orfèvrerie, tapis et kilims sont les fleurons des arts décoratifs et de l’artisanat tunisiens.</p>
<p>Pour tout renseignement, contacter :</p>
<p><a href="http://www.bonjour-tunisie.com/" class="spip_out" rel="external">Office national du tourisme tunisien (ONTT)</a> à Paris<br class="manualbr">32, avenue de l’Opéra<br class="manualbr">75002 Paris<br class="manualbr">Tél : 01 47 42 72 67- Télécopie : 01 47 42 52 68</p>
<p>ou</p>
<p><a href="http://www.bonjour-tunisie.com/" class="spip_out" rel="external">Office national du tourisme tunisien (ONTT)</a> à Lyon<br class="autobr">12, rue de Sèze<br class="manualbr">69006 Lyon<br class="manualbr">Tél : 03 78 52 35 86- Télécopie : 03 72 74 49 75<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/vie-pratique/article/loisirs-et-culture#ontt.lyon#mc#wanadoo.fr#" title="ontt.lyon..åt..wanadoo.fr" onclick="location.href=mc_lancerlien('ontt.lyon','wanadoo.fr'); return false;" class="spip_mail">Courriel</a></p>
<h3 class="spip"><a id="sommaire_2"></a>Activités culturelles</h3>
<h4 class="spip">Activités culturelles en français</h4>
<p>Peu de films français sont distribués dans les salles commerciales. A Sfax, il n’existe que trois salles de cinéma et les films projetés sont de médiocre qualité. L’Institut français de coopération propose, tout au long de l’année, dans des structures tunisiennes, différentes manifestations et spectacles (réception d’écrivains, pièces de théâtre, festivals de cinéma francophone, etc.). La musique tient une place à part à l’occasion de l’Octobre musical de Carthage, du Festival de la Médina (en période de Ramadan) et des festivals d’été de Carthage et d’Hammamet.</p>
<p><strong>Institut français de Tunis</strong><br class="manualbr">87, avenue de la Liberté - BP 180<br class="manualbr">1080 TUNIS Cedex<br class="manualbr">Tél : (216) 71 105 200 - Fax : (216) 71 105 203<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/vie-pratique/article/loisirs-et-culture#contact#mc#ifctunisie.org#" title="contact..åt..ifctunisie.org" onclick="location.href=mc_lancerlien('contact','ifctunisie.org'); return false;" class="spip_mail">Courriel</a></p>
<p><strong>Maison de France à Sfax</strong><br class="manualbr">9 avenue Bourguiba <br class="manualbr">3000 SFAX Tél : (216) 74 221 533<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/vie-pratique/article/loisirs-et-culture#mdf#mc#ifctunisie.org#" title="mdf..åt..ifctunisie.org" onclick="location.href=mc_lancerlien('mdf','ifctunisie.org'); return false;" class="spip_mail">Courriel</a></p>
<p><strong>Relais culturel français à Sousse</strong><br class="manualbr">4 rue des Jasmins - 4002 Sousse<br class="manualbr">Tel : 73 227935</p>
<h4 class="spip">Activités culturelles locales</h4>
<p>La vie culturelle est assez riche à Tunis.</p>
<p>Des pièces de théâtre, tirées du répertoire classique ou contemporain, sont jouées deux à trois fois par trimestre, en arabe dialectal et littéraire. Des concerts de musique traditionnelle tunisienne et de musique classique sont organisés régulièrement.</p>
<p>Des films sont projetés dans une dizaine de salles, dont quelques-unes de bon confort, en version arabe s’il s’agit de productions régionales, en français pour les films d’origine étrangère, notamment américaine.</p>
<p>Plusieurs galeries, à Tunis, Sidi Bou Saïd et Hammamet notamment, organisent des expositions.</p>
<p>Il existe de nombreux clubs vidéo.</p>
<p>La radio tunisienne diffuse en langue arabe et en langue française (RTCI).</p>
<h3 class="spip"><a id="sommaire_3"></a>Sports</h3>
<p>Les principaux sports peuvent être pratiqués, notamment football, tennis, golf, équitation, plongée. Quelques clubs sportifs existent. Certains hôtels disposent d’installations sportives.</p>
<p>La chasse est possible. Le gibier est abondant et varié. Les formalités d’introduction d’une arme et d’obtention d’un permis sont strictes et très longues. Il est impératif à l’arrivée en Tunisie de présenter les armes à la douane, qui les conserve jusqu’à la régularisation de leur introduction et la délivrance des autorisations de détention de port d’arme et de permis de chasse par les commissariats de la localité de résidence. Un permis « Forêt » et une assurance sont également requis. Les périodes d’ouverture sont fixées chaque année par le ministère de l’Agriculture (généralement entre octobre et février).</p>
<p>Pour la pêche, un permis est également nécessaire. Il est délivré par l’Office de Pêche.</p>
<h3 class="spip"><a id="sommaire_4"></a>Télévision – Radio</h3>
<h4 class="spip">Télévision</h4>
<p>Il existe une quinzainede télévisions tunisiennes maisseules les deux télévisions publiques (Wataniya 1 et Wataniya 2) diffusent constamment sur le réseau hertzien oùl’on peut également regarder la Rai uno.</p>
<p>On peut capterles chaînes de télévision françaises par satellite.</p>
<h4 class="spip">Radio</h4>
<p>Radio Tunis chaîne internationale (R.T.C.I.) propose la plupart de ses programmes en français (quelques émissions en anglais, italien, allemand et espagnol).</p>
<p>Les radios françaises peuvent être écoutées sur internet ou sur les bouquets satellitaires,sans aucun problème.</p>
<p>Pour RFI, se référer au <a href="http://www.rfi.fr/radiofr/statiques/capterrfi.asp" class="spip_out" rel="external">site web de la radio</a></p>
<p>Pour le canal arabe de RFI : <a href="http://www.mc-doualiya.com/frequencies" class="spip_out" rel="external">Monte Carlo Doualiya</a>.</p>
<h3 class="spip"><a id="sommaire_5"></a>Presse française</h3>
<p>La presse française entre librement en Tunisie mais tous les titres ne sont pas diffusés pour des raisons de rentabilité commerciale.</p>
<p>On trouve les kiosques spécialisés dans la distribution de la presse internationale dans les endroits où sont concentrés les Européens (résidents ou touristes), sur les artères des grandes villes et dans les complexes commerciaux.</p>
<p><strong>Médiathèques françaises</strong></p>
<p>Il existe trois médiathèques françaises en Tunisie, à Tunis, Sousse et Sfax. Renseignements sur le <a href="http://www.mediatheques.institutfrancais-tunisie.com/opacwebaloes/index.aspx" class="spip_out" rel="external">site de l’Institut français de Tunisie</a>.</p>
<p><i>Mise à jour : novembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/vie-pratique/article/loisirs-et-culture). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
