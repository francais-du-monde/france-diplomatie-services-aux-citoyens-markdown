# Fiscalité du pays

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/fiscalite/article/fiscalite-du-pays#sommaire_1">Impôt sur le revenu</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/fiscalite/article/fiscalite-du-pays#sommaire_2">Liquidation</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/fiscalite/article/fiscalite-du-pays#sommaire_3">Paiement de l’impôt</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/fiscalite/article/fiscalite-du-pays#sommaire_4">Date, conditions et lieu de dépôt des déclarations de revenus des personnes physiques </a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/fiscalite/article/fiscalite-du-pays#sommaire_5">Impôt sur le revenu des non-résidents (IRNR)</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/fiscalite/article/fiscalite-du-pays#sommaire_6">Régime des impatriés dit « Beckham »</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/fiscalite/article/fiscalite-du-pays#sommaire_7">Impôt sur les sociétés (IS)</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/fiscalite/article/fiscalite-du-pays#sommaire_8">Taxe sur la valeur ajoutée (IVA)</a></li>
<li><a id="so_9" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/fiscalite/article/fiscalite-du-pays#sommaire_9">Impôts patrimoniaux</a></li>
<li><a id="so_10" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/fiscalite/article/fiscalite-du-pays#sommaire_10">Impôts locaux</a></li>
<li><a id="so_11" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/fiscalite/article/fiscalite-du-pays#sommaire_11">Les autres impôts et taxes en Espagne</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Impôt sur le revenu</h3>
<p><strong>Détermination de la base, déductions et autres modalités</strong></p>
<p>L’impôt sur le revenu des personnes physiques a fait l’objet d’une réforme adoptée en novembre 2014, dont la mise en œuvre est échelonnée sur la période 2015-2016.<br class="autobr">Il s’applique sur l’ensemble du territoire national, sur la base des lois établies par l’Etat central, sans préjudice des compétences normatives dont jouissent les communautés autonomes auxquelles est reversée une fraction du produit de la plupart des impôts d’Etat. Par dérogation, le Pays Basque et la Navarre sont dotés d’un régime fiscal spécial dit foral qui leur confère davantage d’autonomie dans la conception du système fiscal.</p>
<p>L’IRPF est dû par les personnes physiques ayant leur résidence fiscale sur le territoire espagnol, ainsi que les personnes domiciliées à l’étranger mais relevant de statuts professionnels dérogatoires (les agents de la fonction publique espagnole en activité à l’étranger, les personnes dotées du statut diplomatique et assimilé). <br class="autobr">La notion de domicile fiscal s’apprécie au regard des critères liés à la durée du séjour sur le territoire espagnol (supérieure à 183 jours au cours de l’année civile) et au lieu où la personne situe le centre principal ou la base des activités/intérêts économiques. Une présomption simple de résidence fiscale est opposable au contribuable dont le conjoint non séparé et les enfants mineurs résident habituellement sur le territoire espagnol.</p>
<p>La loi définit les revenus expressément exonérés d’IRPF, parmi lesquels figurent notamment :</p>
<ul class="spip">
<li>les prestations de sociales pour incapacité permanente absolue ou grande invalidité</li>
<li>les pensions alimentaires reçues des parents en vertu d’une décision de justice ;</li>
<li>les bourses publiques ou versées par des organismes sans but lucratif ;</li>
<li>les indemnités de rupture de contrat de travail (dans la limite d’un plafond) ;</li>
<li>certaines prestations et aides à caractère social.</li></ul>
<p>Les revenus imposables, retenus après déduction des charges engagées pour leur obtention, sont affectés, selon leur nature, soit à l’assiette générale à laquelle s’applique le barème progressif à sept tranches, soit à la catégorie des revenus de l’épargne soumis à un barème à trois taux.</p>
<p>L’assiette générale comprend les différentes catégories de revenus suivantes :</p>
<ul class="spip">
<li>revenus du travail</li>
<li>revenus des activités économiques (BIC, BNC, BA)</li>
<li>revenus de capitaux mobiliers non affectés à la base spéciale  barème général progressif</li>
<li>revenus du capital immobilier (RF)</li>
<li>plus-values mobilières à court terme</li>
<li>plus-values patrimoniales hors produits de cession</li>
<li>revenus théoriques prévus par la loi</li></ul>
<p>Les revenus de l’épargne sont constitués des :</p>
<ul class="spip">
<li>revenus de capitaux mobiliers (RCM) / barème spécifique à 3 taux</li>
<li>plus-values et moins-values patrimoniales issues de cessions</li></ul>
<p>Les revenus théoriques englobent les revenus fictifs tirés de la possession d’immeubles non loués (ni affectés à leur résidence principale) et ceux correspondant aux participations dans des organismes d’investissement collectif ou dans des sociétés transparentes étrangères.</p>
<p>La prise en compte de la situation personnelle et familiale dans le cadre de l’IRPF prend la forme d’une tranche non imposable, qui varie en fonction de l’âge, du nombre d’enfants ou d’ascendants à charge du contribuable, et de sa condition au regard d’une éventuelle incapacité physique.</p>
<p>Les différentes déductions applicables depuis 2015 se décomposent de la manière suivante :</p>
<ul class="spip">
<li>minimum personnel : 5.550 €</li>
<li>majoration au-delà de 65 ans : + 1.150 €</li>
<li>majoration au-delà de 75 ans : + 1.400 €</li>
<li>ascendants à charge : mêmes déductions et majorations en fonction de l’âge 65 ou 75 ans)</li>
<li>enfants à charge : 2.400 € (1er), 2.700 (2ème), 4.000 (3ème) et 4.500 € (à partir du 4ème)</li>
<li>majoration en faveur des mineurs de 3 ans : de 2.800 €</li>
<li>personnes atteintes d’une incapacité : en fonction du taux d’incapacité et des frais d’assistance (de 3.000 € à 12.000 €)</li>
<li>Par ailleurs, un impôt négatif est accordé depuis 2015 aux contribuables faisant partie d’une famille nombreuse ou ayant à leur charge des personnes atteintes d’une incapacité et qui exercent une activité professionnelle (d’un montant de 1.200 € ou 2.400 €).</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Liquidation</h3>
<p>L’ensemble des revenus imposables constitue la base « liquidable », de laquelle peuvent s’imputer, sous certaines conditions, diverses déductions ou avantages fiscaux. Tel est le cas notamment des investissements dans le capital d’entreprises nouvelles, des dons, des revenus obtenus dans les villes autonomes de Ceuta et Melilla et des contributions aux plans de prévoyance sociale.</p>
<p>L’imposition individuelle constitue la modalité de droit commun en Espagne, avec une option possible pour l’imposition commune des membres qui forment le foyer fiscal (valable uniquement pour l’année au titre de laquelle elle est exercée). L’option ne peut être exercée que dans le cadre d’une unité familiale constituée de contribuables qui relèvent de l’IRPF (à l’exclusion de l’IRNR qui s’applique aux personnes fiscalement non-résidentes d’Espagne). Sous certaines conditions, l’option ouvre droit à une déduction complémentaire de 2.150 € pour les familles monoparentales avec personnes à charge (mineures ou majeures atteintes d’une incapacité).<br class="autobr">Suite à la réforme fiscale adoptée fin 2014, le barème applicable à la base générale de l’IRPF se décompose de la manière suivante à compter du 1er janvier 2015 :</p>
<p class="document_doc">
<a class="spip_in" title="Doc:Barême IRPF , 12.2 ko, 0x0" href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/tableau-consolide_cle0166e5.pdf"><img width="18" height="21" alt="Doc:Barême IRPF , 12.2 ko, 0x0" src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L18xH21/pdf-7444b.png" style="vertical-align: middle;"></a>
<a href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/tableau-consolide_cle0166e5.pdf">Barême IRPF - (PDF, 12.2 ko)</a>
</p>
<p>En ce qui concerne la base constituée des revenus de l’épargne, le barème à trois tranches applicable à partir de l’année 2015 se présente ainsi :</p>
<p class="document_doc">
<a class="spip_in" title="Doc:Barême revenus Epargne (2015) , 11 ko, 0x0" href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/tableau-epargne_cle08343e.pdf"><img width="18" height="21" alt="Doc:Barême revenus Epargne (2015) , 11 ko, 0x0" src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L18xH21/pdf-7444b.png" style="vertical-align: middle;"></a>
<a href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/tableau-epargne_cle08343e.pdf">Barême revenus Epargne (2015) - (PDF, 11 ko)</a>
</p>
<h3 class="spip"><a id="sommaire_3"></a>Paiement de l’impôt</h3>
<p>De la même manière que les revenus de capitaux mobilier, les revenus du travail et de remplacement font l’objet d’une première imposition sous forme de retenue à la source, avant, le cas échéant, la liquidation définitive de l’impôt sur la base des éléments déclarés. Si, à ce stade, l’administration constate un trop-perçu d’impôt, le contribuable a droit à un remboursement équivalent.</p>
<p>Concrètement, les personnes fiscalement résidentes d’Espagne perçoivent leurs salaire après déduction de la retenue à la source par l’employeur, sur la base des informations qui lui ont été transmises concernant la situation personnelle et familiale du salarié.</p>
<p>S’agissant des revenus de capitaux, l’établissement financier qui les verse au bénéficiaire est généralement responsable de pratiquer la retenue à la source et d’en reverser le produit au Trésor espagnol.</p>
<h3 class="spip"><a id="sommaire_4"></a>Date, conditions et lieu de dépôt des déclarations de revenus des personnes physiques </h3>
<p>Les déclarations d’IRPF sont à déposer auprès des administraciones (équivalent de nos centres des impôts en France) de la Agencia Estatal de Administracion Tributaria (AEAT), voire des établissements bancaires qui ont conclu un accord de collaboration avec l’AEAT. Toutefois, la plupart des contribuables espagnols présentent leurs déclarations par voie électronique.</p>
<p>La campagne déclarative annuelle s’échelonne en principe entre début avril et fin juin. Une période raccourcie est prévue pour la formalité présentielle (généralement à partir du mois de mai), ainsi qu’un délai spécifique pour la confirmation de la déclaration pré-remplie (borrador).</p>
<p>Les formulaires à souscrire appartiennent à la série n° 100 à 199.<br class="autobr">Dans la mesure où l’impôt sur le revenu des personnes physiques (IRPF) repose sur un système quasi-généralisé de retenues à la source, l’obligation de présenter une déclaration est déterminée par un certain nombre de conditions liées au montant des revenus perçus et à leur nature.</p>
<p>D’une manière générale, les personnes qui disposent de salaires dont le montant annuel excède 22.000 € sont soumis à une obligation déclarative. Ce seuil est ramené à 11.200 € dans l’hypothèse d’une pluralité d’employeurs. Divers autres seuils (généralement inférieurs) sont prévus en fonction de la nature des revenus (revenus de capitaux mobiliers, revenus immobiliers « imputés », plus-values etc…).</p>
<p>La présentation de la déclaration d’IRPF donne lieu à la liquidation définitive de l’impôt, suite à la régularisation des retenues à la source opérées en amont. Lorsque le cumul des retenues à la source préalablement appliquées sur les revenus déclarés fait apparaître un excédent par rapport au montant résultant de la liquidation définitive, le contribuable a droit à un remboursement du trop-perçu d’impôt.</p>
<p>Pour toute précision, il est recommandé de consulter le site internet de l’AEAT) à partir du lien suivant :<a href="http://www.agenciatributaria.es/AEAT.internet/Renta2014.shtml" class="spip_out" rel="external">http://www.agenciatributaria.es/AEAT.internet/Renta2014.shtml</a></p>
<h3 class="spip"><a id="sommaire_5"></a>Impôt sur le revenu des non-résidents (IRNR)</h3>
<p><strong>Régime de droit commun</strong></p>
<p>Sont redevables de l’IRNR les personnes physiques ou juridiques n’ayant pas leur domicile fiscal en Espagne qui perçoivent des revenus de source espagnole, ainsi que, sous certaines conditions, les associés d’entités relevant du régime de la transparence fiscale.</p>
<p>Les particuliers fiscalement résidents de France qui perçoivent des revenus de source espagnole sans l’intermédiation d’un établissement permanent sont susceptibles d’être redevables de l’IRNR selon les modalités suivantes en fonction des revenus concernés (liste non exhaustive) :</p>
<ul class="spip">
<li>salaires, dividendes, intérêts et plus-values : retenue à la source au taux de 19,5 % pour les revenus perçus entre le 12 juillet et le 31 décembre 2015 (20 % entre le 1er janvier et le 12 juillet 2015) ; ce taux sera abaissé à 19 % pour les revenus perçus à compter du 1er janvier 2016</li>
<li>primes de jeux au-delà de la tranche non imposable de 2.500 € : 20 %</li>
<li>pensions de retraite : retenue au taux échelonné entre 8 % et 40 % en fonction de leur montant</li></ul>
<p>Un certain nombre d’exonérations sont prévues par la loi, en particulier les bourses publiques, les intérêts et les revenus de comptes bancaires ouverts en Espagne par des non-résidents.</p>
<p>Par ailleurs, les personnes fiscalement résidentes de France titulaires de droits immobiliers ou propriété situé »s en Espagne sont susceptibles d’être redevables de l’IRNR au taux de droit commun sur une base correspondant à 2 % (ou 1,1 %) de la valeur cadastrale des biens.</p>
<p>Enfin, les non-résidents qui perçoivent au moins 75 % de leur revenu mondial de source espagnole ont la faculté d’opter pour le régime de l’IRPF, en vue de bénéficier du barème progressif et de la prise en compte de leur situation personnelle et familiale en Espagne.</p>
<h3 class="spip"><a id="sommaire_6"></a>Régime des impatriés dit « Beckham »</h3>
<p>Le régime des impatriés en Espagne est issu d’une loi adoptée en 2004 et codifiée par l’article 93 de la loi sur l’impôt sur le revenu des personnes physiques (LIRPF). Ses conditions d’application ont été durcies en 2010 et le dispositif a fait l’objet de divers aménagements dans le cadre de la réforme fiscale adoptée fin 2014.<br class="autobr">Ce régime dérogatoire s’applique pour une période comprenant l’année du transfert et les 5 années postérieures, sur option exercée par les personnes qui satisfont aux conditions cumulatives suivantes :</p>
<p>a)  elles n’étaient pas fiscalement résidentes d’Espagne au cours des dix années précédant le transfert de leur domicile dans cet Etat ;</p>
<p>b)  le transfert de leur domicile intervient en une conséquence de leur contrat de travail ;</p>
<p>c)  leur activité professionnelle est exercée sur le territoire espagnol au profit d’une entreprise ou entité résidente d’Espagne (ou de l’établissement stable en Espagne d’une entité non-résidente) ;</p>
<p>d)  les revenus perçus au titre de cette activité ne sont pas exonérés d’impôt sur le revenu des non-résidents (IRNR).</p>
<p>Depuis 2015, les professions du sport sont exclues du bénéfice des impatriés. <br class="autobr">Les revenus d’activité des impatriés sont imposés jusqu’à concurrence de 600.000 € au taux unique de 24 % en lieu et place du barème progressif de l’IRPF. La fraction excédentaire relève du taux de 47 % (45 % à compter de 2016) et les autres revenus (notamment certains revenus de capitaux) des taux correspondants de l’IRNR.</p>
<h3 class="spip"><a id="sommaire_7"></a>Impôt sur les sociétés (IS)</h3>
<p>L’impôt sur les bénéfices des sociétés a fait l’objet de diverses modifications dans le cadre de la réforme fiscale adoptée en novembre 2014. <br class="autobr">Les assujettis à l’IS sont les entités dotées de la personnalité juridique qui revêtent la forme de sociétés commerciales, coopératives et sociétés agricoles, associations, fondations et institutions de toute nature (publique ou privée), à l’exclusion des sociétés civiles.</p>
<p>D’autres entités dépourvues de personnalité juridique sont nommément désignées par la loi comme relevant également de cet impôt, à savoir pour l’essentiel :</p>
<ul class="spip">
<li>les fonds d’investissement, les fonds de pension et les fonds de capital-risque ;</li>
<li>les groupements temporaires d’entreprises ;</li>
<li>les fonds de régulation du marché hypothécaire ;</li>
<li>les fonds d’actifs bancaires.</li>
<li>La territorialité de l’IS en Espagne est mondiale, de sorte que l’assujetti est soumis dans son Etat de résidence à une obligation fiscale portant sur ses bénéfices mondiaux.</li></ul>
<p>La résidence fiscale en Espagne s’acquiert soit par la constitution de l’entité conformément à la loi espagnole, l’établissement du siège social ou du siège de direction effective sur le territoire espagnol.</p>
<p>Un certain nombre d’entités bénéficient d’une exonération totale d’IS (comme l’Etat, les communautés autonomes, les collectivités locales et les organismes publics), d’autres d’une exonération partielle (notamment les fondations, les associations RUP, les fédérations sportives espagnoles et l’Eglise catholique ainsi que les communautés religieuses conventionnées avec l’Etat espagnol).</p>
<p>Par ailleurs, la loi encadre les régimes spéciaux prévus en faveur de certaines entités, en raison de leur forme ou de la nature de l’activité exercée.</p>
<p>L’assiette imposable est déterminée en application de la méthode de l’estimation directe, c’est-à-dire à partir du résultat comptable faisant l’objet de corrections extra comptables, et après déduction, le cas échéant, des déficits antérieurs. Les micro-entreprises peuvent opter, sous certaines conditions, pour la méthode de l’estimation objective, sur la base d’éléments et indices propres à chaque catégorie d’activité.  <br class="autobr">Depuis la réforme fiscale adoptée fin 2014, les taux nominaux d’IS ont été abaissés et la distinction en fonction de la taille de l’entreprise progressivement supprimée sur la période 2015-2016. Les principaux taux applicables sont indiqués dans le tableau ci-dessous (liste non exhaustive) :</p>
<p class="document_doc">
<a class="spip_in" title="Doc:Taux nominaux d'IS , 12.2 ko, 0x0" href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/tableau-societe_cle03cc5e.pdf"><img width="18" height="21" alt="Doc:Taux nominaux d’IS , 12.2 ko, 0x0" src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L18xH21/pdf-7444b.png" style="vertical-align: middle;"></a>
<a href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/tableau-societe_cle03cc5e.pdf">Taux nominaux d’IS - (PDF, 12.2 ko)</a>
</p>
<p><strong>Pour en savoir plus :</strong></p>
<p>Les modalités d’application de l’impôt sur les bénéfices des sociétés sont présentées dans la brochure publiée par le site internet de l’AEAT (actualisée chaque année), sous la rubrique « Manuales prácticos / Sociedades ».</p>
<h3 class="spip"><a id="sommaire_8"></a>Taxe sur la valeur ajoutée (IVA)</h3>
<p>La taxe s’applique sur l’ensemble du territoire national aux livraisons de biens et prestations de services effectués par des entreprises et professionnels, ainsi qu’aux acquisitions intra-communautaires et aux importations provenant de pays tiers. Toutefois, elle ne s’applique ni sur le territoire des Iles Canaries, ni sur celui des villes autonomes de Ceuta et Melilla, où un autre impôt indirect équivalent est en vigueur (respectivement l’IGIC et l’IPSI).</p>
<p>Dans la mesure où elle est harmonisée au sein de l’UE, la TVA présente en Espagne les mêmes principes et modalités qu’en France, à l’exception des spécificités ayant trait essentiellement aux taux et au champ d’application des taux réduit ou super-réduit.</p>
<p>A noter par ailleurs que la réglementation prévoit un régime de groupe en matière de TVA.</p>
<p><strong>Taux de TVA en vigueur depuis le 1er juillet 2012</strong></p>
<p>Taux normal  21 %<br class="autobr">Taux réduit          10 %<br class="autobr">Taux super-réduit  4%</p>
<h3 class="spip"><a id="sommaire_9"></a>Impôts patrimoniaux</h3>
<p><strong>Impôt sur le patrimoine (IP)</strong></p>
<p>L’impôt sur le patrimoine est un impôt d’Etat cédé aux communautés autonomes, qui, depuis son rétablissement en 2011, sont libres de l’appliquer ou non. <br class="autobr">Dans la communauté de Madrid, l’impôt ouvre droit à une bonification de 100 %, équivalent à une exonération totale d’IP sur le territoire de la communauté madrilène.<br class="autobr">En Catalogne, l’IP s’applique aux éléments du patrimoine d’une valeur supérieure à 500.000 €, sous réserve des bonifications spécifiques à certains biens (notamment le patrimoine protégé des personnes atteintes d’une incapacité ou les propriétés forestières).</p>
<p><strong>Les droits de mutation à titre gratuit (ISD)</strong></p>
<p>L’impôt sur les successions et donations fait partie des impôts d’Etat totalement cédés aux CC.AA.</p>
<p>Il est dû par les personnes physiques bénéficiant de successions (héritiers et légataires) ou de libéralités, ainsi que les bénéficiaires de contrats d’assurance-vie. Les contribuables ayant leur résidence en Espagne sont redevables de l’ISD en vertu d’une obligation personnelle, indifféremment du lieu où se trouvent les biens ou droits faisant partie de la transmission en leur faveur. Les autres sont redevables en vertu d’une obligation réelle, limitée aux biens et droits de toute nature situés sur le territoire espagnol.</p>
<p>Le fait générateur est constitué de l’acquisition de biens ou droits mortis causa ou inter vivos, ainsi que de la perception de sommes par les bénéficiaires de contrats d’assurance-vie, lorsque le titulaire du contrat est distinct du bénéficiaire (sauf exceptions).</p>
<p>Une fois la base déterminée selon les règles applicables à chaque type de transmission, il y a lieu de déduire les abattements fixés par l’Etat et, le cas échéant, les CC.AA, notamment en fonction du lien de parenté avec le défunt (uniquement pour les successions) et de la nature des biens ou droits transmis.</p>
<p>A cette base « liquidable » s’applique le taux correspondant à la tranche du barème d’imposition (entre 7,65 % et 34 %, sans préjudice des modifications de taux adoptées par les communautés autonomes). Ce montant est ensuite affecté d’un coefficient multiplicateur visant à prendre en compte la valeur du patrimoine préexistant que possède le bénéficiaire de la transmission.</p>
<p>La double imposition qui frappe, le cas échéant, les mêmes opérations dans le cadre de transmissions transfrontalières, ouvre droit en Espagne à l’imputation d’un crédit d’impôt plafonné. Au surplus, des déductions spécifiques sont accordées dans les villes autonomiques de Ceuta et Melilla, ainsi que dans les communautés qui ont fait usage de leur capacité normative à cette fin.</p>
<p>Les CCAA jouissent d’une compétence normative totale sur les droits de mutation, portant sur la détermination du tarif, des coefficients multiplicateurs, des déductions et réductions.</p>
<p>L’impôt sur les transmissions patrimoniales et actes juridiques (ITP-AJD)<br class="autobr">Equivalent des droits d’enregistrements en France, l’ITP-AJD regroupe en réalité trois types d’impositions, qui frappent respectivement les transmissions patrimoniales à titre onéreux, les actes juridiques documentés et les opérations financières des sociétés, étant précisé que celles relevant de cet impôt se trouvent en principe exclues du champ d’application de la TVA.</p>
<p>En ce qui concerne les transmissions de biens à titre onéreux, la base imposable est constituée de la valeur réelle du bien ou droit transmis, après déduction des charges (à l’exclusion des dettes afférentes). Les taux applicables à cette base varient en fonction de la nature des biens transmis (mobiliers ou immobiliers) et de l’opération considérée (entre 1 % et 6 %, sans préjudice des modifications adoptées, le cas échéant, par la CC.AA).</p>
<p>S’agissant des actes juridiques documentés, ils donnent lieu au paiement de l’impôt lorsqu’ils sont formalisés sur le territoire espagnol ou ont des effets juridiques ou économiques sur le territoire espagnol. Les actes notariés, documents de commerce et documents administratifs sont soumis à une imposition variable, soit sous forme de tarif fixe ou proportionnel, soit en application d’un barème progressif.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p>Les conditions d’application de ces impôts d’Etat cédés aux CC.AA dépendent dans une large mesure des compétences normatives qu’elles ont exercées (ou non). Pour plus d’information, il est nécessaire de contacter les services géographiquement compétents.</p>
<p>Le site internet de l’AEAT met à disposition des usagers les liens vers les administrations autonomiques compétentes, sous le menu « Enlaces de interés / Fiscalidad autonomica y local / Enlaces ».</p>
<h3 class="spip"><a id="sommaire_10"></a>Impôts locaux</h3>
<p><strong>Impôt sur les biens immobiliers (IBI)</strong></p>
<p>La propriété d’un bien immobilier donne lieu au paiement annuel de l’IBI (équivalent de la taxe foncière en France), calculé sur la base de la valeur cadastrale.<br class="autobr">Les conditions d’application de l’IBI (taux, déductions et exonérations) dépendent dans une large mesure des compétences exercées par chaque commune. Pour plus d’information, il est nécessaire de contacter les services compétents de la commune concernée.</p>
<p>La taxe d’habitation française n’a pas d’équivalent en Espagne. En revanche, le revenu « fictif » tiré de la libre disposition d’un immeuble non loué et non affecté à la résidence principale est susceptible d’une imposition à l’IRPF dans la catégorie des rentas imputadas (voir rubrique consacrée à l’impôt sur le revenu).</p>
<h3 class="spip"><a id="sommaire_11"></a>Les autres impôts et taxes en Espagne</h3>
<p>Les informations contenues dans cette rubrique ne sont pas exhaustives. La fiscalité espagnole comprend en effet divers autres impôts et taxes, à charge des opérateurs économiques ou des particuliers.</p>
<p>L’une des spécificités du système fiscal espagnol ressort de son caractère décentralisé, qui laisse aux communautés autonomes et aux collectivités locales d’importantes compétences normatives.</p>
<p>Par conséquent, pour toute information sur les conditions d’application d’un impôt cédé aux collectivités territoriales, il y a lieu de se rapprocher des services géographiquement compétents.</p>
<p><strong>Sources d’informations utiles sur la fiscalité espagnole</strong></p>
<p><strong>Le site internet du consulat général d’Espagne  </strong></p>
<p>Un certain nombre d’informations utiles concernant les obligations fiscales en Espagne sont mises à disposition des usagers sur le site internet du consulat général, sous la rubrique « Démarches administratives / Fiscalité ».  Une mise à jour régulière est assurée en tant que de besoin.</p>
<p>L’administration fiscale espagnole (AEAT) dispose d’un site internet qui propose un grand nombre d’informations à caractère fiscal, aussi bien concernant les impôts d’Etat que la fiscalité autonomique et locale. Ces informations sont disponibles à partir du lien suivant : <a href="http://www.agenciatributaria.es/AEAT.internet/Inicio.shtml" class="spip_url spip_out auto" rel="nofollow external">http://www.agenciatributaria.es/AEAT.internet/Inicio.shtml</a></p>
<p><sub>Mise à jour : novembre 2015</sub></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/fiscalite/article/fiscalite-du-pays). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
