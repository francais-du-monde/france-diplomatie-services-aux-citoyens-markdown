# Fiscalité

<h2 class="rub22977">Fiscalité du pays</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/cambodge/fiscalite-22977/#sommaire_1">Présentation </a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/cambodge/fiscalite-22977/#sommaire_2">Année fiscale </a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/cambodge/fiscalite-22977/#sommaire_3">Barème de l’impôt </a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/cambodge/fiscalite-22977/#sommaire_4">Quitus fiscal </a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/cambodge/fiscalite-22977/#sommaire_5">Solde du compte en fin de séjour </a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/cambodge/fiscalite-22977/#sommaire_6">Coordonnées des centres d’information fiscale </a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Présentation </h3>
<p><strong>L’Impôt sur les bénéfices</strong></p>
<p>L’impôt sur le bénéfice des entreprises vise les entreprises publiques ou privées exerçant une activité industrielle, artisanale, commerciale, de services ou d’exploitation des ressources naturelles.</p>
<p><strong>La taxe minimale</strong></p>
<p>La <i>loi sur la fiscalité de 1997</i> introduit un « impôt minimum », distinct de l’impôt sur les bénéfices pour les entreprises soumises au régime réel. A payer à la fin de l’exercice fiscal, il est égal à 1 % du chiffre d’affaires annuel, mais ne doit pas être acquitté s’il est inférieur au montant de l’impôt sur les bénéfices à verser en fin d’exercice.</p>
<p><strong>La taxe sur la valeur ajoutée</strong></p>
<p>Conformément aux <i>articles 59 et 76 de la loi sur la fiscalité de 1997</i>, toute entreprise soumise au régime réel et offrant des biens ou services imposables est soumise à la T.V.A. et doit se faire enregistrer en tant que telle auprès de la Direction des Impôts, dès le début de son activité. En outre, toute entreprise produisant des biens et dont le chiffre d’affaires excède 125 millions de riels (soit environ 33 000 USD) ou offrant des services pour un chiffre d’affaires supérieur à 60 millions de riels (environ 16 000 USD) durant les trois derniers mois est redevable de la T.V.A. La collecte de la T.V.A. est un mécanisme à deux temps, à l’instar du système français. L’entreprise perçoit tout d’abord les 10 % correspondant à l’<i>output tax</i>, puis elle reverse cette somme déduite du montant total de la T.V.A. de ses propres achats (<i>input tax</i>) à la Direction des Impôts.</p>
<p>A noter : les biens et services destinés à l’exportation sont exonérés de la T.V.A., de même que la plupart des services publics marchands (service et matériel médicaux, poste, électricité, eau, transports publics, etc.).</p>
<p><strong>L’impôt sur le revenu</strong></p>
<p>L’impôt sur les salaires (articles 9 à 24 de la loi de finances de 1995) est un impôt mensuel. Il est exigible des personnes résidentes au Cambodge, quelle que soit l’origine de leur revenu, ainsi que des personnes qui ne résident pas au Cambodge mais qui perçoivent un revenu de source cambodgienne.</p>
<p><strong>La taxe spécifique sur certaines marchandises et services (TSCMS)</strong></p>
<p>La TSCMS est imposée également aux produits fabriqués au Cambodge, sur le prix de vente facturé par le fabricant tous frais et taxes compris, à l’exclusion de la TSCMS elle-même.</p>
<p><strong>La Patente (Business License Fee)</strong></p>
<p>La patente (décret loi n°22 du 14 juin 1985, article 39 de la loi de finances 1995), annuelle, vise l’existence d’une entreprise industrielle, artisanale ou commerciale au cours de la période d’imposition. L’entreprise nouvellement installée doit adresser à l’administration fiscale une déclaration d’existence. Les autres entreprises adressent au 1er janvier de chaque année une déclaration.</p>
<p><strong>Les pénalités de retard ou de non-paiement des taxes</strong></p>
<p>Tout retard ou non-paiement d’une taxe est sanctionné d’une surtaxe de 10 % de la somme due et de 2 % d’intérêt par mois écoulé. A noter que si l’impôt n’est pas versé dans les 15 jours suivant l’avis de la Direction des Impôts, une nouvelle surtaxe de 25 % sera exigée et encore une autre de 40 % si le débiteur ne produit pas de promesse de paiement.</p>
<p>Enfin, l’évasion fiscale est passible d’une amende pouvant atteindre jusqu’à 12,5 millions KHR, voire d’une peine de prison d’une durée de 5 ans maximum.</p>
<h3 class="spip"><a id="sommaire_2"></a>Année fiscale </h3>
<p>La période de l’année fiscale s’étend du 1er au 31 décembre</p>
<h3 class="spip"><a id="sommaire_3"></a>Barème de l’impôt </h3>
<p><strong>L’Impôt sur les bénéfices</strong></p>
<p>Le taux d’imposition dépend de l’activité de l’entreprise (<i>article 20 de la loi sur la fiscalité du 8 janvier 1997</i>) :</p>
<ul class="spip">
<li>taux standard de 20 % pour les sociétés ;</li>
<li>taux de 30 % pour les exploitants de ressources naturelles ;</li>
<li>taux progressif par tranches de bénéfice pour les entreprises individuelles : 0-20 % ;</li>
<li>taux de 5 % pour les compagnies d’assurance ;</li>
<li>taux de 9 % pour les entreprises bénéficiant d’une exonération dans le cadre du Code des Investissements.</li>
<li>exonération d’impôt pendant huit ans pour les entreprises bénéficiant du code des investissements si la qualité de leur projet le justifie.</li></ul>
<p>Certaines déductions sont autorisées : frais généraux de toute nature, amortissements réalisés, soutien financier des œuvres caritatives, etc. La liste des déductions autorisées figure dans les articles 11 à 19 de la loi sur la fiscalité du 24 février 1997.</p>
<p><strong>La taxe sur la valeur ajoutée</strong></p>
<p>Depuis le 1er janvier 1999, une taxe sur la valeur ajoutée au taux unique de 10 % remplace l’ancien système de taxe sur le chiffre d’affaires (TCA) pour les entreprises imposées au régime réel. Les sociétés imposées au régime forfaitaire ou simplifié demeurent soumises à la TCA, mais à un taux ramené à 2 %.</p>
<p><strong>L’impôt sur le revenu</strong></p>
<p>La base d’imposition est calculée <i>ad valorem</i> ; le barème est le suivant :</p>
<table class="spip">
<thead><tr class="row_first"><th id="idf69c_c0">Rémunération mensuelle  </th><th id="idf69c_c1">Soit en USD au taux de 3 800 KHR = 1 USD  </th><th id="idf69c_c2">Imposition  </th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="idf69c_c0">moins de 500 000 KHR</td>
<td headers="idf69c_c1">moins de 132 USD</td>
<td headers="idf69c_c2">0 %</td></tr>
<tr class="row_even even">
<td headers="idf69c_c0">de 500 001 à 1 250 000 KHR</td>
<td headers="idf69c_c1">de 132 à 329 USD</td>
<td headers="idf69c_c2">5 %</td></tr>
<tr class="row_odd odd">
<td headers="idf69c_c0">de 1 250 001 à 8 500 000 KHR</td>
<td headers="idf69c_c1">de 329 à 2 237 USD</td>
<td headers="idf69c_c2">10 %</td></tr>
<tr class="row_even even">
<td headers="idf69c_c0">de 8 500 001 à 12 500 000 KHR</td>
<td headers="idf69c_c1">de 2 237 à 3 289 USD</td>
<td headers="idf69c_c2">15 %</td></tr>
<tr class="row_odd odd">
<td headers="idf69c_c0">plus de 12 500 000 KHR</td>
<td headers="idf69c_c1">plus de 3 289 USD</td>
<td headers="idf69c_c2">20 %</td></tr>
</tbody>
</table>
<p>L’employeur acquitte l’impôt au nom de ses employés lors du dépôt de la déclaration obligatoire, par prélèvement à la source. En outre, l’employeur et le salarié sont responsables solidairement du paiement de l’impôt sur les salaires. Par ailleurs, la loi sur la fiscalité de 1997 prévoit dans son article 25 un système de retenues à la source pour les revenus tirés d’activités spécifiques. Ces retenues sont :</p>
<ul class="spip">
<li>au taux de 15 % pour les revenus correspondant aux services de gestion, conseil et sur les intérêts ou royalties perçus par une société ou une entreprise individuelle ;</li>
<li>au taux de 10 % pour les revenus provenant de la location d’un bien meuble ou immeuble ;</li>
<li>au taux de 5 % pour les intérêts d’un compte-épargne versés par une banque à une entreprise individuelle.</li></ul>
<p>De plus, tout paiement de source cambodgienne versé par un résident à une personne non-résidente se voit imposer une retenue à la source au taux de 15 % (article 27).</p>
<p><strong>La taxe spécifique sur certaines marchandises et services (TSCMS)</strong></p>
<p>La TSCMS (articles 17 à 30 de la loi de finances rectificative pour 1995 et 85 de la loi du 8 janvier 1997), vise certains produits et services limitativement énumérés.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Véhicules de plus de 2000cc (cat. 8703) : 30 % <br class="manualbr">- Essences, lubrifiants et véhicules de moins de 2000cc : 20 % <br class="manualbr">- Boissons, tabac, hôtel, services hôteliers et de loisirs et véhicules motorisés deux roues à partir de 125cc : 10 % <br class="manualbr">- Ventes intérieures de billets d’avions pour l’étranger : 2 % </p>
<p><strong>La Patente (Business License Fee)</strong></p>
<p>La patente est calculée de manière différente entre d’une part, l’industrie et le commerce et d’autre part, les services autres que la restauration et l’hôtellerie. Pour ce premier secteur, elle varie entre 15 000 KHR pour une entreprise réalisant moins de 7500 000 KHR de CA et 0,1% du chiffre d’affaires des entreprises réalisant plus de 100 millions de KHR de chiffre d’affaires. Pour les services, le taux varie de 15 000 KHR (pour moins de 3 000 000 KHR de CA) et un maximum de 0,25 % du chiffre d’affaires pour les entreprises réalisant plus de 40 millions de KHR de chiffre d’affaires.</p>
<h3 class="spip"><a id="sommaire_4"></a>Quitus fiscal </h3>
<p>Il n’est pas exigé de quitus fiscal avant de quitter le pays.</p>
<h3 class="spip"><a id="sommaire_5"></a>Solde du compte en fin de séjour </h3>
<p>Un expatrié français peut solder son compte en fin de séjour</p>
<h3 class="spip"><a id="sommaire_6"></a>Coordonnées des centres d’information fiscale </h3>
<p><strong>Direction des Impôts (ministère de l’Economie et des Finances)</strong><br class="manualbr">Rue 63<br class="manualbr">Phnom-Penh <br class="manualbr">Tél. : (855) 12 812 245 <br class="manualbr">Fax : (855) 23 125 963 <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/cambodge/fiscalite-22977/#taxdept#mc#forum.org.kh#" title="taxdept..åt..forum.org.kh" onclick="location.href=mc_lancerlien('taxdept','forum.org.kh'); return false;" class="spip_mail">Courriel</a></p>
<p><i>Mise à jour : janvier 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-cambodge-fiscalite-22977-article-convention-fiscale-111053.md" title="Convention fiscale">Convention fiscale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-cambodge-fiscalite-22977-article-fiscalite-du-pays-111052.md" title="Fiscalité du pays">Fiscalité du pays</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/cambodge/fiscalite-22977/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
