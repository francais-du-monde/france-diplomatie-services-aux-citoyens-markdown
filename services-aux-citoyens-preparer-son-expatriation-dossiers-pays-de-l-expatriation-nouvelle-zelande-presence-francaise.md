# Présence française

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/presence-francaise/#sommaire_1">Ambassade et consulat de France</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/presence-francaise/#sommaire_2">Autorités françaises dans le pays</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/presence-francaise/#sommaire_3">Associations dans le pays</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Ambassade et consulat de France</h3>
<p>Consulter notre <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/article/annuaire-des-representations-francaises-a-l-etranger" class="spip_in">annuaire des ambassades et consulats français à l’étranger</a>, ainsi que l’annuaire des <a href="http://www.ambafrance-nz.org/Consuls-honoraires" class="spip_out" rel="external">consuls honoraires</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Autorités françaises dans le pays</h3>
<h4 class="spip">Réseau français de coopération et d’action culturelle</h4>
<p>Les coordonnées des services du réseau de coopération et d’action culturelle français en Nouvelle-Zélande ainsi que la description de l’activité de ces services se trouvent dans la <a href="http://www.ambafrance-nz.org/-Cooperation-franco-neo-zelandaise-" class="spip_out" rel="external">rubrique culture du site internet de l’Ambassade</a> ou sur le site internet du réseau culturel : <a href="http://latitudefrance.diplomatie.gouv.fr/AnnuaireCarto.html" class="spip_out" rel="external">Latitude France</a>.</p>
<h4 class="spip">Economie : réseau mondial</h4>
<p><a href="http://export.businessfrance.fr/nouvelle-zelande/export-nouvelle-zelande-avec-notre-bureau.html" class="spip_out" rel="external">Business France</a>, l’Agence française pour le développement international des entreprises, <a href="http://export.businessfrance.fr/nouvelle-zelande/export-nouvelle-zelande-avec-notre-bureau.html" class="spip_out" rel="external">est présente en Australie et en Nouvelle-Zélande</a>. Sa mission est d’accompagner les entreprises françaises dans leur développement à l’international.</p>
<p>Les services économiques sont également présents. Ils sont une émanation de la Direction générale du Trésor et ont pour missions principales l’analyse macroéconomique et l’animation des relations économiques bilatérales entre la France et les pays où ils sont implantés.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.ambafrance-nz.org/-Relations-economiques-" class="spip_out" rel="external">Rubrique du service économique français en Nouvelle-Zélande</a> </p>
<h4 class="spip">Vos élus à l’Assemblée des Français de l’étranger</h4>
<p>Pour toute information sur l’Assemblée des Français de l’étranger et pour connaître les conseillers et les Sénateurs qui représentent les Français établis hors de France dans votre circonscription, vous pouvez consulter les sites Internet suivants :</p>
<ul class="spip">
<li>Assemblée des Français de l’étranger (<a href="http://www.assemblee-afe.fr/" class="spip_out" rel="external">AFE</a>)</li>
<li>Sénat au service des Français de l’étranger : <a href="http://www.senat.fr/expatries/dossiers_pays/nouvelle_zelande.html" class="spip_out" rel="external">dossier spécifique à la Nouvelle-Zélande sur le site du Sénat</a></li></ul>
<h4 class="spip">Députés des Français de l’étranger</h4>
<p>La révision constitutionnelle du 23 juillet 2008, publiée au Journal officiel du 19 avril 2011, introduit la représentation à l’Assemblée nationale des Français établis hors de France. La création de onze circonscriptions législatives à l’étranger permet aux Français expatriés d’élire leur député à l’Assemblée nationale à partir du scrutin de 2012.</p>
<p>Pour plus d’information : <a href="http://www.diplomatie.gouv.fr/fr/vivre-a-l-etranger/voter-a-l-etranger-20721/" class="spip_out">Voter à l’étranger</a></p>
<h3 class="spip"><a id="sommaire_3"></a>Associations dans le pays</h3>
<p><a href="http://www.francais-du-monde.org/section/nouvelle-zelande/" class="spip_out" rel="external">Association démocratique des Français à l’étranger - Français du Monde (ADFE-FdM)</a><br class="manualbr">Shackleton Road - Mount Eden - Auckland<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/presence-francaise/#adfenz#mc#yahoo.com.au#" title="adfenz..åt..yahoo.com.au" onclick="location.href=mc_lancerlien('adfenz','yahoo.com.au'); return false;" class="spip_mail">Courriel</a></p>
<p><strong>Auckland Accueil</strong><br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/presence-francaise/#info#mc#aucklandaccueil.org.nz#" title="info..åt..aucklandaccueil.org.nz" onclick="location.href=mc_lancerlien('info','aucklandaccueil.org.nz'); return false;" class="spip_mail">Courriel</a><br class="manualbr">Cette association, créée en 2004, offre tous les renseignements nécessaires à une bonne installation en Nouvelle-Zélande et propose de nombreuses animations pour les parents et leurs enfants.</p>
<p><a href="http://www.frenzschool.org.nz/" class="spip_out" rel="external">Frenz School</a><br class="manualbr">Private bag MBE109 - Auckland<br class="manualbr">Téléphone : [64] 9 446 09 09<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/presence-francaise/#contact#mc#frenzschool.org.nz#" title="contact..åt..frenzschool.org.nz" onclick="location.href=mc_lancerlien('contact','frenzschool.org.nz'); return false;" class="spip_mail">Courriel</a></p>
<p>Frenz School est une association caritative néo-zélandaise qui a pour mission la mise en place d’une éducation bilingue franco-anglaise en Nouvelle Zélande.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li>Liste d’associations socio-culturelles sur le site <a href="http://www.frogs-in-nz.com/tribu/Infos-Expats/Associations-socio-culturelles" class="spip_out" rel="external">Frogs-in-NZ</a></li>
<li>Notre thématique <a href="services-aux-citoyens-preparer-son-expatriation-en-savoir-plus-article-associations-des-francais-de-l-etranger.md" class="spip_in">Associations des Français de l’étranger</a></li></ul>
<p><i>Mise à jour : septembre 2015</i></p>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/presence-francaise/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
