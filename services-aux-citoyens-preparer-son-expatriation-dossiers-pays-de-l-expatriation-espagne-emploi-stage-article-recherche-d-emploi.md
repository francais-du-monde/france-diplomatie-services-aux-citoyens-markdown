# Recherche d’emploi

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/emploi-stage/article/recherche-d-emploi#sommaire_1">Outils pour la recherche d’emploi</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/emploi-stage/article/recherche-d-emploi#sommaire_2">Organismes pour la recherche d’emploi</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Outils pour la recherche d’emploi</h3>
<h4 class="spip">Journaux</h4>
<p><strong>Quotidiens nationaux</strong></p>
<ul class="spip">
<li>El País (le dimanche supplément "Negocios")</li>
<li><a href="http://www.todotrabajo.abc.es/" class="spip_out" rel="external">El ABC</a> (le dimanche supplément "Nuevo Trabajo")</li>
<li><a href="http://www.expansionyempleo.com/" class="spip_out" rel="external">El Mundo</a> (le dimanche supplément spécial économie)</li></ul>
<p><strong>Presse sur l’emploi de Madrid</strong></p>
<ul class="spip">
<li>El Mercado del Trabajo (publié le vendredi)</li>
<li><a href="http://www.segundamano.es/" class="spip_out" rel="external">El Segunda Mano</a> (publié les lundi, mercredi et vendredi)</li>
<li>El Mercado Laboral (publié le samedi)</li></ul>
<p><strong>Presse quotidienne et sur l’emploi de Barcelone</strong></p>
<ul class="spip">
<li><a href="http://www.lavanguardia.es/" class="spip_out" rel="external">La Vanguardia</a>, quotidien (supplément économie le dimanche)</li>
<li><a href="http://www.elperiodico.com/" class="spip_out" rel="external">El Periódico</a>, quotidien (supplément économie le dimanche)</li>
<li>Anuntis, deux fois par semaine</li>
<li><a href="http://www.laboris.net/" class="spip_out" rel="external">Laboris</a>, une fois par semaine</li></ul>
<p><strong>Les journaux régionaux</strong></p>
<ul class="spip">
<li>Segunda Mano (Madrid)</li>
<li>La Vanguardia (Catalogne)</li></ul>
<p>Consultez aussi les revues spécialisées :</p>
<ul class="spip">
<li>Segunda Mano, journal régional publié à Madrid (supplément du lundi, mercredi et vendredi) proposant offres et demandes d’emploi, offres de travail bénévole, etc.</li>
<li>Tabajo Empresa</li>
<li>Mercado de Trabajo (publié le vendredi)</li>
<li>Campus et Gaceta</li>
<li>Actualidad Económica (hebdomadaire qui fait paraître des annonces pour des postes de cadre).</li></ul>
<p>Les chaînes publiques de télévision proposent également des programmes spécifiques d’emploi : "Aqui hay trabajo", du lundi au vendredi à 9h00 sur la <a href="http://www.rtve.es/" class="spip_out" rel="external">RTVE</a>.</p>
<h4 class="spip">Sites internet</h4>
<p>Il existe de très nombreux sites généralistes sur l’emploi. Vous trouverez ci-dessous une liste non exhaustive des sites les plus importants :</p>
<ul class="spip">
<li><a href="http://www.infojobs.net/" class="spip_out" rel="external">Infojobs.net</a></li>
<li><a href="http://www.infoempleo.com/" class="spip_out" rel="external">Infoempleo.com</a> (offres d’emploi)</li>
<li><a href="http://www.monster.es/" class="spip_out" rel="external">Monster.es</a></li>
<li><a href="http://www.acciontrabajo.com/" class="spip_out" rel="external">Acciontrabajo.com</a></li>
<li><a href="http://www.bolsadetrabajo.com/" class="spip_out" rel="external">Bolsadetrabajo.com</a> (Bourse pour l’emploi)</li>
<li><a href="http://www.laboris.net/" class="spip_out" rel="external">Laboris.net</a> (Offres d’emploi)</li>
<li><a href="http://www.oficinaempleo.com/" class="spip_out" rel="external">Oficinaempleo.com</a> (Offres d’emploi tous secteurs)</li>
<li><a href="http://www.trabajos.com/" class="spip_out" rel="external">Trabajos.com</a> (Offres d’emploi tous secteurs)</li>
<li><a href="http://www.trabajo.org/" class="spip_out" rel="external">Trabajo.org</a> (Offres d’emploi tous secteurs)</li>
<li><a href="http://www.hacesfalta.org/" class="spip_out" rel="external">Hacesfalta.org</a> (Offres d’emploi dans le domaine social)</li>
<li><a href="http://www.segundamano.es/" class="spip_out" rel="external">Segundamano.es</a></li>
<li><a href="http://www.computrabajo.es/" class="spip_out" rel="external">Computrabajo.es</a></li>
<li><a href="http://www.opcionempleo.com/" class="spip_out" rel="external">Opcionempleo.com</a> (moteur de recherche d’emploi regroupant plus de 50 000 offres provenant de plus de 300 sites d’Espagne)</li></ul>
<p><strong>Travailler dans l’agriculture</strong></p>
<p><a href="http://www.infoagro.com/" class="spip_out" rel="external">www.infoagro.com</a></p>
<p>Les médias français et francophones publient également des offres d’emploi (voir la rubrique "Vie pratique cadre de vie ambiance pour un Français").</p>
<p><i>Le signalement d’organismes sur ce site ne saurait engager la responsabilité de la MFE quant à la qualité de leurs services.</i></p>
<h4 class="spip">Annuaires</h4>
<p>La <a href="http://www.lachambre.es/" class="spip_out" rel="external">Chambre de commerce franco-espagnole</a> édite un annuaire (payant) des sociétés à participation française implantées en Espagne, qu’il est possible de commander.</p>
<h3 class="spip"><a id="sommaire_2"></a>Organismes pour la recherche d’emploi</h3>
<p>Le Comité consulaire pour l’emploi de Barcelone, qui avait pour but d’aider gratuitement les ressortissants français installés en Catalogne, Aragon et Baléares, à trouver un emploi, <strong>est fermé depuis octobre 2009</strong>.</p>
<p>L’association française "Le Cercle des Français - Barcelone Accueil" <strong>peut répondre à vos questions relatives à l’installation et à l’emploi. Elle ne reçoit pas les candidatures, ni ne place les candidats. </strong>Aussi, il n’est pas utile de lui expédier votre curriculum-vitae.</p>
<p><a href="http://www.cercledesfrancais.es/accueil/index.html" class="spip_out" rel="external">Le Cercle des Français</a><br class="manualbr">Carrer Moià, 8, 3º<br class="manualbr">08006 Barcelona<br class="manualbr">Tél. 93 200 41 85</p>
<p><strong>Le réseau Eures</strong></p>
<p><a href="http://ec.europa.eu/eures/home.jsp?lang=fr" class="spip_out" rel="external">EURES</a> a pour vocation d’offrir des informations, des conseils et des services de recrutement/placement aux travailleurs et aux employeurs, ainsi qu’à tout citoyen désireux de tirer profit du principe de la libre circulation des personnes. EURES met à disposition un réseau de conseillers pour fournir les informations requises par les chercheurs d’emploi et les employeurs dans le cadre de contacts personnels.</p>
<p>Outre les informations sur le marché du travail et sur les conditions de vie et de travail locales, le site EURES offre la possibilité de consulter les offres d’emploi et le calendrier des manifestations liées à l’emploi dans le pays.</p>
<p><strong>Les agences pour l’emploi</strong></p>
<p>Le <a href="http://www.sepe.es/" class="spip_out" rel="external">SEPE (Servicio Público de Empleo Estatal)</a> est l’équivalent du Pôle emploi et des ASSEDIC français.</p>
<p>Dans le cadre de la libre circulation des travailleurs et au nom du principe d’égalité de traitement, le ressortissant d’un pays membre de l’Union européenne peut s’inscrire auprès de l’agence de l’emploi de sa localité et bénéficiera des mêmes droits qu’un ressortissant espagnol.</p>
<p>Pour trouver le bureau le plus proche de votre domicile en Espagne, consulter le site du SEPE  rubrique "contacto"</p>
<p>Siège à Madrid <br class="manualbr">Calle Condesa de Venadito n.9 <br class="manualbr">28027 - Madrid <br class="manualbr">Tel. : 91 585 98 88</p>
<p>Pour toute information, composer le 901 119 999.</p>
<p><strong>Les chambres franco-espagnoles de commerce et d’industrie</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Madrid</strong> <br class="manualbr">C/Ruiz de Alarcon, n°7 - 1° <br class="manualbr">28014 Madrid<br class="manualbr">Tél. : 91 522 67 42</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Barcelone</strong> : <br class="manualbr">Passeig de Gracia, 2 <br class="manualbr">08007 Barcelone <br class="manualbr">Tél. : 93 270 24 50</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Valence</strong><br class="manualbr">C/Baron de Carcer, 48, despacho 9F <br class="manualbr">46001 Valence <br class="manualbr">Tél. : 96 394 31 06</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Bilbao</strong><br class="manualbr">C/Alameda de Mazarredo, 15 6° <br class="manualbr">48003 Bilbao<br class="manualbr">Tél. : 94 423 90 83</p>
<p>Vous pouvez vous adresser au service emploi de la <a href="http://www.camarafrancesa.es/" class="spip_out" rel="external">Chambre de commerce et d’industrie de Barcelone</a>.</p>
<p>Si vous résidez en Catalogne, la Chambre propose un "atelier emploi" par mois sur de thèmes variés en relation avec la recherche d’emploi.</p>
<p><strong>Les chambres de commerce et d’industrie espagnoles </strong></p>
<p>Chaque province espagnole dispose au minimum d’une <a href="https://www.camaras.org/publicado/" class="spip_out" rel="external">chambre de commerce et d’industrie</a> située dans la capitale de la province. Elles disposent souvent d’une Bourse de l’emploi.</p>
<p><strong>Les agences privées (<i>agencias privadas de colocacion</i>)</strong></p>
<p>Elles pourvoient des postes temporaires dans les secteurs administratifs, techniques et informatiques. Ces agences peuvent vous demander de verser un montant minimal qui couvre les frais occasionnés par les démarches relatives à votre placement.</p>
<p>Il est possible de trouver leurs noms et leurs adresses dans les Pages Jaunes (<i>Paginas Amarillas</i>) de l’annuaire téléphonique, sous la rubrique "Empresas de Trabajo Temporal", ainsi que dans la presse de l’emploi.</p>
<p><strong>Les "Colegios profesionale"</strong></p>
<p>Ce sont des associations professionnelles destinées aux diplômés des principales disciplines. Pour exercer certaines professions (avocat, psychologue, médecin, etc.) en tant qu’indépendant, il faut être affilié au <a href="http://www.porticolegal.com/int/int_Colegios.html" class="spip_out" rel="external">colegio profesional</a> correspondant.</p>
<p><i>Mise à jour : avril 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/emploi-stage/article/recherche-d-emploi). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
