# Convention fiscale

<p>La France et la Norvège ont signé, le 19 décembre 1980, une convention en vue d’éviter les doubles impositions et d’établir des règles d’assistance réciproques en matière d’impôt sur le revenu et sur la fortune. Elle a été publiée au Journal Officiel le 25 octobre 1981.</p>
<p>Cette Convention a été modifiée par l’avenant du 14 novembre 1984 publié au Journal Officiel du 27 octobre 1985 et par l’avenant du 7 avril 1995 publié au JO du 17 octobre 1996.</p>
<p>Une convention internationale ayant primauté sur la loi interne, les dispositions de la loi du 29 décembre 1976 modifiant les règles de territorialité et les conditions d’imposition des Français de l’étranger (Journal Officiel du 30 décembre 1976) ne sont applicables que si elles ne contreviennent pas aux dispositions de la convention.</p>
<p>Avant d’analyser ce texte et de vous informer sur les modalités d’imposition des différents revenus catégoriels, vous trouverez ci-après des informations relatives à vos obligations vis-à-vis du service des impôts français. La dernière partie de ce chapitre est consacrée à la fiscalité applicable en Norvège.</p>
<h4 class="spip">Analyse de la Convention</h4>
<p>Cette Convention tend à éviter les doubles impositions qui pourraient résulter de l’application des législations des deux Etats, de prévenir l’évasion fiscale et d’établir des règles d’assistance administrative réciproques en matière d’impôt sur le revenu et sur la fortune.</p>
<p>Le texte de la Convention et de son avenant peut être obtenu à la Direction des Journaux Officiels :<br class="manualbr">par courrier : 26 rue Desaix, 75727 PARIS Cedex 15 ;<br class="manualbr">par télécopie : 01 40 58 77 80</p>
<p>Il est également consultable sur le site Internet de l’<a href="http://www.impots.gouv.fr/" class="spip_out" rel="external">administration fiscale</a>.</p>
<p>Les dispositions conventionnelles qui ont primauté sur les dispositions du droit interne, selon l’article 55 de la Constitution française répartissent entre les deux Etats, le droit d’imposer les revenus pour leurs résidents respectifs.</p>
<h4 class="spip">Champ d’application de la convention</h4>
<p>Cet accord a pour objet de protéger les résidents de chacun des Etats contractants en matière d’impôts prélevés directement sur le revenu et sur la fortune, quelque soit le mode de perception.</p>
<p>La convention trouve donc à s’appliquer aux résidents de ces Etats.</p>
<p>L’article 4, paragraphe 1 de la Convention précise que l’expression « résident d’un Etat » désigne toute personne qui, en vertu de la législation de cet Etat y est assujettie à l’impôt à raison de son domicile, de sa résidence, de son siège de direction ou de tout autre critère analogue.</p>
<p>Le paragraphe 2 du même article énumère des critères subsidiaires permettant de résoudre le cas de double résidence si l’assujettissement à l’impôt ne pouvait suffire.</p>
<p>Ces critères sont :</p>
<ul class="spip">
<li>le foyer d’habitation permanent ;</li>
<li>le centre de ses intérêts vitaux ;</li>
<li>le lieu de séjour habituel ;</li>
<li>à défaut, l’Etat dont elle possède la nationalité.</li></ul>
<p>Si une personne possède la double nationalité ou si elle ne possède la nationalité d’aucun d’eux, la question est tranchée d’un commun accord par les autorités des deux Etats contractants (article 4, paragraphe 2d).</p>
<h4 class="spip">Elimination de la double imposition</h4>
<p>La double imposition est évitée selon un dispositif prévu par l’article 24 de la Convention.</p>
<p>L’élimination de la double imposition pour les résidents de France qui perçoivent des revenus de source norvégienne, imposables dans cet Etat en application de la Convention, s’opère aux termes du paragraphe 1 de l’article 24 selon le régime de l’imputation, sur l’impôt français, d’un crédit d’impôt.</p>
<p>L’élimination de la double imposition pour les résidents de Norvège qui perçoivent des revenus imposables en France s’opère par une déduction, sur l’impôt perçu par la Norvège, d’un montant égal à l’impôt sur le revenu payé en France (article 24 paragraphe 2a).</p>
<p>Les revenus pour lesquels le droit d’imposer est dévolu à titre exclusif à la Norvège sont maintenus en dehors de la base de l’impôt français, réserve faite toutefois de leur prise en compte pour la détermination du taux effectif.</p>
<p>Cela revient à déterminer l’impôt "fictif" correspondant au total des revenus perçus (en France et en Norvège). L’impôt dû en France est égal à :</p>
<p>Impôt "fictif " x revenu net imposable en France / revenu mondial.</p>
<p>Au résultat ainsi obtenu peuvent être appliquées les réfactions prévues par la loi interne.</p>
<h4 class="spip">Modalités d’imposition des revenus catégoriels</h4>
<p>Cette analyse ne tient pas compte des dispositions conventionnelles relatives aux revenus de capitaux mobiliers.</p>
<h5 class="spip">Traitements, salaires, pensions et rentes</h5>
<p><strong>Rémunérations privées</strong></p>
<p>L’article 15, paragraphe 1 précise, sous réserves des dispositions des articles suivants, que les traitements et salaires d’origine privée qu’un résident reçoit au titre d’un emploi salarié sont imposables dans l’Etat où est exercée l’activité.</p>
<p>Le maintien de l’imposition dans l’Etat de résidence du bénéficiaire est prévu par le paragraphe 2 du même article sous réserve de trois conditions simultanément remplies :</p>
<ul class="spip">
<li>le séjour temporaire du bénéficiaire dans l’autre Etat ne dépasse pas une durée totale de 183 jours au cours de l’année fiscale écoulée ;</li>
<li>la rémunération est payée par un employeur qui n’est pas résident de l’Etat d’exercice ;</li>
<li>la rémunération ne doit pas être à la charge d’un établissement stable ou d’une base fixe de l’employeur dans l’Etat.</li></ul>
<p>Il résulte des dispositions du paragraphe 3 de l’article 15 de la Convention que les revenus professionnels des salariés employés à bord d’un navire, d’un aéronef en trafic international ou à bord d’un bateau servant à la navigation intérieure ne sont imposables que dans l’Etat où se trouve la direction effective de l’entreprise.</p>
<p><strong>Rémunérations publiques</strong></p>
<p>L’article 19, paragraphe 1 indique que les traitements, salaires et rémunérations analogues payés par un Etat ou une personne morale de droit public de cet Etat sont imposables dans l’Etat qui les verse.</p>
<p>L’article 19, paragraphe 2 précise que les pensions payées par un Etat ou une personne morale de droit public de cet Etat restent imposables dans cet Etat.</p>
<p>Toutefois, en vertu des dispositions du paragraphe 3 du même article, ces règles ne sont pas applicables aux rémunérations ou aux pensions versées au titre de services rendus dans le cadre d’une activité industrielle ou commerciale exercée par un Etat ou une personne morale de droit public.</p>
<p>Les sommes versées à ce titre sont imposées soit dans l’Etat d’exercice de l’activité (article 15 de la Convention), soit dans l’Etat de résidence du bénéficiaire (article 18 de la Convention).</p>
<p><strong>Pensions et rentes</strong></p>
<p>L’article 18, paragraphe 1 prévoit que les pensions de retraite de source privée ainsi que les rémunérations similaires sont imposables dans l’Etat dont le bénéficiaire est un résident.</p>
<p>Toutefois, le paragraphe 2 du même article prévoit que les pensions payées en application de la législation sur la Sécurité sociale d’un Etat restent imposables dans cet Etat.</p>
<p><strong>Etudiants, stagiaires, enseignants, chercheurs</strong></p>
<p>Les étudiants, les stagiaires d’un Etat qui séjournent dans l’autre Etat à seule fin d’y poursuivre leurs études ou leur formation n’y sont pas imposables si les sommes perçues pour couvrir leurs frais d’entretien, d’études ou de formation sont d’origine étrangère (article 20 paragraphe 1 de la Convention).</p>
<p>Il en est de même pour les rémunérations perçues par ces étudiants ou stagiaires si la durée des services, en rapport avec leurs études ou leur formation, n’excède pas une année ou si ces rémunérations sont nécessaires pour compléter les ressources dont ils disposent pour leur entretien (article 20 paragraphe 2).</p>
<p>L’article 21, paragraphe 1 précise que les rémunérations versées aux professeurs ou aux chercheurs résidents d’un Etat se rendant temporairement dans l’autre Etat en vue d’y exercer une activité pédagogique dans l’intérêt public pendant une période ne dépassant pas deux restent imposables dans l’Etat de résidence.</p>
<h5 class="spip">Autres catégories de revenus</h5>
<p><strong>Bénéfices industriels et commerciaux</strong></p>
<p>L’article 7, paragraphe 1 dispose que les entreprises industrielles et commerciales sont imposables sur le territoire où est situé l’établissement stable.</p>
<p>Bénéfices des professions non commerciales et des revenus non commerciaux</p>
<p>Aux termes de l’article 14, les revenus provenant de l’exercice d’une profession libérale ou d’autres activités indépendantes sont imposables dans l’Etat sur le territoire duquel se trouve l’installation permanente où s’exerce de façon régulière l’activité personnelle.</p>
<p>Les revenus que les professionnels du spectacle ainsi que les sportifs réalisent en cette qualité dans l’un des deux Etats, à titre indépendant, restent imposables dans l’Etat d’exercice de l’activité (article 17 de la Convention).</p>
<p>L’article 12, paragraphe 1 pose en principe que les revenus non commerciaux (redevances et droits d’auteur) sont imposables dans l’Etat de résidence du bénéficiaire.</p>
<p><strong>Revenus immobiliers</strong></p>
<p>L’article 6, paragraphe 1 dispose que les revenus des biens immobiliers y compris les bénéfices des exploitations agricoles ou forestières sont imposables dans l’Etat où sont situés ces biens.</p>
<p>Cette règle s’applique également aux gains provenant de l’aliénation desdits biens ou droits selon les dispositions de l’article 13, paragraphe 1.</p>
<p>En ce qui concerne les gains provenant de l’aliénation de tous biens autres que ceux visés aux paragraphes 1 à 3 de l’article 13, ils restent imposables dans l’Etat de résidence du cédant.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/norvege/fiscalite/article/convention-fiscale). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
