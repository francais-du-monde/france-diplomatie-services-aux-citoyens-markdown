# Communications

<h4 class="spip">Téléphone – Internet</h4>
<p>Les liaisons téléphoniques sont excellentes.</p>
<p>Le coût d’un appel depuis un téléphone portable français est exorbitant. Il est possible d’en acheter sur place ou d’en louer. Vous avez également la possibilité d’acquérir un téléphone à carte (d’un coût de 1 000, 3 000 ou 5 000 yens). Vous pourrez appeler l’étranger depuis une cabine (peu répandues cependant) ou depuis les grands hôtels internationaux. Les appels internationaux bénéficient de tarif réduit de 23 heures à 8 heures du matin.</p>
<p>Pour appeler de France : composer l’indicatif pays (0081) + indicatif de ville sans le zéro.</p>
<p>Pour appeler du Japon la France : composer l’indicatif pays (0033) + indicatif de la zone sans le zéro. L’accès au réseau internet est répandu.</p>
<p>Des cybercafés sont présents dans toutes les grandes villes du Japon</p>
<h4 class="spip">Téléphoner gratuitement par Internet </h4>
<p>Il est généralement très avantageux d’utiliser <strong>des logiciels de téléphonie sur IP</strong> (Skype, Google talk, Live messenger, Yahoo ! messenger, etc.) car les tarifs proposés à l’international sont souvent moindre que ceux des opérateurs locaux exploitant des lignes classiques, voire gratuits dans certaines conditions.</p>
<h4 class="spip">Poste</h4>
<p>Les liaisons postales sont très fiables. Les délais d’acheminement du courrier sont de quatre jours entre Paris et Tokyo, 6 de province à province, en tarif rapide, de deux à trois jours en service express.</p>
<p>Pour connaître les tarifs des envois postaux, consultez le site de la <a href="http://www.post.japanpost.jp/cgi-charge/index.php?lang=_en" class="spip_out" rel="external">poste japonaise</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/vie-pratique/article/communications). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
