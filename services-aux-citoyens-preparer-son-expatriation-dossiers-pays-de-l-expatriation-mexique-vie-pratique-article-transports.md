# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/vie-pratique/article/transports#sommaire_1">Importation de véhicule</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/vie-pratique/article/transports#sommaire_2">Permis de conduire</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/vie-pratique/article/transports#sommaire_3">Code de la route</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/vie-pratique/article/transports#sommaire_4">Assurances et taxes</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/vie-pratique/article/transports#sommaire_5">Achat et location</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/vie-pratique/article/transports#sommaire_6">Entretien</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/vie-pratique/article/transports#sommaire_7">Réseau routier</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/vie-pratique/article/transports#sommaire_8">Transports en commun</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Importation de véhicule</h3>
<p>Les touristes peuvent importer un véhicule au Mexique pour une durée maximale de 180 jours. Le véhicule importé devra être réexporté à l’issue du séjour.</p>
<p>Pour les résidents, l’importation de véhicules personnels et/ou de moto est interdite au Mexique. L’achat d’un véhicule sur place est possible.</p>
<p><strong>Pour en savoir plus </strong> : <a href="http://www.aduanas.sat.gob.mx/" class="spip_out" rel="external">site des douanes mexicaines</a> (en espagnol)</p>
<h3 class="spip"><a id="sommaire_2"></a>Permis de conduire</h3>
<p>Le permis local est nécessaire pour les résidents : il convient de s’adresser à la "delegación" (mairie d’arrondissement) la plus proche de votre domicile, au service "transito, expedicion de licencias", en présentant notamment son titre de résidence ainsi qu’un justificatif de domicile. Ce permis sera valable 3 ans et il vous en coûtera environ 37 €.</p>
<p><a href="http://www.setravi.df.gob.mx/" class="spip_out" rel="external">Secrétariat aux transports du district fédéral</a>, rubrique : « vehiculos particulares ».</p>
<p><a href="http://www.edomexico.gob.mx/" class="spip_out" rel="external">Portail du gouvernement de l’Etat de Mexico</a> tramites y servicios  catálogo de trámites  expedición de licencias o permisos para conducir vehiculos de servicio particular.</p>
<h3 class="spip"><a id="sommaire_3"></a>Code de la route</h3>
<p>La conduite s’effectue à droite, et le code de la route prévoit la priorité à droite. Dans la mesure où on peut obtenir sans examen le permis de conduire, le code de la route n’est pratiquement pas observé. Il est donc recommandé de conduire avec une très grande prudence, notamment à Mexico, où il n’est pas rare que les automobilistes ne respecte par les feux de signalisation et où aucune règle de priorité (priorité à droite notamment) n’est respectée.</p>
<p>La vitesse est réglementée à 60 km/h en ville, 80 km/h sur route et 110 km/h sur autoroute. Un règlement de circulation à Mexico a durci en 2007 les obligations (ex. port de la ceinture) et les sanctions (ex. abus d’alcool).</p>
<p>A Mexico, il est impératif de respecter les jours d’alternance de circulation fixés en fonction de la plaque minéralogique, afin de lutter contre la pollution. Les véhicules immatriculés à l’étranger ne peuvent pas circuler dans le district fédéral le vendredi.</p>
<p>Un contrôle anti-pollution semestriel existe pour les véhicules de plus de 2 ans.</p>
<p>En toutes circonstances, conduire prudemment, respecter les signalisations et s’armer de patience vis-à-vis des autres conducteurs. Les déplacements collectifs en autocars de première et de deuxième classes sont en général confortables et fiables. Toutefois, les voyages de nuit en voiture ou en autocar sont à proscrire.</p>
<h3 class="spip"><a id="sommaire_4"></a>Assurances et taxes</h3>
<p>Les assurances émises à l’étranger ne sont pas valables. Il est impératif de contracter une assurance mexicaine.</p>
<p>Il existe comme en France plusieurs formules d’assurance, mais il convient de privilégier les assurances tous risques : les primes annuelles d’assurances tous risques, avec franchise, vont de 500 à 3 300 USD suivant le type de véhicules.</p>
<h3 class="spip"><a id="sommaire_5"></a>Achat et location</h3>
<p>Les marques françaises Renault et Peugeot sont représentées, ainsi que les principales marques étrangères (européennes, américaines et asiatiques) : Chrysler, Mercedes, BMW, Ford, General Motors, Volkswagen, Nissan et Honda. Il convient d’éviter les véhicules non commercialisés sur place. Il est même recommandé d’acquérir un véhicule construit au Mexique.</p>
<table class="spip" summary="">
<caption>Exemples de coûts (prix TTC en € à partir de)</caption>
<tbody>
<tr class="row_odd odd">
<td>Peugeot 207 (5 portes essence)</td>
<td class="numeric ">10 750</td></tr>
<tr class="row_even even">
<td>Peugeot 208 (3 et 5 portes essence)</td>
<td class="numeric ">11 850</td></tr>
<tr class="row_odd odd">
<td>Peugeot 308 (5 portes)</td>
<td class="numeric ">19 130</td></tr>
<tr class="row_even even">
<td>Renault Sandero</td>
<td class="numeric ">9 750</td></tr>
<tr class="row_odd odd">
<td>Renault Fluence</td>
<td class="numeric ">12 350</td></tr>
</tbody>
</table>
<p>Il est généralement déconseillé d’acheter un véhicule d’occasion, ces derniers étant souvent mal entretenus.</p>
<p><strong>Location</strong></p>
<p>Il est possible, à condition d’être âgé d’au moins 21 ans, de louer des véhicules auprès de sociétés spécialisées (Avis, Budget, Hertz), en présentant un permis de conduire national ou international valide, une pièce d’identité et une carte de crédit. Il est prudent de vérifier que la police d’assurances est "tous risques" et qu’elle est en cours de validité. Il faut compter 60 euros environ par jour pour la location d’une berline.</p>
<h3 class="spip"><a id="sommaire_6"></a>Entretien</h3>
<p>Il faut compter de 1.500 à 2.500 pesos pour une révision classique.</p>
<h3 class="spip"><a id="sommaire_7"></a>Réseau routier</h3>
<p>Les routes asphaltées desservent la majeure partie du pays. Les routes ou autoroutes à péage présentent les meilleures garanties de sécurité. Il faut toutefois faire attention à la présence répétée de déformations, voire de nids-de-poule, même sur le réseau autoroutier. En outre, il faut se méfier de la présence extrêmement fréquentes de « gendarmes couchés » (« topes ») à l’entrée et dans les localités, pas toujours signalés ni visibles à l’avance. Il existe de bons services d’autocars de 1ère classe sur les principaux trajets mais ils sont peu utilisés par les étrangers.</p>
<p>Il vous est conseillé de consulter la <a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/mexique/" class="spip_in">fiche Mexique</a> du site Conseils aux voyageurs du ministère des Affaires étrangères.</p>
<p><strong>En cas d’accident</strong></p>
<p>En cas d’accident, les conducteurs peuvent être mis à la disposition de la justice jusqu’à conclusion de l’enquête. En cas d’insolvabilité, ils peuvent être placés en garde à vue et le véhicule peut être saisi. Quand il y a des personnes décédées ou blessées graves, le juge peut écrouer les automobilistes jusqu’à ce que les responsabilités respectives soient déterminées.</p>
<p>Après un accident, ne jamais prendre la fuite. Garder son calme, rester courtois et, en accord avec les protagonistes, appeler une patrouille de police ainsi que les assureurs des deux parties pour l’établissement du constat. Si l’accident est sérieux, ne déplacer les véhicules que sur ordre de la police.</p>
<h3 class="spip"><a id="sommaire_8"></a>Transports en commun</h3>
<p><strong>Réseau ferroviaire</strong></p>
<p>Le réseau est obsolète et désorganisé (non-respect des horaires, confort spartiate excepté en première classe). Le train qui dessert la ligne Chihuahua - Pacifico est une attraction touristique (« Barranca del Cobre »). Il vaut mieux l’emprunter avec peu d’argent sur soi.</p>
<p><strong>Transport aérien</strong></p>
<p>Il existe plus de 50 aéroports dans l’ensemble du pays. Les agglomérations de moindre importance sont desservies par des compagnies régionales. Il n’est pas certain que la maintenance de leurs appareils soit scrupuleusement respectée. L’opérateur historique Aeromexico est en concurrence sur les lignes intérieures avec deux compagnies « low cost », Interjet et Volaris qui offrent un service de qualité.</p>
<p><strong>Transports urbains</strong></p>
<p>En ville, les transports en commun (métro, autobus) sont pratiques et économiques (prix du ticket de métro : 3 pesos), mais très encombrés. Les expatriés utilisent de préférence le taxi ou le véhicule personnel.</p>
<p>Le <strong>métro de Mexico </strong>est très étendu avec 12 lignes (163 stations) et fonctionne de 5h à minuit. Il vaut mieux éviter de le prendre la nuit et aux heures de pointe (7h-9h et 15h-19h) ; il est recommandé de faire très attention à ses effets personnels.</p>
<p>Pour plus d’informations sur le <a href="http://www.metro.df.gob.mx/red/index.html" class="spip_out" rel="external">métro de Mexico</a>.</p>
<p>Depuis 2005, un <strong>metrobus</strong> été a mis en œuvre. Il s’agit d’un réseau de bus à grande capacité qui circule en site propre. Le réseau compte quatre lignes. Même s’il convient d’éviter les heures de pointe, ce système de transport est apprécié par ses usagers.</p>
<p>Le paiement se fait par carte électronique metrobus. Son coût est de 10 pesos. Elle est rechargeable. Le coût du trajet est de 6 pesos.</p>
<p>Pour plus d’informations sur le <a href="http://www.metrobus.df.gob.mx/mapa.html" class="spip_out" rel="external">metrobus de Mexico</a>.</p>
<p>Si vous prenez <strong>l’autobus ou le pesero</strong>, il vous faut de la monnaie. Le pesero est un minibus vert et blanc, sorte de taxi collectif, très pratique pour se déplacer sur les grandes artères, économique (2,50 pesos pour un trajet de moins de 5 km jusqu’à 5 pesos pour 12 km) mais souvent bondé et peu confortable.</p>
<p>Le <strong>taxi</strong> est un moyen de transport très usité et relativement bon marché. Les taxis possèdent tous un compteur (pensez à le vérifier systématiquement). Il est recommandé d’emprunter les taxis de « sitios » (radio-taxi), plus sûrs que les taxis que l’on hèle dans la rue. A l’aéroport, il est indispensable, pour votre sécurité, d’utiliser les compagnies de taxis autorisées dont vous pouvez acquérir les billets à l’intérieur des terminaux 1 et 2 de l’aéroport international de Mexico et dans ceux de province.</p>
<p><i>Mise à jour : septembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/vie-pratique/article/transports). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
