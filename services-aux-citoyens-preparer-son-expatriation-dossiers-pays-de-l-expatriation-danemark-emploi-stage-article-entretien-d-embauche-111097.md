# Entretien d’embauche

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/emploi-stage/article/entretien-d-embauche-111097#sommaire_1">Conduite de l’entretien</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/emploi-stage/article/entretien-d-embauche-111097#sommaire_2">Questions préférées des recruteurs</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/emploi-stage/article/entretien-d-embauche-111097#sommaire_3">Erreurs à éviter</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/emploi-stage/article/entretien-d-embauche-111097#sommaire_4">Après l’entretien</a></li></ul>
<p>L’entretien constitue naturellement l’étape suivant votre candidature à un emploi par lettre et/ou par formulaire. Il est difficile de généraliser sur le nombre d’entretiens à passer par les candidats potentiels, qui varie considérablement d’une entreprise à l’autre en fonction de la taille de celle-ci, du poste sollicité et du recours ou non à d’autres tests. Cependant, la norme semble être de deux à trois entretiens.</p>
<p>Durant l’entretien, votre interlocuteur essaiera d’évaluer vos compétences professionnelles ainsi que votre personnalité (aspect très important au Danemark). Il ou elle souhaitera vous entendre sur vos responsabilités antérieures, l’importance des budgets gérés et surtout, sur vos résultats. Vos réalisations antérieures et votre motivation pour le poste sont les aspects les plus importants à ce stade.</p>
<p>Prenez avec vous des photocopies de votre CV, de vos diplômes, des attestations d’employeurs et de tout autre document jugé utile. Dans certains cas, une photographie du candidat sera prise lors du premier entretien. N’en soyez pas surpris.</p>
<p>Les procédures de recrutement, bien qu’elles varient considérablement, prennent presque toujours moins de deux mois au Danemark.</p>
<p>Les tests psychologiques sont de plus en plus fréquents, 50 à 60 % des directeurs des ressources humaines les jugeant indispensables. Ce sont en particulier les grandes entreprises qui y ont recours.</p>
<p>Il existe différents types de tests mais la majorité consiste en des tests d’aptitude et des simulations.</p>
<p>Le recours à des centres d’évaluation se multiplie et l’éventail des emplois concernés s’élargit. Les centres d’évaluation pratiquent des tests reflétant des situations réelles, correspondant au poste à pourvoir. Les techniques utilisées sont des tests d’intelligence et d’aptitude, des études de cas, des interactions de groupe et des jeux de rôle. Les centres d’évaluation s’attachent en particulier à la manière dont le candidat se comporte dans certaines situations prédéfinies.</p>
<p><strong>En résumé </strong></p>
<p>Le recruteur examinera le candidat sur les aspects suivants :</p>
<ul class="spip">
<li>Qualifications, formation</li>
<li>Capacité à exécuter le travail et à faire avancer les objectifs de l’entreprise</li>
<li>Capacité à intégrer l’entreprise et sa culture Préparez-vous à l’entretien en recherchant des informations sur l’entreprise en consultant son site internet. Le recruteur sera impressionné si vous êtes capable de démontrer vos connaissances sur les activités de l’entreprise et que vous posez des questions intelligentes par rapport à la société. Si le recruteur ne vous voit pas, il vous évaluera par rapport à votre communication téléphonique : la clarté de votre discours, le ton de votre voix et le contenu de vos réponses.</li></ul>
<p><strong>L’essentiel à savoir</strong></p>
<p>Ayez <strong>confiance</strong>, ne vous donnez pas d’excuse, les employeurs préfèrent les <strong>attitudes positives, l’assurance, l’optimisme.</strong></p>
<p>Les <strong>capacités à la communication</strong> sont importantes, vous devez paraître <strong>plaisant, intéressé et intéressant</strong>. Vous pouvez poliment demander à votre interlocuteur de parler plus doucement ou d’expliciter une question.</p>
<h3 class="spip"><a id="sommaire_1"></a>Conduite de l’entretien</h3>
<h4 class="spip">Apparence et attitude</h4>
<p>Soignez particulièrement votre apparence.</p>
<p>80% de l’impression des recruteurs est basée sur le visuel. L’aspect soigné est de rigueur, même si au Danemark les relations sont moins formelles qu’en France. Les codes vestimentaires danois sont à peu près similaires aux codes français lors d’une première rencontre, même si les tenues vestimentaires sont moins classiques. Il faut savoir rester simple et sobre, c’est la règle d’or au Danemark.</p>
<p>Faites attention à votre attitude et à votre comportement.</p>
<p>Faites attention à la façon dont vous vous tenez : épaules en arrière et tête relevée montrent que vous êtes confiants sans avoir l’air trop sûr de vous. La modestie est une qualité que les danois apprécient tout particulièrement. Maintenez toujours un comportement positif et énergique tout en sachant rester humble. Il importe aussi d’être à l’écoute de votre interlocuteur, qualité très appréciée par les recruteurs danois. Il faut savoir que près de 70 % de la communication est non verbale, ainsi soignez vos réactions et votre attitude tout en restant vous-mêmes. Maintenez un contact visuel avec votre/vos interlocuteurs(s).Enfin et surtout, adaptez votre discours à celui du recruteur (volume, rapidité) et n’élevez pas la voix plus que de raison.</p>
<h4 class="spip">Négociation du salaire</h4>
<p>Dans le cadre d’un emploi ou d’un stage, vous devez pouvoir anticiper les questions concernant votre salaire ou vos indemnités. Le recruteur pourra vous poser les questions suivantes : combien gagniez-vous lors de votre précédent travail ? Quelles sont vos prétentions salariales ?</p>
<p>Pour pouvoir formuler une réponse appropriée, cela présuppose de prendre en compte plusieurs éléments :</p>
<ul class="spip">
<li>Quelles sont les rémunérations proposées à ce type de poste en général ?</li>
<li>S’agit-il d’un fixe, d’un fixe + commissions, d’une avance sur commissions ?</li>
<li>Tenez compte des autres variables : ce poste était-il accompagné d’avantages tels que voiture de fonction, remboursements de frais, bonus, stock options, divers autres avantages (assurance, etc.)</li>
<li>Il faut savoir qu’en plus des indemnités de stage ou d’un salaire, d’autres bénéfices peuvent être négociés. Pour connaître les actualités du marché et avoir des prétentions salariales réalistes, consultez des sites Internet tels que <a href="http://www.jobindex.dk/" class="spip_out" rel="external">JobIndex.dk</a> (site en danois). Vous aurez un panel de salaires suivant la zone, les fonctions exercées, le niveau d’expérience et de formation.</li></ul>
<p>Dans le cadre d’un stage, <strong>être rémunéré n’est en aucun cas une obligation</strong>, l’entreprise peut vous payer mais elle n’est pas tenue de le faire.</p>
<p>Si vous n’êtes pas rémunéré, certains frais peuvent être pris en charge par l’entreprise (loyer, voiture, transport…). Ce sont des points à négocier avec l’employeur.</p>
<h3 class="spip"><a id="sommaire_2"></a>Questions préférées des recruteurs</h3>
<p>Voici une liste de questions susceptibles de vous être posées lors d’un entretien :</p>
<ul class="spip">
<li>Parlez-nous de vous. Que faites-vous dans votre temps libre ?</li>
<li>Qu´est-ce qui vous rend heureux d´aller travailler ?</li>
<li>Pourquoi avez-vous candidaté à ce poste ?</li>
<li>Pourquoi voulez-vous quitter/avez-vous quitté votre (dernier) emploi ?</li>
<li>Que savez-vous de cette entreprise ?</li>
<li>Qu´est ce qui va assurer votre réussite à ce poste ?</li>
<li>Que faites vous pour entretenir vos compétences professionnelles ?</li>
<li>Comment vous décrirait votre ancien chef ?</li>
<li>Comment vous décriraient vos anciens collègues ?</li>
<li>Avez-vous postulé à d´autres emplois ?</li>
<li>Comment réagissez-vous face à la pression ?</li>
<li>Quelle est votre source de motivation au travail ?</li>
<li>Qu´est ce qui vous démotive au travail ?</li>
<li>Quels sont vos points forts ?</li>
<li>Quels sont vos points faibles ?</li>
<li>Comment voyez-vous votre rôle dans une équipe de travail/qui doit collaborer autour d´un projet ?</li>
<li>Parlez-nous d´une tâche dans laquelle vous avez eu du succès au cours de votre carrière ou d’une réalisation dont vous êtes fier. <a href="http://www.diplomatie.gouv.fr/fr/" class="spip_url spip_out"></a></li>
<li>Quelles sont vos prétentions salariales ?</li>
<li>Avez-vous des questions à nous poser ?</li>
<li>Pourquoi devrions-nous vous embaucher ?</li></ul>
<p><i>Source : <a href="http://www.avisen.dk/" class="spip_out" rel="external">Avisen.dk</a> – 20 mai 2012</i></p>
<h3 class="spip"><a id="sommaire_3"></a>Erreurs à éviter</h3>
<p>Quelques recommandations sur ce qu’il ne faut pas faire :</p>
<ul class="spip">
<li>Eviter de faire des commentaires négatifs ou désobligeants sur vos employeurs anciens ou actuels ou sur vos collègues anciens ou actuels.</li>
<li>Soyez ponctuel !</li>
<li>Eviter les questions portant sur le salaire dès le premier entretien. Laissez l’employeur vous en parler.</li>
<li>Eviter de poser des questions sur les jours de congés : vacances, jours fériés, maladie, etc. lors du premier entretien.</li>
<li>Eviter d’énumérer d’emblée une liste de tâches que vous ne souhaitez pas faire !</li>
<li>Eviter de demander si l’entreprise a d’autres domaines d’activités…</li>
<li>Eviter de dire : "Je n’ai aucun point faible".</li>
<li>Eviter d’embellir la réalité et de faire des déclarations malhonnêtes ou trompeuses.</li></ul>
<p><strong>Voici quelques conseils donnés par le journal Avisen.dk :</strong></p>
<ul class="spip">
<li>Ne soyez pas en retard à l´entretien et n’oubliez pas de contacter l’entreprise en cas de problème. A votre arrivée ne vous penchez pas en arrière sur votre chaise.</li>
<li>Ne dites jamais que la raison pour laquelle vous postulez est l´ennui d´être au chômage, le manque d´argent ou simplement parce que votre bonne copine travaille aussi dans cette entreprise.</li>
<li>Ne cherchez pas la compassion de votre interlocuteur en racontant que le monde entier est contre vous. Ne dites pas que vous avez été licencié d’un emploi parce que vos anciens chefs et collègues étaient bêtes et injustes.</li>
<li>Ne vous présentez pas en vêtements de sport si vous postulez à un emploi de bureau et de la même manière ne vous présentez pas en chemise et cravate si vous candidatez à un emploi manuel.</li>
<li>Surveillez votre langage et ne faites jamais de plaisanteries mal placées.</li>
<li>Ne dites pas que vous n´êtes pas matinal lorsqu´on vous questionne sur vos points faibles. Ne dites pas non plus que vous êtes le meilleur en tout.</li>
<li>Ne vous offusquez pas d´être interrogé sur vos loisirs.</li>
<li>N’exagérez pas vos compétences et ne refusez pas d´expliquer comment vous avez acquis vos connaissances. Ne dites pas que vous êtes ami avec le chef du groupe alors que vous ne l´avez rencontré qu´une fois.</li>
<li>N’exigez pas un trop haut salaire, un jour libre dans la semaine ou encore des avantages en nature. N´oubliez pas de montrer avant tout votre intérêt pour l´entreprise dans laquelle vous souhaitez être engagé.</li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Après l’entretien</h3>
<ul class="spip">
<li>Lettre de remerciements
<br>— <a href="http://www.diplomatie.gouv.fr/fr/IMG/docx/Lettre_de_remerciements_1_cle03daf1.docx" class="spip_in" type="application/vnd.openxmlformats-officedocument.wordprocessingml.document">Exemple n°1</a>
<br>— <a href="http://www.diplomatie.gouv.fr/fr/IMG/docx/Lettre_de_remerciements_2_cle03c543.docx" class="spip_in" type="application/vnd.openxmlformats-officedocument.wordprocessingml.document">Exemple n°2 – (si vous êtes convoqué à un second entretien)</a></li>
<li><a href="http://www.diplomatie.gouv.fr/fr/IMG/docx/Lettre_de_remerciements_-_accepter_l_offre_cle0777f9.docx" class="spip_in" type="application/vnd.openxmlformats-officedocument.wordprocessingml.document">Lettre pour décliner une offre d’emploi</a></li>
<li><a href="http://www.diplomatie.gouv.fr/fr/IMG/docx/Lettre_de_remerciements_-_decliner_l_offre_cle0c3d23.docx" class="spip_in" type="application/vnd.openxmlformats-officedocument.wordprocessingml.document">Lettre pour accepter une offre d’emploi</a></li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/emploi-stage/article/entretien-d-embauche-111097). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
