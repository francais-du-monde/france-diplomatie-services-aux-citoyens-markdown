# Déménagement

<p>Le recours à un transitaire est nécessaire. Le déménagement peut s’effectuer, soit par voie maritime (compter six à huit semaines, dédouanement compris), soit par avion (un mois environ, dédouanement compris).</p>
<p><a href="http://www.au-senegal.com/IMG/pdf/guide-douanier.pdf" class="spip_out" rel="external">Guide douanier du voyageur au Sénégal</a>.</p>
<p>S’agissant d’un déménagement international, il est primordial de s’entourer d’un maximum de garanties en faisant appel à un professionnel disposant de certifications reconnues (marque NF Service, ISO 9002, etc.)</p>
<p>En France, l’Association française des déménageurs internationaux (AFDI) devenue <a href="http://www.fidi-france.com/fidi-france" class="spip_out" rel="external">Fédération internationale des déménageurs internationaux - France</a> (FIDI) se porte garante de la qualité des prestations assurées par ses membres. Elle propose au particulier des renseignements pratiques ainsi que des adresses pour organiser un déménagement à l’étranger.</p>
<p>Pour en savoir plus : <a href="services-aux-citoyens-preparer-son-expatriation-douanes.md" class="spip_in">Douanes</a>.</p>
<p><a href="http://www.csdemenagement.fr/" class="spip_out" rel="external">Chambre syndicale du déménagement</a><br class="manualbr">Téléphone : 01 49 88 61 40 <br class="manualbr">Télécopie : 01 49 88 61 46<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/entree-et-sejour/article/demenagement-110052#contact#mc#csdemenagement.fr#" title="contact..åt..csdemenagement.fr" onclick="location.href=mc_lancerlien('contact','csdemenagement.fr'); return false;" class="spip_mail">Courriel</a></p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/senegal/entree-et-sejour/article/demenagement-110052). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
