# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/vie-pratique/article/transports-114265#sommaire_1">Démarches</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/vie-pratique/article/transports-114265#sommaire_2">Code de la route</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/vie-pratique/article/transports-114265#sommaire_3">Assurance et taxes</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/vie-pratique/article/transports-114265#sommaire_4">Achat et location</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/vie-pratique/article/transports-114265#sommaire_5">Immatriculation</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/vie-pratique/article/transports-114265#sommaire_6">Entretien</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/vie-pratique/article/transports-114265#sommaire_7">Réseau routier</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/vie-pratique/article/transports-114265#sommaire_8">Transports en commun</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Démarches</h3>
<p>Compte tenu de la complexité et de la longueur des démarches, il est recommandé de s’adresser à un prestataire extérieur pour vous accompagner.</p>
<p><strong>Importation de véhicule</strong></p>
<p>L’importation d’un véhicule se fait avec l’aide d’un transitaire en plusieurs étapes :</p>
<p>A l’arrivée du bateau, le consignataire contacte un commissionnaire en douane agréé qui le représente au service de la vérification des marchandises pour solliciter une visite à priori du véhicule.</p>
<p>Le consignataire donne mandat au commissionnaire en douanes pour récupérer les documents originaux ci-dessous auprès de l’agence qui représente le transporteur. Ces documents seront ensuite joints à la déclaration en douane :</p>
<ul class="spip">
<li>le titre de propriété du véhicule (original et copie), les copies de la carte d’identité ou permis de conduire, du matricule fiscal (à faire a la DGI), du passeport ;</li>
<li>l’original du connaissement délivré par le transporteur ;</li>
<li>la facture d’achat du véhicule ;</li>
<li>la liste de colisage.</li></ul>
<p>Le dossier est transmis par le commissionnaire au service de la recevabilité qui fait le suivi auprès des différents services de la Douane allant de la vérification de la taxation à la liquidation des droits de taxes.</p>
<p>Les droits de taxes ayant été collectés par les services accrédités de la Douane, un bon de sortie est délivré. Une fois en possession du bon de sortie, le commissionnaire accompagne le consignataire au service de la visite ou au magasin pour prendre livraison du véhicule.</p>
<p><i>Source : site des <a href="http://www.douane.gouv.ht/" class="spip_out" rel="external">douanes haïtiennes</a></i></p>
<p><strong>Permis de conduire</strong></p>
<p>Il convient de se faire faire un permis de conduire haïtien. Il est délivré par le Service de la circulation (Delmas 2, Port-au-Prince).</p>
<p>Pièces à présenter au service :</p>
<ul class="spip">
<li>Passeport ;</li>
<li>Permis de conduire français ;</li>
<li>Numéro d’immatriculation fiscale délivré par la Direction générale de l’immigration. Un bureau de la DGI est présent au service de circulation. Il peut donc être demandé sur place.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Code de la route</h3>
<p>La conduite s’effectue à droite. La priorité est en principe à droite, mais de facto elle est coutumière.</p>
<p>Le port de la ceinture de sécurité dans les véhicules et celui du casque pour les motocyclistes sont obligatoires.</p>
<h3 class="spip"><a id="sommaire_3"></a>Assurance et taxes</h3>
<p><strong>Une obligation légale</strong></p>
<p>L’obligation d’assurer tout véhicule terrestre à moteur concerne à la fois le propriétaire et les personnes qui en ont temporairement la garde.</p>
<p>Le contrôle de la souscription à l’assurance devra être effectué par l’OAVCT (Office Assurance Véhicule Contre-tiers).</p>
<p>Le contrat conclu entre l’OAVCT et un propriétaire de véhicule est un <strong>contrat d’assurance Contre Tiers</strong>, au profit d’une éventuelle victime appelée Tiers. Dans ce contrat, toute autre personne est considérée comme étant tiers.</p>
<p>Toutes les personnes qui possèdent un ou plusieurs véhicules terrestre à moteur sont obligées de souscrire un contrat d’assurance avec l’OAVCT pour chaque véhicule.</p>
<p>L’Assurance véhicules contre tiers couvre seulement les accidents de la circulation.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.oavct.gouv.ht/" class="spip_out" rel="external">Office Assurance véhicule contre tiers</a> </p>
<h3 class="spip"><a id="sommaire_4"></a>Achat et location</h3>
<p>Plusieurs marques sont représentées sur place : Toyota, Hyundai, Nissan, Jeep, Porsche, Mitsubishi.</p>
<p>Un 4x4 de ville est suffisant pour la capitale et les principales villes ; sur le reste du réseau routier, un véhicule 4x4 à essieux élevés est conseillé en raison du mauvais état des routes.</p>
<p>A titre d’exemple, le coût de location pour un véhicule 4x4 Nissan avec chauffeur (hors carburant) est compris entre 100€ et 150€ par jour.</p>
<h3 class="spip"><a id="sommaire_5"></a>Immatriculation</h3>
<p><strong>Procédure générale relative à la demande de plaques</strong></p>
<ul class="spip">
<li><strong>Etape I : Direction / Service de la circulation et de la police routière</strong>
<br>— Contrôle des documents du véhicule. Pièces à soumettre : carte d’enregistrement du véhicule ou certificat de douane ou certificat de vente délivré par le concessionnaire, police d’assurances, matricule fiscal, copie du passeport ;
<br>— Contrôle de l’état physique du véhicule ;
<br>— Emission de la fiche d’autorisation d’immatriculation</li>
<li><strong>Etape II : DGI (vérification des documents pour l’enregistrement des données informatiques)</strong><br class="manualbr">Pièces à fournir :
<br>— Fiche d’autorisation d’immatriculation - Carte d’enregistrement et police d’assurance valide du véhicule ;
<br>— Autorisation du transporteur public délivrée par la Direction de la circulation et de la police routière (DCPR), le cas échéant ;
<br>— Déclaration définitive du propriétaire.</li></ul>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Etape III : DGI (enregistrement informatique des données et perception des frais pour l’émission de la plaque)</strong>
<br>— Enregistrement des données (cellule informatique) ;
<br>— Perception et impression de la nouvelle carte d’enregistrement ;
<br>— Signature de la carte d’enregistrement par les trois instances : SCV-OAVCT-DGI ;
<br>— Livraison de la plaque.</p>
<p><strong>Documents requis pour l’obtention de la plaque d’immatriculation (véhicule privé/ véhicule public (transporteur))</strong></p>
<ul class="spip">
<li>Matricule fiscal du propriétaire du véhicule ;</li>
<li>Demande de plaque à produire au lieu d’utilisation habituelle ;</li>
<li>Police d’assurance valide (OAVCT) - Carte d’enregistrement du véhicule ou certificat de vente ou certificat de douane délivré par le concessionnaire (pour les véhicules neufs) ;</li>
<li>Autorisation d’immatriculation délivrée par la Direction de la circulation et de la prévention routière (DCPR), au vu de l’état physique du véhicule (après inspection du véhicule) ;</li>
<li>Copie du passeport ;</li>
<li>Titre de séjour.</li></ul>
<h3 class="spip"><a id="sommaire_6"></a>Entretien</h3>
<p>Le coût d’entretien d’un véhicule est inférieur aux prix français en ce qui concerne la main d’œuvre avec une qualité de service moyenne ; le prix des pièces de rechange neuves est en revanche très élevé. Celles-ci ne sont pas disponibles immédiatement.</p>
<h3 class="spip"><a id="sommaire_7"></a>Réseau routier</h3>
<p>Hormis les grands axes, l’état des routes est aléatoire. Sur les pistes, un véhicule peut être gravement endommagé par les ornières. La plus grande vigilance est recommandée en temps de pluie et la nuit. Respecter une vitesse raisonnable permet de ne pas se laisser surprendre par des obstacles non signalés (camion en panne, troupeau, véhicule lent…) ou par l’éclatement d’un pneu.</p>
<p><strong>En cas d’accident grave, se rendre au plus vite au poste de police le plus proche.</strong> En cas d’arrestation, ne protestez pas et demandez à joindre votre consulat le plus rapidement possible (arguer de votre droit si cela vous était interdit).</p>
<p><strong>Il est fortement déconseillé de se déplacer de nuit.</strong></p>
<h3 class="spip"><a id="sommaire_8"></a>Transports en commun</h3>
<p>Il n’existe pas à proprement parler de services de transport public en Haïti. En revanche, un tissu dense de transports privés s’est développé (Taptap, taxis, bus collectifs etc.). Ils ne sont pas fiables, les véhicules étant souvent mal entretenus.</p>
<p>La surcharge et la vétusté des bateaux assurant les liaisons intérieures sont telles que des catastrophes se produisent régulièrement. Plusieurs navires ont fait naufrage les années passées causant la mort de nombreuses personnes. Il est donc formellement déconseillé d’emprunter ce type de moyen de transport. Haïti ne dispose pas de port de plaisance.</p>
<p>Les eaux territoriales haïtiennes ne sont pas surveillées ; elles sont réputées être un lieu de passage privilégié pour le trafic de drogue en provenance d’Amérique du Sud.</p>
<p>Il n’existe pas de compagnie aérienne nationale. Les vols intérieurs, de Port-au-Prince vers les grandes villes de province (principalement Cap-Haïtien, Jérémie et les Cayes), ainsi que les vols régionaux (vers la République dominicaine, la Jamaïque, les îles Turques-et-Caïques et les Bahamas), sont assurés par des compagnies privées dont les appareils sont anciens. Pour des raisons météorologiques, privilégier les vols en début plutôt qu’en fin de journée.</p>
<p><i>Mise à jour : août 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/vie-pratique/article/transports-114265). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
