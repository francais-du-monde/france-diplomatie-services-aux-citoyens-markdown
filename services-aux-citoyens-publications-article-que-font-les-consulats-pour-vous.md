# Que font les consulats pour vous ?

<p class="spip_document_93695 spip_documents spip_documents_center">
<img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L1000xH2611/que_fait_un_consulat_maj_juin2016_v2_cle47efe6-66736.png" width="1000" height="2611" alt=""></p>
<p class="document_doc">
<a class="spip_in" title="Doc:Version accessible , 524.7 ko, 0x0" href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/que_fait_un_consulat_maj_juin2016_v2_cle47efe6.pdf"><img width="18" height="21" alt="Doc:Version accessible , 524.7 ko, 0x0" src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L18xH21/pdf-7444b.png" style="vertical-align: middle;"></a>
<a href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/que_fait_un_consulat_maj_juin2016_v2_cle47efe6.pdf">Version accessible - (PDF, 524.7 ko)</a>
</p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/publications/article/que-font-les-consulats-pour-vous). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
