# Fiscalité

<h2 class="rub23456">Fiscalité du pays</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/fiscalite/#sommaire_1">Présentation</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/fiscalite/#sommaire_2">Année fiscale</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/fiscalite/#sommaire_3">Barème de l’impôt</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/fiscalite/#sommaire_4">Quitus fiscal </a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/fiscalite/#sommaire_5">Solde du compte en fin de séjour </a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/fiscalite/#sommaire_6">Coordonnées des centres d’information fiscale </a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Présentation</h3>
<p><strong>Date et lieu de dépôt des déclarations de revenus</strong></p>
<p>Le dépôt des déclarations s’effectue le second trimestre de l’année civile auprès de la Direction Générale des Impôts (DGI).</p>
<p><strong>Modalités de paiement des impôts pour un salarié, pour une activité non salariée</strong></p>
<ul class="spip">
<li>Pour les salariés l’impôt sur le revenu est effectué par prélèvement à la source</li>
<li>Les non-salariés s’acquittent d’un impôt minimum forfaitaire mensuel ainsi que d’un solde payé à la fin du mois d’avril, au titre de l’exercice précédent.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Année fiscale</h3>
<p>L’année fiscale correspond à l’année civile.</p>
<h3 class="spip"><a id="sommaire_3"></a>Barème de l’impôt</h3>
<p><strong>Impôt sur le revenu des personnes physiques</strong></p>
<p>L’impôt unique sur les traitements et salaires (IUTS) concerne tous les salariés résidant au Burkina. Il s’applique à tous les traitements, salaires, indemnités et émoluments servis par le secteur public et privé y compris les avantages en nature excepté ceux supportés par l’Etat, les collectivités locales (provinces, départements, communes) et les établissements publics n’ayant pas un caractère industriel ou commercial.</p>
<p>Il est appliqué un taux progressif (IUTS) par tranche de revenu en FCFA :</p>
<ul class="spip">
<li>de 0 à 10 000 : 2 %</li>
<li>de 10 100 à 20 000 : 5 %</li>
<li>de 20 100 à 30 000 : 10 %</li>
<li>de 30 100 à 50 000 : 17 %</li>
<li>de 50 100 à 80 000 : 19 %</li>
<li>de 80 100 à 120 000 : 21 %</li>
<li>de 120 100 à 170 000 : 24 %</li>
<li>de 170 100 à 250 000 : 27 %</li>
<li>plus de 250 000 : 30 %</li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Quitus fiscal </h3>
<p>Il n’est pas exigé de quitus fiscal avant de quitter le pays.</p>
<h3 class="spip"><a id="sommaire_5"></a>Solde du compte en fin de séjour </h3>
<p>Un expatrié du secteur privé peut solder son compte en fin de séjour.</p>
<h3 class="spip"><a id="sommaire_6"></a>Coordonnées des centres d’information fiscale </h3>
<p><strong>Ministère de l’Economie et des Finances</strong><br class="manualbr">Direction générale des impôts<br class="manualbr">Résidence Aziz - Avenue Kwamé N’Krumah<br class="manualbr">01 BP 119 Ouagadougou<br class="manualbr">Tél : (226) 30 89 85<br class="manualbr">Fax : (226) 31 27 70</p>
<p><i>Mise à jour : juin 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-burkina-faso-fiscalite-article-convention-fiscale.md" title="Convention fiscale">Convention fiscale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-burkina-faso-fiscalite-article-fiscalite-du-pays.md" title="Fiscalité du pays">Fiscalité du pays</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/fiscalite/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
