# Marché du travail

<p class="chapo">
      Le marché du travail luxembourgeois ne semble pas avoir été affecté par la crise économique, puisque, selon Eurostat, le pays jouit de l’un des plus faibles taux de chômage en Europe. Certains indicateurs, comme par exemple les créations d’emploi, se classent parmi les meilleures performances au niveau européen. De tels indicateurs globaux mèneraient donc à penser que tout va bien sur le marché du travail luxembourgeois puisque celui-ci affiche un dynamisme inégalé au niveau européen, même en temps de crise économique.
</p>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/emploi-stage/article/marche-du-travail-109894#sommaire_1">Secteurs à fort potentiel</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/emploi-stage/article/marche-du-travail-109894#sommaire_2">Rémunération</a></li></ul>
<p>Mais :</p>
<ul class="spip">
<li>la situation de la population active au Luxembourg s’est nettement détériorée au cours des dernières années, avec <strong>un taux de chômage qui a atteint des niveaux historiquement élevés (7 % en octobre 2013)</strong> ;</li>
<li>l’intégration des femmes sur le marché du travail semble rester difficile, comme le montre <strong>un des taux d’emploi féminin parmi les plus faibles de l’UE</strong> ;</li>
<li>même si le taux de chômage des jeunes est un des plus faibles de l’UE, <strong>le rapport entre le taux de chômage des jeunes et le taux de chômage global, un rapport de 3,5 à 1</strong>, montre que le Luxembourg est l’un des pays les moins performants dans l’UE ;</li>
<li><strong>la part des plus de 50 ans parmi les demandeurs d’emploi est passée</strong> de 13 % en 2000 à 16,5 % en 2005, puis à 21,2 % en 2009 et enfin <strong>23,2 % en 2010</strong>, dont 62 % restent au chômage pendant plus de 12 mois ;</li>
<li>la catégorie des chômeurs sans emploi depuis plus de 12 mois est la plus élevée dans toutes les catégories de formation (45 % pour les chômeurs avec une formation inférieure, 34 % pour ceux avec une formation moyenne et 27 % pour ceux avec une formation supérieure), "ce qui traduit <strong>une difficulté croissante pour les chômeurs à retrouver du travail</strong>" ;</li>
<li>un élément positif : le Luxembourg continue à faire partie du groupe de pays pour lesquels le taux de chômage de longue durée est plus de deux fois inférieur à la moyenne de l’UE.</li></ul>
<p>Le Luxembourg est obligé de faire appel à la main d’œuvre étrangère pour faire face aux besoins de son économie, principalement transfrontalière dont plus de la moitié est française (78 000 frontaliers français, 39 000 belges et 39 000 allemands). Les travailleurs non-luxembourgeois représentent environ 70% de la main d’œuvre occupée sur le territoire national.</p>
<h3 class="spip"><a id="sommaire_1"></a>Secteurs à fort potentiel</h3>
<ul class="spip">
<li>le service aux entreprises ;</li>
<li>le secteur financier (banques, assurances, etc.) ;</li>
<li>la santé et l’action sociale ;</li>
<li>la construction ;</li>
<li>l’hôtellerie, la restauration ;</li>
<li>les transports et les communications.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Rémunération</h3>
<p>Voir l’article <a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-luxembourg-emploi-stage-article-reglementation-du-travail.md" class="spip_in">Réglementation du travail</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/emploi-stage/article/marche-du-travail-109894). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
