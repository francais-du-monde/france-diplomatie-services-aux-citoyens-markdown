# Vie pratique

<h2 class="rub23543">Logement</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/vie-pratique/#sommaire_1">Conditions de location</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/vie-pratique/#sommaire_2">Electricité</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/vie-pratique/#sommaire_3">Electroménager</a></li></ul>
<p>Où se loger ?</p>
<p>Les quartiers résidentiels sont situés dans le centre de l’île, sur les hauteurs (Curepipe, Floréal) ou en bord de mer, dans le nord (Pointe aux Canonniers, Grand Baie, Pereybère, Cap Malheureux) et dans l’ouest (Rivière Noire).</p>
<p><strong>Grand Baie</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Prix moyen d’une chambre d’hôtel (chambre double)</td>
<td>MUR</td>
<td>Euros</td></tr>
<tr class="row_even even">
<td>Grand tourisme</td>
<td>5833</td>
<td>140</td></tr>
<tr class="row_odd odd">
<td>Moyen tourisme</td>
<td>3 000</td>
<td>72</td></tr>
<tr class="row_even even">
<td>Loyer mensuel dans un quartier résidentiel</td>
<td>MUR</td>
<td>Euros</td></tr>
<tr class="row_odd odd">
<td>3 pièces</td>
<td>30 000</td>
<td>732</td></tr>
<tr class="row_even even">
<td>5 pièces</td>
<td>45 000</td>
<td>1100</td></tr>
<tr class="row_odd odd">
<td>Villa</td>
<td>60 000 ou plus</td>
<td>1500 ou plus</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_1"></a>Conditions de location</h3>
<p>Le choix d’une résidence dépend souvent de la scolarisation des enfants. Le lycée Labourdonnais (jusqu’au baccalauréat) se trouve à Curepipe au centre de l’île à 20 km au sud-est de Port-Louis. L’École du Nord (primaire+collège) se trouve à 20 km au nord de Port-Louis. La localisation de ces établissements scolaires est souvent pris en considération. Il convient de signaler que les encombrements routiers sont importants du fait des migrations quotidiennes entre le plateau central et Port-Louis (7h30 - 9h00 vers Port-Louis, 16h00- 17h00 pour en sortir).</p>
<p>On peut trouver aussi bien une villa qu’un appartement. Il n’existe pas de studios. Les logements sont en général meublés. Le délai moyen de recherche à prévoir est de un à deux mois. Les baux sont établis pour un an renouvelable et l’on doit verser un à deux mois de caution pour le loyer à titre de garantie. Il est préférable d’établir un état des lieux avant la signature du bail.</p>
<p>Il convient également de noter qu’il est difficile de partager une colocation à Maurice dans la mesure où les Mauriciens sont peu habitués à ce type d’offre.</p>
<p>En cas de location d’un logement par une agence, la commission représente un mois de loyer, partagée entre propriétaire et locataire. Le programme RES (Real Estate Scheme) permet à un étranger l’acquisition d’un bien immobilier en pleine propriété à Maurice.</p>
<h3 class="spip"><a id="sommaire_2"></a>Electricité</h3>
<p>Le courant est alternatif (220 Volts). Les prises de courant sont de type britannique à trois fiches. Des adaptateurs pour prises françaises existent localement.</p>
<h3 class="spip"><a id="sommaire_3"></a>Electroménager</h3>
<p>Les cuisines sont en général équipées de plaques de cuisson et four, d’un réfrigérateur, d’un congélateur et souvent d’une machine à laver le linge. L’équipement électroménager est disponible sur place à des prix raisonnables, même s’ils sont généralement supérieurs à ceux pratiqués en France.</p>
<p>La climatisation est appréciable sur la côte et des appareils de chauffage d’appoint peuvent être utiles pour les logements situés sur le plateau central.</p>
<p><strong>Sécurisation</strong></p>
<p>De nombreux logements sont équipés de systèmes d’alarme. Les complexes de maisons ont en général un gardien jour et nuit, dont le coût est inclus dans le loyer.</p>
<p>Pour les villas, le gardiennage est peu utilisé ; certaines peuvent être gardées quand les occupants sont absents plusieurs semaines (le coût est d’environ 26 000 Rs/mois pour une villa 5/6 pièces).</p>
<p><i>Mise à jour : juin 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-maurice-vie-pratique-article-loisirs-et-culture.md" title="Loisirs et culture">Loisirs et culture</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-maurice-vie-pratique-article-communications.md" title="Communications">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-maurice-vie-pratique-article-transports-114416.md" title="Transports">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-maurice-vie-pratique-article-cout-de-la-vie-114415.md" title="Coût de la vie">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-maurice-vie-pratique-article-scolarisation.md" title="Scolarisation">Scolarisation</a></li>
<li><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/vie-pratique/article/sante" title="Santé">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-maurice-vie-pratique-article-logement-114412.md" title="Logement">Logement</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/vie-pratique/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
