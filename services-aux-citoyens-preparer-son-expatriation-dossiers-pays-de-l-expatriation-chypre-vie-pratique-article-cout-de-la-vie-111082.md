# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/vie-pratique/article/cout-de-la-vie-111082#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/vie-pratique/article/cout-de-la-vie-111082#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/vie-pratique/article/cout-de-la-vie-111082#sommaire_3">Alimentation</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/vie-pratique/article/cout-de-la-vie-111082#sommaire_4">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>Depuis le 1er janvier 2008, l’unité monétaire en République de Chypre, est l’euro (€). La partie Nord de l’île, qui échappe au contrôle de la République de Chypre, utilise la livre turque.</p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<p>Plusieurs banques étrangères sont représentées à Chypre, notamment les banques grecques (National Bank of Greece, Alpha Bank, Bank of Pireus) et les banques arabes (Arab Bank, Federal Bank of Middle East).</p>
<p>S’agissant des établissements français, la Société Générale (SGB-Cyprus) y est présente alors que le Crédit Agricole est présent de manière indirecte de par sa participation au capital social de National Bank of Greece.</p>
<p>En mars 2013, des mesures de contrôle de circulation des capitaux ont été mises en place afin d’éviter l’instabilité financière au lendemain des décisions prises par l’Eurogroupe dans le but de restructurer le système bancaire chypriote. Ces mesures seront progressivement levées selon une feuille de route présentée par le gouvernement chypriote. En conséquence, les voyageurs sont invités, avant le départ, à se renseigner sur le montant de la somme en espèces pouvant quitter l’île, le montant étant sujet à des changements de règlementation.</p>
<p>Les chèques et cartes bancaires sont utilisés. Toutes les grandes cartes de crédit (Visa, Eurocard, Mastercard, American express) sont acceptées à Chypre. On y trouve de nombreux distributeurs de billets dans les villes. Il convient de se montrer vigilent sur l’usage de la carte bancaire dans la partie nord, les sommes effectivement débitées pouvant se révéler supérieures au montant attendu. A noter que certaines enseignes n’acceptent que les paiements en liquide.</p>
<p>Les banques sont ouvertes le lundi de 8h30 à 13h30 et de 15h à 16h45 (fermées l’après-midi de mai à septembre) et du mardi au vendredi de 8h30 à 13h30.</p>
<h4 class="spip">Transfert de fonds</h4>
<p>Un résident étranger travaillant dans une société locale ne pourra transférer qu’une partie de son salaire (jusqu’à 20%), correspondant à son épargne, après contrôle de la Banque Centrale. Il devra de plus ouvrir un compte spécial de résident auprès d’un organisme agréé.</p>
<p>Un résident travaillant dans une société <i>offshore</i> peut transférer la totalité de son salaire, après paiement des impôts, sans contrôle de la Banque Centrale.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p><a href="http://www.visitcyprus.com/wps/portal/getting_to_cyprus/money_and_currency/%21ut/p/c5/04_SB8K8xLLM9MSSzPy8xBz9CP0os3hXN0fHYE8TIwMDo0BLAyNDAyOvMD9jIyNDM6B8JJK8u4ExUI2rkbOHiUeQgXewEQHd4SD7zOINcABHA4g8PvNR5C0MvSyA8t6mXl5GJkaGFkb49RsZ6Pt55Oem6hfkRhhkeuo6AgAVFzT8/dl3/d3/L2dJQSEvUUt3QS9ZQnZ3LzZfRUZBQVNJNDIwMDJROTAyMTAySlZOMzIyNjA%21/" class="spip_out" rel="external">Office de Tourisme de Chypre</a>.</p>
<h3 class="spip"><a id="sommaire_3"></a>Alimentation</h3>
<p>Les conditions d’approvisionnement sont satisfaisantes et ne posent pas de difficultés.</p>
<p>S’agissant des poissons et crustacés, les relevés de prix figurant dans le tableau ci-dessus ne concernent que des produits surgelés.</p>
<p><strong>Prix moyen d’un repas dans un restaurant</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Restaurant de première catégorie</td>
<td>de 20 à 25 €</td></tr>
<tr class="row_even even">
<td>Restaurant de catégorie moyenne</td>
<td>de 15 à 18 €</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_4"></a>Evolution des prix</h3>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/vie-pratique/article/cout-de-la-vie-111082). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
