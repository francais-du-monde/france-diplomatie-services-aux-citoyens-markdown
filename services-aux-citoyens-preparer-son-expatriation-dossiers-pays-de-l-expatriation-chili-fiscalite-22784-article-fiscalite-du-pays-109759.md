# Fiscalité du pays

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/fiscalite-22784/article/fiscalite-du-pays-109759#sommaire_1">Inscription auprès du fisc</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/fiscalite-22784/article/fiscalite-du-pays-109759#sommaire_2">Impôts directs et indirects</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/fiscalite-22784/article/fiscalite-du-pays-109759#sommaire_3">Quitus fiscal</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/fiscalite-22784/article/fiscalite-du-pays-109759#sommaire_4">Solde du compte en fin de séjour</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/fiscalite-22784/article/fiscalite-du-pays-109759#sommaire_5">Coordonnées des centres d’information fiscale</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Inscription auprès du fisc</h3>
<p>Les personnes résidentes, ou domiciliées au Chili, sont soumises à des impôts sur leurs revenus, quelle qu’en soit l’origine, et que la source des revenus se trouve à l’intérieur du pays ou à l’extérieur de celui-ci.</p>
<p>Sans préjudice des éléments susmentionnés, si un étranger installe son domicile ou sa résidence au Chili, il est grevé pendant les trois premières années de résidence seulement sur ses revenus de source chilienne, délai qui peut être prolongé dans certains cas.</p>
<p>Les personnes sans domicile, ni résidence au Chili sont grevées sur leurs revenus qui trouvent leur origine au Chili (biens situés sur le territoire chilien et activités développées en territoire chilien). Une personne est considérée domiciliée ou résidente au Chili à deux conditions :</p>
<ul class="spip">
<li>on peut présumer par ses activités qu’elle désire rester dans le pays sur une base permanente (domicile).</li>
<li>elle reste durant une année civile plus de six mois dans le pays ou si elle y reste pendant une période de deux ans.</li></ul>
<h4 class="spip">Année fiscale et période de paiement</h4>
<p>L’année fiscale correspond à l’année civile, du 1er janvier au 31 décembre. Pour les particuliers, la déclaration se fait sur le formulaire n° 29. Les déclarations doivent être établies au plus tard le 30 avril de chaque année. Il existe deux modalités de dépôt : par internet, mensuellement ou par dépôt de la déclaration dans une banque. Au Chili, aujourd’hui 98% des déclarations sur le revenu se font par internet.</p>
<p>Pour les salariés, <strong>la retenue s’effectue à la source chaque mois</strong>. Le paiement annuel lors de la remise de la déclaration est également possible.</p>
<p>Le Service des impôts internes dispose de trois ans pour réviser un impôt. Cette période est de six ans si le contribuable est considéré de mauvaise foi ou s’il n’a pas déposé de déclaration.</p>
<h3 class="spip"><a id="sommaire_2"></a>Impôts directs et indirects</h3>
<h4 class="spip">Les principaux impôts indirects au Chili</h4>
<h5 class="spip">Les droits de douane</h5>
<p>Les importations sont taxées par le droit <i>ad valorem</i> (6%) qui se calcule à partir de la valeur CIF de la marchandise. Ce taux moyen de 6% peut différer en fonction des accords de libre-échange signés par le Chili, actuellement au nombre de 60. Le taux est ramené à 0% pour une majorité de produits français qui entrent au Chili dans le cadre de l’Accord UE-Chili. C’est le service national des Douanes qui se charge de la perception de ces droits et impôts.</p>
<h5 class="spip">La TVA</h5>
<p>Une taxe de 19% s’applique sur la vente et autre transferts de propriété de biens, certaines prestations de services, licences software ou brevets. Les importations sont également soumises à la TVA.</p>
<h5 class="spip">Les droits de timbre</h5>
<p>Les documents ou actes qui engendrent une opération monétaire ou de crédit, mais également certains documents spécialement visés (lettre de change, billet à ordre…) sont taxés à 0,033% par mois.</p>
<h5 class="spip">Les impôts locaux</h5>
<p>Même si a priori la quasi-totalité des impôts au Chili sont nationaux, il existe des impôts municipaux. Les sources de revenus collectées par les municipalités sont :</p>
<ul class="spip">
<li><strong>L’impôt sur la propriété</strong> qui correspond à la taxe foncière en France. C’est un impôt direct qui est calculé en fonction de la valeur et du caractère des biens considérés.</li>
<li><strong>Les « patentes »</strong> qui désignent les permis nécessaires pour entreprendre toute activité commerciale nécessitant un local fixe. Il existe trois types de patentes au Chili : les commerciales (pour la vente de produits et services), les professionnelles (pour les professions libérales) et les industrielles (pour la production de biens). Elles doivent être renouvelées et peuvent être payées semestriellement.</li>
<li><strong>Les permis de circuler</strong> sont obligatoires pour tous les propriétaires de véhicules motorisés. Ils sont payés annuellement et dépendent de la valeur et du type de véhicule.</li>
<li>Les autres sources de financement : <strong>les « derechos »</strong> qui sont des impôts directs payés en contrepartie d’un service rendu par la municipalité (droit de faire de la publicité, service d’hygiène…), <strong>les redevances de concessions, les amendes</strong>.</li></ul>
<h5 class="spip">Les droits d’accises</h5>
<p>Ce sont des taxes additionnelles qui s’appliquent sur les boissons alcoolisées. Le taux est de 15% pour la bière, le vin, le champagne et le cidre et de 27% pour tous les alcools distillés (pisco, whisky, eaux de vie).</p>
<h4 class="spip">Le régime commun : détermination du revenu imposable</h4>
<p><strong>1)</strong> <strong>Le régime fiscal applicable aux sociétés de droit commun dépend de la structure juridique concernée, mais aussi de la nature de l’activité économique.</strong></p>
<p>Le régime fiscal distingue trois types d’impôt :</p>
<p><strong>L’impôt de <span class="caps">première catégorie</span> : « renta de capital »</strong></p>
<p>Il concerne les revenus issus d’activités commerciales, industrielles, agricoles ou minières ainsi que les revenus tirés des investissements, de l’immobilier ou de la pêche. La loi 20.630 publiée dans le JO le 27/09/2012 vient d’augmenter le taux de 17% à 20% pour les impôts à payer à compter du 1er octobre 2013, ce qui inclut les revenus de l’année 2012.</p>
<p><strong>L’impôt de <span class="caps">seconde catégorie</span> : « renta del trabajo »</strong><br class="manualbr">Il s’applique : <br>— aux revenus des salariés,<br>— aux revenus de travailleurs indépendants, des professions libérales, des directeurs de sociétés anonymes, des sociétés d’exercice libéral.</p>
<p>Les bénéficiaires de ces revenus sont soumis à un impôt personnel progressif dont le taux varie par tranche de rémunération de 0 à 40 %.</p>
<p>Pour les travailleurs salariés, cet impôt est déduit de la rémunération et versé mensuellement par l’employeur. Il est prélevé à la source sur la base du salaire brut, après déduction des contributions sociales.</p>
<p>Il est calculé selon le système d’unités mensuelles d’imposition (UTM) dont la valeur en pesos chiliens (CLP) est réévaluée chaque mois (1UTM = 40 286 CLP au mois de mai 2013, environ 65 €).</p>
<table class="spip">
<thead><tr class="row_first"><th id="id5ba0_c0">Revenus mensuels en UTM (mai 2013)</th><th id="id5ba0_c1">Revenus mensuels en pesos</th><th id="id5ba0_c2">Pourcentage imposable</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="id5ba0_c0">moins de 13,5 UTM</td>
<td headers="id5ba0_c1">moins de 543 860 CLP</td>
<td headers="id5ba0_c2">0%</td></tr>
<tr class="row_even even">
<td headers="id5ba0_c0">de 13,5 à 30 UTM</td>
<td headers="id5ba0_c1">de 543860 à 1208 580</td>
<td headers="id5ba0_c2">4%</td></tr>
<tr class="row_odd odd">
<td headers="id5ba0_c0">de 30 à 50 UTM</td>
<td headers="id5ba0_c1">de 1208580 à 2014 300</td>
<td headers="id5ba0_c2">8%</td></tr>
<tr class="row_even even">
<td headers="id5ba0_c0">de 50 à 70 UTM</td>
<td headers="id5ba0_c1">de 2014300 à 2820 020</td>
<td headers="id5ba0_c2">13,5%</td></tr>
<tr class="row_odd odd">
<td headers="id5ba0_c0">de 70 à 90 UTM</td>
<td headers="id5ba0_c1">de 2820020 à 3625 740</td>
<td headers="id5ba0_c2">23%</td></tr>
<tr class="row_even even">
<td headers="id5ba0_c0">de 90 à 120 UTM</td>
<td headers="id5ba0_c1">de 3625740 à 4834 320</td>
<td headers="id5ba0_c2">30,4%</td></tr>
<tr class="row_odd odd">
<td headers="id5ba0_c0">de 120 à 150 UTM</td>
<td headers="id5ba0_c1">de 4834320 à 6042 900</td>
<td headers="id5ba0_c2">35,5%</td></tr>
<tr class="row_even even">
<td headers="id5ba0_c0">plus de 150 UTM</td>
<td headers="id5ba0_c1">plus de 6042 900</td>
<td headers="id5ba0_c2">40%</td></tr>
</tbody>
</table>
<p><strong>L’impôt additionnel</strong></p>
<p>Il <strong>s’applique à l’intégralité des revenus chiliens des entreprises ou des personnes non résidentes</strong>. Les bénéfices générés par une société, dont les associés ou actionnaires sont non résidents, sont sujets à cet impôt quand ces bénéfices sont distribués sous forme de dividendes ou transférés à l’étranger.</p>
<p>Le taux en vigueur est de 35 % (taux qui inclut les 17 % 20%<span class="spip_note_ref"> [<a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/fiscalite-22784/article/fiscalite-du-pays-109759#nb4-1" class="spip_note" rel="footnote" title="à partir du 1er octobre 2013" id="nh4-1">1</a>]</span> d’impôt de <span class="caps">première catégorie</span>).</p>
<p>Les étrangers domiciliés ou résidant au Chili ne sont imposés que sur leurs revenus de source chilienne, durant les trois premières années suivant leur arrivée. Cette règle est reconductible une seule fois.</p>
<p>Selon le type de revenus, l’impôt peut faire l’objet d’une déclaration mensuelle (paiement à la Trésorerie générale de la République le mois suivant) ou annuelle.</p>
<p><strong> 2) L’imposition des personnes physiques </strong> :</p>
<p><strong>L’impôt de <span class="caps">seconde catégorie</span></strong></p>
<p>Déjà évoqué précédemment, c’est un impôt qui est déduit à la source de la rémunération du salarié par l’employeur.</p>
<p><strong>L’impôt global complémentaire</strong></p>
<p>C’est un impôt personnel, global, progressif et complémentaire qui se détermine et se paye une fois par an. Il s’applique sur les revenus provenant d’autres sources de revenus que les salaires des personnes résidentes (revenus provenant de placements financiers par exemples). Le taux varie de 0 à 40 %, selon les tranches applicables à l’impôt de seconde catégorie, mais sur une base annuelle.</p>
<p>Cet impôt est réglé au <i>Servicio de Impuestos Internos</i> (SII), au mois d’avril, par le contribuable qui bénéficie d’un crédit d’impôt correspondant au montant versé le cas échéant par l’employeur. Les impôts de catégorie sont donc déductibles des impôts globaux.</p>
<p>La situation de famille des personnes n’est pas prise en compte pour le calcul de l’impôt.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Le régime simplifié, une alternative intéressante</strong></p>
<p>Les entreprises de petite taille peuvent opter pour un régime fiscal simplifié dans le cadre duquel les impôts de première catégorie, additionnel ou global complémentaire sont évalués sur la base de toutes les distributions faites aux propriétaires, c’est-à-dire des revenus que ceux-ci s’attribuent. Lorsqu’une entreprise choisit le régime simplifié, elle s’engage dans cette voie pour une durée minimum de trois ans.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Les déductions : détermination du bénéfice imposable</strong></p>
<p><strong>Les charges d’exploitation</strong> ainsi que toute dépense engagée pour générer le revenu sont généralement déductibles dans l’année où elles sont comptabilisées. En matière automobile, seules les dépenses relatives aux véhicules utilitaires sont déductibles.</p>
<p><strong>Les taxes payées au Chili</strong> (à l’exception des impôts sur le revenu et sur la taxe foncière quand elle constitue un crédit d’impôt) sont déductibles du bénéfice imposable. Dans le cas où une entreprise investit à l’étranger, elle aura le droit de déduire les taxes payées à l’étranger sous certaines conditions fixées par la loi chilienne.</p>
<p><strong>La TVA de 19 %, est déductible</strong> dans les seuls cas où elle ne peut être créditée. Les pertes subies durant un exercice, peuvent être reportées et déduites des bénéfices suivants de manière indéfinie. Il n’existe pas de taux réduit.</p>
<p><strong>L’amortissement des actifs </strong>est une charge déductible. La méthode prise en compte est celle de l’amortissement linéaire sans valeur résiduelle. Toutefois, l’entreprise dispose d’une autre possibilité : « l’amortissement accéléré ». C’est un amortissement linéaire, la durée de vie comptable de l’actif étant réduite des deux tiers par rapport à celle normalement prévue.</p>
<p><strong>Les frais d’établissement </strong>peuvent être amortis sur une période maximum de six ans.</p>
<p>Les seules méthodes <strong>d’évaluation des stocks </strong>admises pour le calcul de l’impôt sont celles du FIFO (« First In, First Out ») et du coût unitaire moyen pondéré.</p>
<p>Les dépenses consacrées à la <strong>recherche et au développement sont entièrement déductibles </strong>l’année même de leur règlement ou réparties sur 6 années. Par ailleurs, il existe un crédit d’impôt pour les dépenses de recherche et développement.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>La loi sur les investissements : le régime particulier d’invariabilité fiscale pour les sociétés étrangères du Décret-loi 600</strong></p>
<p><strong>La Loi permet à toute entreprise étrangère ayant investi au Chili en choisissant le « DL 600 » d’opter pour le régime particulier de l’invariabilité fiscale, applicable pour une durée de 10 ans.</strong> Pendant cette période, l’entreprise a l’assurance que ses revenus seront imposés à un taux fixé à l’avance, quelles que soient les modifications susceptibles d’affecter le système fiscal chilien par ailleurs. A l’expiration de cette période, l’entreprise devra se conformer au régime commun d’imposition. Le régime de l’invariabilité fiscale peut être porté à 20 ans en cas de projets d’investissement d’un montant supérieur à 50 millions USD, ayant pour objectif des développements industriels ou extractifs, y compris les projets miniers.</p>
<p>L’entreprise ayant opté pour le régime de l’invariabilité fiscale peut renoncer à ce droit à tout moment. Elle est alors soumise au régime fiscal courant de manière automatique et irrévocable.</p>
<p>Le régime de l’invariabilité fiscale signifie une charge d’impôt effective totale pendant 10 ans de 42 % (impôt de <span class="caps">première catégorie</span> + impôt sur le rapatriement des bénéfices), soit sept points de plus que le régime de droit commun (35 %). Ce coût supplémentaire est assuré par les investisseurs étrangers pour bénéficier des avantages de l’invariabilité fiscale.</p>
<p>Concernant les impôts indirects, principalement la TVA (« IVA ») et les droits de douane, le DL 600 prévoit le choix pour l’investisseur étranger entre le régime commun et un régime particulier qui lui garantit l’invariabilité de ces impôts indirects.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Des avantages fiscaux qui font du Chili une plateforme régionale</strong> (article 41 D, loi du 13 novembre 2002)</p>
<p>En 2002 a été lancé le programme « Chili : plateforme d’investissements », dont l’objectif est d’inciter, par des avantages fiscaux, les entreprises étrangères à s’établir au Chili afin que le pays devienne le centre de ses opérations dans la région. Qui plus est, la société bénéficie ainsi du développement technologique et de la solidité des institutions politiques et économiques du Chili, ce qui peut ne pas être le cas dans le pays de l’investissement.</p>
<p><strong>Le statut de plateforme régionale donne à la société étrangère les avantages d’une société non domiciliée au Chili</strong>, c’est-à-dire qu’elle ne paie pas d’impôts sur les bénéfices réalisés à l’étranger. En revanche, toute activité au Chili (notamment les revenus provenant de dividendes perçus suite à des investissements effectués dans les sociétés anonymes installées au Chili) reste soumise à l’impôt sur le revenu à 35%.</p>
<p>Pour bénéficier des avantages fiscaux offerts par le statut de plateforme régionale, les sociétés étrangères doivent répondre à plusieurs conditions parmi lesquelles :</p>
<ul class="spip">
<li>être une société anonyme ouverte ou une société anonyme fermée dont les statuts permettent l’application des normes pour les SA ouvertes,</li>
<li>se consacrer exclusivement à des activités d’investissements,</li>
<li>renoncer au secret bancaire,</li>
<li>s’enregistrer au <strong>service des impôts internes</strong></li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Quitus fiscal</h3>
<p>Un quitus fiscal est exigé avant de quitter le pays.</p>
<h3 class="spip"><a id="sommaire_4"></a>Solde du compte en fin de séjour</h3>
<p>Un expatrié peut solder son compte en fin de séjour.</p>
<h3 class="spip"><a id="sommaire_5"></a>Coordonnées des centres d’information fiscale</h3>
<p>Coordonnées de l’<a href="http://home.sii.cl/" class="spip_out" rel="external">administration fiscale chilienne</a> :<br class="manualbr">Servicios de Impuesto Internos<br class="manualbr">Teatinos 120, piso 6<br class="manualbr">Santiago</p>
<p><i>Mise à jour : décembre 2013</i></p>
<p><span class="spip_note_ref">[<a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/fiscalite-22784/article/fiscalite-du-pays-109759#nh4-1" class="spip_note" title="Notes 4-1" rev="footnote">1</a>] </span>à partir du 1er octobre 2013</p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/fiscalite-22784/article/fiscalite-du-pays-109759). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
