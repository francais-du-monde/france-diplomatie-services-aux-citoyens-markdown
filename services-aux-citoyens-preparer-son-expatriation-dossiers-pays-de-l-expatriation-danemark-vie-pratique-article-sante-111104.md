# Santé

<p>Le système de santé est efficace. Les délais d’attente sont parfois importants dans le réseau public (un réseau privé très performant existe, mais a un coût exorbitant).</p>
<p>Les soins sont gratuits pour la médecine générale pour les bénéficiaires de la sécurité sociale danoise. Pour la médecine spécialisée, les coûts sont élevés et ne sont pris en charge que partiellement par la sécurité sociale. Voir Rubrique "protection sociale" pour plus de détails.</p>
<p>Pour un court séjour, pensez à vous munir de la Carte européenne d’assurance maladie (CEAM) à demander à votre caisse d’assurance maladie au moins 15 jours avant le départ.</p>
<h4 class="spip">Informations pratiques</h4>
<p>Numéros d’urgence : <strong>112</strong> ou <strong>1813</strong></p>
<h4 class="spip">Principaux hôpitaux</h4>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.rigshospitalet.dk/" class="spip_out" rel="external">Rigshospitalet (Hôpital universitaire)</a><br class="manualbr">Blegdamsvej 9 <br class="manualbr">2100 Copenhague O <br class="manualbr">Tél. 35 45 35 45 </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.amagerhospital.dk/" class="spip_out" rel="external">Amager Hospital</a><br class="manualbr">Italiansvej 1 <br class="manualbr">2300 Copenhague S <br class="manualbr">Tél. : 32 34 32 34 </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.bispebjerghospital.dk/" class="spip_out" rel="external">Bispebjerg Hospital</a><br class="manualbr">Bispebjerg Bakke 23 <br class="manualbr">2400 Copenhague NV <br class="manualbr">Tél. : 35 31 35 31</p>
<p><a href="http://www.nordsjaellandshospital.dk/" class="spip_out" rel="external">Nordsjællands Hospitalet</a><br class="manualbr">Dyrehavevej 29 <br class="manualbr">3400 Hillerød <br class="manualbr">Tél. : 48 29 48 29</p>
<p>Pour obtenir la liste de médecins francophones ainsi que la liste des hôpitaux, consultez la section consulaire du site de l’<a href="http://www.ambafrance-dk.org/" class="spip_out" rel="external">Ambassade de France au Danemark</a>.</p>
<p>Pour une présentation des risques épidémiologiques ainsi que des conseils sur la médecine au quotidien, consultez La page dédiée à la santé au Danemark dans la rubrique <a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/danemark/" class="spip_in">Conseils aux voyageurs du site du ministère des Affaires étrangères français</a></p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/vie-pratique/article/sante-111104). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
