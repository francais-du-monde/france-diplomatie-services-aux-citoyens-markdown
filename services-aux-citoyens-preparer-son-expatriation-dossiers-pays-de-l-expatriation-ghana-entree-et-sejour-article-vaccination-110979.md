# Vaccination

<p>La vaccination contre la fièvre jaune est exigée à l’entrée du pays sauf pour les enfants de moins d’un an.</p>
<p>Les vaccinations suivantes sont conseillées :</p>
<ul class="spip">
<li>pour les adultes : mise à jour des vaccinations contre la diphtérie, le tétanos, et la poliomyélite ; vaccinations contre la typhoïde, l’hépatite A, l’hépatite B.</li>
<li>pour les enfants : vaccinations recommandées en France en fonction de leur âge, par le ministère de la Santé, et en particulier : B.C.G. et hépatite B dès le premier mois, rougeole dès l’âge de neuf mois. Hépatite A possible à partir d’un an ; typhoïde à partir de cinq ans.</li></ul>
<p>Pour en savoir plus, lisez <a href="services-aux-citoyens-preparer-son-expatriation-sante.md" class="spip_in">notre article thématique sur la vaccination</a> ou consulter le site du <a href="http://www.cimed.org/" class="spip_out" rel="external">Centre d’informations médicales (CIMED)</a>.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/entree-et-sejour/article/vaccination-110979). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
