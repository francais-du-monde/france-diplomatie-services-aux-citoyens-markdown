# Recherche d’emploi

<h4 class="spip">Outils pour la recherche d’emploi</h4>
<p><strong>Journaux</strong></p>
<p>En particulier le journal des petites annonces <i>Expressz</i>, ainsi que les quotidiens hongrois <i>Népszabadsàg</i> et <i>Magyar Nemzet</i>.</p>
<p><strong>Sites internet</strong></p>
<p>Sites en hongrois :</p>
<ul class="spip">
<li><a href="http://allas.lap.hu/" class="spip_out" rel="external">Allas.lap.hu</a></li>
<li><a href="http://allas.wyw.hu/" class="spip_out" rel="external">Allas.wyw.hu</a></li></ul>
<p>Site en hongrois/anglais :</p>
<ul class="spip">
<li><a href="http://www.randstad.hu/" class="spip_out" rel="external">Randstad.hu</a></li>
<li><a href="http://internet.afsz.hu/" class="spip_out" rel="external">Internet.afsz.hu</a></li>
<li><a href="http://www.cvonline.hu/" class="spip_out" rel="external">Cvonline.hu</a></li></ul>
<p>Sites en anglais :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://eu.munka.hu/" class="spip_out" rel="external">Agence pour l’emploi</a></p>
<p>Sites en français :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.ccifh.hu/services/emploi/" class="spip_out" rel="external">Chambre de commerce et d’industrie franco-hongroise</a></p>
<h4 class="spip">Organismes pour la recherche d’emploi</h4>
<p>La Chambre de commerce et d’industrie franco-hongroise dispose d’un service emploi. Des offres d’emploi sont consultables sur son site Internet. Il est également possible d’envoyer son curriculum vitae.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.ccifh.hu/" class="spip_out" rel="external">Chambre de commerce et d’industrie franco-hongroise (CCIFH)</a><br class="manualbr">Adresse : Rákóczi út 1-3 .III - 1088 Budapest<br class="manualbr">Téléphone : [36] 1 317 82 68 - 318 85 13 <br class="manualbr">Télécopie [36] 1 338 41 74<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/emploi-stage/article/recherche-d-emploi#ccifh#mc#ccifh.hu#" title="ccifh..åt..ccifh.hu" onclick="location.href=mc_lancerlien('ccifh','ccifh.hu'); return false;" class="spip_mail">Courriel</a> </p>
<p>En Hongrie, l’activité de placement est exercée par les agences du service public pour l’emploi <i>Állami Foglalkoztatási Szolgálat</i> (ÁFSZ), ainsi que par des agences de placement privées autorisées. Toute personne peut recourir à leurs services.</p>
<p>Le portail de l’ÁFSZ permet d’accéder directement aux prestations de services du site <a href="https://ec.europa.eu/eures/page/index" class="spip_out" rel="external">EURES (Service européen pour l’emploi)</a>, qui offre également des informations sur les postes vacants en Hongrie.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/emploi-stage/article/recherche-d-emploi). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
