# Vaccination

<p>Il n’y a pas de vaccination exigée pour entrer sur le territoire haïtien, sauf pour les personnes provenant d’une zone infectée par la malaria.</p>
<p>Toutefois certaines vaccinations sont fortement recommandées par les autorités médicales françaises.</p>
<p><strong>Vaccinations systématiquement recommandées</strong></p>
<ul class="spip">
<li>Mise à jour des vaccinations incluses dans le calendrier vaccinal français</li>
<li>Hépatite A* (pour les enfants : à partir de l’âge de un an).</li></ul>
<p><strong>En fonction de la durée et des modalités du séjour</strong></p>
<ul class="spip">
<li>Typhoïde : si le séjour doit se dérouler dans des conditions d’hygiène précaires (pour les enfants : à partir de l’âge de deux ans)</li>
<li>Rage à titre préventif : pour des séjours prolongés en situation d’isolement (pour les enfants : dès qu’ils sont en âge de marcher)</li>
<li>Hépatite B : pour des séjours fréquents ou prolongés</li>
<li>Choléra : recommandé uniquement pour les personnels de santé concernés.</li></ul>
<p><strong>En fonction de la saison et des facteurs de risques individuels</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Grippe : pour tous les adultes et enfants (à partir de six mois) faisant l’objet d’une recommandation dans le calendrier vaccinal français, participant à un voyage notamment en groupe, ou en bateau de croisière.</p>
<p>Il est préférable de réaliser toutes les vaccinations nécessaires avant de partir, car une fois sur place, on peut rencontrer des difficultés d’approvisionnement.</p>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li>Site de l’<a href="http://www.ambafrance-ht.org/Examens-medicaux-et-vaccination" class="spip_out" rel="external">Ambassade de France en Haïti</a></li>
<li>Fiche <a href="http://www.pasteur-lille.fr/fr/sante/vaccin/desc_pays/HAITI.htm" class="spip_out" rel="external">Haïti</a> du site de l’institut Pasteur</li>
<li>Fiche Port-au-Prince sur le site du <a href="http://www.cimed.org" class="spip_out" rel="external">CIMED</a>.</li></ul>
<p><i>Mise à jour : août </i><i>2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/entree-et-sejour/article/vaccination). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
