# L’école maternelle, l’école primaire/élémentaire

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/enseignement/article/l-ecole-maternelle-et-l-ecole#sommaire_1">L’école maternelle</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/enseignement/article/l-ecole-maternelle-et-l-ecole#sommaire_2">L’école primaire/élémentaire</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>L’école maternelle</h3>
<p>Facultative, l’école maternelle est gratuite.</p>
<p>Les enfants peuvent y être accueillis à partir de trois ans jusqu’à six ans, dans la limite des places disponibles. Ils peuvent également être admis, dans la limite des places disponibles, s’ils ont atteint l’âge de deux ans le jour de la rentrée scolaire et à condition qu’ils soient physiquement et psychologiquement prêts à fréquenter l’école. L’école maternelle comprend trois sections : la petite, la moyenne et la grande section.</p>
<p>Renseignez-vous en cours d’année auprès de l’école ou de l’inspection académique.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.education.gouv.fr/pid35/ecole-maternelle.html" class="spip_out" rel="external">Education.gouv.fr</a></p>
<h3 class="spip"><a id="sommaire_2"></a>L’école primaire/élémentaire</h3>
<p>L’école primaire les classes suivantes : cours préparatoire (CP), cours élémentaires 1 et 2 (CE1 et CE2), cours moyens 1 et 2 (CM1 et CM2).</p>
<p>Vous pouvez scolariser votre enfant dans une école publique ou privée ou encore l’instruire à la maison.</p>
<p>Dans ce dernier cas, vous devez faire au préalable une déclaration au maire et à l’inspecteur d’Académie, renouvelée chaque année. Des contrôles seront effectués pour s’assurer du niveau d’instruction et de l’état de santé de l’enfant.</p>
<p><strong>L’inscription</strong> de votre enfant dans une école publique a lieu au plus tard au mois de juin précédant la rentrée scolaire. Dans certaines communes, les inscriptions se prennent dès le mois de janvier. Renseignez-vous au plus tôt sur les dates d’inscription. Contactez ou présentez-vous à la <strong>mairie de votre domicile </strong>avec les documents suivants :</p>
<ul class="spip">
<li>le livret de famille, une carte d’identité ou une copie d’extrait d’acte de naissance,</li>
<li>un document attestant que l’enfant a subi les vaccinations obligatoires pour son âge ou un document attestant d’une contre-indication,</li>
<li>un justificatif de domicile, indispensable pour toute inscription. Si vous ne connaissez pas encore votre future adresse en France, l’inscription ne se fera que lors du retour, quand vous pourrez justifier d’un domicile.</li></ul>
<p>La mairie vous délivre un <strong>certificat d’inscription </strong>indiquant l’école où est affecté votre enfant. En cas de difficultés pour l’inscription, adressez-vous aux services de l’Inspection académique de votre département.</p>
<p><strong>Vous vous présenterez ensuite à l’école.</strong> L’inscription de votre enfant sera enregistrée par le directeur sur présentation :</p>
<ul class="spip">
<li>du certificat d’inscription délivré par la mairie,</li>
<li>du livret de famille, d’une carte d’identité ou d’une copie d’extrait d’acte de naissance,</li>
<li>d’un certificat délivré par le médecin de famille attestant que l’état de santé de l’enfant est compatible avec la vie en milieu scolaire,</li>
<li>d’un document attestant que l’enfant a subi les vaccinations obligatoires pour son âge.</li></ul>
<p>S’il s’agit d’une inscription après un changement de domicile, prévoyez le certificat de radiation délivré par l’ancienne école (celui-ci est également délivré par les écoles françaises à l’étranger).</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.education.gouv.fr/pid34/ecole-elementaire.html" class="spip_out" rel="external">Education.gouv.fr</a></p>
<p><i>Mise à jour : avril 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/enseignement/article/l-ecole-maternelle-et-l-ecole). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
