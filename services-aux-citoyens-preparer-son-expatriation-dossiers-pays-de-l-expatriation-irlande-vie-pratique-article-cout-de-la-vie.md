# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/vie-pratique/article/cout-de-la-vie#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/vie-pratique/article/cout-de-la-vie#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/vie-pratique/article/cout-de-la-vie#sommaire_3">Budget</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/vie-pratique/article/cout-de-la-vie#sommaire_4">Alimentation</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/vie-pratique/article/cout-de-la-vie#sommaire_5">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>L’Irlande fait partie de la zone euro depuis le 1er janvier 1999 et a remplacé sa monnaie nationale, la livre irlandaise ou <i>punt</i> (IEP), par la monnaie européenne le 1er janvier 2002.</p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<p>Les transferts de fonds (salaires, cotisations, etc.) pour les Français expatriés relevant du secteur privé ne sont soumis à aucune procédure, ni déclaration particulière. Par ailleurs, ils peuvent solder leur compte sans aucune difficulté en fin de séjour.</p>
<p>Il n’existe pas en Irlande de banques commerciales françaises recevant des comptes de particuliers. En revanche, BNP Paribas est implantée en tant que banque d’affaires.</p>
<p>Les moyens de paiement utilisés sont le chéquier, la carte bancaire, la Laser Card (carte de paiement) et le virement. Les factures de téléphone ou encore d’électricité peuvent être réglées dans un bureau de poste, au téléphone par Laser Card ou encore par prélèvement automatique « direct debit ».</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  En matière de sécurité, vous pouvez consulter les <a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/irlande/" class="spip_in">Conseils aux voyageurs</a>. </p>
<h3 class="spip"><a id="sommaire_3"></a>Budget</h3>
<p>Le candidat à l’expatriation devra se garder de chercher à apprécier sa situation en tentant de comparer de façon partielle son pouvoir d’achat à l’étranger et en France. Cette appréciation doit se faire avant tout sur le montant global des dépenses correspondant à son budget dans le pays d’expatriation.</p>
<p>En 2010, Dublin occupe le 42ème rang dans la liste des villes les plus chères du monde, tandis que Paris se place au 17ème rang.</p>
<h3 class="spip"><a id="sommaire_4"></a>Alimentation</h3>
<p>Les conditions d’approvisionnement sont identiques à celles rencontrées en France. Il n’existe pas de différence majeure dans la gamme des aliments ou des produits proposés à la vente. Les produits alimentaires caractéristiques du goût français (fromages, produits laitiers, baguette, etc.) sont de plus en plus présents. La vente d’alcool est libre cependant il n’est pas possible d’acheter de l’alcool le dimanche avant 12h00.</p>
<p>Les prix sont globalement supérieurs à ceux pratiqués en France, en raison de l’insularité et des frais de transport induits (pour les produits frais notamment), ainsi que du caractère peu concurrentiel du marché local. Depuis la crise économique l’inflation a fortement diminué, en 2011 le prix des produits alimentaires est resté relativement stable avec une hausse de 1,1%.</p>
<p>En ce qui concerne la réglementation en matière de denrées alimentaires, l’Union européenne forme depuis le 1er janvier 1993 un seul espace commercial au sein duquel est garantie la libre circulation des marchandises. L’Irlande admet donc sans restriction et en franchise douanière les marchandises provenant des autres Etats membres.</p>
<h4 class="spip">Prix moyen d’un repas dans un restaurant</h4>
<p>Le prix d’un repas au restaurant est très variable selon la qualité de l’établissement. On trouve à Dublin un grand choix d’établissements proposant de la cuisine internationale. Le service est inclus dans l’addition ; le pourboire reste à la discrétion du client. Il est toutefois de bon ton de laisser un pourboire équivalent à 10% du prix du repas.</p>
<p><strong>Prix d’un repas</strong> (par personne) :</p>
<ul class="spip">
<li>dans un restaurant de première catégorie : de 75 à 100 euros</li>
<li>dans un restaurant de catégorie moyenne : de 25 à 40 euros</li>
<li>en restauration rapide : 6,50 euros</li></ul>
<h3 class="spip"><a id="sommaire_5"></a>Evolution des prix</h3>
<p>Depuis 2002, l’inflation irlandaise a considérablement ralenti, passant de 4,7% à 4% en 2003 puis 2,8% en 2007 et 2,5% en 2011. Les produits alimentaires et les boissons non alcoolisées ont vu leurs prix reculer, contrairement aux prix des transports et des loyers.</p>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/vie-pratique/article/cout-de-la-vie). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
