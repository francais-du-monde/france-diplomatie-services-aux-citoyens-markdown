# Présence française

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/presence-francaise/#sommaire_1">Ambassade et consulat de France</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/presence-francaise/#sommaire_2">Autorités françaises dans le pays</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/presence-francaise/#sommaire_3">Associations dans le pays</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Ambassade et consulat de France</h3>
<p>Consulter notre <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/article/annuaire-des-representations-francaises-a-l-etranger" class="spip_in">annuaire des ambassades et consulats de France à l’étranger</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Autorités françaises dans le pays</h3>
<h4 class="spip">Réseau français de coopération et d’action culturelle</h4>
<p>Les coordonnées des services du réseau de coopération et d’action culturelle français au Maroc ainsi que la description de l’activité de ces services se trouvent dans la <a href="http://www.ambafrance-gh.org/-S-C-A-C-" class="spip_out" rel="external">rubrique culture</a> du site internet de l’Ambassade ou sur le site internet du réseau culturel : <a href="http://latitudefrance.diplomatie.gouv.fr/AnnuaireCarto.html" class="spip_out" rel="external">Latitude France</a>.</p>
<h4 class="spip">Economie : réseau mondial</h4>
<p>Business France, l’Agence française pour le développement international des entreprises, dépend du bureau Business France de la Côte d’Ivoire. Sa mission est d’accompagner les entreprises françaises dans leur développement à l’international. Vous trouverez cependant une <a href="http://export.businessfrance.fr/ghana/export-ghana-avec-notre-bureau.html" class="spip_out" rel="external">page dévolue au Ghana sur le site Business France</a>.</p>
<p>Les services économiques sont également présents. Ils sont une émanation de la Direction générale du Trésor et ont pour missions principales l’analyse macroéconomique et l’animation des relations économiques bilatérales entre la France et les pays où ils sont implantés.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Site internet du <a href="http://www.tresor.economie.gouv.fr/Pays/ghana" class="spip_out" rel="external">service économique français au Ghana</a>.</p>
<h4 class="spip">L’Agence française de développement (AFD)</h4>
<p>Etablissement public au cœur du dispositif français de coopération, l’Agence française de développement (AFD) <a href="http://www.afd.fr/home/pays-d-intervention-afd/afrique-sub-saharienne/pays-afrique/ghana" class="spip_out" rel="external">est présente au Ghana</a> depuis 1986. Elle a concentré ses activités sur les grands projets économiques de développement d’infrastructures dans le pays, notamment dans les secteurs de la télécommunication, du transport, de l’énergie ainsi que dans l’agriculture et le développement rural.</p>
<h4 class="spip">Vos élus à l’Assemblée des Français de étranger</h4>
<p>Pour toute information sur l’Assemblée des Français de l’étranger et pour connaître les conseillers et les Sénateurs qui représentent les Français établis hors de France dans votre circonscription, vous pouvez consulter les sites Internet suivants :</p>
<ul class="spip">
<li><a href="http://www.assemblee-afe.fr/" class="spip_out" rel="external">Assemblée des Français de l’étranger (AFE)</a></li>
<li>Sénat au service des Français de l’étranger : dossier spécifique <a href="http://www.senat.fr/expatries/dossiers_pays/ghana.html" class="spip_out" rel="external">au Ghana</a> sur le site du Sénat.</li></ul>
<h4 class="spip">Députés des Français de l’étranger</h4>
<p>La révision constitutionnelle du 23 juillet 2008, publiée au Journal officiel du 19 avril 2011, introduit la représentation à l’Assemblée nationale des Français établis hors de France. La création de onze circonscriptions législatives à l’étranger permet aux Français expatriés d’élire leur député à l’Assemblée nationale à partir du scrutin de 2012.</p>
<p>Pour plus d’information consultez le site <a href="http://www.votezaletranger.gouv.fr/" class="spip_out" rel="external">Votez à l’étranger</a></p>
<p><strong>Au Ghana, votre unique bureau de vote pour les scrutins organisés à l’étranger se situe dans les locaux de l’Ambassade de France.</strong></p>
<h3 class="spip"><a id="sommaire_3"></a>Associations dans le pays</h3>
<p>Il existe une association française au Ghana, qui propose rencontres et activités sociales et sportives. Moyennant une cotisation annuelle, il est possible d’en devenir membre et participer à ses activités :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.accraaccueil.com" class="spip_out" rel="external">Accra Accueil</a></p>
<h4 class="spip">Les Alliances françaises</h4>
<p>Le Ghana compte un réseau <strong>de cinq Alliance françaises</strong> dans la capitale Accra et en province : à Kumasi, Tema, Cape Coast et Takoradi.</p>
<p>L’Alliance Française d’Accra, en plus de son programme de cours de français, organise régulièrement des évènements culturels (concerts de musique, danse…).</p>
<p>Les centres de Tema, Cape Coast et Takoradi offrent quant à eux des programmes d’enseignement de la langue française.</p>
<p><i>Mise à jour : septembre 2015</i></p>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/presence-francaise/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
