# Stages

<p>Les offres de stages sont peu nombreuses. Les entreprises étrangères, qui sont plus susceptibles de prendre des stagiaires, recherchent souvent des étudiants parlant japonais qui sont en fin de Master 1 ou de Master 2 (stage de fin d’études).</p>
<p><strong>Voici quelques associations et sites Internet qui pourront vous aider à trouver un stage au Japon :</strong></p>
<p><strong>Association franco-japonaise pour les échanges de jeunes</strong> (AFJEJ)<br class="autobr">c/o Chambre de commerce et d’industrie française du Japon (CCIFJ) <br class="autobr">Iida Building - 5-5 Rokubancho - Chiyoda-ku - Tokyo 102-0085<br class="manualbr">Téléphone : [81] (0)3 3288 9635 <br class="manualbr">Télécopie : [81] (0)3 3288 9558<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/emploi-stage/article/stages#info#mc#afjej.org#" title="info..åt..afjej.org" onclick="location.href=mc_lancerlien('info','afjej.org'); return false;" class="spip_mail">Courriel</a></p>
<p>Née de la prise de conscience du nombre croissant d’étudiants désireux d’effectuer un stage dans une entreprise d’un de nos deux pays, l’AFJEJ propose à tous les partenaires intéressés de donner un nouvel élan aux échanges de jeunes entre le Japon et la France. M. Carlos GHOSN, ancien président et CEO de Nissan Motor Co. Ltd. est le parrain de cette association lancée à l’initiative de M. Bernard de MONTFERRAND, ancien Ambassadeur de France au Japon. Chaque jeune peut y enregistrer son profil en y précisant sa requête. Les sociétés ou établissements membres de l’AFJEJ disposent d’un accès personnel à cette base de candidats.</p>
<p><a href="http://www.eujapan.com/" class="spip_out" rel="external">EU – Japan Centre for Industrial Cooperation</a></p>
<ul class="spip">
<li>Tokyo Office<br class="manualbr">Round Cross Ichibancho 4F<br class="manualbr">13-3 Ichibancho, Chiyoda-ku<br class="manualbr">Tokyo 102-0082</li>
<li>European Office
52, rue Marie de Bourgogne
B-1000 Bruxelles, Belgique</li></ul>
<p>Le programme Vulcanus, destiné aux étudiants élèves-ingénieurs et étudiants universitaires bac+3 de l’Union européenne, propose des stages en entreprises industrielles d’une durée d’un an (de septembre à août de l’année suivante).</p>
<h4 class="spip">Avez-vous pensé au Volontariat International (V.I.E ou V.I.A.) ?</h4>
<p>Le V.I.E. permet aux entreprises françaises de confier à un jeune ressortissant de l’espace économique européen, âgé de 18 à 28 ans, une mission professionnelle (rémunérée) à l’étranger durant une période modulable de 6 à 24 mois.</p>
<p>Pour en savoir plus :</p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/emplois-stages-concours/volontaires-internationaux/" class="spip_in">Page thématique sur le volontariat international</a></li>
<li><a href="http://www.civiweb.com/FR/index.aspx" class="spip_out" rel="external">Civiweb.com</a></li>
<li><a href="http://export.businessfrance.fr/default.html" class="spip_out" rel="external">Business France.fr</a> - <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/emploi-stage/article/stages#infoVIE#mc#ubifrance.fr#" title="infoVIE..åt..ubifrance.fr" onclick="location.href=mc_lancerlien('infoVIE','ubifrance.fr'); return false;" class="spip_mail">courriel</a>.</li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/emploi-stage/article/stages). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
