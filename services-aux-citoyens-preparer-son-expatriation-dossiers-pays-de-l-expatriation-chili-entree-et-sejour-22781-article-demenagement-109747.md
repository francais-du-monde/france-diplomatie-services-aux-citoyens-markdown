# Déménagement

<p>Le fret aérien est conseillé pour le transport de cantines. Pour un déménagement complet mieux vaut avoir recours au fret maritime. Il faut compter un délai d’acheminement d’un mois environ depuis la France. Les biens acheminés par voie maritime arrivent à Valparaiso où ils sont dédouanés par les services chiliens.</p>
<p>Compter 8 400€ pour un conteneur de 40 pieds.</p>
<p>S’agissant d’un déménagement international, il est primordial de s’entourer d’un maximum de garanties en faisant appel à un professionnel disposant de certifications reconnues (marque NF Service, ISO 9002).</p>
<p><a href="http://www.csdemenagement.fr" class="spip_out" rel="external">Chambre syndicale du déménagement</a> <br class="manualbr">Téléphone : 01 49 88 61 40<br class="manualbr">Télécopie : 01 49 88 61 46<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/entree-et-sejour-22781/article/demenagement-109747#contact#mc#csdemenagement.fr#" title="contact..åt..csdemenagement.fr" onclick="location.href=mc_lancerlien('contact','csdemenagement.fr'); return false;" class="spip_mail">Courriel</a></p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/entree-et-sejour-22781/article/demenagement-109747). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
