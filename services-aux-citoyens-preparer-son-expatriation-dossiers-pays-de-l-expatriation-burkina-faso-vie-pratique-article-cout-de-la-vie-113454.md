# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/vie-pratique/article/cout-de-la-vie-113454#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/vie-pratique/article/cout-de-la-vie-113454#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/vie-pratique/article/cout-de-la-vie-113454#sommaire_3">Alimentation</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/vie-pratique/article/cout-de-la-vie-113454#sommaire_4">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>Le Burkina Faso est l’un des 14 pays de la zone franc CFA. L’unité monétaire est le franc CFA. Le franc CFA est à parité fixe avec l’euro. Le franc CFA vaut 0,001524 euros, c’est-à-dire qu’un euro équivaut à 656,17 francs CFA*.</p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<p>Si les chéquiers et les cartes bancaires peuvent parfois être utilisés, les espèces demeurent le moyen de paiement le plus répandu.</p>
<p>La carte de crédit "Visa" permet d’effectuer des retraits aux distributeurs automatiques de la BICIA-B (filiale de la BNP - 479 avenue Kwame N’Krumah - BP8 - Ouagadougou - tél. (226) 30 62 26) et de la SGBB (filiale de la Société Générale - BP 585 - rue du Marché - Ouagadougou - tél : (226) 32 32 32).</p>
<p><strong>Transfert de fonds :</strong>Un contrôle, souple, est appliqué pour les transferts supérieurs à 7600€ (auparavant 50 000 FF). Le transfert est autorisé sur présentation de justificatifs ou sur simple déclaration. La déclaration auprès de la Banque centrale est effectuée par la banque concernée.</p>
<h3 class="spip"><a id="sommaire_3"></a>Alimentation</h3>
<p>Les conditions d’approvisionnement sont relativement bonnes et régulières. Les pénuries sont rares et les commerçants locaux y pallient rapidement. Le coût des denrées varie également d’une saison à l’autre.</p>
<p>Prix moyen d’un repas dans un restaurant</p>
<p>Il faut compter 12 000 FCFA (boissons non comprises) pour un repas dans un restaurant de bonne tenue.</p>
<h3 class="spip"><a id="sommaire_4"></a>Evolution des prix</h3>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/vie-pratique/article/cout-de-la-vie-113454). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
