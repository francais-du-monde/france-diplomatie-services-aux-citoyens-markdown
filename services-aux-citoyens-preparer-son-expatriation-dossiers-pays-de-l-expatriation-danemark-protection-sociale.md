# Protection sociale

<h2 class="rub22988">Régime local de sécurité sociale</h2>
<p>Vous trouverez une présentation détaillée du système de sécurité sociale au Danemark sur le site de notre partenaire, le <a href="http://www.cleiss.fr/" class="spip_out" rel="external">CLEISS</a> (Centre des liaisons européennes et internationales de sécurité sociale). En voici la table des matières :</p>
<p><strong>Le régime des salariés</strong></p>
<ul class="spip">
<li><a href="http://www.cleiss.fr/docs/regimes/regime_danemark-salaries.html#generalites" class="spip_out" rel="external">Généralités</a> (Organisation, financement, taux de cotisation sur les salaires…)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_danemark-salaries.html#maladie" class="spip_out" rel="external">Maladie</a> et <a href="http://www.cleiss.fr/docs/regimes/regime_danemark-salaries.html#maternite" class="spip_out" rel="external">Maternité</a> (Prestations en nature et en espèces)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_danemark-salaries.html#invalidite" class="spip_out" rel="external">Invalidité</a>, <a href="http://www.cleiss.fr/docs/regimes/regime_danemark-salaries.html#vieillesse" class="spip_out" rel="external">Vieillesse</a>, <a href="http://www.cleiss.fr/docs/regimes/regime_danemark-salaries.html#survivants" class="spip_out" rel="external">Survivants</a> (Pensions de vieillesse, d’invalidité, de survivant, aide sociale)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_danemark-salaries.html#adtmp" class="spip_out" rel="external">Accidents du travail et maladies professionnelles</a> (Régime indemnitaire, y compris pour les survivants)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_danemark-salaries.html#pf" class="spip_out" rel="external">Prestations familiales</a> (Allocations familiales, de naissance, d’adoption…)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_danemark-salaries.html#chomage" class="spip_out" rel="external">Chômage</a> (Indemnités et assistance chômage)</li></ul>
<p><strong>Le régime des non-salariés</strong></p>
<ul class="spip">
<li><a href="http://www.cleiss.fr/docs/regimes/regime_danemark-nonsalaries.html#generalites" class="spip_out" rel="external">Généralités</a> (Organisation, financement, taux de cotisation sur les salaires…)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_danemark-nonsalaries.html#maladie" class="spip_out" rel="external">Maladie</a> et <a href="http://www.cleiss.fr/docs/regimes/regime_danemark-nonsalaries.html#maternite" class="spip_out" rel="external">Maternité</a> (Prestations en nature et en espèces)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_danemark-nonsalaries.html#invalidite" class="spip_out" rel="external">Invalidité</a>, <a href="http://www.cleiss.fr/docs/regimes/regime_danemark-nonsalaries.html#vieillesse" class="spip_out" rel="external">Vieillesse</a>, <a href="http://www.cleiss.fr/docs/regimes/regime_danemark-nonsalaries.html#survivants" class="spip_out" rel="external">Survivants</a> (Pensions de vieillesse, d’invalidité, de survivant, aide sociale)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_danemark-nonsalaries.html#adtmp" class="spip_out" rel="external">Accidents du travail et maladies professionnelles</a> (Régime indemnitaire, y compris pour les survivants)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_danemark-nonsalaries.html#pf" class="spip_out" rel="external">Prestations familiales</a> (Allocations familiales, de naissance, d’adoption…)</li>
<li><a href="http://www.cleiss.fr/docs/regimes/regime_danemark-nonsalaries.html#chomage" class="spip_out" rel="external">Chômage</a> (Indemnités et assistance chômage)</li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-danemark-protection-sociale-article-convention-de-securite-sociale.md" title="Convention de sécurité sociale">Convention de sécurité sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-danemark-protection-sociale-article-regime-local-de-securite-sociale-111099.md" title="Régime local de sécurité sociale">Régime local de sécurité sociale</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/protection-sociale/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
