# « Un consul pour quoi faire ?  Regards croisés de nos consuls à Montréal, Tunis et Téhéran » (20 juin 2016)

<p class="chapo">
      Du Cap à Reykjavik, de New-York à Shanghai, le ministère des Affaires étrangères et du Développement international pilote un vaste réseau consulaire au service de la communauté française de l’étranger et de l’attractivité de notre pays.
</p>
<p class="spip_document_93590 spip_documents spip_documents_center">
<img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L1000xH354/fond_scene_jdrc2016_cle8cf98c-0bd95.jpg" width="1000" height="354" alt=""></p>
<p>Si les fonctions consulaires existent depuis le Moyen Âge, le métier de consul est loin d’être figé : il se modernise, évolue constamment en fonction du  contexte international et de l’environnement politique, économique et culturel local.</p>
<p>Quel est le quotidien d’un(e) consul(e) au XXIe siècle ? Comment assiste-t-il (elle) les Français voyageant ou résidant à l’étranger ? Quel est son rôle par rapport à celui de l’ambassadeur(drice) ? Comment devient-on consul(e) aujourd’hui ?</p>
<p>Dans le cadre des Journées du réseau consulaire les 20 et 21 juin 2016, le ministère des Affaires étrangères et du Développement international, en partenariat avec TV5Monde, proposait une table ronde ouverte au public sur l’évolution du métier de consul, animée par Xavier Lambrechts de TV5 Monde, en présence des consuls de Montréal, Tunis et Téhéran.</p>
<ul class="rslides" id="slider_diapo">
<li><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L800xH533/001-7_cle0cac17-120-bb7d5-abd2f.jpg" width="800" height="533" alt="Image Diaporama - Photo : B. Chapiron / MAEDI">
<p>
            Photo : B. Chapiron / MAEDI
</p></li>
<li><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L800xH533/002-7_cle0f17e4-118-94e95-42a48.jpg" width="800" height="533" alt="Image Diaporama - Photo : B. Chapiron / MAEDI">
<p>
            Photo : B. Chapiron / MAEDI
</p></li>
<li><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L800xH533/003-8_cle045313-99-eb799-8a4a2.jpg" width="800" height="533" alt="Image Diaporama - Photo : B. Chapiron / MAEDI">
<p>
            Photo : B. Chapiron / MAEDI
</p></li>
<li><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L800xH533/004-9_cle088831-78-6546c-c725c.jpg" width="800" height="533" alt="Image Diaporama - Photo : B. Chapiron / MAEDI">
<p>
            Photo : B. Chapiron / MAEDI
</p></li>
<li><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L800xH533/005-8_cle03ae99-64-18a1c-6c328.jpg" width="800" height="533" alt="Image Diaporama - Photo : B. Chapiron / MAEDI">
<p>
            Photo : B. Chapiron / MAEDI
</p></li>
<li><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L800xH533/006-10_cle471ab4-57-ec517-9626c.jpg" width="800" height="533" alt="Image Diaporama - Photo : B. Chapiron / MAEDI">
<p>
            Photo : B. Chapiron / MAEDI
</p></li>
<li><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L800xH533/007-10_cle478ff4-47-48057-0a7c1.jpg" width="800" height="533" alt="Image Diaporama - Photo : B. Chapiron / MAEDI">
<p>
            Photo : B. Chapiron / MAEDI
</p></li>
<li><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L800xH533/008-10_cle477528-35-d6464-a1ddc.jpg" width="800" height="533" alt="Image Diaporama - Photo : B. Chapiron / MAEDI">
<p>
            Photo : B. Chapiron / MAEDI
</p></li>
<li><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L800xH533/009-8_cle01f2ec-31-a5bea-7f729.jpg" width="800" height="533" alt="Image Diaporama - Photo : B. Chapiron / MAEDI">
<p>
            Photo : B. Chapiron / MAEDI
</p></li>
<li><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L800xH533/010-8_cle05fbf4-24-d7dd4-8e855.jpg" width="800" height="533" alt="Image Diaporama - Photo : B. Chapiron / MAEDI">
<p>
            Photo : B. Chapiron / MAEDI
</p></li>
<li><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L800xH533/011-9_cle02a32b-18-783df-0cc42.jpg" width="800" height="533" alt="Image Diaporama - Photo : B. Chapiron / MAEDI">
<p>
            Photo : B. Chapiron / MAEDI
</p></li>
</ul>
<center><blockquote class="twitter-tweet" data-conversation="none" data-lang="fr">
<p lang="fr" dir="ltr">"Une de mes missions est d'aller à la rencontre de la communauté française à Téhéran" C. Remm, chef de section consulaire de <a href="https://twitter.com/FranceenIran">@FranceenIran</a></p>
<p>— France Diplomatie (@francediplo) <a href="https://twitter.com/francediplo/status/744948248918564864">20 juin 2016</a></p>
</blockquote>
</center><center><blockquote class="twitter-tweet" data-conversation="none" data-lang="fr">
<p lang="fr" dir="ltr">"L'exigence des usagers est toujours plus grande, nous devons nous adapter" C. Reigneaud, consul général à Tunis</p>
<p>— France Diplomatie (@francediplo) <a href="https://twitter.com/francediplo/status/744947169053052929">20 juin 2016</a></p>
</blockquote>
</center><center><blockquote class="twitter-tweet" data-conversation="none" data-lang="fr">
<p lang="fr" dir="ltr">"Nous devons faire œuvre de pédagogie et expliquer quelles sont les limites de la protection que nous pouvons offrir en tant que consulat"</p>
<p>— France Diplomatie (@francediplo) <a href="https://twitter.com/francediplo/status/744946537147564032">20 juin 2016</a></p>
</blockquote>
</center><center><blockquote class="twitter-tweet" data-conversation="none" data-lang="fr">
<p lang="fr" dir="ltr">"Le rôle du consul est de montrer que l'administration est là, au bout du fil et sur Internet" C.Reigneaud, consul général à Tunis</p>
<p>— France Diplomatie (@francediplo) <a href="https://twitter.com/francediplo/status/744946196851138561">20 juin 2016</a></p>
</blockquote>
</center><center><blockquote class="twitter-tweet" data-cards="hidden" data-lang="fr">
<p lang="fr" dir="ltr">[Table ronde "Un consul, pour quoi faire ?"]Les consuls témoignent de la grande diversité des missions des consulats <a href="https://t.co/wEt0YlDfsh">pic.twitter.com/wEt0YlDfsh</a></p>
<p>— France Diplomatie (@francediplo) <a href="https://twitter.com/francediplo/status/744945843443236864">20 juin 2016</a></p>
</blockquote>
</center><center><blockquote class="twitter-tweet" data-lang="fr">
<p lang="fr" dir="ltr">[Table ronde "Un consul, pour quoi faire ?"] "Le lien citoyen doit être maintenu avec les Français expatriés" <a href="https://twitter.com/CGFeuillet">@CGFeuillet</a></p>
<p>— France Diplomatie (@francediplo) <a href="https://twitter.com/francediplo/status/744944233061179392">20 juin 2016</a></p>
</blockquote>
</center><center><blockquote class="twitter-tweet" data-cards="hidden" data-lang="fr">
<p lang="fr" dir="ltr">.<a href="https://twitter.com/CGFeuillet">@CGFeuillet</a>, C.Reigneaud et Claudine Remm témoignent de leur rôle à la table ronde "Un consul pour quoi faire ?" <a href="https://t.co/Bd1yfbxveo">pic.twitter.com/Bd1yfbxveo</a></p>
<p>— France Diplomatie (@francediplo) <a href="https://twitter.com/francediplo/status/744941616885030912">20 juin 2016</a></p>
</blockquote>
</center>
<p><strong>Voir aussi :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="services-aux-citoyens-inscription-consulaire-et-communaute-francaise-article-quel-est-le-role-d-un-consulat.md" class="spip_in">Quel est le rôle d’un consulat ?</a></p>
<p class="spip_document_93578 spip_documents spip_documents_left">
<img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L230xH71/logo_tv5monde_cle8f91de-0c3bc.jpg" width="230" height="71" alt=""></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/actualites/article/un-consul-pour-quoi-faire-regards-croises-de-nos-consuls-a-montreal-tunis-et). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
