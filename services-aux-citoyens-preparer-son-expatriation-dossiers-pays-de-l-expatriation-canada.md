# Canada

<p><strong>Au 31 décembre 2014</strong>, <strong>86 837 Français </strong>étaient inscrits au registre des Français établis hors de France tenu par les consulats français au Canada. (57 469 inscrits à Montréal, 10 069 à Québec, 10 626 à Toronto, 7628 à Vancouver, 1045 à Moncton et Halifax).</p>
<p>La communauté française installée au Canada est estimée à 150 000 personnes. Le Québec est naturellement la destination privilégiée pour l’immigration française en raison de la langue et de l’implantation de nombreuses filiales françaises dans cette province. Le mode de vie québécois et montréalais, la qualité de vie, la facilité des démarches pour créer une entreprise sont autant d’atouts en faveur de l’immigration française dans cette province.</p>
<p>Il demeure cependant difficile de chiffrer avec précision cette communauté compte tenu du nombre de Français non inscrits et de la possibilité d’acquérir la citoyenneté canadienne après quelques années de résidence permanente au Canada (quelques 150 000 personnes acquièrent ainsi chaque année la citoyenneté canadienne).</p>
<p>L’autre facteur concerne les motivations des Français désireux de s’expatrier au Canada. L’expatriation française n’est pas obligatoirement permanente et, selon les estimations, seul un Français sur deux reste au Canada. Besoin de changement ou envie d’entreprendre autre chose ailleurs dans un environnement différent, le retour en France après quelques années passées au Canada est motivé par différentes raisons : gain d’expérience suffisant, raison familiale ou nostalgie du pays.</p>
<p>Pays riche au pouvoir d’achat élevé, le Canada continue d’attirer chaque année des milliers d’immigrants du monde entier (270 000 en 2007), trois fois plus qu’aux Etats-Unis. Avec l’Australie, il est la destination la plus prisée au monde. Possibilités d’emploi, qualité de vie et sécurité en sont les principaux attraits !</p>
<p>La France est le <strong>2ème investisseur étranger au Québec</strong>, derrière les Etats-Unis mais devant le Royaume-Uni, <strong>avec 360 filiales</strong>. Les grandes banques (BNP Paribas, Société générale, Dexia, etc.) et tous les grands groupes (Alstom, Veolia, Suez-Ondeo, Vinci, Danone, Air Liquide, Axa, Essilor, l’Oréal, Hachette, etc.) sont présents, de même que de nombreuses PMI-PME très actives notamment dans les secteurs innovants et dans les services. A noter la présence d’entreprises dans l’électronique (Gemplus, Dassault Systèmes), l’informatique, le multimédia (Ubisoft) et l’aéronautique (Thalès, Safran, etc.). Les PME françaises s’intéressent de plus en plus au Québec comme un marché-test en Amérique du Nord.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/dossiers-pays/canada-y-compris-quebec/" class="spip_in">Une description du Canada de sa situation politique et économique</a></li>
<li><a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/canada-quebec-compris/" class="spip_in">Des informations actualisées sur les conditions locales de sécurité au Canada</a></li>
<li><a href="http://www.ambafrance-ca.org/" class="spip_out" rel="external">Ambassade de France au Canada</a></li></ul>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-canada-entree-et-sejour.md">Entrée et séjour</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-canada-entree-et-sejour-article-passeport-visa-permis-de-travail.md">Passeport, visa, permis de travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-canada-entree-et-sejour-article-demenagement.md">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-canada-entree-et-sejour-article-vaccination-112480.md">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-canada-entree-et-sejour-article-animaux-domestiques.md">Animaux domestiques</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-canada-emploi-stage.md">Emploi, stage</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-canada-emploi-stage-article-marche-du-travail.md">Marché du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-canada-emploi-stage-article-reglementation-du-travail.md">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-canada-emploi-stage-article-recherche-d-emploi.md">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-canada-emploi-stage-article-curriculum-vitae-112485.md">Curriculum vitae</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-canada-emploi-stage-article-lettre-de-motivation-112486.md">Lettre de motivation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-canada-emploi-stage-article-entretien-d-embauche-112487.md">Entretien d’embauche</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-canada-emploi-stage-article-stages.md">Stages</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-canada-protection-sociale-23181.md">Protection sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-canada-protection-sociale-23181-article-regime-local-de-securite-sociale-112489.md">Régime local de sécurité sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-canada-protection-sociale-23181-article-convention-de-securite-sociale-112490.md">Convention de sécurité sociale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-canada-fiscalite.md">Fiscalité</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-canada-fiscalite-article-fiscalite-du-pays.md">Fiscalité du pays</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-canada-fiscalite-article-convention-fiscale-112492.md">Convention fiscale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-canada-vie-pratique.md">Vie pratique</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-canada-vie-pratique-article-logement.md">Logement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-canada-vie-pratique-article-sante-112494.md">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-canada-vie-pratique-article-scolarisation-112495.md">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-canada-vie-pratique-article-cout-de-la-vie-112496.md">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-canada-vie-pratique-article-transports.md">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-canada-vie-pratique-article-communications.md">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-canada-vie-pratique-article-loisirs-et-culture.md">Loisirs et culture</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-canada-presence-francaise.md">Présence française</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-canada-presence-francaise-article-presence-francaise.md">Présence française</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
