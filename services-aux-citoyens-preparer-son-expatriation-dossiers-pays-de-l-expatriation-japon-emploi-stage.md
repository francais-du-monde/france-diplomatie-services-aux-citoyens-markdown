# Emploi, stage

<h2 class="rub22801">Marché du travail</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/emploi-stage/#sommaire_1">Secteurs à fort potentiel</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/emploi-stage/#sommaire_2">Rémunération</a></li></ul>
<p>Le Japon compte seulement 1,57% (à l’exclusion des diplomates, officiels et personnel de l’armée US) d’étrangers, qui sont majoritairement des Asiatiques ou des Sud-Américains descendants de Japonais. <strong>Les offres d’emploi sont essentiellement à destination de Japonais, ou d’étrangers bilingues en japonais résidant au Japon. </strong></p>
<p>Les emplois les plus souvent proposés aux Français sont des postes qualifiés, voire très qualifiés, qui s’adressent à des candidats de formation type école de commerce ou d’ingénieur, maîtrisant le japonais et l’anglais.</p>
<p><strong>Parler japonais est devenu une condition essentielle</strong> pour trouver un emploi au Japon. Mais la maîtrise de la langue n’est pas suffisante : <strong>il faut aussi pouvoir justifier d’une spécialisation supplémentaire</strong>. Dans certains cas, cette expertise est même aussi importante que la maîtrise de la langue. <strong>La maîtrise de l’anglais est évidemment indispensable.</strong></p>
<p>Le marché de l’emploi japonais se caractérise depuis 1998 par :</p>
<ul class="spip">
<li>la diminution de la population active due au vieillissement général de la population (et à la diminution de la population japonaise depuis 2005) ;</li>
<li>un taux de chômage de l’ordre de 4-5%, qui touche particulièrement les hommes jeunes et les seniors.</li></ul>
<p>La précarisation de l’emploi, avec un accroissement de la part des travailleurs non réguliers (34% en 2007, soit 1/3 de la population active), aux salaires bas et à la protection sociale faible. Les femmes comptent pour 2/3 de ces travailleurs non réguliers (41% sont employées à temps partiel).</p>
<p>La crise de mars 2011 a évidemment chamboulé toute la politique économique du pays. Les mesures favorisant la croissance annoncées pour 2012 n’ont pas pu être appliquée en l’espèce.</p>
<p>Le ratio offres d’emploi / demandeurs d’emploi reste lui toujours bien loin des années 2006 / 2007 où le nombre d’offres excédait le nombre de demandeurs. Le ratio de 2011 est en légère progression par rapport à celui de 2010, avec une moyenne de 65 offres pour 100 demandeurs sur 2011. Ce ratio est à mettre en parallèle avec la dualité caractéristique du marché du travail japonais entre travailleurs réguliers et non-réguliers, ces derniers représentant encore plus de 38% de la population active, et particulièrement été encore plus mis à mal par la crise, car les premiers à souffrir d’une précarisation de l’emploi de plus en plus importante.</p>
<p>Pour en savoir plus : <a href="http://www.tresor.economie.gouv.fr/se/japon/documents_new.asp?V=5_PDF_149917" class="spip_out" rel="external">fiche sur l’emploi au Japon de la Mission économique française</a></p>
<h3 class="spip"><a id="sommaire_1"></a>Secteurs à fort potentiel</h3>
<p>Les postes (à statut local) susceptibles d’être offerts à des Français(es) dans les entreprises françaises au Japon sont des postes de : directeur de filiale, responsable produit, responsable marketing, responsable clientèle, responsable commercial, ingénieur commercial, chargé d’affaires, contrôleur de gestion, trader, auditeur interne, informaticien…</p>
<p>Si vous cherchez un employeur français, vous pouvez faire acte de candidature spontanée (lettre de candidature et curriculum vitae) auprès du responsable de la société au Japon, qui est généralement Français.</p>
<h3 class="spip"><a id="sommaire_2"></a>Rémunération</h3>
<p>Il existe au Japon un salaire minimum garanti qui varie selon les régions. Il s’élève à 791 yens par heure (2009) pour la ville de Tokyo. A titre indicatif, le salaire minimum en moyen sur l’ensemble du Japon est 713 yens par heure (en France, il est de 8,86 euros, soit 966 yens). Le bonus, l’allocation familiale, l’allocation de transport ainsi que des heures supplémentaires ne sont pas inclus dans ces données.</p>
<p>La rémunération est composée d’un salaire de base et du paiement des heures supplémentaires. S’y ajoute généralement une rémunération facultative, généralement sous forme d’un bonus payé en deux fois, en été et en hiver (juin et décembre le plus souvent). Le bonus représente l’équivalent de trois à six mois de salaire dans la majorité des cas. L’employeur n’a cependant pas d’obligation légale relative au paiement de ce bonus, dans la mesure où il n’en est pas fait mention dans le règlement intérieur ou dans le contrat de travail.</p>
<p>Le bonus peut être fixe, lié à la performance individuelle de l’employé, ou à celle de l’entreprise, ces différents critères pouvant être combinés.</p>
<p>Vous pouvez consulter le guide « Travailler au Japon » de la <a href="http://www.ccifj.or.jp/BE" class="spip_out" rel="external">Chambre de commerce et d’industrie française du Japon</a> qui fournit le salaire annuel moyen pour l’année 2006 (tous secteurs confondus ).</p>
<p><i>Mise à jour : décembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-japon-emploi-stage-article-stages.md" title="Stages">Stages</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-japon-emploi-stage-article-lettre-de-motivation.md" title="Lettre de motivation">Lettre de motivation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-japon-emploi-stage-article-curriculum-vitae-109847.md" title="Curriculum vitae">Curriculum vitae</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-japon-emploi-stage-article-recherche-d-emploi-109846.md" title="Recherche d’emploi">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-japon-emploi-stage-article-reglementation-du-travail-109845.md" title="Réglementation du travail">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-japon-emploi-stage-article-marche-du-travail-109844.md" title="Marché du travail">Marché du travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/emploi-stage/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
