# Marché du travail

<p class="chapo">
      La priorité est donnée au recrutement et à l’emploi de la main d’œuvre nationale. L’emploi de personnel étranger est réservé aux postes pour lesquels une qualification particulière est exigée, non disponible auprès des travailleurs locaux.
</p>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/emploi-stage/article/marche-du-travail-111067#sommaire_1">Secteurs à fort potentiel</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/emploi-stage/article/marche-du-travail-111067#sommaire_2">Secteurs à faible potentiel</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/emploi-stage/article/marche-du-travail-111067#sommaire_3">Professions règlementées</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/emploi-stage/article/marche-du-travail-111067#sommaire_4">Rémunération</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Secteurs à fort potentiel</h3>
<p>Si traditionnellement, des opportunités d’emploi se présentent dans différents secteurs et notamment l’informatique, le tourisme et les services aux entreprises, l’importante hausse du taux de chômage en raison de la crise économique constitue un frein à l’embauche, sauf éventuellement, sur des profils spécialisés qui font localement défaut. A noter en outre que l’énergie devrait constituer, à moyen terme, un secteur à fort potentiel.</p>
<p>Secteur 1 : services</p>
<p>Secteur 2 : énergie</p>
<h3 class="spip"><a id="sommaire_2"></a>Secteurs à faible potentiel</h3>
<p>Secteur 1 : construction</p>
<p>Secteur 2 : secteur financier</p>
<h3 class="spip"><a id="sommaire_3"></a>Professions règlementées</h3>
<p>Tous <strong>les emplois relevant de l’administration</strong> sont réservés aux ressortissants chypriotes.</p>
<h3 class="spip"><a id="sommaire_4"></a>Rémunération</h3>
<p>Les salaires des cadres sont situés en moyenne dans une fourchette de 1500 à 2500 €.</p>
<p>La rémunération est généralement versée sur une base mensuelle ou hebdomadaire après prélèvement des cotisations obligatoires (assurances sociales, impôt sur le revenu et tous autres prélèvements, par exemple caisse de prévoyance, soins médicaux et pharmaceutiques etc.).</p>
<p>Le décret de 2009 relatif aux salaires minimums (K.D.P. 194/2009) fixe le salaire minimum légal pour les professions suivantes : vendeurs, employés de bureau, aides-infirmiers, aides-puériculteurs, personnel scolaire auxiliaire, gardiens et surveillants. A compter de 2014, le salaire minimum d’embauche s’élève à 870 € avec une augmentation à 924 € si l’on travaille de manière continue pendant six mois chez le même employeur.</p>
<p>Le salaire minimum pour un employé de maison est de 455 € puis de 520 € après six mois de travail continu chez le même employeur.</p>
<p>La feuille de paie doit contenir des détails tels que les données concernant l’employé, le salaire de base, les prélèvements, le salaire net. Le salaire est généralement présenté sous forme de chèque avec la feuille de paie ou bien la feuille de paie est présentée et le salaire est directement déposé sur le compte de l’employé. En général, 12 salaires mensuels sont versés, ainsi qu’un 13e pour Noël. Quelques entreprises versent, à la période de Pâques, un 14e mois de salaire. En cas de convention collective, le paiement des rémunérations respecte ces règles. L’entreprise (l’employeur) est responsable du versement des cotisations aux assurances sociales et de l’impôt sur le revenu de l’employé. En tant qu’employé, vous recevrez une liste récapitulative de tous les prélèvements à la fin de l’année, tandis que, au cours de l’année, les prélèvements sont mentionnés sur la feuille de paie.</p>
<p>Pour plus de renseignements, adressez-vous au département des relations de travail (<i>Τμήμα Εργασιακών Σχέσεων</i>) au +357 22 80 31 00 ou au ministère du travail et des assurances sociales (<i>Υπουργείο Εργασίας και Κοινωνικών Ασφαλίσεων</i>) au +357 22 40 16 00.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.mlsi.gov.cy/mlsi/mlsi.nsf/index_en/index_en?OpenDocument" class="spip_out" rel="external">Ministère du Travail et de l’assurance sociale</a></p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/emploi-stage/article/marche-du-travail-111067). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
