# Fiscalité du pays

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/fiscalite/article/fiscalite-du-pays-110027#sommaire_1">Présentation </a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/fiscalite/article/fiscalite-du-pays-110027#sommaire_2">Année fiscale </a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/fiscalite/article/fiscalite-du-pays-110027#sommaire_3">Quitus fiscal </a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/fiscalite/article/fiscalite-du-pays-110027#sommaire_4">Solde du compte en fin de séjour </a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/fiscalite/article/fiscalite-du-pays-110027#sommaire_5">Inscription auprès du fisc</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Présentation </h3>
<h4 class="spip">L’impôt sur le revenu applicable aux personnes physiques résidanats de Turquie</h4>
<h5 class="spip">Les lois régissant le système fiscal</h5>
<p>Les quatre principales lois en matière fiscale en Turquie sont :</p>
<ul class="spip">
<li>Loi sur les procédures fiscales n°213 du 4.1.1961,</li>
<li>Loi sur l’impôt sur le revenu n°193 du 31.12.1960,</li>
<li>Loi sur l’impôt sur les sociétés n°5422 du 3.6.1949 et</li>
<li>Loi sur la TVA n°3065 du 25.10.1984.</li></ul>
<p>La Loi fiscale définit le revenu comme « le montant net de toutes sortes de revenus, source d’épargne et de dépenses sur une année ». Le revenu comprend donc : les bénéfices commerciaux, les bénéfices agricoles, les traitements et salaires, les bénéfices non commerciaux, les bénéfices des professions indépendantes, les intérêts et dividendes ainsi que les revenus fonciers et les revenus des valeurs mobilières.</p>
<p>Les résidents sont imposables sur l’ensemble de leurs revenus, quelque soit la provenance. Les non-résidents sont, quant à eux, uniquement imposables sur leurs revenus de source turque mais selon les mêmes critères et les mêmes taux que les résidents.</p>
<p>Des taux différents sont applicables, suivant qu’il s’agit de l’impôt sur les salaires ou de l’impôt sur leurs autres revenus. La loi fiscale combine un taux fixe avec un taux progressif.</p>
<p>Un particulier, dont les revenus sont constitués uniquement de salaire, n’est pas dans l’obligation de faire une déclaration, contrairement aux professionnels indépendants qui se doivent de remplir les obligations fiscales.</p>
<p>Le taux de l’impôt sur le revenu est progressif, en fonction de la somme gagnée pendant une année fiscale.</p>
<p><strong>Taux applicables pour les revenus de 2013 :</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Plafonds</td>
<td>Taux fixe</td>
<td>Taux progressif</td></tr>
<tr class="row_even even">
<td>Jusqu’à 10 700 TRL</td>
<td>-</td>
<td>15%</td></tr>
<tr class="row_odd odd">
<td>Jusqu’à 26 000 TRL</td>
<td>1605 TRL</td>
<td>+ 20% pour la différence entre la somme déclarée et le plafond</td></tr>
<tr class="row_even even">
<td>Jusqu’à 60 000 TRL</td>
<td>4465 TRL</td>
<td>+ 27% pour la différence entre la somme déclarée et le plafond</td></tr>
<tr class="row_odd odd">
<td>Au-delà de 60 000 TRL</td>
<td>13 845 TRL</td>
<td>+ 35% pour la différence entre la somme déclarée et le plafond</td></tr>
</tbody>
</table>
<h5 class="spip">Modalité de paiement de l’impôt</h5>
<p>L’impôt des personnes salariées est <strong>prélevé directement à la source par l’employeur.</strong> Pour une activité non salariée, commerciale ou de prestation de services, une déclaration est établie au mois de mars et l’impôt est payé en deux fois, aux mois de mars et juillet.</p>
<p>Pour plus d’informations concernant la fiscalité en Turquie, vous pouvez contacter le <a href="http://www.gib.gov.tr/" class="spip_out" rel="external">ministère turc des Finances</a> :</p>
<p>T.C. Maliye Bakanligi Gelir Idaresi Baskanligi Ilkadim Cad. <br class="manualbr">06450 Dikmen Ankara / TURQUIE <br class="manualbr">Téléphone : +90 (312) – 415 29 00 ou +90 (312) – 415 30 00<br class="manualbr">Télécopie : +90 (312) – 415 28 21 ou 22</p>
<h4 class="spip">L’impôt sur les sociétés</h4>
<p>Les sociétés non-résidentes ne sont soumises à l’IS que pour les revenus de source turque, tandis que les sociétés résidentes sont soumises à l’IS pour l’ensemble de leurs revenus.</p>
<p>Le taux d’imposition est de 20%. Une retenue à la source (<i>stopaj</i>) de 15% est applicable pour les dividendes distribués.</p>
<h4 class="spip">Les impôts indirects </h4>
<h5 class="spip">La TVA</h5>
<p>Le système de TVA applicable en Turquie est similaire au système applicable dans les pays de l’Union européenne.</p>
<p>Toute personne physique ou morale effectuant une activité commerciale, industrielle, agricole ou indépendante à l’intérieur du territoire turc ou lorsqu’elle importe en Turquie des biens ou services, est redevable de la TVA. Toute importation est soumise au paiement de la TVA et les exportations sont exclues du champ d’application de la TVA.</p>
<p><strong>Les personnes redevables de la TVA</strong></p>
<p>La loi sur la TVA définit les personnes redevables comme toute personne engagée dans les transactions imposables, quel que soit leur statut légal, leur nature ou leur situation au regard des autres taxes et impôts.</p>
<p>Sont donc soumis à la TVA :</p>
<ul class="spip">
<li>les fournisseurs de biens et de services,</li>
<li>les importateurs de biens et de services,</li>
<li>les activités suivantes : postes, téléphone, télécopie, télévision, organisation de concerts et d’événements sportifs, les transferts de pétrole et de gaz par gazoduc.</li></ul>
<p>Toute personne physique ou morale, résidente ou non, privée ou publique, engagée dans ce type de transaction est soumise à la TVA.</p>
<p>En matière d’importation, l’assujetti à la TVA est la personne en possession du titre d’importation (connaissement, document représentant la marchandise).</p>
<p><strong>Les taux de TVA </strong></p>
<p>Le taux applicable pour les opérations de transaction a été fixé à 18% le 15 mai 2001. Deux autres taux réduits subsistent : 1 et 8 %. Ils sont applicables à différents types de biens et services.</p>
<p>La liste I énumère ceux qui sont soumis au taux de 1 %. En font notamment partie les aliments secs, le coton, le blé, les journaux, les magazines.</p>
<p>La liste II énumère les biens et services imposés au taux de 8 %. Sont en particulier concernés les aliments de base (lait, pâtes, huile), les médicaments, les livres, le textile, les chaussures.</p>
<p>Les véhicules sont soumis à une TVA de 18%.</p>
<h5 class="spip">La taxe spéciale à la consommation (ÖTV)</h5>
<p>La loi n°4760 du 6 juin 2002 instituant une taxe spéciale à la consommation (ÖTV) a apporté les modifications suivantes : sur les cinq catégories de taux de TVA, seules trois subsistent : celles constituées des taux à 1%, 8% et 18%. Les taux de 26% et 40% ont été supprimés et les produits, jusqu’ici soumis à ces taux, sont désormais imposés au taux de 18%. Il ne s’agit donc en aucun cas d’annuler la TVA pour les produits imposables aux taux de 26 et 40%, mais uniquement de ramener le taux applicable à 18%.</p>
<p>Une distinction est faite entre quatre groupes de biens, lesquels sont soumis à des taux d’imposition différents :</p>
<ul class="spip">
<li>Groupe 1 : produits pétroliers, gaz naturel (taux spécifiques) ;</li>
<li>Groupe 2 : automobiles et autres véhicules motorisés, motocyclettes, hélicoptères, yachts (taux de 1 à 130%) ;</li>
<li>Groupe 3 : tabac, boissons alcoolisées et gazeuses (taux de 25 à 65,25% et montants forfaitaires) ;</li>
<li>Groupe 4 : produits de luxe (taux de 6,7 à 20%)</li></ul>
<p>Cette taxe est applicable de manière identique aux produits importés et à certains produits domestiques.</p>
<h5 class="spip">Impôts locaux</h5>
<p>Les impôts fonciers varient entre 0,1% et 0,4% de la valeur fiscale pour les bâtiments et entre 0,1% et 0,6% pour les terrains.</p>
<h5 class="spip">Droits et taxes sur le commerce international</h5>
<p>Dans le cadre de l’union douanière entre l’Union européenne et la Turquie, les produits en provenance des pays européens sont exemptés des droits de douane (sauf pour les produits agricoles non transformés).</p>
<p>Le barème des droits de douane pour les produits en provenance des autres pays varie selon le produit. Différentes listes préparées conformément au système harmonisé de désignation et de codification des marchandises sont disponibles sur le site du <a href="http://www.ekonomi.gov.tr/" class="spip_out" rel="external">ministère turc de l’Economie</a>.</p>
<p>Il est cependant indispensable de payer la TVA à l’importation et la taxe spéciale à la consommation (notamment pour certaines catégories de produits de luxe).</p>
<h3 class="spip"><a id="sommaire_2"></a>Année fiscale </h3>
<p>L’année fiscale correspond à l’année civile. La déclaration de revenus des personnes physiques (les associés des sociétés dites collectives, commandites ainsi que les associations ordinaires) est faite au plus tard à la fin du mois de mars au bureau des impôts. Les formulaires sont disponibles sur internet et aux bureaux des impôts. Le bureau des impôts compétent est soit celui de la région où la personne réside, soit celui de la région où le travail est effectué.</p>
<p>Si le revenu est constitué uniquement de revenu foncier, la déclaration est faite au plus tard à la fin du mois de janvier.</p>
<p>Les déclarations de revenus des sociétés se font au plus tard à la fin du mois d’avril au bureau des impôts de la région où se trouve le siège de la société ou via internet. Les formulaires sont disponibles à ce même bureau ou sur internet. Cette déclaration est différente de celle de la déclaration de revenus.</p>
<h3 class="spip"><a id="sommaire_3"></a>Quitus fiscal </h3>
<p>Il ne sera exigé de quitus fiscal que dans le cas d’un responsable de société.</p>
<h3 class="spip"><a id="sommaire_4"></a>Solde du compte en fin de séjour </h3>
<p>Un expatrié français relevant du secteur privé peut solder son compte en fin de séjour.</p>
<p>Coordonnées des <a href="http://www.gib.gov.tr/" class="spip_out" rel="external">centres d’information fiscale</a> :</p>
<p>MALIYE BAKANLIGI<br class="manualbr">Gelir Idaresi Baskanligi<br class="manualbr">Ilkadim Caddesi 06450 Dikmen – ANKARA / TURQUIE<br class="manualbr">Tél : +90 (312) 415 29 00<br class="manualbr">Télécopie : +90 (312) 415 28 21</p>
<p>Le cas échéant, renseigner les rubriques suivantes :</p>
<h3 class="spip"><a id="sommaire_5"></a>Inscription auprès du fisc</h3>
<p>Si un expatrié a des revenus à déclarer en Turquie, il doit s’inscrire auprès du fisc local (voir article <a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-turquie-fiscalite-article-convention-fiscale.md" class="spip_in">Convention fiscale</a>).</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/fiscalite/article/fiscalite-du-pays-110027). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
