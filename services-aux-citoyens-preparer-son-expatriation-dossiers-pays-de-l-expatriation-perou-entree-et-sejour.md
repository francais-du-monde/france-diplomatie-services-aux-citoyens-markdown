# Entrée et séjour

<h2 class="rub23022">Passeport, visa, permis de travail</h2>
<p>Quel que soit le motif de votre séjour, renseignez-vous auprès du Consulat du Pérou à Paris qui vous informera sur la règlementation en matière d’entrée et de séjour au Pérou, règlementation que vous devrez impérativement respecter.</p>
<p>Une fois sur place c’est la DIGEMIN (<i>Direccion general de migraciones</i>) qui prend le relais. Consultez le site internet de la Surintendance des migrations pour obtenir de plus amples informations sur les formalités et les différents types de visas. En aucun cas, un touriste étranger <strong>n’est autorisé à travailler au Pérou sans permis adéquat. </strong></p>
<p>Le consulat de France au Pérou n’est pas l’organisme compétent pour répondre à vos demandes d’information concernant votre séjour au Pérou.</p>
<p>Pour toute information relative aux conditions de séjour au Pérou, il est vivement conseillé de prendre l’attache du <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-etrangers-en-france/article/annuaire-des-representations-etrangeres-en-france" class="spip_in">consulat du Pérou à Paris</a>, ou de consulter la rubrique <a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/perou/" class="spip_in">Pérou</a>.</p>
<p><i>Les informations suivantes sont données à titre indicatif</i>.</p>
<h4 class="spip">Séjour de moins de six mois</h4>
<p>Pour un séjour touristique inférieur ou égal à six mois, le passeport en cours de validité suffit. Il n’est pas nécessaire de demander un visa auprès du consulat péruvien. A l’arrivée sur le sol péruvien, le service migratoire péruvien vous accordera une autorisation de séjour allant d’un à six mois (la durée de séjour maximum est de 183 jours). Le formulaire (<i>tarjeta andina de migración</i>) rempli à l’arrivée devra être impérativement remis lors de la sortie. Un tampon d’entrée sur le territoire devra impérativement être apposé sur le passeport.</p>
<h4 class="spip">Séjour de plus de six mois avec installation</h4>
<p>Si vous envisagez de rester plus de six mois, il convient de déposer une demande de visa de long séjour auprès du consulat du Pérou à Paris.</p>
<p><strong>Attention</strong> : il n’est plus possible de prolonger votre séjour auprès de la DIGEMIN.</p>
<p>Les visiteurs qui souhaitent exercer une activité professionnelle de longue durée doivent être en possession d’un <strong>visa de résident</strong> qui leur donne le droit de séjourner sur le territoire pendant un an (renouvelable). Les salariés et indépendants (investisseurs et professionnels) bénéficient de ce type de visa, qu’ils doivent demander à la DIGEMIN. Dans les 30 jours après l’obtention, le titulaire doit demander son inscription au registre central des étrangers et faire les démarches pour obtenir un <i>carnet de extranjeria </i>auprès de la DIGEMIN. Au bout de deux ans, il peut se transformer en visa d’immigrant.</p>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li><a href="http://www.conper.fr/" class="spip_out" rel="external">Consulat du Pérou à Paris</a></li>
<li><a href="http://www.migraciones.gob.pe/" class="spip_out" rel="external">Surintendance des migrations</a></li></ul>
<h4 class="spip">Les différents types de visa</h4>
<h4 class="spip">Visa étudiant</h4>
<p>Le consulat du Pérou ne délivre plus de visa d’étudiant directement.</p>
<p>Pour obtenir un visa d’étudiant, il y a deux possibilités :</p>
<ul class="spip">
<li>soit l’institution éducative au Pérou (l’université ou l’école d’accueil) en fait la demande auprès de la direction générale de l’immigration du ministère de l’Intérieur (DIGEMIN), qui à son tour autorisera le consulat du Pérou à délivrer le visa ;</li>
<li>soit l’étudiant entre au Pérou en tant que touriste et fait les démarches sur place à la DIGEMIN pour obtenir le visa d’étudiant.</li></ul>
<h4 class="spip">Résidents travailleurs</h4>
<p>Pour les personnes souhaitant s’installer et travailler au Pérou, la présentation d’un contrat de travail est nécessaire pour obtenir la carte de résident au Pérou (« carnet de extranjeria ») renouvelable tous les ans. Ces démarches s’effectuent auprès de la DIGEMIN ;</p>
<h4 class="spip">Les conjoints</h4>
<p>Les conjoints de ressortissants péruviens, doivent également solliciter la carte de résident au Pérou (« carnet de extranjeria »). Les démarches s’effectuent auprès de la DIGEMIN.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-perou-entree-et-sejour-article-animaux-domestiques.md" title="Animaux domestiques">Animaux domestiques</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-perou-entree-et-sejour-article-vaccination-111259.md" title="Vaccination">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-perou-entree-et-sejour-article-demenagement-111258.md" title="Déménagement">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-perou-entree-et-sejour-article-passeport-visa-permis-de-travail.md" title="Passeport, visa, permis de travail">Passeport, visa, permis de travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/entree-et-sejour/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
