# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/vie-pratique/article/transports#sommaire_1">Importation de véhicule</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/vie-pratique/article/transports#sommaire_2">Permis de conduire</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/vie-pratique/article/transports#sommaire_3">Code de la route</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/vie-pratique/article/transports#sommaire_4">Assurances et taxes</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/vie-pratique/article/transports#sommaire_5">Achat et location</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/vie-pratique/article/transports#sommaire_6">Entretien</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/vie-pratique/article/transports#sommaire_7">Réseau routier</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/vie-pratique/article/transports#sommaire_8">Transports en commun</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Importation de véhicule</h3>
<p>Il est déconseillé d’importer son véhicule personnel en raison des taxes très importantes sur les véhicules importés.</p>
<h3 class="spip"><a id="sommaire_2"></a>Permis de conduire</h3>
<p>Vous pouvez conduire avec votre permis de conduire international pendant un an.</p>
<p>Passé ce délai, vous devrez solliciter la délivrance d’un permis de conduire thaïlandais auprès du bureau du ministère thaïlandais des transports terrestres (<i>Department of Land transportation</i>) de votre lieu de résidence.</p>
<p>Vous devrez produire les documents suivants :</p>
<ul class="spip">
<li>un certificat de résidence établi en anglais et en français par l’Ambassade de France en Thaïlande et datant de moins de 30 jours ;</li>
<li>votre passeport français, muni du visa de séjour, tous deux en cours de validité, et la copie de ces documents ;</li>
<li>votre autorisation de travail (<i>work permit</i>) et la copie de ce document ;</li>
<li>un certificat médical établi par l’hôpital ou la clinique de votre choix datant de moins de 30 jours ;</li>
<li>votre permis de conduire français en cours de validité accompagné d’une copie de ce document. Vous devrez faire traduire votre permis en anglais ou en thaï et faire ensuite vérifier la traduction par l’Ambassade de France à Bangkok ;</li>
<li>deux photographies d’identité (1 x 1 inch) datant de moins de six mois ;</li>
<li>la somme de 515 THB.</li></ul>
<p><strong>Ministère du Transport (Jatujak)</strong><br class="manualbr">Building N° 4 <br class="manualbr">2nd Floor</p>
<h3 class="spip"><a id="sommaire_3"></a>Code de la route</h3>
<p>La conduite s’effectue à gauche et la priorité est à gauche. La vitesse maximale autorisée est de 50 km/h en agglomération, 90km/h sur route et 120 km/h sur autoroute. Les accidents de la circulation étant de plus en plus nombreux, il faut faire preuve de la plus grande vigilance et éviter, si possible, de conduire la nuit.</p>
<p>L’utilisation des deux roues est fortement déconseillée.</p>
<h3 class="spip"><a id="sommaire_4"></a>Assurances et taxes</h3>
<p>Il faut être affilié à une compagnie d’assurance locale pour être autorisé à conduire son véhicule.</p>
<p>Exemples de coûts des assurances :</p>
<ul class="spip">
<li>Responsabilité civile (obligatoire minimum) : voiture - 7 passagers.</li>
<li>Assurance tous risques (complémentaire) : entre 410 et 2 700 euros par an.</li></ul>
<h3 class="spip"><a id="sommaire_5"></a>Achat et location</h3>
<p><strong>Achat</strong></p>
<p>Aucun type de véhicule particulier n’est conseillé (sauf des véhicules avec air conditionné). Toutes les marques sont représentées, dont les marques françaises (Peugeot, Citroën, Renault) mais les marques japonaises sont les plus courantes (Toyota, Mazda, …). Aucune norme particulière n’est exigée, sauf pour la revente de véhicules et l’entrée de véhicule sur le territoire (normes de pollution). L’Etat effectue des contrôles techniques réguliers.</p>
<p>L’achat de véhicules d’occasion est possible (distributeur au marché de l’occasion) mais ils sont chers.</p>
<p><strong>Location</strong></p>
<p>Il existe de nombreuses sociétés de location de voitures à l’Aéroport Suvarnabhumi de Bangkok. La location d’un véhicule coûte environ 45 à 60 euros par jour. Cependant, les voitures et les motocyclettes en location ne sont pas assurées.</p>
<p>Certaines agences de location d’automobiles proposent des contrats ne comportant aucune assurance. Avant de louer un véhicule, il est vivement recommandé de vérifier ce point de manière exhaustive : assurance conducteur, passagers, responsabilité civile, et en cas de dommages sur le véhicule lui-même.</p>
<p>En cas d’accident, et à défaut d’assurance, le conducteur peut faire l’objet de poursuites judiciaires et est passible d’emprisonnement s’il n’est pas en mesure de dédommager le loueur et la(les) victime(s) de l’accident. Les montants exigés (en fonction de la gravité des dommages) sont extrêmement importants (plusieurs milliers d’euros). Par ailleurs, en cas de dommages corporels, les frais d’hospitalisation demeurent entièrement à la charge du client.</p>
<p>Dans les zones touristiques, de nombreuses agences louent des deux-roues ou des "scooters de mer" sans jamais proposer d’assurance. Compte tenu des risques encourus, il est vivement recommandé de ne pas avoir recours à leurs services.</p>
<h3 class="spip"><a id="sommaire_6"></a>Entretien</h3>
<p>Il est possible de faire réparer son véhicule sur place dans des conditions très variables selon les garages. Il est possible de se procurer des pièces détachées de voitures françaises avec des délais très longs (plusieurs mois).</p>
<h3 class="spip"><a id="sommaire_7"></a>Réseau routier</h3>
<p><strong>Etat du réseau routier</strong></p>
<p>Le réseau routier est dans l’ensemble en bon état et les conditions de circulation sont satisfaisantes mais la discipline des conducteurs laisse à désirer. Les accidents de la circulation sont extrêmement nombreux. Le respect des limitations de vitesse est vivement recommandé et la plus grande prudence est conseillée lors de la conduite de nuit. Le recours aux deux-roues est particulièrement dangereux. Les casques sont rarement aux normes.</p>
<p>L’état des pistes est très variable car elles sont souvent inondées pendant la mousson. Il est donc conseillé de se renseigner sur l’état des routes avant d’entreprendre un voyage. Il existe une autoroute à péages (Bangkok - Pattaya) et en général les liaisons entre les grandes villes se font par nationales à deux voies.</p>
<p><strong>Sécurité</strong></p>
<p>La liberté de circulation est totale, toutefois il convient de faire preuve de prudence en cas de déplacement dans les zones frontalières (Birmanie, Laos, Cambodge.</p>
<h3 class="spip"><a id="sommaire_8"></a>Transports en commun</h3>
<p>Le métro de la capitale thaïlandaise, de très bonne qualité, a soulagé une ville qui étouffe sous des embouteillages légendaires (le prix du ticket est cher et varie de 20 à 40 bahts, en fonction de la distance). Une nouvelle ligne de transport reliant l’aéroport international Suvarnabhumi à Bangkok appelée Airport Rail Link n’a besoin que de 15 minutes pour effectuer la liaison pour 150 baths environ.</p>
<p>Il existe différent sortes de bus de qualité et de coûts différents (entre 3 et 12 bahts). Les taxis individuels coûtent entre 80 et 100 bahts en moyenne la course.</p>
<p>Pour en savoir plus</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.bangkok.com/information-travel-around/" class="spip_out" rel="external">Transports à Bangkok</a> (site en anglais)</p>
<p><i>Mise à jour : juillet 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/vie-pratique/article/transports). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
