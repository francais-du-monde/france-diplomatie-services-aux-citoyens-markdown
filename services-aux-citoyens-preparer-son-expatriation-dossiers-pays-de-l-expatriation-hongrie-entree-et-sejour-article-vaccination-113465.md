# Vaccination

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/entree-et-sejour/article/vaccination-113465#sommaire_1">Vaccinations exigées à l’entrée du pays</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/entree-et-sejour/article/vaccination-113465#sommaire_2">Vaccinations recommandées d’un point de vue médical et basé sur des critères épidémiologiques</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/entree-et-sejour/article/vaccination-113465#sommaire_3">Disponibilité des vaccins sur place</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Vaccinations exigées à l’entrée du pays</h3>
<p><strong>D’un point de vue administratif </strong> : aucune vaccination n’est exigée des voyageurs internationaux, quelle que soit leur provenance</p>
<h3 class="spip"><a id="sommaire_2"></a>Vaccinations recommandées d’un point de vue médical et basé sur des critères épidémiologiques</h3>
<p><strong>Systématiquement :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Mise à jour des vaccinations incluses dans <a href="http://www.sante.gouv.fr/IMG/pdf/Calendrier_vaccinal_detaille_2013_ministere_Affaires_sociales_et_Sante-_pdf.pdf" class="spip_out" rel="external">le calendrier vaccinal français</a>.</p>
<ul class="spip">
<li><strong>Pour les adultes :</strong> mise à jour des vaccinations contre la diphtérie, le tétanos et la poliomyélite, vaccination contre la typhoïde (longs séjours), l’hépatite A (à partir de 50 ans, une recherche préalable d’anticorps sériques totaux est justifiée), l’hépatite B (longs séjours et/ou séjours à risques).</li>
<li><strong>Pour les enfants :</strong> vaccinations recommandées en France par le ministère de la Santé - et en particulier les vaccinations contre l’hépatite B et la tuberculose, qui peuvent être réalisées dès la naissance et la vaccination contre la rougeole, la rubéole et les oreillons, dès l’âge de neuf mois. Hépatite A* (pour les enfants : à partir de l’âge d’un an).</li></ul>
<p><strong> <i>* pour les personnes nées avant 1945, ayant passé leur enfance dans un pays en développement ou ayant des antécédents d’ictère, une recherche préalable d’anticorps sériques (Ig G) est justifiée.</i> </strong></p>
<p><strong>En fonction de la durée et des modalités du séjour </strong> :</p>
<p>Encéphalite à tiques : en cas de séjours en zone rurale et pour des activités de plein air du printemps à l’automne.</p>
<p>Encéphalite à tiques dès l’âge de trois mois : (vaccin non disponible en France).</p>
<p><strong>En fonction de la saison et des facteurs de risques individuels </strong> :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Grippe : pour tous les adultes et enfants (à partir de six mois) faisant l’objet d’une recommandation dans le calendrier vaccinal français, participant à un voyage notamment en groupe, ou en bateau de croisière.</p>
<h3 class="spip"><a id="sommaire_3"></a>Disponibilité des vaccins sur place</h3>
<ul class="spip">
<li><strong>Fièvre jaune : </strong>oui, toujours le centre départemental de santé publique, ANTSZ</li>
<li><strong>Diphtérie </strong> : oui.</li>
<li><strong>Hépatite A </strong> : oui.</li>
<li><strong>Hépatite B </strong> : oui.</li>
<li><strong>Méningite </strong> : oui.</li>
<li><strong>Poliomyélite </strong> : oui.</li>
<li><strong>Tétanos </strong> : oui.</li>
<li><strong>Typhoïde </strong> : oui.</li>
<li><strong>Vaccins antirabiques (cf. chapitre Rage) </strong> : oui.</li>
<li><strong>Autres </strong> : Rougeole, oreillons, coqueluche, rubéole, rage, vaccin contre l’encéphalite à tique</li></ul>
<p>Il est préférable de réaliser toutes les vaccinations nécessaires avant de partir, car une fois sur place on peut rencontrer des difficultés d’approvisionnement.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  notre article thématique <a href="services-aux-citoyens-preparer-son-expatriation-sante.md" class="spip_in">Santé</a>.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/entree-et-sejour/article/vaccination-113465). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
