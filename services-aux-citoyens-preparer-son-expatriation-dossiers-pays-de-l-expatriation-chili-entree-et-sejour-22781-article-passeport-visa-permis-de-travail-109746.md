# Passeport, visa, permis de travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/entree-et-sejour-22781/article/passeport-visa-permis-de-travail-109746#sommaire_1">Les différents types de visa</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/entree-et-sejour-22781/article/passeport-visa-permis-de-travail-109746#sommaire_2">Sites à consulter</a></li></ul>
<p>Quel que soit le motif de votre séjour, renseignez-vous auprès de la section consulaire de l’ambassade du Chili à Paris qui vous informera sur la règlementation en matière d’entrée et de séjour au Chili, règlementation que vous devrez impérativement respecter. N’hésitez pas à consulter le site ministère de l’Intérieur chilien afin de vous familiariser avec les modalités d’entrée et de séjour dans le pays.</p>
<p>Une fois sur place c’est le <i>Departamento Extranjería y Migración</i> (service des étrangers et de l’immigration) chilien qui prend le relais. <strong>En aucun cas, vous n’êtes autorisé à travailler au Chili sans permis adéquat. </strong></p>
<p>Le consulat de France au Chili n’est pas l’organisme compétent pour répondre à vos demandes d’information concernant votre séjour au Chili.</p>
<h3 class="spip"><a id="sommaire_1"></a>Les différents types de visa</h3>
<h4 class="spip">Visa de touriste</h4>
<p>Les Français n’ont pas besoin de visa de tourisme pour voyager au Chili <strong>pour une période maximale de 90 jours</strong>. Il suffit d’un passeport en règle et d’un billet de sortie du Chili.</p>
<p>Si vous désirez prolonger votre séjour, vous avez la possibilité de demander une prorogation de trois mois. Pour cela, vous devrez vous munir d’une photocopie de votre passeport et de la carte de touriste qui vous a été délivrée à l’entrée dans le pays. Les frais de prorogation s’élèvent à 100 USD.</p>
<p><strong>Votre statut de touriste ne vous autorise pas à travailler au Chili.</strong></p>
<h4 class="spip">Visa temporaire</h4>
<p>Si vous effectuez une mission de moins de trois mois au Chili, vous devez demander un permis temporaire de travail auprès du :</p>
<p><a href="http://www.extranjeria.gob.cl/" class="spip_out" rel="external">Ministère de l´Intérieur</a><br class="manualbr"><i>Departamento de Extranjeria y Migracion</i> (service des étrangers et de l’immigration) <br class="manualbr">San Antonio n°580 - piso 2 - Santiago Centro.<br class="manualbr">Téléphone : [56] (2) 600 626 42 22</p>
<p>Les documents demandés sont la promesse de travail et la légalisation de vos diplômes (de préférence auprès du consulat général du Chili en France). Les personnes ayant un lien de parenté ou mariées avec un ressortissant chilien, ainsi que les investisseurs et les commerçants, peuvent demander un visa temporaire qui leur permettra de travailler légalement dans le pays. Les documents nécessaires pour son obtention sont indiqués sur le site Internet du service des étrangers et de l’immigration du ministère chilien de l’Intérieur.</p>
<h4 class="spip">Visa soumis à un contrat de travail</h4>
<p>Pour un séjour de plus de trois mois, il faut solliciter un visa <i>Sujeto a Contrato</i> en présentant les documents suivants :</p>
<ul class="spip">
<li>formulaire de résident dûment complété,</li>
<li>photocopie du passeport,</li>
<li>photocopie de la carte de touriste,</li>
<li>4 photos d’identité récentes,</li>
<li>un contrat de travail en original signé devant notaire.</li></ul>
<p>Ce type de visa est délivré pour une durée ne pouvant excéder deux ans. Au-delà, vous avez la possibilité de demander un permis de résidence définitive.</p>
<p>Tout étranger désirant s’installer au Chili doit être en possession d’un visa de long séjour à son entrée dans le pays pour pouvoir effectuer les formalités de dédouanement lors du déménagement. Ce visa s’obtient auprès du <a href="http://www.cgparis.cl/" class="spip_out" rel="external">consulat général du Chili en France</a>.</p>
<h4 class="spip">Visa étudiant / stages</h4>
<p>Les étudiants français doivent demander un visa pour poursuivre leurs études au Chili. Ce visa ne les autorise pas à travailler dans le pays. Après deux années d’études, ils peuvent demander un permis de résidence définitive.</p>
<p>Pour effectuer un stage rémunéré au Chili, vous devez demander un visa temporaire et présenter les documents suivants :</p>
<ul class="spip">
<li>formulaire dûment complété,</li>
<li>photocopie du passeport et de la carte de tourisme,</li>
<li>3 photos d’identités,</li>
<li>la convention de stage,</li>
<li>une lettre de l’entreprise attestant du stage ainsi que du montant de la rémunération.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Sites à consulter</h3>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://chileabroad.gov.cl/paris/en" class="spip_out" rel="external">Section consulaire de l’ambassade du Chili à Paris</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/entree-et-sejour-22781/article/passeport-visa-permis-de-travail-109746). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
