# Risque nucléaire

<p>Si vous demeurez dans une zone à risque, prévoyez un stock de sécurité dans un endroit clos, connu de toute la famille comprenant :</p>
<ul class="spip">
<li>des éléments permettant le confinement : bandes adhésives, mastic, tissus pour calfeutrer les portes, fenêtres et bouches d’aération ;</li>
<li>un kit de médicaments ;</li>
<li>des vêtements, couvertures, bougies, lampes électriques, piles… ;</li>
<li>des réserves d’eau potable et de nourriture pour une période de sept jours ;</li>
<li>une radio, voire une télévision.</li></ul>
<p><strong></strong></p>
<h4 class="spip">En cas d’alerte nucléaire</h4>
<p> <br class="autobr">Restez à l’écoute des consignes délivrées par les autorités locales.</p>
<p>Si vous êtes à l’extérieur :</p>
<ul class="spip">
<li>rejoignez un lieu clos et restez y confiné ;</li>
<li>ne touchez pas aux objets (à votre véhicule notamment), aux aliments ;</li>
<li>vous pouvez boire l’eau du robinet sauf indication contraire des pouvoirs publics ;</li>
<li>s’il pleut, laissez tout ce qui aurait pu être mouillé à l’extérieur (parapluie, chaussures, manteau, imperméable…) ;</li>
<li>si vous êtes dans un véhicule, gagnez un abri (immeuble, logement…) le plus rapidement possible. Un véhicule ne constitue pas une bonne protection.</li></ul>
<p>Si vous êtes à l’intérieur (logement, lieu de travail…) :</p>
<ul class="spip">
<li>gardez les fenêtres et toutes les ouvertures fermées ;</li>
<li>arrêtez la climatisation, la ventilation et le chauffage ;</li>
<li>bouchez soigneusement les fentes des portes, fenêtres et bouches d’aération ;</li>
<li>si vous craignez d’avoir été exposé à des poussières radioactives, débarrassez-vous de vos vêtements (manteau, veste, chaussures éventuellement…) avant de rentrer dans un lieu clos ;</li>
<li>douchez-vous et changez-vous si vous le pouvez ;
écoutez les informations (la radio, la télévision) ;</li>
<li>ne quittez pas le lieu où vous vous trouvez sans en avoir reçu l’autorisation.</li></ul>
<p>Ne tentez pas d’aller chercher vos enfants à l’école, ceux-ci seront pris en charge par les enseignants. Ne tentez pas de rejoindre vos proches, vous seriez fortement exposé au danger de la radioactivité en sortant. Le confinement ne devrait pas durer plus de quelques heures.</p>
<p><strong></strong></p>
<h4 class="spip">Après l’alerte</h4>
<p>  <br class="autobr">Lorsque vous sortez :</p>
<ul class="spip">
<li>ne touchez pas aux objets, aux aliments, à l’eau qui ont pu être contaminés ;</li>
<li>ne consommez pas les fruits et légumes récoltés dans la zone contaminée ;</li>
<li>ne consommez pas de lait collecté dans la zone contaminée.</li></ul>
<p>Le réseau téléphonique peut être très perturbé. Ne l’encombrez pas. L’ambassade pourrait tenter de vous joindre. Si vous avez de la famille en France, pensez à contacter un de vos proches et demandez-lui de faire passer le message aux autres.</p>
<p>Dans tous les cas, pensez à vous informer, auprès de l’ambassade ou du consulat le plus proche de votre résidence, des mesures prévues pour la sécurité de la communauté française.</p>
<p><strong>En savoir plus</strong> <br class="manualbr"><a href="http://www.asn.fr" class="spip_out" rel="external">Autorité de sûreté nucléaire</a> <br class="manualbr"><a href="http://www.irsn.fr" class="spip_out" rel="external">Institut de radioprotection et de sûreté nucléaire</a></p>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/infos-pratiques/risques/risque-nucleaire/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
