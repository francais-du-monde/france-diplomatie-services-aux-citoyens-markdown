# Les aides financières

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/enseignement/article/les-aides-financieres#sommaire_1">L’allocation de rentrée scolaire (ARS)</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/enseignement/article/les-aides-financieres#sommaire_2">Les bourses de collège et lycée</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/enseignement/article/les-aides-financieres#sommaire_3">L’aide au mérite</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/enseignement/article/les-aides-financieres#sommaire_4">Pour en savoir plus</a></li></ul>
<p>Pour vous aider à financer une partie des dépenses de scolarité, les pouvoirs publics ont mis en place un certain nombre d’aides financières. Il importe de préciser qu’un retour en France, après une expatriation, ne donne droit à aucune aide spécifique.</p>
<h3 class="spip"><a id="sommaire_1"></a>L’allocation de rentrée scolaire (ARS)</h3>
<p>L’ARS est versée par la caisse d’allocations familiales, sous condition de ressources, aux familles disposant de revenus modestes, pour tout enfant scolarisé âgé de 6 à 18 ans.</p>
<p>Renseignez-vous auprès de la <a href="http://www.caf.fr/" class="spip_out" rel="external">caisse d’allocations familiales</a></p>
<h3 class="spip"><a id="sommaire_2"></a>Les bourses de collège et lycée</h3>
<p>Elles sont attribuées sous conditions de ressources en fonction des charges des familles. Des primes d’entrée en Seconde, Première et en Terminale peuvent être attribuées aux élèves accédant à l’une de ces classes. De plus, les collèges et lycées sont dotés de fonds sociaux. Ceux-ci sont destinés à apporter une aide aux familles qui connaissent des difficultés pour couvrir les dépenses de scolarité.</p>
<p>Tous renseignements utiles, tant en matière de bourses que de fonds sociaux, sont fournis par :</p>
<ul class="spip">
<li>le secrétariat de l’établissement fréquenté par l’élève ;</li>
<li>l’inspection académique, service des bourses.</li></ul>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li><a href="http://vosdroits.service-public.fr/particuliers/F984.xhtml" class="spip_out" rel="external">pour le collège</a></li>
<li><a href="http://vosdroits.service-public.fr/particuliers/F616.xhtml" class="spip_out" rel="external">pour le lycée</a></li></ul>
<h3 class="spip"><a id="sommaire_3"></a>L’aide au mérite</h3>
<p>Elle s’adresse aux étudiants, futurs boursiers ou boursiers sur critères sociaux qui sont titulaires d’une mention « très bien »à la dernière session du baccalauréat. Une demande de dossier social étudiant (DSE) doit être déposée sur le site Internet du CROUS de son Académie, avant le 30 avril précédant la rentrée. Se renseigner auprès du secrétariat de l’établissement fréquenté.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://vosdroits.service-public.fr/particuliers/F1010.xhtml" class="spip_out" rel="external">Financement des études supérieures : aide au mérite</a></p>
<h3 class="spip"><a id="sommaire_4"></a>Pour en savoir plus</h3>
<p><strong>Les horaires et programmes d’enseignement de l’Education nationale</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.education.gouv.fr/cid38/horaires-et-programmes.html" class="spip_out" rel="external">Pour l’enseignement primaire</a> </p>
<p><strong>L’orientation</strong></p>
<ul class="spip">
<li><a href="http://www.onisep.fr/" class="spip_out" rel="external">Fiches métiers, atlas des formations, annuaire des lieux d’information CIO, PAIO, missions locales)</a></li>
<li><a href="http://www.education.gouv.fr/pid24243/orientation-au-lycee.html" class="spip_out" rel="external">Au lycée</a></li>
<li><a href="http://www.education.gouv.fr/pid24223/orientation-au-college.html" class="spip_out" rel="external">Au collège</a></li>
<li><a href="http://www.enseignementsup-recherche.gouv.fr/pid24573/enseignement-superieur.html" class="spip_out" rel="external">Formations et diplômes dans l’enseignement supérieur, adresse des établissements</a></li></ul>
<p><strong>L’inscription dans un établissement scolaire ou universitaire</strong></p>
<ul class="spip">
<li><a href="http://www.education.gouv.fr/cid53011/l-annuaire-de-l-education.html" class="spip_out" rel="external">Ecole primaire, collège, lycée</a></li>
<li><a href="http://vosdroits.service-public.fr/particuliers/N20400.xhtml" class="spip_out" rel="external">Université</a></li>
<li><a href="http://www.admission-postbac.fr/" class="spip_out" rel="external">Classe préparatoire aux grandes écoles</a></li>
<li><a href="http://www.education.gouv.fr/cid53011/l-annuaire-de-l-education.html" class="spip_out" rel="external">L’annuaire des rectorats et académies</a></li>
<li><a href="http://www.education.gouv.fr/cid265/l-evaluation-globale-du-systeme-educatif.html" class="spip_out" rel="external">Les indicateurs de performance des lycées</a></li></ul>
<p><strong>Les bourses et aides financières</strong></p>
<ul class="spip">
<li><a href="http://vosdroits.service-public.fr/particuliers/N67.xhtml" class="spip_out" rel="external">Aides financières pour la scolarité</a></li>
<li><a href="http://www.caf.fr/" class="spip_out" rel="external">Allocation de rentrée scolaire</a></li>
<li><a href="http://www.cnous.fr/" class="spip_out" rel="external">Enseignement supérieur</a></li></ul>
<p><i>Mise à jour : avril 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/enseignement/article/les-aides-financieres). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
