# Lettre de motivation

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/emploi-stage/article/lettre-de-motivation-111387#sommaire_1">Rédaction</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Rédaction</h3>
<p>Forme : lettre dactylographiée, d’une longueur d’une page.</p>
<p>Fond : la lettre d’accompagnement est d’autant plus importante que le CV ne contient pas d’encadré avec les objectifs professionnels du candidat. La lettre a donc pour but de compléter le CV, factuel, en exposant le projet professionnel du candidat. En cela, elle ressemble à la lettre française.</p>
<p><i>Mise à jour : février 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/emploi-stage/article/lettre-de-motivation-111387). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
