# Convention fiscale

<p><strong>Dans les pays n’ayant pas conclu de convention fiscale avec la France (ce qui est le cas pour le Pérou)</strong>, la situation de l’expatrié français est réglée à l’égard du fisc français par la loi du 29 décembre 1976 modifiant les règles de territorialité et les conditions d’imposition des Français de l’étranger (Journal Officiel du 30 décembre 1976).</p>
<p>Le texte du Journal Officiel peut être obtenu auprès de la Direction des Journaux Officiels, 26, rue Desaix PARIS 15ème 75727 PARIS Cedex 15 (métro Dupleix) ou sur le <a href="http://www.impots.gouv.fr/" class="spip_out" rel="external">site de l’administration fiscale</a> (choisir la rubrique non-résidents au moyen du menu « vos rubriques »).</p>
<p>Retrouvez dans notre rubrique <a href="services-aux-citoyens-preparer-son-expatriation-fiscalite.md" class="spip_in">Fiscalité</a> les principes et modalités s’appliquant lorsqu’il n’existe pas de convention fiscale entre le pays de votre résidence et la France.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/fiscalite-23025/article/convention-fiscale-111271). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
