# Lettre de motivation

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/emploi-stage/article/lettre-de-motivation-109898#sommaire_1">Rédaction</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Rédaction</h3>
<p>La lettre de motivation va de pair avec le CV, elle apporte des informations nouvelles qui viendront le compléter. Elle devra être rédigée dans la langue de l’offre pour laquelle vous postulez et doit être manuscrite.</p>
<p>Bien que très soignée dans le style et le fond, <strong>la lettre de motivation doit généralement rester assez succincte et ne pas dépasser une page.</strong></p>
<p>La lettre de motivation peut être construite selon le plan suivant :</p>
<ul class="spip">
<li>Tout d’abord, précisez pourquoi vous avez décidé de vous adresser à cette entreprise en particulier, et montrez que vous la connaissez ;</li>
<li>Ensuite, vous devez exposer les compétences qui vous permettront d’être à la hauteur de la mission qui vous sera confiée en expliquant votre parcours (toujours en adéquation avec les prérequis définis dans l’offre), vos qualités personnelles, etc. ;</li>
<li>Enfin, il est bon de conclure par la description de votre projet professionnel au sein de ladite entreprise, tout en restant ouvert au dialogue et à toutes autres propositions.</li></ul>
<p>Evitez les phrases trop longues ainsi que les fautes d’orthographe et de syntaxe.</p>
<p>Lorsque votre candidature a été envoyée, vous pouvez <strong>effectuer un "suivi" </strong>en relançant son destinataire par téléphone. Un délai d’une à deux semaines est recommandé. C’est souvent, pour le recruteur potentiel, l’occasion de juger la détermination du candidat et sa capacité à engager directement le contact.</p>
<p><strong>Ce suivi augmente de fait les chances de décrocher un entretien, </strong>aussi bien à l’occasion d’une candidature spontanée qu’en cas de réponse à une offre.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/emploi-stage/article/lettre-de-motivation-109898). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
