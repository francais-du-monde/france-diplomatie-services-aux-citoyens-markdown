# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/cambodge/emploi-stage/article/reglementation-du-travail-111043#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/cambodge/emploi-stage/article/reglementation-du-travail-111043#sommaire_2">Fêtes légales</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/cambodge/emploi-stage/article/reglementation-du-travail-111043#sommaire_3">Emploi du conjoint</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<p><strong>Le Code du travail de 1997 couvre l’essentiel des relations entre l’employeur et l’employé</strong>. Les relations de travail individuelles et collectives sont abordées par cette législation récente.</p>
<p>La durée légale hebdomadaire ne peut excéder 48 heures, soit 8 heures de travail quotidien. Le repos hebdomadaire est d’une durée d’un jour au minimum. Il existe un régime de congés annuels, le salarié ayant droit à un jour et demi de congé par mois travaillé. Il y a une vingtaine de jours fériés par an au Cambodge.</p>
<p>Il n’existe pas à ce jour de système de sécurité sociale, la seule obligation vise les accidents du travail pour lesquels la souscription d’une assurance est obligatoire.</p>
<p>La loi ne garantit pas de salaire minimum, mais à titre indicatif, le salaire mensuel de référence d’un ouvrier dans l’industrie de la confection textile est de 45 USD par mois.</p>
<h3 class="spip"><a id="sommaire_2"></a>Fêtes légales</h3>
<ul class="spip">
<li>Nouvel an cambodgien (en avril)</li>
<li>Fête des morts (en octobre)</li>
<li>Anniversaire du Roi (31 octobre)</li>
<li>Indépendance (9 novembre)</li>
<li>Fête des Eaux (en novembre)</li></ul>
<p>Certaines dates varient selon le calendrier lunaire.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.tv5.org/TV5Site/voyageurs/accueil.php" class="spip_out" rel="external">TV5.org</a></p>
<h3 class="spip"><a id="sommaire_3"></a>Emploi du conjoint</h3>
<p>Les possibilités sont extrêmement réduites et les salaires faibles.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/cambodge/emploi-stage/article/reglementation-du-travail-111043). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
