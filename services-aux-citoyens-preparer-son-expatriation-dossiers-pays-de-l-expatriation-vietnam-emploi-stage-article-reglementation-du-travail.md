# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/emploi-stage/article/reglementation-du-travail#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/emploi-stage/article/reglementation-du-travail#sommaire_2">Contrat de travail – spécificités</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/emploi-stage/article/reglementation-du-travail#sommaire_3">Fêtes légales</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/emploi-stage/article/reglementation-du-travail#sommaire_4">Emploi du conjoint</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/emploi-stage/article/reglementation-du-travail#sommaire_5">Création d’entreprises – spécificités</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<p>L’employeur se doit de respecter les dispositions du Code du Travail. L’Etat encourage la signature de conventions collectives (accord entre le représentant des salariés et l’employeur). La production d’un règlement intérieur est obligatoire pour les entreprises qui emploient au moins 10 salariés.</p>
<h4 class="spip">Les principales conditions de travail</h4>
<p>La durée de travail ne doit pas excéder huit heures par jour et 48 heures hebdomadaires. Les heures supplémentaires ne peuvent être supérieures à quatre heures par jour et 200 heures par an. Les heures supplémentaires sont rémunérées de la façon suivante :</p>
<ul class="spip">
<li>Les jours ouvrables, chacune des heures supplémentaires donne lieu à une majoration de salaire de 50% au minimum ;</li>
<li>Les jours de repos hebdomadaires, chacune des heures supplémentaires donne lieu à une majoration de salaire de 100 % au minimum ;</li>
<li>Les jours fériés et durant les congés payés, chacune des heures supplémentaires donne lieu à une majoration de 200% au minimum</li>
<li>Le salarié a droit à une journée de temps de repos hebdomadaire et à 12 jours de congés annuels. Le nombre de jours de congé annuel est majoré en raison de l’ancienneté du salarié occupé dans la même entreprise ou par le même employé, chaque période de cinq années apportant un jour de congé annuel supplémentaire.</li>
<li>Des congés spéciaux pour mariage ou décès sont accordés.</li>
<li>La résiliation du contrat de travail par l’employé est possible avec notification d’un préavis de 45 jours pour un CDI, 30 jours pour un CDD compris entre 12 et 36 mois et 3 jours au moins pour un contrat saisonnier ou un CDD de moins de 12 mois. La résiliation par l’employeur n’est possible que dans les cas prévus par le Code du travail avec notification de préavis comme indiqué ci-dessus.</li>
<li>Le montant de la rémunération est fixé dans le contrat de travail d’un commun accord entre les parties signataires et peut varier en fonction de la productivité, de la qualité et de l’efficacité du travail effectué par le salarié. La rémunération du salarié ne doit pas être inférieure au salaire minimum fixé par le Gouvernement.</li>
<li>Les employés peuvent prendre leur retraite entre 55 ans (femmes) et 60 ans (hommes).</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Contrat de travail – spécificités</h3>
<p>Le contrat de travail ne peut être établi que sous l’une des trois formes suivantes :</p>
<ul class="spip">
<li>Le contrat de travail à durée indéterminée ;<br class="manualbr">Le contrat de travail à durée indéterminée est un contrat dont les parties contractantes ne prévoient pas le terme</li>
<li>Le contrat de travail à durée déterminée ;<br class="manualbr">Le contrat de travail à durée déterminée est un contrat dont les parties contractantes prévoient le terme. La durée d’un contrat de ce type doit être comprise entre 12 et 36 mois.</li>
<li>Le contrat de travail saisonnier ou conclu pour l’exécution d’une tâche précise dont la durée ne dépasse pas un an.</li></ul>
<p>Les contrats de travail à durée déterminée et de travail saisonnier doivent faire l’objet d’un renouvellement dans un délai de 30 jours à compter de la date de leur échéance si le salarié continue à travailler. A défaut, le contrat initialement conclu devient automatiquement un contrat à durée indéterminée et reste valable. Si le contrat renouvelé comporte toujours un terme, l’échéance de ce terme ne peut pas donner lieu à un deuxième renouvellement mais à la conclusion d’un contrat à durée indéterminée si la relation contractuelle de travail se poursuit.</p>
<p>Il ne peut pas être conclu de contrat de travail saisonnier ou pour l’exécution d’une tâche précise dont la durée est inférieure à <i>12 mois</i> si l’emploi faisant l’objet du contrat est lié à une activité permanente et régulière et a une durée supérieure à <i>12 mois</i>, sauf s’il s’agit de remplacer provisoirement un salarié faisant son service militaire ou absent en raison d’un congé de maternité ou de tout type de congé provisoire.</p>
<p>Le contrat de travail doit être établi par écrit et en deux exemplaires, chaque partie en conservant un. Pour les emplois provisoires dont la durée est inférieure à trois mois et pour un travail d’employé de maison, le contrat de travail peut être conclu oralement. Ce contrat oral engage les deux parties devant la législation du travail.</p>
<p>Le contrat de travail doit obligatoirement comporter les mentions principales suivantes : la désignation du poste de travail, les horaires, le temps de repos, le montant de la rémunération, la date d’échéance du terme, le terme du contrat, les conditions d’hygiène, de sécurité du travail et d’assurance sociale pour le salarié.</p>
<p><strong>Résiliation du contrat de travail</strong></p>
<ul class="spip">
<li>Dans le cas où l’employeur résilie unilatéralement le contrat de travail contrairement à la loi, il devra réembaucher le salarié dans des condition identiques à celles établies dans le contrat de travail initial et lui verser une indemnité correspondant au montant de la rémunération et des primes et compléments de travail (s’il y a lieu) de la période d’arrêt de travail majorée de deux mois de salaire et de primes et compléments de salaire (s’il y a lieu).</li>
<li>Dans le cas où le salarié résilie unilatéralement le contrat de travail contrairement à la loi, il ne recevra aucune indemnité de licenciement et doit payer à l’employeur l’équivalent de la moitié du salaire et des primes et compléments du salaire (s’il y a lieu) d’un mois.</li>
<li>Dans le cas où une partie résilie unilatéralement le contrat de travail sans respecter le délai du préavis, le contrevenant devra verser à l’autre partie une indemnité correspondant au salaire du salarié pour la période normalement couverte par le préavis.</li></ul>
<p><i>Source : code du travail vietnamien</i></p>
<h3 class="spip"><a id="sommaire_3"></a>Fêtes légales</h3>
<ul class="spip">
<li><strong>1er janvier :</strong> Nouvel An</li>
<li><strong>Fin janvier-début février :</strong> Fête du Têt, nouvel An lunaire vietnamien</li>
<li><strong>3 février</strong> : Anniversaire de la création du Parti Communiste vietnamien</li>
<li><strong>30 avril</strong> : Commémoration de la libération d’Hô Chi Minh-Ville</li>
<li><strong>1er mai</strong> : Fête du Travail</li>
<li><strong>19 mai</strong> : Commémoration de la naissance de Hô Chi Minh</li>
<li><strong>2 septembre</strong> : Fête nationale</li></ul>
<p>Si les jours fériés précités coïncident avec un jour de repos hebdomadaire, le salarié prend son congé les jours suivants.</p>
<p>A noter : si vous devez voyager pendant le Têt, il est recommandé de réserver les vols bien à l’avance, l’affluence étant à son comble à cette période.</p>
<h3 class="spip"><a id="sommaire_4"></a>Emploi du conjoint</h3>
<p>Les possibilités d’emploi du conjoint sont limitées et peu rémunératrices.</p>
<p>L’emploi des femmes est stable. Elles sont principalement représentées dans les secteurs cadres et professions libérales et intellectuelles.</p>
<h3 class="spip"><a id="sommaire_5"></a>Création d’entreprises – spécificités</h3>
<p>Pour tout renseignement concernant la création d’entreprise au Vietnam, l’interlocuteur est <a href="http://www.ccifv.org/" class="spip_out" rel="external">la Chambre de Commerce d’Industrie Française au Vietnam (CCIFV).</a></p>
<p><strong>La Chambre de commerce et d’industrie française de Hanoï (CCIFV)</strong> <br class="manualbr">Sofitel Plaza Hanoi <br class="manualbr">No 1 Thanh Nien, Ba Dinh - HANOI <br class="manualbr">Tel : (+84.4) 37 15 22 29 - Fax : (+84.4) 37 15 22 30 <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/emploi-stage/article/reglementation-du-travail#ccifv.hn#mc#ccifv.org#" title="ccifv.hn..åt..ccifv.org" onclick="location.href=mc_lancerlien('ccifv.hn','ccifv.org'); return false;" class="spip_mail">Courriel</a></p>
<p><strong>La Chambre de commerce et d’industrie française de Ho Chi Minh-Ville (CCIFV)</strong> <br class="manualbr">3rd Floor, 49 Mac Dinh Chi<br class="manualbr">Ben Nghe Ward Distric 1- HO CHI MINH VILLE <br class="manualbr">Tel : (+84.8) 38 25 86 25 <br class="manualbr">Fax : (+84.8) 38 25 89 15 <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/emploi-stage/article/reglementation-du-travail#ccifv.hcm#mc#ccifv.org#" title="ccifv.hcm..åt..ccifv.org" onclick="location.href=mc_lancerlien('ccifv.hcm','ccifv.org'); return false;" class="spip_mail">Courriel</a></p>
<p><i>Mise à jour : février 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/emploi-stage/article/reglementation-du-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
