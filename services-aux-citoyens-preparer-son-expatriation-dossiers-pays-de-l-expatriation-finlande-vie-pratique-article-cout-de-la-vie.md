# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/vie-pratique/article/cout-de-la-vie#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/vie-pratique/article/cout-de-la-vie#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/vie-pratique/article/cout-de-la-vie#sommaire_3">Alimentation</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/vie-pratique/article/cout-de-la-vie#sommaire_4">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>L’euro est la monnaie officielle depuis le 1er janvier 2002.</p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<p>Les cartes bancaires sont très souvent utilisées : retrait d’argent liquide, crédit. C’est le mode de paiement le plus courant pour les achats quotidiens. Les chèques ne sont plus utilisés depuis de nombreuses années. Le règlement des factures s’effectue par l’intermédiaire des sites Internet des banques.</p>
<p>Les coordonnées des banques sont disponibles sur le site du superviseur des activités financières : <a href="http://www.finanssivalvonta.fi/en/About_us/Supervised/Pages/supervisedentities.aspx" class="spip_out" rel="external">Financial Supervisory Authority</a>.</p>
<h3 class="spip"><a id="sommaire_3"></a>Alimentation</h3>
<p>L’approvisionnement est convenable. Les conserves sont peu utilisées, les Finlandais préférant les produits surgelés. Durant l’hiver certains produits frais peuvent faire défaut (légumes importés par exemple) et les prix des produits frais sont très élevés. Il est cependant facile de compenser ce manque par de nombreux surgelés.</p>
<p>Il y a des <strong>restrictions à l’importation des boissons alcoolisées</strong>, celles-ci sont en vente pratiquement uniquement dans les magasins du monopole étatique d’importation "ALKO" et sont soumises à des <strong>taxes fiscales élevées</strong>. On peut acheter de la bière, du cidre et du « long drink » dans les magasins alimentaires, mais tout le reste se trouve à Alko.</p>
<p>Pour 2014, la taxe pour les boissons alcoolisées est (€ par litre d’alcool pur) : 32,05 € pour la bière, 30,82 € pour les vins et 45,55 € pour les alcools forts. Parmi les pays d’Europe, la Finlande a le taux le plus élevé pour la bière et est au 2ème rang pour le vin et les alcools forts.</p>
<p><strong>Prix moyen d’un repas dans un restaurant</strong></p>
<p>Le prix moyen d’un repas dans un restaurant est de 10 à 15 euros à midi (repas chaud dit "lounas") et de 35 à 60 euros le soir.</p>
<p>Le vin est cher. Le pourboire n’est pas d’usage en Finlande.</p>
<h3 class="spip"><a id="sommaire_4"></a>Evolution des prix</h3>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : janvier 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/vie-pratique/article/cout-de-la-vie). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
