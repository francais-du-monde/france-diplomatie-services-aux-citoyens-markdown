# Entretien d’embauche

<p>En Israël, le style de l’entretien pourra paraître familier, il ne faut pas s’en offusquer. Certains patrons offriront une boisson au candidat, d’autres pourront interrompre l’entretien pour répondre sans gêne au téléphone, même lorsqu’il s’agit d’une conversation personnelle.</p>
<p>Le style vestimentaire dans le monde du travail israélien est généralement décontracté, cependant il est préférable de venir bien habillé pour un entretien d’embauche.</p>
<p>Il arrive que l’employeur interroge le candidat sur son conjoint et ses enfants ; il faut toujours répondre poliment, tout en gardant une certaine distance. Le candidat n’est pas obligé de tout raconter sur sa vie personnelle, mais il faut éviter de mentir car les déclarations sont vérifiables et un mensonge ferait perdre sa crédibilité au candidat.</p>
<p>Pour réussir un entretien d’embauche, la préparation et l’entraînement sont les meilleurs atouts :</p>
<ul class="spip">
<li>bien s’informer sur l’entreprise, son marché, ses concurrents ;</li>
<li>se renseigner sur le poste visé et ses particularités (vous pouvez essayer d’obtenir un entretien d’information avec des personnes travaillant dans le domaine qui vous intéresse afin d’en cerner la définition et les codes spécifiques dans le pays) ;</li>
<li>préparer des réponses aux questions les plus fréquemment posées afin de ne pas être pris au dépourvu.</li></ul>
<p><strong>Source</strong> : <a href="http://www.ami-emploi-israel.org/" class="spip_out" rel="external">http://www.ami-emploi-israel.org</a>,  <a href="http://www.moia.gov.il/Moia_fr/" class="spip_out" rel="external">fiches et site du Ministère de l’intégration et de l’immigration en Israël</a>, <a href="http://www.ambafrance-il.org/" class="spip_out" rel="external">Ambassade de France en Israël</a>, <a href="http://www.israelvalley.com/" class="spip_out" rel="external">CCFI</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/emploi-stage/article/entretien-d-embauche). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
