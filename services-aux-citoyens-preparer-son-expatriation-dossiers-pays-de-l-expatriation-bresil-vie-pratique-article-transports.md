# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/vie-pratique/article/transports#sommaire_1">Importation de véhicule</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/vie-pratique/article/transports#sommaire_2">Permis de conduire</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/vie-pratique/article/transports#sommaire_3">Assurances et taxes</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/vie-pratique/article/transports#sommaire_4">Achat et location</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/vie-pratique/article/transports#sommaire_5">Immatriculation</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/vie-pratique/article/transports#sommaire_6">Entretien</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/vie-pratique/article/transports#sommaire_7">Réseau routier</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/vie-pratique/article/transports#sommaire_8">Transports en commun</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Importation de véhicule</h3>
<p>Tous les véhicules ainsi que les bateaux ou moteurs de bateaux sont soumis aux taxes d’importation.</p>
<p>Les véhicules d’occasion ne sont pas autorisés à l’importation définitive. L’importation de véhicules à petites cylindrées et à moteur diesel est interdite. L’entrée du véhicule sur le territoire brésilien doit être autorisée par les services du pays d’accueil, y compris l’<a href="http://www.ibama.gov.br/" class="spip_out" rel="external">IBAMA</a> (Institut en charge des questions sur l’environnement).</p>
<p>Les véhicules importés peuvent être revendus trois ans plus tard.</p>
<p>Compte tenu de la complexité des démarches à accomplir pour l’importation d’un véhicule, mieux vaut s’adresser directement aux concessionnaires locaux pour l’achat d’un nouveau véhicule.</p>
<p>Au Brésil, toutes les marques françaises sont représentées.</p>
<h3 class="spip"><a id="sommaire_2"></a>Permis de conduire</h3>
<p>Il existe un accord de réciprocité en matière de permis de conduire entre le Brésil et la France.</p>
<p>Avant le départ, il convient de se renseigner directement auprès de <a href="http://paris.itamaraty.gov.br/fr/" class="spip_out" rel="external">l’Ambassade du Brésil</a> en France.</p>
<p>Dès lors que vous établissez votre résidence principale au Brésil, vous devez échanger votre permis de conduire français contre le permis de conduire brésilien en vous adressant au DETRAN (<i>Departamento Estatual de Trânsito</i>) de votre lieu de résidence.</p>
<p>Vous pouvez consulter sur le site de <a href="http://www.ambafrance-br.org" class="spip_out" rel="external">l’Ambassade de France au Brésil</a> à la rubrique " Consulats ", les différentes modalités à accomplir.</p>
<h3 class="spip"><a id="sommaire_3"></a>Assurances et taxes</h3>
<p>Seule une assurance responsabilité civile du propriétaire est obligatoire, payable en même temps que la vignette (coût 30 euros/an environ) ; il est donc recommandé de souscrire une assurance complémentaire. Le montant de la prime annuelle pour une assurance tous risques avec franchise représente alors de 10 à 12% du prix du véhicule.</p>
<h3 class="spip"><a id="sommaire_4"></a>Achat et location</h3>
<p>De nombreuses marques de véhicules sont représentées au Brésil. Renault, Peugeot et Citroën y ont des usines de montage et des concessionnaires.</p>
<p>Il est conseillé d’acheter un véhicule sur place, soit neuf, soit d’occasion. Les prix sont environ 60% plus élevés qu’en France. Il est préférable de choisir une voiture climatisée, robuste. L’entretien d’un véhicule fabriqué sur place ne pose pas de problèmes. La qualité du service rendu est bonne. L’achat d’un véhicule de marque française ou étrangère fabriqué au Brésil ne présente aucune difficulté.</p>
<p>Les véhicules peuvent se louer à la journée. Le prix de location, carburant et assurance compris, varie de 100 réaux (bas de gamme sans air conditionné) à 150 réaux (milieu de gamme avec air conditionné), soit 40 à 60 <i>€.</i></p>
<h3 class="spip"><a id="sommaire_5"></a>Immatriculation</h3>
<p>Les coûts de la carte grise et de la vignette dépendent de l’année et de la puissance du véhicule.</p>
<p>Après l’entrée sur le territoire, le véhicule doit être enregistré et immatriculé auprès des services du <a href="http://www.denatran.gov.br/" class="spip_out" rel="external">DENATRAN</a> (<i>Departamento Estadual de Trânsito</i>) de votre lieu de résidence.</p>
<h3 class="spip"><a id="sommaire_6"></a>Entretien</h3>
<p>Les véhicules peuvent être entretenus sur place. La qualité du service rendu est plutôt bonne, même si on peut noter de nombreuses négligences et certains abus. Il n’y a pas de problème pour se procurer des pièces détachées pour les marques implantées depuis longtemps au Brésil. Pour les marques françaises, actuellement toutes les pièces de rechange ne sont pas toutes fabriquées sur place. Certaines pièces, commandées en France, parviennent seulement après trois mois de délai. Les conditions d’obtention de pièces sont plus aléatoires pour les voitures importées.</p>
<h3 class="spip"><a id="sommaire_7"></a>Réseau routier</h3>
<p>La liberté de circulation est totale sur tout le territoire. L’état général du réseau routier est médiocre. En plus des routes à quatre voies, il existe plusieurs autoroutes à péages reliant les grandes villes. Les routes, principales et secondaires, sont en règle générale dangereuses en raison du manque d’entretien, de l’indiscipline et de la violence des conducteurs. Il est déconseillé de circuler de nuit et d’effectuer de longs trajets.</p>
<h3 class="spip"><a id="sommaire_8"></a>Transports en commun</h3>
<p>En raison de l’immensité du territoire, les moyens de transports privilégiés sont l’avion et l’autocar :</p>
<ul class="spip">
<li>le réseau d’autobus (plusieurs compagnies) est bien développé et le prix des trajets modique ;</li>
<li>les nombreuses liaisons aériennes intérieures sont assurées par les compagnies locales.</li></ul>
<p>Le réseau ferroviaire interurbain est quasi inexistant. Les transports urbains :</p>
<ul class="spip">
<li>le métro, avec un réseau assez peu développé, existe dans les principales villes du pays : Brasilia, Rio de Janeiro, Sao Paulo, Recife, Belo Horizonte, Fortaleza et Porto Alegre ;</li>
<li>Rio de Janeiro et Sao Paulo disposent d’une très bonne desserte des compagnies de bus.</li></ul>
<p>Les taxis collectifs se montrent de plus en plus nombreux (déconseillés à Recife).</p>
<p>Tous les taxis individuels possèdent un compteur. Il est conseillé aux clients d’en exiger son utilisation. Deux tarifs sont en vigueur : tarif 1 la journée ; tarif 2 la nuit, le dimanche et les jours fériés.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/vie-pratique/article/transports). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
