# Santé

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/vie-pratique/article/sante-111297#sommaire_1">Le système de santé</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/vie-pratique/article/sante-111297#sommaire_2">Structure médicale</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/vie-pratique/article/sante-111297#sommaire_3">Point sur les vaccinations</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/vie-pratique/article/sante-111297#sommaire_4">Risques sanitaires </a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/vie-pratique/article/sante-111297#sommaire_5">Conditions de vie</a></li></ul>
<p>Le système de santé portugais est proche du système français dans son fonctionnement. Classé parmi les plus efficaces par l’OCDE, il permet une bonne prise en charge de tout problème sanitaire, même pour les étrangers.</p>
<p>Le Portugal est un pays de faible risque épidémiologique. Les maladies présentes sur le territoire sont similaires à celles que l’on peut rencontrer en France.</p>
<h3 class="spip"><a id="sommaire_1"></a>Le système de santé</h3>
<p>La Constitution portugaise reconnaît à tous le droit à la santé et à sa préservation. Toute personne, même étrangère, a donc le droit d’accéder aux structures de soins sans pouvoir se voir opposer de refus du fait de sa nationalité, de sa situation économique ou pour d’autres motifs.</p>
<p>Pour bénéficier de la prise en charge du système national de santé (Sistema Nacional de Saúde – SNS), chaque personne est tenue de se présenter dans l’un des centres de soins dont relève sa zone de résidence pour y être répertoriée.</p>
<p>En tant que ressortissant européen, vous serez tenu de vous munir d’un document d’identité et de votre carte européenne d’assurance maladie. Celle-ci doit être demandée 15 jours au minimum avant le départ et est valable pour une durée d’un an. Si le départ est trop rapide et empêche le respect du délai de 15 jours, l’Assurance maladie pourra vous fournir un certificat provisoire de couverture santé.</p>
<p>Si l’un des membres de votre famille ne dispose pas de la nationalité d’un Etat membre, il devra présenter au centre de santé les documents justifiant de son séjour au Portugal. Un certificat de travail ou de résidence peut être demandé.</p>
<h3 class="spip"><a id="sommaire_2"></a>Structure médicale</h3>
<p>Afin de mieux répondre à la demande de soins des patients étrangers, les hôpitaux ont mis en place des services d’accueil et d’accompagnement personnalisé du patient, où l’anglais est la langue de base de la communication.</p>
<p>Le Portugal possède aussi un système intégré d’urgence médicale qui assure aux personnes victimes d’accident ou de maladie subite, la prestation rapide et adéquate de soins de santé, avec de hauts niveaux d’efficacité dans la mise en œuvre des moyens de secours.</p>
<h4 class="spip">Paiement des prestations de santé</h4>
<p>Les citoyens étrangers qui recourent au SNS sont soumis aux mêmes contraintes que les résidents portugais. Le paiement de tickets modérateurs vous sera réclamé pour toute consultation et acte médical.</p>
<p><strong>Sont exonérés de ce paiement :</strong></p>
<ul class="spip">
<li>les enfants jusqu’à l’âge de 12 ans,</li>
<li>les femmes enceintes,</li>
<li>les demandeurs d’emploi inscrits dans un centre d’emploi portugais,</li>
<li>les personnes en situation de vulnérabilité économique</li>
<li>Les personnes avec un taux d’invalidité supérieur ou égal à 60 %.</li></ul>
<h4 class="spip">Médicaments</h4>
<p>L’État portugais subventionne partiellement le coût de la plupart des médicaments. Certains sont totalement subventionnés, notamment ceux qui sont indispensables au traitement de certaines maladies.</p>
<p>Les bénéficiaires du SNS ne paient qu’une partie des médicaments prescrits par le médecin du SNS ou par leur médecin privé dans la mesure où ils présentent leur carte d’usager du SNS.</p>
<p>Les médicaments qui doivent faire l’objet d’une ordonnance sont encore vendus essentiellement en pharmacie (bien que la vente de médicaments dans les espaces commerciaux, comme les supermarchés ou les magasins de proximité soit autorisée depuis fin 2005 dans la mesure où elle est dûment contrôlée par du personnel qualifié).</p>
<h4 class="spip">Praticiens</h4>
<p>Le Portugal compte 38,68 médecins généralistes pour 10000 habitants (la France en compte environ 45), et 7,17 dentistes pour le même nombre de personnes. Le réseau de professionnels de la santé est donc développé et permet un suivi satisfaisant des pathologies.</p>
<h3 class="spip"><a id="sommaire_3"></a>Point sur les vaccinations</h3>
<p>Avant de partir, il est recommandé d’avoir à jour les vaccinations contre la diphtérie, le tétanos et la poliomyélite. Il est aussi important d’être vacciné contre la coqueluche et la rougeole.</p>
<p>Grippe : Pour tous les adultes et enfants (à partir de 6 mois) faisant l’objet d’une recommandation dans le calendrier vaccinal français, participant à un voyage notamment en groupe, ou en bateau de croisière.</p>
<h3 class="spip"><a id="sommaire_4"></a>Risques sanitaires </h3>
<p>Voyager au Portugal ne présente aucun risque sanitaire différent de ceux présents en France. Les mêmes pathologies se rencontrent dans des conditions équivalentes. Les conditions de salubrité des logements et des équipements publics permettent de réduire considérablement les risques. La consommation d’eau, particulièrement, n’est de ce fait pas problématique.</p>
<h4 class="spip">VIH/sida</h4>
<p>Le VIHest présent au Portugal. Même si les taux de contamination demeurent à peu près aussi faibles que dans les autres pays d’Europe occidentale, il convient d’adopter les protections d’usage contre ce virus, à savoir notamment l’usage de protection lors des rapports sexuels. Entre 0,5 et 0,9 % des adultes de plus de 15 ans vivent avec le virus. Ce taux représente entre 38 000 et 62 000 personnes. Le taux de mortalité équivaut à 2,3 personnes pour 100 000.</p>
<h4 class="spip">Grippe</h4>
<p>La grippe frappe de manière régulière le pays. Comme en France, les épidémies atteignent un pic aux alentours de la fin du mois de février. Les souches virales sont similaires à celles qui se propagent en France, et les conséquences en sont semblables.</p>
<h4 class="spip">Tuberculose</h4>
<p>La tuberculose est toujours présente, bien que faiblement, au Portugal, avec environ 1,3 décès pour 100 000 personnes chaque année. Le total des cas notifiés en 2012 était proche de 2500. Pour rappel, cette maladie est le plus souvent contractée dans des endroits insalubres, exigus et peu aérés, tels que les centres de détention ou les foyers pour sans-abris.</p>
<h4 class="spip">Dengue</h4>
<p>Des cas de dengue sont détectés chaque année sur l’île de Madères. Déclenchée par un moustique porteur du virus, cette maladie se déclenche dans les 4 à 10 jours suivant la piqûre. Les symptômes sont similaires à ceux d’un fort état grippal et peuvent être mortels marginalement en l’absence de soins. Il importe de consulter un thérapeute dès l’apparition des prémices de la maladie afin d’éviter tout risque.</p>
<h4 class="spip">Accidents de la route</h4>
<p>Les morts causées par des accidents de la route représentent au Portugal 7 personnes pour 100 000, soit environ 700 morts par an.</p>
<p>Ici aussi, il convient de transposer les bonnes pratiques pour minimiser les risques (port de la ceinture de sécurité, respect des limitations de vitesse, port du casque pour les deux-roues, etc.).</p>
<h4 class="spip">Tabac</h4>
<p>Le tabac est encore à ce jour toléré dans certains lieux publics. Restaurants, bars ou boîtes de nuit octroient encore la liberté de fumer à leurs clients. Une loi faisant disparaître ces pratiques devrait entrer en vigueur courant 2014.</p>
<h4 class="spip">Rage, paludisme</h4>
<p>Ces maladies ne sont pas répandues au Portugal.</p>
<h3 class="spip"><a id="sommaire_5"></a>Conditions de vie</h3>
<p>Les habitations portugaises sont souvent dépourvues de chauffage central. Le manque ne s’en fait généralement que peu sentir, les températures restant clémentes tout au long de l’année. Prévoyez toutefois l’achat de chauffages d’appoint pour les personnes fragiles, pour éviter les désagréments liés à une éventuelle et ponctuelle chute des températures.</p>
<p>La climatisation n’est pas présente non plus dans les logements portugais. La chaleur pouvant se faire lourdement sentir en été, une vigilance particulière doit être apportée aux personnes fragiles.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.portaldasaude.pt/" class="spip_out" rel="external">Portaldasaude.pt</a></li>
<li><a href="http://www.cimed.org/" class="spip_out" rel="external">Comité d’informations médicales</a></li>
<li><a href="http://www.ameli.fr/" class="spip_out" rel="external">Assurance maladie en ligne</a></li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/vie-pratique/article/sante-111297). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
