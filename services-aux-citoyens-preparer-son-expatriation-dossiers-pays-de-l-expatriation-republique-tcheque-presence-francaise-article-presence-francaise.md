# Présence française

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/presence-francaise/article/presence-francaise#sommaire_1">Ambassade et consulat de France</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/presence-francaise/article/presence-francaise#sommaire_2">Autorités françaises dans le pays</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/presence-francaise/article/presence-francaise#sommaire_3">Associations dans le pays</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Ambassade et consulat de France</h3>
<p>Consulter notre <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/article/annuaire-des-representations-francaises-a-l-etranger" class="spip_in">annuaire des ambassades et consulats de France à l’étranger</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Autorités françaises dans le pays</h3>
<h4 class="spip">Réseau français de coopération et d’action culturelle</h4>
<p>Les coordonnées des services du réseau de coopération et d’action culturelle français en République tchèque ainsi que la description de l’activité de ces services se trouvent dans la rubrique culture du site internet de l’Ambassade ou sur le site internet du réseau culturel : <a href="http://latitudefrance.diplomatie.gouv.fr/AnnuaireCarto.html" class="spip_out" rel="external">Latitude France</a>.</p>
<h4 class="spip">Economie : réseau mondial</h4>
<p>Business France, l’Agence française pour le développement international des entreprises, est présente en République tchèque. Sa mission est d’accompagner les entreprises françaises dans leur développement à l’international.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://export.businessfrance.fr/republique-tcheque/export-republique-tcheque-avec-notre-bureau.html" class="spip_out" rel="external">Site de la Mission économique Business France en République tchèque</a></p>
<p>Les services économiques sont également présents. Ils sont une émanation de la Direction générale du Trésor et ont pour missions principales l’analyse macroéconomique et l’animation des relations économiques bilatérales entre la France et les pays où ils sont implantés.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.tresor.economie.gouv.fr/Pays/republique-tcheque" class="spip_out" rel="external">Site internet du service économique français en République tchèque</a></p>
<h4 class="spip">Vos élus à l’Assemblée des Français de l’étranger</h4>
<p>Pour toute information sur l’Assemblée des Français de l’étranger et pour connaître les conseillers et les Sénateurs qui représentent les Français établis hors de France dans votre circonscription, vous pouvez consulter les sites Internet suivants :</p>
<ul class="spip">
<li><a href="http://www.assemblee-afe.fr/" class="spip_out" rel="external">Assemblée des Français de l’étranger (AFE)</a>.</li>
<li>Sénat au service des Français de l’étranger : <a href="http://www.senat.fr/expatries/dossiers_pays/republique_tcheque.html" class="spip_out" rel="external">dossier spécifique sur le site du Sénat</a></li></ul>
<h4 class="spip">Députés des Français de l’étranger</h4>
<p>La révision constitutionnelle du 23 juillet 2008, publiée au Journal officiel du 19 avril 2011, introduit la représentation à l’Assemblée nationale des Français établis hors de France. La création de onze circonscriptions législatives à l’étranger permet aux Français expatriés d’élire leur député à l’<a href="http://www.assemblee-nationale.fr/14/tribun/fiches_id/610936.asp" class="spip_out" rel="external">Assemblée nationale</a> à partir du scrutin de 2012.</p>
<p>Pour en savoir plus : <a href="http://www.votezaletranger.gouv.fr/" class="spip_out" rel="external">Votez à l’étranger</a></p>
<h3 class="spip"><a id="sommaire_3"></a>Associations dans le pays</h3>
<h4 class="spip">Associations françaises</h4>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Association démocratique des Français à l’étranger - Français du monde (ADFE-FdM)</strong><br class="manualbr">Nad Zlatnici 7 6 160 00 Prague 6<br class="manualbr">Téléphone : [420] 222 230 577 / 78<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/presence-francaise/article/presence-francaise#adfe.rt#mc#quick.cz#" title="adfe.rt..åt..quick.cz" onclick="location.href=mc_lancerlien('adfe.rt','quick.cz'); return false;" class="spip_mail">Courriel</a> </p>
<h4 class="spip">Associations franco-tchèques</h4>
<p><strong>Association " Asso’90 " </strong><br class="manualbr">Husova 23 - 110 00 Prague 1 <br class="manualbr">Téléphone : [420] 222 22 12 35</p>
<p><strong>Pour en savoir plus :</strong><br class="autobr">Vous pouvez consulter l’article <a href="services-aux-citoyens-preparer-son-expatriation-en-savoir-plus-article-associations-des-francais-de-l-etranger.md" class="spip_in">Associations des Français de l’étranger</a>.</p>
<p><i>Mise à jour : septembre 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/presence-francaise/article/presence-francaise). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
