# Loisirs et culture

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/vie-pratique/article/loisirs-et-culture#sommaire_1">Tourisme</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/vie-pratique/article/loisirs-et-culture#sommaire_2">Activités culturelles</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/vie-pratique/article/loisirs-et-culture#sommaire_3">Sports</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/vie-pratique/article/loisirs-et-culture#sommaire_4">Télévision – Radio</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/vie-pratique/article/loisirs-et-culture#sommaire_5">Presse française</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Tourisme</h3>
<p>Si la <strong>population est à 100% urbanisée</strong>, la Cité-Etat de Singapour dispose néanmoins de plans d’eau et d’espaces verts foisonnant d’espèces végétales et d’oiseaux : <strong>Bukit Timah</strong>, la réserve naturelle constituée de 50 hectares de forêt tropicale au centre de l’île et le <strong>MacRitchie Reservoir</strong>, le jardin chinois et le jardin japonais à l’ouest, le jardin botanique et <strong>Fort Canning Park</strong> au sud, le <strong>Pasir Ris Park</strong> doté d’une plage au nord-est, les mangroves du <strong>Sungei Buloh Nature Park</strong> au nord-ouest.</p>
<p><strong>L’île de Sentosa</strong>, au sud, offre des attractions variées et des possibilités de baignade.</p>
<p>D’abord ville de commerces et de services, Singapour possède une singularité architecturale née du contraste entre les édifices modernes, les bâtiments de style colonial ou classique, et les quartiers restaurés des vieilles villes chinoise et indienne. Quelques quartiers tels que Chinatown, Little India, Arab Street conservent un certain charme. Une croisière sur la Singapore River est à conseiller, de même que la visite de certains temples ou mosquées.</p>
<p>Le Singapore History Museum (ou <strong>National Museum</strong>) consacré à l’histoire de l’île, les collections, expositions temporaires et permanentes du musée des civilisations d’Asie (<strong>Asian Civilisations Museum</strong>) et le <strong>Singapore Art Museum</strong> ainsi que les édifices religieux sont également représentatifs du patrimoine multi-culturel de l’île.</p>
<p>Il existe de nombreuses possibilités de tourisme dans les pays de la zone (Cambodge, Thaïlande, Vietnam, Indonésie, Chine, etc…) au départ de l’aéroport de Changi et à des prix raisonnables. La Malaisie est accessible facilement, bien que les délais à la frontière puissent être considérables certains week-ends (jusqu’à plus de trois heures). On trouve un « resort » hippique de qualité à moins de 100 km de Singapour. L’île indonésienne de Bintan est également une destination proche ; on y trouve un Club Med et plusieurs hôtels haut de gamme.</p>
<p><a href="http://www.stb.com.sg/" class="spip_out" rel="external">Singapore Tourism Board</a><br class="manualbr">Tourism Court<br class="manualbr">1 Orchard Spring Lane<br class="manualbr">Singapore 247729<br class="manualbr">Tél. : (65) 6736 6622 - Fax : (65) 6736 9423</p>
<p>Pour en savoir plus :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.yoursingapore.com/content/traveller/en/experience.html" class="spip_out" rel="external">Yoursingapore.com</a></p>
<h3 class="spip"><a id="sommaire_2"></a>Activités culturelles</h3>
<h4 class="spip">Activités culturelles en français</h4>
<p>Site internet de l’<a href="http://www.alliancefrancaise.org.sg/" class="spip_out" rel="external">Alliance française de Singapour</a></p>
<p>La mission de l’Alliance française de Singapour, fondée en 1949, participe activement au rayonnement de la culture française à l’extérieur de la France.</p>
<p>Certains événements qu’elle organise font maintenant partie intégrante du paysage culturel de la ville. Le <i>Rendez vous with French Cinema</i> attire plus de 6000 spectateurs chaque année, la saison culturelle française <i>Voilah</i> la fête de la musique et le marché de Noël sont des événements qui rythment l’agenda annuel.</p>
<p>Le théâtre de l’Alliance a une capacité de 236 sièges. La médiathèque possède, elle, une collection de plus de 15 000 livres.</p>
<p>Chaque mardi, un film français ou francophone est à l’affiche du cinéma de l’Alliance.</p>
<h4 class="spip">Activités culturelles locales</h4>
<p>La <strong>vidéo</strong> (système PAL) et le DVD sont répandus et sont soumis à une censure officielle. Un grand choix de lecteurs est disponible sur place.</p>
<p>Une douzaine de complexes <strong>multi-salles</strong> d’un excellent confort propose des films américains récents et quelques productions européennes auxquels s’ajoutent les projections du Goethe Institut et du British Council.</p>
<p>Quatre salles de <strong>théâtre</strong> programment des comédies anglaises ou d’auteurs locaux, ainsi que du théâtre et de l’opéra chinois. Des concerts de musique classique ont lieu au "Singapore Symphony Orchestra".</p>
<p>De <strong>nombreux concerts</strong> sont proposés et de grands solistes et orchestres prestigieux donnent des concerts dans le complexe de l’Esplanade qui dispose de deux salles remarquables. Il est facile de trouver également des concerts de musiques ou des opéras chinois, quelques comédies musicales en anglais également. Quelques concerts de Jazz ou Pop sont parfois organisés, pour peu que les textes de leurs chansons ne soient pas hors normes.</p>
<p>Des <strong>expositions de peintres</strong> asiatiques contemporains ont lieu au "Singapore Arts Museum", ainsi que des expositions patrimoniales au Musée des Arts Asiatiques.</p>
<p>La <strong>National Library</strong> et ses annexes, et la Médiathèque du British Council sont ouvertes au public.</p>
<h3 class="spip"><a id="sommaire_3"></a>Sports</h3>
<p>Les sports pratiqués à Singapour sont surtout individuels : golf, tennis, natation, squash, équitation, escalade… De nombreux clubs privés offrent d’excellents équipements sportifs. Les droits d’inscription et les cotisations y sont très élevés.</p>
<p>La plupart des résidences possèdent une piscine, un ou deux terrains de tennis, une salle de gymnastique climatisée et parfois un terrain de squash.</p>
<p>Les rencontres sportives sont fréquentes pour les disciplines populaires à Singapour (football, golf, tennis, polo et beach volley sur l’île de Sentosa).</p>
<p>Il est possible de se procurer tous les équipements sur place.</p>
<h3 class="spip"><a id="sommaire_4"></a>Télévision – Radio</h3>
<p>Mediacorp est le groupe de médias singapourien le plus important (télévision, radio, jourmaux…). La branche Media Corporation TV produit des programmes multiples diffusés en plusieurs langues. Son impact est local et régional.</p>
<p>Il existe un système de censure à Singapour.</p>
<p>La télévision locale offre cinq chaînes (deux en anglais, CNA et Channel I), une chaîne en mandarin, une en tamoul et une en malais (toutes les trois sous-titrées en anglais). Sur le câble, une grande variété de chaînes est accessible (CNN, BBC, NBC, TV5, Fox News, national Geographic, chaînes chinoises, japonaises, …). France 24 est diffusée via internet et sur la chaine de câble Singtel Mio (canal 686).</p>
<h4 class="spip">La radio</h4>
<p>Les <strong>radios</strong> sont nombreuses : une quinzaine de stations (en anglais, chinois et malais) du groupe "Radio Corporation of Singapore", deux stations appartenant aux forces armées, "PassionFM", la radio du National Art Council. Au total une quarantaine de stations est accessible sur la bande FM si l’on inclut les stations étrangères.</p>
<p>Radio France Internationale (RFI) peut être captée en ondes courtes toute la journée et deux heures par jour sur la bande FM (93,6).</p>
<h3 class="spip"><a id="sommaire_5"></a>Presse française</h3>
<p>Les principaux quotidiens et magazines français sont disponibles dans plusieurs points de vente (kiosques, supermarchés, Alliance française et réceptions des grands hôtels). Les abonnements souscrits à l’étranger peuvent être soumis à la censure.</p>
<p>Par ailleurs, plusieurs librairies assurent la diffusion d’ouvrages français.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/vie-pratique/article/loisirs-et-culture). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
