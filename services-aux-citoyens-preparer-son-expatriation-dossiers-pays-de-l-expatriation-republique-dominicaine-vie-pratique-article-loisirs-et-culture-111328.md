# Loisirs et culture

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/vie-pratique/article/loisirs-et-culture-111328#sommaire_1">Tourisme</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/vie-pratique/article/loisirs-et-culture-111328#sommaire_2">Activités culturelles</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/vie-pratique/article/loisirs-et-culture-111328#sommaire_3">Sports</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/vie-pratique/article/loisirs-et-culture-111328#sommaire_4">Presse française</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Tourisme</h3>
<p>Première ville fondée en Amérique, Saint-Domingue, capitale de la République Dominicaine, abrite de nombreux musées, sites historiques, lieux dédiés aux arts et à la musique et notamment l’Alcázar de Colon (forteresse de Colomb), le Reloj de Sol (Horloge du soleil) et le Monasterio de los Padres Dominicos (monastère des Pères dominicains) …</p>
<p>Au cœur de Saint-Domingue, la Cité coloniale comporte la première rue, le premier hôpital, la première université et la première cathédrale des Amériques. On pense que la dépouille de Christophe Colomb se trouve encore sur l’île sur laquelle il a débarqué il y a quelques centaines d’années.</p>
<p>Par ailleurs, la région orientale, bordée par une longue ligne de plages, offre de belles stations balnéaires : Boca Chica, Juan Dolio et Guayacanes, La Romana, ville-type dominicaine et ses alentours, notamment Casa de Campo, grand complexe balnéaire et sportif, Punta Cana, Playa del Macao, Playa del Muerte et Bahia de la Gina près de Sabana de la Mar.</p>
<p>La baie de Samaná est également propice au tourisme avec notamment en février-mars la présence de baleines à bosses qui viennent s’y reproduire. Sur la côte nord, en direction de l’ouest, se succèdent une série de plages. Le grand centre de cette côte est la ville de Puerto Plata, caractérisée par ses bâtiments de style victorien, son grand port de navigation de plaisance et le panorama de la "Montagne Isabel de Torres". Dans les environs, il faut citer les plages de Cofresi, Costambar, Playa Dorada, Playa Sosua, puis vers l’ouest, le petit port de Luperon, les ruines de La Isabela et enfin, à la frontière haïtienne, Monte Cristi et Pepillo Salcedo.</p>
<p>A l’intérieur des terres, se trouve la ville de Higüey, célèbre pour sa cathédrale moderne dédiée à Nuestra Señora de Altagracia, patronne du pays, dont la fête, le 21 janvier, donne lieu à un grand pèlerinage et à de nombreuses et pittoresques réjouissances populaires.</p>
<p>Les plaines et montagnes offrent de magnifique possibilités d’excursions vers Santiago, La Vega, Constanza, le grand lac Enriquillo ou encore le Pic Duarte, avec de surprenants paysages de cascades et de petites vallées.</p>
<p>Enfin, il existe en République Dominicaine de nombreux parcs naturels de toute beauté, tels que le Parc National de l’Est, au Sud de Boca de Yuma, le Parc de Miches, sur la côte nord, le Parc National de Los Haitises, sur la baie de Samaná et le Parc du Bahoruco, la région de Pedernales, Bahia de las Aguilas …</p>
<p>La célébration du carnaval se déroule dans l’ensemble du pays, chaque ville présentant sa propre version de la fête. Les rues se remplissent de masques colorés, de musique et, bien sûr, de danse. Le carnaval dure tout le mois de février et atteint son apogée le 27.</p>
<h3 class="spip"><a id="sommaire_2"></a>Activités culturelles</h3>
<h4 class="spip">Activités culturelles en français </h4>
<p>Les activités culturelles en français ont lieu essentiellement dans le cadre des manifestations organisées par le service culturel de l’Ambassade de France et le réseau des Alliances françaises (à Saint-Domingue, Santiago de los Caballeros, Mao et Montecristi).</p>
<p><a href="http://www.afsd.net/" class="spip_out" rel="external">Alliance française de Saint-Domingue</a><br class="manualbr">Horacio Vicioso # 103, <br class="manualbr">Centro de los Héroes <br class="manualbr">Saint-Domingue <br class="manualbr">République Dominicaine <br class="manualbr">Tél. : (1 809) 532-2844 <br class="manualbr">Télécopie : (1 809) 535-0533 <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/vie-pratique/article/loisirs-et-culture-111328#alianza.francesa#mc#afsd.net#" title="alianza.francesa..åt..afsd.net" onclick="location.href=mc_lancerlien('alianza.francesa','afsd.net'); return false;" class="spip_mail">Courriel</a></p>
<p><a href="http://www.afsantiago.org.do/" class="spip_out" rel="external">Alliance française de Santiago</a> <br class="manualbr">Av. Estrella Sadhala <br class="manualbr">Aptdo 597 <br class="manualbr">Santiago <br class="manualbr">République Dominicaine <br class="manualbr">Tél. : (1 809) 582-4998 <br class="manualbr">Télécopie : (1 809) 582-9434</p>
<h4 class="spip">Activités culturelles locales</h4>
<p><strong>Radio</strong></p>
<p>Les radios locales privilégient la musique locale et les débats politiques.</p>
<p>Une antenne de <a href="http://www.afsd.net/index.php/rfi" class="spip_out" rel="external">Radio France Internationale</a>, installée dans les locaux de l’Alliance française de Saint-Domingue diffuse en modulation de fréquence sur 90,9 des programmes en français et en espagnol.</p>
<p>Des émissions locales en décrochage sont diffusées du lundi au vendredi de 18h à 18h30 (et rediffusées le matin suivant de 8h à 8h30).</p>
<p><strong>Télévision</strong></p>
<p>Le réseau câblé de télévision est important (nombreuses chaînes américaines). Le bouquet de base comprend TV5 et il est possible de recevoir Canal Satellite Antilles, sur abonnement et avec le matériel adéquat (bouquet plus ou moins réduit selon que l’abonnement est contracté en République Dominicaine ou dans les Antilles).</p>
<p><strong>Cinéma</strong></p>
<p>Les nombreuses salles de cinéma de Saint-Domingue, très confortables, projettent essentiellement des films américains et dominicains. Le cinéma « Fine Arts » situé dans la tour « Novo centro » diffuse des films d’auteurs européens et sud-américains et la cinémathèque propose des films cultes.</p>
<p><strong>Spectacles vivants et expositions</strong></p>
<p>Les spectacles de théâtre ou musicaux sont de qualité variable avec cependant une programmation intéressante au « Palacio de Bellas Artes » et dans un petit centre culturel situé dans la zone coloniale « Casa de Teatro ».</p>
<p>Des expositions temporaires de peinture, d’artisanat et d’art complètent les expositions permanentes présentées dans les galeries d’art et les musées. A distinguer : musée d’art moderne, musée des Beaux-arts, musée Bellapart (peintures du patrimoine dominicain) et le Centro León Jimenez à Santiago de los Caballeros qui est un des centres culturels pluridisciplinaires le plus important de la République Dominicaine, et qui regroupe un musée, avec des expositions permanentes et temporaires de grande qualité. Il organise la « Biennal de Arte Leon Jimenez », un concours d’art plastique dont les œuvres primées sont exposées dans l´une des salles.</p>
<p>Parmi les galeries d’art privées, à noter les Galerie Nader et Arte Berri.</p>
<p><strong>Bibliothèques</strong></p>
<p>Il existe deux grandes bibliothèques : la bibliothèque nationale et la bibliothèque de l’Université autonome de Saint-Domingue, auxquelles il faut ajouter la bibliothèque de l’Alliance française qui comporte près de 14 000 ouvrages, CD, DVD…</p>
<h3 class="spip"><a id="sommaire_3"></a>Sports</h3>
<p>La plupart des activités sportives peuvent être pratiquées et il est possible de se procurer l’équipement nécessaire sur place. Les clubs privés sont nombreux et offrent généralement les meilleures installations.</p>
<p>Il est déconseillé de fréquenter des plages non signalées. La circulation des engins nautiques motorisés près des plages n’étant pas réglementée, la plongée en solitaire peut se révéler dangereuse. En outre, le traitement des accidents de plongée avec bouteilles est mal assuré dans le pays : peu de caissons de décompression sont disponibles et le personnel chargé de les faire fonctionner n’est pas toujours bien formé.</p>
<p>La pratique de la pêche ne nécessite pas de permis. Elle est autorisée toute l’année, mais certaines espèces sont protégées.</p>
<h3 class="spip"><a id="sommaire_4"></a>Presse française</h3>
<p>Il est possible de s’abonner personnellement à des journaux français, mais ceux-ci ne parviennent à leurs destinataires qu’avec beaucoup de retard. Les principaux hebdomadaires français, ainsi qu’une quarantaine de revues sont disponibles à la librairie privée "Libros y Prensa" (Tél. : 809 533 59 82), installée dans les locaux de l’Alliance française de Saint-Domingue.</p>
<p>La librairie française est ouverte au public :</p>
<ul class="spip">
<li>du lundi au vendredi : de 9 heures à 12 heures et de 15 heures à 18 heures</li>
<li>le samedi : de 9 heures à 12 heures.</li>
<li>fermeture : le dimanche</li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-dominicaine/vie-pratique/article/loisirs-et-culture-111328). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
