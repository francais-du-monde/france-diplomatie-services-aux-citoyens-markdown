# Passeport, visa, permis de travail

<p>Quel que soit le motif de votre séjour, renseignez-vous auprès de la section consulaire de l’ambassade de Burkina Faso à Paris qui vous informera sur la règlementation en matière d’entrée et de séjour au Burkina Faso, règlementation que vous devrez impérativement respecter. N’hésitez pas à consulter le site ministère de l’Intérieur burkinabé afin de vous familiariser avec les modalités d’entrée et de séjour dans le pays.</p>
<p><strong>En aucun cas, vous n’êtes autorisé à travailler au Burkina Faso sans permis adéquat. </strong></p>
<p>Le Consulat de France à Ouagadougou n’est pas l’organisme compétent pour répondre à vos demandes d’information concernant votre séjour au Burkina Faso.</p>
<p>Pour un séjour de moins de trois mois (90 jours), un visa de court séjour est délivré par l’Ambassade du Burkina Faso en France.</p>
<p>Pour un séjour de plus de trois mois, l’entrée sur le territoire se fait avec un visa de court séjour ; puis, un visa de long séjour (un an renouvelable) est délivré par la Direction générale de la police nationale sur présentation du contrat de travail enregistré auprès de l’Office national pour l’emploi (ONPE).</p>
<p>Les formalités d’entrée du conjoint sont identiques.</p>
<p>D’une manière générale, il est cependant conseillé de s’assurer des conditions d’entrée et de séjour auprès des services consulaires de l’Ambassade du Burkina Faso en France.</p>
<p><strong>Un permis de travail n’est pas nécessaire pour travailler au Burkina Faso.</strong> La régularité de la situation de l’intéressé est apportée par le visa de long séjour apposé sur le passeport.</p>
<p>Il faut cependant savoir que les contrats proposés par les entreprises doivent être agréés par l’ONPE (Office national pour l’emploi) qui vérifie si l’emploi ne peut d’abord être accordé à un Burkinabé.</p>
<p><strong>ONPE</strong><br class="manualbr">Sis à la cité An III - Secteur 301 <br class="manualbr">BP 521 Ouagadougou 01 <br class="manualbr">Tél : 30 77 40 <br class="manualbr">Fax : 30 00 97</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/entree-et-sejour/article/passeport-visa-permis-de-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
