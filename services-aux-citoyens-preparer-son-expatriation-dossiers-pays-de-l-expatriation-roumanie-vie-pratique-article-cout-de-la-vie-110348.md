# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/vie-pratique/article/cout-de-la-vie-110348#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/vie-pratique/article/cout-de-la-vie-110348#sommaire_2">Alimentation</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/vie-pratique/article/cout-de-la-vie-110348#sommaire_3">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>L’unité monétaire est le leu, au pluriel lei (RON).</p>
<p>Au 13 décembre 2013, un leu vaut 0,224415 euros (1 euro vaut 4,45604 lei). Il est difficile en France de s’approvisionner directement en monnaie roumaine. Il existe sur place de nombreux bureaux de change, celui-ci pouvant aussi être effectué auprès des différentes banques commerciales existantes. Le paiement en liquide domine. Le paiement par carte se répand lentement, surtout auprès des plus jeunes. Les chèques sont peu utilisés. Le virement bancaire est souvent utilisé pour les affaires. Les salaires locaux sont souvent payés en liquide.</p>
<p>Les principales banques françaises et étrangères installées en Roumanie sont la Banque Roumaine de Développement (BRD, groupe Société Générale), ING Barings, Raiffeisen Bank et Crédit agricole.</p>
<h3 class="spip"><a id="sommaire_2"></a>Alimentation</h3>
<p>Il n’existe pas de problèmes d’approvisionnement , de plus en plus de produits français étant présents dans les supermarchés des groupes de distribution français Carrefour, Cora et Auchan qui sont maintenant bien implantés en Roumanie.</p>
<h4 class="spip">Prix moyen d’un repas dans un restaurant</h4>
<p><strong>Bucarest</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td></td>
<td>lei</td>
<td>euros</td></tr>
<tr class="row_even even">
<td>Restaurant de qualité supérieure</td>
<td>170</td>
<td>41,31</td></tr>
<tr class="row_odd odd">
<td>Restaurant de qualité moyenne</td>
<td>60</td>
<td>14,58</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_3"></a>Evolution des prix</h3>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/vie-pratique/article/cout-de-la-vie-110348). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
