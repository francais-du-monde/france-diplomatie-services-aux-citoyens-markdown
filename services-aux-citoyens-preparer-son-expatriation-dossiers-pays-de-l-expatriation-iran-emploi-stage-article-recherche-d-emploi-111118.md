# Recherche d’emploi

<p>Le permis de travail est un préalable, sollicité par l’employeur. Les Français qui souhaitent travailler en Iran doivent donc s’y faire employer depuis ailleurs, et par exemple chercher un emploi depuis la France.</p>
<p><i>Mise à jour : février 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/emploi-stage/article/recherche-d-emploi-111118). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
