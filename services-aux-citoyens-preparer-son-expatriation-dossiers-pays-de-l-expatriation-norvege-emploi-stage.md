# Emploi, stage

<h2 class="rub23011">Marché du travail</h2>
<p>Avec la crise financière mondiale, la Norvège a vu son chômage passer de 2% à 3%, le secteur privé étant le plus touché (en particulier le bâtiment et l’industrie).</p>
<p>En janvier 2014, le chômage est stabilisé à environ 3,3% en moyenne nationale, pour environ 90 000 personnes.</p>
<p>A moyen terme les domaines de la santé, des soins aux personnes âgées, ainsi que le secteur de l’enseignement vont avoir besoin d’un important apport de personnel. Ce qui peut vite être valable pour d’autres secteurs d’activités car la pyramide démographique norvégienne confirme que la population va devenir vieillissante, avec de moins en moins de personnes actives pour le nombre de personnes inactives.</p>
<p>Par exemple : médecins, dentistes, personnel infirmier, enseignants, ingénieurs, informaticiens, artisans (charpentiers, plombiers, électriciens), coiffeurs, cuisiniers, serveurs….</p>
<p>Il n’y a, par ailleurs, pas d’emplois à déconseiller aux Français qui parlent le norvégien. Néanmoins, les études commerciales étant très prisées en Norvège, la concurrence se montre plus rude pour les emplois commerciaux.</p>
<h4 class="spip">Rémunération</h4>
<p>Le site officiel du <a href="http://www.ssb.no/" class="spip_out" rel="external">bureau des statistiques norvégien</a> indique des moyennes de salaires pour de nombreux groupes professionnels. Les sites des syndicats peuvent également être une source d’information.</p>
<p>Le salaire moyen tous secteurs était en 2013 de 49 500 NOK (couronnes norvégiennes) soit environ 4800 euros, par mois et brut (avant impôts).</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Le site officiel du <a href="http://www.ssb.no/en/forside;jsessionid=898F538EF8F6490C4371147338A97383.kpld-as-prod10?hide-from-left-menu=true&amp;language-code=en&amp;menu-root-alternative-language=true" class="spip_out" rel="external">bureau des statistiques norvégien</a></p>
<p><i>Mise à jour : janvier 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-norvege-emploi-stage-article-stages.md" title="Stages">Stages</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-norvege-emploi-stage-article-entretien-d-embauche.md" title="Entretien d’embauche">Entretien d’embauche</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-norvege-emploi-stage-article-lettre-de-motivation.md" title="Lettre de motivation">Lettre de motivation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-norvege-emploi-stage-article-curriculum-vitae.md" title="Curriculum vitae">Curriculum vitae</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-norvege-emploi-stage-article-recherche-d-emploi.md" title="Recherche d’emploi">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-norvege-emploi-stage-article-reglementation-du-travail-111205.md" title="Réglementation du travail">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-norvege-emploi-stage-article-marche-du-travail.md" title="Marché du travail">Marché du travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/norvege/emploi-stage/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
