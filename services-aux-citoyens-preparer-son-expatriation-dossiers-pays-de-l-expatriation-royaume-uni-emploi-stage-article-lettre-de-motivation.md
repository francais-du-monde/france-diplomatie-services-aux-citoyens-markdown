# Lettre de motivation

<h4 class="spip">Rédaction</h4>
<p><strong>Les indications suivantes sont fournies à titre indicatif et constituent des recommandations d’ordre général. </strong>Une approche différente peut être demandée selon le poste recherché, les compétences du candidat et sa personnalité.</p>
<p>La lettre de motivation doit être dactylographiée. Les Britanniques ont rarement recours aux techniques d’analyse graphologique dans le cadre du recrutement et une lettre manuscrite pourrait être interprétée comme un manque de rigueur.</p>
<p>Bien que très soignée dans le style et le fond, la lettre doit généralement rester assez succincte. L’approche est plus directe qu’en France : la lettre doit être individualisée en fonction du destinataire et de l’entreprise visée.</p>
<p>La lettre de motivation peut être construite selon le plan suivant :</p>
<ul class="spip">
<li>référence à l’emploi souhaité et, le cas échéant, à la source d’information ayant permis de connaître l’offre ;</li>
<li>présentation des qualifications et de l’expérience du candidat, en évitant les redites avec le CV ;</li>
<li>mise en relief des points forts du candidat, en s’appuyant si possible sur des exemples concrets (résultats obtenus lors des expériences antérieures, réalisations particulières, etc) ;</li>
<li>disponibilités du candidat pour un rendez-vous.</li></ul>
<p><strong>Préparation</strong></p>
<p>Faites des recherches sur l’entreprise avant d’entreprendre la rédaction de la lettre. Individualisez votre lettre en fonction du destinataire et de ce que vous aurez appris de l’entreprise.</p>
<p><strong>Format de la lettre</strong></p>
<p>Le plus souvent, la lettre contient quatre parties, sur une seule page (environ 200 mots).</p>
<p><strong>Dans la première partie</strong>, vous devrez faire référence à l’emploi pour lequel vous postulez et à la source d’information grâce à laquelle vous avez eu connaissance de l’offre.</p>
<p>Formules courantes :</p>
<ul class="spip">
<li>I am writing to you in response to your advertisement in the … for the vacancy of "…"</li>
<li>I would like to apply for the position of …… as advertised in the "…"</li>
<li>With regard to my particular interest in your company, I am writing in anticipation that you may have any vacancies related to the areas of…</li>
<li>As you will see from my enclosed Curriculum Vitae, I graduated from the University of ……. and since then I have had a successful year working as a…</li></ul>
<p><strong>La deuxième partie </strong>fait état de vos qualifications et de votre expérience. Soyez bref et essayez de ne pas être redondant par rapport à votre CV.</p>
<p>Formules courantes :</p>
<ul class="spip">
<li>As you will see from my enclosed Curriculum Vitae, I graduated from the University of ……. and since then I have had a successful year working as a….</li>
<li>Due to my keen interest in… I am now seeking a position in that field.</li>
<li>I am seeking a position involving contacts in an international environment</li></ul>
<p><strong>Dans la troisième partie</strong>, mettez en avant vos points forts et montrez en quoi ils vous permettront de correspondre au profil demandé. Évitez d’utiliser des clichés et de vous décrire comme la personne la plus "géniale", mais soyez positif, confiant, et sachez mettre en valeur vos spécificités. Si possible, illustrez vos propos par des exemples concrets (exemple : résultats commerciaux, réalisations).</p>
<p>Formules courantes :</p>
<ul class="spip">
<li>As you will see from my enclosed Curriculum Vitae, I graduated from the University of ……. and since then I have had a successful year working as a…</li>
<li>I believe my skills and experience match your requirements closely. I have extensive experience in publishing and customer service</li>
<li>My varied work experience has developed and consolidated my excellent communication/organisational skills and initiative whilst ….,</li>
<li>I am enclosing a Curriculum Vitae for your perusal which I trust contains all the information you may require.</li>
<li>You will see from the enclosed Curriculum Vitae that I am fluent in several European languages and that I possess a good knowledge of a variety of computer packages.</li>
<li>I would like to consider any other relevant position you may have.</li></ul>
<p><strong>Terminez votre lettre</strong> en réaffirmant votre intérêt pour le poste et votre disponibilité pour en discuter lors d’un entretien.</p>
<p>Formules courantes :</p>
<ul class="spip">
<li>Should you require any further details or should you wish to discuss my suitability for the position, please do not hesitate to contact me. Furthermore, I would like to inform you that I am available immediately.</li>
<li>I look forward to hearing from you in the very near future.</li>
<li>Yours sincerely.</li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/emploi-stage/article/lettre-de-motivation). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
