# Entretien d’embauche

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/emploi-stage/article/entretien-d-embauche-109899#sommaire_1">Conduite de l’entretien</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/emploi-stage/article/entretien-d-embauche-109899#sommaire_2">Apparence et attitude</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/emploi-stage/article/entretien-d-embauche-109899#sommaire_3">Négociation du salaire</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/emploi-stage/article/entretien-d-embauche-109899#sommaire_4">Questions préférées des recruteurs</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/emploi-stage/article/entretien-d-embauche-109899#sommaire_5">Erreurs à éviter</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/emploi-stage/article/entretien-d-embauche-109899#sommaire_6">Après l’entretien</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Conduite de l’entretien</h3>
<p>L’entretien d’embauche est un exercice qu’il faut soigneusement préparer.</p>
<p>L’entretien peut avoir lieu avec une seule personne ou un panel de recruteurs qui vous poseront des questions pour savoir si vous êtes la personne recherchée pour le poste. Ces questions porteront sur la motivation, les études et l’entreprise elle-même. Si certains points de la lettre de motivation n’ont pas été suffisamment développés, des questions pourront être posées pour en savoir davantage.</p>
<p>Il est essentiel de s’informer sur l’entreprise mais aussi sur toutes les questions d’actualité dans son secteur d’activité. Si vous parlez sans hésitation de l’entreprise, vous laisserez une bonne impression.</p>
<p>Vous pourrez également apporter avec vous une copie de votre CV, de vos diplômes, des témoignages ou lettres de recommandation d’employeurs ainsi que tout autre document qui pourrait vous être utile.</p>
<p>Lors de l’entretien, vous devez vous exprimer avec assurance, faire en sorte de développer vos réponses par des exemples concrets venant de votre expérience professionnelle. L’employeur attend que vous soyez positif et convaincant.</p>
<p>A la fin de l’entretien, il est bien vu de poser des questions sur le poste ou l’entreprise afin de montrer votre intérêt.</p>
<h3 class="spip"><a id="sommaire_2"></a>Apparence et attitude</h3>
<p>L’impression des recruteurs se base essentiellement sur le visuel. Par conséquent, l’aspect soigné et professionnel est de rigueur. En ce qui concerne la tenue vestimentaire, adoptez un style plutôt classique.</p>
<h3 class="spip"><a id="sommaire_3"></a>Négociation du salaire</h3>
<p>En règle générale le salaire est <strong>librement déterminé</strong> par les deux parties au moment de la signature du <a href="http://www.guichet.public.lu/entreprises/fr/ressources-humaines/contrat-convention/contrat-travail/duree-indeterminee/index.html" class="spip_out" rel="external">contrat de travail</a>.</p>
<h3 class="spip"><a id="sommaire_4"></a>Questions préférées des recruteurs</h3>
<ul class="spip">
<li>Pourquoi avez-vous suivi cette formation ?</li>
<li>Quelles sont les missions et les responsabilités qui vous ont été confiées lors de vos précédentes expériences professionnelles ? Quelle est votre situation sociale ?</li>
<li>Quels sont vos centres d’intérêt ?</li>
<li>Quels sont vos qualités et vos défauts ?</li>
<li>Quels journaux ou magazines lisez-vous régulièrement ? Quel est votre point de vue sur tel aspect d’une actualité ?</li>
<li>Quelles sont vos ambitions et vos attentes dans notre entreprise ? Quel intérêt aurait notre entreprise à vous recruter ?</li>
<li>Quels sont vos objectifs professionnels ? Pourquoi avez-vous postulé pour ce poste ?</li></ul>
<h3 class="spip"><a id="sommaire_5"></a>Erreurs à éviter</h3>
<ul class="spip">
<li>Paraître hésitant et ne pas bien connaître son CV ;</li>
<li>Négocier la rémunération dès le premier entretien (dans la plupart des cas, vous participerez à plusieurs entretiens avant que l’employeur ne fasse définitivement son choix) ;</li>
<li>Evoquer les choses que vous ne voulez pas faire ;</li>
<li>Les déclarations malhonnêtes et trompeuses.</li></ul>
<h3 class="spip"><a id="sommaire_6"></a>Après l’entretien</h3>
<p>Adresser un mot de remerciements au recruteur à l’issue de l’entretien est un "plus" qui peut faire la différence.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/emploi-stage/article/entretien-d-embauche-109899). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
