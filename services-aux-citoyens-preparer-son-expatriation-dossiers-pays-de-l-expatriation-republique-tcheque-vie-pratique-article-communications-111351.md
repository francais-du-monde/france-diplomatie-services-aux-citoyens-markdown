# Communications

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/vie-pratique/article/communications-111351#sommaire_1">Téléphone – Internet</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/vie-pratique/article/communications-111351#sommaire_2">Poste</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Téléphone – Internet</h3>
<h4 class="spip">Téléphone</h4>
<p>On ne rencontre aucune difficulté avec les liaisons téléphoniques.</p>
<p>Le préfixe international de la République tchèque est le +420 (ou 00 420). Si vous appelez de l’étranger le préfixe est nécessaire avant de composer le numéro à neuf chiffres de votre correspondant en République tchèque.</p>
<h5 class="spip">Téléphones mobiles</h5>
<p>Sur le territoire de la République tchèque il existe trois opérateurs de téléphones mobiles. Le pays est couvert pratiquement à 100% et les réseaux fonctionnent sous le système GSM 900 et 1800 qui est compatible avec toute l’Europe et l’Australie. Certains problèmes peuvent arriver avec des appareils conçus pour l’Amérique du Nord et le Japon.</p>
<p>Les clients reçoivent un message automatique les informant du coût des appels, des SMS et des services de transmission de données à l’étranger (en cas de navigation sur internet ou de téléchargement d’un film, p. ex.). Les prix de gros que les opérateurs se facturent mutuellement sont également plafonnés, ce qui devrait réduire les coûts supportés par les consommateurs. Un mécanisme a également été mis en place afin d’éviter aux consommateurs de devoir payer des factures astronomiques pour des services d’itinérance : après un premier avertissement, la connexion internet mobile est suspendue dès que le montant atteint un certain niveau. Une limite de 50 euros par mois s’applique par défaut si le client n’a pas défini un montant spécifique.</p>
<p>Vous trouverez sur un <a href="http://ec.europa.eu/information_society/activities/roaming/index_en.htm" class="spip_out" rel="external">site web de l’UE</a> les eurotarifs appliqués par les opérateurs des 28 pays membres, ainsi que les liens vers leurs sites. Vous pourrez également y comparer les tarifs pratiqués pour l’envoi de SMS ou l’utilisation de services de données.</p>
<p><i>Source : <a href="http://europa.eu/travel/comm/index_fr.htm#phone" class="spip_out" rel="external">Europa.eu</a></i></p>
<h5 class="spip">Opérateurs de mobiles </h5>
<ul class="spip">
<li><a href="http://www.cz.o2.com/" class="spip_out" rel="external">O2</a> – numéro de téléphone 800 02 02 02</li>
<li><a href="http://www.t-mobile.cz/" class="spip_out" rel="external">T-Mobile</a> – numéro de téléphone <strong>603 603 603</strong></li>
<li><a href="http://www.vodafone.cz/" class="spip_out" rel="external">Vodafone</a> – numéro de téléphone 800 777 777</li></ul>
<p>Tous les opérateurs offrent aussi des cartes SIM avec un crédit prépayé (300 – 2 000 CZK) sans nécessité de contrat ce qui est très pratique surtout pour les séjours de courte ou moyenne durée. Pour utiliser les cartes SIM tchèques il faut néanmoins avoir un appareil téléphonique débloqué.</p>
<h5 class="spip">Cabines téléphoniques publiques</h5>
<p>La République tchèque possède un nombre très important de cabines téléphoniques. Dans la plupart de cas elles fonctionnent avec des cartes téléphoniques (150, 200 et 300 CZK) ou des pièces tchèques (minorité).</p>
<p>Avec la carte téléphonique TRICK vous pouvez même envoyer des messages SMS d’une cabine téléphonique</p>
<h4 class="spip">Internet</h4>
<p>La République tchèque a un bon réseau de connexion Internet. La connexion par modem dans les foyers est remplacée par des lignes ISDN et des réseaux Wi-Fi.</p>
<p>Une connexion internet haut débit coûte environ 1000 couronnes par mois.</p>
<h5 class="spip">Possibilités d’accès</h5>
<p>Vous pouvez vous connecter à Internet grâce aux technologies Wi-Fi, ADSL, connexion par mobile ou grâce aux câbles avec un Internet à grande vitesse.</p>
<h5 class="spip">Accessibilité à Internet</h5>
<p>Un ordinateur avec un accès à Internet est déjà un équipement standard dans chaque hôtel. Des cafés Internet sont aussi à votre disposition où les prix sont autour de 60 CZK (2 EUR) par heure.</p>
<h5 class="spip">Internet gratuit</h5>
<p>Surtout dans les grandes villes il y a de plus en plus de points hot spot (Wi-Fi) où vous pouvez vous connecter gratuitement. Ils sont le plus souvent dans les restaurants ou les hôtels mais un réseau d’Internet gratuit peut être trouvé par exemple dans certaines mairies.</p>
<p><i>Source : <a href="http://www.czechtourism.com/" class="spip_out" rel="external">Czechtourism.com</a> (informations pratiques)</i></p>
<h4 class="spip">Téléphoner gratuitement par Internet </h4>
<p>Il est généralement très avantageux d’utiliser <strong>des logiciels de téléphonie sur IP</strong> (Skype, Google talk, Live messenger, Yahoo ! messenger, etc.) car les tarifs proposés à l’international sont souvent moindre que ceux des opérateurs locaux exploitant des lignes classiques, voire gratuits dans certaines conditions.</p>
<h3 class="spip"><a id="sommaire_2"></a>Poste</h3>
<p>On ne rencontre aucune difficulté avec les liaisons postales. Les délais postaux sont en général de 2/3 jours entre la France et la République tchèque.</p>
<p>Les services officiels de la poste sont assurés par la poste tchèque (Ceská pošta). Elle a le monopole pour les colis de moins de 50 grammes. Pour le reste vous pouvez sélectionner parmi des dizaines de sociétés tchèques ou internationales qui sont présentes sur le territoire de la République tchèque.</p>
<h4 class="spip">Colis internationaux</h4>
<p><strong>La poste tchèque</strong> (<a href="http://www.cpost.cz/" class="spip_out" rel="external">www.cpost.cz</a>) distribue le courrier dans le monde entier. Une lettre de moins de 20 grammes en Europe coûte 10 CZK (environs 0,30 EUR) et 12 CZK hors Europe. Les colis internationaux peuvent être contrôlés pour des raisons de sécurité comme c´est le cas ailleurs dans le monde.</p>
<h4 class="spip">Colis en République tchèque</h4>
<p>Les colis de moins de 50 grammes sont délivrés en République tchèque le jour ouvrable suivant.</p>
<p><i>Source : <a href="http://www.czechtourism.com/" class="spip_out" rel="external">Czechtourism.com</a></i></p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/vie-pratique/article/communications-111351). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
