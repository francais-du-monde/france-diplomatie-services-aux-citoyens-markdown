# Logement

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/vie-pratique/article/logement-110344#sommaire_1">Où se loger ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/vie-pratique/article/logement-110344#sommaire_2">Conditions de location</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/vie-pratique/article/logement-110344#sommaire_3">Hôtels</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/vie-pratique/article/logement-110344#sommaire_4">Electricité</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/vie-pratique/article/logement-110344#sommaire_5">Electroménager</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Où se loger ?</h3>
<h4 class="spip">Bucarest</h4>
<p>Les quartiers résidentiels se situent dans le centre-ville.</p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Type de logement</td>
<td>Loyer mensuel dans les quartiers résidentiels (euros)</td>
<td>Loyer mensuel dans la banlieue (euros)</td></tr>
<tr class="row_even even">
<td>Studio</td>
<td>350</td>
<td>250</td></tr>
<tr class="row_odd odd">
<td>2 pièces</td>
<td>500</td>
<td>350</td></tr>
<tr class="row_even even">
<td>3 pièces</td>
<td>760</td>
<td>500</td></tr>
<tr class="row_odd odd">
<td>4 pièces</td>
<td>1000</td>
<td>700</td></tr>
</tbody>
</table>
<p>Le montant des charges varie de 80 à 250 euros par mois. Ce poste de dépense ne cesse d’augmenter.</p>
<h3 class="spip"><a id="sommaire_2"></a>Conditions de location</h3>
<p>Il existe un marché locatif privé, mais les offres doivent être examinées avec prudence.</p>
<p>La durée d’un bail est généralement d’un an. Le loyer est payable d’avance et une caution de un à trois mois est exigée. Un état des lieux est absolument nécessaire. En cas de location par l’intermédiaire d’une agence, la commission de celle-ci correspond généralement à un mois de loyer. Les commissions en pourcentage sont négociables.</p>
<h3 class="spip"><a id="sommaire_3"></a>Hôtels</h3>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Prix moyen d’une chambre d’hôtel (chambre double)</td>
<td>lei</td>
<td>euros</td></tr>
<tr class="row_even even">
<td>Grand tourisme</td>
<td>760</td>
<td>170</td></tr>
<tr class="row_odd odd">
<td>Moyen tourisme</td>
<td>270</td>
<td>60</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_4"></a>Electricité</h3>
<p>Le courant est de 220 volts (50 hz par seconde). Les prises sont de type européen à l’exception des prises de terre.</p>
<h3 class="spip"><a id="sommaire_5"></a>Electroménager</h3>
<p>Les appartements à louer sont majoritairement meublés et équipés, en bon état, surtout ceux situés dans la zone centrale et des quartiers résidentiels du nord de Bucarest.</p>
<p>La climatisation est appréciable pendant les mois de juillet et août, où les températures peuvent atteindre 37°C.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/vie-pratique/article/logement-110344). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
