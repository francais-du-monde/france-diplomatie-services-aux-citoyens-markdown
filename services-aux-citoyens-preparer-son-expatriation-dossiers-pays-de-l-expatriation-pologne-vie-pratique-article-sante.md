# Santé

<p>Pour une présentation des risques épidémiologiques ainsi que des conseils sur la médecine au quotidien, consultez les pages suivantes :</p>
<ul class="spip">
<li>Conseils, <a href="http://www.ambafrance-pl.org/Sante,311" class="spip_out" rel="external">liste de médecins et hôpitaux</a> sur le site de l’ambassade de France à Varsovie</li>
<li>Page dédiée à la santé en Pologne sur le site Conseils aux voyageurs</li>
<li>Fiche Varsovie sur le site du <a href="http://www.cimed.org" class="spip_out" rel="external">CIMED</a></li>
<li>Fiche Cracovie sur le site du <a href="http://www.cimed.org" class="spip_out" rel="external">CIMED</a></li></ul>
<h4 class="spip">Médecine de soin</h4>
<p>L’infrastructure hospitalière est satisfaisante à Varsovie, où les soins d’urgence sont corrects alors qu’ils peuvent être de qualité inégale dans le reste du pays. Le paiement des soins est parfois exigé à l’avance et en liquide. Les cliniques privées sont plus onéreuses mais bien équipées.</p>
<p>Le tarif d’une consultation chez un médecin généraliste varie entre 120-200 PLN ; la consultation à domicile (médecin français) coûte de 150 à 300 PLN.</p>
<p><strong>Dans le cadre d’un bref séjour, se munir de la carte européenne d’assurance maladie à demander à votre caisse d’assurance maladie au moins 15 jours avant le départ.</strong> Pour plus d’information, consultez le site de l’<a href="http://www.ameli.fr/assures/droits-et-demarches/a-l-etranger/index.php" class="spip_out" rel="external">Assurance maladie</a> en ligne.</p>
<p><strong>Consulter son médecin traitant avant le départ et contracter une assurance de rapatriement sanitaire.</strong></p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Site de l’<a href="http://www.ambafrance-pl.org/" class="spip_out" rel="external">ambassade de France en Pologne</a> </p>
<p>Pour de plus amples renseignements, vous pouvez consulter le site de l’<a href="http://www.invs.sante.fr/" class="spip_out" rel="external">INVS</a>, ainsi que celui de l’<a href="http://www.who.int/fr/index.html" class="spip_out" rel="external">OMS</a>, qui vous renseigneront sur l’état sanitaire de ce pays, ou les sites de <a href="http://www.pasteur-lille.fr/" class="spip_out" rel="external">l’institut Pasteur de Lille</a> et de <a href="http://www.pasteur.fr/" class="spip_out" rel="external">l’institut Pasteur de Paris</a>.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/vie-pratique/article/sante). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
