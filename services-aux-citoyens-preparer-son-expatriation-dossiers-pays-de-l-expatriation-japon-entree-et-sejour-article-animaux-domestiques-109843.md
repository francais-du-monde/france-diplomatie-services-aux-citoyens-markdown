# Animaux domestiques

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/entree-et-sejour/article/animaux-domestiques-109843#sommaire_1">Chiens et chats</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/entree-et-sejour/article/animaux-domestiques-109843#sommaire_2">Autres animaux</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Chiens et chats</h3>
<ul class="spip">
<li>Il vous faudra faire une déclaration d’importation au moins quarante jours avant la date prévue d’arrivée au Japon</li>
<li>Il sera procédé à une identification de l’animal par micropuce (normes ISO).</li></ul>
<p>Dans le cas de chiens ou de chats provenant de régions ne figurant pas dans la liste des régions indemnes de rage définie par le ministre de l’Agriculture, des forêts et des pêches (MAFF) il faudra après l’implantation d’une micropuce permettant l’identification (la France n’étant pas classée pays indemne de rage les procédures données ci-après s’appliquent obligatoirement) :</p>
<ul class="spip">
<li>plus de deux injections d’un vaccin antirabique contenant des virus inactivés (à partir de l’âge de 91 jours et en respectant l’intervalle prescrit entre les inoculations) ;</li>
<li>Un test sanguin (certificat de titrage des anticorps neutralisant le virus rabique) ;</li>
<li>Un délai d’attente de 180 jours après ce dosage des anticorps, sur place dans le pays exportateur, permettra de réduire considérablement la période de mise en quarantaine pour examen au moment de l’arrivée au Japon.</li></ul>
<p>Pour les détails, nous vous conseillons de prendre connaissance des consignes publiées sur le site (en anglais) du service de contrôle sanitaire ou de quarantaine des animaux du <a href="http://www.maff.go.jp/aqs/english/animal/dog/import-other.html" class="spip_out" rel="external">ministère de l’agriculture, des forêts et de la pêche du Japon</a></p>
<h3 class="spip"><a id="sommaire_2"></a>Autres animaux</h3>
<p>Veuillez vous référer au <a href="http://www.maff.go.jp/aqs/english/animal/im_index.html" class="spip_out" rel="external">site (en anglais) de la quarantaine animalière</a>.</p>
<p>Dans le cas ou votre animal ne se trouverait pas dans les catégories mentionnées sur ce site, veuillez contacter directement le service de la Quarantaine animalière de votre aéroport d’arrivée (site en anglais) : <a href="http://www.maff.go.jp/aqs/english/contactus.html" class="spip_out" rel="external">http://www.maff.go.jp/aqs/english/contactus.html</a></p>
<p><strong>Pour plus d’informations :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Consultez notre article <a href="services-aux-citoyens-preparer-son-expatriation-douanes-article-animaux-domestiques.md" class="spip_in">Animaux domestiques</a></p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/entree-et-sejour/article/animaux-domestiques-109843). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
