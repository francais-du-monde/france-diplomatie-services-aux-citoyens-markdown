# Santé

<p>Pour une présentation des risques épidémiologiques ainsi que des conseils sur la médecine au quotidien, vous pouvez consulter les pages suivantes :</p>
<ul class="spip">
<li>Conseils, <a href="http://www.ambafrance-ht.org/Service-ambulancier" class="spip_out" rel="external">liste de médecins et hôpitaux</a> sur le site du consulat de France en Haïti.</li>
<li>Page dédiée à la santé en Haïti sur le site <a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/haiti/" class="spip_in">Conseils aux voyageurs</a></li>
<li>Fiche Haïti sur le site du <a href="http://www.cimed.org" class="spip_out" rel="external">CIMED</a>.</li></ul>
<p><i>Mise à jour : août 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/vie-pratique/article/sante). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
