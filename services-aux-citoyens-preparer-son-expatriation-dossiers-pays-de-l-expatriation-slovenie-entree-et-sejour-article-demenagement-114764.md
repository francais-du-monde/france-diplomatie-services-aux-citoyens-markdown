# Déménagement

<p>S’agissant d’un déménagement international, il est primordial de s’entourer d’un maximum de garanties en faisant appel à un professionnel disposant de certifications reconnues (marque NF Service, ISO 9002, etc.).</p>
<p><a href="http://www.csdemenagement.fr/" class="spip_out" rel="external">Chambre syndicale du déménagement</a><br class="manualbr">Téléphone : 01 49 88 61 40 <br class="manualbr">Télécopie : 01 49 88 61 46<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/entree-et-sejour/article/demenagement-114764#contact#mc#csdemenagement.fr#" title="contact..åt..csdemenagement.fr" onclick="location.href=mc_lancerlien('contact','csdemenagement.fr'); return false;" class="spip_mail">Courriel</a></p>
<p>Pour un déménagement de France vers la Slovénie, le transport par la route apparaît comme la meilleure solution. Le délai d’acheminement par ce mode de transport n’excède pas trois jours. Un acheminement par voie maritime (port de Koper) est envisageable pour les provenances plus lointaines.</p>
<p>En 2013, le coût pour un déménagement de 40 m<sup>3</sup> entre la France et la Slovénie est d’environ 8500 euros. Il existe une dizaine de transporteurs spécialisés, français et slovènes, travaillant sur la Slovénie.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Article thématique <a href="services-aux-citoyens-preparer-son-expatriation-douanes-article-demenagement.md" class="spip_in">Déménagement</a>.</p>
<p><i>Mise à jour : septembre 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/entree-et-sejour/article/demenagement-114764). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
