# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/emploi-stage/article/reglementation-du-travail#sommaire_1">La durée légale du travail </a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/emploi-stage/article/reglementation-du-travail#sommaire_2">Les contrats de travail</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/emploi-stage/article/reglementation-du-travail#sommaire_3">Le licenciement</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/emploi-stage/article/reglementation-du-travail#sommaire_4">Les congés</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/emploi-stage/article/reglementation-du-travail#sommaire_5">Les cotisations sociales</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/emploi-stage/article/reglementation-du-travail#sommaire_6">La retraite</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/emploi-stage/article/reglementation-du-travail#sommaire_7">Fêtes légales</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/emploi-stage/article/reglementation-du-travail#sommaire_8">Emploi du conjoint</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>La durée légale du travail </h3>
<p>La durée légale hebdomadaire de travail est de 48 heures étalée sur six jours, soit huit heures par jour. Les heures supplémentaires donnent droit à un paiement additionnel de 25% par heure pour les deux premières et de 35% au-delà. L’employé et l’employeur peuvent également les compenser par un système de repos.</p>
<h3 class="spip"><a id="sommaire_2"></a>Les contrats de travail</h3>
<p>Le <strong>contrat à durée indéterminée</strong> est conclu par écrit ou par oral et ne stipule pas de date d’échéance.</p>
<p>La période d’essai est de trois mois. Elle peut être prolongée jusqu’à six mois pour les travailleurs qualifiés et un an pour le personnel de direction et les travailleurs de confiance. Il n’est pas nécessaire de l’enregistrer au ministère du Travail.</p>
<p>Les contrats de travail du personnel étranger doivent être faits par écrit, pour une durée maximum de trois ans. Ils pourront être renouvelés sur simple accord des parties et devront être déclarés au ministère du Travail.</p>
<p>Le <strong>contrat à durée déterminée</strong> est obligatoirement conclu par écrit, en trois exemplaires, et prend fin à la date indiquée (en règle générale, il est d’une durée maximale d’un an). Il doit répondre à un besoin provisoire. Le contrat doit être déclaré par l’entreprise au ministère du Travail dans les 15 jours à compter de la signature.</p>
<p>Le <strong>contrat à temps partiel</strong> est conclu par écrit et comporte une date de début et une date d’achèvement. Il est utilisé en cas de besoin exceptionnel, notamment pour des activités saisonnières. Le temps de travail ne doit pas excéder quatre heures par jour ou 24 heures par semaine. Ce type de contrat doit être déclaré au ministère du Travail et comporte des droits sociaux limités (licenciement sans conditions).</p>
<h3 class="spip"><a id="sommaire_3"></a>Le licenciement</h3>
<p>En cas de démission, le préavis obligatoire est de 30 jours.</p>
<p>En cas de licenciement, l’employé dispose de six jours après la notification pour se défendre, sauf en cas de faute grave.</p>
<p>S’il est renvoyé sans motifs légitimes, il a droit à une indemnisation spéciale pour licenciement arbitraire correspondant à un mois et demi de salaire par année de service avec un plafond fixé à 12 mois d’indemnités. Dans le cas d’un licenciement pour faute grave, l’employé n’a droit à aucune indemnité.</p>
<h3 class="spip"><a id="sommaire_4"></a>Les congés</h3>
<p>Tout salarié a droit à un jour de repos par semaine, de préférence le dimanche. Les jours fériés sont rémunérés comme une journée de travail normale. Les salariés obtiennent, après un an de travail, 30 jours de vacances. Ils peuvent les aménager, en concertation avec l’employeur, en plusieurs périodes d’au moins sept jours ou les réduire à 15, en contrepartie d’une compensation financière. Les employés à temps partiel (jusqu’à 4h de travail quotidien) n’ont droit qu’à six jours maximum de congés annuels.</p>
<p>Le congé de maternité est de 45 jours avant l’accouchement et de 45 jours après.</p>
<h3 class="spip"><a id="sommaire_5"></a>Les cotisations sociales</h3>
<h4 class="spip">A la charge de l’employeur</h4>
<p><i>La sécurité sociale : </i>l’entreprise verse pour la prise en charge des soins de santé courants 9% du salaire mensuel de l’employé au <i>Seguro Social de Salud <strong>EsSalud</strong> </i>(sécurité sociale péruvienne) ou à une des entreprises prestataires de services (EPS).</p>
<p><i>La compensation pour temps de travail (CTS)</i> : la CTS protège les employés au moment de leur cessation d’activité : ils percevront l’équivalent d’un salaire par année de service. Au Pérou, l’employeur doit déposer en mai et en novembre, sur le compte bancaire du travailleur, un douzième de la rémunération mensuelle, primes incluses, multiplié par le nombre de mois travaillés dans le semestre.</p>
<p><i>Les allocations familiales</i> : les salariés ayant des enfants mineurs ou majeurs de moins de 25 ans suivant des études supérieures, reçoivent 10% du salaire minimum indépendamment du nombre d’enfants.</p>
<p><i>L’assurance-vie</i> : après quatre ans de travail, l’employeur doit cotiser à une assurance-vie équivalente à 0,63% de la rémunération mensuelle pour les salariés, pour subvenir aux besoins de l’employé et de sa famille en cas d’incapacité permanente ou de décès par accident.</p>
<h4 class="spip">A la charge de l’employé</h4>
<p><i>Les cotisations de retraite</i> : le salarié doit cotiser auprès d’un fonds de retraite soit public (Sistema Nacional de Pensiones) en versant 13% de son salaire, soit privé (Administradoras de Fondos de Pensiones - AFP) en versant en moyenne 12,9%.</p>
<h3 class="spip"><a id="sommaire_6"></a>La retraite</h3>
<p>L’âge normal de la retraite est fixé à 65 ans par la législation péruvienne.</p>
<p><i>Source : L’essentiel d’un marché Pérou et Bolivie - Ubifrance 2010-2011</i></p>
<p>L’ensemble de la réglementation du travail au Pérou est consultable sur le site internet du <a href="http://www.mintra.gob.pe/" class="spip_out" rel="external">ministère péruvien du Travail et de l’Emploi</a>.</p>
<h3 class="spip"><a id="sommaire_7"></a>Fêtes légales</h3>
<ul class="spip">
<li>1er janvier - jour de l’An</li>
<li>Jeudi et Vendredi Saints</li>
<li>1er mai - Fête du travail</li>
<li>29 juin - San Pedro et San Pablo</li>
<li>28 et 29 juillet - Fête Nationale</li>
<li>30 août - Sainte Rose à Lima</li>
<li>8 octobre - bataille d’Angamos</li>
<li>1er novembre - Toussaint</li>
<li>8 décembre - Immaculée conception</li>
<li>25 décembre - Noël</li></ul>
<h3 class="spip"><a id="sommaire_8"></a>Emploi du conjoint</h3>
<p>Les possibilités d’emplois pour les expatriés ou leurs conjoints sont quasiment nulles. Le conjoint marié devra effectuer une demande de visa <strong>rattaché de famille</strong> (<i>Llamado de familia</i>) en fournissant une copie de l’acte de mariage traduite et légalisée. Le concubinage n’est pas reconnu au Pérou, le concubin est conduit à effectuer une succession de "séjours touristiques".</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/emploi-stage/article/reglementation-du-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
