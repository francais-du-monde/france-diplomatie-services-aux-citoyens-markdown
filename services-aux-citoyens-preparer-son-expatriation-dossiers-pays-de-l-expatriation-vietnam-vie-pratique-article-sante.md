# Santé

<p>Pour une présentation des risques épidémiologiques ainsi que des conseils sur la médecine au quotidien, consultez les pages suivantes :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Fiches Hanoi et Ho Chi Minh Ville sur le site du <a href="http://www.cimed.org/" class="spip_out" rel="external">CIMED</a>.</p>
<h4 class="spip">Médecine de soins</h4>
<h4 class="spip">Soins sur place</h4>
<p>Il n’y a pas de risque majeur sur le plan sanitaire et médical à vivre au Vietnam, surtout si vous vivez dans les grandes villes.</p>
<p>Des hôpitaux très modernes ont fait leur apparition, surtout à Hanoi et à Ho Chi Minh Ville. Les centres de soins privés et internationaux sont de bonne qualité mais à des tarifs élevés.</p>
<p>Le tarif des consultations varie de 40 à 100$ (clinique internationale), à partir de 10$ (praticiens locaux).</p>
<p>Le coût d’une journée d’hôpital, modéré dans les structures vietnamiennes, est très élevé et variable selon le motif de l’hospitalisation dans une structure internationale.</p>
<p>L’hôpital Franco-vietnamien d’Ho Chi Minh Ville, ouvert en mars 2003, offre des prestations de qualité européenne à un tarif raisonnable. A Hanoi, les soins sont de bonne qualité dans les établissements de standart international (Hopital français, SOS International, Hanoi Family Medical Practice).</p>
<h4 class="spip">Recommandations </h4>
<p>Avant le départ : consultez votre médecin (éventuellement votre dentiste) et souscrivez à une compagnie d’assistance couvrant les frais médicaux et de rapatriement sanitaire.</p>
<h5 class="spip">Grippe Aviaire</h5>
<p>Il s’agit d’une maladie virale animale (volailles) exceptionnellement transmissible à l’homme.</p>
<p>La direction générale de la santé recommande aux voyageurs d’éviter tout contact avec les volailles et les oiseaux, en évitant notamment de se rendre dans des élevages ou les marchés aux volatiles. Les recommandations générales d’hygiène qui visent à se protéger des infections microbiennes, sont préconisées :</p>
<ul class="spip">
<li>éviter de consommer des produits alimentaires crus ou peu cuits, en particulier les viandes et les œufs ;</li>
<li>se laver régulièrement les mains à l’eau et au savon ou un soluté hydro-alcoolique. Le virus se transmet par voie aérienne (voie respiratoire) soit par contact direct, notamment avec les sécrétions respiratoires et les matières fécales des animaux malades, soit de façon indirecte par l’exposition à des matières contaminées (par l’intermédiaire de la nourriture, de l’eau, du matériel et des mains ou des vêtements souillés).</li></ul>
<p>Les espaces confinés favorisent la transmission du virus.</p>
<p>Sur place, ou après le retour en France, la survenue de fièvre doit inciter à consulter un médecin.</p>
<p>Par ailleurs, des pathologies transmises par des moustiques (dengue, paludisme…) sévissent dans ces mêmes zones géographiques. Face à un syndrome de type grippal, ces deux maladies restent plus probables que la grippe A/H5N1. La DGS rappelle donc les recommandations concernant la protection contre les piqûres de moustique et prophylaxie antipaludéenne adaptée.</p>
<h5 class="spip">Prévention des maladies transmises par les piqûres d’insectes</h5>
<ul class="spip">
<li>Dengue : présence endémique de la dengue : des précautions doivent être prises dans l’ensemble du pays. Cette maladie virale est transmise par les piqûres de moustiques : il convient donc de respecter les mesures habituelles de protection (vêtements longs, produits anti-moustiques à utiliser sur la peau et sur les vêtements, diffuseurs électriques) La dengue pouvant prendre une forme potentiellement grave il est vivement recommandé de consulter un médecin en cas de fièvre (la prise d’aspirine est déconseillée). D’autres maladies virales, également transmises par les piqûres de moustiques, peuvent parfois survenir.</li>
<li>Paludisme : prévention du paludisme (malaria) : maladie parasitaire transmise par les piqûres de moustiques qui impose le recours à des mesures de protection individuelle (sprays, crèmes, diffuseurs électriques, moustiquaires…) A ces mesures, doit s’ajouter un traitement médicamenteux adapté à chaque individu : il convient de s’adresser à votre médecin habituel ou à un centre de conseils aux voyageurs. Le traitement devra être poursuivi après le retour en France durant une durée variable selon le produit utilisé. Classification : bande côtière et deltas : pas de chimioprophylaxie. Partout ailleurs, zone 3.</li>
<li>Encéphalite japonaise : cette maladie, qui n’existe qu’en Asie, se transmet par des moustiques, en zone rurale. Des cas humains ont été recensés dans les provinces du Nord du Vietnam. Ces cas peuvent être mortels ou engendrer des séquelles neurologiques graves. De ce fait, la vaccination contre l’encéphalite japonaise peut être nécessaire (à pratiquer dans un centre médical sur place). Dans le cadre d’un voyage touristique, il semble que les mesures physiques (vêtements longs, répulsifs…) soient une arme efficace.</li></ul>
<h5 class="spip">Vaccinations</h5>
<p>La mise à jour de la vaccination diphtérie-tétanos-polimyélite est conseillée.</p>
<p>Autres vaccinations conseillées (selon conditions d’hygiène et durée du séjour) : fièvre typhoïde, hépatites virales A et B.</p>
<p>Autres vaccinations : en cas de séjour en zone rurale, la vaccination contre l’encéphalite japonaise peut être nécessaire.</p>
<p>La rage est endémique au Vietnam ainsi que dans toute l’Asie du Sud-est. La plus grande prudence vis-à-vis des animaux errants est recommandée sur l’ensemble du territoire vietnamien, villes incluses. La prévention repose sur l’absence de contact avec des animaux suspects et la vaccination préventive - en dehors de toute exposition au virus- qui comprend trois injections initiales (J0 - J7 - J21 ou J28), suivies par un rappel à un an puis tous les cinq ans. Elle est particulièrement recommandée pour les professions à risques, les enfants en bas âge, les amateurs de jogging, mais aussi pour toute personne susceptible de rentrer en contact avec un animal atteint, surtout en cas de séjour en milieu rural ou éloigné des villes principales.</p>
<h5 class="spip">Hygiène alimentaire</h5>
<p>Il est conseillé de ne pas boire l’eau du robinet : préférer les eaux en bouteilles capsulées, d’origine locale ou importées. A défaut, filtrer l’eau et la faire bouillir avant consommation. Eviter la consommation de poisson de viande et de volailles crus.</p>
<h5 class="spip">Sida</h5>
<p>Prévalence non négligeable du VIH - sida. Des mesures de prévention doivent être appliquées.</p>
<h4 class="spip">Contacts utiles sur place </h4>
<h4 class="spip">Hanoi</h4>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Hôpital Français <br class="manualbr">Phuong Mai, Dong Da. <br class="manualbr">Tél. : (84-4) 3577 11 00 </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Société SOS International <br class="manualbr">31 Hai Ba Trung. <br class="manualbr">Tél. : (84-4) 3934 05 55 (urgences) ou 3934 06 66 </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Hanoi Family Medical Practice <br class="manualbr">A1 Van Phuc, Kim Ma. <br class="manualbr">Tél. : (84-4) 3846 17 48/49 ou 3843 0748, urgences 24h/24 : 0903 401 919 </p>
<h4 class="spip">Hô Chi Minh Ville</h4>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Hôpital franco-vietnamien <br class="manualbr">6, rue Nguyen Luang Bang, Salle Than Phu, District 7. <br class="manualbr">Tél. : (84-8) 3411 33 33 </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Société SOS International<br class="manualbr">65, Nguyen Du, Q1. <br class="manualbr">Tél. : (84-8) 3829 84 24 </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Centre médical international <br class="manualbr">1, rue Han Thuyen, District 1. <br class="manualbr">Tél. : (84-8) 3865 40 25 (urgences, 24h/24), 3827 23 66 </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  SOS International <br class="manualbr">1, Le Ngoc Han. <br class="manualbr">Tél. : (84-64) 3858 776 </p>
<p>Pour de plus amples renseignements, consulter les sites Internet suivants :</p>
<ul class="spip">
<li>Le <a href="http://www.sante.gouv.fr/" class="spip_out" rel="external">site du ministère de la Santé</a></li>
<li>Le <a href="http://www.grippeaviaire.gouv.fr/" class="spip_out" rel="external">site interministériel relatif à la grippe aviaire</a></li>
<li>Les informations actualisées de l’<a href="http://www.invs.sante.fr/" class="spip_out" rel="external">Institut de veille sanitaire</a></li>
<li>Le <a href="http://www.cimed.org/" class="spip_out" rel="external">site du CIMED</a></li>
<li>Le <a href="http://www.who.int/" class="spip_out" rel="external">site de l’organisation mondiale de la santé</a> (anglais et français)</li>
<li>Le <a href="http://www.pasteur.fr/" class="spip_out" rel="external">site de l’Institut Pasteur de Paris</a> ou de <a href="http://www.pasteur-lille.fr/" class="spip_out" rel="external">Lille</a></li></ul>
<p><i>Mise à jour : février 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/vie-pratique/article/sante). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
