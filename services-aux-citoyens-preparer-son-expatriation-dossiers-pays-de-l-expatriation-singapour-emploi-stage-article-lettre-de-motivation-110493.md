# Lettre de motivation

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/emploi-stage/article/lettre-de-motivation-110493#sommaire_1">Rédaction</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/emploi-stage/article/lettre-de-motivation-110493#sommaire_2">Modèles de lettre de motivation</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Rédaction</h3>
<h4 class="spip">Sa forme</h4>
<p>Aujourd’hui, la lettre de motivation est dorénavant tapée et non plus manuscrite. Utilisez le correcteur d’orthographe et faites-la corriger afin de vous assurer qu’elle ne contienne aucune erreur.</p>
<p>Elle doit être rédigée en anglais et adressée au <i>Recruiting Manager</i> ou <i>Human Resource Manager</i>. Si vous disposez du nom de la personne, indiquez-le. Votre courrier aura d’autant moins de chance d’être égaré et atteindra directement la personne concernée. Assurez-vous de bien orthographier le nom et de vérifier son poste. Idéalement, vous devez contacter la personne directement en charge de votre recrutement, qui peut-être le directeur technique ou le directeur commercial. Si vous ne savez pas à qui l’adresser ou s’il s’agit d’une organisation de taille modeste, envoyez votre courrier au <i>President</i> ou au <i>Chief executive officer</i> (CEO).</p>
<p>A Singapour, il est beaucoup plus stratégique de s’adresser directement au responsable hiérarchique du poste à pourvoir en complément du Directeur des Ressources Humaines.</p>
<p>Le nom de la compagnie, le nom et le poste du destinataire ainsi que l’adresse doivent apparaître correctement et au complet.</p>
<p>Votre lettre de motivation doit faire une page, être brève, claire et directe. Utilisez un ton professionnel.</p>
<p>Evitez que votre lettre soit monotone, soyez créatif afin d’éveiller l’attention du lecteur, utilisez des phrases telles que : « My responsibilities included … » et décrivez les résultats de vos projets (chiffrés si cela est possible).</p>
<p>Signez manuellement vos lettres.</p>
<p>Vous pouvez inclure un PS afin de mettre en valeur un point fort très illustratif.</p>
<p>Par exemple : « J’ai été le premier stagiaire à recevoir la récompense du meilleur employé du mois chez … »</p>
<p>Si vous voulez un maximum d’impact, écrivez ce PS à la main, ce sera la première chose lue sur votre lettre de motivation.</p>
<p>Assurez-vous que l’impression de vos documents soit de bonne qualité.</p>
<ul class="spip">
<li>Placez le CV derrière la lettre.</li>
<li>Envoyez toujours des originaux, jamais de photocopie.</li></ul>
<h4 class="spip">Sa composition</h4>
<p>Votre lettre sera composée idéalement de quatre paragraphes :</p>
<ul class="spip">
<li>Une brève introduction qui explique qui vous êtes, pourquoi vous écrivez et auquel cas, à quelle annonce vous répondez.</li>
<li>Un ou deux paragraphes pour souligner les points forts de votre cursus et/ou de vos expériences professionnelles qui sont en relation avec le poste que vous convoitez. Vous devez faire passer le message suivant : vous êtes le candidat idéal pour ce poste. Et pourquoi.</li>
<li>Une explication sur votre éligibilité à pouvoir travailler à Singapour (comment vous comptez obtenir votre visa ou pourquoi l’entreprise devrait vous embaucher vous, plutôt qu’un singapourien).<br class="manualbr">Si vous disposez d’un <i>Dependant Pass</i> (dans l’optique où vous avez déjà rejoint votre conjoint à Singapour), précisez-le. Cela signifie que l’entreprise n’aura rien à débourser, ni aucun dossier à mettre en place pour votre permis de travail puisque vous n’aurez qu’à signer une <i>Letter of Consent</i> pour avoir le droit de travailler. Par ailleurs, précisez également votre durée de séjour approximative à Singapour. Plus vous souhaitez rester longtemps (plus de 2-3 ans, plus vous aurez de chance de décrocher un emploi).<br class="manualbr">Si cela vous convient, n’hésitez pas non plus à préciser que vous n’avez pas besoin de <i>package</i> pour expatriés dans le cadre de votre contrat de travail (assurance, logement, scolarité des enfants, déménagement…).</li>
<li>Un paragraphe de clôture dans lequel vous expliquez dans combien de temps vous allez contacter l’employeur, par quel biais et le moyen par lequel il peut vous joindre.</li></ul>
<p>Terminez par une formule de politesse et signez à la main.</p>
<h4 class="spip">Le suivi</h4>
<p>Relancez par téléphone la personne à qui était adressée votre lettre de motivation, environ une semaine après l’envoi de celle-ci. Dans ce but, vous devez tenir à jour un tableau ou tout autre document vous permettant de visualiser les dates et les entreprises auxquelles vous avez envoyé vos candidatures.</p>
<p>Le premier appel téléphonique ou le premier courriel aura pour but de savoir si votre candidature a bien été reçue. Demandez alors s’ils sont intéressés par votre profil. Dans beaucoup de cas, on jugera votre détermination et votre candidature par votre capacité à renouer contact avec les décideurs.</p>
<p>Ce suivi est essentiel que vous répondiez à une offre ou que vous fassiez une candidature spontanée. Il augmente vos chances d’obtenir un entretien de façon considérable et vous permet de garder une ligne de conduite stricte dans l’optique où vous candidatez auprès d’un grand nombre d’entreprises, dans un espace temps limité.</p>
<h3 class="spip"><a id="sommaire_2"></a>Modèles de lettre de motivation</h3>
<ul class="spip">
<li><p class="document_image">
<a class="spip_in" title="Doc:Lettre de motivation - 1er exemple  , 14.5 ko, 0x0" href="http://www.diplomatie.gouv.fr/fr/IMG/docx/lettre_motivation_1_cle8b8456.docx"><img width="52" height="52" alt="Doc:Lettre de motivation - 1er exemple  , 14.5 ko, 0x0" src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L52xH52/docx-1c5f7.png" style="vertical-align: middle;"></a>
<a href="http://www.diplomatie.gouv.fr/fr/IMG/docx/lettre_motivation_1_cle8b8456.docx">Lettre de motivation - 1er exemple  - (Word, 14.5 ko)</a>
</p></li>
<li><p class="document_image">
<a class="spip_in" title="Doc:Lettre de motivation - 2ème exemple  , 14.5 ko, 0x0" href="http://www.diplomatie.gouv.fr/fr/IMG/docx/lettre_motivation_2_cle845a36.docx"><img width="52" height="52" alt="Doc:Lettre de motivation - 2ème exemple  , 14.5 ko, 0x0" src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L52xH52/docx-1c5f7.png" style="vertical-align: middle;"></a>
<a href="http://www.diplomatie.gouv.fr/fr/IMG/docx/lettre_motivation_2_cle845a36.docx">Lettre de motivation - 2ème exemple  - (Word, 14.5 ko)</a>
</p></li></ul>
<p><i>Mise à jour : juin 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/emploi-stage/article/lettre-de-motivation-110493). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
