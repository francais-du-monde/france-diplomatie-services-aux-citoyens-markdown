# Recherche d’emploi

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/emploi-stage/article/recherche-d-emploi#sommaire_1">Outils pour la recherche d’emploi</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/emploi-stage/article/recherche-d-emploi#sommaire_2">Organismes pour la recherche d’emploi</a></li></ul>
<p>Il est difficile de trouver un emploi au Portugal sans une connaissance approfondie de la langue portugaise. Il existe en outre un certain nombre d’outils permettant de trouver des offres d’emploi.</p>
<h3 class="spip"><a id="sommaire_1"></a>Outils pour la recherche d’emploi</h3>
<h4 class="spip">Presse</h4>
<p>La presse portugaise est encore largement utilisée par les entreprises portugaises pour communiquer leurs offres d’emploi.</p>
<p>L’hebdomadaire <a href="http://expresso.sapo.pt/" class="spip_out" rel="external">Expresso</a> est le principal magazine d’annonces d’emploi. Publié le samedi, le supplément emploi <a href="http://expressoemprego.pt/" class="spip_out" rel="external">Expresso Emprego</a> contient jusqu’à une vingtaine de pages d’offres d’emploi, principalement destinées aux cadres, consultants ou dirigeants.</p>
<p>Parmi les quotidiens, le journal de Lisbonne <a href="http://www.dn.pt/" class="spip_out" rel="external">Diário de Notícias</a> est le plus grand annonceur pour les offres d’emploi, particulièrement dans son édition du dimanche. Le quotidien du matin, le <a href="http://www.cmjornal.xl.pt/" class="spip_out" rel="external">Correio da Manha</a>, très lu dans le sud du pays, publie aussi de nombreuses offres d’emploi, en particulier pour des postes qualifiés et non qualifiés dans le secteur tertiaire. Le journal <a href="http://www.publico.pt/" class="spip_out" rel="external">Público</a> possède également une rubrique consacrée aux offres d’emploi. Le <a href="http://www.jn.pt/paginainicial/" class="spip_out" rel="external">Jornal de Notícias</a> propose des emplois essentiellement sur Porto et sa région. Les quotidiens régionaux ou locaux peuvent être très utiles pour des postes d’ouvriers et de techniciens.</p>
<p>Pour des démarches spontanées, à noter également que le mensuel <i>Exame</i> publie de nombreuses informations sur l’économie des entreprises.</p>
<h4 class="spip">Sites internet</h4>
<p>Plusieurs sites internet sont devenus des plateformes importantes où les employeurs portugais diffusent leurs offres d’emploi :</p>
<ul class="spip">
<li><a href="http://www.netemprego.gov.pt/" class="spip_out" rel="external">Netemprego.gov.pt</a></li>
<li><a href="http://www.empregosonline.pt/" class="spip_out" rel="external">Empregosonline.pt</a></li>
<li><a href="http://www.net-empregos.com/" class="spip_out" rel="external">Net-empregos.com</a></li>
<li><a href="http://www.emprego.sapo.pt/" class="spip_out" rel="external">Emprego.sapo.pt</a></li>
<li><a href="http://www.emprego.pt/%20%20%20" class="spip_out" rel="external">Emprego.pt</a></li>
<li><a href="https://www.ofertasdeemprego.pt/" class="spip_out" rel="external">Ofertas-emprego.com/formacao</a></li>
<li><a href="http://www.adecco.pt/" class="spip_out" rel="external">Adecco.pt</a></li>
<li><a href="http://www.expressoemprego.pt/%20" class="spip_out" rel="external">Expressoemprego.pt</a></li></ul>
<p><i>Le signalement d’organismes sur ce site ne saurait engager la responsabilité de la MFE quant à la qualité de leurs services.</i></p>
<h4 class="spip">Les démarches</h4>
<p>Pour multiplier ses chances de succès, le candidat à l’emploi ne doit négliger aucune piste :</p>
<ul class="spip">
<li>les relations professionnelles</li>
<li>les candidatures spontanées (lettre de motivation + curriculum vitae)</li>
<li>la publication de sa propre annonce</li>
<li>les agences pour l’emploi</li>
<li>les centres d’orientation professionnelle</li>
<li>les agences d’intérim</li>
<li>les cabinets de chasseurs de têtes</li>
<li>les cabinets de recrutement</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Organismes pour la recherche d’emploi</h3>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Le Comité consulaire pour l’emploi et la formation professionnelle (CCPEFP)</strong><br class="manualbr">Le Comité consulaire pour l’emploi et la formation professionnelle de Lisbonne a pour but d’aider les Français résidant au Portugal à trouver, sur place, un emploi grâce à la <a href="http://www.ccilf.pt/" class="spip_out" rel="external">Bourse-Emploi</a> gérée par la Chambre de commerce et d’industrie luso-française (environ 1000 emplois/an) mettant en relation les demandeurs d’emplois ayant un profil luso-français avec des entreprises françaises ou portugaises. Chaque mois, <strong>La Lettre de l’emploi</strong> est adressée aux entreprises avec une sélection des curriculum vitae parvenus au cours des trente derniers jours. Tout candidat peut effectuer une inscription en ligne en remplissant un questionnaire. La même démarche peut être effectuée pour une recherche de stage. Les candidats à l’emploi peuvent également consulter la liste des implantations françaises au Portugal.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.ccilf.pt/" class="spip_out" rel="external">Chambre de commerce et d’industrie luso-française</a><br class="manualbr">Service Emploi-Formation<br class="manualbr">Avenida da Liberdade n° 9-7° <br class="manualbr">1250-139 Lisbonne<br class="manualbr">Tél. : [351] 21 324 19 90 <br class="manualbr">Télécopie : [351] 21 342 48 81<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/emploi-stage/article/recherche-d-emploi#emploi#mc#ccilf.pt#" title="emploi..åt..ccilf.pt" onclick="location.href=mc_lancerlien('emploi','ccilf.pt'); return false;" class="spip_mail">Courriel</a></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>CCILF – Délégation du Nord</strong><br class="manualbr">Avenida da Boavista, 1203 - Sala 607, 6° <br class="manualbr">4100 Porto<br class="manualbr">Tél. : [351] 22 605 15 00 <br class="manualbr">Télécopie : [351] 22 605 15 09</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Les services publics portugais pour l’emploi et la formation professionnelle</strong><br class="manualbr">L’institut pour l’emploi et la formation professionnelle (<a href="http://www.iefp.pt/" class="spip_out" rel="external">Instituto do Emprego e da Formaçao Profissional – IEFP</a>) est le principal organisme public de promotion de l’emploi et de lutte contre le chômage au Portugal. Les agences de l’IEFP mettent à disposition de leur public des euroconseillers, qui peuvent renseigner les candidats sur le marché du travail au Portugal. Les services de ces agences publiques pour l’emploi concernent à la fois l’embauche, la formation, l’orientation professionnelle, le chômage. L’inscription n’est pas obligatoire pour consulter les offres. Elle est cependant indispensable pour percevoir les allocations de chômage ou pour bénéficier d’une formation.<br class="manualbr">Serviços Centrais <br class="manualbr">Av. José Malhoa, 11 <br class="manualbr">1099-018 Lisboa <br class="manualbr">Tél. : [351] 21 330 74 00 <br class="manualbr">Télécopie : [351] 21 330 76 17</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/emploi-stage/article/recherche-d-emploi). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
