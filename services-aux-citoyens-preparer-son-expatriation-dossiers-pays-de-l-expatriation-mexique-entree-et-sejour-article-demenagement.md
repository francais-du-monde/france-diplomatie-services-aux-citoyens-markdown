# Déménagement

<p class="chapo">
      S’agissant d’un déménagement international, il est primordial de s’entourer d’un maximum de garanties en faisant appel à un professionnel disposant de certifications reconnues (marque NF Service, ISO 9002, etc.).
</p>
<h4 class="spip">Déménagement</h4>
<p>Pour des raisons de prix, la préférence est généralement donnée au déménagement par bateau, sauf pour de faibles volumes (moins de 1 000 kg) où la voie aérienne est conseillée.</p>
<p>Selon le type de conteneur choisi, les tarifs sont, <strong>par voie maritime</strong>, de l’ordre de :</p>
<ul class="spip">
<li>conteneur exclusif de 20’ (entre 15 et 30 m3) : entre 6 500 et 7 500 €</li>
<li>conteneur exclusif de 40’ (entre 35 et 60 m3) : entre 7 500 et 9 000 €</li>
<li>conteneur partagé (entre 3 et 6 m3) : entre 2 000 et 3 000 €</li>
<li>conteneur partagé (entre 6 et 12 m3) : entre 3 000 et 5 000 €</li></ul>
<p><strong>Par voie aérienne </strong>(en général pour des expéditions inférieures à 6 m3/ 1000 kg) :</p>
<ul class="spip">
<li>fourchette 100 kg - 300 kg : entre 2 000 et 3 000 €</li>
<li>fourchette 300 kg - 500 kg : entre 3 000 et 4 000 €</li>
<li>fourchette 500 kg - 1 000 kg : entre 4 000 et 7 000 €</li></ul>
<p>Ces prix sont des tarifs porte à porte, tout compris, sauf assurance.</p>
<p><strong>Très important : </strong></p>
<ul class="spip">
<li>Faire attention avec le package de vos biens, surtout ceux qui sont fragiles ;</li>
<li>Tout les cartons et paquets doivent être numérotes et figurer sur une liste d’inventaire ;</li>
<li>La liste d’inventaire doit indiquer le total de paquets, le poids (kg), le volume (m3) et la valeur (USD$) à des fins d’assurance</li></ul>
<p><strong>A noter</strong> : la franchise de douane ne peut être obtenue que sur justification de votre statut migratoire : résident temporaire ou résident permanent (voir fiche « passeport, visa, permis de travail »).</p>
<p>Compte tenue de la législation locale, il est vivement déconseillé d’apporter des ordinateurs dans le déménagement, ainsi que d’importer un véhicule neuf ou d’occasion. D’une manière générale, ne pas apporter d’appareils électriques, le courant étant de 110/120 volts et 60Hz. L’importation d’armes à feu (fusil de chasse) est soumise à une réglementation très stricte et nécessite l’obtention d’un permis d’importation délivré par le Ministère de l’Economie (Secretaría de Economía).</p>
<p>Pour en savoir plus, consultez notre <a href="services-aux-citoyens-preparer-son-expatriation-douanes-article-demenagement.md" class="spip_in">article thématique</a> ainsi que le <a href="http://www.aduanas.sat.gob.mx" class="spip_out" rel="external">site de la douane mexicaine</a>.</p>
<p><strong>Chambre syndicale du déménagement :</strong><br class="manualbr">Téléphone : 01 49 88 61 40 <br class="manualbr">Télécopie : 01 49 88 61 46<br class="manualbr">Courriel : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/entree-et-sejour/article/demenagement#contact#mc#csdemenagement.fr#" title="contact..åt..csdemenagement.fr" onclick="location.href=mc_lancerlien('contact','csdemenagement.fr'); return false;" class="spip_mail">contact<span class="spancrypt"> [at] </span>csdemenagement.fr</a><br class="manualbr">Internet : <a href="http://www.csdemenagement.fr/" class="spip_out" rel="external">http://www.csdemenagement.fr</a></p>
<p><i>Mise à jour : septembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/entree-et-sejour/article/demenagement). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
