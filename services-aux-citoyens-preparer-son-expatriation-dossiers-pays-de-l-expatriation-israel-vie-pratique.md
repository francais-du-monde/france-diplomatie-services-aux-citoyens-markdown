# Vie pratique

<h2 class="rub22900">Logement</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/vie-pratique/#sommaire_1">Où se loger ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/vie-pratique/#sommaire_2">Conditions de location</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/vie-pratique/#sommaire_3">Hôtels</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/vie-pratique/#sommaire_4">Electricité </a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/vie-pratique/#sommaire_5">Electroménager </a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Où se loger ?</h3>
<p>Tel AvivA Tel Aviv, près de l’école française, le quartier artistique de Neve Tzedek a su conserver son charme et son authenticité. Les loyers y sont cependant fort élevés et il est très difficile de s’y garer. Le quartier de Jaffa offre une bonne alternative car il tend aussi à devenir de plus en plus résidentiel. Il est également possible de se loger en front de mer, mais les loyers sont très élevés. De manière générale, il est très difficile et très cher de se loger à Tel Aviv si l’on arrive en été en raison de la saison touristique. Beaucoup de studios ou d’appartements mis sur le marché de location sont loués meublés à la semaine ou au mois. Les hôtels affichent complets et augmentent leurs prix. Si vous en avez la possibilité, il est donc préférable de n’arriver sur place qu’à partir du 1er septembre où les prix redeviennent « normaux ».</p>
<p>Voici une estimation approximative du niveau des loyers.</p>
<p><strong>Tel Aviv</strong></p>
<table class="spip">
<thead><tr class="row_first"><th id="id846f_c0"> </th><th id="id846f_c1"> </th><th id="id846f_c2"> Centre ville </th><th id="id846f_c3"> Ramat Aviv - Jaffa </th><th id="id846f_c4"> Neve Tzedek - Front de mer</th></tr></thead>
<thead><tr class="row_first"><th id="id846f_c0"> Meublés</th><th id="id846f_c1"> </th><th id="id846f_c2"> </th><th id="id846f_c3"> </th><th id="id846f_c4"></th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="id846f_c0">2 chambres</td>
<td headers="id846f_c1">80-120 m²</td>
<td headers="id846f_c2">2000 à 2400 USD</td>
<td headers="id846f_c3">2200 à 2600 USD</td>
<td headers="id846f_c4">à partir de 3200 USD</td></tr>
<tr class="row_even even">
<td headers="id846f_c0">3 chambres</td>
<td headers="id846f_c1">120-160 m²</td>
<td headers="id846f_c2">2500 à 3500 USD</td>
<td headers="id846f_c3">2700 à 3700 USD</td>
<td headers="id846f_c4">à partir de 4500 USD</td></tr>
<tr class="row_odd odd">
<td headers="id846f_c0"><strong>Non meublés</strong></td>
<td headers="id846f_c1"></td>
<td headers="id846f_c2"></td>
<td headers="id846f_c3"></td>
<td headers="id846f_c4"></td></tr>
<tr class="row_even even">
<td headers="id846f_c0">1 chambre</td>
<td headers="id846f_c1">50-70 m²</td>
<td headers="id846f_c2">700 à 1000 USD</td>
<td headers="id846f_c3">1000 à 1400 USD</td>
<td headers="id846f_c4">1300 à 1700 USD</td></tr>
<tr class="row_odd odd">
<td headers="id846f_c0">2 chambres</td>
<td headers="id846f_c1">80-120 m²</td>
<td headers="id846f_c2">1200 à 1600 USD</td>
<td headers="id846f_c3">1600 à 2500 USD</td>
<td headers="id846f_c4">2200 à 2800 USD</td></tr>
<tr class="row_even even">
<td headers="id846f_c0">3 chambres</td>
<td headers="id846f_c1">120-160 m²</td>
<td headers="id846f_c2">1 800 à 2 600 USD</td>
<td headers="id846f_c3">2 800 à 3 200 USD</td>
<td headers="id846f_c4">3 000 à 4 200 USD</td></tr>
<tr class="row_odd odd">
<td headers="id846f_c0">4 chambres</td>
<td headers="id846f_c1">160-200 m²</td>
<td headers="id846f_c2">2 800 à 3 200 USD</td>
<td headers="id846f_c3">3 400 à 4 500 USD</td>
<td headers="id846f_c4">à partir de 4 500 USD</td></tr>
</tbody>
</table>
<p><strong>Haïfa</strong></p>
<p>A Haïfa, les quartiers résidentiels se situent principalement sur le Mont Carmel (Denya, Vardiya, Carmel Français…)</p>
<p>Ce tableau présente le coût locatif moyen par type d’habitation et de quartier en euros :</p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Location/Prix du m²</strong></td>
<td>3 pièces</td>
<td>5 pièces</td>
<td>Villa</td></tr>
<tr class="row_even even">
<td>Centre et périphéries</td>
<td>800 €</td>
<td>1300 €</td>
<td>2000 €</td></tr>
<tr class="row_odd odd">
<td>Quartiers résidentiels</td>
<td>1500 €</td>
<td>2500 €</td>
<td>2500 €</td></tr>
<tr class="row_even even">
<td>Banlieue</td>
<td>900 €</td>
<td>1400 €</td>
<td>2200 €</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_2"></a>Conditions de location</h3>
<p>Les baux sont généralement de 1 à 3 ans avec option d’une année supplémentaire. Les loyers sont le plus souvent libellés en US dollars, ou en Euros, et payables en shekels, le plus souvent trimestriellement mais de plus en plus de 6 mois à 1 an d’avance.</p>
<p>Pour les logements meublés, la caution est obligatoire et peut représenter deux à trois mois de loyers. Pour les logements vides la caution est moins systématique, surtout si on paye plusieurs mois d’avance, auquel cas il est souvent possible de ne pas payer de caution.</p>
<p>Le recours à un agent immobilier est recommandé en raison du petit nombre de transactions de particulier à particulier, sa rémunération s’élève à un mois de loyer.</p>
<p>Les charges annexes sont l’électricité, l’eau, le gaz, l’arnona (sorte de taxe d’habitation et de voirie). En immeuble de copropriété, selon le standing, les charges communes peuvent être très élevées en fonction des équipements (piscine, salles de sports, ascenseurs) et atteindre jusqu’à 500 USD par mois. Les climatiseurs nouvelle génération ont deux positions (été/hiver), ils s’avèrent très utiles tout au long de l’année (pour chauffer en hiver, rafraîchir en été ainsi que réduire l’humidité). Leur coût de fonctionnement vient donc augmenter la facture d’électricité.</p>
<p>De manière générale, sauf pour les maisons et les appartements de grand standing, les pièces sont beaucoup plus exiguës qu’en France. Les mètres carrés sont comptabilisés en prenant en compte la totalité de l’aire extérieure du logement, cloisons et espaces compris. Si les terrasses sont décentes, les balcons sont souvent petits.</p>
<h3 class="spip"><a id="sommaire_3"></a>Hôtels</h3>
<p>Prix moyens d’une chambre d’hôtel (chambre double) :</p>
<ul class="spip">
<li>Grand tourisme : 150 €</li>
<li>Moyen tourisme : 80 €</li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Electricité </h3>
<p>Le courant électrique est livré sous une tension de 220 volts. Les prises de courant sont à trois plots avec terre. Les prises françaises sont souvent compatibles, dans le cas contraire, des adaptateurs sont disponibles sur place.</p>
<h3 class="spip"><a id="sommaire_5"></a>Electroménager </h3>
<p>Les cuisines sont la plupart du temps équipées (plaques de cuisson et four, machine à laver le linge, machine à laver la vaisselle, réfrigérateur et congélateur) mais les appareils sont souvent usagés. Tous les types d’appareils électroménagers, généralement de fabrication allemande ou américaine, sont disponibles sur place.</p>
<p>Concernant les appareils vidéo, le standard utilisé est le PAL.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-israel-vie-pratique-article-loisirs-et-culture.md" title="Loisirs et culture">Loisirs et culture</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-israel-vie-pratique-article-communications.md" title="Communications">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-israel-vie-pratique-article-transports-110473.md" title="Transports">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-israel-vie-pratique-article-cout-de-la-vie.md" title="Coût de la vie">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-israel-vie-pratique-article-scolarisation.md" title="Scolarisation">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-israel-vie-pratique-article-sante.md" title="Santé">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-israel-vie-pratique-article-logement-110469.md" title="Logement">Logement</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/vie-pratique/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
