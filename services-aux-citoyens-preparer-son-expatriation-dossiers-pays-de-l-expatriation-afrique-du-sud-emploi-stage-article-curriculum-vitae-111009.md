# Curriculum vitae

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/emploi-stage/article/curriculum-vitae-111009#sommaire_1">Rédaction</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Rédaction</h3>
<p>Les CV sud-africains ne comportent pas de photo du candidat.</p>
<p>Il est important d’indiquer des contacts de référence (postes précédents, maîtres de stage etc).</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/emploi-stage/article/curriculum-vitae-111009). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
