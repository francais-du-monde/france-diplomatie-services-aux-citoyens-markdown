# Entrée et séjour

<h2 class="rub23547">Passeport, visa, permis de travail</h2>
<p>Quel que soit le motif de votre séjour, renseignez-vous auprès de la section consulaire de l’ambassade de Thaïlande à Paris qui vous informera sur la règlementation en matière d’entrée et de séjour en Thaïlande, règlementation que vous devrez impérativement respecter. N’hésitez pas à consulter le site du Ministère de l’Intérieur Thaïlandaises afin de vous familiariser avec les modalités d’entrée et de séjour dans le pays.</p>
<p>Une fois sur place c’est le Ministère de l’Emploi et de la Sécurité sociale qui prend le relais. <strong>En aucun cas, vous n’êtes autorisé à travailler en Thaïlande sans permis adéquat. </strong></p>
<p>Le Consulat de France en Thaïlande n’est pas l’organisme compétent pour répondre à vos demandes d’information concernant le droit de séjour en Thaïlande.</p>
<h4 class="spip">Les différents types de visa</h4>
<p><strong>Certificat de résident</strong></p>
<p>Une personne qui désire résider en Thaïlande est tenue de demander une carte de résident délivrée par le bureau de l’immigration. Il existe un quota limitatif et des conditions très strictes à l’octroi du <i>Certificate of Residence</i>.</p>
<p><strong>Permis de travail</strong></p>
<p>Pour pouvoir travailler en Thaïlande, il faut être détenteur d’un Visa <i>Non-immigrant </i>B et obtenir un permis de travail (<i>Work Permit</i>). Certaines professions, énumérées par un décret royal, sont interdites aux étrangers.</p>
<p>Les critères d’appréciation pour l’octroi d’un permis de travail sont :</p>
<ul class="spip">
<li>la preuve que le travail effectué par un étranger ne pourrait l’être par un Thaïlandais ;</li>
<li>quatre travailleurs thaïlandais sont employés pour un étranger ou 35 ouvriers pour un technicien étranger (ce critère peut être modifié à tout moment) ;</li>
<li>le capital social est supérieur ou égal à deux millions de bahts par permis de travail.</li>
<li>Le salaire mensuel est d’au moins 50 000 bahts pour un Français</li></ul>
<p>Attention, pour certaines catégories d’employés la procédure prévoit l’intervention d’autres administrations thaïlandaises ou d’ordres professionnels. C’est notamment le cas des enseignants. Il convient de se renseigner auprès de l’employeur et de vérifier que celui-ci a suivi les procédures administratives requises.</p>
<p>La demande de permis peut être faite par la personne même ou par l’entreprise qui veut l’employer. Un Centre de services unique (One Stop Service Center) gère l’ensemble des attributions de visas et de permis de travail dans un délai de 3 heures.</p>
<p><strong>Pour le permis de travail thaïlandais, s’adresser au :</strong></p>
<p>Ministère de l’Emploi et de la Sécurité sociale<br class="manualbr">Tél. : (02) 248 5558 ou (02) 223 4912 à 14<br class="manualbr">ou encore : Service du permis de travail<br class="manualbr">Tél. : (02) 617 6583 à 85</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.thaiembassy.fr/fr/" class="spip_out" rel="external">Section consulaire de l’ambassade de Thaïlande à Paris</a></p>
<p><i>Mise à jour : juillet 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-thailande-entree-et-sejour-article-animaux-domestiques-114427.md" title="Animaux domestiques">Animaux domestiques</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-thailande-entree-et-sejour-article-vaccination-114426.md" title="Vaccination">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-thailande-entree-et-sejour-article-demenagement-114425.md" title="Déménagement">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-thailande-entree-et-sejour-article-passeport-visa-permis-de-travail.md" title="Passeport, visa, permis de travail">Passeport, visa, permis de travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/entree-et-sejour/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
