# Stages

<p>Il est possible d’effectuer un stage en Thaïlande. L’étudiant doit, avant de partir, obtenir un visa « ED » (voir <a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-thailande-entree-et-sejour-article-passeport-visa-permis-de-travail.md" class="spip_in">Passeport, visa, permis de travail</a>) et avoir une convention de stage. L’entreprise d’accueil doit selon la loi thaïe demander un permis de travail pour le stagiaire ; les mêmes conditions s’appliquent que pour un employé étranger même s’il n’y a aucune rémunération (quotas, capital libéré). Cela réduit considérablement l’offre de stage. Attention, de nombreuses entreprises ignorent ces formalités.</p>
<p>Il est possible de consulter des offres de stage en ligne sur le site de la <a href="http://www.francothaicc.com" class="spip_out" rel="external">Chambre de commerce franco-thaïe</a></p>
<h4 class="spip">Avez-vous pensé au volontariat international (VIE ou VIA) ?</h4>
<p>Le VIE permet aux entreprises françaises de confier à un jeune ressortissant de l’espace économique européen, âgé de 18 à 28 ans, une mission professionnelle (rémunérée) à l’étranger durant une période modulable de 6 à 24 mois.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/emplois-stages-concours/volontaires-internationaux/" class="spip_in">Volontaires internationaux</a></li>
<li><a href="http://www.civiweb.com/FR/index.aspx" class="spip_out" rel="external">Civiweb.com</a></li>
<li><a href="http://export.businessfrance.fr/default.html" class="spip_out" rel="external">Business France.fr</a> - Courriel : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/emploi-stage/article/stages#infoVIE#mc#ubifrance.fr#" title="infoVIE..åt..ubifrance.fr" onclick="location.href=mc_lancerlien('infoVIE','ubifrance.fr'); return false;" class="spip_mail">infoVIE<span class="spancrypt"> [at] </span>ubifrance.fr</a></li></ul>
<p><i>Mise à jour : juillet 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/emploi-stage/article/stages). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
