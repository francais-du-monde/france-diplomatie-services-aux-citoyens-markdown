# Curriculum vitae

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/emploi-stage-23527/article/curriculum-vitae-114252#sommaire_1">Rédaction</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/emploi-stage-23527/article/curriculum-vitae-114252#sommaire_2">Diplômes (équivalence, légalisation)</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Rédaction</h3>
<p>Le CV pour postuler à un emploi en Haïti reprend les grands principes du CV en France : soyez précis, en évitant les formulations vagues et en personnalisant votre CV en fonction de chaque employeur. Soyez concis et efficace dans la rédaction des principales rubriques de votre CV (formation, expérience professionnelle, connaissances linguistiques et informatiques, activités extra-professionnelles).</p>
<p>Limitez-vous à une page (deux maximum).</p>
<h3 class="spip"><a id="sommaire_2"></a>Diplômes (équivalence, légalisation)</h3>
<p>Il n’y a pas d’équivalence de diplômes légalement attestée, mais les diplômes français peuvent être reconnus par les recruteurs.</p>
<p><i>Mise à jour : août 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/haiti/emploi-stage-23527/article/curriculum-vitae-114252). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
