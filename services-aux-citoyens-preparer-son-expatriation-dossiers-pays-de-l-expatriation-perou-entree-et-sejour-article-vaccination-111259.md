# Vaccination

<p>Aucune vaccination n’est exigée à l’entrée du pays pour un voyageur en provenance d’Europe. Cependant, pour les expatriés en provenance d’une zone infectée ou désirant se rendre en Amazonie, il est nécessaire de se faire vacciner contre la fièvre jaune.</p>
<p>Il est également conseillé, pour des raisons médicales :</p>
<p><strong>Pour les adultes :</strong></p>
<ul class="spip">
<li>mise à jour des vaccinations contre la diphtérie, le tétanos et la poliomyélite ;</li>
<li>vaccinations contre la typhoïde (longs séjours), l’hépatite A (à partir de 50 ans, une recherche préalable d’anticorps sériques totaux est justifiée) et l’hépatite B (longs séjours et/ou séjours à risques).</li></ul>
<p><strong>Pour les enfants :</strong><br class="manualbr">Toutes les vaccinations recommandées en France par le ministère de la Santé ; et en particulier :</p>
<ul class="spip">
<li>dès le premier mois : le B.C.G. et l’hépatite B (longs séjours) ;</li>
<li>dès l’âge de neuf mois, la rougeole ;</li>
<li>dès l’âge de six mois : la fièvre jaune en cas de séjour en zone rurale (région amazonienne).</li></ul>
<p>Il est préférable de réaliser toutes les vaccinations nécessaires avant de partir, car une fois sur place on peut rencontrer des difficultés d’approvisionnement.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  consultez la rubrique thématique <a href="services-aux-citoyens-preparer-son-expatriation-sante.md" class="spip_in">Santé</a>.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/entree-et-sejour/article/vaccination-111259). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
