# Tunisie

<p>Au 31 décembre 2014, le nombre <strong> <i>d’inscrits au Registre des Français établis hors de France</i> </strong> s’élève à 23 279. Ce chiffre est en augmentation constante depuis 2007 et a doublé depuis 1985.</p>
<table class="spip">
<thead><tr class="row_first"><th id="id49af_c0">Inscrits</th><th id="id49af_c1">Femmes</th><th id="id49af_c2">Hommes</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td class="numeric " headers="id49af_c0">23 042</td>
<td headers="id49af_c1">52%</td>
<td headers="id49af_c2">48 %</td></tr>
</tbody>
</table>
<p>Les nouveaux inscrits sont en majorité des double-nationaux (couple mixtes et enfants nés de ces unions) qui reviennent s’installer en Tunisie pour convenances personnelles ou des conjoints ayant acquis la nationalité française. La Tunisie accueille également un nombre croissant de retraités français.</p>
<p>1,5 million de Français de seule nationalité française ont visité la Tunisie en 2010 (dont 50% durant la période d’été et pour la seule destination de Djerba). Ce chiffre, qui était tombé à 785 000 durant l’année 2011, est remonté légèrement à 901 000 en 2012, soit 18 % du nombre total (5 millions) de touristes cette année.</p>
<p>Pour les cinq premiers mois de l’année 2013, il s’élève à 233 000 Français contre 289 000 pour la même période en 2012.</p>
<p>Du fait de ses tarifs avantageux, la Tunisie attire plus spécialement les touristes appartenant aux classes modestes ou moyennes.</p>
<p>A ce chiffre, il convient d’ajouter les franco-tunisiens résidant en France qui regagnent leur pays d’origine durant l’été et voyagent avec un passeport tunisien.</p>
<p>A noter que la suppression de l’obligation de résidence les amène à solliciter auprès de ce Consulat Général le renouvellement de leurs documents d’identité et titres de voyage, plutôt que de se rendre à la Préfecture de leur lieu de domicile en France ce qui entraîne une très forte fréquentation de nos services durant l’été.</p>
<p>La répartition de la communauté française en Tunisie sur la pyramide des âges, en 2012, est globalement homogène à travers le pays, et donne la classification suivante :</p>
<table class="spip">
<thead><tr class="row_first"><th id="idc688_c0">Classe d’âge </th><th id="idc688_c1">0 à 18 ans </th><th id="idc688_c2">18 à 60 ans </th><th id="idc688_c3">60 ans et plus </th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="idc688_c0">Pourcentage</td>
<td headers="idc688_c1">31,7 %</td>
<td headers="idc688_c2">49,4 %</td>
<td headers="idc688_c3">18,9 %</td></tr>
</tbody>
</table>
<p>70 % des inscrits sont établis dans le gouvernorat de Tunis (Tunis et localités à l’est de la capitale). 22 % sur la bande côtière et 4 % dans la région de Bizerte. Les 4% restants sont disséminés dans les gouvernorats du nord-ouest, du centre et du sud du pays.</p>
<p>Dans la tranche d’âge de 24 à 65 ans, qui représentent plus de 52 % des Français inscrits au registre, moins de 58 % d’entre eux exercent une activité professionnelle dont :</p>
<ul class="spip">
<li>45 % dans l’encadrement et les professions intellectuelles (enseignants, médecins)</li>
<li>12 % dans les professions intermédiaires (avocats, juristes, chefs de services)</li>
<li>19 % artisans et commerçants</li>
<li>19 % employés et ouvriers divers</li>
<li>2 % dans l’agriculture</li></ul>
<h3 class="spip">Entreprises françaises</h3>
<p>La présence française est particulièrement significative. Hors secteur énergétique, la France est le premier pays investisseur, tant par le nombre des entreprises créées que par le montant des investissements réalisés. La France est impliquée dans la création de 1270 entreprises générant près de 115 000 emplois.</p>
<p>De grands groupes sont présents, notamment dans le secteur bancaire (Société générale, BNP Paribas, BPCE), dans les assurances (Groupama), dans la Grande Distribution (Carrefour, Casino, Auchan depuis 2012), dans l’industrie (Air Liquide, Valéo, Sagem, EADS./Aérolia, BIC depuis 2012…), dans la distribution des produits pétroliers (Total), dans le tourisme (Fram, Club Med, Accor).</p>
<h3 class="spip">Consuls honoraires</h3>
<p>Les compétences suivantes ont été conférées, par arrêté, aux <a href="http://www.consulfrance-tunis.org/Agences-consulaires,489" class="spip_out" rel="external">cinq consuls honoraires</a> :</p>
<ul class="spip">
<li>délivrance des certificats de vie, de résidence et divers,</li>
<li>certification de conformité à l’original de copies de documents,</li>
<li>accomplissement des formalités en cas de disparition ou de décès d’un ressortissant français.</li></ul>
<p>De nationalité française, ils sont tous habilités à recueillir les procurations de vote.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/dossiers-pays/tunisie/" class="spip_in">Une description de la Tunisie, de sa situation politique et économique</a> ;</li>
<li><a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/tunisie/" class="spip_in">Des informations actualisées sur les<strong> conditions locales de sécurité</strong> en Tunisie</a> ;</li>
<li><a href="http://www.ambassadefrance-tn.org/" class="spip_out" rel="external">Ambassade de France en Tunisie</a>.</li></ul>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-tunisie-entree-et-sejour.md">Entrée et séjour</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-tunisie-entree-et-sejour-article-passeport-visa-permis-de-travail.md">Passeport, visa, permis de travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-tunisie-entree-et-sejour-article-demenagement-110513.md">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-tunisie-entree-et-sejour-article-vaccination-110514.md">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-tunisie-entree-et-sejour-article-animaux-domestiques.md">Animaux domestiques</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-tunisie-emploi-stage.md">Emploi, stage</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-tunisie-emploi-stage-article-marche-du-travail.md">Marché du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-tunisie-emploi-stage-article-reglementation-du-travail.md">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-tunisie-emploi-stage-article-recherche-d-emploi.md">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-tunisie-emploi-stage-article-curriculum-vitae.md">Curriculum vitae</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-tunisie-emploi-stage-article-lettre-de-motivation.md">Lettre de motivation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-tunisie-emploi-stage-article-entretien-d-embauche.md">Entretien d’embauche</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-tunisie-emploi-stage-article-stages.md">Stages</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-tunisie-protection-sociale.md">Protection sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-tunisie-protection-sociale-article-regime-local-de-securite-sociale.md">Régime local de sécurité sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-tunisie-protection-sociale-article-convention-de-securite-sociale-110524.md">Convention de sécurité sociale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-tunisie-fiscalite.md">Fiscalité</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-tunisie-fiscalite-article-fiscalite-du-pays-110526.md">Fiscalité du pays</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-tunisie-fiscalite-article-convention-fiscale.md">Convention fiscale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-tunisie-vie-pratique.md">Vie pratique</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-tunisie-vie-pratique-article-logement.md">Logement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-tunisie-vie-pratique-article-sante.md">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-tunisie-vie-pratique-article-scolarisation.md">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-tunisie-vie-pratique-article-cout-de-la-vie-110531.md">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-tunisie-vie-pratique-article-transports-110532.md">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-tunisie-vie-pratique-article-communications.md">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-tunisie-vie-pratique-article-loisirs-et-culture.md">Loisirs et culture</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-tunisie-presence-francaise.md">Présence française</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-tunisie-presence-francaise-article-presence-francaise.md">Présence française</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
