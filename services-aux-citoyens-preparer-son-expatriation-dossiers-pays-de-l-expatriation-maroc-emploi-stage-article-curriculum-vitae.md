# Curriculum vitae

<h4 class="spip">Rédaction</h4>
<p>Le curriculum vitae doit mentionner les diplômes et détailler les activités professionnelles antérieures, non seulement en énumérant les employeurs successifs mais aussi en décrivant les fonctions précises et les niveaux d’encadrement exercés.</p>
<p>Le CV doit être précis et concis. Il est généralement composé des parties suivantes :</p>
<ul class="spip">
<li>Une <strong>présentation (état civil)</strong> située tout en haut du CV et comprenant le nom, le prénom, les coordonnées et la date de naissance ;</li>
<li>Un <strong>titre</strong> qui permet de visualiser la fonction du candidat ou encore celle à laquelle il aspire ;</li>
<li>L’<strong>expérience professionnelle</strong> décrivant les compétences, les postes occupés, les durées et les noms des entreprises ;</li>
<li>La <strong>formation</strong> initiale et/ou professionnelle avec les diplômes obtenus, les dates, les écoles et la localisation ;</li>
<li>Les <strong>langues</strong> parlées et écrites avec les séjours significatifs à l’étranger, la destination et la durée.</li>
<li>Les <strong>connaissances informatiques</strong> présentant les logiciels maîtrisés</li>
<li>Les <strong>activités extra-professionnelles</strong> comprenant les activités pratiquées par le candidat à l’extérieur du cadre professionnel.</li></ul>
<p>Le CV marocain doit tenir sur deux pages maximum. L’ordre chronologique inversé est recommandé (du plus récent au plus ancien). Les éléments relatifs à la photo, situation familiale et possession d’un permis de conduire sont facultatifs. La rubrique « formation » peut être placée avant ou après celle faisant mention de l’expérience professionnelle.</p>
<h4 class="spip">Diplômes</h4>
<p>Indiquer les diplômes obtenus en mentionnant l’intitulé, la date d’obtention, l’école et le lieu.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/emploi-stage/article/curriculum-vitae). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
