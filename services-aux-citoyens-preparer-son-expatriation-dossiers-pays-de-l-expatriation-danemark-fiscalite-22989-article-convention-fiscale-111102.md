# Convention fiscale

<p><strong>Il n’existe plus de convention fiscale entre la France et le Danemark.</strong></p>
<p>En effet, la convention entre la France et le Danemark tendant à éviter les doubles impositions et à établir des règles d’assistance administrative réciproque en matière d’impôts sur les revenus et la fortune, signée à Paris le 8 février 1957, a été dénoncée et <strong>ne s’applique plus entre les deux pays depuis le 1er janvier 2009.</strong></p>
<p>Le décret n°2009-46 portant publication de cette dénonciation a été publié au Journal officiel du 15 janvier 2009 et peut être consulté sur le site Internet <a href="http://www.legifrance.gouv.fr/" class="spip_out" rel="external">Legifrance.gouv.fr</a>.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/fiscalite-22989/article/convention-fiscale-111102). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
