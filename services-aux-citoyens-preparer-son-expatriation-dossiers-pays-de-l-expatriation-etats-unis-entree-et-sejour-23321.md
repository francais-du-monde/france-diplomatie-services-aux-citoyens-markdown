# Entrée et séjour

<h2 class="rub23321">Passeport, visa, permis de travail</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/entree-et-sejour-23321/#sommaire_1">Les différents types de visa</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/entree-et-sejour-23321/#sommaire_2">Résidence permanente </a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/entree-et-sejour-23321/#sommaire_3">Carte de sécurité sociale</a></li></ul>
<p>Quel que soit le motif de votre séjour, renseignez-vous auprès de la section consulaire de l’ambassade des Etats-Unis à Paris qui vous informera sur la règlementation en matière d’entrée et de séjour aux Etats-Unis, règlementation que vous devrez impérativement respecter.</p>
<p>Une fois sur place c’est le <a href="http://www.uscis.gov/" class="spip_out" rel="external">service de la citoyenneté et de l’immigration américain</a> qui prend le relais. <strong>En aucun cas, vous n’êtes autorisé à travailler aux Etats-Unis sans permis adéquat.</strong></p>
<p>Le consulat de France aux Etats-Unis n’est pas l’organisme compétent pour répondre à vos demandes d’information concernant votre séjour aux Etats-Unis.</p>
<p>Les informations suivantes sont données à titre indicatif et sont susceptibles d’être modifiées. Pour tout renseignement complémentaire, contactez l’<a href="http://france.usembassy.gov/" class="spip_out" rel="external">ambassade des Etats-Unis en France</a>.</p>
<h3 class="spip"><a id="sommaire_1"></a>Les différents types de visa</h3>
<p>La France fait partie du <a href="http://french.france.usembassy.gov/visas/vwp.html" class="spip_out" rel="external">programme d’exemption de visas</a>. Si votre séjour est inférieur à 90 jours et remplit certaines conditions, il est possible que vous n’ayez pas besoin de visa pour votre voyage aux Etats-Unis mais simplement de l’autorisation électronique de voyage « ESTA ».</p>
<h4 class="spip">Visa étudiant</h4>
<p><strong>Le visa de type F</strong></p>
<ul class="spip">
<li>réservé aux personnes souhaitant poursuivre des études supérieures et/ou participer à des programmes linguistiques aux Etats-Unis.</li>
<li>vous devrez justifier des ressources nécessaires pour le paiement des frais d’inscription et de séjour pour les 12 premiers mois, de vos attaches en France et de votre intention de quitter les Etats-Unis à l’issue de votre séjour.</li>
<li>les étudiants à temps complet peuvent travailler sur le campus 20 heures au plus par semaine.</li></ul>
<p>Dans tous les cas, le niveau baccalauréat est requis au minimum et, dans le cas d’un programme d’études, il faut qu’il existe une véritable adéquation entre la formation initiale du candidat et la formation envisagée aux Etats-Unis. Vous devrez étudier à temps complet et maîtriser la langue anglaise.</p>
<p><strong>Le visa de type M</strong></p>
<ul class="spip">
<li>réservé à la poursuite d’études extrascolaires, techniques et professionnelles.</li>
<li>il ne vous autorise pas à exercer une activité professionnelle.</li>
<li>vous devrez justifier de ressources suffisantes pour la durée totale de votre séjour</li></ul>
<p>Dans les deux cas vous devrez être inscrit dans un établissement d’enseignement agréé aux Etats-Unis ou à un programme d’échange. Celui-ci vous délivrera alors le formulaire I-20 qui doit obligatoirement être enregistré dans le système SEVIS (<i>Student and Exchange Visitor Information System</i>). L’enregistrement est payant (200 dollars US). Vous devrez justifier du paiement des frais d’enregistrement SEVIS au moins trois jours avant la date de l’entretien.</p>
<h4 class="spip">Visa dans le cadre d’un programme d’échange, un stage ou un séjour au pair</h4>
<p><strong>Le visa de type J</strong></p>
<p>réservé aux programmes d’échange, aux activités de recherche et d’assistanat, aux études en médecine, aux jobs d’été, aux stages et aux séjours au pair.</p>
<p>Programmes d’échanges :</p>
<p>§ Vous devrez justifier de ressources financières suffisantes pour vos frais de séjour, de vos attaches en France et de votre intention de quitter les Etats-Unis à l’issue de votre séjour.</p>
<p>§ Les personnes effectuant une formation devront attester de leurs diplômes ou qualifications et joindre un descriptif du programme. Comme pour les visas F et M, l’inscription préalable dans un établissement d’enseignement aux Etats-Unis ou à un programme d’échange est obligatoire, ainsi que l’enregistrement SEVIS (voir ci-dessus), dont le coût s’élève à 180 dollars US.</p>
<p>Stages :</p>
<p>Un stage aux Etats-Unis n’est pas autorisé dans le cadre du programme d’exemption de visa. Même si vous partez moins de 90 jours, <strong>vous devez obtenir un visa J-1</strong>, que vous soyez rémunéré ou non.</p>
<p>Il existe deux profils de stagiaires :</p>
<ul class="spip">
<li>Les <i>trainee</i> sont des stagiaires en apprentissage. Ils ont soit un diplôme de l’enseignement supérieur plus au moins un an d’expérience professionnelle, soit au moins cinq ans d’expérience professionnelle. La durée maximum autorisée pour un stage est de 18 mois.</li>
<li>Les <i>interns</i> sont des stagiaires en entreprise. Ils sont encore inscrits dans un établissement d’enseignement supérieur dans leur pays d’origine, soit titulaire d’un diplôme de l’enseignement supérieur depuis moins de 12 mois. La durée maximum autorisée pour un stage est de 12 mois.</li></ul>
<p>Le candidat-stagiaire doit avoir un niveau d’anglais qui lui permet de communiquer aisément à l’intérieur d’une entreprise. Une reconnaissance du niveau peut être demandée par le biais d’un test d’anglais reconnu, d’une certification universitaire ou d’une école d’enseignement de l’anglais ou par un entretien vidéo ou téléphonique.</p>
<p>Le candidat-stagiaire a la possibilité de chercher son futur employeur par ses propres moyens mais <strong>ne peut obtenir de visa J-1 sans passer par un organisme agréé</strong> souvent appelé « organisme sponsor ». Cet organisme doit avoir reçu l’agrément du Département d’Etat américain. L’organisme sponsor a l’autorisation de vous fournir un certificat d’éligibilité appelé <strong>certificat DS-2019</strong>, indispensable pour faire une demande de visa J-1.</p>
<p>le candidat-stagiaire doit en plus du DS-2019 présenter au service consulaire de l’ambassade des Etats-Unis le <strong>formulaire DS-7002</strong> « Training / Internship Placement Plan »</p>
<p>Séjours Au pair :</p>
<ul class="spip">
<li>Vous devez effectuer les démarches dans le cadre d’un programme d’échange agrée par les services culturels Américains. La liste des organismes français agrées est disponible sur le <a href="http://french.france.usembassy.gov/niv/aupair.html" class="spip_out" rel="external">site de l’ambassade des Etats-Unis en France</a>.</li>
<li>L’enregistrement SEVIS est obligatoire et payant (35 dollars US).</li>
<li>Vous devez être âgé de 18 à 26 ans.</li>
<li>Photocopie de votre baccalauréat ou de votre certificat de fin d’études secondaires devra être jointe à votre dossier.</li></ul>
<h4 class="spip">Visas de travail</h4>
<p>Il existe plusieurs catégories de visas de travail (types H, L et Q) en fonction notamment du statut professionnel du demandeur, de son domaine d’activité ou de certaines qualifications particulières. La durée de séjour autorisée et la possibilité de prolonger son séjour est fonction du type de visa. Le tableau des différentes catégories de visa de travail est disponible sur le site de l’Ambassade des Etats-Unis en France.</p>
<p><strong>Pour tous les visas de travail, une offre d’emploi est nécessaire.</strong> L’employeur aux Etats-Unis doit déposer une requête auprès des Services d’Immigration Américains (<i>Department of Homeland Security</i> – DHS). Si la demande est approuvée, l’employeur reçoit une autorisation de travail (<i>notice of approval</i>), obligatoire pour pouvoir déposer une demande de visa de travail, mais qui ne garantit pas la délivrance du visa. Le permis de travail doit également être confirmé par la voie électronique pour que les services consulaires puissent délivrer le visa. Les délais pour obtenir cette confirmation vont de deux à cinq jours, mais peuvent être plus longs lorsqu’il s’agit d’une prorogation de permis de travail.</p>
<p><strong>Le visa de type H1B</strong></p>
<ul class="spip">
<li>C’est le plus courant des visas de travail</li>
<li>Il est destiné aux emplois spécialisés demandant un diplôme de l’enseignement supérieur ou une « activité spéciale » (artistes, gens du spectacle, sportifs et mannequins renommés…) : le demandeur doit justifier de l’obtention d’un diplôme universitaire et que sa formation est en adéquation avec l’emploi envisagé aux Etats-Unis.</li>
<li>La durée du visa est de trois ans maximum. Le visa peut être renouvelé mais ne peut dépasser une période totale de six ans.</li>
<li><strong>le nombre de visas H1B est limité 65 000 visas</strong> par année fiscale (du 1er octobre au 30 septembre) et pour l’ensemble du monde. Cependant, les premières 20 000 demandes effectuées par les titulaires d’un diplôme américain de niveau master ou plus seront exemptés de ce quota. De plus, les demandeurs qui seront employés par une institution de l’enseignement supérieur, une entité à but non lucratif ou une organisation de recherche gouvernementale seront également exemptés du quota.</li></ul>
<p><strong>Le visa de type L(1)</strong></p>
<p>§ Il est destiné aux directeurs ou cadres supérieurs de société multinationales qui sont mutés dans une filiale ou au siège de l’entreprise. Ils doivent justifier d’au moins un an d’expérience au sein de l’entreprise.</p>
<h3 class="spip"><a id="sommaire_2"></a>Résidence permanente </h3>
<h4 class="spip">Le visa d’immigrant</h4>
<p>Ce type de visa (<i>immigrant visa</i>) est destiné aux personnes souhaitant s’établir de façon permanente aux Etats-Unis, que ce soit ou non pour y chercher un emploi. La possession d’un visa d’immigrant est nécessaire pour l’obtention, une fois aux Etats-Unis, de la carte de séjour de résident permanent (<i>permanent resident card</i> ou <i>green card</i>).</p>
<p>Dans la plupart des cas, vous ne pouvez demander vous-même un visa d’immigrant. La demande doit être déposée en votre faveur par une tierce personne (<i>sponsor</i>) de nationalité américaine ou ayant le statut de résident permanent aux Etats-Unis. Mais ces dernières ne peuvent sponsoriser que leur conjoint ou leurs enfants, quel que soit leur âge. A noter que les visas d’immigrant pour raisons familiales et professionnelles sont divisés en plusieurs catégories préférentielles.</p>
<p>Il existe quatre catégories de visas d’immigrant :</p>
<ul class="spip">
<li>Visa « parents proches » (<i>immediate relatives visa</i>) : on entend par parent proche le conjoint, les enfants célibataires âgés de moins de 21 ans et les parents d’un citoyen américain âgé de 21 ans ou plus ;</li>
<li>visa « raisons familiales » (<i>family-based visa</i>) : il concerne les frères et sœurs ou les enfants mariés d’un ressortissant américain ou encore le conjoint d’un résident permanent aux Etats-Unis. A noter qu’il existe un quota pour ce type de visa ;</li>
<li>visa « raisons professionnelles » (<i>employment-based visa</i>) : ce type de visa est divisé en cinq catégories préférentielles (<i>preference category</i>). La demande doit être déposée par l’employeur aux Etats-Unis. Le nombre de visas est limité à 140 000 par an ;</li>
<li>visa « fiancé(e) »<i>(fiancé(e)-based)</i> : permet au/à la fiancé(e) étranger(e) d’un citoyen américain d’entrer aux Etats-Unis, de se marier dans les 90 jours aux Etats-Unis puis de faire une demande de green card aux Etats-Unis.</li>
<li>Loterie (<i>Diversity Immigrant Visa Program</i> ou <i>green card lottery</i>). Dans la plupart des cas, la demande de visa d’immigrant ne peut pas être déposée par le sponsor auprès de l’Ambassade des Etats-Unis à Paris. Les visas de type <i>immediate relative</i>, employment-based et fiancé(e)-based doivent être déposés auprès des <a href="http://www.uscis.gov/" class="spip_out" rel="external">services américains de la Citoyenneté et de l’Immigration (<i>US Citizenship and Immigration Services</i> - USCIS)</a> aux Etats-Unis. La plupart du temps, le sponsor doit être établi aux Etats-Unis.</li></ul>
<p>Une fois la demande approuvée par l’USCIS, une notification de l’accord (<i>notice of approval</i>) est adressée au sponsor, ainsi qu’au Centre National des Visas (<i>National Visa Center</i> - NVC). Ce dernier prend ensuite contact avec le futur émigrant et transmet la demande de visa à l’Ambassade des Etats-Unis en France. Comme pour les autres types de visa, un entretien est nécessaire et des documents sont exigés. Vous devrez également subir un examen médical.</p>
<p>Vous trouverez des informations détaillées sur les visas d’immigrant sur le site Internet (<strong>version anglaise</strong>) de l’<a href="http://france.usembassy.gov/" class="spip_out" rel="external">Ambassade des Etats-Unis en France</a>.</p>
<p><strong>Carte de résident permanent</strong></p>
<p>Une fois le visa obtenu, vous disposez de six mois pour entrer sur le territoire américain et déposer votre demande de carte de séjour pour étranger (<i>alien registration card</i> ou <i>green card</i>). Celle-ci vous permettra de travailler et de résider aux Etats-Unis. A noter que le ministère de la Sécurité intérieure (<i>Department of Homeland Security</i>) a besoin de plusieurs mois pour vous établir votre carte de séjour. En attendant de recevoir ce document, le tampon apposé sur votre passeport lors de votre entrée sur le territoire américain et indiquant votre numéro d’étranger (<i>alien number</i>) vous permet pendant un an de résider et de travailler aux Etats-Unis.</p>
<p>La carte de résident permanent est valable 10 ans et doit être renouvelée généralement six mois avant sa date d’expiration. Tout changement d’adresse soit être signalé dans les 10 jours. Si vous séjournez hors des Etats-Unis pendant plus d’un an, mais moins de deux ans, vous devrez demander un permis de rentrée.</p>
<p>Vous pouvez perdre votre statut de résident permanent dans différents cas, notamment si vous vous établissez dans un autre pays de façon permanente et, sous certaines conditions, en cas de séjour prolongé hors des Etats-Unis.</p>
<p><strong>Programme de loterie (<i>Diversity Immigrant Visa Program</i> ou <i>Diversity Lottery</i>)</strong></p>
<p>Cette loterie propose chaque année un maximum de 55 000 visas de résident permanent. Les candidats sélectionnés sont tirés au sort par un ordinateur parmi les demandes recevables et en se basant sur le nombre de visas disponibles pour chaque région et pays.</p>
<p>Les visas sont répartis selon six régions géographiques, un nombre plus important de visas étant accordé aux régions présentant un faible taux d’immigration aux Etats-Unis. Dans chaque région, aucun pays ne peut obtenir plus de 7 % des visas accordés par la loterie. La loterie prend fin dès que les 55 000 visas ont été accordés ou à la fin de l’année fiscale qui s’étend du 1er octobre au 30 septembre de l’année suivante.</p>
<p>La participation à ce programme est toutefois soumise à conditions :</p>
<ul class="spip">
<li>le candidat doit, dans la plupart des cas, être né dans un pays autorisé à participer à la loterie. Ce n’est pas forcément le pays dont vous avez la nationalité ou où vous résidez. Sous certaines conditions, il est possible de se prévaloir du pays de naissance du conjoint ou de ceux des parents. Le ministère des Affaires étrangères publie chaque année la liste des pays qui peuvent participer à la loterie ;</li>
<li>le candidat doit justifier d’un diplôme d’enseignement secondaire ou d’une expérience professionnelle de deux ans au cours des cinq dernières années dans un emploi qui nécessite au moins deux années de formation ou d’expérience. Pour savoir si votre expérience professionnelle vous permet de participer à la loterie, vous pouvez consulter la base de données du <a href="http://www.doleta.gov/programs/onet/" class="spip_out" rel="external">ministère américain du Travail (<i>Department of Labor</i>)</a>. Pour la loterie, il s’agit des professions des zones 4 et 5 et classées à l’échelle 7 ou plus de préparation professionnelle spécifique (<i>Specific Vocational Preparation</i> - SVP). La participation à la loterie est gratuite. La période d’inscription à la loterie débute généralement chaque année début octobre et des informations sont mises en ligne sur le site Internet du ministère des Affaires étrangères dès le mois d’août.</li></ul>
<p>Les candidatures sont présentées <strong>uniquement par la voie électronique (le seul site gouvernemental américain pour la loterie américaine est le suivant</strong> : <a href="http://www.dvlottery.state.gov/" class="spip_out" rel="external">DVlottery.state.gov</a>).</p>
<p><strong>Une seule candidature par personne</strong> est autorisée. Un couple peut s’inscrire séparément si chacun rempli les critères d’éligibilité. Si l’un des deux est sélectionné, l’autre en bénéficie en tant qu’ayant-droit. Il est recommandé de soumettre sa candidature le plus tôt possible.</p>
<p>Vous pourrez savoir si votre candidature a été sélectionnée à travers une liste appelée « Entrant Status Check » sur le <a href="http://www.dvlottery.state.gov/" class="spip_out" rel="external">site web du programme</a>. Le département d’Etat n’envoie plus de notifications par e-mail ou de lettres par courrier postal aux candidats sélectionnés.</p>
<p><strong>Attention</strong> : le nombre de candidatures retenues étant supérieur à celui des visas accordés, la sélection ne garantit pas automatiquement l’obtention d’un visa. Vous devez donc effectuer les démarches d’obtention du visa au plus vite, sachant qu’il s’écoule au moins un an entre l’enregistrement de la candidature sur Internet et l’établissement du visa correspondant.</p>
<h3 class="spip"><a id="sommaire_3"></a>Carte de sécurité sociale</h3>
<p>Lorsqu’on veut travailler aux Etats-Unis, obtenir une carte de sécurité sociale (<strong>Social Security Card</strong>) est une démarche prioritaire à entreprendre. Votre numéro de sécurité sociale (<strong>Social Security Number</strong>) sera votre numéro d’identification valable en permanence auprès de l’Administration américaine et permet de déterminer si vous êtes éligible à la sécurité sociale.</p>
<p>Il y a deux manières de demander une carte de sécurité sociale :</p>
<ul class="spip">
<li>Dans votre pays d’origine, en même temps que votre demande de visa d’immigrant. Dans ce cas, vous n’aurez pas à vous déplacer dans un des bureaux de la sécurité sociale à votre arrivée.</li>
<li>A votre arrivée, en vous déplaçant dans le bureau de la sécurité sociale le plus proche de chez vous.</li></ul>
<p>Lorsque que vous n’êtes pas autorisé à travailler aux Etats-Unis, vous n’avez pas besoin de numéro de sécurité sociale. Sachez que vous n’avez pas besoin d’un numéro de sécurité sociale pour obtenir un permis de conduire, une assurance santé privée, pour s’inscrire dans une école, à la cantine ou pour un logement subventionné. Toutefois, si une agence gouvernementale ou privée vous demande votre numéro de sécurité sociale, demandez si vous pouvez être identifié par un autre moyen. Cela ne pose pas de problèmes dans la plupart des cas.</p>
<p>Si vous faite une demande de carte de sécurité sociale alors que vous n’êtes pas autorisé à travailler aux Etats-Unis, il vous faudra trouver une raison valable à cette demande (pour des raisons fiscales par exemple).</p>
<p>Pour toute information complémentaire, renseignez-vous auprès de l’<a href="http://www.socialsecurity.gov/immigration/" class="spip_out" rel="external">administration de la sécurité sociale</a>.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li>Site de l’<a href="http://french.france.usembassy.gov/" class="spip_out" rel="external">Ambassade des Etats-Unis en France</a></li>
<li><a href="http://travel.state.gov/" class="spip_out" rel="external">Bureau des Affaires consulaires du ministère des Affaires étrangères</a> (<i>Department of State - Bureau of Consular Affairs</i>)</li>
<li><a href="http://www.ice.gov/" class="spip_out" rel="external">US Immigration and Customs Enforcement</a></li>
<li><a href="http://www.dhs.gov/" class="spip_out" rel="external">Ministère de la Sécurité intérieure</a> (<i>Department of Homeland Security</i>)</li>
<li><a href="http://www.cbp.gov/" class="spip_out" rel="external">U.S. Customs and Border Protection</a>  (<i>Department of Homeland Security</i>)</li>
<li><a href="http://visas-etats-unis.com/" class="spip_out" rel="external">Cabinet Juridique Capp  Associates, droit américain de l’immigration</a></li></ul>
<p><i>Mise à jour : juin 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-etats-unis-entree-et-sejour-23321-article-animaux-domestiques-113141.md" title="Animaux domestiques">Animaux domestiques</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-sante.md" title="Vaccination">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-etats-unis-entree-et-sejour-23321-article-demenagement-113139.md" title="Déménagement">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-etats-unis-entree-et-sejour-23321-article-passeport-visa-permis-de-travail-113138.md" title="Passeport, visa, permis de travail">Passeport, visa, permis de travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/entree-et-sejour-23321/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
