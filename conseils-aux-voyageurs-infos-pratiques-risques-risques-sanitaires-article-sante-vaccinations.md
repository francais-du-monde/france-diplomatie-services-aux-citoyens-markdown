# Santé / Vaccinations

<p><strong><font color="red">31 octobre 2012</font></strong> / <strong><a href="http://www.sante.gouv.fr/information-concernant-la-vaccination-contre-la-typhoide.html" class="spip_out" rel="external">Informations du Ministère des Affaires sociales et de la Santé concernant la vaccination contre la typhoïde</a></strong>.</p>
<p>Des informations sommaires sur les conditions sanitaires sont indiquées dans les fiches pays de ce site. Il s’agit avant tout d’un aide mémoire. Il y a tout intérêt à compléter ces informations par celles qui sont disponibles sur d’autres sites, notamment les sites suivants :</p>
<ul class="spip">
<li><a href="http://www.who.int/fr/index.html" class="spip_out" rel="external">Organisation mondiale de la santé</a></li>
<li><a href="http://www.pasteur.fr/fr" class="spip_out" rel="external">Institut Pasteur</a></li>
<li><a href="http://www.invs.sante.fr/Dossiers-thematiques/Veille-et-alerte/Veille-sanitaire-internationale" class="spip_out" rel="external">Informations actualisées de l’Institut de veille sanitaire</a></li>
<li><a href="http://www.sante.gouv.fr/alertes-sanitaires.html" class="spip_out" rel="external">Ministère de la Santé</a></li></ul>
<p>Consultez en particulier votre médecin traitant avant tout voyage dans un pays impaludé ou touché par des maladies endémiques nécessitant des soins ou des mesures préventives (vaccinations, traitement du paludisme, règles d’hygiène de base, etc.).</p>
<p><a href="http://www.sante.gouv.fr/liste-des-centres-de-vaccination-habilites-a-effectuer-la-vaccination-anti-amarile.html" class="spip_out" rel="external">Liste des centres</a> habilités à effectuer la vaccination antiamarile et à délivrer les certificats internationaux de vaccination contre la fièvre jaune.</p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/infos-pratiques/risques/risques-sanitaires/article/sante-vaccinations). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
