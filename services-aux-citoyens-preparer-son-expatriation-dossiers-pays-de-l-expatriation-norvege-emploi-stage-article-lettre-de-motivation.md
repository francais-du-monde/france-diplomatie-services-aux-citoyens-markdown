# Lettre de motivation

<h4 class="spip">Rédaction</h4>
<p>La lettre de candidature doit être dactylographiée et ne doit pas excéder deux pages A4 standard. Une page est souvent suffisante et préférable.</p>
<ul class="spip">
<li>Lisez attentivement l’annonce et soyez certain de répondre à ce qu’elle demande.</li>
<li>Votre lettre doit expliquer pourquoi vous voulez ce poste précis ou pourquoi vous envoyez une candidature spontanée.</li>
<li>Indiquez clairement au destinataire que l’entreprise vous est familière, que vous avez les qualifications requises et que vous répondez aux critères.</li>
<li>Décrivez brièvement votre motivation pour postuler et expliquer pourquoi vous voulez déménager en Norvège.</li>
<li>Le CV et les références sont toujours demandés. Certains employeurs exigent en outre de joindre des certificats/diplômes/attestations de travail etc.</li>
<li>Envoyez votre lettre de candidature par courrier, par courrier électronique ou par fax.</li>
<li>Respectez la date limite de candidature (<i>søknadsfrist</i>). Il est conseillé de faire suite à la lettre de candidature par téléphone après la date limite de candidature.</li></ul>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.norvege.no/studywork/Travail/giude/#.UzGBKq10ZYc" class="spip_out" rel="external">Guide en ligne</a> sur le site de l’ambassade de France en Norvège</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/norvege/emploi-stage/article/lettre-de-motivation). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
