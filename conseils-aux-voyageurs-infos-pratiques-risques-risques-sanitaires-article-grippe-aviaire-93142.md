# Grippe aviaire

<p><strong>La grippe aviaire est une infection virale du groupe Influenzavirus A. Elle peut toucher presque toutes les espèces d’oiseaux, sauvages ou domestiques. Elle est responsable d’une épizootie dans certains pays (notamment en Asie du sud-est).</strong></p>
<p>Fortement contagieuse pour certaines espèces, elle peut entraîner une mortalité élevée. Le virus Influenza aviaire peut éventuellement infecter d’autres espèces animales comme le porc ou d’autres mammifères.<br class="autobr">Dans certaines conditions (contacts fréquents et intensifs avec des sécrétions respiratoires ou des déjections d’animaux infectés), la maladie peut se transmettre à l’homme.</p>
<p>Avant un séjour à l’étranger, il est recommandé :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  de prendre contact avec son médecin traitant afin de vérifier les mesures de prévention sanitaires et les vaccinations nécessaires pour votre voyage  (la question de la vaccination anti-grippale saisonnière pourra, notamment être abordée) ;</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  de se renseigner sur la situation sanitaire locale en consultant les sites Internet suivants : </p>
<ul class="spip">
<li><a href="http://www.grippeaviaire.gouv.fr/" class="spip_out" rel="external">Site interministériel traitant des menaces pandémiques grippales</a></li>
<li><a href="http://www.sante.gouv.fr" class="spip_out" rel="external">Ministère de la Santé (France)</a></li>
<li><a href="http://www.invs.sante.fr" class="spip_out" rel="external">Institut de veille sanitaire (France)</a></li>
<li><a href="http://www.anses.fr" class="spip_out" rel="external">Agence de sécurité sanitaire des aliments (France)</a></li>
<li><a href="http://www.who.int" class="spip_out" rel="external">OMS</a> et notamment <a href="http://www.who.int/mediacentre/factsheets/avian_influenza/fr/" class="spip_out" rel="external">la page Grippe Aviaire</a></li>
<li><a href="http://www.oie.int/fr/" class="spip_out" rel="external">Organisation Mondiale de la Santé Animale</a></li>
<li><a href="http://www.promedmail.org/" class="spip_out" rel="external">Site « Informations quotidiennes sur les pathologies émergentes »</a></li>
<li><a href="http://www.isid.org" class="spip_out" rel="external">Société internationale de maladies infectieuses</a></li>
<li><a href="http://www.eurosurveillance.org" class="spip_out" rel="external">Eurosurveillance</a></li></ul>
<p>Pendant le voyage, afin d’éviter tout risque de contamination, il est conseillé :</p>
<ul class="spip">
<li>de se tenir à distance des élevages de volailles et de porcs, des marchés d’animaux vivants, des fermes ou des zoos, des combats de coqs ;</li>
<li>de ne pas manipuler les cadavres d’oiseaux et d’éviter le contact avec leurs déjections (l’attention des chasseurs est particulièrement attirée) ;</li>
<li>de ne pas ramener de volaille vivante à son domicile ;</li>
<li>de se laver les mains régulièrement avec de l’eau savonneuse ou avec des solutions de lavage hydro-alcooliques ;</li>
<li>d’éviter de marcher pieds nus ou en sandales dans les marchés ou à proximité d’élevage de volailles (penser à laver soigneusement les pieds des enfants).</li></ul>
<p>Sur le plan alimentaire, il est recommandé :</p>
<ul class="spip">
<li>d’éviter la consommation de produits alimentaires crus ou peu cuits ;</li>
<li>d’isoler la viande de volaille crue des aliments cuits ou prêts à la consommation ;</li>
<li>de ne pas utiliser la même planche à découper ni le même couteau pour préparer les viandes crues et les aliments cuits ou prêts à la consommation ;</li>
<li>d’éviter de gober les œufs, de ne pas utiliser d’œuf cru ou à la coque dans des plats qui ne seront pas cuits ensuite ;</li>
<li>de laver scrupuleusement, à l’eau et au savon, toutes les surfaces et tous les ustensiles ayant servi à manipuler des aliments ;</li>
<li>de se laver les mains soigneusement après la manipulation des produits crus avant cuisson.</li></ul>
<p>Après le retour, en cas de syndrome grippal (fièvre, fatigue, douleurs musculaires, maux de tête, toux, écoulement nasal) et/ou de conjonctivite, contactez votre médecin traitant ou le Centre 15 de votre département (Tél. : 15).</p>
<p><strong>Pour en savoir plus</strong></p>
<p>Vous pouvez contacter la ligne Info’grippe au 0 825 302 302 (0,15 € ttc/min depuis un poste fixe en France. Service joignable depuis l’étranger). Le numéro est ouvert du lundi au samedi (hors jours fériés) de 9h00 à 19h00.</p>
<p><strong> <i>Document réalisé en collaboration avec la Direction Générale de la Santé.</i> </strong></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/infos-pratiques/risques/risques-sanitaires/article/grippe-aviaire-93142). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
