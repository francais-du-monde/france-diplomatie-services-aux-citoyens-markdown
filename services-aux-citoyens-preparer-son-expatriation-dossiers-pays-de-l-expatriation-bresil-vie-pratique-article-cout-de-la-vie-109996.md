# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/vie-pratique/article/cout-de-la-vie-109996#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/vie-pratique/article/cout-de-la-vie-109996#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/vie-pratique/article/cout-de-la-vie-109996#sommaire_3">Budget</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/vie-pratique/article/cout-de-la-vie-109996#sommaire_4">Alimentation</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/vie-pratique/article/cout-de-la-vie-109996#sommaire_5">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>L’unité monétaire est le real. Le real, divisé en 100 centimes (<i>centavos</i>), a cours depuis le 1er juillet 1994.</p>
<p>Il existe plusieurs taux de change mais sans différence notable.</p>
<p><strong>Au 16 novembre 2013</strong>, le real vaut 0,324 euros, c’est-à-dire qu’un euro vaut 3,08 réaux (sources : service économique régional).</p>
<p>Le change est difficile en France, à part à la Banco do Brasil à Paris car le real n’est pas convertible. Sur place, pas de difficultés dans les hôtels, dans les banques et les bureaux de change.</p>
<p>Les cartes de crédit et les chéquiers sont couramment utilisés pour effectuer des opérations bancaires sur les comptes ouverts au Brésil. Dans les grands hôtels et dans certains lieux touristiques, le dollar US est accepté mais cette pratique n’est pas officielle.</p>
<p>Les principales banques françaises sont représentées au Brésil (BNP Paribas, Crédit Agricole, Crédit Lyonnais, Société Générale). Toutefois, leurs activités sont uniquement destinées aux entreprises ; elles ne gèrent pas de comptes courants pour les particuliers.</p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<p>Il existe un contrôle des changes et le real n’est pas librement convertible.</p>
<p>Les transferts de fonds depuis l’extérieur sont illimités, mais les transferts vers l’extérieur sont limités à 3 000 USD par personne et par voyage hors du Brésil. Au-delà de ce montant, les démarches à effectuer sont complexes et nécessitent des justificatifs.</p>
<h3 class="spip"><a id="sommaire_3"></a>Budget</h3>
<p>Le candidat à l’expatriation devra se garder de chercher à apprécier sa situation en convertissant tous les chiffres en euros, ou en tentant de déterminer le pouvoir d’achat en France du montant obtenu. Cette appréciation doit se faire avant tout sur le montant des dépenses dans le pays d’expatriation, qu’elles soient exprimées en monnaie locale ou en euros.</p>
<h3 class="spip"><a id="sommaire_4"></a>Alimentation</h3>
<h4 class="spip">Conditions d’approvisionnement</h4>
<p>Les prix des produits alimentaires frais varient selon la saison. Le prix des poissons et crustacés est majoré de 50% environ à Brasilia du fait du transport. L’eau minérale est le plus souvent conditionnée en bonbonnes de 20 litres, livrées à domicile.</p>
<p>L’approvisionnement est bon. Les grandes surfaces de type hypermarchés sont nombreuses dans les grandes villes. On y trouve aussi bien des produits locaux que des produits importés mais ces derniers sont particulièrement chers (alcools, épicerie fine, parfumerie…). Une bouteille de vin français coûte trois à cinq fois plus cher qu’en France.</p>
<p>A Récife, les produits importés sont plus rares et l’approvisionnement moins régulier.</p>
<h4 class="spip">Prix moyen d’un repas dans un restaurant</h4>
<p>Selon qu’il s’agit d’un <i>fast food</i> ou d’un dîner gastronomique, le prix moyen d’un repas peut varier de :</p>
<ul class="spip">
<li>3 à 65 € à Rio ;</li>
<li>3 à 40 € à Sao Paulo.</li></ul>
<p>Le pourboire est généralement inclus dans le prix du repas ; sinon, il est laissé à l’appréciation du client (10% de la note environ).</p>
<h3 class="spip"><a id="sommaire_5"></a>Evolution des prix</h3>
<p>Selon l’enquête internationale Mercer sur le coût de la vie de 2012, Sao Paulo était la 12ème ville la plus chère du monde et Rio de Janeiro la 13ème alors qu’en 2008 elles occupaient respectivement le 62ème et 64ème rang. Brasilia figure quant à elle à la 45ème place.</p>
<p>Pour plus d’information sur l’indice des prix à la consommation, vous pouvez consulter le site internet de l’Institut brésilien de la géographie et de la statistique (<i><a href="http://www.ibge.gov.br/" class="spip_out" rel="external">Instituto Brasileiro de Geografia e Estadistica - IBGE</a></i>) ainsi que celui de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/vie-pratique/article/cout-de-la-vie-109996). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
