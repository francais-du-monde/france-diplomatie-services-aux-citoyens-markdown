# Scolarisation

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/vie-pratique/article/scolarisation#sommaire_1">Les établissements scolaires français à Singapour</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/vie-pratique/article/scolarisation#sommaire_2">Enseignement supérieur</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Les établissements scolaires français à Singapour</h3>
<p>Pour toute information sur la scolarisation dans le système français à l’étranger, vous pouvez consulter notre thématique sur les <a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures.md" class="spip_in">Etudes supérieures</a> et la <a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais.md" class="spip_in">Scolarité en français</a>.</p>
<p>Vous y trouverez des renseignements sur :</p>
<ul class="spip">
<li>les établissements français du primaire et du secondaire à l’étranger ;</li>
<li>les bourses scolaires et la prise en charge des frais de scolarité à l’étranger ;</li>
<li>les possibilités qui s’offrent à vous si votre enfant ne peut être scolarisé à l’étranger dans le système français (enseignement à distance par le CNED, programme français langue maternelle (FLAM), internats en France) ;</li>
<li>les épreuves du baccalauréat à l’étranger ;</li>
<li>les bourses d’études supérieures en France et à l’étranger ;</li>
<li>l’équivalence des diplômes.</li></ul>
<p>Deux établissements à Singapour :</p>
<p>Le <a href="http://www.lfs.edu.sg/" class="spip_out" rel="external">Lycée français</a>, établissement conventionné de la maternelle à la terminale.</p>
<p><a href="http://www.lapetiteecole.com.sg/" class="spip_out" rel="external">La petite école</a>, en cours d’homologation, maternelle uniquement.</p>
<h3 class="spip"><a id="sommaire_2"></a>Enseignement supérieur</h3>
<p>D’une manière générale, le système éducatif singapourien est inspiré du modèle britannique bien que l’influence américaine soit de plus en plus perceptible. Il n’y a aucun point commun avec le double système français des Grandes écoles et des universités.</p>
<p>L’enseignement supérieur singapourien se caractérise par deux mots clés : sélection et qualité. Comme souvent en Asie, l’accès à l’Université est associé à une forte sélection. À ceci s’ajoute le fait que les universités singapouriennes sont très bien classées.</p>
<h4 class="spip">Accès à l’enseignement supérieur</h4>
<p>Au cours de leurs études secondaires, les élèves subissent une première sélection dans une classe équivalente à notre classe de seconde (appelée <i>GCE ‘O’ LEVEL</i>).</p>
<p>Les meilleurs lycéens poursuivent en <i>Junior Colleges</i> pendant deux ans et passent l’équivalent du baccalauréat (<i>GCE ‘A’ LEVEL</i>) qui leur donne accès à l’université. Héritage britannique, les examens <i>‘O’ LEVEL</i> et <i>‘A’ LEVEL</i>, sont encore corrigés à l’Université de Cambridge, en Grande-Bretagne.</p>
<p>Les élèves ayant obtenu leur <i>GCE ‘O’ LEVEL</i> avec un profil plutôt technique ou commercial se dirigent vers les <i>Polytechnic Institutes</i>. Ils y acquièrent en trois ans les connaissances nécessaires pour devenir des techniciens qualifiés. Les élèves ayant obtenu de très bons résultats en <i>Polytechnic Institutes</i> peuvent également, en fin de formation, rejoindre l’université. Chaque année, en moyenne, environ 1 000 diplômés, intègrent l’université par ce canal.</p>
<p>Ceux qui ont échoué ou ont obtenu de plus faibles résultats au <i>GCE ‘O’ LEVEL</i> sont dirigés vers les <i>Institutes of Technical Education</i> (lycées techniques) dans lesquels ils ont la possibilité de suivre une formation technique théorique ou une formation par apprentissage. Les meilleurs élèves d’<i>Institute of Technical Education </i>(ITE) peuvent entrer dans les <i>Polytechnic Institutes </i>après avoir décroché leur Certificat d’Études d’ITE.</p>
<p>Une nouvelle alternative au système <i>‘O’ LEVEL / ’A’ LEVEL</i> a été mise en place à Singapour : le <i>International Baccalaureate Diploma program</i>. Pour le moment, seule une école nationale, <i>Anglo-Chinese School</i> (indépendante), offre ce programme, déjà dispensé par les lycées étrangers présents à Singapour.</p>
<h4 class="spip">Etablissements scolaires</h4>
<p>Il existe actuellement quatre universités autonomes à Singapour délivrant des diplômes nationaux. Les deux premières universités <i>National University of Singapore</i> (NUS) et <i>Nanyang Technological University</i> (NTU) sont situées à proximité des parcs technologiques qui accueillent des entreprises de technologies de pointe. La troisième université (<i>Singapore Management University</i> (SMU), à vocation économique et commerciale, s’est installée à la rentrée 2005 sur un nouveau campus de 4,5 hectares au cœur du centre ville pour être proche du quartier d’affaires et des banques.</p>
<p>La quatrième université, <i>Singapore University of Technology and Design</i> (SUTD), a été créée en 2008 en collaboration avec la prestigieuse université américaine <i>Massachusetts Institute of Technology</i> (MIT).</p>
<p>Plusieurs universités privées ou instituts spécialisés ont choisi Singapour, de par sa position de <i>hub</i> dans la région, pour implanter leur campus asiatique et délivrer leur diplôme <i>undergraduate</i> ou <i>postgraduate</i>. Ces écoles sont plutôt à vocation Business ou Services.</p>
<p>Enfin, des établissements français d’enseignement ont un campus à Singapour avec le double objectif, d’une part d’accueillir en Asie les étudiants inscrits en France, d’autre part de recruter plus massivement des étudiants en Asie.</p>
<h4 class="spip">Coopération</h4>
<p>La coopération française avec Singapour a toujours été très active en termes d’éducation, à travers des échanges académiques, le Double Degree, et des programmes de Coopération. Le programme FDDP (<i>French Double Degree Programme</i>) résulte d’accords entre NUS et six Grandes Écoles d’ingénieurs françaises. Il s’agit de l’École Polytechnique (X), de l’École Centrale de Paris (ECP), de l’École Supérieure d’Électricité (Supélec), de l’École Nationale Supérieure des Mines de Paris (ENSMP ou Mines ParisTech), de Télécom ParisTech, et <a href="http://www.ensta-paristech.fr/en/home" class="spip_out" rel="external">ENSTA ParisTech (École Nationale Supérieure de Techniques Avancées)</a></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Pour en savoir plus, vous pouvez consulter le site de l’<a href="http://www.institutfrancais.sg/?lang=en" class="spip_out" rel="external">Institut Français à Singapour</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/singapour/vie-pratique/article/scolarisation). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
