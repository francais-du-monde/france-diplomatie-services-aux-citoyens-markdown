# Curriculum vitae et lettre de motivation

<p><strong>Rédaction</strong></p>
<p>Votre CV - obligatoirement dactylographié - doit être actualisé. Ce dernier doit tout d’abord mentionner vos emplois et vos formations les plus récents. Les informations les plus anciennes figureront en fin de CV. La longueur de celui-ci ne doit pas excéder deux pages A4. Si vous le souhaitez, vous pouvez joindre une photo, surtout si vous cherchez du travail dans le secteur des services. Indiquez votre situation de famille, mentionnez d’éventuelles fonctions honorifiques et les coordonnées de personnes auxquelles il est possible de demander des références (anciens professeurs, etc.).</p>
<p>L’Union européenne a adopté un modèle de CV européen unique. Celui-ci convient aux personnes disposant d’un diplôme professionnel ou universitaire. Ce CV Europass présente clairement la valeur et les compétences du candidat. Il est actuellement disponible en plusieurs langues et peut être créé sur le site <a href="https://europass.cedefop.europa.eu/fr/documents/curriculum-vitae" class="spip_out" rel="external">Europass</a>.</p>
<p>Outre le CV, vous devez également rédiger une lettre de motivation dactylographiée expliquant les raisons de votre candidature et indiquant pourquoi vous êtes la bonne personne pour ce poste. Écrivez une lettre de motivation spécifique pour chaque fonction et consacrez le temps nécessaire à sa rédaction.</p>
<p><strong>Diplômes</strong></p>
<p>Les personnes ayant obtenu un diplôme d’enseignement supérieur à l’étranger peuvent demander une reconnaissance de leurs acquis à la <a href="http://www.oph.fi/" class="spip_out" rel="external">Direction générale de l’enseignement</a>.</p>
<p>Si vous êtes convoqué à un entretien d’embauche, il serait bon de vous y préparer minutieusement. Réfléchissez à l’avance aux questions que l’employeur pourrait vous poser. Pensez également à ce que vous souhaitez savoir au sujet de cet emploi. Amenez vos principaux certificats professionnels et diplômes à l’entrevue. Vérifiez que toutes vos attestations sont à jour. Il convient également de faire traduire les principaux documents en finnois, en suédois ou en anglais selon l’employeur.</p>
<p><i>Mise à jour : janvier 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/emploi-stage/article/curriculum-vitae-et-lettre-de-motivation). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
