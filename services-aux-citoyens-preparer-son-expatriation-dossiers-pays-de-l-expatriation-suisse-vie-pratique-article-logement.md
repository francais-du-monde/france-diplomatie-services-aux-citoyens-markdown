# Logement

<h4 class="spip">Hôtels - Auberges</h4>
<p>La Suisse est une destination touristique qui peut sembler chère, par conséquent, il est important de préparer son séjour de manière méthodique en cherchant les offres touristiques. Les villes comme Genève, Zurich ou celles qui bordent le lac Léman sont très chères en comparaison au reste du pays où par exemple les hébergements chez l’habitant se sont considérablement développés. Les tarifs appliqués sont compétitifs et généralement moins élevés que ceux des hôtels traditionnels. Les types d’hébergement sont nombreux : nuit sur la paille, dans un iglou, dans un tipi, à la belle étoile, dans une chambre d’hôte, dans un gîte, dans un hôtel prestigieux, dans une auberge prestigieuse, à la ferme ou encore dans un camping…</p>
<p>Suisse Tourisme (STS) et les centres locaux et régionaux d’information touristique sont à la disposition des visiteurs pour tous renseignements.</p>
<p><a href="http://www.swisscamps.ch/" class="spip_out" rel="external">Association suisse des campings</a></p>
<p><i>Mise à jour : février 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/vie-pratique/article/logement). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
