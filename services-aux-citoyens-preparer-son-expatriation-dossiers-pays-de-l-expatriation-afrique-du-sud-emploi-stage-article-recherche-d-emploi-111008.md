# Recherche d’emploi

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/emploi-stage/article/recherche-d-emploi-111008#sommaire_1">Outils pour la recherche d’emploi</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/emploi-stage/article/recherche-d-emploi-111008#sommaire_2">Organismes pour la recherche d’emploi</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/emploi-stage/article/recherche-d-emploi-111008#sommaire_3">Opportunités d’emploi et de stages</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Outils pour la recherche d’emploi</h3>
<h4 class="spip">Journaux</h4>
<p>Les grands quotidiens nationaux comportent des offres d’emplois. C’est d’ailleurs dans ces pages que l’entreprise désireuse de recruter une personne étrangère doit faire paraître une offre d’emploi. A titre d’exemple, le quotidien <a href="http://www.iol.co.za/the-star" class="spip_out" rel="external">The Star</a> publie le mercredi des offres d’emploi aux pages <i>classified</i>.</p>
<h4 class="spip">Sites internet</h4>
<ul class="spip">
<li><a href="http://www.careerjunction.co.za/" class="spip_out" rel="external">Careerjunction.co.za</a></li>
<li><a href="http://www.kelly.co.za/" class="spip_out" rel="external">Kelly.co.za</a></li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Organismes pour la recherche d’emploi</h3>
<h4 class="spip">Avant le départ</h4>
<ul class="spip">
<li><a href="http://www.pole-emploi-international.fr/" class="spip_out" rel="external">Pôle Emploi International</a></li>
<li><a href="https://www.civiweb.com/FR/index.aspx" class="spip_out" rel="external">Volontariat international en entreprise ou en administration (VIE/VIA)</a></li></ul>
<h4 class="spip">Sur place</h4>
<p><a href="http://www.fsacci.co.za/" class="spip_out" rel="external">Chambre de commerce et d’industrie franco-sud-africaine (FSACCI)</a></p>
<ul class="spip">
<li><strong>Bureau de Johannesburg</strong><br class="manualbr">52 Grosvenor Road, Fairway Office Park <br class="manualbr">Sable House, Ground Floor, Bryanston<br class="manualbr">Johannesburg 2021<br class="manualbr">Téléphone : (011) 267 5750 <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/emploi-stage/article/recherche-d-emploi-111008#fsacci#mc#fsacci.co.za#" title="fsacci..åt..fsacci.co.za" onclick="location.href=mc_lancerlien('fsacci','fsacci.co.za'); return false;" class="spip_mail">Courriel</a></li>
<li><strong>Bureau du Cap</strong><br class="manualbr">2 Dean Street, Gardens<br class="manualbr">+27 21 422 1972<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/emploi-stage/article/recherche-d-emploi-111008#capeadmin#mc#fsacci.co.za#" title="capeadmin..åt..fsacci.co.za" onclick="location.href=mc_lancerlien('capeadmin','fsacci.co.za'); return false;" class="spip_mail">Courriel</a></li>
<li><strong>Bureau de Durban</strong><br class="manualbr">c/o Alliance Française de Durban<br class="manualbr">22 Sutton Crescent, Morningside<br class="manualbr">+27 31 312 9582<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/emploi-stage/article/recherche-d-emploi-111008#durban#mc#fsacci.co.za#" title="durban..åt..fsacci.co.za" onclick="location.href=mc_lancerlien('durban','fsacci.co.za'); return false;" class="spip_mail">Courriel</a></li></ul>
<p><a href="http://www.dav.co.za/" class="spip_out" rel="external">DAV Professional Placement Group</a><br class="manualbr">Johannesburg +27 11 217 0000<br class="manualbr">Cape Town +27 21 468 7000</p>
<p><a href="http://www.kelly.co.za/" class="spip_out" rel="external">KELLY</a><br class="manualbr">6 Protea Place, Cnr Fredman Drive,<br class="manualbr">Sandton<br class="manualbr">Tel : +27 11722 8300</p>
<h3 class="spip"><a id="sommaire_3"></a>Opportunités d’emploi et de stages</h3>
<p>Des offres d’emploi ou de stage sont ponctuellement publiées sur le site de l’ambassade de France et des consulats :</p>
<ul class="spip">
<li><a href="http://www.ambafrance-rsa.org/-Francais-" class="spip_out" rel="external">Ambassade</a></li>
<li><a href="http://www.consulfrance-jhb.org/" class="spip_out" rel="external">Consulat de France à Johannesburg</a></li>
<li><a href="http://www.consulfrance-lecap.org" class="spip_out" rel="external">Consulat de France au Cap</a></li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/emploi-stage/article/recherche-d-emploi-111008). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
