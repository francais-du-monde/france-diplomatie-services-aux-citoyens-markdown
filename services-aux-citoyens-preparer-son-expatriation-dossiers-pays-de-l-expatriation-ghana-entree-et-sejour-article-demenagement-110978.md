# Déménagement

<p>Il est particulièrement conseillé de procéder à l’acheminement de ses effets personnels par bateau pour les gros déménagements, en containers plombés avec assurance tous risques, à cause des vols (délai de trois semaines environ). L’avion est recommandé pour les petits déménagements (trois jours hors dédouanement). Les formalités administratives sont identiques dans les deux cas.</p>
<p>Si la société de déménagement que vous avez choisie n’est pas représentée au Ghana, vous devrez vous adresser à un transitaire sur place qui se chargera des opérations de dédouanement.</p>
<p>Le Ghana étant un pays anglophone, il est conseillé de faire traduire en anglais l’inventaire valorisé de vos effets pour gagner du temps en douane.</p>
<p>Il est déconseillé d’apporter des objets neufs ou de grande valeur, compte tenu de l’importance des taxes à payer sur certains produits. De même, en cas d’importation de véhicules d’occasion de 10 ans ou plus, l’importance de la taxation est à souligner, d’autant que la valeur du véhicule qui sera prise en compte est celle qui sera estimée par les douanes.</p>
<p>S’agissant d’un déménagement international, il est primordial de s’entourer d’un maximum de garanties en faisant appel à un professionnel disposant de certifications reconnues (marque NF Service, ISO 9002, FAIM etc.)</p>
<p>Pour en savoir plus sur les restrictions, interdictions et taxes à l’importation au Ghana consultez la <a href="http://www.gra.gov.gh/" class="spip_out" rel="external">page de l’autorité fiscale ghanéenne</a>.</p>
<p><a href="http://www.csdemenagement.fr/" class="spip_out" rel="external">Chambre syndicale du déménagement</a><br class="manualbr">Tél. : 01 49 88 61 40 - Télécopie : 01 49 88 61 46<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/entree-et-sejour/article/demenagement-110978#contact#mc#csdemenagement.fr#" title="contact..åt..csdemenagement.fr" onclick="location.href=mc_lancerlien('contact','csdemenagement.fr'); return false;" class="spip_mail">Courriel</a></p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/entree-et-sejour/article/demenagement-110978). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
