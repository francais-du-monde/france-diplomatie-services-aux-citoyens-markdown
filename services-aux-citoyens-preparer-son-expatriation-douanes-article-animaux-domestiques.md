# Animaux domestiques

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/douanes/article/animaux-domestiques#sommaire_1">Le pays de destination se trouve hors de l’Union européenne</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/douanes/article/animaux-domestiques#sommaire_2">Le pays de destination se trouve dans l’Union européenne (sauf Irlande, Malte, Suède et Royaume-Uni) </a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/douanes/article/animaux-domestiques#sommaire_3">Le pays de destination est l’Irlande, Malte, la Suède ou le Royaume-Uni</a></li></ul><dl class="spip_document_84243 spip_documents spip_documents_center">
<dt>
<img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L1000xH190/DSC_0277-1000x190_cle054bd8-7e86f.jpg" width="1000" height="190" alt="JPEG - 77.2 ko"></dt>
</dl>
<h3 class="spip"><a id="sommaire_1"></a>Le pays de destination se trouve hors de l’Union européenne</h3>
<p>Certains pays réglementent l’entrée des animaux sur leur territoire (permis d’importation, quarantaine, interdiction). Prévoyez un délai d’au moins dix jours pour effectuer toutes les formalités, voire de plusieurs mois pour les pays exigeant une quarantaine.</p>
<p>Pour connaître les conditions exactes, vous devrez prendre contact :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  avec <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/article/annuaire-des-representations-francaises-a-l-etranger" class="spip_in">l’ambassade en France</a> du pays de destination. Si, au cours de son transport de la France vers le pays de destination, l’animal doit transiter par un ou plusieurs pays, vous devrez également vous conformer à la réglementation du ou des pays de transit de l’animal. A noter que certains pays refusent le transit d’animaux.
</p>
<p>Des informations générales sur la réglementation de nombreux pays sont également disponibles sur le site Internet de l’<a href="http://www.iatatravelcentre.com/" class="spip_out" rel="external">Association internationale du transport aérien (AITA)</a>.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  le cas échéant, avec la ou les compagnies aériennes pour connaître les conditions de transport de l’animal (en soute ou en cabine, normes des cages, nourriture, etc.).
</p>
<p>Pour connaître les normes internationales de transport des animaux et des cages, vous pouvez consulter le site Internet de l’<a href="http://www.iata.org/" class="spip_out" rel="external">Association internationale du transport aérien (AITA)</a>. Si l’ambassade dispose d’une information particulière, celle-ci doit être communiquée à votre vétérinaire traitant, titulaire d’un mandat sanitaire, qui vous guidera pour la suite.</p>
<p>Dans le cas où l’ambassade ne disposerait pas d’informations sur la réglementation sanitaire de son pays, vous devrez respecter, par défaut, les conditions suivantes :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  l’animal doit être en règle vis-à-vis des exigences réglementaires françaises en matière de santé et de protection animales. Pour plus de renseignements sur la réglementation française, vous pouvez prendre contact avec votre vétérinaire traitant, titulaire d’un mandat sanitaire, ou les services vétérinaires du Ministère de l’Agriculture et de la Pêche (anciennes Directions départementales des services vétérinaires). Vous trouverez leurs coordonnées sur le site Internet de la Direction régionale de l’Alimentation, de l’Agriculture et de la Forêt (DRAAF).</p>
<p>Les documents suivants sont obligatoires pour l’animal et doivent être établis par le vétérinaire traitant, titulaire d’un mandat sanitaire :</p>
<ul class="spip">
<li>identification par micropuce ou tatouage ;</li>
<li>certificat de vaccination contre la rage en cours de validité ;</li>
<li>certificat international de bonne santé, établi par le vétérinaire traitant, titulaire du mandat sanitaire, dans la semaine précédant le départ de France.</li></ul>
<p>Il est également conseillé de faire procéder à un titrage des anticorps anti-rabiques dans un laboratoire agréé et de se munir du carnet de vaccination tenu à jour de l’animal. Vous devrez ensuite prendre rendez-vous avec le service " santé et protection animales " des services vétérinaires dépendant du Ministère de l’Agriculture dont relève le vétérinaire traitant, pour la validation des documents établis par celui-ci.</p>
<p><strong>Légalisation des documents</strong></p>
<p>Certains pays exigent que les documents validés par les services vétérinaires dépendants du Ministère de l’Agriculture soient ensuite légalisés ou munis de l’Apostille. Il convient donc de se renseigner sur ce point auprès de l’<a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-etrangers-en-france/article/annuaire-des-representations-etrangeres-en-france" class="spip_in">ambassade du pays de destination</a>.</p>
<p>Pour connaître le régime de légalisation du pays de destination, vous pouvez également consulter le site Internet du <a href="services-aux-citoyens-legalisation-et-notariat-legalisation-et-certification-de-signatures-article-la-legalisation-de-documents-publics-francais-destines-a-une-autorite-etrangere.md" class="spip_in">ministère des Affaires étrangères</a>.</p>
<p>L’apostille s’obtient auprès des cours d’appels. Vous pouvez trouver leurs coordonnées sur le site Internet du <a href="http://www.justice.gouv.fr/" class="spip_out" rel="external">ministère de la Justice</a>.</p>
<p>La légalisation est effectuée par le bureau des légalisations du ministère des affaires étrangères. Pour toute information sur les légalisations, vous pouvez consulter le site Internet du <a href="services-aux-citoyens-legalisation-et-notariat-legalisation-et-certification-de-signatures-article-la-legalisation-de-documents-publics-francais-destines-a-une-autorite-etrangere.md" class="spip_in">ministère des Affaires étrangères</a> ou contacter :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Bureau des légalisations</strong><br class="manualbr">57 boulevard des Invalides - 75007 Paris<br class="manualbr">Téléphone (de 14 à 16 heures) : 01 53 69 38 28 / 01 53 69 38 29 <br class="manualbr">Télécopie : 01 53 69 38 31
</p>
<p>Pour toute information complémentaire, vous pouvez consulter le site Internet du <a href="http://agriculture.gouv.fr/" class="spip_out" rel="external">ministère de l’Agriculture</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Le pays de destination se trouve dans l’Union européenne (sauf Irlande, Malte, Suède et Royaume-Uni) </h3>
<p>Les chiens, les chats et les furets doivent satisfaire aux conditions suivantes :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  être identifiés par puce électronique (<strong>NB : A partir du 3 juillet 2011</strong>, pour venir en France avec son animal de compagnie à partir d’un pays de l’UE ou pour voyager au sein de l’Union européenne, l’animal identifié à partir de cette date doit obligatoirement disposer d’<strong>une identification par puce électronique</strong>.</p>
<p><strong>Attention</strong> : Les animaux <strong>identifiés par tatouage avant le 03 juillet 2011</strong> pourront continuer à voyager au sein de l’UE pourvu qu’il soit clairement lisible.)</p>
<ul class="spip">
<li>être valablement vaccinés contre la rage ;</li>
<li>être titulaires d’un passeport délivré par un vétérinaire habilité attestant de l’identification et de la vaccination contre la rage de l’animal ;</li>
<li>dans le cas de la Finlande, avoir subi un traitement contre l’échinococcose moins de 30 jours avant le départ. Pour en savoir plus, vous pouvez consultez le site Internet <a href="http://www.evira.fi/portal/en/" class="spip_out" rel="external">Evira.fi/portal/en</a>. Le système d’identification électronique, ainsi que la reconnaissance de la validité de la vaccination contre la rage, peuvent varier d’un Etat membre à l’autre. Il est donc vivement recommandé de prendre contact avec l’<a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-etrangers-en-france/article/annuaire-des-representations-etrangeres-en-france" class="spip_in">ambassade du pays de destination</a>.</li></ul>
<p>En France, la réalisation de la primo-vaccination antirabique n’est considérée comme valable qu’à partir de 21 jours après la fin du protocole de vaccination prescrit par le fabricant. La vaccination antirabique de rappel est considérée en cours de validité le jour de sa réalisation.</p>
<h3 class="spip"><a id="sommaire_3"></a>Le pays de destination est l’Irlande, Malte, la Suède ou le Royaume-Uni</h3>
<p>Les chiens et les chats doivent satisfaire aux conditions suivantes :</p>
<ul class="spip">
<li>être âgés d’au moins trois mois ;</li>
<li>être identifiés par puce électronique. Mais la Suède reconnaît également la méthode d’identification par tatouage) ;</li>
<li>être valablement vaccinés contre la rage ;</li>
<li>avoir subi un titrage sérique des anticorps antirabiques, sauf pour les furets (examen de laboratoire effectué sur un prélèvement sanguin et permettant de s’assurer de la validité de la vaccination de l’animal contre la rage) dans un <a href="http://ec.europa.eu/food/animal/liveanimals/pets/approval_en.htm" class="spip_out" rel="external">laboratoire agréé par l’Union européenne</a>. Le résultat doit être supérieur ou égal à 0,5 UI/ml ;</li>
<li>être titulaire d’un passeport délivré par un vétérinaire titulaire d’un mandat sanitaire ;</li>
<li>avoir subi un traitement contre les tiques et l’échinococcose ;</li>
<li>pour Malte et le Royaume-Uni, être acheminés par un moyen de transport reconnu.</li></ul>
<p><strong>Attention :</strong> <br class="autobr">La réglementation diffère selon le pays sur les points suivants :</p>
<ul class="spip">
<li>la méthode d’identification ;</li>
<li>le délai à respecter entre la vaccination contre la rage et le prélèvement sanguin ;</li>
<li>le délai à respecter entre le prélèvement sanguin et la date d’expédition de l’animal ;</li>
<li>le délai à respecter entre la date du traitement contre les tiques et contre l’échinococcose et l’expédition de l’animal. Il est, par conséquent, conseillé de prendre contact avec l’<a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-etrangers-en-france/article/annuaire-des-representations-etrangeres-en-france" class="spip_in">ambassade du pays de destination</a> et de consulter les sites Internet suivants :</li></ul>
<ul class="spip">
<li>Site du <a href="http://www.agriculture.gov.ie/" class="spip_out" rel="external">ministère irlandais de l’Agriculture</a>.</li>
<li>Site du <a href="http://www.jordbruksverket.se/swedishboardofagriculture.4.6621c2fb1231eb917e680002462.html" class="spip_out" rel="external">ministère suédois de l’Agriculture</a>.</li>
<li>Site du <a href="http://www.defra.gov.uk/" class="spip_out" rel="external">ministère de l’Environnement, de l’Alimentation et des Questions rurales du Royaume-Uni</a>.</li></ul>
<p>Pour plus d’informations :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Site <a href="https://www.service-public.fr/particuliers/vosdroits/F21374" class="spip_out" rel="external">Service-public.fr</a></p>
<p><i>Mise à jour : juin 2016</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/douanes/article/animaux-domestiques). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
