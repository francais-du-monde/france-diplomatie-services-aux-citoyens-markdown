# Recherche d’emploi

<h4 class="spip">Outils pour la recherche d’emploi</h4>
<p><strong>Journaux</strong></p>
<ul class="spip">
<li><a href="http://www.bangkokpost.com/" class="spip_out" rel="external">Bangkok Post</a></li>
<li><a href="http://www.nationmultimedia.com/" class="spip_out" rel="external">The Nation</a></li></ul>
<h4 class="spip">Organismes pour la recherche d’emploi</h4>
<p>La Chambre de commerce franco-thaïe dispose d’un service emploi. Les offres sont publiées sur leur site internet. Si vous souhaitez postuler à l’un de ces emplois, vous devez adresser par courriel votre curriculum vitae et votre lettre de motivation (en anglais) avec la référence de l’annonce à l’adresse suivante : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/emploi-stage/article/recherche-d-emploi#employment#mc#francothaicc.com#" title="employment..åt..francothaicc.com" onclick="location.href=mc_lancerlien('employment','francothaicc.com'); return false;" class="spip_mail">employment<span class="spancrypt"> [at] </span>francothaicc.com</a></p>
<p>En cas de candidature spontanée, la lettre de motivation doit contenir le type de poste recherché, ainsi que votre disponibilité.</p>
<p><a href="http://www.francothaicc.com" class="spip_out" rel="external">Chambre de commerce franco-thaïe (CCFT)</a><br class="manualbr">5th Floor - Indosuez House - 152 Wireless Road<br class="manualbr">Lumpini - Pathumwan - Bangkok 10330<br class="manualbr">Téléphone : [66] (0) 26 50 96 13-4 <br class="manualbr">Télécopie : [66] (0) 26 50 97 39<br class="manualbr">Courriel : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/emploi-stage/article/recherche-d-emploi#employment#mc#francothaicc.com#" title="employment..åt..francothaicc.com" onclick="location.href=mc_lancerlien('employment','francothaicc.com'); return false;" class="spip_mail">employment<span class="spancrypt"> [at] </span>francothaicc.com</a></p>
<p><i>Mise à jour : juillet 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/emploi-stage/article/recherche-d-emploi). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
