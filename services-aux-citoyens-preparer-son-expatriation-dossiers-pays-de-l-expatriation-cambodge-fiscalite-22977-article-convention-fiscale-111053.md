# Convention fiscale

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/cambodge/fiscalite-22977/article/convention-fiscale-111053#sommaire_1">Règles d’imposition en France</a></li></ul>
<p><strong>Dans les pays n’ayant pas conclu de convention fiscale avec la France (ce qui est le cas pour le Cambodge)</strong>, la situation de l’expatrié français est réglée à l’égard du fisc français par la loi du 29 décembre 1976 modifiant les règles de territorialité et les conditions d’imposition des Français de l’étranger (Journal Officiel du 30 décembre 1976) dont les principes et les modalités d’application sont exposés ci-dessous.</p>
<p>Le texte du Journal Officiel peut être obtenu auprès de la Direction des Journaux Officiels, par courrier (26 rue Desaix 75727 Paris Cedex 15), par Fax (01 40 58 77 80), par Minitel code 36 16 JOURNAL OFFICIEL ou sur le site Internet du <a href="http://www.impots.gouv.fr/" class="spip_out" rel="external">ministère des Finances</a> (choisir la rubrique non-résidents).</p>
<h3 class="spip"><a id="sommaire_1"></a>Règles d’imposition en France</h3>
<h4 class="spip">Principes</h4>
<p>Les personnes qui ont en France leur domicile fiscal sont imposables sur la totalité de leurs revenus, quelle que soit leur source.</p>
<p>Celles dont le domicile fiscal est hors de France ne sont imposables que sur leurs revenus de source française.</p>
<p>Sont considérées comme ayant un domicile fiscal en France :</p>
<ul class="spip">
<li>les personnes qui ont en France leur foyer ou le lieu de leur séjour principal ;</li>
<li>celles qui exercent en France une activité professionnelle salariée ou non, à moins qu’elles ne justifient que cette activité y est exercée à titre accessoire ;</li>
<li>celles qui ont en France le centre de leurs intérêts économiques.</li></ul>
<p>Il suffit de répondre à une seule de ces trois conditions pour être réputé domicilié en France du point de vue fiscal.</p>
<p>Etant donné la diversité des situations, le Français qui travaille dans un pays étranger sans convention fiscale en vigueur avec la France pourra déterminer le régime fiscal qui lui est applicable en France en étudiant les divers cas exposés ci-après et en répondant aux questions ci-dessous :</p>
<ul class="spip">
<li>Suis-je domicilié en France ?</li>
<li>Si oui, est-ce que je perçois des revenus de source étrangère et dans quelles conditions ?</li>
<li>Ai-je la disposition en France d’une ou plusieurs habitations ?</li></ul>
<h4 class="spip">Situation des Français domiciliés en France, mais exerçant provisoirement à l’étranger</h4>
<p>Les salariés envoyés à l’étranger par leur employeur mais dont la famille reste en France conservent leur domicile fiscal en France. Ils sont ainsi taxables par le fisc français sur la totalité de leurs revenus.</p>
<p>Toutefois, la loi prévoit deux cas d’exonération au bénéfice des salariés envoyés à l’étranger par un employeur établi en France :</p>
<p><strong>Cas des salariés français soumis à l’impôt à l’étranger</strong></p>
<p>Si le salarié français répondant aux conditions ci-dessus justifie avoir été soumis, dans le pays étranger et sur le salaire qu’il y a perçu, à un impôt égal au moins aux deux tiers de l’impôt qu’il aurait acquitté en France sur un revenu identique, il n’est pas redevable d’un impôt en France sur cette partie de son revenu.</p>
<p><strong>Cas des salariés français dont l’activité à l’étranger s’exerce dans les activités</strong></p>
<ul class="spip">
<li>chantier de construction ou de montage, installation d’ensembles industriels, leur mise en route et leur exploitation, la prospection et l’ingénierie y afférentes ;</li>
<li>prospection, recherche et extraction de ressources naturelles.</li></ul>
<p>Dès lors que l’exercice de ces activités a justifié un séjour à l’étranger supérieur à 183 jours (plus de six mois) au cours d’une période de douze mois consécutifs, les salaires rémunérant l’activité à l’étranger sont exonérés de l’impôt français sur le revenu (sans que l’intéressé ait à prouver qu’il est soumis à un impôt à l’étranger).</p>
<p><strong>Précisions concernant les deux cas précités</strong></p>
<p>Il peut se faire que le contribuable ait perçu dans l’année d’autres revenus qui ne bénéficient d’aucune exonération (revenus d’une activité exercée en France, revenus du conjoint, etc).</p>
<p>Pour des raisons d’équité fiscale, le taux d’imposition applicable à la partie non exonérée de ses revenus sera calculé sur l’ensemble de son revenu (non exonéré et exonéré), mais appliqué bien entendu exclusivement sur la part non exonérée.</p>
<p>Dans l’hypothèse où aucun des deux cas d’exonération ne serait applicable, les rémunérations ne sont soumises à l’impôt en France qu’à concurrence du montant du salaire qui aurait été perçu si l’activité avait été exercée en France.</p>
<p>Cela revient à exonérer les avantages particuliers (primes, indemnités, etc) qui s’attachent à l’exercice d’une profession à l’étranger.</p>
<p>Sous cette seule réserve, les modalités de calcul et de règlement de l’impôt sont celles de droit commun.</p>
<h4 class="spip">Situation des Français domiciliés à l’étranger</h4>
<p><strong>Ne disposant pas d’habitation en France et percevant un salaire en rémunération de leur activité à l’étranger</strong></p>
<p>Ils ne sont pas imposables en France, sauf s’ils perçoivent d’autres revenus de source française.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Disposant d’une ou plusieurs habitations en France : </p>
<p>(Par "disposant", il convient d’entendre non seulement les habitations dont l’intéressé est propriétaire, mais éventuellement aussi celles dont il est locataire, usufruitier ou dont il jouit gratuitement par l’intermédiaire d’un tiers.)</p>
<p>Dans ce cas, une imposition forfaitaire minimale est établie d’après la valeur locative de l’habitation en France ; la base de l’impôt sur le revenu est constituée par trois fois la valeur locative réelle de cette ou de ces habitations.</p>
<p>Ce mode de taxation ne donne pas lieu à application du taux minimum de 25 % évoqué ci-après (imposition des revenus de source française).</p>
<p>Lorsque les revenus de source française sont supérieurs à la base d’imposition forfaitaire minimale, c’est le montant de ces revenus qui doit être retenu pour l’établissement de l’impôt.</p>
<p>En outre, les Français domiciliés à l’étranger peuvent être exonérés de cette imposition minimale s’ils justifient qu’ils sont soumis dans le pays où ils ont leur domicile à un impôt sur l’ensemble de leurs revenus au moins égal aux deux tiers de celui qui leur serait applicable en France.</p>
<p><strong>Impositions des revenus de source française</strong></p>
<p>Ces revenus sont déterminés et imposés dans les conditions du droit commun français avec deux exceptions à ce principe général :</p>
<ul class="spip">
<li>Les charges déductibles du revenu global ne sont pas prises en compte.</li>
<li>Quels que soient le montant des revenus et le résultat de l’application du barème, la loi fixe un taux minimum d’imposition de 25 % (sauf aux personnes qui pourraient justifier que l’impôt français sur leur revenu global serait inférieur à ce taux).</li></ul>
<p>Ces dispositions s’appliquent, soit au contribuable qui ne dispose pas d’habitation en France, soit à celui qui, disposant d’une telle habitation, perçoit des revenus de source française supérieurs à trois fois la valeur locative de cette habitation.</p>
<p>La loi a prévu un régime plus favorable pour les Français domiciliés à l’étranger qui perçoivent des traitements, salaires, pensions et rentes de source française.</p>
<table class="spip" summary="{{Taux de  15&amp;nbsp;%}}  |{{Taux de  25&amp;nbsp;%}}">
<caption><strong>Taux de 0 %</strong></caption>
<tbody>
<tr class="row_odd odd">
<td>Durée de l’activité ou période correspondant au paiement</td>
<td>Fraction des sommes nettes soumises à retenue</td>
<td>Fraction des sommes nettes soumises à retenue :</td>
<td>Fraction des sommes nettes soumises à retenue</td></tr>
<tr class="row_even even">
<td>Année</td>
<td>inférieure à 9839 €</td>
<td>de 9839 à 28 548 €</td>
<td>supérieure à 28 548 €</td></tr>
</tbody>
</table>
<p><strong>Modalités de recouvrement de l’impôt</strong></p>
<p>Pour les traitements, salaires, pensions et rentes viagères, la loi institue une retenue à la source dont le taux est celui indiqué au paragraphe ci-dessus. La retenue est opérée par le ou les débiteurs. L’obligation déclarative du contribuable dépend du nombre de débiteurs, car :</p>
<ul class="spip">
<li>Lorsque les salaires et pensions n’excèdent pas 28 548 € <strong>et</strong> sont versés par un seul débiteur, le contribuable <strong>n’est pas tenu de souscrire une déclaration</strong> ;</li>
<li>En revanche, lorsque ces revenus excèdent 28 548 € <strong>ou</strong> sont versés par plusieurs débiteurs, une déclaration de revenus <strong>devra être obligatoirement souscrite.</strong></li></ul>
<p>Nota : la base est déterminée comme en matière d’impôt sur le revenu.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Traitements et salaires diminués de la déduction normale de 10 % éventuellement de la déduction supplémentaire pour frais professionnels et de l’abattement de 20 %.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/cambodge/fiscalite-22977/article/convention-fiscale-111053). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
