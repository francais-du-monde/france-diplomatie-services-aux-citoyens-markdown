# Vaccination

<p>Aucune vaccination n’est exigée à l’entrée du pays pour un voyageur en provenance de France. Les éventuelles vaccinations des voyageurs venant de zones endémiques seraient précisées par les ambassades du Vietnam auprès desquelles serait déposée la demande de visa.</p>
<p>Un certificat de vaccination contre la fièvre jaune est exigé, uniquement pour les voyageurs en provenance d’un pays à risque de transmission de la fièvre jaune.</p>
<p>Les vaccinations suivantes sont conseillées :</p>
<p><strong>Adultes</strong> : mise à jour des vaccinations contre la diphtérie, le tétanos, et la poliomyélite ; vaccination contre la typhoïde, l’hépatite A, l’hépatite B ; vaccinations contre les méningites A + C en période d’épidémie.</p>
<p><strong>Enfants</strong> : vaccinations recommandées en France par le ministère de la Santé - et en particulier : B.C.G. et hépatite B dès la naissance, rougeole dès l’âge de neuf mois suivie d’une revaccination six mois plus tard en association avec les oreillons et rubéole, typhoïde à partir de l’âge de deux ans. Ne pas oublier le vaccin pentavalent (Diphtérie, Tétanos, Poliomyélite, Coqueluche, Haemophilus influenzae b) dès la naissance.</p>
<p>Encéphalite japonaise à partir de l’âge d’un an pour tout séjour prolongé en zone rurale durant la période de transmission (saison des pluies : mai à octobre), plutôt au nord du pays.</p>
<p>Le vaccin contre la rage à titre préventif (J0, J7, J21 ou J28) est recommandé en situation isolée mais ne dispense pas d’un traitement curatif (deux injections de rappel espacées de trois jours) en cas d’exposition avérée.</p>
<p>Il est recommandé de consulter un médecin et d’effectuer toutes les vaccinations nécessaires avant de partir.</p>
<p>Pour en savoir plus, lisez notre rubrique thématique <a href="services-aux-citoyens-preparer-son-expatriation-sante.md" class="spip_in">Santé</a>.</p>
<p><i>Mise à jour : février 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/entree-et-sejour/article/vaccination-111357). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
