# Documents d’identité

<h4 class="spip">Passeport</h4><dl class="spip_document_84235 spip_documents spip_documents_center">
<dt>
<img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L1000xH190/DSC_0279-1000x190_cle0acb11-326c7.jpg" width="1000" height="190" alt="JPEG - 74.9 ko"></dt>
</dl>
<p><strong>Si vous êtes domicilié en France</strong>, vous devez déposer votre demande de passeport en vous rendant personnellement (prise des empreintes digitales) à la mairie de votre ville ou à la préfecture ou sous-préfecture compétente selon votre résidence.</p>
<p>Pour toute information, vous pouvez consulter les sites Internet suivants :</p>
<ul class="spip">
<li>le portail de l’<a href="http://www.service-public.fr/" class="spip_out" rel="external">administration française</a> ;</li>
<li>le portail du <a href="http://www.interieur.gouv.fr/" class="spip_out" rel="external">ministère de l’Intérieur</a> ;</li>
<li>le portail de l’<a href="http://www.ants.interieur.gouv.fr/" class="spip_out" rel="external">Agence nationale des titres sécurisés</a>.</li></ul>
<p><strong>Si vous êtes résident à l’étranger</strong>, vous devez déposer votre demande de passeport en vous rendant personnellement (prise des empreintes digitales) auprès du <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/" class="spip_in">consulat ou l’ambassade</a> proche de votre domicile.</p>
<p>Pour connaître les conditions de délivrance du passeport biométrique aux Français établis hors de France, vous pouvez consultez l’article <a href="services-aux-citoyens-documents-officiels-a-l-etranger-article-comment-faire-une-demande-de-passeport.md" class="spip_in">Comment faire une demande de passeport ?</a>.</p>
<p>A l’étranger, le délai de délivrance d’un passeport est de l’ordre de trois semaines à un mois <i>à compter de la réception par le consulat du dossier complet</i>.</p>
<p><i>Mise à jour : janvier 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/documents-de-voyage/article/documents-d-identite). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
