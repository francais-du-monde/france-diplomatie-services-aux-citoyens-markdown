# Déménagement

<p>Le déménagement peut s’effectuer soit par bateau, soit par avion suivant les volumes et le poids transportés. Dans les deux cas, un justificatif et un inventaire sont nécessaires.</p>
<p>Par voie maritime (solution moins onéreuse), le délai varie de trois semaines à un mois depuis un port européen, alors qu’il est de quinze jours pour un déménagement par avion.</p>
<p><strong>Franchise de droits :</strong></p>
<p>Les personnes qui transfèrent <strong>pour la première fois leur résidence au Canada pour s’y établir de façon permanente ou pour un séjour temporaire d’au moins trois ans</strong> bénéficient d’une franchise de droits pour l’importation de leurs effets personnels.</p>
<p>Des dispositions particulières s’appliquent aux anciens résidents qui reviennent vivre au Canada, aux étudiants, ainsi qu’aux travailleurs séjournant moins de trois ans au Canada. Vous trouverez toute information utile sur le site de l’<a href="http://www.cbsa.gc.ca/" class="spip_out" rel="external">Agence des services frontaliers du Canada</a>.</p>
<p>Les articles autorisés sont les vêtements et le linge de maison, l’ameublement, les appareils ménagers, l’argenterie, les bijoux, les antiquités, les objets de famille, les collections privées de pièces de monnaie, de timbres et d’œuvres d’art, les ordinateurs personnels, les livres, les instruments de musique, les outils et autres articles de bricolage, les véhicules privés, les bateaux de plaisance et les remorques servant à les transporter, les remorques mobiles n’excédant pas 2,6 mètres de largeur que les propriétaires peuvent transporter eux-mêmes, les remorques utilitaires, les autocaravanes, les aéronefs privés, les hangars pour outils ou garages qui ne sont pas fixés à une habitation ou qui n’en font pas partie.</p>
<p>Pour être admis en franchise de droits, ces objets doivent répondre aux conditions suivantes :</p>
<ul class="spip">
<li>ils doivent vous accompagner au moment de votre entrée sur le territoire canadien. Si vos effets personnels sont importés à une date ultérieure, vous devrez les avoir déclarés au moment de votre arrivée au Canada ;</li>
<li>vous devez en être le propriétaire ;</li>
<li>ils doivent avoir été en votre possession avant votre arrivée au Canada ;</li>
<li>vous devez les avoir utilisés avant votre entrée sur le territoire canadien ;</li>
<li>vous ne devez pas vendre ou céder ces effets personnels admis en franchise de droits dans les 12 mois suivant leur importation. Dans le cas contraire, vous devrez acquitter des droits calculés sur la valeur de l’objet au moment de la vente ou de la cession.</li></ul>
<p>Il pourra être utile, à cet effet, de vous munir des factures pour certains de ces objets.</p>
<p>A noter que l’importation de certaines marchandises est soumise à des conditions particulières, voire interdite :</p>
<ul class="spip">
<li>les véhicules automobiles (pour en savoir plus, reportez-vous au chapitre <a href="services-aux-citoyens-preparer-son-expatriation-douanes.md" class="spip_in">Douanes</a>) ;</li>
<li>les boissons alcoolisées et les produits du tabac (site Internet de l’<a href="http://www.cbsa.gc.ca/" class="spip_out" rel="external">Agence des services frontaliers au Canada</a>) ;</li>
<li>les <a href="services-aux-citoyens-preparer-son-expatriation-douanes-article-animaux-domestiques.md" class="spip_in">animaux domestiques</a> ;</li>
<li>les armes à feu (site Internet du <a href="http://www.cfc-cafc.gc.ca/" class="spip_out" rel="external">Centre des armes à feu Canada</a>) ;</li>
<li>les explosifs, les pièces pyrotechniques et les munitions</li>
<li>les produits alimentaires, les plantes et les animaux (site Internet de l’<a href="http://www.inspection.gc.ca/" class="spip_out" rel="external">Agence canadienne d’inspection des aliments</a>) ;</li>
<li>les espèces menacées d’extinction</li></ul>
<p>Votre déménageur vous indiquera la liste des documents à fournir aux douanes canadiennes.</p>
<p>Dans tous les cas, vous devrez, à votre arrivée au Canada, présenter à l’inspecteur des douanes les documents suivants :</p>
<ul class="spip">
<li>une <strong>liste détaillée</strong>, en deux exemplaires, de préférence dactylographiée de tous les effets que vous prévoyez d’importer. <br class="manualbr">Dans la mesure du possible, vous indiquerez la marque, le modèle, le numéro de série et la valeur approximative de chaque article. Pour les effets domestiques à caractère général (par exemple, ustensiles de cuisine), vous pouvez vous contenter d’indiquer une valeur globale.<br class="manualbr">Cette liste devra être divisée en deux parties : la première indiquera les objets que vous avez avec vous le jour de votre arrivée au Canada ; la deuxième listera les effets qui entreront ultérieurement au Canada.<br class="manualbr">Cette liste peut être remplacée par les formulaires <a href="http://www.cbsa.gc.ca/publications/forms-formulaires/b4-fra.pdf" class="spip_out" rel="external">B4</a> et <a href="http://www.cbsa.gc.ca/publications/forms-formulaires/b4a.pdf" class="spip_out" rel="external">B4A</a> . Ces documents peuvent être téléchargés sur le site Internet de l’<a href="http://www.cbsa.gc.ca/" class="spip_out" rel="external">Agence des services frontaliers du Canada</a>.</li>
<li>votre passeport avec le visa permanent.</li></ul>
<p>A noter que les douanes canadiennes ne gardent les effets personnels que seulement 40 jours. Passé ce délai, vos effets seront considérés comme non réclamés. Si les marchandises arrivent après votre entrée au Canada, votre transitaire devra présenter aux douanes une copie du formulaire B4. Seules les marchandises indiquées sur ce formulaire peuvent être admises en franchise de droits. Il n’existe aucune limite de temps pour faire venir vos effets personnels.</p>
<p><i>Source : <a href="http://www.cbsa.gc.ca/" class="spip_out" rel="external">Agence des services frontaliers du Canada</a></i></p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li>Site Internet de l’<a href="http://www.cbsa.gc.ca/" class="spip_out" rel="external">Agence des services frontaliers du Canada</a> ;</li>
<li>L’article <a href="services-aux-citoyens-preparer-son-expatriation-douanes-article-demenagement.md" class="spip_in">Déménagement</a>.</li></ul>
<p>S’agissant d’un déménagement international, il est primordial de s’entourer d’un maximum de garanties en faisant appel à un professionnel disposant de certifications reconnues (marque NF Service, ISO 9002, etc.).</p>
<p><a href="http://www.csdemenagement.fr/" class="spip_out" rel="external">Chambre syndicale du déménagement</a><br class="manualbr">Téléphone : 01 49 88 61 40 <br class="manualbr">Télécopie : 01 49 88 61 46<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/entree-et-sejour/article/demenagement#contact#mc#csdemenagement.fr#" title="contact..åt..csdemenagement.fr" onclick="location.href=mc_lancerlien('contact','csdemenagement.fr'); return false;" class="spip_mail">Courriel</a></p>
<p><i>Mise à jour : avril 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/entree-et-sejour/article/demenagement). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
