# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/vie-pratique/article/cout-de-la-vie#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/vie-pratique/article/cout-de-la-vie#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/vie-pratique/article/cout-de-la-vie#sommaire_3">Budget</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/vie-pratique/article/cout-de-la-vie#sommaire_4">Alimentation </a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/vie-pratique/article/cout-de-la-vie#sommaire_5">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>L’unité monétaire est le bolivar fort, son code ISO est VEF. Un bolivar se divise en 100 céntimos. Les différentes pièces de monnaies sont de 1, 5, 10, 12½, 25 et 50 céntimos et 1 bolivar. Les billets sont au nombre de six (5, 10, 20, 50 et 100 bolivars.). Un euro vaut environ 8,60 bolivars.</p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<p>Des distributeurs sont présents dans tout le pays. Les principales banques sont : Banco Provincial, Mercantil et Banesco, elles sont ouvertes du lundi au vendredi, de 8h30 à 11h30 et de 14h à 16h30.</p>
<h3 class="spip"><a id="sommaire_3"></a>Budget</h3>
<p>Le candidat à l’expatriation devra se garder de chercher à apprécier sa situation en convertissant tous les chiffres en euros, ou en tentant de déterminer le pouvoir d’achat en France du montant obtenu. Cette appréciation doit se faire avant tout sur le montant des dépenses dans le pays d’expatriation, qu’elles soient exprimées en monnaie locale ou en euros, en regard du salaire qui est susceptible d’y être perçu.</p>
<p>D’une façon générale, en raison de son économie largement dominée par les hydrocarbures, le Venezuela est l’un des pays d’Amérique Latine où le coût de la vie est assez élevé. Il évolue cependant à des rythmes différents selon les villes, ainsi Valencia et Maracaibo connaissent une plus forte expansion.</p>
<h3 class="spip"><a id="sommaire_4"></a>Alimentation </h3>
<p>Il est possible de trouver à peu près tous les produits sur le marché local. Attention, toutefois, les délais sont très longs et il est courant d’avoir des ruptures de stocks.</p>
<p>Les commerces sont ouverts de 9h à 18 ou 19h, certains ferment pour le déjeuner. A Caracas, les centres commerciaux ferment plus tard et sont ouverts 7 jours sur 7.</p>
<p>Pour connaître les prix des denrées alimentaires, consultez les sites internet des  supermarchés en ligne :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="https://www.elplazas.com/index/retail" class="spip_url spip_out" rel="external">https://www.elplazas.com/index/retail</a></p>
<h4 class="spip">Prix moyen d’un repas dans un restaurant </h4>
<p>Le midi les restaurants proposent des <i>menu del dia</i> à des prix avantageux. De même les <i>panaderias</i> vendent des plats de type pain au fromage ou <i>empañadas</i> à petits prix. Dans les restaurants traditionnels les prix varient selon le type d’établissement et la qualité de la nourriture.</p>
<h3 class="spip"><a id="sommaire_5"></a>Evolution des prix</h3>
<p>La dépréciation du bolivar face au dollar et à l’euro s’est considérablement accélérée depuis le début des années 2000 conduisant à une forte inflation. A titre d’exemple le taux d’inflation était d’environ 27% en 2010.</p>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/vie-pratique/article/cout-de-la-vie). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
