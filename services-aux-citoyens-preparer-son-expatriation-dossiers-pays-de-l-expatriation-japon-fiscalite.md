# Fiscalité

<h2 class="rub22803">Fiscalité du pays</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/fiscalite/#sommaire_1">Impôts sur le revenu des particuliers</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/fiscalite/#sommaire_2">Année fiscale</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/fiscalite/#sommaire_3">Barème de l’impôt</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/fiscalite/#sommaire_4">Solde du compte en fin de séjour</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/fiscalite/#sommaire_5">Coordonnées des centres d’information fiscale</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Impôts sur le revenu des particuliers</h3>
<p>Il n’existe pas, au Japon, de déclaration conjointe ; les époux sont imposés séparément. L’imposition du revenu est opérée à la fois <strong>aux niveaux national et local</strong>.</p>
<h4 class="spip">Organisation de l’impôt sur le revenu des particuliers</h4>
<ul class="spip">
<li>Les impôts nationaux comportent l’impôt national sur le revenu.</li>
<li>Les impôts locaux sont divisés en impôts à montant fixe (kintô wari) et en impôts locaux sur le revenu, eux-mêmes subdivisés en impôt local municipal et impôt local préfectoral.</li></ul>
<h4 class="spip">Revenus imposables, taux d’imposition et crédits d’impôt</h4>
<p>La base imposable des impôts sur le revenu des particuliers varie en fonction de leur lieu de résidence.</p>
<p>Est considérée comme fiscalement non-résidente toute personne n’ayant pas son domicile au Japon et/ou y résidant depuis moins d’un an.</p>
<p>Est considérée comme fiscalement résidente toute personne ayant son domicile (lieu de séjour principal) ou ayant eu sa résidence (lieu d’habitation pour une certaine période) au Japon pendant plus d’un an.</p>
<p>On distingue deux catégories de résidents :</p>
<ul class="spip">
<li>les résidents non permanents (il s’agit des personnes qui n’ont pas l’intention de s’établir définitivement au Japon et qui y résident depuis une période continue de moins de cinq ans) ;</li>
<li>les résidents permanents (il s’agit des personnes ayant leur domicile ou résidence au Japon depuis plus de cinq ans et qui souhaitent s’y établir définitivement).</li></ul>
<h4 class="spip">Revenus imposables en fonction du statut de résidence</h4>
<table class="spip">
<thead><tr class="row_first"><th id="ideb83_c0">Statut du contribuable</th><th id="ideb83_c1">Revenus de source japonaise</th><th id="ideb83_c2">Revenus de source étrangère perçus au Japon</th><th id="ideb83_c3">Revenus de source étrangère perçus à l’étranger</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<th headers="ideb83_c0" id="ideb83_l0">Résident permanent</th>
<td headers="ideb83_c1 ideb83_l0">Imposable</td>
<td headers="ideb83_c2 ideb83_l0">Imposable</td>
<td headers="ideb83_c3 ideb83_l0">Imposable</td></tr>
<tr class="row_even even">
<th headers="ideb83_c0" id="ideb83_l1">Résident non-permanent</th>
<td headers="ideb83_c1 ideb83_l1">Imposable</td>
<td headers="ideb83_c2 ideb83_l1">Imposable</td>
<td headers="ideb83_c3 ideb83_l1">Non-imposable</td></tr>
<tr class="row_odd odd">
<th headers="ideb83_c0" id="ideb83_l2">Non-résident</th>
<td headers="ideb83_c1 ideb83_l2">Imposable</td>
<td headers="ideb83_c2 ideb83_l2">Non-imposable</td>
<td headers="ideb83_c3 ideb83_l2">Non-imposable</td></tr>
</tbody>
</table>
<p>Les catégories de revenus imposables à l’impôt national sur le revenu sont notamment :</p>
<ul class="spip">
<li>les intérêts (soumis à un prélèvement libératoire de 15 %) ;</li>
<li>les dividendes (soumis à un prélèvement libératoires de 20 %) ;</li>
<li>les revenus immobiliers (déduction faite des dépenses de réparation et d’entretien) ;</li>
<li>les traitements et salaires ;</li>
<li>les revenus exceptionnels autres que ceux résultant de l’aliénation de biens ou pouvant être assimilés à des rémunérations pour services rendus ;</li>
<li>les revenus divers ;</li>
<li>les plus-values et moins-values sur cession de biens immobiliers ou de valeurs mobilières.</li></ul>
<h4 class="spip">Déclaration et paiement</h4>
<p>En principe, les résidents doivent remplir et renvoyer leur déclaration à leur centre d’impôts entre le 15 février et le 15 mars de l’année suivante.</p>
<p>En pratique, la plupart des salariés au Japon ne sont pas astreints à une déclaration d’impôts car c’est l’employeur qui est chargé d’acquitter tous les impôts sur le revenu pour le compte de ses employés lors du versement des salaires (<strong>principe de la retenue à la source</strong>).</p>
<p>Une déclaration individuelle reste néanmoins obligatoire pour les non-salariés et pour tous les salariés percevant des revenus différents du salaire supérieurs à 200 000 yens ou un salaire brut supérieur à 20 millions de yens par an.</p>
<h3 class="spip"><a id="sommaire_2"></a>Année fiscale</h3>
<p>Pour les particuliers, l’année fiscale correspond à l’année civile : du 1er janvier au 31 décembre.</p>
<h3 class="spip"><a id="sommaire_3"></a>Barème de l’impôt</h3>
<h4 class="spip">Impôts sur le revenu des particuliers</h4>
<p><strong>Détermination des traitements et salaires imposables à l’impôt national sur le revenu</strong></p>
<p>S’agissant des traitements et salaires, un système de <strong>prélèvement à la source </strong>par l’employeur est mis en place pour la majorité des salariés de sorte que ces derniers ne sont normalement pas astreints au paiement direct de l’impôt.</p>
<p>Ne sont pas imposables les avantages en nature perçus au titre de l’activité salariale :</p>
<ul class="spip">
<li>les frais de déplacements ;</li>
<li>les frais de réaménagement et de déménagement ;</li>
<li>les frais de formation ;</li>
<li>les frais de départ en retraite ;</li>
<li>les frais de logement sont partiellement exonérés.</li></ul>
<p><strong>Taux de l’impôt local municipal et préfectoral sur le revenu et montant du crédit d’impôt</strong></p>
<p>Le montant de l’impôt local à montant fixe (kintô wari) varie entre 2000 et 4000 yens par contribuable en fonction de la population de la ville de résidence du contribuable.</p>
<h4 class="spip">Déductions</h4>
<p><strong>Les déductions familiales</strong></p>
<table class="spip">
<thead><tr class="row_first"><th id="id7644_c0">Déductions</th><th id="id7644_c1">Sur l’impôt national</th><th id="id7644_c2">Sur l’impôt local</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="id7644_c0">Déduction pour conjoint*</td>
<td class="numeric " headers="id7644_c1">380 000</td>
<td class="numeric " headers="id7644_c2">330 000</td></tr>
<tr class="row_even even">
<td headers="id7644_c0">Par enfant à charge</td></tr>
<tr class="row_odd odd">
<td headers="id7644_c0">- moins de 16 ans</td>
<td class="numeric " headers="id7644_c1">380 000</td>
<td class="numeric " headers="id7644_c2">330 000</td></tr>
<tr class="row_even even">
<td headers="id7644_c0">- entre 16 et 22 ans</td>
<td class="numeric " headers="id7644_c1">630 000</td>
<td class="numeric " headers="id7644_c2">450 000</td></tr>
</tbody>
</table>
<p>*Le revenu annuel imposable du conjoint doit être inférieur à 1 030 000 yens, et le revenu du contribuable inférieur à 10 millions de yens par an.<br class="manualbr">La déduction spéciale pour conjoint a été supprimée depuis le 1er janvier 2004.</p>
<p><strong>Autres déductions</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Déductions</strong></td>
<td><strong>Montant</strong> (en yens)</td></tr>
<tr class="row_even even">
<td>Déduction de base</td>
<td>Impôt national : 380 000
<p>Impôt local : 330 000</p>
</td></tr>
<tr class="row_odd odd">
<td>Abattement pour personne à charge</td>
<td>de 480 000 à 630 000</td></tr>
<tr class="row_even even">
<td>Abattement pour personne handicapée à charge</td>
<td>de 730 000 à 980 000</td></tr>
<tr class="row_odd odd">
<td>Primes d’assurance-vie</td>
<td>Déduction maximum de 50 000</td></tr>
<tr class="row_even even">
<td>Prime d’assurance pour accidents</td>
<td>Déduction maximum de 15 000</td></tr>
<tr class="row_odd odd">
<td>Cotisations sociales (sécurité sociale, assurance-vieillesse)</td>
<td>Entièrement déductibles</td></tr>
<tr class="row_even even">
<td>Dons</td>
<td>Déduction plafonnée à 25 % du revenu annuel</td></tr>
<tr class="row_odd odd">
<td>Dépenses médicales (concerne uniquement les dépenses non couvertes par une assurance)</td>
<td>La déduction est de :<br> <br>
<ul class="spip">
<li>5 % des dépenses en-dessous de 100 000 yens ;</li>
<li>du montant dépassant 100 000 yens à partir de ce plafond. La déduction maximale est de 2 millions de yens par an</li></ul></td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_4"></a>Solde du compte en fin de séjour</h3>
<p>Un expatrié peut solder son compte en fin de séjour, il lui sera nécessaire d’obtenir un quitus fiscal avant de quitter le pays.</p>
<h3 class="spip"><a id="sommaire_5"></a>Coordonnées des centres d’information fiscale</h3>
<ul class="spip">
<li><a href="http://www.nta.go.jp/" class="spip_out" rel="external">Administration fiscale nationale</a> (Kokuzei Cho) :</li>
<li><a href="http://www.nta.go.jp/taxanswer/english/12003.htm" class="spip_out" rel="external">Bureaux régionaux des impôts nationaux</a> (Tokyo Regional Taxation Bureau)</li>
<li><a href="http://www.metro.tokyo.jp/ENGLISH/index.htm" class="spip_out" rel="external">Mairie de Tokyo</a> (impôts locaux)</li>
<li><a href="http://www.tax.metro.tokyo.jp/" class="spip_out" rel="external">Administration fiscale préfectorale de Tokyo</a>(uniquement en japonais)</li></ul>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="https://www.tresor.economie.gouv.fr/pays/japon" class="spip_out" rel="external">Service économique français au Japon</a></p>
<p><i>Mise à jour : décembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-japon-fiscalite-article-convention-fiscale.md" title="Convention fiscale">Convention fiscale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-japon-fiscalite-article-fiscalite-du-pays.md" title="Fiscalité du pays">Fiscalité du pays</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/fiscalite/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
