# Loisirs et culture

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/vie-pratique/article/loisirs-et-culture#sommaire_1">Tourisme</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/vie-pratique/article/loisirs-et-culture#sommaire_2">Activités culturelles</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/vie-pratique/article/loisirs-et-culture#sommaire_3">Sports</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/vie-pratique/article/loisirs-et-culture#sommaire_4">Télévision – Radio</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/vie-pratique/article/loisirs-et-culture#sommaire_5">Presse française</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Tourisme</h3>
<p>La Lituanie offre de nombreux sites d’intérêt touristique. Le tourisme vert se développe dans les régions des lacs et à la campagne. On trouve toutes sortes d’hébergement : hôtel, location de maison, logement chez l’habitant. Les déplacements en transport en commun sont encore peu commodes. Le réseau routier est quant à lui suffisamment étendu et en bon état.</p>
<p>Office de tourisme de Lituanie à Paris : <a href="http://www.infotourlituanie.fr/index.php?id=636" class="spip_out" rel="external">Infotourlituanie.fr</a></p>
<h3 class="spip"><a id="sommaire_2"></a>Activités culturelles</h3>
<h4 class="spip">Activités culturelles en français</h4>
<p>Sous l’égide du service de coopération et d’action culturelle de l’ambassade de France, l’Institut français de Lituanie ( IFL) propose des cours de français ainsi qu’une programmation culturelle et artistique variée. Un ciné-club est notamment programmé chaque semaine. L’IFL dispose également d’une médiathèque dotée de magazines, livres, CD et DVD en français.</p>
<p>Institut Français de Lituanie<br class="manualbr">Didzioji 1<br class="manualbr">LT-01128 Vilnius<br class="manualbr">Tél. : (370) 52 19 96 96<br class="manualbr">Fax : (370) 52 19 96 39</p>
<p><a href="http://www.didzioji1.lt/fr" class="spip_out" rel="external">Blog franco-lituanien</a>  d’information et de dialogue.</p>
<p>LTV, la chaîne nationale, a un accord avec CFI et diffuse régulièrement des reportages, documentaires, séries et des longs métrages de fiction, pour la plupart doublés en lituanien. Les autres chaînes privées diffusent de nombreux films français doublés ou sous-titrés.</p>
<h4 class="spip">Activités culturelles locales</h4>
<p><strong>Radio </strong> : la radio nationale est à dominante d’information. Les radios indépendantes sont à dominante musicale.</p>
<p><strong>Télévision </strong> : il existe cinq chaînes lituaniennes. Les programmes locaux nécessitent de connaître le lituanien ou le russe.</p>
<p>La vidéo est répandue et il existe des clubs vidéo (cassettes doublées en russe).</p>
<p><strong>Cinéma </strong> : l’étranger qui ne pratique pas les langues lituanienne ou russe peut aller voir des films étrangers grand public récents en V.O. sous-titrée. Un cinéma est affilié au réseau Europe-Cinéma qui prévoit une programmation majoritairement européenne. Enfin, les sociétés de distribution locales achètent de plus en plus de productions françaises.</p>
<p><strong>Théâtre </strong> : programme local riche avec les nombreux théâtres de Vilnius (du classique au contemporain).</p>
<p>L’Opéra-ballet de Lituanie présente tous les soirs un spectacle de danse (classique et moderne). Des concerts de qualité sont donnés à la Philharmonie nationale de Lituanie, à l’Académie de musique, etc.</p>
<p>Le centre d’art contemporain de Vilnius est reconnu comme étant le plus important et actif des pays baltes. Vilnius compte un grand nombre de galeries d’art commerciales, d’un intérêt variable.</p>
<h3 class="spip"><a id="sommaire_3"></a>Sports</h3>
<p>On peut pratiquer presque tous les sports en Lituanie. La chasse suppose d’adhérer à un club.</p>
<h3 class="spip"><a id="sommaire_4"></a>Télévision – Radio</h3>
<p>Les chaînes TV5, Arte et LCP sont proposées dans des bouquets câblés. Via le satellite Astra, on peut capter Fr2 et Fr3. Il est possible de capter les bouquets canalsat /canal+ et TNT par satellite.</p>
<h3 class="spip"><a id="sommaire_5"></a>Presse française</h3>
<p>Aucune presse papier disponible. Seuls les abonnements par courrier ou en version internet sont possibles.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/vie-pratique/article/loisirs-et-culture). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
