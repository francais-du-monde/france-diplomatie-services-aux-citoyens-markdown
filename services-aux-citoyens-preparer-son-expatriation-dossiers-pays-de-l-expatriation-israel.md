# Israël

<p>Au <strong>31 décembre 2014</strong>, <strong>51 945 Français</strong> étaient enregistrés dans les consulats de France à Tel Tel Aviv (43 342 inscrits) et Haifa (8702 inscrits), auxquels s’ajoutent 23 274 inscrits au Consulat général de France à Jérusalem.</p>
<p>La présence française en Israël est forte d’une centaine d’entreprises, dans les secteurs des transports, de la banque, de l’alimentaire, de l’énergie, du tourisme, des cosmétiques, de l’informatique, de la publicité, de la restauration, du luxe, de la pharmacie, etc.</p>
<p>Beaucoup de groupes industriels français sont représentés en Israël sur le marché des biens de consommation (Renault, PSA, Lacoste, L’Occitane, Lalique, Seb/Téfal, Arc, Roche Bobois, Dior, Bongrain, Danone, Maille, Fromageries Bel, Beghin, Daum, Christofle, Bernardaud, Saint Louis, etc.), mais aussi par des implantations directes souvent liées à une participation à de grands travaux d’infrastructures (Alcatel-Lucent, Alstom, BNP Paribas, Dexia, JC Decaux, L’Oréal, Louis Vuitton , Sanofi Aventis, ST Microelectronics, Veolia, etc.)</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/dossiers-pays/israel-territoires-palestiniens/" class="spip_in">Une description d’Israël, de sa situation politique et économique</a> ;</li>
<li><a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/israel-territoires-palestiniens/" class="spip_in">Des informations actualisées sur les <strong>conditions locales de sécurité</strong> en Israël</a> ;</li>
<li><a href="http://www.ambafrance-il.org/" class="spip_out" rel="external">Ambassade de France en Israël</a>.</li></ul>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-israel-entree-et-sejour.md">Entrée et séjour</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-israel-entree-et-sejour-article-passeport-visa-permis-de-travail.md">Passeport, visa, permis de travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-israel-entree-et-sejour-article-demenagement-110454.md">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-sante.md">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-israel-entree-et-sejour-article-animaux-domestiques.md">Animaux domestiques</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-israel-emploi-stage.md">Emploi, stage</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-israel-emploi-stage-article-marche-du-travail.md">Marché du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-israel-emploi-stage-article-reglementation-du-travail.md">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-israel-emploi-stage-article-recherche-d-emploi.md">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-israel-emploi-stage-article-curriculum-vitae.md">Curriculum vitae</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-israel-emploi-stage-article-lettre-de-motivation.md">Lettre de motivation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-israel-emploi-stage-article-entretien-d-embauche.md">Entretien d’embauche</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-israel-emploi-stage-article-stages.md">Stages</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-israel-protection-sociale-22898.md">Protection sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-israel-protection-sociale-22898-article-regime-local-de-securite-sociale-110464.md">Régime local de sécurité sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-israel-protection-sociale-22898-article-convention-de-securite-sociale-110465.md">Convention de sécurité sociale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-israel-fiscalite.md">Fiscalité</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-israel-fiscalite-article-fiscalite-du-pays.md">Fiscalité du pays</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-israel-fiscalite-article-convention-fiscale-110467.md">Convention fiscale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-israel-vie-pratique.md">Vie pratique</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-israel-vie-pratique-article-logement-110469.md">Logement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-israel-vie-pratique-article-sante.md">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-israel-vie-pratique-article-scolarisation.md">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-israel-vie-pratique-article-cout-de-la-vie.md">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-israel-vie-pratique-article-transports-110473.md">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-israel-vie-pratique-article-communications.md">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-israel-vie-pratique-article-loisirs-et-culture.md">Loisirs et culture</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-israel-presence-francaise.md">Présence française</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-israel-presence-francaise-article-presence-francaise.md">Présence française</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
