# Fiscalité

<h2 class="rub22811">Fiscalité du pays</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/fiscalite/#sommaire_1">Présentation</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/fiscalite/#sommaire_2">Année fiscale</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/fiscalite/#sommaire_3">Barème de l’impôt </a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/fiscalite/#sommaire_4">Quitus fiscal</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/fiscalite/#sommaire_5">Solde du compte en fin de séjour</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/fiscalite/#sommaire_6">Pour en savoir plus</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Présentation</h3>
<p>Pour toute information relative à la fiscalité du Grand-Duché du Luxembourg, veuillez consulter le <a href="http://www.impotsdirects.public.lu/" class="spip_out" rel="external">site internet</a></p>
<h4 class="spip">Inscription auprès du fisc</h4>
<p>Les rémunérations versées au titre de l’exercice d’une occupation salariée et les pensions versées par des caisses de pensions légales luxembourgeoises sont passibles de la retenue d’impôt à la source. La retenue d’impôt à la source, calculée selon les <a href="http://www.impotsdirects.public.lu/az/b/barem_impo/index.html" class="spip_out" rel="external">barèmes de l’impôt</a> sur les salaires ou pensions, est déterminée sur base des inscriptions mentionnées sur la fiche de retenue d’impôt (appelée communément "carte d’impôt") par :</p>
<ul class="spip">
<li>l’<strong>employeur</strong> pour le compte du salarié ;</li>
<li>la <strong>caisse de pension légale luxembourgeoise</strong> pour le compte du pensionné. A défaut de fiche de retenue d’impôt remise à l’employeur ou à la caisse de pension, la retenue d’impôt sur les salaires ou pensions est déterminée selon la classe d’impôt 1, sans que l’impôt puisse être inférieur à 33 %.</li></ul>
<p>A partir de l’année d’imposition 2013, les fiches de retenue d’impôt des salariés et des pensionnés résidents seront <strong>établies automatiquement par l’administration des contributions directes</strong>.</p>
<p>La fiche d’impôt est <strong>envoyée directement par courrier postal</strong> à leurs destinataires. Il n’est pas possible de récupérer directement cette fiche de retenue d’impôt auprès des bureaux RTS émetteurs.</p>
<p>Si le contribuable n’a pas reçu sa fiche de retenue d’impôt avant le 1er mars de l’année considérée, il doit alors contacter le bureau RTS compétent.</p>
<p>En règle générale, une fiche de retenue d’impôt pour contribuables résidents <strong>est émise et mise à jour par l’administration des contributions</strong>, sans intervention et sans demande de la part du contribuable :</p>
<ul class="spip">
<li>pour toute affiliation ou désaffiliation d’un salarié par un employeur auprès du Centre commun de la sécurité sociale ;</li>
<li>pour tout changement d’état civil, d’adresse ou de composition du ménage du contribuable auprès de l’administration communale luxembourgeoise ;</li>
<li>pour tout changement d’adresse ;</li>
<li>pour tout départ à la retraite en application de la législation luxembourgeoise.</li></ul>
<p>Il convient de noter que les contribuables résidents doivent continuer à s’adresser à l’administration communale luxembourgeoise pour tout changement d’état civil (mariage, divorce, décès), de composition de ménage (naissance d’un enfant) et d’adresse.</p>
<h4 class="spip">Date et lieu de dépôt des déclarations de revenus</h4>
<p>Vous pouvez consulter le calendrier fiscal sur le site de l’<a href="http://www.impotsdirects.public.lu/" class="spip_out" rel="external">administration des contributions directes</a> rubrique A à Z / calendrier fiscal.</p>
<h4 class="spip">Modalités de paiement des impôts</h4>
<ul class="spip">
<li>retenue à la source pour les salariés (uniquement pour l’impôt sur le revenu) ;</li>
<li>paiement par avances trimestrielles pour les autres revenus.</li></ul>
<h4 class="spip">Les classes d’impôts</h4>
<p>Les contribuables résidents sont répartis en trois classes :</p>
<ul class="spip">
<li>la <strong>classe 1</strong> comprend les personnes qui n’appartiennent ni à la classe 1a ni à la classe 2 ;</li>
<li>la <strong>classe 1a </strong>comprend les personnes veuves, celles qui ont plus de 64 ans au début de l’année d’imposition ou encore celles qui bénéficient d’une modération d’impôt pour enfant ;</li>
<li>la <strong>classe 2 </strong>comprend les époux et les partenaires imposés collectivement, les personnes veuves dont le mariage a été dissous par décès au cours des trois années précédant l’année d’imposition, les personnes divorcées séparées de corps ou de fait en vertu d’une dispense de la loi ou d’une autorité judiciaire au cours des trois années précédant l’année d’imposition.</li></ul>
<p>Le minimum vital exempt d’impôt s’élève à partir de 2009 à 11 265 €.<br class="manualbr">Il s’en suit que les montants du revenu exonéré s’élèvent à partir de l’année d’imposition 2009 :</p>
<ul class="spip">
<li>à 11 265 € pour les contribuables de la <a href="http://www.impotsdirects.public.lu/az/c/class_resid/index.html" class="spip_out" rel="external">classe d’impôt 1</a>, et</li>
<li>à 22 530 € pour les contribuables des <a href="http://www.impotsdirects.public.lu/az/c/class_resid/index.html" class="spip_out" rel="external">classes d’impôt 1a et 2</a></li></ul>
<p>Pour l’année d’imposition 2008 :</p>
<ul class="spip">
<li>à 10 335 € pour les contribuables de la <a href="http://www.impotsdirects.public.lu/az/c/class_resid/index.html" class="spip_out" rel="external">classe d’impôt 1</a>, et</li>
<li>à 20 670 € pour les contribuables des <a href="http://www.impotsdirects.public.lu/az/c/class_resid/index.html" class="spip_out" rel="external">classes d’impôt 1a et 2</a>.</li></ul>
<p>Pour les années d’imposition 2002 à 2007 :</p>
<ul class="spip">
<li>à 9 750 € pour les contribuables de la <a href="http://www.impotsdirects.public.lu/az/c/class_resid/index.html" class="spip_out" rel="external">classe d’impôt 1</a>, et</li>
<li>à 19 500 € pour les contribuables des <a href="http://www.impotsdirects.public.lu/az/c/class_resid/index.html" class="spip_out" rel="external">classes d’impôt 1a et 2</a>.</li></ul>
<h4 class="spip">Imposition collective des époux</h4>
<ul class="spip">
<li>Sont imposés collectivement :</li>
<li>les époux qui au début de l’année d’imposition sont contribuables <a href="http://www.impotsdirects.public.lu/az/r/resid_nonresid/index.html" class="spip_out" rel="external">résidents</a> et ne vivent pas en fait séparés en vertu d’une dispense de la loi ou de l’autorité judiciaire ;</li>
<li>les contribuables <a href="http://www.impotsdirects.public.lu/az/r/resid_nonresid/index.html" class="spip_out" rel="external">résidents</a> qui se marient en cours de l’année d’imposition ;</li>
<li>les époux qui deviennent contribuables <a href="http://www.impotsdirects.public.lu/az/r/resid_nonresid/index.html" class="spip_out" rel="external">résidents</a> en cours de l’année d’imposition et qui ne vivent pas en fait séparés en vertu d’une dispense de la loi ou de l’autorité judiciaire ;</li>
<li><strong>sur demande conjointe</strong>, les époux qui ne vivent pas en fait séparés, dont l’un est un contribuable <a href="http://www.impotsdirects.public.lu/az/r/resid_nonresid/index.html" class="spip_out" rel="external">résident</a> et l’autre une personne <a href="http://www.impotsdirects.public.lu/az/r/resid_nonresid/index.html" class="spip_out" rel="external">non résidente</a>, à condition que l’époux résident réalise au Luxembourg au moins 90 % des <a href="http://www.impotsdirects.public.lu/az/r/reven_pro/index.html" class="spip_out" rel="external">revenus professionnels</a> du ménage pendant l’année d’imposition.</li>
<li>Les contribuables <a href="http://www.impotsdirects.public.lu/az/r/resid_nonresid/index.html" class="spip_out" rel="external">non-résidents</a> mariés, ne vivant pas en fait séparés, sont imposés dans la classe d’impôt 2 s’ils sont imposables au Luxembourg de plus de 50% des revenus professionnels de leur ménage. Si les deux époux réalisent des <a href="http://www.impotsdirects.public.lu/az/r/reven_pro/index.html" class="spip_out" rel="external">revenus professionnels</a> imposables au Luxembourg, les conjoints sont imposables collectivement.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Année fiscale</h3>
<p>L’année fiscale au Luxembourg débute le 1er janvier et se termine le 31 décembre.</p>
<h3 class="spip"><a id="sommaire_3"></a>Barème de l’impôt </h3>
<h4 class="spip">L’impôt sur le revenu des personnes physiques et morales</h4>
<p>Le tarif en matière de l’impôt sur le revenu applicable aux personnes physiques est basé sur le principe de l’imposition selon la capacité contributive des contribuables en appliquant peu ou pas de pression fiscale sur les revenus modestes et en instaurant une progression de la charge fiscale moyenne au fur et à mesure que les revenus augmentent, ce qui constitue le garant de l’équité et de l’efficacité.</p>
<p>La dernière adaptation tarifaire est celle en vigueur à partir du 1er janvier 2013.</p>
<h4 class="spip">Calcul de l’impôt</h4>
<p>La tranche exonérée est fixée à 11 264 euros dans le tarif 2013 et le taux marginal maximum d’imposition s’établit à 40%, il s’applique à la tranche de revenus supérieure à 100 000 euros.</p>
<h4 class="spip">Impôts directs et indirects</h4>
<p>En sa séance du 1er février 2013, le Conseil de gouvernement a adopté un <strong>projet de loi visant l’adaptation de certaines dispositions en matière d’impôts indirects. </strong>A l’avenir, le citoyen ne devra plus se déplacer physiquement vers un bureau de l’<a href="http://www.guichet.public.lu/home/fr/index.html" class="spip_out" rel="external">Administration de l’enregistrement</a> pour l’apposition de <strong>timbres mobiles pour l’acquittement de droits, de taxes et de redevances</strong>, comme par exemple pour l’immatriculation d’une voiture, ou bien l’obtention du permis de conduire, ou bien encore l’autorisation de commerce, voire la prolongation du permis de pêche. Un <strong>paiement par voie de simple virement ou versement des droits</strong> sera accepté.</p>
<h4 class="spip">Impôts locaux</h4>
<p>Tout <strong>particulier, propriétaire ou locataire</strong>, habitant une commune du Grand-Duché de Luxembourg, ainsi que toute <strong>entreprise</strong> légalement établie dans une commune luxembourgeoise sont concernés par les impôts locaux et par les taxes communales.</p>
<p>Des personnes résidant en dehors du Grand-Duché peuvent aussi être concernées lorsqu’elles utilisent des services rendus ou des prestations fournies par une commune sur le territoire luxembourgeois, par exemple lorsqu’elles disposent d’une résidence secondaire au Luxembourg et y consomment de l’eau.</p>
<p>Les taxes communales à payer ne tiennent en principe pas compte de la situation personnelle (marié ou non, avec ou sans enfants), du statut légal (particulier, commerçant, profession libérale, société commerciale, association) ou des revenus / de la fortune de l’intéressé. Elles représentent en général la rémunération d’un service rendu ou d’une prestation fournie.</p>
<p>L’<a href="http://www.guichet.public.lu/entreprises/fr/fiscalite/impots-benefices/impots-divers/impot-commercial-communal/index.html" class="spip_out" rel="external">impôt commercial communal</a> (pour les entreprises) et l’<a href="http://www.guichet.public.lu/citoyens/fr/impots-taxes/bien-immobilier/terrain-bien-immobilier/payer-impot-foncier/index.html" class="spip_out" rel="external">impôt foncier</a> (entreprises et particuliers) procurent aux communes 1/3 de leurs recettes destinées à financer les dépenses que ces dernières ont à supporter (écoles, infrastructures routières, conduites d’eau, canalisations, infrastructures sportives et culturelles, etc.). Les communes fixent chaque année le taux communal applicable en matière d’impôt foncier et en matière d’impôt commercial.</p>
<p>Les types et montants des taxes communales prélevées (par exemple pour la fourniture d’eau potable, l’évacuation des eaux usées ou pour la gestion des déchets) dépendent du coût de ces services ou fournitures et <strong>peuvent ainsi varier d’une commune à l’autre</strong>.</p>
<p>Les principales taxes communales sont les suivantes :</p>
<ul class="spip">
<li>taxes de chancellerie (pour la délivrance de certificats de résidence, d’extraits d’actes de l’état civil, la légalisation de signatures, des autorisations diverses etc.) ;</li>
<li>taxes pour la gestion des déchets ;</li>
<li>taxe pour la fourniture d’eau potable ;</li>
<li>taxe pour l’évacuation et l’élimination des eaux usées ;</li>
<li>taxes de raccordement (raccordements aux réseaux d’eau, à la canalisation, etc.) ;</li>
<li>taxes d’électricité et de gaz (pour les communes qui gèrent un tel service) ;</li>
<li>taxe d’octroi de concessions aux cimetières ;</li>
<li><a href="http://www.guichet.public.lu/citoyens/fr/famille/animaux-domestiques/acquisition-chien/identifier-declarer-chien/index.html" class="spip_out" rel="external">taxe sur les chiens</a> ;</li>
<li>taxe sur le stationnement (paiement pour le stationnement sur la voie publique) ;</li>
<li>taxe spécifique sur les immeubles d’habitation inoccupés et certains terrains non-bâtis. Les taxes communales sont payables soit de manière <strong>périodique</strong> (par trimestre, par semestre ou par an) soit de manière <strong>ponctuelle</strong>.</li></ul>
<p>Il est conseillé de consulter le site Internet de la commune concernée ou de contacter l’administration communale afin d’obtenir plus de détails. Les coordonnées des sites et les adresses des administrations communales et de leurs services peuvent être consultées sur le site du <a href="http://www.syvicol.lu/" class="spip_out" rel="external">Syndicat des villes et communes de Luxembourg</a>.</p>
<h4 class="spip">Taxe sur la valeur ajoutée</h4>
<p>Le Grand-Duché de Luxembourg dispose de quatre taux d’application de la taxe sur la valeur ajoutée :</p>
<ul class="spip">
<li>un taux normal de 15% ;</li>
<li>un taux réduit de 6% ;</li>
<li>un taux super-réduit de 3% ;</li>
<li>un taux intermédiaire de 12%.</li></ul>
<p><strong>Pour en savoir plus :</strong></p>
<p><a href="http://www.aed.public.lu/" class="spip_out" rel="external">Le site de l’administration de l’enregistrement des domaines</a> / rubrique TVA.</p>
<h4 class="spip">Taxe sur les véhicules routiers</h4>
<p>Selon la loi du 22 décembre 2006, une nouvelle taxe sur les véhicules routiers est due à partir du 1er janvier 2007 et ce, indépendamment de l’échéance de la vignette fiscale.</p>
<p>Cette nouvelle imposition est basée soit sur les émissions de CO2 pour les véhicules immatriculés pour la première fois après le 1er janvier 2001, soit sur la cylindrée pour ceux immatriculés avant le 1er janvier 2001.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Le site de l’<a href="http://www.do.etat.lu/vehaut/Taxes/index.htm" class="spip_out" rel="external">administration des douanes et accises</a>.</p>
<h3 class="spip"><a id="sommaire_4"></a>Quitus fiscal</h3>
<p>Il n’est pas exigé de quitus fiscal avant de quitter le pays.</p>
<h3 class="spip"><a id="sommaire_5"></a>Solde du compte en fin de séjour</h3>
<p>Au Luxembourg, un expatrié relevant du secteur privé peut solder son compte en fin de séjour moyennant une petite retenue.</p>
<h3 class="spip"><a id="sommaire_6"></a>Pour en savoir plus</h3>
<ul class="spip">
<li><a href="http://www.guichet.public.lu/entreprises/fr/marche-international/index.html" class="spip_out" rel="external">Droits et taxes sur le commerce international</a></li>
<li><a href="http://www.ambafrance-lu.org/" class="spip_out" rel="external">Service économique régional pour la zone Benelux</a><br class="manualbr">Ambassade de France – Mission économique<br class="manualbr">38 rue de la Loi<br class="manualbr">B – 1040 Bruxelles<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/fiscalite/#bruxelles#mc#missioneco.org#" title="bruxelles..åt..missioneco.org" onclick="location.href=mc_lancerlien('bruxelles','missioneco.org'); return false;" class="spip_mail">Courriel</a></li>
<li><a href="http://www.impotsdirects.public.lu/" class="spip_out" rel="external">Administration des contributions directes</a> (Direction)<br class="manualbr">45 boulevard Roosevelt <br class="manualbr">L-2982 Luxembourg<br class="manualbr">Tél. : (+352) 40 800-1 <br class="manualbr">Fax : (+352) 40 800-2022<br class="manualbr">Le standard téléphonique oriente les demandes vers les différentes sections concernées, en fonction du lieu d’imposition.</li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-luxembourg-fiscalite-article-convention-fiscale.md" title="Convention fiscale">Convention fiscale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-luxembourg-fiscalite-article-fiscalite-du-pays.md" title="Fiscalité du pays">Fiscalité du pays</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/fiscalite/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
