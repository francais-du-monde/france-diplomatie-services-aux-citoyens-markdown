# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/paraguay/vie-pratique/article/cout-de-la-vie-113507#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/paraguay/vie-pratique/article/cout-de-la-vie-113507#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/paraguay/vie-pratique/article/cout-de-la-vie-113507#sommaire_3">Alimentation</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/paraguay/vie-pratique/article/cout-de-la-vie-113507#sommaire_4">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>L’unité monétaire est le Guarani (PYG).</p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<p>Les transferts de fonds ne sont soumis à aucun contrôle des changes.</p>
<p>Plusieurs banques étrangères sont implantées au Paraguay : Sudaméris (entité franco-italienne), BBVA, ABN Amro, CityBank, Banco do Brasil, Banco de la Nación Argentina, ITAU.</p>
<p>Les moyens de paiement utilisés localement sont la carte bancaire, le chèque pour les entreprises et le virement swift.</p>
<h3 class="spip"><a id="sommaire_3"></a>Alimentation</h3>
<p>Les produits d’alimentation sont généralement frais et de bonne qualité.</p>
<p>L’approvisionnement ne pose pas de difficulté, sauf cas particulier en raison de la saison ou du coût de certaines denrées (notamment pour les fruits et légumes).</p>
<p><strong>Prix moyen d’un repas dans un restaurant (par personne) </strong></p>
<ul class="spip">
<li>dans un restaurant de première catégorie : 150 000 PYG</li>
<li>dans un restaurant de catégorie moyenne : 100 000 PYG</li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Evolution des prix</h3>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/paraguay/vie-pratique/article/cout-de-la-vie-113507). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
