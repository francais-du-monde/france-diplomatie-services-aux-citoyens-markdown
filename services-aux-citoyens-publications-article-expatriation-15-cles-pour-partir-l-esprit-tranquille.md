# Expatriation : 15 clés pour partir l’esprit tranquille

<p class="spip_document_92333 spip_documents spip_documents_left">
<img src="http://www.diplomatie.gouv.fr/fr/IMG/jpg/expatriation_rs2_cle48b818.jpg" width="720" height="360" alt=""></p>
<p>Mon permis de conduire est-il valable à l’étranger ? Que dois-je faire l’année qui suit mon départ ? Quel sera mon statut à l’étranger ? Quelles solutions pour la garde des enfants ? … l’expatriation ne s’improvise pas !</p>
<p>Le guide "Les 15 clés pour partir l’esprit tranquille" vous permet de retrouver les conseils pratiques et adresses indispensables pour vivre sereinement toutes les étapes de votre expatriation :</p>
<ul class="spip">
<li>Préparer son départ</li>
<li>Vivre à l’étranger</li>
<li>Préparer son retour en France</li>
<li>La check-list de l’expatriation</li></ul>
<iframe src="http://www.diplomatie.gouv.fr/fr/spip.php?page=pdfjs&amp;id_document=91503" width="100%" height="600" class="lecteurpdf lecteufpdf-91503spip_documents"></iframe>
<p>Pour imprimer le document, téléchargez la version PDF :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/guide_expatriation_2016-web_cle0d5a88.pdf" class="spip_in" type="application/pdf">Expatriation : 15 clés pour partir et revenir l’esprit tranquille (PDF - 1.4 Mo)</a></p>
<p><i>Mise à jour : février 2016</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/publications/article/expatriation-15-cles-pour-partir-l-esprit-tranquille). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
