# Vaccination

<p>Consulter notre <a href="http://www.diplomatie.gouv.fr/fr/spip.php?page=article&amp;id_article=117330" class="spip_out">article thématique traitant spécifiquement de la vaccination</a>.</p>
<p>Pour plus d’informations médicales concernant la Colombie, nous vous invitons à consulter le site du <a href="http://www.cimed.org/" class="spip_out" rel="external">CIMED</a> (Comité d’Informations Médicales).</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/entree-et-sejour/article/vaccination). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
