# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/vie-pratique/article/transports#sommaire_1">Importation de véhicule</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/vie-pratique/article/transports#sommaire_2">Permis de conduire</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/vie-pratique/article/transports#sommaire_3">Code de la route</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/vie-pratique/article/transports#sommaire_4">Assurances et taxes</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/vie-pratique/article/transports#sommaire_5">Achat et location</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/vie-pratique/article/transports#sommaire_6">Immatriculation</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/vie-pratique/article/transports#sommaire_7">Entretien</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/vie-pratique/article/transports#sommaire_8">Réseau routier</a></li>
<li><a id="so_9" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/vie-pratique/article/transports#sommaire_9">Transports en commun</a></li></ul>
<p>Des restrictions de circulation sont imposées dans les régions de la Haute-Egypte (voyages en convois) et du Fayoum (escortes policières). La présence de mines dans certaines parties du territoire, régions frontalières notamment, doit inciter à la plus grande vigilance lors de déplacements en véhicule : se renseigner au préalable et éviter de quitter les routes ou les sentiers balisés.</p>
<p>Dans la pratique, les conducteurs sont indisciplinés et ne respectent pas le code de la route. Les véhicules sont souvent mécaniquement défectueux. Il faut donc être très prudent et éviter en particulier de conduire de nuit. En cas d’accident, l’assistance est quasi inexistante. Il faut donc compter sur l’aide éventuelle d’autres usagers.</p>
<h3 class="spip"><a id="sommaire_1"></a>Importation de véhicule</h3>
<p>Il est préférable d’importer son véhicule personnel. L’importation de voitures diesel ou de véhicules de plus de quatre ans est interdite. Les marques Peugeot et Renault sont représentées en Egypte ainsi que les marques allemandes, japonaises et coréennes. Il est possible d’acheter une voiture d’occasion sur place mais les prix sont généralement élevés à cause des taxes.</p>
<p>Un véhicule climatisé est conseillé. Pour se rendre dans le désert ou les oasis, un 4 x 4 est tout indiqué.</p>
<h3 class="spip"><a id="sommaire_2"></a>Permis de conduire</h3>
<p>Le permis de conduire français est reconnu les trois premiers mois. Au-delà, l’expatrié est tenu de se faire délivrer un permis local par le Bureau du trafic central de sa ville de résidence, sur présentation des documents suivants : permis français avec sa traduction en arabe, passeport avec visa long séjour en Egypte, certificats médicaux délivrés par un généraliste et un ophtalmologue locaux, 4 photos d’identité.</p>
<p>Au Caire, le principal bureau du trafic, appelé Bureau Ain El Sira, se situe rue Salah Salam, quartier du Vieux-Caire (téléphone : 02 36 21 278).</p>
<p>A Alexandrie, on s’adressera au Bureau Abiss, quartier de Smouha (téléphone : 03 42 71 899).</p>
<h3 class="spip"><a id="sommaire_3"></a>Code de la route</h3>
<p>La conduite et la priorité sont à droite. La vitesse maximale autorisée est de 60 km/h en ville et de 100 km/h hors agglomération (routes et autoroutes). La vitesse et le port de la ceinture, obligatoire à l’avant, sont souvent contrôlés et sanctionnés.</p>
<h3 class="spip"><a id="sommaire_4"></a>Assurances et taxes</h3>
<p>L’assurance au tiers, obligatoire, est incluse dans la carte grise (coût du renouvellement : 1500 à 2000 EGP selon la puissance du véhicule). Il est conseillé de souscrire une assurance complémentaire sur place.</p>
<h3 class="spip"><a id="sommaire_5"></a>Achat et location</h3>
<p>Le coût d’une location varie selon les agences (environ 70 euros par jour auquel s’ajoute un montant variable par km au-dessus de 200 km). Une caution est à verser.</p>
<h3 class="spip"><a id="sommaire_6"></a>Immatriculation</h3>
<p>Le contrôle technique a lieu lors du renouvellement - annuel - de la carte grise (<i>Khoxa</i>).</p>
<h3 class="spip"><a id="sommaire_7"></a>Entretien</h3>
<p>Il est possible d’entretenir et de faire réparer son véhicule sur place. Les travaux de carrosserie sont peu chers ; les réparations mécaniques sont plus chères qu’en France en raison des taxes sur les pièces détachées.</p>
<h3 class="spip"><a id="sommaire_8"></a>Réseau routier</h3>
<p>Le réseau routier est convenable sur les grands axes mais de médiocre qualité sur les voies secondaires. La signalisation reste généralement insuffisante. Les postes d’essence sont moins nombreux qu’en Europe et il est difficile de trouver des cartes routières fiables.</p>
<h3 class="spip"><a id="sommaire_9"></a>Transports en commun</h3>
<p>Le réseau ferroviaire égyptien est ancien, surchargé et mal entretenu. Des accidents graves ne peuvent être exclus. La ligne Le Caire-Alexandrie est correcte (2 heures de trajet). Pour des raisons de sécurité, il est déconseillé d’utiliser les trains à destination de la Haute-Egypte et de privilégier les transports aériens.</p>
<p>La compagnie Egypt Air assure des vols intérieurs. Leur ponctualité est variable. Les avions sont plutôt récents.</p>
<p>Il existe des liaisons maritimes entre les principaux ports de la mer Rouge. La ligne Safaga-Djeddah, empruntée notamment par les pèlerins de La Mecque, a connu plusieurs naufrages.</p>
<p>Au Caire, deux lignes de métro modernes et de bonne qualité desservent notamment les quartiers de Maadi, Héliopolis, Dokki et Guizeh. Le plan de chaque ligne est consultable sur le site Internet de la <a href="http://www.nat.org.eg/" class="spip_out" rel="external">National authority for tunnels</a>. Il convient d’éviter de prendre le métro aux heures de pointe (7h00-9h00 et 14h00-17h00). Le prix du ticket est de 0,10 euros. Les deux voitures de tête sont réservées aux femmes.</p>
<p>Les taxis sont peu chers. C’est le moyen le plus pratique de se déplacer en ville et dans les environs. Les "taxis jaunes", confortables, climatisés et équipés d’un compteur fiable sont à recommander. Les bus sont peu utilisés par les expatriés.</p>
<p><i>Mise à jour : avril 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/egypte/vie-pratique/article/transports). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
