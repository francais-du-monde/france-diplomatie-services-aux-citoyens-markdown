# Fiscalité

<h2 class="rub23550">Fiscalité du pays</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/fiscalite/#sommaire_1">Année fiscale</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/fiscalite/#sommaire_2">Inscription auprès du fisc</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/fiscalite/#sommaire_3">Impôts directs et indirects</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/fiscalite/#sommaire_4">Quitus fiscal</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/fiscalite/#sommaire_5">Solde du compte en fin de séjour</a></li></ul>
<p>Chaque personne, résidant ou non en Thaïlande, qui touche un revenu imposable par l’intermédiaire d’un emploi ou d’affaires traitées en Thaïlande est soumise à l’impôt sur le revenu des personnes physiques, que le revenu soit payé en Thaïlande ou à l’étranger. Des exceptions sont accordées à certaines personnes comme les membres des Nations Unies, les diplomates et certains experts, sous les termes d’accords internationaux et bilatéraux.</p>
<p>Une personne présente en Thaïlande plus de 183 jours de n’importe quelle année fiscale est considérée comme résident en Thaïlande en ce qui concerne la fiscalité. Les résidents doivent aussi payer un impôt sur les revenus qui proviennent de l’étranger à partir du moment où ces revenus sont touchés en Thaïlande lors de la même année fiscale que celles où ils sont résidents.</p>
<p>En fait, les devises étrangères touchées par un résident (qu’elles proviennent ou non d’une relation de travail ou d’affaires en Thaïlande), et apportées en Thaïlande doivent être vendues à ou déposées dans une banque commerciale dans les 15 jours, à moins d’une permission spéciale.</p>
<p>Tout apport de devise étrangère en Thaïlande de plus de 20,000 USD doit faire l’objet d’une déclaration auprès de la douane et/ou banque centrale.</p>
<p><strong>Les revenus de dividendes</strong></p>
<p>Les revenus de dividendes sont généralement taxables à titre d’IR. Le contribuable peut choisir d’avoir une retenue d’impôt à la source au taux de 10% sur le revenu des dividendes, et il n’inclura pas alors ces revenus de dividendes dans le calcul de ses revenus globaux à la fin de l’année fiscale</p>
<p>Le contribuable peut réclamer 3/7e d’acompte sur les dividendes reçus lorsqu’il incorpore de tels dividendes dans son revenu imposable à la fin de l’année fiscale.</p>
<h3 class="spip"><a id="sommaire_1"></a>Année fiscale</h3>
<p>La déclaration doit être remplie chaque année avant la fin du mois de mars de l’année suivant celle où les revenus sont touchés.</p>
<h3 class="spip"><a id="sommaire_2"></a>Inscription auprès du fisc</h3>
<p>Coordonnées des centres d’information fiscale</p>
<p>Site internet du <a href="http://www.rd.go.th/" class="spip_out" rel="external">Revenue Department of Thailand</a> (en thaï et en anglais) :  <br class="manualbr">90 Soi Phaholyothin7, Phaholyothin Road, Bangkok 10400<br class="manualbr">Tel. : (662) 272-8000</p>
<h3 class="spip"><a id="sommaire_3"></a>Impôts directs et indirects</h3>
<p><strong>Les taux sur l’impôt sur le revenu</strong></p>
<p>Les taux sont les suivants selon le montant des revenus nets :</p>
<ul class="spip">
<li>Inférieurs à 150 000 Bahts : 0 %</li>
<li>De 150 001 à 500 000 Bahts : 10%</li>
<li>De 500 001 à 1 million Bahts : 20%</li>
<li>De 1 à 4 millions Bahts : 30%</li>
<li>Supérieur à 4 millions Bahts : 37%</li></ul>
<p><strong>Calcul des revenus</strong></p>
<p>Les revenus que l’on prend en compte sont les paiements pour services et tout autre gain, propriété ou bénéfice provenant d’un service rendu ou d’un emploi. Sont aussi inclus les dividendes, intérêts et autres royalties ou honoraires pour assistance technique. Toucher de l’argent est considéré comme un revenu normal sauf dans le cas d’une vente de meubles corporels acquis sans intention d’en faire des bénéfices.</p>
<p>Notons que l’impôt sur le revenu payé et absorbé par l’employeur (celui-ci verse en effet un salaire net au salarié), le reste est considéré comme un revenu imposable pour l’employeur, ce qui conduit à un effet de <strong>pyramide d’impôt</strong>. Il faut en effet inclure dans le revenu imposable toutes les indemnités, la prime au logement, les frais de scolarité payés par l’employeur, les allocations de voyage pour le congé annuel et la valeur monétaire de n’importe quel autre avantage accordé par l’employeur.</p>
<p>On ne prend, cependant, pas en compte dans le calcul du revenu brut des revenus tels que les frais de déplacement professionnels, certaines dépenses professionnelles, les intérêts sur l’épargne des banques en Thaïlande, les intérêts sur des obligations du gouvernement thaï, les primes d’assurance, les héritages et les bourses d’étude.</p>
<p><strong>Les déductions</strong></p>
<p>Plusieurs sortes de déductions sont permises par le <i>Revenue Code</i> comme les abattements pour charges de famille, les primes d’assurance vie, les dividendes, les caisses de prévoyance ou de retraite, des services d’intérêts et donations charitables.</p>
<p>Une retenue à la source est faite par l’employeur au moment du versement des salaires. Cette retenue s’impute sur l’IR au titre de l’année au cours de laquelle elle a été opérée. Pour les revenus salariaux, une déduction de 40% est autorisée sans excéder les 60 000 Bahts. Le calcul de la retenue se fait de la manière suivante :</p>
<ul class="spip">
<li>multiplier le salaire mensuel par 12 (pour obtenir le salaire annuel)</li>
<li>déduire certains abattements afin d’obtenir le montant net des sommes payées à titre de traitements et salaires ;</li>
<li>appliquer aux revenus nets salariaux les taux d’imposition des revenus des personnes physiques mentionnées ci-dessus, vous obtiendrez ainsi le montant annuel de la retenue à effectuer ;</li>
<li>pour obtenir le montant mensuel de retenue, diviser cette somme par 12.</li></ul>
<p>Cette déduction de 40% (limitée à 60 000 Bahts) est aussi valable pour les revenus provenant de services rendus ou sur des droits d’auteur.</p>
<p>Des déductions de 10 à 85% sont aussi permises sur les autres catégories de revenus, mais généralement le contribuable peut choisir de détailler ses dépenses au lieu de calculer des déductions standard sur le revenu de certaines sources spécifiées par la loi.</p>
<p>Les déductions pour charges de famille annuelles suivantes sont autorisées :</p>
<ul class="spip">
<li>Pour le contribuable : 30 000 Bahts</li>
<li>Pour le conjoint du contribuable : 30 000 Bahts</li>
<li>Pour chaque enfant : 15 000 Bahts</li>
<li>Pour chaque enfant scolarisé (en Thaïlande) : 2 000 Bahts</li>
<li>Pour le contribuable et son conjoint en ce qui concerne la prime d’ assurance-vie : 10 000 Bahts ou le paiement effectif si inférieur</li>
<li>Pour le contribuable et son conjoint en ce qui concerne les contributions à une Caisse de Prévoyance : 10 000 Bahts ou la contribution effective si inférieure</li>
<li>Pour le contribuable et son conjoint en ce qui concerne les prêts pour l’achat, les contrats de location-vente ou la construction d’immeubles résidentiels : 10000 Bahts ou le paiement effectif si inférieur</li>
<li>Pour le contribuable et son conjoint en ce qui concerne les contributions au Fonds de Sécurité Sociale : Contribution actuelle</li></ul>
<p>Les enfants du contribuable ou de son conjoint sont acceptés pour l’allocation familiale. Les enfants comptabilisés pour l’allocation familiale ne doivent cependant pas être plus de trois. Cette limite ne s’applique qu’aux enfants nés après 1979. Cependant, pour déterminer le nombre d’enfants ayant droit aux allocations familiales, on peut aussi prendre en compte un enfant né avant 1979, puisque le contribuable ayant quatre enfants nés en ou avant 1979 continue de bénéficier d’un abattement fiscal de 60 000 Bahts. Par contre un 5ème enfant né après 1979 n’est pas pris en compte.</p>
<p>Il n’y a pas de réduction de taux d’impôt pour des déclarations remplies par à la fois le mari et la femme. Une déclaration remplie ne prendra en compte que les revenus du mari. Des déclarations séparées, cependant, peuvent être remplies dans le cas de salaires mais des règles spéciales s’appliquent sur l’attribution d’allocations et de déductions des revenus entre conjoints.</p>
<p>Des réductions pour le conjoint et les enfants d’un non-résident sont autorisées à condition qu’ils résident en fait en Thaïlande.</p>
<p><strong>Fiscalité des retraités</strong></p>
<p>Les retraités doivent également payer des impôts sur les revenus générés en Thaïlande ou qui proviennent de l’étranger à partir du moment où ces revenus sont touchés en Thaïlande lors de la même année fiscale que celles où ils sont résidents.</p>
<h3 class="spip"><a id="sommaire_4"></a>Quitus fiscal</h3>
<p>Aucun quitus fiscal n’est exigé avant de quitter le pays.</p>
<h3 class="spip"><a id="sommaire_5"></a>Solde du compte en fin de séjour</h3>
<p>Un expatrié français relevant du secteur privé peut solder son compte en fin de séjour.</p>
<p><strong>Pour en savoir plus</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="https://www.tresor.economie.gouv.fr/pays/thailande" class="spip_out" rel="external">Service économique français en Thaïlande</a></p>
<p><i>Mise à jour : juillet 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-thailande-fiscalite-article-convention-fiscale.md" title="Convention fiscale">Convention fiscale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-thailande-fiscalite-article-fiscalite-du-pays-114437.md" title="Fiscalité du pays">Fiscalité du pays</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/fiscalite/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
