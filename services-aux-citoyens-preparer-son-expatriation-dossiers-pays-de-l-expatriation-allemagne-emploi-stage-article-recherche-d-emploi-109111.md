# Recherche d’emploi

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/emploi-stage/article/recherche-d-emploi-109111#sommaire_1">Outils pour la recherche d’emploi</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/emploi-stage/article/recherche-d-emploi-109111#sommaire_2">Organismes pour la recherche d’emploi</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Outils pour la recherche d’emploi</h3>
<h4 class="spip">Journaux</h4>
<p>De nombreuses offres d’emploi sont insérées dans la presse allemande, nationale, régionale et locale. Les quotidiens y consacrent en général un cahier spécial dans leur édition du samedi. Vous pouvez également consulter ces annonces sur Internet :</p>
<ul class="spip">
<li><a href="http://fazjob.net/" class="spip_out" rel="external">Frankfurter Allgemeine Zeitung</a></li>
<li><a href="http://www.fr-online.de/" class="spip_out" rel="external">Frankfurter Rundschau</a></li>
<li><a href="http://www.sueddeutsche.com/" class="spip_out" rel="external">Süddeutsche Zeitung</a> rubrique "Anzeigen"</li>
<li><a href="http://www.handelsblatt.com/" class="spip_out" rel="external">Das Handelsblatt</a> rubrique "Jobturbo"</li>
<li><a href="http://www.zeit.de/jobs/index" class="spip_out" rel="external">Die Zeit</a></li></ul>
<p>L’<i>Agentur für Arbeit</i> publie également un journal d’offres d’emploi <i>Markt und Chance</i> qui paraît tous les vendredis. On peut le consulter dans toutes les agences de l’<i>Agentur für Arbeitt</i>, ainsi que dans les agences Pôle emploi frontalières.</p>
<p>Il est également possible de contacter des agences de travail intérimaire (<i>Arbeitnehmerüberlassung</i>, <i>Personaldienstleistungen</i>, <i>Zeitarbeit</i>), comme Randstat, Manpower, ADECCO, ainsi que les cabinets de recrutement. Vous trouverez une liste complète d’adresses dans les <a href="http://www.gelbeseiten.de/yp/start.yp" class="spip_out" rel="external">pages jaunes</a> de l’annuaire téléphonique aux rubriques "<i>Zeitarbeit</i>" et " <i>Personalberatung</i> ".</p>
<h4 class="spip">Sites internet</h4>
<h5 class="spip">Des guides sur Internet (à télécharger au format pdf) </h5>
<ul class="spip">
<li>" <i>Se former et travailler en Allemagne</i> ", édité par Athena (Comité consulaire pour l’emploi et la formation professionnelle de Francfort). Ce guide est téléchargeable sur le site <a href="http://www.emploi-allemagne.de/" class="spip_out" rel="external">http://www.emploi-allemagne.de/</a></li>
<li><i>Guide "vivre et travailler en Allemagne</i> (éditions Lextenso) et le site <a href="http://www.travailler-en-allemagne.com/" class="spip_out" rel="external">Travailler en Allemagne</a></li>
<li><i>La recherche d’emploi et de stage en Allemagne - Conseils et adresses</i> ". Cette brochure est éditée par le <a href="http://www.cidal.diplo.de" class="spip_out" rel="external">centre d’information et de documentation de l’ambassade d’Allemagne en France</a> (CIDAL) et est téléchargeable dans la rubrique <i>Publications</i>.</li></ul>
<h5 class="spip">Des sites spécialisés </h5>
<ul class="spip">
<li><a href="http://www.bza.de" class="spip_out" rel="external">Association fédérale des services de personnel temporaire</a> (Bundesverband Zeitarbeit - Personal - Dienstleistungen e.V.) ;</li>
<li><a href="http://www.afasp.net" class="spip_out" rel="external">Association franco-allemande des stagiaires professionnels</a> ;</li>
<li><a href="http://www.forum-franco-allemand.org" class="spip_out" rel="external">Forum franco-allemand</a></li></ul>
<p>Pour les jeunes désireux de travailler en Allemagne et de connaître les entreprises qui recrutent, l’équipe du Forum franco-allemand, qui se déroulera désormais chaque année à Strasbourg, met son catalogue d’entreprises allemandes et françaises à la disposition du public.</p>
<h5 class="spip">Des sites de recherche d’emploi </h5>
<ul class="spip">
<li>Sites emplois généralistes</li>
<li>Sites emplois spécialisés</li>
<li>Sites emplois français</li>
<li>Pôle emploi International, APEC, Eures,</li>
<li>Les réseaux sociaux professionnels</li>
<li>Les cabinets de recrutement et agences d’intérim</li>
<li>Candidatures spontanées sur les sites des entreprises</li>
<li><a href="http://www.meinestadt.de" class="spip_out" rel="external">Annonces dans plus de 12 000 villes allemandes</a></li></ul>
<h4 class="spip">Réseaux</h4>
<p>Les comités pour l´emploi et la formation professionnelle de Düsseldorf, Munich et Berlin ont cessé leur activité en 2009.</p>
<p><a href="http://www.emploi-allemagne.de" class="spip_out" rel="external">Emploi-Allemagne</a> regroupe désormais sur une même plateforme les réseaux et savoir-faire des « Missions Emploi » des consulats français, créées à l‘initiative du ministère des Affaires étrangères français en 1994.</p>
<p>Le site est géré par Athena e.V, association reconnue d’utilité publique, actuellement dirigée par un comité composé de représentants des milieux de l’industrie et de la formation. Elle s’appuie sur les réseaux consulaires et les institutions d’aide à l’emploi européen et allemand. Sa mission principale consiste à favoriser l’insertion professionnelle des Français résidant en Allemagne et de ceux désirant s’y installer. Elle est partenaire des entreprises et institutions locales pour faciliter la recherche de personnel francophone qualifié.</p>
<p>Vous trouverez sur leurs sites toutes les coordonnées et modalités d´inscription, des pages pratiques concernant votre candidature, des exemples de CV, des tests de niveau d´allemand et des contacts utiles pour la vie pratique sur place.</p>
<p>Contact à Francfort :</p>
<p>Catherine ZAVARD<br class="manualbr">Athena<br class="manualbr">Consulat général de France<br class="manualbr">Zeppelinallee 35<br class="manualbr">60325 Frankfurt am Main<br class="manualbr">Tél. : 069 / 79 50 96-21<br class="manualbr">Fax : 069 / 79 50 96 47 <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/emploi-stage/article/recherche-d-emploi-109111#info#mc#emploi-allemagne.de#" title="info..åt..emploi-allemagne.de" onclick="location.href=mc_lancerlien('info','emploi-allemagne.de'); return false;" class="spip_mail">Courriel</a></p>
<h4 class="spip">Les salons et forums pour l’emploi</h4>
<p>N’hésitez pas à fréquenter les salons professionnels qui vous donnent l’occasion de prendre contact directement avec les responsables des entreprises susceptibles de vous embaucher.</p>
<p>De nombreux salons professionnels et forums pour l’emploi ont lieu en Allemagne. Vous pouvez obtenir des informations complètes à ce sujet en vous adressant au Centre de documentation de l’ambassade d’Allemagne en France (CIDAL). Vous trouverez une description des sites consacrés à ces forums sur le site de <a href="http://www.paris.diplo.de" class="spip_out" rel="external">l’ambassade d’Allemagne en France</a> / rubrique "économie, sciences et technologie / foires et salons ".</p>
<h4 class="spip">Annuaires</h4>
<ul class="spip">
<li>Les IHK, Industrie und Handelskammer, chambres de commerce et d´industrie allemandes disposent de bases de données d´entreprises.
L´accès à un extrait de base de données est généralement payant et réservé aux membres de l´IHK, les conditions d´accès et les tarifs variant selon l´IHK.
Néanmoins quelques sélections d´entreprises peuvent se faire à partir de leurs sites internet ou sur place, dans leur service d´accueil au public contre une somme modique ou simplement le prix des photocopies. Dans certaines IHK vous pourrez ainsi obtenir une liste de certaines grandes entreprises de leur circonscription ayant une filiale en France (Firmen aus dem Bezirk der IHK … mit Niederlassung in Frankreich) ou ayant des relations commerciales particulièrement développées avec la France (Firmen, die mit Frankreich handeln).</li>
<li>Le service économique de Düsseldorf édite une liste des représentations françaises en Allemagne. Cette liste est payante et un extrait par land peut être obtenu sur demande à <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/emploi-stage/article/recherche-d-emploi-109111#dusseldorf#mc#missioneco.org#" title="dusseldorf..åt..missioneco.org" onclick="location.href=mc_lancerlien('dusseldorf','missioneco.org'); return false;" class="spip_mail">dusseldorf<span class="spancrypt"> [at] </span>missioneco.org</a>.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Organismes pour la recherche d’emploi</h3>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.emploi-allemagne.de" class="spip_out" rel="external">Emploi-Allemagne</a></p>
<h4 class="spip">Le service de la Bundesagentur für Arbeit</h4>
<p>Equivalent du Pôle emploi et de l’Assedic en France, cet organisme fournit plus ou moins les mêmes prestations. On trouve des agences locales pratiquement dans toutes les villes moyennes allemandes.</p>
<p>La Bundesagentur für Arbeit possède une bourse du travail informatisée, le système Stellen-Information-Service (SIS), mise à disposition du grand public dans toutes les agences. Cet outil vous permet d’accéder aux offres d’emploi proposées par l’Arbeitsamt dans un rayon de 60 km autour de l’agence ou, pour les diplômés de l’université ou d’une école technique supérieure, au niveau fédéral. Vous pouvez également accéder aux offres d’emploi de l’Arbeitsamt en consultant son site Internet. Vous y trouverez également les sites Internet les plus importants pour la recherche d’emploi : <a href="http://www.arbeitsagentur.de" class="spip_out" rel="external">www.arbeitsagentur.de</a>.</p>
<h4 class="spip">La Zentrale Auslands- und Fachvermittlung (ZAV)</h4>
<p>Cet organisme, à compétence fédérale, aide les candidats étrangers désireux de travailler en Allemagne dans leur recherche d’emploi et les Allemands candidats à l’expatriation. Ses services sont gratuits et vous pouvez en bénéficier directement à partir de la France.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.arbeitsagentur.de" class="spip_out" rel="external">Zentralstelle für Auslands- und Fachvermittlung</a> (ZAV)<br class="manualbr">Internationale Stellenvermittlung<br class="manualbr">Villemombler Strasse 76 - 53123 Bonn<br class="manualbr">Téléphone : (0049) 228 713 13 13 <br class="manualbr">Télécopie : (0049) 228 / 713 270 14 00<br class="manualbr">Courriel : zav-bonn<span class="spancrypt"> [at] </span>arbeitsagentur.de </p>
<h4 class="spip">La Chambre de commerce française en Allemagne (CCFA)</h4>
<p>Vous pourrez obtenir auprès d’elle le répertoire des implantations françaises en Allemagne. Par ailleurs, la CCFA possède un site <a href="http://www.strategie-action.de" class="spip_out" rel="external">Stratégie &amp; Action</a> (rubrique "jobs &amp; carrières) qui recense des candidatures bilingues et qui met les CV des candidats à la disposition des entreprises qui peuvent les consulter.</p>
<p><a href="http://www.ccfa.de" class="spip_out" rel="external">Chambre de commerce française en Allemagne</a> (CCFA)<br class="manualbr">Lebacherstrasse 4 - 66113 Saarbrücken<br class="manualbr">Téléphone : (0049) 681/99 630<br class="manualbr">Télécopie : (0049) 681/99 63 111<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/emploi-stage/article/recherche-d-emploi-109111#info#mc#ccfa-saa.de#" title="info..åt..ccfa-saa.de" onclick="location.href=mc_lancerlien('info','ccfa-saa.de'); return false;" class="spip_mail">Courriel</a></p>
<p><i>Mise à jour : juillet 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/emploi-stage/article/recherche-d-emploi-109111). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
