# Publications

<p>Dans cette rubrique,  retrouvez les publications relatives à l’expatriation et aux services consulaires !</p>
<p>Vous trouverez dans ces publications les coordonnées des organismes compétents pour vous conseiller ainsi que toutes les étapes à effectuer afin de préparer votre <strong>départ </strong> à l’étranger, votre <strong>installation </strong> et votre éventuel <strong>retour</strong>.</p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-publications-article-que-font-les-consulats-pour-vous.md" title="Que font les consulats pour vous ?">Que font les consulats pour vous ?</a></li>
<li><a href="services-aux-citoyens-publications-article-guide-action-consulaire.md" title="Guide « L’action consulaire »">Guide « L’action consulaire »</a></li>
<li><a href="services-aux-citoyens-publications-article-expatriation-la-check-list.md" title="Expatriation : la Check-list">Expatriation : la Check-list</a></li>
<li><a href="services-aux-citoyens-publications-article-expatriation-15-cles-pour-partir-l-esprit-tranquille.md" title="Expatriation : 15 clés pour partir l’esprit tranquille">Expatriation : 15 clés pour partir l’esprit tranquille</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/publications/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
