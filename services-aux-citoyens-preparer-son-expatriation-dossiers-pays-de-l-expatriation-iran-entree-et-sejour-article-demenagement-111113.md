# Déménagement

<p>Le déménagement peut s’effectuer par la route ou par bateau si le déménagement est important. L’avion peut être plus intéressant pour des délais réduits, en cas de petit déménagement.</p>
<p>Dans tous les cas, il faut impérativement être titulaire de sa carte de résident au moment du dédouanement des marchandises en Iran. Pour éviter des frais de stockage, il convient donc de ne pas déménager ses effets personnels trop longtemps à l’avance. Attention, toute importation d’alcool est formellement interdite.</p>
<p>S’agissant d’un déménagement international, il est primordial de s’entourer d’un maximum de garanties en faisant appel à un professionnel disposant de certifications reconnues.</p>
<p><a href="http://www.csdemenagement.fr/" class="spip_out" rel="external">Chambre syndicale du déménagement</a><br class="manualbr">Tél. : 01 49 88 61 40 <br class="manualbr">Télécopie : 01 49 88 61 46<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/entree-et-sejour/article/demenagement-111113#contact#mc#csdemenagement.fr#" title="contact..åt..csdemenagement.fr" onclick="location.href=mc_lancerlien('contact','csdemenagement.fr'); return false;" class="spip_mail">Courriel</a></p>
<p><i>Mise à jour : février 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/entree-et-sejour/article/demenagement-111113). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
