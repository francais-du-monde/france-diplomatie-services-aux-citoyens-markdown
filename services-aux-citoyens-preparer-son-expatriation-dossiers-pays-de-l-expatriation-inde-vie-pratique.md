# Vie pratique

<h2 class="rub22888">Logement</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/vie-pratique/#sommaire_1">Conditions de location</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/vie-pratique/#sommaire_2">Hôtels</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/vie-pratique/#sommaire_3">Electricité</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/vie-pratique/#sommaire_4">Electroménager</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Conditions de location</h3>
<h4 class="spip">New-Delhi</h4>
<p>Il existe un marché locatif à Delhi. Cependant, le marché des logements susceptibles de convenir à des Européens est étroit et onéreux par rapport à la qualité proposée.</p>
<p>Il est utile, ne serait-ce que pour avoir une perception du marché immobilier, de s’adresser à des agents immobiliers qui ont une bonne connaissance du marché locatif local. Une liste d’agents sérieux peut être communiquée par le Consulat. L’agent demande en général un mois de loyer à titre de commission à répartir entre le propriétaire et le locataire (commission contractuelle à négocier au départ).</p>
<p>Les quartiers résidentiels sont très dispersés : Vasant Vihar, West End et Shanti Niketan au sud-ouest ; Golf Links, Jor Bagh, Sunder Nagar, Nizamuddin East au centre ; Defence Colony, Anand Lok, Gulmohar Park, Hauz Khas ; New Friends Colony et Maharani Bagh à l’est sont plus distants. Vastes <i>farm houses</i> encore plus loin.</p>
<p>Il y a deux principaux types de logements :</p>
<p><strong>Les appartements</strong>, souvent spacieux, d’architecture ancienne dans les quartiers créés par les Anglais. Ils peuvent être situés au rez-de-chaussée, avec un petit jardin. Ils sont souvent dans ce cas mieux isolés de la chaleur, aérés et verts, mais aussi plus sombres. Ils peuvent également être situés en étage, avec balcon et/ou terrasse. Ils sont dans ce cas plus chauds. Les <i>barsatis</i> (dernier étage en terrasse), très agréables en hiver, deviennent quasiment inhabitables en saison chaude (cinq mois par an). L’état des sanitaires et de la cuisine laisse souvent à désirer dans les logements anciens.</p>
<p><strong>Les maisons indépendantes</strong> bénéficiant le plus souvent d’un jardin, sont beaucoup plus chères. Les villas permettent également d’éviter les problèmes de cohabitation. Elles nécessitent cependant impérativement un gardien.</p>
<p>Il n’est pas plus difficile de trouver une villa qu’un appartement. Pour une famille disposant d’un budget moyen, il est actuellement plus facile de trouver un appartement à prix abordable qu’une villa. On peut aussi trouver des logements meublés mais la qualité des meubles est en règle générale médiocre.</p>
<p>Le délai moyen de recherche pour trouver un logement se situe entre deux et trois semaines, après de multiples et ardues négociations. Il est à noter que de nombreuses familles choisissent de déménager au bout d’un an de séjour, après avoir trouvé une maison moins chère et/ou plus spacieuse ou du fait que le propriétaire exige arbitrairement une augmentation de loyer exorbitante (parfois de 20 à 60 % d’augmentation du loyer).</p>
<p>Une caution ou dépôt de garantie est systématiquement demandé par les propriétaires : de deux à trois mois de loyers mais il conviendrait de négocier ce dépôt à un mois de loyer, tout en sachant que les bailleurs sont souvent réticents à restituer ce dépôt de garantie.</p>
<p>Les baux sont en général de deux ans. Ce n’est pas toujours obligatoire, mais il est recommandé de procéder à un état des lieux contradictoire avec le propriétaire. Trois, six mois, voire un an ou plus de loyers d’avance sont souvent exigés. Cette clause du contrat peut parfois être négociée, surtout pour les loyers élevés. Les exigences des propriétaires sont parfois démesurées sur ce point.</p>
<p>Il est impossible juridiquement pour un étranger d’acheter un bien immobilier en Inde. De plus, les prix sont exorbitants.</p>
<p><strong>Montant moyen des locations</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td></td>
<td>Studio</td>
<td>3 pièces</td>
<td>5 pièces</td>
<td>villa</td></tr>
<tr class="row_even even">
<td>Quartier résidentiel</td>
<td>—</td>
<td>1800 €</td>
<td>2500 €</td>
<td>5000 €</td></tr>
<tr class="row_odd odd">
<td>Banlieue</td>
<td>—</td>
<td>1500 €</td>
<td>2000 €</td>
<td>4000 €</td></tr>
</tbody>
</table>
<h4 class="spip">Bombay</h4>
<p>Les prix des loyers à Bombay sont extrêmement élevés, atteignant des niveaux aussi élevés qu’à Tokyo. Il faut payer une voire même deux années d’avance, sauf pour des séjours de courte durée chez l’habitant. C’est pourquoi les loyers sont souvent pris en charge par l’employeur.</p>
<p>Les quartiers les mieux situés sont ceux de Malabar Hill, Breach Candy, Kemps Corner, Colaba, Cuffe Parade pour le Sud et Bandra, Juhu, Powai pour le Nord.</p>
<p>Le délai de recherche est d’environ trois mois. Les logements sont loués vides ou meublés. On ne trouve pas de villa. Les baux sont conclus pour un à trois ans. La commission d’agence représente en général deux mois de loyer par année de location.</p>
<p><strong>Montant moyen des locations</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td></td>
<td>Studio</td>
<td>3 pièces</td>
<td>5 pièces</td></tr>
<tr class="row_even even">
<td>Quartier résidentiel</td>
<td>1000 à 2000 €</td>
<td>3000 à 6000 €</td>
<td>6000 à 15 000 €</td></tr>
</tbody>
</table>
<h4 class="spip">Calcutta</h4>
<p>Les prix de location des appartements à Calcutta ont beaucoup augmenté depuis 2008, période à laquelle de nombreux expatriés s’y sont installés. Il convient dans la mesure du possible de prendre ses distances vis-à-vis des agents mobiliers qui multiplient considérablement le prix dès qu’ils voient un occidental. Il vaut mieux faire jouer les relations de travail ou amicales.</p>
<p>Les quartiers résidentiels se situent principalement dans le sud de la ville (Alipore, New Alipore, Ballygunge) mais d’autres quartiers, plus typiques et moins excentrés offrent une qualité de vie égale voire supérieure.</p>
<p>Les appartements, souvent spacieux mais d’architecture simple (blocs de béton à plusieurs niveaux) représentent la majorité des habitations envisageables à Calcutta. Dans les cuisines et les sanitaires l’aménagement de base est souvent insuffisant : il est alors nécessaire de procéder à quelques travaux de rénovation (peintures et pose de carrelage en particulier) lors d’un emménagement. A négocier avec le propriétaire.</p>
<p>Les maisons isolées bénéficient le plus souvent d’un jardin : plus spacieuses que les appartements, elles sont aussi plus chères et très difficiles à trouver.</p>
<p>Le délai moyen de recherche est d’environ de deux à six semaines. Une caution ou dépôt de garantie peut être demandée (de un à six mois de loyers) et, théoriquement, récupérée en fin de séjour. La durée des baux est de deux à trois ans. L’établissement d’un état des lieux est recommandé. Il peut être demandé un an de loyer d’avance, à négocier.</p>
<p><strong>Pondichéry</strong></p>
<p>En l’absence de marché locatif structuré (pas d’agences immobilières), les locations se trouvent par relations. Les quartiers considérés comme résidentiels se situent à l’est du canal et dans la ville dite "blanche". Les logements n’y sont pas plus confortables, tout au plus un peu moins bruyants mais à peine plus éloignés des quartiers commerçants.</p>
<p>La quête d’un logement doit se faire sans idées préconçues. Le plus difficile n’est pas de trouver villa ou appartement, mais un logement qui corresponde aux normes de confort occidental. Le délai moyen de recherche peut être de 15 jours à deux mois. En l’absence de réglementation, la durée des baux est variable. Le propriétaire demande souvent une avance pouvant aller jusqu’à six mois de loyer. Un état des lieux doit être établi de façon précise afin d’éviter toute contestation lors du départ. De nombreux travaux sont souvent nécessaires et doivent être renouvelés au cours du séjour, car la dégradation des immeubles est constante.</p>
<p>Un étranger ne peut acheter un logement sans l’aval de la Reserve Bank à Bombay, procédure longue et coûteuse, à éviter de toute manière en l’absence d’une Conservation des Hypothèques à Pondichéry et de tout système d’information fiable sur le propriétaire d’un bien immobilier.</p>
<p><strong>Montant moyen des locations</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td></td>
<td>3 pièces</td>
<td>5 pièces</td>
<td>villa</td></tr>
<tr class="row_even even">
<td>Quartier résidentiel</td>
<td>300 euros</td>
<td>700 euros</td>
<td>A partir de 500 euros</td></tr>
</tbody>
</table>
<h4 class="spip">Bangalore</h4>
<p>Il existe dans la ville de Bangalore un marché locatif important qui offre un choix d’appartements pour toutes les bourses. Pour les loyers les moins élevés, il faut accepter une localisation décentrée et proche de la périphérie d’une ville de 8 millions d’habitants dont les routes sont très vite encombrées conséquence d’une forte densité de population et d’un réseau routier déficient.</p>
<p>Cependant, la très grande majorité des Français sont logés dans les quartiers résidentiels (Cooke Town, Benson Town, Frazer Town) dans de bonnes conditions. La dépense locative pour cette catégorie se situe dans les prix indiqués ci-dessus.</p>
<p>La recherche d’un appartement aujourd’hui si l’on compare à l’année 2008 est assez aisée soit par l’entremise d’un agent immobilier (broker) dont la rémunération équivaut à un mois de loyer ou simplement par le bouche à oreille.</p>
<p>Toutefois, un inconvénient d’importance : au moment de la signature du contrat de bail, il faut verser une garantie (deposit) équivalente à 10 mois de loyer récupérable au moment du départ. Même si parfois un arrangement peut se faire avec le propriétaire, il n’en demeure pas moins que le principe de ce versement reste la règle.</p>
<p><strong>Montant moyen des locations</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td></td>
<td>3 pièces</td>
<td>5 pièces</td>
<td>villa</td></tr>
<tr class="row_even even">
<td>Quartier résidentiel</td>
<td>1200 €</td>
<td>1800 €</td>
<td>3000 €</td></tr>
</tbody>
</table>
<p>Pour en savoir plus sur les logements à New Dehli et Calcutta (quartiers, agences immobilières, contrats de location, etc ) :</p>
<ul class="spip">
<li><a href="http://www.delhi-accueil.com/" class="spip_out" rel="external">Delhi accueil</a></li>
<li><a href="http://sites.google.com/site/calcuttaaccueil/Home/presentation/logement" class="spip_out" rel="external">Calcutta accueil</a></li>
<li><a href="http://bangaloreaccueil.wix.com/bangaloreaccueil#!liens/c1a4e" class="spip_out" rel="external">Bangalore accueil</a></li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Hôtels</h3>
<p><strong>Bangalore</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Prix moyen d’une chambre d’hôtel (chambre double)</td>
<td>Euros</td></tr>
<tr class="row_even even">
<td>Grand tourisme</td>
<td>150</td></tr>
<tr class="row_odd odd">
<td>Moyen tourisme</td>
<td>60</td></tr>
</tbody>
</table>
<p><strong>Bombay</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Prix moyen d’une chambre d’hôtel (chambre double)</td>
<td>Euros</td></tr>
<tr class="row_even even">
<td>Grand tourisme</td>
<td>150</td></tr>
<tr class="row_odd odd">
<td>Moyen tourisme</td>
<td>100</td></tr>
</tbody>
</table>
<p><strong>New Delhi</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Prix moyen d’une chambre d’hôtel (chambre double)</td>
<td>Euros</td></tr>
<tr class="row_even even">
<td>Grand tourisme</td>
<td>250</td></tr>
<tr class="row_odd odd">
<td>Moyen tourisme</td>
<td>100</td></tr>
</tbody>
</table>
<p><strong>Pondichéry</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Prix moyen d’une chambre d’hôtel (chambre double)</td>
<td>Euros</td></tr>
<tr class="row_even even">
<td>Grand tourisme</td>
<td>100</td></tr>
<tr class="row_odd odd">
<td>Moyen tourisme</td>
<td>50</td></tr>
</tbody>
</table>
<h4 class="spip">Auberges de jeunesse</h4>
<p>Dans les lieux touristiques, les <i>Tourist Lodge</i> offrent un confort sommaire mais extrêmement bon marché.</p>
<p>Les <i>Guest Houses</i> peuvent être, dans certains cas, acceptables ; il convient de se renseigner au préalable sur les prix et l’hygiène.</p>
<p>Il existe une quarantaine d’auberges de jeunesse réparties dans toute l’Inde. Pour plus d’informations, consulter le site <a href="http://www.hostels.com/india" class="spip_out" rel="external">Hostels.com</a>.</p>
<h3 class="spip"><a id="sommaire_3"></a>Electricité</h3>
<p>Le courant électrique est alternatif, 220 Volts et 50Hz. Il existe différents types de prises, plus ou moins compatibles avec les prises françaises. Bien que les quartiers où résident les expatriés soient généralement épargnés, l’électricité est souvent sujette à d’importantes variations, coupures, surtensions, etc. L’achat de régulateurs de tension, disponibles sur place, est recommandé pour les principaux appareils électriques, ainsi que l’achat éventuel d’un générateur, ou de batteries pour pallier les fréquentes coupures d’électricité. Les tarifs d’électricité sont relativement élevés.</p>
<h3 class="spip"><a id="sommaire_4"></a>Electroménager</h3>
<p>Les cuisines sont rarement équipées, et le plus souvent très archaïques. On trouve sur place des réfrigérateurs, cuisinières et petits équipements ménagers de fabrication locale ou asiatique, de qualité variable, à acheter ou à louer.</p>
<h4 class="spip">Chauffage / climatisation</h4>
<p>Compte tenu du climat, le chauffage s’effectue à l’aide de petits appareils d’appoint soufflants, ou de radiateurs à bain d’huile. En revanche, un système de climatisation qu’il est possible de louer ou d’acheter sur place, est indispensable pendant six à huit mois de l’année.</p>
<p>Le chauffage est inexistant à Pondichéry, mais la climatisation est indispensable pendant 10 mois sur 12.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-inde-vie-pratique-article-loisirs-et-culture.md" title="Loisirs et culture">Loisirs et culture</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-inde-vie-pratique-article-communications.md" title="Communications">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-inde-vie-pratique-article-transports.md" title="Transports">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-inde-vie-pratique-article-cout-de-la-vie-110417.md" title="Coût de la vie">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-inde-vie-pratique-article-scolarisation.md" title="Scolarisation">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-inde-vie-pratique-article-sante.md" title="Santé">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-inde-vie-pratique-article-logement-110414.md" title="Logement">Logement</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/vie-pratique/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
