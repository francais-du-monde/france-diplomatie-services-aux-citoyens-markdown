# Convention fiscale

<p>Une convention en vue d’éviter les doubles impositions et de prévenir l’évasion et la fraude fiscales en matière d’impôts sur le revenu a été signée le 19 juin 2003 à Paris entre le gouvernement de la République française et le gouvernement de la République de Croatie. Elle est assortie d’un protocole formant partie intégrante de la convention.</p>
<p>La loi n°2005-681 du 21 juin 2005 (J.O n° 144 du 22 juin 2005,p. 10447) a autorisé l’approbation de cette convention du côté français qui a été publiée par le décret n° 2005-1292 du 13 octobre 2005 (J.O n° 246 du 21 octobre 2005, p. 16659).</p>
<p>Cette convention est entrée en vigueur le 1er septembre 2005.</p>
<p>Le texte de la convention est disponible en ligne <a href="http://bofip.impots.gouv.fr/bofip/448-PGP?datePubl=16/11/2012" class="spip_out" rel="external">sur le site de l’administration fiscale</a>.</p>
<p><strong>Champ d’application de la convention</strong></p>
<p>L’article 29 de la convention prévoit que les stipulations qu’elle comporte s’appliquent :</p>
<ul class="spip">
<li>en ce qui concerne les impôts sur le revenu perçus par voie de retenue à la source, aux sommes imposables à compter du 1er janvier 2006 ;</li>
<li>en ce qui concerne les impôts sur le revenu qui ne sont pas perçus par voie de retenue à la source, aux revenus afférents, suivant les cas, à toute année civile ou tout exercice commençant au 1er janvier 2006 ;</li>
<li>en ce qui concerne les autres impôts, aux impositions dont le fait générateur interviendra à compter du 1er janvier 2006.</li></ul>
<p>La convention franco-yougoslave du 28 mars 1974 a cessé de s’appliquer aux relations avec la République de Croatie à compter de la date à laquelle les stipulations correspondantes de la présente convention se sont appliquées pour la première fois.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/fiscalite/article/convention-fiscale). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
