# Fiscalité du pays

<p class="chapo">
      Le code fiscal qui régit actuellement la fiscalité vénézuélienne date de 2001. Crée en 1993 le <i>Servicio Nacional Integrado de Administracion Aduana y Tributaria</i> (SENIAT) est l’équivalent à la fois de notre direction générale des impôts et de notre direction générale des douanes et droits indirects.
</p>
<p>Les sociétés étrangères qui souhaitent exercer une activité au Venezuela doivent s’inscrire au SENIAT obligatoirement. Le formulaire d’inscription doit être fourni avec l’original et une copie du document constitutif, le numéro fiscal, les coordonnées du représentant légal au Venezuela et une copie du contrat.</p>
<p>La législation fiscale vénézuélienne est pro-cyclique : ainsi, les taux d’imposition ou de taxes en vigueur sont souvent modifiés et de nouveaux impôts créés ou suspendu en fonction de la conjoncture économique.</p>
<h4 class="spip">Typologie de l’impôt</h4>
<p>La fiscalité vénézuélienne repose sur trois niveaux d’impôts et de taxes, eux-mêmes répartis entre les composantes directes et indirectes :</p>
<ul class="spip">
<li><strong>Les impôts nationaux</strong> qui comprennent la TVA, l’impôt sur le revenu, les droits de douanes et les charges sociales.</li>
<li><strong>Les impôts provinciaux</strong>. La loi organique régissant les états provinciaux publiée en 2001 permet aux Etats de se financer en partie par des revenus fiscaux. Ce sont donc principalement des timbres fiscaux, nécessaires à l’élaboration des papiers d’identité, à l’obtention des titres de propriété…</li>
<li><strong>Les impôts municipaux</strong>. La loi organique sur le régime municipal permet aussi aux municipalités de collecter des taxes et impôts locaux, les principaux étant les suivants : la taxe d’habitation, la patente sur les activités d’industrie et de commerce, la redevance publicitaire, la taxe sur les spectacles publics et la taxe sur les véhicules.</li></ul>
<h4 class="spip">Impôt sur le revenu</h4>
<p>L’ISLR <i>(Impuesto sobre la renta)</i> est applicable aux personnes physiques ou juridiques qui résident au Venezuela, sur tous les revenus (qu’ils proviennent du Venezuela ou de l’étranger). Le taux d’imposition est calculé en fonction du revenu annuel imposable exprimé en unités fiscales. Il s’étend de 6 à 34%. Les personnes physiques non résidentes au Venezuela ne sont imposées que sur les revenus qu’elles ont créés sur le territoire vénézuélien, au taux unique de 34%.</p>
<h4 class="spip">TVA</h4>
<p>Le taux standard de l’IVA (<i>Impuesto sobre el Valor Agregado</i>) est de 12%, bien que 10% supplémentaires puissent être demandés sur les produits de luxe. Il existe plusieurs autres niveaux de TVA dont les taux varient entre 8 et 9% (produits issus du secteur agricole, véhicules automobiles…). Certains produits (produits de première nécessité et véhicules destinés aux transports publics) et zones (Margarita, péninsule de Paraguana) font l’objet d’une exonération totale de TVA. Sont également concernés les produits dont la production dans le pays est jugée insuffisante. Enfin le gouvernement a créé un impôt indirect sur les cigarettes (50%) et le tabac (35%).</p>
<h4 class="spip">Barème de l’impôt</h4>
<p>Le système d’imposition au Venezuela est exprimé en unités d’impôt qui définissent les taux d’imposition. Depuis février 2012, une unité d’impôt est égale à 90,00 VEB.</p>
<table class="spip">
<thead><tr class="row_first"><th id="id6d35_c0"> Impôt sur le revenu   </th><th id="id6d35_c1"> Taux appliqué   </th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="id6d35_c0">Jusqu’à 1000 UVT</td>
<td headers="id6d35_c1">6%</td></tr>
<tr class="row_even even">
<td headers="id6d35_c0">1001 à 1500 UVT</td>
<td headers="id6d35_c1">9%</td></tr>
<tr class="row_odd odd">
<td headers="id6d35_c0">1500 à 2000 UVT</td>
<td headers="id6d35_c1">12%</td></tr>
<tr class="row_even even">
<td headers="id6d35_c0">2000 à 2500</td>
<td headers="id6d35_c1">16%</td></tr>
<tr class="row_odd odd">
<td headers="id6d35_c0">2500 à 3000 UVT</td>
<td headers="id6d35_c1">20%</td></tr>
<tr class="row_even even">
<td headers="id6d35_c0">De 3000 à 4000 UVT</td>
<td headers="id6d35_c1">24%</td></tr>
<tr class="row_odd odd">
<td headers="id6d35_c0">De 4000 à 6000 UVT</td>
<td headers="id6d35_c1">29%</td></tr>
<tr class="row_even even">
<td headers="id6d35_c0">Au-delà de 6000 UVT</td>
<td headers="id6d35_c1">34%</td></tr>
</tbody>
</table>
<h4 class="spip">Année fiscale</h4>
<p>Du 1er janvier au 31 décembre.</p>
<p>Pour plus d’informations, consultez le site du <a href="http://contribuyente.seniat.gob.ve/index.htm" class="spip_out" rel="external">ministère des Finances du Venezuela</a></p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/fiscalite/article/fiscalite-du-pays). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
