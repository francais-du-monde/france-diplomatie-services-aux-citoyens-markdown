# Recherche d’emploi

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/emploi-stage/article/recherche-d-emploi#sommaire_1">Outils pour la recherche d’emploi</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/emploi-stage/article/recherche-d-emploi#sommaire_2">Organismes pour la recherche d’emploi</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/emploi-stage/article/recherche-d-emploi#sommaire_3">Opportunités d’emploi et de stages</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Outils pour la recherche d’emploi</h3>
<h4 class="spip">Journaux</h4>
<ul class="spip">
<li><a href="http://www.lematin.ma/" class="spip_out" rel="external">Le Matin</a></li>
<li>La revue <a href="http://www.cfcim.org/" class="spip_out" rel="external">Conjoncture</a> de la Chambre française de commerce et d’industrie du Maroc.</li></ul>
<h4 class="spip">Sites internet</h4>
<ul class="spip">
<li><a href="http://www.bayt.com/fr/jobs/countries/morocco/" class="spip_out" rel="external">Bayt</a></li>
<li><a href="http://www.marocemploi.net/" class="spip_out" rel="external">Maroc emploi</a></li>
<li><a href="http://www.rekrute.com/" class="spip_out" rel="external">Rekrute</a></li>
<li><a href="http://www.optioncarriere.ma/" class="spip_out" rel="external">Option carrière</a></li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Organismes pour la recherche d’emploi</h3>
<ul class="spip">
<li><a href="http://www.cfcim.org/" class="spip_out" rel="external">Chambre française de commerce et d’industrie du Maroc (CFCIM)</a><br class="manualbr">15 avenue Mers Sultan - 21000 Casablanca<br class="manualbr">Téléphone : [212] (0)5 22 20 90 90 - Télécopie : [212] (0)5 22 20 01 30<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/emploi-stage/article/recherche-d-emploi#cfcim#mc#cfcim.org#" title="cfcim..åt..cfcim.org" onclick="location.href=mc_lancerlien('cfcim','cfcim.org'); return false;" class="spip_mail">Courriel</a></li>
<li><strong>Agence nationale de l’accueil des étrangers et des migrations (ANAEM)</strong><br class="manualbr">Rue Rakib El Moussaoui – ex rue Calamé – Aïn Borjda <br class="manualbr">BP 13002 – 21000 Casablanca<br class="manualbr">Téléphone [212] (0)5 22 61 87 74</li>
<li><a href="http://www.emploi.gov.ma/" class="spip_out" rel="external">Ministère de l’Emploi et de la Formation professionnelle</a> <br class="manualbr">Avenue Mohammed V - Quartier des ministères - Rabat <br class="manualbr">Téléphone : [212] (0)5 37 76 05 21 ou 25<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/emploi-stage/article/recherche-d-emploi#communication#mc#emploi.gov.ma#" title="communication..åt..emploi.gov.ma" onclick="location.href=mc_lancerlien('communication','emploi.gov.ma'); return false;" class="spip_mail">Courriel</a></li>
<li><a href="http://www.cnops.org.ma/" class="spip_out" rel="external">Caisse nationale des organismes de prévoyance sociale (CNOPS)</a></li>
<li><a href="http://www.ofppt.ma/" class="spip_out" rel="external">Office de la formation professionnelle et de la promotion du travail (OFPPT)</a></li>
<li><a href="http://www.anapec.org/" class="spip_out" rel="external">Agence nationale de promotion de l’emploi et des compétences (ANAPEC)</a></li>
<li><a href="http://www.cnss.ma/" class="spip_out" rel="external">Caisse nationale de sécurité sociale (CNSS)</a></li>
<li><a href="http://www.manpower-maroc.com/" class="spip_out" rel="external">Manpower</a><br class="manualbr">27 rue Jallal Eddine Essayouti - Place Nid d’Iris - Quartier Racine <br class="manualbr">20100 Casablanca<br class="manualbr">Téléphone : [212] (0)5 22 95 96 80 - Télécopie : [212] (0)5 22 95 11 25</li>
<li><a href="http://www.adecco-maroc.com/" class="spip_out" rel="external">Adecco</a><br class="manualbr">125 boulevard Zerktouni - 20190 Casablanca<br class="manualbr">Téléphone : [212] (0)5 22 99 10 23 ou 24 ou 25 / [2112] (0)5 22 99 42 05<br class="manualbr">Télécopie : [212] (0)5 22 99 10 26<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/emploi-stage/article/recherche-d-emploi#adecco.emploi#mc#adecco.co.ma#" title="adecco.emploi..åt..adecco.co.ma" onclick="location.href=mc_lancerlien('adecco.emploi','adecco.co.ma'); return false;" class="spip_mail">Courriel</a></li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Opportunités d’emploi et de stages</h3>
<p><strong>Ce que recherchent les recruteurs</strong></p>
<p>Parler la langue du pays n’est pas obligatoire. L’arabe est la langue officielle du Maroc mais le français est couramment utilisé. L’anglais et l’espagnol peuvent être parfois exigés et représentent un atout indéniable.</p>
<p>Des cours d’arabe sont dispensés par l’Institut culturel français (cours particuliers : environ 150 MAD de l’heure) et par des organismes privés.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/emploi-stage/article/recherche-d-emploi). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
