# Vaccination

<p>Aucune vaccination n’est exigée à l’entrée du pays, pour un voyageur en provenance de France.</p>
<p>Il est conseillé, pour des raisons médicales :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Adultes :</strong> mise à jour des vaccinations contre la diphtérie, le tétanos et la poliomyélite ; vaccination contre la typhoïde (longs séjours), l’hépatite A (à partir de 50 ans, une recherche préalable d’anticorps sériques totaux est justifiée), l’hépatite B (longs séjours et/ou séjours à risques). </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Enfants :</strong> vaccinations recommandées en France par le ministère de la Santé - et en particulier : B.C.G. et hépatite B dès le premier mois (longs séjours), rougeole dès l’âge de neuf mois. Hépatite A à partir d’un an. Typhoïde à partir de cinq ans (longs séjours). </p>
<p>Vaccination contre la rage dès que l’enfant est en âge de marcher.</p>
<p>Le vaccin polio inactivé étant recommandé en France, il est conseillé de se le procurer.</p>
<p>On peut trouver à Istanbul (à l’Institut Mérieux tél. 288.41.40 et dans certaines pharmacies) des vaccins sous forme orale ou injectable : hépatite A, hépatite B, typhoïde, tétanos, poliomyélite.</p>
<p>Pour en savoir plus, lisez <a href="services-aux-citoyens-preparer-son-expatriation-sante.md" class="spip_in">notre thématique sur la vaccination</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/turquie/entree-et-sejour/article/vaccination-110015). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
