# Lettre de motivation

<p>La lettre de motivation est avant tout un message permettant d’argumenter sa candidature et d’exprimer ses motivations en fonction de l’offre à laquelle vous répondez ou que vous proposez.</p>
<p>L’objectif de la lettre de motivation est de permettre d’obtenir un entretien et non un emploi.</p>
<p><strong>Rédaction</strong></p>
<ul class="spip">
<li>L’introduction :
<br>— Objet de la lettre
<br>— Prise de connaissance de l’existence de l’offre
<br>— Quelques informations basiques sur vous-même en rapport avec l’offre ou le secteur d’activité</li>
<li>Le développement :
<br>— Explication simple des raisons pour lesquelles vous êtes intéressé par le poste en mettant en évidence les liens entre vos expériences/compétences et les besoins du poste</li>
<li>La proposition de rdv :
<br>— Souhait d’un entretien
<br>— Relance dans 2 semaines</li>
<li>Formule de politesse</li></ul>
<p><i>Mise à jour : juillet 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/emploi-stage/article/lettre-de-motivation-114432). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
