# Recherche d’emploi

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/emploi-stage/article/recherche-d-emploi#sommaire_1">Outils pour la recherche d’emploi</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/emploi-stage/article/recherche-d-emploi#sommaire_2">Organismes pour la recherche d’emploi</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/emploi-stage/article/recherche-d-emploi#sommaire_3">Opportunités d’emploi et de stages</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Outils pour la recherche d’emploi</h3>
<h4 class="spip">Journaux</h4>
<p>Le principal journal du Chili, <a href="http://www.emol.cl/" class="spip_out" rel="external">El Mercurio</a>), publie des offres de travail dans son édition du dimanche.</p>
<p>Vous pouvez également consulter les journaux économiques suivants :</p>
<ul class="spip">
<li><a href="http://www.estrategia.cl/" class="spip_out" rel="external">Estrategia</a></li>
<li><a href="http://www.capital.cl/" class="spip_out" rel="external">Capital</a></li>
<li><a href="http://www.quepasa.cl/" class="spip_out" rel="external">Qué pasa ?</a></li></ul>
<h4 class="spip">Sites internet</h4>
<p>Les universités chiliennes possèdent souvent leur propre service d’aide à la recherche d’emploi ; les deux suivantes proposent des emplois dans tous les secteurs :</p>
<ul class="spip">
<li><a href="http://www.udec.cl/" class="spip_out" rel="external">Université de Concepción</a></li>
<li><a href="http://www.utfsm.cl/" class="spip_out" rel="external">Universidad Técnica Federico Santa María</a></li></ul>
<p><strong>A noter</strong> : si vous possédez la nationalité française, vous pouvez postuler aux offres publiées sur le site de <a href="http://www.pole-emploi-international.fr/www.eei.com.fr/eei/accueil.jsp" class="spip_out" rel="external">Pôle emploi international</a></p>
<p>Les sites suivants sont réservés aux personnes possédant la nationalité chilienne :</p>
<ul class="spip">
<li>le <a href="http://www.sence.cl/" class="spip_out" rel="external">Bureau national de la formation et de l’emploi</a> (<i>Servicio Nacional de Capacitación y de Empleo</i>) propose des formations.</li>
<li>le <a href="http://www.sercotec.cl/" class="spip_out" rel="external">Bureau national de coopération technique</a> (<i>Servicio Nacional de Cooperación Técnica</i>) apporte un appui à la création et au développement des micro-entreprises.</li></ul>
<h4 class="spip">Réseaux</h4>
<p>Le Chili est un petit pays dont la capitale représente plus de 40% de la population et 65% du PIB. L’élite y est regroupée autour d’une dizaine de grands lycées et de grandes universités. C’est un pays dans lequel les effets de réseau comptent énormément. Il est donc important de pouvoir se faire recommander par quelqu’un.</p>
<h4 class="spip">Annuaires</h4>
<h5 class="spip">Les filiales françaises</h5>
<p>Vous pouvez demander la liste des filiales de groupes français auprès du <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/emploi-stage/article/recherche-d-emploi#jacqueline#mc#camarafrancochilena.cl#" title="jacqueline..åt..camarafrancochilena.cl" onclick="location.href=mc_lancerlien('jacqueline','camarafrancochilena.cl'); return false;" class="spip_mail">service de recrutement</a> de la Chambre de commerce et d’industrie franco-chilienne.</p>
<h5 class="spip">Les associations professionnelles</h5>
<p>Les associations professionnelles, relativement actives au Chili, représentent une des meilleures portes d’entrée pour cibler les entreprises agissant dans le secteur qui vous intéresse. La liste suivante regroupe les principales associations professionnelles référencées par la Chambre de commerce et d’industrie franco-chilienne.</p>
<p><strong>Agriculture</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  ANPROS (Association des producteurs de semences) : <a href="http://www.anpros.cl/" class="spip_out" rel="external">www.anpros.cl</a></p>
<p><strong>Alimentaire</strong></p>
<ul class="spip">
<li><a href="http://www.achic.cl/" class="spip_out" rel="external">ACHIC</a> (Association chilienne de la viande)</li>
<li><a href="http://www.salmonchile.cl/" class="spip_out" rel="external">APSTCH</a> (Association des producteurs de saumon)</li>
<li><a href="http://www.asprocer.cl/" class="spip_out" rel="external">ASPROCER</a> (Association des producteurs de porcs du Chili)</li>
<li><a href="http://www.fedefruta.cl/" class="spip_out" rel="external">FEDEFRUTA</a> (Fédération nationale des producteurs de fruits)</li>
<li><a href="http://www.soprole.cl/" class="spip_out" rel="external">SOPROLE</a> (Société des producteurs de lait)</li></ul>
<p><strong>Bois</strong></p>
<ul class="spip">
<li><a href="http://www.asimad.cl/" class="spip_out" rel="external">ASIMAD</a> (Association de l’industrie du bois)</li>
<li><a href="http://www.corma.cl/" class="spip_out" rel="external">CORMA</a> (Fédération des industries privées chiliennes du bois)</li></ul>
<p><strong>Chimie</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.asiquim.cl/" class="spip_out" rel="external">ASIQUIM</a></p>
<p><strong>Construction</strong></p>
<ul class="spip">
<li><a href="http://www.cchc.cl/" class="spip_out" rel="external">Cámara Chilena de la Construcción</a> (Chambre chilienne de la construction)</li>
<li><a href="http://www.iconstruccion.cl/" class="spip_out" rel="external">Instituto Chileno de la Construcción</a> (Institut chilien de la construction)</li>
<li><a href="http://www.icha.cl/" class="spip_out" rel="external">Instituto Chileno del Acero</a> (Institut chilien de l’acier)</li>
<li><a href="http://www.ich.cl/" class="spip_out" rel="external">Institut chilien du ciment et du béton</a></li>
<li><a href="http://www.asimet.cl/" class="spip_out" rel="external">ASIMET</a> (Association des industries métallurgiques)</li></ul>
<p><strong>Cosmétique</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.camaracosmetica.cl/" class="spip_out" rel="external">Cámara de la industria Cosmética de Chile</a> (Chambre de l’industrie cosmétique du Chili) </p>
<p><strong>Culture</strong></p>
<ul class="spip">
<li><a href="http://www.camaradellibro.cl/" class="spip_out" rel="external">Cámara Chilena del Libro</a> (Chambre chilienne du livre)</li>
<li><a href="http://www.consejodelacultura.cl" class="spip_out" rel="external">Consejo Nacional de la Cultura y de las Artes</a> (Conseil national de la culture et des arts)</li></ul>
<p><strong>Electronique</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.aie.cl/" class="spip_out" rel="external">AIE - Asociación de la Industria Eléctrica-Electrónica</a> (association de l’industrie électronique)</p>
<p><strong>Emballages</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.cenem.cl/" class="spip_out" rel="external">CENEM - Centro de Envases y Embalajes de Chile</a></p>
<p><strong>Exportations</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.asoex.cl/" class="spip_out" rel="external">ASOEX - Asociación de Exportadores de Chile</a></p>
<p><strong>Mine</strong></p>
<ul class="spip">
<li><a href="http://www.aprimin.cl/" class="spip_out" rel="external">APRIMIN - Asociación de Grandes Provedores Industriales de la Minería</a></li>
<li><a href="http://www.sernageomin.cl/" class="spip_out" rel="external">SERNAGEOMIN - Servicio Nacional de Geología y de Minas</a> (Office national de géologie et des mines)</li></ul>
<p><strong>Organismes</strong></p>
<ul class="spip">
<li><a href="http://www.fundacionchile.cl/" class="spip_out" rel="external">FUNDACIÓN CHILE</a></li>
<li><a href="http://www.kompass.cl/" class="spip_out" rel="external">KOMPASS</a></li>
<li><a href="http://www.sofofa.cl/" class="spip_out" rel="external">SOFOFA - Sociedad de Fomento Fabril</a></li></ul>
<p><strong>Plastique</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.asipla.cl/" class="spip_out" rel="external">ASIPLA - Asociación Gremial de Industriales de Plástico de Chile</a></p>
<p><strong>Pêche</strong></p>
<ul class="spip">
<li><a href="http://www.mardechile.cl/" class="spip_out" rel="external">Fundación Mar de Chile</a></li>
<li><a href="http://www.mundomaritimo.cl/" class="spip_out" rel="external">Mundo Maritimo</a></li></ul>
<p><strong>Santé</strong></p>
<ul class="spip">
<li><a href="http://www.asilfa.cl/" class="spip_out" rel="external">ASILFA - Asociación Industrial de Laboratorios Farmacéuticos chilenos</a> (association industrielle des laboratoires pharmaceutiques chiliens)</li>
<li><a href="http://www.colegioveterinario.cl/" class="spip_out" rel="external">Colegio Médico Veterinario</a></li></ul>
<p><strong>Technologie</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.acti.cl/" class="spip_out" rel="external">Asociación Chilena de Empresas de Tecnologías de Información</a></p>
<p><strong>Textile</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.cnct.cl/" class="spip_out" rel="external">Comité Nacional de Conservación Textil</a></p>
<p><strong>Vins</strong></p>
<ul class="spip">
<li><a href="http://www.vinasdechile.cl/" class="spip_out" rel="external">Asociación de Viñas de Chile</a></li>
<li><a href="http://www.enologo.cl/" class="spip_out" rel="external">Asociación Nacional de Ingenieros Agrónomos Enólogos de Chile</a></li>
<li><a href="http://www.winesofchile.org/" class="spip_out" rel="external">Wines of Chile</a></li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Organismes pour la recherche d’emploi</h3>
<h4 class="spip">L’Agence pour l’Emploi locale</h4>
<p>Il n’existe pas au Chili d’équivalent de Pôle Emploi. Vous pouvez consulter les sites Internet gouvernementaux dédiés à l’emploi. Sachez cependant que, du fait des documents à fournir, ces offres ne sont accessibles qu’aux Français déjà bien intégrés au Chili.</p>
<p>Le site de la <a href="http://www.bolsadeempleo.cl" class="spip_out" rel="external">Bolsa de Empleo</a> (bourse d’emploi) reprend les offres de plus de 120 bureaux communaux du travail (OMIL, <i>Oficinas Municipales de Intermediación Laboral</i>). Ces bureaux recensent les offres des entreprises dans tout le pays. Pour s’inscrire à ces sites, plusieurs documents sont exigés, mais l’accès aux offres est libre.</p>
<h4 class="spip">L’Office des migrations internationales</h4>
<p>L’Office des Migrations Internationales (OMI) dispose d’une antenne à Santiago. Défendant le principe « une immigration organisée bénéficie à tous », l’OMI se propose d’aider les migrants et leur famille, notamment en les orientant vers des programmes institutionnels.</p>
<p><a href="http://www.chile.iom.int/" class="spip_out" rel="external">Organización Internacional para las Migraciones</a> (OIM)<br class="manualbr">Tel : (00 56) 2 – 274 67 13 <br class="manualbr">Télécopie : (00 56) 2 – 204 97 04<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/emploi-stage/article/recherche-d-emploi#iomsantiago#mc#iom.int#" title="iomsantiago..åt..iom.int" onclick="location.href=mc_lancerlien('iomsantiago','iom.int'); return false;" class="spip_mail">Courriel</a></p>
<p><strong>La Chambre de commerce et d’industrie franco-chilienne</strong></p>
<p>A travers son service de recrutement, la Chambre de commerce et d’industrie franco-chilienne peut vous aider dans vos démarches de recherche d’emploi. Ce service est chargé d’accueillir, d’informer et d’orienter les Français présents au Chili qui cherchent un emploi ou un stage et de mettre en relation les offres d’emploi reçues et les candidats inscrits dans leur site web.</p>
<p>Pour accéder à ce service vous devez déposer <a href="http://www.camarafrancochilena.cl/fr/" class="spip_out" rel="external">en ligne</a> votre CV en espagnol et vous pouvez aussi leur demander un entretien individuel d’évaluation, de conseil et d’orientation.</p>
<h4 class="spip">Les agences d’intérim</h4>
<p>Au Chili, les agences d’intérim sont appelées <i>Agencias de Servicios Temporales</i>.</p>
<p>De nombreux sites de recrutement, ainsi que la plupart des agences de recrutement, proposent également des emplois temporaires, appelés <i>temporales</i>.</p>
<p><strong>Le service économique et UbiFrance</strong></p>
<p>Rappelons que les services économiques à l’étranger n’ont pas pour mission d’appuyer les Français dans leur recherche d’emploi. Dans le cadre d’une création d’entreprise, le service économique et UbiFrance peuvent en revanche vous aider à travers les conseils des différents attachés spécialisés dans chaque secteur (biens de consommation, agriculture).</p>
<h3 class="spip"><a id="sommaire_3"></a>Opportunités d’emploi et de stages</h3>
<p><strong>Les postes les plus demandés à la CFCCI :</strong></p>
<ul class="spip">
<li>Secrétaire bilingue – assistante de direction bilingue ou trilingue</li>
<li>Traducteurs – interprètes</li>
<li>Commercial : marketing – import / export – logistique</li>
<li>Tourisme</li>
<li>Finances</li></ul>
<p><i>Contact</i> : responsable du service emploi de la <a href="http://www.camarafrancochilena.cl/" class="spip_out" rel="external">Chambre de commerce</a><br class="manualbr">Marchant Pereira 201 - Piso 7 - Of. 701 - Providencia - Santiago de Chile<br class="manualbr">Téléphone : +56 (2) 225 55 47 <br class="manualbr">Télécopie : +56 (2) 225 55 45</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/emploi-stage/article/recherche-d-emploi). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
