# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/vie-pratique/article/transports-110899#sommaire_1">Importation de véhicule</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/vie-pratique/article/transports-110899#sommaire_2">Permis de conduire</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/vie-pratique/article/transports-110899#sommaire_3">Code de la route</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/vie-pratique/article/transports-110899#sommaire_4">Assurances et taxes</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/vie-pratique/article/transports-110899#sommaire_5">Achat et location</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/vie-pratique/article/transports-110899#sommaire_6">Immatriculation</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/vie-pratique/article/transports-110899#sommaire_7">Entretien</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/vie-pratique/article/transports-110899#sommaire_8">Réseau routier</a></li>
<li><a id="so_9" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/vie-pratique/article/transports-110899#sommaire_9">Transports en commun</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Importation de véhicule</h3>
<p>L’importation d’un véhicule passe par cinq étapes :</p>
<ul class="spip">
<li>Autorisation d’importation du véhicule (copie de la carte grise et de la carte de résidence croate, numéro d’identification croate OIB) ;</li>
<li>Déclaration auprès des douanes(passeport, carte grise, taxe à payer) ;</li>
<li>Contrôle technique et Ecotest auprès d’un centre de contrôle technique (carte grise, déclaration d’importation douanière, carte de résidence croate, prévoir de payer l’écotaxe) ;</li>
<li>Assurance du véhicule : pour pouvoir immatriculer le véhicule, il faut l’assurance « obligatoire » croate dit « OBAVEZNO OSIGURANJE » ce qui correspond à l’assurance dite au tiers ;</li>
<li>Immatriculation du véhicule auprès de la police (déclaration douanière, carte de résidence croate originale, carte grise originale, attestation d’assurance croate, certificat du contrôle technique, certificat d’écotaxe).</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Permis de conduire</h3>
<p>Le permis de conduire français est reconnu en Croatie pour les séjours de moins de trois mois (véhicule privé ou de location) : il faut se munir de la carte grise et de la carte verte internationale d’assurance automobile (le véhicule doit être assuré sur le territoire croate : sigle HR sur la carte verte ; sinon il faut souscrire un complément d’assurance à la frontière).</p>
<p>Le permis de conduire croate n’est pas indispensable pour conduire un véhicule en Croatie si vous êtes un touriste et si vous passez moins de six mois sur le territoire croate. Un permis de conduire internationale (valable un an) peut vous permettre de l’utiliser au-delà de six mois mais pas plus que 12 mois.</p>
<p>Si vous êtes expatrié (votre séjour dépasse donc 6 mois), vous devez solliciter une simple équivalence de permis. Pour faire cette démarche, vous devez vous rendre personnellement à la police croate.</p>
<h3 class="spip"><a id="sommaire_3"></a>Code de la route</h3>
<p>La vitesse est limitée à 130 km/h sur autoroute, 90 km/h sur les routes nationales et à 50 km/h en agglomération. Les contrevenants s’exposent à de fortes amendes (entre 300 et 3 000 HRK).</p>
<p>Le port de la ceinture de sécurité est obligatoire pour le chauffeur et les passagers du véhicule (amende : 500 HRK)</p>
<p>L’usage du téléphone portable est interdit au volant (amende : 500 HRK). Cependant, l’usage d’un appareil muni d’un kit « mains libres » est autorisé.</p>
<p>Le taux d’alcoolémie autorisé au volant est limité à 0,5g/l. La conduite en état d’ébriété entraîne le paiement d’une amende (de 500 à 3000 HRK) et le retrait immédiat du permis de conduire.</p>
<p>Infractions courantes et amendes : <br class="manualbr">franchissement de feu rouge : de 2000 à 5000 HRK<br class="manualbr">excès de vitesse : de 300 à 15 000 HRK<br class="manualbr">défaut de priorité : 2 000 HRK<br class="manualbr">dépassement dangereux : de 300 à 2 000 HRK</p>
<p>Fourrière : si le conducteur de la voiture revient avant que son véhicule soit emmené par la fourrière, il paie la moitié du prix de l’enlèvement (environ 250 pour un total de 500 HRK). Il paiera aussi l’amende qui variera selon l’infraction réalisée (c’est-à-dire 300, 400, 500 ou 700 kunas).</p>
<h3 class="spip"><a id="sommaire_4"></a>Assurances et taxes</h3>
<p>Le propriétaire d’un véhicule doit obligatoirement l’assurer au tiers : « OBAVEZNO OSIGURANJE ».</p>
<p>L’assurance tous risques ou « KASKO » est possible mais n’est pas obligatoire.</p>
<h3 class="spip"><a id="sommaire_5"></a>Achat et location</h3>
<p>Toutes les grandes sociétés de location de véhicules sont présentes en Croatie.</p>
<h3 class="spip"><a id="sommaire_6"></a>Immatriculation</h3>
<p><strong>L’immatriculation doit être renouvelée tous les ans</strong>. Pour cela, il faut présenter à la police une carte de résidence croate valide, un certificat d’assurance, le contrôle technique annuel et le justificatif de paiement de l’écotaxe.</p>
<h3 class="spip"><a id="sommaire_7"></a>Entretien</h3>
<p>Un contrôle technique doit être réalisé chaque année.</p>
<h3 class="spip"><a id="sommaire_8"></a>Réseau routier</h3>
<p>Le réseau routier est globalement satisfaisant. En raison de la dangerosité de certains axes (intensité du trafic, mauvais état des chaussées), il est conseillé de faire preuve de la plus grande vigilance au volant, notamment sur les routes secondaires mais aussi en ville.</p>
<h3 class="spip"><a id="sommaire_9"></a>Transports en commun</h3>
<p>Le réseau ferroviaire est peu développé et propose des liaisons interurbaines relativement lentes, alors que les liaisons proposées par les autobus sont liaisons nombreuses et peu onéreuses.</p>
<p>Les grandes villes disposent de systèmes de bus, tramways et trains de banlieue.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/croatie/vie-pratique/article/transports-110899). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
