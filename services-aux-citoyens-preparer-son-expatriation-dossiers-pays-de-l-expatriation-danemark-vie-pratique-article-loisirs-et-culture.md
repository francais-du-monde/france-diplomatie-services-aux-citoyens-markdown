# Loisirs et culture

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/vie-pratique/article/loisirs-et-culture#sommaire_1">Tourisme</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/vie-pratique/article/loisirs-et-culture#sommaire_2">Activités culturelles</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/vie-pratique/article/loisirs-et-culture#sommaire_3">Sports</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/vie-pratique/article/loisirs-et-culture#sommaire_4">Télévision – Radio</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/vie-pratique/article/loisirs-et-culture#sommaire_5">Presse française</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Tourisme</h3>
<p>Le Danemark présente une grande variété de sites touristiques. Copenhague, la capitale millénaire, offre au promeneur le spectacle de ses vieilles rues colorées, de Tivoli, le vieux parc d’attractions et propose plusieurs musées de grand intérêt. Dans les villes comme à la campagne, les châteaux et manoirs sont nombreux et l’on trouve de très beaux musées d’art ancien ou moderne. Les différentes îles danoises présentent un paysage varié, avec plus de 7 300 km de plages.</p>
<p>Le <a href="http://www.visitdenmark.com/frankrig/fr-fr/menu/turist/turistforside.htm" class="spip_out" rel="external">site officiel danois du tourisme</a>  présente une information très complète et offre des idées originales de séjour.</p>
<p><strong>Danmarks Turistraad</strong><br class="manualbr">Vesterbrogade 6D <br class="manualbr">1620 Copenhague V <br class="manualbr">Tél. : 33 11 14 15</p>
<h3 class="spip"><a id="sommaire_2"></a>Activités culturelles</h3>
<h4 class="spip">Activités culturelles en français</h4>
<h5 class="spip">Cinéma</h5>
<p>Quelques films "grand public" sont présentés en version française sous-titrée dans les salles de cinéma.</p>
<p>Les cinémas Grand à Copenhague et Ost for Paradis à Aarhus proposent tout au long de l´année une sélection à 60% européenne où le cinéma français est bien représenté.</p>
<ul class="spip">
<li><a href="http://www.grandteatret.dk/" class="spip_out" rel="external">Grandteatret.dk</a></li>
<li><a href="http://www.paradisbio.dk/" class="spip_out" rel="external">Paradisbio.dk</a></li></ul>
<h5 class="spip">Manifestations culturelles</h5>
<p>Des spectacles français (théâtre, concerts…) se produisent assez souvent. Des ballets et opéras sont parfois représentés à Copenhague. Les expositions d’artistes français sont courantes.</p>
<p>Par ailleurs, plusieurs activités (conférences, films, concerts, cours) sont proposées par l’<a href="http://www.institutfrancais.dk/fr" class="spip_out" rel="external">Institut français du Danemark</a>, à l’adresse suivante :</p>
<p>Østergade 18, 1. et 2. étage<br class="manualbr">1100 Copenhague K<br class="manualbr">Tél : 33 38 77 00</p>
<h4 class="spip">Activités culturelles locales</h4>
<h5 class="spip">Audiovisuel</h5>
<p>Aucune émission n’est diffusée en langue française par les médias danois (radio ou télévision). En revanche, les programmes et séries importés (américains essentiellement) sont régulièrement diffusés en version originale, éventuellement sous-titrée.</p>
<p>L’utilisation d’une antenne parabolique ouvre l’accès aux chaînes internationales. Par ailleurs, il est aisé de se procurer sur place du matériel vidéo et de louer des films, provenant pour la plupart des Etats-Unis en version originale.</p>
<h5 class="spip">Cinéma</h5>
<p>Les cinémas projettent des films en exclusivité. Les salles sont confortables. Bien que la production américaine occupe une large part des sorties en salle, le cinéma danois a connu une consécration internationale à travers l’œuvre de réalisateurs de renom comme Carl Theodor Dreyer et, plus récemment, Gabriel Axel (<i>Le Festin de Babette</i>), Lars von Trier (<i>Breaking the waves, Antichrist</i>) ou encore Thomas Vinterberg (<i>Fête de Famille</i>).</p>
<h5 class="spip">Manifestations culturelles </h5>
<p>Le <a href="http://www.danemark.dt.dk/" class="spip_out" rel="external">site officiel danois du tourisme</a> tient à jour la liste des nombreuses manifestations à travers le pays : théâtre, festivals, concerts, expositions…</p>
<h3 class="spip"><a id="sommaire_3"></a>Sports</h3>
<p>Avec un point culminant à 170 mètres, le Danemark est caractérisé par son relief plat qui favorise la pratique de la bicyclette, comme sport et moyen de déplacement. Avec plus de 12,000 km de voies cyclables signalées, terrains faciles, nature unique et de courtes distances entre les différents villages et villes, le Danemark est un pays fait pour les cyclistes.</p>
<p>Compte tenu de sa géographie, le Danemark est un pays idéal pour la pratique des sports aquatiques : baignade, voile, plongée, canöe-kayak. Le drapeau bleu a été décerné à 200 plages et 79 ports de plaisance.</p>
<p>L’équitation, les sports de randonnée, la pêche et le golf (dont la pratique au Danemark compte parmi les moins chères d’Europe) sont également assez répandus.</p>
<h3 class="spip"><a id="sommaire_4"></a>Télévision – Radio</h3>
<p>Toutes les radios et chaînes de télévision françaises et européennes sont disponibles par satellite (à un coût élevé).</p>
<h4 class="spip">Radio</h4>
<p>Il est possible de capter RFI et France Inter (162 MHz). La réception est moyenne sur les ondes classiques mais elle est en revanche très bonne par antenne satellite. RFI est également diffusée en F.M. par câble.</p>
<h4 class="spip">Télévision</h4>
<p>Satellite : toutes chaînes accessibles. <br class="manualbr">Câble : TV5 en accès libre, TF1, FRANCE 2, FRANCE 3, M6, ARTE, MUZZIK et MCM en accès payant.</p>
<h3 class="spip"><a id="sommaire_5"></a>Presse française</h3>
<p>A Copenhague, quelques kiosques proposent des titres français :</p>
<ul class="spip">
<li>Sankt Annae Kiosk, Store Kongensgade 40</li>
<li>Gare Centrale (Hovedbanegaard)</li>
<li>Magasin du Nord, Kongens Nytorv Sur abonnement, les délais sont d’un ou deux jours pour les quotidiens et les tarifs sont un peu plus élevés qu’en France. Pour souscrire, s’adresser éventuellement à :</li></ul>
<p><strong>Dansk Bladdistribution </strong><br class="manualbr">a/s Postboks 1918 0075 <br class="manualbr">DK-2300 Copenhague S</p>
<p>Il existe une librairie française assurant la diffusion de la presse et d’ouvrages français :</p>
<p><a href="http://denfranskebogcafe.dk/da/" class="spip_out" rel="external">Den Franske Bogcafé</a> <br class="manualbr">Fiolstraede 16, st<br class="manualbr">1171 Copenhague K<br class="manualbr">Tel : 36 99 16 92 <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/vie-pratique/article/loisirs-et-culture#bogcafe#mc#mac.com#" title="bogcafe..åt..mac.com" onclick="location.href=mc_lancerlien('bogcafe','mac.com'); return false;" class="spip_mail">Courriel</a></p>
<p><a href="http://www.institutfrancais.dk/fr/bibliotek/" class="spip_out" rel="external">Médiathèque de l’Institut français</a><br class="manualbr">Østergade 18, 2. sal - 1100 København K<br class="manualbr">Tél. : 33 38 77 06 <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/vie-pratique/article/loisirs-et-culture#%20mediatheque#mc#institutfrancais.dk#" title="%20mediatheque..åt..institutfrancais.dk" onclick="location.href=mc_lancerlien('%20mediatheque','institutfrancais.dk'); return false;" class="spip_mail">Courriel</a></p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/vie-pratique/article/loisirs-et-culture). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
