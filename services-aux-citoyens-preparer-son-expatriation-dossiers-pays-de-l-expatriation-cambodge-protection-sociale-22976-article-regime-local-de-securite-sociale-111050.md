# Régime local de sécurité sociale

<p><strong>Liens utiles : </strong></p>
<ul class="spip">
<li><a href="http://www.cleiss.fr/" class="spip_out" rel="external">Centre des liaisons européennes et internationales de sécurité sociale</a></li>
<li><a href="http://www.cfe.fr/" class="spip_out" rel="external">Caisse des Français de l’étranger</a></li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/cambodge/protection-sociale-22976/article/regime-local-de-securite-sociale-111050). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
