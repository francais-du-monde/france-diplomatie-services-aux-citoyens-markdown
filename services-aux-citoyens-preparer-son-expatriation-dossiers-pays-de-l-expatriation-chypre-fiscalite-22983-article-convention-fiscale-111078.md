# Convention fiscale

<p><strong>La France et Chypre ont signé, le 18 décembre 1981, une convention en matière de fiscalité</strong> publiée au Journal Officiel du 30 mars 1983. Le texte intégral de la convention est disponible sur le site de l’<a href="http://www.impots.gouv.fr/" class="spip_out" rel="external">administration fiscale</a> ; il peut également être obtenu auprès de la Direction des Journaux Officiels par courrier : 26, rue Desaix - 75727 Paris cedex 15, par télécopie : 01 40 58 77 80.</p>
<p>Cette convention tend à éviter les doubles impositions en matière d’impôts sur les revenus et sur la fortune qui pourraient résulter de l’application des législations de ces deux Etats.</p>
<p>Des pourparlers ont été engagés pour réviser cette convention, qui reste en vigueur jusqu’à l’adoption d’un nouveau texte.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chypre/fiscalite-22983/article/convention-fiscale-111078). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
