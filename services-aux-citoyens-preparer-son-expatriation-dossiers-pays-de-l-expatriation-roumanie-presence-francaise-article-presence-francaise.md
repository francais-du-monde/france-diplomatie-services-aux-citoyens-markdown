# Présence française

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/presence-francaise/article/presence-francaise#sommaire_1">Ambassade et consulat de France</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/presence-francaise/article/presence-francaise#sommaire_2">Autorités françaises dans le pays</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/presence-francaise/article/presence-francaise#sommaire_3">Associations dans le pays</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/presence-francaise/article/presence-francaise#sommaire_4">Associations franco-roumaines</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Ambassade et consulat de France</h3>
<p>Consulter notre <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/article/annuaire-des-representations-francaises-a-l-etranger" class="spip_in">annuaire de l’ambassade de France à l’étranger</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Autorités françaises dans le pays</h3>
<h4 class="spip">Réseau français de coopération et d’action culturelle</h4>
<p>Les coordonnées des services du réseau de coopération et d’action culturelle français en Roumanie ainsi que la description de l’activité de ces services se trouvent dans la rubrique culture du site internet de l’Ambassade ou sur le site internet du réseau culturel : <a href="http://latitudefrance.diplomatie.gouv.fr/AnnuaireCarto.html" class="spip_out" rel="external">Latitude France</a>, ainsi que le site de l’Institut français.</p>
<h4 class="spip">Economie : réseau mondial</h4>
<p>Business France, l’Agence française pour le développement international des entreprises, est présente en Roumanie. Sa mission est d’accompagner les entreprises françaises dans leur développement à l’international.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://export.businessfrance.fr/roumanie/export-roumanie-avec-notre-bureau.html" class="spip_out" rel="external">Mission Business France en Roumanie</a></p>
<p>Les services économiques sont également présents. Ils ont pour missions principales l’analyse macroéconomique et l’animation des relations économiques bilatérales entre la France et les pays où ils sont implantés.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.tresor.economie.gouv.fr/pays/roumanie" class="spip_out" rel="external">Site internet du service économique français en Roumanie</a></p>
<h4 class="spip">Vos élus à l’Assemblée des Français de l’étranger</h4>
<p>Pour toute information sur l’Assemblée des Français de l’étranger et pour connaître les conseillers et les Sénateurs qui représentent les Français établis hors de France dans votre circonscription, vous pouvez consulter les sites Internet suivants :</p>
<ul class="spip">
<li><a href="http://www.assemblee-afe.fr/" class="spip_out" rel="external">Assemblée des Français de l’étranger (AFE)</a></li>
<li>Sénat au service des Français de l’étranger : <a href="http://www.senat.fr/expatries/dossiers_pays/roumanie.html" class="spip_out" rel="external">dossier spécifique à la Roumanie sur le site du Sénat</a></li></ul>
<h4 class="spip">Députés des Français de l’étranger</h4>
<p>La révision constitutionnelle du 23 juillet 2008, publiée au Journal officiel du 19 avril 2011, introduit la représentation à l’Assemblée nationale des Français établis hors de France. La création de onze circonscriptions législatives à l’étranger permet aux Français expatriés d’élire leur député à l’Assemblée nationale depuis le scrutin de 2012.</p>
<p>Pour plus d’information : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/voter-a-l-etranger/" class="spip_in">Voter à l’étranger</a></p>
<h3 class="spip"><a id="sommaire_3"></a>Associations dans le pays</h3>
<h4 class="spip">Associations françaises</h4>
<p><strong>Association démocratique des Français à l’étranger - Français du Monde (ADFE - FdM)</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Bucarest</strong><br class="manualbr">Responsable : Gérard GOURON<br class="manualbr">c/o Lycée français - 22 rue C. Tel - Bucarest<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/presence-francaise/article/presence-francaise#adfe.roumanie#mc#gmail.com#" title="adfe.roumanie..åt..gmail.com" onclick="location.href=mc_lancerlien('adfe.roumanie','gmail.com'); return false;" class="spip_mail">Courriel</a> </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Constanta</strong><br class="manualbr">Responsable : Roger CORDIER<br class="manualbr">Strada Cuza Voda n°46 - 8700 Constanza<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/presence-francaise/article/presence-francaise#rogercordier#mc#yahoo.fr#" title="rogercordier..åt..yahoo.fr" onclick="location.href=mc_lancerlien('rogercordier','yahoo.fr'); return false;" class="spip_mail">Courriel</a></p>
<p><a href="http://www.ufe.ro/" class="spip_out" rel="external">Union des Français de l’étranger (UFE)</a></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Ambassade de France<br class="manualbr">Strada Biserica Amzei nr. 13-15, Bucarest<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/presence-francaise/article/presence-francaise#Benoit.mayrand#mc#ufe.ro#" title="Benoit.mayrand..åt..ufe.ro" onclick="location.href=mc_lancerlien('Benoit.mayrand','ufe.ro'); return false;" class="spip_mail">Courriel</a><br class="manualbr">Tél. : +40 743 09 09 33</p>
<ul class="spip">
<li>Calea Victoriei nr. 37 B, Sector 1, Bucarest</li>
<li><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/presence-francaise/article/presence-francaise#capeljyc#mc#yahoo.fr#" title="capeljyc..åt..yahoo.fr" onclick="location.href=mc_lancerlien('capeljyc','yahoo.fr'); return false;" class="spip_mail">Courriel</a></li>
<li>Tél. : +40 744 54 60 87</li></ul>
<p><a href="http://www.bucarestaccueil.com" class="spip_out" rel="external">Accueil francophone de Bucarest (FIAFE)</a> <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/presence-francaise/article/presence-francaise#accueilafb#mc#gmail.com#" title="accueilafb..åt..gmail.com" onclick="location.href=mc_lancerlien('accueilafb','gmail.com'); return false;" class="spip_mail">Courriel</a></p>
<h3 class="spip"><a id="sommaire_4"></a>Associations franco-roumaines</h3>
<p><a href="http://www.ccifer.ro/" class="spip_out" rel="external">Chambre française de commerce, d’industrie et d’agriculture en Roumanie</a> <br class="manualbr">Vasile Conta 4 - Sector 2 - 020951 Bucarest <br class="manualbr">Tél. : [40] (0)21 317 12 84 <br class="manualbr">Télécopie : [40] (0)21 317 10 62<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/presence-francaise/article/presence-francaise#ccifer#mc#algoritma.ro#" title="ccifer..åt..algoritma.ro" onclick="location.href=mc_lancerlien('ccifer','algoritma.ro'); return false;" class="spip_mail">Courriel</a></p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/presence-francaise/article/presence-francaise). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
