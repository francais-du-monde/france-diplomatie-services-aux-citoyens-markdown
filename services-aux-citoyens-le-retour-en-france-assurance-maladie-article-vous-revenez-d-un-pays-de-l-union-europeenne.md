# Vous revenez d’un pays de l’Union européenne

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/assurance-maladie/article/vous-revenez-d-un-pays-de-l-union-europeenne#sommaire_1">Séjour temporaire en France (tourisme, visites familiales) </a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/assurance-maladie/article/vous-revenez-d-un-pays-de-l-union-europeenne#sommaire_2">Transfert de résidence permanente</a></li></ul>
<p>Plusieurs <a href="http://www.cleiss.fr/reglements/index.html" class="spip_out" rel="external">règlements communautaires</a> favorisent la libre circulation des travailleurs au sein de l’Union européenne (UE) en organisant une coordination des régimes de sécurité sociale des pays de l’Union européenne, de l’Espace économique européen (Norvège, Islande, Liechtenstein) et de Suisse.</p>
<p>La coordination impose aux Etats de prendre en compte les périodes d’assurance, de cotisation, de résidence, de l’intéressé au sein d’autres Etats de l’Union européenne afin de lui ouvrir immédiatement des droits à l’Assurance maladie lors de son retour en France.</p>
<p>L’expatrié doit se procurer auprès de la caisse étrangère, un ou plusieurs <a href="http://www.cleiss.fr/reglements/883_documents_portables.html" class="spip_out" rel="external">formulaires</a> pour faire reconnaître ses droits à prestation. Ces attestations de droits varient selon la nature du déplacement, le risque protégé, le statut social de l’intéressé.</p>
<h3 class="spip"><a id="sommaire_1"></a>Séjour temporaire en France (tourisme, visites familiales) </h3>
<p><a href="http://www.cleiss.fr/particuliers/ceam.html" class="spip_out" rel="external">La carte européenne d’assurance maladie</a> (qui remplace l’ancien formulaire E 111) permet d’être soigné en France pour des soins « inopinés ». Le travailleur et les membres de sa famille qui effectuent un séjour temporaire en France ont droit aux prestations en nature si leur état vient à nécessiter des soins immédiatement nécessaires. Il convient de se la procurer dans le pays européen de résidence, auprès de l’organisme local de sécurité sociale.</p>
<p><strong>Transfert de résidence à but thérapeutique</strong></p>
<p><a href="http://www.cleiss.fr/reglements/s2.html" class="spip_out" rel="external">Le formulaire S2</a> (ex E 112) autorise l’expatrié assuré et les membres de sa famille à se rendre sur le territoire d’un autre Etat membre (en France notamment) pour se faire soigner. Il devra présenter une demande d’autorisation auprès de l’institution compétente, laquelle supportera la charge des prestations.</p>
<h3 class="spip"><a id="sommaire_2"></a>Transfert de résidence permanente</h3>
<p>Il existe de nombreux formulaires applicables à diverses situations de retour et qui obligent la CPAM à vous rouvrir des droits à l’Assurance maladie sur la base des périodes d’assurance accomplies dans un Etat membre de l’Union européenne.</p>
<p>Il s’agit du <a href="http://www.cleiss.fr/reglements/s1.html" class="spip_out" rel="external">formulaire S1</a> délivré à l’étranger par l’institution compétente en matière d’assurance maladie ou, dans certains cas par la caisse débitrice de la pension. Il permet à la personne assurée et/ou aux membres de sa famille qui résident sur le territoire d’un État membre autre que l’État compétent de s’inscrire auprès de l’institution d’assurance maladie de leur lieu de résidence afin de bénéficier des prestations en nature de l’assurance maladie maternité.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><strong>La protection sociale en France</strong> - l’ADECRI propose une brochure très complète.</li>
<li><a href="http://www.securite-sociale.fr/" class="spip_out" rel="external">La sécurité sociale</a></li>
<li>L’<a href="http://www.ameli.fr/" class="spip_out" rel="external">Assurance maladie</a> (remboursements, prestations, adresse de votre CPAM) 3646 ou depuis l’étranger 08 11 70 36 46</li>
<li><a href="http://www.cleiss.fr/" class="spip_out" rel="external">Centre des liaisons européennes et internationales de sécurité sociale (CLEISS)</a> <br class="manualbr">Tél. : 01 45 26 33 41 <br class="manualbr">Fax : 01 49 95 06 50</li></ul>
<p><i>Mise à jour : avril 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/assurance-maladie/article/vous-revenez-d-un-pays-de-l-union-europeenne). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
