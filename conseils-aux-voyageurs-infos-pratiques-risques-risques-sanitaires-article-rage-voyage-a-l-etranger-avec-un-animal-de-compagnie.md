# Rage - voyage à l’étranger avec un animal de compagnie

<p>Depuis 2001, onze cas de rage ont été importés en France, alors que la maladie est absente du territoire national, contrairement à de nombreux autres pays. <strong>Ces cas étaient tous liés à des animaux contaminés à l’étranger.</strong></p>
<p>Il est ainsi recommandé aux voyageurs de :</p>
<ul class="spip">
<li>ne pas ramener un animal d’un pays touché par la rage ;</li>
<li>penser à faire vacciner leur animal lorsqu’ils partent à l’étranger.</li></ul>
<p>Des démarches préalables sont nécessaires, <strong>avant de voyager </strong> avec son animal de compagnie :</p>
<ul class="spip">
<li>il convient de prendre contact avec un vétérinaire 4 mois avant le départ ;</li>
<li>l’animal doit être identifié par son passeport ;</li>
<li>les vaccins de l’animal doivent être à jour ;</li>
<li>une prise de sang est nécessaire pour les pays à risque.</li></ul>
<p><strong>Au retour en France</strong>, il faut impérativement présenter son animal de compagnie aux autorités douanières. <br class="autobr">Le non-respect des obligations réglementaires est passible de sanctions pénales (article L237-3 du Code rural et de la pêche maritime).</p>
<p>Pour plus d’informations (dépliant, vidéo, visuels, liste des pays à risque rage maîtrisé), consulter le <a href="http://agriculture.gouv.fr/transport-des-animaux-de-compagnie" class="spip_out" rel="external">site internet du ministère en charge de l’Agriculture</a>.</p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/infos-pratiques/risques/risques-sanitaires/article/rage-voyage-a-l-etranger-avec-un-animal-de-compagnie). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
