# Les autres modes de garde

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/les-modes-de-garde-des-enfants/article/les-autres-modes-de-garde#sommaire_1">L’assistante maternelle agréée</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/les-modes-de-garde-des-enfants/article/les-autres-modes-de-garde#sommaire_2">L’embauche d’une nourrice à domicile</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/les-modes-de-garde-des-enfants/article/les-autres-modes-de-garde#sommaire_3">La halte-garderie</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>L’assistante maternelle agréée</h3>
<p>Appelée autrefois nourrice, elle garde votre enfant à son domicile (3 enfants au maximum). Agréée par le département, elle est conseillée par les services de la Protection maternelle et infantile (PMI). Vous pouvez convenir d’horaires plus souples que ceux de la crèche dans la mesure où vous êtes son employeur.</p>
<p>Comme employeur, vous devez :</p>
<ul class="spip">
<li>établir un contrat de travail,</li>
<li>la déclarer à l’URSSAF</li>
<li>lui établir un bulletin de paye mensuel.</li></ul>
<p>Si l’enfant est âgé de moins de 6 ans, sous certaines conditions, les familles peuvent avoir droit à la prestation d’accueil du jeune enfant (PAJE).</p>
<p>Pour en savoir plus, vous pouvez consulter le site de la Caisse d’allocation familiale : <a href="http://www.caf.fr/aides-et-services/s-informer-sur-les-aides/petite-enfance" class="spip_out" rel="external">www.caf.fr/</a>, (Rubrique « Aides et services &gt; S’informer sur les aides &gt; Petite enfance »).</p>
<p>Pour connaître les adresses des nourrices, adressez-vous à la mairie ou au service de Protection maternelle et infantile (PMI).</p>
<p>Pour les formalités d’emploi, adressez-vous à l’URSSAF (<a href="http://www.urssaf.fr/" class="spip_out" rel="external">www.urssaf.fr/</a>) ou la Caisse d’allocations familiales (<a href="http://www.caf.fr/" class="spip_out" rel="external">www.caf.fr/</a>).</p>
<h3 class="spip"><a id="sommaire_2"></a>L’embauche d’une nourrice à domicile</h3>
<p>L’enfant peut être gardé au domicile familial par une personne que vous employez comme salariée. Dans ce cas, vous bénéficiez :</p>
<ul class="spip">
<li>d’une réduction d’impôt</li>
<li>sous certaines conditions, d’une allocation de garde d’enfant à domicile pour un enfant de moins de 6 ans.</li></ul>
<p>Pour en savoir plus : <a href="http://vosdroits.service-public.fr/particuliers/F12.xhtml" class="spip_out" rel="external">www.service-public.fr</a>, (Rubrique « Particuliers &gt; Argent &gt; Impôts, taxes et douane &gt; Impôt sur le revenu : déductions, réductions et crédit d’impôt &gt; Réduction d’impôt ou crédit d’impôt pour l’emploi d’un salarié à domicile »).</p>
<h3 class="spip"><a id="sommaire_3"></a>La halte-garderie</h3>
<p>La <a href="http://vosdroits.service-public.fr/particuliers/F853.xhtml" class="spip_out" rel="external">halte-garderie</a> accueille les enfants de moins de 6 ans de manière occasionnelle, à raison de 3 demi-journées par semaine au maximum. Sachez toutefois que ce mode de garde est destiné aux enfants dont l’un des parents ne travaille pas ou à temps partiel.</p>
<p>S’offrent à vous deux possibilités : vous pouvez souscrire à un contrat en début d’année qui réserve plusieurs demi-journées fixes hebdomadaires pour votre enfant ou bien le déposer occasionnellement, après avoir retenu une place. Les haltes-garderies étant très prisées, il convient de les contacter des mois à l’avance pour inscrire votre enfant. Les tarifs sont calculés en fonction des ressources.</p>
<p>Pour connaître les adresses, renseignez-vous à la mairie de votre lieu de domicile.</p>
<p><i>Mise à jour : avril 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/les-modes-de-garde-des-enfants/article/les-autres-modes-de-garde). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
