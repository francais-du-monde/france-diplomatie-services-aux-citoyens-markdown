# Scolarisation

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/vie-pratique/article/scolarisation-114441#sommaire_1">Les établissements scolaires français en Thaïlande</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/vie-pratique/article/scolarisation-114441#sommaire_2">Enseignement supérieur</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Les établissements scolaires français en Thaïlande</h3>
<p>Pour toute information sur la scolarisation dans le système français à l’étranger, vous pouvez consulter nos rubriques <a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures.md" class="spip_in">Etudes supérieures</a> et <a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais.md" class="spip_in">Scolarité en français</a>.</p>
<p>Vous y trouverez des renseignements sur :</p>
<ul class="spip">
<li>les établissements français du primaire et du secondaire à l’étranger ;</li>
<li>les bourses scolaires et la prise en charge des frais de scolarité à l’étranger ;</li>
<li>les possibilités qui s’offrent à vous si votre enfant ne peut être scolarisé à l’étranger dans le système français (enseignement à distance par le CNED, programme français langue maternelle (FLAM), internats en France) ;</li>
<li>les épreuves du baccalauréat à l’étranger ;</li>
<li>les bourses d’études supérieures en France et à l’étranger ;</li>
<li>l’équivalence des diplômes.</li></ul>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.aefe.fr/" class="spip_out" rel="external">Agence pour l’enseignement français à l’étranger</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Enseignement supérieur</h3>
<p>La Thaïlande compte, 78 universités publiques et 40 universités privées. En 2011, les universités thaïlandaises (publiques et privées confondues) offraient un total de 685 programmes internationaux accrédités et enseignés en anglais. Ce total se composait de 251 programmes de Licence, de 314 programmes de Master, de 105 programmes doctoraux et de 11 autres programmes diplômant. Les programmes internationaux sont ouverts aux étudiants étrangers et thaïlandais.</p>
<p>Les étudiants étrangers peuvent y poursuivre l’ensemble d’un programme et obtenir le diplôme, ou bien, y effectuer un semestre ou une année d’échange dans le cadre d’accords de coopération. En 2008, un total de 16 361 étudiants étrangers (principalement asiatiques) étaient inscrits dans les universités thaïlandaises. Ce chiffre est en forte progression.</p>
<p>Au sein des plus grandes universités de Bangkok ou de province, ces programmes internationaux sont en général regroupés au sein d’un « collège international ».</p>
<ul class="spip">
<li>L’Université Mahidol (économie, gestion, sciences)</li>
<li>L’Université Chulalongkorn (littérature, langues, sciences)</li>
<li>L’Université Kasetsart (environnement, agriculture, agronomie, ingénierie)</li>
<li>L’Université Thammasat (droit, sciences politiques, langues, commerce et relations internationales)</li>
<li>L’Université de Chiang Mai</li></ul>
<p>Dans le système d’enseignement supérieur, le premier diplôme est le <i>bachelor</i>, que l’on obtient après quatre années d’études supérieures. Toutefois, dans les domaines de la peinture, l’architecture, la sculpture, les arts graphiques et la pharmacie, il faut cinq ans d’enseignement supérieur pour obtenir le diplôme de <i>bachelor</i> ; en médecine, dentaire et vétérinaire il faut six ans pour obtenir ce diplôme.</p>
<p>Suite à deux ans d’études après le <i>bachelor</i> et rédaction d’un mémoire, on peut obtenir un master. Le doctorat est obtenu après trois années d’études après le master.</p>
<p><i>Mise à jour : juillet 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/vie-pratique/article/scolarisation-114441). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
