# Entretien d’embauche

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/emploi-stage/article/entretien-d-embauche#sommaire_1">Questions préférées des recruteurs</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/emploi-stage/article/entretien-d-embauche#sommaire_2">Erreurs à éviter</a></li></ul>
<p>Quelques conseils pour l’entretien d’embauche :</p>
<p><strong>Savoir justifier sa démarche d’installation au Mexique</strong><br class="manualbr">C’est de loin le point le plus important et c’est ce qui préoccupe le plus le recruteur. Il faut comprendre que votre formation et votre intégration dans l’entreprise a un coût pour l’entreprise. Le recruteur souhaite donc s’assurer que vous n’allez pas quitter l’entreprise au bout de 6 mois et que vous avez un projet sérieux à moyen ou long terme au Mexique (et donc éventuellement dans l’entreprise).</p>
<p>Ainsi, préparez-vous à répondre aux questions telles que :</p>
<ul class="spip">
<li>Pourquoi le Mexique ?</li>
<li>Depuis combien de temps êtes-vous au Mexique ?</li>
<li>Êtes-vous marié(e) ou en couple avec un(e) mexicain(e) ?</li>
<li>Pour combien de temps comptez-vous vous installer au Mexique ?</li>
<li>Qu’est-ce que vous aimez du Mexique ?</li></ul>
<p><strong>Être sûr de son niveau d’espagnol avant d’accepter un entretien</strong><br class="manualbr">L’entretien, même s’il s’agit d’un poste pour une entreprise française ou internationale, <strong>se déroulera en espagnol</strong>. Ainsi, soyez prêt à tenir une conversation en espagnol avant d’accepter un entretien. <br class="manualbr">Selon les postes on testera également votre niveau d’anglais, qui peut être <strong>la langue de travail</strong> dans certaines entreprises.</p>
<p><strong>Ne pas avoir peur des questions qui concernent la vie privée</strong> <br class="manualbr">Le recruteur ne va pas hésiter à vous poser des <strong>questions personnelles</strong> (vos goûts, vos hobbies, votre lieu de résidence, etc…). Parfois pour certains postes, le recruteur sera intéressé par vos contacts personnels (ex : postes en vente ou en RP, etc..) : on peut donc vous demander la profession de vos parents, dans quelle école vous avez étudié, les entreprises avec lesquelles vous avez travaillées dans votre emploi précédent, etc.</p>
<p><strong>Tenue stricte</strong><br class="manualbr">Tailleur pour les femmes, costume pour les hommes.</p>
<p><strong>Avoir un projet professionnel bien défini</strong><br class="manualbr">Bien entendu il faut être sûr de soi, et pouvoir présenter un projet professionnel bien défini au recruteur. Comment vous voyez vous évoluer dans l’entreprise ?</p>
<p><strong>Négociation du salaire</strong><br class="manualbr">La question du salaire sera toujours abordée en entretien car le salaire est un critère de sélection.</p>
<p>Pour vous préparer(notamment pour un premier emploi au Mexique) :</p>
<ul class="spip">
<li>Ne convertissez pas votre dernier salaire français en pesos mexicains ! Le niveau de vie et les coûts ne étant pas les mêmes en France qu’au Mexique, tout est question de pouvoir d’achat.</li>
<li>Ainsi, pensez avant toute chose : quels sont mes frais fixes mensuels(loyer, forfait téléphonie mobile, transport, etc…) ? De combien ai-je besoin pour vivre au Mexique ? Combien je souhaite mettre de côté par mois ?</li></ul>
<p>A noter qu’il n’y a pas vraiment de grilles salariales au Mexique, sauf dans les grands groupes qui ont leurs propres barèmes.</p>
<p><strong>Après l’entretien</strong><br class="manualbr">Suite à l’entretien, il est toujours bien d’envoyer un courrier de remerciement à la personne qui vous a reçu en entretien.</p>
<p><strong class="caractencadre-spip spip">Estimada/o Lic. XXX.</strong></p>
<p><strong class="caractencadre-spip spip">Por este medio le quiero agradecer por haberme recibido en sus oficinas el día de hoy, fue un gusto conocerlo y poder platicar más acerca de la vacante.</strong></p>
<p><strong class="caractencadre-spip spip">Estoy a sus órdenes para cualquier información adicional que pueda requerir y quedo en espera de sus noticias.</strong></p>
<p><strong class="caractencadre-spip spip">Reciba un cordial saludo.</strong></p>
<p>Par ailleurs, n’hésitez pas à relancer le recruteur par téléphone si vous n’avez pas encore eu de retour.</p>
<blockquote class="texteencadre-spip spip">
<h3 class="spip"><a id="sommaire_1"></a>Questions préférées des recruteurs</h3>
<p>Vous pouvez consulter les liens suivants pour des exemples de questions préférées des recruteurs :</p>
<ul class="spip">
<li><a href="http://www.kellyservices.com.mx/MX/Careers/Career-Forward/Preguntas-Frecuentes-en-una-Entrevista/" class="spip_out" rel="external">http://www.kellyservices.com.mx/MX/Careers/Career-Forward/Preguntas-Frecuentes-en-una-Entrevista/</a></li>
<li><a href="http://www.uv.mx/bolsadetrabajo/files/2012/12/25-preguntas-tipicas-entrevista.pdf" class="spip_out" rel="external">http://www.uv.mx/bolsadetrabajo/files/2012/12/25-preguntas-tipicas-entrevista.pdf</a></li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Erreurs à éviter</h3>
<p>Vous pouvez consulter les liens suivants pour des exemples d’erreur à éviter en entretien :</p>
<ul class="spip">
<li><a href="http://www.cnnexpansion.com/mi-carrera/2009/03/23/los-10-errores-antes-de-una-entrevista" class="spip_out" rel="external">http://www.cnnexpansion.com/mi-carrera/2009/03/23/los-10-errores-antes-de-una-entrevista</a></li>
<li><a href="http://blog.zonajobs.com/postulantes/5-errores-que-danan-la-entrevista-de-trabajo/" class="spip_out" rel="external">http://blog.zonajobs.com/postulantes/5-errores-que-danan-la-entrevista-de-trabajo/</a></li>
<li><a href="http://www.cnnexpansion.com/mi-carrera/2012/12/03/la-falta-que-el-reclutador-no-perdona" class="spip_out" rel="external">http://www.cnnexpansion.com/mi-carrera/2012/12/03/la-falta-que-el-reclutador-no-perdona</a></li>
<li><a href="http://blog.occeducacion.com/blog/bid/172425/Los-7-errores-m%C3%A1s-comunes-en-la-Entrevista" class="spip_out" rel="external">http://blog.occeducacion.com/blog/bid/172425/Los-7-errores-m%C3%A1s-comunes-en-la-Entrevista</a></li>
<li><a href="http://www.cnnexpansion.com/mi-carrera/2012/03/01/5-errores-mortales-frente-al-reclutador" class="spip_out" rel="external">http://www.cnnexpansion.com/mi-carrera/2012/03/01/5-errores-mortales-frente-al-reclutador</a></li></ul></blockquote>
<p><i>Mise à jour : septembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/emploi-stage/article/entretien-d-embauche). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
