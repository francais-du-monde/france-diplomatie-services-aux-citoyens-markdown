# Entrée et séjour

<h2 class="rub22992">Passeport, visa, permis de travail</h2>
<p>Quel que soit le motif de votre séjour, renseignez-vous auprès de la section consulaire de l’ambassade d’Iran à Paris qui vous informera sur la règlementation en matière d’entrée et de séjour en Iran, règlementation que vous devrez impérativement respecter.</p>
<p>Une fois sur place c’est le ministère des Affaires étrangères iranien qui prend le relais et délivre ou non le visa. <strong>En aucun cas, vous n’êtes autorisé à travailler en Iran sans permis adéquat.</strong></p>
<p>L’ambassade de France en Iran n’est pas l’organisme compétent pour répondre à vos demandes d’information concernant votre séjour en Iran.</p>
<p>Il est conseillé d’entamer les formalités d’obtention du visa plusieurs semaines à l’avance auprès de l’ambassade d’Iran à Paris et de se souvenir que le visa peut n’être obtenu qu’au dernier moment. Les visas sont délivrés, sur invitation, par les ambassades d’Iran à l’étranger, après que la demande ait été transmise à Téhéran, au ministère des Affaires étrangères iranien, pour approbation.</p>
<p>En règle générale, la durée de validité des visas de tourisme ou d’affaires ne dépasse pas un mois. Il est recommandé de vérifier la durée accordée au moment de la délivrance, car elle est souvent inférieure à la durée demandée ; ne pas confondre à cette occasion la date-limite à laquelle le visa autorise l’entrée en Iran et la durée de séjour autorisée. Tout visa peut être prorogé sur place auprès de la police des étrangers (ministère de l’Intérieur).</p>
<p>Il est aussi possible mais sans garantie d’obtenir un visa touristique, d’une durée allant de 7 à 14 jours dans l’un des aéroports internationaux du pays. Pour cela, vous aurez besoin d’un passeport valable de six mois, d’une photo d’identité, d’un billet de retour (peu importe la compagnie aérienne), du formulaire à remplir sur place et d’environ 60 dollars d’argent liquide pour les frais du dossier.</p>
<p><strong>A savoir :</strong> si le temps de séjour autorisé est dépassé, la police des frontières empêche le contrevenant de quitter le territoire iranien. Celui-ci devra alors se présenter devant un tribunal spécial et payer une amende journalière et obtenir ainsi un visa de sortie.</p>
<p>Il est interdit d’importer de l’alcool et bien entendu des stupéfiants. Il est interdit d’exporter sans autorisation des antiquités ou des tapis anciens. Pour tous les achats de valeur, demander une facture, permettant au retour en France de justifier, si nécessaire, le coût de l’objet auprès des douanes françaises. L’importation de caviar iranien vers la France, même à titre individuel, est soumis à une réglementation qui encadre sévèrement les quantités.</p>
<p><strong>Rappel : le concubinage, le pacs et le mariage des personnes du même sexe ne sont pas reconnus en Iran.</strong></p>
<p><strong>Les cartes bancaires internationales ne fonctionnent pas en Iran.</strong> Il faut par conséquent prévoir suffisamment de devises pour couvrir les frais de la durée du séjour.</p>
<p>Pour toute information complémentaire, prendre l’attache de l’<a href="http://fr.paris.mfa.ir/" class="spip_out" rel="external">ambassade d’Iran à Paris</a>.</p>
<p><strong>Contact ambassade : </strong></p>
<ul class="spip">
<li>Accueil : 01 40 69 79 00</li>
<li>Fax : 01 40 70 01 57</li>
<li><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/entree-et-sejour/#cabinet#mc#amb-iran.fr#" title="cabinet..åt..amb-iran.fr" onclick="location.href=mc_lancerlien('cabinet','amb-iran.fr'); return false;" class="spip_mail">Courriel</a></li></ul>
<p><strong>Service visas</strong></p>
<ul class="spip">
<li>Adresse : 16 rue Fresnel – 75116 PARIS</li>
<li>Tél. : 0140697960 – 0140697965</li>
<li><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/entree-et-sejour/#visa#mc#amb-iran.fr#" title="visa..åt..amb-iran.fr" onclick="location.href=mc_lancerlien('visa','amb-iran.fr'); return false;" class="spip_mail">Courriel</a></li></ul>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://fr.paris.mfa.ir/" class="spip_out" rel="external">Site de l’ambassade d’Iran à Paris</a></li>
<li><a href="http://www.amb-iran.fr/fr/consulat/visa.htm" class="spip_out" rel="external">Site du service des visas de l’ambassade d’Iran à Paris</a></li></ul>
<p><i>Mise à jour : février 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-iran-entree-et-sejour-article-animaux-domestiques.md" title="Animaux domestiques">Animaux domestiques</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-iran-entree-et-sejour-article-vaccination-111114.md" title="Vaccination">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-iran-entree-et-sejour-article-demenagement-111113.md" title="Déménagement">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-iran-entree-et-sejour-article-passeport-visa-permis-de-travail-111112.md" title="Passeport, visa, permis de travail">Passeport, visa, permis de travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/entree-et-sejour/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
