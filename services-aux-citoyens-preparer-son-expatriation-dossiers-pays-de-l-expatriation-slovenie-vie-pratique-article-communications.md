# Communications

<h4 class="spip">Téléphone – Internet</h4>
<p>Les communications téléphoniques sont bonnes.</p>
<p>Pour appeler depuis la France, composer l’indicatif du pays - 00 386 - puis le numéro du correspondant sans le 0.</p>
<p>Depuis la Slovénie, un appel vers la France est composé de l’indicatif 00 33 suivi du numéro du correspondant sans le 0.</p>
<p>L’usage d’Internet est très répandu en Slovénie. Il est possible de s’adresser à un prestataire local. On trouve facilement des cyber-cafés avec un accès internet wifi.</p>
<p><strong>Utilisation de votre portable français</strong></p>
<p>Pour se renseigner sur les différents prestataires de téléphonie mobile, il suffit de consulter leur site générique (en choisissant sa langue dans la bande déroulante en haut) :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.mobilna-telefonija.com/mobilni-operaterji.html" class="spip_out" rel="external">Mobilna-telefonija.com</a></p>
<p><strong>Téléphoner gratuitement par Internet </strong></p>
<p>Il est généralement très avantageux d’utiliser <strong>des logiciels de téléphonie sur IP</strong> (Skype, Google talk, Live messenger, Yahoo ! messenger, etc.) car les tarifs proposés à l’international sont souvent moindre que ceux des opérateurs locaux exploitant des lignes classiques, voire gratuits dans certaines conditions.</p>
<h4 class="spip">Poste</h4>
<p>L’acheminement du courrier prend en moyenne cinq jours et offre une garantie de bonne réception.</p>
<p><i>Mise à jour : septembre 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/slovenie/vie-pratique/article/communications). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
