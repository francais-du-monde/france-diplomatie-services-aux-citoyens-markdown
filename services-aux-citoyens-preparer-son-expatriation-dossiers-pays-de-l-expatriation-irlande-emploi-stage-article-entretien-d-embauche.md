# Entretien d’embauche

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/emploi-stage/article/entretien-d-embauche#sommaire_1">Conduite de l’entretien</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/emploi-stage/article/entretien-d-embauche#sommaire_2">Apparence et attitude</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/emploi-stage/article/entretien-d-embauche#sommaire_3">Questions préférées des recruteurs</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/emploi-stage/article/entretien-d-embauche#sommaire_4">Erreurs à éviter</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Conduite de l’entretien</h3>
<p>L’entretien d’embauche est un exercice qu’il faut soigneusement préparer.</p>
<p>L’entretien peut avoir lieu avec une seule personne ou un panel de recruteurs qui vous poseront des questions pour savoir si vous êtes la personne recherchée pour le poste. Ces questions porteront sur la motivation, les études et l’entreprise elle-même. Si certains points de la lettre de motivation n’ont pas été suffisamment développés, des questions pourront être posées pour en savoir davantage.</p>
<p>Il est essentiel de s’informer sur l’entreprise mais aussi sur toutes les questions d’actualité dans son secteur d’activité. Si vous parlez sans hésitation de l’entreprise, vous laisserez une bonne impression. Vous pourrez également apporter avec vous une copie de votre CV, de vos diplômes, des témoignages ou lettres de recommandation d’employeurs ainsi que tout autre document qui pourrait vous être utile.</p>
<p>Lors de l’entretien, vous devez vous exprimer avec assurance, faire en sorte de développer vos réponses par des exemples concrets venant de votre expérience professionnelle. L’employeur attend que vous soyez positif et convaincant.</p>
<p>A la fin de l’entretien, il est bien vu de poser des questions sur le poste ou l’entreprise afin de montrer votre intérêt.</p>
<h4 class="spip">Les questionnaires de personnalité</h4>
<p>Dans le processus de recrutement, un bon nombre d’entreprises élargissent leur méthode. "Profiler la personnalité " vient s’ajouter à l’éternel face à face de l’entretien.</p>
<p>Les questionnaires de personnalité (le plus commun étant l’OPQ - <i>Occupational Personality Questionnaire</i>) font ressortir certaines compétences de la personne qui permettent aux recruteurs de vérifier certains points mentionnés durant l’entretien.</p>
<p>Ce sont la plupart du temps des questionnaires à choix multiples. Ils permettent d’établir quelle "relation à l’autre " adopte le candidat, comment gère-t-il ses émotions, ce qui le motive et quelles sont ses perspectives dans la vie générale.</p>
<h3 class="spip"><a id="sommaire_2"></a>Apparence et attitude</h3>
<p>L’impression des recruteurs se base essentiellement sur le visuel. Par conséquent, l’aspect soigné et professionnel est de rigueur.</p>
<p>En ce qui concerne la tenue vestimentaire, adoptez un style plutôt classique.</p>
<p>Dans la culture anglo-saxonne, le comportement et l’attitude sont privilégiés : soyez positif et énergique.</p>
<h3 class="spip"><a id="sommaire_3"></a>Questions préférées des recruteurs</h3>
<p>Questions les plus couramment posées lors d’un entretien :</p>
<ul class="spip">
<li><i>Why are you here ?</i></li>
<li><i>If you had only one word to describe yourself, what would it be ?</i></li>
<li><i>When have you failed ? Describe what happened and what you learned from it.</i></li>
<li><i>What qualities in your co-workers bother you most ? </i><i>Do you appreciate most ?</i></li>
<li><i>What changes have you made in working with others to be more effective at work ?</i></li>
<li><i>How do you make decisions ?</i></li>
<li><i>Describe a crisis you faced at work. What was your role ? How did you resolve it ? </i><i>What were the results ?</i></li>
<li><i>What will make you love coming to work here everyday ?</i></li>
<li><i>What would you do if management made a decision you did not agree with ?</i></li>
<li><i>What is there about this opportunity that most excites you ?</i></li>
<li><i>Is there any question I haven’t asked you that I should ?</i></li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Erreurs à éviter</h3>
<ul class="spip">
<li>les commentaires négatifs sur votre employeur actuel ou passé ainsi que sur vos collègues ;</li>
<li>les discussions concernant le salaire durant la première étape de l’entretien. Laissez l’employeur en parler ;</li>
<li>les questions sur les jours de vacances et les congés maladie au début de l’entretien</li>
<li>les choses que vous ne voulez pas faire ;</li>
<li>les déclarations malhonnêtes et trompeuses…</li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/emploi-stage/article/entretien-d-embauche). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
