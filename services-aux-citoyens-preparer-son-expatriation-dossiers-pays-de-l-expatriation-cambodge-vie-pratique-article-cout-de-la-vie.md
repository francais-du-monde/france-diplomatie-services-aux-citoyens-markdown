# Coût de la vie

<h4 class="spip">Monnaie et change</h4>
<p>L’unité monétaire est le riel.</p>
<p>L’économie cambodgienne est pratiquement complètement dollarisée depuis 1995. La monnaie nationale est le riel (KHR). Les meilleures monnaies de facturation sont l’USD et parfois l’euro.</p>
<p>La monnaie nationale est librement convertible. Il n’y a pas de contrôle des changes.</p>
<h4 class="spip">Opérations bancaires</h4>
<p>Les cartes bancaires ne peuvent être utilisées que dans les grands hôtels. Les possibilités d’utilisation de chèques en francs français sont très réduites. Les <i>travellers cheques</i> ne sont acceptés que dans certaines banques. Les banques étrangères installées dans le pays sont : Crédit Agricole Indosuez, Standard Chartered Bank, Canadian Bank, May Bank, etc.</p>
<h4 class="spip">Alimentation</h4>
<p>Les conditions d’approvisionnement en produits d’importation soit de France, soit des grands pays voisins, sont très satisfaisantes. Pratiquement tous les produits de consommation courante peuvent se trouver sur place.</p>
<p><strong>Prix moyen d’un repas dans un restaurant</strong></p>
<p>Un repas dans un restaurant de qualité supérieure revient à 25 euros et à 12 euros en moyenne dans un restaurant de qualité moyenne. Selon la qualité du service, le pourboire varie de 5 à 10 %.</p>
<h4 class="spip">Evolution des prix</h4>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/cambodge/vie-pratique/article/cout-de-la-vie). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
