# Déménagement

<p>Il peut être utile de prévoir dans le déménagement les produits pharmaceutiques et de beauté, les produits pour bébé, les chaussures pour femmes, la plupart des équipements sportifs, les lainages d’hiver, lingerie et bas. Il peut être appréciable également d’avoir couvertures, couettes et draps housses, boîte à outils, cristallerie peu fragile (on trouve néanmoins de tout sur place).</p>
<p>En dépit des délais, le transport maritime est la meilleure solution pour les déménagements importants car moins onéreux. Il faut cependant éviter de déménager en période de mousson en raison des risques de stockage à l’air libre.</p>
<p>Les déménageurs français assurent en général très bien les déménagements sur l’Inde, avec l’aide d’un correspondant local. Pour New Delhi, Calcutta et Pondichéry (via Chennai), le délai est de deux mois de porte à porte. Pour Bombay, il est d’environ de un mois.</p>
<p>Formalités : à produire aux autorités douanières indiennes :</p>
<ul class="spip">
<li>le connaissement maritime</li>
<li>l’inventaire détaillé chiffré (en anglais)</li>
<li>le <i>bill of entry</i></li>
<li>une copie de la police d’assurance ou du certificat d’assurance</li>
<li>une copie du passeport.</li></ul>
<p>Une surveillance étroite lors de l’ouverture du conteneur et de son déchargement est indispensable.</p>
<p>Le transport par voie aérienne est plus rapide voire immédiat. Les documents requis sont les mêmes, la Lettre de Transport Aérien (LTA) se substituant au connaissement maritime.</p>
<p>Une taxe de 60 % sera perçue sur la valeur résiduelle de tout matériel électrique informatique et l’électro-ménager. Les autres effets, vêtements, meubles, livres, bibelots, ne sont pas taxés.</p>
<p>S’agissant d’un déménagement international, il est primordial de s’entourer d’un maximum de garanties en faisant appel à un professionnel disposant de certifications reconnues (marque NF Service, ISO 9002, etc.)</p>
<p><a href="http://www.csdemenagement.fr/" class="spip_out" rel="external">Chambre syndicale du déménagement</a><br class="manualbr">Téléphone : 01 49 88 61 40 <br class="manualbr">Télécopie : 01 49 88 61 46<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/entree-et-sejour/article/demenagement-110400#contact#mc#csdemenagement.fr#" title="contact..åt..csdemenagement.fr" onclick="location.href=mc_lancerlien('contact','csdemenagement.fr'); return false;" class="spip_mail">Courriel</a></p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/entree-et-sejour/article/demenagement-110400). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
