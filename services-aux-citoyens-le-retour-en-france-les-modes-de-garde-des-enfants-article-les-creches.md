# Les crèches

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/les-modes-de-garde-des-enfants/article/les-creches#sommaire_1">La crèche collective</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/les-modes-de-garde-des-enfants/article/les-creches#sommaire_2">La crèche familiale</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/les-modes-de-garde-des-enfants/article/les-creches#sommaire_3">La crèche parentale</a></li></ul>
<p>Quand les deux parents travaillent, il faut songer à un mode de garde pour les jeunes enfants. <br class="autobr">Il existe diverses possibilités pour faire garder son enfant. Tous les modes de garde sont payants. Des aides financières peuvent être versées par les caisses d’allocations familiales.</p>
<h3 class="spip"><a id="sommaire_1"></a>La crèche collective</h3>
<p>Les enfants âgés de deux mois et demi à trois ans dont les deux parents travaillent, sont accueillis pour la journée, tout au long de l’année. Les horaires relativement stricts (7h30–18h30 en moyenne) ne sont pas adaptés pour les parents travaillant la nuit ou le samedi. Toutefois ce mode de garde est très demandé et le nombre de places est limité. Aussi est-il conseillé de se renseigner avant la naissance de l’enfant et de demander un dossier d’inscription auprès de sa mairie (si la crèche est municipale) ou de la direction de la crèche (si la crèche est privée ou parentale) et le rapporter avec les pièces justificatives demandées.</p>
<p>Les tarifs sont calculés en fonction des revenus et des charges de la famille. Les familles peuvent bénéficier de réductions d’impôts.</p>
<h3 class="spip"><a id="sommaire_2"></a>La crèche familiale</h3>
<p>Ce type de garde associe la garde chez une assistante maternelle (agréée et rémunérée par la crèche) qui peut accueillir jusqu’à 3 enfants et des activités en collectivité. <br class="autobr">Ce mode de garde permet des horaires plus souples que dans une crèche collective.</p>
<p>Les tarifs sont calculés en fonction des revenus et des charges de la famille. Les inscriptions sont effectuées auprès de la crèche ou en mairie.</p>
<h3 class="spip"><a id="sommaire_3"></a>La crèche parentale</h3>
<p>La crèche parentale est une crèche collective privée gérée par les parents. Ceux-ci participent, à tour de rôle, à la garde des enfants avec l’assistance d’une personne qualifiée. <br class="autobr">La participation financière des parents est calculée en fonction de leurs revenus. Ils peuvent bénéficier de réductions d’impôts. <br class="autobr">Les inscriptions se font directement auprès de la crèche (adresse disponible en mairie).</p>
<p><i>Mise à jour : avril 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/les-modes-de-garde-des-enfants/article/les-creches). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
