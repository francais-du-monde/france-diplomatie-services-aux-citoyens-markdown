# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/emploi-stage/article/reglementation-du-travail-110404#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/emploi-stage/article/reglementation-du-travail-110404#sommaire_2">Fêtes légales</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/emploi-stage/article/reglementation-du-travail-110404#sommaire_3">Emploi du conjoint</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<p>Il est indispensable, compte tenu du resserrement des règles de résidence des étrangers, d’obtenir un visa de travail (« E » - <i>Employmen</i>t pour les entreprises du secteur privé, et « E » ou « X » pour les personnels d’ONG) quand on vient travailler en Inde. L’intervention de l’employeur et la fourniture d’un contrat de travail sont nécessaires.</p>
<p>Dans l’industrie, un ouvrier ne peut être contraint de travailler plus de neuf heures par jour ou plus de 48 heures par semaine (jusqu’à 60 h en intégrant les heures supplémentaires). Il est courant de travailler six jours par semaine. On ne peut obliger une ouvrière à travailler avant 6 heures du matin ni après 19 heures. Les entreprises recourent également à des travailleurs indépendants pour contourner la législation sociale.</p>
<p>Tout ouvrier a droit à un jour de congé payé pour 20 journées de travail effectuées par lui durant l’année précédente, à condition qu’il ait travaillé au moins 240 jours. Dans le commerce, un employé a droit à 21 jours de congés payés annuels pour 240 jours de travail effectués.</p>
<p>A noter toutefois qu’il existe en Inde plus d’une cinquantaine de texte auxquels viennent s’ajouter de nombreuses réglementations introduites par les 28 Etats fédérés et 7 territoires de l’Union indienne. Les textes les plus importants sont <i>l’Industrial Disputes Act</i> de 1947, le <i>Glops and Establishments Act</i>, le <i>Factories Act</i> de 1948, le <i>Trade Union Act</i> de 1926.</p>
<h4 class="spip">Les primes de départ</h4>
<p>Un ouvrier/employé qui présente un minimum de cinq ans d’ancienneté pourra percevoir une prime de départ (<i>gratuity</i>) dont le montant sera équivalent à 15 jours de salaire multiplié par le nombre de ses années de service, sous réserve d’un montant maximal de 350 000 roupies (<i>Gratuity Act</i>).</p>
<p>L’âge maximum légal de départ à la retraite est fixé à 60 ans, 58 ans auparavant. Un schéma d’épargne est obligatoire pour couvrir les besoins sociaux des employés pour les entreprises de plus de 20 salariés.</p>
<p>Lorsque l’employé est licencié par la société, une prime de licenciement équivalent à 15 jours de salaire par année travaillée (<i>rentrenchment compensation</i>) est normalement versée (<i>Industrial Dispute Act).</i></p>
<h4 class="spip">Contrat de travail et représentation syndicale</h4>
<p>La réalisation effective des tâches constitue un emploi, qu’il existe ou non un contrat de travail.</p>
<p>Jusqu’à 12 semaines sous réserve d’avoir travaillé au moins 20 jours dans l’entreprise l’année précédente.</p>
<p>Le droit à la représentation syndicale est garanti par la Constitution et par le <i>Trade Union Act</i> de 1926 ; lié aux structures politiques.</p>
<h3 class="spip"><a id="sommaire_2"></a>Fêtes légales</h3>
<p>La plupart des dates de fêtes légales varient en fonction du calendrier lunaire. Elles sont différentes selon les Etats.</p>
<p><strong>Principales fêtes et jours fériés à New-Delhi</strong></p>
<ul class="spip">
<li>1er janvier : Jour de l’An</li>
<li>26 janvier : Fête de la République (Republic Day)</li>
<li>14 février : Mahashivaratri</li>
<li>1er mars (date variable selon le calendrier lunaire) : Holi (Fête des couleurs)</li>
<li>2 avril : Good Friday</li>
<li>17 avril : Shivaji Jayanti</li>
<li>1er mai : Maharashtra day</li>
<li>15 août : Jour de l’Indépendance de l’Inde</li>
<li>22 août : Parsi New Year</li>
<li>2 octobre : Anniversaire de Mahatma Gandhi</li>
<li>19 octobre : Dussehra</li>
<li>5 novembre (date variable selon le calendrier lunaire) : Diwali (équivalent à Noël pour les Hindous)</li>
<li>20 janvier : Idu’l Fitr pour les Musulmans</li>
<li>29 mars : Idu’l Zuha pour les Musulmans</li>
<li>23 novembre Guru Nanak pour les Sikhs</li></ul>
<p><strong>Principales fêtes à Calcutta</strong></p>
<ul class="spip">
<li>26 janvier : Fête de la République</li>
<li>2 mars : Doljatra (Holi)</li>
<li>29 mars : Id-Uz-Zoha (Bakrid)</li>
<li>15 avril : Bengali New Year</li>
<li>15 août : Fête de l’Indépendance</li>
<li>2 octobre : Anniversaire de Gandhi</li>
<li>Octobre : Durga Puja</li>
<li>7 novembre : Kali Puja</li>
<li>25 décembre : Noël</li></ul>
<p><strong>Principales fêtes à Bombay</strong></p>
<ul class="spip">
<li>26 janvier : Fête de la République</li>
<li>14 février : Mahashivaratri*</li>
<li>2 mars : Holi*</li>
<li>18 mars : Gudi Padwa*</li>
<li>17 avril : Shivaji Jayanti</li>
<li>13 septembre : Ganesh chaturthi*</li>
<li>1er mai : Maharashtra day</li>
<li>15 août : Fête de l’Indépendance indienne</li>
<li>22 août : Parsi New Year*</li>
<li>2 octobre : Gandhi Jayanti</li>
<li>14 octobre : Dassehra</li>
<li>1er novembre : Toussaint</li>
<li>7 novembre : Diwali (Laxmi Poojan)*</li>
<li>8 novembre : Diwali (Balipratipada)*</li>
<li>10 novembre : Diwali (Bhaubeej) *</li>
<li>25 décembre : Noël</li></ul>
<p>* fêtes mobiles</p>
<p><strong>Principales fêtes à Pondichery</strong></p>
<ul class="spip">
<li>Vendredi 15 janvier : Pongal</li>
<li>Mardi 26 janvier : Republic Day</li>
<li>Vendredi 5 novembre : Deepavali</li></ul>
<p><strong>Principales fêtes à Bangalore</strong></p>
<ul class="spip">
<li>14 janvier : Makara Sankranthi</li>
<li>26 janvier : Republic Day</li>
<li>16 mars : Ugadi</li>
<li>2 avril : Good Friday</li>
<li>1er mai : May Day</li>
<li>15 aôut : Independence Day</li>
<li>11 septembre : Ganesh Chaturthi</li>
<li>16 octobre : Ayudha Pooja</li>
<li>1er novembre : Rajyotsava Day</li>
<li>5 novembre : Dussehra</li>
<li>25 décembre : Christmas</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Emploi du conjoint</h3>
<p>La législation locale ne permet pas aux conjoints d’exercer une activité professionnelle sauf s’ils viennent pour le compte d’une société française installée en Inde ou exécutant un contrat. Dans ce cas, l’accord préalable de la Banque Centrale est indispensable, exception faite pour la restauration. La législation locale ne permet pas au conjoint de travailler, sauf en tant qu’expatrié arrivant en Inde avec un visa de travail (<i>employment visa</i>). Le conjoint désirant se rendre en Inde peut obtenir un visa d’entrée (visa X).</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/inde/emploi-stage/article/reglementation-du-travail-110404). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
