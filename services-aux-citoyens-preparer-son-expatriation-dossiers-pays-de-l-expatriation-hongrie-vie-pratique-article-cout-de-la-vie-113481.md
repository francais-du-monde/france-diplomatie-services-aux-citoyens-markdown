# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/vie-pratique/article/cout-de-la-vie-113481#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/vie-pratique/article/cout-de-la-vie-113481#sommaire_2">Moyens de paiements </a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/vie-pratique/article/cout-de-la-vie-113481#sommaire_3">Banques françaises et étrangères installées dans le pays</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/vie-pratique/article/cout-de-la-vie-113481#sommaire_4">Opérations bancaires</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/vie-pratique/article/cout-de-la-vie-113481#sommaire_5">Alimentation</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/vie-pratique/article/cout-de-la-vie-113481#sommaire_6">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>L’unité monétaire est le forint hongrois (HUF).</p>
<h3 class="spip"><a id="sommaire_2"></a>Moyens de paiements </h3>
<p>Concernant le contrôle des changes, il n’existe pas de limitation, mais des justificatifs peuvent être demandés. L’approvisionnement en devises se fait sur place dans les bureaux de change (banques, hôtels, agences de voyage, offices de tourisme) ou par voie bancaire.</p>
<p>Il est possible de retirer du liquide avec une carte de crédit internationale, dans les nombreux distributeurs automatiques de billets à Budapest et en province. Les cartes de crédit internationales sont acceptées dans les hôtels et dans un nombre croissant de restaurants et magasins.</p>
<h3 class="spip"><a id="sommaire_3"></a>Banques françaises et étrangères installées dans le pays</h3>
<p>Les étrangers peuvent ouvrir un compte en forints ou en devises dans les banques.</p>
<p>Les banques sont généralement ouvertes du lundi au jeudi à partir de 8h en continu jusqu’à 16h-18h (en fonction des établissements et de la taille de la commune), et le vendredi de 8h à 15h.</p>
<p>Les principales banques françaises et étrangères installées dans le pays sont BNP, CIC, Ing, Citibank, Raiffeisen, Erste, CIB (Intesa Sao Paolo). Les banques françaises ne gèrent pas de comptes de particuliers.</p>
<h3 class="spip"><a id="sommaire_4"></a>Opérations bancaires</h3>
<p>Le montant de la taxe sur les transactions financières pour un virement bancaire est de 0,3% (montant maximum de 6000 HUF, soit environ 20 EUR par transaction) et de 0,6% pour un retrait d’argent.</p>
<p>Il faut noter qu’à partir du 1er février 2014, les personnes résidant en Hongrie ont droit à deux retraits d’espèces gratuits par mois jusqu’à un montant global de 150 000 HUF, soit environ 500 EUR. Pour pouvoir en bénéficier, il est nécessaire d’en faire la demande auprès de sa banque (démarche possible en ligne dans certaines banques) et de posséder une carte de résidence (<i>lakcímkártya</i>). La carte de résidence doit être demandée auprès des services de l’état civil (<i>okmányiroda</i>).</p>
<h3 class="spip"><a id="sommaire_5"></a>Alimentation</h3>
<p>L’approvisionnement des produits s’effectue principalement en fonction des saisons, l’habitude locale étant de consommer les produits de saison.</p>
<p>On trouve très rarement du poisson frais et il s’agit principalement de poissons d’eau douce ; les produits de la mer sont en général surgelés, avec un choix limité. Les vins et fromages français sont rares et chers néanmoins il est possible de s’en procurer dans les épiceries fines, les magasins spécialisés, ainsi que dans les hypermarchés, notamment dans les enseignes françaises ou encore dans certains <i>discounters</i>.</p>
<p>Le réseau de distribution est constitué par des hypermarchés situés dans les agglomérations des grandes villes du pays, de petits supermarchés dans les centres villes et des épiceries de quartier, qui sont tous en général ouverts le dimanche. Les commerces spécialisés, tels que les boucheries sont relativement rares.</p>
<p><strong>Prix moyen d’un repas dans un restaurant</strong></p>
<p>A Budapest, le prix d’un repas dans un restaurant de qualité supérieure est d’environ 14 000 HUF (soit approximativement 46 euros) et le prix dans un restaurant de qualité moyenne de 8000 HUF, en province, les prix sont respectivement de 10 000 HUF, 6000 HUF. En semaine, le midi, les restaurants dans la capitale proposent en général des menus de 1500 HUF à 2500 HUF, et en province de 1000 HUF à 1500 HUF. Le pourboire représente environ 10 % de l’addition. Il convient de préciser le montant du pourboire au moment de payer, plutôt que de laisser l’argent sur la table.</p>
<h3 class="spip"><a id="sommaire_6"></a>Evolution des prix</h3>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/vie-pratique/article/cout-de-la-vie-113481). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
