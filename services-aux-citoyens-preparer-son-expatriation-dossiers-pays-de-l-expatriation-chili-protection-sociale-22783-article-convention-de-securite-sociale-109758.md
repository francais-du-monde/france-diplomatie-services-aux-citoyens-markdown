# Convention de sécurité sociale

<p>Le texte de la convention générale de sécurité sociale entre la France et le Chili du 25 juin 1999 (entrée en vigueur le 1er septembre 2001), complétée par un arrangement administratif du 22 octobre 1999, est intégralement disponible sur le site internet du <a href="http://www.cleiss.fr/docs/textes/index.html" class="spip_out" rel="external">Centre des liaisons européennes et internationales de sécurité sociale</a> (CLEISS).</p>
<p>Pour plus d’information, consultez notre <a href="services-aux-citoyens-preparer-son-expatriation-protection-sociale.md" class="spip_in">rubrique thématique sur la protection sociale</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/protection-sociale-22783/article/convention-de-securite-sociale-109758). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
