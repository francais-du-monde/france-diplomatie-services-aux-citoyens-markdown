# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/emploi-stage/article/reglementation-du-travail-113521#sommaire_1">Temps du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/emploi-stage/article/reglementation-du-travail-113521#sommaire_2">Contrat de travail – spécificités</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/emploi-stage/article/reglementation-du-travail-113521#sommaire_3">Fêtes légales</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/emploi-stage/article/reglementation-du-travail-113521#sommaire_4">Emploi du conjoint</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Temps du travail</h3>
<p>Légalement, la durée du travail hebdomadaire ne doit pas excéder 40 heures. Bien souvent, ce temps maximal se voit réduit par les conventions collectives conclues entre les syndicats et les organisations patronales. Les horaires flexibles sont très courants.</p>
<p>Des heures supplémentaires peuvent être imposées par l’employeur si nécessaire. Le nombre total d’heures supplémentaires par an ne peut dépasser 200heures. Le travail le dimanche est réglementé par des conventions collectives. Dans la plupart de celles-ci, le samedi et le dimanche sont considérés comme des jours de repos. Il existe toutefois de nombreuses exceptions à cette règle</p>
<p>Chaque salarié a droit à au moins 25 jours ouvrables de congés payés par an, soit cinq semaines. Si les congés peuvent en principe être pris à tout moment de l’année, l’employeur et les syndicats peuvent négocier sur ce point. Le salarié bénéficie, pour ses congés, d’une prime de 2,4 % de son salaire annuel.</p>
<h3 class="spip"><a id="sommaire_2"></a>Contrat de travail – spécificités</h3>
<p>D’après le droit du travail (SFS 1982 : 80), un contrat de travail doit être à durée indéterminée (CDI). Une période d’essai allant de trois à six mois maximum peut être prévue, avant la signature du contrat. Cependant, dans certains cas particuliers (contrat saisonnier, contrat pour une mission définie, remplacement), un contrat à durée déterminée (CDD) peut être accepté. Le CDD se termine automatiquement à l’arrivée de son terme, sans aucune formalité. Si un CDD se poursuit au-delà de trois ans, l’employeur est tenu de proposer un CDI.</p>
<p>L’employeur doit remplir certaines obligations à l’embauche d’un salarié. Il doit, au maximum un mois après le début effectif de son travail par le salarié, l’informer par écrit des termes et conditions de son emploi (rémunération, convention collective, période d’essai, congés payés, …). De plus, pour résilier un contrat de travail pendant la période d’essai, l’employeur devra donner un préavis au plus tard le dernier jour de la période d’essai. Si l’employeur ne licencie pas le salarié pendant cette période, le contrat se transforme automatiquement en un contrat à durée indéterminée soumis à la loi suédoise sur la protection de l’emploi (<i>Lagen om anställningsskydd</i> - LAS).</p>
<p><i>Pour en savoir plus :</i></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://ec.europa.eu/eures/" class="spip_out" rel="external">Site internet d’EURES</a></p>
<h3 class="spip"><a id="sommaire_3"></a>Fêtes légales</h3>
<ul class="spip">
<li>1er janvier</li>
<li>Epiphanie</li>
<li>Vendredi saint et lundi de Pâques</li>
<li>1er mai</li>
<li>Ascension</li>
<li>Lundi de Pentecôte</li>
<li>la fête nationale (6 juin)</li>
<li>21 juin (Saint-Jean ou Midsommar)</li>
<li>Toussaint</li>
<li>Sainte-Lucie (13 décembre)</li>
<li>Noël (25 et 26 décembre)</li></ul>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.tv5.org/TV5Site/voyageurs/accueil.php" class="spip_out" rel="external">TV5.org</a></p>
<h3 class="spip"><a id="sommaire_4"></a>Emploi du conjoint</h3>
<p>Les possibilités d’emploi existent. En dépit de l’usage très répandu de l’anglais, en pratique une bonne connaissance du suédois est le plus souvent attendue par les employeurs. Il n’existe aucune restriction à l’exercice d’une activité professionnelle par le conjoint.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/emploi-stage/article/reglementation-du-travail-113521). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
