# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/emploi-stage/article/reglementation-du-travail-111384#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/emploi-stage/article/reglementation-du-travail-111384#sommaire_2">Fêtes légales</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/emploi-stage/article/reglementation-du-travail-111384#sommaire_3">Création d’entreprises – spécificités</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<h4 class="spip">Temps de travail</h4>
<p>La loi sur la durée du temps de travail (<i><a href="http://www.ris.bka.gv.at/GeltendeFassung.wxe?Abfrage=BundesnormenGesetzesnummer=10008238" class="spip_out" rel="external">Arbeitszeitgesetz</a></i>) est applicable à presque tous les salariés de l’économie privée âgés de plus de 18 ans.</p>
<p>La durée normale du temps de travail correspond à</p>
<ul class="spip">
<li>une journée de travail de huit heures (sur 24 heures),</li>
<li>une semaine de travail de 40 heures (du lundi au dimanche inclus). Dans de nombreux secteurs, les conventions collectives limitent le temps de travail hebdomadaire à 38 heures, par exemple.</li></ul>
<p>Exceptions : les conventions collectives peuvent autoriser que la durée du temps de travail journalier soit portée à dix heures. Pour permettre d’obtenir un temps libre prolongé ininterrompu (par exemple, un week-end prolongé), la durée normale du temps de travail peut être portée de huit heures à neuf heures par jour au maximum.</p>
<p>Dans certaines conditions, une semaine de quatre jours de travail (4 x 10 heures) est possible.</p>
<p>Bon nombre de conventions collectives prévoient que le temps de travail hebdomadaire normal doit être atteint en moyenne pour une période déterminée (allant jusqu’à un an, dans certains cas parfois plus) tout en pouvant être parfois supérieure, parfois inférieure selon les semaines (dans le tourisme, l’hôtellerie et la gastronomie, le commerce, par exemple).</p>
<h5 class="spip">Pauses et temps de repos</h5>
<p>Lorsque la durée totale du temps de travail est comprise entre 6 et 9 heures, le travail doit être interrompu par une pause d’au moins une demi-heure. Si la durée totale du temps de travail dépasse neuf heures, une pause d’au moins 45 minutes est obligatoire. Cette pause n’est pas rémunérée et n’est pas prise en considération comme temps de travail. Au terme du temps de travail quotidien, les travailleurs ont droit à un temps de repos d’au moins onze heures consécutives. Le repos hebdomadaire est réglementé par la législation sur le repos des travailleurs. Les travailleurs ont en principe droit à un repos de 36 heures consécutives, allant du samedi à 13 heures au dimanche inclus (repos de fin de semaine). Cette réglementation prévoit également des exceptions.</p>
<h5 class="spip">Travail à temps partiel</h5>
<p>Toute discrimination des travailleurs à temps partiel par rapport aux travailleurs à temps plein est interdite. Par travail supplémentaire (<i>Mehrarbeit</i>), on entend les heures d’appoint (<i>Mehrstunden</i>) effectuées au-delà de la durée du temps de travail convenue contractuellement (25 heures par exemple) ou de la durée de travail réduite convenue par les conventions collectives (38 heures par exemple), mais en-deça de la durée normale du travail prévue par la loi. Tout travail effectué au delà de la durée normale du travail, telle que prévue par la loi, correspond à des heures supplémentaires (<i>Überstunden</i>). Les heures d’appoint donnent droit à un supplément de rémunération de 25 % (toutefois seulement dans la mesure où une compensation du crédit d’heures n’est pas possible au cours d’un trimestre, ou au cours d’une autre période de trois mois convenue).</p>
<h5 class="spip">Travail en équipes</h5>
<p>Le travail en équipes prévoit que les postes de travail sont occupés par différents travailleurs dans une période déterminée. Dans certaines conditions, la durée du temps de travail d’une équipe peut être de douze heures.</p>
<h5 class="spip">Horaires flottants</h5>
<p>On appelle horaire flottant la possibilité de déterminer soi-même le début et la fin de la durée normale du temps de travail journalier dans un certain cadre. La présence est obligatoire dans une plage horaire fixe (<i>Blockzeit, Kernzeit</i>). Les horaires flottants sont fixés par un accord d’entreprise ou un accord sur les horaires flottants.</p>
<h5 class="spip">Travail de nuit</h5>
<p>Depuis 2002, le travail de nuit est autorisé pareillement pour les hommes et les femmes. Il n’est interdit que pour les femmes enceintes, les mères allaitantes et les jeunes âgés de moins de 18 ans.</p>
<h5 class="spip">Heures supplémentaires</h5>
<p>Tout dépassement de la durée normale du temps de travail (soit 8 heures par jour ou 40 heures par semaine) correspond à la prestation d’heures supplémentaires (<i>Überstunden</i>). La loi sur la durée du temps de travail prévoit l’indemnisation des heures supplémentaires sous la forme d’un supplément salarial ou d’un crédit d’heures.</p>
<h4 class="spip">Congés </h4>
<p>Les travailleurs ont droit à un minimum de cinq semaines de congés annuels par année de travail, c’est-à-dire à 30 jours de congés ouvrés par année (le samedi compris). Ce droit est porté à 6 semaines au bout de 25 ans d’ancienneté. L’année ouvrée commence à courir à partir de l’entrée en service du travailleur. Les salariés exerçant une activité « à temps très réduit » et les salariés à temps partiel ont le même droit que les salariés à temps plein. En plus de leur salaire normal, un grand nombre de travailleurs autrichiens perçoivent une prime de congé (« treizième mois »), qui équivaut à un mois de salaire et est moins lourdement imposée que les autres salaires mensuels.</p>
<p>Au cours des six premiers mois de la première année de travail, le crédit de congés est calculé au <i>prorata temporis</i>. À partir du septième mois d’activité, le droit aux congés s’exerce pleinement et, à partir de la deuxième année d’activité dans l’entreprise, le droit aux congés prend effet au début de l’année ouvrée.</p>
<p>Vous devez convenir de vos dates de congés annuels avec votre employeur, qui doit marquer son accord avec les dates proposées.</p>
<p>Si vous tombez malade durant votre congé, celui-ci sera réputé interrompu pour cause de maladie si votre incapacité de travail excède trois jours calendaires. Vous devez cependant signaler votre maladie à votre employeur immédiatement après ces trois premiers jours de maladie et lui présenter un certificat médical.</p>
<p>Les travailleurs handicapés et les adolescents n’ont en principe pas droit à davantage de jours de congé, sauf si des conventions collectives ou conventions d’entreprise le prévoient.</p>
<h5 class="spip">Maladie et maintien de la rémunération</h5>
<p>Le principe du maintien de la rémunération en cas de maladie, accidents du travail, maladie professionnelle et pour les séjours de cure et de convalescence garantit que vous continuerez à percevoir votre salaire. La durée du maintien de la rémunération dépend de votre ancienneté et peut être différente selon que le travailleur est un employé ou un ouvrier. Ensuite, vous recevez une indemnité de maladie de votre caisse maladie. Le montant de cette indemnité dépend du dernier salaire perçu avant la maladie et du montant du versement effectué au titre du maintien de la rémunération. En tant que travailleur, vous êtes tenu de notifier sans délai toute incapacité de travail à votre employeur dès qu’un tel cas se présente.</p>
<h5 class="spip">Protection de la maternité</h5>
<p>La protection de la maternité (<i><a href="http://www.arbeiterkammer.at/beratung/berufundfamilie/Mutterschutz/index.html" class="spip_out" rel="external">Mutterschutz</a></i>) débute généralement huit semaines avant la naissance et se termine huit semaines après (interdiction absolue de travailler). Durant cette période, le contrat de travail est maintenu, c’est-à-dire que le traitement ou le salaire continue d’être versé et que la (future) mère qui a travaillé jusqu’au début de cette période sur la base d’un contrat de travail ou qui dispose encore d’une couverture maladie perçoit une indemnité de maternité. À partir du 1er janvier 2008, les femmes qui travaillent sur la base d’un contrat de louage de services, c’est-à-dire les travailleuses indépendantes (<i><a href="http://www.arbeiterkammer.at/beratung/steuerundeinkommen/freiedienstnehmerinnen/index.html" class="spip_out" rel="external">freie Dienstnehmerin</a></i>), ont également droit à une indemnité de maternité.</p>
<h5 class="spip">Congé parental</h5>
<p>Les mères et les pères ont droit à un congé non rémunéré dont la durée maximale possible est limitée à la date à laquelle l’enfant vivant sous le même toit atteint l’âge de quatre ans révolus. La durée minimale du congé parental est de deux mois. Si les conditions prévues sont remplies, il est possible de percevoir, pendant cette période, une allocation parentale d’éducation. Dans ce contexte, les parents dont les enfants sont nés après le 30 septembre 2009 peuvent à partir du 1er janvier 2010 choisir entre cinq modèles, dont l’un est fonction des revenus. Le licenciement est interdit pendant les quatre semaines suivant l’expiration du congé parental.</p>
<h5 class="spip">Congé de formation</h5>
<p>À partir du 1er août 2009 et jusqu’au 31 décembre 2011, un congé de formation convenu peut être pris au bout de six mois d’ancienneté, au lieu de 12 mois comme auparavant. La durée minimum d’un tel congé a été réduite à deux mois. Si le congé de formation est pris par tranche, chaque tranche est désormais limitée à deux mois seulement mais, comme par le passé, toutes les tranches de congé de formation doivent être prises dans un délai de quatre ans. La durée maximale de tout congé de formation subventionné est de 12 mois. Durant cette période, le travailleur ne reçoit plus aucun salaire ou traitement, mais seulement une allocation de formation continue qui lui est versée par le service public de l’emploi (<i>AMS</i>) et dont le montant correspond à l’allocation chômage qui lui serait accordée, le cas échéant. La condition en est la participation à une mesure de formation continue de 20 heures hebdomadaires au moins.</p>
<h5 class="spip">Congé de solidarité familiale</h5>
<p>Le congé de solidarité familiale (<i><a href="http://www.arbeiterkammer.at/beratung/arbeitundrecht/krankheitundpflege/pflege/Familienhospizkarenz.html" class="spip_out" rel="external">Familienhospizkarenz</a></i>) permet aux travailleurs de prendre un congé, de réduire leur temps de travail provisoirement ou de reporter l’exécution de leurs activités dans le temps pour pouvoir accompagner un enfant gravement malade ou un parent mourant.</p>
<p><i>Source : site EURES</i></p>
<h3 class="spip"><a id="sommaire_2"></a>Fêtes légales</h3>
<ul class="spip">
<li>1er janvier</li>
<li>6 janvier - Epiphanie</li>
<li>Lundi de Pâques</li>
<li>1er mai</li>
<li>Ascension</li>
<li>Lundi de Pentecôte</li>
<li>Fête-Dieu</li>
<li>15 août - Assomption</li>
<li>26 octobre - Fête Nationale</li>
<li>1er novembre - Toussaint</li>
<li>2 novembre</li>
<li>8 décembre</li>
<li>25 et 26 décembre</li></ul>
<p>Pour les salariés de confession protestante (luthérienne et calviniste), vieille-catholique et méthodiste, le vendredi saint est également un jour férié.</p>
<h3 class="spip"><a id="sommaire_3"></a>Création d’entreprises – spécificités</h3>
<p>D’une manière générale, <strong>la création d’entreprise nécessite de nombreuses démarches administratives qui prennent du temps</strong>. Le passage d’un examen d’aptitude peut être demandé pour certaines activités réglementées.</p>
<p>La chambre de commerce et d’industrie autrichienne (<i><a href="https://www.wko.at/Content.Node/iv/index.html" class="spip_out" rel="external">Wirtschaftskammer Österreichs</a></i>) possède un service de conseil aux créateurs d’entreprises. Une liste d’adresses de ces services <a href="https://www.gruenderservice.net/Content.Node/wir_ueber_uns/Wir_ueber_uns_-_Startseite_Wien.html" class="spip_out" rel="external">est disponible</a> sur le site (Rubrique "Kontakt"). De plus, elle publie le guide du créateur d’entreprise (« <i>Leitfaden für Gründerinnen und Gründer</i> ») téléchargeable gratuitement sur le site Internet <a href="http://www.gruenderservice.net/" class="spip_out" rel="external">www.gruenderservice.net</a> (rubrique "Betriebsgründung").</p>
<p>Pour la création de SARL (<i>GmbH</i>) un capital social de 35 000 € est nécessaire.</p>
<p>Les activités non soumises à concession doivent être enregistrées auprès du centre des impôts (<i><a href="https://www.bmf.gv.at/zoll/zoll.html" class="spip_out" rel="external">Finanzamt</a></i>) du lieu de résidence principal ou du lieu principal d’exercice de l’activité et être déclarées à la caisse des indépendants pour les assurances accident, maladie et retraite à partir d’un certain montant de revenus annuels (6453,36 € au 25 avril 2007)</p>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li>Site Internet du <a href="http://www.gruenderservice.net/" class="spip_out" rel="external">service pour la création d’entreprise</a>  de la chambre de commerce et d’industrie autrichienne ;</li>
<li>Site Internet de la <a href="https://www.wko.at/Content.Node/iv/index.html" class="spip_out" rel="external">chambre de commerce et d’industrie autrichienne</a> ;</li>
<li>Site Internet de la ville de Vienne pour l’<a href="http://www.wirtschaftsagentur.at/" class="spip_out" rel="external">implantation des entreprises</a> ;</li>
<li>Site Internet du <a href="http://www.dgtpe.fr/se/autriche/" class="spip_out" rel="external">Service économique en Autriche</a> ;</li>
<li>Site Internet du <a href="http://www.advantageaustria.org/fr/Oesterreich-in-Frankreich.fr.html" class="spip_out" rel="external">portail officiel de l’économie autrichienne en France</a>.</li></ul>
<p><i>Mise à jour : février 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/emploi-stage/article/reglementation-du-travail-111384). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
