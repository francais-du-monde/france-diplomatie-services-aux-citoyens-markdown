# Loisirs et culture

<p><strong>Tourisme</strong></p>
<p>Avec l’amélioration globale de la sécurité dans le pays et dans les grandes villes, le tourisme se développe peu à peu. Les bateaux de croisières recommencent à faire escale dans les principaux ports de la côte caraïbe et des complexes hôteliers voient le jour. Selon l’association hôtelière de Colombie près de 12 000 chambres d’hôtels ont été construites entre 2004 et 2011. Le gouvernement a mis en place une législation fiscale favorable au développement de ce secteur.</p>
<p><strong>Activités culturelles</strong></p>
<p><strong>Activités culturelles en français</strong></p>
<p>Les Alliances françaises en Colombie proposent des conférences, des concerts, des séances de cinéma, des expositions ainsi que l’accès à leur bibliothèque et vidéothèque.</p>
<p>L’Alliance française est présente dans 16 villes de Colombie : Armenia, Barrancabermeja, Barranquilla, Bogota, Bucaramanga, Cali, Cartago, Cartagena, Cúcuta, Manizales, Medellin, Pereira, Popayán, Santa Marta, Tunja, Valledupare, Barrancabermeja, Cúcuta et Tunja.</p>
<p>Le service culturel de l’ambassade de France à Bogota organise régulièrement des évènements en partenariat avec les centres culturels de la ville.</p>
<p><strong>Activités culturelles locales</strong></p>
<p>La Colombie est une véritable mosaïque de peuples qui s’exprime à travers la culture, le folklore, les arts et l’artisanat du pays où se ressentent les influences indiennes, caribéennes, espagnoles et africaines. La vie culturelle colombienne est foisonnante et ponctuée par d’innombrables fêtes, carnavals et manifestations diverses. Parmi les plus importantes, retenons le Carnaval de Barranquilla, la Semana Santa dont les plus belles célébrations ont lieu à Popayán et Mompós et la Feria de la Flores à Medellin en août. Tout au long de l’année ont également lieu des festivals de cinéma de nombreux pays ainsi qu’un festival international de théâtre qui a lieu deux fois par an.</p>
<p>Il existe un grand choix de TV étrangères par câble ou par antenne parabolique.</p>
<p>La presse française, de même que celle d’autres pays, est disponible en Colombie dans certaines librairies.</p>
<p><strong>Sports</strong></p>
<p>Tous les sports pratiqués en Europe existent également en Colombie. On y trouve des clubs sportifs locaux et des centres de sport privés très bien équipés.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/vie-pratique/article/loisirs-et-culture). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
