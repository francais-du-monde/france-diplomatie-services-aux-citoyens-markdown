# Scolarisation

<h4 class="spip">Les établissements scolaires français au Maroc</h4>
<p>Pour toute information sur la scolarisation dans le système français à l’étranger, vous pouvez consulter notre thématique sur les <a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures.md" class="spip_in">Etudes supérieures</a> et la <a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais.md" class="spip_in">Scolarité en français</a>.</p>
<p>Vous y trouverez des renseignements sur :</p>
<ul class="spip">
<li>les établissements français du primaire et du secondaire à l’étranger ;</li>
<li>les bourses scolaires et la prise en charge des frais de scolarité à l’étranger ;</li>
<li>les possibilités qui s’offrent à vous si votre enfant ne peut être scolarisé à l’étranger dans le système français (enseignement à distance par le CNED, programme français langue maternelle (FLAM), internats en France) ;</li>
<li>les épreuves du baccalauréat à l’étranger ;</li>
<li>les bourses d’études supérieures en France et à l’étranger ;</li>
<li>l’équivalence des diplômes.</li></ul>
<h4 class="spip">Enseignement supérieur</h4>
<p>Il est possible de poursuivre localement des études supérieures. L’enseignement supérieur au Maroc est constitué d’établissements d’enseignement supérieur privé et d’établissements d’enseignement supérieur public. Il est placé sous la tutelle du ministère de l’Enseignement supérieur, de la Formation des cadres et de la Recherche scientifique et forme des cadres moyens à supérieurs en trois, cinq ou huit ans après l’obtention du baccalauréat.</p>
<p>Les écoles de l’enseignement supérieur privé marocain offrent des formations variées. Quant à l’enseignement supérieur public marocain, il est constitué d’universités, d’écoles et d’instituts. Il est possible de préparer le BTS de commerce international à Rabat. A Marrakech, l’Ecole supérieure de commerce travaille en liaison avec l’Ecole supérieure de commerce de Toulouse (mêmes programmes et examens). L’Ecole française des affaires (Casablanca et Oudja) prépare à un BTS de gestion ; diplôme homologué par l’Etat français.</p>
<p><strong>Pour en savoir plus </strong> :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.enssup.gov.ma/" class="spip_out" rel="external">Ministère de l’Education nationale, de l’Enseignement supérieur, de la Formation des cadres et de la Recherche scientifique</a></p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/vie-pratique/article/scolarisation). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
