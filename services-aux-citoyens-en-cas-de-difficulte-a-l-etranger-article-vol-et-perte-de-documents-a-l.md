# Vol et perte de documents à l’étranger

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/en-cas-de-difficulte-a-l-etranger/article/vol-et-perte-de-documents-a-l#sommaire_1">Déclaration du vol ou de la perte</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/en-cas-de-difficulte-a-l-etranger/article/vol-et-perte-de-documents-a-l#sommaire_2">Remplacement du passeport et de la carte nationale d’identité</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/en-cas-de-difficulte-a-l-etranger/article/vol-et-perte-de-documents-a-l#sommaire_3">Remplacement du permis de conduire</a></li></ul>
<p>Afin d’éviter les désagréments liés au vol ou à la perte de vos documents d’identité, il est conseillé :</p>
<ul class="spip">
<li><strong>de ne les garder sur soi que si cela est absolument nécessaire</strong> ;</li>
<li><strong>d’en conserver séparément une photocopie recto-verso</strong> ou mieux <strong>de les numériser (scanner)</strong> et de les envoyer par courriel à votre propre adresse électronique. En cas de perte ou de vol, il suffira de les récupérer depuis internet ce qui facilitera la demande de renouvellement de ces documents.</li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Déclaration du vol ou de la perte</h3>
<p>Les déclarations de vol ou de perte de documents doivent être faites auprès <strong>des autorités locales de police du lieu</strong> présumé du vol ou de la perte.</p>
<p><strong>Afin d’éviter une utilisation frauduleuse</strong> par une tierce personne de vos documents d’identité et de dégager votre responsabilité, <strong>il est conseillé de déclarer dans les plus brefs délais</strong> le vol ou la perte de ces documents.</p>
<p>Vous devrez ensuite <strong>faire enregistrer</strong> cette déclaration de perte ou de vol auprès du <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/article/annuaire-des-representations-francaises-a-l-etranger" class="spip_in">consulat français</a> qui vous remettra <strong>un récépissé</strong> que vous présenterez, le cas échéant, au moment du renouvellement de ces documents. A noter que ce récépissé est valable deux mois et tient lieu de permis de conduire pendant cette période.</p>
<p>Les consulats français à l’étranger ne peuvent enregistrer que les déclarations de perte ou de vol des documents d’identité français suivants : passeport, carte nationale d’identité, permis de conduire.</p>
<h3 class="spip"><a id="sommaire_2"></a>Remplacement du passeport et de la carte nationale d’identité</h3>
<p>En cas d’urgence, le <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/article/annuaire-des-representations-francaises-a-l-etranger" class="spip_in">consulat</a> pourra vous établir un laissez-passer valable pour le seul retour en France ou un passeport d’urgence d’une validité d’un an si vous devez vous rendre dans un autre pays. <strong>Le consulat devra procéder aux vérifications d’usage concernant votre nationalité et votre identité avant la délivrance du document</strong>.</p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/documents-officiels-a-l-etranger/article/comment-faire-une-demande-de-passeport#sommaire_2" class="spip_in">Pièces requises pour une demande de passeport</a></li>
<li><a href="services-aux-citoyens-documents-officiels-a-l-etranger-article-carte-nationale-d-identite.md" class="spip_in">Pièces requises pour une demande de carte nationale d’identité sécurisée</a></li></ul>
<p>La demande de nouveau passeport peut être déposée auprès de n’importe quel <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/article/annuaire-des-representations-francaises-a-l-etranger" class="spip_in">consulat</a> (ou auprès d’une mairie en France, à l’occasion d’un retour en France).<br class="autobr">En revanche, pour une demande de nouvelle carte nationale d’identité sécurisée (CNIS), vous devez vous adresser au consulat auprès duquel vous êtes inscrit.</p>
<h3 class="spip"><a id="sommaire_3"></a>Remplacement du permis de conduire</h3>
<p>Les consulats français à l’étranger ne délivrent pas de duplicata de permis de conduire.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Si vous êtes résident permanent dans un pays avec lequel la France échange les permis de conduire et que votre permis français a été égaré ou volé avant d’avoir pu être échangé, la déclaration de perte ou de vol vous permettra d’obtenir de la préfecture ayant délivré le permis, une attestation (« relevé d’information restreint ») au vu de laquelle les autorités de votre pays de résidence pourront, le cas échéant, vous établir un permis local. Ce dernier sera échangé contre un permis français si vous rentrez définitivement en France. <br class="manualbr"><a href="services-aux-citoyens-preparer-son-expatriation-permis-de-conduire.md" class="spip_in">Pour en savoir plus sur le permis de conduire international et l’échange de permis à l’étranger</a></p>
<ul class="spip">
<li>Si vous êtes résident permanent dans un pays avec lequel la France n’échange pas les permis de conduire, l’obtention du permis local par examen constituera la seule solution.</li>
<li>Enfin, si vous êtes Français de passage, le récépissé remis par le consulat vous servira pour l’obtention d’un duplicata du permis perdu ou volé auprès de la préfecture de votre lieu de résidence en France. <br class="manualbr"><strong>Pour en savoir plus</strong>, aller sur Service-Public.fr : "<a href="http://vosdroits.service-public.fr/F1450.xhtml" class="spip_out" rel="external">Vol du permis de conduire</a>" et "<a href="http://vosdroits.service-public.fr/F1727.xhtml" class="spip_out" rel="external">Perte du permis de conduire</a>"</li></ul>
<p><i>Mise à jour : août 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/en-cas-de-difficulte-a-l-etranger/article/vol-et-perte-de-documents-a-l). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
