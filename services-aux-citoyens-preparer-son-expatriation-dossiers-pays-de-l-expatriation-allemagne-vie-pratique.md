# Vie pratique

<h2 class="rub22727">Logement</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/vie-pratique/#sommaire_1">Recherche d’un logement</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/vie-pratique/#sommaire_2">Budget</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/vie-pratique/#sommaire_3">Conditions de location</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/vie-pratique/#sommaire_4">Electricité</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/vie-pratique/#sommaire_5">Electroménager</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Recherche d’un logement</h3>
<p>Le centre d’information et de documentation de l’ambassade d’Allemagne (CIDAL) publie une fiche d’information pour vous aider dans la recherche d’un logement. Vous pouvez obtenir cette fiche en contactant le CIDAL par téléphone ou en lui adressant un courriel :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.cidal.diplo.de/" class="spip_out" rel="external">Centre d’information et de documentation de l’ambassade d’Allemagne</a> (CIDAL)<br class="manualbr">24 rue Marbeau - 75116 Paris<br class="manualbr">Téléphone : 01 44 17 31 31 <br class="manualbr">Télécopie : 01 45 00 45 27<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/vie-pratique/#cid#mc#amb-allemagne.fr#" title="cid..åt..amb-allemagne.fr" onclick="location.href=mc_lancerlien('cid','amb-allemagne.fr'); return false;" class="spip_mail">Courriel</a> </p>
<p>Pour les nouveaux arrivants célibataires, la colocation en WG (<i>Wohngemeinschaft</i>) permet, à plusieurs personnes, de partager un appartement, tout en disposant d’une pièce individuelle. Cette formule présente comme avantage de pouvoir se loger à un prix abordable, de rencontrer rapidement des Allemands ou d’autres expatriés et de faciliter ainsi son insertion dans le pays. C’est une pratique très courante en Allemagne. Si cette forme de cohabitation vous intéresse, sachez qu’il existe des <a href="http://www.mitwohnzentrale.de/" class="spip_out" rel="external">Mitwohnzentralen</a> qui gèrent la location de logements temporaires. Il existe environ 40 agences en Allemagne, surtout dans les grandes villes. Depuis la France, vous pouvez les joindre en composant leur numéro de téléphone national 19430 (0049 + indicatif de la ville + 19430).</p>
<p>On trouve beaucoup de petites annonces de location dans les journaux nationaux et régionaux, surtout ceux du week-end, mais aussi dans les journaux de quartier, généralement distribués gratuitement. Vous trouverez ci-dessous les sites Internet des principaux journaux allemands qui mettent en ligne des annonces immobilières :</p>
<ul class="spip">
<li><a href="http://www.welt.de/" class="spip_out" rel="external">Die Welt</a> rubrique "Immobilien"</li>
<li><a href="http://www.zeit.de/" class="spip_out" rel="external">Zeit</a> rubrique "Angebote  Immobilien"</li>
<li><a href="http://www.faz.net/" class="spip_out" rel="external">Frankfurter Allgemeine Zeitung</a> rubrique "Immobilienmarkt"</li>
<li><a href="http://www.fr-online.de/" class="spip_out" rel="external">Frankfurter Rundschau</a> rubrique "Anzeigenmarkt  Immobilien"</li>
<li><a href="http://www.sueddeutsche.de/" class="spip_out" rel="external">Süddeutsche Zeitung</a> rubrique "Immobilienmarkt"</li>
<li><a href="http://www.tagesspiegel.de/" class="spip_out" rel="external">Der Tagesspiegel</a> rubrique "Anzeigenmarkt  Immobilienmarkt"</li>
<li><a href="http://www.abendblatt.de/" class="spip_out" rel="external">Hamburger Abendblatt</a> rubrique "Anzeigen  Immobilien"</li>
<li><a href="http://www.mopo.de/" class="spip_out" rel="external">Hamburger Morgenpost</a> rubrique "Immomarkt"</li>
<li>magazine <a href="http://www.spiegel.de/" class="spip_out" rel="external">Spiegel</a> rubrique "Angebote  Immobilien-Börse"</li>
<li>magazine <a href="http://www.focus.de/" class="spip_out" rel="external">Focus</a> rubrique "Immobilien"</li></ul>
<p>Les sites Internet des grandes villes allemandes proposent également des informations sur le logement. Ces sites sont faciles à retrouver (www.<strong>nom</strong>dela<strong>ville</strong>.de). A titre d’exemple :</p>
<ul class="spip">
<li><a href="http://www.berlin.de/" class="spip_out" rel="external">www.berlin.de</a> et <a href="http://www.stadtentwicklung.berlin.de/" class="spip_out" rel="external">www.stadtentwicklung.berlin.de</a> rubrique "wohnen"</li>
<li><a href="http://www.aachen.de/" class="spip_out" rel="external">www.aachen.de</a></li>
<li><a href="http://www.koeln.de/" class="spip_out" rel="external">www.koeln.de</a></li>
<li><a href="http://www.frankfurt.de/" class="spip_out" rel="external">www.frankfurt.de</a></li>
<li><a href="http://www.hamburg.de/" class="spip_out" rel="external">www.hamburg.de</a></li>
<li><a href="http://www.stuttgart.de/" class="spip_out" rel="external">www.stuttgart.de</a></li>
<li><a href="http://www.muenchen.de/" class="spip_out" rel="external">www.muenchen.de</a> , etc.</li></ul>
<p>Vous pouvez également consulter les sites Internet suivants (source CIDAL) :</p>
<ul class="spip">
<li><a href="http://www.zimmersucher.de/" class="spip_out" rel="external">www.zimmersucher.de</a></li>
<li><a href="http://www.wohnpool.de/" class="spip_out" rel="external">www.wohnpool.de</a></li>
<li><a href="http://www.die-wg-boerse.de/" class="spip_out" rel="external">www.die-wg-boerse.de</a> (uniquement pour la colocation)</li>
<li><a href="http://www.wg-welt.de/" class="spip_out" rel="external">www.wg-welt.de</a></li>
<li><a href="http://www.wg-gesucht.de/" class="spip_out" rel="external">www.wg-gesucht.de</a> (uniquement pour la colocation).</li></ul>
<p>Le recours à une agence immobilière est généralement onéreux. Celle-ci ne peut cependant exiger une provision dépassant deux mois de loyer, charges non comprises, à laquelle il convient d’ajouter la TVA.</p>
<p>Vous pouvez enfin consulter le guide "Vivre et travailler en Allemagne"(Lextenso éditions), et le site du même nom qui fournit des informations très pratiques, notamment sur le logement : <a href="http://www.travailler-en-allemagne.com/" class="spip_out" rel="external">www.travailler-en-allemagne.com/</a></p>
<h3 class="spip"><a id="sommaire_2"></a>Budget</h3>
<p>Depuis le milieu des années 90, le marché immobilier allemand s’est sensiblement détendu. Les candidats ne sont plus obligés de faire la queue pour visiter un logement.</p>
<p>De façon générale, il faut savoir que le coût du logement en Allemagne varie d’un land ou d’une ville à l’autre. Les loyers les plus chers se trouvent à Munich et dans sa banlieue (la ville de Dachau étant la plus chère d’Allemagne), à Wiesbaden, à Düsseldorf, à Francfort sur le Main, à Cologne, à Stuttgart et à Hambourg. A l’inverse, les nouveaux Länder connaissent les loyers les moins élevés et la ville de Berlin reste relativement abordable.</p>
<p>A noter que le prix du m² à la location dépend de l’ancienneté du bâtiment, de l’équipement (avec ou sans douche, avec ou sans chauffage, etc.) et de l’état du logement. Les sites des villes allemandes publient souvent un baromètre des loyers (<i>Mietspiegel</i>). La rubrique "Service  Mietspiegel" du site privé <a href="http://www.wohnpool.de/" class="spip_out" rel="external">www.wohnpool.de</a> recense par ville les sites municipaux donnant des renseignements sur le montant des loyers.</p>
<p>Pour vous donner un ordre de grandeur, voici quelques exemples de prix moyen du m² à la location (charges non comprises) d’un logement :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Berlin</strong> : 6,20 euros (moyenne). Vous trouverez à la rubrique "<i>Wohnen</i>" du site de la <a href="http://www.stadtentwicklung.berlin.de/" class="spip_out" rel="external">municipalité de Berlin</a> une carte (<i>Wohnlagenkarte</i>) qui classe les quartiers de la ville en fonction de la qualité de l’habitat. Vous trouverez également sur ce site un baromètre des loyers (<i>Mietspiegel</i>) qui vous permettra d’obtenir en fonction du quartier, de l’ancienneté du bâtiment et de la surface du logement une fourchette de prix du m² à la location. </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Francfort sur le Main</strong> : 10 euros (moyenne). Les quartiers résidentiels sont situés à Westend, Sachsenhausen et près du Taunus. La banlieue de Francfort est souvent plus résidentielle que le centre ville. D’après les chiffres publiés par la <a href="http://www.frankfurt.de/" class="spip_out" rel="external">ville de Francfort</a> rubrique "Leben in Frankfurt  Planen, Bauen und Wohnen". Comme pour les autres villes, le prix moyen au m² dépend de la surface du logement et de l’âge du bâtiment. Ainsi, pour un logement de 65 m², le prix varie entre 6,55 euros et 7,25 euros au m². Les petites surfaces sont plus onéreuses (entre 10,54 euros et 11,24 euros au m² pour un logement de 30 m²) et les plus grandes meilleur marché (entre 5,35 euros et 6,06 euros au m² pour un logement de 100 m²). Vous trouverez à la rubrique "Leben in Frankfurt  Planen, Bauen und Wohnen  Wohnen  Miete  Mietspiegel 2006" des tableaux qui vous permettront d’évaluer avec précision le coût total d’un logement à la location. </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Hambourg</strong> : 10 euros (moyenne). Les quartiers résidentiels sont situés sur les rives du lac Alster, à Eppendorf, le long de l’Elbe à Blankensee et à Wellingsbüttel. D’après les chiffres publiés par la <a href="http://www.hamburg.de/" class="spip_out" rel="external">mairie de Hambourg</a> rubrique "Stadt und Staat  Behörden  Stadtentwicklung Umwelt  Service  Mietenspiegel" pour 2005, une petite surface (entre 25 et 41 m²) se loue entre 5,38 et 7,82 euros au m² dans un quartier non résidentiel. Pour un quartier résidentiel, les prix au m2 varient entre 7,28 et 10,95 euros. Pour une surface moyenne (entre 41 et 66 m²), les prix varient entre 4,55 et 8,46 euros pour un quartier non-résidentiel et entre 5,02 et 9,28 euros pour un quartier résidentiel. Enfin, pour une grande surface (entre 66 et 91 m²), les prix varient entre 5,03 et 7,79 euros pour un quartier non résidentiel et entre 4,60 et 9,21 euros pour un quartier résidentiel. </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Munich</strong> : 9,50 euros (moyenne). Les quartiers résidentiels sont situés à Bogenhausen, Lehel, Schwabing, Pullach et Grünwald. D’après les chiffres publiés par la <a href="http://www.muenchen.de/" class="spip_out" rel="external">mairie de Munich</a> rubrique "Rathaus  Sozialreferat  Amt für Wohnen und Migration  Mietberatung  Mietspiegel"), les charges mensuelles au m² s’élèvent en moyenne à 3 euros. Une petite surface (30 m²) se loue entre 12,24 et 13,28 euros au m2. Une surface moyenne (50 m²) se loue entre 10,43 et 11,42 au m². Une grande surface (100 m²) se loue entre 9,13 et 10,13 euros au m². Ce sont les bâtiments les plus anciens qui sont les plus chers. </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Stuttgart </strong> : 8,40 euros (moyenne). La ville de Stuttgart étant située dans une cuvette entourée de collines, les quartiers résidentiels se trouvent sur les hauteurs en raison de la vue, de l’absence de pollution et de la (relative) fraîcheur en été. Les quartiers les plus prisés sont Degerloch, Gablenberg, Killesberg, Botnang et Sillenbuch. Les prix varient beaucoup en fonction du quartier, de l’état du logement et de son équipement (cuisine et salle de bain, notamment). Un appartement vide se loue en moyenne à 13 euros au m², soit environ 400 euros pour un studio, 800 euros pour un 3 pièces, 1 300 euros pour un 5 pièces, à partir de 1 500 euros pour une petite maison et plus de 2 700 euros pour une villa (chiffres de 2006 fournis par le consulat général de France à Stuttgart). La mairie de Stuttgart publie chaque année un baromètre des loyers (<i>Mietspiegel</i>). Vous pouvez vous le procurer, moyennant paiement, à la rubrique "Politik und Verwaltung  Lebenssituationen  Wohnen und Bauen  Gebäude und Wohnen  Mietspiegel Erhebung" du site de la <a href="http://www.stuttgart.de/" class="spip_out" rel="external">ville de Stuttgart</a>. </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Cologne (Köln)</strong> : 8 euros (moyenne). Le prix au m² varie entre 6,35 et 8,85 euros. Vous trouverez sur le site de la <a href="http://www.koeln.de/" class="spip_out" rel="external">ville de Cologne</a> rubrique "Wirtschaft  Wohnen  Bezirke und Stadtteile") une carte qui vous permettra de connaître la qualité de l’habitat pour chacun des quartiers de la ville, ainsi que le prix moyen du m2 à la location. </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Düsseldorf</strong> : 6,70 euros (moyenne). Les quartiers résidentiels sont principalement situés à Oberkassel, Golzheim, Derendorf, Benrath et au nord de la ville (Stockum et Kaiserswerth). D’après les chiffres publiés par la <a href="http://www.duesseldorf.de/" class="spip_out" rel="external">mairie de Düsseldorf</a> rubrique "Umwelt, Wohnen"), le prix moyen pour un logement en bon état (surface 70 m²) est de 7,50 euros au m², celui pour un logement de luxe (surface de 100 m²) est de 9,03 euros au m² et celui pour logement meublé (surface de 35 m²) est de 9,36 euros au m². </p>
<p><strong> <i>Exemples de coût de location </i> </strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Berlin </strong></td>
<td>Prix du m2</td>
<td>Studio</td>
<td>3 Pièces</td>
<td>Pièces</td>
<td>Villa</td></tr>
<tr class="row_even even">
<td>Proche de l’ambassade de France</td>
<td>10-20 €</td>
<td>400-800</td>
<td>700-2 000</td>
<td>1 000</td>
<td>1 500</td></tr>
<tr class="row_odd odd">
<td>Banlieue</td>
<td>6-10 €</td>
<td>250-500</td>
<td>500-1 000</td>
<td>700-1 500</td>
<td>1 500</td></tr>
</tbody>
</table>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Francfort sur le Main</strong></td>
<td>Prix du m2</td>
<td>Studio</td>
<td>3 Pièces</td>
<td>5 Pièces</td>
<td>Villa</td></tr>
<tr class="row_even even">
<td>Quartier résidentiel</td>
<td>15-40 €</td>
<td>800</td>
<td>1 400</td>
<td>2 400</td>
<td>5 000</td></tr>
<tr class="row_odd odd">
<td>Banlieue</td>
<td>7-30 €</td>
<td>450</td>
<td>1 000</td>
<td>1 600</td>
<td>3 500</td></tr>
</tbody>
</table>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Munich</strong></td>
<td>Prix du m2</td>
<td>Studio</td>
<td>3 Pièces</td>
<td>5 Pièces</td>
<td>Villa</td></tr>
<tr class="row_even even">
<td>Quartier résidentiel</td>
<td>14-17 €</td>
<td>1 000</td>
<td>1 400</td>
<td>1 800</td>
<td>3 000</td></tr>
<tr class="row_odd odd">
<td>Banlieue</td>
<td>9-12 €</td>
<td>600</td>
<td>800</td>
<td>1 000</td>
<td>2 000</td></tr>
</tbody>
</table>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Stuttgart</strong></td>
<td>Prix du m2</td>
<td>Studio</td>
<td>3 Pièces</td>
<td>5 Pièces</td>
<td>Villa</td></tr>
<tr class="row_even even">
<td>Quartier résidentiel</td>
<td>13 €</td>
<td>400</td>
<td>800</td>
<td>1 500</td>
<td>2 700</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_3"></a>Conditions de location</h3>
<p>Le contrat de location peut être oral, mais cela est très rare, ou écrit. La forme la plus usitée est le contrat écrit : il peut se présenter sous la forme d’un formulaire standard pré-imprimé ou résumé en quelques lignes. Quelque soit la forme choisie, il faut, pour que le contrat produise ses effets, que soit clairement identifiés le locataire et le bailleur, le logement loué, le montant du loyer et la date de début du contrat.</p>
<p>Le <strong>contrat à durée illimitée</strong> (<i>unbefristeter Mietvertrag</i>) est la forme la plus courante. La date de fin du contrat n’est pas spécifiée. Le locataire doit respecter un préavis de 3 mois et n’a pas à indiquer de motif de résiliation. Pour le bailleur, le délai de préavis dépend de la durée de la location et peut varier entre 3 et 9 mois. Le bailleur ne peut résilier un contrat que dans certains cas prévus par la loi.</p>
<p>Le <strong>contrat à durée limitée</strong> (<i>Zeitmietvertrag</i>) est toujours écrit. Les dates de début et de fin de la location sont indiquées dans le contrat. Pendant la durée du contrat, ni le locataire, ni le bailleur ne peuvent le dénoncer, sauf dans le cas d’une résiliation sans préavis. Il existe cependant, pour le locataire, certains cas où la résiliation est possible (modernisation du logement, augmentation du loyer, etc.). Depuis le 1er septembre 2001, il n’existe pratiquement plus que le contrat à durée limitée qualifié (<i>qualifizierter Mietvertrag</i>) dans lequel est spécifié le motif de résiliation du contrat.</p>
<p>L’évolution du montant du loyer peut être spécifiée dans le contrat. C’est le cas du contrat de location avec échelonnement (<i>Staffelmietvertrag</i>), dans lequel est indiquée à l’avance l’augmentation annuelle du loyer, et du contrat de location indexé (<i>Indexmietvertrag</i>), pour lequel l’augmentation du loyer est fonction de l’index des prix de l’Office fédéral des statistiques.</p>
<p>Le montant de la caution est négociable et il est préférable d’en indiquer le montant dans le contrat de location. Le bailleur n’a pas le droit d’exiger une caution excédant 3 mois de loyer, charges non comprises. La caution est généralement réglée en espèces ou par virement sur un compte spécial (<i>Sonderkonto </i>ou <i>Kautionskonto)</i> ouvert à cet effet par le bailleur. La caution est récupérable en fin de bail.</p>
<p>Les charges peuvent être comprises dans le loyer. Dans ce cas, le loyer dû est un loyer "chaud" (<i>Warmmiete</i>). Dans le cas contraire, le locataire paie un loyer "froid" (<i>Kaltmiete</i>). Lorsque les charges sont comprises, elles le sont toujours au minimum et les régularisations en début d’année peuvent être douloureuses (notamment en raison des coûts de chauffage). Il importe donc de bien se faire préciser la teneur des charges. Les charges sont assez élevées (de 30 à 60 % en plus du loyer net) peuvent comprendre : l’impôt foncier, l’eau, l’ascenseur, l’enlèvement des ordures, l’entretien de la chaussée et du bâtiment, la dératisation, l’entretien du jardin, l’éclairage, le ramonage, l’assurance, les frais de gardiennage, les frais de connexion au câble, etc. Une partie de ces frais est à la charge du locataire. La répartition peut être fonction du nombre d’occupants du logement ou de la surface de celui-ci.</p>
<p>Enfin, il est vivement conseillé de faire procéder à un état des lieux. En règle générale, on entre dans un logement rénové que l’on rend également rénové quelle que soit la durée de la location.</p>
<p>A noter qu’en Allemagne, le tri des ordures ménagères est obligatoire et leur enlèvement est moins fréquent qu’en France. Selon le quartier, il est fréquent de disposer de quatre poubelles qui sont généralement enlevées à tour de rôle toutes les 3 ou 4 semaines. Les gros objets ("encombrants") ne sont pris en charge que une à deux fois par an ou sur demande spéciale et préalable auprès de la mairie. Pour les objets polluants volumineux (réfrigérateur, matériel électronique, etc.), il est nécessaire de faire appel à un service spécialisé qui se chargera, moyennant finances, de l’enlèvement. Il est utile de se renseigner auprès de la mairie du lieu de résidence pour obtenir le calendrier et les modalités d’enlèvement des ordures (<i>Müllkalender</i> ou <i>Abfallkalender</i>)</p>
<p>Sites à consulter :</p>
<ul class="spip">
<li><a href="http://www.gesetze-im-internet.de/" class="spip_out" rel="external">Ministère fédéral de la Justice</a> (lois sur Internet) rubrique "Gesetze / Verordnungen  Lettre B  BGB (bürgerliches Gesetzbuch)". Les questions relatives aux contrats de location sont traitées au titre 5 du BGB (<i>Mietvertrag, Pachtvertrag</i>).</li>
<li><a href="http://www.mieterbund.de/" class="spip_out" rel="external">Deutscher Mieterbund</a> (Union allemande de locataires). Cette association dispose souvent de branches dans les grandes villes.</li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Electricité</h3>
<p>Le voltage est le même qu’en France : 220-240 volts / 50 hertz. Les prises de courant sont aux normes européennes et sont toujours munies d’une prise de terre.</p>
<h3 class="spip"><a id="sommaire_5"></a>Electroménager</h3>
<p>On trouve en Allemagne toutes les marques d’électroménager. A noter que les stocks disponibles dans les magasins sont souvent très limités et qu’il n’est pas rare de devoir attendre plusieurs semaines avant d’être livré.</p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-vie-pratique-article-loisirs-et-culture.md" title="Loisirs et culture">Loisirs et culture</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-vie-pratique-article-communications-109127.md" title="Communications">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-vie-pratique-article-transports.md" title="Transports">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-vie-pratique-article-cout-de-la-vie-109124.md" title="Coût de la vie">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-vie-pratique-article-scolarisation-109123.md" title="Scolarisation">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-vie-pratique-article-sante-109122.md" title="Santé">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-allemagne-vie-pratique-article-logement-109121.md" title="Logement">Logement</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/vie-pratique/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
