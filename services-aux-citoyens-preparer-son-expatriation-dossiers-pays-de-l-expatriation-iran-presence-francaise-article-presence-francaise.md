# Présence française

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/presence-francaise/article/presence-francaise#sommaire_1">Ambassade de France en Iran</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/presence-francaise/article/presence-francaise#sommaire_2">Autorités françaises dans le pays</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/presence-francaise/article/presence-francaise#sommaire_3">Associations dans le pays</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Ambassade de France en Iran</h3>
<p>Consulter notre <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/article/annuaire-des-representations-francaises-a-l-etranger" class="spip_in">annuaire des ambassades et consulats de France à l’étranger</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Autorités françaises dans le pays</h3>
<h4 class="spip">Réseau français de coopération et d’action culturelle</h4>
<p>Les coordonnées des services du réseau de coopération et d’action culturelle français en Iran ainsi que la description de l’activité de ces services se trouvent dans la rubrique culture du <a href="http://www.ambafrance-ir.org/" class="spip_out" rel="external">site internet de l’ambassade</a> ou sur le site internet du réseau culturel : <a href="http://latitudefrance.diplomatie.gouv.fr/AnnuaireCarto.html" class="spip_out" rel="external">Latitude France</a>.</p>
<h4 class="spip">Economie : réseau mondial</h4>
<p>Le service économique est temporairement fermé et devrait ouvrir au cours du premier semestre 2014.</p>
<p>Il convient de noter que les procédures habituelles d’aide de l’Etat au commerce extérieur sont temporairement suspendues et que les Volontaires Internationaux en Entreprises (VIE) ne sont pas autorisés à se rendre en Iran à titre professionnel ou touristique. Par ailleurs vous êtes invités à consulter le site suivant pour prendre connaissance des restrictions imposées sur le commerce avec <a href="http://www.tresor.economie.gouv.fr/3745_Iran" class="spip_out" rel="external">l’Iran</a> :</p>
<ul class="spip">
<li><a href="http://www.tresor.economie.gouv.fr/3745_Iran" class="spip_out" rel="external">textes</a>,</li>
<li><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/presence-francaise/article/presence-francaise#sanctions-gel-avoirs#mc#dgtresor.gouv.fr#" title="sanctions-gel-avoirs..åt..dgtresor.gouv.fr" onclick="location.href=mc_lancerlien('sanctions-gel-avoirs','dgtresor.gouv.fr'); return false;" class="spip_mail">Courriel</a>.</li></ul>
<p>Le service économique régional pour le Proche et Moyen Orient se trouve à <a href="http://www.tresor.economie.gouv.fr/Pays/liban" class="spip_out" rel="external">Beyrouth</a>.</p>
<p>Les entreprises iraniennes qui veulent exporter leurs produits vers la France doivent s’adresser directement à la <a href="http://www.cfici.org/" class="spip_out" rel="external">Chambre franco-iranienne de commerce et d’industrie</a>.</p>
<h4 class="spip">Vos élus à l’Assemblée des Français de l’étranger</h4>
<p>Pour toute information sur l’Assemblée des Français de l’étranger et pour connaître les conseillers, les députés et les Sénateurs qui représentent les Français établis hors de France dans votre circonscription, vous pouvez consulter les sites Internet suivants :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.votezaletranger.gouv.fr/" class="spip_out" rel="external">Votez à l’étranger</a> </p>
<ul class="spip">
<li><a href="http://www.assemblee-afe.fr/" class="spip_out" rel="external">Assemblée des Français de l’étranger (AFE)</a></li>
<li>Sénat au service des Français de l’étranger : <a href="http://www.senat.fr/expatries/dossiers_pays/iran.html" class="spip_out" rel="external">dossier spécifique à l’Iran sur le site du Sénat</a></li>
<li>Députés des Français de l’étranger : la révision constitutionnelle du 23 juillet 2008, publiée au Journal officiel du 19 avril 2011, introduit la représentation à l’Assemblée nationale des Français établis hors de France. Depuis 2012, la création de onze circonscriptions législatives à l’étranger permet désormais aux Français expatriés d’élire leur député à l’Assemblée nationale.</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Associations dans le pays</h3>
<ul class="spip">
<li><strong>l’Association des parents d’élèves</strong> de l’Ecole française de Téhéran</li>
<li><strong>L’association Téhéran Bienvenue</strong> <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/presence-francaise/article/presence-francaise#teheranbienvenue#mc#yahoo.fr#" title="teheranbienvenue..åt..yahoo.fr" onclick="location.href=mc_lancerlien('teheranbienvenue','yahoo.fr'); return false;" class="spip_mail">Courriel</a></li>
<li><a href="http://www.ufe.org/fr/vie-ufe/les-ufe-dans-le-monde/consultez-lannuaire-de-lufe.html" class="spip_out" rel="external">Section de l’Union des Français de l’étranger (UFE)</a></li>
<li><a href="http://www.francais-du-monde.org/" class="spip_out" rel="external">Section du Monde - Association démocratique des Français de l’étranger (Français du monde-adfe)</a> <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/presence-francaise/article/presence-francaise#contact#mc#adfe.org#" title="contact..åt..adfe.org" onclick="location.href=mc_lancerlien('contact','adfe.org'); return false;" class="spip_mail">Courriel</a></li></ul>
<p><i>Mise à jour : février 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/presence-francaise/article/presence-francaise). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
