# Fiscalité du pays

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/fiscalite/article/fiscalite-du-pays#sommaire_1">Impôt sur le revenu des personnes physiques </a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/fiscalite/article/fiscalite-du-pays#sommaire_2">Imposition des sociétés</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/fiscalite/article/fiscalite-du-pays#sommaire_3">La taxe sur la valeur ajoutée (TVA)</a></li></ul>
<p>Le niveau d’imposition des personnes physiques et morales à Maurice est relativement bas. Il est de 15%, avec toutefois des exemptions importantes.</p>
<h3 class="spip"><a id="sommaire_1"></a>Impôt sur le revenu des personnes physiques </h3>
<p>L’impôt sur le revenu des personnes physiques est régi par l’<i>Income Tax Act</i> de 1995. Un taux unique de 15% est appliqué. L’impôt sur le revenu fait l’objet d’une retenue à la source.</p>
<p>Pour l’imposition des revenus de 2013, une personne avec un salaire mensuel inférieur à 20 800 roupies est exemptée de l’impôt sur le revenu.</p>
<p>Une personne physique résidente à l’ile Maurice peut, dans certains cas, bénéficier d’un seuil d’exemption de son revenu, qu’elle déduit de son assiette pour calculer son revenu imposable.</p>
<p>Le seuil d’exonération imposable des revenus pour l’année 2013 est comme suit :</p>
<table class="spip">
<thead><tr class="row_first"><th id="ide6bd_c0">Catégories  </th><th id="ide6bd_c1"> Montant  en roupies  </th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="ide6bd_c0">A- Un particulier sans aucun dépendant</td>
<td class="numeric " headers="ide6bd_c1">270 000</td></tr>
<tr class="row_even even">
<td headers="ide6bd_c0">B- Un particulier avec 1 dépendant</td>
<td class="numeric " headers="ide6bd_c1">380 000</td></tr>
<tr class="row_odd odd">
<td headers="ide6bd_c0">C- Un particulier avec 2 dépendants</td>
<td class="numeric " headers="ide6bd_c1">440 000</td></tr>
<tr class="row_even even">
<td headers="ide6bd_c0">D- Un particulier avec 3 dépendants</td>
<td class="numeric " headers="ide6bd_c1">480 000</td></tr>
<tr class="row_odd odd">
<td headers="ide6bd_c0">E- Un retraité sans aucun dépendant</td>
<td class="numeric " headers="ide6bd_c1">320 000</td></tr>
<tr class="row_even even">
<td headers="ide6bd_c0">F- Un retraité avec 1 dépendant</td>
<td class="numeric " headers="ide6bd_c1">430 000</td></tr>
</tbody>
</table>
<p>Ces déductions comportent certaines exceptions. Notamment , une personne ne peut prétendre aux déductions ci-dessus si :</p>
<ul class="spip">
<li>elle appartient à la catégorie B ou F et que le revenu total annuel de son dépendant dépasse 110 000 roupies,</li>
<li>elle appartient à la catégorie C et que le revenu annuel total de son deuxième dépendant dépasse 60 000 roupies,</li>
<li>elle appartient à la catégorie D et que le revenu total annuel de son troisième dépendant dépasse 40 000 roupies,</li></ul>
<p>Pour l’ imposition des personnes, t rois systèmes sont en vigueur : il s’ agit principalement du système PAYE, destiné aux salariés, du CPS pour les travailleurs indépendant s. Il existe également une catégorie " TDS" qui permet d’enregistrer les revenus provenant d’ intérêts, de dividendes étrangers, de rentes, etc.</p>
<h4 class="spip">PAYE (Pay As You Earn)</h4>
<p>Avec le système PAYE, l’ employeur doit retenir à la source l’ impôt dû par ses employés. L’ employeur doit remplir un <strong>formulaire de déclaration de l’employé</strong> (<i>Employee Declaration Form</i>) afin de calculer le montant de l’impôt . Chaque mois, il l’ adressera au centre des impôts de la <i>Mauritius Revenue Authority</i> (MRA).</p>
<h4 class="spip">CPS (Current Payment System)</h4>
<p>Le CPS s’ applique aux personnes tirant leurs revenus d’un commerce, des affaires, de loyers et s’ élevant à un chiffre d’affaires de plus de 4 millions de roupies annuellement .</p>
<h4 class="spip">Tax Deduction at Source (TDS)</h4>
<p>Avec le système TDS, l’impôt doit être versé au moment où le paiement est effectué ou crédité sur le compte du bénéficiaire (retenu à la source)</p>
<p>Les types de paiements sujets à la TDS sont principalement, les intérêts, les royalties, les locations, les paiements effectués à des architectes, ingénieurs, géomètres, project managers dans la construction, experts immobiliers et métreurs vérificateurs pour services rendus, et les paiements à des fournisseurs et sous-traitants.</p>
<p>Dans le système TDS, le t aux d’ imposition est différent selon la nature du paiement :</p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Nature du paiement</td>
<td>Taux d’ imposition</td></tr>
<tr class="row_even even">
<td>Intérêts</td>
<td>15%</td></tr>
<tr class="row_odd odd">
<td>Royalties (destinés à des résidents)</td>
<td>10%</td></tr>
<tr class="row_even even">
<td>Royalties (destinés à des non-Résidents)</td>
<td>15%</td></tr>
<tr class="row_odd odd">
<td>Loyers</td>
<td>5%</td></tr>
<tr class="row_even even">
<td>Paiements à des prestataires de services</td>
<td>3%</td></tr>
<tr class="row_odd odd">
<td>Paiements à des fournisseurs et sous-traitants</td>
<td>0,75 %</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_2"></a>Imposition des sociétés</h3>
<h4 class="spip">L’impôt sur les bénéfices est régi par l’<i>Income Tax Act</i> de 1995 </h4>
<p>Depuis le 1er juillet 2007, il existe à Maurice un taux d’imposition unique de 15% sur les bénéfices, applicable à toutes les sociétés à l’exception des compagnies opérant dans la zone franche du port et celles travaillant pour l’international (GBL1 et GBL2)</p>
<p>Il existe un minimum d’imposition (<i>Alternative Minimum Tax</i>). Si l’impôt sur les sociétés d’une compagnie correspond à moins de 7,5% de son profit comptable alors elle devra payer soit 7,5% de son profit comptable ou 10% des dividendes qu’elle a déclaré (selon ce qui est moindre).</p>
<p>Le budget 2008/2009 a introduit le <i>Advanced Payment System (APS)</i> qui oblige les entreprises ayant réalisé un chiffre d’affaires supérieur à 100 millions de roupies (environ 2,5 MRs) à payer trimestriellement leurs impôts, sur le même principe que pour les particuliers.</p>
<h4 class="spip">Statut particulier du port franc de Maurice</h4>
<p>Des mesures incitatives fiscales sont octroyées aux entreprises opérant des activités commerciales dans la zone du port franc de Maurice, à Port-Louis. Il s’agit notamment de :</p>
<ul class="spip">
<li>Exemption totale d’impôt pour les activités d’import-export,</li>
<li>Imposition de 15% pour l’activité de transformation,</li>
<li>Exemption des droits de douane sur toute marchandise importée à travers le port franc,</li>
<li>Libre rapatriement des bénéfices,</li>
<li>Pas d’imposition de dividendes.</li></ul>
<h4 class="spip">Activités à destination d’une clientèle internationale ou non résidentes</h4>
<p>Il existe deux types d’entreprises :</p>
<p>Les sociétés <strong>Global Business Licence catégorie 1 (GBL1)</strong> sont des sociétés avec une résidence fiscale à Maurice et bénéficiant des accords de non double imposition et de taux réduits sur leur fiscalité mauricienne. Le taux d’imposition de base est de 15% mais un crédit d’import forfaitaire de 80% de l’impôt mauricien est accordé pour tenir compte des taxes payées à l’étranger sur les montants financiers transférés à Maurice. De ce fait, l’imposition des entreprises des entreprises GL1 s’élève à 3% des bénéfices.</p>
<p>Elles ne sont pas autorisées à effectuer des transactions en monnaie locale avec des résidents ou à détenir des avoirs immobiliers à l’île Maurice. Des activités de banque, d’assurance et de gestion de fonds sont possibles.</p>
<p>Les sociétés <strong>Global Business Licence catégorie 2 (GBL2)</strong> sont non-résidentes et sont donc des entités franche d’impôt qui ne peuvent se prévaloir des abattements prévus dans les conventions de non- double imposition en vigueur à l’île Maurice. Elles doivent être enregistrée auprès d’un agent agrée (<i>Trust ou Management Company</i>). La GBL2 peut par exemple être utilisée comme société de commerce, facturation, contrats internationaux.</p>
<h3 class="spip"><a id="sommaire_3"></a>La taxe sur la valeur ajoutée (TVA)</h3>
<p>Instaurée en 1998, la taxe sur la valeur ajoutée s’applique à tous les biens et services destinés à une utilisation finale ou à la revente. Elle s’apparente à la TVA française. Le taux <strong>en est de 15 %</strong>.</p>
<p>Toute entité réalisant un chiffre d’affaires annuel supérieur à deux millions MUR et toute personne exerçant une profession telle que comptable, ingénieur, agent immobilier à son compte quel que soit son chiffre d’affaires annuel doit s’enregistrer auprès de la <strong>Mauritius Revenue Authority</strong>. Dès lors, la société enregistrée aura pour obligation de facturer la TVA sur les biens et services assujettis. Cette même société sera en mesure de récupérer la TVA sur ses achats.</p>
<p>Les personnes non-résidentes à Maurice peuvent se faire rembourser, au moment de leur départ, le montant de la TVA payée sur les achats effectués pendant leur séjour, à condition que ceux-ci soient réexportés.</p>
<p>N.B : Certains produits alimentaires de base, les services vendus à l’extérieur de Maurice ou importés dans le pays bénéficient d’un taux zéro, de même que certaines marchandises exportées sous certaines conditions</p>
<p>Par ailleurs, les services suivants ne sont pas soumis à la TVA : location de logement, services liés à l’éducation, transport, et certains services financiers.</p>
<p>Enfin, il convient de noter que, à Maurice, il n’y a pas de taxe sur les dividendes, les gains du capital et le rapatriement des bénéfices, dividendes et capitaux.</p>
<p><strong>Mauritius Revenue Authority</strong><br class="manualbr">Ehram Court, Cnr Mgr. Gonin  Sir Virgil Naz Streets <br class="manualbr">PORT LOUIS <br class="manualbr">Tél : +230 207 6000 <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/fiscalite/article/fiscalite-du-pays#headoffice#mc#mra.mu#" title="headoffice..åt..mra.mu" onclick="location.href=mc_lancerlien('headoffice','mra.mu'); return false;" class="spip_mail">Courriel</a></p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/fiscalite/article/fiscalite-du-pays). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
