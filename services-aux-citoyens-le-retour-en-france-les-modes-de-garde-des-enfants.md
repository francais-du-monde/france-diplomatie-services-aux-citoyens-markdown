# Les modes de garde des enfants

<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-le-retour-en-france-les-modes-de-garde-des-enfants-article-les-creches.md" title="Les crèches">Les crèches</a></li>
<li><a href="services-aux-citoyens-le-retour-en-france-les-modes-de-garde-des-enfants-article-les-autres-modes-de-garde.md" title="Les autres modes de garde">Les autres modes de garde</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/les-modes-de-garde-des-enfants/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
