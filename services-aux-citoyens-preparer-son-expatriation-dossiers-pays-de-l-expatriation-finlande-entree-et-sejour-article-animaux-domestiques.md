# Animaux domestiques

<p>Pour entrer en Finlande avec ses animaux de compagnie, il faut se munir du passeport européen délivré et mis à jour par le vétérinaire, comprenant la vaccination antirabique, l’attestation de bonne santé et s’ils sont âgés de plus de trois mois, le traitement contre l"’echinococcus" (qui doit être pratiqué dans les 30 jours précédant l’entrée en Finlande).</p>
<p><strong>Information générale</strong></p>
<p>Une information très détaillée est disponible sur le site Internet du <a href="http://agriculture.gouv.fr/" class="spip_out" rel="external">ministère de l’Agriculture</a> Rubrique Santé et protection des animaux / Animaux de compagnie / Transport / Voyager avec son animal de compagnie dans l’Union européenne.</p>
<p>Les chiens, les chats et les furets doivent satisfaire aux conditions suivantes :</p>
<ul class="spip">
<li>être identifiés par tatouage ou par puce électronique ;</li>
<li>être valablement vaccinés contre la rage ;</li>
<li>être titulaires d’un passeport délivré par un vétérinaire habilité attestant de l’identification et de la vaccination contre la rage de l’animal ;</li>
<li>dans le cas de la Finlande, avoir subi un traitement contre l’échinococcose moins de 30 jours avant le départ. Pour en savoir plus, vous pouvez consultez le site Internet suivant : <a href="http://www.evira.fi/portal/en/animals/import_and_export/" class="spip_out" rel="external">Evira.fi</a> rubrique Animals and health / Import and export. Le système d’identification électronique, ainsi que la reconnaissance de la validité de la vaccination contre la rage, peuvent varier d’un Etat membre à l’autre. Il est donc vivement recommandé de prendre contact avec l’ambassade de Finlande.</li></ul>
<p>En France, la réalisation de la primo-vaccination antirabique n’est considérée comme valable qu’à partir de 21 jours après la fin du protocole de vaccination prescrit par le fabricant. La vaccination antirabique de rappel est considérée en cours de validité le jour de sa réalisation.</p>
<p><strong>Pour plus d’informations :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Notre article thématique <a href="services-aux-citoyens-preparer-son-expatriation-douanes-article-animaux-domestiques.md" class="spip_in">Animaux domestiques</a></p>
<p><i>Mise à jour : janvier 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/finlande/entree-et-sejour/article/animaux-domestiques). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
