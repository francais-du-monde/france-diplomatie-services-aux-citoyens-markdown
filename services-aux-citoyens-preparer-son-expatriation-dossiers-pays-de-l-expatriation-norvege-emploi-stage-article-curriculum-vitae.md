# Curriculum vitae

<h4 class="spip">Rédaction</h4>
<p>Les critères de rédaction d’un CV en France conviennent tout à fait en Norvège.</p>
<p>En Norvège, il est d’usage de joindre un CV à la lettre de candidature. Le CV doit de préférence tenir sur une page et il n’est pas d’usage d’y joindre une photo. Les informations contenues dans le CV peuvent varier considérablement. Certains CV sont sommaires, d’autres plus détaillés. Toutefois, la fonction la plus importante de votre CV est de donner des informations exactes et de les présenter clairement.</p>
<p>Un CV comprend ordinairement quatre ou cinq parties :</p>
<p>1) <i>Coordonnées personnelles</i> : nom, adresse, numéro de téléphone (y compris le préfixe téléphonique de votre pays), date et lieu de naissance, statut marital et nationalité.</p>
<p>2) <i>Formation</i> : cette partie du CV doit mentionner vos qualifications formelles, éventuellement votre niveau d’études universitaires, avec indication éventuelle du niveau correspondant dans le système éducatif norvégien. Il est toujours souhaitable d’expliquer le contenu essentiel de la formation acquise. Les connaissances en langues étrangères (avec indication de niveau) doivent figurer dans cette partie du CV.</p>
<p>3) <i>Expérience professionnelle</i> : cette partie de votre CV revêt une importance particulière. Décrivez brièvement chacun des emplois/postes occupés et qui présentent en l’occurrence un intérêt. Il est souhaitable que les jeunes diplômés fassent mention de l’expérience qu’ils ont retirée de leurs éventuels emplois de vacances ou à temps partiel.</p>
<p>4) <i>Centres d’intérêt personnels, emplois bénévoles, etc</i>. : décrivez en quelques lignes vos occupations de loisirs ou à caractère non professionnel. Mentionnez la connaissance éventuelle que vous pourriez avoir d’autres pays. Si vous avez eu auparavant l’occasion de séjourner en Norvège, n’hésitez pas à l’indiquer.</p>
<p>5) <i>Références</i> : en Norvège, il est considéré comme très important de citer au minimum deux références issues de vos activités professionnelles actuelles ou précédentes. D’ordinaire, ces références doivent être vos supérieurs hiérarchiques directs. Mentionnez les noms et numéros de téléphones des personnes concernées, après les avoir averties de votre démarche. Il est préférable que ces personnes parlent anglais ou une langue scandinave.</p>
<p>Pour en savoir plus, consultez le site de l’<a href="http://www.norvege.no/studywork/Travail/giude/#.UzGBKq10ZYc" class="spip_out" rel="external">ambassade de Norvège en France</a>.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/norvege/emploi-stage/article/curriculum-vitae). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
