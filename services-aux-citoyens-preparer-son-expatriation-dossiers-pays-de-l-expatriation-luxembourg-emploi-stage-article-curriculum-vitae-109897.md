# Curriculum vitae

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/emploi-stage/article/curriculum-vitae-109897#sommaire_1">Rédaction</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/emploi-stage/article/curriculum-vitae-109897#sommaire_2">Diplômes</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Rédaction</h3>
<p>Le CV devra être rédigé dans la langue de l’offre d’emploi pour laquelle vous postulez.</p>
<p>S’il n’existe pas de règle générale quant à la longueur du CV, <strong>il est conseillé de se limiter à deux pages A4</strong>.</p>
<p>Après mention de vos coordonnées personnelles (nom, prénom, adresse, téléphone, âge, situation familiale, nationalité et mobilité géographique), le CV est organisé en rubriques dans lesquelles vous détaillerez :</p>
<ul class="spip">
<li>les expériences professionnelles (ou votre formation si vous postulez pour un premier emploi) en indiquant les dates d’exercice, le lieu d’activité, les fonctions occupées ainsi que les responsabilités) ;</li>
<li>la formation (cursus universitaire, stages, etc.) ;</li>
<li>les connaissances linguistiques (cette rubrique peut faire l’objet d’un point dans la rubrique " divers ") ;</li>
<li>la rubrique " divers " (loisirs, connaissance en informatique, activités extraprofessionnelles, etc.).</li></ul>
<p>Sachez adapter votre CV en fonction de l’offre d’emploi que vous convoitez en insistant sur les compétences utiles pour le poste et que l’employeur ne manquera pas de relever.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.adem.public.lu/" class="spip_out" rel="external">Le site de l’administration de l’emploi</a></li>
<li><a href="http://www.guichet.public.lu/entreprises/fr/ressources-humaines/recrutement/index.html" class="spip_out" rel="external">Guichet public - recrutement</a></li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Diplômes</h3>
<p>Une réglementation au niveau européen a été envisagée et vise à faciliter la reconnaissance des diplômes et des qualifications professionnelles en vertu desquelles les particuliers peuvent exercer une profession spécifique.</p>
<p>Pour un nombre limité de professions (architecte, sage-femme, pharmacien, médecin, infirmier, dentiste et vétérinaire), des directives sectorielles instituent un système de reconnaissance automatique des diplômes.</p>
<p>Pour les autres professions réglementées et en fonction du niveau de formation post-secondaire attesté par le diplôme, la reconnaissance des qualifications professionnelles opère conformément à un système général de reconnaissance, réglementé par deux directives générales modifiées par la directive 2001/19/CE.</p>
<p>La reconnaissance vise le diplôme, certificat, titre ou ensemble de titres qui sanctionne une formation professionnelle complète et permet d’exercer la profession dans l’Etat de provenance. Vous devez adresser une demande de reconnaissance à l’autorité compétente dans l’Etat membre d’accueil qui devra examiner individuellement votre cas.</p>
<p><a href="http://www.men.public.lu/" class="spip_out" rel="external">Ministère de l’Education nationale et de la Formation professionnelle</a><br class="manualbr">29 rue Aldringen<br class="manualbr">L-1118 Luxembourg<br class="manualbr">Tél. : (+352) 2478 51 00 <br class="manualbr">Fax : (+352) 2478 51 13 <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/emploi-stage/article/curriculum-vitae-109897#info#mc#men.public.lu#" title="info..åt..men.public.lu" onclick="location.href=mc_lancerlien('info','men.public.lu'); return false;" class="spip_mail">Courriel</a></p>
<p>Vous pouvez télécharger directement sur le site une demande type de reconnaissance de diplôme.</p>
<p>Au Luxembourg, la reconnaissance formelle d’un diplôme d’enseignement supérieur obtenu à l’étranger est réalisée conformément à la législation nationale en vigueur, selon deux procédures différentes :</p>
<ul class="spip">
<li>l’inscription au registre des titres d’enseignement supérieur ;</li>
<li>l’homologation.</li></ul>
<p>Le ministère de l’Enseignement supérieur et de la Recherche du Grand-Duché de Luxembourg est en charge de cette homologation.</p>
<p><a href="http://www.mesr.public.lu/" class="spip_out" rel="external">Ministère de l’Enseignement supérieur et de la Recherche</a><br class="manualbr">20 Montée de la Pétrusse<br class="manualbr">L-2273 Luxembourg<br class="manualbr">Tél : (+352) 2478 66 19 <br class="manualbr">Fax : (+352) 26 29 60 37</p>
<p><strong>Pour en savoir plus :</strong></p>
<p>Le site de <a href="http://www.enic-naric.net/" class="spip_out" rel="external">ENIC-NARIC</a> (European Network of Information Centres in the European Region - National Academic Recognition Informations Centres in the European Region).</p>
<p><strong>CV Europass</strong></p>
<p>Vous avez également la possibilité de créer un CV européen, remplacé depuis 2002 par le <strong>CV Europass</strong>.</p>
<p><strong>Europass </strong>a été établi par la décision 2241/2004/CE du Parlement européen et du Conseil du 15 décembre 2004 instaurant un cadre unique pour la transparence et la qualification des compétences. Il a été élaboré en collaboration avec les différents gouvernements de l’Union européenne, des syndicats ainsi que des employeurs, et permet de favoriser la mobilité professionnelle des ressortissants européens à l’intérieur de l’Union européenne.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/emploi-stage/article/curriculum-vitae-109897). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
