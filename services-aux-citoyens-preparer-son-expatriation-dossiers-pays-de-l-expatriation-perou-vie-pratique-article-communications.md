# Communications

<h4 class="spip">Téléphone – Internet</h4>
<p>Les liaisons téléphoniques entre le Pérou et la France sont bonnes. De nombreux fournisseurs d’accès proposent des accès à tarif réduit. Les prix des communications restent toutefois plus élevés qu’en France.</p>
<p>L’indicatif téléphonique du Pérou est le 51, l’indicatif de Lima, le 1.</p>
<p>Pour en savoir plus : <a href="http://www.tv5.org/TV5Site/voyageurs/accueil.php" class="spip_out" rel="external">Tv5.org</a>.</p>
<p>Téléphoner gratuitement par Internet Il est généralement très avantageux d’utiliser <strong>des logiciels de téléphonie sur IP</strong> (Skype, Google talk, Live messenger, Yahoo ! messenger, etc.) car les tarifs proposés à l’international sont souvent moindre que ceux des opérateurs locaux exploitant des lignes classiques.</p>
<h4 class="spip">Poste</h4>
<p>Pour les liaisons postales (lettres, journaux, colis), il faut compter environ 10 jours par voie aérienne et deux mois par voie maritime. Noter que les colis inférieurs à un kilo passent plus facilement la douane.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/vie-pratique/article/communications). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
