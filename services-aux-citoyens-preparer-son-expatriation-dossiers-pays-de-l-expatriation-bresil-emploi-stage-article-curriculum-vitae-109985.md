# Curriculum vitae

<p>Le CV doit être rédigé en portugais. La tendance actuelle au Brésil est de présenter des CV plus synthétiques et concis que par le passé dans lesquels le recruteur doit immédiatement identifier les responsabilités et les résultats antérieurs du candidat.</p>
<p>Si le demandeur est titulaire d’un visa permanent de travail, il convient de le mentionner sur le CV.</p>
<p>Le CV doit être adapté à l’entreprise et au poste pour lequel on postule. Le recruteur doit pouvoir retrouver dans ce dernier tous les renseignements sur les études, les stages, les activités professionnelles passées, les activités sportives et culturelles du candidat. Les pages doivent être dactylographiées et accompagnées de justificatifs (photocopies des diplômes, rapports de stages…).</p>
<p>Il comporte les rubriques suivantes :</p>
<ul class="spip">
<li>L’état civil (estado civil)</li>
<li>Objectif (objectivo)</li>
<li>Les expériences professionnelles (experiências professnals)</li>
<li>La formation (formaçao)</li>
<li>Le cursus (cursos)</li>
<li>Les langues (idiomas)</li>
<li>L’informatique (informatica)</li></ul>
<p>La lettre de candidature n’est pas obligatoire, en rédiger une (dactylographiée) peut donc être un avantage.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/emploi-stage/article/curriculum-vitae-109985). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
