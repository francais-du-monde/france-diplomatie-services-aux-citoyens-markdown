# Animaux domestiques

<p><strong>Le pays de destination se trouve hors de l’Union européenne</strong></p>
<p>Certains pays réglementent l’entrée des animaux sur leur territoire (permis d’importation, quarantaine, interdiction). Prévoyez un délai d’au moins dix jours pour effectuer toutes les formalités, voire de plusieurs mois pour les pays exigeant une quarantaine.</p>
<p>Pour connaître les conditions exactes, vous devrez prendre contact :</p>
<ul class="spip">
<li>Avec <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-etrangers-en-france/article/annuaire-des-representations-etrangeres-en-france" class="spip_in">l’ambassade en France du pays de destination</a>. Si, au cours de son transport de la France vers le pays de destination, l’animal doit transiter par un ou plusieurs pays, vous devrez également vous conformer à la réglementation du ou des pays de transit de l’animal. A noter que certains pays refusent le transit d’animaux.<br class="manualbr">Des informations générales sur la réglementation de nombreux pays sont également disponibles sur le site Internet de l’<a href="http://www.iatatravelcentre.com/" class="spip_out" rel="external">Association internationale du transport aérien (AITA)</a>, ainsi que sur celui de l’<a href="http://www.vet-alfort.fr/" class="spip_out" rel="external">Ecole nationale vétérinaire de Maisons-Alfort</a>.</li>
<li>Le cas échéant, avec la ou les compagnies aériennes pour connaître les conditions de transport de l’animal (en soute ou en cabine, normes des cages, nourriture, etc.).<br class="manualbr">Pour connaître les normes internationales de transport des animaux et des cages, vous pouvez consulter le site Internet de l’<a href="http://www.iata.org/" class="spip_out" rel="external">Association internationale du transport aérien (AITA)</a>. Si l’ambassade dispose d’une information particulière, celle-ci doit être communiquée à votre vétérinaire traitant, titulaire d’un mandat sanitaire, qui vous guidera pour la suite.</li></ul>
<p>Dans le cas où l’ambassade ne disposerait pas d’informations sur la réglementation sanitaire de son pays, vous devrez respecter, par défaut, les conditions suivantes :</p>
<ul class="spip">
<li>l’animal doit être en règle vis-à-vis des exigences réglementaires françaises en matière de santé et de protection animales. Pour plus de renseignements sur la réglementation française, vous pouvez prendre contact avec votre vétérinaire traitant, titulaire d’un mandat sanitaire, ou la Direction Départementale des Services Vétérinaires (DDSV) de votre département. Vous trouverez les coordonnées des DDSV sur le site Internet du <a href="http://agriculture.gouv.fr/" class="spip_out" rel="external">ministère de l’Agriculture</a>.</li>
<li>les documents suivants sont obligatoires pour l’animal et doivent être établis par le vétérinaire traitant, titulaire d’un mandat sanitaire :
<br>— identification par micropuce ou tatouage ;
<br>— certificat de vaccination contre la rage en cours de validité ;
<br>— certificat international de bonne santé, établi par le vétérinaire traitant, titulaire du mandat sanitaire, dans la semaine précédant le départ de France.</li>
<li>il est également conseillé de faire procéder à un titrage des anticorps anti-rabiques dans un laboratoire agréé et de se munir du carnet de vaccination tenu à jour de l’animal. Vous devrez ensuite prendre rendez-vous avec le service " santé et protection animales " de la direction départementale des services vétérinaires (DDSV) dont relève le vétérinaire traitant, pour la validation des documents établis par celui-ci.</li></ul>
<h4 class="spip">Chiens et chats</h4>
<p>La règlementation pour l’importation d’animaux domestiques et particulièrement les chiens et chats est stricte au Ghana ; un certain nombre de certificats et vaccinations sont exigés avant de pouvoir importer votre animal.</p>
<p><strong>Chats :</strong></p>
<ul class="spip">
<li>Vaccination anti-rabbique ;</li>
<li>Vaccination contre la grippe des chats ;</li>
<li>Certificat deliver par un vétérinaire agréé.</li></ul>
<p><strong>Chiens :</strong></p>
<ul class="spip">
<li>Vaccination (voir ci-dessous la liste en anglais des examens et vaccins requis) ;</li>
<li>Test sanguin (de moins d’un mois avant l importation du chien).</li></ul>
<p>Pour en savoir plus :</p>
<ul class="spip">
<li>la rubrique du site du <a href="http://www.mofa.gov.gh/" class="spip_out" rel="external">ministère de l’Agriculture et de l’Alimentation (MOFA) ghanéen qui traite de ces sujets</a> :</li>
<li>le site de la <a href="http://www.thevetsplaceghana.com/" class="spip_out" rel="external">clinique vétérinaire à Accra « The Vet’s place »</a> qui pourra vous renseigner et vous aider dans vos démarches.</li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/entree-et-sejour/article/animaux-domestiques). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
