# Recherche d’emploi

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/emploi-stage/article/recherche-d-emploi#sommaire_1">Outils pour la recherche d’emploi</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/emploi-stage/article/recherche-d-emploi#sommaire_2">Organismes pour la recherche d’emploi</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Outils pour la recherche d’emploi</h3>
<h4 class="spip">Sites internet</h4>
<ul class="spip">
<li><a href="http://www.odia.com.br/" class="spip_out" rel="external">Odia.com.br</a></li>
<li><a href="http://www.vagas.com.br/" class="spip_out" rel="external">Vagas.com.br</a></li>
<li><a href="http://www.apinfo.com/" class="spip_out" rel="external">Apinfo.com</a></li>
<li><a href="http://www.empregos.net/" class="spip_out" rel="external">Empregos.net</a></li>
<li><a href="http://www.curriculum.com.br/" class="spip_out" rel="external">Curriculum.com.br</a></li>
<li><a href="http://www.catho.com.br/" class="spip_out" rel="external">Catho.com.br</a></li>
<li><a href="http://www.contacto-rh.com.br/" class="spip_out" rel="external">Contacto-rh.com.br</a></li>
<li><a href="http://www.ltm.com.br/" class="spip_out" rel="external">Ltm.com.br</a></li>
<li><a href="http://www.timaster.com.br/" class="spip_out" rel="external">Timaster.com.br</a> (technologies de l´information, informatique et internet)</li>
<li><a href="http://www.infojobs.com.br/" class="spip_out" rel="external">Infojobs.com.br</a></li>
<li><a href="http://www.empregos.com.br/" class="spip_out" rel="external">Empregos.com.br</a></li>
<li><a href="http://www.manager.com.br/" class="spip_out" rel="external">Manager.com.br</a></li>
<li><a href="http://www.central-emprego.com/" class="spip_out" rel="external">Central-emprego.com</a></li>
<li><a href="http://www.anunciosbrasil.com.br/" class="spip_out" rel="external">Anunciosbrasil.com.br</a></li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Organismes pour la recherche d’emploi</h3>
<h4 class="spip">Chambre de commerce France-Brésil (CCFB)</h4>
<p><a href="http://www.ccfb.com.br/" class="spip_out" rel="external">CCFB - Sao Paulo</a><br class="manualbr">Tél. : [55] (11) 30 60 22 90 <br class="manualbr">Fax : [55] (11) 30 61 15 53<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/emploi-stage/article/recherche-d-emploi#rh#mc#ccfb.com.br#" title="rh..åt..ccfb.com.br" onclick="location.href=mc_lancerlien('rh','ccfb.com.br'); return false;" class="spip_mail">Courriel</a></p>
<p><strong>CCFB - Rio de Janeiro</strong><br class="manualbr">Tel. : 55 (21) 2220-1015<br class="manualbr">Fax : 55 (21) 2533-3925<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/emploi-stage/article/recherche-d-emploi#rhrj#mc#ccfb.com.br#" title="rhrj..åt..ccfb.com.br" onclick="location.href=mc_lancerlien('rhrj','ccfb.com.br'); return false;" class="spip_mail">Courriel</a></p>
<p><strong>CCFB – Paraná</strong><br class="manualbr">Tel. : (41) 3254-2854<br class="manualbr">Fax : (41) 3254-2854<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/emploi-stage/article/recherche-d-emploi#ccfbpr#mc#ccfb.com.br#" title="ccfbpr..åt..ccfb.com.br" onclick="location.href=mc_lancerlien('ccfbpr','ccfb.com.br'); return false;" class="spip_mail">Courriel</a></p>
<p>CCFB - Minas Gerais<br class="manualbr">Tel : +55 (31) 3213 1576<br class="manualbr">Fax : +55 (31) 3213 1577<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/emploi-stage/article/recherche-d-emploi#ccfbmg#mc#ccfb.com.br#" title="ccfbmg..åt..ccfb.com.br" onclick="location.href=mc_lancerlien('ccfbmg','ccfb.com.br'); return false;" class="spip_mail">Courriel</a></p>
<p>La <a href="http://www.ccfb.com.br/" class="spip_out" rel="external">CCFB</a>, créée en 1900, publie un annuaire de ses membres qui peut être commandé à partir de son site Internet.</p>
<p>Il est également possible de passer une annonce dans la revue France-Brésil éditée par la Chambre de commerce et diffusée auprès des associés de la CCFB.</p>
<h4 class="spip">Mission économique</h4>
<p>La Mission économique publie également un annuaire des filiales des sociétés françaises au Brésil. La dernière édition date de 2009 et peut être commandée sur le <a href="http://www.ubifrance.fr/" class="spip_out" rel="external">site Internet de Business France</a> / rubrique "Librairie internationale "</p>
<h4 class="spip">Comité consulaire pour l’emploi et la formation professionnelle (CCPEFP)</h4>
<p><strong>Sao Paulo</strong><br class="manualbr">c/o <a href="http://www.ccfb.com.br/" class="spip_out" rel="external">Chambre de commerce France-Brésil à Sao Paulo</a><br class="manualbr">Alameda Itu, 852 - 19° andar - Sao Paulo 01421- 001<br class="manualbr">Téléphone : [55] (11) 30 60 22 90 <br class="manualbr">Télécopie : [55] (11) 30 61 15 53<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/emploi-stage/article/recherche-d-emploi#rh#mc#ccfb.com.br#" title="rh..åt..ccfb.com.br" onclick="location.href=mc_lancerlien('rh','ccfb.com.br'); return false;" class="spip_mail">Courriel</a></p>
<p>Il existe par ailleurs un grand nombre d´agences de recrutement généralistes ou spécialisées et de chasseurs de têtes qui ne recrutent en général que les résidents (ayant une carte de séjour et de travail de permanent).</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/bresil/emploi-stage/article/recherche-d-emploi). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
