# Curriculum vitae

<p>Le CV doit être rédigé en espagnol. Les CV font généralement plus d’une page. Il est parfois utile de fournir un CV "documenté" (copie des diplômes, lettres de recommandation ou à défaut certificats de travail). Les niveaux d’études étant différents, il est recommandé de chercher l’équivalence et/ou de l’expliquer.</p>
<p>Peu utilisées au Pérou, les <strong>lettres de motivation</strong> font généralement très bonne impression.</p>
<p>Il est souvent demandé d’indiquer sur son CV ou lors de l’entretien ses prétentions salariales. Un salaire trop élevé est souvent éliminé d’office. Beaucoup d’entretiens à tests sont réalisés (psychologiques, études de cas, etc).</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/emploi-stage/article/curriculum-vitae-111264). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
