# Convention fiscale

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/fiscalite-22971/article/convention-fiscale-111017#sommaire_1">Champ d’application de la convention</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/fiscalite-22971/article/convention-fiscale-111017#sommaire_2">Dispositions conventionnelles pour certaines catégories de revenus</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/fiscalite-22971/article/convention-fiscale-111017#sommaire_3">Elimination de la double imposition</a></li></ul>
<p>La France et l’Afrique du Sud ont signé à Paris le <strong>8 novembre 1993</strong> une convention en vue d’éviter les doubles impositions et de prévenir l’évasion et la fraude fiscales en matière d’impôts sur le revenu et sur la fortune.</p>
<p>Cette convention est entrée en vigueur le <strong>1er novembre 1995</strong> et a été publiée au Journal officiel du 23 novembre 1995 (décret n°95-1236 du 16 novembre 1995).</p>
<p>Le texte de la convention peut être consulté sur le site Internet de l’<a href="http://www.impots.gouv.fr/" class="spip_out" rel="external">administration fiscale</a>.</p>
<h3 class="spip"><a id="sommaire_1"></a>Champ d’application de la convention</h3>
<p>Cet accord a pour objet de protéger les résidents de chacun des Etats contractants en matière d’impôts prélevés directement sur le revenu ou sur la fortune.</p>
<h4 class="spip">Personnes concernées</h4>
<p>L’article 1 de la Convention s’applique aux personnes qui sont des résidents d’un Etat contractant ou des deux Etats contractants.</p>
<h4 class="spip">Notion de résidence </h4>
<p>Aux termes de l’article 4, paragraphe 1, de la convention, une personne est considérée comme "résident d’un Etat contractant" lorsque, en vertu de la législation dudit Etat, elle se trouve assujettie à l’impôt à raison de son domicile, de sa résidence ou de critères analogues.</p>
<p>Le paragraphe 2 du même article prévoit que si l’assujettissement à l’impôt ne pouvait suffire, d’autres critères permettent également de résoudre le cas de double résidence.</p>
<p>Ces critères sont :</p>
<ul class="spip">
<li>un foyer d’habitation permanent (il s’agit, par exemple, du lieu de situation du conjoint ou des enfants) ;</li>
<li>l’Etat où la personne possède le centre de ses intérêts vitaux (tant professionnels que privés) ;</li>
<li>l’Etat dans lequel elle séjourne de façon habituelle (notion de 183 jours de présence physique sur le territoire au cours d’une année fiscale) ;</li>
<li>à défaut, l’Etat dont elle possède la nationalité.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Dispositions conventionnelles pour certaines catégories de revenus</h3>
<p>Cette analyse ne tient pas compte des dispositions conventionnelles relatives aux revenus de capitaux mobiliers.</p>
<h4 class="spip">Traitements, salaires, pensions et rentes</h4>
<h5 class="spip">Rémunérations privées</h5>
<p>L’article 15, paragraphe 1, précise que les traitements et salaires d’origine privée ne sont, en règle générale, imposables que dans l’Etat où <strong>s’exerce l’activité personnelle</strong>.</p>
<p>Exceptions à cette règle générale :</p>
<p>Le maintien de l’imposition dans l’Etat de résidence du bénéficiaire est prévu par le paragraphe 2 du même article sous réserve de trois conditions simultanément remplies :</p>
<ul class="spip">
<li>le séjour temporaire du bénéficiaire dans l’autre Etat ne dépasse pas une durée totale de 183 jours au cours de l’année civile ;</li>
<li>la rémunération est payée par un employeur qui n’est pas résident de l’Etat d’exercice ;</li>
<li>la rémunération ne doit pas être à la charge d’un établissement stable ou d’une base fixe de l’employeur dans l’Etat. Exemple : Monsieur X est envoyé en Afrique du Sud trois mois, soit 90 jours (mai, juin, juillet de l’année n) par une PME fabriquant de la maroquinerie en vue de prospecter le marché d’Afrique du Sud. Cette entreprise ne dispose d’aucune succursale ni bureau en Afrique du Sud. Dans un tel cas, Monsieur X devra déclarer ses revenus en France.</li></ul>
<p>Au contraire, si Monsieur X est envoyé du mois de février inclus au mois de novembre inclus, son séjour de plus de 183 jours en Afrique du Sud entraîne son imposition dans ce pays.</p>
<p>Il résulte des dispositions du paragraphe 3 de l’article 15 de la Convention que les rémunérations des salariés autres qu’intérimaires employés à bord d’un navire, d’un aéronef en trafic international ou à bord d’un bateau servant à la navigation intérieure ne sont imposables que dans l’Etat où se trouve la direction effective de l’entreprise.</p>
<h5 class="spip">Rémunérations publiques</h5>
<p>L’article 19, paragraphe 1-a et 2-a indique que les traitements, salaires et rémunérations analogues ainsi que les <strong>pensions de retraite</strong> payés par un Etat ou une personne morale de droit public de cet Etat restent imposables dans cet Etat.</p>
<p>Exemple : Monsieur X est fonctionnaire de l’Etat Français, résident en activité en France et décide d’aller prendre sa retraite chez l’un de ses enfants en Afrique du Sud. Les montants de ses pensions resteront imposés en France ; son dossier étant pris en charge par un Centre des Impôts spécial, le Centre des Impôts des Non-Résidents.</p>
<p>Toutefois, en vertu de l’article 19, paragraphe 1-b et 2-b, cette règle ne s’applique pas lorsque le bénéficiaire possède <strong>la nationalité de l’autre Etat</strong> sans être en même temps ressortissant de l’Etat payeur. L’imposition est réservée à l’Etat dont l’intéressé est le résident.</p>
<p>D’autre part, en vertu des dispositions du paragraphe 3 du même article, les règles fixées aux paragraphes 1 et 2 dudit article ne sont pas applicables aux rémunérations ou aux pensions versées au titre <strong>de services rendus dans le cadre d’une activité industrielle ou commerciale exercée par un Etat ou une personne morale de droit public</strong>.</p>
<p>Les sommes versées à ce titre sont imposées soit dans l’Etat d’exercice de l’activité (article 15 de la Convention), soit dans l’Etat de résidence du bénéficiaire (article 18 de la Convention).</p>
<p><strong>Exemples : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Monsieur X, agent E.D.F. est envoyé en Afrique du Sud afin d’effectuer des travaux de conception avec les services locaux. Monsieur X est rémunéré par E.D.F. France. E.D.F. étant un établissement de droit public à caractère industriel et commercial, les rémunérations allouées à Monsieur X seront imposées en Afrique du Sud. </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Monsieur X, retraité de la S.N.C.F., perçoit une pension de la Caisse vieillesse de la S.N.C.F. Or, Monsieur X, résident en France a décidé de vivre à Johannesbourg. Cette retraite se verra ainsi imposable en Afrique du Sud puisque la S.N.C.F. a le caractère d’un établissement public, industriel et commercial. </p>
<h5 class="spip">Pensions et rentes</h5>
<p>L’article 18 prévoit que les pensions de retraite de source privée ainsi que les rentes viagères restent imposables dans l’Etat dont le bénéficiaire est un résident.</p>
<p>Cette disposition couvre également les pensions publiques auxquelles les règles de l’article 19 ne sont pas applicables.</p>
<p>Exemple : Monsieur X, citoyen français qui a exercé une activité salariée en Afrique, décide de venir prendre sa retraite en France. Les pensions versées par l’Afrique du Sud au titre de cette activité sont imposables en France.</p>
<h5 class="spip">Etudiants, stagiaires</h5>
<p>L’article 20 de la Convention prévoit que les étudiants, les stagiaires d’un Etat qui séjournent dans l’autre Etat à seule fin d’y poursuivre leurs études ou leur formation et qui perçoivent des subsides d’origine étrangère à cet Etat sont exonérés d’impôt par ce dernier Etat.</p>
<h4 class="spip">Autres catégories de revenus</h4>
<h5 class="spip">Bénéfices industriels et commerciaux</h5>
<p>L’article 7, paragraphe 1, dispose que les entreprises industrielles et commerciales sont imposables sur le territoire où se trouve un établissement stable.</p>
<h5 class="spip">Bénéfices des professions non commerciales et des revenus non commerciaux</h5>
<p>L’article 14, paragraphe 1, dispose que les revenus provenant de l’exercice d’une profession libérale ou d’autres activités indépendantes sont imposables dans l’Etat sur le territoire duquel se trouve l’installation fixe où s’exerce de façon régulière l’activité personnelle.</p>
<p>Les revenus que les professionnels du spectacle ainsi que les sportifs réalisent en cette qualité dans l’un des deux Etats, à titre indépendant, restent imposables dans l’Etat d’exercice de l’activité selon les dispositions de l’article 17 de la Convention, paragraphes 1 et 2.</p>
<p>L’article 12, paragraphe 1 pose en principe que les revenus non commerciaux (redevances et droits d’auteur) sont imposables dans l’Etat de résidence du bénéficiaire, sous couvert de la production de formulaires spécifiques remis auprès des autorités fiscales de l’Etat dont relève le créancier des revenus.</p>
<h5 class="spip">Revenus immobiliers</h5>
<p>L’article 6, paragraphe 1 dispose que les revenus des biens immobiliers y compris les bénéfices des exploitations agricoles sont imposables dans l’Etat où ils sont situés.</p>
<p>Cette règle s’applique également aux gains provenant de la cession ou de l’échange desdits biens ou droits selon les dispositions de l’article 13, paragraphe 1-a.</p>
<p>En ce qui concerne les gains provenant de l’aliénation de tous biens autres que mobiliers ou immobiliers, le paragraphe 4 de l’article 13 précise qu’ils restent imposables dans l’Etat de résidence du cédant.</p>
<h3 class="spip"><a id="sommaire_3"></a>Elimination de la double imposition</h3>
<p>L’élimination de la double imposition pour les résidents de France qui perçoivent des revenus de source sud-africaine s’opère aux termes du paragraphe 1 de l’article 23 selon le régime de l’imputation.</p>
<p>Les revenus de source française ou sud-africaine pour lesquels le droit d’imposer est dévolu à titre exclusif à l’Afrique du Sud doivent être maintenus en dehors de la base de l’impôt français (article 23, paragraphe 3-a), réserve faite toutefois de leur prise en compte pour la détermination du taux effectif.</p>
<p>Le calcul du taux effectif consiste à calculer l’impôt sur les seuls revenus imposables en France mais en appliquant le taux d’imposition correspondant à l’ensemble des revenus de sources françaises et étrangère.</p>
<p>Au résultat ainsi obtenu peuvent être appliquées les réfactions prévues par la loi interne.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/fiscalite-22971/article/convention-fiscale-111017). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
