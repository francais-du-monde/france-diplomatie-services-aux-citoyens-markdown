# Marché du travail

<p><strong>Israël affiche un taux de chômage dans la moyenne des pays occidentaux.</strong> Au plus fort de la crise financière, en 2009, il avait atteint les 7,7% mais est redescendu depuis à 6,8% pour l’année 2010.</p>
<p>On compte en Israël <strong>3,5 millions de salariés</strong>, dont <strong>130 000 étrangers</strong>, 1/4 d’entre eux étant palestiniens (chiffres approximatifs). Les secteurs qui embauchent le plus d’étrangers sont ceux de l’agriculture et des soins à domicile mais les salaires y sont assez bas (voire très bas pour les soins à domicile, voir ci-dessous).</p>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/emploi-stage/article/marche-du-travail#sommaire_1">Secteurs à fort potentiel</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/emploi-stage/article/marche-du-travail#sommaire_2">Secteurs à faible potentiel</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/emploi-stage/article/marche-du-travail#sommaire_3">Professions règlementées</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/emploi-stage/article/marche-du-travail#sommaire_4">Rémunération</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Secteurs à fort potentiel</h3>
<p>Les secteurs à fort potentiel sont la technologie (énergies renouvelables, infrastructures, aéronautique, pièces automobiles, équipement médical, pharmacie, biotechnologies…) et l’informatique. Le tourisme, l’agriculture et la restauration sont des secteurs qui embauchent aussi beaucoup d’étrangers.</p>
<h3 class="spip"><a id="sommaire_2"></a>Secteurs à faible potentiel</h3>
<p>La <strong>fonction publique n’est pas accessible aux étrangers</strong>.</p>
<h3 class="spip"><a id="sommaire_3"></a>Professions règlementées</h3>
<p>Certaines professions nécessitent des permis d’exercer en Israël : professions médicales ou paramédicales, avocats, experts-comptables, assureurs, agents de voyages, guides touristiques, chauffeurs, électriciens, agents immobiliers, maîtres nageurs, chimistes, etc.</p>
<p>Il existe toutefois des examens de qualification et des cours de préparation associés accessibles aux immigrants par le biais des « oulpan », pour plus d’information, voir la <a href="http://www.moia.gov.il/Moia_fr/Employment/VocationalUlpan.htm" class="spip_out" rel="external">page dédiée à la qualification professionnelle sur le site du ministère de l’Intégration israélien</a>.</p>
<h3 class="spip"><a id="sommaire_4"></a>Rémunération</h3>
<p>Au premier janvier 2011, le <strong>salaire minimum</strong> légal est de <strong>4100 shekel par mois</strong>. Il est fixé par la loi à <strong>45% du salaire moyen</strong> publié par le bureau central des statistiques d’Israël.</p>
<p>Le <strong>salaire brut mensuel moyen </strong>en Israël est de <strong>8307 ILS par mois</strong> en 2011.</p>
<p>De fortes disparités géographiques existent : dans l’hôtellerie, par exemple, les salaires moyens à Tel Aviv sont supérieurs de 50% de ceux à Tibériade ou Eilat. Le niveau de salaire varie aussi en fonction du secteur d’activité : dans la construction, le salaire moyen est de 7580 ILS / mois, alors qu’il n’est que de 5557 ILS / mois dans l’agriculture.</p>
<p>Les <strong>étrangers travaillant sur le sol israélien</strong> (hors Palestiniens) ont cependant un salaire moyen <strong>très inférieur</strong> à la moyenne nationale : <strong>4326 shekels / mois</strong>. Cela se ressent particulièrement dans le domaine de la <strong>santé</strong> (2300 shekels / mois en moyenne pour les étrangers), dans les <strong>affaires</strong> (4139 shekels/mois) ou dans <strong>l’agriculture</strong> (4760 shekels/mois). Leur situation est cependant meilleure dans <strong>l’hôtellerie, la restauration et la construction</strong> (6 200 shekels/mois). Le salaire moyen dans les autres secteurs est de <strong>7800 shekels/mois</strong>.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p>Sources : Ambassade de France en Israël, <a href="https://www.cia.gov/library/publications/the-world-factbook/geos/is.html" class="spip_out" rel="external"><i>CIA world factbook</i></a>, <a href="http://www1.cbs.gov.il/reader/cw_usr_view_Folder?ID=141" class="spip_out" rel="external"><i>Israel Central Bureau of Statistics</i></a>, <a href="http://www.moia.gov.il/" class="spip_out" rel="external"><i>Ministère de l’Intégration israélien</i></a> et <a href="http://www.israelvalley.com/" class="spip_out" rel="external"><i>CCFI</i></a></p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/emploi-stage/article/marche-du-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
