# Vaccination

<p>Aucune vaccination n’est obligatoire pour se rendre au Canada. Les vaccinations suivantes sont cependant recommandées : diphtérie, poliomyélite, tétanos et hépatite B.</p>
<p>Les personnes travaillant dans la restauration auront également tout intérêt à se faire vacciner contre l’hépatite A.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li>Lisez notre rubrique thématique <a href="services-aux-citoyens-preparer-son-expatriation-sante.md" class="spip_in">Santé</a>.</li>
<li>Site de l’<a href="http://www.phac-aspc.gc.ca/" class="spip_out" rel="external">Agence de la Santé publique du Canada</a>.</li></ul>
<p><i>Mise à jour : avril 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/entree-et-sejour/article/vaccination-112480). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
