# Convention fiscale

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/fiscalite/article/convention-fiscale-111295#sommaire_1">Champ d’application de la convention</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/fiscalite/article/convention-fiscale-111295#sommaire_2">Règles d’imposition</a></li></ul>
<p>La France et le Portugal ont signé <strong>le 14 janvier 1971</strong> une convention en vue d’éviter les doubles impositions et de prévenir l’évasion fiscale en matière d’impôts sur le revenu et sur la fortune. Cette convention est <strong>entrée en vigueur le 4 janvier 1973.</strong></p>
<p>Son texte intégral est consultable sur le <a href="http://www.impots.gouv.fr" class="spip_out" rel="external">site Internet de l’administration fiscale</a>.</p>
<p>Voici un résumé de ses points clés pour les particuliers :</p>
<h3 class="spip"><a id="sommaire_1"></a>Champ d’application de la convention</h3>
<h4 class="spip">Personnes concernées et impôts visés</h4>
<p>Cet accord a pour objet de protéger les résidents de chacun des Etats contractants en vue d’éviter les doubles impositions et d’établir des règles d’assistance administrative réciproque en matière d’impôts sur le revenu. La Convention trouve donc à s’appliquer aux résidents des deux Etats.</p>
<h4 class="spip">Notion de résidence</h4>
<p>L’article 4, paragraphe 1 de la Convention précise que l’expression « résident d’un Etat contractant » désigne la personne qui, en vertu de la législation de cet Etat, y est assujettie à l’impôt en raison de son domicile, de sa résidence, de son siège de direction ou de tout autre critère de nature analogue.</p>
<p>Le paragraphe 2 du même article énumère des critères subsidiaires permettant de résoudre le cas de double résidence si l’assujettissement à l’impôt ne pouvait suffire.</p>
<p>Ces critères sont :</p>
<ul class="spip">
<li>le foyer d’habitation permanent ;</li>
<li>le centre de ses intérêts vitaux (Etat où les liens personnels et économiques sont les plus étroits) ;</li>
<li>le lieu de séjour habituel ;</li>
<li>à défaut, l’Etat dont elle possède la nationalité.</li></ul>
<p>Si une personne possède la double nationalité ou si elle ne possède la nationalité d’aucun des deux Etats, la question est tranchée d’un commun accord par les autorités des deux Etats contractants (article 4, paragraphe 2d).</p>
<h3 class="spip"><a id="sommaire_2"></a>Règles d’imposition</h3>
<h4 class="spip">Elimination de la double imposition</h4>
<p>La double imposition est évitée selon un dispositif prévu par l’article 24 de la convention.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  En ce qui concerne la France : les revenus dont l’imposition est attribuée au Portugal par la Convention sont exonérés des impôts français, réserve faite toutefois de leur prise en compte pour la détermination du taux effectif.</p>
<p>Les revenus de source portugaise imposés au Portugal donnent droit, au bénéficiaire résident de France, à un crédit d’impôt.</p>
<ul class="spip">
<li>En ce qui concerne le Portugal : un résident du Portugal qui reçoit des revenus imposables en France a droit à une déduction égale à l’impôt français payé sur ces revenus. Toutefois, la somme déduite ne peut excéder le moins élevé de ces montants :</li>
<li>la fraction de l’impôt français correspondant à la fraction du revenu imposé au Portugal ;</li>
<li>la fraction de l’impôt portugais sur le revenu, calculé avant la déduction, correspondant aux revenus imposés en France.</li></ul>
<p>L’analyse qui suit ne tient pas compte des dispositions conventionnelles relatives aux revenus de capitaux mobiliers.</p>
<h4 class="spip">Rémunérations privées</h4>
<p>L’article 16 paragraphe 1 précise, sous réserves des dispositions contenues dans les articles suivants, que les traitements et salaires d’origine privée qu’un résident d’un Etat reçoit au titre d’un emploi salarié sont imposables dans l’Etat où est exercée l’activité.</p>
<p>Exceptions à cette règle générale :</p>
<ul class="spip">
<li>le maintien de l’imposition dans l’Etat de résidence du bénéficiaire est prévu par le paragraphe 2 du même article sous réserve de trois conditions simultanément remplies :</li>
<li>le séjour temporaire du bénéficiaire dans l’autre Etat ne dépasse pas une durée totale de 183 jours ;</li>
<li>la rémunération est payée par un employeur qui n’est pas résident de l’Etat d’exercice ;</li>
<li>la rémunération ne doit pas être à la charge d’un établissement stable ou d’une base fixe de l’employeur dans l’Etat d’exercice de l’activité.</li></ul>
<p>Il résulte des dispositions du paragraphe 3 de l’article 16 de la convention que les revenus professionnels des salariés autres qu’intérimaires employés à bord d’un navire, d’un aéronef en trafic international ou à bord d’un bateau servant à la navigation intérieure ne sont imposables que dans l’Etat où se trouve la direction effective de l’entreprise.</p>
<h4 class="spip">Rémunérations publiques</h4>
<p>L’article 20, paragraphe 1 indique que les traitements, salaires, rémunérations analogues et les pensions de retraite payés par un Etat ou l’une de ses collectivités locales sont imposables dans l’Etat qui les verse.</p>
<p>Ne sont pas concernées par cette règle, les rémunérations et pensions publiques correspondant à une activité industrielle ou commerciale exercée par l’un des Etats contractants ou l’une de ses collectivités locales.</p>
<p>Cette règle ne s’applique pas lorsque le bénéficiaire possède la nationalité de l’autre Etat sans être en même temps ressortissant de l’Etat payeur ; l’imposition est alors réservée à l’Etat dont l’intéressé est le résident.</p>
<h4 class="spip">Pensions et rentes</h4>
<p>L’article 19 prévoit que les pensions de retraite de source privée ainsi que les rentes viagères restent imposables dans l’Etat dont le bénéficiaire est un résident.</p>
<h4 class="spip">Etudiants, stagiaires</h4>
<p>L’article 22, paragraphe 1, de la convention prévoit que les étudiants, les stagiaires d’un Etat qui séjournent dans l’autre Etat à seule fin d’y poursuivre leurs études ou leur formation n’y sont pas imposables si les sommes reçues pour couvrir leurs frais d’entretien, d’études ou de formation sont d’origine étrangère.</p>
<h4 class="spip">Enseignants</h4>
<p>L’article 21 précise que les enseignants résidents d’un Etat se rendant temporairement dans l’autre Etat en vue d’y exercer, dans un établissement appartenant à l’Etat, une activité pédagogique pendant une période ne dépassant pas deux ans, sont exonérés d’impôt dans chaque Etat pour la rémunération perçue au titre de cet enseignement qui a sa source dans l’autre Etat.</p>
<h4 class="spip">Autres catégories de revenus</h4>
<h5 class="spip">Bénéfices industriels et commerciaux</h5>
<p>L’article 7, paragraphe 1, dispose que les entreprises industrielles et commerciales sont imposables sur le territoire où se trouve un établissement stable.</p>
<h5 class="spip">Bénéfices des professions non commerciales et des revenus non commerciaux</h5>
<p>L’article 15, paragraphe 1, dispose que les revenus provenant de l’exercice d’une profession libérale ou d’autres activités indépendantes sont imposables dans l’Etat sur le territoire duquel se trouve l’installation permanente où s’exerce de façon régulière l’activité personnelle.</p>
<p>Les revenus que les professionnels du spectacle ainsi que les sportifs réalisent en cette qualité dans l’un des deux Etats, à titre indépendant, restent imposables dans l’Etat contractant dont le bénéficiaire est un résident. Toutefois, ces revenus peuvent également être imposés dans l’Etat d’exercice de l’activité, selon la législation de cet Etat (article 18 de la convention, paragraphe 2).</p>
<p>L’article 13, paragraphe 1, pose en principe que les redevances et droits d’auteur sont imposables dans l’Etat de résidence du bénéficiaire. Toutefois, selon le paragraphe 2, ces redevances peuvent être imposées dans l’Etat d’où elles proviennent. L’impôt ne peut excéder 5 % du montant brut des redevances.</p>
<h4 class="spip">Revenus immobiliers</h4>
<p>L’article 6, paragraphe 1, dispose que les revenus des biens immobiliers, y compris les bénéfices des exploitations agricoles, sont imposables dans l’Etat où ils sont situés.</p>
<p>Cette règle s’applique également aux gains provenant de la cession ou de l’échange de ces biens ou droits selon les dispositions de l’article 14, paragraphe 1.</p>
<p>En ce qui concerne les gains provenant de l’aliénation de tous biens autres que ceux visés aux paragraphes 1 et 2 de l’article 14, le paragraphe 3 de ce même article précise qu’ils restent imposables dans l’Etat de résidence du cédant.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/fiscalite/article/convention-fiscale-111295). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
