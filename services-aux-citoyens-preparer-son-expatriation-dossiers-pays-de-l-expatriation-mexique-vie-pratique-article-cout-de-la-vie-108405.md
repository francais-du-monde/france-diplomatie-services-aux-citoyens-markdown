# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/vie-pratique/article/cout-de-la-vie-108405#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/vie-pratique/article/cout-de-la-vie-108405#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/vie-pratique/article/cout-de-la-vie-108405#sommaire_3">Alimentation</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/vie-pratique/article/cout-de-la-vie-108405#sommaire_4">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>L’unité monétaire est le peso mexicain. Son sigle est $ mxn, à ne pas confondre avec le dollar américain.</p>
<p>En août 2013, le peso mexicain vaut 0,0598 euros, c’est-à-dire qu’un euro équivaut à 16,72 pesos*. Son taux vis-à-vis de l’euro est fluctuant et dépend largement de l’évolution du taux €/USD.</p>
<p>Le paiement en liquide ou par carte bancaire est le plus utilisé. Les chèques sont parfois acceptés pour le règlement de services, mais pratiquement jamais dans les commerces. Les cartes bancaires sont acceptées dans la plupart des commerces mais il convient d’être prudent dans la mesure où de nombreuses fraudes ont été constatées.</p>
<p>Il est recommandé d’être très vigilant lors de retraits d’argent dans les distributeurs automatiques. Utiliser de préférence ceux qui se trouvent à l’intérieur des supermarchés ou centres commerciaux.</p>
<p>Les banques françaises présentes au Mexique (BNP-Paribas, CIC, Crédit Agricole, Natexis, Société Générale) ne gèrent pas les comptes de particuliers et travaillent en tant que banque de financement et d’investissement.</p>
<p>Les principales banques mexicaines sont Banamex et BBVA Bancomer, Banorte, HSBC, Santander et Scotia Bank. Elles sont ouvertes en semaine généralement entre 9h00 et 16h00 et le samedi matin. Il existe de nombreux bureaux de change en dehors des banques.</p>
<p>* <a href="http://www.xe.com/ucc/fr/" class="spip_out" rel="external">Convertisseur de devises</a></p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<p>Il existe de nombreux bureaux de change en dehors des banques. Les transferts de fonds sont libres. Il est possible de demander à une banque française d’adresser un virement sur le compte ouvert par l’intéressé dans une banque mexicaine.</p>
<h3 class="spip"><a id="sommaire_3"></a>Alimentation</h3>
<p>Prix moyen d’un repas dans un restaurant</p>
<p>Le prix moyen d’un repas dans un restaurant de qualité supérieure varie de 400 à 500 pesos (25 à 30 euros environ) et nettement plus en cas de consommation de vin (650 à 750 pesos) et dans un restaurant de qualité moyenne de 100 à 250 pesos. Il est d’usage de laisser un pourboire d’environ 10 à 15 %.</p>
<h3 class="spip"><a id="sommaire_4"></a>Evolution des prix</h3>
<p>Evolution de l’indice des prix au cours des cinq dernières années :</p>
<ul class="spip">
<li>2008 : 5,1 %</li>
<li>2009 : 5,3 %</li>
<li>2010 : 4,2 %</li>
<li>2011 : 3,4 %</li>
<li>2012 : 4,1 %</li></ul>
<p><i>Mise à jour : septembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/vie-pratique/article/cout-de-la-vie-108405). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
