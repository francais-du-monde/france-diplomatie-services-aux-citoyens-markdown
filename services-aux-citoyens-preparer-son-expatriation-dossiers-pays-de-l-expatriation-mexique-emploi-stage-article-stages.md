# Stages

<p>Pour trouver un stage au Mexique, plusieurs options s’offrent à vous :</p>
<ul class="spip">
<li>Envoyer des candidatures spontanées aux grands groupes français implantés au Mexique et aux entreprises gérées par des compatriotes. Pour cela vous pouvez consulter la liste des entreprises membres de la <a href="http://www.franciamexico.com" class="spip_out" rel="external">Chambre franco-mexicaine de commerce et d’industrie (CFMCI)</a> et contacter les entreprises qui vous intéressent.</li>
<li>Envoyer votre CV en espagnol avec vos dates de stage au <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/emploi-stage/article/stages#emploi2#mc#cfmci.com#" title="emploi2..åt..cfmci.com" onclick="location.href=mc_lancerlien('emploi2','cfmci.com'); return false;" class="spip_mail">service emploi et formation de la CFMCI</a>. Votre CV sera alors diffusé à l’ensemble des entreprises membres de la CFMCI et sera automatiquement envoyé aux entreprises sollicitant des stagiaires. En 2012, ce service a placé 94 stagiaires français au Mexique.</li>
<li>Postuler par le biais d’étudiants ou d’anciens étudiants de vos écoles ou universités qui ont déjà effectué un stage au Mexique.</li></ul>
<ul class="spip">
<li>Postuler via des sites internet spécialisés :<ul class="spip">
<li><a href="http://jobs-stages.letudiant.fr/stages-etudiants/offres/pays-3996063/page-1.html" class="spip_out" rel="external">http://jobs-stages.letudiant.fr/stages-etudiants/offres/pays-3996063/page-1.html</a></li>
<li><a href="http://www.ies-consulting.es/stage/stage/mexique.php" class="spip_out" rel="external">http://www.ies-consulting.es/stage/stage/mexique.php</a></li></ul></li></ul>
<p><strong>Le visa de stagiaire</strong><br class="manualbr">Les démarches sont simplifiées pour les étudiants qui souhaitent réaliser un stage non rémunéré au Mexique pour une durée inférieure à 180 jours puisque ceux-ci n’auront plus besoin de visa de travail et pourront entrer sur le territoire en qualité de touristes.<br class="manualbr">Pour les autres cas de figure (stage rémunéré et/ou durée supérieure à 180 jours), consultez directement la section consulaire de l’Ambassade du Mexique en France.</p>
<p>Contact :<br class="manualbr"><a href="http://embamex.sre.gob.mx/francia/index.php/es/visas/191" class="spip_out" rel="external">Section consulaire de l’Ambassade du Mexique</a><br class="manualbr">Service des visas ouvert au public du lundi au vendredi de 9h00 à 12h00<br class="manualbr">4, rue Notre Dame des Victoires - 75002 Paris<br class="manualbr">Tél : 01 42 86 56 26 et 01 42 86 56 40<br class="manualbr">Fax : 01 49 26 02 78<br class="manualbr">Courriel : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/emploi-stage/article/stages#consularmex#mc#wanadoo.fr#" title="consularmex..åt..wanadoo.fr" onclick="location.href=mc_lancerlien('consularmex','wanadoo.fr'); return false;" class="spip_mail">mailto:consularmex<span class="spancrypt"> [at] </span>wanadoo.fr</a></p>
<h4 class="spip">Le volontariat international (V.I.E ou V.I.A.) ?</h4>
<p>Le V.I.E. permet aux entreprises françaises de confier à un jeune ressortissant de l’espace économique européen, âgé de 18 à 28 ans, une mission professionnelle (rémunérée) à l’étranger durant une période modulable de 6 à 24 mois.</p>
<p>Au Mexique, la plupart des V.I.E. proposés sont destinés à des <strong>profils d’ingénieurs ou de technico-commerciaux </strong>ainsi que pour des postes de <strong>Business Development</strong> afin de réaliser les premières étapes de l’implantation de l’entreprise sur le marché mexicain.</p>
<p>L’indemnisation offerte pour le Mexique est de l’ordre de 2,196€ mensuels (à compter de juillet 2013) : consultez la page internet du <a href="https://www.civiweb.com/FR/mon-espace-perso/mon-statut/bareme-indemnites.aspx" class="spip_out" rel="external">Centre d’information sur le volontariat international</a>.</p>
<p>Enfin, le 30 mai 2012, l’ambassade de France au Mexique a <a href="http://www.ambafrance-mx.org/Lancement-du-Club-VIE-Mexique" class="spip_out" rel="external">inauguré le Club VIE</a>, réseau rassemblant des employeurs ainsi que des nouveaux et anciens volontaires internationaux (VI).</p>
<p>Pour trouver un V.I. nous vous conseillons :</p>
<ul class="spip">
<li>de consulter le site du <a href="https://www.civiweb.com/FR/mon-espace-perso/mon-statut/bareme-indemnites.aspx" class="spip_out" rel="external">Centre d’information sur le volontariat international</a> ;</li>
<li>d’envoyer des candidatures spontanées aux grands groupes français implantés au Mexique ;</li>
<li>de démarcher en France les PME qui vous intéressent et qui pourraient avoir un projet d’export au Mexique ;</li>
<li>de consulter les CCI de vos régions qui pourront peut-être vous renseigner sur ces entreprises intéressées par un projet d’implantation au Mexique.</li></ul>
<p>Enfin, sachez que la <a href="http://www.franciamexico.com/" class="spip_out" rel="external">Chambre franco-mexicaine de commerce et d’industrie</a> offre des structures d’accueil au sein de ses bureaux pour les V.I. qui arrivent au Mexique.</p>
<p>Pour l’obtention du visa, voir la fiche <a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-mexique-entree-et-sejour-article-passeport-visa-permis-de-travail-108383.md" class="spip_in">passeport, visa, permis de travail</a>.</p>
<p>Pour plus de renseignements sur les V.I. :</p>
<ul class="spip">
<li><a href="https://www.civiweb.com/FR/index.aspx" class="spip_out" rel="external">Civiweb.com</a></li>
<li>Notre rubrique <a href="services-aux-citoyens-preparer-son-expatriation-emploi.md" class="spip_in">Emploi</a></li>
<li><a href="http://export.businessfrance.fr/default.html" class="spip_out" rel="external">Businessfrance.fr</a> ou email : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/emploi-stage/article/stages#infoVIE#mc#ubifrance.fr#" title="infoVIE..åt..ubifrance.fr" onclick="location.href=mc_lancerlien('infoVIE','ubifrance.fr'); return false;" class="spip_mail">infoVIE<span class="spancrypt"> [at] </span>ubifrance.fr</a>.</li></ul>
<p><i>Mise à jour : septembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/emploi-stage/article/stages). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
