# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/emploi-stage/article/reglementation-du-travail#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/emploi-stage/article/reglementation-du-travail#sommaire_2">Fêtes légales</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/emploi-stage/article/reglementation-du-travail#sommaire_3">Emploi du conjoint</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/emploi-stage/article/reglementation-du-travail#sommaire_4">Création d’entreprises – spécificités</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<p>Plusieurs lois encadrent de façon très restrictive l’accès au travail à Maurice.</p>
<p>Ainsi, tout citoyen étranger dont le projet d’activité n’entre pas dans les objectifs de développement du pays ou dont les compétences professionnelles existent sur le marché mauricien, sera peu susceptible d’obtenir les autorisations nécessaires.</p>
<p>La loi de 1970 (<i>Employment (Non-Citizens) (Restriction) Exemptions</i>) fixe la liste des exemptions accordées aux non-mauriciens en matière de permis de travail.</p>
<p>La loi de 1973 quant à elle (<i>Non-Citizens Employment Restriction act</i>) réglemente les conditions de l’emploi pour un étranger. Elle impose un permis de travail pour les étrangers, précise le caractère discrétionnaire de l’octroi du permis de travail, par ailleurs éventuellement révocable. Enfin il est à signaler que le permis est octroyé pour un emploi bien défini (ainsi en cas de changement d’employeur, celui-ci n’est plus valable).</p>
<p>La candidature doit être soumise à l’aide du formulaire disponible auprès de l’<i>Employment Division</i>.</p>
<p>La durée du travail est de 40h par semaine, 8 heures par jour. Le nombre de jours de repos hebdomadaires est de 2, celui des jours fériés payés est de 15.</p>
<p>20 jours de congés annuels sont accordés ainsi que deux jours supplémentaires.</p>
<h3 class="spip"><a id="sommaire_2"></a>Fêtes légales</h3>
<ul class="spip">
<li>Nouvel An et lendemain de Nouvel An</li>
<li>17 janvier : Thaipoosam Cavadee <sup>1</sup></li>
<li>Janvier ou février : Fête du Printemps, nouvel an chinois<sup>2</sup></li>
<li>1er février : abolition de l’esclavage</li>
<li>27 Février : Maha Shivaratree<sup>1</sup></li>
<li>12 mars : Fête de l’Indépendance et proclamation de la République</li>
<li>31 mars : Ougadi</li>
<li>1er mai : Fête du Travail</li>
<li>29 juillet : Eid (en fonction de la lune)<sup>3</sup></li>
<li>15 août : Assomption</li>
<li>30 août : Ganesh Chaturthi<sup>1</sup></li>
<li>23 octobre : Divali<sup>1</sup></li>
<li>2 novembre : Arrivée des travailleurs engagés</li>
<li>25 décembre : Noël</li></ul>
<p>(1) fête indienne, (2) fête chinoise, (3) fête musulmane (dont la date varie en fonction du calendrier lunaire).</p>
<h3 class="spip"><a id="sommaire_3"></a>Emploi du conjoint</h3>
<p>Le conjoint d’un détenteur d’un <i>Occupation Permit </i>peut travailler à l’ile Maurice. Dans ce cas, une demande de permis de travail doit être effectuée.</p>
<p>Il est difficile d’obtenir un emploi sur place si un contrat n’a pas été négocié depuis la France.</p>
<h3 class="spip"><a id="sommaire_4"></a>Création d’entreprises – spécificités</h3>
<p>La constitution des sociétés et l’enregistrement des activités commerciales relèvent des dispositions du <i>Companies Act</i> de 2001 et du <i>Business Registration Act</i> de 2002.</p>
<p>La <i>Companies Act</i> de 2001 reconnaît les sociétés anonymes, les sociétés à responsabilité limitée par garantie et sans capital d’actions, les sociétés à responsabilité limitée avec un capital d’actions et les compagnies à durée de vie limitée. Les entreprises peuvent être unipersonnelles ou pluripersonnelles.</p>
<p>Toute entreprise doit se faire immatriculer auprès du greffier du Registre des Sociétés (Registrar of Companies) et obtenir sa carte d’enregistrement (Business Registration Card) afin de démarrer ses activités.</p>
<p>Une société constituée à Maurice peut être à 100% étrangère sans capital minimum.</p>
<p>Le gouvernement mauricien impose toutefois que tout investisseur étranger, s’il veut obtenir le permis de résidence et de travail à Maurice, génère annuellement par son entreprise 4 millions de roupies mauriciennes et investisse un montant initial minimum de 100 000 dollars américains.</p>
<p>Pour en savoir plus :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.investmauritius.com/" class="spip_out" rel="external">Investmauritius.com</a></p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/emploi-stage/article/reglementation-du-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
