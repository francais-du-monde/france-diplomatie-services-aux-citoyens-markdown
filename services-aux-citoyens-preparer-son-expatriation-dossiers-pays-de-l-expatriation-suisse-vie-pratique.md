# Vie pratique

<h2 class="rub22406">Coût de la vie</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/vie-pratique/#sommaire_1">Opérations bancaires</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/vie-pratique/#sommaire_2">Budget</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/vie-pratique/#sommaire_3">Evolution des prix</a></li></ul>
<p><strong>Monnaie et change</strong></p>
<p>La monnaie de la Confédération helvétique est le franc suisse (CHF ou SFR), divisé en centimes. Il existe des pièces de 5, 10, 20 et 50 centimes, des pièces de 1, 2 et 5 francs suisses, ainsi que des billets de 10, 20, 50, 100, 500 et 1000 francs suisses.</p>
<p>Pour en savoir plus sur la monnaie suisse, vous pouvez consulter le site Internet de la <a href="http://www.snb.ch/" class="spip_out" rel="external">Banque nationale suisse</a> : rubrique Billets et monnaies.</p>
<p>Depuis le 6 septembre 2011, la BNS (Banque Nationale Suisse) a institué un cours plancher de 1,20 CHF pour un euro.</p>
<p><a href="http://www.oanda.com/convert/classic" class="spip_out" rel="external">Convertisseur de devises</a></p>
<p>Les cartes de crédit internationales sont acceptées dans la majorité des magasins et pour les retraits auprès de la plupart des banques. Les espèces sont beaucoup plus couramment utilisées qu’en France. Les chèques sont extrêmement rares, les commerçants ne les acceptent pas. La pratique est de régler les factures par virement postal ou bancaire. Bien que la Suisse soit un pays limitrophe de la France, les achats effectués en Suisse se règlent en francs suisses et non en euros.</p>
<p>A noter que l’importation de devises ayant cours légal en Suisse n’est soumise à aucune restriction. Vous devrez cependant respecter la réglementation communautaire en matière de contrôle des changes lorsque vous quittez la France ou un pays membre de l’Union européenne. Toute importation de sommes d’argent en Suisse doit être déclarée au moment du passage de la douane.</p>
<h3 class="spip"><a id="sommaire_1"></a>Opérations bancaires</h3>
<p><strong>La banque au quotidien</strong></p>
<p>Pour les opérations bancaires, les banques suisses proposent de nombreux services. En général, la tenue d’un compte courant est payante, mais, en contrepartie, ce compte est rémunéré. Il inclut une carte de débit immédiat (l’argent est prélevé directement sur votre compte lorsque vous faites des retraits aux bancomats / postomats ou chez les commerçants).</p>
<p>Les cartes de crédit sont facturées en plus et peuvent être prises dans un autre établissement que celui où vous possédez un compte. Le principe du crédit n’est pas vraiment dans les habitudes des consommateurs suisses.</p>
<p>Les travailleurs frontaliers ont des besoins spécifiques. Ils devront avoir un compte dans une banque suisse sur lequel sera versé le salaire et dans une banque française car les dépenses des travailleurs frontaliers se font principalement en euros. Certaines banques proposent des services qui permettent à l’employeur suisse de verser directement le salaire sur un compte français libellé en francs suisses. Il n’y a pas de solution idéale, chacune ayant ses avantages et ses inconvénients.</p>
<p>La monnaie nationale est librement convertible et les transferts de fonds sont libres. Il y a 140 banques étrangères installées en Suisse, dont les principales banques françaises : BNP Paribas, Crédit Agricole, Crédit Lyonnais, Société Générale…</p>
<p><strong>Transferts de fonds entre la Suisse et la France</strong></p>
<p>Pour transférer des fonds entre la Suisse et la France, il existe plusieurs possibilités :</p>
<ul class="spip">
<li>virement international (SWIFT / IBAN) : ce service est proposé par toutes les banques et peut concerner des virements réguliers ou occasionnels ;</li>
<li>LSV : ce service est proposé par certaines banques et concerne des virements réguliers ;</li>
<li>Transfert physique : pour des raisons de sécurité, il est fortement déconseillé de passer de l’argent en espèces à la frontière. Vous devrez par ailleurs respecter la réglementation communautaire en matière de contrôle des changes.</li></ul>
<p>Conformément à l’ordonnance suisse sur le contrôle du trafic transfrontière de l’argent liquide, l’entrée de devises suisses ou étrangères sur le territoire est strictement réglementée. Une des missions de l’administration fédérale des douanes (AFD) est la lutte contre le blanchiment d’argent et le financement du terrorisme.</p>
<p>Pour en savoir plus, consulter le site internet de l’<a href="http://www.douane.admin.ch/" class="spip_out" rel="external">AFD</a>.</p>
<p>Dans le cadre d’un contrôle douanier, les douanes suisses peuvent procéder à un interrogatoire en vue d’obtenir des renseignements sur le détenteur des espèces, de connaître l’origine et le montant de l’argent dès lors où le montant transporté est supérieur ou égal à 10 000 CHF ou d’un montant équivalent en monnaie étrangère voire d’une somme inférieure en cas de soupçon. Sont considérés comme argent liquide, les espèces ainsi que tout titre au porteur transmissible tels que les actions, les obligations, les chèques et autres titres similaires, pouvant faire l’objet d’une retenue temporaire. Il est donc vivement recommandé de déclarer les espèces.</p>
<p><strong>Secret bancaire</strong></p>
<p>Il fait l’objet depuis plusieurs années de débats intenses, en Suisse comme à l’étranger, en relation avec la gestion, par les établissements bancaires suisses d’avoirs de non-résidents non déclarés au fisc des pays d’origine.</p>
<p><strong>Pour en savoir plus</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span> <a href="http://www.swissbanking.org/" class="spip_out" rel="external">Site Internet de l’Association suisse des banquiers</a> : rubrique Information à l’attention de la clientèle bancaire</p>
<h3 class="spip"><a id="sommaire_2"></a>Budget</h3>
<p>Le candidat à l’expatriation devra se garder de chercher à apprécier sa situation en convertissant tous les chiffres en euros ou en tentant de déterminer le pouvoir d’achat en France du montant obtenu. Cette appréciation doit se faire avant tout sur le montant des dépenses dans le pays d’expatriation, qu’elles soient exprimées en monnaie locale ou en euros. Il est en effet indispensable de tenir compte du coût de la vie.</p>
<p>La Suisse est un des pays de l’OCDE qui possède le coût de la vie le plus élevé. Il était, par exemple, 22% plus élevé en Suisse qu’en France en 2007. Avec le temps, cette différence s’amenuise (l’écart était en 2001 d’environ 30%).</p>
<p>Selon une étude de l’UBS de 2012, Zurich est classée parmi les villes les plus chères du monde avec Oslo et Tokyo.</p>
<p>Si on attribue à Zurich un indice 100 en terme de niveau de prix (coût du loyer inclus), Genève se situe à l’indice 94,4, Paris à l’indice 75,6 et Lyon à l’indice 67,1.</p>
<p>Une étude de l’Office fédéral de la statistique a fixé en pourcentage du revenu brut des ménages leurs principales dépenses :</p>
<ul class="spip">
<li>loyer : 26,1 %</li>
<li>santé : 14,6 %</li>
<li>transport : 10,8 %</li>
<li>alimentation : 10,3 %</li>
<li>loisirs et culture : 9,5 %</li>
<li>restaurant et l’hôtel : 8,8 %</li>
<li>équipement ménager : 4,7 %</li>
<li>habillement : 4 %</li>
<li>communications : 3 %</li>
<li>autres : 8,2 %</li></ul>
<p>A noter que ces chiffres sont des moyennes nationales et qu’il existe des disparités importantes entre les cantons et entre les villes qui bordent le Lac Léman et celles de l’intérieur du pays.</p>
<p><strong>Pour en savoir plus</strong></p>
<p><a href="http://www.bfs.admin.ch/" class="spip_out" rel="external">Office fédéral de la statistique</a> : rubrique " Prix "</p>
<h3 class="spip"><a id="sommaire_3"></a>Evolution des prix</h3>
<p><strong>Evolution générale des prix depuis 2000 </strong></p>
<p>Entre 2000 et 2008, l’indice suisse des prix à la consommation (IPC) a augmenté en moyenne de 8,8%.</p>
<p>L’augmentation des prix est restée très faible au cours des dernières années. La Suisse a connu en 2012 une période que quasi déflation.</p>
<p>Pour en savoir plus, se reporter au site de l’<a href="http://www.bfs.admin.ch/" class="spip_out" rel="external">Office fédéral des statistiques</a>.</p>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : février 2013</i></p>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/vie-pratique/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
