# Stages

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/emploi-stage/article/stages#sommaire_1">Trouver un stage au Danemark depuis la France</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/emploi-stage/article/stages#sommaire_2">La protection sociale des stagiaires au Danemark</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Trouver un stage au Danemark depuis la France</h3>
<p>Contacter les organismes suivants :</p>
<p><a href="http://www.aiesec.org/" class="spip_out" rel="external">Association internationale des étudiants en sciences économiques et commerciales (AIESEC)</a><br class="manualbr">14, rue de Rouen <br class="manualbr">75019 Paris<br class="manualbr">Tél. : 01 40 36 22 33</p>
<p><strong>Association française pour les stages techniques à l´étranger (AFSTE)</strong><br class="manualbr">Campus Jallard- 81 Route de Teillet<br class="manualbr">81013 ALBI cedex 09<br class="manualbr">Tél. : 05 63 49 31 09</p>
<p><a href="http://www.agriplanete.com/" class="spip_out" rel="external">Service des Echanges et des Stages Agricoles dans le Monde (SESAME)</a><br class="manualbr">6, rue de la Rochefoucault <br class="manualbr">75009 Paris<br class="manualbr">Tél. : 01 40 54 07 08</p>
<p><a href="http://ec.europa.eu/education/leonardo-da-vinci/participate_fr.htm" class="spip_out" rel="external">Stages européens Leonardo da Vinci</a></p>
<p><a href="http://www.cidj.com/" class="spip_out" rel="external">Centre d’information et de documentation jeunesse (CIDJ)</a><br class="manualbr">101 Quai Branly - 75740 Paris cedex 15<br class="manualbr">Tél. : 01 44 49 12 00 <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/emploi-stage/article/stages#cidj#mc#cidj.com#" title="cidj..åt..cidj.com" onclick="location.href=mc_lancerlien('cidj','cidj.com'); return false;" class="spip_mail">Courriel</a></p>
<p>Il existe également d’autres CIDJ dans tous les départements. Leurs coordonnées sont disponibles <a href="http://www.cidj.com/" class="spip_out" rel="external">en ligne</a>.</p>
<p><a href="http://www.apec.fr/" class="spip_out" rel="external">Association Pour l’Emploi des Cadres (APEC)</a> (les services sont gratuits et ouverts aux jeunes diplômés à la recherche d’un premier emploi).</p>
<h4 class="spip">Stage auprès du service économique de l’Ambassade de France au Danemark ou des services d´Ubifrance</h4>
<p>Ces services accueillent deux ou trois stagiaires par an. Les éléments déterminants pour le choix des stagiaires est la connaissance du danois (ou d’une langue scandinave) et de l’anglais. Les profils recherchés sont surtout des candidats ayant suivi un cursus économique, financier, juridique (droit des affaires) ou commercial, de niveau Bac+3 au minimum.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.tresor.economie.gouv.fr/se/danemark" class="spip_out" rel="external">Service Economique de l’Ambassade de France au Danemark</a><br class="manualbr">Kongens Nytorv 4<br class="manualbr">1050 Copenhague K<br class="manualbr">Tél. : 33 67 01 00</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://export.businessfrance.fr/danemark/export-danemark-avec-notre-bureau.html" class="spip_out" rel="external">Business France</a><br class="manualbr">Knabrostræde 3,2 TV. <br class="manualbr">1210 Copenhague K<br class="manualbr">Tél. : 33 37 61 74</p>
<h4 class="spip">Stages au Danemark auprès d’entreprises ou d’administrations danoises</h4>
<p>La recherche d’un stage au Danemark est un exercice difficile. La majorité des entreprises ou administrations danoises exige des candidats la connaissance partielle du danois ou d’une langue scandinave (la connaissance d’une langue scandinave permet de se faire comprendre dans tous les pays scandinaves) et la maîtrise parfaite de l’anglais.</p>
<p>Pour faciliter votre recherche de stage, vous trouverez ci-dessous une liste de différents sites internet d’entreprises ou d’administrations danoises.</p>
<ul class="spip">
<li><a href="http://www.denmark.dk/" class="spip_out" rel="external">Site en anglais et français</a> créé par le ministère des Affaires étrangères danois fournissant des informations sur le Danemark en général et son organisation administrative.</li>
<li>Site de l’agence d’investissement pour les régions de Copenhague et de l’Oresund. <a href="http://www.copcap.com/" class="spip_out" rel="external">Copenhagen Capacity</a> regroupant, entre autres, des informations sur l’actualité politique et économique et le marché de l’emploi. Site en anglais.</li>
<li>Le site de <a href="http://www.proff.dk/" class="spip_out" rel="external">Proff The Business Finder</a> vous permet d´effectuer votre recherche d´entreprise par produit ou raison sociale.</li>
<li>Site en anglais créé par le ministère des Affaires étrangères danois et destiné à <a href="http://www.investindk.com/" class="spip_out" rel="external">promouvoir et faciliter l’implantation au Danemark d’entreprises étrangères</a>.</li>
<li><a href="http://www.europages.com/" class="spip_out" rel="external">Europages est l’annuaire européen</a> des affaires permettant de trouver des entreprises par le biais d’une recherche par produit ou par raison sociale (site en français).</li>
<li><a href="http://www.kompass.com/" class="spip_out" rel="external">Kompass - annuaire mondial des sociétés</a>, la recherche s’effectue par produit et par raison sociale. Site en plusieurs langues dont le français.</li></ul>
<h4 class="spip">Stages au Danemark auprès d’entreprises françaises</h4>
<p>Si vous souhaitez effectuer un stage auprès de filiales d’entreprises françaises au Danemark, votre demande de stage doit de préférence être présentée directement aux maisons-mères. Veuillez noter que la maîtrise de l’anglais et la connaissance du danois (ou, au cas échéant, d’une langue scandinave) sera certainement exigée.</p>
<p>Pour faciliter votre recherche de stage, vous trouverez ci-dessous une liste non exhaustive d’entreprises françaises implantées au Danemark. Il faut consulter leur site internet pour obtenir des informations quant aux possibilités de stages.</p>
<p>Le service économique de l’Ambassade de France au Danemark dispose également d’une liste d’entreprises françaises ayant une implantation au Danemark.</p>
<p>Veuillez trouver ci-dessous la liste (non exhaustive) des sociétés françaises implantées au Danemark :</p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td>Air France</td>
<td>Alcatel</td>
<td>Alcatel Alsthom</td>
<td>Alcatel Space Industries S.A</td>
<td>Alsthom S.A</td></tr>
<tr class="row_even even">
<td>Ascom Monetel</td>
<td>Atoglas S.A</td>
<td>Automobiles Citroën</td>
<td>Bacou S.A</td>
<td>Bite AB</td></tr>
<tr class="row_odd odd">
<td>Bonduelle</td>
<td>Bostik AB</td>
<td>Brandt S.A</td>
<td>Bull</td>
<td>Bureau Veritas S.A</td></tr>
<tr class="row_even even">
<td>Canal +</td>
<td>Cap Gemini</td>
<td>Cartier International B.V</td>
<td>Citroën</td>
<td>Coates Lorilleux</td></tr>
<tr class="row_odd odd">
<td>Colas</td>
<td>Compagnie Financière Michelin</td>
<td>Crédit Agricole Indosuez</td>
<td>Décathlon</td>
<td>Dégremont S.A</td></tr>
<tr class="row_even even">
<td>DIM S.A</td>
<td>Dollfus-Mieg  Co</td>
<td>Elf Atochem SA</td>
<td>Elf Lubrifiant</td>
<td>Engelhard-Clal</td></tr>
<tr class="row_odd odd">
<td>Essilor International</td>
<td>Euro RSCG International SA</td>
<td>France Telecom</td>
<td>G.T.I.E (Compagnie Générale de travaux et d’Ingéniérie Electricité)</td>
<td>GEMPLUS</td></tr>
<tr class="row_even even">
<td>GEMS</td>
<td>Global One Communications Holding BV</td>
<td>Groupe Danone</td>
<td>Groupe Moulinex</td>
<td>Groupe Primagaz</td></tr>
<tr class="row_odd odd">
<td>Groupe Schneider SA</td>
<td>Hypred SA</td>
<td>Image SA</td>
<td>Jager Jeune SA</td>
<td>Jet World-Wide SA/Chronopost International</td></tr>
<tr class="row_even even">
<td>Kremlin SA</td>
<td>La Parmentière</td>
<td>Lacie Group SA</td>
<td>Lafarge SA</td>
<td>Le carbone-Lorraine</td></tr>
<tr class="row_odd odd">
<td>Lectra Systemes S.A</td>
<td>Legrand SNG</td>
<td>L’Oréal</td>
<td>Louis Vuitton Malletier</td>
<td>Lyreco</td></tr>
<tr class="row_even even">
<td>Matra Nortel Communication</td>
<td>Mazars  Guerard</td>
<td>Merial</td>
<td>OTV</td>
<td>Parfums Christian Dior</td></tr>
<tr class="row_odd odd">
<td>Pasteur Mérieux MSD</td>
<td>Pebeo</td>
<td>Pechiney World Trade</td>
<td>Pernod Ricard</td>
<td>Promosalons</td></tr>
<tr class="row_even even">
<td>Publicis</td>
<td>R.Bourgeois SA</td>
<td>Remy Associés</td>
<td>Réseau Tradimar</td>
<td>Rhodias SA</td></tr>
<tr class="row_odd odd">
<td>Rhone-Poulenc Rorer</td>
<td>Royal Canin SA</td>
<td>S.A Louis Dreyfus  Cie</td>
<td>S.C.R.A.S</td>
<td>SAF</td></tr>
<tr class="row_even even">
<td>Saint-Gobain</td>
<td>SANOFI</td>
<td>Scanglas A/S</td>
<td>Schneider Electric SA</td>
<td>SEB SA</td></tr>
<tr class="row_odd odd">
<td>Sollac Usinor</td>
<td>Ste Chardin Val d’Or</td>
<td>Synthelabo Groupe</td>
<td>Total SA (filiale Hutchinson)</td>
<td>Trigano</td></tr>
<tr class="row_even even">
<td>Ubi Soft SA</td>
<td>Vent du Nord</td>
<td>Verrerie cristallerie d’arques J.G Durand  Cie</td>
<td></td>
<td></td></tr>
</tbody>
</table>
<h4 class="spip">Le volontariat international en entreprise ou dans l´administration (V.I.E ou V.I.A.)</h4>
<p>Le V.I.E. permet aux entreprises françaises de confier à un jeune ressortissant de l’espace économique européen, âgé de 18 à 28 ans, une mission professionnelle (rémunérée) à l’étranger durant une période modulable de 6 à 24 mois.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/emplois-stages-concours/volontaires-internationaux/" class="spip_in">Volontaires internationaux</a></li>
<li>Auprès <a href="http://export.businessfrance.fr/default.html" class="spip_out" rel="external">des services de Business France</a></li></ul>
<h3 class="spip"><a id="sommaire_2"></a>La protection sociale des stagiaires au Danemark</h3>
<p><i>Source : CLEISS</i></p>
<h4 class="spip">Les obligations en matière d’assujettissement</h4>
<h5 class="spip">Les assurances nationales</h5>
<p>Il existe au Danemark un régime unique de sécurité sociale qui couvre, en principe tous les résidents de la même façon, quel que soit leur revenu professionnel ou leur statut.</p>
<p>Les stagiaires ne bénéficiant pas d’un régime de protection sociale spécifique, tous les stagiaires résidant au Danemark relèvent donc du régime universel de protection sociale.</p>
<p>Les assurances sociales nationales au titre de la résidence offrent les prestations des assurances maladie, maternité, vieillesse, invalidité et prestations familiales.</p>
<p>Est considérée comme "résidente" au Danemark (au sens des lois relatives à la sécurité sociale), toute personne résidant de façon licite dans ce pays.</p>
<p>Cependant, sont considérées comme telles, les personnes partant s’installer et travailler au Danemark (activité salariée ou non salariée) et les personnes qui résident dans un autre État de l’Espace économique européen mais qui exercent leur activité professionnelle au Danemark.</p>
<h5 class="spip">Les assurances complémentaires liées à l’activité</h5>
<p>Parallèlement au système national lié à la résidence, il existe un régime d’assurance sociale complémentaire qui vise uniquement la personne qui exerce une activité.</p>
<p>Ce régime s’applique également aux stagiaires et selon qu’ils sont salariés ou non salariés, ils ne sont pas soumis aux mêmes dispositions quant à l’obligation d’affiliation aux assurances complémentaires.</p>
<p>Les régimes sociaux qui sont liés à l’exercice d’une activité sont ceux des indemnités journalières de maladie et de maternité, de l’assurance chômage, de la pension complémentaire "ATP", et de l’assurance contre les accidents du travail.</p>
<p>Par ailleurs, l’article 1er, Partie 1, de la loi portant sur les accidents du travail prévoit que toute personne qui est engagée dans une activité rémunérée ou comme "assistante" non rémunérée pour le compte d’un employeur afin de mener à bien un travail permanent, temporaire ou occasionnel au Danemark, est assurée contre les risques générés par les accidents du travail, à condition qu’une relation de travail ou un lien professionnel existe entre le travailleur et l’employeur.</p>
<p>C’est la Commission nationale des accidents du travail qui statue, suivant une liste de critères particuliers, en matière de détermination de l’existence éventuelle d’un lien de subordination.</p>
<p>Lorsque le lien de subordination est établi, l’employeur est tenu de se conformer aux dispositions légales en matière d’assujettissement.</p>
<p>L’employeur satisfait à cette obligation en contractant :</p>
<ul class="spip">
<li>une assurance contre les accidents du travail auprès d’une compagnie d’assurances,</li>
<li>une assurance contre les maladies professionnelles auprès du Fonds des maladies professionnelles.</li></ul>
<h4 class="spip">Les cotisations</h4>
<p>Les assurances maladie, maternité, invalidité-vieillesse (pension sociale) et prestations familiales sont financées par l’impôt au Danemark.</p>
<p>La pension complémentaire liée au salaire (pension ATP) est financée au moyen de cotisations.</p>
<p>Cependant, il existe pour le versement de telles cotisations une condition de durée de temps de travail : la durée minimum mensuelle du travail doit être au moins égale à 39 heures par mois.</p>
<p>Montant : la cotisation ATP s’élève à 270 DKK (36,45 €) par mois (un tiers à la charge du salarié et deux tiers à la charge de l’employeur).</p>
<p>Cette cotisation n’est due que pour les stages rémunérés.</p>
<p><strong>Important</strong> : l’assurance chômage n’est, au Danemark, pas obligatoire mais volontaire. Tous les travailleurs âgés de seize à soixante-sept ans peuvent, sur la base du volontariat, s’affilier à une caisse de chômage. La cotisation est à la seule charge du travailleur. Son montant varie d’une caisse d’assurance chômage à l’autre. Comptez environ 60 Euros/mois.</p>
<p>Voici la liste des principales caisses :</p>
<ul class="spip">
<li><a href="http://www.min-a-kasse.dk/" class="spip_out" rel="external">Min A-kasse</a></li>
<li><a href="http://www.detfagligehus.dk/" class="spip_out" rel="external">Det faglige hus</a></li>
<li><a href="http://www.krifa.dk/a-kasse.aspx" class="spip_out" rel="external">Knifa</a></li>
<li><a href="http://www.ma-kasse.dk/" class="spip_out" rel="external">MA-kasse</a></li>
<li><a href="http://forsiden.3f.dk/" class="spip_out" rel="external">3F A-kasse</a></li>
<li><a href="http://www.lederne.dk/" class="spip_out" rel="external">Lederne A-kasse</a></li>
<li><a href="http://www.ca.dk/" class="spip_out" rel="external">CA a-kasse</a><i></i></li>
<li><a href="http://www.ase.dk/" class="spip_out" rel="external">ASE</a></li>
<li><a href="http://www.hk.dk/" class="spip_out" rel="external">HK DANMARK A-kasse</a></li>
<li><a href="http://www.dlfa.dk/" class="spip_out" rel="external">Lærernes a-kasse</a></li></ul>
<p>S’agissant des accidents du travail, l’employeur, et lui seul, cotise auprès de caisses d’assurances privées agréées, pour l’ensemble de ses employés, qu’ils soient rémunérés ou non. Les taux de cotisations varient en fonction des risques liés à l’activité de l’entreprise.</p>
<p>Enfin, depuis le 1er janvier 1994, les travailleurs salariés doivent verser une contribution au marché du travail qui s’élève à 8 % de la totalité du salaire.</p>
<p>Les stagiaires non rémunérés ne sont redevables ni de cette contribution au marché du travail, ni de la cotisation ATP mentionnée ci-dessus.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/emploi-stage/article/stages). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
