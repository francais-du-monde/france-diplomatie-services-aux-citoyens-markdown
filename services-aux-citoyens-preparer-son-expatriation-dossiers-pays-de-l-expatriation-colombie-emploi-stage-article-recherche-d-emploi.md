# Recherche d’emploi

<p class="chapo">
      La maîtrise de l’espagnol est indispensable pour trouver un emploi en Colombie.
</p>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/emploi-stage/article/recherche-d-emploi#sommaire_1">Journaux</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/emploi-stage/article/recherche-d-emploi#sommaire_2">Sites Internet</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/emploi-stage/article/recherche-d-emploi#sommaire_3">Organismes</a></li></ul>
<p>Il n’existe pas d’équivalent de pôle emploi en Colombie.</p>
<h3 class="spip"><a id="sommaire_1"></a>Journaux</h3>
<p>Vous pouvez trouver des annonces d’emploi dans la presse quotidienne, cependant il est préférable de s’en tenir à son réseau de connaissance pour des raisons de sécurité.</p>
<p>Les sites des journaux colombiens publient des annonces d’emploi sur leur site en les classant selon le domaine de métier. Le quotidien <a href="http://clasificados.eltiempo.com" class="spip_out" rel="external">El tiempo</a> propose notamment une rubrique « clasificados » avec des offres d’emploi.</p>
<h3 class="spip"><a id="sommaire_2"></a>Sites Internet</h3>
<ul class="spip">
<li><a href="http://www.expat.com/fr/emploi/amerique-du-sud/colombie" class="spip_url spip_out" rel="external">http://www.expat.com/fr/emploi/amer...</a></li>
<li><a href="http://co.3wjobs.com" class="spip_url spip_out" rel="external">http://co.3wjobs.com</a></li>
<li><a href="http://www.learn4good.com/jobs/language/english/list/country/colombia" class="spip_url spip_out" rel="external">http://www.learn4good.com/jobs/lang...</a></li>
<li><a href="http://www.intel.com/jobs/colombia" class="spip_url spip_out" rel="external">http://www.intel.com/jobs/colombia</a></li>
<li><a href="http://www.elempleo.com" class="spip_url spip_out" rel="external">http://www.elempleo.com</a></li>
<li><a href="http://www.computrabajo.com" class="spip_url spip_out" rel="external">http://www.computrabajo.com</a></li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Organismes</h3>
<ul class="spip">
<li><a href="http://www.france-colombie.com" class="spip_out" rel="external">Chambre de commerce et d’industrie franco-colombienne</a></li>
<li><a href="http://www.pole-emploi-international.fr" class="spip_out" rel="external">Pôle emploi international</a></li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/emploi-stage/article/recherche-d-emploi). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
