# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/vie-pratique/article/cout-de-la-vie#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/vie-pratique/article/cout-de-la-vie#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/vie-pratique/article/cout-de-la-vie#sommaire_3">Budget</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/vie-pratique/article/cout-de-la-vie#sommaire_4">Alimentation</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/vie-pratique/article/cout-de-la-vie#sommaire_5">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>L’unité monétaire est le nouveau shekel (abréviation : NIS, son code ISO est ILS). Un shekel comprend 100 agoroths. Il existe des billets de 200, 100, 50 et 20 shekels, des pièces de monnaie de 10, 5, 2 et 1 shekels et de 50 et 10 agoroths. Un euro vaut environ 4,8 shekel (donnée juin 2013).</p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<p>Les cartes de crédit Visa, Mastercard et American Express sont acceptées presque partout sauf sur les marchés. Il est aussi possible d’effectuer des retraits par carte bancaire dans les distributeurs, généralement avec de fortes commissions, mais cela dépend de la carte. Aucune banque française ne fait d’opération de guichet. Les plus grandes banques israéliennes sont LEUMI, HAPOALIM et ISRAEL DISCOUNT BANK. Les banques acceptent les euros, les chèques et les chèques de voyage.</p>
<h3 class="spip"><a id="sommaire_3"></a>Budget</h3>
<p>La vie en Israël est assez chère. Le candidat à l’expatriation devra se garder de chercher à apprécier sa situation en convertissant tous les chiffres en euros, ou en tentant de déterminer le pouvoir d’achat en France du montant obtenu. Cette appréciation doit se faire avant tout sur le montant des dépenses dans le pays d’expatriation, qu’elles soient exprimées en monnaie locale ou en euros, en regard du salaire qui est susceptible d’y être perçu.</p>
<h3 class="spip"><a id="sommaire_4"></a>Alimentation</h3>
<p>Depuis quelques années les centres commerciaux se multiplient, aussi bien au centre des villes que dans les périphéries. Tout comme eu Europe, les commerces et les services les plus variés se retrouvent dans ces lieux. Les magasins ferment généralement entre 18h30 et 19h30 en semaine.</p>
<p>La plus grande chaîne de supermarchés en Israël est Supersol, suivie par Blue Square, ce dernier distribuant-via un accord de franchise les produits Leader Price. D’autres acteurs sont apparus sur la scène de la grande distribution alimentaire : Tiv Taam, ainsi que des <i>discounters</i> (Hatzi Hinam, Hyper Ramah, Abba Victory…).</p>
<h4 class="spip">Prix moyen d’un repas dans un restaurant</h4>
<p>En moyenne un repas complet coûtera autour de 20 € dans un restaurant moyen, compter le double pour un restaurant chic. Les menus sont peu répandus, les commandes se font en général à la carte. Un pourboire, de l’ordre de 12%, est d’usage car le service n’est pas compris dans les prix.</p>
<p>Les stands de nourriture à emporter sont très nombreux et permettent de manger pour environ 3 à 5 €.</p>
<h4 class="spip">Exemples de prix de quelques biens de consommation</h4>
<p><strong>Voir les supermarchés en ligne ci-dessous :</strong></p>
<ul class="spip">
<li><a href="http://mymakolet.com/" class="spip_out" rel="external">Mymakolet.com</a></li>
<li><a href="http://www.mega.co.il/" class="spip_out" rel="external">Mega.co.il</a></li></ul>
<h3 class="spip"><a id="sommaire_5"></a>Evolution des prix</h3>
<p>L’inflation a été maintenue à un niveau inférieur à 4 % depuis 2005 et, fait remarquable, inférieur à 3 % depuis 2010 (le taux d’inflation est estimé à 1,6 % pour l’année 2013) alors même que le pays a retrouvé les chemins d’une croissance forte (plus de 3 %, après avoir atteint les 5 %)</p>
<p>L’indice des prix à la consommation est légèrement inférieur à la moyenne des pays de l’OCDE : 100,5 en 2013 (base 100). A titre de comparaison, la France avait un indice des prix de 127 la même année. Les prix moyens des biens de consommation sont donc sensiblement inférieurs aux prix pratiqués en France, à l’exception notoire des produits alimentaires, laitiers en particulier, qui sont beaucoup plus chers qu’en France.</p>
<p><i>Pour en savoir plus : </i><a href="http://export.businessfrance.fr/israel/export-israel-avec-nos-bureaux.html" class="spip_out" rel="external"><i>dossier Israël sur le site de Business France</i></a></p>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/vie-pratique/article/cout-de-la-vie). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
