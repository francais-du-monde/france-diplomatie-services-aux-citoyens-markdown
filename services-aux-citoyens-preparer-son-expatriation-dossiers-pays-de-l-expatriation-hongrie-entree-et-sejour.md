# Entrée et séjour

<h2 class="rub23459">Passeport, visa, permis de travail</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/entree-et-sejour/#sommaire_1">Séjour de moins de trois mois</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/entree-et-sejour/#sommaire_2">Séjour de plus de trois mois</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/entree-et-sejour/#sommaire_3">Permis de travail</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/entree-et-sejour/#sommaire_4">Résidence permanente</a></li></ul>
<p>Quel que soit le motif de votre séjour, renseignez-vous auprès de la section consulaire de l’ambassade de Hongrie à Paris qui vous informera sur la règlementation en matière d’entrée et de séjour en Hongrie, règlementation que vous devrez impérativement respecter. N’hésitez pas à consulter notre <a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/hongrie/" class="spip_in">rubrique Conseils aux voyageurs</a> afin de vous familiariser avec les modalités d’entrée et de séjour dans le pays.</p>
<p>Les ressortissants français doivent impérativement se munir d’un <strong>passeport</strong> ou d’une <strong>carte nationale d’identité en cours de validité, quel que soit leur âge et la durée de leur séjour en Hongrie</strong>. En cas de contrôle de police, toute personne doit pouvoir présenter une pièce d’identité (carte nationale d’identité ou passeport) en cours de validité. Il est rappelé qu’un permis de conduire n’est pas recevable comme pièce d’identité. Les <strong>enfants mineurs ne sont pas exemptés de la présentation d’un document de voyage avec photographie d’identité. S’ils voyagent sans leurs parents il leur est recommandé d’être porteurs d’une attestation signée des parents les autorisant à voyager.</strong></p>
<p><strong>Depuis le 21 décembre 2007</strong>, la Hongrie fait partie de l’espace Schengen. En conséquence, le contrôle aux frontières terrestres des voyageurs en provenance de pays de l’espace Schengen a été supprimé depuis cette date.</p>
<p>Le ministère hongrois de la Justice et de la Police est compétent pour les questions de police des étrangers. La mise en œuvre des décisions est assurée par l’<a href="http://www.bmbah.hu/a_bah_ismertetese.php" class="spip_out" rel="external">office de l’immigration et de la nationalité</a> (<i>Bevándorlási és állampolgársági hivatal</i>) et par ses agences.</p>
<h3 class="spip"><a id="sommaire_1"></a>Séjour de moins de trois mois</h3>
<p>Les ressortissants français et les membres de leur famille peuvent séjourner en Hongrie <strong>sans visa</strong>, sous réserve qu’ils ne représentent pas une charge injustifiée pour le système hongrois de prestations sociales. Il n’est pas nécessaire de se déclarer auprès des autorités hongroises.</p>
<h3 class="spip"><a id="sommaire_2"></a>Séjour de plus de trois mois</h3>
<p>Les ressortissants français et les membres de leur famille ont le droit de séjourner plus de trois mois s’ils remplissent les conditions de séjour liées au statut de travailleur, de travailleur indépendant, d’étudiant ou s’ils subviennent à leurs propres besoins.</p>
<p>Ont ainsi droit à une autorisation de séjour pour une durée supérieure à trois mois les personnes suivantes :</p>
<ul class="spip">
<li>les personnes souhaitant exercer une activité professionnelle ;</li>
<li>les personnes disposant de revenus suffisants pour subvenir à leurs besoins et à ceux de leur famille et titulaires d’une couverture maladie suffisante ou, à défaut, disposant de moyens financiers permettant de faire face à leurs dépenses de santé ;</li>
<li>les personnes inscrites dans un établissement d’enseignement reconnu pour y poursuivre des études ou une formation, disposant de revenus suffisants pour subvenir à leurs besoins et à ceux de leur famille et titulaires d’une couverture maladie suffisante ou, à défaut, disposant de moyens financiers permettant de faire face à leurs dépenses de santé ;</li>
<li>les membres de la famille des personnes mentionnées ci-dessus. <strong>Le séjour d’une durée supérieure à trois mois doit être déclaré en personne à la Mairie de son domicile ou auprès de la direction régionale de l’office de l’immigration</strong> au plus tard le 93<sup>ème</sup> jour de présence. Suite à cette déclaration, un <strong>certificat d’enregistrement</strong> à durée indéterminée est remis au requérant le jour de la demande.</li></ul>
<p>Pour obtenir ce certificat d’enregistrement, les ressortissants français doivent justifier de leur nationalité, de leur identité, de leur domicile en Hongrie, de leur emploi en Hongrie, d’une assurance maladie couvrant tous les risques santé valable pour la durée de leur séjour, de ressources pour les étudiants et les inactifs et s’acquitter d’un timbre fiscal de 1000 HUF.</p>
<h3 class="spip"><a id="sommaire_3"></a>Permis de travail</h3>
<p>En tant que ressortissant français, si vous souhaiter exercer une activité professionnelle en Hongrie, il n’est pas nécessaire de solliciter la délivrance d’un permis de travail.</p>
<h3 class="spip"><a id="sommaire_4"></a>Résidence permanente</h3>
<p>Le statut de résident permanent est accordé aux ressortissants français, ainsi qu’aux membres de leur famille, ayant séjourné de façon légale et continue en Hongrie pendant 5 ans. La demande de carte de séjour est effectuée auprès de la direction régionale de l’office de l’immigration.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.mfa.gov.hu/kulkepviselet/FR/fr" class="spip_out" rel="external">Section consulaire de l’ambassade de Hongrie en France</a></li>
<li><a href="http://www.bmbah.hu/" class="spip_out" rel="external">Service de l’immigration et de la nationalité</a> (site en anglais, allemand et français) ;</li>
<li><a href="http://www.kormany.hu/hu" class="spip_out" rel="external">Ministère hongrois de la justice et de la police</a> (site uniquement en hongrois) ;</li>
<li><a href="http://www.magyarorszag.hu/" class="spip_out" rel="external">Site officiel du gouvernement hongrois</a> (site également en anglais) ;</li>
<li><a href="http://ec.europa.eu/eures/" class="spip_out" rel="external">Portail européen sur la mobilité de l’emploi</a>.</li></ul>
<p><i>Mise à jour : juin 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-hongrie-entree-et-sejour-article-animaux-domestiques.md" title="Animaux domestiques">Animaux domestiques</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-hongrie-entree-et-sejour-article-vaccination-113465.md" title="Vaccination">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-hongrie-entree-et-sejour-article-demenagement-113464.md" title="Déménagement">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-hongrie-entree-et-sejour-article-passeport-visa-permis-de-travail.md" title="Passeport, visa, permis de travail">Passeport, visa, permis de travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/entree-et-sejour/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
