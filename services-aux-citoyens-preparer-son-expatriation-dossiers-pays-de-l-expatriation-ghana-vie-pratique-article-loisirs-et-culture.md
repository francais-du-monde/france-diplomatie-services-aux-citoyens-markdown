# Loisirs et culture

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/vie-pratique/article/loisirs-et-culture#sommaire_1">Tourisme</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/vie-pratique/article/loisirs-et-culture#sommaire_2">Activités culturelles</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/vie-pratique/article/loisirs-et-culture#sommaire_3">Sports</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/vie-pratique/article/loisirs-et-culture#sommaire_4">Presse française</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Tourisme</h3>
<p>Parmi les nombreux sites d’intérêt on peut retenir les forts portugais et hollandais jalonnant la côte (le plus ancien date de 1460) et en particulier les villes de Cape Coast et Elmina. Quelques jolies plages sont également à découvrir en région ouest et en région Volta.</p>
<p>Le Ghana possède plusieurs réserves naturelles dont le parc de Mole (réserve d’éléphants) au nord du pays et des sites naturels tels que le lac Bosuntwi (région Ashanti), le lac Volta, la chaine des monts du Togo (Région Volta) des chutes d’eau (Boti Falls, Wli Falls…), et au nord du pays, une mosquée datant du XVIIème siècle.</p>
<p>La région de Kumasi est particulièrement propice à la découverte de l’artisanat Ashanti dont le tissage en kente et les imprimés adinkra sont les fleurons.</p>
<h3 class="spip"><a id="sommaire_2"></a>Activités culturelles</h3>
<h4 class="spip">Activités culturelles en français</h4>
<p>Les Alliances françaises d’Accra et de Kumasi proposent des cours de français et offrent un programme culturel varié tout au long de l’année. L’Alliance française d’Accra propose également des cours d’anglais et une formation bilingue en secrétariat.</p>
<p><a href="http://afaccra.org/fr/" class="spip_out" rel="external">Alliance française d’Accra</a><br class="manualbr">Liberation link, Airport Residential Area<br class="manualbr">P.O. Box CT4904, Cantonments Accra – Ghana<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/vie-pratique/article/loisirs-et-culture#info#mc#afaccra.com#" title="info..åt..afaccra.com" onclick="location.href=mc_lancerlien('info','afaccra.com'); return false;" class="spip_mail">Courriel</a><br class="manualbr">Tél. : +233 (0) 501287814<br class="manualbr"><a href="http://www.facebook.com/AllianceFrancaisedAccra" class="spip_out" rel="external">Page Facebook</a> <br class="manualbr">Twitter : AF_Accra</p>
<p>L’Alliance française d’Accra est devenue au cours du temps le centre culturel le plus important d’Accra de part le nombre (environ 80 par an) et la variété des événements culturels proposés (musique, danse, théâtre, cinéma, expositions). L’alliance dispose également d’une médiathèque, d’un hall d’exposition, d’une librairie francophone et d’un restaurant. Parmi les événements réguliers, on citera la paillotte takpekpe, plateforme de lancement de talents, un mercredi soir par mois et des séances de ciné-clubs tous les mercredis à 19h, proposant des films français, francophone ou du cinéma du monde.</p>
<p>D’autres activités culturelles sont également proposées par le Goethe Institut, le Dei Center (Tesano)(expositions) et la Nubuke Foundation (East Legon) (expositions, spectacles).</p>
<p><strong>Alliance française de Kumasi</strong><br class="manualbr">Tel : +233 3220 230 26 / +233242686 212<br class="manualbr">Courriel <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/vie-pratique/article/loisirs-et-culture#information#mc#afkumasi.org#" title="information..åt..afkumasi.org" onclick="location.href=mc_lancerlien('information','afkumasi.org'); return false;" class="spip_mail">information<span class="spancrypt"> [at] </span>afkumasi.org</a> et <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/vie-pratique/article/loisirs-et-culture#secretary#mc#afkumasi.org#" title="secretary..åt..afkumasi.org" onclick="location.href=mc_lancerlien('secretary','afkumasi.org'); return false;" class="spip_mail">secretary<span class="spancrypt"> [at] </span>afkumasi.org</a></p>
<p>L’alliance française de Kumasi propose également des activités culturelles régulières telles qu’un cinéma club hebdomadaire, le mercredi soir ainsi que des expositions, des concerts et soirées atypiques tout au long de l’année.</p>
<h4 class="spip">Activités culturelles locales</h4>
<p>Les activités culturelles font l’objet d’un certain regain depuis quelques années à Accra.</p>
<p><strong>Cinéma</strong></p>
<p>Accra dispose désormais d’un cinéma multiplex à Accra Mall (East legon) tandis qu’une autre salle devrait ouvrir prochainement dans le centre d’Accra dans le quartier d’Osu.</p>
<p>Des séances de cinéma sont également proposées une fois par semaine par les alliances françaises d’Accra et de Kumasi ainsi que par le Goethe Institut.</p>
<p>Des festivals de cinéma ont également lieu tout au long de l’année organisés par les différentes représentations diplomatiques présentes à Accra (Canada, Inde, Israël, Corée…). En mars la communauté francophone se réunit pour célébrer durant 15 jours la francophonie au travers d’un programme culturel varié.</p>
<p><strong>Musique</strong></p>
<p>Des concerts de musique live ont lieu chaque semaine à Accra, principalement à l’Alliance française d’Accra, au Goethe Institute, au club de jazz +233 et dans de nombreux bars de la capitale.</p>
<p><strong>Théâtre</strong></p>
<p>Le Théâtre national propose de manière plus régulière désormais des pièces de théâtre d’auteurs ghanéens contemporains et accueille d’autres événements tels que des avant-premières de films et des concerts.</p>
<p><strong>Audiovisuels ghanéens</strong></p>
<p>GBC, la radio-télévision nationale, TV3, Metro TV et Viasat I sont les principales chaînes de TV. Il existe désormais des bouquets satellitaires dont celui de DSTV et Multi TV.</p>
<p>La radio d’Etat (GBC) émet en FM à Accra (radio GAR 95,7). De nombreuses radio privées émettent en FM dans la région d’Accra : Atlantic FM (87,9), Gold FM (90,5), Vibe FM (91,9), Joy JM (99,7), Choice FM (102,3), Groove FM (106,3).</p>
<p><strong>Audiovisuels français</strong></p>
<p>Radio France Internationale (RFI) émet en FM à Accra sur 89.5 et à Kumasi, sur 92.9 FM.</p>
<p>Les bouquets de Canal + sont maintenant disponibles au Ghana via les comptoirs de vente de DSTV. Une des agences de DSTV héberge un représentant de Canal Plus ((Multichoice Ghana – 15 Senchi Street, Airport Residential, Accra- - 0302 740540) La chaîne d’info en continu France 24 vient également de s’installer.</p>
<h3 class="spip"><a id="sommaire_3"></a>Sports</h3>
<p>Les sports sont nombreux et variés. Il est aisé de pratiquer à la fois les sports collectifs tels que le football ou le volley-ball ou encore des sports individuels comme le tennis, le golf, le polo, la musculation, la gymnastique, le squash, le ping-pong et le badminton au sein de salles de sport ou de clubs sportifs (Golf Club, Polo Club, Accra Lawn Club). La natation et l’équitation peuvent être pratiquées dans le centre sportif du Burma Camp.</p>
<p>Il est également possible de faire des randonnées (Ghana Hiking group), des circuits à vélo et d’observer les oiseaux dans les environs d’Accra.</p>
<p>Les plages permettent la pratique des sports nautiques ; l’estuaire de la Volta et le Lac Volta permettent de s’adonner à la pêche qui se pratique librement.</p>
<h3 class="spip"><a id="sommaire_4"></a>Presse française</h3>
<p>Les journaux, périodiques et livres français sont plus difficile à trouver et d’un coût plus élevé. Il est par conséquent recommandé de s’abonner personnellement aux journaux et quotidiens français de son choix (les délais d’acheminement peuvent être longs).</p>
<p>A noter qu’il existe une librairie française à l’alliance française d’Accra, proposant des fournitures scolaires, des manuels scolaires, des ouvrages généralistes et des romans.</p>
<p><strong>Sites internet</strong></p>
<p>Le site internet <a href="http://www.accraexpat.com/" class="spip_out" rel="external">Accraexpat.com</a> est à ce jour le plus complet pour se tenir informés des activités culturelles, sportives et festives en tout genre. Il est possible de s’abonner à la newsletter hebdomadaire.</p>
<p>Pour d’autres informations sur la vie au Ghana, <a href="http://www.noworriesghana.com/" class="spip_out" rel="external">ce site anglophone</a> est assez complet et mis à jour régulièrement.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/ghana/vie-pratique/article/loisirs-et-culture). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
