# Déménagement

<p>Le bateau est la meilleure solution pour procéder au déménagement de ses effets personnels de France vers la Tunisie. Délai moyen : un mois. Prix moyens :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Paris-Tunis pour un container de 40 pieds par bateau : 7000 à 10 000 € pour une famille avec 3 enfants. Pour un container de 20 pieds : 3500 à 5000 €.</p>
<p>L’importation des devises étrangères est libre sans limitation de montant. Si le montant des devises est égal ou supérieur à l’équivalent de 25 000 dinars tunisiens vous êtes tenu de le déclarer à la douane à l’entrée.</p>
<p>Les voyageurs non résidents ayant l’intention de réexporter un reliquat de devises d’un montant égal ou supérieur à l’équivalent de 5 000 dinars tunisiens ou d’ouvrir un compte en devise ou en dinars convertibles, sont tenus de déclarer au service des douanes à l’entrée les sommes importées.</p>
<p>La déclaration de devises doit être faite sur le formulaire « Déclaration d’importation de devises en billet de banque » avant de quitter la zone sous douane.</p>
<p>S’agissant d’un déménagement international, il est primordial de s’entourer d’un maximum de garanties en faisant appel à un professionnel disposant de certifications reconnues (marque NF Service, ISO 9002, etc.).</p>
<p><a href="http://www.csdemenagement.fr/" class="spip_out" rel="external">Chambre syndicale du déménagement</a> <br class="manualbr">Tél. : 01 49 88 61 40 - Télécopie : 01 49 88 61 46<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/entree-et-sejour/article/demenagement-110513#contact#mc#csdemenagement.fr#" title="contact..åt..csdemenagement.fr" onclick="location.href=mc_lancerlien('contact','csdemenagement.fr'); return false;" class="spip_mail">Courriel</a></p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/entree-et-sejour/article/demenagement-110513). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
