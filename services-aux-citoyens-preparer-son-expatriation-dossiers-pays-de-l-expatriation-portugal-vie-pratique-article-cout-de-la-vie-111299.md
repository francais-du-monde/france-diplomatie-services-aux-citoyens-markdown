# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/vie-pratique/article/cout-de-la-vie-111299#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/vie-pratique/article/cout-de-la-vie-111299#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/vie-pratique/article/cout-de-la-vie-111299#sommaire_3">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>L’euro est l’unité monétaire en vigueur au Portugal depuis 2002.</p>
<p>La forme de paiement privilégiée, particulièrement dans les magasins et boutiques de petite taille, est le règlement en espèce. Beaucoup de commerçants et de prestataires de services disposent de terminaux de paiement par carte bancaire, mais certains n’acceptent que les cartes de type Multibanco (équivalent national de la carte bleue). De même, il arrive que les plafonds fixés pour un paiement par carte bancaire soient peu élevés (parfois 20 euros). Les chèques, enfin, sont de moins en moins acceptés. Il est en conséquence recommandé de demander avant de réaliser des achats quels modes de paiement et quelles conditions sont requises par le propriétaire de la boutique.</p>
<p>Les <i>chèques de voyage</i> ne sont que difficilement acceptés par les hôteliers portugais. Il est donc recommandé de les échanger contre du numéraire, même si une commission est prélevée sur la transaction.</p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<h4 class="spip">Retraits d’espèces</h4>
<p>Le retrait d’argent est aisé et exonéré de frais bancaire si votre établissement d’origine est situé dans la zone euro. La plupart des banques portugaises sont pourvues de distributeurs automatiques de billets et vous n’aurez aucun mal à trouver l’un de ces guichets en ville. L’affichage se fait souvent automatiquement en français, ce qui minimise le risque d’erreur.</p>
<h4 class="spip">Transactions bancaires</h4>
<p>Avant d’effectuer des transactions bancaires au Portugal, il importe de vérifier l’existence ou non de conventions conclues entre votre établissement bancaire et un ou plusieurs établissements locaux. De même, une vérification des éventuels frais s’appliquant aux divers types d’opérations s’impose.</p>
<p>Pour éviter de mauvaises surprises, une solution peut consister en l’ouverture d’un compte dans une banque locale.</p>
<h4 class="spip">Ouvrir un compte dans une banque portugaise</h4>
<p>Cette opération est gratuite et ne requiert pas d’être titulaire d’un contrat de travail au Portugal.</p>
<p>Une ouverture de compte bancaire au Portugal nécessite au préalable l’enregistrement auprès de l’administration fiscale et l’obtention d’un numéro de contribuable (<i>número de contribuinte</i>). Pour ce faire, il convient de se rendre dans l’un des points d’accueil de proximité (<i>loja do cidadão</i>) muni d’un justificatif de résidence et d’identité.</p>
<p>Une fois cette démarche effectuée, il faudra vous rendre au guichet de l’établissement bancaire choisi, où vous devrez présenter le numéro de contribuable obtenu, ainsi qu’un justificatif d’identité, un justificatif de domicile portugais ou de votre pays de résidence. Si vous choisissez de vous présenter en tant que résident étranger, une adresse au Portugal vous sera demandée pour y faire parvenir les correspondances.</p>
<p><strong>Les principaux établissements bancaires portugais</strong></p>
<ul class="spip">
<li>Caixa Geral de Depósitos</li>
<li>Banco Comercial Português,</li>
<li>Banco Espírito Santo,</li>
<li>Banco Português de Investimento</li>
<li>Santander Totta</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Evolution des prix</h3>
<p>Le pouvoir d’achat des Portugais se situe 25 % en deçà de la moyenne communautaire, ce qui se ressent mécaniquement sur le niveau des prix pratiqués. La tendance depuis 2012 a donc été baissière, puisque les prix ont en moyenne perdu 5 % fin 2013 par rapport à mi 2012. Le niveau de vie au Portugal tend à s’effriter du fait de la situation économique difficile.</p>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<h4 class="spip">Alimentation</h4>
<h5 class="spip">Prix des denrées alimentaires</h5>
<p>Les prix des denrées alimentaires au Portugal sont généralement inférieurs aux standards français de 25 %. Le poisson, les légumes, ainsi que tous les autres produits de base sont bien moins chers et de qualité équivalente. Les marques mondialisées se retrouvent au Portugal.</p>
<p><strong>NB</strong> : Il est facile de trouver du poisson frais et de bonne qualité dans la plupart des restaurants et des magasins au Portugal. Ceci est d’autant plus vrai sur le littoral.</p>
<h5 class="spip">Epiceries et supermarchés </h5>
<p>Les supermarchés sont nombreux, et offrent les possibilités d’approvisionnement les moins chères.</p>
<p>On retrouve au Portugal certaines chaînes européennes de grande distribution, comme Carrefour, Leclerc, Intermarché ou Lidl. Des supermarchés locaux sont aussi très présents, comme Pingo Doce, Minipreço, <i>Continente</i>, etc.</p>
<p>Nombre de boutiques de proximité résistent à l’arrivée des géants de l’agroalimentaire et offrent encore des fruits et légumes frais, ainsi que des produits alimentaires de base (huile, sucre, pâtes, riz, etc.) et quelques produits ménagers. Les prix affichés sont proches de ceux des grandes surfaces.</p>
<p>Des marchés traditionnels se trouvent dans la plupart des municipalités. Il convient de se renseigner dans l’un des points d’accueil de proximité (<i>loja do cidadão</i>) pour obtenir des informations sur le lieu et l’horaire de leur déroulement.</p>
<h5 class="spip">Secteur de la restauration</h5>
<p>Les restaurants portugais sont eux-aussi moins onéreux qu’en France, puisque les prix de départ pour un plat peuvent être proches de cinq euros. Ils oscillent plus généralement entre 7 et 14 euros. Ces intervalles sont notamment valables pour les petits et nombreux restaurants de proximité. Pour des établissements de qualité supérieure, comptez le même prix qu’en France pour un repas.</p>
<h4 class="spip">Vêtements</h4>
<p>Du fait de la permanence de manufactures textiles sur le territoire national, le Portugal affiche des prix compétitifs dans le secteur de l’habillement, inférieurs d’environ 25 à 30 % par rapport aux standards français. La plupart des grandes enseignes de prêt-à-porter son présentes et pratiquent des tarifs spécifiques à la péninsule ibérique.</p>
<p>Le shopping est particulièrement développé à Lisbonne, qui s’enorgueillit d’accueillir les acheteurs du monde entier dans sa multitude de boutiques et dans le plus grand centre commercial d’Europe (el Corte Inglês).</p>
<h4 class="spip">Transports</h4>
<p>Les prix des transports urbains des grandes villes avoisinent ceux de nos réseaux français. Il vous appartient de payer un supplément pour le premier achat de billet rechargeable.</p>
<p>Les transports inter cités sont bon marché. Les trajets inter régionaux sont légèrement moins chers qu’en France.</p>
<h4 class="spip">Hébergement</h4>
<p>Les hébergements temporaires, hôtels, auberges de jeunesse et autres pousadas, sont disponibles à tous prix au Portugal. Une hiérarchie peut être dessinée comme suit :</p>
<ul class="spip">
<li>Bon marché (certaines chambres chez l’habitant) : de 25 à 45 euros.</li>
<li>Prix moyens : de 45 à 70 euros.</li>
<li>Prix élevés : de 70 à 100 euros.</li>
<li>Prix très élevés : plus de 100 euros.</li></ul>
<p>Lors des saisons touristiques (à Pâques et de juin à octobre), les prix peuvent connaître une augmentation de l’ordre de 30 à 50%.</p>
<h4 class="spip">Essence</h4>
<p>Le prix du litre de carburant au Portugal est proche de celui pratiqué en France.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li>Site officiel de la <a href="http://www.livinginlisbon.com/" class="spip_out" rel="external">ville de Lisbonne</a> ;</li>
<li>Site officiel de l’<a href="http://www.livinginportugal.com/fr/" class="spip_out" rel="external">accueil au Portugal</a></li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/vie-pratique/article/cout-de-la-vie-111299). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
