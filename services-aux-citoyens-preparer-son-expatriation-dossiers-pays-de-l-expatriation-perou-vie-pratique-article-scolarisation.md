# Scolarisation

<p>Pour toute information sur la scolarisation dans le système français à l’étranger, vous pouvez consulter nos thématiques sur les <a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures.md" class="spip_in">Etudes supérieures</a> et la <a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais.md" class="spip_in">Scolarité en français</a>.</p>
<p>Vous y trouverez des renseignements sur :</p>
<ul class="spip">
<li>les établissements français du primaire et du secondaire à l’étranger ;</li>
<li>les bourses scolaires et la prise en charge des frais de scolarité à l’étranger ;</li>
<li>les possibilités qui s’offrent à vous si votre enfant ne peut être scolarisé à l’étranger dans le système français (enseignement à distance par le CNED, programme français langue maternelle (FLAM), internats en France) ;</li>
<li>les épreuves du baccalauréat à l’étranger ;</li>
<li>les bourses d’études supérieures en France et à l’étranger ;</li>
<li>l’équivalence des diplômes.</li></ul>
<h4 class="spip">Les établissements scolaires français au Pérou</h4>
<p>Pour trouver un établissement scolaire français à l’étranger, consultez le site de l’<a href="http://www.aefe.fr/" class="spip_out" rel="external">Agence pour l’enseignement français à l’étranger</a>.</p>
<h4 class="spip">Enseignement supérieur</h4>
<p>L’enseignement supérieur au Pérou se décline en quatre catégories d’établissements, publics ou privés.</p>
<p>En 2010, le Pérou compte 92 <strong>universités</strong>, dont 36 publiques et 56 privées.</p>
<p><strong>Les instituts supérieurs technologiques (IST)</strong>, indépendants des universités, dispensent des formations courtes et professionnalisantes de trois ans dans les domaines de la santé, de la mécanique, de la construction, de la comptabilité, du secrétariat, du tourisme, de la gastronomie ou encore du secteur tertiaire en général. 906 Instituts technologiques sont actuellement répertoriés.</p>
<p><strong>Les instituts supérieurs pédagogiques (ISP) </strong>forment en cinq ans les futurs maîtres de collège. Le Pérou compte actuellement près de 416 ISP (179 publics et 237 privés) et 54 facultés d’éducation (29 publiques et 25 privées), lesquels forment chaque année environ 30 000 nouveaux enseignants.</p>
<p><strong>Les instituts de spécialisation et de recherche (<i>Escuelas de Postgrado</i>). </strong>Il s’agit d’Instituts de formation supérieure et de spécialisation, liés aux universités publiques ou privées, accessibles aux étudiants titulaires d’un <i>Bachiller</i> (cinq années d’études supérieures après la fin de la classe de seconde, équivalent à une licence dans le système L-M-D), pour y suivre des études de Maestria (Master), puis de <i>Doctorado</i> (doctorat). Les formations de <i>Postgrado</i>, qu’elles se déroulent au sein d’un établissement public ou privé, sont toutes soumises à des droits d’inscription élevés. Les formations doctorales sont principalement offertes par les universités privées (80), contre 17 formations au sein d’établissements publics.</p>
<h4 class="spip">Organisation des études</h4>
<p>Les étudiants péruviens sont soumis à sélection pour intégrer une université à l’issue de leurs études secondaires qu’ils effectuent dans un <i>colegio</i> jusqu’à l’âge de 16 ans. Chaque année, se présentent environ 470 000 candidats à l’entrée à l’université (303 000 dans les universités publiques et 167 000 dans les universités privées). Seuls environ 183 000 d’entre eux seront retenus, représentant un taux d’admission de l’ordre de 39% (19% dans les universités publiques et 75% dans les universités privées). Cette entrée sélective à l’université explique que de nombreux candidats choisissent de préparer leur concours d’entrée dans des « académies » universitaires, souvent coûteuses. Beaucoup d’entre eux sont, par ailleurs, contraints de tenter le concours à plusieurs reprises.</p>
<p>Le cursus universitaire péruvien est organisé par semestre.</p>
<p>Le premier titre universitaire, le <i>Bachiller</i>, correspond au titre de Licence du système licence-master-doctorat (LMD) et s’acquiert après cinq ans d’études, comprenant deux années d’études générales, suivies d’une spécialisation de trois ans. À l’issue de ces 10 semestres, l’université délivre, souvent automatiquement, le diplôme de <i>Bachiller</i> dans la discipline étudiée pendant les trois dernières années. Certains corps de métiers exigent, cependant, qu’outre le <i>Bachiller</i>, les étudiants obtiennent un <i>Título Profesional </i>, en complétant leur formation par un stage professionnel ou un travail de recherche d’une année supplémentaire.</p>
<p>La poursuite des études en <i>Maestria</i>, équivalent au master du système LMD (<i>Postgrado</i>), suppose d’être titulaire du <i>Bachiller</i>, mais pas du <i>Título profesional</i>. Deux ans d’études sont alors nécessaires pour obtenir la Maestria (quatre cycles-semestres).</p>
<p>Le <i>Doctorado</i> (doctorat) requiert, quant à lui, trois années supplémentaires. Il est à noter que l’obtention du doctorat n’implique pas systématiquement la réalisation d’un véritable travail de recherche. Il s’agit encore bien souvent pour l’étudiant d’une phase d’acquisition des connaissances, plus qu’une analyse et une production personnelles.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/vie-pratique/article/scolarisation). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
