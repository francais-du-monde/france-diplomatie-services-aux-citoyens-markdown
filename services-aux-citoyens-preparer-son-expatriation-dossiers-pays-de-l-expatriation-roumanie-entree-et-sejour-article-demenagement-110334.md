# Déménagement

<p>Un déménagement par la route est conseillé pour les gros volumes et par avion pour les cantines et les vêtements. Le délai d’acheminement par la route est généralement d’une semaine. Les prix des transporteurs routiers varient de 100 à 150 euros par m3. Un inventaire complet du déménagement est nécessaire (photos des meubles et des objets de valeur).</p>
<p>Pour le transfert de résidence dans un Etat membre de l’UE il n’y a pas de formalités douanières à accomplir. Toutefois la sortie de France de certains biens est soumise à des formalités particulières. Par ailleurs, il est préférable de déclarer les objets de valeur lors de l’entrée sur le territoire.</p>
<p>Pour en savoir plus :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.customs.ro/ro/calatori.aspx" class="spip_out" rel="external">Site de la direction générale roumaine des Douanes</a>.</p>
<p>S’agissant d’un déménagement international, il est primordial de s’entourer d’un maximum de garanties en faisant appel à un professionnel disposant de certifications reconnues (marque NF Service, ISO 9002, etc.).</p>
<p><a href="http://www.csdemenagement.fr/" class="spip_out" rel="external">Chambre syndicale du déménagement</a><br class="manualbr">Téléphone : 01 49 88 61 40 <br class="manualbr">Télécopie : 01 49 88 61 46<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/entree-et-sejour/article/demenagement-110334#contact#mc#csdemenagement.fr#" title="contact..åt..csdemenagement.fr" onclick="location.href=mc_lancerlien('contact','csdemenagement.fr'); return false;" class="spip_mail">Courriel</a></p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/entree-et-sejour/article/demenagement-110334). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
