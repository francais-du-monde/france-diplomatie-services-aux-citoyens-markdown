# Lettre de motivation

<p>La lettre de motivation se nomme en allemand <i>Bewerbungsbrief</i> ou <i>Bewerbungsschreiben</i>.</p>
<p>La lettre de motivation est le document le plus important de votre dossier de candidature : mettez-la en évidence dans une chemise transparente au-dessus des autres pièces de votre dossier, sans l’attacher.</p>
<p>La lettre de motivation doit être <strong>dactylographiée</strong>. Une lettre manuscrite sera perçue comme faisant brouillon et votre dossier risque de ne pas franchir la première étape de sélection.</p>
<p><strong>Rédigez votre dossier en allemand</strong>, même pour les filiales d’entreprises françaises. En effet, étant donnée la complexité du droit du travail local, les postes de DRH sont généralement occupés par des Allemands.</p>
<p>Il faut souligner que les fautes de forme, surtout dans la présentation écrite, sont prises très au sérieux et que l’imperfection dans ce domaine est perçue comme un manque de professionnalisme.</p>
<p>Le style de la lettre doit être le plus direct et le plus clair possible, sans négliger la politesse. Quel que soit le nombre de candidatures que vous adressez, il est important de ne pas banaliser votre lettre de motivation. Elle doit rester personnelle et refléter votre personnalité.</p>
<p>Veillez à ne pas trop mettre en avant vos qualités. La publicité de soi-même est vue d’un œil plus critique en Allemagne qu’en France. En effet, chaque déclaration doit en principe être vérifiable par le CV et par les certificats qui y sont joints.</p>
<p>Un style littéraire, encore assez fréquent dans les lettres de motivation françaises, n’est pas du tout bien vu dans des lettres allemandes, car jugé trop long et trop compliqué à lire. Par conséquent, il faut absolument éviter les introductions classiques très fleuries et un peu hypocrites comme « société dynamique dans le secteur X, votre notoriété m’incite à vous contacter… », etc. Il en est de même pour la formule finale. Elle consiste pratiquement sans exception en un simple « <i>Mit freundlichen Grüssen</i> ».</p>
<p>Pour le contenu, il faut suivre les mêmes règles qu’en France. Mais le profil de poste doit être repris très précisément et explicitement afin de démontrer que l’on a bien compris l’objet de l’offre d’emploi.</p>
<p>Insistez sur votre intérêt pour l’entreprise ciblée et sur ce que vous pouvez lui apporter. Le mieux est d’exposer clairement et simplement ce que vous avez à dire, dans la limite d’une page. Les raisons de choix pour l’entreprise et le poste visés doivent apparaître clairement dans votre lettre.</p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/allemagne/emploi-stage/article/lettre-de-motivation-109113). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
