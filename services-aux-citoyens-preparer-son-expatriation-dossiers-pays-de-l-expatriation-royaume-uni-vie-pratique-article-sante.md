# Santé

<p>Pour une présentation des risques épidémiologiques ainsi que des conseils sur la médecine au quotidien, consultez les pages suivantes :</p>
<ul class="spip">
<li><a href="http://www.ambafrance-uk.org/Medecins-et-professions,12099" class="spip_out" rel="external">Conseils, liste de médecins et hôpitaux sur le site du consulat de France à Londres</a></li>
<li><a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/royaume-uni/" class="spip_in">Page dédiée à la santé au Royaume-Uni sur le site Conseils aux voyageurs</a></li></ul>
<h4 class="spip">Médecine de soins</h4>
<h4 class="spip">Inscription au système de santé</h4>
<p>En cas d’installation durable au Royaume-Uni, il est recommandé de souscrire à une assurance médicale privée (les plus importantes sont BUPA et PPP). Les cotisations sont souvent élevées, mais certaines sont prises en charge par l’employeur.</p>
<p>A défaut de recourir à la médecine privée fort coûteuse au Royaume-Uni (à titre d’exemple, le prix d’une consultation chez un généraliste du secteur privé varie de 75 £ (95 €) à 250 £ (315€) <strong>les ressortissants français peuvent bénéficier gratuitement</strong> des soins dispensés par les médecins généralistes et les hôpitaux appartenant au <a href="http://www.nhs.uk/" class="spip_out" rel="external">National Health Service</a>, le système de santé publique.</p>
<p>Pour en bénéficier, il est recommandé dès son arrivée au Royaume-Uni, dès lors que l’on dispose d’une adresse fixe, de <strong>s’inscrire sur la liste des patients d’un médecin généraliste (<i>General Practitioner - GP</i>) de son quartier </strong>et ce avant même la première consultation. Munissez-vous d’un justificatif de domicile et d’une pièce d’identité afin de pouvoir bénéficier de l’accès aux services de soins gratuits du NHS. Cette démarche peut s’avérer très utile en cas d’urgence : le praticien facturera ses services au tarif de la médecine privée si vous n’êtes pas inscrit.</p>
<p>Une liste des GP est disponible sur le <a href="http://www.nhs.uk/service-search/gp/locationsearch/4" class="spip_out" rel="external">site du NHS</a>.</p>
<p>Après avoir rempli un formulaire, l’assuré reçoit une carte médicale avec son immatriculation auprès du NHS.</p>
<p>Le système de santé britannique, dans son ensemble, a fait l’objet d’investissements financiers importants au cours des années 2000 qui ont permis d’en améliorer le fonctionnement et de régler notamment pour une grande part le problème des listes d’attente pour la réalisation de certaines interventions qui le caractérisaient dans les années 90. Toutefois, certains problèmes demeurent, comme l’encombrement des services d’urgence, liés notamment aux difficultés de mise en œuvre de la permanence des soins.</p>
<p>Contrairement à la France, la quasi-totalité des médecins spécialistes exercent à l’hôpital et sont salariés du NHS. Pour pouvoir consulter un médecin spécialiste dans le cadre du NHS, il est nécessaire d’être référé par son médecin généraliste. L’admission à l’hôpital se fait par l’intermédiaire du service des accidents ou des urgences ou sur demande d’un médecin généraliste. Les séjours dans les hôpitaux gérés par le service national de santé sont gratuits sauf si l’assuré souhaite des aménagements spéciaux (chambre individuelle, par exemple).</p>
<p>Certains hôpitaux disposent également de services de médecine ambulatoire (<i>walk in</i>), où l’attente peut être très longue, jusqu’à plusieurs heures.</p>
<p>Pour obtenir un rendez-vous chez un spécialiste de médecine publique, l’assuré doit systématiquement passer par l’intermédiaire de son GP.</p>
<p>Les tarifs pour les soins dentaires varient, selon que le patient est traité dans le cadre du NHS ou à titre privé. Pour les patients traités dans le cadre du NHS, le système de tarification est organisé autour de trois tranches : il n’existe pas de tarif fixé pour chaque acte spécifique au sein de ces trois tranches. Le patient paie pour un traitement complet. (80% des frais jusqu’à concurrence de 360 £ (515 €) par traitement), à l’exclusion des prothèses dentaires, couronnes, bridges. Les dentistes exerçant dans le cadre du NHS sont cependant de moins en moins nombreux et l’on doit souvent recourir aux soins coûteux du secteur privé.</p>
<p>Les tarifs sont généralement réévalués chaque année. Au 1er avril 2012, ces tarifs étaient fixés en Angleterre à :</p>
<ul class="spip">
<li>17,50£ pour les traitements de la tranche 1 (examen, détartrage, application d’une préparation à base de fluore ou un scellant de fissure si besoin) ;</li>
<li>48£ pour les traitements de la tranche 2 (traitement de la tranche 1, plus extraction de dents, plombage, traitement du canal radiculaire) ;</li>
<li>209£ pour les traitements de la tranche 3 (couvrent l’ensemble des traitements de la tranche 1 et 2, plus la pose de couronnes, bridges et dentier).</li></ul>
<p>A titre d’exemple, l’extraction d’une dent de sagesse figure dans la 2ème tranche (48£), de même que les traitements endodontiques. Voici une <a href="http://www.nhs.uk/NHSEngland/AboutNHSservices/dentists/Pages/common-nhs-dental-treatments.aspx" class="spip_out" rel="external">liste des traitements</a> dentaires disponibles dans le cadre du NHS, comprenant leur coût.</p>
<p>Aucune participation financière n’est toutefois due pour :</p>
<ul class="spip">
<li>Les femmes enceintes ou ayant un enfant âgé de moins d’un an ; les enfants âgés de moins de 18 ans ;</li>
<li>les jeunes âgés de moins de 19 ans qui font des études à temps complet ;</li>
<li>les soins dentaires effectués lors d’un séjour à l’hôpital ;</li>
<li>les bénéficiaires de certaines prestations non-contributives telles que l’aide au revenu (<i>Income Support</i>), l’allocation de chômage versée sous condition de ressources (<i>Income-based Jobseeker’s Allowance</i>) ou l’allocation de travail et de soutien (<i>Employment and Support Allowance</i>) ;</li>
<li>les personnes bénéficiaires du crédit d’impôt pour pensionnés (<i>Pension Credit Guarantee</i>) ou disposant d’un certificat d’exemption fiscale (<i>NHS tax credit exemption certificate</i>). Pour les patients traités dans le cadre du secteur privé, le tarif des actes varie en fonction des cabinets dentaires.</li></ul>
<p>Les médicaments inscrits sur la liste du NHS (<i>drug tarif</i>) sont obtenus moyennant le paiement d’une somme forfaitaire de <strong>7,85 £ </strong>par médicament prescrit. Voici <a href="http://www.ambafrance-uk.org/Medecins-et-professions%2C12099" class="spip_out" rel="external">une liste des médecins et autres praticiens français accrédités</a> par le Consulat général de France à Londres.</p>
<p>Pour plus d’informations sur les médecins français présents à Londres et notamment à South Kensington :</p>
<ul class="spip">
<li><a href="http://www.medicare-francais.co.uk/" class="spip_out" rel="external">Medicare-francais.co.uk</a></li>
<li><a href="http://www.lamaisonmedicale.co.uk/" class="spip_out" rel="external">La maison médicale</a></li>
<li><a href="http://www.medecinefrancaiselondres.org.uk/" class="spip_out" rel="external">Médecine francaise Londres</a></li>
<li><a href="http://www.cabinetdentaire.co.uk/" class="spip_out" rel="external">Cabinet dentaire</a></li></ul>
<p><strong>Dispensaire français</strong><br class="manualbr">184 Hammersmith Road <br class="manualbr">London W6 7DJ <br class="manualbr">Tél. : +44 (0) 208 222 8822<br class="manualbr">Ouvert du lundi au vendredi sur rendez-vous, de 9h30 à 17h30.</p>
<p>Numéro pour joindre les <strong>urgences </strong> (ambulances, police et pompiers) : <strong>999</strong>.</p>
<h4 class="spip">Affiliation au régime de la <i>National Insurance</i> (sécurité sociale britannique)</h4>
<p>Vous devez téléphoner au <i>Jobcentre Plus</i> le plus proche pour faire une demande de <i>National Insurance Number</i> (numéro de cotisant social). Vous devez avoir le droit de travailler ou d’étudier au Royaume-Uni afin d’obtenir un NIN. <i>Jobcentre Plus</i> vous enverra un formulaire d’inscription. Le centre peut aussi vous demander de vous présenter pour un entretien.</p>
<p>Le formulaire d’inscription précise les documents nécessaires à présenter :</p>
<ul class="spip">
<li>Passeport/ carte d’identité</li>
<li>Permis de résidence</li>
<li>Certificat de naissance/ d’adoption</li>
<li>Certificat de mariage / PACS</li>
<li>Permis de conduire</li></ul>
<p>Cette inscription est indispensable pour que les cotisations (chômage et retraite) qui sont prélevées tous les mois sur votre salaire soient bien reversées sur votre compte de sécurité sociale britannique. Vous devrez également remplir un formulaire P46 qui sera adressé au <i>Tax Office</i> de l’employeur et obtiendrez ainsi un <i>Tax Code</i>.</p>
<p>Jobcentre Plus Application Line<br class="manualbr">Tel : 0845 600 0643<br class="autobr">Lundi à vendredi, 08h00 à 18h00</p>
<p>NB : le <i>National Insurance Number</i> ne peut être exigé de vous lors d’un entretien préalable à l’obtention d’un emploi. Expliquez qu’il s’agit de votre premier emploi et que vous effectuerez les démarches indiquées ci-dessus.</p>
<h4 class="spip">Carte européenne d’assurance maladie</h4>
<p>L’accès aux soins de santé se fait uniquement <strong>sur présentation de la carte européenne d’assurance maladie </strong>ou du certificat provisoire de remplacement.</p>
<p>La personne qui ne présenterait pas l’un de ces deux documents serait susceptible d’être traitée comme un patient privé et se verrait facturer la totalité des soins reçus.</p>
<p>D’autre part, lorsque vous résidez au Royaume-Uni et souhaitez effectuer un déplacement en France, pour être couvert sur une courte période, il est nécessaire de se procurer la carte européenne d’assurance maladie (EHIC - european health insurance card) par internet auprès de <a href="http://www.ehic.org.uk/" class="spip_out" rel="external">www.ehic.org.uk</a>. Vous pouvez aussi téléphoner le EHIC application service : 0300 33 01 350.</p>
<p><strong>Il est vivement conseillé de consulter le médecin traitant avant le départ et de souscrire une assurance rapatriement.</strong></p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/vie-pratique/article/sante). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
