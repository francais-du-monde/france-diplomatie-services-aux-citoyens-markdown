# Logement

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/vie-pratique/article/logement-113451#sommaire_1">Où se loger ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/vie-pratique/article/logement-113451#sommaire_2">Auberges de jeunesse </a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/vie-pratique/article/logement-113451#sommaire_3">Conditions de location</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/vie-pratique/article/logement-113451#sommaire_4">Hôtels</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/vie-pratique/article/logement-113451#sommaire_5">Electricité</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/vie-pratique/article/logement-113451#sommaire_6">Electroménager</a></li></ul>
<p>Toutes les issues des villas doivent être munies de barreaux de protection. Le salaire d’un gardien est estimé à environ 50 000 FCFA (76€).</p>
<h3 class="spip"><a id="sommaire_1"></a>Où se loger ?</h3>
<p>A Ouagadougou, les quartiers résidentiels (Zone du Bois, Petit Paris…), se trouvent dans plusieurs zones de la ville, relativement proches du centre, excepté le nouveau quartier "Ouaga 2000" situé à 20 minutes du centre ville. Seules les villas sont disponibles.</p>
<p><strong>Ouagadougou </strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Loyer mensuel </strong> <strong>quartier résidentiel</strong></td>
<td>francs CFA</td>
<td>Euros</td></tr>
<tr class="row_even even">
<td>Villa</td>
<td>650 000</td>
<td>991</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_2"></a>Auberges de jeunesse </h3>
<p>Il n’y a pas d’auberges de jeunesse au Burkina Faso.</p>
<h3 class="spip"><a id="sommaire_3"></a>Conditions de location</h3>
<p>D’une manière générale, les logements sont mal entretenus par leur propriétaire. En général, les baux sont renouvelables tous les ans par tacite reconduction. Le loyer est payable un an à l’avance.</p>
<p>Estimation des charges (non comprises dans le loyer) :</p>
<ul class="spip">
<li>Eau : 60 000 FCFA (91€)</li>
<li>Electricité : 250 000 FCFA (soit 381€ - la climatisation est nécessaire presque toute l’année).</li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Hôtels</h3>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Prix moyen d’une chambre d’hôtel (chambre double)</strong></td>
<td>francs CFA</td>
<td>Euros</td></tr>
<tr class="row_even even">
<td>Grand tourisme</td>
<td>75 000</td>
<td>114</td></tr>
<tr class="row_odd odd">
<td>Moyen tourisme</td>
<td>45 000</td>
<td>69</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_5"></a>Electricité</h3>
<p>Le courant électrique est de 220 volts (prises françaises).</p>
<h3 class="spip"><a id="sommaire_6"></a>Electroménager</h3>
<p>L’équipement électroménager disponible sur place, de qualité très moyenne, est importé. Le courant électrique est de 220 volts (prises françaises). La climatisation est nécessaire presque toute l’année.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/vie-pratique/article/logement-113451). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
