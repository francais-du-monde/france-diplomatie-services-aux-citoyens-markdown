# Vaccination

<p>A l’entrée du pays aucune vaccination n’est exigée en provenance de France ; vaccination contre la fièvre jaune en provenance d’une zone endémique.</p>
<p>Sont conseillées pour des raisons médicales :<br class="manualbr"><strong>Adultes</strong> : mise à jour des vaccinations contre la diphtérie, le tétanos et la poliomyélite ; vaccination contre la typhoïde, l’hépatite A, l’hépatite B. Il est recommandé d’être vacciné contre la grippe pour les voyages en période d’hiver.<br class="manualbr"><strong>Enfants</strong> : vaccinations recommandées en France par le ministère de la Santé – et en particulier : B.C.G. et hépatite B dès la naissance, rougeole dès l’âge de 9 mois.</p>
<p>On trouve tous les vaccins sur place.</p>
<p>Pour en savoir plus consultez :</p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/spip.php?page=article&amp;id_article=117330" class="spip_out">notre article thématique</a> sur la vaccination</li>
<li>la <a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/mexique/" class="spip_in">fiche Mexique de la rubrique Conseils aux voyageurs</a></li>
<li>la <a href="http://www.cimed.org/" class="spip_out" rel="external">fiche Mexique sur le site du CIMED</a></li></ul>
<p><i>Mise à jour : septembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/entree-et-sejour/article/vaccination). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
