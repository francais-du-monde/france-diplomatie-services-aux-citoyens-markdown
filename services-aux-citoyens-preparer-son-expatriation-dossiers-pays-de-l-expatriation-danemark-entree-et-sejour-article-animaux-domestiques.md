# Animaux domestiques

<p>Les informations ci-dessous indiquent les documents nécessaires pour pouvoir emmener votre animal de compagnie (chat, chien, furet, rongeur, oiseau de volière, reptile et amphibien, invertébrés et poisson d’aquarium) au Danemark.</p>
<p>Le site du <a href="http://www.uk.foedevarestyrelsen.dk/forside.htm" class="spip_out" rel="external">ministère de l’Agriculture</a> danois vous apportera des informations très complètes sur l’importation de votre animal de compagnie sur le territoire danois.</p>
<p>Une information très détaillée est également disponible sur le site Internet du <a href="http://agriculture.gouv.fr/" class="spip_out" rel="external">ministère de l’Agriculture français</a>.</p>
<h4 class="spip">Informations générales</h4>
<p>Le pays de destination se trouve dans l’Union européenne (sauf Irlande, Malte, Suède, Royaume-Uni et Finlande) Les chiens, les chats et les furets doivent satisfaire aux conditions suivantes :</p>
<ul class="spip">
<li>être identifiés par puce électronique ou par tatouage clairement visible ;</li>
<li>être valablement vaccinés contre la rage ;</li>
<li>être titulaires d’un passeport délivré par un vétérinaire habilité attestant de l’identification et de la vaccination antirabique de l’animal.Le système d’identification électronique, ainsi que la reconnaissance de la validité de la vaccination contre la rage, peuvent varier d’un Etat membre à l’autre. Il est donc vivement recommandé de prendre contact avec l’<a href="http://frankrig.um.dk/fr.aspx" class="spip_out" rel="external">ambassade du Danemark à Paris</a> .</li></ul>
<p>En France, la réalisation de la primo-vaccination antirabique n’est considérée comme valable qu’à partir de 21 jours après la fin du protocole de vaccination prescrit par le fabricant. La vaccination antirabique de rappel est considérée en cours de validité le jour de sa réalisation.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Notre article thématique <a href="services-aux-citoyens-preparer-son-expatriation-douanes-article-animaux-domestiques.md" class="spip_in">Animaux domestiques</a>.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/entree-et-sejour/article/animaux-domestiques). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
