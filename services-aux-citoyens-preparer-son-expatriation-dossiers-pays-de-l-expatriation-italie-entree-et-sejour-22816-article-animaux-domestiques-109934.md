# Animaux domestiques

<p>Les règles qui s’appliquent pour les mouvements de chiens, de chats et de furets (animaux de compagnie) entre les Etats membres de l’Union européenne (UE) ont été harmonisées. Cette réglementation européenne est entrée en vigueur le 1er octobre 2004.</p>
<p>Depuis cette date, tout chien, chat ou furet provenant d’un Etat membre de l’Union européenne doit, lorsqu’il voyage dans un autre Etat membre :</p>
<ul class="spip">
<li>être identifié par puce électronique,</li>
<li>être valablement vacciné contre la rage, soit entre 30 jours et 12 mois avant le début du séjour,</li>
<li>être titulaire d’un passeport délivré par un vétérinaire habilité attestant de l’identification et de la vaccination contre la rage de l’animal.</li></ul>
<p>L’Italie refuse des animaux de moins de trois mois et non vacciné contre la rage.</p>
<p>Le système d’identification électronique, ainsi que la reconnaissance de la validité de la vaccination contre la rage, peuvent varier d’un Etat membre à l’autre. Il est donc vivement recommandé de prendre contact avec <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-etrangers-en-france/article/annuaire-des-representations-etrangeres-en-france" class="spip_in">l’ambassade d’Italie en France</a>.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Notre article thématique sur l’<a href="services-aux-citoyens-preparer-son-expatriation-douanes-article-animaux-domestiques.md" class="spip_in">exportation d’animaux domestiques</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/entree-et-sejour-22816/article/animaux-domestiques-109934). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
