# Programme Vacances-Travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/emploi/article/programme-vacances-travail#sommaire_1">Voyager et découvrir d’autres cultures en travaillant : le visa « vacances- travail »</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/emploi/article/programme-vacances-travail#sommaire_2">Les conditions à remplir</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/emploi/article/programme-vacances-travail#sommaire_3">Formalités pratiques </a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/emploi/article/programme-vacances-travail#sommaire_4">Données relatives au programme vacances-travail pour l’année 2014</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Voyager et découvrir d’autres cultures en travaillant : le visa « vacances- travail »</h3>
<p class="spip_document_84930 spip_documents spip_documents_center">
<img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L1000xH190/PVT-11-1000x190_cle09aaf3-0de00.jpg" width="1000" height="190" alt=""></p>
<h4 class="spip">Partir au titre du programme « vacances travail » </h4>
<p>Ce programme s’adresse à un public jeune de 18 à 30 ans et désireux de s’expatrier, durant une durée maximale d’un an, à des fins touristique et culturelle dans l’un des pays partenaires, en ayant la possibilité de travailler sur place pour compléter leurs moyens financiers.</p>
<p>Le cadre de ce programme est précisé, de manière réciproque, par un accord bilatéral que la France a conclu avec douze pays ou territoires (à ce jour) : Japon, Nouvelle Zélande, Australie, Canada, Corée du Sud, Russie, Argentine, Hong Kong, Chili, Colombie, et Taiwan (entrée en vigueur le 8 août 2016). Par ailleurs, des accords qui devraient bientôt entrer en vigueur, ont été signés avec le Brésil en 2013 et avec l’Uruguay en 2016.</p>
<p class="spip_document_84931 spip_documents spip_documents_center">
<img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L1000xH190/PVT-bas_de_page_FD_cle8521e1-1024d.jpg" width="1000" height="190" alt=""></p>
<h4 class="spip">Comment faire la demande de visa « vacances travail » ? </h4>
<p>La demande de visa doit être faite auprès de la <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-etrangers-en-france/article/annuaire-des-representations-etrangeres-en-france" class="spip_in">représentation diplomatique ou consulaire</a> en France de la destination d’accueil.</p>
<p>Le visa de long séjour à entrées multiples qui vous sera délivré est valable un an. La durée du séjour ne doit pas excéder un an sauf dispositif particulier, sans possibilité de prolongation. Il n’est, en principe, pas possible de changer de statut pendant la durée du séjour. La durée d’emploi chez un même employeur peut, en outre, être limitée.</p>
<p>A l’exception de l’Australie et de la Nouvelle-Zélande, le nombre de participants à ce programme est limité et fixé chaque année entre les pays participants. A ce jour, ces quotas s’élèvent à 6750 pour le Canada, 2 000 pour la Corée, 1 500 pour le Japon, 700 pour l’Argentine, 500 pour la Russie, 500 pour Hong-Kong, 500 pour Taïwan, 300 pour le Chili et 300 pour la Colombie.</p>
<h3 class="spip"><a id="sommaire_2"></a>Les conditions à remplir</h3>
<p>Elles sont définies par chacun des accords bilatéraux et sont disponibles en consultant notamment les sites internet de chacune de nos ambassades dans les pays concernés. En général, ce sont les suivantes :</p>
<ul class="spip">
<li>Ne pas avoir déjà bénéficié de ce programme dans le pays en question. Exception à cette règle : depuis le 1er novembre 2005, les bénéficiaires d’un premier visa " vacances - travail " et qui ont fait au moins 3 mois de récoltes saisonniers dans certaines régions d’Australie peuvent solliciter un deuxième visa de ce type</li>
<li>Être âgé de 18 à 30 ans révolus à la date du dépôt de la demande de visa (35 ans pour le Canada) ;</li>
<li>Ne pas être accompagné d’enfants à charge ;</li>
<li>Être titulaire d’un passeport français en cours de validité ;</li>
<li>Être en possession d’un billet de retour ou de ressources suffisantes pour acheter un billet de retour ;</li>
<li>Disposer de ressources financières nécessaires pour subvenir aux besoins au début du séjour. Le montant minimal des ressources est fixé chaque année par les États signataires. A ce jour, le montant minimal des ressources par pays est fixé à : Australie - 5000 A$, soit environ 3800 euros ; Canada - 2100 euros ; Corée du Sud - 2500 euros ; Japon - 3100 euros ; Nouvelle-Zélande – 2500 euros ; Argentine - 2500 euros ; Russie : pas de montant de ressources financières minimum fixé) ; Hong-Kong - HK$ 25.000, soit environ 2500 euros, Chili-2500 euros, Taïwan - 2100€ et pour la Colombie, les jeunes Français doivent attester qu’ au cours des 3 derniers mois ils ont eu des revenus bancaires moyens d’au moins 5 salaires minimum mensuels - actuellement  COP 3 200 000-, soit un montant d’environ 1000 €.</li>
<li>Le cas échéant, fournir une lettre de motivation, voire un curriculum vitae ;</li>
<li>Le cas échéant, présenter un certificat médical et un casier judiciaire vierge ;</li>
<li>Justifier de la possession d’une assurance privée couvrant tous les risques liés à la maladie, la maternité, l’invalidité, l’hospitalisation et le rapatriement pour la durée du séjour.</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Formalités pratiques </h3>
<p>Toutes les informations sur le programme et les dossiers de demande de visas sont disponibles auprès des consulats des pays concernés ou sur leur site Internet :</p>
<p><strong>Argentine</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>   <a href="http://www.efran.mrecic.gov.ar/" class="spip_out" rel="external">Ambassade d’Argentine en France</a> </p>
<p><strong>Australie</strong></p>
<ul class="spip">
<li><a href="http://www.ambafrance-au.org/" class="spip_out" rel="external">Consulat général de France à Sydney</a> ;</li>
<li><a href="http://www.france.embassy.gov.au/" class="spip_out" rel="external">Ambassade d’Australie en France</a> ;</li>
<li><a href="http://www.immi.gov.au/" class="spip_out" rel="external">Ministère de l’Immigration et de la Citoyenneté</a></li></ul>
<p><strong>Canada</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.cic.gc.ca/" class="spip_out" rel="external">Ambassade du Canada en France</a></p>
<p><strong>Japon</strong></p>
<ul class="spip">
<li><a href="http://www.fr.emb-japan.go.jp/" class="spip_out" rel="external">Ambassade de Japon en France</a></li>
<li><a href="http://www.jawhm.or.jp/" class="spip_out" rel="external">Japan Association for working holidays makers</a></li></ul>
<p><strong>Nouvelle-Zélande</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.nzembassy.com/netherlands/going-new-zealand/working-new-zealand" class="spip_out" rel="external">Ambassade de Nouvelle-Zélande en France</a></p>
<p><strong>Corée du Sud</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://fra.mofa.go.kr/worldlanguage/europe/fra/visa/working/index.jsp" class="spip_out" rel="external">Ambassade de la République de Corée du Sud en France</a></p>
<p><strong>Hong-Kong</strong></p>
<ul class="spip">
<li><a href="http://www.consulfrance-hongkong.org/Visa-vacances-travail" class="spip_out" rel="external">Consulat Général de France à Hong-Kong et Macao</a></li>
<li><a href="http://www.immd.gov.hk/en/services/hk-visas/working-holiday.html" class="spip_out" rel="external">Site du service de l’immigration à Hong-Kong</a></li></ul>
<p><strong>Chili</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.ambafrance-cl.org/" class="spip_out" rel="external">Ambassade de France au Chili</a></p>
<p><strong>Colombie</strong></p>
<ul class="spip">
<li><a href="http://www.ambafrance-co.org/" class="spip_out" rel="external">Ambassade de France en Colombie</a></li>
<li><a href="http://paris.consulado.gov.co/" class="spip_out" rel="external">Consulat de Colombie à Paris</a></li></ul>
<p><strong>Taïwan</strong></p>
<ul class="spip">
<li><a href="http://www.roc-taiwan.org/fr_fr/post/10014.html" class="spip_out" rel="external">Bureau de représentation de Taipei en France</a></li>
<li><a href="http://www.france-taipei.org/Visas-Vacances-Travail-4453" class="spip_out" rel="external">Bureau Français de Taipei</a></li></ul>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li>Le site internet dédié aux <a href="http://www.pvtistes.net/" class="spip_out" rel="external">pvtistes</a> (titulaires du PVT) :</li>
<li>Le <a href="http://www.working-holiday-visas.com/" class="spip_out" rel="external">site généraliste</a> sur toutes les destinations pour le visa vacances travail</li>
<li>Le site <a href="http://www.guide-australie.com/" class="spip_out" rel="external">dédié à l’Australie avec une section spéciale pour les jeunes en WHV (Working Holiday Visa)</a></li>
<li>Le site dédié aux <a href="http://www.pvtcanada.com/" class="spip_out" rel="external">Français souhaitant partir au Canada</a> dans le cadre du PVT (Programme Vacances Travail)</li></ul>
<blockquote class="texteencadre-spip spip"><strong>Référence des accords en vigueur</strong>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span> <strong>Argentine</strong> : accord entre la France et l’Argentine relatif au programme "vacances - travail" signé à Paris le 18 février 2011 et entré en vigueur le 1er juin 2011 (décret n°2011-800 paru au Journal officiel du 3 juillet 2011) ; </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span> <strong>Australie</strong> : accord entre la France et l’Australie relatif au programme " vacances - travail " signé à Canberra le 24 novembre 2003 et entré en vigueur le 21 février 2004 (décret n°2004-264 paru au Journal officiel du 26 mars 2004) ; </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span> <strong>Canada</strong> : accord entre la France et le Canada relatif aux échanges de jeunes signé à Ottawa le 14 mars 2013 et entré en vigueur le 1er janvier 2015 (décret n°2015-8 paru au Journal officiel du 9 janvier 2015) ; </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span> <strong>Corée du Sud</strong> : accord entre la France et la République de Corée relatif au programme" vacances-travail " signé à Séoul le 20 octobre 2008 et entré en vigueur le 1er janvier 2009 (décret n°2009-31 paru au Journal officiel du 11 janvier 2009) ; </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span> <strong>Japon</strong> : accord entre la France et le Japon relatif au visa " vacances - travail " signé à Paris le 8 janvier 1999 et entré en vigueur le 15 juillet 2000 " (décret n°2000-725 paru au Journal officiel du 2 août 2000). </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span> <strong>Nouvelle-Zélande</strong> : Convention relative au programme vacances-travail entre la France et la Nouvelle-Zélande signée à Paris le 2 juin 1999 et entrée en vigueur le 6 avril 2000 (décret n°2000-400) paru au Journal officiel du 12 mai 2000). </p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span> <strong>Hong-Kong </strong> : accord entre la France et le Gouvernement de la Région Administrative Spéciale de Hong-Kong de la République Populaire de Chine relatif au programme "vacances-travail" signé à Hong-Kong le 6 mai 2013 et entré en vigueur le 1er juillet 2013 (décret n°2013-600 paru au Journal officiel du 10 juillet 2013).</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Colombie</strong> : accord entre la France et la Colombie relatif au programme "vacances-travail" signé à Bogotá le 25 juin 2015 et entré en vigueur le 1er décembre 2015 (<a href="http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000031600396&amp;fastPos=1&amp;fastReqId=1616191598&amp;categorieLien=id&amp;oldAction=rechTexte" class="spip_out" rel="external">Décret n° 2015-1632 du 10 décembre 2015, paru au Journal officiel n°0288 du 12 décembre 2015</a>).</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Chili</strong> : accord entre la France et le Chili relatif au programme "vacances-travail" signé à Paris le 8 juin 2015 et entré en vigueur ler novembre 2015 (décret n°2015- 1472 paru au Journal officiel du 14 novembre 2015).</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <strong>Uruguay</strong> : accord entre la France et l’Uruguay relatif au programme "vacances-travail"signé le 25 février 2016.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span> Par ailleurs, la France a signé avec la <strong>Russie</strong> à Rambouillet le 27 novembre 2009 un « accord sur les migrations professionnelles », entré en vigueur le 1er mars 2011, et qui comporte des dispositions relatives aux « visas vacances-travail ». (décret n° 2011-450 du 22 avril 2011 paru au Journal officiel du 24 avril 2011).</p></blockquote>
<h3 class="spip"><a id="sommaire_4"></a>Données relatives au programme vacances-travail pour l’année 2014</h3>
<p>Téléchargez au format ouvert les <a href="https://www.data.gouv.fr/fr/datasets/programme-vacances-travail-pvt-pour-lannee-2014/" class="spip_out" rel="external">données relatives au programme vacances-travail au titre de l’année 2014</a>, sur la plateforme ouverte des données publiques françaises.</p>
<p><i>Mise à jour : mars 2016</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/emploi/article/programme-vacances-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
