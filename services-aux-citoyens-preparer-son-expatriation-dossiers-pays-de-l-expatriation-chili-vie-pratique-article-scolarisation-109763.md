# Scolarisation

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/vie-pratique/article/scolarisation-109763#sommaire_1">Les établissements scolaires français au Chili</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/vie-pratique/article/scolarisation-109763#sommaire_2">Enseignement supérieur</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Les établissements scolaires français au Chili</h3>
<p>Pour toute information sur la scolarisation dans le système français à l’étranger, vous pouvez consulter nos thématiques sur les <a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures.md" class="spip_in">études</a> et la <a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais.md" class="spip_in">scolarisation</a>.</p>
<p>Vous y trouverez des renseignements sur :</p>
<ul class="spip">
<li>Les établissements français du primaire et du secondaire à l’étranger ;</li>
<li>Les bourses scolaires et la prise en charge des frais de scolarité à l’étranger ;</li>
<li>Les possibilités qui s’offrent à vous si votre enfant ne peut être scolarisé à l’étranger dans le système français (enseignement à distance par le CNED, programme français langue maternelle (FLAM), internats en France) ;</li>
<li>Les épreuves du baccalauréat à l’étranger ;</li>
<li>Les bourses d’études supérieures en France et à l’étranger ;</li>
<li>L’équivalence des diplômes.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Enseignement supérieur</h3>
<p>Les universités locales offrent toutes les possibilités de formation. Il existe une modalité spéciale d’admission pour les étudiants étrangers.</p>
<p>Vous trouverez sur le site <a href="http://www.universia.cl/" class="spip_out" rel="external">Universia.cl</a> des informations sur les universités chiliennes, ainsi que des renseignements utiles sur les formalités administratives.</p>
<p>Par ailleurs une <a href="http://www.diplomatie.gouv.fr/fr/dossiers-pays/http-publication-diplomatie-gouv-fr-fr-dossiers-pays-chili/presentation-du-chili/article/enseignement-superieur-71383" class="spip_in">fiche très détaillée sur l’enseignement supérieur</a> au Chili est disponible sur le site du ministère des Affaires étrangères.</p>
<p>Vous pouvez également consulter les sites Internet des universités chiliennes :</p>
<ul class="spip">
<li><a href="http://www.uchile.cl/" class="spip_out" rel="external">Université du Chili</a> (<i>Universidad de Chile</i>)</li>
<li><a href="http://www.udesantiago.cl/" class="spip_out" rel="external">Université de Santiago du Chili</a> (<i>Universidad de Santiago de Chile</i>)</li>
<li><a href="http://www.uc.cl/" class="spip_out" rel="external">Université catholique</a>  (<i>Pontificia universidad catolica de Chile</i>)</li>
<li><a href="http://www.unap.cl/" class="spip_out" rel="external">Université Arturo Prat</a> (<i>Universidad Arturo Prat - Chile</i>)</li>
<li><a href="http://www.uach.cl/" class="spip_out" rel="external">Université australe du Chili</a> (<i>Universidad austral de Chile</i>)</li>
<li><a href="http://www.uctemuco.cl/" class="spip_out" rel="external">Université catholique de Temuco</a>  (<i>Universitad catolica de Temuco</i>)</li>
<li><a href="http://www.ucv.cl/" class="spip_out" rel="external">Université catholique de Valparaiso</a> (<i>Pontificia universidad catolica de Valparaiso</i>)</li>
<li><a href="http://www.ucsc.cl/" class="spip_out" rel="external">Université catholique de l’Immaculée Conception</a> (<i>Universidad catolica de la Santisima Concepcion</i>)</li>
<li><a href="http://www.ucm.cl/" class="spip_out" rel="external">Université catholique del Maule</a> (<i>Universidad catolica del Maule</i>)</li>
<li><a href="http://www.ucn.cl/" class="spip_out" rel="external">Université catholique du nord</a> (<i>Universidad catolica del Norte</i>)</li>
<li><a href="http://www.uantof.cl/" class="spip_out" rel="external">Université d’Antofagasta</a> (<i>Universidad de Antofagasta</i>)</li>
<li><a href="http://www.uda.cl/" class="spip_out" rel="external">Université de l’Atacama</a> (<i>Universidad de Atacama</i>)</li>
<li><a href="http://www.udec.cl/" class="spip_out" rel="external">Université de Conception</a> (<i>Universidad de Concepcion Chile</i>)</li>
<li><a href="http://www.userena.cl/" class="spip_out" rel="external">Université de la Serena</a> (<i>Universidad de La Serena</i>)</li>
<li><a href="http://www.ufro.cl/" class="spip_out" rel="external">Université de la Frontera</a> (<i>Universidad de la Frontera</i>)</li>
<li><a href="http://www.ulagos.cl/" class="spip_out" rel="external">Université des Lacs</a> (<i>Universidad de los Lagos</i>)</li>
<li><a href="http://www.umag.cl/" class="spip_out" rel="external">Université magellane</a> (<i>Universidad de Magallanes - Chile</i>)</li>
<li><a href="http://www.upla.cl/inicio/" class="spip_out" rel="external">Université de Playa Ancha</a> (<i>Universidad de Playa Ancha</i>)</li>
<li><a href="http://www.utalca.cl/" class="spip_out" rel="external">Université de Talca</a> (<i>Universidad de Talca</i>)</li>
<li><a href="http://www.uta.cl/" class="spip_out" rel="external">Université de Tarapaca</a> (<i>Universidad de Tarapaca de Arica</i>)</li>
<li><a href="http://www.uv.cl/" class="spip_out" rel="external">Université de Valparaiso</a> (<i>Universidad de Valparaiso - Chile</i>)</li>
<li><a href="http://www.ubiobio.cl/" class="spip_out" rel="external">Université du Bio-Bio</a> (<i>Universidad del Bio-Bio</i>)</li>
<li><a href="http://www.umce.cl/" class="spip_out" rel="external">Université métropolitaine des sciences de l’éducation</a> (<i>Universidad metropolitana de Ciencias de la education</i>)</li>
<li><a href="http://www.utfsm.cl/" class="spip_out" rel="external">Université technique Sainte-Marie</a> (<i>Universidad tecnica Federico Santa Maria</i>)</li>
<li><a href="http://www.utem.cl/" class="spip_out" rel="external">Université technologique métropolitaine</a> (<i>Universidad tecnologica metropolitana</i>)</li>
<li><a href="http://www.academia.cl/" class="spip_out" rel="external">Universidad Academia de Humanismo Cristiano</a></li>
<li><a href="http://www.uai.cl/" class="spip_out" rel="external">Université Adolfo Ibanez</a> (<i>Universidad Adolfo Ibanez</i>)</li>
<li><a href="http://www.uahurtado.cl/" class="spip_out" rel="external">Université Alberto Hurtado</a> (<i>Universidad Alberto Hurtado</i>)</li>
<li><a href="http://www.unab.cl/" class="spip_out" rel="external">Université Andrés Bello</a> (<i>Universidad Andrés Bello</i>)</li>
<li><a href="http://www.uas.cl/" class="spip_out" rel="external">Université autonome du Chili</a> (<i>Universidad autonoma de Chile</i>)</li>
<li><a href="http://www.ubohiggins.cl/" class="spip_out" rel="external">Université Bernardo O’Higgins</a> (<i>Universidad Bernardo O’Higgins</i>)</li>
<li><a href="http://www.ubolivariana.cl/" class="spip_out" rel="external">Universidad bolivariana</a></li>
<li><a href="http://www.ucsh.cl/portada/" class="spip_out" rel="external">Université catholique Silva Henriquez</a> (<i>Universidad catolica Silva Henriquez</i>)</li>
<li><a href="http://www.ucentral.cl/" class="spip_out" rel="external">Université centrale du Chili</a> (<i>Universidad central de Chile</i>)</li>
<li><a href="http://www.uniacc.cl/" class="spip_out" rel="external">Université des Arts, des Sciences et de la Communication - UNIACC</a> (<i>Universidad de Artes, Ciencias et Comunicacion</i>)</li>
<li><a href="http://www.ucinf.cl/" class="spip_out" rel="external">Université des Sciences de l’informatique</a> (<i>Universidad de Ciencias de la Informatica</i>)</li>
<li><a href="http://www.udla.cl" class="spip_out" rel="external">Université des Amériques</a> (<i>Universidad de las Américas</i>)</li>
<li><a href="http://www.uandes.cl/" class="spip_out" rel="external">Université des Andes</a> (<i>Universidad de los Andes</i>)</li>
<li><a href="http://www.uvm.cl/" class="spip_out" rel="external">Université de Viña del Mar</a> (<i>Universidad de Viña del Mar</i>)</li>
<li><a href="http://www.udd.cl/" class="spip_out" rel="external">Universidad del desarrollo</a></li>
<li><a href="http://www.upacifico.cl/" class="spip_out" rel="external">Université du Pacifique</a> (<i>Universidad del Pacifico</i>)</li>
<li><a href="http://www.udp.cl/" class="spip_out" rel="external">Université Diego Portales</a> (<i>Universidad Diego Portales</i>)</li>
<li><a href="http://www.finisterrae.cl/" class="spip_out" rel="external">Université Finis Terrae</a> (<i>Universidad Finis Terrae</i>)</li>
<li><a href="http://www.ugm.cl/" class="spip_out" rel="external">Université Gabriela Mistral</a> (<i>Universidad Gabriela Mistral</i>)</li>
<li><a href="http://www.unicit.cl/" class="spip_out" rel="external">Université hispano-américaine des sciences et de technologie</a> (<i>Universidad Iberoamericana de Ciencias et Tecnologia</i>)</li>
<li><a href="http://www.uisek.cl/" class="spip_out" rel="external">Université internationale SEK</a> (<i>Universidad internacional SEK</i>)</li>
<li><strong>Universidad La Republica</strong></li>
<li><a href="http://www.umayor.cl/" class="spip_out" rel="external">Universidad Mayor</a></li>
<li><a href="http://www.upv.cl/" class="spip_out" rel="external">Université Pedro de Valdivia</a> (<i>Universidad Pedro de Valdivia</i>)</li>
<li><a href="http://www.umaritima.cl/" class="spip_out" rel="external">Université maritime du Chili</a> (<i>Universidad Maritima de Chile</i>)</li>
<li><a href="http://www.uss.cl/" class="spip_out" rel="external">Université Saint-Sébastien</a> (<i>Universidad San Sebastian</i>)</li>
<li><a href="http://www.ust.cl/" class="spip_out" rel="external">Université Saint-Thomas</a> (<i>Universidad Santo Tomas</i>)</li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/vie-pratique/article/scolarisation-109763). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
