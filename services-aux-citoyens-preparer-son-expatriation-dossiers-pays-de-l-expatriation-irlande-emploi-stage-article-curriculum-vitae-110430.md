# Curriculum vitae

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/emploi-stage/article/curriculum-vitae-110430#sommaire_1">Rédaction</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/emploi-stage/article/curriculum-vitae-110430#sommaire_2">Diplômes</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/emploi-stage/article/curriculum-vitae-110430#sommaire_3">Modèles de CV</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Rédaction</h3>
<p><strong>Le CV doit être traduit en anglais, dactylographié et d’une présentation très soignée</strong>. Il sera détaillé tout en restant sobre. Le CV doit mentionner les références, c’est-à-dire le nom et les coordonnées des employeurs ou de toute personne susceptible d’apporter un témoignage sur les réalisations et les compétences professionnelles du candidat. Ce point n’est pas à négliger dans la mesure où les employeurs irlandais vérifient régulièrement les informations contenues dans les CV.</p>
<p>S’il n’existe pas de règle générale quant à la longueur du CV, <strong>il est conseillé de se limiter à deux pages A4.</strong></p>
<p>Après mention du nom et des coordonnées ou "Personal Details", placés au centre et en haut du document, le CV est organisé en rubriques :</p>
<ul class="spip">
<li><strong>une section "Education" </strong>relative à la formation scolaire et universitaire ;</li>
<li><strong>une rubrique "Employment History"</strong> ou "Work Experience" présentant de la plus récente à la plus ancienne les différentes expériences et décrivant avec précision les tâches réalisées et les résultats obtenus. Autant que possible, il convient d’éviter les vides chronologiques ;</li>
<li>les compétences informatiques et linguistiques sont précisées sous <strong>la rubrique "Languages and Other Skills"</strong>, qui doit laisser apparaître le niveau de pratique pour chaque langue ou environnement informatique ;</li>
<li><strong>une rubrique "Additional Information" ou "Interests"</strong> peut mettre en valeur des expériences complémentaires, susceptibles d’attirer l’attention de l’employeur dans la perspective du poste recherché ; certaines activités associatives peuvent par exemple constituer un plus ;</li>
<li>enfin, il est important d’ajouter les coordonnées de deux personnes pouvant apporter leurs recommandations sous <strong>la rubrique "References" ou "Referees"</strong> ; une mention indiquant que les références sont disponibles sur demande sera inscrite lorsqu’on ne souhaite pas faire figurer directement leur identité sur le CV, écrire par exemple "References available on request".</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Diplômes</h3>
<p>La libre circulation des personnes et des services constitue un objectif fondamental de la Communauté européenne. Pour les ressortissants communautaires, celle-ci comporte notamment la faculté d’exercer une profession, à titre indépendant ou salarié, dans un autre Etat membre que celui où ils ont acquis leurs qualifications professionnelles. Par conséquent, une réglementation au niveau européen a été envisagée et vise à faciliter la reconnaissance des diplômes et des qualifications professionnelles en vertu desquelles les particuliers peuvent exercer une profession spécifique.</p>
<p>Pour un nombre limité de professions (architecte, sage-femme, pharmacien, médecin, infirmier, dentiste et vétérinaire), des directives sectorielles instituent un système de reconnaissance automatique des diplômes.</p>
<p>Pour les autres professions réglementées et en fonction du niveau de formation postsecondaire attesté par le diplôme, la reconnaissance des qualifications professionnelles opère conformément à un système général de reconnaissance, réglementé par deux directives générales modifiées par la directive 2001/19/CE.</p>
<p>La reconnaissance vise le diplôme, certificat, titre ou ensemble de titres qui sanctionne une formation professionnelle complète et permet d’exercer la profession dans l’Etat de provenance. Vous devez adresser une demande de reconnaissance à l’autorité compétente dans l’Etat membre d’accueil qui devra examiner individuellement votre cas.</p>
<p><a href="http://www.enic-naric.net/" class="spip_out" rel="external">ENIC-NARIC</a> (<i>European Network of Information Centres - National Academic Recognition Centres</i>)<br class="manualbr">NQAI (<i>National Qualifications Authority of Ireland</i>)<br class="manualbr">5th Floor Jervis House - Jervis Street - Dublin 1<br class="manualbr">Tél : (353) 1 887 15 00 - Fax : (353) 1 887 15 95 <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/emploi-stage/article/curriculum-vitae-110430#info#mc#qualificationsrecognition.ie#" title="info..åt..qualificationsrecognition.ie" onclick="location.href=mc_lancerlien('info','qualificationsrecognition.ie'); return false;" class="spip_mail">Courriel</a></p>
<p>Lors de l’examen d’une demande, l’autorité nationale compétente prendra en compte plusieurs éléments et vérifiera que : la profession réglementée que vous souhaitez exercer en Irlande est la même que celle pour laquelle vous êtes pleinement qualifié dans votre Etat de provenance, la durée et le contenu de votre formation ne présentent pas de différences substantielles par rapport à la durée et au contenu de la formation requise en Irlande.</p>
<p>L’autorité compétente dispose de quatre mois pour traiter une demande et prendre une décision : reconnaissance des qualifications, subordination de la reconnaissance à une mesure de compensation ou rejet de la demande.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li>Voir notre rubrique thématique <a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures.md" class="spip_in">Etudes supérieures</a></li>
<li><a href="http://www.qualificationsrecognition.ie/" class="spip_out" rel="external">Qualificationsrecognition.ie</a></li></ul>
<p><i>Source : site de la Commission européenne</i></p>
<h3 class="spip"><a id="sommaire_3"></a>Modèles de CV</h3>
<p>Plusieurs guides peuvent vous aider à rédiger votre CV en anglais. Vous pouvez notamment consulter :</p>
<ul class="spip">
<li><i>Le Guide du CV en anglais</i>, collectif, éd. L’étudiant pratique, coll. Premier emploi, 2007 ;</li>
<li><i>Guide du CV et de l’entretien d’embauche en anglais</i>, Elisabeth BLANCHET, éd. Express prélude et fugue, coll. Guides l’express, 2007</li></ul>
<h4 class="spip">CV Europass</h4>
<p>Vous avez également la possibilité de créer un <a href="http://www.europass.cedefop.europa.eu/" class="spip_out" rel="external">CV Europass</a>. Etabli par décision du Parlement européen et du Conseil, il a été élaboré en collaboration avec les différents gouvernements de l’Union européenne, des syndicats ainsi que des employeurs, et permet de favoriser la mobilité professionnelle des ressortissants européens à l’intérieur de l’Union européenne.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/irlande/emploi-stage/article/curriculum-vitae-110430). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
