# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/emploi-stage/article/reglementation-du-travail#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/emploi-stage/article/reglementation-du-travail#sommaire_2">Contrat de travail – spécificités</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/emploi-stage/article/reglementation-du-travail#sommaire_3">Fêtes légales</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<h4 class="spip">Durée du travail </h4>
<p>Le temps de travail hebdomadaire est fixé par la loi à <strong>43 heures</strong> (certains accords sectoriels le font passer à 45 heures).</p>
<p>La journée de travail maximum est fixée à <strong>huit heures nettes</strong> plus deux arrêts de 15 minutes et 30 minutes pour déjeuner, soit neuf heures brutes.</p>
<h4 class="spip">Heures supplémentaires</h4>
<p>Les heures supplémentaires sont strictement encadrées par la loi afin d’en limiter le nombre. Le principe général est que les heures sont comptabilisées quotidiennement, donc ne peuvent pas être compensées simplement d’un jour sur l’autre (sauf dans le cas où l’emploi est associé à un décompte mensuel du temps de travail, voir ci-dessous).</p>
<p>La limite d’heures supplémentaires à ne pas franchir est double : un salarié ne peut pas en effectuer plus de <strong>quatre par jour</strong>, et pas plus de <strong>12 dans une même semaine</strong>. Dans ce cas, chaque jour, les deux premières heures sont payées <strong>125%</strong> du salaire horaire, les deux suivantes <strong>150%</strong>.</p>
<p>Dans le cas où les employés optent pour un <strong>décompte mensualisé du temps de travail</strong>, l’employeur peut choisir de remplacer le paiement de ces heures supplémentaires par l’octroi d’un repos compensateur sur la base de <strong>1h15 de repos</strong> pour chacune des deux premières heures supplémentaires quotidiennes, puis <strong>1h30</strong> pour les deux suivantes. Dans cette situation, chaque heure de travail effectuée un <strong>jour de repos ou férié</strong> doit donner lieu à <strong>1h30</strong> de récupération.</p>
<p>Toute dérogation à ce système de décompte doit faire l’objet d’une demande de dérogation auprès du ministère du Travail.</p>
<h4 class="spip">Jours de repos hebdomadaires</h4>
<p>La Loi prévoit 36 heures consécutives (un jour et demi) de repos hebdomadaire, incluant généralement le shabbat (du vendredi soir au samedi soir) mais des variations sont parfois possibles en fonction des accords sectoriels, de la religion du salarié…</p>
<p>Le dimanche, qui correspond au premier jour de la semaine, n’est pas chômé en Israël. De manière générale, tout est fermé durant le <strong>shabbat</strong> : du coucher du soleil le vendredi au coucher du soleil le samedi. Certains commerces rouvrent leurs portes le samedi soir.</p>
<h4 class="spip">Congés</h4>
<h5 class="spip">Congés payés</h5>
<p>Chaque employé a droit à un nombre minimal de jours congés payés, calculé en fonction de son ancienneté au poste qu’il occupe :</p>
<ul class="spip">
<li><strong>14 jours / an</strong> les quatre premières années</li>
<li><strong>16 jours</strong> la 5ème année</li>
<li><strong>18 jours</strong> la 6ème année</li>
<li><strong>21 jours</strong> la 7ème année</li>
<li>A partir de la 8ème année, <strong>un jour / an</strong> supplémentaire par année d’ancienneté jusqu’à un maximum de <strong>28 jours / an</strong>Pour un salarié à temps partiel, le nombre de jours de congés sera calculé au prorata du nombre d’heures de travail.</li></ul>
<p>En théorie, les congés doivent être pris en <strong>une seule période ininterrompue</strong> (sauf cas particuliers), mais il est parfois possible de répartir ces jours sur <strong>deux périodes</strong> en accord avec l’employeur. Chaque période de congés annuels <strong>devra durer sept jours minimum</strong>.</p>
<p>Dans certains secteurs ou entreprises, il existe des conventions collectives qui sont plus généreuses que la loi avec les salariés ; dans le secteur public par exemple, un salarié bénéficiera de 22 jours ouvrables de congés annuels, soit un peu plus que quatre semaines.</p>
<p>Le salarié doit prendre les jours de congés dans l’année où il les a gagnés. Cependant, sous réserve que le salarié utilise au moins sept jours de congés dans l’année où il les a obtenus, l’employeur peut accepter d’en reporter le solde sur les deux années suivantes. Certaines conventions collectives autorisent le cumul des jours de congés jusqu’à un certain plafond (par exemple jusqu’à 50 jours) ; au delà, les jours non utilisés sont perdus.</p>
<p>En fin de contrat, si un salarié n’a pas pris tous ses jours de congés, l’employeur se doit de les lui payer à hauteur du salaire correspondant à une journée de travail normale par jour de congé non pris. Le contrat de travail ou la convention collective dont dépend le salarié peuvent cependant encadrer plus spécifiquement cette pratique.</p>
<p>Les jours de congés annuels ne prennent pas en compte les jours de réserves militaires, les jours fériés, les congés de maternité, les arrêts de maladie ou d’accidents du travail, les jours de deuil familial et les jours de grève.</p>
<h5 class="spip">Congés maladie</h5>
<p>Chaque salarié a droit à 18 jours par an de congés maladie payés. Si le salarié n’a pas utilisé tous ses droits à congés en fin d’année, ceux-ci sont reportés sur l’année suivante, dans la limite de 90 jours cumulés au total.</p>
<p>Les jours fériés ou de repos hebdomadaires inclus dans une période continue de congés maladie <strong>sont décomptés du maximum des 18 jours annuels comme les jours ouvrés</strong>.</p>
<p>Le salaire journalier est versé comme suit :</p>
<ul class="spip">
<li>0 % du salaire journalier le premier jour d’interruption de travail</li>
<li>37,5 % les deuxième et troisième jours</li>
<li>75 % à partir du quatrième jourJusqu’à 6 des 18 jours annuels auxquels un salarié a droit peuvent être utilisé en cas <strong>d’enfant de moins de 16 ans malade</strong>, à condition que le salarié prouve que son conjoint travaille également et n’a pas bénéficié de cette disposition. Les mêmes dispositions existent en cas de maladie du <strong>conjoint du salarié</strong>, ou encore d’un <strong>ascendant direct de plus de 65 ans</strong> du salarié ou de son conjoint.</li></ul>
<h5 class="spip">Congé de maternité</h5>
<p>La durée légale du congé de maternité est de 14 semaines, six de ces semaines peuvent être prises avant l’accouchement.</p>
<h3 class="spip"><a id="sommaire_2"></a>Contrat de travail – spécificités</h3>
<h4 class="spip">Période d’essai et préavis</h4>
<p>La période d’essai est généralement de trois mois, mais peut être portée à six mois de manière contractuelle.</p>
<p>En cas de licenciement, le préavis légal est d’un mois.</p>
<h3 class="spip"><a id="sommaire_3"></a>Fêtes légales</h3>
<p>Les grandes fêtes sont chômées, le repos associé est de 36 heures pour chaque fête. Les jours de repos associés à ces fêtes ne peuvent pas remplacer le repos obligatoire hebdomadaire, ils s’y ajoutent (compenser un jour férié par un week-end travaillé est interdit).</p>
<p><strong>A noter</strong> : dans certaines conditions, les employés qui ne sont pas juifs peuvent être autorisés sur demande à suivre les jours de congés de leur religion en lieu et places des jours fériés nationaux basés sur les fêtes juives.</p>
<p><strong>Source </strong> : <a href="http://www.ambafrance-il.org/-Accueil-Francais-.html" class="spip_out" rel="external">Ambassade de France en Israël</a>, <a href="http://www.israelvalley.com/" class="spip_out" rel="external">CCFI</a> (dont extrait du “Guide des stages et emplois en Israël” de Jacques Bendelac - Edition IsraelValley - 2008), <a href="http://economy.gov.il/English/Pages/default.aspx" class="spip_out" rel="external">ministère de l’Industrie, du Commerce et du Travail</a>, <a href="http://www.cleiss.fr/docs/regimes/regime_israel.html" class="spip_out" rel="external">CLEISS</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/emploi-stage/article/reglementation-du-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
