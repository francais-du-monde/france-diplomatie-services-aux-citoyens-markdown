# Vaccination

<p>Aucune vaccination n’est exigée à l’entrée dans le pays. Sont recommandées avant le départ : la mise à jour de la vaccination contre la diphtérie, le tétanos et la poliomyélite ; les vaccinations contre l’hépatite A et, en cas de séjour long et/ou à risque, contre hépatite B et la typhoïde. Pour les enfants, les vaccinations recommandées par le ministère de la Santé français doivent être à jour, en particulier, en cas de long séjour, le B.C.G. dès le premier mois et le vaccin contre la rougeole dès l’âge de neuf mois.</p>
<p>Pour en savoir plus :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.cimed.org/" class="spip_out" rel="external">Cimed</a></p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/entree-et-sejour/article/vaccination-110335). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
