# Vaccination

<p>Aucune vaccination n’est exigée à l’entrée du pays.</p>
<p><strong>Vaccinations recommandées d’un point de vue médical</strong></p>
<p>Systématiquement :</p>
<ul class="spip">
<li>diphtérie,</li>
<li>tétanos,</li>
<li>poliomyélite : à mettre à jour ;</li>
<li>hépatite A pour les enfants à partir de l’âge de un an.</li></ul>
<p>Pour des séjours prolongés et/ou à risques (conditions d’hygiène précaires) :</p>
<ul class="spip">
<li>typhoïde : pour les enfants à partir de l’âge de deux ans ;</li>
<li>hépatite B ;</li>
<li>rage (situation d’isolement ou accès au traitement sur place difficile).</li></ul>
<p>Pour les enfants, toutes les vaccinations incluses dans le calendrier vaccinal français devront également être à jour. Dans le cas d’un long séjour, le BCG est recommandé dès le premier mois et le vaccin contre la rougeole-oreillons-rubéole dès l’âge de neuf mois.</p>
<p><strong>Il est recommandé de réaliser toutes les vaccinations nécessaires avant de partir.</strong></p>
<p>Pour en savoir plus :</p>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-sante.md" class="spip_in">notre article thématique sur la vaccination</a></li>
<li>site internet du <a href="http://www.cimed.org/" class="spip_out" rel="external">CIMED</a></li></ul>
<p><i>Mise à jour : juillet 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/entree-et-sejour-22781/article/vaccination-109748). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
