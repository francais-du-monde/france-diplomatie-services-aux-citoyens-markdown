# Déménagement

<p>Il faut s’adresser à un transitaire pour faciliter les démarches de dédouanement. De nombreux transporteurs français travaillent avec le Maroc. La voie routière constitue le moyen de transport le moins onéreux, le plus rapide et le plus simple pour le dédouanement.</p>
<p>Le dédouanement se fait aux postes frontières (Tanger-Ceuta), il est possible de l’effectuer à Rabat (fret aérien) et à Casablanca (transport maritime).</p>
<p><strong>Par voie routière, compter de 7 à 15 jours de transport (entre 200 et 250 € le m3), un mois par voie maritime (environ 6 000 € pour un container de 30 m3 en fonction du volume et du conditionnement).</strong></p>
<p>Dans le cadre d’un transfert de résidence au Maroc, l’importation d’effets et de biens personnels peut se faire en franchise, c’est à dire sans paiement des droits et taxes normalement exigibles. Le changement de résidence autorise l’importation de mobilier et d’effets personnels en cours d’usage.</p>
<p>Le bénéfice de la franchise est subordonné à la présentation au service douanier :</p>
<ul class="spip">
<li>d’un inventaire détaillé des objets importés, daté et signé par la personne concernée,</li>
<li>d’un certificat de changement de résidence établi par l’autorité municipale du lieu de départ ou par le Consulat du Maroc du ressort de l’ancienne résidence, ou de tout autre document établissant le changement de résidence, présenté à la satisfaction des services douaniers (contrat de travail par exemple),</li>
<li>une attestation sur l’honneur. A produire dans le cas où un seul des conjoints d’une famille établie à l’étranger rentre définitivement au Maroc.</li></ul>
<p>La liste des objets entrant dans le champ d’application du régime de franchise est consultable sur le site des <a href="http://www.douane.gov.ma/" class="spip_out" rel="external">douanes marocaines</a> ainsi que les objets faisant l’objet d’exclusion.</p>
<p>A noter que l’importation du mobilier et le changement de résidence devront être simultanés, les objets et les effets mobiliers devront être importés en une seule fois.</p>
<p>Les véhicules sont exclus de cette franchise. Ils doivent être soumis au paiement de droits et de taxes dont les taux sont élevés (il est d’ailleurs possible de calculer les droits et taxes pour le dédouanement des véhicules sur le site des douanes marocaines). Le dédouanement des véhicules automobiles et motocycles à deux roues d’une cylindrée supérieure à 80cm3 acquis à l’étranger par des résidants au Maroc (marocains ou étrangers) est effectué obligatoirement au bureau des douanes d’entrée. Le dédouanement définitif peut être effectué trois mois après l’entrée au Maroc.</p>
<p>S’agissant d’un déménagement international, il est primordial de s’entourer d’un maximum de garanties en faisant appel à un professionnel disposant de certifications reconnues (marque NF Service, ISO 9002, etc.)</p>
<p><a href="http://www.csdemenagement.fr/" class="spip_out" rel="external">Chambre syndicale du déménagement</a><br class="manualbr">Téléphone : 01 49 88 61 40 - Télécopie : 01 49 88 61 46<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/entree-et-sejour/article/demenagement-110836#contact#mc#csdemenagement.fr#" title="contact..åt..csdemenagement.fr" onclick="location.href=mc_lancerlien('contact','csdemenagement.fr'); return false;" class="spip_mail">Courriel</a></p>
<p><i>Mise à jour : janvier 2016</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/entree-et-sejour/article/demenagement-110836). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
