# Santé

<p>La qualité des soins est très bonne en Nouvelle-Zélande. Tous les types de soins y sont disponibles, néanmoins le niveau des médecins est très variable. La pédiatrie est inexistante en dehors des hôpitaux.</p>
<p>En cabinet, la consultation d’un généraliste peut coûter entre une dizaine et une centaine de dollars (soit jusqu’à environ 60€). Les tarifs sont souvent parmi les moins élevés pour les personnes de moins de 25 ans et pour les patients qui ont déjà déclaré un médecin généraliste auprès du ministère de la santé. Ils sont plus élevés pour les personnes qui n’ont pas la nationalité néo-zélandaise. Un simple examen chez le dentiste coûte environ 65 dollars néo-zélandais (autour de 40€). Le passage par un généraliste est le plus souvent obligatoire pour accéder au spécialiste.</p>
<p>Pour bénéficier du système de protection sociale néo-zélandais (de la gratuité ou de l’aide publique en matière de soins), il convient d’être résident permanent ou être titulaire d’un permis de travail depuis deux ans au moment de la demande et de répondre aux critères d’affiliation édictés par le gouvernement.</p>
<p>En général, la plupart des visiteurs en Nouvelle-Zélande <strong>ne peut pas avoir accès au service public de santé</strong>. Le <strong>recours à une assurance médicale privée </strong>(sur place ou via une assurance pour expatriés correspondant à leur situation) s’avère donc nécessaire pour la prise en charge des soins.</p>
<p>Le site du Ministère de la santé local vous permettra de vérifier votre éligibilité aux services de soins subventionnés par le gouvernement néo-zélandais.</p>
<p>Dans le public, les délais d’attente pour les soins non-urgents peuvent être assez longs et certains ont recours au secteur privé pour des délais accélérés de prise en charge.</p>
<p>Si vous partez en NZ dans le cadre d’un PVT, il est nécessaire de fournir une attestation d’assurance santé privée qui vous couvrira pendant l’intégralité de votre séjour dans le pays. Si vous planifiez de rester plus de 24 mois en Nouvelle-Zélande, vous devrez présenter un certificat médical et une radiographie des poumons.</p>
<p>Pour obtenir des informations (en anglais) sur le fonctionnement du système de santé néo-zélandais et connaître vos droits :</p>
<ul class="spip">
<li>Site du <a href="http://www.health.govt.nz/new-zealand-health-system/eligibility-publicly-funded-health-services/guide-eligibility-publicly-funded-health-services-0" class="spip_out" rel="external">ministère de la santé</a>.</li>
<li><a href="http://www.acc.co.nz/index.htm" class="spip_out" rel="external">ACC - Accident Compensation Corporation</a></li></ul>
<p>Pour une présentation des risques épidémiologiques ainsi que des conseils sur la médecine au quotidien, consultez les pages suivantes :</p>
<ul class="spip">
<li>Conseils, liste de médecins et hôpitaux sur les <a href="http://www.ambafrance-nz.org/-Conseils-voyageurs-residents-" class="spip_out" rel="external">pages du consulat de France</a>.</li>
<li>Page dédiée à la santé en Nouvelle-Zélande sur le site <a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/nouvelle-zelande/" class="spip_in">Conseils aux voyageurs</a>.</li>
<li>Fiche Wellington sur le site du <a href="http://www.cimed.org" class="spip_out" rel="external">CIMED</a>.</li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/nouvelle-zelande/vie-pratique/article/sante). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
