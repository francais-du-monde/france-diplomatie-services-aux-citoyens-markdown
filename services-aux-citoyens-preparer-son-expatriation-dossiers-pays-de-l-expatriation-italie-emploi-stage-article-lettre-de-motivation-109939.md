# Lettre de motivation

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/emploi-stage/article/lettre-de-motivation-109939#sommaire_1">Rédaction</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/emploi-stage/article/lettre-de-motivation-109939#sommaire_2">Modèles de lettre de motivation</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Rédaction</h3>
<p>Les lettres de motivation sont généralement dactylographiées. Il s’agira plutôt d’une lettre de présentation en accompagnement du curriculum vitae (CV) et non d’une lettre de motivation au sens propre du terme.</p>
<p>Il est conseillé d’élaborer une lettre "à la française", en faisant ressortir ses compétences et aptitudes, ce qui est généralement bien perçu par des recruteurs italiens qui n’en ont que peu l’habitude chez leurs compatriotes. Le candidat doit se vendre, mais de façon souple, pour ne pas paraître trop entreprenant.</p>
<p>Vérifiez le nom du destinataire. Plus que le DRH qui souvent n’existe pas, adressez votre candidature au responsable de la division susceptible de vous embaucher (directeur commercial, financier, logistique …) en indiquant son titre <i>Dott.</i>, <i>Ing.</i>,… Dans le doute, <i>Dott.</i> est toujours bienvenu.</p>
<p>N’oubliez pas que de nombreuses entreprises, surtout celles susceptibles d’embaucher, sont des PME/PMI, voire des TPE (très petites entreprises) de moins de 20 personnes. Il sera bon de montrer que vous connaissez bien l’entreprise et son environnement.</p>
<h3 class="spip"><a id="sommaire_2"></a>Modèles de lettre de motivation</h3>
<p><strong>Des modèles</strong> de <i>lettere di accompagnamento </i>sont proposés sur les sites suivants :</p>
<ul class="spip">
<li>Site <a href="http://www.cambiolavoro.com" class="spip_out" rel="external">Cambiolavoro</a></li>
<li>Site <a href="http://www.cv-resume.org/curriculum/" class="spip_out" rel="external">CV-resume.org</a></li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/italie/emploi-stage/article/lettre-de-motivation-109939). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
