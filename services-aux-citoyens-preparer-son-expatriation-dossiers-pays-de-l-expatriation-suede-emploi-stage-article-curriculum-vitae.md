# Curriculum vitae

<h4 class="spip">Rédaction</h4>
<p>Les candidatures spontanées sont assez courantes en Suède et il peut s’avérer utile de tenter cette approche. Avant d’envoyer votre candidature, téléphonez au responsable des ressources humaines ou à un autre cadre moyen ou supérieur de l’entreprise. La candidature spontanée vous permet de vous faire connaître, de mettre en évidence votre sens de l’initiative et de vous aider à savoir quel type de profil pourrait intéresser l’entreprise. Pensez à préparer les questions ou les points particuliers que vous pourrez aborder. Les adresses des entreprises sélectionnées figurent dans les <a href="http://www.eniro.se/" class="spip_out" rel="external">Pages jaunes</a> de l’annuaire téléphonique de Suède.</p>
<p><i>Source : <a href="http://www.ccfs.se/" class="spip_out" rel="external">Chambre de commerce française en Suède</a></i></p>
<p>Lettre de motivation et curriculum vitae (CV) doivent être rédigés en suédois ou en anglais.</p>
<p>Le CV doit être bref (une page), sa présentation soignée. Il n’est pas nécessaire de joindre vos diplômes ou votre photo.</p>
<p>Le CV suédois mentionne généralement les éléments suivants :</p>
<ul class="spip">
<li><strong>Titre / Heading : </strong>Curriculum Vitae or Meritförteckning ;</li>
<li><strong>Informations Personnelles / Personal Information </strong> : nom, prénom, adresse, téléphone, e-mail, situation de famille ;</li>
<li><strong>Expérience Professionnelle / Professional Experience (Arbetserfarenhet) : </strong>nom de la compagnie et secteur d’activité, titre du poste occupé, dates, brève description des tâches ;</li>
<li><strong>Etudes, Formations / Studies (Utbildning) : </strong>cursus formel, cours divers, formations, nom de l’établissement, dates, diplôme obtenu</li>
<li><strong>Langues Etrangères / Languages (Språk) : </strong>indiquer votre niveau (pensez à indiquer votre niveau selon la classification harmonisée européenne)</li>
<li><a href="http://europass.cedefop.europa.eu/fr/resources/european-language-levels-cefr" class="spip_url spip_out auto" rel="nofollow external">http://europass.cedefop.europa.eu/fr/resources/european-language-levels-cefr</a></li>
<li><strong>Compétences Informatiques / Computer Skills (Dator erfarenhet / färdighet eller datorkunskaper)</strong></li></ul>
<p>Les informations relatives à la formation et à l’expérience professionnelle doivent être datées et présentées en ordre chronologique inverse (les plus récentes en premier).</p>
<h4 class="spip">Modèles de CV</h4>
<p>Pour rédiger votre CV, vous trouverez de nombreux modèles sur des sites spécialisés dans le travail à mobilité géographique internationale, comme iAgora et Europass, et des modèles plus particuliers à la Suède sur le site Internet du travail suédois</p>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li><a href="http://www.babla.fr/phrases/candidature/cv/francais-suedois/" class="spip_out" rel="external">Quelques formules communes sur le CV</a>
Le site de l’Agence suédoise pour l’emploi propose également des modèles et ateliers de CV.</li>
<li><a href="http://www.iagora.com/work/en/resumes/chrono_model_sweden.html" class="spip_out" rel="external">Modèles de CV</a></li>
<li><a href="http://work.sweden.se/how-to-find-a-job/cv-cover-letter-and-interview/" class="spip_out" rel="external">Quelques conseils sur le CV, la lettre de motivation et l’entretien d’embauche</a></li>
<li><a href="http://work.sweden.se/" class="spip_out" rel="external">Le site du travail en Suède vous sera utile</a></li>
<li><a href="http://www.arbetsformedlingen.se/Globalmeny/Other-languages/Languages/Francais-franska.html" class="spip_out" rel="external">Le site de l’Agence pour l’emploi de Suède, en français</a></li>
<li><a href="http://europass.cedefop.europa.eu/fr/home" class="spip_out" rel="external">Europass</a>, pour les professionnels de l’UE</li></ul>
<p>Les sites Internet du Conseil des Universités et des Grandes écoles ainsi que celui de la Chancellerie des Universités pourront vous renseigner quant aux équivalences de diplômes.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.uhr.se/" class="spip_out" rel="external">The Swedish Council for Higher Education</a> <br class="manualbr">Tél : + 46 10 470 03 00<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/emploi-stage/article/curriculum-vitae#kontaktpunkt#mc#uhr.se#" title="kontaktpunkt..åt..uhr.se" onclick="location.href=mc_lancerlien('kontaktpunkt','uhr.se'); return false;" class="spip_mail">Courriel</a> </p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/emploi-stage/article/curriculum-vitae). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
