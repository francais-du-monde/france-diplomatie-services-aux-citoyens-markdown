# Scolarisation

<p>Pour toute information sur la scolarisation dans le système français à l’étranger, vous pouvez consulter notre thématique sur les <a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures.md" class="spip_in">Etudes supérieures</a> et la <a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais.md" class="spip_in">Scolarité en français</a>.</p>
<p>Vous y trouverez des renseignements sur :</p>
<ul class="spip">
<li>les établissements français du primaire et du secondaire à l’étranger ;</li>
<li>les bourses scolaires à l’étranger ;</li>
<li>les possibilités qui s’offrent à vous si votre enfant ne peut être scolarisé à l’étranger dans le système français (enseignement à distance par le CNED, programme français langue maternelle (FLAM), internats en France) ;</li>
<li>les épreuves du baccalauréat à l’étranger ;</li>
<li>les bourses d’études supérieures en France et à l’étranger ;</li>
<li>l’équivalence des diplômes.</li></ul>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.aefe.fr/" class="spip_out" rel="external">consultez le site de l’Agence pour l’enseignement français à l’étranger</a>.</p>
<h4 class="spip">L’enseignement français à Maurice</h4>
<p>Outre les trois établissements conventionnés avec l’AEFE <a href="http://www.aefe.fr/" class="spip_out" rel="external">Agence pour l’enseignement français à l’étranger</a> (l’école-collège-lycée <a href="http://www.lyceelabourdonnais.org/" class="spip_out" rel="external">La Bourdonnais</a> à Curepipe (centre),  le <a href="http://www.lyceedesmascareignes.org/" class="spip_out" rel="external">Lycée des Mascareignes</a> à Moka (centre) et l’<a href="http://www.ecoledunord.net/" class="spip_out" rel="external">école-collège du Nord</a>) et deux établissements homologués (<a href="http://www.ecoleducentre.org/" class="spip_out" rel="external">école du Centre – collège Pierre Poivre</a> et <a href="http://www.ecolepauletvirginie.com/" class="spip_out" rel="external">école maternelle et élémentaire Paul et Virginie</a>, à Tamarin, dans l’ouest) on relèvera essentiellement cinq établissements qui délivrent des formations en Français et des diplômes français ou reconnus par le ministère français de l’Education, aux niveaux secondaires et supérieur :</p>
<h4 class="spip">Au niveau secondaire technique et professionnel</h4>
<p>Le lycée de Flacq prépare, en trois ans, près de 400 élèves à quatre <strong>brevets de technicien</strong> : mécanique automobile, fabrication et maintenance mécanique, électrotechnique et électronique, bâtiment.</p>
<h4 class="spip">Au niveau supérieur</h4>
<p><strong>Université des Mascareignes</strong></p>
<p><a href="https://www.udm.ac.mu/" class="spip_out" rel="external">L’Université des Mascareignes</a> propose une trentaine de formations construites principalement en collaboration avec l’Université de Limoges qui répondent aux standards européens du LMD et permettent aux étudiants d’obtenir à leur sortie un double diplôme français et mauricien. L’UDM est la seule université mauricienne à proposer un tel schéma de double reconnaissance. L’enseignement est dispensé en anglais et en français.</p>
<p>Spécialisée en ingénierie, technologie et management, l’UDM comprend trois facultés et sept départements :<br class="autobr"> - Ingénierie et développement durable (génie civil, génie électrique et informatique industrielle, génie électrique et automatisme) ;</p>
<ul class="spip">
<li>Technologie de l’information et de la communication (informatique appliquée et <i>Sofware engineering</i>) ;</li>
<li>Economie et gestion (<i>Finance and Banking et Management</i>).</li></ul>
<p>Les cursus proposés au niveau master sont orientés vers un enseignement professionnalisant et les activités de recherche vers les services industriels ou commerciaux, en relation étroite avec le secteur privé, les étudiants étant encouragés à s’engager activement dans l’entrepreneuriat.</p>
<p>L’UDM est fondée sur l’interdisciplinarité (cf. création toute récente d’une école de Géopolitique, unique dans la région, en collaboration avec La Sorbonne), l’innovation et les transferts de technologies. Ses priorités sont l’énergie durable, les TIC appliquées à la santé et à l’environnement, les risques, la sécurité des personnes et des biens, l’économie et le management. La création de laboratoires mixtes avec l’industrie, autour de programmes de recherche communs, est encouragée.</p>
<p>L’internalisation de l’UDM, également prioritaire, vise l’accueil de spécialistes étrangers de haut niveau et d’étudiants sélectionnés d’Afrique australe et orientale et de l’Océan indien. L’exigence du double diplôme imposant un niveau reconnu par la communauté internationale, l’accent est mis sur la qualité des recrutements. Chaque étudiant peut choisir son orientation et combiner des unités de cours pour élaborer son meilleur profil professionnel, développer ses responsabilités et faire émerger des compétences professionnelles.</p>
<p><strong>Centre d’études supérieures de la Chambre de commerce et d’industrie de Maurice</strong></p>
<p><a href="http://www.etudes-superieures.org/" class="spip_out" rel="external">Le Centre d’études supérieures de la Chambre de commerce et d’industrie de Maurice</a> prépare 200 étudiants à :</p>
<ul class="spip">
<li>trois brevets de technicien supérieur (BTS), en partenariat avec le Lycée La Bourdonnais et l’académie de la Réunion : assistant de gestion, management des unités commerciales, informatiques de gestion ;</li>
<li>une licence professionnelle <strong>commerce électronique</strong>, avec l’Institut universitaire de technologie de Saint-Pierre de la Réunion ;</li>
<li>une maîtrise de sciences de gestion (MSG) et un diplôme d’études supérieures spécialisées (DESS) – master d’administration des entreprises avec l’Institut d’administration des entreprises de Poitiers.</li></ul>
<p><strong>Sciences Po Aix à Maurice</strong></p>
<p><a href="http://www.lyceelabourdonnais.org/" class="spip_out" rel="external">Sciences Po Aix à Maurice</a> propose, en partenariat avec le lycée La Bourdonnais et la Chambre de commerce et d’industrie de Maurice, une formation post bac diplômante en trois ans qui débouche sur l’attribution du Bachelor d’études politiques de l’Institut d’études politiques d’Aix-en- Provence.</p>
<p><strong>Institut de la Francophonie pour l’entreprenariat</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  L’Institut de la Francophonie pour l’entreprenariat prépare une trentaine d’étudiants à un Diplôme d’études professionnelles approfondies (DEPA) de gestion et création d’entreprises avec l’Université de Bordeaux.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/vie-pratique/article/scolarisation). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
