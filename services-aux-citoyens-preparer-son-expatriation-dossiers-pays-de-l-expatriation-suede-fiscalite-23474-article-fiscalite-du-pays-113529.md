# Fiscalité du pays

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/fiscalite-23474/article/fiscalite-du-pays-113529#sommaire_1">Services au contribuable</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/fiscalite-23474/article/fiscalite-du-pays-113529#sommaire_2">Les services d’accueil</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/fiscalite-23474/article/fiscalite-du-pays-113529#sommaire_3">Les services à distance</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/fiscalite-23474/article/fiscalite-du-pays-113529#sommaire_4">Année fiscale </a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/fiscalite-23474/article/fiscalite-du-pays-113529#sommaire_5">Barème de l’impôt </a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/fiscalite-23474/article/fiscalite-du-pays-113529#sommaire_6">Quitus fiscal </a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Services au contribuable</h3>
<p>Depuis le 1er janvier 2004, c’est l’<a href="http://www.skatteverket.se/servicelankar/otherlanguages/inenglish/moreonskatteverket.4.7856a2b411550b99fb780009630.html" class="spip_out" rel="external">agence nationale des impôts (<i>Skatteverket</i>)</a>, sous tutelle du ministère suédois des Finances (<i>Finansdepartementet</i>) qui est le collecteur quasi-unique des prélèvements obligatoires en Suède (impôts, contributions sociales, taxes…).</p>
<p>Selon nos interlocuteurs locaux, 99% des PO sont collectés par cette agence et le reste par l’agence des transports (taxe sur les véhicules), <i>Transportstyrelsen</i>.</p>
<p>L’agence des impôts est responsable de toute la chaine de missions fiscales qu’ils s’agisse de conseils fiscaux, de la collecte des impôts/taxes, de la gestion des déclarations/fiches d’impôts, des contrôles fiscaux et du recouvrement, hormis le recouvrement forcé qui relève de l’<a href="http://www.kronofogden.se/InEnglish.html" class="spip_out" rel="external">Agence nationale de recouvrement forcé (Kronofogdemyndigheten)</a>, également sous tutelle du ministère suédois des Finances.</p>
<h3 class="spip"><a id="sommaire_2"></a>Les services d’accueil</h3>
<p>Les horaires de réception du grand public sont, de 8h00 à 16h00. Au delà d’une spécialisation du centre des impôts de la capitale, qui a un service réservé exclusivement aux entreprises et qui dispose de personnes particulièrement compétentes pour les secteurs transports et restauration, il convient de souligner que les autres centres des impôts répartis dans le pays n’ont pas de spécialisation particulière, mais assurent un service général et polyvalent.</p>
<h3 class="spip"><a id="sommaire_3"></a>Les services à distance</h3>
<p>Les services téléphoniques automatiques permettent notamment de commander de nombreuses brochures, fiches d’information ou des fiches d’état civil.</p>
<p>Coordonnées téléphoniques (service accessible du lundi au jeudi de 8h à 18h ; le vendredi, de 8h à 16h) :</p>
<p>Pour appeler depuis l’étranger : +46 8 564 851 60 <br class="autobr">Pour appeler depuis la Suède : 0771–567 567</p>
<p>Il est également possible de consulter le site de la <a href="http://www.skatteverket.se/#_blank" class="spip_out" rel="external">direction des impôts</a> – avec certains documents en anglais et français.</p>
<h3 class="spip"><a id="sommaire_4"></a>Année fiscale </h3>
<p>La période de l’année fiscale s’étend <strong>du 1er janvier au 31 décembre</strong>. Les modalités de paiement des impôts sont la <strong>retenue à la source</strong> par l’employeur pour les salariés et le versement provisionnel pour les professions indépendantes. Le paiement est mensuel.</p>
<p>La date limite de remise des déclarations annuelles simplifiées, c’est-à-dire la formule où la plupart des renseignements sont pré-imprimés, est <strong>fixée au 5 Mai</strong>. Pour ce faire, le contribuable reçoit avant la mi-avril la liste de tous les relevés de contrôle par les services fiscaux, le formulaire pré-imprimé de déclaration simplifiée en deux exemplaires, ainsi qu’un calcul préliminaire du montant de l’impôt. Le contribuable, qui est libre de compléter ou de rectifier sa déclaration simplifiée, reçoit par la suite l’avis d’imposition définitive fin août ou début septembre.</p>
<p>Le trop-perçu d’impôt est remboursé à la mi-juin pour les déclarations simplifiées et en novembre pour les autres, par versement sur un compte bancaire indiqué préalablement par le contribuable. En cas de difficulté particulière, les services fiscaux peuvent retarder le versement jusqu’à la mi-décembre.</p>
<p>Il existe également une déclaration de revenus spéciale, destinée en premier lieu aux chefs d’entreprises, associés d’une entreprise, entrepreneurs, etc. Cette déclaration doit être remise fin mars. L’avis d’imposition définitive est ensuite envoyé, et le trop-perçu d’impôt remboursé, vers la mi-décembre.</p>
<h3 class="spip"><a id="sommaire_5"></a>Barème de l’impôt </h3>
<p><strong>L’impôt sur les personnes physiques</strong></p>
<p>On distingue deux catégories de revenus : les revenus provenant d’activités salariées et les revenus des activités non salariées. Le calcul du revenu imposable est effectué séparément pour chaque catégorie, sur la base de l’exercice fiscal (année civile).</p>
<p>L’impôt sur le revenu est pratiquement applicable à tous. Les revenus annuels inférieurs à 18 782 (2014) SEK ne sont pas imposables. Il se compose de deux parties : l’IR communal (<i>den kommunala inkomstskatten</i>) et l’IR national (<i>den statliga inkomstskatten</i>). Le taux moyen de l’impôt communal, qui frappe la totalité du revenu imposable, est de 31,86 %, avec des variations selon le lieu de résidence, entre 29 % au minimum et 35 % au maximum. Par ailleurs, tous les contribuables versent 200 SEK aux autorités fiscales au titre de l’impôt national. De plus, ceux qui perçoivent un revenu brut supérieur à 433 900 SEK par an doivent acquitter un impôt national supplémentaire de 20 % sur la tranche supérieure. Ce taux est porté à 25 %, au-delà de 615 700 SEK.</p>
<p>Au total, le taux de prélèvements obligatoires sur les revenus en Suède varie de 37 à 57% % du revenu imposable. L’employeur procède à une retenue à la source sur salaire ou traitement, en tenant compte des abattements et déductions, et la verse au nom de l’employé à l’administration fiscale. Ces versements anticipés sont à valoir sur l’impôt définitif. Pour les activités indépendantes, l’impôt est versé par avance selon un système d’évaluation prenant pour base le montant final acquitté l’année précédente.</p>
<p>Les membres de l’Eglise suédoise payent également un impôt à l’Eglise (1% en 2014), et tout le monde paye un impôt pour ses obsèques (0,24% en 2014).</p>
<ul class="spip">
<li>Un revenu de 300 000 SEK/an est imposé à environ 24%</li>
<li>Un revenu de 500 000 SEK/an est imposé à environ 31%</li>
<li>Un revenu de 1 000 000 SEK/an est imposé à environ 44%</li></ul>
<p><strong>L’impôt sur le capital</strong></p>
<p>Sont pris en compte pour le calcul de l’impôt sur le capital (<i>kapitalskatten</i>) : les revenus de valeurs mobilières, les dividendes d’actions, les plus-values ou moins-values, les revenus fonciers. Le taux de l’impôt sur les capitaux est de 30 %, sur le montant net imposable.</p>
<p>A noter, par ailleurs, la suppression <strong>de l’impôt sur les héritages et les dons en 2005 et de l’impôt sur la fortune en 2007</strong>.</p>
<p><strong>L’impôt sur la propriété</strong></p>
<p>La taxe foncière a été aboli le 1er Janvier 2008 et remplacé par une redevance municipale plafonnée à environ 780€ par an pour les maisons (0,75% de la valeur imposable) et de 135€ pour les appartements (0,3% de la valeur imposable).</p>
<p>Pour les terrains non construits, la taxe foncière reste une recette de l’État. La valeur de site utilisée pour déterminer cette taxe (de 1%) est estimée tous les quatre ans et correspond en pratique à 70-75% de la valeur de marché.</p>
<p>Le taux d’imposition est de 0,5 % pour les bâtiments industriels et de 1 % pour les locaux commerciaux.</p>
<p><strong>Les impôts indirects</strong></p>
<p>La TVA et connue sous le terme de « moms » (diminutif de <i>mervärdesskatt</i>).</p>
<p>La TVA a un taux normal de 25 % sur les ventes de biens et de services. Depuis 1996, un taux réduit de 12 % est applicable sur les produits alimentaires, les services hôteliers, les campings et les importations d’œuvres artistiques. Un taux plancher de 6 % est perçu sur les journaux, les services de transports de passagers (depuis le 1er janvier 2001) et sur les événements culturels.</p>
<p>Barème de la TVA :</p>
<ul class="spip">
<li>25% (20% du prix) en général</li>
<li>12% (10,71% du prix) sur par exemple l’alimentaire, la restauration ou les nuits d’hôtel</li>
<li>6% (5,66% du prix) sur les journaux quotidiens, les services de transport, les livres, etc</li></ul>
<p>Certains secteurs tels que le secteur médical, dentaire et de l’aide sociale, la vente de biens immobiliers, l’éducation, le système bancaire et les assurances, ainsi que certaines activités culturelles et sportives sont exonérés de TVA.</p>
<p>Il existe en Suède d’autres impôts indirects. L’impôt sur les produits énergétiques, importante recette de l’Etat, incite le consommateur à utiliser des produits moins polluants.</p>
<p>Plusieurs droits de timbre sont levés : le plus élevé est de 1,5 %, prélevé lors de la réalisation de transactions immobilières. La taxe sur les véhicules varie en fonction du poids du véhicule (le taxe est plus élevée pour les voitures diesel). Des droits d’accise particulièrement élevés sont également perçus sur l’alcool (croissant en fonction du degré d’alcool) et sur le tabac.</p>
<h3 class="spip"><a id="sommaire_6"></a>Quitus fiscal </h3>
<p>Un quitus fiscal est exigé avant de quitter le pays.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p>Site sur la fiscalité <a href="http://europa.eu/" class="spip_out" rel="external">Votre Europe</a> (en anglais).</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/fiscalite-23474/article/fiscalite-du-pays-113529). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
