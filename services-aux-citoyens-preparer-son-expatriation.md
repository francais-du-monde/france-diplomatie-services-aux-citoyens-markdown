# Préparer son expatriation

<p class="spip_document_84764 spip_documents spip_documents_center">
<img src="http://www.diplomatie.gouv.fr/fr/IMG/jpg/rubrique_preparer_expatriation_cle05ff78.jpg" width="660" height="140" alt=""></p>
<p>A la recherche d’informations pratiques pour bien préparer votre expatriation ?</p>
<p>Vous trouverez dans cette rubrique des informations concernant votre pays d’accueil et les démarches administratives à effectuer, avant de partir, auprès des divers organismes français.</p>
<p>Pour n’oublier aucune étape, pensez à consulter :</p>
<ul class="spip">
<li>notre <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/article/expatriation-15-cles-pour-partir-l" class="spip_out">guide de l’expatriation</a></li>
<li>notre « <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/article/expatriation-la-check-list" class="spip_out">check-list</a> »</li></ul>
<p>Enfin, vous pouvez vous  abonner à <a href="https://www.facebook.com/pegase.expatriation.francaisaletranger" class="spip_out" rel="external">Pégase</a>, la page Facebook de l’expatriation et des Français résidant à l’étranger, pour recevoir des actualités quotidiennes et dialoguer sur les thématiques qui vous intéressent.</p>
<h2>Dernières actualités</h2>
<ul class="actus_articles_bloc">
<li class="enrichi">
<span class="visuel_article"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/article/pegase-la-page-de-l-expatriation"><img class="spip_logos" alt="" src="http://www.diplomatie.gouv.fr/fr/IMG/arton118579.jpg?1435090601" width="170" height="110"></a> </span>
<a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/article/pegase-la-page-de-l-expatriation" title="Pegase, la page de l'expatriation et des Français de l'étranger">Pegase, la page de l’expatriation et des Français de l’étranger</a>
<p>
          Une page Facebook dédiée aux Français de l’étranger et à ceux qui préparent leur expatriation a été créée par le Ministère des Affaires étrangères et du Développement international.

https://www.facebook.com/pegase.exp...

Vous y trouverez des informations pratiques et concrètes sur les démarches consulaires ainsi que des liens utiles pour répondre aux questions que vous vous posez quand vous résidez à l’étranger sur la santé, la protection sociale, la retraite, la scolarité française et de nombreux autres (...)
</p></li>
</ul>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation.md">Dossiers pays de l’expatriation</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-documents-de-voyage.md">Documents de voyage</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-documents-de-voyage-article-documents-d-identite.md">Documents d’identité</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-documents-de-voyage-article-visas.md">Visas</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-protection-sociale.md">Protection sociale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-sante.md">Santé</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-handicap-et-expatriation.md">Handicap et expatriation</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-fiscalite.md">Fiscalité</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-emploi.md">Emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-emploi-article-programme-vacances-travail.md">Programme Vacances-Travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-emploi-article-stage-117331.md">Stage</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-emploi-article-rechercher-un-emploi-a-l-etranger-117334.md">Rechercher un emploi à l’étranger</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-emploi-article-volontariat.md">Volontariat</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais.md">Scolarité en français</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais-article-agence-pour-l-enseignement-francais-a-l-etranger-aefe.md">Agence pour l’enseignement français à l’étranger (AEFE) </a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais-article-mission-laique-francaise.md">Mission laïque française</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais-article-enseignement-a-distance.md">Centre national d’enseignement à distance (CNED)</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais-article-aide-a-la-scolarite.md">Dispositif de bourses scolaires</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures.md">Etudes supérieures</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-douanes.md">Douanes</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-douanes-article-animaux-domestiques.md">Animaux domestiques</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-douanes-article-demenagement.md">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-douanes-article-transfert-d-argent.md">Transfert d’argent</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-permis-de-conduire.md">Permis de conduire</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-retraite.md">Retraite</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-retraite-article-calculer-sa-retraite-lorsqu-on-a.md">Calculer sa retraite lorsque l’on part travailler à l’étranger</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-retraite-article-vivre-sa-retraite-a-l-etranger.md">Vivre sa retraite à l’étranger</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-en-savoir-plus.md">En savoir plus</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-en-savoir-plus-article-librairies-specialisees.md">Librairies spécialisées</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-en-savoir-plus-article-associations-des-francais-de-l-etranger.md">Associations des Français de l’étranger</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
