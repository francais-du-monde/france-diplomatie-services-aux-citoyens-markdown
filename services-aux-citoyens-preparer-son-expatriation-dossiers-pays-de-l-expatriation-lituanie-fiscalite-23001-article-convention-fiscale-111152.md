# Convention fiscale

<p>Une convention entre le Gouvernement de la République française et le Gouvernement de la République de Lituanie en vue d’éviter les doubles impositions et de prévenir l’évasion et la fraude fiscale en matière d’impôts sur le revenu et sur la fortune a été signée le 7 juillet 1997.</p>
<p>Elle est entrée en vigueur le 1er mai 2001 et publiée par décret n° 2001-406 du 2 mai 2001 (JO du 11 mai 2001).</p>
<p>Le texte de la convention peut être obtenu à la Direction des Journaux Officiels,</p>
<ul class="spip">
<li>par courrier : 26 rue Desaix - 75727 Paris cedex 15,</li>
<li>par télécopie : 01 40 58 77 80,</li>
<li>ou sur le site du <a href="http://www.impots.gouv.fr/" class="spip_out" rel="external">ministère des Finances</a>.</li></ul>
<p>Les dispositions de la convention s’appliquent aux impôts suivants (article 2) :</p>
<ul class="spip">
<li>en ce qui concerne la France : l’impôt sur le revenu, l’impôt sur les sociétés, la taxe sur les salaires et l’impôt de solidarité sur la fortune (ISF) ;</li>
<li>en ce qui concerne la Lituanie : l’impôt sur le bénéfice des personnes morales, l’impôt sur le revenu des personnes physiques, l’impôt sur les entreprises qui utilisent le capital de l’Etat et l’impôt sur les biens immobiliers.</li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/fiscalite-23001/article/convention-fiscale-111152). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
