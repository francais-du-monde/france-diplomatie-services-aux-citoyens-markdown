# Convention fiscale

<p>La France et la Tunisie ont signé <strong>le 28 mai 1973</strong> une convention en vue d’éviter les doubles impositions et de prévenir l’évasion fiscale en matière d’impôts sur le revenu et sur la fortune. Cette convention est <strong>entrée en vigueur le 13 novembre 1975. </strong></p>
<p>Voici un résumé de ses points clés pour les particuliers :</p>
<h4 class="spip">Champ d’application de la convention</h4>
<h4 class="spip">Personnes visées</h4>
<p>Cet accord a pour objet de protéger les personnes (personne physique, personne morale ou groupement de personnes physiques qui n’a pas la personnalité morale) qui sont des résidents d’un Etat contractant ou de chacun des deux Etats, selon l’article 1er de la Convention.</p>
<h4 class="spip">Notion de domicile</h4>
<p>L’article 3, paragraphe 1, de la Convention stipule que l’expression « résident d’un Etat » désigne toute personne qui, en vertu de la législation de cet Etat y est assujettie à l’impôt en raison de son domicile, de sa résidence, de son siège de direction ou de tout autre critère analogue.</p>
<p>Le deuxième alinéa du même article fournit les critères subsidiaires permettant de résoudre les cas de double domiciliation si l’assujettissement à l’impôt ne pouvait suffire.</p>
<p>Ces critères sont :</p>
<ul class="spip">
<li>le foyer d’habitation permanent ;</li>
<li>le centre de ses intérêts vitaux ;</li>
<li>le lieu de séjour habituel ;</li>
<li>à défaut, l’Etat dont elle possède la nationalité.</li></ul>
<p>Si une personne possède la double nationalité ou si elle ne possède la nationalité d’aucun d’eux, la question est tranchée d’un commun accord par les autorités des deux Etats contractants (article 3, paragraphe 2d).</p>
<p><i>Mise à jour : novembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/fiscalite/article/convention-fiscale). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
