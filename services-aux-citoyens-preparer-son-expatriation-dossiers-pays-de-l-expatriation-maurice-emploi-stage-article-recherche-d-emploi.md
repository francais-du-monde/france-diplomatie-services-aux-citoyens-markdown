# Recherche d’emploi

<h4 class="spip">Outils pour la recherche d’emploi</h4>
<p><strong>Journaux</strong></p>
<p>Les petites annonces des journaux <a href="http://www.lexpress.mu/" class="spip_out" rel="external">L’Express</a>, <a href="http://www.lemauricien.com/" class="spip_out" rel="external">Le Mauricien</a>, <a href="http://www.lematinal.com/" class="spip_out" rel="external">Le Matinal</a>.</p>
<p><strong>Sites internet</strong></p>
<ul class="spip">
<li><a href="http://www.myjob.mu/" class="spip_out" rel="external">Myjob.mu</a></li>
<li><a href="http://mu.3wjobs.com/" class="spip_out" rel="external">3wjobs.com</a></li>
<li><a href="http://www.learn4good.com/jobs/language/english/list/country/mauritius/" class="spip_out" rel="external">Learn4good.com/jobs</a></li></ul>
<p><i>Le signalement d’organismes sur ce site ne saurait engager la responsabilité du MAEDI quant à la qualité de leurs services.</i></p>
<h4 class="spip">Organismes pour la recherche d’emploi</h4>
<p><strong>Ministère du Travail (Division Emploi)</strong><br class="manualbr">Tél. : 212.8369 <br class="manualbr">Fax : 211.00.19<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/emploi-stage/article/recherche-d-emploi#empdiv#mc#intnet.mu#" title="empdiv..åt..intnet.mu" onclick="location.href=mc_lancerlien('empdiv','intnet.mu'); return false;" class="spip_mail">Courriel</a></p>
<p><strong>The Mauritius Chamber of Commerce and Industry</strong><br class="manualbr">3, Royal Street, Port-Louis, Mauritius<br class="manualbr">Tél. : (230) 208 3301<br class="manualbr">Fax : (230) 208 0076</p>
<p><strong>Board of Investment</strong><br class="manualbr">Ground Floor, One Cathedral Square Building<br class="manualbr">16, Jules Koenig Street<br class="manualbr">Port Louis, Mauritius<br class="manualbr">Tel : (+230) 203 3800 <br class="manualbr">Fax : (+230) 208 8160<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/emploi-stage/article/recherche-d-emploi#wl#mc#investmauritius.com#" title="wl..åt..investmauritius.com" onclick="location.href=mc_lancerlien('wl','investmauritius.com'); return false;" class="spip_mail">Courriel</a></p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maurice/emploi-stage/article/recherche-d-emploi). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
