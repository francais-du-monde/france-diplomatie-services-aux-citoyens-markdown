# Communications

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/vie-pratique/article/communications-111023#sommaire_1">Téléphone – Internet</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/vie-pratique/article/communications-111023#sommaire_2">Poste</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Téléphone – Internet</h3>
<p>Les liaisons téléphoniques sont excellentes quelle que soit la destination de l’appel.</p>
<p>Indicatif téléphonique du pays : 27 (puis <strong>11</strong> pour Johannesburg, <strong>21</strong> pour Le Cap et <strong>12</strong> pour Pretoria).</p>
<p>L’Afrique du Sud dispose de services de communication multimédia comparables à ceux de l’Europe Les connexions internet, de qualité généralement correcte, sont toutefois coûteuses.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p>Site Internet de TV5 : <a href="http://www.tv5.org/cms/chaine-francophone/voyageurs/Outils/p-6716-Indicatifs-telephoniques.htm" class="spip_out" rel="external">Tv5.org</a>.</p>
<h4 class="spip">Téléphoner gratuitement par Internet </h4>
<p>Il est généralement très avantageux d’utiliser <strong>des logiciels de téléphonie sur IP</strong> (Skype, Google talk, Live messenger, Yahoo ! messenger, etc.) car les tarifs proposés à l’international sont souvent moindre que ceux des opérateurs locaux exploitant des lignes classiques, voire gratuits dans certaines conditions.</p>
<h3 class="spip"><a id="sommaire_2"></a>Poste</h3>
<p>Les services postaux se révèlent relativement fiables à l’envoi. En revanche, l’acheminement est aléatoire (entre une et quatre semaines) et les garanties de réception sont faibles.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/vie-pratique/article/communications-111023). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
