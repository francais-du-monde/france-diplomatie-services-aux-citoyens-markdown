# Attestation de recensement et de la participation aux journées défense et citoyenneté

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/documents-officiels-a-l-etranger/article/attestation-de-recensement-et-de-la-participation-aux-journees-defense-et#sommaire_1">Comment se faire recenser à l’étranger ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/documents-officiels-a-l-etranger/article/attestation-de-recensement-et-de-la-participation-aux-journees-defense-et#sommaire_2">Comment effectuer la journée défense et citoyenneté à l’étranger ?</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/documents-officiels-a-l-etranger/article/attestation-de-recensement-et-de-la-participation-aux-journees-defense-et#sommaire_3">Quelles sont les dispositions applicables aux doubles nationaux ?</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Comment se faire recenser à l’étranger ?</h3>
<p><strong>Le recensement est une obligation légale</strong> pour tous, garçons et filles, et intervient dans les mois qui suivent le seizième anniversaire.</p>
<p>Les jeunes Français qui sont établis avec leur famille à l’étranger, quel que soit leur lieu de naissance, sont soumis aux mêmes obligations que ceux vivant en France.</p>
<p>A l’étranger, le recensement s’effectue auprès de votre consulat : <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/article/annuaire-des-representations-francaises-a-l-etranger" class="spip_in">trouver le consulat dont vous dépendez</a></p>
<p>Deux cas peuvent se présenter :</p>
<ul class="spip">
<li>vous êtes <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/inscription-consulaire-et-communaute-francaise/article/comment-s-inscrire-et-mettre-ses" class="spip_in">inscrit au Registre des Français établis hors de France</a> : le recensement est automatique et ne nécessite aucune intervention de votre part</li>
<li>vous n’êtes pas inscrit au Registre des Français établis hors de France : il vous appartient de prendre l’attache de votre consulat au plus tard avant votre 16ème anniversaire.</li></ul>
<p><strong>A noter</strong> : les Français qui possèdent une autre nationalité doivent le signaler au consulat s’ils souhaitent bénéficier des possibilités offertes aux doubles nationaux.</p>
<p>A l’issue de la procédure de recensement, le consulat vous remettra <strong>une attestation de recensement</strong> à conserver précieusement.</p>
<p>En effet, elle vous sera réclamée si vous voulez vous inscrire aux examens ou concours soumis au contrôle de l’autorité publique (CAP, BEP, Baccalauréat, permis de conduire, etc.).</p>
<p>Tous les jeunes recensés à l’étranger relèvent du centre du service national de Perpignan.</p>
<p>Messagerie du CSN de Perpignan disponible sur le <a href="http://www.defense.gouv.fr/jdc/ma-jdc" class="spip_out" rel="external">site du ministère de la Défense</a> ou, <strong>et seulement en cas de pièces à joindre</strong>, à l’adresse suivante : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/documents-officiels-a-l-etranger/article/attestation-de-recensement-et-de-la-participation-aux-journees-defense-et#dsn-csn-etranger-jdc.courrier.fct#mc#intradef.gouv.fr#" title="dsn-csn-etranger-jdc.courrier.fct..åt..intradef.gouv.fr" onclick="location.href=mc_lancerlien('dsn-csn-etranger-jdc.courrier.fct','intradef.gouv.fr'); return false;" class="spip_mail">dsn-csn-etranger-jdc.courrier.fct<span class="spancrypt"> [at] </span>intradef.gouv.fr</a></p>
<ul class="spip">
<li><strong>Téléphone</strong> : 04 68 35 85 85</li>
<li><strong>Télécopie </strong> : 04 68 35 89 62</li>
<li><strong>Adresse postale</strong> :<br class="manualbr"><strong>Centre du service national de Perpignan</strong><br class="manualbr">Section d’administration JDC des français à l’étranger<br class="manualbr">Boite postale 60910<br class="manualbr">66020 PERPIGNAN Cedex</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Comment effectuer la journée défense et citoyenneté à l’étranger ?</h3>
<p>La <strong>journée défense et citoyenneté (JDC) remplace la journée d’appel de préparation à la défense (JAPD)</strong> depuis le 1er juillet 2010.</p>
<p><strong>Qui participe à la Journée défense et citoyenneté ?</strong></p>
<p><strong>Tous les jeunes recensés ayant entre 16 et 25 ans</strong> doivent participer à la journée défense et citoyenneté.</p>
<p>Personnes exemptées : à leur demande, les personnes handicapées titulaires d’une carte d’invalidité ou détentrices d’un certificat médical délivré par un médecin agréé auprès du ministère de la Défense ne sont pas soumises à l’obligation de la Journée défense et citoyenneté.</p>
<p><strong>Qu’est-ce-que la journée défense et citoyenneté ?</strong></p>
<p>La journée défense et citoyenneté propose aux jeunes un contact direct avec la communauté militaire, et la découverte des multiples métiers et spécialités civiles et militaires qu’offre aujourd’hui la Défense aux jeunes Français.</p>
<p>A l’étranger, la Journée défense et citoyenneté prend souvent la forme d’une session de formation théorique (interventions et films) d’une journée ou d’une demi-journée.</p>
<p>A l’issue de la session, les jeunes concernés reçoivent un certificat individuel de participation attestant qu’ils sont en situation régulière au regard du code du service national. Ce document leur sera notamment réclamé pour s’inscrire aux examens et concours soumis au contrôle de l’autorité publique (baccalauréat, formation à l’université, permis de conduire, etc.).</p>
<p><strong>Déroulement de la journée défense et citoyenneté à l’étranger</strong></p>
<p>Des sessions de journée défense et citoyenneté sont organisées à l’étranger par les consulats sous la responsabilité du chef de poste.</p>
<p>Des aménagements ont été prévus pour simplifier les procédures et tenir compte des spécificités locales. Ainsi, lorsque pour des raisons de géographie ou de contexte politique local, il n’est pas possible d’organiser une Journée défense et citoyenneté normale, elle peut être remplacée par l’envoi d’une information par courrier. Les jeunes recensés vivant à l’étranger ne sont donc pas pénalisés par leur situation.</p>
<p>A l’étranger, une fois recensés, vous pouvez donc vous trouver dans deux cas :</p>
<ul class="spip">
<li>si le consulat organise une Journée défense et citoyenneté normale : vous serez convoqué par le consulat. Il vous faudra donner suite à ce courrier et confirmer ou infirmer votre présence.</li>
<li>si le consulat ne peut organiser de Journée défense et citoyenneté normale et organise une Journée défense et citoyenneté "adaptée" : vous recevrez directement par courrier votre attestation de participation à la journée défense et citoyenneté, accompagnée des liens utiles vers le site du ministère de la Défense.</li></ul>
<p><strong>A noter</strong> :</p>
<ul class="spip">
<li>les jeunes ayant participé à une session de journée défense et citoyenneté "adaptée" peuvent demander, selon leur intérêt, à l’occasion d’un séjour en France, à participer à une session réelle de Journée défense et citoyenneté. Ils contacteront à cet effet le centre du service national dont ils relèvent, et par défaut celui de Perpignan dont les coordonnées figurent ci-dessus.</li>
<li>les Français âgés de moins de 25 ans qui reviennent résider en France et qui, compte tenu de leur ancienne résidence à l’étranger n’ont pu participer à une Journée défense et citoyenneté, ni réelle, ni adaptée, doivent demander à l’organisme du service national dont ils relèvent désormais, à participer à une session défense et citoyenneté.</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Quelles sont les dispositions applicables aux doubles nationaux ?</h3>
<p>De façon générale, les doubles nationaux sont soumis aux obligations du service national à l’égard des deux Etats dont ils possèdent la nationalité.</p>
<p>Toutefois, des conventions bilatérales ou la Convention du Conseil de l’Europe du 6 mai 1963 ont dispensé les doubles nationaux de leurs obligations à l’égard d’un des deux Etats dont ils possèdent la nationalité.</p>
<p>Les jeunes doubles nationaux peuvent donc se retrouver face à un des cas suivants :</p>
<p><strong>Il n’existe pas de convention entre la France et l’autre Etat dont vous avez la nationalité</strong></p>
<p>Les doubles nationaux doivent satisfaire aux obligations du service national à l’égard des deux Etats. C’est le cas de la Turquie ou de la Russie par exemple.</p>
<p>Il est donc vivement conseillé aux doubles nationaux résidant en France et qui se rendent occasionnellement dans le pays dont ils possèdent également la nationalité de régulariser leur situation militaire à l’égard de cet Etat.</p>
<p><strong>Il existe une convention entre la France et l’autre Etat dont vous avez la nationalité</strong></p>
<p><strong>Conventions bilatérales</strong></p>
<p>La France est liée par une convention bilatérale relative aux obligations des doubles nationaux en matière de service national avec les Etats suivants :</p>
<ul class="spip">
<li>Algérie</li>
<li>Argentine</li>
<li>Belgique</li>
<li>Chili</li>
<li>Colombie</li>
<li>Espagne</li>
<li>Israël</li>
<li>Italie</li>
<li>Luxembourg</li>
<li>Paraguay</li>
<li>Pérou</li>
<li>Suisse</li>
<li>Tunisie</li></ul>
<p><strong>Nota bene</strong> : Ces conventions ne sont pas applicables pour les jeunes qui résident dans les pays où le service national a été supprimé ou suspendu (cas de la Belgique, de l’Espagne, du Luxembourg et de l’Italie).</p>
<p><strong>Convention du Conseil de l’Europe du 6 mai 1963</strong></p>
<p>La Convention du Conseil de l’Europe du 6 mai 1963 sur la réduction des cas de pluralité de nationalités et sur les obligations militaires en cas de pluralité de nationalités est toujours en vigueur.</p>
<p>Vous êtes concernés par cette convention si en plus de votre nationalité française, vous avez la nationalité d’un des Etats suivants :</p>
<ul class="spip">
<li>Autriche</li>
<li>Danemark</li>
<li>Irlande</li>
<li>Norvège</li>
<li>Pays-Bas</li>
<li>Royaume-Uni</li>
<li>Suède</li></ul>
<p><strong>Nota bene</strong> : Cependant, cette convention n’est pas applicable pour les jeunes qui résident dans les pays où le service national a été supprimé ou suspendu (cas de l’Irlande, des Pays-Bas et du Royaume-Uni).</p>
<p><strong>Vos obligations militaires lorsqu’il existe une convention</strong></p>
<p>Si en plus de votre nationalité française, vous avez la nationalité d’un des Etats cité dans les deux tableaux ci-dessus, votre cas est régi selon la procédure suivante :</p>
<p>Le double national est soumis aux obligations militaires de l’Etat sur le territoire duquel il réside habituellement.</p>
<p>Dans certains pays, il peut déroger à cette disposition uniquement s’il exerce un droit d’option variable en fonction des pays :</p>
<ul class="spip">
<li>Algérie : option à prendre dès le recensement et au maximum avant l’âge de 25 ans pour la JDC.</li>
<li>Israël : option à prendre avant 18 ans (pour les résidents dans un pays tiers).</li>
<li>Suisse : option à prendre avant l’âge de 19 ans.</li>
<li>Tunisie : la convention franco-tunisienne prévoit que les franco-tunisiens résidant en Tunisie mais choisissant d’effectuer leurs obligations militaires à l’égard de la France doivent accomplir un engagement dans les forces armées françaises de 12 mois minimum.</li>
<li>Pays concernés par la seule Convention du Conseil de l’Europe du 6 mai 1963 (cf. liste ci-dessus) : option à prendre avant l’âge de 19 ans.</li></ul>
<p>Le double national libéré du service national à l’égard d’un des deux Etats est considéré comme ayant satisfait aux mêmes obligations à l’égard de l’autre Etat dont il possède la nationalité.</p>
<p><strong>Procédure à suivre à l’étranger</strong></p>
<p>Pour bénéficier des dispositions relatives à une convention signée par la France, les doubles nationaux doivent signaler <strong>lors de leur inscription au Registre des Français établis hors de France</strong> ou au plus tard lors du recensement à 16 ans qu’ils possèdent également une nationalité étrangère.</p>
<p>Le consulat les informera des options qui, le cas échéant, s’offrent à eux et leur fera compléter le formulaire prévu par chaque convention.</p>
<p>Cette procédure est suivie quand bien même l’Etat de résidence n’aurait pas signé de convention bilatérale avec la France (un Franco-italien résidant en Australie bénéficie des dispositions de la convention signée avec l’Italie).</p>
<p>Pour toute information complémentaire, <a href="http://www.defense.gouv.fr/jdc" class="spip_out" rel="external">consulter le site du ministère de la Défense relatif au recensement et à la JDC</a>.</p>
<p><i>Mise jour : janvier 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/documents-officiels-a-l-etranger/article/attestation-de-recensement-et-de-la-participation-aux-journees-defense-et). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
