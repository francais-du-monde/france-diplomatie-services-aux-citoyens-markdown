# Animaux domestiques

<p>Toute personne désirant venir avec son animal domestique au Chili doit effectuer les formalités auprès de la Direction sanitaire départementale (ministère de l’agriculture) muni d’un certificat vétérinaire et d’un certificat de vaccination contre la rage.</p>
<p>Ces documents doivent être traduits en espagnol et légalisés par le ministère des affaires étrangères français puis par le consulat du Chili à Paris. Ils doivent être présentés à la douane à l’arrivée au Chili.</p>
<p>Pour en savoir plus :</p>
<ul class="spip">
<li>Notre <a href="services-aux-citoyens-preparer-son-expatriation-douanes-article-animaux-domestiques.md" class="spip_in">article thématique sur l’exportation d’animaux domestiques</a></li>
<li><a href="http://www.sag.cl/" class="spip_out" rel="external">site du ministère chilien de l’Agriculture</a></li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/entree-et-sejour-22781/article/animaux-domestiques-109749). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
