# Stages

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/emploi-stage/article/stages#sommaire_1">Types de stage</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/emploi-stage/article/stages#sommaire_2">Organismes pour la recherche de stage</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/emploi-stage/article/stages#sommaire_3">Avez-vous pensé au Volontariat International (V.I.E ou V.I.A.) ?</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Types de stage</h3>
<p>Au Royaume-Uni, on distingue plusieurs catégories de stages :</p>
<ul class="spip">
<li>"Sandwich and industrial placements" : placement en entreprise rémunéré pendant une période déterminée d’un cursus universitaire, pris en compte pour l’obtention du diplôme. Sa durée est souvent d’une année entière.</li>
<li>"Work-based project" : mission spécifique obligatoire dans le cursus effectuée dans une entreprise.</li>
<li>"Work placement" : période de travail rémunéré ou non faisant partie intégrale des études. Le placement peut être organisé entre l’université et l’employeur ou par l’étudiant lui-même.</li>
<li>"Internship" : placement d’un jeune dans une grande entreprise pour une durée de 6 à 12 semaines, souvent pendant les vacances d’été.</li></ul>
<p>La durée du stage doit être inférieure à un an. Le stagiaire rémunéré ou non a le même statut que les employés. La convention de stage tripartite doit impérativement fixer les différentes modalités du stage (rémunération, assurances, responsabilités…)</p>
<h3 class="spip"><a id="sommaire_2"></a>Organismes pour la recherche de stage</h3>
<h4 class="spip">Agences de recrutement</h4>
<p><a href="http://www.cei-london.com/" class="spip_out" rel="external">Centre d’échanges internationaux</a> (CEI)<br class="manualbr">114-116 Curtain Road<br class="manualbr">EC2A 3AH London – GB<br class="manualbr">Tél depuis la France : 0810.67.63.70 <br class="manualbr">Tél : 00 44 (0) 207 749 7700<br class="manualbr">Etudiants européens de 18 à 30 ans dans les domaines suivants : commerce, logistique, marketing, communication, administration, secrétariat, tourisme, hôtellerie. Frais d’inscription.</p>
<p><a href="http://www.teli.asso.fr/" class="spip_out" rel="external">Club TELI</a><br class="manualbr">2 chemin de Golemme<br class="manualbr">74600 SEYNOD <br class="manualbr">Tél. 04 50 52 26 58<br class="manualbr">Frais d’inscription : 40 €</p>
<h4 class="spip">Organismes de placement à but non lucratif</h4>
<p><a href="http://www.placement-uk.com/" class="spip_out" rel="external">Placement UK</a><br class="manualbr">Pour les étudiants de premier ou deuxième cycle avec convention de stage. Placement gratuit. Publication sur le site d’offres de stages.</p>
<p><a href="http://www.prospectsnet.com/ncwe.rd/index.jsp" class="spip_out" rel="external">The National Council for Work Experience</a><br class="manualbr">Offres de stages sur le site.<br class="manualbr">Possibilité de télécharger le <i>Student Guide</i> sur le site qui donne un nombre important d’informations et de conseils sur les placements en stages.</p>
<p><a href="http://www.thegraduate.co.uk/" class="spip_out" rel="external">The graduate</a><br class="manualbr">Information sur les entreprises qui recrutent des stagiaires au niveau <i>graduate</i>, offres en ligne. Beaucoup d’information sur l’entreprise. Possibilité de postuler à une offre de stage directement auprès de l’entreprise sur le site.</p>
<p>Des offres de stages sont publiées sur le site de grandes universités : London University.</p>
<p>Internet : <a href="http://www.careers.lon.ac.uk/" class="spip_out" rel="external">www.careers.lon.ac.uk</a></p>
<h4 class="spip">Stage dans les services officiels français</h4>
<p>Tous les étudiants des établissements universitaires avec lesquels le ministère des Affaires étrangères a conclu une convention cadre sont susceptibles de postuler à un stage qui peut être de durée variable (de deux à six mois, éventuellement plus). Ils concernent des étudiants ayant au minimum un niveau licence pour les universités, et deuxième année de scolarité pour les grandes écoles. Les étudiants des IEP peuvent aussi postuler afin d’effectuer leur stage dans les postes diplomatiques et consulaires français. Vous devez prendre contact avec le responsable des stages de votre établissement en vue de constituer un dossier puis le transmettre au MAE.</p>
<p>Pour en savoir plus : lire la rubrique <a href="http://www.diplomatie.gouv.fr/fr/emplois-stages-concours/stagiaires/" class="spip_in">Stagiaires</a></p>
<p><strong>Les régions</strong>, parce qu’elles ont des accords privilégiés entre elles en Europe, pourront vous apporter des pistes de recherche, des informations et éventuellement vous proposer les programmes inter-régionaux qu’elles ont mis en place.</p>
<h3 class="spip"><a id="sommaire_3"></a>Avez-vous pensé au Volontariat International (V.I.E ou V.I.A.) ?</h3>
<p>Le V.I.E. permet aux entreprises françaises de confier à un jeune ressortissant de l’espace économique européen, âgé de 18 à 28 ans, une mission professionnelle (rémunérée) à l’étranger durant une période modulable de 6 à 24 mois.</p>
<p>Pour en savoir plus :</p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/emplois-stages-concours/volontaires-internationaux/" class="spip_in">Page thématique sur le volontariat international sur notre site</a></li>
<li><a href="http://www.civiweb.com/FR/index.aspx" class="spip_out" rel="external">http://www.civiweb.com/FR/index.aspx</a></li>
<li><a href="http://export.businessfrance.fr/default.html" class="spip_out" rel="external">Business France.fr</a> - <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/emploi-stage/article/stages#infoVIE#mc#ubifrance.fr#" title="infoVIE..åt..ubifrance.fr" onclick="location.href=mc_lancerlien('infoVIE','ubifrance.fr'); return false;" class="spip_mail">courriel</a></li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/emploi-stage/article/stages). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
