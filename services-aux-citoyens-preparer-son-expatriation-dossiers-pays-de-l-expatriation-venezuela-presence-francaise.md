# Présence française

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/presence-francaise/#sommaire_1">Autorités françaises dans le pays </a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/presence-francaise/#sommaire_2">Associations françaises dans le pays </a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Autorités françaises dans le pays </h3>
<h4 class="spip">Ambassade et consulat de France au Venezuela </h4>
<p>Consultez notre article <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/article/annuaire-des-representations-francaises-a-l-etranger" class="spip_in">Annuaire des représentations françaises à l’étranger</a>.</p>
<h4 class="spip">Réseau français de coopération et d’action culturelle</h4>
<p>Les coordonnées des services du réseau de coopération et d’action culturelle français au Venezuela ainsi que la description de l’activité de ces services se trouvent sur le <a href="http://www.latitudefrance.org/AnnuaireCarto.html" class="spip_out" rel="external">site internet du réseau culturel : Latitude France</a>.</p>
<h4 class="spip">Économie : réseau mondial</h4>
<p>Les <a href="http://www.tresor.economie.gouv.fr/Pays/venezuela" class="spip_out" rel="external">services économiques</a> sont présent à l’ambassade de France au Venezuela. Ils sont une émanation de la direction générale du Trésor et ont pour missions principales l’analyse macroéconomique et l’animation des relations économiques bilatérales entre la France et les pays où ils sont implantés.</p>
<p>Vous pouvez également consulter le <a href="http://www.cciavf.com.ve/fr/" class="spip_out" rel="external">site internet de la chambre franco-vénézuélienne de commerce et d’industrie</a></p>
<h4 class="spip">Le centre d’information universitaire et scientifique (CAMPUSFRANCE)</h4>
<p>La représentation au Venezuela de l’agence CAMPUS FRANCE est située à l’espace France de Caracas :</p>
<p>Edificio centro Solano, 1 piso<br class="manualbr"><strong>Espace France de l’Alliance Francaise de Chacaito</strong><br class="manualbr">avenida Francisco Solano con 3a calle las delicias<br class="manualbr"><strong>Chacaito</strong></p>
<h3 class="spip"><a id="sommaire_2"></a>Associations françaises dans le pays </h3>
<p>Une dizaine d’associations françaises sont actives dans le pays, vous trouverez la liste complète sur le <a href="http://www.ambafrance-ve.org/" class="spip_out" rel="external">site de l’ambassade de France au Venezuela</a>. Parmi les plus importantes :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.francais-du-monde.net/" class="spip_out" rel="external">Association démocratique des Français à l’étranger</a></p>
<p>Pour en savoir plus :</p>
<ul class="spip">
<li>notre article thématique : <a href="services-aux-citoyens-preparer-son-expatriation-en-savoir-plus-article-associations-des-francais-de-l-etranger.md" class="spip_in">Associations des Français de l’étranger</a></li>
<li><a href="http://www.ambafrance-ve.org/" class="spip_out" rel="external">site de l’ambassade de France au Venezuela</a></li></ul>
<p><i>Mise à jour : septembre 2015</i></p>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/venezuela/presence-francaise/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
