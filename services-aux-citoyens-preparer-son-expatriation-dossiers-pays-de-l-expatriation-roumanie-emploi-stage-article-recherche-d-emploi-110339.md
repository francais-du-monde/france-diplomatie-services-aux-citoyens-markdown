# Recherche d’emploi

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/emploi-stage/article/recherche-d-emploi-110339#sommaire_1">Outils pour la recherche d’emploi</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/emploi-stage/article/recherche-d-emploi-110339#sommaire_2">Organismes pour la recherche d’emploi</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/emploi-stage/article/recherche-d-emploi-110339#sommaire_3">Opportunités d’emploi et de stages</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Outils pour la recherche d’emploi</h3>
<h4 class="spip">Journaux</h4>
<p>Des cabinets de recrutement existent parmi lesquels AMROP International / Stein  Partner, Hill International et Ward Howell. Des offres d’emploi sont diffusées dans la presse écrite, notamment dans les quotidiens Evenimentul Zilei, Romania Libera, Adevarul et Ziua.</p>
<h4 class="spip">Sites internet</h4>
<ul class="spip">
<li><a href="http://www.anofm.ro/" class="spip_out" rel="external">Le site de l’Agence nationale pour l’emploi - l’ANOFM (Agentia nationala pentru ocuparea fortei de munca)</a>.</li>
<li><a href="http://www.ejobs.ro/" class="spip_out" rel="external">Ejobs.ro</a></li>
<li><a href="http://www.anuntul.ro/" class="spip_out" rel="external">Anuntul.ro</a></li>
<li><a href="http://www.bestjobs.ro/" class="spip_out" rel="external">Best jobs.ro</a></li>
<li><a href="http://www.myjob.ro/" class="spip_out" rel="external">My job.ro</a></li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Organismes pour la recherche d’emploi</h3>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Agence nationale pour emploi : <a href="http://www.anofm.ro/" class="spip_out" rel="external">ANOFM</a></p>
<h3 class="spip"><a id="sommaire_3"></a>Opportunités d’emploi et de stages</h3>
<p>Il est recommandé également de déposer des candidatures spontanées directement auprès des structures qui vous intéressent.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/emploi-stage/article/recherche-d-emploi-110339). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
