# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/vie-pratique/article/transports#sommaire_1">Importation de véhicule</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/vie-pratique/article/transports#sommaire_2">Permis de conduire</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/vie-pratique/article/transports#sommaire_3">Code de la route</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/vie-pratique/article/transports#sommaire_4">Assurances et taxes</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/vie-pratique/article/transports#sommaire_5">Achat et location</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/vie-pratique/article/transports#sommaire_6">Entretien</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/vie-pratique/article/transports#sommaire_7">Réseau routier</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/vie-pratique/article/transports#sommaire_8">Transports en commun</a></li></ul>
<p>En cas d’accident de la circulation routière, il est recommandé de se mettre sous la protection de la police en composant le numéro gratuit 113 (urgences 115 / pompiers 114) et de prendre contact avec l’ambassade de France à Hanoï (04-39 44 57 00) qui répond 24h/24 ou le consulat général de France à Hô Chi Minh-Ville (08-35 20 68 00).</p>
<h3 class="spip"><a id="sommaire_1"></a>Importation de véhicule</h3>
<p>Une fois par an les véhicules sont soumis à un contrôle technique dont la périodicité varie en fonction de la date de mise en circulation de la voiture (coût : 140 000 dongs).</p>
<p>Aucune norme spécifique n’est exigée pour l’importation de son véhicule, la date de première mise en circulation doit être de moins de cinq ans. Les droits de douane sont de 150 à 200% voire 250%.</p>
<p>La procédure à suivre pour l’importation d’un véhicule est indiquée sur le site du <a href="http://www.consulfrance-hcm.org/article.php3?id_article=47" class="spip_out" rel="external">consulat de France à Ho Chi Minh Ville</a> et sur celui de l’<a href="http://www.ambafrance-vn.org/" class="spip_out" rel="external">Ambassade de France</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Permis de conduire</h3>
<p><strong>Les permis de conduire français ou internationaux ne sont pas reconnus</strong>, il est en principe interdit aux touristes de conduire une moto ou une voiture. Il est donc nécessaire de recourir aux services d’un chauffeur. Seuls les résidents sont autorisés à faire les démarches pour l’obtention d’un permis local. Les étrangers en mission de longue durée (au-delà de 3 mois) qui souhaitent conduire doivent transformer leur permis de conduire français en permis de conduire vietnamien.</p>
<p>Une traduction officielle du permis de conduire étranger doit être effectuée par le Bureau de législation des districts et la demande doit être déposée auprès de la Direction des routes avec copie du passeport, du permis de travail, du permis de séjour, du permis de conduire étranger et deux photos d’identité.</p>
<p>Le coût est de 60 000 dongs environ et il faut compter deux semaines de délai. La validité du permis de conduire est identique à celle du permis de séjour, le renouvellement doit être demandé à chaque échéance.</p>
<p>Il est rappelé à tous les ressortissants français, de passage ou résidant au Vietnam, que les conditions de circulation rendent très dangereux les transports à motocyclette. Le port du casque est obligatoire. Une extrême prudence s’impose. Les taxis sont l’un des moyens de transport les plus sûrs.</p>
<p>La procédure à suivre pour l’obtention du permis vietnamien est indiquée sur les sites du <a href="http://www.consulfrance-hcm.org/article.php3?id_article=44" class="spip_out" rel="external">Consulat de France à Ho Chi Minh Ville</a> et de <a href="http://www.ambafrance-vn.org/article.php3?id_article=315" class="spip_out" rel="external">l’Ambassade de France à Hanoi</a>.</p>
<h3 class="spip"><a id="sommaire_3"></a>Code de la route</h3>
<p>La liberté de circulation est totale, sauf quelques zones interdites aux étrangers.</p>
<p>Le code de la route n’est pas respecté et les accidents très fréquents font souvent des victimes, dont la plupart sont des conducteurs de deux-roues. Le port du casque de moto est obligatoire. Les casques disponibles sur le marché local étant de qualité variable, il est recommandé d’apporter un casque aux normes européennes.</p>
<p>La conduite est à droite, la priorité également, mais le code de la route n’est pas respecté. La vitesse moyenne sur route est de 45 km/heure.</p>
<h3 class="spip"><a id="sommaire_4"></a>Assurances et taxes</h3>
<p>L’assurance au tiers-collision / personnes transportées est obligatoire et revient à environ 150 dollars US (120 euros) pour une voiture de tourisme cinq places d’une valeur de 20 000 dollars US. L’assurance tout risque est recommandée.</p>
<h3 class="spip"><a id="sommaire_5"></a>Achat et location</h3>
<p>Les principales marques représentées sont japonaises ou coréennes.</p>
<p>Le coût d’un véhicule de tourisme d’occasion acheté sur place de particulier à particulier oscille entre 7000 et 40 000 dollars US voire plus, sans garantie. Les taxes sur les véhicules sont particulièrement élevées (200% à 205% sur la valeur du véhicule). Il est conseillé de s’équiper d’un véhicule tout terrain, tropicalisé.</p>
<p>Il est possible de louer des véhicules à la journée mais généralement avec chauffeur, ce qui est par ailleurs recommandé.</p>
<h3 class="spip"><a id="sommaire_6"></a>Entretien</h3>
<p>Il est possible de faire entretenir son véhicule sur place dans un garage de marque internationale ; ou dans un garage vietnamien indépendant, meilleur marché, mais la qualité du service est plus aléatoire. Les pièces détachées de voitures de marques françaises sont difficiles, voire impossibles à trouver. Elles doivent être commandées directement en France avec des délais importants et des coûts élevés.</p>
<h3 class="spip"><a id="sommaire_7"></a>Réseau routier</h3>
<p>Le réseau routier est de qualité inégale et le trafic désordonné.</p>
<h3 class="spip"><a id="sommaire_8"></a>Transports en commun</h3>
<p>La maintenance des avions de la flotte intérieure « Vietnam Airlines » n’appelle pas de remarque particulière. Des annulations inopinées peuvent intervenir, notamment sur les vols intérieurs.</p>
<p>Le prix du billet d’avion Hanoï - Ho Chi Minh-Ville, aller-retour coûte environ 210 euros.</p>
<p>Le réseau ferroviaire s’améliore et constitue un moyen de transport intéressant même si le train reste vétuste, peu confortable et lent. Les réservations sont parfois aléatoires, il est préférable de se présenter à la gare suffisamment à l’avance.</p>
<p>Deux trains de nuit quotidiens effectuent le trajet Hanoï-Lao Cai (Sapa) en couchettes. Trois fois par semaine, un wagon tout confort est accessible aux clients de l’hôtel Victoria.</p>
<p>En ville, les déplacements peuvent s’effectuer en taxis (munis de compteurs : environ 0,4€ du km), moto-taxis (xe ôm). Le car est plus difficile d’accès car le montant est variable et doit être négocié avant le début du trajet afin d’éviter une mauvaise surprise à l’arrivée.</p>
<p>Pour en savoir plus : site internet du <a href="http://www.consulfrance-hcm.org/article.php3?id_article=48" class="spip_out" rel="external">Consulat de France à Ho Chi Minh Ville</a> et site internet de l’<a href="http://www.ambafrance-vn.org/" class="spip_out" rel="external">Ambassade de France au Vietnam</a>.</p>
<p><i>Mise à jour : février 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/vietnam/vie-pratique/article/transports). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
