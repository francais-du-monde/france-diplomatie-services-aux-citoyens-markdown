# Recherche d’emploi

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/emploi-stage/article/recherche-d-emploi#sommaire_1">Outils pour la recherche d’emploi</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/emploi-stage/article/recherche-d-emploi#sommaire_2">Organismes pour la recherche d’emploi</a></li></ul>
<p><strong>Le marché caché de l’emploi</strong></p>
<p>Une part significative des offres et possibilités d’emploi ne sont pas publiées. Il s’avère indispensable d’effectuer ses recherches d’emploi au-delà des annonces publiées dans les journaux, les médias, les centres d’emploi, les agences de placement ou sur Internet. La recherche d’emploi se caractérise par une approche plus directe à l’égard des entreprises. Il est courant de les appeler pour connaître les disponibilités en matière d’emploi ou de s’adresser directement au responsable des ressources humaines ou du service qui vous intéresse. Cette attitude est bien accueillie, de même que celle qui consiste à envoyer sa candidature spontanée aux entreprises ou à contacter les employeurs potentiels afin de se faire connaître et de leur offrir ses services. L’entretien d’information au sein des entreprises est également très développé. Il se distingue de l’entretien d’embauche. Dans le souci d’élargir votre réseau, il consiste à solliciter une personne clé dans une entreprise de votre choix, pour un rendez-vous d’information sur les activités d’un service, le développement d’un produit ou le rapport annuel d’activités. Cette approche approfondie et directe facilitera l’accès au marché caché de l’emploi.</p>
<p>Avant de se lancer à la découverte du marché caché de l’emploi, il est conseillé d’avoir :</p>
<ul class="spip">
<li>un aperçu du marché visible de l’emploi (offres d’emplois, annonces, etc.) ;</li>
<li>validé vos diverses compétences (rédaction du curriculum vitae, lettre de motivation, etc.) ;</li>
<li>défini votre ou vos objectifs professionnels ;</li>
<li>une ouverture d’esprit et une habilité à communiquer.</li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Outils pour la recherche d’emploi</h3>
<h4 class="spip">Journaux</h4>
<p><strong>Au Québec : </strong></p>
<p>La presse écrite quotidienne se compose de 10 journaux francophones et d’un quotidien anglophone. <i>Le Devoir</i>, <i>le Journal de Montréal </i>et <i>La Presse</i> sont les principaux quotidiens de Montréal. <i>Le Soleil </i>paraît à Québec.</p>
<p>C’est dans l’édition du samedi des quotidiens que vous trouverez le plus d’offres d’emplois. Le lundi et le samedi, le quotidien <i>La Presse</i> consacre une partie de ses pages "Affaires" au marché du travail (le lundi) et de l’emploi (le samedi).</p>
<p>L’hebdomadaire <i>Les Affaires </i>vous informera sur l’environnement économique du Québec. Les mensuels <i>Affaires Plus</i>, <i>Commerce et PME </i>vous permettront de mieux connaître les entreprises québécoises et les pratiques de gestion d’entreprise.</p>
<p>Le magazine <i>l’Autonome </i>s’adresse aux travailleurs indépendants. La revue <i>Jobboom</i>, disponible gratuitement à l’entrée des librairies québécoises, vous informe sur les perspectives d’emploi. Le magazine d’informations générales l<i>’Actualité </i>paraît toutes les deux semaines.</p>
<h4 class="spip">Sites internet</h4>
<ul class="spip">
<li><a href="http://www.guichetemplois.gc.ca/" class="spip_out" rel="external">Guichet Emplois</a> est un site gouvernemental canadien. C’est le plus exhaustif en termes d’offres d’emploi. Outre de nombreux conseils, ce site donne également des liens Internet dans le domaine du travail et propose des outils pratiques, parmi lesquels un concepteur virtuel de curriculum vitae ;</li>
<li><a href="http://www.emploietudiant.qc.ca/" class="spip_out" rel="external">Placement étudiant du Québec</a> ;<br class="manualbr">Ce site, créé par le ministère de l’Emploi et de la solidarité sociale, permet aux étudiants d’avoir accès à une banque de milliers d’employeurs ;</li>
<li><a href="http://www.rhdsc.gc.ca/" class="spip_out" rel="external">Ministère canadien des Ressources humaines et du Développement social (RHDSC)</a> ;</li>
<li>Site <a href="http://travailleraucanada.gc.ca/" class="spip_out" rel="external">Travailler au Canada</a> ;</li>
<li>Site <a href="http://www.labourmarketinformation.ca/" class="spip_out" rel="external">Information sur le marché du travail</a> de Service Canada ;</li>
<li><a href="http://jobs-emplois.gc.ca/" class="spip_out" rel="external">Commission de la fonction publique du Canada</a> : offres d’emploi dans la fonction publique fédérale ;</li>
<li><a href="http://www.emploitoronto.com/" class="spip_out" rel="external">Emploi Toronto</a> ;</li>
<li><a href="http://www.gov.mb.ca/tce/" class="spip_out" rel="external">Ministère de la Compétitivité, de la Formation professionnelle et du Commerce du Manitoba</a> ;</li>
<li><a href="http://www.workinfonet.bc.ca/" class="spip_out" rel="external">Site de l’emploi en Colombie Britannique</a> ;</li>
<li><a href="http://www.gov.mb.ca/tce/" class="spip_out" rel="external">Ministère de la Compétitivité, de la Formation professionnelle et du Commerce du Manitoba</a> ;</li>
<li><a href="http://www.labour.gov.sk.ca/" class="spip_out" rel="external">Ministère de la Formation professionnelle, de l’Emploi et du Travail de la Saskatchewan</a>.</li></ul>
<h4 class="spip">Réseaux</h4>
<p><strong>Le réseautage</strong></p>
<p>L’objectif premier du réseau, appelé couramment réseautage (en anglais <i>networking</i>), n’est pas de trouver un emploi, mais de communiquer avec des personnes ou des entreprises susceptibles de recruter. Il s’agit tout simplement d’être curieux et attentif, de s’informer, de demander conseil, de repérer les nouvelles pistes pour agrandir son réseau de contacts personnels et trouver les personnes-ressources fiables. Le principe est simple. Il repose sur l’entraide et l’échange mutuel par le biais de cartes de visite qu’il est préférable d’avoir en permanence sur soi. Il ne faut pas hésiter à donner sa carte à chaque occasion sans oublier de demander celle des personnes que vous rencontrez au cours d’événements à caractère commercial (salons, foires, etc.) ou professionnel, mais aussi privé (loisirs, repas, vernissage, spectacles, etc.). Vous pouvez créer et activer votre réseautage à partir de votre carnet d’adresses.</p>
<ul class="spip">
<li><a href="http://www.entreprisescanada.ca/gol/cbec/site.nsf" class="spip_out" rel="external">Entreprises Canada - Services aux entrepreneurs</a> ;</li>
<li><a href="http://www.rce-nce.gc.ca/indexfr.htm" class="spip_out" rel="external">Réseaux de centres d’excellence</a> ;</li>
<li><a href="http://www.dec-ced.gc.ca/" class="spip_out" rel="external">Développement économique Canada pour les régions du Québec</a> ;</li>
<li><a href="http://www.acoa-apeca.gc.ca/" class="spip_out" rel="external">Agence de promotion économique du Canada Atlantique</a> ;</li></ul>
<h4 class="spip">Annuaires</h4>
<p>Vous trouverez des annuaires d’entreprises au Québec sur le site Internet du ministère québécois du Développement économique, de l’<a href="http://www.mdeie.gouv.qc.ca/" class="spip_out" rel="external">Innovation et de l’Exportation</a>. Vous trouverez dans chaque secteur des répertoires d’associations et d’entreprises, des personnes ressources, etc.</p>
<ul class="spip">
<li>Liste des membres de la Chambre de commerce France-Canada à Paris téléchargeable au format PDF ou à consulter sur le site Internet de la CCFC-&gt;<a href="http://www.ccfc-france-canada.com/" class="spip_url spip_out auto" rel="nofollow external">http://www.ccfc-france-canada.com/</a>].</li>
<li><a href="http://www.entreprisescanada.ca/eng/" class="spip_out" rel="external">Entreprises Canada service aux entrepreneurs</a>.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Organismes pour la recherche d’emploi</h3>
<p>De nombreux centres de ressources humaines et développement social Canada (RHDSC) sont répartis sur l’ensemble du territoire canadien. Vous trouverez leurs coordonnées sur le site Internet de <a href="http://www.servicecanada.gc.ca/" class="spip_out" rel="external">Service Canada</a>.</p>
<p>RHDSC peut s’apparenter à l’ANPE et à l’ASSEDIC réunies et couvre de nombreux programmes et services dans les domaines social et de l’emploi (aide à la recherche d’emploi, tendances du marché de l’emploi, législation et conseils juridiques, recensement des sites canadiens consacrés à l’emploi, dont le recrutement et les supports d’annonces, contrôle et gestion de l’allocation chômage, apprentissage, développement social et professionnel, etc.).</p>
<p>Si vous résidez en Ontario, dans le Manitoba ou dans la province de la Saskatchewan, vous pouvez contacter le Comité consulaire pour l’emploi et la formation professionnelle du consulat général de France à Toronto :</p>
<p>c/o Consulat général de France à Toronto<br class="manualbr">2 Bloor Street East - suite 2200 - 22ème étage <br class="manualbr">Toronto (Ontario) M4W 1A8<br class="manualbr">Téléphone : [1] (416) 847 18 88 <br class="manualbr">Télécopie : [1] (416) 847 19 01<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/emploi-stage/article/recherche-d-emploi#emploi#mc#consulfrance-toronto.org#" title="emploi..åt..consulfrance-toronto.org" onclick="location.href=mc_lancerlien('emploi','consulfrance-toronto.org'); return false;" class="spip_mail">Courriel</a><br class="manualbr">Site internet : <a href="http://www.emploitoronto.com/" class="spip_out" rel="external">Emploi Toronto</a></p>
<p><strong>Au Québec</strong></p>
<p>De nombreux organismes d’aide à l’emploi proposent une assistance à la préparation de curriculum vitae, de lettre de motivation et à l’entretien d’embauche. Cependant, si les structures québécoises demeurent très performantes, un immigrant français pourrait être intéressé à consulter des organismes connaissant les deux cultures. Les consultants de ces établissements, fins connaisseurs des deux systèmes, vous expliqueront, par une mise en perspective avec votre pays d’origine, les us et coutumes du Québec en matière d’emploi.</p>
<p>Les deux principales structures d’aide de ce type sont la <a href="http://www.citim.org/" class="spip_out" rel="external">Clef pour l’intégration au travail des immigrants</a> (CITIM) et l’<a href="http://www.ofiicanada.ca/" class="spip_out" rel="external">Office français de l’immigration et de l’intégration</a> (OFII). La CITIM et l’OFII vous aideront tous deux à trouver un emploi. Ils vous conseilleront et vous aideront à rédiger votre curriculum vitae. Tous deux disposent de consultants français et québécois qui vous renseigneront sur le marché du travail québécois.</p>
<p>La CITIM et l’OFII sont deux structures complémentaires. Si tous deux mettent à votre disposition des listes d’entreprises et vous orientent vers leurs contacts dans ces sociétés, l’OFII a dirigé ses efforts vers la prospection auprès des entreprises, alors que la CITIM dispose d’un concept original : les clubs de recherche d’emploi. Ces ateliers mettent les candidats en situation réelle de recherche d’emploi. Vous serez alors mieux à même de vous présenter à un employeur lors d’une entrevue. En faisant appel à ces deux organismes, vous mettrez toutes les chances de votre côté pour trouver un emploi.</p>
<p><i>Mise à jour : avril 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/canada/emploi-stage/article/recherche-d-emploi). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
