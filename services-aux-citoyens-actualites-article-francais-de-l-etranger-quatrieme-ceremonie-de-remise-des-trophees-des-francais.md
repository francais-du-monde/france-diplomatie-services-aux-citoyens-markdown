# Français de l’étranger - Quatrième cérémonie de remise des trophées des Français de l’étranger présidée par Matthias Fekl (15.03.16)

<p class="chapo">
      Matthias Fekl, secrétaire d’État chargé du commerce extérieur, de la promotion du tourisme et des Français de l’étranger, a présidé au Quai d’Orsay la cérémonie de remise des "Trophées des Français de l’étranger".
</p>
<p>Organisée par lepetitjournal.com, cette cérémonie visait à mettre à l’honneur sept parcours remarquables de compatriotes établis à l’étranger qui se sont distingués dans l’éducation, l’environnement, l’entreprenariat ou encore le social et l’humanitaire.</p>
<p>Cette année, ce sont près de 270 Français qui ont présenté leur candidature à un jury composé de personnalités du monde de la mobilité internationale et d’experts de l’expatriation. Nous adressons nos félicitations aux lauréats de l’année 2016 :</p>
<ul class="spip">
<li><strong>Prix du public</strong> : Fabien YOON - Corée du Sud - Acteur</li>
<li><strong>Meilleur Espoir</strong> : Ryan CURATOLO - Etats-Unis - Jockey</li>
<li><strong>Environnement</strong> : Christelle COLIN - Guinée – Vétérinaire – Directrice exécutive du centre de conservation pour chimpanzés</li>
<li><strong>Art de vivre</strong> : Remi VERRIER- Emirats arabes unis – Chef de cuisine Restaurant Le Classique</li>
<li><strong>Education</strong> : Charlotte POLLET – Taiwan - Enseignant chercher national Chiao-Tung University</li>
<li><strong>Entrepreneur</strong> : Alexandre LEBEUAN – Inde – Chef d’entreprise Shanti Travel</li>
<li><strong>Social humanitaire</strong> : Laure DELAPORTE  - Inde – Cofondatrice et coordinatrice des partenariats de Life Project 4 Youth (LP4Y)</li></ul>
<ul class="rslides" id="diapoun">
<li><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L800xH533/_t4a8754_copienet_cle0efec1-0d4ed-7a2c7.jpg" alt="" title="" width="800" height="533">
<p>
<span class="caption_text"></span>
</p></li>
<li><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L800xH533/_t4a8764_copienet_cle0e6edc-30238-4ab49.jpg" alt="" title="" width="800" height="533">
<p>
<span class="caption_text"></span>
</p></li>
</ul>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/actualites/article/francais-de-l-etranger-quatrieme-ceremonie-de-remise-des-trophees-des-francais). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
