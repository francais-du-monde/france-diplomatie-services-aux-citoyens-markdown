# Convention de sécurité sociale

<p><strong>Il n’existe pas de convention de sécurité sociale entre la France et la Hongrie.</strong> Vous pouvez vous trouver dans l’une des deux situations suivantes :</p>
<ul class="spip">
<li>Travailleurs salariés détachés dans le cadre de la législation française. Pour cette catégorie, il est possible de s’affilier à la <a href="http://www.cfe.fr/" class="spip_out" rel="external">Caisse des Français de l’étranger</a> qui gère la sécurité sociale des expatriés.</li>
<li>Travailleurs français expatriés (salariés, non-salariés, retraités, autres catégories) relevant de la législation locale.</li></ul>
<p>Pour en savoir plus :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  consultez notre rubrique thématique <a href="services-aux-citoyens-preparer-son-expatriation-protection-sociale.md" class="spip_in">Protection sociale</a>.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/protection-sociale-23461/article/convention-de-securite-sociale-113475). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
