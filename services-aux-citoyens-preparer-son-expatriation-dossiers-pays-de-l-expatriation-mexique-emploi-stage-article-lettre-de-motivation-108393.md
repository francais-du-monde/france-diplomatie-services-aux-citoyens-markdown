# Lettre de motivation

<p>La lettre de motivation n’est pas nécessaire au Mexique, <strong>sauf demande spéciale</strong> de l’entreprise.</p>
<p>Nous conseillons plutôt aux candidats de rédiger un <strong>courrier électronique</strong> clair, personnalisé et d’apparence soignée qui vous permette de vous mettre en valeur brièvement auprès des recruteurs et d’introduire votre recherche d’emploi.</p>
<p>Quelques formules de politesse pour s’adresser aux recruteurs :</p>
<ul class="spip">
<li><i>Estimado Sr. XXX / Lic. XXX / Ing. XXX</i></li>
<li><i>Estimada Sra. / Srita. XXX</i></li>
<li><i>Por medio de la presente, le hago llegar mi candidatura al puesto de …</i></li>
<li><i>De antemano lo agradezco</i></li>
<li><i>Cualquier duda quedo a sus ordenes</i></li>
<li><i>Sin más por el momento</i></li>
<li><i>Saludos cordiales</i></li>
<li><i>Reciba un cordial saludo</i></li>
<li><i>Dando seguimiento a nuestra llamada de hoy</i></li></ul>
<p>Enfin, suite à l’envoi de votre CV via courrier électronique, n’hésitez pas à <strong>relancer le recruteur par téléphone</strong> si vous n’avez pas encore eu de retour. Montrez que vous êtes présent, motivé par le poste et disponible pour un entretien.</p>
<p><i>Mise à jour : septembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/emploi-stage/article/lettre-de-motivation-108393). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
