# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/vie-pratique/article/transports#sommaire_1">Importation de véhicule</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/vie-pratique/article/transports#sommaire_2">Taxe de performance des véhicules </a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/vie-pratique/article/transports#sommaire_3">Permis de conduire</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/vie-pratique/article/transports#sommaire_4">Code de la route</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/vie-pratique/article/transports#sommaire_5">Assurances et taxes</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/vie-pratique/article/transports#sommaire_6">Achat et location</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/vie-pratique/article/transports#sommaire_7">Contrôle et entretien</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/vie-pratique/article/transports#sommaire_8">Réseau routier</a></li>
<li><a id="so_9" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/vie-pratique/article/transports#sommaire_9">Transports en commun</a></li>
<li><a id="so_10" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/vie-pratique/article/transports#sommaire_10">Transport ferroviaire </a></li>
<li><a id="so_11" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/vie-pratique/article/transports#sommaire_11">Transport aérien</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Importation de véhicule</h3>
<p>Il est possible d’importer son véhicule personnel, toutefois il devra satisfaire au contrôle technique des autorités hongroises et depuis septembre 2011 il doit être obligatoirement immatriculé en Hongrie et une taxe d’enregistrement doit être alors acquittée, conformément au principe que tout véhicule d’un <strong>résidant en Hongrie</strong> doit être autorisé à circuler par les autorités hongroises (délivrance d’une carte verte équivalente de la carte grise française et d’une immatriculation).</p>
<p><strong>Procédure pour l’immatriculation du véhicule par les autorités locales</strong></p>
<p>Après l’entrée du véhicule sur le territoire hongrois et avant le paiement de la taxe d’enregistrement, il convient d’effectuer le contrôle du véhicule auprès de l’autorité nationale des transports</p>
<p><a href="http://www.nkh.hu/Lapok/default.aspx" class="spip_out" rel="external">Nemzeti Közlekedés Hatósághoz</a> <br class="manualbr">Budapest, Mozaik utca 5. <br class="manualbr">Tel:06-1-430-2718</p>
<p>qui délivre la décision permettant l’autorisation de circulation du véhicule sur le territoire hongrois et détermine sa classification environnementale. Les services des douanes fixent ensuite le montant de la taxe sur la base du document technique établi par l’autorité nationale des transports et la plaque d’immatriculation est à retirer auprès des services de l’administration d’Etat (<i>Okmànyirodà</i>).</p>
<p>Les frais liés à la procédure auprès de de l’autorité nationale des transports sont normalement compris entre 23 000 et 40 000 forints (environ 75–130 euros) et ceux pour l’établissement de la carte verte d’autorisation de circulation et de la plaque d’immatriculation auprès des services de l’administration d’Etat (<i>Okmànyirodà</i>) sont d’environ 25 000 forints (soit environ 82 euros).</p>
<p>Depuis le 1er janvier 2014, un droit de transfert doit être également versé, suivant les mêmes conditions que les droits de transfert suite à la vente d’un véhicule à l’intérieur du pays. Pour le calcul des droits de transfert vous pouvez consulter le site suivant :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://kalkulatorlap.hu/atirasi_koltseg_kalkulator.html" class="spip_out" rel="external">Kalkulatorlap.hu</a> </p>
<p>L’assurance du véhicule doit être contractée avant de procéder aux démarches auprès des services de l’administration d’Etat (<i>Okmànyirodà</i>), qui en exigeront une attestation.</p>
<p>Document nécessaires pour la taxe d’enregistrement :</p>
<ul class="spip">
<li>La facture d’achat du véhicule s’il a été acheté auprès d’une société, ainsi que le contrat de vente, en deux langues dont le hongrois ;</li>
<li>Document d’autorisation de circulation étranger (carte grise) ;</li>
<li>feuille signalétique ;</li>
<li>document technique du véhicule établi par l’autorité nationale des transports ;</li>
<li>Numéro d’identification VPID délivré par les services des douanes ;</li>
<li>Documents nécessaires dans le cas d’une entreprise:extrait de registre, spécimen de signature ;</li>
<li>Documents nécessaires dans le cas d’un particulier : carte d’identité, carte de domiciliation, carte d’impôt.</li></ul>
<p>Calcul de la taxe d’enregistrement :</p>
<p>Le calcul de la taxe d’enregistrement s’effectue sur la base de la puissance du véhicule, de la date de première mise en circulation, de la classification environnementale et du type de carburant.</p>
<p>Pour le calcul de la taxe d’enregistrement vous pouvez consulter le site suivant :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://kalkulatorlap.hu/regisztracios_ado_kalkulator.html" class="spip_out" rel="external">Kalkulatorlap.hu</a> </p>
<p>Il n’y a pas de droit de douane à acquitter pour les véhicules provenant de France et des pays de l’Union européenne. S’il s’agit d’un véhicule neuf (véhicule de moins de six mois ou moins de 6000 km) le montant de la TVA hongroise (AFA) est exigé.</p>
<h3 class="spip"><a id="sommaire_2"></a>Taxe de performance des véhicules </h3>
<p>Chaque année le propriétaire d’un véhicule doit payer une taxe calculée sur la base de la puissance du véhicule et de son année de mise en circulation. Pour le calcul de cette taxe vous pouvez consulter le site suivant :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://kalkulatorlap.hu/teljesitmenyado_kalkulator.html" class="spip_out" rel="external">Kalkulatorlap.hu</a> </p>
<h3 class="spip"><a id="sommaire_3"></a>Permis de conduire</h3>
<p>Les permis français et international sont reconnus en Hongrie. Il est possible de passer le permis sur place.</p>
<h3 class="spip"><a id="sommaire_4"></a>Code de la route</h3>
<p><strong>Un changement de réglementation du code de la route hongrois est intervenu au 1er juillet 2011.</strong></p>
<p>Une amende devra être payée immédiatement pour les infractions suivantes :</p>
<ul class="spip">
<li>excès de vitesse</li>
<li>défaut de ceinture de sécurité</li>
<li>conduite en sens interdit</li>
<li>conduite en état d’ivresse (<strong>tolérance zéro, soit 0 g/l)</strong>.</li></ul>
<p>Les autorités sont en mesure d’infliger, sans attendre l’établissement d’un arrêt écrit, une amende dont le montant peut varier de 10 000 à 300 000 forints (environ 33 – 1000 euros). Le véhicule peut être retenu par les autorités jusqu’au paiement de ladite amende. Les autorités informeront le conducteur contrevenant par écrit, en hongrois, en anglais, en allemand ou en russe, du montant de l’amende à payer, de l’endroit où le véhicule est retenu et des moyens de s’y rendre, ainsi que des dispositions légales liées à la procédure. En cas de rétention du véhicule, la carte grise doit être remise aux autorités, en échange d’une attestation valable pour la durée de la rétention prévue. Dans le cas où la carte grise émise par les autorités étrangères ne serait pas récupérée dans les trois jours suivant la date de fin de la durée de rétention prévue, celle-ci serait envoyée à l’autorité l’ayant originellement délivrée.</p>
<p>La prudence est recommandée au passage des voies ferrées sur le réseau secondaire, la signalisation étant souvent imparfaite.</p>
<p>Le port de la ceinture de sécurité est obligatoire à l’avant et à l’arrière du véhicule, pour tout trajet. Les feux de croisement sont obligatoires lors des déplacements hors agglomération, même dans la journée. On n’observe aucun problème d’approvisionnement en carburants.</p>
<p>En cas d’accident, il est fortement recommandé de prendre immédiatement contact avec la police (tél. : 107). Munissez-vous du constat amiable européen (en version française) qui est la copie exacte du constat en hongrois. Le triangle et le gilet de signalisation sont obligatoires dans chaque véhicule.</p>
<p>Les vignettes (<i>matrica</i>) pour les autoroutes sont obligatoires et disponibles dans les stations-services à l’entrée des autoroutes, ainsi qu’à proximité des postes frontières. la vignette étant électronique, seul le reçu peut servir de justificatif. Pour plus d’information vous pouvez consulter le site suivant en anglais :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="https://en-ematrica.autopalya.hu/" class="spip_out" rel="external">En-ematrica.autopalya.hu</a> </p>
<p>La conduite s’effectue à droite et en l’absence de signalisation spécifique la priorité à droite est la règle. La liberté de circulation est totale. La vitesse maximale autorisée est de 50 km/h en agglomération, 90 km/h sur route et 130 km/h sur autoroute.</p>
<h3 class="spip"><a id="sommaire_5"></a>Assurances et taxes</h3>
<p>Assurance au tiers obligatoire.</p>
<h3 class="spip"><a id="sommaire_6"></a>Achat et location</h3>
<p>Toutes les grandes marques de voitures françaises et étrangères sont représentées en Hongrie. On peut acheter sur place un véhicule français à un tarif 10 % inférieur à celui pratiqué en France.</p>
<p>La plupart des grandes enseignes de location de véhicules sont représentés en Hongrie. Les tarifs sont comparables à ceux pratiqués en France.</p>
<h3 class="spip"><a id="sommaire_7"></a>Contrôle et entretien</h3>
<p>Le contrôle technique des véhicules est obligatoire et doit être effectué selon l’échéancier suivant : quatre ans après la mise en service du véhicule, puis tous les trois ans pour les véhicules de moins de six ans et tous les deux ans pour les véhicules plus anciens.</p>
<p>Il est possible de faire entretenir son véhicule sur place. Le service rendu est de qualité correcte pour un coût moins élevé qu’en France. Par contre les délais d’acheminement des pièces détachées sont parfois assez longs, en fonction des stocks.</p>
<h3 class="spip"><a id="sommaire_8"></a>Réseau routier</h3>
<p>Le réseau autoroutier est de bonne qualité mais les réseaux secondaire et urbain sont de qualité moyenne (chaussée déformée, nids-de-poule, signalisation des travaux parfois insuffisante).</p>
<h3 class="spip"><a id="sommaire_9"></a>Transports en commun</h3>
<p><strong>Transports en commun à Budapest </strong><br class="autobr">Le réseau de transports en commun de la capitale se compose de lignes d’autobus, de tramways et de trolleybus, qui circulent entre 4h30 et 23h00, de quatre lignes de métro, accessibles de 4h30 à 23h10 et de trains de banlieue (HÉV).</p>
<p>Les trois trains de banlieue (HÉV) partent de différents points de la capitale en direction de Csepel et Ráckeve vers le sud, Szentendre vers le nord et Csömör et Gödöllõ vers l’est. Les titres de transport sont vendus dans les stations de métro, aux distributeurs automatiques, dans certains kiosques à journaux et petits commerces. Le même ticket simple qui doit être composté avant le transport, est valable pour le bus, le trolleybus, le métro, le tramway, ainsi que pour le HÉV pour les trajets à l’intérieur de Budapest. Pour consulter les différents tarifs et pour toute information sur les conditions de transport en commun à Budapest veuillez consulter le site en anglais suivant :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.bkv.hu/en/" class="spip_out" rel="external">Bkv.hu</a></p>
<p>Dans les moyens de transport et à l’entrée du métro, des contrôleurs munis de brassards et de cartes professionnelles peuvent contrôler votre titre de transport.</p>
<p>La <strong>carte Budapest</strong> valable pour deux ou trois jours, et qui offre de nombreux avantages aux touristes, permet également d’utiliser gratuitement les transports en commun.</p>
<h3 class="spip"><a id="sommaire_10"></a>Transport ferroviaire </h3>
<p>Le réseau ferroviaire est satisfaisant.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.mavcsoport.hu/" class="spip_out" rel="external">Informations sur les lignes nationales et internationales</a></p>
<h3 class="spip"><a id="sommaire_11"></a>Transport aérien</h3>
<p>Il n’existe pas de liaison aérienne interne. Outre, l’aéroport international Liszt Ferenc de Budapest, il existe quatre autres aéroports internationaux en Hongrie (Debrecen, Györ, Sàrmellék et Pécs), dont le nombre de passagers est toutefois très faible.</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/hongrie/vie-pratique/article/transports). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
