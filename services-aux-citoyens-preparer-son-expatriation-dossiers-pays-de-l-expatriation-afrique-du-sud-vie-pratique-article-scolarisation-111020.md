# Scolarisation

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/vie-pratique/article/scolarisation-111020#sommaire_1">Le système scolaire français à l’étranger</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/vie-pratique/article/scolarisation-111020#sommaire_2">Les établissements scolaires français en Afrique du Sud</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/vie-pratique/article/scolarisation-111020#sommaire_3">Enseignement supérieur</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Le système scolaire français à l’étranger</h3>
<p>Pour toute information sur la scolarisation dans le système français à l’étranger, vous pouvez consulter notre thématique sur les <a href="services-aux-citoyens-preparer-son-expatriation-etudes-superieures.md" class="spip_in">Etudes supérieures</a> et la <a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais.md" class="spip_in">Scolarité en français</a>.</p>
<p>Vous y trouverez des renseignements sur :</p>
<ul class="spip">
<li>les établissements français du primaire et du secondaire à l’étranger ;</li>
<li>les bourses scolaires et la prise en charge des frais de scolarité à l’étranger ;</li>
<li>les possibilités qui s’offrent à vous si votre enfant ne peut être scolarisé à l’étranger dans le système français (enseignement à distance par le CNED, programme français langue maternelle (FLAM), internats en France) ;</li>
<li>les épreuves du baccalauréat à l’étranger ;</li>
<li>les bourses d’études supérieures en France et à l’étranger ;</li>
<li>l’équivalence des diplômes.</li></ul>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.aefe.fr/" class="spip_out" rel="external">Agence pour l’enseignement français à l’étranger</a></p>
<h3 class="spip"><a id="sommaire_2"></a>Les établissements scolaires français en Afrique du Sud</h3>
<p>Trois établissements scolaires français sont installés en Afrique du Sud :</p>
<ul class="spip">
<li><a href="http://www.lyceejulesverne.co.za/" class="spip_out" rel="external">Lycée Jules Verne à Johannesburg</a></li>
<li><a href="http://www.lyceejulesverne.co.za/" class="spip_out" rel="external">Ecole primaire de Pretoria (antenne du Lycée J. Verne)</a></li>
<li><a href="http://www.ecolefrancaiseducap.co.za/fr/" class="spip_out" rel="external">Lycée François le Vaillant au Cap</a></li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Enseignement supérieur</h3>
<p>L’Afrique du Sud dispose de nombreuses universités de qualité, parmi lesquelles peuvent être citées :</p>
<ul class="spip">
<li>La <a href="http://www.wits.ac.za/" class="spip_out" rel="external">WITS University</a> (université de Witwatersrand), située à Johannesburg, est la plus importante et prestigieuse université du pays.</li>
<li><a href="http://www.unisa.ac.za/" class="spip_out" rel="external">UNISA</a> est la plus grande université du continent africain. Elle compte plus de 300 000 étudiants, originaire d’environ 130 pays. Fondée au Cap, elle est située à Pretoria.</li>
<li><a href="http://www.uct.ac.za/" class="spip_out" rel="external">UCT, University of Cape Town</a>, est la plus vieille université d’Afrique du Sud.</li>
<li>L’<a href="http://www.sun.ac.za/Home.aspx" class="spip_out" rel="external">université de Stellenbosh</a> permet à ses étudiants de passer leurs examens en anglais ou en afrikaans.</li>
<li><a href="http://www.uj.ac.za/EN/Pages/Home.aspx" class="spip_out" rel="external">UJ, University of Johannesburg</a>, a été créée en 2005 suite à la fusion de trois universités (Rand Afrikaans University, <a href="http://en.wikipedia.org/wiki/Technikon_Witwatersrand" class="spip_out" rel="external">Technikon Witwatersrand</a> et Vista University).</li>
<li><a href="http://web.up.ac.za/" class="spip_out" rel="external">UP, University of Pretoria</a>, est l’une des plus anciennes universités du pays et accueille la seule école vétérinaire d’Afrique du Sud. Souvent appelée TUKS (ancien nom).</li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/vie-pratique/article/scolarisation-111020). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
