# Vaccination

<p>Aucune vaccination n’est exigée à l’entrée du pays mais il est conseillé d’avoir les mêmes vaccins qu’en France.</p>
<p>Il est préférable de réaliser toutes les vaccinations nécessaires avant de partir.</p>
<p>Pour en savoir plus, lisez notre <a href="services-aux-citoyens-preparer-son-expatriation-sante.md" class="spip_in">article thématique sur la vaccination</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/entree-et-sejour/article/vaccination-109842). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
