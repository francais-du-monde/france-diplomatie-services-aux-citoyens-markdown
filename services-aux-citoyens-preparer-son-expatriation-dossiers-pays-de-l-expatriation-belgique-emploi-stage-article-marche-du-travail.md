# Marché du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/emploi-stage/article/marche-du-travail#sommaire_1"> Secteurs à fort et faible potentiel </a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/emploi-stage/article/marche-du-travail#sommaire_2">Barèmes de rémunération </a></li></ul>
<p>L’accès à l’emploi est parfois subordonné à la possession de diplômes ou de qualifications professionnelles et connaissances linguistiques. Une connaissance satisfaisante des langues nationales peut être exigée.</p>
<p>En Belgique, il existe une liste de professions dont l’accès est réglementé. Certains emplois, qui ont trait à l’exercice de l’autorité publique et la sauvegarde des intérêts généraux de l’Etat, peuvent être réservés à des nationaux.</p>
<p>Vous pouvez consulter, profession par profession, les dispositions légales et les formalités à remplir sur les sites :</p>
<ul class="spip">
<li><a href="http://www.belgium.be/fr/economie/entreprise/professions_reglementees/" class="spip_out" rel="external">Portail Belgium.be</a></li>
<li><a href="http://www.economie.fgov.be/fr/" class="spip_out" rel="external">Ministère de l’économie</a></li>
<li><a href="http://www.be.brussels/" class="spip_out" rel="external">Portail de la région de Bruxelles Capitale</a></li></ul>
<p>Tout renseignement au sujet des professions réglementées peut être obtenu auprès du :<br class="manualbr"><strong>SPF Economie, P.M.E., Classes Moyennes et Energie</strong></p>
<p>Service des professions commerciales et artisanales et de l’organisation des classes moyennes<br class="manualbr">WTC Tour III, 26ème étage, Boulevard Simon Bolivar 30, à 1000 Bruxelles<br class="manualbr">Tél. : (+32.2) 208 51 86<br class="manualbr">Fax : (+32.2) 208 51 80</p>
<h3 class="spip"><a id="sommaire_1"></a> Secteurs à fort et faible potentiel </h3>
<p>« Europe 2020 », la nouvelle stratégie à long terme de l’Union européenne pour une économie forte et durable offrant de nombreux d’emplois, prévoit un taux d’emploi de 75% pour les Européens âgés de 20 à 64 ans d’ici 2020. La Belgique, comme tous les Etats membres de l’Union européenne, s’est fixé un objectif national : 73,2%. Les indicateurs clés du marché du travail en Belgique publiés pour l’année 2011 par le SPF Emploi, Travail et Concertation sociale, montrent que le pays connait un retard important sur la moyenne européenne, avec un taux d’emploi de 67,3 %.</p>
<p>7,6 % de la population active était à la recherche d’un travail en 2012, avec un taux très élevé chez les jeunes (18,7 %) et avec de fortes disparités géographiques (17,2% à Bruxelles, 4,3% en Flandre, et 9,5 % en Wallonie)</p>
<p>Certains sites indiquent les métiers connaissant une pénurie en Belgique :</p>
<ul class="spip">
<li><a href="http://www.leforem.be/index.html" class="spip_out" rel="external">Service public wallon de l’emploi et de la formation</a> / Particuliers / Chercher un emploi / Découvrir les métiers porteurs / Tous les métiers qui manquent de bras</li>
<li><a href="http://www.belgium.be/" class="spip_out" rel="external">Portail Belgium.be</a> / Emploi / Recherche d’emploi / Marché du travail / Métiers en pénurie</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Barèmes de rémunération </h3>
<p>Les salaires sont fixés dans les conventions collectives de travail (accords conclus entre les syndicats et les représentants des employeurs). Depuis le 1er octobre 2013, le revenu minimum mensuel moyen s’élève à :</p>
<ul class="spip">
<li>1 501 € pour les travailleurs de 21 ans et plus ;</li>
<li>1 542 € pour les travailleurs de 21 ans et demi, comptant six mois d’ancienneté ;</li>
<li>1 559 € pour les travailleurs de 22 ans, comptant 12 mois d’ancienneté.</li></ul>
<p>En Belgique, on parle de salaire net et de salaire brut. Le salaire net est la somme effectivement perçue par le travailleur. Il est égal au salaire brut moins les cotisations de sécurité sociale et les avances pour impôts (précompte professionnel), directement prélevées par l’employeur.</p>
<p>Pour obtenir des <strong>informations sur les salaires</strong>, vous pouvez consulter le site suivant :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.emploi.belgique.be/home.aspx" class="spip_out" rel="external">Service public fédéral - Emploi, Travail et concertation sociale</a> / Guide de A à Z / Rémunération du travail</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/emploi-stage/article/marche-du-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
