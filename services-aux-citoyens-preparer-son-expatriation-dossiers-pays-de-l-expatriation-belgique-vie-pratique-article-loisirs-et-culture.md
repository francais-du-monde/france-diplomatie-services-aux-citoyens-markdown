# Loisirs et culture

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/vie-pratique/article/loisirs-et-culture#sommaire_1">Tourisme </a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/vie-pratique/article/loisirs-et-culture#sommaire_2">Activités culturelles</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/vie-pratique/article/loisirs-et-culture#sommaire_3">Sports</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/vie-pratique/article/loisirs-et-culture#sommaire_4">Télévision - Radio</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/vie-pratique/article/loisirs-et-culture#sommaire_5">Presse française</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Tourisme </h3>
<p>Outre les stations balnéaires de la mer du Nord, nombre de villes belges - Bruges, Gand, Anvers, Mons, Tournai, Leuven, Malines - sont d’anciennes cités d’art et méritent d’être visitées ; les châteaux de Beloeil, du Roeulx, de Chimay également.</p>
<p>Les sites de Dinant, Huy, Namur, La Roche en Ardennes, Bouillon et Coo, les grottes de Han et de Remouchamps, les Ardennes, la région des Fagnes sont autant de lieux pittoresques qui invitent à la randonnée.</p>
<p>Pour tous renseignements, s’adresser à :</p>
<p><a href="http://www.belgique-tourisme.be/accueil/fr/index.html" class="spip_out" rel="external">Office belge de tourisme</a>  (fermé au public - contact par <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/vie-pratique/article/loisirs-et-culture#info#mc#belgique-tourisme.fr#" title="info..åt..belgique-tourisme.fr" onclick="location.href=mc_lancerlien('info','belgique-tourisme.fr'); return false;" class="spip_mail">courriel</a>)<br class="manualbr">274, boulevard Saint Germain<br class="manualbr">75007 PARIS<br class="manualbr">Tél : 01 53 85 05 20</p>
<p>Voir aussi : <a href="http://www.tourismebelgique.com/" class="spip_out" rel="external">Tourisme Belgique</a></p>
<h3 class="spip"><a id="sommaire_2"></a>Activités culturelles</h3>
<h4 class="spip">Activités socioculturelles en français</h4>
<p>Il faut bien sûr faire la distinction entre, d’une part, la région wallonne et celle de Bruxelles-Capitale où les médias français (outre les médias francophones belges) bénéficient d’une très large diffusion, et, d’autre part, la région flamande qui privilégie évidemment le néerlandais.</p>
<p>Le cinéma français est très présent, en particulier à Bruxelles et dans les régions francophones. Les films sortent généralement en même temps en France et en Belgique.</p>
<p>De nombreuses manifestations artistiques et culturelles françaises (théâtre, concert, ballet, expositions) sont régulièrement organisées.</p>
<p>L’Alliance française en Belgique dispose de plusieurs antennes locales. Instrument de promotion de la langue française et des cultures francophones, elle dispense des cours de français et propose des conférences.</p>
<p><a href="http://www.alliancefr.be/" class="spip_out" rel="external">Alliance française de Bruxelles</a> <br class="manualbr">Rue de la Loi 26<br class="manualbr">1040 Bruxelles <br class="manualbr">Tél : (32 2) 502 46 49<br class="manualbr">Fax : (32 2) 736 47 00</p>
<p>Les coordonnées des différentes antennes sont disponibles sur le site.</p>
<p>Le Centre européen de langue française (CELF) a pour objectif de valoriser le français dans les institutions européennes, au moment où se pose le problème de l’équilibre linguistique au sein du système institutionnel de l’Union.</p>
<p>Le CELF dispose d’un centre de ressources documentaires sur la France et la Communauté française de Belgique : <br class="manualbr">Rue de la Loi 26<br class="manualbr">1040 Bruxelles <br class="manualbr">Tél : (32 2) 502 46 49</p>
<h4 class="spip">Activités socioculturelles locales</h4>
<p>Les programmes de la radio belge sont très variés et il est possible de capter plusieurs stations FM, dont sept à huit proposent de la musique classique.</p>
<p>Outre les chaînes de télévision en français, il existe plusieurs chaînes flamandes (VTM - Kanaal 2 - Ketnet/Canvas - VT4 - Kanal Z), des chaînes néerlandaises et de nombreuses autres chaînes européennes (chaînes allemandes, BBC One et BBC Two …) et américaine (CNN). Au total, le câble offre au téléspectateur plus d’une quarantaine de programmes.</p>
<p>Le système adopté est PAL Européen. La vidéo est très répandue et les clubs sont nombreux.</p>
<p>Sur le plan cinématographique, la Belgique offre les mêmes possibilités que la France, avec des salles nombreuses et confortables proposant tous les genres de films. Les films étrangers sont en général proposés en version originale, sous-titrés en français et en néerlandais à Bruxelles, doublés en Wallonie.</p>
<p>Bruxelles offre également un très grand choix de spectacles de théâtre, ballets, opéras (le Théâtre de la Monnaie mondialement connu) et concerts. La saison théâtrale s’étend de septembre à mai. De grandes expositions internationales sont organisées chaque année par les différents musées et il existe de nombreuses galeries privées.</p>
<p>La vie culturelle est également très active dans les grandes villes de Belgique comme Anvers, Bruges, Gand, Liège où se déroulent chaque année des manifestations de prestige.</p>
<p>Il existe de nombreuses bibliothèques bien fournies dans toutes les villes universitaires de Belgique.</p>
<p>Consultez l’<a href="http://www.agenda.be/" class="spip_out" rel="external">agenda culturel belge</a> (cinéma, théâtre, expositions, concerts, spectacles, etc).</p>
<h3 class="spip"><a id="sommaire_3"></a>Sports</h3>
<p>Tous les sports peuvent être pratiqués en Belgique. Les clubs sont nombreux et comparables à ceux que l’on trouve en France. L’équipement est disponible sur place.</p>
<p><a href="http://www.infosport.be/" class="spip_out" rel="external">Portail du sport belge francophone</a>.</p>
<p>Chasse et pêche nécessitent un permis. Le permis de chasse s’obtient à l’issue d’un examen écrit, organisé chaque année à la fin du mois d’avril. L’importation et la détention d’une arme de chasse sont libres pour les ressortissants de l’Union européenne possédant une carte européenne d’armes à feu, mais il est nécessaire de se procurer un permis de port d’arme auprès du commissariat de la commune de résidence et de produire alors une assurance et un certificat de bonne vie et mœurs. Les périodes de chasse sont fixées tous les ans par arrêté royal. Le permis de pêche, quant à lui, est délivré par les bureaux de poste. La période autorisée s’étend généralement de la fin mars à la fin décembre.</p>
<p><a href="http://www.chasse.be/" class="spip_out" rel="external">Portail de la chasse en Belgique</a>.</p>
<h3 class="spip"><a id="sommaire_4"></a>Télévision - Radio</h3>
<h4 class="spip">Télévision</h4>
<p>L’ensemble du territoire belge est couvert par des réseaux câblés qui diffusent, selon les régions linguistiques et selon les cablo-opérateurs, un certain nombre de chaînes en langue française : RTBF1 (La Une) - RTBF2 (la Deux) - RTL/TVI (1ère chaîne belge privée francophone) - Club RTL (2nde chaîne belge privée francophone) - TV5 - Eurosport - Arte/ la Cinquième - Be TV (équivalent de Canal +) - TF1 - France 2 - France 3). Il existe une offre complémentaire de TV numérique où on retrouve France 4, France O, BFM TV, etc…</p>
<h4 class="spip">Radio</h4>
<p>Les divers cablo-opérateurs qui se partagent la couverture du territoire offrent également, dans leur bouquet de base, la retransmission des programmes radio de France-Inter, France-Culture, France-Musique et Radio-Fréquence-Nord. Les programmes de France-Inter, Europe 1 et RTL peuvent être reçus directement dans tout le pays en modulation d’amplitude.</p>
<h3 class="spip"><a id="sommaire_5"></a>Presse française</h3>
<p>A Bruxelles, comme à Liège, de nombreuses librairies assurent la diffusion de journaux ou livres français. A Anvers, nombre de journaux, périodiques et ouvrages français sont également disponibles dans les librairies (en particulier : Librairie de l’Harmonie, 255 Mechelsesteenweg - 2018 Antwerpen), la FNAC, les kiosques ou les grandes surfaces. En province et au Limbourg, ceux-ci sont plus difficiles à trouver.</p>
<p>Pour en savoir plus et connaître les librairies francophones de Belgique, consultez le site du <a href="http://www.libraires.be/" class="spip_out" rel="external">Syndicat des libraires francophones de Belgique</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/belgique/vie-pratique/article/loisirs-et-culture). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
