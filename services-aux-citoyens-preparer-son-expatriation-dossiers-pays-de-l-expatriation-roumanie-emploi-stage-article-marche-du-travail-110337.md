# Marché du travail

<p class="chapo">
      Pour travailler en Roumanie, il est préférable de venir avec un contrat de travail français. Les opportunités d’emplois s’adressent avant tout à des candidats qualifiés (techniciens, ingénieurs, commerciaux). Il est aisé de trouver des postes à responsabilité. Les candidats peu qualifiés ont cependant des chances de trouver du travail notamment dans la filière bois et commerce de détail.
</p>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/emploi-stage/article/marche-du-travail-110337#sommaire_1">Secteurs à fort potentiel</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/emploi-stage/article/marche-du-travail-110337#sommaire_2">Professions règlementées</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/emploi-stage/article/marche-du-travail-110337#sommaire_3">Rémunération</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Secteurs à fort potentiel</h3>
<p>Les secteurs à fort potentiel relèvent principalement du secteur tertiaire (finance, commerce, marketing, télécommunications, informatique, comptabilité, audits, communication) et de l’industrie. Le secteur public n’est pas accessible aux ressortissants étrangers.</p>
<p><strong>L’automobile</strong> est un secteur clé, avec un chiffre d’affaires qui dépasse les 8% du PIB roumain, et qui représente 60% des parts de marché de la France et 200 000 emplois.</p>
<p><strong>Les infrastructures de transport </strong> (ferroviaires, routiers, autoroutiers, aéroportuaires, maritimes), en particulier les chemins de fer, nécessitent un développement accru, soutenu fortement d’ailleurs par l’Union européenne.</p>
<p><strong>Les télécommunications </strong> sont un secteur concurrentiel et en plein essor. Orange est le leader de ce marché qui est estimé à 3,6 Mds d’euros en 2012.</p>
<h3 class="spip"><a id="sommaire_2"></a>Professions règlementées</h3>
<p>Une profession réglementée est une profession dont l’accès et son exercice sont subordonnés à la possession d’une qualification professionnelle spécifique.</p>
<ul class="spip">
<li>Agent de sécurité (certificat sanctionnant un cycle d’études secondaires)</li>
<li>Médecin spécialiste en allergologie et immunologie clinique</li>
<li>Médecin spécialiste en anatomie pathologique</li>
<li>Médecin spécialiste en anesthésie et thérapie intensive</li>
<li>Architecte (formation d’architecte)</li>
<li>Architecte (droits acquis)</li>
<li>Archiviste (diplôme certifiant des études d’enseignement post-secondaire, quatre ans)</li>
<li>Assistant médical généraliste (formation d’infirmière responsable de soins généraux)</li>
<li>Assistant social (diplôme sanctionnant une formation du niveau de l’enseignement post-secondaire)</li>
<li>Assistant vétérinaire (attestation de compétence)</li>
<li>Auditeur financier (diplôme sanctionnant une formation du niveau de l’enseignement post-secondaire)</li>
<li>Avocat (diplôme sanctionnant une formation du niveau de l’enseignement post-secondaire)</li>
<li>Médecin (formation médicale de base)</li>
<li>Biochimiste dans le système de santé (diplôme sanctionnant une formation du niveau de l’enseignement post-secondaire)</li>
<li>Biologiste dans le système de santé (diplôme sanctionnant une formation du niveau de l’enseignement post-secondaire)</li>
<li>Médecin en maladies infectieuses (formation de médecin spécialiste)</li>
<li>Enseignant/Assistant de l’université et des grandes écoles (diplôme sanctionnant une formation du niveau de l’enseignement post-secondaire)</li>
<li>Cardiologue (formation de médecin spécialiste)</li>
<li>Chimiste dans le système de santé (diplôme certifiant des études au niveau d’enseignement post-secondaire (exactement quatre ans)</li>
<li>Médecin en chirurgie générale (formation de médecin spécialiste)</li>
<li>Médecin en chirurgie pédiatrique (formation de médecin spécialiste)</li>
<li>Médecin en chirurgie plastique (formation de médecin spécialiste)</li>
<li>Médecin en chirurgie thoracique (formation de médecin spécialiste)</li>
<li>Médecin en chirurgie vasculaire (formation de médecin spécialiste)</li>
<li>Conducteur poids lourds (attestation de compétence)</li>
<li>Transporteur routier de voyageurs (attestation de compétence)</li>
<li>Chauffeur de taxi agréé / Conducteur de camionnette (attestation de compétence)</li>
<li>Chauffeur routier pour le transport de matières dangereuses (attestation de compétence)</li>
<li>Agent de probation et de libération conditionnelle (PS3 – diplôme sanctionnant une formation du niveau de l’enseignement post-secondaire 3-4 ans)</li>
<li>Contrôleur technique de poids lourds pour le transport de marchandises dangereuses (attestation de compétence)</li>
<li>Conseiller juridique (PSM - diplôme certifiant des études au niveau d’enseignement post-secondaire, plus que quatre ans)</li>
<li>Expert-comptable et conseiller fiscal autres que contrôleurs de comptes (diplôme certifiant des études au niveau d’enseignement post-secondaire, plus que quatre ans)</li>
<li>Contrôleur de comptes/Expert-comptable (certificat sanctionnant un cycle d’études secondaires)</li>
<li>Aiguilleur du ciel (attestation de compétence)</li>
<li>Médecin spécialiste en dermato-vénéréologie (formation de médecin spécialiste)</li>
<li>Détective privé (diplôme sanctionnant une formation du niveau de l’enseignement post-secondaire)</li>
<li>Médecin spécialiste en endocrinologie (formation de médecin spécialiste)</li>
<li>Expert immobilier/gérant/agent immobilier/conseil juridique (diplôme sanctionnant une formation du niveau de l’enseignement post-secondaire, 3-4 ans)</li>
<li>Expert-comptable et conseiller fiscal (diplôme certifiant des études au niveau d’enseignement post-secondaire de quatre ans)</li>
<li>Expert technique judiciaire (diplôme sanctionnant une formation du niveau de l’enseignement post-secondaire, 3-4 ans)</li>
<li>Pharmacien (formation de pharmacien général)</li>
<li>Médecin spécialiste en pharmacologie clinique (formation de médecin spécialiste)</li>
<li>Gastroentérologue (formation de médecin spécialiste)</li>
<li>Médecin spécialiste en génétique médicale (formation de médecin spécialiste)</li>
<li>Médecin spécialiste en gériatrie et gérontologie (formation de médecin spécialiste)</li>
<li>Guide touristique (certificat sanctionnant un cycle d’études secondaires)</li>
<li>Médecin spécialiste en hématologie (formation de médecin spécialiste)</li>
<li>Contrôleur technique de véhicules (diplôme sanctionnant une formation du niveau de l’enseignement post-secondaire)</li>
<li>Moniteur d’auto-école (attestation de compétence)</li>
<li>Interprète en langue des signes (certificat sanctionnant un cycle d’études secondaires)</li>
<li>Navigateur aérien (attestation de compétence)</li>
<li>Médiateur maritime (diplôme sanctionnant une formation du niveau de l’enseignement post-secondaire, 3-4 ans)</li>
<li>Médecin spécialiste en médecin de famille (formation en médecine générale)</li>
<li>Médecin spécialiste en médecine du travail (formation de médecin spécialiste)</li>
<li>Médecin spécialiste en médecine de laboratoire (formation de médecin spécialiste)</li>
<li>Médecin urgentiste (formation de médecin spécialiste)</li>
<li>Médecin interne (formation de médecine spécialiste)</li>
<li>Médecin spécialiste en médecine nucléaire (formation de médecin spécialiste)</li>
<li>Sage-femme (formation de sage-femme)</li>
<li>Navigateur vol air (certificat sanctionnant un cycle d’études secondaires)</li>
<li>Médecin spécialiste en néphrologie (formation de médecin spécialiste)</li>
<li>Neurochirurgien (formation de médecin spécialiste)</li>
<li>Neurologue (formation de médecin spécialiste)</li>
<li>Médecin obstétricien-gynécologue (formation de médecin spécialiste)</li>
<li>Médecin ophtalmologue (formation de médecin spécialiste)</li>
<li>Médecin oncologue (formation de médecin spécialiste)</li>
<li>Opérateur radio de l’aviation (certificat sanctionnant un cycle d’études secondaires)</li>
<li>Médecin spécialiste en orthopédie et traumatologie (formation de médecin spécialiste)</li>
<li>Médecin spécialiste en otorhinolaryngologie (formation de médecin spécialiste)</li>
<li>Médecin pédiatre (formation de médecin spécialiste)</li>
<li>Aérostier, pilote de ballon (attestation de compétence)</li>
<li>Pilote d’avion (certificat sanctionnant un cycle d’études secondaires)</li>
<li>Pilote d’hélicoptère (certificat sanctionnant un cycle d’études secondaires)</li>
<li>Pyrotechnicien (attestation de compétence)</li>
<li>Pneumologue (formation de médecin spécialiste)</li>
<li>Liquidateur judiciaire (attestation de compétence)</li>
<li>Enseignant de la législation routière (diplôme certifiant d’études au niveau d’enseignement post-secondaire, quatre ans)</li>
<li>Enseignant dans l’enseignement secondaire (diplôme sanctionnant une formation du niveau de l’enseignement post-secondaire, 3-4 ans)</li>
<li>Médecin psychiatre (formation de médecin spécialiste)</li>
<li>Médecin en psychiatrie pédiatrique (formation de médecin spécialiste)</li>
<li>Psychologue (diplôme sanctionnant une formation du niveau de l’enseignement post-secondaire 3-4 ans)</li>
<li>Médecin spécialiste en radiologie-imagerie médicale (formation de médecin spécialiste)</li>
<li>Médecin spécialiste en radiothérapie (formation de médecin spécialiste)</li>
<li>Médecin spécialiste en récupération, médecine physique et balnéothérapie (formation de médecin spécialiste)</li>
<li>Conservateur/restaurateur patrimoine (attestation de compétence)</li>
<li>Médecin spécialiste en rhumatologie (formation de médecin spécialiste)</li>
<li>Praticien de l’art dentaire, spécialisé en chirurgie dento-alvéolaire</li>
<li>Praticien de l’art dentaire, spécialisé dans l’orthodontie et l’orthopédie dentofaciale</li>
<li>Médecin spécialiste en santé publique et management (formation de médecin spécialiste)</li>
<li>Prothésiste dentaire (diplôme sanctionnant une formation du niveau de l’enseignement post-secondaire, 3-4 ans)</li>
<li>Traducteur / Interprète (diplôme sanctionnant une formation du niveau de l’enseignement post-secondaire, 3-4 ans)</li>
<li>Urbaniste / spécialiste agrée de l’aménagement du territoire (diplôme certifiant des études au niveau d’enseignement post-secondaire de quatre ans)</li>
<li>Médecin urologue (formation de médecin spécialiste)</li>
<li>Vétérinaire (formation de vétérinaire)</li></ul>
<p>Source : <a href="http://ec.europa.eu/internal_market/qualifications/regprof/index.cfm" class="spip_out" rel="external">Ec.europa.eu</a>.</p>
<h3 class="spip"><a id="sommaire_3"></a>Rémunération</h3>
<p>Au 1er janvier 2013, le salaire minimum mensuel brut en Roumanie est de 157,50 euros. Le salaire moyen nominal net au mois de juillet 2013 était de 367 euros, mais pour un même poste, les salaires peuvent varier selon un rapport de 1 à 20. Les emplois de cadre commercial polyvalent à 1 500 euros sont considérés comme exceptionnels et recherchés.</p>
<p>Sources : <a href="http://epp.eurostat.ec.europa.eu/portal/page/portal/eurostat/home/" class="spip_out" rel="external">Eurostat</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/emploi-stage/article/marche-du-travail-110337). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
