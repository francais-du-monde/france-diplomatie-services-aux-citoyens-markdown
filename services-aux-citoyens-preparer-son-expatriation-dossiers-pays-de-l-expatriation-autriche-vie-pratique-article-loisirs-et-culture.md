# Loisirs et culture

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/vie-pratique/article/loisirs-et-culture#sommaire_1">Tourisme</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/vie-pratique/article/loisirs-et-culture#sommaire_2">Activités culturelles</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/vie-pratique/article/loisirs-et-culture#sommaire_3">Sports</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/vie-pratique/article/loisirs-et-culture#sommaire_4">Presse française</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Tourisme</h3>
<p>Le tourisme est très développé et la variété des paysages offre des possibilités d’excursions intéressantes et nombreuses. Les villes (Vienne, Salzbourg notamment) sont particulièrement riches en monuments historiques.</p>
<p>Pour plus de renseignements, adressez-vous à :</p>
<p><a href="http://www.austria.info/fr" class="spip_out" rel="external">Office National Autrichien du Tourisme - Maison de I’Autriche</a><br class="manualbr">22, rue de Caumartin (fermé au public)<br class="manualbr">75009 Paris<br class="manualbr">Tél. : +33 (0)1 53 83 95 30<br class="manualbr">Fax : +33 (0)1 45 61 97 67<br class="autobr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/vie-pratique/article/loisirs-et-culture#vacances#mc#austria.info#" title="vacances..åt..austria.info" onclick="location.href=mc_lancerlien('vacances','austria.info'); return false;" class="spip_mail">Courriel</a></p>
<p>Cet organisme a pour vocation de promouvoir et informer sur le tourisme, l’hébergement et les manifestations culturelles en Autriche.</p>
<h3 class="spip"><a id="sommaire_2"></a>Activités culturelles</h3>
<h4 class="spip">Activités culturelles en français</h4>
<p>Elles sont essentiellement proposées par les Instituts français de Vienne, et Innsbruck : concerts, conférences, débats, expositions intra et extra-muros, cinéma, prêt de livres et de vidéos, presse et ouvrages divers en ligne, etc.</p>
<p>Le cinéma français est parfois diffusé dans le circuit commercial local, exceptionnellement en version originale.</p>
<p><a href="http://institut-francais.at/vienne/" class="spip_out" rel="external">Institut Français de Vienne</a><br class="manualbr">Währingerstrasse 30<br class="manualbr">1090 Wien<br class="manualbr">Tel. : (+43) (0)1 50 27 53 00<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/vie-pratique/article/loisirs-et-culture#info#mc#institutfr.at#" title="info..åt..institutfr.at" onclick="location.href=mc_lancerlien('info','institutfr.at'); return false;" class="spip_mail">Courriel</a></p>
<p><strong>Institut Français d’Innsbruck</strong><br class="manualbr">Herzog-Friedrich-Strasse 3<br class="manualbr">Bâtiment "Claudiana", 1er étage<br class="manualbr">6020 Innsbruck<br class="manualbr">Tel. : +4351250739152<br class="manualbr">Fax : +4351250739159<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/vie-pratique/article/loisirs-et-culture#info#mc#ifibk.com#" title="info..åt..ifibk.com" onclick="location.href=mc_lancerlien('info','ifibk.com'); return false;" class="spip_mail">Courriel</a></p>
<p>Dans d’autres villes (Graz, Linz, Klagenfurt, Eisenstadt, Salzbourg, Wels), des associations France – Autriche proposent ponctuellement des activités culturelles en français.</p>
<h4 class="spip">Activités culturelles locales</h4>
<p>L’Autriche offre une grande diversité d’activités culturelles. Vienne, en particulier, propose des spectacles de qualité dans tous les domaines (théâtre, opéra, concerts, cinéma, expositions, etc). De nombreux musées, très riches, peuvent être visités, tels le Musée d’Art Moderne et l’Albertina. Les grandes institutions culturelles viennoises offrent toutefois des places à un coût nettement supérieur aux institutions comparables en France.</p>
<p>Le pays offre par ailleurs un réseau important de bibliothèques populaires.</p>
<p>Des festivals sont organisés dans tout le pays, notamment à Vienne (Wiener Festwochen, Wien Modern), à Salzbourg (chaque été), à Graz (Steirischer Herbst), à Bregenz, en Carinthie (Festival d’Ossiach).</p>
<p>L’importance donnée à la musique est considérable et les orchestres de musique classique sont de renommée internationale.</p>
<p>La télévision dispose de trois chaînes (ORF1,ORF2 et ORF 3). Le système adopté est PAL. Il est possible de capter des chaînes françaises par le câble (TV5, ARTE, France 24 - etc.) ou sur satellite (TF1, France 2, M6, ARTE, TELE MONTE CARLO, etc.). La réception par satellite permet évidemment, là où une antenne parabolique peut être installée et dirigée vers le bon satellite, de recevoir toutes les chaînes françaises. Le coût d’un bouquet satellitaire est supérieur à l’équivalent en France.</p>
<h3 class="spip"><a id="sommaire_3"></a>Sports</h3>
<p>Tous les sports peuvent être pratiqués. En hiver, il s’agit plus particulièrement du ski alpin, du ski de fond, de la luge, du patin à glace, du hockey sur glace. En été et pendant les saisons intermédiaires, ces activités cèdent le pas à l’alpinisme, le tennis, la natation, l’équitation, le golf ou le canoë. Les rencontres sportives sont surtout locales, très peu de rencontres internationales sont organisées, à l’exception des sports d’hiver.</p>
<p>La chasse et la pêche peuvent être pratiquées avec un permis. Le permis de chasse est délivré à l’issue d’une formation suivie d’un examen. S’adresser à l’organisme local de formation (pour Vienne : Wiener Landesjagdverband-Stadioncenter Olympiaplatz 2, <a href="http://www.jagd-wien.at/" class="spip_out" rel="external">Jagd-wien.at</a>). Période d’ouverture : du 16 mai au 16 octobre (période principale).</p>
<p>La licence de pêche est délivrée par les organismes locaux pour Vienne :</p>
<ul class="spip">
<li>Bezirkhauptmannschaft Wien, 3400 Klosterneuburg, Leopoldstraße</li>
<li>Fischereirevierverband V-Wiener Neustadt Johannesg 23 2500 Baden</li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Presse française</h3>
<p>Elle est plus difficile à trouver qu’auparavant sous forme papier, du fait du développement des supports numériques. Les grands quotidiens restent toutefois disponibles dans les magasins de journaux.</p>
<p><i>Mise à jour : février 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/vie-pratique/article/loisirs-et-culture). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
