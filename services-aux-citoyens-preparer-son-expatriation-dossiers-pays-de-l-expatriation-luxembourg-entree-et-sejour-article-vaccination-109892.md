# Vaccination

<p>Aucune vaccination n’est obligatoire pour se rendre au Luxembourg.</p>
<p>Cependant, il est recommandé de mettre à jour les vaccinations suivantes : diphtérie, poliomyélite, tétanos et hépatite B.</p>
<p>Pour plus d’informations :</p>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-sante.md" class="spip_in">notre thématique sur la vaccination</a>.</li>
<li><a href="http://www.sante.lu/" class="spip_out" rel="external">Sante.lu</a></li></ul>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/luxembourg/entree-et-sejour/article/vaccination-109892). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
