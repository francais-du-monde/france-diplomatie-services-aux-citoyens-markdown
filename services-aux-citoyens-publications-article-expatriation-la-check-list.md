# Expatriation : la Check-list

<p>La Check-list de l’expatriation a été créée spécialement pour vous permettre de vérifier en un coup d’œil toutes les formalités administratives à réaliser avant votre départ, une fois sur place et avant de revenir en France.</p>
<p>Vous y trouverez également les contacts des organismes pouvant vous accompagner dans vos démarches.</p>
<iframe src="http://www.diplomatie.gouv.fr/fr/spip.php?page=pdfjs&amp;id_document=93322" width="100%" height="700" class="lecteurpdf lecteufpdf-93322spip_documents"></iframe>
<p>Pour imprimer le document, téléchargez la version PDF :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/checklist_reduc03062016_cle85ab64.pdf" class="spip_in" type="application/pdf">Check-list de l’expatriation</a></p>
<p><i>Mise à jour : juin 2016</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/publications/article/expatriation-la-check-list). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
