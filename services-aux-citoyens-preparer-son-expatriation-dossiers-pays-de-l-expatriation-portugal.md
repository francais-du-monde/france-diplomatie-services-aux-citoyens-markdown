# Portugal

<p>Au 31 décembre 2014, <strong>15 181 Français</strong> étaient inscrits au registre des Français établis hors de France (9829 à Lisbonne et 5352 à Porto).</p>
<p><strong>Estimée à quelques 30 000 personnes</strong>, la communauté française compte une forte proportion de binationaux. La majorité des Français sont établis à Lisbonne et Porto. Un millier environ résident dans le sud du pays. En 2011, le Portugal a reçu 1 900 000 visiteurs français sur l’ensemble du territoire (5ème destination favorite des Français). L’importance du français dans l’éducation jusqu’à une époque récente et la densité des liens avec la France fondés sur l’émigration des années 70 y entretiennent une francophonie encore bien vivante.</p>
<p>Un peu plus <strong>de 400 entreprises portugaises à participation française</strong> ont été recensées par la mission économique Ubifrance de Lisbonne dans le cadre de l’enquête filiale 2009 : si la présence française se réduit dans les secteurs traditionnels d’activité au Portugal que sont le textile et l’habillement, elle se maintient dans les secteurs qui font la force de l’industrie française : la chimie et l’environnement les laboratoires pharmaceutiques, les équipementiers de l’automobile, l’équipement et l’installation électrique et électronique, l’agroalimentaire, etc.</p>
<p>Selon le classement 2011 des 500 premières entreprises portugaises publié par la revue Exame, 31 filiales directes ou indirectes d’entreprises à capitaux français figurent parmi les 500 premières sociétés en termes de chiffre d’affaires au Portugal.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/dossiers-pays/portugal/" class="spip_in">Une description du Portugal, de sa situation politique et économique</a> ;</li>
<li><a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/portugal/" class="spip_in">Des informations actualisées sur les <strong>conditions locales de sécurité</strong> au Portugal</a> ;</li>
<li><a href="http://www.ambafrance-pt.org/" class="spip_out" rel="external">Ambassade de France au Portugal</a>.</li></ul>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-entree-et-sejour.md">Entrée et séjour</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-entree-et-sejour-article-passeport-visa-permis-de-travail.md">Passeport, visa, permis de travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-entree-et-sejour-article-demenagement-111282.md">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-entree-et-sejour-article-vaccination-111283.md">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-entree-et-sejour-article-animaux-domestiques.md">Animaux domestiques</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-emploi-stage.md">Emploi, stage</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-emploi-stage-article-marche-du-travail-111285.md">Marché du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-emploi-stage-article-reglementation-du-travail-111286.md">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-emploi-stage-article-recherche-d-emploi.md">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-emploi-stage-article-curriculum-vitae.md">Curriculum vitae</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-emploi-stage-article-lettre-de-motivation-111289.md">Lettre de motivation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-emploi-stage-article-entretien-d-embauche-111290.md">Entretien d’embauche</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-emploi-stage-article-stages.md">Stages</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-protection-sociale-23030.md">Protection sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-protection-sociale-23030-article-regime-local-de-securite-sociale-111292.md">Régime local de sécurité sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-protection-sociale-23030-article-convention-de-securite-sociale-111293.md">Convention de sécurité sociale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-fiscalite.md">Fiscalité</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-fiscalite-article-fiscalite-du-pays.md">Fiscalité du pays</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-fiscalite-article-convention-fiscale-111295.md">Convention fiscale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-vie-pratique.md">Vie pratique</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-vie-pratique-article-logement-111296.md">Logement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-vie-pratique-article-sante-111297.md">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-vie-pratique-article-scolarisation.md">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-vie-pratique-article-cout-de-la-vie-111299.md">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-vie-pratique-article-transports.md">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-vie-pratique-article-communications.md">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-vie-pratique-article-loisirs-et-culture-111302.md">Loisirs et culture</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-vie-pratique-article-environnement-culturel.md">Environnement culturel</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-vie-pratique-article-pour-en-savoir-plus-111303.md">Pour en savoir plus</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-presence-francaise.md">Présence française</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-presence-francaise-article-presence-francaise.md">Présence française</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
