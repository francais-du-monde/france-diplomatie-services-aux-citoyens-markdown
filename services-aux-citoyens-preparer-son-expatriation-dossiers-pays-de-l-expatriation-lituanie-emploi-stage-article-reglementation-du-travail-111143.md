# Réglementation du travail

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/emploi-stage/article/reglementation-du-travail-111143#sommaire_1">Droit du travail</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/emploi-stage/article/reglementation-du-travail-111143#sommaire_2">Contrat de travail – spécificités</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/emploi-stage/article/reglementation-du-travail-111143#sommaire_3">Fêtes légales</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/emploi-stage/article/reglementation-du-travail-111143#sommaire_4">Emploi du conjoint</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Droit du travail</h3>
<p>Adopté en juin 2002 et entré en vigueur le 1er janvier 2003, le code du travail est récent. Il transpose la majeure partie du droit communautaire en matière de législation du travail. Il est très complet et détaille notamment les procédures en cas de litige entre l’employeur et l’employé, les droits et obligations de l’un comme de l’autre, le fonctionnement des représentations syndicales et patronales. De plus, il offre des garanties supplémentaires aux groupes de personnes particulièrement vulnérables (mineurs, personnes handicapées, femmes enceintes entre autres). Le cadre ainsi appliqué se veut à la fois rigoureux, souple et respectueux des principaux droits des salariés.</p>
<p>La liberté contractuelle est relativement étendue en la matière. La règle générale veut qu’un contrat de travail soit conclu pour une période indéterminée. De même, si le contrat ne spécifie pas explicitement de durée, il est considéré de facto à durée indéterminée. Le contrat de travail à durée déterminée existe mais il ne peut dépasser cinq ans. Tout contrat de travail doit impérativement contenir les dispositions suivantes :</p>
<ul class="spip">
<li>le lieu de travail de l’employé ;</li>
<li>les fonctions de l’employé ;</li>
<li>les conditions de rémunération pour le travail effectué ;</li>
<li>s’il y a lieu, le terme de validité du contrat et la nature de l’emploi, temporaire ou saisonnier.</li></ul>
<p>Le contrat peut par ailleurs contenir des clauses additionnelles qui résultent d’un accord entre les parties. La période d’essai est de trois mois maximum. Elle n’est pas applicable à certains types d’embauche (mineurs notamment). La rupture du contrat de travail peut se faire en raison de la démission de l’employé ou d’une procédure de licenciement initiée par l’employeur. Un employé est autorisé à démissionner avec un préavis de quatorze jours, ce délai pouvant être étendu à un mois si la convention collective le prévoit, ou réduit à trois jours dans des situations spécifiques, notamment en cas de non-respect par l’employeur de ses obligations contractuelles. L’employeur peut licencier un salarié moyennant un préavis de deux mois minimum. Lorsqu’il n’est pas motivé par une faute de l’employé, le licenciement doit être justifié par des arguments portant sur la qualification de l’employé, sa conduite sur son lieu de travail, ou par des raisons économiques ou technologiques. Le licenciement sans préavis n’est autorisé que dans des situations particulières, par exemple en cas de faute caractérisée de l’employé ou en cas de liquidation de l’entreprise. Le contrat de travail temporaire existe en Lituanie mais celui-ci ne doit pas excéder deux mois.</p>
<h3 class="spip"><a id="sommaire_2"></a>Contrat de travail – spécificités</h3>
<p>Le contrat de travail peut être fait soit dans le cadre d’un <strong>détachement</strong>, soit dans le cadre d’une <strong>expatriation</strong>.</p>
<p>Dans le cadre du détachement, l’entreprise emploie un salarié en France puis le détache à l’étranger, pour une période variable mais limitée, au terme de laquelle l’employé retrouve son poste en France. Ce contrat sera soumis au droit français.</p>
<p>Dans le cadre de l’expatriation, le salarié est recruté soit en France, soit à l’étranger pour le compte d’une entreprise implantée à l’étranger ou d’une société locale.</p>
<p>Les conditions de négociation ne seront pas les mêmes selon que l’employeur fait venir le salarié de France ou qu’il l’engage selon les modalités d’un contrat de travail local.</p>
<p>Dans le premier cas, si les parties en décident ainsi, le contrat pourra être soumis au droit français. S’il s’agit d’un contrat local, les relations de travail seront régies par le droit local.</p>
<h3 class="spip"><a id="sommaire_3"></a>Fêtes légales</h3>
<ul class="spip">
<li>1er janvier : Nouvel An</li>
<li>16 février : fête de l’indépendance de 1918</li>
<li>11 mars : déclaration de l’indépendance de 1990</li>
<li>20 et 21 avril 2014 : Pâques</li>
<li>1er mai : Jour du travail</li>
<li>24 juin : Saint Jean (Jonines)</li>
<li>6 juillet : jour de l’Etat lituanien ( commémoration du couronnement du roi Mindaugas)</li>
<li>15 août : Assomption</li>
<li>1er novembre : Toussaint</li>
<li>24 décembre : veille de Noël</li>
<li>25-26 décembre : Noël</li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Emploi du conjoint</h3>
<p>En adhérant à l’Union européenne, le 1er mai 2004, la Lituanie a été amenée à adopter le principe de libre circulation des travailleurs européens à l’intérieur de l’espace communautaire. De ce fait, l’emploi d’un Français en Lituanie ne pose en soi aucune difficulté d’ordre réglementaire.</p>
<p>Il convient toutefois, au moment de rechercher un emploi, d’être attentif aux caractéristiques du marché local, en ne négligeant pas notamment l’environnement linguistique qui peut s’avérer contraignant pour un étranger.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/lituanie/emploi-stage/article/reglementation-du-travail-111143). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
