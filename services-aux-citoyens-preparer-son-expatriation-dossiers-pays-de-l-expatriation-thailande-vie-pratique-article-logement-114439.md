# Logement

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/vie-pratique/article/logement-114439#sommaire_1">Où se loger ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/vie-pratique/article/logement-114439#sommaire_2">Conditions de location</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/vie-pratique/article/logement-114439#sommaire_3">Electricité</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/vie-pratique/article/logement-114439#sommaire_4">Electroménager</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Où se loger ?</h3>
<p>Les quartiers résidentiels de Bangkok sont éparpillés dans la ville, on peut cependant noter deux zones à forte concentration de logements résidentiels : autour de Sukhumvit, Yen Akat, Silom, Sathorn, et Wittayu.</p>
<p>Les locations sont généralement meublées.</p>
<p>Bangkok :</p>
<table class="spip">
<thead><tr class="row_first"><th id="idaa53_c0">Loyer mensuel en euros</th><th id="idaa53_c1">quartier résidentiel</th><th id="idaa53_c2">banlieue</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="idaa53_c0">Studio</td>
<td class="numeric " headers="idaa53_c1">700</td>
<td class="numeric " headers="idaa53_c2">350</td></tr>
<tr class="row_even even">
<td headers="idaa53_c0">3 pièces</td>
<td class="numeric " headers="idaa53_c1">1500</td>
<td class="numeric " headers="idaa53_c2">1100</td></tr>
<tr class="row_odd odd">
<td headers="idaa53_c0">5 pièces</td>
<td class="numeric " headers="idaa53_c1">3000</td>
<td class="numeric " headers="idaa53_c2">2000</td></tr>
<tr class="row_even even">
<td headers="idaa53_c0">Villa</td>
<td class="numeric " headers="idaa53_c1">3500</td>
<td class="numeric " headers="idaa53_c2">2000</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_2"></a>Conditions de location</h3>
<p>Les baux de location sont de un à trois ans. Il est possible de négocier un bail à loyer fixe sur trois ans mais il est alors plus difficile et plus coûteux (perte de la caution) de quitter son logement en cours de bail.</p>
<p>A l’issue du bail, le loyer est renégocié à la hausse pour chaque renouvellement.</p>
<p>Une caution de deux mois de loyers est, en général, demandée.</p>
<p>La plupart des logements sont meublés mais il est possible de demander de les enlever en accord avec le propriétaire.</p>
<p><strong>Auberges de jeunesse</strong></p>
<p>L’hébergement est également possible dans les "guesthouses" ou dans les auberges de jeunesse (YMCA ou YWCA).</p>
<h3 class="spip"><a id="sommaire_3"></a>Electricité</h3>
<p>Le courant est de 220 V, 50Hz. Les prises électriques sont généralement de type A et C -rectangulaires ou rondes- et prévues pour deux fiches plates.</p>
<h3 class="spip"><a id="sommaire_4"></a>Electroménager</h3>
<p>Les cuisines sont équipées de plaques de cuisson et four, réfrigérateur et congélateur. Tout le matériel HiFi-Video est disponible en Thaïlande. De nombreuses marques sont présentes sur le marché, aucun problème de compatibilité n’est à signaler.</p>
<p><strong>Chauffage / climatisation </strong></p>
<p>La climatisation est indispensable.</p>
<p><i>Mise à jour : juillet 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/thailande/vie-pratique/article/logement-114439). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
