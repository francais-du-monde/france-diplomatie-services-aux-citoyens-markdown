# Coût de la légalisation

<p><strong>Coût de la légalisation</strong></p>
<p>Le tarif en vigueur est de 10 € par document.</p>
<p>Le montant des droits de timbre est fixé par décret. Les frais sont directement payables au guichet soit en espèces, soit par carte bancaire, soit par chèque libellé à l’ordre de « Régie des légalisations (DFAE) », soit par virement bancaire sur le compte suivant :</p>
<p>Titulaire du compte : REGIE DES LEGALISATIONS (DFAE)<br class="autobr">IBAN : FR76 1007 1750 0000 0010 0101 542<br class="autobr">Etablissement bancaire : TRESOR PUBLIC</p>
<p><a href="http://www.diplomatie.gouv.fr/fr/IMG/pdf/decret_no_2016-92_du_1er_fevrier_2016_fixant_les_droits_de_chancellerie_cle4ea569.pdf" class="spip_in" type="application/pdf">Décret n° 2016-92 du 1er février 2016</a></p>
<p><i>Mise à jour : mars 2016</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/legalisation-et-notariat/legalisation-et-certification-de-signatures/article/cout-de-la-legalisation). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
