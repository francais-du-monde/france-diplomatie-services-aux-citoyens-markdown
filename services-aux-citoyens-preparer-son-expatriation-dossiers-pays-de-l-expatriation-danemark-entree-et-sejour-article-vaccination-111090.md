# Vaccination

<p>L’état sanitaire du Danemark, ainsi que son infrastructure médicale, sont comparables à ceux des autres pays européens. Les hôpitaux publics sont bien équipés et disposent le plus souvent d’un service d’urgences. Il y a très peu de cliniques et d’hôpitaux privés. Il n’y a pas de vaccinations obligatoires.</p>
<p>Pour obtenir la liste de médecins francophones ainsi que la liste des hôpitaux et des numéros d’urgence, consultez la section consulaire du site de l’<a href="http://www.ambafrance-dk.org/" class="spip_out" rel="external">Ambassade de France au Danemark</a>.</p>
<p>Pour en savoir plus, lisez [notre rubrique thématique <a href="services-aux-citoyens-preparer-son-expatriation-sante.md" class="spip_in">Santé</a>.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/danemark/entree-et-sejour/article/vaccination-111090). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
