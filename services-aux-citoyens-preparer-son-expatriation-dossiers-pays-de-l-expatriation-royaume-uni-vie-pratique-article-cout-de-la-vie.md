# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/vie-pratique/article/cout-de-la-vie#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/vie-pratique/article/cout-de-la-vie#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/vie-pratique/article/cout-de-la-vie#sommaire_3">Alimentation</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/vie-pratique/article/cout-de-la-vie#sommaire_4">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>L’unité monétaire est la livre sterling (£).</p>
<p>Le Royaume-Uni est l’un des trois pays de l’Union européenne (avec le Danemark et la Suède) qui ne font pas partie de la zone Euro.</p>
<p>Il est possible de se procurer des livres sterling, soit par le change sur place, soit en utilisant la voie bancaire et les transferts de fonds de compte à compte. La plupart des grandes banques françaises (BNP Paribas, Caylon, Natexis, Société Générale) sont représentées sur place mais se concentrent essentiellement sur les produits financiers et la gestion des portefeuilles d’investissement et offrent rarement des services bancaires traditionnels.</p>
<p>Les cartes bancaires (à paiement immédiat ou différé) sont le plus souvent utilisées pour tout achat de biens ou de services.</p>
<p>Les chèques servent essentiellement aux règlements par correspondance. Afin d’être accepté comme moyen de paiement dans les magasins ou pour le règlement de services, un chèque doit être accompagné d’une carte bancaire, émise par la banque qui a délivré le chéquier. Cette carte garantit le paiement du chèque jusqu’à hauteur de 50 ou 100 £.</p>
<p>Les guichets des banques sont ouverts au public de 9h30 à 16h30, certaines agences ouvrent également le samedi matin.</p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<p>Les transferts de fonds sont libres.</p>
<p>Les banques françaises n’assurant pas de service aux particuliers, il vous faudra choisir parmi les banques anglaises existantes (HSBC, Bank of England, Barclays Bank, Royal Bank of Scotland, Lloyds TSB, etc) afin d’ouvrir un compte, qui vous sera utile, ne serait-ce que pour vous faire verser votre salaire.</p>
<p>L’<strong>ouverture d’un compte bancaire</strong> au Royaume-Uni est facile et rapide. Toutefois l’obtention des différents moyens de paiement peut prendre du temps, et particulièrement pour les nouveaux expatriés : la banque ne vous connaissant pas, elle ne vous accordera pas immédiatement de <strong>chéquiers</strong>, de <strong>cartes de débit </strong>et encore moins de cartes de crédit, et attendra de constater des rentrées d’argent régulières et une gestion « normale » du compte.</p>
<p>L’obtention d’une <strong>carte de crédit est généralement lente et compliquée au Royaume-Uni</strong>. Sur place, pour ouvrir un compte, il vous faudra fournir plusieurs documents :</p>
<ul class="spip">
<li>pièce d’identité,</li>
<li>attestation de domicile,</li>
<li>copies de vos relevés bancaires des <strong>six derniers mois</strong>,</li>
<li><strong>certificat d’un garant</strong>, qui devra avoir vécu au Royaume-Uni depuis plusieurs années et certifie que vous êtes quelqu’un de respectable. Si vous arrivez en ayant déjà un contrat d’embauche, il s’agira en général de votre employeur, qui pourra également donner une indication de votre salaire annuel,</li>
<li><strong>lettre de référence</strong> (à traduire en anglais) de votre banque française. Ce document n’est pas obligatoire mais permet d’accélérer les choses, surtout en ce qui concerne l’obtention d’une carte de crédit. Demandez à votre banque qu’elle stipule vos renseignements bancaires : date d’ouverture de votre compte, solde actuel, solde moyen sur l’année, existence ou non de découverts…</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Alimentation</h3>
<h4 class="spip">Prix moyen d’un repas dans un restaurant</h4>
<p>Le prix d’un repas au restaurant est très variable selon la qualité de l’établissement. Il est difficile, même pour de la restauration rapide, de trouver un repas à moins de 6 £ (6,70 €). Le service n’est pas toujours inclus dans l’addition ; le pourboire reste à la discrétion du client.</p>
<p>Prix d’un repas (par personne) :</p>
<ul class="spip">
<li>Dans un restaurant de première catégorie : de 47 à 85 £ (de 52 à 95 €).</li>
<li>Dans un restaurant de catégorie moyenne : de 17 à 25 £ ; (de 19 à 28 €).</li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Evolution des prix</h3>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/vie-pratique/article/cout-de-la-vie). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
