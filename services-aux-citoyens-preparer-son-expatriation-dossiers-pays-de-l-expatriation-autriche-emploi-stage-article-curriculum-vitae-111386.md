# Curriculum vitae

<p><strong>Le curriculum vitae</strong> (<i>Lebenslauf</i>) <strong>et la lettre de motivation doivent être dactylographiés</strong>. Le curriculum vitae doit être rédigé dans un ordre chronologique, mais le modèle français est également accepté. La longueur du curriculum vitae ne doit pas dépasser deux pages. Il doit être daté, signé et faire mention, à la fin, du lieu d’établissement (exemple : Vienne, le…). La traduction, en allemand, des études et des diplômes doit être compréhensible pour un recruteur autrichien ne connaissant pas le système français. Il convient, par conséquent, de rechercher les équivalents autrichiens. Enfin, il est conseillé de faire relire le curriculum vitae par une personne de langue maternelle allemande disposant, dans la mesure du possible, d’une expérience dans la rédaction de curriculum vitae.</p>
<p>On retrouve les rubriques usuelles dans cet ordre :</p>
<ul class="spip">
<li>Persönliche Daten (Etat Civil)</li>
<li>Schulbildung (Formation initiale)</li>
<li>Berufsausbildung (Formation continue)</li>
<li>Berufserfahrung (Expérience professionnelle)</li>
<li>Sprachkenntnisse (Langues étrangères)</li>
<li>Sonstiges (Divers)</li></ul>
<p>Site en allemand de conseils pour les lettres de motivation et les curriculum vitae :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.bewerben.at/" class="spip_out" rel="external">Bewerben.at</a></p>
<p><i>Dernière mise à jour : février 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/autriche/emploi-stage/article/curriculum-vitae-111386). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
