# Adresses et liens utiles

<h4 class="spip">Bureau des Légalisations</h4>
<p>57, boulevard des Invalides <br class="manualbr">75007 PARIS <br class="manualbr">Métro : Duroc <br class="manualbr">Tél. : 01 53 69 38 28 - 01 53 69 38 29 (de 14h à 16h) <br class="manualbr">Courriel : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/legalisation-et-notariat/legalisation-et-certification-de-signatures/article/adresses-et-liens-utiles#bureau.legalisation#mc#diplomatie.gouv.fr#" title="bureau.legalisation..åt..diplomatie.gouv.fr" onclick="location.href=mc_lancerlien('bureau.legalisation','diplomatie.gouv.fr'); return false;" class="spip_mail">bureau.legalisation<span class="spancrypt"> [at] </span>diplomatie.gouv.fr</a></p>
<p>Le Bureau est ouvert au public du lundi au vendredi (sauf les jours fériés) de 8h30 à 13h15.</p>
<p>Un délai minimum de 48h environ sera demandé pour tout dossier comportant plus de 15 pièces.</p>
<p>Tous les dossiers de moins de 15 pièces pourront être récupérés légalisés sans délai.</p>
<p>Il pourra vous être demandé de <strong>présenter à nos guichets un justificatif de la nationalité</strong>.</p>
<p><strong>Ambassades et consulats français à l’étranger et étrangers en France</strong></p>
<p><a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/" class="spip_in">Consultez les coordonnées des ambassades et consulats français à l’étranger ainsi que les coordonnées des ambassades et consulats étrangers en France</a></p>
<p><strong>Chambre de Commerce et d’Industrie de Paris</strong><br class="manualbr">Département des facilitations du commerce extérieur<br class="manualbr">2, rue de Viarmes<br class="manualbr">75001 PARIS<br class="manualbr">Tél. : 08 20 01 21 12 <br class="manualbr">Courriel : <a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/legalisation-et-notariat/legalisation-et-certification-de-signatures/article/adresses-et-liens-utiles#dfce#mc#ccip.fr#" title="dfce..åt..ccip.fr" onclick="location.href=mc_lancerlien('dfce','ccip.fr'); return false;" class="spip_mail">dfce<span class="spancrypt"> [at] </span>ccip.fr</a></p>
<p><strong>Conseil National de l’Ordre des Médecins</strong><br class="manualbr">180, Boulevard Haussmann<br class="manualbr">75008 Paris<br class="manualbr">Tél. : 01 53 89 32 00</p>
<p><strong>Ordre National des Pharmaciens</strong><br class="manualbr">4, avenue Ruysdael<br class="manualbr">75008  Paris<br class="manualbr">Tél. : 01 56 21 34 34</p>
<p><strong>Cour de Cassation</strong> – <a href="https://www.courdecassation.fr/informations_services_6/experts_judiciaires_8700.html#experts" class="spip_out" rel="external">Liste des traducteurs assermentés selon la Cour d’Appel compétente</a></p>
<p><i>Mise à jour : janvier 2015</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/legalisation-et-notariat/legalisation-et-certification-de-signatures/article/adresses-et-liens-utiles). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
