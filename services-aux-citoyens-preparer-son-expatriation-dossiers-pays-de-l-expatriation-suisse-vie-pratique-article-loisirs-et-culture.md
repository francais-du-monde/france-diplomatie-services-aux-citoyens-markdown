# Loisirs et culture

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/vie-pratique/article/loisirs-et-culture#sommaire_1">Tourisme</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/vie-pratique/article/loisirs-et-culture#sommaire_2">Activités culturelles</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/vie-pratique/article/loisirs-et-culture#sommaire_3">Presse</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/vie-pratique/article/loisirs-et-culture#sommaire_4">Sports</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Tourisme</h3>
<p>Les paysages suisses sont variés : lacs, plaines et montagnes. Ses jolies villes et villages traditionnels, ainsi que ses stations de sports d’hiver très réputées font de ce pas une destination touristique très prisée. L’hôtellerie y est de bonne qualité et la circulation intérieure est facilitée par un très bon réseau routier et ferroviaire. La gastronomie suisse et les produits du terroir et artisanaux locaux jouissent d’une bonne réputation.</p>
<p>L’activité touristique, qui demeure une des branches principales de l’économie suisse et une source importante d’emplois, se décline en plusieurs variantes : tourisme local, tourisme international, tourisme rural, écotourisme, tourisme d’affaires…</p>
<p>Différentes antennes touristiques (Offices de tourisme), aux échelles locale, cantonale et régionale, ont pour mission l’information des visiteurs. Le site internet <a href="http://www.myswitzerland.com/fr/accueil.html" class="spip_out" rel="external">Swiss Tourisme</a> regroupe de nombreuses informations clé.</p>
<h4 class="spip">Déplacements et hébergement</h4>
<p>Les centres urbains se visitent aisément à pied ou à vélo grâce à un important réseau de pistes cyclables clairement signalées. Les réseaux routier, ferré et de bus sont aussi très développés (voir notre article <a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-suisse-vie-pratique-article-transports-105257.md" class="spip_in">Transports</a>).</p>
<p>Des excursions en bateau sur le lac Léman sont organisées depuis plusieurs villes.</p>
<p>Des solutions d’hébergement variées existent pour les touristes, voir la section dédiée dans notre article <a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-suisse-vie-pratique-article-logement.md" class="spip_in">Logement</a>.</p>
<h4 class="spip">Gastronomie</h4>
<p>Bien que la gastronomie suisse soit surtout connue pour ses plats à base de fromage (raclette et fondue) et pour son chocolat, elle comprend aussi de nombreuses spécialités régionales :</p>
<ul class="spip">
<li>Le Pizzocheri à Puschlav (les Grisons)</li>
<li>La viande des Grisons</li>
<li>Le Brasato con polenta (Tessin)</li>
<li>Le Gâteau du Vully</li>
<li>La tarte aux quetsches</li>
<li>Le rösti avec "gschwellti" (pommes de terre en robe des champs dans le Mitteland)</li>
<li>La meringue à la double crème de gruyère (Gruyère)</li>
<li>Le bircher müesli</li></ul>
<h4 class="spip">Pour en savoir plus</h4>
<ul class="spip">
<li><a href="http://www.myswitzerland.com/fr/accueil.html" class="spip_out" rel="external">Office du tourisme helvétique</a></li>
<li><a href="http://www.geneva-tourism.ch" class="spip_out" rel="external">Office du Tourisme de Genève</a></li>
<li><a href="http://www.lausanne-tourisme.ch" class="spip_out" rel="external">Office du Tourisme de Lausanne</a></li>
<li><a href="http://www.zuerich.com" class="spip_out" rel="external">Office du Tourisme de Zurich</a></li>
<li><a href="http://www.berninfo.com" class="spip_out" rel="external">Office du Tourisme de Berne</a></li>
<li><a href="http://www.basel.com/" class="spip_out" rel="external">Office du Tourisme de Bâle</a></li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Activités culturelles</h3>
<p>Les activités culturelles locales en français sont nombreuses.<br class="autobr">Des manifestations sont également organisées par le service de coopération et d’action culturelle de <a href="http://www.ambafrance-ch.org/-Accueil-Francais-" class="spip_out" rel="external">l’ambassade de France à Berne</a>. Films, pièces de théâtre, concerts, ballets, expositions, conférences sont proposés de manière permanente.</p>
<h4 class="spip">Radio et télévision</h4>
<p>Le système adopté est le système PAL. Pour la lecture des DVD, la Suisse se trouve, comme la France, en zone 2.</p>
<p>La Société suisse de radiodiffusion et télévision SRG SSR offre un service de radio télévision de service public à l’ensemble du territoire suisse et dans les quatre langues officielles du pays (français, allemand / suisse-allemand, italien et romanche).</p>
<p>Les sites internet des différentes unités de radio-télévision :</p>
<ul class="spip">
<li><a href="http://www.rts.ch" class="spip_out" rel="external">Radio Télévision Suisse romande (RTS)</a></li>
<li><a href="http://www.srf.ch" class="spip_out" rel="external">Radio Télévision Suisse alémanique – Schweizer Radio und Fernsehen (SRF)</a></li>
<li><a href="http://www.rsi.ch/" class="spip_out" rel="external">Radio Télévision Suisse italienne – Radiotelevisione svizzera (RSI) :</a></li>
<li><a href="http://www.rtr.ch" class="spip_out" rel="external">Radio Télévision romanche – Radiotelevisiun Svizra Rumantscha (RTR)</a></li>
<li><a href="http://www.swissinfo.ch/" class="spip_out" rel="external">Swissinfo</a></li></ul>
<p>SRG SSR propose 18 programmes de radio (dont 4 francophones : La Première, Espace 2, Couleur 3, Option musique et 1 anglophone « World Radio Switzerland »), 8 chaînes TV (dont 2 francophones) et a également cofondé le réseau télévisé 3SAT.</p>
<p>Outre les programmes de ce groupe, les chaînes suivantes peuvent être captées directement ou par le câble en suisse romande (de même que plusieurs radios françaises) : TV5Monde, TF1, France 2, France 3, M6, Arte (en français ou en allemand selon la zone), France 5, Eurosport, Euronews, CNN, Canal+.</p>
<p>La télévision par câble propose un large choix de plus de 140 chaînes (opérateur : Swisscom - Bluewin TV).</p>
<p>La plupart des concerts de l’Orchestre de la Suisse Romande sont enregistrés par la RTS-Espace 2 et retransmis en direct ou en différé.</p>
<p>Plus d’information :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.bakom.admin.ch/themen/radio_tv/marktuebersicht/index.html?lang=fr" class="spip_out" rel="external">Liste de chaînes de télévision et radio sur le site de l’OFCOM</a> (avec liens vers les sites internet des différentes chaînes)</p>
<h4 class="spip">Cinéma</h4>
<p>Tous les films sont projetés en version originale sous-titrée (en français, allemand, anglais suivant la langue d’origine), avec une prédominance de films français ou sous-titrés en français à Fribourg, Genève et Lausanne.</p>
<ul class="spip">
<li><a href="http://www.pardo.ch" class="spip_out" rel="external">Festival du film de Locarno</a></li>
<li><a href="http://www.cinematheque.ch" class="spip_out" rel="external">Cinémathèque suisse de Lausanne</a></li></ul>
<h4 class="spip">Spectacles</h4>
<p>Les pièces de théâtre proposées sont nombreuses et variées. Elles sont jouées en général dans la langue de la zone linguistique.</p>
<p>Des concerts sont donnés dans toutes les grandes villes suisses, notamment à Genève, Lausanne, Zurich, Lucerne, Montreux et Nyon où se déroulent des festivals de notoriété internationale, brassant différents genres musicaux.</p>
<p>Les spectacles de variété, les opéras, les ballets sont également nombreux ; parmi les plus célèbres, ceux proposés à l’Opéra de Genève et les Ballets Béjart à Lausanne, à l’opéra de Zurich, au Palais du Festival de Lucerne.</p>
<p><strong>Rendez-vous importants</strong></p>
<ul class="spip">
<li><a href="http://www.fetesdegeneve.ch" class="spip_out" rel="external">Les Fêtes de Genève</a></li>
<li><a href="http://www.paleo.ch" class="spip_out" rel="external">Paléo Festival de Nyon</a> (concerts et arts de la scène)</li>
<li><a href="http://www.festivalcite.ch" class="spip_out" rel="external">Festival de La Cité de Lausanne</a> (danse et arts de la rue)</li>
<li><a href="http://www.montreuxjazz.com" class="spip_out" rel="external">Montreux Jazz Festival</a></li>
<li><a href="http://www.salon-auto.ch" class="spip_out" rel="external">Salon de l’automobile de Genève</a></li>
<li><a href="http://www.sion-festival.ch" class="spip_out" rel="external">Festival international de musique classique de Sion</a></li>
<li>Technofest de Zurich</li>
<li>Festival international de Lucerne.</li></ul>
<h4 class="spip">Expositions</h4>
<p>Elles sont fréquentes et de grande qualité dans les nombreux musées, fondations et galeries des principales villes suisses : la fondation Gianadda à Martigny, l’Office des Nations Unies et le musée de la Croix Rouge à Genève, le musée Olympique à Lausanne, le musée international de l’Horlogerie à la Chaux-de-Fonds, le musée des Beaux Arts et le musée national suisse à Zurich, le château de Chillon à Veytoux (près de Montreux/Canton de Vaud), la fondation Beyeler à Riehen (près de Bâle), musée d’Arts de Bâle, la bibliothèque de Saint-Gall et le centre Paul Klee à Berne…</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.museums.ch" class="spip_out" rel="external">Musées en Suisse</a></p>
<h4 class="spip">Pour en savoir plus</h4>
<ul class="spip">
<li><a href="http://www.loisirs.ch" class="spip_out" rel="external">Idées d’activités de loisir en Suisse</a> (balades, randonnées, bricolage, découverte et curiosité, idées weekend, détente et bien-être, etc.…), répertoriées par genre et par canton.</li>
<li><a href="http://www.myswitzerland.com" class="spip_out" rel="external">Calendrier des manifestations organisées en Suisse</a></li>
<li><a href="http://www.auxartsetc.ch" class="spip_out" rel="external">Culture à Zurich et dans sa région</a></li>
<li><a href="http://www.regart.ch" class="spip_out" rel="external">Agenda culturel romand</a></li>
<li><a href="http://www.gianadda.ch" class="spip_out" rel="external">Fondation Gianadda à Martigny</a></li>
<li><a href="http://www.ville-geneve.ch/" class="spip_out" rel="external">Ville de Genève, Département de la culture</a></li>
<li><a href="http://www.nb.admin.ch" class="spip_out" rel="external">Office fédéral de la culture (OFC)</a></li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Presse</h3>
<p>La plupart des journaux et magazines français sont disponibles dans toutes les librairies et kiosques en Suisse romande et dans certaines grandes villes de la Suisse alémanique et du Tessin. A Genève, la bibliothèque municipale et celle de l’Université des Sciences économiques et sociales mettent à disposition les plus grands quotidiens français et internationaux.</p>
<p>La <strong>presse francophone suisse</strong> comporte aussi de nombreux titres :</p>
<ul class="spip">
<li><a href="http://www.letemps.ch" class="spip_out" rel="external">Le Temps</a> : quotidien d’audience nationale et internationale</li>
<li><a href="http://www.agefi.com" class="spip_out" rel="external">L’AGEFI</a> : quotidien de la finance et de l’économie</li>
<li><a href="http://www.24heures.ch" class="spip_out" rel="external">24 Heures</a> : quotidien du canton de Vaud</li>
<li><a href="http://www.tdg.ch" class="spip_out" rel="external">Tribune de Genève</a> : quotidien de la région genevoise et de la France voisine</li>
<li><a href="http://www.lenouvelliste.ch/fr" class="spip_out" rel="external">Le Nouvelliste</a> : quotidien du canton du Valais</li>
<li><a href="http://lqj.ch" class="spip_out" rel="external">Le Quotidien jurassien</a> : quotidien du canton du Jura</li>
<li><a href="http://www.laliberte.ch" class="spip_out" rel="external">La Liberté</a> : quotidien du canton de Fribourg</li>
<li><a href="http://www.lematin.ch" class="spip_out" rel="external">Le Matin</a></li>
<li><a href="http://www.lecourrier.ch" class="spip_out" rel="external">Le courrier</a> : quotidien indépendant d’information et d’opinion</li>
<li><a href="http://www.hebdo.ch" class="spip_out" rel="external">L’Hebdo</a> : hebdomadaire d’informations</li>
<li><a href="http://www.bilan.ch" class="spip_out" rel="external">Bilan</a> : mensuel économique</li></ul>
<p><strong>Presse germanophone suisse</strong> :</p>
<ul class="spip">
<li><a href="http://www.nzz.ch" class="spip_out" rel="external">Die Neue Zürcher Zeitung</a> : quotidien d’audience nationale et internationale, qui traite également l’actualité zurichoise</li>
<li><a href="http://www.tagesanzeiger.ch" class="spip_out" rel="external">Der TagesAnzeiger</a> : quotidien d’audience nationale</li>
<li><a href="http://www.blick.ch" class="spip_out" rel="external">Blick</a> : quotidien le plus lu en Suisse, format tabloïd</li>
<li><a href="http://www.derbund.ch" class="spip_out" rel="external">Der Bund</a> : quotidien du canton de Berne</li>
<li><a href="http://www.bernerzeitung.ch" class="spip_out" rel="external">Die Berner Zeitung</a> : quotidien du canton de Berne</li>
<li><a href="http://www.luzernerzeitung.ch" class="spip_out" rel="external">Die Neue Luzerner Zeitung</a> : quotidien du canton de Lucerne</li>
<li><a href="http://www.aargauerzeitung.ch" class="spip_out" rel="external">Die Aargauer Zeitung</a> : quotidien du canton d’Argovie</li>
<li><a href="http://www.suedostschweiz.ch" class="spip_out" rel="external">Die Südostschweiz</a> : quotidien des cantons de Glaris, Schwytz, St-Gall et des Grisons</li>
<li><a href="http://www.tagblatt.ch" class="spip_out" rel="external">Das St. Galler Tagblatt</a> : quotidien du canton de St-Gall</li>
<li><a href="http://www.sonntagszeitung.ch" class="spip_out" rel="external">Die SonntagsZeitung</a> : hebdomadaire dominical</li>
<li><a href="http://www.schweizer-illustrierte.ch" class="spip_out" rel="external">Die Schweizer Illustrierte</a> : hebdomadaire</li>
<li><a href="http://www.woz.ch" class="spip_out" rel="external">Die Wochenzeitung</a> : hebdomadaire indépendant d’opinion</li>
<li><a href="http://www.beobachter.ch" class="spip_out" rel="external">Der Beobachter</a> : bimensuel axé sur les questions de société</li>
<li><a href="http://www.bilanz.ch" class="spip_out" rel="external">Bilanz</a> : mensuel économique</li></ul>
<p><strong>Presse italophone suisse</strong> :</p>
<ul class="spip">
<li><a href="http://www.cdt.ch" class="spip_out" rel="external">Il Corriere del Ticino</a> : quotidien du canton du Tessin</li>
<li><a href="http://www.illustrazione.ch" class="spip_out" rel="external">Illustrazione Ticinese</a> : mensuel familial</li></ul>
<h3 class="spip"><a id="sommaire_4"></a>Sports</h3>
<p>La Suisse offre un cadre naturel aux sports de montagne, ainsi qu’aux VTT, randonnées et courses pédestres avec dénivelé, vol à voile, parapente, escalade, etc. ainsi qu’aux sports nautiques (canoë, kayak, rafting, voile, ski nautique, pêche…). Les sports de glace (luge, bobsleigh, curling, hockey sur glace, patinage) ont toute leur place en Suisse. Il est possible de pratiquer tous les sports collectifs ou individuels. Parmi les plus populaires, on peut citer le tennis, le football, le hockey sur glace, le ski (alpin et de fond) et le curling.</p>
<p>Les villes disposent d’infrastructures sportives de très bonne qualité. Le pays compte de nombreux chemins pédestres et de randonnée, ainsi qu’un important réseau de pistes cyclables, la meilleure saison pour visiter la Suisse est de juin à septembre afin de profiter pleinement des activités de plein air, mais certains itinéraires restent accessibles même pendant l’hiver.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.switzerland-rando.ch" class="spip_out" rel="external">site dédié à la randonnée en Suisse</a></p>
<h4 class="spip">Sport d’hiver</h4>
<p>Les dépenses inhérentes aux sports d’hiver en Suisse sont beaucoup plus élevées qu’en France. Les stations de ski suisses proposent davantage d’hébergements haute gamme que dans la plupart des stations de ski voisines françaises. Il est conseillé dans la mesure du possible de chercher un hébergement dans la périphérie des stations (à quelques kms) où les prix peuvent s’avérer plus abordables. Enfin, avant de se rendre dans une station, ne pas hésiter à se renseigner au préalable, car certaines ne proposent que des forfaits à la journée et donc pas de pass à la demi-journée. Lors d’un séjour de plusieurs jours, il est recommandé d’étudier les offres de forfaits qui selon les stations, permettent l’accès à plusieurs domaines skiables couvrant la Suisse et la France.</p>
<p>Les plus réputées des 57 stations de ski suisses sont :</p>
<p><strong>Verbier : 52 pistes - 150 km - 9 km de ski de fond</strong></p>
<p>Grande station de ski européenne au cœur des 4 vallées, Verbier bénéficie d’un important domaine skiable qui répond aux attentes des skieurs exigeants et confirmés. Elle compte de nombreux parcours hors-pistes. En revanche elle est peu recommandée pour les débutants. Verbier, appréciée aussi pour ses chalets, l’est tout autant pour son atmosphère animée. Elle accueille un tourisme international bien qu’à dominante anglo-saxonne. <a href="http://www.verbier.ch/" class="spip_out" rel="external">www.verbier.ch/</a></p>
<p><strong>Crans Montana : 40 pistes - 140 km - 70 km de ski de fond</strong></p>
<p><a href="http://www.crans-montana.ch/" class="spip_out" rel="external">Crans Montana</a> est une station bien exposée très en vogue, disposant d’un important domaine skiable (160 kms de pistes facilement praticables). L’aménagement de la station, ses nombreuses terrasses solarium, ses magasins de luxe et ses bars et restaurants en font un lieu très convoité. Depuis Crans Montana, il est possible de contempler le Cervin ou le Mont Blanc et c’est le point de départ de randonnées. Ouverte tout au long de l’année.</p>
<p><strong>Davos : 103 pistes - 300 km - 115 km de ski de fond</strong></p>
<p>En plus d’être une des plus grandes stations de ski européennes, elle est aussi le rendez-vous annuel du <a href="http://www.davos.ch/" class="spip_out" rel="external">Forum de Davos</a> (World Economic Forum). Cette ville est entourée d’un remarquable domaine skiable aménagé pour répondre à toutes les attentes. Elle est connue pour accueillir une clientèle internationale de prestige.</p>
<p><strong>Les Diablerets : 26 pistes - 65 km de ski et 30km de ski de fond</strong></p>
<p>Authentique station suisse dans un cadre naturel magnifique, les Diablerets sont surtout connus pour son glacier 3000 et l’ascension vertigineuse pour y accéder.</p>
<p>Ce joli petit village vaudois est également une destination estivale incontournable. Elle accueille snowboarders et skieurs du monde entier en quête d’émotions fortes ! Pendant l’hiver, l’atmosphère y est beaucoup plus tranquille. <a href="http://www.diablerets.ch/" class="spip_out" rel="external">Les Diablerets</a> sont situés à proximité de la <a href="http://www.glacier3000.ch/" class="spip_out" rel="external">station de Gstaad</a>.</p>
<p><strong>Les Marécottes : 5 pistes : 25 km - 3 km de ski de fond</strong></p>
<p>Cette station est connue pour son accueil et son ambiance familiale et reste une halte incontournable des Alpes valaisannes. Son domaine de ski est réputé pour être "spectaculaire" et s’adresse à tout type de skieurs et de sports. Les abords permettent la randonnée en ski de fond, en raquette ainsi que la luge dans un cadre naturel exceptionnel. Les <a href="http://www.marecottes.ch/" class="spip_out" rel="external">Marécottes</a> offrent également aux passionnés des cimes vertigineuses, un magnifique et vaste espace de loisirs.</p>
<p><strong>Gstaad : 21 pistes - 250 km - 195 km de ski de fond</strong></p>
<p>Gstaad et son magnifique décor naturel est d’abord "une destination" avant d’être une station de ski. Avec ses nombreuses boutiques de luxe et ses grands hôtels, elle est le rendez-vous d’une clientèle internationale haut de gamme. Elle dispose d’un vaste domaine skiable facile d’accès et d’une multitude de services et prestations à la hauteur des attentes de sa clientèle. Depuis Gstaad, un petit train parcourt les montagnes et relie les différentes stations de l’espace <a href="http://www.gstaad.ch/" class="spip_out" rel="external">Ski Gstaad</a>.</p>
<p><strong>Saint Moritz : 80 km - 20 km Ski de fond</strong></p>
<p><a href="http://www.stmoritz.ch/" class="spip_out" rel="external">Saint Moritz</a>, station réputée, continue de forger son propre style qui fait d’elle un lieu atypique très largement apprécié.</p>
<p><strong>Zermatt : 59 pistes - 150 km - 10 km de ski de fond</strong></p>
<p>Nichée au pied du Cervin, Zermatt est le symbole de la Suisse avec ses hautes Alpes et ses magnifiques paysages naturels. Domaine skiable le plus élevé des Alpes, particulièrement bien aménagé, Zermatt est le rendez-vous des amateurs de hors-piste et de ski de randonnée. Le domaine convient peu aux débutants.</p>
<p><a href="http://www.zermatt.ch/" class="spip_out" rel="external">Zermatt</a> propose des activités tout au long de l’année.</p>
<p><strong>Wengen :</strong></p>
<p>Station où est organisée la célèbre course du ’Lauberhorn" chaque année à la mi-janvier.</p>
<p><strong>Arosa :</strong></p>
<p>Station climatique réputée, située dans les Grisons à 1800 m d’altitude.</p>
<p><strong>Engelberg</strong></p>
<p>La station d’Engelberg Titlis est l’un des plus grands domaines skiables de Suisse centrale. Grâce au glacier du Titlis, la saison de ski y commence en automne et durer jusqu’à fin mai.<strong></strong></p>
<p>Si l’ensemble de ces stations connaissent un mouvement touristique important l’hiver, elles offrent également des activités pendant la saison estivale.</p>
<p><strong>Sports nautiques</strong></p>
<p>La Suisse, pays de lacs permet la pratique de nombreux sports nautiques tels que rafting et canyoning, navigation, plongée, ski nautique et windsurf ainsi que la baignade.</p>
<p><strong>Pêche</strong></p>
<p>La pêche nécessite un permis. Les périodes autorisées varient selon les espèces et les régions.</p>
<p><strong>Chasse</strong></p>
<p>La chasse est possible sous certaines conditions et répond à une réglementation cantonale. Un permis de chasse est indispensable.</p>
<p><strong>Pour plus de renseignements </strong> :</p>
<p><a href="http://www.chassesuisse.ch/" class="spip_out" rel="external">http://www.chassesuisse.ch/</a></p>
<p><strong>Rendez-vous sportifs suisses à vivre en tant que spectateur et/ou participant</strong></p>
<p>Lausanne abrite le siège du Comité olympique et la Suisse accueille de grands rendez-vous sportifs mondiaux dans les domaines du ski (championnats du monde de ski alpin), du hockey sur glace, de l’athlétisme, etc. Le lac Léman offre à lui seul un large éventail d’activités de loisirs et de détente.</p>
<ul class="spip">
<li>La course de l’Escalade (Genève)</li>
<li>La course Morat-Fribourg</li>
<li>Le meeting d’athlétisme (Lausanne)</li>
<li>Meeting d’athlétisme du monde « Weltklasse » Zurich</li>
<li>La semaine olympique (Lausanne)</li>
<li>Davidoff Swiss Indoords (Bâle)</li>
<li>Championnat du monde de billard (Lausanne)</li>
<li>Championnat du monde de bobsleigh (Saint Moritz)</li>
<li>White Turf (Saint Moritz)</li>
<li>Technofest de Zurich</li>
<li>Festival international (Lucerne)</li></ul>
<p><strong>Rendez-vous importants</strong></p>
<ul class="spip">
<li><a href="http://www.escalade.ch" class="spip_out" rel="external">La Course de l’Escalade à Genève</a></li>
<li><a href="http://www.athletissima.ch" class="spip_out" rel="external">Athlétissima</a> (rencontre d’athlétisme internationale annuelle à Lausanne)</li>
<li>White Turf St Moritz</li></ul>
<p><i>Mise à jour : février 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suisse/vie-pratique/article/loisirs-et-culture). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
