# Communications

<h4 class="spip">Téléphone – Internet</h4>
<p>Le réseau téléphonique, surtout mobile, ainsi que le réseau internet est bien développé.</p>
<p>Pour appeler depuis la France, composez l’indicatif du pays - 00 40 - puis l’indicatif du département (21 pour Bucarest, 232 pour Iasi, 241 pour Constanta) et enfin le numéro du correspondant, à six chiffres pour Bucarest et à sept chiffres pour la province.</p>
<p>Depuis la Roumanie, un appel vers la France est composé de l’indicatif 00 33 suivi du numéro du correspondant sans le 0.</p>
<h4 class="spip">Téléphoner gratuitement par Internet </h4>
<p>Il est généralement très avantageux d’utiliser <strong>des logiciels de téléphonie sur IP</strong> (Skype, Google talk, Live messenger, Yahoo ! messenger, etc.) car les tarifs proposés à l’international sont souvent moindre que ceux des opérateurs locaux exploitant des lignes classiques, voire gratuits dans certaines conditions.</p>
<p>De même pour les applications pour téléphones portables telles que Viber ou Whatsapp qui permettnt de téléphoner gratuitement et d’échanger SMS et MMS gratuitement.</p>
<h4 class="spip">Poste</h4>
<p><a href="http://www.posta-romana.ro/" class="spip_out" rel="external">Le site internet de la poste roumaine</a>.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/roumanie/vie-pratique/article/communications). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
