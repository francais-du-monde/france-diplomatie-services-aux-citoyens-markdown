# Animaux domestiques

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/entree-et-sejour/article/animaux-domestiques#sommaire_1">Chiens et chats</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/entree-et-sejour/article/animaux-domestiques#sommaire_2">Autres animaux</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/entree-et-sejour/article/animaux-domestiques#sommaire_3">Pendant le séjour en Australie</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/entree-et-sejour/article/animaux-domestiques#sommaire_4">Pour plus d’informations</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Chiens et chats</h3>
<p>Pour importer un chien ou un chat domestique en Australie, vous devez <strong>strictement vous conformer</strong> aux conditions d’importation fixées par le <a href="http://www.australia.gov.au/information-and-services/business-and-industry/primary-industry/biosecurity-inspection-and-quarantine" class="spip_out" rel="external">service australien de quarantaine et d’inspection</a> (<i>Australian Quarantine and Inspection Service</i> - AQIS).</p>
<p>Vous trouverez ci-dessous les grandes lignes régissant l’importation en Australie d’un animal en provenance de France. Vous trouverez une notice très détaillée sur le site Internet du <a href="http://www.agriculture.gov.au/" class="spip_out" rel="external">ministère australien de l’Agriculture</a>. Les formalités à accomplir sont complexes et nombreuses et doivent être effectuées en stricte conformité avec les règles fixées par l’AQIS. En cas de doute, n’hésitez pas à contacter leurs services. <strong>N’attendez pas le dernier moment pour effectuer ces formalités.</strong></p>
<p>La première étape est de déposer auprès de l’AQIS une demande de <strong>permis d’importation pour chaque animal</strong>. Cette demande peut être faite électroniquement via le <a href="http://www.agriculture.gov.au/cats-dogs" class="spip_out" rel="external">site Internet</a>  ou manuellement :</p>
<p>Les documents nécessaires à l’étude de votre dossier devront être scannés et joints à votre envoi. Au moment du dépôt de la demande du permis d’importation, vous devrez payer 75 dollars australiens par animal si la demande est déposée via Internet ou 130 dollars australiens, quel que soit le nombre d’animaux, pour les demandes déposées manuellement.</p>
<p><strong>Conditions à remplir</strong></p>
<ul class="spip">
<li>L’animal doit avoir vécu de façon continue dans le pays d’exportation pendant au moins 6 mois avant la date de départ vers l’Australie ou depuis sa naissance.</li>
<li>L’animal doit être âgé d’au moins 12 mois à la date de son exportation de France.</li>
<li>Les femelles ne doivent pas être enceintes de plus de 3 mois ou encore allaiter.</li>
<li>Certaines races de chiens (dogo Argentino, fila Brazileiro, Japanese tosa, pit bull terrier, American pit bull et Presa Canario) et de chats (servals, ainsi que les croisements avec un chat domestique et un serval ou avec un chat Savannah) ne peuvent être importées en Australie.</li></ul>
<p>Une micro-puce pouvant être lue par un lecteur Avid, Trovan, Destro ou compatible avec les normes ISO doit être implantée sur l’animal avant d’effectuer les examens sanguins.</p>
<p>L’animal doit avoir été vacciné contre la rage dans les 12 mois au moins précédant son importation en Australie. L’animal doit avoir été âgé d’au moins 3 mois à la date de la vaccination.</p>
<p>Un titrage sérique des anticorps antirabiques doit être effectué auprès d’un vétérinaire ou d’un laboratoire reconnu et agréé par le pays d’exportation, 12 mois au plus et 60 jours au moins avant la date d’exportation. En cas de primo-vaccination, celle-ci doit avoir été effectuée au moins 4 semaines avant l’examen sanguin. La date à laquelle est fait le prélèvement détermine la durée pendant laquelle l’animal devra être placé en quarantaine. Plus cette date est proche de la date d’exportation, plus la durée de quarantaine sera longue. Le résultat du titrage sérique devra être supérieur ou égal à 0,5UI/ml. Dans le cas contraire, l’animal devra être de nouveau vacciné contre la rage. Le titrage sérique est valable un an à compter de la date du prélèvement.</p>
<p>Votre animal doit être vacciné contre d’autres maladies et avoir subi certains traitements (notamment contre les puces, les tics et certains parasites). Veillez à respecter les dates et les délais lors de la vaccination de votre animal.</p>
<p><strong>Etude de votre dossier</strong></p>
<p>Il faut compter une dizaine de jours pour l’étude du dossier, sous réserve qu’il soit complet, et l’établissement du permis d’importation. Ce délai peut être augmenté de quelques jours en cas d’envoi du permis par la voie postale. L’examen de la demande est payant (200 dollars australiens pour le premier animal et 80 dollars australiens pour tout animal supplémentaire).</p>
<p>Si l’animal remplit les conditions et si votre dossier est complet, un permis d’importation vous sera délivré. Celui-ci est valable six mois à compter de sa date de délivrance. Il est possible d’en obtenir la prorogation pour une durée de 3 mois supplémentaires moyennant le paiement de 130 dollars australiens.</p>
<p><strong>Le séjour en quarantaine</strong></p>
<p>Une fois en possession du permis d’importation, <strong>vous devez tout de suite réserver une place </strong>dans l’un des trois centres de quarantaine pour chiens et chats en Australie (Sydney, Melbourne et Perth). Vous trouverez leurs coordonnées sur le site du <a href="http://www.agriculture.gov.au/" class="spip_out" rel="external">ministère australien de l’Agriculture</a></p>
<p>La durée du séjour est au minimum de 30 jours et peut aller jusqu’à 120 jours. La date à laquelle est fait le test détermine la durée pendant laquelle l’animal devra être placé en quarantaine. Plus cette date est proche de la date d’exportation, plus la durée de quarantaine sera longue.</p>
<p>Le séjour en quarantaine est payant (12 dollars canadiens pour l’admission en quarantaine, 71 dollars australiens pour l’examen vétérinaire, 30 dollars australiens par animal pour l’approbation des documents, entre 95 et 120 dollars australiens pour le transport de l’animal de l’aéroport au centre de quarantaine, entre 13,55 et 17, 55 dollars australiens pour les frais journaliers de séjour de l’animal, etc.). Une facture vous sera adressée et devra être réglée avant la remise de l’animal à son propriétaire.</p>
<p>Vous pourrez rendre visite à l’animal pendant la semaine, mais pas les week-ends ou les jours fériés, ni pendant les deux semaines de congés des fêtes de fin d’année.</p>
<p><strong>Certificats vétérinaires A et B</strong></p>
<p>Quatre jours au plus avant l’importation de l’animal, vous devrez faire compléter le certificat vétérinaire A par un vétérinaire agréé lequel procédera à l’examen de l’animal. Vous devrez vous munir des documents concernant l’animal (notamment les certificats de vaccination).</p>
<p>Le jour du départ, un vétérinaire agréé devra sceller la cage de transport dans laquelle sera placé l’animal et établir le certificat vétérinaire B mentionnant le numéro du sceau apposé sur la cage. Les documents à présenter sont la copie ou l’original du permis d’importation, les certificats vétérinaires A et B, pour approbation par le vétérinaire agréé, dûment complétés, les certificats de vaccinations (y compris celui contre la rage) et les résultats des tests sanguins pour le titrage sérique des anticorps antirabiques.</p>
<p><strong>Transport de l’animal</strong></p>
<p>L’animal doit obligatoirement arriver à l’un des aéroports suivants : Sydney, Melbourne ou Perth. L’animal ne peut pas être réceptionné le week-end ou les jours fériés. Une taxe de 25 dollars australiens est exigée si l’animal arrive en dehors des heures de bureaux (8 heures à 16 heures 30).</p>
<p>Renseignez-vous auprès de la compagnie aérienne pour connaître les conditions de transport de l’animal (normes de la cage, documents à fournir, réservation en soute, etc.).</p>
<p>Les documents concernant l’animal (permis d’importation, certificats vétérinaires A et B, etc.) doivent voyager avec l’animal. Conservez avec vous un jeu de copies.</p>
<p><strong>Arrivée en Australie</strong></p>
<p>L’animal ira directement au service australien de quarantaine et d’inspection et ensuite au centre de quarantaine. Vous ne serez pas autorisé à voir l’animal à son arrivée à l’aéroport.</p>
<h3 class="spip"><a id="sommaire_2"></a>Autres animaux</h3>
<p>Oiseaux : seules les espèces sélectionnées en provenance de Nouvelle-Zélande peuvent être importées.</p>
<ul class="spip">
<li>Lapins : ne peuvent être importés que les lapins provenant de pays approuvés le ministère de l’Agriculture australien</li>
<li>Chevaux : seuls peuvent être importés les chevaux en provenance de Nouvelle-Zélande.</li>
<li>Les autres animaux domestiques (poissons, hamsters, cochon d’Inde, lézards, serpents, souris, araignées et tortues) ne peuvent pas être importés en Australie.</li></ul>
<p>Pour en savoir plus, vous pouvez consulter le site du <a href="http://www.agriculture.gov.au/" class="spip_out" rel="external">ministère australien de l’Agriculture</a>.</p>
<h3 class="spip"><a id="sommaire_3"></a>Pendant le séjour en Australie</h3>
<p>Les lois australiennes sur la protection des animaux sont très strictes et punissent sévèrement les sévices dont les animaux sont victimes qu’il s’agisse d’animaux domestiques ou de la faune sauvage. Elles stipulent la race de l’animal domestique, ainsi que le nombre qu’il est possible d’accueillir à son domicile. La législation concernant le bien-être des animaux relève des gouvernements des états et des territoires. L’administration locale dispose également d’une législation concernant le traitement des animaux de compagnie. Pour connaître la législation de votre lieu de résidence, vous pouvez consulter le site Internet suivant :</p>
<ul class="spip">
<li><a href="http://www.agriculture.gov.au/" class="spip_out" rel="external">ministère australien de l’Agriculture</a> (<i>Department of Agriculture, Fisheries and Forestry</i>)</li>
<li><a href="http://www.rspca.org.au/" class="spip_out" rel="external">l’organisme australien de protection des animaux</a> (<i>Royal society for the prevention of cruelty to animals - RSPCA</i>). Ses inspecteurs ont le pouvoir de retirer tout animal maltraité et de poursuivre les propriétaires en les condamnant à des amendes et à des peines d’emprisonnement</li></ul>
<p>Sur place, si vous souhaitez adopter un animal de compagnie, il est vivement conseillé de se renseigner auprès de l’administration de votre ville quant aux procédures d’immatriculation de l’animal.</p>
<p>Il est interdit d’emmener des animaux domestiques dans un véhicule de location (camping-car). Dans tous les cas, n’hésitez à vous renseigner auprès de l’entreprise de location. Il en va de même lors de la location d’un logement.</p>
<h3 class="spip"><a id="sommaire_4"></a>Pour plus d’informations</h3>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="services-aux-citoyens-preparer-son-expatriation-douanes-article-animaux-domestiques.md" class="spip_in">Notre article thématique sur l’exportation d’animaux domestiques</a></p>
<p><i>Mise à jour : octobre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/australie/entree-et-sejour/article/animaux-domestiques). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
