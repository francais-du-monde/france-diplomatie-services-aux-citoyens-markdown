# Transports

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/vie-pratique/article/transports#sommaire_1">Importation de véhicule</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/vie-pratique/article/transports#sommaire_2">Permis de conduire</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/vie-pratique/article/transports#sommaire_3">Code de la route</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/vie-pratique/article/transports#sommaire_4">Assurances et taxes</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/vie-pratique/article/transports#sommaire_5">Achat et location</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/vie-pratique/article/transports#sommaire_6">Immatriculation</a></li>
<li><a id="so_7" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/vie-pratique/article/transports#sommaire_7">Entretien</a></li>
<li><a id="so_8" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/vie-pratique/article/transports#sommaire_8">Carburant</a></li>
<li><a id="so_9" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/vie-pratique/article/transports#sommaire_9">Réseau routier</a></li>
<li><a id="so_10" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/vie-pratique/article/transports#sommaire_10">Transports en commun</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Importation de véhicule</h3>
<p>Les normes chiliennes d’importation d’un véhicule sont très strictes. Les taxes et les frais de mise aux normes chiliennes sont élevés. Il est, par conséquent, préférable d’acheter un véhicule sur place correspondant aux normes locales, plutôt que de l’importer.</p>
<p>A noter que l’importation de véhicules d’occasion (plus de deux ans) est interdite, sauf pour les personnes de nationalité chilienne ayant vécu plus d’un an à l’étranger. L’importation de véhicules est également possible dans les zones franches (Punta Arenas à l’extrême-sud et Arica à l’extrême-nord). On voit donc dans ces zones de nombreux véhicules d’occasion en provenance d’Asie. Des lois très strictes régissent l’importation dans les zones franches. Il est, par exemple, interdit d’importer un véhicule via une zone franche, puis de l’utiliser sur l’ensemble du territoire national.</p>
<p>Pour en savoir plus : <a href="http://www.aduana.cl/" class="spip_out" rel="external">site des douanes chiliennes</a></p>
<h3 class="spip"><a id="sommaire_2"></a>Permis de conduire</h3>
<p>Le permis de conduire français n’est pas reconnu pour les ressortissants français résident ou de passage. Toute personne désirant conduire sur le territoire chilien doit être munie d’un permis de conduire international délivré par la préfecture du lieu de résidence en France. Ce permis international est reconnu et est valable pour les autorités chiliennes pendant une durée d’un an à compter de la date d’émission du document.</p>
<p>Au-delà, elle devra échanger son permis de conduire contre un permis chilien auprès de la municipalité (mairie) de son quartier, après avoir passé un examen théorique en espagnol portant essentiellement sur le code de la route.</p>
<p>Le permis de conduire chilien doit être renouvelé tous les six ans.</p>
<h3 class="spip"><a id="sommaire_3"></a>Code de la route</h3>
<p>Certaines règles de circulation appliquées en France ne le sont pas au Chili. Par exemple, le dépassement par la droite est systématique et la priorité à droite n’est pas respectée.</p>
<p>Santiago est divisé en grandes avenues sur le modèle des villes nord-américaines. Aux carrefours des panneaux signalent les noms des rues croisées.</p>
<p>Certaines avenues de Santiago changent de sens deux à trois fois par jour afin d’absorber les flux de circulation Santiago-Banlieue / Banlieue-Santiago. Soyez donc très vigilants !</p>
<p>La pollution atmosphérique est forte à Santiago, particulièrement en hiver. Les jours de pic de pollution, un système de circulation alternée est mis en place : les voitures dont la plaque d’immatriculation se termine par tel ou tel numéro sont interdites de circuler. Les restrictions diffèrent selon que le véhicule est équipé ou non d’un pot catalytique.</p>
<h3 class="spip"><a id="sommaire_4"></a>Assurances et taxes</h3>
<p>L’affiliation à une compagnie d’assurances locale est obligatoire pour le risque tiers (compter 10 000 pesos à l’année pour un <i>seguro obligatorio</i>).</p>
<h3 class="spip"><a id="sommaire_5"></a>Achat et location</h3>
<p>Toutes les marques françaises et étrangères sont représentées au Chili. Le choix est donc vaste pour les voitures neuves. Les prix sont globalement moins élevés qu’en France. L’importation de véhicules d’occasion est interdite aux étrangers.</p>
<p>Il est conseillé d’acheter un véhicule robuste permettant de rouler sur le réseau secondaire (4x4 conseillé sur les pistes). Il existe un marché de l’occasion, mais le prix des véhicules usagés est assez élevé.</p>
<h3 class="spip"><a id="sommaire_6"></a>Immatriculation</h3>
<p>Tout propriétaire d’un véhicule automobile doit payer chaque année une vignette (<i>permiso de circulación</i>) dont le montant est fonction de la valeur de la voiture.</p>
<p>La plaque d’immatriculation est remise avec le véhicule.</p>
<p>Chaque véhicule doit être inscrit à la préfecture (<i>registro civil</i>). A l’issue de cette procédure, une carte grise (<i>padrón del auto</i>) vous sera délivrée.</p>
<p>Il vous faudra également être à jour pour le contrôle technique (<i>revisión técnica</i>).</p>
<h3 class="spip"><a id="sommaire_7"></a>Entretien</h3>
<p>La qualité de la main-d’œuvre est moyenne. Le coût est équivalent à celui de la France. Les garagistes ont tendance à surévaluer les factures. Le coût des pièces détachées est plus élevé en province qu’à Santiago.</p>
<h3 class="spip"><a id="sommaire_8"></a>Carburant</h3>
<p>Les carburants disponibles au Chili sont :</p>
<ul class="spip">
<li>le super,</li>
<li>le carburant sans plomb à 97 ou 95 octanes</li>
<li>le diesel.</li></ul>
<p>Les carburants alternatifs (biocarburants GPL et gaz naturel) sont peu répandus.</p>
<h3 class="spip"><a id="sommaire_9"></a>Réseau routier</h3>
<p>Le réseau routier est satisfaisant : routes bien asphaltées et pistes en bon état. Eviter toutefois de quitter les grands axes avec un véhicule de tourisme normal et emporter des réserves de carburant lors de longs trajets (espacement important des stations services).</p>
<p>Le réseau autoroutier (de type autoroute à péage) est excellent sur l’axe principal nord-sud (<i>ruta 5</i>), ainsi qu’entre Santiago et la côte.</p>
<p>La qualité du réseau peut être médiocre sur les routes secondaires (terre ou graviers fréquents). Le 4x4 devient nécessaire sur les pistes, notamment dans les parcs nationaux.</p>
<p>Un système de paiement automatique a été récemment mis en place sur les autoroutes urbaines de Santiago. Les automobilistes qui désirent emprunter ces autoroutes doivent installer sur leur véhicule un boitier appelé <a href="http://www.tag.cl/" class="spip_out" rel="external">TAG</a>qui permet la facturation automatique envoyée à domicile. Il est cependant possible d’acheter un pass journalier à l’entrée de l’autoroute.</p>
<h3 class="spip"><a id="sommaire_10"></a>Transports en commun</h3>
<h4 class="spip">Bus urbains (micros)</h4>
<p>Les bus urbains ne respectent généralement pas les règles minimales de conduite et représentent le principal danger en ville.</p>
<p>Les arrêts ne sont pas toujours bien définis. Il faut donc se renseigner auprès du chauffeur pour connaître l’arrêt auquel descendre. Les bus ne s’arrêtent pas automatiquement aux arrêts. Levez la main pour les arrêter.</p>
<p>Les horaires ne sont pas réguliers. Il peut arriver de devoir attendre le bus pendant une heure. Le projet Transantiago, nouveau dispositif de fonctionnement des transports publics de la capitale, incluant bus et métro, a été inauguré en 2007 et a pour but d’améliorer la fluidité du trafic.</p>
<p>Pensez à faire l’appoint pour payer le ticket.</p>
<p>Le panneau à gauche du pare-brise du bus indique les principales avenues et quartiers desservis. Prenez la précaution de toujours demander confirmation au chauffeur. Les bus qui montent (<i>suben</i>) se dirigent vers les quartiers situés près de la Cordillère des Andes. Les bus qui descendent (<i>bajan</i>) vont vers le centre ville et s’éloignent de la Cordillère des Andes.</p>
<h4 class="spip">Métro</h4>
<p>Le métro de Santiago est fiable, propre et surveillé. Le réseau est ouvert de 06h00 à 23h00.</p>
<p>Il existe deux tarifs : heures de pointe ($680) et heures creuses ($ 610).</p>
<p>La carte <i>Multivia ou Bip </i>est un porte-monnaie électronique que vous pouvez utiliser uniquement dans le métro. Elle présente l’avantage de pouvoir être rechargée une fois par semaine et d’éviter ainsi les queues interminables aux heures de pointe.</p>
<p>Un <a href="http://www.metro.cl/" class="spip_out" rel="external">plan du métro</a> de Santiago est disponible en ligne.</p>
<h4 class="spip">Taxis</h4>
<p>Les taxis se reconnaissent facilement : ils sont noirs avec un toit jaune. Les tarifs sont très bon marché. Il est recommandé de voyager en taxi la nuit, surtout à Santiago. Le tarif de base est de 200 pesos, puis 100 pesos par tranche de 200 mètres.</p>
<p>Il est préférable de donner une indication précise sur la destination car certaines avenues de Santiago s’étendent sur plusieurs dizaines de kilomètres. Par exemple « a Condell con Providencia » signifie « à l’intersection de la rue Condell et de l’avenue Providencia ».</p>
<h4 class="spip">Taxis collectifs (colectivos)</h4>
<p>Solution alternative au bus et moins coûteuse que le taxi, il est possible de prendre des taxis collectifs, reconnaissables à la plaque blanche sur le toit. Ceux-ci se divisent les destinations par secteurs. Le taxi ne part que lorsqu’il est rempli (quatre passagers).</p>
<h4 class="spip">Avion</h4>
<p>Compte tenu des distances, l’avion reste le mode de transport le plus rapide et le plus sûr pour des voyages vers le nord ou le sud. Deux compagnies proposent des vols intérieurs :</p>
<ul class="spip">
<li><a href="http://www.lan.cl/" class="spip_out" rel="external">Lanchile</a></li>
<li><a href="http://www.skyairline.cl/" class="spip_out" rel="external">Skyairline</a></li></ul>
<h4 class="spip">Bus interurbains</h4>
<p>Des compagnies d’autobus desservent l’ensemble du pays et les grandes villes des pays voisins dans des conditions de confort au choix du client. Les prix sont compétitifs, mais les temps de voyage peuvent parfois être très longs (28 heures pour Santiago-Arica – 2 086 km).</p>
<p>Des bus partent tous les heures de Santiago à destination de Valparaíso et plusieurs bus par jour circulent entre les principales villes du pays.</p>
<p>Les billets s’achètent sur place. Il n’est en général pas nécessaire de réserver, sauf pour les longs voyages. Arrivez tout de même une demi-heure à l’avance au terminal des bus.</p>
<h4 class="spip">Train</h4>
<p>La compagnie nationale (EFE) propose des liaisons entre Santiago et Chillán (402 km au sud de Santiago), ainsi que quelques liaisons régionales dans les VIIème et VIIIème régions. Le réseau est peu développé et les prix sont équivalents à un trajet en bus interurbain.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/chili/vie-pratique/article/transports). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
