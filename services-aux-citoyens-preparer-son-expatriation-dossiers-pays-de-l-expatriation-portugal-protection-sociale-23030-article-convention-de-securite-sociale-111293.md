# Convention de sécurité sociale

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/protection-sociale-23030/article/convention-de-securite-sociale-111293#sommaire_1">Travailleurs détachés</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/protection-sociale-23030/article/convention-de-securite-sociale-111293#sommaire_2">Travailleurs non détachés bénéficiant des dispositions prévues par les règlements communautaires</a></li></ul>
<p>Vous pouvez bénéficier de prestations de la part d’un seul Etat. Celui-ci vous versera alors les prestations en fonction de sa propre réglementation, majorées d’un complément si le montant des prestations correspondantes dans l’autre État est d’un montant supérieur :</p>
<ul class="spip">
<li>en priorité, le pays compétent est celui dans lequel vous exercez votre activité professionnelle, où les cotisations sont acquittées ;</li>
<li>si votre conjoint ou concubin exerce une activité professionnelle dans un autre État membre, le pays compétent sera celui dans lequel résident vos enfants ;</li>
<li>si ni vous, ni votre conjoint ou concubin n’exercez d’activité et si l’un de vous deux bénéficie d’une pension, c’est le pays qui vous verse cette pension qui est compétent pour vous verser les prestations familiales ;</li>
<li>enfin, si vous n’êtes ni en activité professionnelle, ni bénéficiaire d’une pension, c’est votre pays de résidence qui est compétent.</li></ul>
<p>Pour plus de renseignements, vous pouvez vous adresser à l’organisme portugais compétent pour vous verser les prestations familiales. En France, vous pouvez contacter la Caisse d’allocations familiales.</p>
<p>Les Français occupés au Portugal peuvent, s’ils le désirent, adhérer à l’assurance volontaire "expatriés" auprès de la Caisse des Français de l’étranger. Il convient de préciser qu’une telle adhésion ne dispense pas les intéressés des obligations d’assurance existant dans le pays de travail.</p>
<p>Bien entendu, les Français se trouvant au Portugal en tant que touristes, étudiants, retraités ou chômeurs cherchant un emploi, peuvent bénéficier également des règlements communautaires.</p>
<p>Tout renseignement complémentaire au sujet de l’application des règlements communautaires en matière de sécurité sociale peut être obtenu auprès du :</p>
<p><a href="http://www.cleiss.fr/docs/textes/rgt_index.html" class="spip_out" rel="external">Centre des liaisons européennes et Internationales de sécurité sociale</a><br class="manualbr">11, rue de la Tour des Dames<br class="manualbr">75436 PARIS Cedex 09<br class="manualbr">Tél. : 01.45.26.33.41<br class="manualbr">Télécopie : 01.49.95.06.50</p>
<h3 class="spip"><a id="sommaire_1"></a>Travailleurs détachés</h3>
<p>Si vous venez exercer, en tant que salarié d’une entreprise ou d’une administration établie en France, votre activité sur le territoire portugais, votre employeur maintient vos droits d’accès au régime français de sécurité sociale :</p>
<h4 class="spip">Soins de santé</h4>
<h5 class="spip">Vos droits aux soins de santé</h5>
<p>Afin d’être en mesure de bénéficier de la prise en charge des soins qui vous seront dispensés dans le pays de détachement comme si vous y étiez affilié, vous devez vous inscrire au régime local de sécurité sociale. Pour ce faire, vous devez demander le document portable S1 « Inscription en vue de bénéficier de la couverture d’assurance maladie » (équivalent du formulaire E 106) à la caisse française d’assurance maladie où se situe le siège social de votre employeur et le remettre à la caisse maladie locale du lieu de résidence.</p>
<p>Vous pouvez saisir directement votre caisse d’affiliation afin d’obtenir la prise en charge des frais médicaux. Les plafonds de remboursements français s’appliquent alors. Vous avez aussi la possibilité d’obtenir la prise en charge des frais exposés dans le pays de détachement en présentant votre carte européenne d’assurance maladie (CEAM).</p>
<p>Vous conservez votre qualité d’assuré du régime français pour tous les soins reçus en France.</p>
<h5 class="spip">Les droits de vos ayants droit</h5>
<p>Il appartient à la caisse maladie locale à laquelle vous avez remis le document portable S1 d’examiner si vos ayants droit revêtent la qualité de membre de famille au sens de la législation locale de sécurité sociale.</p>
<p>Dans l’affirmative, ils jouiront des mêmes droits que vous et dans les mêmes conditions.</p>
<h4 class="spip">En cas d’arrêt de travail</h4>
<p>En cas d’incapacité de travail, vous devez vous adresser à un médecin traitant local dès le début de l’incapacité de travail pour obtenir un certificat médical, que vous devrez adresser directement à votre caisse d’assurance maladie française.</p>
<p>Le service du contrôle médical pourra, s’il l’estime nécessaire, demander que vous soyez soumis à un contrôle par l’institution portugaise.</p>
<p>Il vous appartient de transmettre à votre employeur, un certificat d’arrêt de travail ou un certificat d’incapacité de travail délivré par le médecin traitant. Vos prestations en espèces ou indemnités journalières de l’assurance maladie sont examinées et servies directement par l’institution française d’affiliation.</p>
<h4 class="spip">Prestations familiales</h4>
<h5 class="spip">Si votre famille vous accompagne dans le pays de détachement</h5>
<p>En pratique, vous devrez informer la caisse française d’allocations familiales de votre ancien domicile en France de votre changement de résidence afin que votre dossier soit examiné en application de la réglementation européenne.</p>
<p>En fonction de votre activité professionnelle, de la situation de votre conjoint et de la résidence de vos enfants, les prestations familiales seront servies par la France et/ou le pays de détachement.</p>
<p><strong>Vous êtes détaché(e) du régime français de sécurité sociale, votre conjoint(e) ne travaille pas</strong></p>
<p>Vous êtes maintenu au régime français de sécurité sociale et la caisse française d’allocations familiales reste compétente pour vous verser les prestations familiales selon la législation qu’elle applique. A ce titre, vous êtes susceptible de percevoir les prestations familiales françaises exportables dans le pays de détachement. Un différentiel peut vous être versé si les conditions portugaises sont plus favorables que les françaises.</p>
<p><strong>Vous êtes détaché(e) du régime français de sécurité sociale, votre conjoint(e) travaille ou se trouve dans une situation assimilée dans le pays de détachement</strong></p>
<p>La caisse portugaise est alors compétente pour servir les prestations familiales. Dans l’hypothèse où les prestations locales seraient d’un montant plus faible que les prestations françaises, un complément différentiel pourrait vous être servi par l’organisme français en raison de votre affiliation au régime français de sécurité sociale en tant que détaché(e). En pratique, vous devez déposer une demande de prestations familiales auprès de la caisse d’allocations familiales de votre lieu résidence en précisant que vous êtes détaché(e) du régime français.</p>
<h5 class="spip">Si votre famille ne vous accompagne pas dans le pays de détachement</h5>
<p>La caisse d’allocations françaises demeure compétente pour le versement des prestations.</p>
<h4 class="spip">Allocations chômage</h4>
<p>Que vous soyez licencié(e) en cours ou à l’issue de votre détachement, dans la mesure où vous résidez au Portugal, vos droits aux prestations chômage seront examinés conformément à la législation locale par l’institution compétente avec prise en compte de vos périodes d’emploi et d’assurance en France en tant que travailleur détaché. Vous pouvez, à titre complémentaire, vous inscrire auprès des services du Pôle emploi du lieu du siège social de votre ancien employeur en France, en cas de recherche d’emploi en France.</p>
<h3 class="spip"><a id="sommaire_2"></a>Travailleurs non détachés bénéficiant des dispositions prévues par les règlements communautaires</h3>
<p>Selon les dispositions européennes, vous serez, en tant qu’expatrié(e), exclusivement rattaché(e) au régime portugais de sécurité sociale et perdrez de fait la qualité d’assuré du régime français. Il convient alors d’avertir votre caisse d’assurance maladie de votre départ ou d’entrer en contact avec une caisse d’assurance maladie portugaise a posteriori.</p>
<p>Le travailleur expatrié est assujetti au régime portugais au titre de son activité dans ce pays. Des dispositions particulières sont toutefois applicables aux personnes qui exercent normalement une activité salariée sur le territoire de deux ou plusieurs États membres (personnel d’une entreprise effectuant des transports internationaux, V.R.P). Les prestations sont coordonnées.</p>
<h4 class="spip">Maladie, maternité</h4>
<p>Si l’intéressé est assuré auprès d’une caisse maladie portugaise, les périodes d’assurance, d’emploi ou de résidence accomplies sur le territoire d’un ou plusieurs autres États auxquels les règlements sont applicables, sont prises en compte, en tant que de besoin, par l’organisme portugais compétent pour l’examen des droits éventuels de l’intéressé aux prestations.</p>
<p>Le travailleur français non détaché aura donc intérêt à demander avant le départ à sa caisse d’affiliation, l’établissement du formulaire E 104 "Attestation concernant la totalisation des périodes d’assurance, d’emploi ou de résidence", qui sera à remettre à l’organisme portugais compétent.</p>
<h4 class="spip">Invalidité</h4>
<p>Les prestations en espèces ou indemnités journalières de l’assurance maladie sont examinées et servies directement dans les conditions prévues par la législation du pays d’affiliation.</p>
<p>Si l’incapacité de travail est survenue sur le territoire portugais, vous devez vous adresser directement à l’institution portugaise.</p>
<p>Si l’incapacité de travail est survenue en France, vous devez vous adresser à un médecin traitant dès le début de l’incapacité pour obtenir un certificat médical d’arrêt de travail que vous devrez transmettre directement à votre caisse d’affiliation étrangère dans les délais prévus par sa règlementation.</p>
<h4 class="spip">Vieillesse</h4>
<p>Les règlements européens prévoient une coordination en matière de sécurité sociale entre les institutions pour la liquidation des droits à pension.</p>
<p>L’âge à partir duquel il est possible de formuler une demande de pension de vieillesse diffère suivant l’Etat liquidateur de la pension. Les périodes de travail accomplies au Portugal ne font pas l’objet d’une validation par le régime français mais sont validées au regard de la législation de portugaise. Chaque institution auprès de laquelle vous avez cotisé, procède à un double calcul de la pension qu’elle doit vous verser :</p>
<ul class="spip">
<li>en fonction de sa seule législation (pension nationale)</li>
<li>en totalisant l’ensemble des périodes accomplies sous la législation de tout Etat membre et en proratisant en fonction des seules périodes accomplies sous la législation qu’elle applique (pension proportionnelle ou communautaire).</li></ul>
<p>Chaque institution retient le montant le plus avantageux après comparaison de la pension nationale et de la pension proportionnelle ou communautaire.</p>
<h4 class="spip">Prestations familiales</h4>
<p>La France est l’Etat prioritairement compétent pour vous servir les prestations familiales si votre conjoint y travaille. Vous percevez les prestations familiales conformément à ce que prévoit la législation française.</p>
<p>Dans l’hypothèse où les prestations françaises seraient d’un montant plus faible que les prestations servies par le système portugais, un complément différentiel pourra être servi par l’organisme local en raison de votre activité professionnelle exercée sur le territoire de cet Etat. En pratique, votre conjoint(e) devra déposer une demande de prestations familiales auprès de la caisse française d’allocations familiales du lieu de résidence en France en lui précisant que vous travaillez sur le territoire portugais.</p>
<p><strong>Remarque </strong> : la situation de votre conjoint, selon qu’il exerce une activité salariée au Portugal ou non, ou qu’il réside au Portugal ou non, est susceptible d’altérer la situation évoquée précédemment. Pour un complément d’information, merci de bien vouloir vous reporter au site du <a href="http://www.cleiss.fr/particuliers/partir_travailler__expatrie883.html" class="spip_out" rel="external">CLEISS</a>.</p>
<h4 class="spip">Chômage</h4>
<p>Au terme de votre contrat de travail, si vous restez vivre sur le territoire portugais, vous pourrez prétendre aux allocations chômage conformément à la législation locale. Si votre période d’activité est insuffisante pour vous ouvrir des droits, vous pouvez faire prendre en compte vos périodes d’emploi et d’assurance exercées sur le territoire d’un autre Etat membre de l’Union européenne.</p>
<p>Si cette période a eu lieu en France avant votre activité au Portugal, il convient de vous procurer le document portable U1 auprès de la Direction régionale des entreprises, de la concurrence, de la consommation, du travail et de l’emploi du lieu du siège de votre employeur. Ce formulaire sera à remettre à l’institution compétente lors de votre inscription comme demandeur d’emploi.</p>
<p>Si vous êtes au chômage au Portugal, où vous bénéficiez des prestations correspondantes, vous pouvez revenir en France pour y chercher un emploi, en conservant le droit à cette indemnisation à condition que :</p>
<ul class="spip">
<li>avant votre départ, vous ayez été inscrit comme demandeur d’emploi et soyez resté à la disposition des services de l’emploi portugais pendant au moins quatre semaines après le début du chômage ;</li>
<li>vous vous soyez inscrit dès votre arrivée en France auprès des services pour l’emploi et vous soyez soumis aux règles de contrôle organisées en France.</li></ul>
<p>Ce droit aux prestations pourra être maintenu pendant une période maximale de trois mois.</p>
<h4 class="spip">Pendant un séjour temporaire</h4>
<p>Le travailleur et les membres de sa famille qui effectuent un séjour temporaire en France ont droit aux prestations en nature (soins) si leur état vient à nécessiter immédiatement des soins.</p>
<p>Ces prestations pourront être servies par la caisse primaire d’assurance maladie du lieu des soins, sur présentation de la "Carte européenne d’assurance maladie" qui aura été établie avant le départ par la caisse portugaise d’affiliation.</p>
<h5 class="spip">A l’occasion d’un transfert de résidence au cours d’une période d’indemnisation pour maladie, maternité, accident du travail ou maladie professionnelle</h5>
<p>Le travailleur admis au bénéfice de prestations, sous réserve d’être autorisé par l’organisme portugais à retourner en France, conserve ses droits aux prestations. L’intéressé devra solliciter, avant le départ, l’établissement du formulaire E 112 "Attestation concernant le maintien des prestations en cours de l’assurance maladie-maternité" ou E 123 "Attestation de droit aux prestations en nature de l’assurance contre les accidents du travail et les maladies professionnelles".</p>
<h5 class="spip">Transfert de résidence pour se faire soigner</h5>
<p>Pour venir se faire soigner en France, l’assuré ou ses ayants droits doivent obtenir l’autorisation de la caisse d’assurance maladie portugaise.</p>
<p>Cette autorisation ne peut pas être refusée si les soins dont il s’agit figurent parmi les prestations prévues par la législation portugaise et si ces soins ne peuvent, compte tenu de l’état de santé du requérant, être dispensés dans un délai normalement nécessaire.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/protection-sociale-23030/article/convention-de-securite-sociale-111293). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
