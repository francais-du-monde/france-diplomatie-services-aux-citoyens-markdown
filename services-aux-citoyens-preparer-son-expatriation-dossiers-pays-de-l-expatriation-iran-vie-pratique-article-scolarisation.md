# Scolarisation

<p>Pour toute information sur la scolarisation dans le système français à l’étranger, vous pouvez consulter notre thématique sur les études et la scolarisation sur le site de France Diplomatie..</p>
<p>Vous y trouverez des renseignements sur :</p>
<ul class="spip">
<li>les établissements français du primaire et du secondaire à l’étranger ;</li>
<li>les bourses scolaires et la prise en charge des frais de scolarité à l’étranger ;</li>
<li>les possibilités qui s’offrent à vous si votre enfant ne peut être scolarisé à l’étranger dans le système français (enseignement à distance par le CNED, programme français langue maternelle (FLAM), internats en France) ;</li>
<li>les épreuves du baccalauréat à l’étranger ;</li>
<li>les bourses d’études supérieures en France et à l’étranger ;</li>
<li>l’équivalence des diplômes.</li></ul>
<h4 class="spip">Les établissements scolaires français en Iran</h4>
<h4 class="spip">Ecole française de Téhéran</h4>
<p><strong>Etablissement conventionné</strong> avec l’<a href="http://www.aefe.fr/reseau-scolaire-mondial" class="spip_out" rel="external">AEFE</a> et la <a href="http://www.mlfmonde.org/" class="spip_out" rel="external">MLF</a>.</p>
<p>1553, Ave Shariati <br class="manualbr">1939613663 Téhéran – IRAN <br class="manualbr">Tél. : (+98 21) 22 64 17 05 - (+ 98 21) 22 60 49 12 - (+98 21) 22 61 54 56-8 <br class="manualbr">Fax : (+98 21) 22 60 49 22 <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/vie-pratique/article/scolarisation#ecolefrancaiseteheran#mc#yahoo.fr#" title="ecolefrancaiseteheran..åt..yahoo.fr" onclick="location.href=mc_lancerlien('ecolefrancaiseteheran','yahoo.fr'); return false;" class="spip_mail">Courriel</a></p>
<p><strong>Niveau d’enseignements homologués</strong></p>
<ul class="spip">
<li><strong>Ecole maternelle</strong> :
<br>— très petite section (TPS),
<br>— petite section (PS),
<br>— moyenne section (MS)
<br>— grande section (GS)</li>
<li><strong>Ecole élémentaire</strong> : du CP au CM2 homologué</li>
<li><strong>Collège</strong> : enseignement direct homologué (De la 6ème à la 3ème)</li>
<li><strong>Lycée</strong> : enseignement direct homologué (De la 2nde à la Terminale)</li></ul>
<p><strong>Année scolaire :</strong> de septembre à juin</p>
<p><strong>Langues enseignées </strong></p>
<ul class="spip">
<li><strong>Ecole élémentaire :</strong> anglais et persan</li>
<li><strong>Secondaire :</strong> anglais, allemand, espagnol, latin</li></ul>
<p><i>Mise à jour : février 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/iran/vie-pratique/article/scolarisation). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
