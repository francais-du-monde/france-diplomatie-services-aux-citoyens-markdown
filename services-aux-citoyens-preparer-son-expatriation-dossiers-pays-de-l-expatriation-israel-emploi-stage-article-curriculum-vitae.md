# Curriculum vitae

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/emploi-stage/article/curriculum-vitae#sommaire_1">Rédaction</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/emploi-stage/article/curriculum-vitae#sommaire_2">Diplômes</a></li></ul>
<p>La maîtrise de l’hébreu est impérative pour trouver un emploi en Israël.</p>
<p>Il est possible de suivre des cours privés dans des écoles de langues ou les <i><a href="http://www.moia.gov.il/Moia_fr/StudyingHebrew/WhatsUlpan.htm" class="spip_out" rel="external">oulpan</a></i> au tarif de 1500 dollars ou 1200 euros environ pour six mois à raison de neuf heures de cours par semaine. Les <i>oulpans</i> sont des cadres d’étude de la langue hébraïque sous la responsabilité conjointe du ministère de l’intégration, du ministère de l’éducation et de l’Agence juive.</p>
<p>Selon les professions, il pourra aussi être demandé de maîtriser un vocabulaire spécifique avancé (technique, légal, etc.). A cette fin, des <a href="http://www.moia.gov.il/Moia_fr/Employment/VocationalUlpan.htm" class="spip_out" rel="external">cours spécifiques d’hébreu professionnel existent dans les <i>oulpans</i></a>.</p>
<p>Pour plus d’information :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.jafi.org.il/JewishAgency/French/Delegations/Alyah-accueil-delegation-France/" class="spip_out" rel="external">site de l’Agence juive pour Israël</a> </p>
<h3 class="spip"><a id="sommaire_1"></a>Rédaction</h3>
<p>Les normes de présentation d’un C.V. en Israël sont très proches de ce qu’on peut trouver en France :</p>
<ul class="spip">
<li>Une page est la norme, ne jamais dépasser deux pages même en cas de longue carrière, éviter les mises en page trop denses.</li>
<li><strong>Informations personnelles</strong> : nom, prénom, date de naissance, date d’immigration en Israël, numéros de téléphone, adresse mail, adresse postale (pensez à indiquer que vous êtes mobile si cette dernière est éloignée du lieu de travail). Il est très difficile d’intéresser une entreprise locale si vous n’avez pas déjà des coordonnées en Israël.</li>
<li><strong>Objectifs</strong> : à mentionner si vous avez un objectif précis, deux lignes maximum</li>
<li><strong>Parcours professionnel</strong> : expériences passées classées par ordre chronologique inverse. Nom de la société, secteur, poste occupé, pensez à bien décrire précisément les fonctions réelles exercées et les compétences gagnées, surtout pour les expériences les plus récentes.</li>
<li><strong>Formation</strong> : années d’études, établissements, diplômes et certificats obtenus. Il est possible de mentionner le baccalauréat, mais ce n’est pas indispensable si vous avez un diplôme supérieur.</li>
<li><strong>Compétences supplémentaires</strong> : par exemple les outils ou les logiciels ou environnements informatiques maîtrisés, etc.</li>
<li><strong>Langues</strong> : le fait que l’hébreu ne soit pas votre langue maternelle peut être un plus pour votre CV. Précisez cependant toujours votre niveau d’hébreu même s’il est basique.</li>
<li><strong>Service militaire</strong> : à préciser s’il a été accompli en Israël</li>
<li><strong>Loisirs</strong> : rubrique facultative à maintenir très brève car elle n’est que très rarement significative pour les recruteurs</li>
<li><strong>Références</strong> : explicites, ou encore « disponibles sur demande », privilégier les références locales de nature à rassurer les employeurs sur l’adaptation du candidat au monde du travail « à l’israélienne »</li>
<li>Adapter le CV au poste visé, un CV trop générique a moins de chances d’attirer l’attention qu’un CV ciblé.</li>
<li>Eviter à tout prix les fautes d’orthographe, faire relire le CV par un tiers.</li>
<li>Il est recommandé de traduire (ou faire traduire) votre CV en hébreu, mais il est courant que les candidatures envoyées comportent deux versions du CV : l’une en hébreu, l’autre en anglais ou français.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>Diplômes</h3>
<p>Afin d’augmenter vos chances de trouver un emploi, il est recommandé de faire évaluer et valider vos diplômes, certificats et qualifications par les services spécialisés du ministère de l’Education israélien dès son arrivée dans le pays en raison de la longueur de la procédure administrative (deux à trois mois). Pour certaines <a href="http://www.moia.gov.il/Moia_fr/Employment/VocationalUlpan.htm" class="spip_out" rel="external">professions soumises à une autorisation d’exercer</a>, cette étape est indispensable.</p>
<p>Pour plus d’information :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.moia.gov.il/Moia_fr/Employment/Employment.htm" class="spip_out" rel="external">Premiers pas dans l’intégration à l’emploi</a> sur le site du ministère de l’Immigration israélien.</p>
<p><i>Mies à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/israel/emploi-stage/article/curriculum-vitae). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
