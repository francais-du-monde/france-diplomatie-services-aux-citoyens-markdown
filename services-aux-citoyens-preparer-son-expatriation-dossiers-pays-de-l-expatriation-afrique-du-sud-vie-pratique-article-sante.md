# Santé

<p>Pour une présentation des risques épidémiologiques ainsi que des conseils sur la médecine au quotidien, consultez les pages suivantes :</p>
<ul class="spip">
<li>Conseils, liste de médecins et hôpitaux sur le site du <a href="http://www.consulfrance-jhb.org/Liste-de-notoriete-medicale" class="spip_out" rel="external">consulat de France à Johannesburg</a>) et du <a href="http://www.consulfrance-lecap.org/Medecins-liste-de-notoriete,844" class="spip_out" rel="external">consulat de France au Cap</a> ;</li>
<li>Page dédiée à la santé en <a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/afrique-du-sud/" class="spip_in">Afrique du Sud</a> sur le site Conseils aux voyageurs ;</li>
<li>Fiches Johannesburg et Le Cap sur le site du <a href="http://www.cimed.org/" class="spip_out" rel="external">CIMED</a>.</li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/afrique-du-sud/vie-pratique/article/sante). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
