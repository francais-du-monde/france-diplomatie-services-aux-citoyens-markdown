# Mexique

<p>Au <strong>31 décembre 2014</strong>, 17 728 Français étaient inscrits au registre des Français établis hors de France pour le Mexique. 66,1 % d’entre eux sont double-nationaux. Le Consulat général évalue à un total de <strong>30 000 personnes</strong> la présence française dans le pays.</p>
<p>La communauté française est l’une des <strong>plus importantes communautés étrangères au Mexique</strong> (deuxième européenne derrière l’Espagne) et la deuxième en Amérique latine (après celle du Brésil). Elle se place au 22ème rang mondial. Elle est une <strong>des plus anciennes</strong>. Elle est constituée de trois groupes distincts : d’une part, les descendants des immigrants arrivés dès la première moitié du 19ème siècle, puis ceux arrivés peu avant et après la deuxième guerre mondiale, et enfin, les nouveaux expatriés.</p>
<p>Pour l’anecdote, rappelons qu’en 1821, année de l’indépendance du Mexique, les trois frères Arnaud, natifs de la vallée de l’Ubaye, arrivent dans le pays. Leur exemple sera suivi de la majorité des habitants de la région, qui tenteront leur chance dans le Nouveau Monde. Ce mouvement historique ne cessera qu’au milieu du 20ème siècle. De grands groupes industriels et commerciaux ont été créés par des familles françaises (les grands magasins Liverpool, Paris-Londres, Palacio de Hierro, les groupes industriels Rio Blanco, Orizaba).</p>
<p>Les Français nouvellement expatriés sont souvent des cadres travaillant dans le secteur tertiaire et industriel, installés principalement à Mexico et ses environs immédiats, à Guadalajara, à Puebla, à Querétaro et dans une moindre mesure à Monterrey. Toutefois, on note un flux important de personnes tentant, notamment dans les zones touristiques côtière, de créer une entreprise ou un commerce (création de restaurants, d’hôtels, d’agences de sports nautiques…)</p>
<p>On compte près de <strong>400 établissements français</strong> à capital français présents au Mexique dans tous les secteurs d’activité : industrie pharmaceutique et cosmétique, métallurgie, chimie et pétrochimie, fabrication du matériel électrique, télécommunications, aéronautique, agroalimentaire et secteur automobile.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.diplomatie.gouv.fr/fr/dossiers-pays/mexique/" class="spip_in">Une description du Mexique, de sa situation politique et économique</a> ;</li>
<li><a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays/mexique/" class="spip_in">Des informations actualisées sur <strong>les conditions locales de sécurité</strong> au Mexique</a> ;</li>
<li><a href="http://www.ambafrance-mx.org/-Francais-" class="spip_out" rel="external">Ambassade de France au Mexique</a>.</li></ul>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-mexique-entree-et-sejour.md">Entrée et séjour</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-mexique-entree-et-sejour-article-passeport-visa-permis-de-travail-108383.md">Passeport, visa, permis de travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-mexique-entree-et-sejour-article-demenagement.md">Déménagement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-mexique-entree-et-sejour-article-vaccination.md">Vaccination</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-mexique-entree-et-sejour-article-animaux-domestiques.md">Animaux domestiques</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-mexique-emploi-stage.md">Emploi, stage</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-mexique-emploi-stage-article-marche-du-travail-108388.md">Marché du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-mexique-emploi-stage-article-reglementation-du-travail-108390.md">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-mexique-emploi-stage-article-recherche-d-emploi.md">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-mexique-emploi-stage-article-curriculum-vitae.md">Curriculum vitae</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-mexique-emploi-stage-article-lettre-de-motivation-108393.md">Lettre de motivation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-mexique-emploi-stage-article-entretien-d-embauche.md">Entretien d’embauche</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-mexique-emploi-stage-article-stages.md">Stages</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-mexique-protection-sociale-22688.md">Protection sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-mexique-protection-sociale-22688-article-regime-local-de-securite-sociale-108397.md">Régime local de sécurité sociale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-mexique-protection-sociale-22688-article-convention-de-securite-sociale-108398.md">Convention de sécurité sociale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-mexique-fiscalite.md">Fiscalité</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-mexique-fiscalite-article-fiscalite-du-pays.md">Fiscalité du pays</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-mexique-fiscalite-article-convention-fiscale-108400.md">Convention fiscale</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-mexique-vie-pratique.md">Vie pratique</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-mexique-vie-pratique-article-logement-108402.md">Logement</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-mexique-vie-pratique-article-sante.md">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-mexique-vie-pratique-article-scolarisation.md">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-mexique-vie-pratique-article-cout-de-la-vie-108405.md">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-mexique-vie-pratique-article-transports.md">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-mexique-vie-pratique-article-communications.md">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-mexique-vie-pratique-article-loisirs-et-culture.md">Loisirs et culture</a></li>
</ul>
<ul class="spip">
<li class="premier"><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-mexique-presence-francaise.md">Présence française</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-mexique-presence-francaise-article-presence-francaise.md">Présence française</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/mexique/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
