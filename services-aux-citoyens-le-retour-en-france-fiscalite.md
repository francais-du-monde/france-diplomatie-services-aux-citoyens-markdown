# Fiscalité

<h4 class="spip">Les modalités de l’imposition au retour</h4>
<p>Les modalités de l’imposition à laquelle vous serez soumis seront fonction de votre précédent régime fiscal (imposable en France ou à l’étranger).</p>
<p>L’année de retour, la date limite de dépôt est celle fixée pour les résidents.</p>
<p>Votre déclaration doit être envoyée au service des impôts dont vous dépendez. Il peut s’agir :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  du service des impôts des particuliers des non-résidents (SIPNR), si vous disposiez de revenus de source française durant votre séjour à l’étranger. N’oubliez pas de signaler votre nouvelle adresse en France afin que le SIPNR puisse transmettre votre dossier au service des impôts dont vous dépendrez à raison de votre domicile.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  du service des impôts dont relève votre nouveau domicile, si vous ne disposiez d’aucun revenu de source française durant votre séjour à l’étranger. Vous devrez également signaler votre nouvelle adresse à la perception à laquelle vous étiez rattaché pendant votre séjour à l’étranger. </p>
<blockquote class="texteencadre-spip spip"><strong>Service des impôts des particuliers des non-résidents</strong><br class="manualbr">10 rue du Centre - TSA 10001 - 93465 Noisy le Grand cedex<br class="manualbr">Téléphone : 01 57 33 83 00 <br class="manualbr">Télécopie : 01 57 33 81 02 ou 01 57 33 81 03<br class="manualbr">Courriel :<a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/fiscalite/#nonresidents#mc#dgfip.finances.gouv.fr#" title="nonresidents..åt..dgfip.finances.gouv.fr" onclick="location.href=mc_lancerlien('nonresidents','dgfip.finances.gouv.fr'); return false;" class="spip_mail">sip.nonresidents<span class="spancrypt"> [at] </span>dgfip.finances.gouv.fr</a> </blockquote>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.impots.gouv.fr/portal/dgi/public/particuliers.impot?espId=1&amp;pageId=part_horsfrance&amp;sfid=1250" class="spip_out" rel="external">Impots.gouv.fr</a> </p>
<p><i>Mise à jour : avril 2015</i></p>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/le-retour-en-france/fiscalite/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
