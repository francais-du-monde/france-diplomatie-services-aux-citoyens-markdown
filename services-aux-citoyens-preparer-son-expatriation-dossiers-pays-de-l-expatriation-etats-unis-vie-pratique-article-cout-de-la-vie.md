# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/vie-pratique/article/cout-de-la-vie#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/vie-pratique/article/cout-de-la-vie#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/vie-pratique/article/cout-de-la-vie#sommaire_3">Budget</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/vie-pratique/article/cout-de-la-vie#sommaire_4">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>L’unité monétaire est le dollar américain (USD).</p>
<p>Le dollar se divise en 4 <i>quarters</i>, en 10 <i>dimes</i> ou en 100 <i>cents</i>.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Le site des <a href="http://fr.exchange-rates.org/" class="spip_out" rel="external">taux de change et devises internationales</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<p>Il est possible de se procurer des dollars, soit directement par le change sur place ou encore par voie bancaire, en effectuant des transferts de fonds de compte à compte. Les principales banques françaises sont implantées aux Etats-Unis, mais ce sont en règle générale des filiales "affaires".</p>
<p>Il existe également sur place un grand nombre de banques à capitaux américains où l’expatrié pourra ouvrir un compte.</p>
<p>Pour ouvrir un compte bancaire aux États-Unis, il conviendra de présenter votre visa, votre numéro d’assurance sociale, un justificatif de domicile et les coordonnées de votre employeur.</p>
<p>Une <i>carte de débit</i> vous sera délivrée (équivalent de notre carte de crédit française que l’on utilise le plus souvent avec un code mais le débit est immédiat sur votre compte courant). Petit plus, elle permet de retirer de l’argent à la caisse de la plupart des supermarchés. En revanche, si vous retirez de l’argent dans une autre banque que la vôtre, une commission sera prélevée.</p>
<p>Il est plus difficile d’obtenir une première <i>carte de crédit</i> aux États-Unis, les banques refusant généralement de délivrer une carte de crédit aux nouveaux arrivants, le temps qu’ils se constituent un "credit history" (historique de crédit). Après le paiement régulier des premières factures et des premiers loyers sur le sol américain, vous obtiendrez un historique de crédit satisfaisant qui vous permettra d’effectuer une demande de carte de crédit. La carte de crédit permet également de régler ses achats, (en général sans utiliser de code, mais avec une signature du ticket de caisse). Le montant en est alors reporté sur votre compte de crédit, et vous recevez en fin de mois le montant de tous vos achats effectués avec cette carte. C’est l’équivalent d’une carte à débit différé, à la différence (notable) près que le remboursement à la fin du mois n’est pas automatique. Vous pouvez choisir de ne payer qu’une partie du montant mensuel de vos achats : vous restez alors débiteur de l’autre partie qui reste donc en <i>credit</i>, soumis à intérêts, sur votre compte. L’intérêt du compte de crédit est donc celui d’un crédit mensuel gratuit (ou débit différé), qui permet en outre de documenter une <i>credit history</i> (attestant que vous avez à chaque fois réglé votre crédit dans les temps). Or, c’est cette <i>credit history</i> qui est réclamée comme garantie lorsque vous voulez faire un emprunt à la banque.</p>
<p>Tous les moyens de paiement sont utilisés (espèces, cartes bancaires, carnets de chèques, travellers chèques). Les cartes de paiement ou de crédit sont acceptées par la plupart des points de vente sur tout le territoire américain. Elles sont le mode de paiement le plus courant aux Etats-Unis. Elles peuvent émaner soit d’une banque française adhérant à un grand réseau (Visa, Mastercard, etc) soit d’une banque américaine, soit même d’une entreprise non financière (la plupart des compagnies aériennes, des constructeurs automobiles, des distributeurs de carburant ont leurs propres cartes).</p>
<p>Les chèques de banques américaines ne sont acceptés que dans l’Etat de résidence de la Banque et dans certains points de vente.</p>
<h3 class="spip"><a id="sommaire_3"></a>Budget</h3>
<p><strong>Estimation du budget mensuel moyen</strong></p>
<p>Le candidat à l’expatriation devra se garder de chercher à apprécier sa situation en tentant de comparer de façon partielle son pouvoir d’achat à l’étranger et en France. Cette appréciation doit se faire avant tout sur le montant global des dépenses correspondant à son budget dans le pays d’expatriation.</p>
<h3 class="spip"><a id="sommaire_4"></a>Evolution des prix</h3>
<p>Selon l’enquête <a href="http://www.mercer.com/" class="spip_out" rel="external">Mercer</a> sur le coût de la vie en 2012, New York occupe la 32ème place dans la liste des 50 villes les plus chères du monde, tandis que Paris se place au 37ème rang.</p>
<p><strong>Indice de comparaison du coût de la vie</strong></p>
<p>En février 2014, selon le département statistique du ministère américain du Travail (<i>Bureau of Labor Statistics, Department of Labor</i> - BLS), l’indice de comparaison du coût de la vie tous postes de dépenses confondus, s’établit comme suit :</p>
<table class="spip" summary="">
<caption><strong>Période de base : 1982-1984 = 100 </strong></caption>
<tbody>
<tr class="row_odd odd">
<td>Atlanta = 216</td>
<td>Miami = 239</td></tr>
<tr class="row_even even">
<td>Boston = 252</td>
<td>Philadelphie = 241</td></tr>
<tr class="row_odd odd">
<td>Chicago = 223</td>
<td>New York = 257</td></tr>
<tr class="row_even even">
<td>Houston = 209</td>
<td>San Francisco = 245</td></tr>
<tr class="row_odd odd">
<td>Los Angeles = 238</td>
<td>Washington = 153</td></tr>
</tbody>
</table>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><strong>Pour en savoir plus :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Le site du <a href="http://www.bls.gov/cpi/" class="spip_out" rel="external">Bureau américain des statistiques (BLS)</a>. </p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/etats-unis/vie-pratique/article/cout-de-la-vie). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
