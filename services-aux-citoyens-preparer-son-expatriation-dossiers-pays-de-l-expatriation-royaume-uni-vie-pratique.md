# Vie pratique

<h2 class="rub22766">Logement</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/vie-pratique/#sommaire_1">Où se loger ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/vie-pratique/#sommaire_2">Conditions de location</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/vie-pratique/#sommaire_3">Hôtels</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/vie-pratique/#sommaire_4">Electricité</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/vie-pratique/#sommaire_5">Eau</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/vie-pratique/#sommaire_6">Electroménager</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Où se loger ?</h3>
<p>Le prix des locations dans les différentes parties du Royaume-Uni diffère sensiblement. Le Sud-Est ainsi que la région de Londres sont les plus chères du royaume ; le Nord de l’Angleterre, l’Ecosse, le Pays de Galle et l’Ulster sont moins chers, dans le cas où l’on ne choisit pas une grande ville.</p>
<p>A Londres, les quartiers résidentiels sont situés au centre ouest de la ville (Mayfair, Kensington, Chelsea, Belgravia et Hyde Park) et dans certaines banlieues prisées (ouest : Chiswick, sud : Wimbledon, nord : Islington).</p>
<p>South Kensington, qui abrite le Consulat général et le Lycée Français, autrefois considéré comme "le" quartier français par excellence, est devenu inaccessible pour la plupart des Français récemment arrivés à Londres. Ces derniers préfèrent s’installer dans des quartiers périphériques plus abordables.</p>
<p>L’achat est courant. Il peut se faire directement en négociant avec le propriétaire ou par l’intermédiaire d’agents immobiliers <i>(Estate Agents)</i>, très nombreux dans le pays.</p>
<p>Il existe deux formules de propriété :</p>
<ul class="spip">
<li>la propriété <i>leasehold </i>ou bail (baux de 99 ans en général, l’achat pouvant intervenir en cours de bail)</li>
<li>la propriété <i>freehold</i>, libre de tout bail mais plus chère.</li></ul>
<p>Pour l’achat d’un logement, l’emprunt <i>(mortgage)</i> est possible auprès des banques et des sociétés spécialisées, les <i>Building Societies</i>. Ces organismes financiers accordent généralement des prêts pouvant atteindre trois fois le revenu annuel. Il est préférable de tenir compte de la durée du séjour au Royaume-Uni et des variations des taux d’intérêt, notamment pour choisir la formule la plus avantageuse entre achat et location.</p>
<p><strong>Les médias spécialisés</strong></p>
<p>Certains journaux proposent des petites annonces immobilières gratuites :</p>
<ul class="spip">
<li><a href="http://www.loot.com/" class="spip_out" rel="external">LOOT</a> (journal britannique d’annonces de référence, parution journalière)</li>
<li><strong>London Weekly Advertiser </strong>(parution le mercredi)</li>
<li><strong>The evening standard </strong>(parution le mercredi d’un supplément gratuit Homes Property)</li>
<li><a href="http://www.ici-londres.com/" class="spip_out" rel="external">Ici Londres</a> (magazine français de petites annonces à Londres)</li>
<li>The Times</li>
<li>The Daily Telegraph</li>
<li>The Guardian</li>
<li><a href="http://www.londonmacadam.com/" class="spip_out" rel="external">Londonmacadam.com</a></li>
<li><a href="http://www.easyexpat.com/" class="spip_out" rel="external">Easyexpat.com</a></li></ul>
<p>La <strong>mairie </strong>pourra éventuellement vous donner la liste des propriétaires qui proposent des logements bon marché à louer dans votre quartier.</p>
<p><strong>Attention</strong> : la loi interdit aux agences immobilières de facturer leurs listes de logements à louer ou à vendre.</p>
<h4 class="spip">Logement des jeunes</h4>
<p>Pour les jeunes entre 18 et 25 ans, le Centre Charles Péguy, dispose d’un service logement.</p>
<p><a href="http://www.centrecharlespeguy.co.uk/" class="spip_out" rel="external">Centre Charles Péguy</a><br class="manualbr">114 – 116 Curtain Road <br class="manualbr">EC2A 3AH LONDON Old Street – Exit 3<br class="manualbr">Tél. : 0207 749 77 14 <br class="manualbr">Fax : 0207 749 77 19<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/vie-pratique/#info#mc#centrecharlespeguy.co.uk#" title="info..åt..centrecharlespeguy.co.uk" onclick="location.href=mc_lancerlien('info','centrecharlespeguy.co.uk'); return false;" class="spip_mail">Courriel</a></p>
<h5 class="spip">Résidences universitaires</h5>
<p>Pendant les vacances scolaires de juin/juillet à septembre, les logements universitaires (students halls) d’ordinaire réservés aux étudiants, peuvent être loués par des personnes étrangères à l’université dans certaines résidences universitaires.</p>
<p>Vous pouvez vous renseigner auprès de :</p>
<ul class="spip">
<li><a href="http://www.lon.ac.uk/" class="spip_out" rel="external">ULAO</a> (University of London Office)</li>
<li><a href="http://www.ucl.ac.uk/" class="spip_out" rel="external">University College London</a></li>
<li><a href="http://www.kcl.ac.uk/" class="spip_out" rel="external">King’s College London</a></li>
<li><a href="http://www.lsevacations.co.uk/" class="spip_out" rel="external">The London School of Economics</a></li></ul>
<p>Le nombre de places étant limité, il est impératif de se renseigner et de réserver longtemps à l’avance. Le prix des chambres est très variable selon les résidences universitaires.</p>
<h4 class="spip">Aides au logement</h4>
<p>Selon certaines conditions de revenus, deux aides peuvent être consenties localement par les mairies (<i>local council</i>) :</p>
<ul class="spip">
<li>le <i>Housing Benefit</i> (HB), également appelé <i>rent rebate</i> ou <i>"rent allowance"</i>, est une aide au logement consentie pour une période déterminée ;</li>
<li>le <i>Council Tax Benefit</i> (CTB) est destiné aux personnes assujetties à la <i>council tax</i>. La prise en charge peut être partielle ou totale en fonction de la situation personnelle du demandeur.</li></ul>
<p>Généralement, les étudiants ne peuvent en bénéficier.</p>
<p>Pour plus d’information, le site internet officiel du <a href="http://www.dwp.gov.uk/" class="spip_out" rel="external">Department for Work and Pensions</a> fournit tous les renseignements et contacts utiles sur les différentes formes d’aides sociales.</p>
<h3 class="spip"><a id="sommaire_2"></a>Conditions de location</h3>
<h4 class="spip">Location</h4>
<p>Le montant du loyer sur les petites annonces peut être indiqué à la semaine <i>per week</i> (pw) ou au mois <i>"per month" </i>(pm). Il est important de se faire délivrer un contrat de bail <i>(tenancy agreement) </i>en bonne et due forme ; mentionnez notamment la durée du préavis et la prise en compte éventuelle de la taxe d’habitation dans le montant du loyer. Par ailleurs, il est préférable d’établir un état des lieux à l’arrivée et de demander des reçus pour tout paiement effectué.</p>
<p>Il est plus aisé de trouver un appartement meublé <i>(furnished) </i>et il n’y a guère de différence de prix entre une location vide ou meublée. Le délai de recherche varie de deux à trois semaines. Vu les prix très élevés, il peut être préférable pour un célibataire de partager un appartement <i>(flat sharing)</i>. On peut trouver ses colocataires par annonce dans la presse ou par les agences immobilières.</p>
<p>Un dépôt de garantie (d’un montant souvent égal à un mois de loyer) vous sera sans doute réclamé à la signature du loyer.</p>
<p>Si une clause du bail ne vous semble pas claire, n’hésitez pas à prendre conseil avant de le signer. Les <i>Law Centres</i> (centres de consultation juridique), les <i>Citizens Advice Bureaux</i> (bureaux de consultation pour les citoyens) ou l’ONG sociale Shelter, qui gère un service national d’assistance téléphonique 24 h sur 24 pour les problèmes liés au logement (tél : 0808 800 4444), vous orienteront et vous conseilleront gratuitement.</p>
<p>Les <i>solicitors</i> (conseils juridiques) peuvent aussi vous aider à régler ce type de problème. La plupart vous factureront des honoraires, mais certains pourront vous proposer un court entretien (une demi-heure maximum) gratuit ou à tarif réduit. Des avocats bénévoles de langue française répondent à vos questions tous les jeudis à 19h (sauf mois d’août et vacances de Noël) à la crypte de St Mary’s Church – Upper Street N1 2TX (suivre « Islington legal advice center »). Métro : Angel. Il est conseillé d’apporter une photocopie des documents relatifs à votre question.</p>
<h5 class="spip">Charges</h5>
<p>Le locataire doit acquitter une taxe d’habitation <i>(Council Tax)</i>, fixée par la mairie en fonction de la valeur et de la situation de l’immeuble.</p>
<h4 class="spip">Marché locatif</h4>
<h5 class="spip">Londres</h5>
<p>A Londres, le logement est le problème majeur car les loyers sont extrêmement élevés.</p>
<p>Les baux sont généralement d’un an renouvelable, résiliables après six mois, avec deux mois de préavis. Le loyer est payable d’avance (un mois) ; une caution représentant quatre à six semaines de loyer est demandée.</p>
<p><strong>Les prix (en euros) de la location à Londres se situent dans les fourchettes suivantes (par mois). Ces montants restent indicatifs.</strong></p>
<table class="spip">
<thead><tr class="row_first"><th id="id59fb_c0">Quartiers</th><th id="id59fb_c1">1 chambre</th><th id="id59fb_c2">2 chambres</th><th id="id59fb_c3">3 chambres</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<th headers="id59fb_c0" id="id59fb_l0">Covent Garden (WC2)</th>
<td class="numeric " headers="id59fb_c1 id59fb_l0">2353</td>
<td class="numeric " headers="id59fb_c2 id59fb_l0">3849</td>
<td class="numeric " headers="id59fb_c3 id59fb_l0">8818</td></tr>
<tr class="row_even even">
<th headers="id59fb_c0" id="id59fb_l1">Central London (W1)</th>
<td class="numeric " headers="id59fb_c1 id59fb_l1">2746</td>
<td class="numeric " headers="id59fb_c2 id59fb_l1">4625</td>
<td class="numeric " headers="id59fb_c3 id59fb_l1">8086</td></tr>
<tr class="row_odd odd">
<th headers="id59fb_c0" id="id59fb_l2">Paddington (W2)</th>
<td class="numeric " headers="id59fb_c1 id59fb_l2">2217</td>
<td class="numeric " headers="id59fb_c2 id59fb_l2">3380</td>
<td class="numeric " headers="id59fb_c3 id59fb_l2">6022</td></tr>
<tr class="row_even even">
<th headers="id59fb_c0" id="id59fb_l3">Hammersmith (W6)</th>
<td class="numeric " headers="id59fb_c1 id59fb_l3">1458</td>
<td class="numeric " headers="id59fb_c2 id59fb_l3">2151</td>
<td class="numeric " headers="id59fb_c3 id59fb_l3">2839</td></tr>
<tr class="row_odd odd">
<th headers="id59fb_c0" id="id59fb_l4">Kensington (W8)</th>
<td class="numeric " headers="id59fb_c1 id59fb_l4">2337</td>
<td class="numeric " headers="id59fb_c2 id59fb_l4">3953</td>
<td class="numeric " headers="id59fb_c3 id59fb_l4">6399</td></tr>
<tr class="row_even even">
<th headers="id59fb_c0" id="id59fb_l5">Notting Hill (W11)</th>
<td class="numeric " headers="id59fb_c1 id59fb_l5">2228</td>
<td class="numeric " headers="id59fb_c2 id59fb_l5">3281</td>
<td class="numeric " headers="id59fb_c3 id59fb_l5">8010</td></tr>
<tr class="row_odd odd">
<th headers="id59fb_c0" id="id59fb_l6">Sheperd’s Bush (W12)</th>
<td class="numeric " headers="id59fb_c1 id59fb_l6">1321</td>
<td class="numeric " headers="id59fb_c2 id59fb_l6">1725</td>
<td class="numeric " headers="id59fb_c3 id59fb_l6">2260</td></tr>
<tr class="row_even even">
<th headers="id59fb_c0" id="id59fb_l7">Camden (NW1)</th>
<td class="numeric " headers="id59fb_c1 id59fb_l7">1916</td>
<td class="numeric " headers="id59fb_c2 id59fb_l7">2692</td>
<td class="numeric " headers="id59fb_c3 id59fb_l7">6443</td></tr>
<tr class="row_odd odd">
<th headers="id59fb_c0" id="id59fb_l8">Kilburn (NW6)</th>
<td class="numeric " headers="id59fb_c1 id59fb_l8">1474</td>
<td class="numeric " headers="id59fb_c2 id59fb_l8">2118</td>
<td class="numeric " headers="id59fb_c3 id59fb_l8">3423</td></tr>
<tr class="row_even even">
<th headers="id59fb_c0" id="id59fb_l9">St Johns Wood (NW8)</th>
<td class="numeric " headers="id59fb_c1 id59fb_l9">1993</td>
<td class="numeric " headers="id59fb_c2 id59fb_l9">3101</td>
<td class="numeric " headers="id59fb_c3 id59fb_l9">5962</td></tr>
<tr class="row_odd odd">
<th headers="id59fb_c0" id="id59fb_l10">Westminster (SW1)</th>
<td class="numeric " headers="id59fb_c1 id59fb_l10">2790</td>
<td class="numeric " headers="id59fb_c2 id59fb_l10">4455</td>
<td class="numeric " headers="id59fb_c3 id59fb_l10">7557</td></tr>
<tr class="row_even even">
<th headers="id59fb_c0" id="id59fb_l11">Brixton (SW2)</th>
<td class="numeric " headers="id59fb_c1 id59fb_l11">1256</td>
<td class="numeric " headers="id59fb_c2 id59fb_l11">1458</td>
<td class="numeric " headers="id59fb_c3 id59fb_l11">1911</td></tr>
<tr class="row_odd odd">
<th headers="id59fb_c0" id="id59fb_l12">Chelsea (SW3)</th>
<td class="numeric " headers="id59fb_c1 id59fb_l12">2752</td>
<td class="numeric " headers="id59fb_c2 id59fb_l12">4330</td>
<td class="numeric " headers="id59fb_c3 id59fb_l12">7617</td></tr>
<tr class="row_even even">
<th headers="id59fb_c0" id="id59fb_l13">Clapham (SW4)</th>
<td class="numeric " headers="id59fb_c1 id59fb_l13">1474</td>
<td class="numeric " headers="id59fb_c2 id59fb_l13">1938</td>
<td class="numeric " headers="id59fb_c3 id59fb_l13">2501</td></tr>
<tr class="row_odd odd">
<th headers="id59fb_c0" id="id59fb_l14">South Kensington (SW7)</th>
<td class="numeric " headers="id59fb_c1 id59fb_l14">3249</td>
<td class="numeric " headers="id59fb_c2 id59fb_l14">5258</td>
<td class="numeric " headers="id59fb_c3 id59fb_l14">8889</td></tr>
<tr class="row_even even">
<th headers="id59fb_c0" id="id59fb_l15">Battersea (SW11)</th>
<td class="numeric " headers="id59fb_c1 id59fb_l15">1572</td>
<td class="numeric " headers="id59fb_c2 id59fb_l15">2266</td>
<td class="numeric " headers="id59fb_c3 id59fb_l15">3205</td></tr>
</tbody>
</table>
<h5 class="spip">Edimbourg</h5>
<p>Les logements vides sont rares et la location est aléatoire. Les prix sont élevés et le choix des logements de qualité est limité. Pour un long séjour, il peut être préférable d’envisager l’achat.</p>
<p>Les quartiers résidentiels se situent notamment à West-End, New Town, Calton et Murrayfield.</p>
<p>Les baux sont de six mois, puis renouvelables tacitement chaque mois. Le loyer est payable d’avance. Un ou deux mois de caution sont demandés + un à deux mois de frais d’agence.</p>
<p>Les charges sont élevées notamment l’électricité et le gaz. Le coût de l’eau est compris dans les taxes locales.</p>
<p><strong>Les prix de la location à Edimbourg se situent dans les fourchettes suivantes (par mois)</strong></p>
<table class="spip">
<thead><tr class="row_first"><th id="id2f1c_c0">Quartiers</th><th id="id2f1c_c1">Studio</th><th id="id2f1c_c2">3 pièces</th><th id="id2f1c_c3">5 pièces</th><th id="id2f1c_c4">Villa</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<th headers="id2f1c_c0" id="id2f1c_l0">Quartier résidentiel</th>
<td headers="id2f1c_c1 id2f1c_l0">de 690 à 805£</td>
<td headers="id2f1c_c2 id2f1c_l0">de 1100 à 1725£</td>
<td headers="id2f1c_c3 id2f1c_l0">de 1840 à 2300£</td>
<td headers="id2f1c_c4 id2f1c_l0">2875£ et plus</td></tr>
<tr class="row_even even">
<th headers="id2f1c_c0" id="id2f1c_l1">Banlieue</th>
<td headers="id2f1c_c1 id2f1c_l1">de 460 à 575£</td>
<td headers="id2f1c_c2 id2f1c_l1">de 690 à 1150£</td>
<td headers="id2f1c_c3 id2f1c_l1">de 1165 à 1955£</td>
<td headers="id2f1c_c4 id2f1c_l1">1955£ et plus</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_3"></a>Hôtels</h3>
<p><strong>Prix moyen d’une chambre d’hôtel, petit déjeuner inclus (à titre indicatif)</strong></p>
<ul class="spip">
<li>grand tourisme : 235 £ (300 €),</li>
<li>moyen tourisme : 100 £ (130 €).</li></ul>
<p>Il existe également des <i>guest houses</i> qui offrent la chambre, le petit déjeuner, parfois le dîner (moins chères et avec d’excellentes prestations) et le <i>Bed and Breakfast</i> (chez l’habitant).</p>
<h4 class="spip">Auberges de jeunesse</h4>
<p><a href="http://www.yha.org.uk/" class="spip_out" rel="external">Youth Hostels Association</a><br class="autobr"><a href="http://www.ish.org.uk/" class="spip_out" rel="external">International Student House</a></p>
<h3 class="spip"><a id="sommaire_4"></a>Electricité</h3>
<p>La Grande Bretagne utilise un réseau d’électricité à courant de type alternatif, 220-240 volts, 50 Hz.</p>
<p>Un adaptateur pour les prises de courant au standard britannique (trois fiches plates) est nécessaire pour les appareils français.</p>
<h3 class="spip"><a id="sommaire_5"></a>Eau</h3>
<p>Le coût de l’eau est élevé (environ 400 £/an, soit 570 €). Pour le téléphone, le gaz, l’électricité, le marché est ouvert à la concurrence et permet l’accès à des services moins onéreux.</p>
<h3 class="spip"><a id="sommaire_6"></a>Electroménager</h3>
<p>Tout l’équipement pour la maison est disponible au Royaume-Uni à des conditions comparables à celles pratiquées en France. Les cuisines sont en général équipées, mais de façon partielle.</p>
<h4 class="spip">Chauffage et climatisation</h4>
<p>Le moyen de chauffage le plus répandu est le chauffage au gaz, collectif ou individuel.</p>
<p>A Edimbourg, en raison du climat, le chauffage doit être utilisé de manière intermittente même en été.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-royaume-uni-vie-pratique-article-loisirs-et-culture.md" title="Loisirs et culture">Loisirs et culture</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-royaume-uni-vie-pratique-article-communications.md" title="Communications">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-royaume-uni-vie-pratique-article-transports-109709.md" title="Transports">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-royaume-uni-vie-pratique-article-cout-de-la-vie.md" title="Coût de la vie">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-royaume-uni-vie-pratique-article-scolarisation-109707.md" title="Scolarisation">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-royaume-uni-vie-pratique-article-sante.md" title="Santé">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-royaume-uni-vie-pratique-article-logement.md" title="Logement">Logement</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/royaume-uni/vie-pratique/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
