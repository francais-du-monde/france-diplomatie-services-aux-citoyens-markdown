# Logement

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/vie-pratique/article/logement-111346#sommaire_1">Où se loger ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/vie-pratique/article/logement-111346#sommaire_2">Conditions de location</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/vie-pratique/article/logement-111346#sommaire_3">Hôtels</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/vie-pratique/article/logement-111346#sommaire_4">Auberges de jeunesse </a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/vie-pratique/article/logement-111346#sommaire_5">Electricité</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/vie-pratique/article/logement-111346#sommaire_6">Electroménager</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Où se loger ?</h3>
<p><strong>Prague</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Loyer mensuel </strong> <strong>quartier résidentiel</strong></td>
<td>couronnes tchèques</td>
<td>euros</td></tr>
<tr class="row_even even">
<td>Prix du M²</td>
<td>619</td>
<td>25</td></tr>
<tr class="row_odd odd">
<td>Studio</td>
<td>17 327</td>
<td>700</td></tr>
<tr class="row_even even">
<td>3 pièces</td>
<td>38 366</td>
<td>1 550</td></tr>
<tr class="row_odd odd">
<td>5 pièces</td>
<td>57 673</td>
<td>2 330</td></tr>
<tr class="row_even even">
<td><strong>Loyer mensuel banlieue</strong></td>
<td></td>
<td></td></tr>
<tr class="row_odd odd">
<td>Prix du M²</td>
<td>495</td>
<td>20</td></tr>
<tr class="row_even even">
<td>Studio</td>
<td>11 757</td>
<td>475</td></tr>
<tr class="row_odd odd">
<td>3 pièces</td>
<td>35 891</td>
<td>1 450</td></tr>
<tr class="row_even even">
<td>5 pièces</td>
<td>43 811</td>
<td>1 770</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_2"></a>Conditions de location</h3>
<p>Le marché locatif est actuellement en pleine expansion.. Bien qu’il soit possible de s’adresser à une agence (qui vous fera payer ses services au tarif d’un mois de loyer du bien loué) ou de faire passer une annonce, la meilleure solution reste souvent le bouche à oreille. Le délai de recherche est d’environ un mois.</p>
<p>Les quartiers résidentiels sont situés en centre-ville, sur les rives de la Vltava ou en banlieue ouest. Il n’est pas nécessaire de verser un pas-de-porte mais un à deux mois de loyer d’avance sont généralement demandés.</p>
<p>Les baux sont de un à trois ans, renouvelables. Un état des lieux est indispensable.</p>
<p>Les charges représentent environ 12% du montant du loyer, comprenant l’eau chaude, le chauffage et l’électricité des parties communes (il n’est pas rare que ces charges représentent un montant situé entre 3000 et 5000 couronnes par mois). Le coût mensuel d’une ligne téléphonique est de 450 couronnes (19€ environ).</p>
<h3 class="spip"><a id="sommaire_3"></a>Hôtels</h3>
<p><strong>Prague</strong></p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Prix moyen d’une chambre d’hôtel (chambre double)</strong></td>
<td>couronnes tchèques</td>
<td>euros</td></tr>
<tr class="row_even even">
<td>Grand tourisme</td>
<td>4 950</td>
<td>200</td></tr>
<tr class="row_odd odd">
<td>Moyen tourisme</td>
<td>1 980</td>
<td>80</td></tr>
</tbody>
</table>
<h3 class="spip"><a id="sommaire_4"></a>Auberges de jeunesse </h3>
<p>Il existe de nombreuses possibilités de séjours chez l’habitant ou dans les auberges de jeunesse.</p>
<p>Association tchèque des auberges de jeunesse :</p>
<p><a href="http://www.czechhostels.com/" class="spip_out" rel="external">Ceska hostelova asociace (CZYHA)</a> <br class="manualbr">Hastalska 24/729 <br class="manualbr">110 00 Praha 1 - Stare Mesto <br class="manualbr">Tél : (420) 224 826 380 <br class="manualbr">Fax : (420) 224 826 378 <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/vie-pratique/article/logement-111346#info#mc#czechhostels.com#" title="info..åt..czechhostels.com" onclick="location.href=mc_lancerlien('info','czechhostels.com'); return false;" class="spip_mail">Courriel</a></p>
<p>Chambres à Prague à partir d’environ 350 couronnes (environ 15 euros).</p>
<h3 class="spip"><a id="sommaire_5"></a>Electricité</h3>
<p>Le voltage est de 220 V et les prises de courant de type européen.</p>
<h3 class="spip"><a id="sommaire_6"></a>Electroménager</h3>
<p>Les cuisines sont en général équipées et l’on trouve sur place un bon choix en équipement électroménager.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/republique-tcheque/vie-pratique/article/logement-111346). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
