# Fiscalité du pays

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/fiscalite-23535/article/fiscalite-du-pays-114289#sommaire_1">Présentation</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/fiscalite-23535/article/fiscalite-du-pays-114289#sommaire_2">L’impôt sur les sociétés</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/fiscalite-23535/article/fiscalite-du-pays-114289#sommaire_3">Autres impôts</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/fiscalite-23535/article/fiscalite-du-pays-114289#sommaire_4">Barème de l’impôt </a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/fiscalite-23535/article/fiscalite-du-pays-114289#sommaire_5">Quitus fiscal </a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/fiscalite-23535/article/fiscalite-du-pays-114289#sommaire_6">Coordonnées des centres d’information fiscale </a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Présentation</h3>
<p><strong>L’impôt sur le revenu des personnes physiques</strong> (<i>podatek dochodowy od osób fizycznych</i>) est un impôt unitaire qui frappe les revenus des personnes physiques résidentes de Pologne ou des non-résidents au titre de revenus de source polonaise, sous réserve des dispositions des conventions fiscales internationales.</p>
<p>L’impôt sur le revenu des personnes physiques polonais (ci-après l’<strong>IRPP</strong>) est régi par les deux lois suivantes :</p>
<ul class="spip">
<li>la loi en date du 26 juillet 1991 relative à l’impôt sur le revenu des personnes physiques (J.O. de 2010, N° 51, texte 307, amendée), ci-après la <strong>Loi</strong> ;</li>
<li>la loi en date du 20 novembre 1998 relative à l’impôt sur le revenu forfaitaire sur certains revenus réalisés par les personnes physiques (J.O. N° 144, texte 930, amendée).</li></ul>
<p><strong>Les contribuables</strong></p>
<p>Les personnes physiques ayant leur domicile sur le territoire polonais sont, nonobstant leur nationalité, soumises en Pologne à <strong>une obligation fiscale illimitée</strong>.</p>
<p>Cela signifie que ces personnes sont soumises à l’IRPP sur l’intégralité de leurs revenus mondiaux, et ce quels que soient la nature ou le lieu de leur source de revenus.</p>
<p>La notion de personne ayant son domicile sur le territoire polonais désigne toute personne physique qui, soit a en Pologne son centre d’intérêts vitaux, soit séjourne sur le territoire polonais plus de 183 jours durant l’année fiscale donnée.</p>
<p>En revanche, les personnes physiques qui n’ont pas de domicile sur le territoire polonais sont soumises à l’IRPP, mais seulement au titre des revenus réalisés en Pologne, donc de source polonaise (<strong>obligation fiscale limitée</strong>).</p>
<p>Les revenus réalisés sur le territoire polonais (de source polonaise) s’entendent en particulier des revenus provenant :</p>
<p>1) d’un travail effectué sur le territoire polonais sur le fondement d’un contrat de mandat ou d’un contrat de travail, quel que soit le lieu de versement de la rémunération, <br class="manualbr">2) d’une activité économique réalisée personnellement sur le territoire polonais, quel que soit le lieu du versement de la rémunération, <br class="manualbr">3) d’un bien immobilier sis en Pologne, y compris de la location ou de la vente d’un tel bien immobilier.</p>
<p>L’imposition en Pologne des revenus de source polonaise s’applique sous réserve des dispositions des conventions fiscales tendant à éviter la double imposition auxquelles est partie la Pologne.</p>
<p><strong>La base imposable</strong></p>
<ul class="spip">
<li>La base imposable à l’IRPP inclut, en principe, tous les revenus catégoriels, c’est-à-dire par exemple les salaires, les primes et les revenus provenant de la location ou de la vente d’un bien meuble ou immeuble. La notion de revenu englobe en principe les revenus effectivement perçus ou laissés à la disposition du contribuable durant l’année civile sous forme de liquidités, de prestations en nature et de prestations à titre gratuit.</li>
<li>La Loi IRPP prévoit plusieurs exceptions à ce principe général. Ces exceptions concernent la notion de revenus, notamment de l’activité économique, de certains revenus immobiliers, de revenus occultes.</li></ul>
<p><strong>Déclaration et liquidation de l’IRPP</strong></p>
<p>L’ IRPP polonais est :</p>
<ul class="spip">
<li>prélevé au cours de l’année fiscale par le payeur sous forme d’acomptes mensuels progressifs (par exemple par l’employeur au titre des revenus provenant d’un contrat de travail) ou de revenu forfaitaire (par exemple à titre de paiement de dividendes),</li>
<li>versé au cours de l’année fiscale directement par le contribuable menant une activité économique non agricole, sous forme d’acomptes mensuels ou trimestriels (petits entrepreneurs débutant une activité), étant entendu que les contribuables peuvent choisir une forme simplifiée de versement des acomptes, sur le fondement des données au titre de l’année précédente ou des années fiscales,</li>
<li>versé par le contribuable directement lors de la liquidation annuelle de l’impôt au moment du dépôt de la déclaration annuelle des revenus (par exemple, les revenus au titre des pensions alimentaires).</li></ul>
<p><strong>Déclarations fiscales</strong></p>
<p>Chaque personne physique doit déclarer annuellement ses revenus et payer l’impôt avant le 30 avril de l’année suivante, déduction faite des éventuels acomptes mensuels réglés par le payeur (prélèvement à la source). Le calcul de l’impôt incombe donc au contribuable.</p>
<p>La notion de foyer fiscal n’existe pas en tant que telle.</p>
<p>Les époux peuvent, à leur demande et sous certaines conditions, être imposés conjointement.</p>
<p>Leurs revenus sont alors cumulés et la somme totale divisée en deux, l’impôt étant calculé sur cette base. L’impôt ainsi obtenu est ensuite multiplié par deux.</p>
<p>L’option de déclaration conjointe ne concerne que la déclaration annuelle des époux, les acomptes à verser par l’employeur s’appliquant à chacun individuellement. Il existe des allégements pour le calcul des acomptes dont peut bénéficier tout contribuable imposé avec son époux n’ayant pas de revenus propres. Chaque enfant permet de déduire 1173,70 PLN sur l’impôt annuel payé par les parents.</p>
<h4 class="spip">Imposition des non-résidents</h4>
<p>Les non-résidents sont soumis à l’IRPP sur leurs seuls revenus de source polonaise.</p>
<p>Pour certaines catégories de revenus, une retenue à la source est prélevée. Il s’agit des dividendes versés par des sociétés polonaises, intérêts, redevances et droits de licence. Pour les traitements et les salaires, la retenue à la source est identique à celle des personnes <strong>résidentes</strong>.</p>
<p>Les non-résidents qui réalisent des revenus provenant de sources polonaises sans l’intermédiaire de payeurs ou par l’intermédiaire de payeurs qui ne sont pas tenus de procéder chaque année au décompte de l’impôt, ou encore les non-résidents qui ont des revenus boursiers et qui envisagent de quitter le territoire polonais avant le 30 avril, sont tenus, sauf dans quelques exceptions, de déposer la déclaration pour l’année fiscale auprès de l’office fiscal compétent pour les non-résidents avant de quitter le territoire polonais.</p>
<h4 class="spip">Principales déductions du revenu </h4>
<p>Il existe peu de déductions du revenu imposable :</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  les salariés peuvent déduire de leur revenu imposable un certain montant forfaitaire annuel au titre des frais de revient. </p>
<p>Sont également déductibles du revenu imposable :</p>
<ul class="spip">
<li>les cotisations sociales (hors cotisations santé), les cotisations à des organismes professionnels,</li>
<li>les dons pour les institutions culturelles, éducatives, sportives, de protection de la santé, religieuses (à concurrence de 6% de revenu), les dépenses d’Internet, les dépenses liées aux handicapés.</li></ul>
<p>Sont déductibles de l’impôt les dépenses au titre des cotisations santé.</p>
<p>Par ailleurs, le contribuable peut opter pour le transfert de 1% de son impôt au bénéfice de certains organismes à vocation humanitaire ou sociale (la demande d’option s’effectue dans la déclaration fiscale annuelle).</p>
<p><strong>L’imposition forfaitaire</strong></p>
<p>La loi en date du 20 novembre 1998 relative à l’impôt sur le revenu forfaitaire sur certains revenus réalisés par les personnes physiques régit l’imposition de certains revenus (bénéfices) réalisés par les personnes physiques, notamment celles :</p>
<ul class="spip">
<li>exerçant une activité économique non agricole,</li>
<li>celles ayant réalisé des revenus au titre d’un contrat de location.Ce type d’imposition est particulièrement intéressant en ce qui concerne les revenus résultant de la location des biens immobiliers, dans la mesure où l’impôt est forfaitisé à 8,5 % du revenu brut.</li></ul>
<h3 class="spip"><a id="sommaire_2"></a>L’impôt sur les sociétés</h3>
<p>L’imposition à l’impôt sur le revenu des personnes morale est régie par la loi en date du 15 février 1992 sur l’impôt sur le revenu des personnes morales (J.O. de 2000, Nr 54, texte 654, amendée ultérieurement), dénommée ci-après la <strong>Loi CIT</strong>.</p>
<p>L’impôt sur le revenu des personnes morales (ci-après l’<strong>IS</strong>) frappe les revenus réalisés par des personnes morales qui sont :</p>
<ul class="spip">
<li>en particulier des sociétés de capitaux (SA et SARL de droit polonais),</li>
<li>des unités organisationnelles n’ayant pas de personnalité morale, à l’exception des sociétés n’ayant pas de personnalité morale, des contribuables étant des sociétés de capitaux en organisation et des <strong>succursales</strong>,</li>
<li>des groupements de capitaux fiscaux,</li>
<li>des sociétés n’ayant pas de personnalité morale ayant leur siège ou leur directoire dans un autre Etat, si elles sont, conformément aux dispositions légales de cet Etat, considérées comme des personnes morales et sont soumises dans cet Etat à l’imposition sur la totalité de leurs revenus quel que soit le lieu de réalisation de ces derniers.Les contribuables, s’ils ont leur siège ou leur lieu de direction effectif sur le territoire polonais, sont soumis à l’IS sur la totalité de leurs revenus, quel que soit leur lieu de réalisation.</li></ul>
<p>Les contribuables, s’ils n’ont pas leur siège ou directoire sur le territoire polonais, sont soumis à l’imposition seulement sur les revenus qu’ils réalisent sur le territoire polonais (de source polonaise). Dans le cas de ces derniers, si la détermination des revenus n’est pas possible à partir des livres comptables, le revenu est défini par le fisc de manière estimative, en appliquant des coefficients prédéfinis à appliquer sur les bénéfices.</p>
<p><strong>Base imposable</strong></p>
<p>Selon les principes généraux, fait l’objet de l’imposition au CIT l’ensemble des revenus nets réalisés par un contribuable, c’est-à-dire la différence entre la somme des bénéfices et les coûts fiscalement déductibles durant l’année fiscale ; si les coûts fiscalement déductibles excèdent la somme des bénéfices, la différence constitue une perte fiscale. Les pertes fiscales ne sont reportables en Pologne qu’en amont sur les cinq années fiscales consécutives, sachant que le montant de l’imputation fiscale ne peut excéder 50% du montant de la perte.</p>
<p>Certains revenus, par exemple les revenus à titre de participation dans les bénéfices des personnes morales (par exemple, les dividendes) et les revenus des entités étrangères au titre des redevances de licence et des intérêts réalisés par des non-résidents, sont soumis en Pologne à une imposition forfaitaire prélevée par voie de retenue à la source libératoire en Pologne au taux de 20 %, à 19%.</p>
<p>Le taux d’impôt au titre des dividendes, des redevances ou des intérêts peut être réduit en vertu des conventions fiscales internationales et de l’application des directives communautaires.</p>
<p>En cas de prestation de services intragroupes, il est nécessaire de respecter la réglementation du prix de transfert (prix de pleine concurrence) et de tenir la documentation dite de prix de transfert afin de pouvoir justifier le prix pratiqué.</p>
<p>Si le contribuable ne présente pas aux organes fiscaux une telle documentation transactionnelle, la différence entre le revenu déclaré par le contribuable et celui redressé par les organes fiscaux est soumise à l’imposition selon un taux de 50 %.</p>
<p>Les revenus fiscaux sont non seulement ceux effectivement perçus, mais en principe également ceux facturés et non payés, ainsi que d’autres valeurs, par exemple les différences de change, la prestation et les biens obtenus à titre gratuit ou partiellement gratuit, des choses, des droits ou d’autres prestations reçus.</p>
<p>Les coûts fiscalement déductibles s’entendent des coûts engagés afin de réaliser des revenus ou de garantir une source de revenus. Sont donc incluses dans les coûts fiscalement déductibles toutes les sortes de dépenses générales liées au fonctionnement d’une société, qui sont uniquement et indirectement liées aux revenus réalisés par le contribuable, par exemple les pénalités contractuelles.</p>
<p>Les coûts directement liés aux revenus sont calculés durant l’exercice fiscal lors duquel auront été réalisés les revenus liés à ces frais.</p>
<h4 class="spip">Taux principal</h4>
<p>Le taux de l’impôt sur les sociétés est de 19 %.</p>
<p>Les revenus au titre de la participation dans les bénéfices des personnes morales ayant leur siège social sur le territoire de la République de Pologne (dividendes, boni de liquidation) sont imposables, par voie de retenue à la source au taux de 19 %. Toutefois, il existe la possibilité d’exonérer les dividendes en application du régime mère-filiale.</p>
<h4 class="spip">Paiement de l’impôt sur les sociétés</h4>
<p>Les contribuables et les payeurs ne déposent pas durant l’année fiscale de déclarations fiscales, mais sont tenus de verser des acomptes. Cela concerne également l’impôt sur le revenu prélevé de manière forfaitaire. L’IS doit être payé par mensualités ou par versements trimestriels calculés sur une base mensuelle (trimestrielle) et payables jusqu’au 20 du mois (du trimestre) suivant.</p>
<p>Durant l’année fiscale, les contribuables peuvent également s’acquitter des acomptes sur l’impôt selon un système simplifié.</p>
<p>Avant l’expiration du troisième mois suivant la clôture de l’exercice fiscal, il convient de déposer auprès du bureau de l’administration fiscale dont dépend le siège de la société la déclaration fiscale et les comptes sociaux, ainsi que de procéder au paiement du solde de l’IS. La loi pénale fiscale exige de toute personne responsable d’une diminution mensongère du revenu imposable, le paiement d’une amende, voire une peine d’emprisonnement.</p>
<p>Dans les 15 jours suivant l’adoption de la résolution de validation des comptes sociaux, le bilan et les comptes certifiés ainsi que la résolution de l’assemblée générale doivent être adressés à l’administration fiscale.</p>
<h3 class="spip"><a id="sommaire_3"></a>Autres impôts</h3>
<h4 class="spip">Taxe sur la valeur ajoutée</h4>
<p>La TVA a été instaurée en 1993 et reprend la plupart des dispositions résultant de la VI<sup>ème</sup> directive ; elle évolue depuis en fonction de la réglementation communautaire.</p>
<p>Les personnes morales ou les entrepreneurs personnes physiques dont l’activité sera soumise à la TVA doivent s’enregistrer auprès de l’administration fiscale en tant qu’assujettis à la TVA.</p>
<p>Sont exonérés de la TVA certaines opérations ainsi que les contribuables qui ne dépassent pas un certain seuil de chiffre d’affaires.</p>
<p>Le seuil progresse de 50 % pour s’élever désormais à 150 000 zlotys (+/- 37 900 €).</p>
<p><strong>Paiement de la TVA</strong></p>
<p>La TVA doit être calculée et payée tous les mois, avant le 25 du mois suivant, les « petits » contribuables pouvant choisir une périodicité trimestrielle.</p>
<p><strong>Le taux</strong></p>
<p>Au 1<sup>er</sup> janvier 2011, les anciens taux de 7 % et de 22 % ont été portés à 8 % et <strong>23 %</strong>, ce dernier devenant le taux de base en Pologne ; le gouvernement s’est engagé à le réduire d’un point à 22% d’ici à 2016.</p>
<p>Il convient de relever qu’il s’agit de la première modification significative de ce taux depuis l’introduction de la TVA en Pologne en 1993. Par ailleurs a été créé un nouveau taux de TVA de 5 %, qui s’applique aux aliments de base ainsi qu’aux aliments non transformés.</p>
<h4 class="spip">Droits de succession et donation</h4>
<p>Cet impôt dépend de la valeur du bien transmis et du lien de parenté avec le défunt. C’est un impôt progressif. Les bénéficiaires sont répartis en trois groupes distincts : le premier concerne les membres de la famille la plus proche (épouse, ascendants, descendants, beau-fils, genre, belle-fille, frères et sœurs, beau-père, belle-mère et beaux-parents), le deuxième la famille plus éloignée, et enfin le troisième les tiers.</p>
<p>Les conventions fiscales de non-double imposition ne s’appliquent pas aux droits de succession et de donation.</p>
<p>Ainsi, il est possible de payer l’impôt sur la donation d’un bien immobilier se trouvant en Pologne dans ce pays même en tant que pays où se trouve le bien, et par exemple en France, si l’héritier y est domicilié fiscalement.</p>
<p>Fait en principe l’objet d’imposition toute acquisition par un bénéficiaire, par donation ou héritage, de la propriété de choses et de droits patrimoniaux dont la valeur nette excède :</p>
<p>1) 9637 PLN – si le bénéficiaire est une personne appartenant au groupe d’imposition I,</p>
<p>2) 7276 PLN – si le bénéficiaire est une personne appartenant au groupe d’imposition II.</p>
<p>Les donations et l’héritage au profit des bénéficiaires du Ier groupe sont, sous certaines conditions, totalement exonérés d’imposition.</p>
<p>L’impôt est calculé sur la valeur nette de la donation (de l’héritage).</p>
<h4 class="spip">Impôt sur les plus-values</h4>
<p>Le taux de l’impôt est identique à celui qui est applicable aux sociétés.</p>
<h4 class="spip">Impôts locaux</h4>
<p>C’est un impôt annuel (ci-après <strong>IF</strong>). Relèvent de l’IF toutes personnes ayant la qualité de propriétaires, de possesseurs ou d’usufruitiers perpétuels de terrains ou de leurs parties, d’immeubles ou de leurs parties, d’installations fixes ou de leurs parties liées à une activité commerciale autre que l’activité agricole ou forestière.</p>
<p>L’assiette de l’IF sur les terrains et les immeubles est déterminée en fonction de leur surface, celle de l’IF sur les installations fixes est égale à leur valeur comptable nette.</p>
<p>D’autres impôts doivent être mentionnés, et notamment, l’impôt sur les moyens de transport, l’impôt sur les actes de droit civil, les droits de timbre.</p>
<h3 class="spip"><a id="sommaire_4"></a>Barème de l’impôt </h3>
<p>Le taux de l’IRPP de droit commun est progressif.</p>
<p>La Pologne a décidé en 2007 la réduction à deux taux de l’IRPP, qui en comprenait auparavant trois. Le taux d’imposition en 2011 est le suivant :</p>
<table class="spip">
<tbody>
<tr class="row_odd odd">
<td><strong>Revenu annuel</strong> (en zlotys)</td>
<td><strong>Taux</strong></td></tr>
<tr class="row_even even">
<td>0 à 85 528</td>
<td>18%</td></tr>
<tr class="row_odd odd">
<td>au-delà</td>
<td>32%</td></tr>
</tbody>
</table>
<p>Toutefois, il existe des taux forfaitaires ou linéaires concernant certains revenus :</p>
<ul class="spip">
<li>taux linéaire de 19%, sur option, au titre des revenus provenant d’une activité économique non agricole,</li>
<li>taux forfaitaire de 19% d’impôt sur les revenus provenant des capitaux pécuniaires, par exemple de la cession à titre onéreux des valeurs mobilières ou d’instruments financiers dérivés (les revenus imposés de cette nature ne sont pas imposables avec les autres revenus catégoriels),</li>
<li>taux de 19% d’impôt sur les revenus obtenus lors d’une cession à titre onéreux de biens immobiliers acquis après le 31 décembre 2006.</li></ul>
<h3 class="spip"><a id="sommaire_5"></a>Quitus fiscal </h3>
<p>Des non- résidents sont, dans certains cas, tenus de déposer en avance une déclaration fiscale avant de quitter définitivement la Pologne, ainsi que tous de notifier au fisc le changement d’adresse.</p>
<h3 class="spip"><a id="sommaire_6"></a>Coordonnées des centres d’information fiscale </h3>
<p><a href="http://www.mf.gov.pl/ministerstwo-finansow" class="spip_out" rel="external">Ministère des Finances</a><br class="manualbr">Ministerstwo Finansów <br class="manualbr">ul. Swietokrzyska 12<br class="manualbr">00-916 Warszawa<br class="manualbr">Tel. (+48 22) 694 55 55</p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/pologne/fiscalite-23535/article/fiscalite-du-pays-114289). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
