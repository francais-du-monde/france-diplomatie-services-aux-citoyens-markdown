# Présence française

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/presence-francaise/#sommaire_1">Ambassade et consulat de France</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/presence-francaise/#sommaire_2">Autorités françaises dans le pays</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/presence-francaise/#sommaire_3">Associations dans le pays</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Ambassade et consulat de France</h3>
<p>Consultez notre article <a href="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/article/annuaire-des-representations-francaises-a-l-etranger" class="spip_in">Annuaire des représentations françaises à l’étranger</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Autorités françaises dans le pays</h3>
<h4 class="spip">Réseau français de coopération et d’action culturelle</h4>
<p>Les coordonnées des services du réseau de coopération et d’action culturelle français au Japon ainsi que la description de l’activité de ces services se trouvent dans la rubrique culture du site internet de l’Ambassade ou sur le site internet du réseau culturel : <a href="http://latitudefrance.diplomatie.gouv.fr/AnnuaireCarto.html" class="spip_out" rel="external">Latitude France</a>.</p>
<h4 class="spip">Economie : réseau mondial</h4>
<p>Business France, l’Agence française pour le développement international des entreprises, est présente au Japon. Sa mission est d’accompagner les entreprises françaises dans leur développement à l’international.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://export.businessfrance.fr/japon/export-japon-avec-notre-bureau.html" class="spip_out" rel="external">Site de la Mission économique Business France au Japon</a></p>
<p>Les services économiques sont également présents. Ils sont une émanation de la Direction générale du Trésor et ont pour missions principales l’analyse macroéconomique et l’animation des relations économiques bilatérales entre la France et les pays où ils sont implantés.</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.tresor.economie.gouv.fr/pays/japon" class="spip_out" rel="external">Site internet du service économique français au Japon</a></p>
<h4 class="spip">Vos élus à l’Assemblée des Français de l’étranger</h4>
<p>Pour toute information sur l’Assemblée des Français de l’étranger et pour connaître les conseillers et les Sénateurs qui représentent les Français établis hors de France dans votre circonscription, vous pouvez consulter les sites Internet suivants :</p>
<ul class="spip">
<li><a href="http://www.assemblee-afe.fr/" class="spip_out" rel="external">Assemblée des Français de l’étranger (AFE)</a></li>
<li>Sénat au service des Français de l’étranger : dossier spécifique au Japon sur <a href="http://www.senat.fr/expatries/dossiers_pays/japon.html" class="spip_out" rel="external">le site du Sénat</a></li></ul>
<h4 class="spip">Députés des Français de l’étranger</h4>
<p>La révision constitutionnelle du 23 juillet 2008, publiée au Journal officiel du 19 avril 2011, introduit la représentation à l’Assemblée nationale des Français établis hors de France. La création de onze circonscriptions législatives à l’étranger permet aux Français expatriés d’élire leur député à l’Assemblée nationale à partir du scrutin de 2012.</p>
<p>Pour plus d’information : <a href="http://www.votezaletranger.gouv.fr/" class="spip_out" rel="external">Votez à l’étranger</a></p>
<h3 class="spip"><a id="sommaire_3"></a>Associations dans le pays</h3>
<h4 class="spip">Associations françaises</h4>
<p><strong>Association Démocratique des Français à l’étranger</strong> (ADFE)</p>
<ul class="spip">
<li><a href="http://adfetokyo.wordpress.com/" class="spip_out" rel="external">ADFE Tokyo</a> <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/presence-francaise/#francaisdumonde.tokyo#mc#gmail.com#" title="francaisdumonde.tokyo..åt..gmail.com" onclick="location.href=mc_lancerlien('francaisdumonde.tokyo','gmail.com'); return false;" class="spip_mail">Courriel</a></li>
<li><a href="https://sites.google.com/site/adfekanjo/" class="spip_out" rel="external">Japon de l’Ouest</a><br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/presence-francaise/#adfejo#mc#gmail.com#" title="adfejo..åt..gmail.com" onclick="location.href=mc_lancerlien('adfejo','gmail.com'); return false;" class="spip_mail">Courriel</a></li></ul>
<p><a href="http://www.afj-japon.org/" class="spip_out" rel="external">Association des Français au Japon</a> (AFJ)<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/presence-francaise/#secretariat#mc#afj-japon.org#" title="secretariat..åt..afj-japon.org" onclick="location.href=mc_lancerlien('secretariat','afj-japon.org'); return false;" class="spip_mail">Courriel</a></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.afjkansai.org/" class="spip_out" rel="external">Section de Kansai</a> <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/presence-francaise/#ab#mc#afjkansai.org#" title="ab..åt..afjkansai.org" onclick="location.href=mc_lancerlien('ab','afjkansai.org'); return false;" class="spip_mail">Courriel</a></p>
<p><a href="http://www.japon-accueil.org" class="spip_out" rel="external">Japon Accueil</a> <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/presence-francaise/#japon-accueil#mc#afj-japon.org#" title="japon-accueil..åt..afj-japon.org" onclick="location.href=mc_lancerlien('japon-accueil','afj-japon.org'); return false;" class="spip_mail">Courriel</a></p>
<p>Cette association fait partie de la Fédération internationale des Accueils français et francophones à l’étranger (FIAFE).</p>
<h4 class="spip">Associations franco-japonaises</h4>
<p><a href="http://affjj.wordpress.com/" class="spip_out" rel="external">Association des familles franco-japonaises</a> (AFFJJ)<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/presence-francaise/#affjj2000#mc#yahoo.fr#" title="affjj2000..åt..yahoo.fr" onclick="location.href=mc_lancerlien('affjj2000','yahoo.fr'); return false;" class="spip_mail">Courriel</a></p>
<p>Il existe de nombreuses associations franco-japonaises dans les principales villes.</p>
<p>Une liste étendue des associations françaises et franco-japonaises peut être obtenue auprès du Consulat général de France à Osaka / rubrique Présence française.</p>
<p><strong>Pour en savoir plus : </strong></p>
<ul class="spip">
<li><a href="http://www.ambafrance-jp.org/Associations-francaises,6259" class="spip_out" rel="external">Liste des associations françaises maintenue à jour par l’Ambassade de France au Japon</a>.</li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-en-savoir-plus-article-associations-des-francais-de-l-etranger.md" class="spip_in">Notre article dédié aux associations</a></li></ul>
<p><i>Mise à jour : septembre 2015</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-japon-presence-francaise-article-presence-francaise.md" title="Présence française">Présence française</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/japon/presence-francaise/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
