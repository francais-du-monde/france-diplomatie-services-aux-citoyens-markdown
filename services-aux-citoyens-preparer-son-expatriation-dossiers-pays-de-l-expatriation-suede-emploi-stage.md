# Emploi, stage

<h2 class="rub23472">Marché du travail</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/emploi-stage/#sommaire_1">Secteurs à fort potentiel</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/emploi-stage/#sommaire_2">Secteurs à faible potentiel</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/emploi-stage/#sommaire_3">Professions règlementées</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/emploi-stage/#sommaire_4">Rémunération</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Secteurs à fort potentiel</h3>
<p>Les services à fort potentiel sont actuellement toute l’économie de services (informatique, soins médicaux), l’industrie (filière bois, traitement des déchets), l’enseignement (mathématiques, sciences, français).</p>
<p>Les secteurs pouvant offrir des débouchés sont également celles de l’hôtellerie, de la restauration, et du nettoyage, peu recherchées car mal rémunérées.</p>
<h3 class="spip"><a id="sommaire_2"></a>Secteurs à faible potentiel</h3>
<p>Les secteurs à déconseiller sont les télécommunications et l’industrie lourde. Les secteurs de la défense ne sont pas accessibles à un ressortissant étranger.</p>
<p>Pour plus d’informations sur le marché de l’emploi :</p>
<ul class="spip">
<li><a href="http://ec.europa.eu/index_fr.htm" class="spip_out" rel="external">Site de la Commission européenne</a></li>
<li><a href="http://www.ccfs.se/" class="spip_out" rel="external">Site de la chambre de commerce franco-suédoise</a> : service emploi</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Professions règlementées</h3>
<p>Il existe des professions réglementées en Suède (professions médicales, avocat, enseignant, comptable, traducteurs, etc.)</p>
<p>Liste complète en suédois des professions réglementées : <a href="http://ec.europa.eu/" class="spip_out" rel="external">site de la Commission européenne</a>.</p>
<h3 class="spip"><a id="sommaire_4"></a>Rémunération</h3>
<p>Contrairement à la France, il n’existe pas de salaire minimum unique dans le droit du travail. Ce sont les conventions collectives qui déterminent pour chaque catégorie de salaire des minima mensuels.</p>
<p>Le site officiel du <a href="http://www.sbc.se/" class="spip_out" rel="external">bureau des statistiques suédois</a> indique des moyennes de salaires pour de nombreux groupes professionnels. Les sites des syndicats peuvent également être une source d’information.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Site internet <a href="http://www.av.se/" class="spip_out" rel="external">Arbetsmiljö Sverket</a> </p>
<p><i>Mise à jour : juin 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-suede-emploi-stage-article-stages.md" title="Stages">Stages</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-suede-emploi-stage-article-entretien-d-embauche.md" title="Entretien d’embauche">Entretien d’embauche</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-suede-emploi-stage-article-lettre-de-motivation.md" title="Lettre de motivation">Lettre de motivation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-suede-emploi-stage-article-curriculum-vitae.md" title="Curriculum vitae">Curriculum vitae</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-suede-emploi-stage-article-recherche-d-emploi-113522.md" title="Recherche d’emploi">Recherche d’emploi</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-suede-emploi-stage-article-reglementation-du-travail-113521.md" title="Réglementation du travail">Réglementation du travail</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-suede-emploi-stage-article-marche-du-travail.md" title="Marché du travail">Marché du travail</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/suede/emploi-stage/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
