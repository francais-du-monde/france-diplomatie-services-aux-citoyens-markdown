# Scolarité en français

<p>Il existe plusieurs possibilités pour assurer la scolarisation des enfants expatriés :</p>
<ul class="spip">
<li>D’une part les <strong>écoles locales</strong> du pays d’accueil peuvent accueillir les enfants expatriés.</li>
<li>Par ailleurs, des <strong>établissements d’enseignement français</strong> et des <strong>programmes internationaux </strong> permettent d’éviter une rupture dans le suivi du programme scolaire français.</li></ul>
<p> Comment continuer à suivre un programme scolaire français en vivant à l’étranger ? Existe-t-il des aides financières pour la scolarisation des enfants à l’étranger ?</p>
<p>Vous  trouverez, dans ces rubriques,  différents contacts pour connaitre les modalités et les diverses possibilités de suivre l’enseignement français à l’étranger.</p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais-article-agence-pour-l-enseignement-francais-a-l-etranger-aefe.md" title="Agence pour l’enseignement français à l’étranger (AEFE) ">Agence pour l’enseignement français à l’étranger (AEFE) </a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais-article-mission-laique-francaise.md" title="Mission laïque française">Mission laïque française</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais-article-enseignement-a-distance.md" title="Centre national d’enseignement à distance (CNED)">Centre national d’enseignement à distance (CNED)</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-scolarite-en-francais-article-aide-a-la-scolarite.md" title="Dispositif de bourses scolaires">Dispositif de bourses scolaires</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/scolarite-en-francais/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
