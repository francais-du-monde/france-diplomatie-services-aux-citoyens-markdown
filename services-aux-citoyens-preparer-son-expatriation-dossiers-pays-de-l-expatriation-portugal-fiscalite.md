# Fiscalité

<h2 class="rub23031">Fiscalité du pays</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/fiscalite/#sommaire_1">Le système fiscal portugais en bref</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/fiscalite/#sommaire_2">Impôts directs et indirects</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/fiscalite/#sommaire_3">Impôts locaux</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/fiscalite/#sommaire_4">Inscription auprès du fisc</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Le système fiscal portugais en bref</h3>
<p>Les bases du système fiscal portugais actuel ont été fixées au milieu des années 1980 dans le cadre de l’harmonisation de la législation nationale avec les diverses directives européennes adoptées en la matière. Ces dispositions ont cependant fait l’objet de plusieurs réformes significatives ces dernières années.</p>
<p>Une réforme entrée en vigueur en 1989 a introduit l’impôt sur le revenu - dénommé IRS - et l’impôt sur les sociétés - IRC – tous deux adoptés par le décret-loi n°442- A/88. Cette réforme a conduit à la mise en place d’un régime d’imposition direct comparable à celui d’autres États membres de l’Union européenne.</p>
<p>Plus récemment, une réforme en date du 1er janvier 2006 a mis en place un nouveau mode de déclaration fiscale par voie électronique, obligatoire pour les entreprises, professions libérales ou entrepreneurs.</p>
<h4 class="spip">L’autorité portugaise de collecte des impôts</h4>
<p>L’entité que l’on désigne par le sigle <a href="http://www.portaldasfinancas.gov.pt/" class="spip_out" rel="external">ATA (<i>Autoridade Tributária e Aduaneira</i>)</a> est l’administration fiscale qui a pour mission d’assurer la liquidation et le recouvrement des impôts sur le revenu, sur le patrimoine et sur la consommation, ainsi que des droits de douane. Elle exerce le rôle d’inspection fiscale et veille au respect des accords internationaux, européens particulièrement, en collaborant avec les administrations des pays concernés. Enfin, l’ATA a un devoir d’information et d’orientation des contribuables, ce qui en fait l’interlocuteur de référence pour toute question relative au fonctionnement du système fiscal portugais.</p>
<p><strong>Coordonnées :</strong><br class="manualbr">Téléphone : +351 707 206 707</p>
<h4 class="spip">Année fiscale</h4>
<p>L’année fiscale correspond à l’année civile et les revenus de l’année N-1 sont déclarés au cours de l’année N au Centre des impôts de la zone de résidence, jusqu’au 15 mars pour les salariés (30 avril pour les autres catégories). Les déclarations de revenus doivent être effectuées au cours des mois de mars/avril dans un centre d’impôt.</p>
<h3 class="spip"><a id="sommaire_2"></a>Impôts directs et indirects</h3>
<h4 class="spip">L’impôt sur le revenu, IRS (Imposto sobre os Rendimentos das pessoas Singulares)</h4>
<p>L’IRS est un impôt progressif, calculé en fonction de la situation personnelle, ainsi que du type de revenus perçus par le contribuable au cours de l’exercice fiscal. Il fait l’objet d’une retenue à la source.</p>
<h5 class="spip">Règle de la domiciliation fiscale : personnes résidentes et non-résidentes</h5>
<p>Les personnes physiques résidentes sont imposables sur la totalité de leurs revenus indépendamment de l’origine de ces derniers. Est réputée résidente, toute personne qui demeure sur le territoire portugais plus de 183 jours (de manière continue ou discontinue) pendant l’année civile ou y demeure moins longtemps, mais qui dispose, au 31 décembre de cette même année, d’une résidence considérée par les autorités fiscales comme résidence principale. Les personnes physiques non-résidentes sont imposables au titre de l’IRS, seulement sur les revenus perçus sur le territoire portugais.</p>
<h5 class="spip">Calcul de l’impôt</h5>
<p>Il résulte de la différence entre le montant brut de chaque catégorie de revenus et les déductions spécifiques à chacune. Pour le calcul de l’IRS on distingue <strong>six catégories</strong> de revenus :</p>
<ul class="spip">
<li>A : Revenus Salariaux</li>
<li>B : Revenus des entreprises et des travailleurs indépendants (prestation de service, revenus de propriété intellectuelle ou d’activités commerciales…)</li>
<li>C : Activités industrielles et commerciales</li>
<li>D : Activités agricoles</li>
<li>E : Revenus de capitaux</li>
<li>F : Revenus fonciers</li>
<li>G : Augmentation du capital (plus-values)</li>
<li>H : Pensions (retraites, pensions d’invalidité, de réversion et autres pensions alimentaires)</li></ul>
<p><strong>NB</strong> : L’architecture de l’impôt sur le revenu connaît de grandes modifications compte tenu des politiques de restructuration fiscale entreprises par le Portugal depuis le début de la crise.</p>
<h4 class="spip">L’impôt sur les sociétés</h4>
<p> (<i>Imposto sobre os Rendimentos das pessoas Collectivas - IRC</i>)</p>
<p>Conformément à la réforme de 2004, cet impôt s’applique aux sociétés - aussi bien résidentes que non résidentes. Les critères de résidence et/ou d’origine des revenus déterminent son champ d’application.</p>
<h5 class="spip">L’application aux sociétés résidentes</h5>
<p>Par société résidente, on entend les sociétés commerciales ou civiles établies sous la forme de commerces, coopératives, compagnies nationalisées et les autres personnes morales de droit privé ou public, dotées ou non de la personnalité juridique, ayant des organes de direction sur le territoire portugais (siège social ou direction effective).</p>
<h5 class="spip">L’application aux sociétés non-résidentes</h5>
<p>Sont considérées comme non-résidentes, les sociétés, jouissant ou non de la personnalité juridique, qui n’ont pas d’organes de direction sur le territoire portugais et dont les revenus perçus sur le territoire portugais ne sont pas imposés au titre de l’IRS. Parmi les sociétés non-résidentes, il faut opérer une distinction selon qu’elles disposent ou non d’un établissement stable sur le territoire portugais.</p>
<p>On entend par « établissement stable » :</p>
<ul class="spip">
<li>toute installation fixe par le biais de laquelle est exercée une activité de nature commerciale, industrielle ou agricole et notamment un local de production, une succursale, un bureau.</li>
<li>toute personne, autre qu’un agent indépendant, agissant sur le territoire portugais pour le compte d’une entreprise non-résidente et pouvant habituellement conclure des contrats engageant l’entreprise dans la limite du cadre de ses activités. Par ailleurs, cette condition devra être appréciée au regard de la convention fiscale entre la France et le Portugal (article 5).</li></ul>
<h5 class="spip">Les avantages fiscaux de l’IRC</h5>
<p>Le système fiscal portugais offre un certain nombre d’allègements fiscaux, notamment dans le cadre du régime simplifié dont :</p>
<ul class="spip">
<li>des aides fiscales, de nature contractuelle, à l’investissement direct au Portugal. Ces aides se présentent sous diverses formes qui vont du crédit d’impôt à la réduction de la taxe municipale et de droit de timbre, en passant par l’exonération.</li>
<li>des exonérations totales ou partielles de l’impôt municipal sur les transmissions d’immeuble à titre onéreux, afin de promouvoir le développement industriel.</li>
<li>des incitations fiscales pour encourager les implantations à l’intérieur du pays.</li></ul>
<p><strong>NB</strong> : les réformes en cours au Portugal vont faire diminuer progressivement l’impôt sur les sociétés jusqu’à 2016.</p>
<h4 class="spip">La TVA</h4>
<p>Cet impôt indirect est entré en vigueur au Portugal le 1er janvier 1986 en application du régime légal fondé sur la directive européenne du 17 mai 1977 (« 6ème directive »), qui établit le système commun de la TVA dans les pays de l’Union européenne. Cette taxe fonctionne donc de la même manière qu’en France.</p>
<p>Le taux normal de TVA au Portugal est de 23 %.</p>
<h3 class="spip"><a id="sommaire_3"></a>Impôts locaux</h3>
<h4 class="spip">L’impôt municipal sur les biens immeubles, IMI (Imposto Municipal sobre Imóveis)</h4>
<p>C’est un impôt dû par les propriétaires ou usufruitiers, fondé sur la valeur nette taxable des biens immeubles, classés comme propriétés urbaines ou rurales sur le territoire portugais. Il s’agit d’un impôt collecté et utilisé par la municipalité où les biens sont sis.</p>
<h3 class="spip"><a id="sommaire_4"></a>Inscription auprès du fisc</h3>
<p>Il convient, pour être enregistré régulièrement en tant que contribuable au Portugal, de se présenter aux services de la Direction générale des impôts à l’adresse suivante :</p>
<p>Direcçao Geral dos Impostos (DGCI)<br class="manualbr">Avenida Ing. Duarte Pacheco n°28-60<br class="manualbr">1099-013 Lisboa<br class="manualbr">Tél : 21 383 42 00<br class="manualbr">n° azul (gratuit) : 21 383 06 00</p>
<p>Le site de l’administration fiscale permet un <a href="http://www.portaldasfinancas.gov.pt/" class="spip_out" rel="external">enregistrement en ligne</a>.</p>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><a href="http://www.impots.gouv.fr/" class="spip_out" rel="external">Ministère de l’économie et des finances</a> ;</li>
<li><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/fiscalite/#nonresidents#mc#dgi.finances.gouv.fr#" title="nonresidents..åt..dgi.finances.gouv.fr" onclick="location.href=mc_lancerlien('nonresidents','dgi.finances.gouv.fr'); return false;" class="spip_mail">Centre des impôts des non-résidents en France</a> ;</li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-fiscalite-article-convention-fiscale-111295.md" title="Convention fiscale">Convention fiscale</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-portugal-fiscalite-article-fiscalite-du-pays.md" title="Fiscalité du pays">Fiscalité du pays</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/fiscalite/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
