# Recherche d’emploi

<h4 class="spip">Outils pour la recherche d’emploi</h4>
<p><strong>Journaux</strong></p>
<p>Journal francophone "Cambodge Soir" <br class="manualbr">Tél. : 015 833 415 <br class="manualbr">Tél./Fax : 023 362 654</p>
<h4 class="spip">Organismes pour la recherche d’emploi</h4>
<p>Pour toute recherche d’emploi au Cambodge s’adresser :</p>
<ul class="spip">
<li>auprès des employeurs potentiels</li>
<li>la <a href="http://www.ccfcambodge.org/" class="spip_out" rel="external">Chambre de Commerce franco-cambodgienne</a><br class="manualbr">Office # 13 <br class="manualbr">Ground Floor <br class="manualbr">Hotel Cambodiana <br class="manualbr">313 Sisowath Quay <br class="manualbr">Phnom-Penh <br class="manualbr">Tél./Fax : (855) 23 221 453 <br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/cambodge/emploi-stage/article/recherche-d-emploi#ccfc#mc#online.com.kh#" title="ccfc..åt..online.com.kh" onclick="location.href=mc_lancerlien('ccfc','online.com.kh'); return false;" class="spip_mail">Courriel</a></li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/cambodge/emploi-stage/article/recherche-d-emploi). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
