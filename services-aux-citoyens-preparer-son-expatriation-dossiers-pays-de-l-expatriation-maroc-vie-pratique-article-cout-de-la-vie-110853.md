# Coût de la vie

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/vie-pratique/article/cout-de-la-vie-110853#sommaire_1">Monnaie et change</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/vie-pratique/article/cout-de-la-vie-110853#sommaire_2">Opérations bancaires</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/vie-pratique/article/cout-de-la-vie-110853#sommaire_3">Alimentation</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/vie-pratique/article/cout-de-la-vie-110853#sommaire_4">Evolution des prix</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Monnaie et change</h3>
<p>L’unité monétaire est le dirham (MAD).</p>
<p>Les cartes de paiement de type Visa, Eurocard/Mastercard sont acceptées dans les lieux touristiques, les restaurants, les supermarchés, les hôtels, les sociétés de location de voiture. On utilise beaucoup l’argent liquide pour régler les achats, surtout dans les souks et auprès des petits commerçants. La carte bancaire (l’option « internationale » étant parfois nécessaire) permet de retirer de l’argent dans la plupart des distributeurs automatiques. Il convient cependant de se renseigner auprès des commerçants pour savoir s’ils n’appliquent pas une surtaxe.</p>
<p>Toutes les banques sont de droit marocain, mais les banques françaises ont des participations dans de grandes banques marocaines. On recense notamment :</p>
<ul class="spip">
<li>BMCI (Banque Marocaine pour le Commerce et l’Industrie), filiale de BNP-Paribas,</li>
<li>Crédit du Maroc, filiale du Crédit Agricole,</li>
<li>SGMB (Société Générale Marocaine de Banque), filiale de la Société Générale.</li></ul>
<p>Pour ouvrir un compte au Maroc, il convient de se munir d’une pièce d’identité et d’un justificatif de domicile. Les étrangers peuvent ouvrir des comptes en dirhams, en dirhams convertibles ou en devises. L’avantage des comptes en dirhams convertibles est qu’ils permettent le transfert de fonds vers l’étranger et les règlements au Maroc. Seuls les détenteurs d’une carte de séjour (ou les Français possédant également la nationalité marocaine) peuvent ouvrir un compte en dirhams non convertibles.</p>
<p>Pour les virements, ne pas oublier de prendre en compte le délai nécessaire aux transferts de fonds entre la France et le Maroc, qui peut prendre de 7 à 20 jours. Pour plus de renseignements, consulter le site de l’<a href="http://www.oc.gov.ma/portal/fr/content/reglementation-changes" class="spip_out" rel="external">Office des changes</a>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Opérations bancaires</h3>
<p>La monnaie locale n’est pas librement convertible.</p>
<p>L’importation et l’exportation des dirhams sont strictement interdites. L’importation des devises est libre.</p>
<p>Les étrangers salariés ont l’autorisation de transférer 100% de leurs économies sur les revenus perçus en dirhams, ainsi que le montant correspondant à leurs cotisations sociales.</p>
<p>En matière de contrôle des changes :</p>
<ul class="spip">
<li>L’importation de devises en billets de banque d’un montant égal ou supérieur à la contre-valeur de 100 000 dirhams est obligatoirement soumise à une déclaration écrite à souscrire auprès du bureau douanier d’entrée ;</li>
<li>Les résidents sont tenus de céder les devises en billets de banque importées, quel que soit leur montant, sur le marché des changes ou de les verser dans leur compte en devises ou en dirhams convertibles et ce, dans un délai de 30 jours à compter de la date de leur retour au Maroc,</li>
<li>L’exportation de devises en billets de banque par les <strong>non-résidents </strong>d’un montant égal ou supérieur à la contre-valeur de 100 000 dirhams doit être justifiée au bureau douanier de sortie sur présentation notamment de la déclaration souscrite lors de l’entrée sur le territoire national ;</li>
<li>L’exportation de devises en billets de banque par les <strong>résidents </strong>doit être justifiée au bureau douanier de sortie, quelque soit leur montant.</li></ul>
<p>L’importation ou l’exportation du dirham sont tolérées dans la limite d’un montant de 1000 dirhams.</p>
<h3 class="spip"><a id="sommaire_3"></a>Alimentation</h3>
<p>L’approvisionnement courant ne pose pas de problème au Maroc.</p>
<h3 class="spip"><a id="sommaire_4"></a>Evolution des prix</h3>
<p>Pour connaître l’indice des prix à la consommation (IPCH), tous postes de dépenses confondues, consultez le site internet de la <a href="http://donnees.banquemondiale.org/indicateur/FP.CPI.TOTL" class="spip_out" rel="external">Banque mondiale</a>.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/maroc/vie-pratique/article/cout-de-la-vie-110853). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
