# Santé

<p>Pour une présentation des risques épidémiologiques ainsi que des conseils sur la médecine au quotidien, consultez les pages suivantes :</p>
<ul class="spip">
<li>Conseils, liste de médecins et hôpitaux sur le site du consulat de France à Lima ;</li>
<li>Page dédiée à la santé au Pérou sur le site Conseils aux voyageurs ;</li>
<li>Fiche Lima sur le site du <a href="http://www.cimed.org" class="spip_out" rel="external">CIMED</a>.</li></ul>
<h4 class="spip">Avant le départ</h4>
<p>Consultez votre médecin (éventuellement votre dentiste) et souscrivez à une compagnie d’assistance couvrant les frais médicaux et de rapatriement sanitaire.</p>
<p>En cas de soins sur place, à Lima, les cliniques et les hôpitaux privés sont bien équipés et les médecins, souvent formés en Europe ou aux Etats-Unis, sont en général de bon niveau. La médecine étant privée, les soins doivent généralement être réglés d’avance il est cependant important :</p>
<ul class="spip">
<li>de disposer sur soi d’informations sur sa couverture médicale</li>
<li>de s’enquérir si possible, du sérieux de l’établissement auquel on a l’intention de faire appel (le fait d’être privé n’étant pas une garantie absolue de qualité).</li></ul>
<h4 class="spip">Altitude</h4>
<p>Beaucoup de sites touristiques sont situés en altitude, des précautions s’imposent les deux premiers jours (repos, repas légers, éviter l’alcool).</p>
<p>Les personnes qui connaissent des problèmes cardiaques et qui souhaitent se rendre dans les villes d’altitude (Puno, Cusco, Huaraz, Arequipa) doivent prendre l’avis d’un médecin avant tout déplacement. A condition de le faire avec discernement, certains médicaments peuvent limiter les effets négatifs de l’altitude. Eviter cependant, certaines prescriptions à caractère exclusivement commercial (type : « Sorotchi Pills »).</p>
<h4 class="spip">Hygiène alimentaire, hygiène de l’eau</h4>
<p>Il est déconseillé de boire l’eau du robinet : préférer les eaux en bouteilles capsulées. Eviter la consommation d’aliment insuffisamment cuits, des glaces à l’eau, laver les fruits et légumes.</p>
<p><strong>Se protéger efficacement contre les moustiques,</strong> vecteurs du paludisme, ou de virus (comme la dengue ou le virus Oropouche) en utilisant des produits répulsifs adaptés aux pays tropicaux (applications répétées sur les vêtements et les parties de peau découvertes).</p>
<ul class="spip">
<li><strong>Paludisme</strong> : maladie parasitaire transmise par les piqûres de moustiques qui impose le recours à des mesures de protection individuelle (sprays, crèmes, diffuseurs électriques, moustiquaires…) A ces mesures, doit s’ajouter un traitement médicamenteux adapté à chaque individu : il convient de s’adresser à votre médecin habituel ou à un centre de conseils aux voyageurs. Le traitement devra être poursuivi après le retour en France pendant une période dont la durée varie en fonction du produit utilisé. Classification : zone 3 en Amazonie ; zone 1 ailleurs. Il est utile de se munir d’un répulsif contre les insectes dans les zones situées en-dessous de 1 500 mètres d’altitude, dans les régions chaudes et humides telles que l’Amazonie.</li>
<li><strong>Dengue</strong> : maladie virale propagée par un moustique, actif de jour. Il convient donc de respecter les mesures habituelles de protection (vêtements longs, produits anti-moustiques à utiliser sur la peau et sur les vêtements, diffuseurs électriques). La dengue pouvant prendre une forme potentiellement grave il est vivement recommandé de consulter un médecin en cas de fièvre (la prise d’aspirine est déconseillée). Les pluies et les chaleurs de l’été austral (décembre à avril) favorisent la multiplication des moustiques transmetteurs de ce virus. De ce fait, une épidémie de cette maladie se déclare chaque année à cette période, notamment dans le nord et l’ouest du pays. Il convient de se protéger en cas de déplacement dans ces régions.</li></ul>
<h4 class="spip">Infection par le virus HIV</h4>
<p>Il est recommandé de prendre toutes les précautions d’usage en la matière et d’éviter les comportements à risque.</p>
<h4 class="spip">L’ayahuasca</h4>
<p>L’ambassade de France met en garde les voyageurs contre l’usage de l’ayahuasca, plante hallucinogène utilisée par les chamanes en Amazonie. De nombreux guides touristiques peu fiables en proposent aux étrangers. L’usage de l’ayahuasca peut avoir des conséquences mortelles.</p>
<p><strong>Contacts utiles sur place :</strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Clinique anglo-américaine, <br class="manualbr">Alfredo Salazar 350<br class="manualbr">San Isidro <br class="manualbr">Tél. : 712 30 00</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Clinique Ricardo Palma, <br class="manualbr">av. Javier Prado Este 1066 <br class="manualbr">San Isidro <br class="manualbr">Tél. : 224 22 24/224 22 26</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Clinique Peruana-Japonesa, <br class="manualbr">Av. Paso de los Andes 675 <br class="manualbr">Pueblo Libre <br class="manualbr">Tél. : 218 10 17</p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  Clinique San Borja, <br class="manualbr">Av. guardia civil 337<br class="manualbr">San Borja <br class="manualbr">Tél. : 475 40 00</p>
<ul class="spip">
<li>Clinique Tezza, Av. El Polo 570 : 610.50.50</li>
<li>Clinique Santa Isabel, Av. guardia civil 135 ; San Borja : 475.77.77.</li>
<li>Cliniques Maison de Santé (San Isidro, Surco, Chorillos, Cercado de Lima) Tel : 619 60 00Pour de plus amples renseignements, vous pouvez consulter le site de l’<a href="http://www.invs.sante.fr/" class="spip_out" rel="external">INVS</a>, ainsi que celui de l’<a href="http://www.who.int/fr/index.html" class="spip_out" rel="external">OMS</a>, qui vous renseigneront sur l’état sanitaire de ce pays, ou les sites de <a href="http://www.pasteur-lille.fr/" class="spip_out" rel="external">l’institut Pasteur de Lille</a> et de <a href="http://www.pasteur.fr/" class="spip_out" rel="external">l’institut Pasteur de Paris</a>.</li></ul>
<p><i>Source : Rubrique « Conseils aux voyageurs » du site du ministère des affaires étrangères</i></p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/perou/vie-pratique/article/sante). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
