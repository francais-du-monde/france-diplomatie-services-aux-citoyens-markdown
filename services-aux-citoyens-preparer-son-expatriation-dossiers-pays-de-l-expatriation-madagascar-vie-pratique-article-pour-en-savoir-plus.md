# Pour en savoir plus

<h4 class="spip">Librairies spécialisées</h4>
<p><a href="http://www.librairieharmattan.com/" class="spip_out" rel="external">L’Harmattan</a><br class="manualbr">16 rue des Ecoles - 75005 Paris<br class="manualbr">Tél. : 01 40 46 79 10 <br class="manualbr">Télécopie : 01 43 29 86 20<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/vie-pratique/article/pour-en-savoir-plus#harmattan1#mc#wanadoo.fr#" title="harmattan1..åt..wanadoo.fr" onclick="location.href=mc_lancerlien('harmattan1','wanadoo.fr'); return false;" class="spip_mail">Courriel</a></p>
<p><strong>Itinéraires Livres Voyages</strong><br class="manualbr">60 rue Saint Honoré <br class="manualbr">75001 Paris<br class="manualbr">Tél. : 01 42 36 12 63 <br class="manualbr">Télécopie : 01 42 33 92 00<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/vie-pratique/article/pour-en-savoir-plus#itineraires#mc#itineraires.com#" title="itineraires..åt..itineraires.com" onclick="location.href=mc_lancerlien('itineraires','itineraires.com'); return false;" class="spip_mail">Courriel</a></p>
<p><a href="http://www.ulysse.fr/" class="spip_out" rel="external">Ulysse</a><br class="manualbr">26 rue Saint Louis en l’Ile <br class="manualbr">75004 Paris<br class="manualbr">Tél. : 01 43 25 17 35 <br class="manualbr">Télécopie : 01 43 29 52 10<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/vie-pratique/article/pour-en-savoir-plus#ulysse#mc#ulysse.fr#" title="ulysse..åt..ulysse.fr" onclick="location.href=mc_lancerlien('ulysse','ulysse.fr'); return false;" class="spip_mail">Courriel</a></p>
<h4 class="spip">Bibliographie</h4>
<p><strong>Economie, histoire, société :</strong></p>
<ul class="spip">
<li><i>Exporter à Madagascar</i>, Ubifrance, 2004 (Coll. L’essentiel d’un marché).</li>
<li><i>S’implanter à Madagascar</i>, Ubifrance, 2008</li>
<li><i>L’essentiel d’un marché Madagascar</i>, ubifrance, 2009</li>
<li><i>Madagascar. </i>Pierre Vérin. Karthala, 2000.</li>
<li><i>Madagascar, l’île de nulle part ailleurs</i>. A. Le Louvier, H. Bazouges. L’Harmattan, 1999.</li>
<li><i>Géopolitique et problèmes de Madagascar</i>. R. Rabemananjara. L’Harmattan, 1999.</li>
<li><i>Madagascar sous Ravalomanana, la vie politique malgache depuis 2001</i>, J.L. Vivier, 2007</li></ul>
<p><strong>Liste de publications pour la note pays</strong></p>
<ul class="spip">
<li><strong>Ranaivo Velomihanta(23/12/2013) :</strong> Plurilinguisme francophonie et formation des élites à Madagascar, 1795 2012, de la mixité des langues. L’Harmattan (Ed.).</li>
<li><strong>Sylvain Urfer (2012)</strong> : Madagascar une culture en péril ? No comment (Ed.)</li>
<li><strong>Toavina Ralambomahay (2011), </strong>Madagascar dans une crise interminable, Études africaines. L’Harmattan (Ed.)</li>
<li><strong>Toavina Ralambomahay, (2013) :</strong> Comparatif Madagascar – Maurice, Contribution au développement de Madagascar. L’Harmattan (Ed.)</li>
<li><strong>Jérôme Ballet, Mahefasoa Randrianalijaona (2011) :</strong> Vulnérabilité, insécurité alimentaire et environnement à Madagascar. Ethique Economique. L’Harmattan.</li></ul>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/madagascar/vie-pratique/article/pour-en-savoir-plus). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
