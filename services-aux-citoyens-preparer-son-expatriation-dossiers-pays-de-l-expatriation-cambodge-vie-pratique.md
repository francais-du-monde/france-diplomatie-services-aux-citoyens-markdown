# Vie pratique

<h2 class="rub22978">Logement</h2>
<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/cambodge/vie-pratique/#sommaire_1">Où se loger ?</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/cambodge/vie-pratique/#sommaire_2">Exemples des coûts de logement </a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/cambodge/vie-pratique/#sommaire_3">Conditions de location</a></li>
<li><a id="so_4" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/cambodge/vie-pratique/#sommaire_4">Hôtels</a></li>
<li><a id="so_5" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/cambodge/vie-pratique/#sommaire_5">Electricité</a></li>
<li><a id="so_6" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/cambodge/vie-pratique/#sommaire_6">Electroménager</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Où se loger ?</h3>
<p>Les bons quartiers sont Tuol Kork et BKK2 et les meilleurs quartiers sont : River Front (quai Sisowath)), Daun Penh et BKK1.</p>
<h3 class="spip"><a id="sommaire_2"></a>Exemples des coûts de logement </h3>
<p><strong>Maisons </strong></p>
<ul class="spip">
<li>Meublées
<br>— 3 chambres (200 m²) : 1500 euros (très bons quartiers) et 3000euros (meilleurs quartiers)</li>
<li>Non meublées
<br>— 3 chambres (200 m²) : 1200 euros (très bons quartiers) et 2500 euros (meilleurs quartiers)
<br>— 4 chambres (250 m²) : 1400 euros (très bons quartiers) et 3000 euros (meilleurs quartiers)</li></ul>
<p><strong>Appartements </strong></p>
<ul class="spip">
<li>Meublées
<br>— 2 chambres (80-120 m²) : 1200 euros (très bons quartiers) et 2200 euros (meilleurs quartiers)
<br>— 3 chambres (120-160 m²) : 1500 euros (très bons quartiers) et 2800 euros (meilleurs quartiers)</li>
<li>Non meublées
<br>— 1 chambre (50-70 m²) : 550 euros (très bons quartiers) et 900 euros (meilleurs quartiers)
<br>— 2 chambres (80-120 m²) : 1200 euros (très bons quartiers) et 1800 euros (meilleurs quartiers)
<br>— 3 chambres (120-160 m²) : 1400 (très bons quartiers) et 2500 euros (meilleurs quartiers)
<br>— 4 chambres (160-200 m²) : 1800 euros (très bons quartiers) et 2800 euros (meilleurs quartiers)</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Conditions de location</h3>
<p>Pour trouver un logement, il est conseillé de contacter d’autres expatriés dès l’arrivée et, éventuellement, d’avoir recours aux services d’une agence immobilière, ou encore passer une annonce dans un quotidien. Il ne faut pas hésiter à visiter de nombreux logements avant d’arrêter son choix. On peut trouver un appartement aussi bien qu’une villa, un logement vide ou partiellement meublé, généralement dans un délai compris entre deux et quatre semaines. On ne verse pas de pas de porte et on procède rarement à une reprise. La durée des baux est le plus souvent d’un an renouvelable et il est préférable d’effectuer un état des lieux. Le loyer est payable d’avance (deux à six mois). Dans le cas d’une location par agence, le montant de la commission représente un demi-mois de loyer. Enfin, il faut savoir que l’achat d’un logement n’est pas autorisé aux étrangers. Les charges sont directement payées par le locataire. Pour une villa, le coût de services tels que l’eau revient à 50 USD par mois et pour l’électricité entre 100 et 300 USD (voire plus).</p>
<h3 class="spip"><a id="sommaire_4"></a>Hôtels</h3>
<p>Grand choix d’hôtels pour tous les budgets. Voir sur Internet.</p>
<h3 class="spip"><a id="sommaire_5"></a>Electricité</h3>
<p>Le courant électrique est de 220 volts (50 hertz).</p>
<h3 class="spip"><a id="sommaire_6"></a>Electroménager</h3>
<p>Les cuisines ne sont pas toujours équipées. On trouve fréquemment cuisinières à gaz et réfrigérateurs.</p>
<p>Le chauffage est inutile ; la climatisation, en revanche, est nécessaire.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<h2>Dans cette rubrique</h2>
<ul class="spip">
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-cambodge-vie-pratique-article-loisirs-et-culture-111060.md" title="Loisirs et culture">Loisirs et culture</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-cambodge-vie-pratique-article-communications.md" title="Communications">Communications</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-cambodge-vie-pratique-article-transports.md" title="Transports">Transports</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-cambodge-vie-pratique-article-cout-de-la-vie.md" title="Coût de la vie">Coût de la vie</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-cambodge-vie-pratique-article-scolarisation-111056.md" title="Scolarisation">Scolarisation</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-cambodge-vie-pratique-article-sante.md" title="Santé">Santé</a></li>
<li><a href="services-aux-citoyens-preparer-son-expatriation-dossiers-pays-de-l-expatriation-cambodge-vie-pratique-article-logement-111054.md" title="Logement">Logement</a></li>
</ul>

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/cambodge/vie-pratique/). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
