# Marché du travail

<p class="chapo">
      La Colombie affiche un taux de chômage de 11.5% au 1er janvier 2011. Au plus fort de la crise, en 2009, le taux de chômage s’élevait à 12%. Les prévisions pour 2013 donnent une tendance à la baisse avec un chiffre indicatif de 10.5%. L’emploi informel est extrêmement important en Colombie avec plus de 60% de la population.
</p>
<p>On compte en Colombie <strong>22.6 millions de salariés</strong> dont un nombre important de travailleurs étrangers. Les secteurs qui embauchent le plus d’étrangers sont des secteurs clés où la main d’œuvre locale n’est pas suffisante, tels que le pétrole, les exploitations minières, le secteur bancaire ou encore le tourisme.</p>
<p>Le pays compte <strong>une centaine d’entreprises françaises</strong> de toute taille et tout secteur. Les grands groupes français sont notamment très impliqués dans les secteurs de la distribution, du pétrole et de l’environnement. Ainsi, dans le secteur de la distribution, on peut citer la présence du groupe Casino et de son concurrent Carrefour, tous deux comptants de nombreux points de vente sur le sol colombien. La France est également le deuxième investisseur étranger du secteur du pétrole. Parmi les entreprises françaises basées en Colombie on trouve également Renault, Michelin, AGF et Danone. La France est le premier employeur étranger avec 90 000 emplois directs et 250 000 emplois indirects.</p>
<p><strong>Attention</strong> : toute entreprise ne peut employer plus de 10% de main d’œuvre étrangère.</p>
<p><strong>Secteurs porteurs</strong></p>
<p>Les secteurs les plus dynamiques du marché colombien sont les grandes cultures industrielles et d’exportation (café, fruits et légumes, fleurs, sucre, palmier à huile, cacao, tabac, coton, bois…), l’élevage et l’industrie agroalimentaire. Parmi les secteurs porteurs on trouve également l’enseignement, la pharmacie, l’automobile, l’habillement ainsi que les secteurs miniers et électriques. Cependant, le principal secteur de l’économie colombienne reste celui des services en général et du tourisme en particulier.</p>
<p><strong>Rémunération</strong></p>
<p>Le salaire minimum légal est de 433 700 pesos colombiens par mois, ce qui correspond à environ 245 euros. Le salaire moyen brut s’élève, lui, à 675 211 pesos par mois.</p>
<p>Le coût du travail reste relativement faible comparé à celui d’autres pays d’Amérique Latine. En revanche la législation du travail est très protectrice des travailleurs concernant le respect du contrat de travail et les règles applicables à la couverture sociale.</p>
<p><i>Mise à jour : décembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/colombie/emploi-stage/article/marche-du-travail). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
