# Lettre de motivation

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/emploi-stage/article/lettre-de-motivation#sommaire_1">Rédaction</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/emploi-stage/article/lettre-de-motivation#sommaire_2">Modèles de lettre de motivation</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Rédaction</h3>
<ul class="spip">
<li>La graphologie étant peu utilisée en Espagne, <strong>la lettre de motivation doit être dactylographiée et non manuscrite </strong>(sauf indication contraire).</li>
<li>Elle doit tenir sur une page ou deux maximum.</li>
<li>Dans le cadre d’une candidature spontanée, la lettre doit être rédigée en espagnol, même si elle est adressée à une société française. S’il s’agit d’une réponse à une annonce, elle doit être rédigée dans la langue de l’annonce.</li>
<li>Il est nécessaire, dans la mesure du possible, d’adresser la lettre à une personne nominativement désignée, le responsable de sélection ou le responsable du département qui intéresse le candidat.</li></ul>
<p><strong>Globalement, la structure de la lettre est semblable à la lettre de motivation française</strong>.</p>
<h3 class="spip"><a id="sommaire_2"></a>Modèles de lettre de motivation</h3>
<p>Vous trouverez des modèles de lettres sur plusieurs sites, dont :</p>
<ul class="spip">
<li><a href="http://www.ua.es/es/index.html" class="spip_out" rel="external">Université d’Alicante</a></li>
<li>Site <a href="http://www.oficinaempleo.com/cand/carta1.htm" class="spip_out" rel="external">OficinaEmpleo.com</a></li></ul>
<p><strong>Pour en savoir plus :</strong></p>
<ul class="spip">
<li><strong>Le Cv, la lettre de motivation et l’entretien d’embauche en espagnol</strong> de Joselyne Studer-Laurens et Maria Cristina Simonin.</li>
<li><strong>200 modelos de curriculum</strong> de Martha Alicia Alles.</li></ul>
<p><i>Mise à jour : avril 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/espagne/emploi-stage/article/lettre-de-motivation). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
