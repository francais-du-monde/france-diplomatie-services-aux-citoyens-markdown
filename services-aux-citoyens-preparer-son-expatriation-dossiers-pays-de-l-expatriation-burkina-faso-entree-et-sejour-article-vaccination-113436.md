# Vaccination

<p>La vaccination contre la fièvre jaune est exigée à l’entrée dans le pays.</p>
<p>Les vaccinations suivantes sont recommandées pour des raisons médicales :</p>
<ul class="spip">
<li>recommandées systématiquement : fièvre jaune dès l’âge de six mois ; diphtérie, tétanos, poliomyélite : à jour ; hépatite A ; méningite w 135, forme plus rare et redoutable, très répandue au Burkina ; à partir de 50 ans, une recherche préalable des anticorps sériques est justifiée.</li>
<li>recommandées pour des séjours prolongés et/ou à risques : typhoïde à partir de l’âge de deux ans ; hépatite B ; rage (situation d’isolement ou accès au traitement sur place difficile) ; méningite à méningocoques A + C (longs séjours ou situation épidémique).</li></ul>
<p>Les enfants doivent avoir à jour toutes les vaccinations incluses dans le calendrier vaccinal français, en particulier pour les longs séjours : BCG dès le premier mois, rougeole dès l’âge de neuf mois.</p>
<p>Il est préférable de réaliser toutes les vaccinations nécessaires avant de partir, car une fois sur place, les difficultés d’approvisionnement ne sont pas rares.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="services-aux-citoyens-preparer-son-expatriation-sante.md" class="spip_in">Santé</a></p>
<p><i>Mise à jour : juin 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/burkina-faso/entree-et-sejour/article/vaccination-113436). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
