# Curriculum vitae

<h4 class="spip">Rédaction</h4>
<p>Les conseils généraux pour la rédaction des CV et lettre de motivation sont les conseils classiques.</p>
<p>Le CV doit être court, sobre, donner envie au lecteur de connaître la personne, cohérent et pertinent, faire état des résultats obtenus, refléter fidèlement le parcours professionnel, être attractif et irréprochable sur le plan de la présentation et de l’orthographe.</p>
<p>Le CV est généralement composé des parties suivantes :</p>
<ul class="spip">
<li>renseignements personnels</li>
<li>formation</li>
<li>langue et informatique</li>
<li>expérience professionnelle</li></ul>
<p>La photographie est facultative. Les trois types de CV :</p>
<ul class="spip">
<li><i>CV fonctionnel </i>qui fait ressortir un savoir-faire spécifique, conseillé aux personnes qui cherchent un premier emploi ou qui cherchent à se repositionner</li>
<li><i>CV chronologique</i>, idéal pour les personnes qui n’ont pas trop souvent changé d’employeur ou qui ne sont pas restées trop longtemps sans emploi.</li>
<li><i>CV chrono-fonctionnel </i>convenant aux personnes pouvant justifier de réels succès.</li></ul>
<p><i>Mise à jour : novembre 2013</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/tunisie/emploi-stage/article/curriculum-vitae). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
