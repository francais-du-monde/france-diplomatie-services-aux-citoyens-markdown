# Curriculum vitae

<h2 class="sommaire">Sommaire</h2>
<ul class="sommaire">
<li><a id="so_1" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/emploi-stage/article/curriculum-vitae#sommaire_1">Rédaction</a></li>
<li><a id="so_2" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/emploi-stage/article/curriculum-vitae#sommaire_2">Diplômes</a></li>
<li><a id="so_3" href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/emploi-stage/article/curriculum-vitae#sommaire_3">Modèles de CV</a></li></ul>
<h3 class="spip"><a id="sommaire_1"></a>Rédaction</h3>
<p>Auparavant très détaillé, le curriculum vitae portugais ressemble aujourd’hui de plus en plus aux CV des autres pays européens, bien qu’un certain nombre de pratiques traditionnelles persistent.</p>
<h4 class="spip">Définissez clairement vos objectifs</h4>
<p>Avant de rédiger votre CV, faites votre propre évaluation. Il faut penser à votre projet professionnel et orienter votre CV en fonction de l’objectif défini. Si votre intérêt se porte sur différents postes, créez un CV pour chacun d’entre eux.</p>
<h4 class="spip">Faites court</h4>
<p>Le CV est le résumé du parcours universitaire et professionnel. Il est inutile de fournir trop de détails : de plus amples explications pourront être données au cours de l’entretien. A titre indicatif, un bon CV ne doit pas excéder trois pages. Si le CV est trop long, il risque d’être rapidement écarté dès la première lecture.</p>
<h4 class="spip">Faites simple</h4>
<p>Dans le corps de votre CV, utilisez des symboles et des phrases courtes plutôt que de longs paragraphes. En retenant ce format, il sera plus facile pour le lecteur de passer rapidement votre CV en revue et d’en dégager l’essentiel.</p>
<h4 class="spip">Utilisez des mots d’action</h4>
<p>Ils donnent de la vie à votre CV. Pour renforcer cette impression au sein de votre énumération, faites commencer vos phrases par des mots d’action tels que : préparation (<i>preparação</i>), développement (<i>desenvolvimento</i>), suivi (<i>seguimento</i>), etc.</p>
<h4 class="spip">Adaptez votre CV à l’entreprise</h4>
<p>Il est important d’adapter son CV à l’entreprise dans laquelle vous souhaitez postuler à un emploi. Vous devez par conséquent vous renseigner sur son activité, le genre de personnes qui y travaillent et sur ses méthodes de travail. Ces informations peuvent être recherchées sur le site de l’entreprise, dans la presse économique, etc.</p>
<h4 class="spip">Rendez aisée la lecture de votre CV</h4>
<p>Laissez des espaces. Utilisez une police de caractères d’une taille au moins égale à 10 points et aérez votre CV. Utilisez de la couleur (sans en abuser) pour aider le lecteur à passer votre CV en revue le plus efficacement possible.</p>
<h4 class="spip">Retenir l’attention</h4>
<p>Si vous répondez à une annonce, n’oubliez pas que vous n’êtes pas le seul à vous porter candidat. Différentes techniques peuvent être utilisées pour retenir l’attention : utiliser un papier plus épais, un fond légèrement coloré, une police différente.</p>
<h4 class="spip">Soyez honnête</h4>
<p>Il est souvent tentant d’embellir son expérience ou de rajouter quelques mois d’expérience professionnelle. N’oubliez pas que si l’entreprise procède à des vérifications et découvre que vous avez menti, votre candidature a de grandes chances d´être définitivement écartée.</p>
<h4 class="spip">Présentation</h4>
<p>Le curriculum vitae est généralement présenté en cinq parties :</p>
<h5 class="spip">Le titre du CV</h5>
<p>C’est le premier élément orientant le tri des recruteurs. Il est recommandé si vous répondez à une annonce. Mettez en avant votre niveau d’études et la fonction à laquelle vous postulez. <strong>Par exemple</strong> : assistante commerciale, chargé de recrutement, etc. Si vous êtes jeune diplômé, insistez sur une expérience valorisante effectuée pendant vos études, à condition que celle-ci soit en rapport avec le poste pour lequel vous postulez. <strong>Par exemple</strong> : bilingue français-portugais, trésorier de l’association, etc. Mais le titre du CV est surtout affaire d’appréciation personnelle. Il vaut mieux ne pas en mettre plutôt que d’en choisir un maladroit, inadapté ou qui vous cantonne trop à un poste déterminé.</p>
<h5 class="spip">La formation et l’expérience professionnelle (Formação / Experienciâ profissional)</h5>
<p>Ces deux chapitres, indispensables, feront l’objet de deux rubriques séparées. La formation et l’expérience renseignent le recruteur sur votre environnement de développement et de compétence.</p>
<p>Ces deux rubriques doivent être rédigées dans l’ordre chronologique. Les expériences ou les formations les plus récentes doivent apparaître en premier car elles renseignent le recruteur sur ce qui l’intéresse le plus : le dernier diplôme et le dernier emploi.</p>
<p>L’expérience doit figurer en première place si vous avez peu de diplômes ou si vous avez déjà beaucoup d’expérience ou que vous n´êtes plus étudiant. La formation doit figurer en première place si vous avez peu d’expérience ou si vous êtes jeune diplômé. Attention à l’équivalence des diplômes.</p>
<h5 class="spip">Les langues</h5>
<p>Indiquez les langues que vous avez étudiées, ainsi que les séjours à l’étranger.</p>
<h5 class="spip">Connaissances en informatique</h5>
<p>Il est devenu incontournable de posséder la maîtrise d’un ou plusieurs logiciels. Citez les un par un : Excel, Word, Power Point, Photoshop, Internet, etc.</p>
<h5 class="spip">Centres d’intérêt</h5>
<p>Dans cette rubrique, vous placerez tout ce qui permettra au recruteur de mieux vous connaître d’un point de vue extra-professionnel. Rédigez cette partie en privilégiant les compétences nécessaires en entreprise (le sport peut mettre en avant votre sens du travail en équipe).</p>
<h3 class="spip"><a id="sommaire_2"></a>Diplômes</h3>
<p>Le titulaire d’un diplôme français peut exercer au Portugal comme dans n’importe quel autre pays de l’Union européenne. Il n’existe aucune restriction, sauf en ce qui concerne les professions réglementées.</p>
<p><strong>Attention à l’équivalence des diplômes sur votre CV :</strong></p>
<table class="spip">
<thead><tr class="row_first"><th id="idb5c2_c0">DIPLOMES FRANÇAIS</th><th id="idb5c2_c1">DIPLOMES PORTUGAIS</th></tr></thead>
<tbody>
<tr class="row_odd odd">
<td headers="idb5c2_c0">CAP/ BEP</td>
<td headers="idb5c2_c1">Instituto Profissional</td></tr>
<tr class="row_even even">
<td headers="idb5c2_c0">Baccalauréat</td>
<td headers="idb5c2_c1">12° ano</td></tr>
<tr class="row_odd odd">
<td headers="idb5c2_c0">DUT / BTS (*)</td>
<td headers="idb5c2_c1">pas d’équivalence</td></tr>
<tr class="row_even even">
<td headers="idb5c2_c0">Licence</td>
<td headers="idb5c2_c1">Licenciatura</td></tr>
<tr class="row_odd odd">
<td headers="idb5c2_c0">Master</td>
<td headers="idb5c2_c1">Mestrado</td></tr>
<tr class="row_even even">
<td headers="idb5c2_c0">Doctorat</td>
<td headers="idb5c2_c1">Doutoramento</td></tr>
</tbody>
</table>
<p>(*) traduction possible pour les BTS et DUT : « <i>Diploma de técnico superior </i>(<i>2 anos após o 12°</i>) et <i>Diploma Universitário Tecnológico </i>(<i>2 anos após o</i> <i>12°</i>) ».</p>
<p>Pour une demande de <strong>reconnaissance de formation professionnelle</strong>, s’adresser à :</p>
<p><a href="http://www.iefp.pt/" class="spip_out" rel="external">Instituto de Emprego e Formaçao Profissonal (IEFP)</a><br class="manualbr">Direccçao de Serviços de Avalioçao e Certificaçao<br class="manualbr">Lino Soares - Rua de Xabregas, 52 <br class="manualbr">1949-003 Lisboa<br class="manualbr">Tél. : (351) 21 861 41 00 <br class="manualbr">Télécopie : [351] 21 86 14 600<br class="manualbr"><a href="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/emploi-stage/article/curriculum-vitae#pnrq#mc#efp.pt#" title="pnrq..åt..efp.pt" onclick="location.href=mc_lancerlien('pnrq','efp.pt'); return false;" class="spip_mail">Courriel</a></p>
<p>Pour une demande de <strong>reconnaissance de diplôme</strong>, s’adresser à :</p>
<p><strong>Ministerio da Educaçao</strong><br class="manualbr">Gabinete de Planeamento, Estratégia, Avaliação e Relações Internacionais<br class="manualbr">Av. 5 de Outubro, 107 <br class="manualbr">1069-018 Lisboa<br class="manualbr">Tél. : (351) 21 793 12 91 <br class="manualbr">Télécopie : (351) 21 797 89 94</p>
<p>Ou consulter :</p>
<ul class="spip">
<li><a href="http://www.enic-naric.net/" class="spip_out" rel="external">Centres nationaux d’information sur la reconnaissance des diplômes</a> (ENIC : European Network of Information Centres - NARIC : National Academic Recognition Information Centres)</li>
<li>Conseiller culturel chargé de l’enseignement auprès de <a href="http://www.embaixada-portugal-fr.org/" class="spip_out" rel="external">l’ambassade du Portugal à Paris</a>.</li></ul>
<h3 class="spip"><a id="sommaire_3"></a>Modèles de CV</h3>
<p>Le <a href="http://europass.cedefop.europa.eu/" class="spip_out" rel="external">curriculum vitae Europass</a>  est de plus en plus utilisé au Portugal. C’est un document de soutien à la mobilité dont l’objectif est d’aider les ressortissants des pays européens à démontrer clairement et facilement leurs qualifications et leurs compétences partout en Europe.</p>
<p><strong>Pour en savoir plus : </strong></p>
<p class="spip puce_simple_box"><span class="puce_simple"><img src="http://www.diplomatie.gouv.fr/fr/local/cache-vignettes/L4xH8/puce_noire-d6f89.png" alt="-" width="4" height="8"></span>  <a href="http://www.web-emprego.com/modelos-de-curriculum-vitae/" class="spip_out" rel="external">Autres modèles de CV</a>.</p>
<p><i>Mise à jour : janvier 2014</i></p>
<hr class="partage-pied">

----

<small>Cette fiche est issue de [France Diplomatie](http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation/dossiers-pays-de-l-expatriation/portugal/emploi-stage/article/curriculum-vitae). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
